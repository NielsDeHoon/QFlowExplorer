#pragma once

#include "qflowexplorer.h"

#include "qfePoint.h"
#include "qfeVector.h"
#include "qfeMatrix4f.h"
#include <vector>
#include <gl/glew.h>
#include "vtkCamera.h"
#include "qfeGLShaderProgram.h"
#include <array>

enum qfeSeedEllipsoidActionState {
  seedEllipsoidNone,
  seedEllipsoidStart
};

enum qfeGuidingPointState {
  guidingPointNone = 0,
  guidingPointOne,
  guidingPointTwo,
  guidingPointThree
};

/**
* \file   qfeSeedVolume.h
* \author Arjan Broos and Niels de Hoon
* \class  qfeSeedVolume
* \brief  Volume definitions containing seed points for flow visualization
* \note   Confidential
*
* A volume containing seed points for flow visualization
*/

class qfeSeedVolume {
public:
  qfeSeedVolume();
  virtual ~qfeSeedVolume()
  {
	  //shader will be cleaned up automatically
	  //if this line is enabled it will be cleaned up twice...
	  //delete shader;
  };

  void qfeSetGuidePoints(qfePoint _dia1, qfePoint _dia2, qfePoint _apex);
  void qfeGetGuidePoints(qfePoint &_dia1, qfePoint &_dia2, qfePoint &_apex);
  void qfeSetSeedOnClipPlane(bool on);

  void qfeSetVtkCamera(vtkCamera *camera, int _vp_width, int _vp_height);

  virtual float qfeGetVolume()
  {
	  std::cout<<"Unimplemented purely virtual function called"<<std::endl;

	  return 0.0f;
  };//purely virtual
  virtual bool IsInVolume(const qfePoint& p) const
  {
	  std::cout<<"Unimplemented purely virtual function called"<<std::endl;

	  return false;
  }; //purely virtual  
  virtual void qfeRenderVolume(const qfeColorRGB &color, bool surface, bool selected, bool useClipPlane)
  {
	  std::cout<<"Unimplemented purely virtual function called"<<std::endl;
  }; //purely virtual

  void qfeGenerateSurfacePoints();
  void GenerateSeedPointsOnClipPlaneSurface(unsigned nrPoints);
  vector<qfePoint>& qfeGetSurfacePoints();

  void qfeReset();
  void qfeClearSeedPoints();
  void qfeGenerateSeedPoints(unsigned nrPoints);
  void qfeGenerateSeedPointsNotIn(unsigned nrPoints, qfeSeedVolume *other);
  vector<qfePoint>& qfeGetSeedPoints();

  // Decrease or increase the radius along axis orthogonal to plane in which guide points reside
  void qfeSquish();
  void qfeStretch();

  bool IsInFrontOfClippingPlane(const qfePoint &p);
  bool qfeIsPlaced() const;

  void qfeRenderSeedPoints();
  void qfeRenderAxes();

  qfeMatrix4f qfeGetTMatrix() const;
  qfeMatrix4f qfeGetTInverseMatrix() const;
  qfePoint qfeGetCenter() const;
  qfeVector qfeGetAxis1() const;
  qfeVector qfeGetAxis2() const;
  qfeVector qfeGetAxis3() const;
  float qfeGetLambda1() const;
  float qfeGetLambda2() const;
  float qfeGetLambda3() const;

  qfePoint qfeGetDia1() const;
  qfePoint qfeGetDia2() const;
  qfePoint qfeGetApex() const;

//private:
protected:
  void GenerateSeedPointsInVolume(unsigned nrPoints);
  void GenerateSeedPointsOnClipPlane(unsigned nrPoints);
  void CalculateTMatrix();
  void CalculateBillboardCorners(const qfePoint &eye, const qfeVector &up);
  void GenerateUnitSphereSurfacePoints();
  void TransformUnitSphereToEllipsoidSurfacePoints();
  float GetRandomFloat(float min, float max);
  qfePoint GeneratePointInBoundingBox();
  qfePoint GeneratePointInClipRect();

  qfePoint WorldToParameter(const qfePoint &p) const;
  qfePoint ParameterToWorld(const qfePoint &p) const;
  qfeVector WorldToParameter(const qfeVector &v) const;
  qfeVector ParameterToWorld(const qfeVector &v) const;

  qfePoint dia1;
  qfePoint dia2;
  qfePoint apex;

  bool seedOnClipPlane;

  qfeVector axis1, axis2, axis3; // Orthonormal basis
  float lambda1, lambda2, lambda3; // Radii along basis vectors
  qfePoint center;

  // Transformation matrix
  // Transforms points from unit sphere to our ellipse
  qfeMatrix4f T;
  qfeMatrix4f TInv;

  // Corners of the billboard used to render imposter ellipsoid
  // From top-left to top-right, counter-clockwise
  qfePoint bbCorners[4];
  qfePoint pBBCorners[4]; // Parameter space version

  // Parameter space eye point of camera
  qfePoint pEye;

  // Pointer to camera, queried for billboarding
  vtkCamera *camera;
  int vp_width;
  int vp_height;

  // Rendering shader
  qfeGLShaderProgram *shader;

  // Vector in clipping plane, orthogonal to axis3
  // Used to specify rectangle around intersection between ellipsoid and clipping plane
  qfeVector clip;

  qfeVector clipNormal; // Normal that, with center point, defines clipping plane

  // True if guide points have been set
  bool placed;
  
  vector<qfePoint> surfacePoints;
  vector<qfePoint> seedPoints;

  GLint current_fbo_id;
  qfeReturnStatus qfeSetUpFBO(GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &fbo_depth_id);
  qfeReturnStatus qfeDisableFBO();
  qfeReturnStatus qfeEnableFBO(GLuint &fbo_id);
  qfeReturnStatus qfeDeleteFBO(GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &fbo_depth_id);

  void createTexture(GLuint *textureName, GLenum pname1, GLenum pname2, GLenum pname3, GLenum pname4, GLfloat param1, GLfloat param2, GLfloat param3, GLfloat param4, GLint level, GLenum format, GLenum type, int screenSize_x, int screenSize_y,GLfloat *data)
  {
	  if (*textureName > 0)
	  {
		  glDeleteTextures(1, textureName);
	  }
	  glGenTextures(1, textureName);
	  glBindTexture(GL_TEXTURE_2D, *textureName);
	  glTexParameterf(GL_TEXTURE_2D, pname1, param1);
	  glTexParameterf(GL_TEXTURE_2D, pname2, param2);
	  glTexParameterf(GL_TEXTURE_2D, pname3, param3);
	  glTexParameterf(GL_TEXTURE_2D, pname4, param4);
	  glTexImage2D(GL_TEXTURE_2D, 0, level, screenSize_x, screenSize_y, 0, format, type, data);
	  glBindTexture(GL_TEXTURE_2D, 0);
  }
};

class qfeSeedEllipsoid : public qfeSeedVolume
{
public:
  qfeSeedEllipsoid();

  virtual float qfeGetVolume();

  virtual bool IsInVolume(const qfePoint& p) const;

  virtual void qfeRenderVolume(const qfeColorRGB &color, bool surface, bool selected, bool useClipPlane);
};

class qfeSeedCylinder : public qfeSeedVolume
{
public:
  qfeSeedCylinder();

  virtual float qfeGetVolume();

  virtual bool IsInVolume(const qfePoint& p) const;

  virtual void qfeRenderVolume(const qfeColorRGB &color, bool surface, bool selected, bool useClipPlane);
  virtual void qfeRenderVolume(const qfeColorRGB &color, bool surface, bool selected, bool useClipPlane, bool planar);
};