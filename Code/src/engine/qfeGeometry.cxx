#include "qfeGeometry.h"

//----------------------------------------------------------------------------
qfeGeometry::qfeGeometry()
{
  this->geometryType    = -1;
  this->geometryChanged = false;
};

//----------------------------------------------------------------------------
qfeGeometry::qfeGeometry(const qfeGeometry &geo)
{
  this->origin          = geo.origin;
  this->axis[0]         = geo.axis[0];
  this->axis[1]         = geo.axis[1];
  this->axis[2]         = geo.axis[2];
  this->extent[0]       = geo.extent[0];  
  this->extent[1]       = geo.extent[1];  
  this->extent[2]       = geo.extent[2];  
  this->geometryType    = geo.geometryType;
  this->geometryChanged = geo.geometryChanged;
};

//----------------------------------------------------------------------------
qfeGeometry::~qfeGeometry()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeGeometry::qfeGetGeometryType(int& type)
{
  type = this->geometryType;

  return qfeSuccess;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeGeometry::qfeSetGeometryChanged(bool changed)
{
  this->geometryChanged = changed;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
bool qfeGeometry::qfeIsGeometryChanged()
{
  return this->geometryChanged;
}