#pragma once

#include "qflowexplorer.h"

#include "qfeGrid.h"
#include "qfePoint.h"
#include "qfeVector.h"

#include <gl/glew.h>

#include <iostream>
#include <string>
#include <math.h>

using namespace std;

class qfeVolume;

typedef vector< qfeVolume * >   qfeVolumeSeries;

#define QFE_VOL_SCALARS 0
#define QFE_VOL_VECTORS 1

//#define MAX(a, b) (((a) > (b)) ? (a) : (b))
//#define MIN(a, b) (((a) < (b)) ? (a) : (b))

/**
* \file   qfeVolume.h
* \author Roy van Pelt
* \class  qfeVolume
* \brief  Implements a 3D raster of voxels.
* \note   Confidential
*
* A volume is a 3D raster of voxels. A volume consists of
* a number of planes or slices; each consisting of a number of rows
* each consisting of a number of voxels. A voxel is a scalar value 
* representing a single sample of an image volume.
*
*/
class qfeVolume
{
public:
  qfeVolume(qfeGrid *grid = NULL, qfeValueType t = qfeValueTypeGrey, 
            unsigned int nx = 1, unsigned int ny = 1, unsigned int nz = 1, void *v = NULL, 
            qfeFloat tt = 0.0, qfeFloat pd = 0.0, qfeFloat vl = 0, qfeFloat vu = 0, unsigned int nc = 1);
  qfeVolume(const qfeVolume &volume);
  ~qfeVolume();

  qfeReturnStatus qfeSetVolumeGrid(qfeGrid *grid);
  qfeReturnStatus qfeGetVolumeGrid(qfeGrid **grid);

  qfeReturnStatus qfeSetVolumeData(qfeValueType t, unsigned int nx, unsigned int ny, unsigned int nz, void *v);
  qfeReturnStatus qfeSetVolumeData(qfeValueType t, unsigned int nx, unsigned int ny, unsigned int nz, unsigned int c, void *v);

  qfeReturnStatus qfeGetVolumeData(qfeValueType& t, unsigned int& nx, unsigned int& ny, unsigned int& nz, void **v);

  qfeReturnStatus qfeGetVolumeValueType(qfeValueType& t);

  qfeReturnStatus qfeGetVolumeSize(unsigned int& nx, unsigned int& ny, unsigned int& nz);

  qfeReturnStatus qfeSetVolumeValueDomain(qfeFloat vl, qfeFloat vu);
  qfeReturnStatus qfeGetVolumeValueDomain(qfeFloat& vl, qfeFloat& vu);

  qfeReturnStatus qfeSetVolumeVenc(qfeFloat _vencX, qfeFloat _vencY, qfeFloat _vencZ);
  qfeReturnStatus qfeGetVolumeVenc(qfeFloat &_vencX, qfeFloat &_vencY, qfeFloat &_vencZ);

  qfeReturnStatus qfeSetVolumeComponentsPerVoxel(unsigned int nc);
  qfeReturnStatus qfeGetVolumeComponentsPerVoxel(unsigned int &nc);

  qfeReturnStatus qfeSetVolumeTriggerTime(qfeFloat tt);
  qfeReturnStatus qfeGetVolumeTriggerTime(qfeFloat &tt);

  qfeReturnStatus qfeSetVolumePhaseDuration(qfeFloat pd);
  qfeReturnStatus qfeGetVolumePhaseDuration(qfeFloat &pd);

  qfeReturnStatus qfeGetVolumeType(int &t);
  qfeReturnStatus qfeGetVolumeTextureId(GLuint &tid);
  qfeReturnStatus qfeSetVolumeTextureId(GLuint tid);

  qfeReturnStatus qfeSetVolumeLabel(string  label);
  qfeReturnStatus qfeGetVolumeLabel(string& label);

  qfeReturnStatus qfeGetVolumeScalarAtPosition(int px, int py, int pz, float &v);

  //! Point (px, py, pz) in voxel coordinates
  //! Vector v in patient coordinates
  qfeReturnStatus qfeGetVolumeVectorAtPosition(int px, int py, int pz, qfeVector &v);

  //! Point p in voxel coordinates
  //! Vector v in patient coordinates
  qfeReturnStatus qfeGetVolumeVectorAtPositionLinear(qfePoint p, qfeVector &v);

  qfeReturnStatus qfeGetVolumeNorm(qfeVolume **norm);
  qfeReturnStatus qfeGetVolumeHistogram(int **histogram, int &size);

  qfeReturnStatus qfeUploadVolume();
  qfeReturnStatus qfeUpdateVolume();

  void qfeSetHistogramToNull();
  
protected:

  qfeGrid     *grid;
  qfeValueType valueType;
  unsigned int numberVoxelsX, numberVoxelsY, numberVoxelsZ;
  qfeFloat     lowerVoxelValue, upperVoxelValue;
  qfeFloat     vencX, vencY, vencZ;
  unsigned int numberComponents;
  qfeFloat     triggertime;
  qfeFloat     phaseduration;
  void        *voxels;

  int         *histogram;
  int          histogramsize;

  GLuint       texId;
  string       label;  

};
