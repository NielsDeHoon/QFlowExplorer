#include "qfeGrid.h"

//----------------------------------------------------------------------------
qfeGrid::qfeGrid(const qfeGrid &grid)
{
  this->origin[0]  = grid.origin[0];  
  this->origin[1]  = grid.origin[1];  
  this->origin[2]  = grid.origin[2];

  this->axis[0][0] = grid.axis[0][0];
  this->axis[0][1] = grid.axis[0][1];
  this->axis[0][2] = grid.axis[0][2];
  this->axis[1][0] = grid.axis[1][0];
  this->axis[1][1] = grid.axis[1][1];
  this->axis[1][2] = grid.axis[1][2];
  this->axis[2][0] = grid.axis[2][0];
  this->axis[2][1] = grid.axis[2][1];
  this->axis[2][2] = grid.axis[2][2];

  this->extent[0]  = grid.extent[0]; 
  this->extent[1]  = grid.extent[1]; 
  this->extent[2]  = grid.extent[2]; 

  this->orient     = grid.orient;
};

//----------------------------------------------------------------------------
qfeGrid::qfeGrid(qfeFloat ox, qfeFloat oy, qfeFloat oz,
                 qfeFloat axx, qfeFloat axy, qfeFloat axz,
                 qfeFloat ayx, qfeFloat ayy, qfeFloat ayz,
                 qfeFloat azx, qfeFloat azy, qfeFloat azz,
                 qfeFloat ex, qfeFloat ey, qfeFloat ez)
{
  this->qfeSetGridOrigin(ox, oy, oz);
  this->qfeSetGridAxes(axx, axy, axz, ayx, ayy, ayz, azx, azy, azz);
  this->qfeSetGridExtent(ex, ey, ez);
};

//----------------------------------------------------------------------------
qfeGrid::~qfeGrid()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeGrid::qfeSetGridOrigin(qfeFloat ox, qfeFloat oy, qfeFloat oz)
{
  this->origin[0] = ox;
  this->origin[1] = oy;
  this->origin[2] = oz;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGrid::qfeGetGridOrigin(qfeFloat& ox, qfeFloat& oy, qfeFloat& oz)
{
  ox = this->origin[0];
  oy = this->origin[1];
  oz = this->origin[2];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGrid::qfeSetGridAxes(qfeFloat axx, qfeFloat axy, qfeFloat axz,
                                        qfeFloat ayx, qfeFloat ayy, qfeFloat ayz,
                                        qfeFloat azx, qfeFloat azy, qfeFloat azz)
{ 
  this->axis[0][0] = axx;  this->axis[0][1] = axy;  this->axis[0][2] = axz;
  this->axis[1][0] = ayx;  this->axis[1][1] = ayy;  this->axis[1][2] = ayz;
  this->axis[2][0] = azx;  this->axis[2][1] = azy;  this->axis[2][2] = azz;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGrid::qfeGetGridAxes(qfeFloat& axx, qfeFloat& axy, qfeFloat& axz,
                                        qfeFloat& ayx, qfeFloat& ayy, qfeFloat& ayz,
                                        qfeFloat& azx, qfeFloat& azy, qfeFloat& azz)
{
  axx = this->axis[0][0];  axy = this->axis[0][1];  axz = this->axis[0][2];
  ayx = this->axis[1][0];  ayy = this->axis[1][1];  ayz = this->axis[1][2];
  azx = this->axis[2][0];  azy = this->axis[2][1];  azz = this->axis[2][2];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGrid::qfeSetGridExtent(qfeFloat ex, qfeFloat ey, qfeFloat ez)
{
  this->extent[0] = ex;  
  this->extent[1] = ey;  
  this->extent[2] = ez;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGrid::qfeGetGridExtent(qfeFloat& ex, qfeFloat& ey, qfeFloat& ez)
{
  ex = this->extent[0]; 
  ey = this->extent[1];
  ez = this->extent[2];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGrid::qfeSetGridOrientation(qfeGridOrientation orientation)
{
  this->orient = orientation;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGrid::qfeGetGridOrientation(qfeGridOrientation &orientation)
{
  orientation = this->orient;
  return qfeSuccess;
}