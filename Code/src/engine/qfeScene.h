#pragma once

#include "qflowexplorer.h"

#include "qfeDisplay.h"
#include "qfeFrame.h"
#include "qfeGeometry.h"
#include "qfeLightSource.h"
#include "qfeModel.h"
#include "qfeVisual.h"
#include "qfeVolume.h"

#include <vtkCommand.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>

#include <algorithm>
#include <vector>
#include "..\src\corelib\tools\qvector.h"
#include "..\src\corelib\tools\qpair.h"

using namespace std;

#define QFE_SCENE_3D   0
#define QFE_SCENE_2D   1

enum qfeCursorType
{
  cursorArrow,
  cursorCross
};

/**
* \file   qfeScene.h
* \author Roy van Pelt
* \class  qfeScene
* \brief  Implements the scene, consisting of one or more of visuals viewed through a specific view geometry.
* \note   Confidential
*
* A scene defines the display of one or more of visuals viewed through a specific view geometry.
* The actual content of a scene is defined by means of a scene graph.
*/
class qfeScene
{
public:
  qfeScene();
  qfeScene(const qfeScene &scene);
  ~qfeScene();

  qfeReturnStatus qfeSetSceneType(int  type);
  qfeReturnStatus qfeGetSceneType(int &type);

  //! Set the scene geometry. Several derived geometry types are allowed.
  template <typename qfeGeometryType>
  inline qfeReturnStatus qfeSetSceneGeometry(qfeGeometryType *geometry)
  {
    delete this->geo;
    this->geo = new qfeGeometryType(*geometry);
    return qfeSuccess;
  };
  
  //! Get the scene geometry. Several derived geometry types are allowed.
  //! The user is responsible for obtaining the correct geometry type.
  template <typename qfeGeometryType>
  inline qfeReturnStatus qfeGetSceneGeometry(qfeGeometryType **geometry)
  {
    *geometry = static_cast<qfeGeometryType *>(this->geo);
    return qfeSuccess;
  };

  //! Allow derived visual types to be added to the scene
  template <typename qfeVisualType>
  inline qfeReturnStatus qfeAddSceneVisual(qfeVisualType *visual)
  {
    this->visuals.push_back(new qfeVisualType(*visual));
    return qfeSuccess;
  }
  
  qfeReturnStatus qfeGetSceneVisuals(vector<qfeVisual*>&visual);
  qfeReturnStatus qfeGetSceneVisualsSize(unsigned int &visualsSize);
  qfeReturnStatus qfeRemoveSceneVisual(qfeVisual *visual);
  qfeReturnStatus qfeRemoveSceneVisuals();

  qfeReturnStatus qfeSetSceneDisplay(qfeDisplay *display);
  qfeReturnStatus qfeGetSceneDisplay(qfeDisplay **display);

  qfeReturnStatus qfeSetSceneLightSource(qfeLightSource *light);
  qfeReturnStatus qfeGetSceneLightSource(qfeLightSource **light);

  qfeReturnStatus qfeGetSceneCursor(qfeCursorType &curs);
  qfeReturnStatus qfeSetSceneCursor(qfeCursorType curs);

  qfeReturnStatus qfeSceneUpdate();
  qfeReturnStatus qfeSelectionUpdate();

  qfeReturnStatus qfeSetSceneFeatureTF(int id, QVector<QPair<double, double>> &tf, int dataType);
  QVector<QPair<double, double>> &qfeGetSceneFeatureTF(int id);
  int qfeGetSceneFeatureDataType(int id);

private:  
  vector<qfeVisual*> visuals;
  qfeDisplay        *display;
  qfeGeometry       *geo;
  qfeLightSource    *light;
  int                type;
  qfeCursorType      cursor;
  QVector<QPair<double, double>> featureTF[2];
  int featureDataType[2];
};
