#include "qfeVisual.h"
#include <QDirIterator>
#include "qfeColorMaps.h" 

//----------------------------------------------------------------------------
// qfeVisual
//----------------------------------------------------------------------------
qfeVisual::qfeVisual()
{
  this->technique    = visualNone;
  this->study        = NULL;

  this->activeVolume = 0;
};

//----------------------------------------------------------------------------
qfeVisual::qfeVisual(const qfeVisual &visual)
{
  this->technique    = visual.technique;
  this->study        = visual.study;
  this->actions      = visual.actions;
  this->params       = visual.params;
  this->colormap     = visual.colormap;
  this->light        = visual.light;
  this->activeVolume = visual.activeVolume;  
};

//----------------------------------------------------------------------------
qfeVisual::~qfeVisual()
{
  this->params.clear();
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeSetVisualStudy(qfeStudy *study)
{
  this->study = study;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeGetVisualStudy(qfeStudy **study)
{
  *study = this->study;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeAddVisualAction(qfeVisualAction action)
{
  this->actions.push_back(action);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeRemoveVisualAction(string label)
{
  for(int i=0; i<(int)this->params.size(); i++)
  {
	string name;
	this->actions[i].qfeGetVisualActionLabel(name);

	if(name == label)
	{
	  this->actions.erase(this->actions.begin() + i);
	  return qfeSuccess;
	}
  }
  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeGetVisualAction(string label, qfeVisualAction **action)
{
  for(int i=0; i<(int)this->actions.size(); i++)
  {
	string name;
	this->actions[i].qfeGetVisualActionLabel(name);

	*action = NULL;

	if(name == label)
	{
	  *action = &this->actions[i];
	  return qfeSuccess;
	}
  }
  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeGetVisualAction(int index, qfeVisualAction **action)
{
  if(index > (int)this->actions.size()) 
  {
	*action = NULL;
	return qfeError;
  }

  *action = &this->actions[index];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeGetVisualActionCount(int &count)
{
  count = (int)this->actions.size();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeAddVisualParameter(qfeVisualParameter param)
{
  string inputLabel;
  param.qfeGetVisualParameterLabel(&inputLabel);

  // if the label exists, overwrite the value
  for(int i=0; i<(int)this->params.size(); i++)
  {
	string listLabel;
	this->params[i].qfeGetVisualParameterLabel(&listLabel);

	if(inputLabel.compare(listLabel) == 0)
	{
	  this->params[i] = param;
	  return qfeSuccess;
	}
  }

  // if the label does not exist, add it
  this->params.push_back(param);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeRemoveVisualParameter(string label)
{
  for(int i=0; i<(int)this->params.size(); i++)
  {
	string name;
	this->params[i].qfeGetVisualParameterLabel(&name);

	if(name == label)
	{
	  this->params.erase(this->params.begin() + i);
	  return qfeSuccess;
	}
  }
  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeGetVisualParameter(string label, qfeVisualParameter **param)
{
  for(int i=0; i<(int)this->params.size(); i++)
  {
	string name;
	this->params[i].qfeGetVisualParameterLabel(&name);
	
	*param = NULL;

	if(name.compare(label) == 0)
	{
	  *param = &this->params[i];

	  return qfeSuccess;
	}

  }
  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeGetVisualParameter(int index, qfeVisualParameter **param)
{
  if(index > (int)this->params.size()) 
  {
	*param = NULL;
	return qfeError;
  }

  *param = &this->params[index];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeGetVisualParameterCount(int &count)
{
  count = (int)this->params.size();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeSetVisualActiveVolume(float index)
{
  if(this->study == NULL) return qfeError; 

  if(floor(index) < (int)this->study->pcap.size())
	this->activeVolume = index;
  else
	this->activeVolume = 0;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeGetVisualActiveVolume(int &index)
{
  index = floor(this->activeVolume);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeGetVisualActiveVolume(float &index)
{
  index = this->activeVolume;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeUpdateVisual()
{
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeSetVisualColorMap(qfeColorMap colorMap)
{
  // Do not overwrite the colormap in order to prevent copying of textures
  this->colormap.qfeCopyColorMap(colorMap);
  
  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeGetVisualColorMap(qfeColorMap **colorMap)
{
  *colorMap = &this->colormap;

  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisual::qfeGetVisualType(int &technique)
{
  technique = this->technique;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// qfeVisualDefault
//----------------------------------------------------------------------------
qfeVisualDefault::qfeVisualDefault()
{
  this->technique = visualDefault;
};


//----------------------------------------------------------------------------
qfeVisualDefault::~qfeVisualDefault()
{
};

//----------------------------------------------------------------------------
// qfeVisualRaycast
//----------------------------------------------------------------------------
qfeVisualRaycast::qfeVisualRaycast()
{
  this->technique = visualRaycast;

  qfeVisualParameter p;
  qfeRange x, y, z;

  x.min = y.min = z.min = 0.0;
  x.max = y.max = z.max = 1.0;  

  qfeSelectionList dataSet;
  dataSet.list.push_back("pca-p");
  dataSet.list.push_back("pca-m");
  dataSet.list.push_back("ssfp");
  dataSet.list.push_back("t-mip");
  dataSet.list.push_back("ffe");
  dataSet.list.push_back("ftle");
  dataSet.active = 0;

  qfeSelectionList dataComponent;
  dataComponent.list.push_back("magnitude");
  dataComponent.list.push_back("x");
  dataComponent.list.push_back("y");
  dataComponent.list.push_back("z");
  dataComponent.active = 0;

  qfeSelectionList quality;
  quality.list.push_back("low (1x)");
  quality.list.push_back("medium (2x)");
  quality.list.push_back("high (4x)");
  quality.list.push_back("very high (8x)");
  quality.active = 1;

  qfeSelectionList style;
  style.list.push_back("phong shading");
  style.list.push_back("cel shading");
  style.list.push_back("glass shading");
  style.active = 0;

  p.qfeSetVisualParameter("DVR visible", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("DVR data set", dataSet);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("DVR data component", dataComponent);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("DVR gradient", false);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("DVR shading", true);
  this->qfeAddVisualParameter(p); 

   p.qfeSetVisualParameter("DVR shading style", style, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("DVR shading quality", quality, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("DVR shading multires", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("DVR MIDA gamma", -1.0, 0.1, -1.0, 1.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("DVR clipping x", x, 0.01, 0.0, 1.0);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("DVR clipping y", y, 0.01, 0.0, 1.0);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("DVR clipping z", z, 0.01, 0.0, 1.0);
  this->qfeAddVisualParameter(p); 
};

//----------------------------------------------------------------------------
qfeVisualRaycast::qfeVisualRaycast(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualRaycast();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualRaycast::~qfeVisualRaycast()
{
};

//----------------------------------------------------------------------------
// qfeVisualRaycast
//----------------------------------------------------------------------------
qfeVisualRaycastVector::qfeVisualRaycastVector()
{
  this->technique = visualRaycastVector;

  qfeVisualParameter p;
  qfeRange x, y, z;

  x.min = y.min = z.min = 0.0;
  x.max = y.max = z.max = 1.0;  

  qfeSelectionList dataSet;
  dataSet.list.push_back("pca-p");
  dataSet.list.push_back("pca-m");
  dataSet.list.push_back("ssfp");
  dataSet.list.push_back("t-mip");
  dataSet.list.push_back("ffe");
  dataSet.active = 0;

  qfeSelectionList dataFocusRepresentation;
  dataFocusRepresentation.list.push_back("doppler");
  dataFocusRepresentation.list.push_back("angle");
  dataFocusRepresentation.list.push_back("angle derivative");
  dataFocusRepresentation.list.push_back("coherence");
  dataFocusRepresentation.list.push_back("vorticity");
  dataFocusRepresentation.active = 0;

  qfeSelectionList dataContextRepresentation;
  dataContextRepresentation.list.push_back("depth");
  dataContextRepresentation.list.push_back("dvr");
  dataContextRepresentation.active = 0;

  qfeSelectionList quality;
  quality.list.push_back("low (1x)");
  quality.list.push_back("medium (2x)");
  quality.list.push_back("high (4x)");
  quality.list.push_back("very high (8x)");
  quality.active = 1;

  qfeSelectionList style;
  style.list.push_back("phong shading");
  style.list.push_back("cel shading");
  style.list.push_back("glass shading");
  style.active = 0;

  p.qfeSetVisualParameter("VFR visible", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("VFR data set", dataSet);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("VFR data focus", dataFocusRepresentation);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("VFR data context", dataContextRepresentation);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("VFR velocity thr. (cm/s)",  20, 5, 0, 300);
  this->qfeAddVisualParameter(p); 

  /*p.qfeSetVisualParameter("VFR shading", true);
  this->qfeAddVisualParameter(p); 

   p.qfeSetVisualParameter("VFR shading style", style, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); */

  p.qfeSetVisualParameter("VFR quality", quality, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("VFR multires", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("VFR clipping x", x, 0.01, 0.0, 1.0);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("VFR clipping y", y, 0.01, 0.0, 1.0);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("VFR clipping z", z, 0.01, 0.0, 1.0);
  this->qfeAddVisualParameter(p); 
};

//----------------------------------------------------------------------------
qfeVisualRaycastVector::qfeVisualRaycastVector(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualRaycastVector();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualRaycastVector::~qfeVisualRaycastVector()
{
};


//----------------------------------------------------------------------------
// qfeVisualMaximum
//----------------------------------------------------------------------------
qfeVisualMaximum::qfeVisualMaximum()
{
  this->technique = visualMaximum;

  qfeVisualParameter p;

  qfeSelectionList dataSet;
  dataSet.list.push_back("pca-p");
  dataSet.list.push_back("pca-m");
  dataSet.list.push_back("ssfp");
  dataSet.list.push_back("t-mip");
  dataSet.list.push_back("ffe");
  dataSet.list.push_back("ftle");
  dataSet.active = 0;

  qfeSelectionList dataComponent;
  dataComponent.list.push_back("magnitude");
  dataComponent.list.push_back("x");
  dataComponent.list.push_back("y");
  dataComponent.list.push_back("z");
  dataComponent.active = 0;

  qfeSelectionList dataRepresentation;
  dataRepresentation.list.push_back("gray");
  dataRepresentation.list.push_back("transfer function");
  dataRepresentation.active = 0;

  p.qfeSetVisualParameter("MIP visible", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MIP data set", dataSet);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MIP data component", dataComponent, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MIP data representation", dataRepresentation, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MIP gradient", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MIP transparency", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MIP contrast",   (float)0.0, (float)0.01, -1.0, 1.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MIP brightness", (float)0.0, (float)0.01, -1.0, 1.0);  
  this->qfeAddVisualParameter(p);

};

//----------------------------------------------------------------------------
qfeVisualMaximum::qfeVisualMaximum(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualMaximum();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualMaximum::~qfeVisualMaximum()
{
}

//----------------------------------------------------------------------------
// qfeVisualMetrics
//----------------------------------------------------------------------------

qfeVisualMetrics::qfeVisualMetrics()
{
	this->technique = visualMetrics;
	
	qfeColorMaps colorMaps;
	qfeVisualParameter p;

	std::vector<qfeStudy::metric_descriptor> metricList;

	study->qfeGetScalarMetricList(metricList);

	qfeSelectionList mainMetric;

	for(unsigned int i = 0; i<metricList.size(); i++)
	{
		mainMetric.list.push_back(metricList.at(i).name);
	}
	mainMetric.active = 0;

	p.qfeSetVisualParameter("Main metric", mainMetric);
	this->qfeAddVisualParameter(p);
		
	mainMetric.list.insert(mainMetric.list.begin(),"None");

	p.qfeSetVisualParameter("Iso surface", mainMetric);
	this->qfeAddVisualParameter(p);

	p.qfeSetVisualParameter("Iso value (%)", 50, 5, 0, 100);
	this->qfeAddVisualParameter(p);

	qfeSelectionList colorScheme;
	colorScheme.list.push_back("Disable");
	for(unsigned int i = 0; i<colorMaps.qfeGetAvailableColorMaps().size(); i++)
	{
		colorScheme.list.push_back(colorMaps.qfeGetAvailableColorMaps()[i]);
	}

	for(unsigned int i = 0; i<metricList.size(); i++)
	{
		colorScheme.active = 0; //disable most by default
		if(i<0)
		{
			if(metricList.at(i).diverging) //set diverging colorscheme
				colorScheme.active = colorMaps.qfeGetIDDivergingColorScheme() + 1;
			else
				colorScheme.active = colorMaps.qfeGetIDIncreasingColorScheme() + 1;
		}
			
		p.qfeSetVisualParameter(metricList.at(i).name, colorScheme);
		this->qfeAddVisualParameter(p);
	}
};

//----------------------------------------------------------------------------
qfeVisualMetrics::qfeVisualMetrics(const qfeVisualMetrics &visual) : qfeVisual(visual)
{
}

//----------------------------------------------------------------------------
qfeVisualMetrics::~qfeVisualMetrics()
{

};

//----------------------------------------------------------------------------
// qfeVisualInkVis
//----------------------------------------------------------------------------
qfeVisualInkVis::qfeVisualInkVis() : qfeVisual()
{
  this->technique = visualInkVis;

  qfeSelectionList advectionTypes;
  advectionTypes.list.push_back("Stream");
  advectionTypes.list.push_back("Path");
  advectionTypes.list.push_back("Streak");
  advectionTypes.active = 2;

  qfeSelectionList colorSettings;
  colorSettings.list.push_back("Seeding");
  colorSettings.list.push_back("Angular seeding");
  colorSettings.list.push_back("Age");
  colorSettings.list.push_back("Cumulative deviation");
  colorSettings.list.push_back("Depth");
  colorSettings.list.push_back("Seeding and cummulative deviation");
  colorSettings.list.push_back("Angular seeding and cummulative deviation");
  colorSettings.list.push_back("Seeding and age");
  colorSettings.list.push_back("Angular seeding and age");
  colorSettings.list.push_back("Speed");
  colorSettings.active = 9;

  qfeSelectionList particleType;
  particleType.list.push_back("Flat");
  particleType.list.push_back("Gaussian");
  particleType.list.push_back("Sphere");
  particleType.active = 1;

  qfeSelectionList glyphTypes;
  glyphTypes.list.push_back("None");
  glyphTypes.list.push_back("Dashed");
  glyphTypes.list.push_back("Arrow");
  glyphTypes.list.push_back("Tadpole");
  glyphTypes.list.push_back("Ellipsoid");
  glyphTypes.active = 0;

  qfeSelectionList graphTypes;
  graphTypes.list.push_back("Uncertainty");
  graphTypes.list.push_back("Speed");
  graphTypes.active = 0;

  qfeVisualParameter p;
  qfeVisualAction a;

  p.qfeSetVisualParameter("Ink visible", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Advection type", advectionTypes);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Uncertainty", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Show graph", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Graph data", graphTypes, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Invert graph", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Volumetric seeding", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Planar seeding", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Seed from voxel center", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Seed from fixed positions", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Fixed seed positions multiplier", (float)1.0, (float)0.1, 0.0, 200.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Seed throughout mesh", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Clip seed outside mesh", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Clip outside mesh", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Clipping margin (mm)", (float)0.0, (float)0.5, -50.0, 50.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Clip seed volume", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Particle opacity", 0.99f, 0.001f, 0.0f, 1.0f);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Map opacity to uncertainty", true);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Number of lines", 0, 1, 0, 50, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Particle multiplier", (float)5.0, (float)0.25, 0.0, 100000.0);  
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particle lifetime (phases)", (float)5.0, (float)0.5, 0.0, 250.0);  
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Animation step size (phases)", (float)0.25, (float)0.01, 0.01, 250.0);  
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Animation length (phases)", (float)5.0, (float)0.10, 0.0, 250.0);  
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Stop seeding (phases)", (float)-1.0, (float)0.10, -1.0, 250.0, qfeVisualParameterAdvanced);  
  this->qfeAddVisualParameter(p);
    
  p.qfeSetVisualParameter("CFL scaling", (float)1.0, (float)0.1, 0.1, 1.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Inverse colorscheme", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Color setting", colorSettings);
  this->qfeAddVisualParameter(p);

  //p.qfeSetVisualParameter("Number of color bands", 0, 1, 0, 50);
  //this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particle type", particleType, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Render GPU friendly", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Depth enhancement", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Depth enhancement amount", (float)-0.5, (float)0.10, -10.0, 10.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Depth enhancement light", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Chroma depth", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Aerial perspective", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particle size", (float)1.0, (float)0.05, 0.0, 10.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Spatial gap amount 1", 0, 1, 0, 20);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Spatial gap width 1", (float)0.1, (float)0.10, 0.0, 0.5, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Spatial gap amount 2", 0, 1, 0, 20);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Spatial gap width 2", (float)0.1, (float)0.10, 0.0, 0.5, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Temporal gap amount", 0, 1, 0, 250);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Temporal gap width", (float)0.1, (float)0.10, 0.0, 0.5, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Glyph type", glyphTypes, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Glyph frequency", 10, 1, 0, 500, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Render colorscheme", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Render seeding", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Render iso surface", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Iso value", (float)0.1, (float)0.01, 0.0, 1.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Store Screenshots", false);
  this->qfeAddVisualParameter(p);

  a.qfeSetVisualAction("Play", actionAnimateInk);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Particles Clear", actionRemoveInk);
  this->qfeAddVisualAction(a);
    
  a.qfeSetVisualAction("OpenFile", actionLoadInkSeed);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Save", actionSaveInkSeed);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Center", actionCenterInkCamera);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Plot", actionShowTimeLine);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Seed Points Feature-based", actionPlaceFeatureSeedPointsInk);
  qfeAddVisualAction(a);
};

//----------------------------------------------------------------------------
qfeVisualInkVis::qfeVisualInkVis(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualInkVis();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualInkVis::~qfeVisualInkVis()
{
}

//----------------------------------------------------------------------------
// qfeVisualInkVisSecondary
//----------------------------------------------------------------------------
qfeVisualInkVisSecondary::qfeVisualInkVisSecondary() : qfeVisual()
{
  this->technique = visualInkVisSecondary;

  qfeVisualParameter p;

  p.qfeSetVisualParameter("Ink2 visible", true);
  this->qfeAddVisualParameter(p);
}

//----------------------------------------------------------------------------
qfeVisualInkVisSecondary::qfeVisualInkVisSecondary(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualInkVisSecondary();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualInkVisSecondary::~qfeVisualInkVisSecondary()
{
}

//----------------------------------------------------------------------------
// qfeVisualFluid
//----------------------------------------------------------------------------
qfeVisualFluid::qfeVisualFluid()
{
  this->technique = visualFluid;

  qfeVisualParameter p;
  qfeVisualAction a;

  qfeSelectionList dataSet;
  dataSet.list.push_back("pca-p");
  dataSet.list.push_back("energy");  
  dataSet.active = 0;

  p.qfeSetVisualParameter("FS forward simulated coupling", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS backward simulated coupling", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS use viscosity", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS steps per measurement", 4, 1, 1, 32);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS grid visible", false);
  this->qfeAddVisualParameter(p);
 
  a.qfeSetVisualAction("Fluid Simulation Preprocess", actionFluidSimPreprocess);
  this->qfeAddVisualAction(a);
  
  a.qfeSetVisualAction("Add Fluid Source", actionFluidSimSetSource);
  this->qfeAddVisualAction(a);
  
  a.qfeSetVisualAction("Add Fluid Sink", actionFluidSimSetSink);
  this->qfeAddVisualAction(a);
	
  a.qfeSetVisualAction("Remove Fluid Interactor", actionFluidSimRemoveInteractor);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Inspect", actionFluidSimInspect);
  this->qfeAddVisualAction(a);
  
  a.qfeSetVisualAction("Plot", actionFluidSimComputeFlux);
  this->qfeAddVisualAction(a); 
};

//----------------------------------------------------------------------------
qfeVisualFluid::qfeVisualFluid(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualFluid();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualFluid::~qfeVisualFluid()
{
};


//----------------------------------------------------------------------------
// qfeVisualDataAssimilation
//----------------------------------------------------------------------------
qfeVisualDataAssimilation::qfeVisualDataAssimilation()
{
  this->technique = visualDataAssimilation;
  
  qfeVisualParameter p;
  qfeVisualAction a;

  qfeSelectionList dataAssimilationApproach;  
  dataAssimilationApproach.list.push_back("Control Theory");
  dataAssimilationApproach.list.push_back("3D Var");  
  dataAssimilationApproach.list.push_back("de Hoon et al (2014)");
  dataAssimilationApproach.list.push_back("Run once");
  dataAssimilationApproach.list.push_back("Convert (for comparisons)");
  dataAssimilationApproach.active = 0;

  qfeSelectionList filterFunction;  
  filterFunction.list.push_back("Pressure solve");
  filterFunction.list.push_back("Simulation");  
  filterFunction.active = 0;

  p.qfeSetVisualParameter("Visible", true);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Assimilation approach", dataAssimilationApproach);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Filter function", filterFunction);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Alpha", 1.0, 0.1, 0.0, 100.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Beta", 1.0, 0.1, 0.0, 1000.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Gamma", 0.5, 0.1, 0.0, 1000.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Delta (minimizer) *1e-3", 100.0, 1.0, 0.0, 1000.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Epsilon (minimizer) *1e-6", 1000, 100, 0, 1000000, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Maximum iterations", 1000, 0, 1, 2500, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Spatial interpolation", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Temporal interpolation", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Use uncertainty", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Use uncertainty", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Inject noise", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Noise percentage", 5, 1, 1, 100, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Subsample", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Verbose", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Keep intermediate files", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Use feature based", false);
  this->qfeAddVisualParameter(p);  

  std::vector<qfeStudy::metric_descriptor> metricList;

  study->qfeGetScalarMetricList(metricList);

  qfeSelectionList mainMetric;

  for(unsigned int i = 0; i<metricList.size(); i++)
  {
	  mainMetric.list.push_back(metricList.at(i).name);
  }
  mainMetric.active = 0;

  p.qfeSetVisualParameter("Feature", mainMetric);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Render cropping", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Crop X1",0, 1, 0, 1, qfeVisualParameterAdvanced); 
  this->qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("Crop X2",1, 1, 0, 1,  qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("Crop Y1",0, 1, 0, 1,  qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("Crop Y2",1, 1, 0, 1,  qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("Crop Z1",0, 1, 0, 1,  qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("Crop Z2",1, 1, 0, 1,  qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  a.qfeSetVisualAction("Fluid Simulation Preprocess", actionApplyDataAssimilation);
  this->qfeAddVisualAction(a); 
};

//----------------------------------------------------------------------------
qfeVisualDataAssimilation::qfeVisualDataAssimilation(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualDataAssimilation();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualDataAssimilation::~qfeVisualDataAssimilation()
{
};

//----------------------------------------------------------------------------
// qfeVisualSimParticleTrace
//----------------------------------------------------------------------------
qfeVisualSimParticleTrace::qfeVisualSimParticleTrace()
{
  this->technique    = visualSimParticles;    
  
  qfeVisualParameter p;

  qfeSelectionList visualizationStyle;
  visualizationStyle.list.push_back("sphere");  
  visualizationStyle.list.push_back("ellipsoid");    
  visualizationStyle.active = 1;

  qfeSelectionList shadingType;
  shadingType.list.push_back("toon");     
  shadingType.list.push_back("phong"); 
  shadingType.active = 0;

  qfeSelectionList colorType;
  colorType.list.push_back("velocity");     
  colorType.list.push_back("seed time"); 
  colorType.active = 0;

  p.qfeSetVisualParameter("FS particles visible", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS particles percentage", 100, 5, 0, 100);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS particles size (mm)", 2.0, 0.1, 0.1, 10.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS particles style", visualizationStyle);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS particles shading", shadingType);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS particles contour", true);
  this->qfeAddVisualParameter(p);
};

//----------------------------------------------------------------------------
qfeVisualSimParticleTrace::qfeVisualSimParticleTrace(const qfeVisualSimParticleTrace &visual) : qfeVisual(visual)
{
}

//----------------------------------------------------------------------------
qfeVisualSimParticleTrace::~qfeVisualSimParticleTrace()
{

};

//----------------------------------------------------------------------------
// qfeVisualSimLineComparison
//----------------------------------------------------------------------------
qfeVisualSimLinesComparison::qfeVisualSimLinesComparison()
{
  this->technique    = visualSimLinesComparison;    
  
  qfeVisualParameter p;
  qfeVisualAction    a;

  //qfeSelectionList seedSource;
  //seedSource.list.push_back("quasi-random");  
  //seedSource.list.push_back("simulation");    
  //seedSource.active = 0;

  qfeSelectionList colorType;
  colorType.list.push_back("speed");  
  colorType.list.push_back("distance local");    
  colorType.list.push_back("distance mean");    
  colorType.list.push_back("distance Hausdorff");    
  colorType.active = 2;

  //p.qfeSetVisualParameter("FS seeds source", seedSource, qfeVisualParameterAdvanced);
  //this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS line length (phases)", 1.0, 0.01, 0.0, 20.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS seeds count", 250, 10, 1, 10000);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS seeds visible", false);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("FS lines measurement visible", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS lines simulation visible", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS surface visible", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS distance filter", 100, 1, 0, 100);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS color type", colorType);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS highlight measurement", true);
  this->qfeAddVisualParameter(p);
  
  a.qfeSetVisualAction("Particles Inject", actionInjectSeeds);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Particles Clear", actionClearSeeds);
  this->qfeAddVisualAction(a); 
};

//----------------------------------------------------------------------------
qfeVisualSimLinesComparison::qfeVisualSimLinesComparison(const qfeVisualSimLinesComparison &visual) : qfeVisual(visual)
{
}

//----------------------------------------------------------------------------
qfeVisualSimLinesComparison::~qfeVisualSimLinesComparison()
{

};

//----------------------------------------------------------------------------
// qfeVisualSimBoundaryComparison
//----------------------------------------------------------------------------
qfeVisualSimBoundaryComparison::qfeVisualSimBoundaryComparison()
{
  this->technique    = visualSimBoundaryComparison;    
  
  qfeVisualParameter p;
  qfeVisualAction    a;

  //qfeSelectionList seedSource;
  //seedSource.list.push_back("quasi-random");  
  //seedSource.list.push_back("simulation");    
  //seedSource.active = 0;

  //qfeSelectionList colorType;
  //colorType.list.push_back("speed");  
  //colorType.list.push_back("angle");    
  //colorType.active = 1;

  //p.qfeSetVisualParameter("FS seeds source", seedSource, qfeVisualParameterAdvanced);
  //this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("FS arrows visible", false);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("FS angle filter", 180, 1, 0, 180);
  this->qfeAddVisualParameter(p);

  //p.qfeSetVisualParameter("FS color type", colorType);
  //this->qfeAddVisualParameter(p);

  a.qfeSetVisualAction("Particles Inject", actionInjectSeeds);
  this->qfeAddVisualAction(a);
  
  a.qfeSetVisualAction("Particles Clear", actionClearSeeds);
  this->qfeAddVisualAction(a);
  
};


//----------------------------------------------------------------------------
qfeVisualSimBoundaryComparison::qfeVisualSimBoundaryComparison(const qfeVisualSimBoundaryComparison &visual) : qfeVisual(visual)
{
}

//----------------------------------------------------------------------------
qfeVisualSimBoundaryComparison::~qfeVisualSimBoundaryComparison()
{

};



//----------------------------------------------------------------------------
// qfeVisualReformat
//----------------------------------------------------------------------------
qfeVisualReformat::qfeVisualReformat()
{
  this->technique = visualReformat; 
};

//----------------------------------------------------------------------------
qfeVisualReformat::qfeVisualReformat(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualReformat();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualReformat::~qfeVisualReformat()
{
};

//----------------------------------------------------------------------------
// qfeVisualReformatFixed
//----------------------------------------------------------------------------
qfeVisualReformatFixed::qfeVisualReformatFixed()
{
  this->technique = visualReformatFixed;

  qfeVisualParameter p;

  qfeSelectionList dataSet1;
  dataSet1.list.push_back("pca-p");
  dataSet1.list.push_back("pca-m");
  dataSet1.list.push_back("ffe");
  dataSet1.list.push_back("ssfp");
  dataSet1.active = 0;

  qfeSelectionList dataSet2;
  dataSet2.list.push_back("pca-p");
  dataSet2.list.push_back("pca-m");  
  dataSet2.list.push_back("ssfp");
  dataSet2.list.push_back("t-mip");
  dataSet2.active = 0;

  qfeSelectionList dataScalarProperties;
  dataScalarProperties.list.push_back("intensity");
  dataScalarProperties.active = 0;

  qfeSelectionList dataRepresentation;
  dataRepresentation.list.push_back("gray");
  dataRepresentation.list.push_back("transfer function");
  dataRepresentation.list.push_back("red-blue");
  dataRepresentation.active = 0;

  qfeSelectionList dataVector;
  dataVector.list.push_back("through-plane");
  dataVector.list.push_back("speed");
  dataVector.list.push_back("velocity x");
  dataVector.list.push_back("velocity y");
  dataVector.list.push_back("velocity z");  
  dataVector.active = 0;

  p.qfeSetVisualParameter("Planes visible", true);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Plane border visible", true);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Plane transparency", 0, 10, 0, 100);
  this->qfeAddVisualParameter(p); 
  
  p.qfeSetVisualParameter("Plane data set", dataSet1);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("4D flow data set", dataSet2);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("4D flow visibility (%)", 0, 10, 0, 100);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("4D flow vector", dataVector);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Data representation", dataRepresentation);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Contrast", 0.0, 5.0, -100.0, 100.0);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Brightness", 0.0, 5.0, -100.0, 100.0);
  this->qfeAddVisualParameter(p);  
};

//----------------------------------------------------------------------------
qfeVisualReformatFixed::qfeVisualReformatFixed(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualReformatFixed();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualReformatFixed::~qfeVisualReformatFixed()
{
};

//----------------------------------------------------------------------------
// qfeVisualReformatOrtho
//----------------------------------------------------------------------------
qfeVisualReformatOrtho::qfeVisualReformatOrtho()
{
  this->technique = visualReformatOrtho;

  qfeVisualParameter p;
  qfeVisualAction    a;

  qfeSelectionList dataSet;
  dataSet.list.push_back("pca-p");
  dataSet.list.push_back("pca-m");
  dataSet.list.push_back("ssfp");  
  dataSet.list.push_back("tmip"); 
  dataSet.list.push_back("ffe");
  dataSet.list.push_back("cluster"); 
  dataSet.list.push_back("ftle");
  dataSet.active = 0;

  qfeSelectionList dataScalarProperties;
  dataScalarProperties.list.push_back("intensity");
  dataScalarProperties.active = 0;

  qfeSelectionList dataVectorProperties;
  dataVectorProperties.list.push_back("speed");
  dataVectorProperties.list.push_back("velocity x");
  dataVectorProperties.list.push_back("velocity y");
  dataVectorProperties.list.push_back("velocity z");
  dataVectorProperties.list.push_back("divergence");
  dataVectorProperties.list.push_back("rot x");
  dataVectorProperties.list.push_back("rot y");
  dataVectorProperties.list.push_back("rot z");
  dataVectorProperties.list.push_back("coherence (LPC)");  
  dataVectorProperties.list.push_back("coherence (EDC)");  
  dataVectorProperties.active = 0;

  qfeSelectionList dataRepresentation;
  dataRepresentation.list.push_back("gray");
  dataRepresentation.list.push_back("transfer function");
  dataRepresentation.list.push_back("red-blue");
  dataRepresentation.active = 0;

  qfeSelectionList dataInterpolation;
  dataInterpolation.list.push_back("nearest neighbor");
  dataInterpolation.list.push_back("linear");
  dataInterpolation.active = 1;

  // TODO
  //p.qfeSetVisualParameter("Ortho planes visible", true);
  //this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Ortho plane RL (X)", true);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Ortho plane AP (Y)", true);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Ortho plane FH (Z)", true);
  this->qfeAddVisualParameter(p);   

  p.qfeSetVisualParameter("Ortho data set", dataSet);
  this->qfeAddVisualParameter(p); 

  //p.qfeSetVisualParameter("Ortho data scalar", dataScalarProperties);
  //this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Ortho data vector", dataVectorProperties);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Ortho data representation", dataRepresentation);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Ortho data interpolation", dataInterpolation, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Ortho transparency", 0, 1, 0, 100, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Ortho contrast", 15.0, 5.0, -100.0, 100.0);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Ortho brightness", 15.0, 5.0, -100.0, 100.0);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Ortho scroll step (mm)", 2, 1, 1, 20, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  a.qfeSetVisualAction("MPR reset", actionResetMPR);
  this->qfeAddVisualAction(a);

};

//----------------------------------------------------------------------------
qfeVisualReformatOrtho::qfeVisualReformatOrtho(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualReformatOrtho();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualReformatOrtho::~qfeVisualReformatOrtho()
{
};

//----------------------------------------------------------------------------
// qfeVisualReformatOblique
//----------------------------------------------------------------------------
qfeVisualReformatOblique::qfeVisualReformatOblique()
{
  this->technique = visualReformatOblique;

  qfeVisualAction    a;
  qfeVisualParameter p;

  qfeSelectionList dataSet;
  dataSet.list.push_back("pca-p");
  dataSet.list.push_back("pca-m");    
  dataSet.list.push_back("ssfp"); 
  dataSet.list.push_back("tmip");
  dataSet.list.push_back("ffe");
  dataSet.active = 0;

  qfeSelectionList dataScalarProperties;
  dataScalarProperties.list.push_back("intensity");
  dataScalarProperties.active = 0;

  qfeSelectionList dataVectorProperties;
  dataVectorProperties.list.push_back("speed");
  dataVectorProperties.list.push_back("velocity x");
  dataVectorProperties.list.push_back("velocity y");
  dataVectorProperties.list.push_back("velocity z");
  dataVectorProperties.list.push_back("divergence");
  dataVectorProperties.list.push_back("rot x");
  dataVectorProperties.list.push_back("rot y");
  dataVectorProperties.list.push_back("rot z");
  dataVectorProperties.list.push_back("coherence (LPC)");  
  dataVectorProperties.list.push_back("coherence (EDC)");  
  dataVectorProperties.active = 0;

  qfeSelectionList dataRepresentation;
  dataRepresentation.list.push_back("gray");
  dataRepresentation.list.push_back("transfer function");
  dataRepresentation.list.push_back("red-blue");
  dataRepresentation.active = 0;

  qfeSelectionList dataInterpolation;
  dataInterpolation.list.push_back("nearest neighbor");
  dataInterpolation.list.push_back("linear");
  dataInterpolation.active = 1;

  p.qfeSetVisualParameter("MPR visible", false);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("MPR contour", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MPR data set", dataSet);
  this->qfeAddVisualParameter(p); 

  //p.qfeSetVisualParameter("MPR data scalar", dataScalarProperties);
  //this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MPR data vector", dataVectorProperties);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("MPR data representation", dataRepresentation);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MPR data interpolation", dataInterpolation, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MPR mix (%)", 0, 10, 0, 100);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MPR brightness", 0.0, 5.0, -100.0, 100.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MPR contrast", 0.0, 5.0, -100.0, 100.0);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MPR opacity 1", 1.0, 0.1, 0.0, 1.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MPR opacity 2", 1.0, 0.1, 0.0, 1.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MPR scroll step (mm)", 2, 1, 1, 20, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  a.qfeSetVisualAction("MPR reset", actionResetMPR);
  this->qfeAddVisualAction(a);

};

//----------------------------------------------------------------------------
qfeVisualReformatOblique::qfeVisualReformatOblique(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualReformat();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualReformatOblique::~qfeVisualReformatOblique()
{
};

//----------------------------------------------------------------------------
// qfeVisualReformatObliqueProbe
//----------------------------------------------------------------------------
qfeVisualReformatObliqueProbe::qfeVisualReformatObliqueProbe()
{
  this->technique = visualReformatObliqueProbe;

  qfeVisualParameter p;

  qfeSelectionList dataSet;
  dataSet.list.push_back("pca-p");
  dataSet.list.push_back("pca-m");    
  dataSet.list.push_back("ssfp"); 
  dataSet.list.push_back("tmip");
  dataSet.list.push_back("ffe");
  dataSet.active = 0;

  qfeSelectionList dataScalarProperties;
  dataScalarProperties.list.push_back("intensity");
  dataScalarProperties.active = 0;

  qfeSelectionList dataVectorProperties;
  dataVectorProperties.list.push_back("speed");
  dataVectorProperties.list.push_back("velocity x");
  dataVectorProperties.list.push_back("velocity y");
  dataVectorProperties.list.push_back("velocity z");
  dataVectorProperties.list.push_back("divergence");
  dataVectorProperties.list.push_back("rot x");
  dataVectorProperties.list.push_back("rot y");
  dataVectorProperties.list.push_back("rot z");
  dataVectorProperties.list.push_back("coherence (LPC)");  
  dataVectorProperties.list.push_back("coherence (EDC)");  
  dataVectorProperties.active = 0;

  qfeSelectionList dataRepresentation;
  dataRepresentation.list.push_back("gray");
  dataRepresentation.list.push_back("transfer function");
  dataRepresentation.list.push_back("red-blue");
  dataRepresentation.active = 0;

  qfeSelectionList dataInterpolation;
  dataInterpolation.list.push_back("nearest neighbor");
  dataInterpolation.list.push_back("linear");
  dataInterpolation.active = 1;

  qfeSelectionList shape;
  shape.list.push_back("circle");
  shape.list.push_back("square");    
  shape.active = 0;

  qfeSelectionList scrollStep;
  scrollStep.list.push_back("millimeters");
  scrollStep.list.push_back("slices");  
  scrollStep.active = 0;

  p.qfeSetVisualParameter("MPRP visible", true);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("MPRP data set", dataSet);
  this->qfeAddVisualParameter(p); 

  //p.qfeSetVisualParameter("MPRP data scalar", dataScalarProperties);
  //this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MPRP data vector", dataVectorProperties);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("MPRP data representation", dataRepresentation);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MPRP data interpolation", dataInterpolation, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MPRP scale (%)", 100, 5, 0, 500, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  //p.qfeSetVisualParameter("MPRP shape", shape);
  //this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("MPRP mix (%)", 0, 10, 0, 100, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MPRP contrast", 0.0, 5.0, -100.0, 100.0);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MPRP brightness", 0.0, 5.0, -100.0, 100.0);
  this->qfeAddVisualParameter(p);
};

//----------------------------------------------------------------------------
qfeVisualReformatObliqueProbe::qfeVisualReformatObliqueProbe(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualReformat();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualReformatObliqueProbe::~qfeVisualReformatObliqueProbe()
{
};

//----------------------------------------------------------------------------
// qfeVisualReformatUltrasound
//----------------------------------------------------------------------------
qfeVisualReformatUltrasound::qfeVisualReformatUltrasound()
{
  this->technique = visualReformatUltrasound;

  qfeVisualParameter p;

  qfeSelectionList dataSet;
  dataSet.list.push_back("pca-p");
  dataSet.list.push_back("pca-m");    
  dataSet.list.push_back("ssfp");   
  dataSet.list.push_back("tmip");
  dataSet.list.push_back("ffe");  
  dataSet.active = 0;

  qfeSelectionList dataScalarProperties;
  dataScalarProperties.list.push_back("intensity");
  dataScalarProperties.active = 0;

  qfeSelectionList dataVectorPropertiesContext;
  dataVectorPropertiesContext.list.push_back("speed");
  dataVectorPropertiesContext.list.push_back("velocity x");
  dataVectorPropertiesContext.list.push_back("velocity y");
  dataVectorPropertiesContext.list.push_back("velocity z");
  dataVectorPropertiesContext.list.push_back("divergence");
  dataVectorPropertiesContext.list.push_back("rot x");
  dataVectorPropertiesContext.list.push_back("rot y");
  dataVectorPropertiesContext.list.push_back("rot z");
  dataVectorPropertiesContext.list.push_back("coherence (LPC)");  
  dataVectorPropertiesContext.list.push_back("coherence (EDC)");  
  dataVectorPropertiesContext.active = 0;

  qfeSelectionList dataVectorPropertiesFocus;  
  dataVectorPropertiesFocus.list.push_back("speed");
  dataVectorPropertiesFocus.list.push_back("vorticity");
  dataVectorPropertiesFocus.list.push_back("angle");
  dataVectorPropertiesFocus.list.push_back("angle derivative");
  dataVectorPropertiesFocus.list.push_back("coherence");
  dataVectorPropertiesFocus.active = 0;

  qfeSelectionList dataRepresentation;
  dataRepresentation.list.push_back("gray");
  dataRepresentation.list.push_back("transfer function");
  dataRepresentation.active = 0;

  qfeSelectionList dataRepresentation2;
  dataRepresentation2.list.push_back("gray");
  dataRepresentation2.list.push_back("transfer function");
  dataRepresentation2.list.push_back("doppler 1");
  dataRepresentation2.list.push_back("doppler 2");
  dataRepresentation2.list.push_back("variance by coherence");
  dataRepresentation2.active = 4;

  qfeSelectionList dataInterpolation;
  dataInterpolation.list.push_back("nearest neighbor");
  dataInterpolation.list.push_back("linear");
  dataInterpolation.active = 1;

  qfeSelectionList shape;
  shape.list.push_back("circle");
  shape.list.push_back("square");    
  shape.active = 0;

  qfeSelectionList scrollStep;
  scrollStep.list.push_back("millimeters");
  scrollStep.list.push_back("slices");  
  scrollStep.active = 0;

  p.qfeSetVisualParameter("MPRUS visible", true);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("MPRUS data set", dataSet);
  this->qfeAddVisualParameter(p); 

  //p.qfeSetVisualParameter("MPRUS data scalar", dataScalarProperties);
  //this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MPRUS data context", dataVectorPropertiesContext);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("MPRUS data focus", dataVectorPropertiesFocus);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("MPRUS data context repr.", dataRepresentation);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MPRUS data focus repr.", dataRepresentation2);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MPRUS data interpolation", dataInterpolation, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MPRUS speed thr. (cm/s)", 20, 5, 0, 300);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("MPRUS horizontal flip", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("MPRUS scale (%)", 100, 5, 0, 500, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  //p.qfeSetVisualParameter("MPRP shape", shape);
  //this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("MPRUS mix (%)", 0, 10, 0, 100, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MPRUS contrast", 0.0, 5.0, -100.0, 100.0);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MPRUS brightness", 0.0, 5.0, -100.0, 100.0);
  this->qfeAddVisualParameter(p);
};

//----------------------------------------------------------------------------
qfeVisualReformatUltrasound::qfeVisualReformatUltrasound(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualReformat();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualReformatUltrasound::~qfeVisualReformatUltrasound()
{
};

//----------------------------------------------------------------------------
// qfeVisualPlanesArrowHeads
//----------------------------------------------------------------------------
qfeVisualPlanesArrowHeads::qfeVisualPlanesArrowHeads()
{
  this->technique = visualPlanesArrowHeads;

  qfeVisualParameter p;

  qfeSelectionList dataVectorProperties;
  dataVectorProperties.list.push_back("velocity");
  dataVectorProperties.active = 0;

  qfeSelectionList dataRepresentation;
  dataRepresentation.list.push_back("transfer function");  
  dataRepresentation.list.push_back("white");  
  dataRepresentation.active = 0;

  p.qfeSetVisualParameter("Arrows visible", false);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Arrows data vector", dataVectorProperties);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Arrows data representation", dataRepresentation, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Arrows spacing (mm)", 4.0, 0.5, 0.5, 40.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Arrows scaling factor", 2.0, 0.5, 0.0, 40.0);
  this->qfeAddVisualParameter(p);
};

//----------------------------------------------------------------------------
qfeVisualPlanesArrowHeads::qfeVisualPlanesArrowHeads(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualPlanesArrowHeads();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualPlanesArrowHeads::~qfeVisualPlanesArrowHeads()
{
};

//----------------------------------------------------------------------------
// qfeVisualAnatomyIllustrative
//----------------------------------------------------------------------------
qfeVisualAnatomyIllustrative::qfeVisualAnatomyIllustrative()
{
  this->technique    = visualAnatomyIllustrative;

  qfeVisualParameter p;

  qfeSelectionList shadingType;
  shadingType.list.push_back("toon 1");
  shadingType.list.push_back("toon 2");
  shadingType.list.push_back("fresnel");
  shadingType.list.push_back("diffuse");
  shadingType.list.push_back("flat");  
  shadingType.list.push_back("X-Ray");
  shadingType.list.push_back("Gooch");
  shadingType.active = 3;

  p.qfeSetVisualParameter("Surface shading", shadingType);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Surface clipping", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Surface contour", true);
  this->qfeAddVisualParameter(p);
};

//----------------------------------------------------------------------------
qfeVisualAnatomyIllustrative::qfeVisualAnatomyIllustrative(const qfeVisualAnatomyIllustrative &visual) : qfeVisual(visual)
{

}

//----------------------------------------------------------------------------
qfeVisualAnatomyIllustrative::~qfeVisualAnatomyIllustrative()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualAnatomyIllustrative::qfeSetVisualStudy(qfeStudy *study)
{
  qfeVisual::qfeSetVisualStudy(study);

  this->qfeUpdateVisual();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// qfeVisualFlowLines
//----------------------------------------------------------------------------
qfeVisualFlowLines::qfeVisualFlowLines()
{
  this->technique    = visualFlowLinesDepr;  
  this->ringCount    = 0;

  qfeVisualParameter p;

  qfeVector s;
  s.x = s.y = s.z = 0.0;

  qfeSelectionList l;
  l.list.push_back("none");
  l.list.push_back("velocity profile");
  l.list.push_back("streamlines");
  l.list.push_back("pathlines");
  l.active = 0;

  qfeSelectionList strat;
  strat.list.push_back("random: poisson disk");
  strat.list.push_back("random: normal distr.");
  strat.list.push_back("random: rectilinear distr.");
  strat.list.push_back("radial");
  strat.list.push_back("circles");
  strat.list.push_back("rectilinear");  
  strat.active = 0;

  qfeSelectionList templ;
  templ.list.push_back("homogeneous");
  templ.list.push_back("vessel center");
  templ.list.push_back("vessel wall");
  templ.active = 0;

  qfeSelectionList colorType;
  colorType.list.push_back("speed");
  colorType.list.push_back("propagation time");  
  colorType.list.push_back("interpolation distance");
  colorType.list.push_back("probe color");
  colorType.active = 0;

  qfeSelectionList shadingType;
  shadingType.list.push_back("flat");
  shadingType.list.push_back("illuminated");
  shadingType.active = 1;

  qfeSelectionList traceDir;
  traceDir.list.push_back("forward");
  traceDir.list.push_back("backward");  
  traceDir.active = 0;

  p.qfeSetVisualParameter("Line type", l);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Line seed strategy", strat);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Line seed transfer", templ);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Line seed density", 0.2, 0.05, 0.0, 4.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Line trace direction", traceDir);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Line trace step", 0.2, 0.01, 0.0, 5.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Line trace iterations", 50, 1, 0, 200);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Line color", colorType);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Line shading", shadingType);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Line highlight visible", false);
  this->qfeAddVisualParameter(p);

};

//----------------------------------------------------------------------------
qfeVisualFlowLines::qfeVisualFlowLines(const qfeVisualFlowLines &visual) : qfeVisual(visual)
{
  this->ringCount    = visual.ringCount;
}

//----------------------------------------------------------------------------
qfeVisualFlowLines::~qfeVisualFlowLines()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualFlowLines::qfeAddVisualRingParam(qfeVector seedPoint)
{
  this->ringCount++;

  stringstream label;

  qfeVisualParameter p;
  
  label << "Ring " << this->ringCount;

  qfeRingParam r;  
  r.visible        = true;
  r.seedPoint.x    = seedPoint.x;
  r.seedPoint.y    = seedPoint.y;
  r.seedPoint.z    = seedPoint.z;
  r.color.r        = 1.0;
  r.color.g        = 0.0;
  r.color.b        = 0.0;
  r.color.a        = 1.0;

  p.qfeSetVisualParameter(label.str(), r);
  this->qfeAddVisualParameter(p); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// qfeVisualFlowParticleTrace
//----------------------------------------------------------------------------
qfeVisualFlowParticleTrace::qfeVisualFlowParticleTrace()
{
  this->technique    = visualFlowParticles;    

  qfeVisualAction    a;
  qfeVisualParameter p;

  qfeSelectionList trailType;
  trailType.list.push_back("trail + halo");
  trailType.list.push_back("trail");
  trailType.list.push_back("none");
  trailType.active = 0;

  qfeSelectionList interpolationType;
  interpolationType.list.push_back("linear direct");
  interpolationType.list.push_back("linear separate");
  interpolationType.active = 0;

  qfeSelectionList integrationType;
  integrationType.list.push_back("RK 1 (euler-forward)");
  integrationType.list.push_back("RK 2 (midpoint)");
  integrationType.list.push_back("RK 4");
  integrationType.active = 2;

  qfeSelectionList seedingType;
  seedingType.list.push_back("random uniform");  
  seedingType.list.push_back("random poisson disk");  
  seedingType.active = 0;

  qfeSelectionList visualizationStyle;
  visualizationStyle.list.push_back("ellipsoid");  
  visualizationStyle.list.push_back("sphere");  
  visualizationStyle.active = 0;

  qfeSelectionList shadingType;
  shadingType.list.push_back("cel");
  shadingType.list.push_back("phong additive");
  shadingType.list.push_back("phong multiplicative");  
  shadingType.list.push_back("none");
  shadingType.active = 0;

  qfeSelectionList colorType;
  colorType.list.push_back("velocity");     
  colorType.list.push_back("vorticity");     
  colorType.list.push_back("color index");     
  colorType.list.push_back("seed time"); 
  colorType.active = 0;

  p.qfeSetVisualParameter("Particles visible", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particles count", 200, 10, 1, 10000);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particles size (mm)", 2, 1, 1, 10);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particles lifetime", 25, 1, 1, 500);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particles seeding", seedingType, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particles style", visualizationStyle);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particles color", colorType);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Particles shading", shadingType, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Particles contour", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particles trail", trailType);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particles trail lifetime", 0.5, 0.01, 0.0, 1.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Particles color scale (%)", 100, 5, 1, 1000);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particles integration", integrationType, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Particles steps/sample", 50, 1, 1, 1000, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Particles speed (%)", 100, 5, 1, 200, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  
  a.qfeSetVisualAction("Particles Inject", actionInjectSeeds);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Particles Inject File", actionInjectSeedsFile);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Particles Clear", actionClearSeeds);
  this->qfeAddVisualAction(a);
};

//----------------------------------------------------------------------------
qfeVisualFlowParticleTrace::qfeVisualFlowParticleTrace(const qfeVisualFlowParticleTrace &visual) : qfeVisual(visual)
{
}

//----------------------------------------------------------------------------
qfeVisualFlowParticleTrace::~qfeVisualFlowParticleTrace()
{

};

//----------------------------------------------------------------------------
// qfeVisualFlowLineTrace
//----------------------------------------------------------------------------
qfeVisualFlowLineTrace::qfeVisualFlowLineTrace()
{
  this->technique    = visualFlowLines;    

  qfeVisualAction    a;
  qfeVisualParameter p;
  
  qfeSelectionList lineStyle;
  lineStyle.list.push_back("tuboid");
  lineStyle.list.push_back("line");  
  lineStyle.active = 0;

  qfeSelectionList interpolationType;
  interpolationType.list.push_back("linear direct");
  interpolationType.list.push_back("linear separate");
  interpolationType.active = 0;

  qfeSelectionList shadingType;
  shadingType.list.push_back("cel");
  shadingType.list.push_back("phong additive");
  shadingType.list.push_back("phong multiplicative");  
  shadingType.list.push_back("none");
  shadingType.active = 0;

  qfeSelectionList integrationType;
  integrationType.list.push_back("RK 1 (euler-forward)");
  integrationType.list.push_back("RK 2 (midpoint)");
  integrationType.list.push_back("RK 4");
  integrationType.active = 2;

  qfeSelectionList seedingBehavior;
  seedingBehavior.list.push_back("seeding dynamic");
  seedingBehavior.list.push_back("seeding static");
  //lineType.list.push_back("streamline");
  seedingBehavior.active = 0;
  
  qfeSelectionList seedingType;
  seedingType.list.push_back("random uniform");  
  seedingType.list.push_back("random poisson disk");  
  seedingType.active = 1;

  p.qfeSetVisualParameter("Lines visible", true);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Lines count", 100, 10, 1, 10000);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lines phases dynamic", 3, 1, 1, 5);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lines phases static", 20, 1, 1, 60);
  this->qfeAddVisualParameter(p);

  //p.qfeSetVisualParameter("Lines maximum angle", 90, 1, 1, 180);
  //this->qfeAddVisualParameter(p);
/*
  p.qfeSetVisualParameter("Lines interpolation", interpolationType);
  this->qfeAddVisualParameter(p);
*/
  p.qfeSetVisualParameter("Lines style", lineStyle, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lines shading", shadingType, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lines width", 1.0, 0.1, 0.1, 10.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lines contour", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lines halo", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lines color scale (%)", 100, 5, 1, 1000);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lines integration", integrationType, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lines seeding behavior", seedingBehavior);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lines seeding type", seedingType, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lines steps/sample", 50, 1, 1, 1000, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  a.qfeSetVisualAction("Particles Inject", actionInjectSeeds);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Particles Inject File", actionInjectSeedsFile);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Particles Clear", actionClearSeeds);
  this->qfeAddVisualAction(a);
};

//----------------------------------------------------------------------------
qfeVisualFlowLineTrace::qfeVisualFlowLineTrace(const qfeVisualFlowLineTrace &visual) : qfeVisual(visual)
{
}

//----------------------------------------------------------------------------
qfeVisualFlowLineTrace::~qfeVisualFlowLineTrace()
{

};

//----------------------------------------------------------------------------
// qfeVisualFlowSurfaceTrace
//----------------------------------------------------------------------------
qfeVisualFlowSurfaceTrace::qfeVisualFlowSurfaceTrace()
{
  this->technique    = visualFlowSurfaces;    

  qfeVisualAction    a;
  qfeVisualParameter p;

  qfeSelectionList surfaceStyle;
  surfaceStyle.list.push_back("tube");
  surfaceStyle.list.push_back("strip");
  surfaceStyle.list.push_back("cross");
  surfaceStyle.active = 0; 
   
  qfeSelectionList integrationType;
  integrationType.list.push_back("RK 1 (euler-forward)");
  integrationType.list.push_back("RK 2 (midpoint)");
  integrationType.list.push_back("RK 4");
  integrationType.active = 2; 
  
  qfeSelectionList shadingType;
  shadingType.list.push_back("cel");
  shadingType.list.push_back("phong");
  shadingType.list.push_back("stripes");
  shadingType.list.push_back("none");
  shadingType.active = 2;
 
  p.qfeSetVisualParameter("Surface visible", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Surface style", surfaceStyle);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Surface shading", shadingType, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Surface trace phases", 1.0, 0.1, 0.01, 2.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Surface radius (mm)", 10.0, 0.5, 0.1, 200.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Surface tube count", 1, 1, 1, 10);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Surface integration", integrationType, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  a.qfeSetVisualAction("Particles Inject", actionInjectSeeds);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Particles Inject File", actionInjectSeedsFile);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Particles Clear", actionClearSeeds);
  this->qfeAddVisualAction(a);
};

//----------------------------------------------------------------------------
qfeVisualFlowSurfaceTrace::qfeVisualFlowSurfaceTrace(const qfeVisualFlowSurfaceTrace &visual) : qfeVisual(visual)
{
}

//----------------------------------------------------------------------------
qfeVisualFlowSurfaceTrace::~qfeVisualFlowSurfaceTrace()
{

};

//----------------------------------------------------------------------------
// qfeVisualFlowPlanes
//----------------------------------------------------------------------------
qfeVisualFlowPlanes::qfeVisualFlowPlanes()
{
  this->technique    = visualFlowPlanes;  
  this->ringCount    = 0;

  qfeVisualParameter p;

  qfeSelectionList t;
  t.list.push_back("none");
  t.list.push_back("integrated");
  t.list.push_back("exploded");
  t.active = 0;

  qfeSelectionList planeData;
  planeData.list.push_back("pca-p magnitude");
  planeData.list.push_back("pca-p x");
  planeData.list.push_back("pca-p y");
  planeData.list.push_back("pca-p z");
  planeData.list.push_back("pca-p color: RB");
  planeData.list.push_back("pca-p color: RGB");
  planeData.active = 0;

  p.qfeSetVisualParameter("Plane type", t);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Plane data", planeData);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Plane size", 35.0, 1.0, 0.0, 500.0);
  this->qfeAddVisualParameter(p); 
};

//----------------------------------------------------------------------------
qfeVisualFlowPlanes::qfeVisualFlowPlanes(const qfeVisualFlowPlanes &visual) : qfeVisual(visual)
{
  this->ringCount    = visual.ringCount;
}

//----------------------------------------------------------------------------
qfeVisualFlowPlanes::~qfeVisualFlowPlanes()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualFlowPlanes::qfeAddVisualRingParam(qfeVector seedPoint)
{
  this->ringCount++;

  stringstream label;

  qfeVisualParameter p;
  
  label << "Ring " << this->ringCount;

  qfeRingParam r;  
  r.visible        = true;
  r.seedPoint.x    = seedPoint.x;
  r.seedPoint.y    = seedPoint.y;
  r.seedPoint.z    = seedPoint.z;
  //r.dilation       = 0.0;
  r.color.r        = 1.0;
  r.color.g        = 0.0;
  r.color.b        = 0.0;
  r.color.a        = 1.0;

  p.qfeSetVisualParameter(label.str(), r);
  this->qfeAddVisualParameter(p); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// qfeVisualFlowArrowTrail
//----------------------------------------------------------------------------
qfeVisualFlowArrowTrails::qfeVisualFlowArrowTrails()
{
  this->technique    = visualFlowTrails;  
  this->ringCount    = 0;

  qfeVisualParameter p;

  qfeColorRGBA arrowColor;
  arrowColor.r = 0.8;
  arrowColor.g = 0.0;
  arrowColor.b = 0.0;
  arrowColor.a = 1.0;  

  p.qfeSetVisualParameter("Arrow visible", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Arrow trail", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Arrow maximum perc.", 20, 1, 0, 100);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Arrow scale", 0.2, 0.01, 0.0, 10.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Arrow color", arrowColor);
  this->qfeAddVisualParameter(p);
};

//----------------------------------------------------------------------------
qfeVisualFlowArrowTrails::qfeVisualFlowArrowTrails(const qfeVisualFlowArrowTrails &visual) : qfeVisual(visual)
{
  this->ringCount    = visual.ringCount;
}

//----------------------------------------------------------------------------
qfeVisualFlowArrowTrails::~qfeVisualFlowArrowTrails()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualFlowArrowTrails::qfeAddVisualRingParam(qfeVector seedPoint)
{
  this->ringCount++;

  stringstream label;

  qfeVisualParameter p;
  
  label << "Ring " << this->ringCount;

  qfeRingParam r;  
  r.visible        = true;
  r.seedPoint.x    = seedPoint.x;
  r.seedPoint.y    = seedPoint.y;
  r.seedPoint.z    = seedPoint.z;
  //r.dilation       = 0.0;
  r.color.r        = 1.0;
  r.color.g        = 0.0;
  r.color.b        = 0.0;
  r.color.a        = 1.0;

  p.qfeSetVisualParameter(label.str(), r);
  this->qfeAddVisualParameter(p); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// qfeVisualFlowProbe2D
//----------------------------------------------------------------------------
qfeVisualFlowProbe2D::qfeVisualFlowProbe2D()
{
  this->technique    = visualFlowProbe2D;  
  
  qfeVisualParameter p;
  qfeVisualAction    a;

  a.qfeSetVisualAction("Vessel Probe Add", actionNewProbeClick);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Vessel Probe Delete", actionDeleteProbe);
  this->qfeAddVisualAction(a);

  p.qfeSetVisualParameter("Probe visible", true);
  this->qfeAddVisualParameter(p);
};

//----------------------------------------------------------------------------
qfeVisualFlowProbe2D::qfeVisualFlowProbe2D(const qfeVisualFlowProbe2D &visual) : qfeVisual(visual)
{

}

//----------------------------------------------------------------------------
qfeVisualFlowProbe2D::~qfeVisualFlowProbe2D()
{

};


//----------------------------------------------------------------------------
// qfeVisualFlowProbe3D
//----------------------------------------------------------------------------
qfeVisualFlowProbe3D::qfeVisualFlowProbe3D()
{
  this->technique    = visualFlowProbe3D;  

  qfeSelectionList octreeType;
  octreeType.list.push_back("coherence");
  octreeType.list.push_back("level");
  octreeType.list.push_back("centerline");
  octreeType.active = 0;

  qfeSelectionList interactionType;
  interactionType.list.push_back("2 DOF");
  interactionType.list.push_back("3 DOF");
  interactionType.active = 0;

  qfeSelectionList probeAppearance;
  probeAppearance.list.push_back("glass");
  probeAppearance.list.push_back("metal");
  probeAppearance.list.push_back("phong");
  probeAppearance.list.push_back("transparent");
  probeAppearance.list.push_back("flat");
  probeAppearance.active = 0;

  qfeVisualParameter p;
  qfeVisualAction    a;

  a.qfeSetVisualAction("Heart Probe Add", actionNewProbeClick);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Heart Probe Delete", actionDeleteProbe);
  this->qfeAddVisualAction(a);

  //a.qfeSetVisualAction("Heart Probe Stroke", actionNewProbeStroke);
  //this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Probe Fit", actionProbeFit);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Probe Undo", actionProbeUndo);
  this->qfeAddVisualAction(a);

  p.qfeSetVisualParameter("Probe visible", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Probe appearance", probeAppearance, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Probe contour", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  //p.qfeSetVisualParameter("Probe centerline", false);
  //this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Probe axes", false);
  this->qfeAddVisualParameter(p); 
  
  p.qfeSetVisualParameter("Probe interaction", interactionType);
  this->qfeAddVisualParameter(p); 
  
  p.qfeSetVisualParameter("Octree visible", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Octree type", octreeType, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Octree coherence level", 0.5 , 0.05, 0.0, 1.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Octree hierarchy level",  3 , 1, 0, 10, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 
};

//----------------------------------------------------------------------------
qfeVisualFlowProbe3D::qfeVisualFlowProbe3D(const qfeVisualFlowProbe3D &visual) : qfeVisual(visual)
{

}

//----------------------------------------------------------------------------
qfeVisualFlowProbe3D::~qfeVisualFlowProbe3D()
{

};

//----------------------------------------------------------------------------
// qfeVisualClustering
//----------------------------------------------------------------------------
qfeVisualClustering::qfeVisualClustering()
{
  this->technique = visualFlowClustering;

  qfeSelectionList costMeasure;
  costMeasure.list.push_back("Euclidean distance (L2)");
  costMeasure.list.push_back("Elliptical distance");
  costMeasure.list.push_back("Elliptical distance (4D)");
  costMeasure.list.push_back("Linear model"); 
  costMeasure.list.push_back("Scalar model");
  costMeasure.active = 2;

  qfeSelectionList clusterSource;
  clusterSource.list.push_back("pca-p");
  clusterSource.list.push_back("ftle");
  clusterSource.active = 0;

  qfeVisualParameter p;
  qfeVisualAction    a;

  a.qfeSetVisualAction("Generate Clusters", actionGenerateClusters);
  this->qfeAddVisualAction(a);

  p.qfeSetVisualParameter("Source", clusterSource);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Clustering cost", costMeasure);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Elliptical - A", (double)0.9, 0.05, 0.0, 1.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Elliptical - B", (double)0.9, 0.05, 0.05, 0.95, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("Data selection (%)", (double)5.0, 0.5, 0.0, 100.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Hierarchical step size", (double)0.01, 0.01, 0.0, 100.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("Hierarchy level (%)", (double)0.0, 0.01, 0.0, 100.0);
  this->qfeAddVisualParameter(p);  
};

//----------------------------------------------------------------------------
qfeVisualClustering::qfeVisualClustering(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualClustering();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualClustering::~qfeVisualClustering()
{
};

//----------------------------------------------------------------------------
// qfeVisualClusterArrowTrace
//----------------------------------------------------------------------------
qfeVisualClusterArrowTrace::qfeVisualClusterArrowTrace()
{
  this->technique = visualFlowClusterArrows; 

  qfeColorRGBA col;

  col.r = 0.8f;
  col.g = 0.0f;
  col.b = 0.0f;
  col.a = 1.0f;

  qfeSelectionList seedStyle;
  seedStyle.list.push_back("3D dynamic");
  seedStyle.list.push_back("4D dynamic");    
  seedStyle.list.push_back("4D static");
  seedStyle.active = 2;

  qfeSelectionList lineStyle;
  lineStyle.list.push_back("tuboid");
  lineStyle.list.push_back("line");    
  lineStyle.list.push_back("arrow");
  lineStyle.active = 2;

  qfeSelectionList lineColor;
  lineColor.list.push_back("uniform");
  lineColor.list.push_back("speed");      
  lineColor.list.push_back("time");
  lineColor.list.push_back("static transparent");
  lineColor.list.push_back("static saturation");
  lineColor.active = 4;

  qfeVisualParameter p;
  qfeVisualAction    a;

  p.qfeSetVisualParameter("HCR centers visible", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("HCR arrows visible", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("HCR cluster mask", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("HCR length mask (mm)", 0.0, 1.0, 0.0, 1000.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("HCR seed style", seedStyle);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("HCR line style", lineStyle);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("HCR line width (mm)", 1.0, 0.2, 0.0, 20.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("HCR color type", lineColor);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("HCR color uniform", col, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("HCR color scale (%)", 100, 5, 1, 1000, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("HCR shading", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  p.qfeSetVisualParameter("HCR steps/sample", 50, 1, 1, 1000, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p); 

  a.qfeSetVisualAction("Particles Inject", actionInjectSeeds);
  this->qfeAddVisualAction(a);
};

//----------------------------------------------------------------------------
qfeVisualClusterArrowTrace::qfeVisualClusterArrowTrace(const qfeVisualClusterArrowTrace &visual) : qfeVisual(visual)
{
  qfeVisualClusterArrowTrace();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualClusterArrowTrace::~qfeVisualClusterArrowTrace()
{
};

//----------------------------------------------------------------------------
// qfeVisualPatternMatching
//----------------------------------------------------------------------------
qfeVisualPatternMatching::qfeVisualPatternMatching()
{
  this->technique = visualFlowPatternMatching; 

  qfeColorRGBA col;

  col.r = 0.8f;
  col.g = 0.0f;
  col.b = 0.0f;
  col.a = 1.0f;

  qfeSelectionList lineColor;
  lineColor.list.push_back("uniform");
  lineColor.list.push_back("pattern type");        
  lineColor.list.push_back("pattern clusters");
  lineColor.list.push_back("speed");
  lineColor.active = 1;

  qfeSelectionList visiblePhases;
  visiblePhases.list.push_back("all");
  visiblePhases.active = 0;
  
  qfeVisualParameter p;
  qfeVisualAction    a;

  p.qfeSetVisualParameter("PMR lines visible", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("PMR arrows visible", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("PMR seeds visible", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("PMR phase visible", visiblePhases);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("PMR lines span (ms)", 100.0, 5.0, 10.0, 200.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("PMR lines shading", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("PMR lines width (mm)", 2.0, 0.1, 0.5, 10.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("PMR arrow size (%)", 150, 10, 100, 400, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("PMR color type", lineColor);
  this->qfeAddVisualParameter(p);
  
  a.qfeSetVisualAction("Particles Inject File", actionInjectSeedsFile);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Particles Clear", actionClearSeeds);
  this->qfeAddVisualAction(a);
};

//----------------------------------------------------------------------------
qfeVisualPatternMatching::qfeVisualPatternMatching(const qfeVisualPatternMatching &visual) : qfeVisual(visual)
{
  qfeVisualPatternMatching();
  this->qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualPatternMatching::~qfeVisualPatternMatching()
{
}

//----------------------------------------------------------------------------
// qfeVisualAides
//----------------------------------------------------------------------------
qfeVisualAides::qfeVisualAides()
{
  this->technique    = visualAides;  
  
  qfeVisualParameter p;

  p.qfeSetVisualParameter("Support bounding box", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Support orientation marker", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Support axes", false);
  this->qfeAddVisualParameter(p);
}

//----------------------------------------------------------------------------
qfeVisualAides::qfeVisualAides(const qfeVisualAides &visual) : qfeVisual(visual)
{

}

//----------------------------------------------------------------------------
qfeVisualAides::~qfeVisualAides()
{

}

//----------------------------------------------------------------------------
// qfeVisualPostProcessing
//----------------------------------------------------------------------------
qfeVisualPostProcess::qfeVisualPostProcess()
{
  this->technique    = visualPostProcess;  
  
  qfeVisualParameter p;
  
  p.qfeSetVisualParameter("Depth map", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Normal map", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Depth darkening", false);
  this->qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("Depth darkening blend", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("Depth darkening power", 1.0, 0.1, 0.1, 50.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("Depth darkening kernel size", 12, 1, 1, 50.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("SSAO", false);
  this->qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("SSAO blend", true, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("SSAO power", 1.0, 0.05, 0.05, 4.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Contours", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Sobel", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Crosshatching", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Sharpen", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Gaussian Blur", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Gaussian Blur kernel size", 2, 1, 1, 50, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Bilateral filter", false);
  this->qfeAddVisualParameter(p);
    
  p.qfeSetVisualParameter("FQAA", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Tone mapping", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("TM Exposure", 5.0, 0.1, 0.0, 50.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
  
  p.qfeSetVisualParameter("Gamma correction", false);
  this->qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("Gamma", 1.0, 0.1, 0.1, 10.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Gamma", 1.0, 0.1, 0.1, 10.0, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Passe-partout", false);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("3D rendering", false);
  this->qfeAddVisualParameter(p);

  qfeSelectionList rendering3DType;
  rendering3DType.list.push_back("Crystal Eyes");
  rendering3DType.list.push_back("Stereo Red/Blue");        
  rendering3DType.list.push_back("Stereo Interlaced");
  rendering3DType.list.push_back("Left");
  rendering3DType.list.push_back("Right");
  //rendering3DType.list.push_back("Dresden");
  //rendering3DType.list.push_back("Anaglyph");
  //rendering3DType.list.push_back("Checkerboard");
  rendering3DType.active = 1;

  p.qfeSetVisualParameter("3D Rendering Type", rendering3DType, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);
}

//----------------------------------------------------------------------------
qfeVisualPostProcess::~qfeVisualPostProcess()
{
}

qfeVisualFilter::qfeVisualFilter()
{
  this->technique = visualFilterType;

  qfeVisualParameter p;

  qfeSelectionList dataSet;
  dataSet.list.push_back("pca-p");
  dataSet.list.push_back("pca-m");
  dataSet.list.push_back("ssfp");
  dataSet.list.push_back("t-mip");
  dataSet.list.push_back("ffe");
  dataSet.list.push_back("ftle");
  dataSet.active = 0;

  qfeSelectionList dataComponent;
  dataComponent.list.push_back("magnitude");
  dataComponent.list.push_back("x");
  dataComponent.list.push_back("y");
  dataComponent.list.push_back("z");
  dataComponent.active = 0;

  qfeSelectionList dataRepresentation;
  dataRepresentation.list.push_back("gray");
  dataRepresentation.list.push_back("transfer function");
  dataRepresentation.active = 0;

  p.qfeSetVisualParameter("MIP visible", true);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MIP data set", dataSet);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MIP data component", dataComponent, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MIP data representation", dataRepresentation, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MIP gradient", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MIP transparency", false, qfeVisualParameterAdvanced);
  this->qfeAddVisualParameter(p);  

  p.qfeSetVisualParameter("MIP contrast",   (float)0.0, (float)0.01, -1.0, 1.0);
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("MIP brightness", (float)0.0, (float)0.01, -1.0, 1.0);  
  this->qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Invert filters", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Magnitude filter", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Magnitude filter threshold", 0.09f, 0.01f, 0.0f, 1.0f);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("tMIP filter", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("tMIP filter threshold", 0.09f, 0.01f, 0.0f, 1.0f);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("StdDev magnitude filter", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("StdDev magnitude exponent", 0.5f, 0.1f, -1.f, 3.f);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("StdDev velocity filter", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("StdDev velocity exponent", 0.5f, 0.1f, -1.f, 3.f);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("StdDev phase span", 3, 1, 1, 50);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("StdDev threshold", 0.3f, 0.05f, 0.f, 1.f);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Gradient filter", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Vector median filter", false);
  qfeAddVisualParameter(p);

  qfeVisualAction a;
  a.qfeSetVisualAction("Save filtered data", actionSaveFilteredData);
  this->qfeAddVisualAction(a);
}

qfeVisualFilter::~qfeVisualFilter() {

}

//----------------------------------------------------------------------------
qfeVisualCEV::qfeVisualCEV() {
  technique = visualCEVType;

  qfeVisualParameter p;

  p.qfeSetVisualParameter("IVR visible", true);
  qfeAddVisualParameter(p);

  qfeSelectionList contextList;
  contextList.list.push_back("PCA-M");
  contextList.list.push_back("tMIP");
  contextList.active = 1;
  p.qfeSetVisualParameter("Context data", contextList);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Directional transparency", 0.f, 0.5f, -1.f, 32.f);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Depth transparency", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lit Sphere Map rendering", true);
  qfeAddVisualParameter(p);

  // Find current working directory and then lit sphere map directory
  char *cwd = (char *)calloc(_MAX_PATH, sizeof(char));
  _getcwd(cwd, _MAX_PATH);
  std::string scwd(cwd);
  std::string lsmDir = scwd + "\\lsm\\";

  // Find all png files in the lit sphere map directory
  std::vector<QString> litSphereMapFiles;
  QDirIterator dirIt(lsmDir.c_str(),QDirIterator::Subdirectories);

  while (dirIt.hasNext()) 
  {
	dirIt.next();
	if (QFileInfo(dirIt.filePath()).isFile()) 
	{
	  if (QFileInfo(dirIt.filePath()).suffix() == "png") 
	  {
		QString fileName = dirIt.fileName();
		fileName = fileName.left(fileName.size() - 4);
		litSphereMapFiles.push_back(fileName);
	  }
	}
  }

  qfeSelectionList lsmPreset;
  // Add all lit sphere map files as options
  lsmPreset.list.push_back("dots");
  for (auto i = litSphereMapFiles.begin(); i != litSphereMapFiles.end(); i++)
	lsmPreset.list.push_back((*i).toUtf8().constData());
  lsmPreset.active = 4;
  p.qfeSetVisualParameter("Lit Sphere Map preset", lsmPreset);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Sample gradient", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Depth Tone Mapping", true);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Depth Tone Mapping factor", 0.6f, 0.1f, 0.f, 1.f);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Dot LSM: Number of dots", 512*512, 1, 512, 512*512*8);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Dot LSM: Lambda", 8.f, 0.5f, 0.f, 24.f);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Simple opacity", 1.f, 0.05f, 0.f, 1.f);
  qfeAddVisualParameter(p);

  qfeSelectionList traceDirList;
  traceDirList.list.push_back("Forward");
  traceDirList.list.push_back("Backward");
  traceDirList.active = 0;
  p.qfeSetVisualParameter("Trace direction", traceDirList);
  qfeAddVisualParameter(p);
};

//----------------------------------------------------------------------------
qfeVisualCEV::qfeVisualCEV(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualCEV();
  qfeSetVisualStudy(study);
}

//----------------------------------------------------------------------------
qfeVisualCEV::~qfeVisualCEV() {
}

qfeVisualSeedEllipsoid::qfeVisualSeedEllipsoid()
{
  technique = visualSeedEllipsoidType;

  qfeVisualParameter p;
  
  p.qfeSetVisualParameter("Ellipsoid visible", true);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Surface visible", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Guide points visible", true);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Seed points visible", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Seed left ventricle", true);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Number of seed points LV", 500, 10, 10, 3000);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Seed right ventricle", true);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Number of seed points RV", 500, 10, 10, 3000);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Subtract LVE from RVE", true);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Seed on clip plane", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Continuous seeding", false);
  qfeAddVisualParameter(p);

  qfeVisualAction a;
  a.qfeSetVisualAction("Clipped Ellipsoid LV", actionPlaceLVGuidingPoints);
  qfeAddVisualAction(a);

  a.qfeSetVisualAction("Clipped Ellipsoid RV", actionPlaceRVGuidingPoints);
  qfeAddVisualAction(a);
}

qfeVisualSeedEllipsoid::qfeVisualSeedEllipsoid(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap)
{
  qfeVisualSeedEllipsoid();
  qfeSetVisualStudy(study);
}

qfeVisualSeedEllipsoid::~qfeVisualSeedEllipsoid() {}

qfeVisualSeedVolumes::qfeVisualSeedVolumes()
{
  technique = visualSeedVolumesType;

  qfeVisualParameter p;
  qfeVisualAction a;
  
  p.qfeSetVisualParameter("Seed Volumes Visible", true);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Surfaces visible", false);
  qfeAddVisualParameter(p);
    
  a.qfeSetVisualAction("Add Seed Volume", actionAddSeedVolume);
  this->qfeAddVisualAction(a);
	
  a.qfeSetVisualAction("Remove Seed Volume", actionRemoveSeedVolume);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("Save", actionSaveSeedVolumes);
  this->qfeAddVisualAction(a);

  a.qfeSetVisualAction("OpenFile", actionLoadSeedVolumes);
  this->qfeAddVisualAction(a);
}

qfeVisualSeedVolumes::~qfeVisualSeedVolumes()
{
}

qfeVisualPlaneOrthoView::qfeVisualPlaneOrthoView()
{
  technique = visualPlaneOrthoViewType;

  qfeVisualParameter p;

  p.qfeSetVisualParameter("Show view orthogonal plane", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Lock plane", false);
  qfeAddVisualParameter(p);
}

qfeVisualPlaneOrthoView::~qfeVisualPlaneOrthoView(){}

qfeVisualFeatureSeeding::qfeVisualFeatureSeeding()
{
  technique = visualFeatureSeedingType;

  qfeVisualParameter p;
  p.qfeSetVisualParameter("Show volume rendering", false);
  qfeAddVisualParameter(p);
  p.qfeSetVisualParameter("Base probability", 0.35f, 0.05f, 0.f, 1.f);
  qfeAddVisualParameter(p);

  qfeVisualAction a;
  a.qfeSetVisualAction("Seed Points Feature-based", actionPlaceFeatureSeedPoints);
  qfeAddVisualAction(a);
}

qfeVisualFeatureSeeding::~qfeVisualFeatureSeeding(){}

qfeVisualVolumeOfInterest::qfeVisualVolumeOfInterest()
{
  technique = visualVolumeOfInterest;

  qfeVisualParameter p;
  qfeVisualAction a;

  p.qfeSetVisualParameter("VoI visible", true);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Surface visible", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Guide points visible", true);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Clip context visualization", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Clip probability visualization", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Clip seed points", false);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Clip all data", false);
  qfeAddVisualParameter(p);

  a.qfeSetVisualAction("Volume of Interest", actionPlaceVolumeOfInterest);
  qfeAddVisualAction(a);
}

qfeVisualVolumeOfInterest::~qfeVisualVolumeOfInterest() {}

qfeVisualProbabilityDVR::qfeVisualProbabilityDVR()
{
  technique = visualProbabilityDVR;

  qfeVisualParameter p;
  qfeVisualAction a;

  p.qfeSetVisualParameter("Probability visible", true);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Depth Tone Mapping", true);
  qfeAddVisualParameter(p);

  p.qfeSetVisualParameter("Depth Tone Mapping factor", 0.6f, 0.1f, 0.f, 1.f);
  qfeAddVisualParameter(p);
}

qfeVisualProbabilityDVR::~qfeVisualProbabilityDVR() {}
