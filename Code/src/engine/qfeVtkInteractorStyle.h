#pragma once

#include "qflowexplorer.h"

#include "vtkCommand.h"
#include "vtkCallbackCommand.h"
#include "vtkCallbackTimerOneShot.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkTDxInteractorStyleCamera.h"
#include "qfePoint.h"

enum qfeCustomEvents
{
  DoubleClickEvent = vtkCommand::UserEvent + 100
};

/**
* \file   qfeVtkInteractorStyle.h
* \author Roy van Pelt
* \class  qfeVtkInteractorStyle
* \brief  Implements a VTK interactorstyle
*
* Implements a VTK interactorstyle
*/
class qfeVtkInteractorStyle : public vtkInteractorStyleTrackballCamera
{
public:
  static qfeVtkInteractorStyle *New();

  virtual void OnTimer();

  virtual void OnMouseMove();
  virtual void OnLeftButtonDown();
  virtual void OnLeftButtonUp();
  virtual void OnRightButtonDown();
  virtual void OnRightButtonUp();
  virtual void OnMouseWheelForward();
  virtual void OnMouseWheelBackward();
  virtual void OnKeyPress();

  //virtual void On3DMouseMove();

  virtual void OnParentMouseMove();
  virtual void OnParentLeftButtonDown();
  virtual void OnParentLeftButtonUp();
  virtual void OnParentRightButtonDown();
  virtual void OnParentRightButtonUp();
  virtual void OnParentWheelForward();
  virtual void OnParentWheelBackward();
  virtual void OnParentKeyPress();

  //virtual void OnParent3DMouseMove();

  bool handleParentMove;
  bool handleParentLeftDown;
  bool handleParentLeftUp;
  bool handleParentRightDown;
  bool handleParentRightUp;
  bool handleParentWheelForward;
  bool handleParentWheelBackward;
  bool handleParentKeyPress;

  //bool handleParent3DMove;

  static const vtkIdType DoubleClickEvent = vtkCommand::UserEvent + 100; 

protected:
  qfeVtkInteractorStyle();
	~qfeVtkInteractorStyle();

  unsigned int numberOfClicks;
  int          previousPosition[2];
  int          resetPixelDistance;

  vtkCallbackTimerOneShot *timerCallback;
};
