#pragma once

#include <vtkAnnotatedCubeActor.h>
#include <vtkCallbackCommand.h>
#include <vtkCamera.h>
#include <vtkCommand.h>
#include <vtkImageData.h>
#include <vtkLight.h>
#include <vtkLightCollection.h>
#include <vtkMatrix4x4.h>
#include <vtkMultiThreader.h>
#include <vtkMutexLock.h>
#include <vtkObjectFactory.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>
#include <vtkVolume.h>
#include <vtkVolumeRayCastMapper.h>
#include <vtkAssembly.h>

#include "qfeCallbackProgressBar.h"
#include "qfeVtkInteractorStyle.h"
#include "qfeViewport.h"
#include "vtkMedicalCanvas.h"
#include "vtkSubCanvas.h"

#include "vtkTDxInteractorStyleCamera.h" 
#include "vtkTDxInteractorStyleSettings.h" 

#include <vtkWindowToImageFilter.h>
#include <vtkPNGWriter.h>
//Qt includes
#include <QDialog.h>
#include <QDialogButtonBox.h>
#include <QRadioButton.h>
#include <QLabel.h>
#include <QLineEdit.h>
#include <QRegExpValidator>
#include <QVBoxLayout>

#include <sstream>

class vtkDriverMapper;

enum vtkEngineLayout
{
  vtkEngineLayoutSingle,
  vtkEngineLayoutQuad,
  vtkEngineLayoutLeft,
  vtkEngineLayoutRight,
  vtkEngineLayoutUS
};

/**
* \file   qfeVTKEngine.h
* \author Roy van Pelt
* \class  qfeVtkEngine
* \brief  Implements a wrapper for the VTK engine
*
* Implements a wrapper for the VTK engine
*/
class qfeVtkEngine
{
public:
  static qfeVtkEngine* qfeGetInstance();
  static void          qfeDeleteInstance();

  void Attach(vtkRenderWindow *rw);
  void Detach(vtkRenderWindow *rw);
  void Start();
  void Stop();

  // Viewport sizes
  void GetViewport3D(qfeViewport **vp); 
  void GetViewport2D(int index, qfeViewport **vp); 

  // Layout
  void SetLayout(vtkEngineLayout layout, bool fullScreen, bool clickable);

  // Render Events  
  void SetObserverAction(void *caller, void (*f)(void * c, int t)); 
  void SetObserverRender(void *caller, void (*f)(void * c)); 
  void SetObserverRender1(void *caller, void (*f)(void * c)); 
  void SetObserverRender2(void *caller, void (*f)(void * c)); 
  void SetObserverRender3(void *caller, void (*f)(void * c)); 
  void SetObserverResize(void *caller, void (*f)(void * c)); 
  void SetObserverTimer(void *caller, void (*f)(void * c)); 

  // Interaction
  void SetInteractionMouseMove(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction); 
  void SetInteractionMouseLeftButtonDown(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction);
  void SetInteractionMouseLeftButtonUp(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction);
  void SetInteractionMouseRightButtonDown(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction);
  void SetInteractionMouseRightButtonUp(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction);
  void SetInteractionMouseWheelForward(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction);
  void SetInteractionMouseWheelBackward(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction);
  void SetInteractionKeyPress(void *caller, void (*f)(void * c, char * k), bool parentInteraction);

  void SetInteractionMouseMove(void *caller, void (*f)(void * c, int x, int y, int v));   
  void SetInteractionMouseLeftButtonDown(void *caller, void (*f)(void * c, int x, int y, int v));
  void SetInteractionMouseLeftButtonUp(void *caller, void (*f)(void * c, int x, int y, int v));
  void SetInteractionMouseRightButtonDown(void *caller, void (*f)(void * c, int x, int y, int v));
  void SetInteractionMouseRightButtonUp(void *caller, void (*f)(void * c, int x, int y, int v));
  void SetInteractionMouseWheelForward(void *caller, void (*f)(void * c, int x, int y, int v));
  void SetInteractionMouseWheelBackward(void *caller, void (*f)(void * c, int x, int y, int v));
  void SetInteractionKeyPress(void *caller, void (*f)(void * c, char * k));
  
  void CallInteractionParentMouseMove();
  void CallInteractionParentLeftButtonDown();
  void CallInteractionParentLeftButtonUp();
  void CallInteractionParentRightButtonDown();
  void CallInteractionParentRightButtonUp();
  void CallInteractionParentWheelForward();
  void CallInteractionParentWheelBackward();

  bool GetInteractionControlKey();
  bool GetInteractionAltKey();

  // Camera
  void GetViewUp(double &ux, double &uy, double &uz);
  void GetViewUp(qfeVector &viewUp);
  void Rotate(double angle);
  void Azimuth(double angle);
  void Elevation(double angle);
  void Elevation(int viewport, double angle);

  void Refresh();

  // Orientation marker
  void OrientationMarkerEnable();
  void OrientationMarkerEnable(float xx, float xy, float xz, float yx, float yy, float yz, float zx, float zy, float zz);
  void OrientationMarkerDisable();

  // Status message
  void SetStatusMessage(std::string text);

  // Screenshot
  void StoreScreenshot();
  void StoreScreenshot(unsigned int pixel_width, unsigned int pixel_height);

  vtkRenderer *GetRenderer()          {return this->renderer[0];};
  vtkRenderer *GetRenderer(int index) {if(index<0 || index>3) return this->renderer[0];
                                       else                   return this->renderer[index];};

  vtkSmartPointer<vtkMedicalCanvas>   canvas;

protected :
 static void processActions(vtkObject* object, unsigned long event, void* clientdata, void* calldata); 
 static void processEvents(vtkObject* object, unsigned long event, void* clientdata, void* calldata); 
 static void processRender(vtkObject* object, unsigned long event, void* clientdata, void* calldata); 
 static void processRender1(vtkObject* object, unsigned long event, void* clientdata, void* calldata); 
 static void processRender2(vtkObject* object, unsigned long event, void* clientdata, void* calldata); 
 static void processRender3(vtkObject* object, unsigned long event, void* clientdata, void* calldata); 
 static void processResize(vtkObject* object, unsigned long event, void* clientdata, void* calldata); 

 void qfeOrientationMarkerInit();
 
private :
  static qfeVtkEngine                *singleton;
  static bool                         singletonFlag;

  vtkRenderWindowInteractor          *interactor;
  qfeVtkInteractorStyle              *interactorStyle;
  vtkTDxInteractorStyleCamera        *interactorStyleTDx;

  vtkSmartPointer<vtkRenderer>        renderer[4];      
  vtkSmartPointer<vtkDriverMapper>    mapper[4];
  qfeViewport                         viewport[4];
  vtkSmartPointer<vtkVolume>          volume[4];
  vtkSmartPointer<vtkImageData>       data;  

  bool                                layoutClickable;
  
  vtkSmartPointer<vtkCallbackCommand> cbResize;

  vtkSmartPointer<vtkCallbackCommand> actionCallbackCommand;  
  vtkSmartPointer<vtkCallbackCommand> eventCallbackCommand;  
  vtkSmartPointer<vtkCallbackCommand> renderCallbackCommand[4];
  vtkSmartPointer<vtkCallbackCommand> resizeCallbackCommand;

  void (*functionAction)        (void *, int);
  void (*functionRender)        (void *);
  void (*functionRender1)       (void *);
  void (*functionRender2)       (void *);
  void (*functionRender3)       (void *);
  void (*functionResize)        (void *);
  void (*functionSelect)        (void *);
  void (*functionTimer)         (void *);
  void (*functionMouseMove)     (void *, int, int, int);
  void (*functionMouseLeftDown) (void *, int, int, int);
  void (*functionMouseLeftUp)   (void *, int, int, int);
  void (*functionMouseRightDown)(void *, int, int, int);
  void (*functionMouseRightUp)  (void *, int, int, int);
  void (*functionWheelForward)  (void *, int, int, int);
  void (*functionWheelBackward) (void *, int, int, int);
  void (*functionKeyPress)      (void *, char *);  

  void *callerAction;
  void *callerRender[4];
  void *callerResize;
  void *callerTimer;
  void *callerSelect;
  void *callerMouseMove;
  void *callerMouseLeftDown;
  void *callerMouseLeftUp;
  void *callerMouseRightDown;
  void *callerMouseRightUp;
  void *callerWheelForward;
  void *callerWheelBackward;
  void *callerKeyPress;  

  vtkSmartPointer<vtkOrientationMarkerWidget> orientationMarker;
  vtkSmartPointer<vtkAnnotatedCubeActor>      orientationCube;

  qfeVtkEngine();                                 // Private constructor
  qfeVtkEngine(const qfeVtkEngine&);              // Prevent copy-construction
  qfeVtkEngine& operator=(const qfeVtkEngine&);   // Prevent assignment
  ~qfeVtkEngine();                                // Private destructor

  void handleDoubleClick(int x, int y);
  void qfeClearCallbacks();
};

/**
* \file   qfeDriver.h
* \author Roy van Pelt
* \class  vtkDriverMapper
* \brief  Implements a dummy volume mapper
*
* Implements a dummy volume mapper. The mapper invokes a ProgressEvent of type vtkCommand.
* A render function can be attached to attached externally to this event.
*/
class vtkDriverMapper : public vtkVolumeMapper
{
public :
  static vtkDriverMapper *New();

  void Render(vtkRenderer *,vtkVolume *){this->InvokeEvent(vtkCommand::ProgressEvent);};

protected :
  vtkDriverMapper(){};
  ~vtkDriverMapper(){};

};