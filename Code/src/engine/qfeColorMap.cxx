#include "qfeColorMap.h"

#include "qfeConvert.h"

//----------------------------------------------------------------------------
qfeColorMap::qfeColorMap()
{
  /*GLenum err = glewInit();
  if (GLEW_OK != err)
  {
    cout << "qfeColorMap::qfeColorMap - " << glewGetErrorString(err) << endl;
  }*/

  this->nRGB = 0;
  this->nA   = 0;
  this->nG   = 0;

  this->mRGB.clear();
  this->mA.clear();
  this->mG.clear();

  this->interpolation       = qfeColorInterpolationNearest;
  this->colorspace          = qfeColorSpaceRGB;
  this->constantlightness   = false;
  this->paddinguniform      = false;

  this->quantization        = 7;

  this->texId               = 0;
  this->texGradId           = 0;
  this->texUploaded         = false;
  this->texGradUploaded     = false;

};

//----------------------------------------------------------------------------
qfeColorMap::qfeColorMap(const qfeColorMap &colormap)
{
  this->mRGB              = colormap.mRGB;
  this->mA                = colormap.mA;
  this->mG                = colormap.mG;
  this->nRGB              = colormap.nRGB;
  this->nA                = colormap.nA;
  this->nG                = colormap.nG;
  this->texId             = colormap.texId;
  this->texGradId         = colormap.texGradId;
  this->texUploaded       = colormap.texUploaded;
  this->texGradUploaded   = colormap.texGradUploaded;
  this->interpolation     = colormap.interpolation;
  this->quantization      = colormap.quantization;
  this->colorspace        = colormap.colorspace;
  this->constantlightness = colormap.constantlightness;
  this->paddinguniform    = colormap.paddinguniform;

};

//----------------------------------------------------------------------------
qfeColorMap::~qfeColorMap()
{
  this->mRGB.clear();
  this->mA.clear();
  this->mG.clear();

  if (glIsTexture(this->texId))
  {
    glDeleteTextures(1, (GLuint *)&this->texId);
    this->texId = 0;
  }

  if (glIsTexture(this->texGradId))
  {
    glDeleteTextures(1, (GLuint *)&this->texGradId);
    this->texGradId = 0;
  }
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeSetColorMapRGB(unsigned int n, vector<qfeRGBMapping>  mRGB)
{
  this->nRGB = n;
  this->mRGB = mRGB;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetColorMapRGB(unsigned int &n, vector<qfeRGBMapping>  &mRGB)
{
  n    = this->nRGB;
  mRGB = this->mRGB;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeSetColorMapA(unsigned int n, vector<qfeOpacityMapping>  mA)
{
  this->nA = n;
  this->mA = mA;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetColorMapA(unsigned int &n, vector<qfeOpacityMapping>  &mA)
{
  n  = this->nA;
  mA = this->mA;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeSetColorMapG(unsigned int n, vector<qfeOpacityMapping>  mG)
{
  this->nG = n;
  this->mG = mG;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetColorMapG(unsigned int &n, vector<qfeOpacityMapping>  &mG)
{
  n  = this->nG;
  mG = this->mG;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetColorMapRGBSize(unsigned int &n)
{
  n  = this->nRGB;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetColorMapASize(unsigned int &n)
{
  n  = this->nA;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetColorMapInterpolation(int &type)
{
  type = this->interpolation;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeSetColorMapInterpolation(int type)
{
  this->interpolation = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetColorMapQuantization(int &value)
{
  value = this->quantization;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeSetColorMapQuantization(int value)
{
  this->quantization = value;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetColorMapSpace(int &type)
{
  type = this->colorspace;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeSetColorMapSpace(int type)
{
  this->colorspace = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetColorMapConstantLightness(bool &l)
{
  l = this->constantlightness;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeSetColorMapConstantLightness(bool l)
{
  this->constantlightness = l;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetColorMapPaddingUniform(bool &p)
{
  p = this->paddinguniform;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeSetColorMapPaddingUniform(bool p)
{
  this->paddinguniform = p;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetColorMapTextureId(GLuint &id)
{
  id = this->texId;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeGetGradientMapTextureId(GLuint &id)
{
  id = this->texGradId;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeCopyColorMap(qfeColorMap map)
{
  map.qfeGetColorMapRGB(this->nRGB, this->mRGB);
  map.qfeGetColorMapA(this->nA, this->mA);
  map.qfeGetColorMapG(this->nG, this->mG);
  map.qfeGetColorMapInterpolation(this->interpolation);
  map.qfeGetColorMapQuantization(this->quantization);
  map.qfeGetColorMapSpace(this->colorspace);
  map.qfeGetColorMapConstantLightness(this->constantlightness);
  map.qfeGetColorMapPaddingUniform(this->paddinguniform);

  if(texUploaded)
    this->qfeUploadColorMap();

  if(texGradUploaded)
    this->qfeUploadGradientMap();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeUploadColorMap()
{ 
  if (glIsTexture(this->texId))
  {
    glDeleteTextures(1, (GLuint *)&this->texId);
    this->texId = 0;
  }

  int table_size = 256;

  float *tfTable       = new float[table_size * 4];

  vector< qfeFloat>      opacityGradient;
  vector<qfeColorRGB>    colorGradient;

  if(tfTable == NULL) return qfeError;
  if(this->mRGB.size() <= 0 || this->mA.size() <= 0) return qfeError;

  // The given control points should be in the range [0, 1]
  // There has to be a control point on 0 and 1 for both color and opacity

  // Linearly interpolate the opacity
  this->qfeComputeOpacityLinearGradient(this->mA, opacityGradient, table_size);

  // Linearly interpolate the color
  this->qfeComputeColorLinearGradient(this->mRGB, colorGradient, table_size, this->constantlightness);

  // Fill the transfer function texture values
  int tf_index = 0, color_index = 0, scalar_index = 0;
  for(int i = 0; i < table_size; i++)
  {
    if(this->interpolation == qfeColorInterpolationNearest)
    {
      int qindex = i * this->quantization / (qfeFloat)table_size; 
      int index = (qindex + 0.5) * table_size / (qfeFloat)this->quantization;

      tfTable[tf_index+0] = colorGradient.at(index).r;
      tfTable[tf_index+1] = colorGradient.at(index).g;
      tfTable[tf_index+2] = colorGradient.at(index).b;
    }
    else
    {
      tfTable[tf_index+0] = colorGradient.at(i).r;
      tfTable[tf_index+1] = colorGradient.at(i).g;
      tfTable[tf_index+2] = colorGradient.at(i).b;
    } 
    tfTable[tf_index+3] = opacityGradient.at(scalar_index);

    tf_index     += 4;
    color_index  += 3;
    scalar_index += 1;
  } 

  glGenTextures(1, &this->texId);
  glBindTexture  (GL_TEXTURE_1D, this->texId);
  glTexImage1D   (GL_TEXTURE_1D, 0, GL_RGBA, table_size, 0, GL_RGBA, GL_FLOAT, tfTable);
  glTexEnvi      (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,   GL_REPLACE);
  glTexParameterf(GL_TEXTURE_1D,  GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_1D,  GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_1D,  GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
  glBindTexture  (GL_TEXTURE_1D, 0);

  delete tfTable;

  this->texUploaded = true;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeUploadGradientMap()
{ 
  if (glIsTexture(this->texGradId))
  {
    glDeleteTextures(1, (GLuint *)&this->texGradId);
    this->texGradId = 0;
  }

  int table_size = 256;

  float *tfTable       = new float[table_size];

  vector< qfeFloat>      opacityGradient;

  if(this->mG.size() <= 0) return qfeError;

  // The given control points should be in the range [0, 1]
  // There has to be a control point on 0 and 1 for both color and opacity

  // Linearly interpolate the opacity
  this->qfeComputeOpacityLinearGradient(this->mG, opacityGradient, table_size);

  // Fill the transfer function texture values
  int tf_index = 0, color_index = 0, scalar_index = 0;
  for(int i = 0; i < table_size; i++)
  {    
    tfTable[i] = opacityGradient.at(i);  
    //cout << i << ": " << tfTable[i] << endl;
  } 

  glGenTextures(1, &this->texGradId);
  glBindTexture  (GL_TEXTURE_1D, this->texGradId);
  glTexImage1D   (GL_TEXTURE_1D, 0, GL_ALPHA, table_size, 0, GL_ALPHA, GL_FLOAT, tfTable);
  glTexEnvi      (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,   GL_REPLACE);
  glTexParameterf(GL_TEXTURE_1D,  GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_1D,  GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_1D,  GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
  glBindTexture  (GL_TEXTURE_1D, 0);

  delete tfTable;

  this->texGradUploaded = true;

  return qfeSuccess;
}

qfeReturnStatus qfeColorMap::qfeGetAllColorsRGB(vector<qfeColorRGB> &result)
{	
	result.clear();
	result.resize(256);
	
	this->qfeComputeColorLinearGradient(this->mRGB, result, 256, this->constantlightness);

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeComputeOpacityLinearGradient(vector< qfeOpacityMapping > map, vector< qfeFloat > &table, int tablesize)
{
  qfeFloat     currentPos, nextPos;
  qfeFloat     currentOpacity, nextOpacity;

  vector< qfeOpacityMapping > points = map;

  table.clear();  

  // Linearly interpolate between the color points
  for(int i=0; i<(int)points.size()-1; i++)
  {    
    currentPos = points.at(i).value;
    nextPos    = points.at(i+1).value;

    currentOpacity = points.at(i).opacity;   
    nextOpacity    = points.at(i+1).opacity; 

    int steps = (int)((nextPos - currentPos)*tablesize);

    // Fill the color gradient
    for (int j = 0; j < steps; j++)
    {    
      qfeFloat opacity, amount;

      amount  = (qfeFloat)j / (qfeFloat)(steps);
      opacity =  qfeConvertColor::lerp(currentOpacity, nextOpacity, amount);

      table.push_back(opacity);    
    }  
  }

  // Check the table size, and fill if necessary
  int currentSize = table.size();
  if(currentSize < tablesize)
  {
    for(int i=currentSize; i<tablesize; i++)
    {
      table.push_back(table.at(currentSize-1));
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeColorMap::qfeComputeColorLinearGradient(vector< qfeRGBMapping > map, vector< qfeColorRGB > &table, int tablesize, bool constlight)
{
  qfeFloat     currentPos, nextPos;
  qfeColorRGB  cRGB, nRGB;
  qfeColorXYZ  cXYZ, nXYZ, XYZ;
  qfeColorLuv  cLuv, nLuv, Luv;
  qfeColorLab  cLab, nLab, Lab;

  vector< qfeRGBMapping > points = map;

  qfeRGBMapping first, last;

  first.value = 0.0;
  first.color = points.front().color;
  last.value  = 1.0;
  last.color  = points.back().color;

  points.insert(points.begin(), first);
  points.push_back(last);

  table.clear();  

  // Uniform color padding
  qfeColorRGB paddingColor;
  paddingColor.r = paddingColor.g = paddingColor.b =  1.0;

  if(this->paddinguniform)
  {
    points.front().color = paddingColor;
    points.back().color  = paddingColor;
  } 

  // Linearly interpolate between the color points
  for(int i=0; i<(int)points.size()-1; i++)
  {    
    currentPos = points.at(i).value;
    nextPos    = points.at(i+1).value;

    cRGB = points.at(i).color;   
    nRGB = points.at(i+1).color; 
    
    int steps = (int)((nextPos - currentPos)*tablesize);

    // Fill the color gradient
    for (int j = 0; j < steps; j++)
    {    
      qfeColorRGB RGB;

      double amount = (double)j / (double)(steps);

      // For constant lightness, set the lightness to the
      // best possible value, remaining sufficient chromaticity

      switch(this->colorspace)
      {
      case qfeColorSpaceRGB : RGB = qfeConvertColor::lerp(cRGB, nRGB, amount);
        break;
      case qfeColorSpaceLuv : qfeConvertColor::qfeRgb2Xyz(cRGB, cXYZ);
        qfeConvertColor::qfeXyz2Luv(cXYZ, cLuv, qfeConvertColor::whiteD65);
        qfeConvertColor::qfeRgb2Xyz(nRGB, nXYZ);
        qfeConvertColor::qfeXyz2Luv(nXYZ, nLuv, qfeConvertColor::whiteD65);

        Luv = qfeConvertColor::lerp(cLuv, nLuv, amount);

        if(constlight) Luv.L = 72.1;

        qfeConvertColor::qfeLuv2Xyz(Luv, XYZ, qfeConvertColor::whiteD65);
        qfeConvertColor::qfeXyz2Rgb(XYZ, RGB);

        RGB = qfeConvertColor::clamp(RGB);
        break;
      case qfeColorSpaceLab : qfeConvertColor::qfeRgb2Xyz(cRGB, cXYZ);
        qfeConvertColor::qfeXyz2Lab(cXYZ, cLab, qfeConvertColor::whiteD65);
        qfeConvertColor::qfeRgb2Xyz(nRGB, nXYZ);
        qfeConvertColor::qfeXyz2Lab(nXYZ, nLab, qfeConvertColor::whiteD65); 

        Lab = qfeConvertColor::lerp(cLab, nLab, amount);
        
        if(constlight) Lab.L = 72.1;

        qfeConvertColor::qfeLab2Xyz(Lab, XYZ, qfeConvertColor::whiteD65);
        qfeConvertColor::qfeXyz2Rgb(XYZ, RGB);

        RGB = qfeConvertColor::clamp(RGB);
        break;
      default :  RGB = qfeConvertColor::lerp(cRGB, nRGB, amount);
        break;
      }

      if(this->paddinguniform)
      {
        if(i==0)                      
          RGB = cRGB; // padding left  

        if(i==((int)points.size()-2)) 
          RGB = nRGB; // padding right
      } 

      table.push_back(RGB);    
    }  
  }

  // Check the table size, and fill if necessary
  int currentSize = table.size();
  if(currentSize < tablesize)
  {
    for(int i=currentSize; i<tablesize; i++)
    {
      table.push_back(table.at(currentSize-1));
    }
  }
  
  //if nearest neighbor interpolation is selected we update the table
  if(this->interpolation == qfeColorInterpolationNearest)
  {
	  vector<qfeColorRGB> table_copy = table;

	  for(int i=0; i<tablesize; i++)
	  {
		  float index = (float)i/(float)(tablesize-1);

		  float fraction = 0.5f;

		  if(this->quantization > 1)
		  {	
			  float id = index;
			  if(index >= 1.0f)
				  id = 0.99f;//to ensure the banding works for this "extreme" case
		  
			  fraction = floor(id*(float)(this->quantization));
			  fraction /= (float)(this->quantization);
			  fraction += 0.5/((float)(this->quantization));
		  
			  if(fraction >= 1.0f)
				  fraction = 0.999999999999f;
		  }	  

		  table.at(i) = table_copy.at(floor(tablesize*fraction));
	  }
  }

  return qfeSuccess;
}