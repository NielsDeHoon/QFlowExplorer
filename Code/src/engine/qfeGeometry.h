#pragma once

#include "qflowexplorer.h"

#include "qfePoint.h"
#include "qfeVector.h"

/**
* \file   qfeGeometry.h
* \author Roy van Pelt
* \class  qfeGeometry
* \brief  Abstract class for various geometry definitions
* \note   Confidential
*
* Abstract class for various geometry definitions
*/
class qfeGeometry
{
public:
  qfeGeometry();
  qfeGeometry(const qfeGeometry &geo);
  ~qfeGeometry();

  qfePoint  origin;    // in mm       - related to world coordinates
  qfeVector axis[3];   // normalized  - related to world coordinates
  qfeFloat  extent[3]; // in mm 

  virtual qfeReturnStatus qfeGetGeometryType(int& type);

  qfeReturnStatus qfeSetGeometryChanged(bool changed);
  bool            qfeIsGeometryChanged();

protected:
  int  geometryType;
  bool geometryChanged;
};
