#include "qfeVisualParameter.h"



//----------------------------------------------------------------------------
qfeVisualParameter::qfeVisualParameter()
{
  this->qfeClearVisualParameterValues();

  this->valueLevel    = qfeVisualParameterBasic;
  this->valueLabel    = (char*)malloc(sizeof(char) * 200);
  this->valueType     = qfeParamTypeUndefined;  

  this->valueStepSize = 1;
  this->valueRange[0] = 0;
  this->valueRange[1] = 100;

};

//----------------------------------------------------------------------------
qfeVisualParameter::qfeVisualParameter(const qfeVisualParameter &param)
{
  this->valueLevel            = param.valueLevel;
  this->valueLabel            = param.valueLabel;
  this->valueType             = param.valueType;
  this->valueStepSize         = param.valueStepSize;
  this->valueRange[0]         = param.valueRange[0];
  this->valueRange[1]         = param.valueRange[1];

  this->valueBool             = param.valueBool;
  this->valueInt              = param.valueInt;
  this->valueDouble           = param.valueDouble;
  this->valueString           = param.valueString;
  this->valueStringList       = param.valueStringList;
  this->valueVector           = param.valueVector;
  this->valueColor            = param.valueColor;
  this->valueRing             = param.valueRing;
  this->valueRangeMinMax      = param.valueRangeMinMax;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterLevel(qfeVisualParameterLevel level)
{
  this->valueLevel = level;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterLevel(qfeVisualParameterLevel &level)
{
  level = this->valueLevel;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterLabel(string label)
{
  this->valueLabel.assign(label);

  return qfeSuccess;  
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterLabel(string *label)
{
  (*label).assign(this->valueLabel);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterValue(bool value)
{
  this->qfeClearVisualParameterValues();

  this->valueBool  = value;
  this->valueType  = qfeParamTypeBool;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterValue(int value)
{
  this->qfeClearVisualParameterValues();

  this->valueInt   = value;
  this->valueType  = qfeParamTypeInt;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterValue(double value)
{
  this->qfeClearVisualParameterValues();

  this->valueDouble = value;
  this->valueType   = qfeParamTypeDouble;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterValue(string value)
{
  this->qfeClearVisualParameterValues();

  this->valueString = value;
  this->valueType = qfeParamTypeString;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterValue(qfeSelectionList value)
{
  this->qfeClearVisualParameterValues();

  this->valueStringList.list   = value.list;
  this->valueStringList.active = value.active;
  this->valueType = qfeParamTypeStringList;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterValue(qfeVector value)
{
  this->qfeClearVisualParameterValues();

  this->valueVector.x = value.x;
  this->valueVector.y = value.y;
  this->valueVector.z = value.z;

  this->valueType = qfeParamTypeVector;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterValue(qfeColorRGBA value)
{
  this->qfeClearVisualParameterValues();

  this->valueColor = value;
  this->valueType  = qfeParamTypeColorRGB;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterValue(qfeRingParam value)
{
  this->qfeClearVisualParameterValues();

  this->valueRing = value;
  this->valueType = qfeParamTypeRing;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterValue(qfeRange value)
{
  this->qfeClearVisualParameterValues();

  this->valueRangeMinMax.min = value.min;
  this->valueRangeMinMax.max = value.max;
  
  this->valueType = qfeParamTypeRange;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterValue(bool &value)
{
  value = this->valueBool;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterValue(int &value)
{
  value = this->valueInt;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterValue(double &value)
{
  value = this->valueDouble;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterValue(string &value)
{
  value = this->valueString;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterValue(qfeSelectionList &value)
{
  value.list   = this->valueStringList.list;
  value.active = this->valueStringList.active;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterValue(qfeVector &value)
{
  value.x = this->valueVector.x;
  value.y = this->valueVector.y;
  value.z = this->valueVector.z;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterValue(qfeColorRGBA &value)
{
  value = this->valueColor;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterValue(qfeRingParam &value)
{
  value = this->valueRing;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterValue(qfeRange &value)
{
  value = this->valueRangeMinMax;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterType(qfeParamType &t)
{
  t = this->valueType; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeClearVisualParameterValues()
{
  this->valueType   = qfeParamTypeUndefined;

  this->valueBool   = false;
  this->valueInt    = 0;
  this->valueDouble = 0.0;
  this->valueStringList.list.clear();
  this->valueStringList.active = 0;
  this->valueRangeMinMax.min = 0;
  this->valueRangeMinMax.max = 1;
  
  qfeVector::qfeVectorClear(this->valueVector);
  
  this->valueColor.r = 0.0;
  this->valueColor.g = 0.0;
  this->valueColor.b = 0.0;

  this->valueRing.visible             = true;
  this->valueRing.seedPoint.x         = 0.0;
  this->valueRing.seedPoint.y         = 0.0;
  this->valueRing.seedPoint.z         = 0.0;
  //this->valueRing.dilation            = 0.0;
  this->valueRing.color.r             = 1.0;
  this->valueRing.color.g             = 0.0;
  this->valueRing.color.b             = 0.0;
  this->valueRing.color.a             = 1.0;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterStepSize(float s)
{
  this->valueStepSize = s;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterStepSize(float &s)
{
  s = this->valueStepSize;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeSetVisualParameterRange(float rl, float ru)
{
  this->valueRange[0] = rl;
  this->valueRange[1] = ru;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualParameter::qfeGetVisualParameterRange(float &rl, float &ru)
{
  rl = this->valueRange[0];
  ru = this->valueRange[1];

  return qfeSuccess;
}