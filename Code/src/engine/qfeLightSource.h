#pragma once

#include "qflowexplorer.h"

#include "qfeVector.h"

/**
* \file   qfeLightSource.h
* \author Roy van Pelt
* \class  qfeLightSource
* \brief  Implements the characteristics of a light source used by the Phong lighting model
* \note   Confidential
*
* A light source defines the characteristics of a light source used by the Phong lighting model.
*/
class qfeLightSource
{
public:
  qfeLightSource();
  qfeLightSource(const qfeLightSource &light);
  ~qfeLightSource();

  qfeReturnStatus qfeSetLightSourceDirection(qfeVector  direction);
  qfeReturnStatus qfeSetLightSourceDirection(qfeFloat x, qfeFloat y, qfeFloat z);
  qfeReturnStatus qfeGetLightSourceDirection(qfeVector &direction);

  qfeReturnStatus qfeSetLightSourceAmbientContribution(qfeFloat ambient);
  qfeReturnStatus qfeGetLightSourceAmbientContribution(qfeFloat &ambient);

  qfeReturnStatus qfeSetLightSourceDiffuseContribution(qfeFloat diffuse);
  qfeReturnStatus qfeGetLightSourceDiffuseContribution(qfeFloat &diffuse);

  qfeReturnStatus qfeSetLightSourceSpecularContribution(qfeFloat specular);
  qfeReturnStatus qfeGetLightSourceSpecularContribution(qfeFloat &specular);

  qfeReturnStatus qfeSetLightSourceSpecularPower(qfeFloat power);
  qfeReturnStatus qfeGetLightSourceSpecularPower(qfeFloat &power);

private :
  qfeVector direction;
  qfeFloat  lightAmbient;
  qfeFloat  lightDiffuse;
  qfeFloat  lightSpecular;
  qfeFloat  lightSpecularPower;
};
