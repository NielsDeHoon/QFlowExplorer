#pragma once

#include "qflowexplorer.h"

#include "qfeColorMap.h"
#include "qfeLightSource.h"
#include "qfeStudy.h"
#include "qfeVector.h"
#include "qfeVisualAction.h"
#include "qfeVisualParameter.h"
#include "qfeVolume.h"

#include <vtkPolyData.h>

#include <string>
#include <sstream>
#include <vector>
#include <map>

enum qfeVisualTypeId
{
  visualNone,
  visualAides,
  visualPostProcess,
  visualDefault,  
  visualMaximum,
  visualFluid,  
  visualRaycast,
  visualRaycastVector,
  visualReformat,
  visualReformatFixed,
  visualReformatOrtho,
  visualReformatOblique,
  visualReformatObliqueProbe,  
  visualReformatUltrasound,
  visualPlanesArrowHeads,
  visualAnatomyIllustrative,
  visualFlowLinesDepr,
  visualFlowLines,
  visualFlowParticles,
  visualFlowSurfaces,
  visualFlowPlanes,
  visualFlowTrails,
  visualFlowProbe2D,
  visualFlowProbe3D,
  visualFlowClustering,
  visualFlowClusterArrows,
  visualFlowPatternMatching,
  visualSimParticles,
  visualSimLinesComparison,
  visualSimBoundaryComparison,
  visualFilterType,
  visualCEVType,
  visualSeedEllipsoidType,
  visualSeedVolumesType,
  visualPlaneOrthoViewType,
  visualFeatureSeedingType,
  visualMetrics,
  visualInkVis,
  visualInkVisSecondary,
  visualVolumeOfInterest,
  visualProbabilityDVR,
  visualDataAssimilation
};

using namespace std;

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisual
* \brief  Describes the visualization of volume according to specific technique.
* \note   Confidential
*
* A visual defines a visualization of an image volume according to specific technique.
* Each technique requires various arguments either in the form of scalar values or object instances.
*/
//----------------------------------------------------------------------------
class qfeVisual
{
public:
  qfeVisual();
  qfeVisual(const qfeVisual &visual);
  ~qfeVisual();

  qfeReturnStatus qfeSetVisualStudy(qfeStudy*   study);
  qfeReturnStatus qfeGetVisualStudy(qfeStudy**  study);

  qfeReturnStatus qfeSetVisualActiveVolume(float index);
  qfeReturnStatus qfeGetVisualActiveVolume(int   &index);
  qfeReturnStatus qfeGetVisualActiveVolume(float &index);

  qfeReturnStatus qfeAddVisualAction(qfeVisualAction action);
  qfeReturnStatus qfeRemoveVisualAction(string label);

  qfeReturnStatus qfeGetVisualAction(string label, qfeVisualAction **action);
  qfeReturnStatus qfeGetVisualAction(int index, qfeVisualAction **action);  
  qfeReturnStatus qfeGetVisualActionCount(int &count);

  qfeReturnStatus qfeAddVisualParameter(qfeVisualParameter param);
  qfeReturnStatus qfeRemoveVisualParameter(string label);

  qfeReturnStatus qfeGetVisualParameter(string label, qfeVisualParameter **param);
  qfeReturnStatus qfeGetVisualParameter(int index, qfeVisualParameter **param);  
  qfeReturnStatus qfeGetVisualParameterCount(int &count);

  qfeReturnStatus qfeSetVisualColorMap(qfeColorMap   colorMap);
  qfeReturnStatus qfeGetVisualColorMap(qfeColorMap **colorMap);  

  qfeReturnStatus qfeGetVisualType(int &technique);

  qfeReturnStatus qfeUpdateVisual();

protected:  
  int                          technique;
  qfeStudy                    *study;
  vector<qfeVisualAction>      actions;
  vector<qfeVisualParameter>   params;
  qfeColorMap                  colormap;  
  qfeLightSource               light;

  float                        activeVolume;
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualDefault
* \brief  Describes a visual to render a default background.
* \note   Confidential
*
* A visual to render a default background
*/
//----------------------------------------------------------------------------
class qfeVisualDefault : public qfeVisual
{
public:
  qfeVisualDefault();
  ~qfeVisualDefault();

};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualMaximum
* \brief  Describes a visual for a maximum intensity projection.
* \note   Confidential
*
* A visual for a maximum intensity projection.
*/
//----------------------------------------------------------------------------
class qfeVisualMaximum : public qfeVisual
{
public:
  qfeVisualMaximum();
  qfeVisualMaximum(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualMaximum();  
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualFluid
* \brief  Describes a visual for a fluid simulation
*
* A visual for fluid simulation.
*/
//----------------------------------------------------------------------------
class qfeVisualFluid : public qfeVisual
{
public:
  static const int actionFluidSimPreprocess			= 9;
  static const int actionFluidSimSetSource			= 10;
  static const int actionFluidSimSetSink			= 11;
  static const int actionFluidSimRemoveInteractor   = 12;
  static const int actionFluidSimInspect		    = 17;  
  static const int actionFluidSimComputeFlux		= 26;
  
  qfeVisualFluid();
  qfeVisualFluid(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualFluid();  
};

/**
* \file   qfeVisual.h
* \author Niels de Hoon
* \class  qfeVisualDataAssimilation
* \brief  Describes a visual for data assimilation
*
* A visual for data assimilation.
*/
//----------------------------------------------------------------------------
class qfeVisualDataAssimilation : public qfeVisual
{
public:
  static const int actionApplyDataAssimilation			= 27;

  qfeVisualDataAssimilation();
  qfeVisualDataAssimilation(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualDataAssimilation();  
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualSimParticleTrace
* \brief  Describes a visual to render particle traces.
* \note   Confidential
*
* A visual to render particle traces of a fluid simulation
*/
//----------------------------------------------------------------------------
class qfeVisualSimParticleTrace : public qfeVisual
{
public:
  qfeVisualSimParticleTrace();
  qfeVisualSimParticleTrace(const qfeVisualSimParticleTrace &visual);
  ~qfeVisualSimParticleTrace();

private:
  
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualSimLinesComparison
* \brief  Describes a visual to render comparative line traces.
* \note   Confidential
*
* A visual to render comparative line traces
*/
//----------------------------------------------------------------------------
class qfeVisualSimLinesComparison : public qfeVisual
{
public:
  static const int actionInjectSeeds        = 3;
  static const int actionClearSeeds         = 4;

  qfeVisualSimLinesComparison();
  qfeVisualSimLinesComparison(const qfeVisualSimLinesComparison &visual);
  ~qfeVisualSimLinesComparison();

private:
  
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualSimBoundaryComparison
* \brief  Describes a visual to render comparative line traces.
* \note   Confidential
*
* A visual to render comparative arrows at the boundaries
*/
//----------------------------------------------------------------------------
class qfeVisualSimBoundaryComparison : public qfeVisual
{
public:
  static const int actionInjectSeeds        = 3;
  static const int actionClearSeeds         = 4;

  qfeVisualSimBoundaryComparison();
  qfeVisualSimBoundaryComparison(const qfeVisualSimBoundaryComparison &visual);
  ~qfeVisualSimBoundaryComparison();

private:
  
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualRaycast
* \brief  Describes a visual for a direct volume rendering by raycasting.
* \note   Confidential
*
* Abstract visual for a raycaster
*/
//----------------------------------------------------------------------------
class qfeVisualRaycast : public qfeVisual
{
public:
  qfeVisualRaycast();
  qfeVisualRaycast(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualRaycast();
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualRaycastVector
* \brief  Describes a visual for a direct volume rendering by raycasting a vector-field.
* \note   Confidential
*
* Abstract visual for a vector-field raycaster
*/
//----------------------------------------------------------------------------
class qfeVisualRaycastVector : public qfeVisual
{
public:
  qfeVisualRaycastVector();
  qfeVisualRaycastVector(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualRaycastVector();
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualReformat
* \brief  Describes a visual for a multi planar reformat.
* \note   Confidential
*
* Abstract visual for a multi planar reformat.
*/
//----------------------------------------------------------------------------
class qfeVisualReformat : public qfeVisual
{
public:
  qfeVisualReformat();
  qfeVisualReformat(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualReformat();
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualReformatOrtho
* \brief  Describes a visual for a multi planar reformat.
* \note   Confidential
*
* A visual for orthogonal planes.
*/
//----------------------------------------------------------------------------
class qfeVisualReformatOrtho : public qfeVisualReformat
{
public:
  static const int actionResetMPR = 5;

  qfeVisualReformatOrtho();
  qfeVisualReformatOrtho(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualReformatOrtho();
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualReformatOblique
* \brief  Describes a visual for a multi planar reformat.
* \note   Confidential
*
* Visual for an oblique multi planar reformat.
*/
//----------------------------------------------------------------------------
class qfeVisualReformatOblique : public qfeVisualReformat
{
public:
  static const int actionResetMPR = 5;

  qfeVisualReformatOblique();
  qfeVisualReformatOblique(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualReformatOblique();
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualReformatObliqueProbe
* \brief  Describes a visual for a multi planar reformat.
* \note   Confidential
*
* Visual for an oblique multi planar reformat suitable for the probe
* - This should inherit from qfeVisualReformatOblique, 
*   but for now parameters would interfere
*/
//----------------------------------------------------------------------------
class qfeVisualReformatObliqueProbe : public qfeVisualReformat
{
public:

  qfeVisualReformatObliqueProbe();
  qfeVisualReformatObliqueProbe(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualReformatObliqueProbe();
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualReformatUltrasound
* \brief  Mimics an ultrasound plane.
* \note   Confidential
*
*/
//----------------------------------------------------------------------------
class qfeVisualReformatUltrasound : public qfeVisualReformat
{
public:
  qfeVisualReformatUltrasound();
  qfeVisualReformatUltrasound(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualReformatUltrasound();
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualReformatFixed
* \brief  Describes a visual for a multi planar reformat.
* \note   Confidential
*
* A visual for qflow planes.
*/
//----------------------------------------------------------------------------
class qfeVisualReformatFixed : public qfeVisualReformat
{
public:
  qfeVisualReformatFixed();
  qfeVisualReformatFixed(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualReformatFixed();
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualPlanesArrowHeads
* \brief  Describes a visual for vector field arrow heads on a multi planar reformat.
* \note   Confidential
*
* A visual for arrow heads on a multi planar reformat.
*/
//----------------------------------------------------------------------------
class qfeVisualPlanesArrowHeads : public qfeVisualReformat
{
public:
  qfeVisualPlanesArrowHeads();
  qfeVisualPlanesArrowHeads(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualPlanesArrowHeads();
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualAnatomyIllustrative
* \brief  Describes a visual for a silhouette rendering of a mesh.
* \note   Confidential
*
* A visual for a silhouette rendering of a mesh.
*/
//----------------------------------------------------------------------------
class qfeVisualAnatomyIllustrative : public qfeVisual
{
public:
  qfeVisualAnatomyIllustrative();
  qfeVisualAnatomyIllustrative(const qfeVisualAnatomyIllustrative &visual);
  ~qfeVisualAnatomyIllustrative();

  qfeReturnStatus qfeSetVisualStudy(qfeStudy*   study);

private:
  map< qfeModel *, qfeVisualParameter *> paramList;
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualFlowLines
* \brief  Describes a visual to render stream- and pathlines
* \note   Confidential
*
* A visual to render stream- and pathlines
* Requires a 2D probe.
*/
//----------------------------------------------------------------------------
class qfeVisualFlowLines : public qfeVisual
{
public:
  qfeVisualFlowLines();
  qfeVisualFlowLines(const qfeVisualFlowLines &visual);
  ~qfeVisualFlowLines();

  qfeReturnStatus qfeAddVisualRingParam(qfeVector seedPoint);

private:
  int                  ringCount;
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualFlowParticleTrace
* \brief  Describes a visual to render particle traces.
* \note   Confidential
*
* A visual to render particle traces
*/
//----------------------------------------------------------------------------
class qfeVisualFlowParticleTrace : public qfeVisual
{
public:
  static const int actionInjectSeeds     = 3;
  static const int actionInjectSeedsFile = 6;
  static const int actionClearSeeds      = 4;

  qfeVisualFlowParticleTrace();
  qfeVisualFlowParticleTrace(const qfeVisualFlowParticleTrace &visual);
  ~qfeVisualFlowParticleTrace();

private:
  
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualFlowLineTrace
* \brief  Describes a visual to render stream- and pathlines
* \note   Confidential
*
* A visual to render stream- and pathlines
* Improves on qfeVisualFlowLines, which will be deprecated
*/
//----------------------------------------------------------------------------
class qfeVisualFlowLineTrace : public qfeVisual
{
public:
  static const int actionInjectSeeds     = 3;
  static const int actionInjectSeedsFile = 6;
  static const int actionClearSeeds      = 4;

  qfeVisualFlowLineTrace();
  qfeVisualFlowLineTrace(const qfeVisualFlowLineTrace &visual);
  ~qfeVisualFlowLineTrace();

private:

};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualFlowSurfaceTrace
* \brief  Describes a visual to render integration surfaces.
* \note   Confidential
*
* A visual to render path surfaces
*/
//----------------------------------------------------------------------------
class qfeVisualFlowSurfaceTrace : public qfeVisual
{
public:
  static const int actionInjectSeeds     = 3;
  static const int actionInjectSeedsFile = 6;
  static const int actionClearSeeds      = 4;

  qfeVisualFlowSurfaceTrace();
  qfeVisualFlowSurfaceTrace(const qfeVisualFlowSurfaceTrace &visual);
  ~qfeVisualFlowSurfaceTrace();

private:
  
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualFlowPlanes
* \brief  Describes a visual to depict flow planes
* \note   Confidential
*
* A visual to depict flow planes, either integrated or exploded.
* Requires a 2D probe.
*/
//----------------------------------------------------------------------------
class qfeVisualFlowPlanes : public qfeVisual
{
public:
  qfeVisualFlowPlanes();
  qfeVisualFlowPlanes(const qfeVisualFlowPlanes &visual);
  ~qfeVisualFlowPlanes();

  qfeReturnStatus qfeAddVisualRingParam(qfeVector seedPoint);

private:
  int                  ringCount;
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualFlowArrowTrails
* \brief  Describes a flow-rate arrow trail
* \note   Confidential
*
* A flow-rate arrow trail. Arrow magnitude depicts the flow-rate, and the arrow
* origin and position is determined by peak velocity
* Requires a 2D probe.
*/
//----------------------------------------------------------------------------
class qfeVisualFlowArrowTrails : public qfeVisual
{
public:
  qfeVisualFlowArrowTrails();
  qfeVisualFlowArrowTrails(const qfeVisualFlowArrowTrails &visual);
  ~qfeVisualFlowArrowTrails();

  qfeReturnStatus qfeAddVisualRingParam(qfeVector seedPoint);

private:
  int                  ringCount;
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualFlowProbe2D
* \brief  Describes the visualization of a 2D flow probe
* \note   Confidential
*
* A 2D flow probe
*/
//----------------------------------------------------------------------------
class qfeVisualFlowProbe2D : public qfeVisual
{
public:
  static const int actionNewProbeClick   = 0;
  static const int actionDeleteProbe     = 1;
    
  qfeVisualFlowProbe2D();
  qfeVisualFlowProbe2D(const qfeVisualFlowProbe2D &visual);
  ~qfeVisualFlowProbe2D();  

private:

};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualFlowProbe3D
* \brief  Describes the visualization of a 3D virtual flow probe
* \note   Confidential
*
* A virtual flow probe in 3D
*/
//----------------------------------------------------------------------------
class qfeVisualFlowProbe3D : public qfeVisual
{
public:
  static const int actionNewProbeClick   = 0;
  static const int actionDeleteProbe     = 1;
  static const int actionProbeFit        = 2;
  static const int actionProbeUndo       = 7;
    
  qfeVisualFlowProbe3D();
  qfeVisualFlowProbe3D(const qfeVisualFlowProbe3D &visual);
  ~qfeVisualFlowProbe3D();  

private:

};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualClustering
* \brief  Describes a visual for a hierachical clustering rendering.
* \note   Confidential
*
* A visual for a hierachical clustering rendering.
*/
//----------------------------------------------------------------------------
class qfeVisualClustering : public qfeVisualReformat
{
public:
  static const int actionGenerateClusters = 6;

  qfeVisualClustering();
  qfeVisualClustering(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualClustering();
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualClusterArrowTrace
* \brief  Describes a visual to render arrows in clustering
* \note   Confidential
*
* A visual to render arrows in a hierarchical clustered data set
*/
//----------------------------------------------------------------------------
class qfeVisualClusterArrowTrace : public qfeVisual
{
public:
  static const int actionInjectSeeds = 3;

  qfeVisualClusterArrowTrace();
  qfeVisualClusterArrowTrace(const qfeVisualClusterArrowTrace &visual);
  ~qfeVisualClusterArrowTrace();

private:

};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualPatternMatching
* \brief  Describes a visual for a pattern matching result rendering.
* \note   Confidential
*
* A visual for a pattern matching result rendering.
*/
//----------------------------------------------------------------------------
class qfeVisualPatternMatching : public qfeVisual
{
public:
  static const int actionInjectSeedsFile = 6;
  static const int actionClearSeeds      = 4;

  qfeVisualPatternMatching();
  qfeVisualPatternMatching(const qfeVisualPatternMatching &visual);
  ~qfeVisualPatternMatching();
};

/**
* \file   qfeVisual.h
* \author Roy van Pelt
* \class  qfeVisualAides
* \brief  Describes a number of visual aides
* \note   Confidential
*
* A set of visual aides, such as a bounding box and 3D axes.
*/
//----------------------------------------------------------------------------
class qfeVisualAides : public qfeVisual
{
public:
  qfeVisualAides();
  qfeVisualAides(const qfeVisualAides &visual);
  ~qfeVisualAides();

private:

};


/**
* \file   qfeVisual.h
* \author Niels de Hoon
* \class  qfeVisualPostProcess
* \brief  Describes a number of visual filters
*
* A set of visual filters, such as gaussian blur.
*/
//----------------------------------------------------------------------------
class qfeVisualPostProcess : public qfeVisual
{
public:
  qfeVisualPostProcess();
  ~qfeVisualPostProcess();  
};

/**
* \file   qfeVisual.h
* \author Arjan Broos
* \class  qfeVisualFilter
* \brief  Describes visual to show filter results
* \note   Confidential
*
* An MIP visual, with extra parameters to show filter results
*/
//----------------------------------------------------------------------------
class qfeVisualFilter : public qfeVisual
{
public:
  qfeVisualFilter();
  ~qfeVisualFilter();

  static const int actionSaveFilteredData = 13;

private:

};

/**
* \file   qfeVisual.h
* \author Arjan Broos
* \class  qfeVisualCEV
* \brief  Describes a visual for cardiac exploration
* \note   Confidential
*
* A visual for cardiac exploration
*/
//----------------------------------------------------------------------------
class qfeVisualCEV : public qfeVisual
{
public:
  qfeVisualCEV();
  qfeVisualCEV(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualCEV();
};

/**
* \file   qfeVisual.h
* \author Arjan Broos
* \class  qfeVisualSeedEllipsoid
* \brief  Describes a visual for a clipped ellipsoid for seed points
* \note   Confidential
*/
//----------------------------------------------------------------------------
class qfeVisualSeedEllipsoid : public qfeVisual
{
public:
  static const int actionPlaceLVGuidingPoints = 14;
  static const int actionPlaceRVGuidingPoints = 15;

  qfeVisualSeedEllipsoid();
  qfeVisualSeedEllipsoid(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualSeedEllipsoid();
};

/**
* \file   qfeVisual.h
* \author Niels de Hoon
* \class  qfeVisualSeedVolumeCollection
* \brief  Describes a visual for seeding volumes
* \note   Confidential
*/
//----------------------------------------------------------------------------
class qfeVisualSeedVolumes : public qfeVisual
{
public:
  static const int actionAddSeedVolume			= 21;
  static const int actionRemoveSeedVolume		= 22;
  static const int actionSaveSeedVolumes		= 23;
  static const int actionLoadSeedVolumes		= 24;

  qfeVisualSeedVolumes();
  ~qfeVisualSeedVolumes();
};


/**
* \file   qfeVisual.h
* \author Arjan Broos
* \class  qfeVisualPlaneOrthoView
* \brief  Describes a visual with a plane that is always orthogonal to the
*         looking direction of the camera
* \note   Confidential
*/
//----------------------------------------------------------------------------
class qfeVisualPlaneOrthoView : public qfeVisual
{
public:
  qfeVisualPlaneOrthoView();
  ~qfeVisualPlaneOrthoView();
};

/**
* \file   qfeVisual.h
* \author Arjan Broos
* \class  qfeVisualFeatureSeeding
* \brief  Describes a visual that places seed points based on calculated
*         features of the flow and transfer functions
* \note   Confidential
*/
//----------------------------------------------------------------------------
class qfeVisualFeatureSeeding : public qfeVisual
{
public:
  static const int actionPlaceFeatureSeedPoints = 16;

  qfeVisualFeatureSeeding();
  ~qfeVisualFeatureSeeding();
};

/**
* \file   qfeVisual.h
* \author Niels de Hoon
* \class  qfeVisualMetrics
* \brief  Describes a visual for generic fluid metrics
*
* A visual for generic fluid metrics.
*/
//----------------------------------------------------------------------------

class qfeVisualMetrics : public qfeVisual
{
public:
  qfeVisualMetrics();
  qfeVisualMetrics(const qfeVisualMetrics &visual);
  ~qfeVisualMetrics();  
};

/**
* \file   qfeVisual.h
* \author Arjan Broos
* \class  qfeVisualVolumeOfInterest
* \brief  Describes a visual that defines an ellipsoid, which clips out
*         any rendering outside of the ellipsoid, for any cooperating visual.
* \note   Confidential
*/
//----------------------------------------------------------------------------
class qfeVisualVolumeOfInterest : public qfeVisual
{
public:
  static const int actionPlaceVolumeOfInterest = 18;

  qfeVisualVolumeOfInterest();
  ~qfeVisualVolumeOfInterest();
};

/**
* \file   qfeVisual.h
* \author Arjan Broos
* \class  qfeVisualProbabilityDVR
* \brief  Describes a visual to render a scalar field of probabilities,
*         defined by two transfer functions, using direct volume rendering.
* \note   Confidential
*/
//----------------------------------------------------------------------------
class qfeVisualProbabilityDVR : public qfeVisual
{
public:
  qfeVisualProbabilityDVR();
  ~qfeVisualProbabilityDVR();
};

/**
* \file   qfeVisual.h
* \author Niels de Hoon
* \class  qfeVisualInkVis
* \brief  Describes a visual for a high particle count visualization
* \note   Confidential
*
* A visual for a high particle count visualization
*/
//----------------------------------------------------------------------------
class qfeVisualInkVis : public qfeVisual
{
public:
  static const int actionLoadInkSeed			= 28;
  static const int actionSaveInkSeed			= 29;
  static const int actionCenterInkCamera		= 30;
  static const int actionAnimateInk				= 19;
  static const int actionRemoveInk				= 20;
  static const int actionShowTimeLine			= 26;
  static const int actionPlaceFeatureSeedPointsInk = 25;

  qfeVisualInkVis();
  qfeVisualInkVis(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualInkVis();  
};

/**
* \file   qfeVisual.h
* \author Niels de Hoon
* \class  qfeVisualInkVisMinimal
* \brief  Describes a visual for a high particle count visualization
* \note   Confidential
*
* A visual for a high particle count visualization
*/
//----------------------------------------------------------------------------
class qfeVisualInkVisSecondary : public qfeVisual
{
public:
  qfeVisualInkVisSecondary();
  qfeVisualInkVisSecondary(qfeStudy* study, qfeId filter, qfeId cuts, qfeFloat threshold, qfeId colourMap, qfeId opacityMap);
  ~qfeVisualInkVisSecondary();  
};