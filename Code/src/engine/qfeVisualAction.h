#pragma once

#include <iostream>
#include <typeinfo>

#include "qflowexplorer.h"

#include "qfeVector.h"

#include <string>
#include <vector>

using namespace std;

/**
* \file   qfeVisualAction.h
* \author Roy van Pelt
* \class  qfeVisualAction
* \brief  Implements an action class
* \note   Confidential
*
* Template class for various parameters.
* The type of the parameter can be queried.
*
*/
class qfeVisualAction
{
  public:
    qfeVisualAction();
    qfeVisualAction(const qfeVisualAction &param);
    ~qfeVisualAction(){};

    qfeReturnStatus qfeSetVisualActionLabel(string label);
    qfeReturnStatus qfeGetVisualActionLabel(string &label);

    qfeReturnStatus qfeSetVisualActionType(int t);
    qfeReturnStatus qfeGetVisualActionType(int &t);

    qfeReturnStatus qfeSetVisualAction(string label, int type);

  protected:
    string      valueLabel;
    int         valueType;
};
