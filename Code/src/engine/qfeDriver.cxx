#include "qfeDriver.h"

  double PCFreq = 0.0;
__int64 CounterStart = 0;

//----------------------------------------------------------------------------
qfeDriver::qfeDriver(qfeScene *scene)
{
  vtkRenderWindow *renWin;

  qfeDisplay      *display;
  qfeFrame        *geo;
  int              superSampling;
  qfeDisplayLayout layout;
  bool             layoutFullscreen;
  bool             layoutClickable;
  qfeVector        x, y, z, e;
  qfePoint         o;

  // Initialize Glew
  GLenum err = glewInit();
  if (GLEW_OK != err)
  {
    cout << "qfeDriver::qfeDriver - " << glewGetErrorString(err) << endl;
  }

  if(scene == NULL)
  {  
    cout << "qfeDriver::qfeDriver - Scene undefined" << endl;
    return;
  }

  // Get the scene to render
  this->scene = scene;
  this->scene->qfeGetSceneDisplay(&display);
  this->scene->qfeGetSceneGeometry(&geo);
  this->scene->qfeGetSceneVisuals(this->visuals);

  // Get the display and connect to the render widget  
  display->qfeGetDisplayWindow(&renWin);
  display->qfeGetDisplaySize(this->windowsize[0], this->windowsize[1]);   
  display->qfeGetDisplaySuperSampling(superSampling);
  display->qfeGetDisplayLayout(layout, layoutFullscreen, layoutClickable);

  // Initialize textures per viewport
  for(int i=0; i<4; i++)
  {    
    this->frameBufferProps[i].texColor      = 0;
    this->frameBufferProps[i].texSelect     = 0;
    this->frameBufferProps[i].texDepth      = 0;
    this->frameBufferProps[i].supersampling = 1;
  }

  // Initialize geometry per viewport 
  // Note that we initialize the prescribed geometry for all viewports
  // Drivers may change the geometry internally  
  geo->qfeGetFrameOrigin(o.x, o.y, o.z);
  geo->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);
  geo->qfeGetFrameExtent(e.x, e.y, e.z);

  this->frameBufferGeo[0]                 = geo;
  this->frameBufferProps[0].supersampling = superSampling;
  for(int i=1; i<4; i++)
  {
    this->frameBufferGeo[i] = new qfeFrame();
    this->frameBufferGeo[i]->qfeSetFrameOrigin(o.x, o.y, o.z);
    this->frameBufferGeo[i]->qfeSetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);
    this->frameBufferGeo[i]->qfeSetFrameExtent(e.x, e.y);
    this->frameBufferGeo[i]->qfeSetGeometryChanged(true);
  }  

  // Instantiate the VTK render engine
  this->engine = qfeVtkEngine::qfeGetInstance();
  this->engine->Attach(renWin);  
  this->engine->SetLayout((vtkEngineLayout)layout, layoutFullscreen, layoutClickable);
  this->engine->SetObserverResize((void*)this, this->qfeDriverResize);
  this->engine->SetObserverRender((void*)this, this->qfeDriverRender);
  this->engine->SetObserverRender1((void*)this, this->qfeDriverRender1);
  this->engine->SetObserverRender2((void*)this, this->qfeDriverRender2);
  this->engine->SetObserverRender3((void*)this, this->qfeDriverRender3);  
  this->engine->Start();

  this->updateWindowSizeFlag = false;

  this->qfeDriverResize((void*)this);

  // Initialize the FBO
  this->frameBuffer = 0;
  this->qfeGenerateFBO(this->frameBuffer);
 
  // Intialize attachable textures
  this->engine->GetViewport3D(&frameBufferViewport[0]);  
  this->engine->GetViewport2D(0, &frameBufferViewport[1]);  
  this->engine->GetViewport2D(1, &frameBufferViewport[2]);  
  this->engine->GetViewport2D(2, &frameBufferViewport[3]);  
  this->qfeGenerateTextures(this->frameBufferProps[0].supersampling*frameBufferViewport[0]->width, this->frameBufferProps[0].supersampling*frameBufferViewport[0]->height, this->frameBufferProps[0].texColor, this->frameBufferProps[0].texSelect, this->frameBufferProps[0].texDepth);
  this->qfeGenerateTextures(this->frameBufferProps[1].supersampling*frameBufferViewport[1]->width, this->frameBufferProps[1].supersampling*frameBufferViewport[1]->height, this->frameBufferProps[1].texColor, this->frameBufferProps[1].texSelect, this->frameBufferProps[1].texDepth);
  this->qfeGenerateTextures(this->frameBufferProps[2].supersampling*frameBufferViewport[2]->width, this->frameBufferProps[2].supersampling*frameBufferViewport[2]->height, this->frameBufferProps[2].texColor, this->frameBufferProps[2].texSelect, this->frameBufferProps[2].texDepth);
  this->qfeGenerateTextures(this->frameBufferProps[3].supersampling*frameBufferViewport[3]->width, this->frameBufferProps[3].supersampling*frameBufferViewport[3]->height, this->frameBufferProps[3].texColor, this->frameBufferProps[3].texSelect, this->frameBufferProps[3].texDepth);
  
  // Initialize the shader program to determine select color
  this->shaderProgramSelectColor = new qfeGLShaderProgram();
  this->shaderProgramSelectColor->qfeAddShaderFromFile("/shaders/general/uniform-color-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramSelectColor->qfeAddShaderFromFile("/shaders/general/uniform-color-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramSelectColor->qfeLink();

  // Initialize the VTK camera
  this->qfeUpdateCamera(this->engine->GetRenderer(),  this->frameBufferGeo[0]);   
  this->qfeUpdateCamera(this->engine->GetRenderer(1), this->frameBufferGeo[1]);   
  this->qfeUpdateCamera(this->engine->GetRenderer(2), this->frameBufferGeo[2]);   
  this->qfeUpdateCamera(this->engine->GetRenderer(3), this->frameBufferGeo[3]); 

  //Initialize post processing class
  int t;
  bool postProcessingInitialized = false;
  for(int i=0; i<(int)this->visuals.size(); i++)
  {    
		this->visuals[i]->qfeGetVisualType(t);
		if(t == visualPostProcess)
		{
			this->visPostProcess = static_cast<qfeVisualPostProcess *>(this->visuals[i]);
			postProcessingInitialized = true;
			
			this->postProcessor		= new qfePostProcessing();
		}
  }
};

//----------------------------------------------------------------------------
qfeDriver::~qfeDriver()
{  
  vtkRenderWindow *renWin;
  qfeDisplay      *display;

  //Delete post processor if it was initialized
  int t;
  bool postProcessingInitialized = false;
  for(int i=0; i<(int)this->visuals.size(); i++)
  {    
		this->visuals[i]->qfeGetVisualType(t);
		if(t == visualPostProcess)
		{
			this->visPostProcess = static_cast<qfeVisualPostProcess *>(this->visuals[i]);
			postProcessingInitialized = true;
			
			if(this->postProcessor != NULL)
				delete this->postProcessor;
		}
  }


  this->scene->qfeGetSceneDisplay(&display);
  display->qfeGetDisplayWindow(&renWin);
  
  // Destroy the Frame Buffer Object  
  this->qfeDetachFBO(this->frameBuffer);
  this->qfeDeleteFBO(this->frameBuffer);

  // Stop the VTK engine
  this->engine->Stop();
  this->engine->Detach(renWin);

  // Delete geometries
  for(int i=1; i<4; i++)
  {    
    delete this->frameBufferGeo[i];    
  }

  // Delete shader
  delete this->shaderProgramSelectColor;
};

//----------------------------------------------------------------------------
void qfeDriver::qfeRenderStart()
{
  cout << "qfeDriver::qfeRenderStart - Pure virtual function" << endl;
}

//----------------------------------------------------------------------------
void qfeDriver::qfeRenderSelect()
{ 
  cout << "qfeDriver::qfeRenderSelect - Pure virtual function" << endl;
};

//----------------------------------------------------------------------------
void qfeDriver::qfeRenderWait()
{
  cout << "qfeDriver::qfeRenderWait - Pure virtual function" << endl;
};

//----------------------------------------------------------------------------
void qfeDriver::qfeRenderWait1()
{  
  // Implement in descendant driver
};

//----------------------------------------------------------------------------
void qfeDriver::qfeRenderWait2()
{  
  // Implement in descendant driver
};

//----------------------------------------------------------------------------
void qfeDriver::qfeRenderWait3()
{  
  // Implement in descendant driver
};

//----------------------------------------------------------------------------
void qfeDriver::qfeRenderStop()
{
  cout << "qfeDriver::qfeRenderStop - Pure virtual function" << endl;
};

//----------------------------------------------------------------------------
void qfeDriver::qfeRenderRotate(double angle, bool autoupdate)
{
  this->engine->Rotate(angle);
  if(autoupdate) 
    this->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriver::qfeRenderRotate360(double angleStep, bool autoupdate, bool screenshot)
{
  double steps = (int)(360.0/angleStep);
  double alpha = 360.0/(double)steps;
  
  for(int i=0; i<steps; i++)
  {
    this->engine->Rotate(alpha);
    if(autoupdate) 
      this->engine->Refresh();
	
	if(screenshot)
		this->engine->StoreScreenshot(1920, 1080);
  }
}

//----------------------------------------------------------------------------
void qfeDriver::qfeResetFBO()
{
  this->qfeResetFBO(0);
}

//----------------------------------------------------------------------------
void qfeDriver::qfeResetFBO(int index)
{
  if(index >=0 && index<4)
    this->qfeAttachFBO(this->frameBuffer, this->frameBufferProps[index].texColor, this->frameBufferProps[index].texSelect, this->frameBufferProps[index].texDepth);
  else
    this->qfeAttachFBO(this->frameBuffer, this->frameBufferProps[0].texColor, this->frameBufferProps[0].texSelect, this->frameBufferProps[0].texDepth);
}

//----------------------------------------------------------------------------
void qfeDriver::qfeSetSelectColor(qfeColorRGB color)
{
  this->shaderProgramSelectColor->qfeDisable();
  this->shaderProgramSelectColor->qfeSetUniform3f("color", color.r, color.g, color.b);
  this->shaderProgramSelectColor->qfeEnable();
}

void qfeDriver::qfeGetCameraSettings(qfePoint &position, qfePoint &focalPoint, qfeVector &viewUp)
{
	vtkCamera *camera;
	// Retrieve the camera from the renderer.
	camera = this->engine->GetRenderer()->GetActiveCamera();

	double x, y, z;

	camera->GetPosition(x, y, z);
	position.x = x; position.y = y; position.z = z;

	camera->SetFocalPoint(x, y, z); 
	focalPoint.x = x; focalPoint.y = y; focalPoint.z = z;

	camera->GetViewUp(x, y, z);
	viewUp.x = x; viewUp.y = y; viewUp.z = z;
}

void qfeDriver::qfeSetCameraSettings(qfePoint position, qfePoint focalPoint, qfeVector viewUp)
{
  vtkCamera *camera;
  // Retrieve the camera from the renderer.
  camera = this->engine->GetRenderer()->GetActiveCamera();

  // Set the camera position and orientation from the geometry:
  // The focal point is located in the origin.  
  camera->SetViewUp( (viewUp.x), (viewUp.y), (viewUp.z));
  camera->SetPosition(position.x ,
                      position.y,
                      position.z);
  camera->SetFocalPoint(focalPoint.x, focalPoint.y, focalPoint.z); 
  camera->ComputeViewPlaneNormal();
}

//----------------------------------------------------------------------------
bool qfeDriver::qfeRenderCheck()
{
  // Check the required extensions
  if(
    GL_ARB_vertex_shader            && 
    GL_ARB_fragment_shader          &&   
    GL_EXT_framebuffer_object
    )
  {
    cout << "qfeDriver::RenderCheck: Extensions OK" << endl;
    return true;
  }
  else
  {
    cout << "qfeDriver::RenderCheck: Extensions Failed" << endl;
    return false;
  }
};

//----------------------------------------------------------------------------
void qfeDriver::qfeRenderBackground()
{  
  qfeDisplay   *display;
  qfeColorRGB   col1, col2;
  int           type;
  
  if(this->scene == NULL) return;

  this->scene->qfeGetSceneDisplay(&display);

  display->qfeGetDisplayBackgroundColorRGB(col1.r, col1.g, col1.b);
  display->qfeGetDisplayBackgroundColorRGBSecondary(col2.r, col2.g, col2.b);

  display->qfeGetDisplayBackgroundType(type);

  if(type == qfeDisplayBackgroundUniform) col2 = col1;

  glDisable(GL_DEPTH_TEST);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glBegin( GL_QUADS );  
  glColor3f( col2.r, col2.g, col2.b ); 
  glVertex2f( -1.0f, -1.0f );
  glVertex2f(  1.0f, -1.0f );

  glColor3f( col1.r, col1.g, col1.b );
  glVertex2f(  1.0f,  1.0f );
  glVertex2f( -1.0f,  1.0f );
  glEnd();
  
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  
  glMatrixMode(GL_MODELVIEW); 
  glPopMatrix();

  glEnable(GL_DEPTH_TEST);
};

//----------------------------------------------------------------------------
void qfeDriver::qfeDriverRender(void* caller)
{
  qfeDriver* self = reinterpret_cast<qfeDriver*>( caller );

  if(self->frameBufferViewport[0]->width <=0 || self->frameBufferViewport[0]->height <=0) return;
    
  // Set global state
  glDisable(GL_LIGHTING);
  glDisable(GL_FOG);
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_DITHER);
  glDisable(GL_BLEND);  
  glDisable(GL_NORMALIZE);
  glDisable(GL_COLOR_MATERIAL);

  // The main render loop
  self->qfeUpdate();
  self->qfeAttachFBO(self->frameBuffer, self->frameBufferProps[0].texColor, self->frameBufferProps[0].texSelect, self->frameBufferProps[0].texDepth);
  self->qfeEnableFBO(self->frameBuffer);  
  self->qfeSetProjection(self->frameBufferGeo[0], self->frameBufferViewport[0], self->frameBufferProps[0].supersampling);

  // Get state matrices
  GLfloat modelview[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f::qfeSetMatrixElements(self->currentModelView, modelview);

  GLfloat projection[16];
  glGetFloatv(GL_PROJECTION_MATRIX, projection);
  qfeMatrix4f::qfeSetMatrixElements(self->currentProjection, projection);

  // Set the values for clearing the buffers 
  glClearColor(1,1,1,0);
  glClearDepth(1);
  glClearStencil(0);

  // Render the scene with background
  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
  
  self->qfeRenderBackground();  
  self->qfeRenderWait();
  
  int       t; 
  bool postProcessingInitialized = false;
  for(int i=0; i<(int)self->visuals.size(); i++)
  {    
		self->visuals[i]->qfeGetVisualType(t);
		if(t == visualPostProcess)
		{
			self->visPostProcess = static_cast<qfeVisualPostProcess *>(self->visuals[i]);
			postProcessingInitialized = true;
		}
  }

  if(postProcessingInitialized && self->visPostProcess != NULL)
  {
	  self->postProcessor->qfeApplyPostProcessing(self->frameBuffer, &self->frameBufferProps[0].texColor, self->engine, self->visPostProcess);
  }
      
  // Render the selection buffer
  glDrawBuffer(GL_COLOR_ATTACHMENT1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); 

  self->shaderProgramSelectColor->qfeEnable();  
  self->qfeRenderSelect(); 
  self->shaderProgramSelectColor->qfeDisable();
  
  self->qfeResetProjection();
  
  self->qfeDisableFBO();

  self->qfeRenderTextureToScreen(self->frameBufferProps[0].texColor, self->frameBufferViewport[0]);
  //self->qfeRenderTextureToScreen(self->frameBufferProps[0].texSelect, self->frameBufferViewport[0]);
}

//----------------------------------------------------------------------------
void qfeDriver::qfeDriverRender1(void* caller)
{  
  qfeDriver* self = reinterpret_cast<qfeDriver*>( caller );

  if(self == NULL) return;

  if(self->frameBufferViewport[1]->width <=0 || self->frameBufferViewport[1]->height<=0) return;

  // Set global state
  glDisable(GL_LIGHTING);
  glDisable(GL_FOG);
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_DITHER);
  glDisable(GL_BLEND);  
  glDisable(GL_NORMALIZE);
  glDisable(GL_COLOR_MATERIAL);

  // The main render loop
  self->qfeUpdate();
  self->qfeAttachFBO(self->frameBuffer, self->frameBufferProps[1].texColor, self->frameBufferProps[1].texSelect, self->frameBufferProps[1].texDepth);
  self->qfeEnableFBO(self->frameBuffer);
  self->qfeSetProjection(self->frameBufferGeo[1], self->frameBufferViewport[1], self->frameBufferProps[1].supersampling);

  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glClearColor(1,1,1,0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
  
  self->qfeRenderBackground();  
  self->qfeRenderWait1();

  self->qfeResetProjection();

  self->qfeDisableFBO();

  self->qfeRenderTextureToScreen(self->frameBufferProps[1].texColor, self->frameBufferViewport[1]);  
}

//----------------------------------------------------------------------------
void qfeDriver::qfeDriverRender2(void* caller)
{ 
  qfeDriver* self = reinterpret_cast<qfeDriver*>( caller );

  if(self == NULL) return;

  if(self->frameBufferViewport[2]->width <=0 || self->frameBufferViewport[2]->height<=0) return;

  // Set global state
  glDisable(GL_LIGHTING);
  glDisable(GL_FOG);
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_DITHER);
  glDisable(GL_BLEND);  
  glDisable(GL_NORMALIZE);
  glDisable(GL_COLOR_MATERIAL);

  // The main render loop
  self->qfeUpdate();
  self->qfeAttachFBO(self->frameBuffer, self->frameBufferProps[2].texColor, self->frameBufferProps[2].texSelect, self->frameBufferProps[2].texDepth);
  self->qfeEnableFBO(self->frameBuffer);
  self->qfeSetProjection(self->frameBufferGeo[2], self->frameBufferViewport[2], self->frameBufferProps[2].supersampling);

  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glClearColor(1,1,1,0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
  
  self->qfeRenderBackground();  
  self->qfeRenderWait2();

  self->qfeResetProjection();
  self->qfeDisableFBO();

  self->qfeRenderTextureToScreen(self->frameBufferProps[2].texColor, self->frameBufferViewport[2]);  

  //cout << "tex: " << self->frameBufferProps[2].texColor << endl;
}

//----------------------------------------------------------------------------
void qfeDriver::qfeDriverRender3(void* caller)
{
  qfeDriver* self = reinterpret_cast<qfeDriver*>( caller );

  if(self == NULL) return;

  if(self->frameBufferViewport[3]->width <=0 || self->frameBufferViewport[3]->height<=0) return;

  // Set global state
  glDisable(GL_LIGHTING);
  glDisable(GL_FOG);
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_DITHER);
  glDisable(GL_BLEND);  
  glDisable(GL_NORMALIZE);
  glDisable(GL_COLOR_MATERIAL);

  // The main render loop
  self->qfeUpdate();
  self->qfeAttachFBO(self->frameBuffer, self->frameBufferProps[3].texColor, self->frameBufferProps[3].texSelect, self->frameBufferProps[3].texDepth);
  self->qfeEnableFBO(self->frameBuffer);
  self->qfeSetProjection(self->frameBufferGeo[3], self->frameBufferViewport[3], self->frameBufferProps[3].supersampling);

  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glClearColor(1,1,1,0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
  
  self->qfeRenderBackground();  
  self->qfeRenderWait3();

  self->qfeResetProjection();
  self->qfeDisableFBO();

  self->qfeRenderTextureToScreen(self->frameBufferProps[3].texColor, self->frameBufferViewport[3]);  
}

//----------------------------------------------------------------------------
void qfeDriver::qfeDriverResize(void *caller)
{
  qfeDriver*   self = reinterpret_cast<qfeDriver*>( caller );
  
  if(self == NULL) return;

  qfeDisplay  *display;  
  unsigned int window[2];

  self->scene->qfeGetSceneDisplay(&display);

  display->qfeGetDisplaySize(window[0], window[1]);
  
  self->engine->GetViewport3D(&self->frameBufferViewport[0]);  
  self->engine->GetViewport2D(0, &self->frameBufferViewport[1]);  
  self->engine->GetViewport2D(1, &self->frameBufferViewport[2]);  
  self->engine->GetViewport2D(2, &self->frameBufferViewport[3]);  

  // If the window is resized, update the window
  if(self->windowsize[0] != window[0] || 
     self->windowsize[1] != window[1])
  self->updateWindowSizeFlag = true;

  // If the viewport is resized, update the window
  if(self->viewportsize[0] != self->frameBufferViewport[0]->width || 
     self->viewportsize[1] != self->frameBufferViewport[0]->height)
  self->updateWindowSizeFlag = true; 

  self->windowsize[0]   = window[0];
  self->windowsize[1]   = window[1];    
  self->viewportsize[0] = self->frameBufferViewport[0]->width;
  self->viewportsize[1] = self->frameBufferViewport[0]->height;
}

//----------------------------------------------------------------------------
void qfeDriver::qfeSetProjection(qfeFrame *geo, qfeViewport *vp, int supersampling)
{
  //qfeMatrix4f P;
  //GLfloat     p[16];

  glEnable(GL_SCISSOR_TEST);
  glEnable(GL_DEPTH_TEST);

  // Construct the projection matrix P (OpenGL works column-major)
  //qfeTransform::qfeGetMatrixWorldToViewportOrthogonal(P, geo, vp);  
  //qfeMatrix4f::qfeGetMatrixTranspose(P, P);
  //qfeMatrix4f::qfeGetMatrixElements(P,&p[0]);

  glPushAttrib(GL_VIEWPORT_BIT | GL_SCISSOR_BIT);
  glViewport(0,0,supersampling*vp->width, supersampling*vp->height);
  glScissor(0,0,supersampling*vp->width, supersampling*vp->height);
  
  // Set OpenGL state ready for rendering
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
}

//----------------------------------------------------------------------------
void qfeDriver::qfeResetProjection()
{
  glMatrixMode(GL_PROJECTION); 
  glPopMatrix(); 

  glPopAttrib();
}

//----------------------------------------------------------------------------
void qfeDriver::qfeReadColor(int x, int y, qfeColorRGB &color)
{
  float        col[4];
  int          vpid;
  
  col[0] = col[1] = col[2] = 0.5;
  col[3] = 1.0;

  this->qfeGetViewport(x, y, vpid);

  if(vpid < 0 || vpid > 3) return;
  
  this->qfeAttachFBO(this->frameBuffer, this->frameBufferProps[vpid].texColor, this->frameBufferProps[vpid].texSelect, this->frameBufferProps[vpid].texDepth);
  this->qfeEnableFBO(this->frameBuffer);  
  
  glReadBuffer(GL_COLOR_ATTACHMENT0);  
  glReadPixels(this->frameBufferProps[vpid].supersampling*(x - this->frameBufferViewport[vpid]->origin[0]), 
               this->frameBufferProps[vpid].supersampling*(y - this->frameBufferViewport[vpid]->origin[1]),
               1, 1, GL_BGRA, GL_FLOAT, &col);

  this->qfeDisableFBO();  

  color.r = col[2];
  color.g = col[1];
  color.b = col[0];
}

//----------------------------------------------------------------------------
void qfeDriver::qfeReadSelect(int x, int y, qfeColorRGB &color)
{
 float        col[4];
  int          vpid;
  
  col[0] = col[1] = col[2] = 0.5;
  col[3] = 1.0;

  this->qfeGetViewport(x, y, vpid);

  if(vpid < 0 || vpid > 3) return;
  
  this->qfeAttachFBO(this->frameBuffer, this->frameBufferProps[vpid].texColor, this->frameBufferProps[vpid].texSelect, this->frameBufferProps[vpid].texDepth);
  this->qfeEnableFBO(this->frameBuffer);  
  
  glReadBuffer(GL_COLOR_ATTACHMENT1);  
  glReadPixels(this->frameBufferProps[vpid].supersampling*(x - this->frameBufferViewport[vpid]->origin[0]), 
               this->frameBufferProps[vpid].supersampling*(y - this->frameBufferViewport[vpid]->origin[1]),
               1, 1, GL_BGRA, GL_FLOAT, &col);

  this->qfeDisableFBO();  

  color.r = col[2];
  color.g = col[1];
  color.b = col[0];
} 

//----------------------------------------------------------------------------
void qfeDriver::qfeReadDepth(int x, int y, float &depth)
{
  float d;
  int   vpid;
  
  d = 1.0;

  this->qfeGetViewport(x, y, vpid);

  if(vpid < 0 || vpid > 3) return;
  
  this->qfeAttachFBO(this->frameBuffer, this->frameBufferProps[vpid].texColor, this->frameBufferProps[vpid].texSelect, this->frameBufferProps[vpid].texDepth);
  this->qfeEnableFBO(this->frameBuffer);  
  
  glReadPixels(this->frameBufferProps[vpid].supersampling*(x - this->frameBufferViewport[vpid]->origin[0]), 
               this->frameBufferProps[vpid].supersampling*(y - this->frameBufferViewport[vpid]->origin[1]),
               1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &d);

  this->qfeDisableFBO();  

  depth = d;
}

//----------------------------------------------------------------------------
// \brief We use a right-handed coordinate system, similar to OpenGL
void qfeDriver::qfeUpdateCamera(vtkRenderer *renderer, qfeGeometry *geometry)
{
  vtkCamera *camera;
  qfePoint   origin;
  qfeVector  axisY;
  qfeVector  axisZ;
  double		 extentY, distance;

  distance = (double)LARGEST_DISTANCE; 

  origin.x = geometry->origin.x;
  origin.y = geometry->origin.y;
  origin.z = geometry->origin.z;
  axisY.x  = geometry->axis[1].x;
  axisY.y  = geometry->axis[1].y;
  axisY.z  = geometry->axis[1].z;
  axisZ.x  = geometry->axis[2].x;
  axisZ.y  = geometry->axis[2].y;
  axisZ.z  = geometry->axis[2].z;
  extentY  = geometry->extent[1];

  // Retrieve the camera from the renderer.
  camera = renderer->GetActiveCamera();

  // Set the camera position and orientation from the geometry:
  // The focal point is located in the origin.  
  camera->SetViewUp( (axisY.x), (axisY.y), (axisY.z));
  camera->SetPosition(origin.x + 2.0 * distance * axisZ.x ,
                      origin.y + 2.0 * distance * axisZ.y,
                      origin.z + 2.0 * distance * axisZ.z);
  camera->SetFocalPoint(origin.x, origin.y, origin.z); 
  camera->ComputeViewPlaneNormal();
  camera->SetClippingRange(distance, 3.0*distance);
  camera->SetParallelProjection(1);
  camera->SetParallelScale(extentY/2.0);
}

//----------------------------------------------------------------------------
// \brief We use a right-handed coordinate system, similar to OpenGL
void qfeDriver::qfeUpdateGeometry(vtkRenderer *renderer, qfeGeometry *geometry)
{
  vtkCamera *camera;
  double     origin[3];
  double     position[3];
  double     viewUp[3];
  double     scale;

  qfeVector  axisY;
  qfeVector  axisZ;
  double		 distance;

  if(geometry->qfeIsGeometryChanged()) return;

  distance = (double)LARGEST_DISTANCE; 

  // Retrieve the camera from the renderer.
  camera = renderer->GetActiveCamera();

  // Get the camera position and orientation from the geometry:
  camera->GetFocalPoint(origin[0], origin[1], origin[2]); 
  camera->GetPosition(position[0], position[1], position[2]);
  camera->ComputeViewPlaneNormal();
  camera->GetViewUp(viewUp[0], viewUp[1], viewUp[2]);
  scale = camera->GetParallelScale();  

  // Update the geometry
  geometry->origin.x  = origin[0];
  geometry->origin.y  = origin[1];
  geometry->origin.z  = origin[2];
  geometry->axis[1].x = viewUp[0]*1.0;
  geometry->axis[1].y = viewUp[1]*1.0;
  geometry->axis[1].z = viewUp[2]*1.0;
  geometry->axis[2].x = (origin[0] - position[0]) / 2.0 * distance;
  geometry->axis[2].y = (origin[1] - position[1]) / 2.0 * distance;
  geometry->axis[2].z = (origin[2] - position[2]) / 2.0 * distance;
  geometry->extent[1] = (2.0*scale);  
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriver::qfeGenerateFBO(GLuint &fbo)
{ 
  if(glIsFramebuffer(fbo))
    return qfeError;
  else  
  {
    glGenFramebuffers(1, &fbo); 
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);     
    glBindFramebuffer(GL_FRAMEBUFFER, 0);     
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriver::qfeDeleteFBO(GLuint &fbo)
{
  if(!glIsFramebuffer(fbo))
    return qfeError;
  else
    glDeleteFramebuffers(1, &fbo);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeDriver::qfeEnableFBO(GLuint fbo)
{ 
  //GLenum drawBuffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };

  if(glIsFramebuffer(fbo))
  {
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);     
    //glDrawBuffers(2, drawBuffers);
  }
}

//----------------------------------------------------------------------------
void qfeDriver::qfeDisableFBO()
{
  glBindFramebuffer(GL_FRAMEBUFFER, 0);   
}

//----------------------------------------------------------------------------
void qfeDriver::qfeAttachFBO(GLuint fbo, GLuint color, GLuint select, GLuint depth)
{
  // Bind Depth and Color to Frame Buffer
  if(glIsFramebuffer(fbo) && 
     glIsTexture(color) && glIsTexture(select) && glIsTexture(depth))
  {
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
      GL_TEXTURE_2D, color, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, 
      GL_TEXTURE_2D, select, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 
      GL_TEXTURE_2D, depth, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, 
      GL_TEXTURE_2D, depth, 0);

    // Check Frame Buffer Status
    qfeGLSupport::qfeCheckFrameBuffer(); 

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }
}
  
//----------------------------------------------------------------------------
void qfeDriver::qfeDetachFBO(GLuint fbo)
{
  if(glIsFramebuffer(fbo))   
  {
    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
      GL_TEXTURE_2D, 0, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, 
      GL_TEXTURE_2D, 0, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, 
      GL_TEXTURE_2D, 0, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, 
      GL_TEXTURE_2D, 0, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }
}

//----------------------------------------------------------------------------
void qfeDriver::qfeGenerateTextures(GLuint width, GLuint height, GLuint &color, GLuint &select, GLuint &depth)
{
  if(width <=0 || height <=0) return;

  // Generate Color Texture
  if(glIsTexture(color)) glDeleteTextures(1,&color); 
  glGenTextures(1, &color);
  glBindTexture(GL_TEXTURE_2D, color);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP); 
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, 0);

  // Generate Selection Color Texture
  if(glIsTexture(select)) glDeleteTextures(1,&select); 
  glGenTextures(1, &select);
  glBindTexture(GL_TEXTURE_2D, select);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP); 
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, 0);

  // Generate Depth Texture
  if(glIsTexture(depth)) glDeleteTextures(1,&depth);   
  glGenTextures(1, &depth);
  glBindTexture(GL_TEXTURE_2D, depth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  //glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);   
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0);   

  glBindTexture(GL_TEXTURE_2D, 0);
}

//----------------------------------------------------------------------------
void qfeDriver::qfeDeleteTextures(GLuint &color, GLuint &select, GLuint &depth)
{
  this->qfeDeleteTexture(color);
  this->qfeDeleteTexture(select);
  this->qfeDeleteTexture(depth);
}

//----------------------------------------------------------------------------
void qfeDriver::qfeDeleteTexture(GLuint &texture) 
{
  if(glIsTexture(texture)) glDeleteTextures(1,&texture); 
}

//----------------------------------------------------------------------------
void qfeDriver::qfeRenderTextureToScreen(GLuint texture, qfeViewport *vp)
{
  glViewport(vp->origin[0], vp->origin[1], vp->width, vp->height);  

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(-1, 1, -1, 1);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glActiveTexture(GL_TEXTURE0);   
  glBindTexture(GL_TEXTURE_2D, texture);  
 
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glEnable(GL_TEXTURE_2D);
  glDisable(GL_DEPTH_TEST);
  
  glBegin(GL_QUADS);
  glTexCoord2f(0, 0); glVertex2f(-1, -1);
  glTexCoord2f(0, 1); glVertex2f(-1,  1);
  glTexCoord2f(1, 1); glVertex2f( 1,  1);
  glTexCoord2f(1, 0); glVertex2f( 1, -1);
  glEnd();

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_TEXTURE_2D); 

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
}

//----------------------------------------------------------------------------
void qfeDriver::qfeUpdate()
{  
  qfeLightSource *light;
  qfeVector       lightDir;
    
  this->scene->qfeGetSceneLightSource(&light);

  this->qfeUpdateWindowSize();
  this->qfeUpdateViewGeometry();

  this->qfeUpdateGeometry(this->engine->GetRenderer(),  this->frameBufferGeo[0]);
  this->qfeUpdateGeometry(this->engine->GetRenderer(1), this->frameBufferGeo[1]);
  this->qfeUpdateGeometry(this->engine->GetRenderer(2), this->frameBufferGeo[2]);
  this->qfeUpdateGeometry(this->engine->GetRenderer(3), this->frameBufferGeo[3]);
  
  if(light != NULL)
  { 
    light->qfeGetLightSourceDirection(lightDir);
    this->qfeUpdateLight(this->engine->GetRenderer(), lightDir);    
  }
}

//----------------------------------------------------------------------------
void qfeDriver::qfeUpdateWindowSize()
{  
  qfeViewport *vp3D, *vp2D1, *vp2D2, *vp2D3;
 
  this->engine->GetViewport3D(&vp3D);    
  this->engine->GetViewport2D(0, &vp2D1);    
  this->engine->GetViewport2D(1, &vp2D2);    
  this->engine->GetViewport2D(2, &vp2D3);    

  if(this->updateWindowSizeFlag == true)
  {    
    this->qfeGenerateTextures(this->frameBufferProps[0].supersampling*vp3D->width,  this->frameBufferProps[0].supersampling*vp3D->height, this->frameBufferProps[0].texColor, this->frameBufferProps[0].texSelect, this->frameBufferProps[0].texDepth);
    this->qfeGenerateTextures(this->frameBufferProps[1].supersampling*vp2D1->width, this->frameBufferProps[1].supersampling*vp2D1->height, this->frameBufferProps[1].texColor, this->frameBufferProps[1].texSelect, this->frameBufferProps[1].texDepth);
    this->qfeGenerateTextures(this->frameBufferProps[2].supersampling*vp2D2->width, this->frameBufferProps[2].supersampling*vp2D2->height, this->frameBufferProps[2].texColor, this->frameBufferProps[2].texSelect, this->frameBufferProps[2].texDepth);
    this->qfeGenerateTextures(this->frameBufferProps[3].supersampling*vp2D3->width, this->frameBufferProps[3].supersampling*vp2D3->height, this->frameBufferProps[3].texColor, this->frameBufferProps[3].texSelect, this->frameBufferProps[3].texDepth);
   
    this->engine->Refresh();
    this->updateWindowSizeFlag = false;
  } 
}

//----------------------------------------------------------------------------
void qfeDriver::qfeUpdateViewGeometry()
{
  int  nrViewports = 4;
  bool needRefresh = false;
  
  for(int i=0; i<nrViewports; i++)
  {
    if(this->frameBufferGeo[i]->qfeIsGeometryChanged())
    {
      this->qfeUpdateCamera(this->engine->GetRenderer(i), this->frameBufferGeo[i]);  
      this->frameBufferGeo[i]->qfeSetGeometryChanged(false);  
      
      needRefresh = true;
    }
  } 

  if(needRefresh) this->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriver::qfeUpdateLight(vtkRenderer *renderer, qfeVector direction)
{
  //renderer->LightFollowCameraOn();
  //vtkLightCollection *lightCollection = renderer->GetLights();
  //vtkLight *light;

  //lightCollection->InitTraversal();
  //light = lightCollection->GetNextItem();

  //while(light != NULL)
  //{
  //  double pos[3];
  //  double foc[3];

  //  light->GetFocalPoint(foc);
  //  light->GetPosition(&pos[0]);
  //  
  //  //cout << pos[0] << ", " << pos[1] << ", " << pos[2] << endl;
  //  //cout << foc[0] << ", " << foc[1] << ", " << foc[2] << endl << endl;
  //  //light->SetPosition(direction.x, direction.y, direction.z);
  //  light->Print(cerr);

  //  light = lightCollection->GetNextItem();
  //}

  //direction = direction * -1 * LARGEST_DISTANCE;
  //qfeVector::qfeVectorPrint(direction);

  // Overrule VTK light

  // Distinguish headlight, cameralight, scenelight

  float dir[4];
  glGetLightfv(GL_LIGHT0, GL_POSITION, dir);
  //cout << dir[0] << ", " << dir[1] << ", " << dir[2] << " - " << dir[3] << endl; 
   
  dir[0] = direction.x;
  dir[1] = direction.y;
  dir[2] = direction.z;
  //dir[3] = 1.0f;

  //dir[2] = -1.0*dir[2];

  //glLightf(static_cast<GLenum>(GL_LIGHT0), GL_SPOT_EXPONENT, 0);
  //glLightf(static_cast<GLenum>(GL_LIGHT0), GL_SPOT_CUTOFF, 180);
  glLightfv(GL_LIGHT0, GL_POSITION, dir);
}

//----------------------------------------------------------------------------
void qfeDriver::qfeGetViewport(int x, int y, int &vpid)
{
  vpid = 0;

  if(x >= (int)(this->frameBufferViewport[1]->origin[0]) &&
     x <= (int)(this->frameBufferViewport[1]->origin[0] + this->frameBufferViewport[1]->width)  &&
     y >= (int)(this->frameBufferViewport[1]->origin[1]) &&
     y <= (int)(this->frameBufferViewport[1]->origin[1] + this->frameBufferViewport[1]->height))
    vpid = 1;

  if(x >= (int)(this->frameBufferViewport[2]->origin[0]) &&
     x <= (int)(this->frameBufferViewport[2]->origin[0] + this->frameBufferViewport[2]->width)  &&
     y >= (int)(this->frameBufferViewport[2]->origin[1]) &&
     y <= (int)(this->frameBufferViewport[2]->origin[1] + this->frameBufferViewport[2]->height))
    vpid = 2;

  if(x >= (int)(this->frameBufferViewport[3]->origin[0]) &&
     x <= (int)(this->frameBufferViewport[3]->origin[0] + this->frameBufferViewport[3]->width)  &&
     y >= (int)(this->frameBufferViewport[3]->origin[1]) &&
     y <= (int)(this->frameBufferViewport[3]->origin[1] + this->frameBufferViewport[3]->height))
    vpid = 3;
}

/*
//----------------------------------------------------------------------------
void qfeDriver::qfeStartCounter()
{
    LARGE_INTEGER li;
    if(!QueryPerformanceFrequency(&li))
        cout << "QueryPerformanceFrequency failed!\n";

    PCFreq = double(li.QuadPart)/1000.0;

    QueryPerformanceCounter(&li);
    CounterStart = li.QuadPart;
}

//----------------------------------------------------------------------------
double qfeDriver::qfeGetCounter()
{
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    return double(li.QuadPart-CounterStart)/PCFreq;
}
*/
