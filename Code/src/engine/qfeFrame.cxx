#include "qfeFrame.h"

//----------------------------------------------------------------------------
// qfeFrame
//----------------------------------------------------------------------------
qfeFrame::qfeFrame()
{
  this->geometryType = QFE_FRAME;

  this->origin.x   = this->origin.y  = this->origin.z = 0.0;
  this->axis[0].x  = this->axis[1].y = this->axis[2].z = 1.0;
  this->axis[0].y  = this->axis[0].y = 0.0;
  this->axis[1].x  = this->axis[1].z = 0.0;
  this->axis[2].x  = this->axis[2].y = 0.0;
  this->extent[0]  = this->extent[1] = 0.0;
};

//----------------------------------------------------------------------------
qfeFrame::~qfeFrame()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeFrame::qfeSetFrameOrigin(qfeFloat ox, qfeFloat oy, qfeFloat oz)
{
  this->origin.x = ox;
  this->origin.y = oy;
  this->origin.z = oz;

  this->geometryChanged = true;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFrame::qfeGetFrameOrigin(qfeFloat& ox, qfeFloat& oy, qfeFloat& oz)
{
  ox = this->origin.x;
  oy = this->origin.y;
  oz = this->origin.z;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFrame::qfeSetFrameAxes(qfeFloat axx, qfeFloat axy, qfeFloat axz,
                                          qfeFloat ayx, qfeFloat ayy, qfeFloat ayz,
                                          qfeFloat azx, qfeFloat azy, qfeFloat azz)
{
  this->axis[0].x = axx; this->axis[0].y = axy; this->axis[0].z = axz;
  this->axis[1].x = ayx; this->axis[1].y = ayy; this->axis[1].z = ayz;
  this->axis[2].x = azx; this->axis[2].y = azy; this->axis[2].z = azz;

  this->geometryChanged = true;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFrame::qfeGetFrameAxes(qfeFloat& axx, qfeFloat& axy, qfeFloat& axz,
                                          qfeFloat& ayx, qfeFloat& ayy, qfeFloat& ayz,
                                          qfeFloat& azx, qfeFloat& azy, qfeFloat& azz)
{
  axx = this->axis[0].x; axy = this->axis[0].y; axz = this->axis[0].z;
  ayx = this->axis[1].x; ayy = this->axis[1].y; ayz = this->axis[1].z;
  azx = this->axis[2].x; azy = this->axis[2].y; azz = this->axis[2].z;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFrame::qfeSetFrameExtent(qfeFloat ex, qfeFloat ey, qfeFloat ez)
{
  this->extent[0] = ex;
  this->extent[1] = ey;
  this->extent[2] = ez;

  this->geometryChanged = true;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFrame::qfeGetFrameExtent(qfeFloat& ex, qfeFloat& ey, qfeFloat &ez)
{
  ex = this->extent[0];
  ey = this->extent[1];
  ez = this->extent[2];

  return qfeSuccess;
}


//----------------------------------------------------------------------------
// qfeFrameSlice
//----------------------------------------------------------------------------
qfeFrameSlice::qfeFrameSlice(qfeFloat ox,  qfeFloat oy,  qfeFloat oz,
                             qfeFloat axx, qfeFloat axy, qfeFloat axz,
                             qfeFloat ayx, qfeFloat ayy, qfeFloat ayz,
                             qfeFloat azx, qfeFloat azy, qfeFloat azz,
                             qfeFloat ex,  qfeFloat ey,  qfeFloat ez)
{
  this->geometryType = QFE_FRAME_SLICE;

  this->qfeSetFrameOrigin(ox, oy, oz);
  this->qfeSetFrameAxes(axx, axy, axz, ayx, ayy, ayz, azx, azy, azz);
  this->qfeSetFrameExtent(ex, ey, ez);
}

//----------------------------------------------------------------------------
qfeFrameSlice::~qfeFrameSlice()
{
};

//----------------------------------------------------------------------------
// qfeFrameProjection
//----------------------------------------------------------------------------
qfeFrameProjection::qfeFrameProjection( qfeFloat ox,  qfeFloat oy,  qfeFloat oz,
                                        qfeFloat axx, qfeFloat axy, qfeFloat axz,
                                        qfeFloat ayx, qfeFloat ayy, qfeFloat ayz,
                                        qfeFloat azx, qfeFloat azy, qfeFloat azz,
                                        qfeFloat ex,  qfeFloat ey,  qfeFloat ez)
{
  this->geometryType = QFE_FRAME_PROJECTION;

  this->qfeSetFrameOrigin(ox, oy, oz);
  this->qfeSetFrameAxes(axx, axy, axz, ayx, ayy, ayz, azx, azy, azz);
  this->qfeSetFrameExtent(ex, ey, ez);
}

//----------------------------------------------------------------------------
qfeFrameProjection::~qfeFrameProjection()
{
};

