#include "qfeScene.h"

//----------------------------------------------------------------------------
qfeScene::qfeScene()
{
  this->geo      = NULL;
  this->light    = NULL;
  this->display  = NULL;
  this->type     = QFE_SCENE_3D;
  this->cursor   = cursorArrow; 
  featureDataType[0] = 0;
  featureDataType[1] = 0;
};

//----------------------------------------------------------------------------
qfeScene::qfeScene(const qfeScene &scene)
{
  for(int i=0; i<(int)scene.visuals.size(); i++)
    this->visuals.push_back(new qfeVisual(*scene.visuals[i]));

  this->display   = new qfeDisplay(*scene.display);
  this->geo       = new qfeGeometry(*scene.geo); // \todo determine type here
  this->light     = new qfeLightSource(*scene.light);
  this->type      = scene.type;
  this->cursor    = scene.cursor;  
};

//----------------------------------------------------------------------------
qfeScene::~qfeScene()
{
  delete this->geo;
  delete this->light;
  delete this->display;
  
  this->qfeRemoveSceneVisuals();

  this->geo      = NULL;
  this->light    = NULL;
  this->display  = NULL;  
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeSetSceneType(int type)
{
  this->type = type;

  return qfeSuccess;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeGetSceneType(int &type)
{
  type = this->type;

  return qfeSuccess;
};

//---------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeRemoveSceneVisual(qfeVisual *visual)
{
  vector<qfeVisual *>::iterator it = find (this->visuals.begin(), this->visuals.end(), visual);

  this->visuals.erase(it);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeRemoveSceneVisuals()
{
  vector<qfeVisual*>::iterator i;
  for(i = this->visuals.begin(); i < this->visuals.end(); i++) delete *i;
  this->visuals.clear();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeGetSceneVisuals(vector<qfeVisual *>&visual)
{
  if(this->visuals.size() <= 0) return qfeError;

  visual.clear();
  for(int i=0; i<(int)this->visuals.size(); i++)
    visual.push_back(this->visuals[i]);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeGetSceneVisualsSize(unsigned int &visualsSize)
{
  visualsSize = (int)this->visuals.size();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeSetSceneDisplay(qfeDisplay *display)
{
  delete this->display;

  this->display = new qfeDisplay(*display);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeGetSceneDisplay(qfeDisplay **display)
{
  *display = this->display;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeSetSceneLightSource(qfeLightSource *light)
{
  delete this->light;

  this->light = new qfeLightSource(*light);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeGetSceneLightSource(qfeLightSource **light)
{
  *light = this->light;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeSetSceneCursor(qfeCursorType curs)
{
  this->cursor = curs;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeGetSceneCursor(qfeCursorType &curs)
{
  curs = this->cursor;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeSceneUpdate()
{
  vtkRenderWindow *rw;
  this->display->qfeGetDisplayWindow(&rw);

  // We abuse the event-observer approach of vtk 
  // to send a message to the QT gui
  rw->InvokeEvent(vtkCommand::UpdateInformationEvent, NULL);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeScene::qfeSelectionUpdate()
{
  vtkRenderWindow *rw;
  this->display->qfeGetDisplayWindow(&rw);

  // We abuse the event-observer approach of vtk 
  // to send a message to the QT gui
  rw->InvokeEvent(vtkCommand::UpdatePropertyEvent, NULL);

  return qfeSuccess;
}

qfeReturnStatus qfeScene::qfeSetSceneFeatureTF(int id, QVector<QPair<double, double>> &tf, int dataType)
{
  featureTF[id] = tf;
  featureDataType[id] = dataType;
  return qfeSuccess;
}

QVector<QPair<double, double>> &qfeScene::qfeGetSceneFeatureTF(int id)
{
  return featureTF[id];
}

int qfeScene::qfeGetSceneFeatureDataType(int id)
{
  return featureDataType[id];
}

