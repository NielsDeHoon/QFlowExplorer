#pragma once

#include "qfeSeedVolume.h"
#include "qfeColorMaps.h"
#include "qfeVolume.h"
#include "qfeStudy.h"
#include "qfeTransform.h"

#include <assert.h>
#include <QtCore>

#include <qfiledialog.h>

/**
* \file   qfeSeedVolumeCollection.h
* \author Niels de Hoon
* \class  qfeSeedVolumeCollection
* \brief  Volume collection for containing seeding volumes for flow visualization
* \note   Confidential
*
* A set of volumes containing seed points for flow visualization
*/


enum guidePointType
  {
    diameter1,
    diameter2,
    apex,
	other_point_type
  };

enum seedVolumeShape
{
	EllipsoidType,
	CylinderType
};

enum seedVolumeType
{
	seedVolumeTypeUnknown,
	seedVolumeTypeSource,
	seedVolumeTypeSink
};

typedef struct
{
	qfePoint point;
	int index;
	guidePointType type;
} seedVolumeGuidePoint;

typedef struct
{
	unsigned int unique_id;
	unsigned int counter; //can be used to keep track of properties (e.g. nr particles generated/removed)
	seedVolumeShape volumeShape;
	seedVolumeType type;
	qfeSeedVolume *volume;
} seedVolume;

class qfeSeedVolumeCollection {
public:
  qfeSeedVolumeCollection(qfeStudy *_study);
  virtual ~qfeSeedVolumeCollection()
  {
  };

  //array like functionality:
  unsigned int size()
  {
	  return (unsigned int)seedVolumes.size();
  }
  seedVolume* at(unsigned int index)
  {
	  assert(index<this->size());

	  return &seedVolumes[index];
  }

  qfeReturnStatus qfeResetCounters();
  qfeReturnStatus qfeSetCounter(unsigned int index, unsigned int new_value);
  qfeReturnStatus qfeAddToCounter(unsigned int index, unsigned int value_to_add);

  qfeReturnStatus qfeAddSeedVolume(qfePoint dia1, qfePoint dia2, qfePoint apex, seedVolumeShape shape);
  qfeReturnStatus qfeAddSeedVolume(qfePoint dia1, qfePoint dia2, qfePoint apex, seedVolumeShape shape, seedVolumeType type);
  qfeReturnStatus qfeRemoveSeedVolume(unsigned int index);
  qfeReturnStatus qfeGetSeedVolume(unsigned int index, qfePoint &dia1, qfePoint &dia2, qfePoint &apex);
  qfeReturnStatus qfeUpdateSeedVolumeGuidePoint(unsigned int index, qfePoint point, guidePointType type);
  qfeReturnStatus qfeSquishSeedVolume(unsigned int index);
  qfeReturnStatus qfeStretchSeedVolume(unsigned int index);
  qfeReturnStatus qfeGetAllGuidePoints(std::vector<seedVolumeGuidePoint> &pointList);

  qfeReturnStatus qfeSaveSeedVolumes();
  qfeReturnStatus qfeLoadSeedVolumes();

  qfeColorRGB qfeGetSeedVolumeColor(int i);
  qfeColorRGB qfeGetSelectedSeedVolumeColor(seedVolumeGuidePoint selected_point);
  float qfeGetColorIndex(unsigned int _unique_id);

  qfeReturnStatus qfeRenderSeedVolumes(vtkCamera *camera, int vp_width, int vp_height, seedVolumeGuidePoint selected_point, bool renderSurface);

  qfeReturnStatus qfeUpdate();
  qfeReturnStatus GetVolumeIndexTextureID(GLuint &tid);

  qfeColorRGB sourceColor;
  qfeColorRGB sinkColor;

  seedVolumeGuidePoint seedVolumeGuidePointConstructor(qfePoint _point, int _index, guidePointType _type)
  {
	  seedVolumeGuidePoint pSGP;
	  pSGP.point = _point;
	  pSGP.index = _index;
	  pSGP.type = _type;

	  return pSGP;
  }

  
  qfeReturnStatus qfeRenderCube(qfePoint pos, float size, bool draw_outline, qfeColorRGB color);

private:
  QList<seedVolume> seedVolumes;
  vector<qfeColorRGB> seedVolumeColorMapColors;

  qfeStudy *study;

  //stores a volume that contains either the indices of the seed volumes or 0
  qfeVolume indexVolume;
};