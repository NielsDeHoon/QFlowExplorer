#include "qfeLightSource.h"

//----------------------------------------------------------------------------
qfeLightSource::qfeLightSource()
{
  this->direction.qfeSetVectorElements(0.0,0.0,1.0);

  this->lightAmbient       = 0.3f;
  this->lightDiffuse       = 0.3f;
  this->lightSpecular      = 0.3f;
  this->lightSpecularPower = 10.0f;
};

//----------------------------------------------------------------------------
qfeLightSource::qfeLightSource(const qfeLightSource &light)
{
  this->direction          = light.direction;
  this->lightAmbient       = light.lightAmbient;
  this->lightDiffuse       = light.lightDiffuse;
  this->lightSpecular      = light.lightSpecular;
  this->lightSpecularPower = light.lightSpecularPower;
};

//----------------------------------------------------------------------------
qfeLightSource::~qfeLightSource()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeLightSource::qfeSetLightSourceDirection(qfeVector direction)
{
  this->direction = direction;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeLightSource::qfeSetLightSourceDirection(qfeFloat x, qfeFloat y, qfeFloat z)
{
  this->direction.x = x;
  this->direction.y = y;
  this->direction.z = z;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeLightSource::qfeGetLightSourceDirection(qfeVector &direction)
{
  direction = this->direction;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeLightSource::qfeSetLightSourceAmbientContribution(qfeFloat ambient)
{
  this->lightAmbient = ambient;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeLightSource::qfeGetLightSourceAmbientContribution(qfeFloat &ambient)
{
  ambient = this->lightAmbient;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeLightSource::qfeSetLightSourceDiffuseContribution(qfeFloat diffuse)
{
  this->lightDiffuse = diffuse;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeLightSource::qfeGetLightSourceDiffuseContribution(qfeFloat &diffuse)
{
  diffuse = this->lightDiffuse;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeLightSource::qfeSetLightSourceSpecularContribution(qfeFloat specular)
{
  this->lightSpecular = specular;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeLightSource::qfeGetLightSourceSpecularContribution(qfeFloat &specular)
{
  specular = this->lightSpecular;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeLightSource::qfeSetLightSourceSpecularPower(qfeFloat power)
{
  this->lightSpecularPower = power;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeLightSource::qfeGetLightSourceSpecularPower(qfeFloat &power)
{
  power = this->lightSpecularPower;

  return qfeSuccess;
}

