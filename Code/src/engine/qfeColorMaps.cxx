#include "qfeColorMaps.h"

bool qfeColorMaps::transferFunctionColorMapChanged = true;
qfeColorMap qfeColorMaps::transferFunctionColorMap;

void qfeColorMaps::createColorMaps()
{ 	
	std::pair<std::string, qfeColorMap> viridis("Viridis", this->createViridisColorMap());
	colormaps.push_back(viridis);

	std::pair<std::string, qfeColorMap> blackBody("Black body", this->createBlackBodyColorMap());
	colormaps.push_back(blackBody);

	std::pair<std::string, qfeColorMap> blueTurquoiseWhite("Blue Turquoise White", this->createBlueTurquoiseWhiteColorMap());
	colormaps.push_back(blueTurquoiseWhite);

	std::pair<std::string, qfeColorMap> brownOrangeYellow("Brown Orange Yellow", this->createBrownOrangeYellowColorMap());
	colormaps.push_back(brownOrangeYellow);

	std::pair<std::string, qfeColorMap> rainbow("Rainbow", this->createRainbowColorMap());
	colormaps.push_back(rainbow);

	std::pair<std::string, qfeColorMap> jet("Jet (MatLab old)", this->createJetColorMap());
	colormaps.push_back(jet);

	std::pair<std::string, qfeColorMap> doppler1("Doppler1", this->createDoppler1ColorMap());
	colormaps.push_back(doppler1);

	std::pair<std::string, qfeColorMap> doppler2("Doppler2", this->createDoppler2ColorMap());
	colormaps.push_back(doppler2);

	std::pair<std::string, qfeColorMap> invertedColdAndHot("Inverted Cold and Hot", this->createInvertedColdAndHotColorMap());
	colormaps.push_back(invertedColdAndHot);

	std::pair<std::string, qfeColorMap> coldAndHot("Cold and Hot", this->createColdAndHotColorMap());
	colormaps.push_back(coldAndHot);

	std::pair<std::string, qfeColorMap> diverging1("Diverging 1", this->createDiverging1ColorMap());
	colormaps.push_back(diverging1);

	std::pair<std::string, qfeColorMap> diverging2("Diverging 2", this->createDiverging2ColorMap());
	colormaps.push_back(diverging2);
	
	std::pair<std::string, qfeColorMap> diverging3("Diverging 3", this->createDiverging3ColorMap());
	colormaps.push_back(diverging3);

	std::pair<std::string, qfeColorMap> diverging4("Diverging 4", this->createDiverging4ColorMap());
	colormaps.push_back(diverging4);

	std::pair<std::string, qfeColorMap> diverging5("Diverging 5", this->createDiverging5ColorMap());
	colormaps.push_back(diverging5);

	std::pair<std::string, qfeColorMap> coolWarm("Cool / Warm", this->createCoolWarmColorMap());
	colormaps.push_back(coolWarm);

	std::pair<std::string, qfeColorMap> heat("Heat", this->createHeatColorMap());
	colormaps.push_back(heat);

	std::pair<std::string, qfeColorMap> grey("Grey", this->createGreyColorMap());
	colormaps.push_back(grey);

	std::pair<std::string, qfeColorMap> pinkgrey("Pink to Grey", this->createPinkGreyColorMap());
	colormaps.push_back(pinkgrey);

	std::pair<std::string, qfeColorMap> magma("Magma", this->createMagmaColorMap());
	colormaps.push_back(magma);

	std::pair<std::string, qfeColorMap> inferno("Inferno", this->createInfernoColorMap());
	colormaps.push_back(inferno);

	std::pair<std::string, qfeColorMap> plasma("Plasma", this->createPlasmaColorMap());
	colormaps.push_back(plasma);

	std::pair<std::string, qfeColorMap> parula("Parula (MatLab new)", this->createParulaColorMap());
	colormaps.push_back(parula);

	std::pair<std::string, qfeColorMap> isoL("IsoLum", this->createIsoLColorMap());
	colormaps.push_back(isoL);
	
	std::pair<std::string, qfeColorMap> isoAZ("IsoLumAZ", this->createIsoAZColorMap());
	colormaps.push_back(isoAZ);

	std::pair<std::string, qfeColorMap> isoLumRainbow("IsoLumRainbow", this->createIsoLumRainbowColorMap());
	colormaps.push_back(isoLumRainbow);

	std::pair<std::string, qfeColorMap> isoLumRainbow2("IsoLumRainbow2", this->createIsoLumRainbow2ColorMap());
	colormaps.push_back(isoLumRainbow2);

	std::pair<std::string, qfeColorMap> isoLumRainbow3("IsoLumRainbow3", this->createIsoLumRainbow3ColorMap());
	colormaps.push_back(isoLumRainbow3);
	
	std::pair<std::string, qfeColorMap> isoLumViridis("IsoLumViridis", this->createIsoLumViridisColorMap());
	colormaps.push_back(isoLumViridis);

	std::pair<std::string, qfeColorMap> isoLumDivergent1("IsoLumDivergent1", this->createIsoLumDivergent1ColorMap());
	colormaps.push_back(isoLumDivergent1);

	std::pair<std::string, qfeColorMap> isoLumCircular("IsoLumCircular", this->createIsoLumCircularColorMap());
	colormaps.push_back(isoLumCircular);

	std::pair<std::string, qfeColorMap> isoLightViridis("IsoLightViridis", this->createIsoLightViridisColorMap());
	colormaps.push_back(isoLightViridis);

	std::pair<std::string, qfeColorMap> isoLightDarkViridis("IsoLightDarkViridis", this->createIsoLightDarkViridisColorMap());
	colormaps.push_back(isoLightDarkViridis);

	std::pair<std::string, qfeColorMap> basicCircular("Basic circular", this->createBasicCircularColorMap());
	colormaps.push_back(basicCircular);

	std::pair<std::string, qfeColorMap> blueRed("Blue to red", this->createBlueRedColorMap());
	colormaps.push_back(blueRed);

	std::pair<std::string, qfeColorMap> whiteRed("White to red", this->createWhiteRedColorMap());
	colormaps.push_back(whiteRed);

	std::pair<std::string, qfeColorMap> greenWhite("Green to white", this->createGreenWhiteColorMap());
	colormaps.push_back(greenWhite);

	std::pair<std::string, qfeColorMap> EBU("EBU", this->createEBUColorMap());
	colormaps.push_back(EBU);

	for(unsigned int i = 0; i<colormaps.size(); i++)
	{
		colorMapsAvailable.push_back(colormaps[i].first);
	}
}

unsigned int qfeColorMaps::qfeGetIDIncreasingColorScheme()
{
	return 0;
}

unsigned int qfeColorMaps::qfeGetIDDivergingColorScheme()
{
	return 13;
}

void qfeColorMaps::removeColorMaps()
{ 
	while(colormaps.size() != 0)
	{
		colormaps.pop_back();
	}
}

void qfeColorMaps::qfeGetColorMap(const unsigned int id, std::string &name, qfeColorMap &colorMap)
{
	name = colormaps[id].first;
	colorMap = colormaps[id].second;
}

std::vector<std::string> qfeColorMaps::qfeGetAvailableColorMaps()
{
	return this->colorMapsAvailable;
}

void qfeColorMaps::qfeUpdateTransferFunctionColorMap(qfeColorMap colorMap)
{
	transferFunctionColorMapChanged = true;
	transferFunctionColorMap = colorMap;
}

bool qfeColorMaps::qfeIsTransferFunctionColorMapChanged()
{
	return transferFunctionColorMapChanged;
}

qfeColorMap qfeColorMaps::qfeGetTransferFunctionColorMap()
{
	transferFunctionColorMapChanged = false;
	return transferFunctionColorMap;
}

qfeColorMap qfeColorMaps::createBlueTurquoiseWhiteColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(3);
	rgb[0].color.r = 0.000f; rgb[0].color.g = 0.000f; rgb[0].color.b = 1.000f; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.000f; rgb[1].color.g = 1.000f; rgb[1].color.b = 1.000f; rgb[1].value = 0.50f;
	rgb[2].color.r = 1.000f; rgb[2].color.g = 1.000f; rgb[2].color.b = 1.000f; rgb[2].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createBrownOrangeYellowColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(3);
	rgb[0].color.r = 0.400f; rgb[0].color.g = 0.145f; rgb[0].color.b = 0.024f; rgb[0].value = 0.00f;
	rgb[1].color.r = 1.000f; rgb[1].color.g = 0.686f; rgb[1].color.b = 0.240f; rgb[1].value = 0.50f;
	rgb[2].color.r = 1.000f; rgb[2].color.g = 1.000f; rgb[2].color.b = 0.898f; rgb[2].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createBlackBodyColorMap()
{	
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(4);
	rgb[0].color.r = 0.000f; rgb[0].color.g = 0.000f; rgb[0].color.b = 0.000f; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.902f; rgb[1].color.g = 0.000f; rgb[1].color.b = 0.000f; rgb[1].value = 0.40f;
	rgb[2].color.r = 0.902f; rgb[2].color.g = 0.902f; rgb[2].color.b = 0.000f; rgb[2].value = 0.80f;
	rgb[3].color.r = 1.000f; rgb[3].color.g = 1.000f; rgb[3].color.b = 1.000f; rgb[3].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createRainbowColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.000f; rgb[0].color.g = 0.000f; rgb[0].color.b = 1.000f; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.000f; rgb[1].color.g = 1.000f; rgb[1].color.b = 1.000f; rgb[1].value = 0.25f;
	rgb[2].color.r = 0.000f; rgb[2].color.g = 1.000f; rgb[2].color.b = 0.000f; rgb[2].value = 0.50f;
	rgb[3].color.r = 1.000f; rgb[3].color.g = 1.000f; rgb[3].color.b = 0.000f; rgb[3].value = 0.75f;
	rgb[4].color.r = 1.000f; rgb[4].color.g = 0.000f; rgb[4].color.b = 0.000f; rgb[4].value = 1.00f;

	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createJetColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping> rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(7);
	rgb[0].color.r = 0.f/255.f; rgb[0].color.g = 0.f/255.f; rgb[0].color.b = 143.f/255.f; rgb[0].value = 0.0f;
	rgb[1].color.r = 0.f/255.f; rgb[1].color.g = 0.f/255.f; rgb[1].color.b = 255.f/255.f; rgb[1].value = 1.f/9.f;
	rgb[2].color.r = 0.f/255.f; rgb[2].color.g = 255.f/255.f; rgb[2].color.b = 255.f/255.f; rgb[2].value = 23.f/63.f;
	rgb[3].color.r = 128.f/255.f; rgb[3].color.g = 255.f/255.f; rgb[3].color.b = 128.f/255.f; rgb[3].value = 0.5f;
	rgb[4].color.r = 255.f/255.f; rgb[4].color.g = 255.f/255.f; rgb[4].color.b = 0.f/255.f; rgb[4].value = 39.f/63.f;
	rgb[5].color.r = 255.f/255.f; rgb[5].color.g = 0.f/255.f; rgb[5].color.b = 0.f/255.f; rgb[5].value = 8.f/9.f;
	rgb[6].color.r = 128.f/255.f; rgb[6].color.g = 0.f/255.f; rgb[6].color.b = 0.f/255.f; rgb[6].value = 1.0f;

	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createParulaColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping> rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(11);
	rgb[0].color.r = 0.2081; rgb[0].color.g = 0.1663; rgb[0].color.b = 0.5292; rgb[0].value = 0.0f;
	rgb[1].color.r = 0.1147; rgb[1].color.g = 0.3306; rgb[1].color.b = 0.8387; rgb[1].value = 0.1f;
	rgb[2].color.r = 0.0582; rgb[2].color.g = 0.4677; rgb[2].color.b = 0.8589; rgb[2].value = 0.2f;
	rgb[3].color.r = 0.0523; rgb[3].color.g = 0.5727; rgb[3].color.b = 0.8231; rgb[3].value = 0.3f;
	rgb[4].color.r = 0.0282; rgb[4].color.g = 0.6663; rgb[4].color.b = 0.7574; rgb[4].value = 0.4f;
	rgb[5].color.r = 0.1938; rgb[5].color.g = 0.7205; rgb[5].color.b = 0.6338; rgb[5].value = 0.5f;
	rgb[6].color.r = 0.4783; rgb[6].color.g = 0.7489; rgb[6].color.b = 0.4877; rgb[6].value = 0.6f;
	rgb[7].color.r = 0.7176; rgb[7].color.g = 0.7412; rgb[7].color.b = 0.3908; rgb[7].value = 0.7f;
	rgb[8].color.r = 0.9264; rgb[8].color.g = 0.7256; rgb[8].color.b = 0.2996; rgb[8].value = 0.8f;
	rgb[9].color.r = 0.9828; rgb[9].color.g = 0.8184; rgb[9].color.b = 0.1700; rgb[9].value = 0.9f;
	rgb[10].color.r = 0.9763; rgb[10].color.g = 0.9831; rgb[10].color.b = 0.0538; rgb[10].value = 1.0f;

	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createDoppler1ColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.840f; rgb[0].color.g = 0.879f; rgb[0].color.b = 0.355f; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.594f; rgb[1].color.g = 0.137f; rgb[1].color.b = 0.000f; rgb[1].value = 0.25f;
	rgb[2].color.r = 0.001f; rgb[2].color.g = 0.001f; rgb[2].color.b = 0.001f; rgb[2].value = 0.50f;
	rgb[3].color.r = 0.047f; rgb[3].color.g = 0.551f; rgb[3].color.b = 1.000f; rgb[3].value = 0.75f;
	rgb[4].color.r = 0.484f; rgb[4].color.g = 0.965f; rgb[4].color.b = 0.990f; rgb[4].value = 1.00f;

	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createDoppler2ColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.840f; rgb[0].color.g = 0.879f; rgb[0].color.b = 0.355f; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.594f; rgb[1].color.g = 0.137f; rgb[1].color.b = 0.000f; rgb[1].value = 0.25f;
	rgb[2].color.r = 0.001f; rgb[2].color.g = 0.001f; rgb[2].color.b = 0.001f; rgb[2].value = 0.50f;
	rgb[3].color.r = 0.047f; rgb[3].color.g = 0.551f; rgb[3].color.b = 1.000f; rgb[3].value = 0.75f;
	rgb[4].color.r = 0.000f; rgb[4].color.g = 1.000f; rgb[4].color.b = 0.351f; rgb[4].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createInvertedColdAndHotColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);	
	rgb[0].color.r = 0.000f; rgb[0].color.g = 0.000f; rgb[0].color.b = 1.000f; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.000f; rgb[1].color.g = 1.000f; rgb[1].color.b = 1.000f; rgb[1].value = 0.45f;
	rgb[2].color.r = 1.000f; rgb[2].color.g = 1.000f; rgb[2].color.b = 1.000f; rgb[2].value = 0.50f;
	rgb[3].color.r = 1.000f; rgb[3].color.g = 1.000f; rgb[3].color.b = 0.000f; rgb[3].value = 0.55f;	
	rgb[4].color.r = 1.000f; rgb[4].color.g = 0.000f; rgb[4].color.b = 0.000f; rgb[4].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createColdAndHotColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.000f; rgb[0].color.g = 1.000f; rgb[0].color.b = 1.000f; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.000f; rgb[1].color.g = 0.000f; rgb[1].color.b = 1.000f; rgb[1].value = 0.45f;
	rgb[2].color.r = 0.000f; rgb[2].color.g = 0.000f; rgb[2].color.b = 0.500f; rgb[2].value = 0.50f;
	rgb[3].color.r = 1.000f; rgb[3].color.g = 0.000f; rgb[3].color.b = 0.000f; rgb[3].value = 0.55f;
	rgb[4].color.r = 1.000f; rgb[4].color.g = 1.000f; rgb[4].color.b = 0.000f; rgb[4].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createDiverging1ColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(3);
	rgb[0].color.r = 0.365f; rgb[0].color.g = 0.545f; rgb[0].color.b = 0.812f; rgb[0].value = 0.00f;
	rgb[1].color.r = 1.000f; rgb[1].color.g = 1.000f; rgb[1].color.b = 1.000f; rgb[1].value = 0.50f;
	rgb[2].color.r = 0.937f; rgb[2].color.g = 0.424f; rgb[2].color.b = 0.306f; rgb[2].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createDiverging2ColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.102f; rgb[0].color.g = 0.588f; rgb[0].color.b = 0.255f; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.651f; rgb[1].color.g = 0.851f; rgb[1].color.b = 0.416f; rgb[1].value = 0.25f;
	rgb[2].color.r = 1.000f; rgb[2].color.g = 1.000f; rgb[2].color.b = 0.749f; rgb[2].value = 0.50f;
	rgb[3].color.r = 0.992f; rgb[3].color.g = 0.682f; rgb[3].color.b = 0.380f; rgb[3].value = 0.75f;
	rgb[4].color.r = 0.843f; rgb[4].color.g = 0.098f; rgb[4].color.b = 0.110f; rgb[4].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);


	return colorMap;
}

qfeColorMap qfeColorMaps::createDiverging3ColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.549f; rgb[0].color.g = 0.549f; rgb[0].color.b = 0.549f; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.843f; rgb[1].color.g = 0.098f; rgb[1].color.b = 0.098f; rgb[1].value = 0.25f;
	rgb[2].color.r = 1.000f; rgb[2].color.g = 1.000f; rgb[2].color.b = 0.749f; rgb[2].value = 0.50f;
	rgb[3].color.r = 0.102f; rgb[3].color.g = 0.588f; rgb[3].color.b = 0.255f; rgb[3].value = 0.75f;
	rgb[4].color.r = 0.549f; rgb[4].color.g = 0.549f; rgb[4].color.b = 0.549f; rgb[4].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createDiverging4ColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(3);
	rgb[0].color.r = (float)153/255; rgb[0].color.g = (float)142/255; rgb[0].color.b = (float)195/255; rgb[0].value = 0.00f;
	rgb[1].color.r = (float)255/255; rgb[1].color.g = (float)255/255; rgb[1].color.b = (float)255/255; rgb[1].value = 0.50f;	
	rgb[2].color.r = (float)214/255; rgb[2].color.g = (float)163/255; rgb[2].color.b = (float) 64/255; rgb[2].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createDiverging5ColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(3);
	rgb[0].color.r = (float)0/255; rgb[0].color.g = (float)0/255; rgb[0].color.b = (float)255/255; rgb[0].value = 0.00f;
	rgb[1].color.r = (float)0/255; rgb[1].color.g = (float)0/255; rgb[1].color.b = (float)0/255; rgb[1].value = 0.50f;	
	rgb[2].color.r = (float)255/255; rgb[2].color.g = (float)0/255; rgb[2].color.b = (float)0/255; rgb[2].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createCoolWarmColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(2);
	rgb[0].color.r = 0.000f; rgb[0].color.g = 0.000f; rgb[0].color.b = 0.800f; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.800f; rgb[1].color.g = 0.800f; rgb[1].color.b = 0.000f; rgb[1].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createHeatColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(2);
	rgb[0].color.r = 1.000f; rgb[0].color.g = 1.000f; rgb[0].color.b = 0.000f; rgb[0].value = 0.00f;
	rgb[1].color.r = 1.000f; rgb[1].color.g = 0.000f; rgb[1].color.b = 0.000f; rgb[1].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createGreyColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(2);
	rgb[0].color.r = 0.000f; rgb[0].color.g = 0.000f; rgb[0].color.b = 0.000f; rgb[0].value = 0.00f;
	rgb[1].color.r = 1.000f; rgb[1].color.g = 1.000f; rgb[1].color.b = 1.000f; rgb[1].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createPinkGreyColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(2);
	rgb[0].color.r = 1.000f; rgb[0].color.g = 0.400f; rgb[0].color.b = 0.400f; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.500f; rgb[1].color.g = 0.500f; rgb[1].color.b = 0.700f; rgb[1].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createMagmaColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.001462; rgb[0].color.g = 0.000466; rgb[0].color.b = 0.013866; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.316654; rgb[1].color.g = 0.071690; rgb[1].color.b = 0.485380; rgb[1].value = 0.25f;
	rgb[2].color.r = 0.716387; rgb[2].color.g = 0.214982; rgb[2].color.b = 0.475290; rgb[2].value = 0.50f;
	rgb[3].color.r = 0.986700; rgb[3].color.g = 0.535582; rgb[3].color.b = 0.382210; rgb[3].value = 0.75f;
	rgb[4].color.r = 0.987053; rgb[4].color.g = 0.991438; rgb[4].color.b = 0.749504; rgb[4].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createInfernoColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.001462; rgb[0].color.g = 0.000466; rgb[0].color.b = 0.013866; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.341500; rgb[1].color.g = 0.062325; rgb[1].color.b = 0.429425; rgb[1].value = 0.25f;
	rgb[2].color.r = 0.735683; rgb[2].color.g = 0.215906; rgb[2].color.b = 0.330245; rgb[2].value = 0.50f;
	rgb[3].color.r = 0.978422; rgb[3].color.g = 0.557937; rgb[3].color.b = 0.034931; rgb[3].value = 0.75f;
	rgb[4].color.r = 0.988362; rgb[4].color.g = 0.998364; rgb[4].color.b = 0.644924; rgb[4].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createPlasmaColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.050383; rgb[0].color.g = 0.029803; rgb[0].color.b = 0.527975; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.494877; rgb[1].color.g = 0.011990; rgb[1].color.b = 0.657865; rgb[1].value = 0.25f;
	rgb[2].color.r = 0.798216; rgb[2].color.g = 0.280197; rgb[2].color.b = 0.469538; rgb[2].value = 0.50f;
	rgb[3].color.r = 0.973416; rgb[3].color.g = 0.585761; rgb[3].color.b = 0.251540; rgb[3].value = 0.75f;
	rgb[4].color.r = 0.940015; rgb[4].color.g = 0.975158; rgb[4].color.b = 0.131326; rgb[4].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createViridisColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.267004; rgb[0].color.g = 0.004874; rgb[0].color.b = 0.329415; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.229739; rgb[1].color.g = 0.322361; rgb[1].color.b = 0.545706; rgb[1].value = 0.25f;
	rgb[2].color.r = 0.127568; rgb[2].color.g = 0.566949; rgb[2].color.b = 0.550556; rgb[2].value = 0.50f;
	rgb[3].color.r = 0.369214; rgb[3].color.g = 0.788888; rgb[3].color.b = 0.382914; rgb[3].value = 0.75f;
	rgb[4].color.r = 0.993248; rgb[4].color.g = 0.906157; rgb[4].color.b = 0.143936; rgb[4].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createIsoLColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(6);
	rgb[0].color.r = 0.9102; rgb[0].color.g = 0.2236; rgb[0].color.b = 0.8997; rgb[0].value = 0.0f;
	rgb[1].color.r = 0.4027; rgb[1].color.g = 0.3711; rgb[1].color.b = 1.0000; rgb[1].value = 0.2f;
	rgb[2].color.r = 0.0422; rgb[2].color.g = 0.5904; rgb[2].color.b = 0.5899; rgb[2].value = 0.4f;
	rgb[3].color.r = 0.0386; rgb[3].color.g = 0.6206; rgb[3].color.b = 0.0201; rgb[3].value = 0.6f;
	rgb[4].color.r = 0.5441; rgb[4].color.g = 0.5428; rgb[4].color.b = 0.0110; rgb[4].value = 0.8f;
	rgb[5].color.r = 1.0000; rgb[5].color.g = 0.2288; rgb[5].color.b = 0.1631; rgb[5].value = 1.0f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(6, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createIsoAZColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(32);

	rgb[0].color.r = 1.0000; rgb[0].color.g = 0.2627; rgb[0].color.b = 1.0000; rgb[0].value = 0.0f/31.0f;
	rgb[1].color.r = 0.9765; rgb[1].color.g = 0.2941; rgb[1].color.b = 1.0000; rgb[1].value = 1.0f/31.0f;
	rgb[2].color.r = 0.9373; rgb[2].color.g = 0.3255; rgb[2].color.b = 1.0000; rgb[2].value = 2.0f/31.0f;
	rgb[3].color.r = 0.8824; rgb[3].color.g = 0.3647; rgb[3].color.b = 1.0000; rgb[3].value = 3.0f/31.0f;
	rgb[4].color.r = 0.8157; rgb[4].color.g = 0.4078; rgb[4].color.b = 1.0000; rgb[4].value = 4.0f/31.0f;
	rgb[5].color.r = 0.7451; rgb[5].color.g = 0.4549; rgb[5].color.b = 1.0000; rgb[5].value = 5.0f/31.0f;
	rgb[6].color.r = 0.6471; rgb[6].color.g = 0.5137; rgb[6].color.b = 0.9961; rgb[6].value = 6.0f/31.0f;
	rgb[7].color.r = 0.4902; rgb[7].color.g = 0.5882; rgb[7].color.b = 0.9765; rgb[7].value = 7.0f/31.0f;
	rgb[8].color.r = 0.3020; rgb[8].color.g = 0.6745; rgb[8].color.b = 0.9412; rgb[8].value = 8.0f/31.0f;
	rgb[9].color.r = 0.1333; rgb[9].color.g = 0.7490; rgb[9].color.b = 0.9020; rgb[9].value = 9.0f/31.0f;
	rgb[10].color.r = 0.0235; rgb[10].color.g =	0.8000; rgb[10].color.b = 0.8510; rgb[10].value = 10.0f/31.0f;
	rgb[11].color.r = 0.0000; rgb[11].color.g =	0.8196; rgb[11].color.b = 0.7961; rgb[11].value = 11.0f/31.0f;
	rgb[12].color.r = 0.0000; rgb[12].color.g =	0.8275; rgb[12].color.b = 0.6980; rgb[12].value = 12.0f/31.0f;
	rgb[13].color.r = 0.0000; rgb[13].color.g =	0.8314; rgb[13].color.b = 0.5725; rgb[13].value = 13.0f/31.0f;
	rgb[14].color.r = 0.0000; rgb[14].color.g =	0.8353; rgb[14].color.b = 0.4353; rgb[14].value = 14.0f/31.0f;
	rgb[15].color.r = 0.0000; rgb[15].color.g =	0.8392; rgb[15].color.b = 0.3137; rgb[15].value = 15.0f/31.0f;
	rgb[16].color.r = 0.0000; rgb[16].color.g =	0.8392; rgb[16].color.b = 0.2275; rgb[16].value = 16.0f/31.0f;
	rgb[17].color.r = 0.0588; rgb[17].color.g =	0.8353; rgb[17].color.b = 0.1647; rgb[17].value = 17.0f/31.0f;
	rgb[18].color.r = 0.1961; rgb[18].color.g =	0.8196; rgb[18].color.b = 0.1059; rgb[18].value = 18.0f/31.0f;
	rgb[19].color.r = 0.3725; rgb[19].color.g =	0.7961; rgb[19].color.b = 0.0549; rgb[19].value = 19.0f/31.0f;
	rgb[20].color.r = 0.5490; rgb[20].color.g =	0.7686; rgb[20].color.b = 0.0196; rgb[20].value = 20.0f/31.0f;
	rgb[21].color.r = 0.6824; rgb[21].color.g =	0.7412; rgb[21].color.b = 0.0000; rgb[21].value = 21.0f/31.0f;
	rgb[22].color.r = 0.7647; rgb[22].color.g =	0.6941; rgb[22].color.b = 0.0039; rgb[22].value = 22.0f/31.0f;
	rgb[23].color.r = 0.9098; rgb[23].color.g =	0.5176; rgb[23].color.b = 0.0627; rgb[23].value = 23.0f/31.0f;
	rgb[24].color.r = 0.9647; rgb[24].color.g =	0.4275; rgb[24].color.b = 0.1098; rgb[24].value = 24.0f/31.0f;
	rgb[25].color.r = 0.9961; rgb[25].color.g =	0.3569; rgb[25].color.b = 0.1647; rgb[25].value = 25.0f/31.0f;
	rgb[26].color.r = 1.0000; rgb[26].color.g =	0.3255; rgb[26].color.b = 0.2275; rgb[26].value = 26.0f/31.0f;
	rgb[27].color.r = 1.0000; rgb[27].color.g =	0.3059; rgb[27].color.b = 0.3294; rgb[27].value = 27.0f/31.0f;
	rgb[28].color.r = 1.0000; rgb[28].color.g =	0.2863; rgb[28].color.b = 0.4667; rgb[28].value = 28.0f/31.0f;
	rgb[29].color.r = 1.0000; rgb[29].color.g =	0.2745; rgb[29].color.b = 0.6314; rgb[29].value = 29.0f/31.0f;
	rgb[30].color.r = 1.0000; rgb[30].color.g =	0.2667; rgb[30].color.b = 0.8235; rgb[30].value = 30.0f/31.0f;
	rgb[31].color.r = 1.0000; rgb[31].color.g = 0.2627; rgb[31].color.b = 1.0000; rgb[31].value = 31.0f/31.0f; //same as first

	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(32, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createIsoLumRainbowColorMap() //equidistance
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);//order is changed to match to other rainbows:
	rgb[4].color.r = 0.505882353; rgb[4].color.g = 0.639215686; rgb[4].color.b = 0.835294118; rgb[0].value = 0;
	rgb[3].color.r = 0.047058824; rgb[3].color.g = 0.705882353; rgb[3].color.b = 0.686274510; rgb[1].value = 0.25;
	rgb[2].color.r = 0.407843137; rgb[2].color.g = 0.698039216; rgb[2].color.b = 0.396078431; rgb[2].value = 0.5;
	rgb[1].color.r = 0.733333333; rgb[1].color.g = 0.623529412; rgb[1].color.b = 0.231372549; rgb[3].value = 0.75;
	rgb[0].color.r = 0.945098039; rgb[0].color.g = 0.509803922; rgb[0].color.b = 0.388235294; rgb[4].value = 1;
			
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createIsoLumRainbow2ColorMap() //equidistance
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(12);
	rgb[0].color.r = 0.776470588; rgb[0].color.g = 0.419607843; rgb[0].color.b = 0.317647059; rgb[0].value = 0;
	rgb[1].color.r = 0.733333333; rgb[1].color.g = 0.450980392; rgb[1].color.b = 0.250980392; rgb[1].value = 0.090909091;
	rgb[2].color.r = 0.666666667; rgb[2].color.g = 0.486274510; rgb[2].color.b = 0.203921569; rgb[2].value = 0.181818182;
	rgb[3].color.r = 0.584313725; rgb[3].color.g = 0.517647059; rgb[3].color.b = 0.192156863; rgb[3].value = 0.272727273;
	rgb[4].color.r = 0.494117647; rgb[4].color.g = 0.545098039; rgb[4].color.b = 0.219607843; rgb[4].value = 0.363636364;
	rgb[5].color.r = 0.396078431; rgb[5].color.g = 0.564705882; rgb[5].color.b = 0.282352941; rgb[5].value = 0.454545455;
	rgb[6].color.r = 0.294117647; rgb[6].color.g = 0.576470588; rgb[6].color.b = 0.360784314; rgb[6].value = 0.545454545;
	rgb[7].color.r = 0.188235294; rgb[7].color.g = 0.584313725; rgb[7].color.b = 0.450980392; rgb[7].value = 0.636363636;
	rgb[8].color.r = 0.094117647; rgb[8].color.g = 0.580392157; rgb[8].color.b = 0.537254902; rgb[8].value = 0.727272727;
	rgb[9].color.r = 0.133333333; rgb[9].color.g = 0.572549020; rgb[9].color.b = 0.611764706; rgb[9].value = 0.818181818;
	rgb[10].color.r = 0.266666667; rgb[10].color.g = 0.556862745; rgb[10].color.b = 0.662745098; rgb[10].value = 0.909090909;
	rgb[11].color.r = 0.403921569; rgb[11].color.g = 0.529411765; rgb[11].color.b = 0.682352941; rgb[11].value = 1;

	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(12, rgb);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createIsoLumRainbow3ColorMap()
{
	//iso lum, but not necessarily equidistant in the HSL color space
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	unsigned int nr_colors = 9;
	rgb.resize(nr_colors);

	rgb[0].color.r = 190.0f/255.0f; rgb[0].color.g = 126.0f/255.0f; rgb[0].color.b = 255.0f/255.0f; rgb[0].value = 0.0/(nr_colors-1);
	rgb[1].color.r = 121.0f/255.0f; rgb[1].color.g = 173.0f/255.0f; rgb[1].color.b = 255.0f/255.0f; rgb[1].value = 1.0/(nr_colors-1);
	rgb[2].color.r =   0.0f/255.0f; rgb[2].color.g = 200.0f/255.0f; rgb[2].color.b = 202.0f/255.0f; rgb[2].value = 2.0/(nr_colors-1);
	rgb[3].color.r =   0.0f/255.0f; rgb[3].color.g = 204.0f/255.0f; rgb[3].color.b = 106.0f/255.0f; rgb[3].value = 3.0/(nr_colors-1);
	rgb[4].color.r =   0.0f/255.0f; rgb[4].color.g = 208.0f/255.0f; rgb[4].color.b =   0.0f/255.0f; rgb[4].value = 4.0/(nr_colors-1);
	rgb[5].color.r = 114.0f/255.0f; rgb[5].color.g = 198.0f/255.0f; rgb[5].color.b =   0.0f/255.0f; rgb[5].value = 5.0/(nr_colors-1);
	rgb[6].color.r = 178.0f/255.0f; rgb[6].color.g = 184.0f/255.0f; rgb[6].color.b =   0.0f/255.0f; rgb[6].value = 6.0/(nr_colors-1);
	rgb[7].color.r = 255.0f/255.0f; rgb[7].color.g = 150.0f/255.0f; rgb[7].color.b =   0.0f/255.0f; rgb[7].value = 7.0/(nr_colors-1);
	rgb[8].color.r = 255.0f/255.0f; rgb[8].color.g =  94.0f/255.0f; rgb[8].color.b =  54.0f/255.0f; rgb[8].value = 8.0/(nr_colors-1);


	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(nr_colors, rgb);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createIsoLumViridisColorMap()//equidistance
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);

	rgb[0].color.r = 0.827450980; rgb[0].color.g = 0.596078431; rgb[0].color.b = 0.807843137; rgb[0].value = 0;
	rgb[1].color.r = 0.474509804; rgb[1].color.g = 0.694117647; rgb[1].color.b = 0.866666667; rgb[1].value = 0.25;
	rgb[2].color.r = 0.164705882; rgb[2].color.g = 0.745098039; rgb[2].color.b = 0.717647059; rgb[2].value = 0.5;
	rgb[3].color.r = 0.407843137; rgb[3].color.g = 0.741176471; rgb[3].color.b = 0.462745098; rgb[3].value = 0.75;
	rgb[4].color.r = 0.694117647; rgb[4].color.g = 0.690196078; rgb[4].color.b = 0.278431373; rgb[4].value = 1;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createIsoLumDivergent1ColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(10);
	rgb[0].color.r = 0.431372549; rgb[0].color.g = 0.505882353; rgb[0].color.b = 0.929411765; rgb[0].value = 0;
	rgb[1].color.r = 0.498039216; rgb[1].color.g = 0.509803922; rgb[1].color.b = 0.815686275; rgb[1].value = 0.111111111;
	rgb[2].color.r = 0.541176471; rgb[2].color.g = 0.517647059; rgb[2].color.b = 0.701960784; rgb[2].value = 0.222222222;
	rgb[3].color.r = 0.564705882; rgb[3].color.g = 0.525490196; rgb[3].color.b = 0.588235294; rgb[3].value = 0.333333333;
	rgb[4].color.r = 0.580392157; rgb[4].color.g = 0.529411765; rgb[4].color.b = 0.474509804; rgb[4].value = 0.444444444;
	rgb[5].color.r = 0.596078431; rgb[5].color.g = 0.529411765; rgb[5].color.b = 0.454901961; rgb[5].value = 0.555555556;
	rgb[6].color.r = 0.705882353; rgb[6].color.g = 0.486274510; rgb[6].color.b = 0.392156863; rgb[6].value = 0.666666667;
	rgb[7].color.r = 0.807843137; rgb[7].color.g = 0.435294118; rgb[7].color.b = 0.329411765; rgb[7].value = 0.777777778;
	rgb[8].color.r = 0.894117647; rgb[8].color.g = 0.372549020; rgb[8].color.b = 0.266666667; rgb[8].value = 0.888888889;
	rgb[9].color.r = 0.980392157; rgb[9].color.g = 0.282352941; rgb[9].color.b = 0.200000000; rgb[9].value = 1;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(10, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createIsoLumCircularColorMap()
{
	// http://tristen.ca/hcl-picker/#/hcl/6/0.9/EC7968/EA7C5D
	// #EC7968,#C985BA,#47A6C7,#3AAE7D,#A59E37,#EA7C5D

	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 
	
	rgb.resize(6);
	rgb[0].color.r = 236.0f/255.0f; rgb[0].color.g = 121.0f/255.0f; rgb[0].color.b = 104.0f/255.0f; rgb[0].value = 0.0f/5.0f;
	rgb[1].color.r = 201.0f/255.0f; rgb[1].color.g = 133.0f/255.0f; rgb[1].color.b = 186.0f/255.0f; rgb[1].value = 1.0f/5.0f;
	rgb[2].color.r =  71.0f/255.0f; rgb[2].color.g = 166.0f/255.0f; rgb[2].color.b = 199.0f/255.0f; rgb[2].value = 2.0f/5.0f;
	rgb[3].color.r =  58.0f/255.0f; rgb[3].color.g = 174.0f/255.0f; rgb[3].color.b = 125.0f/255.0f; rgb[3].value = 3.0f/5.0f;
	rgb[4].color.r = 165.0f/255.0f; rgb[4].color.g = 158.0f/255.0f; rgb[4].color.b =  55.0f/255.0f; rgb[4].value = 4.0f/5.0f;
	rgb[5].color.r = 236.0f/255.0f; rgb[5].color.g = 121.0f/255.0f; rgb[5].color.b = 104.0f/255.0f; rgb[5].value = 5.0f/5.0f;

	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createIsoLightViridisColorMap() //lightness = 50%
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.792156863; rgb[0].color.g = 0.011764706; rgb[0].color.b = 0.988235294; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.301960784; rgb[1].color.g = 0.411764706; rgb[1].color.b = 0.701960784; rgb[1].value = 0.25f;
	rgb[2].color.r = 0.184313725; rgb[2].color.g = 0.815686275; rgb[2].color.b = 0.784313725; rgb[2].value = 0.50f;
	rgb[3].color.r = 0.184313725; rgb[3].color.g = 0.815686275; rgb[3].color.b = 0.207843137; rgb[3].value = 0.75f;
	rgb[4].color.r = 0.988235294; rgb[4].color.g = 0.890196078; rgb[4].color.b = 0.011764706; rgb[4].value = 1.00f;

	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createIsoLightDarkViridisColorMap() //lightness = 20%
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(5);
	rgb[0].color.r = 0.317647059; rgb[0].color.g = 0.003921569; rgb[0].color.b = 0.396078431; rgb[0].value = 0.00f;
	rgb[1].color.r = 0.121568627; rgb[1].color.g = 0.164705882; rgb[1].color.b = 0.278431373; rgb[1].value = 0.25f;
	rgb[2].color.r = 0.074509804; rgb[2].color.g = 0.325490196; rgb[2].color.b = 0.31372549;  rgb[2].value = 0.50f;
	rgb[3].color.r = 0.101960784; rgb[3].color.g = 0.301960784; rgb[3].color.b = 0.105882353; rgb[3].value = 0.75f;
	rgb[4].color.r = 0.396078431; rgb[4].color.g = 0.356862745; rgb[4].color.b = 0.003921569; rgb[4].value = 1.00f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(5, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createBasicCircularColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(7);
	rgb[0].color.r = 1.0; rgb[0].color.g = 0.0; rgb[0].color.b = 0.0; rgb[0].value = 0.0f/6.0f;
	rgb[1].color.r = 1.0; rgb[1].color.g = 1.0; rgb[1].color.b = 0.0; rgb[1].value = 1.0f/6.0f;
	rgb[2].color.r = 0.0; rgb[2].color.g = 1.0; rgb[2].color.b = 0.0; rgb[2].value = 2.0f/6.0f;
	rgb[3].color.r = 0.0; rgb[3].color.g = 1.0; rgb[3].color.b = 1.0; rgb[3].value = 3.0f/6.0f;
	rgb[4].color.r = 0.0; rgb[4].color.g = 0.0; rgb[4].color.b = 1.0; rgb[4].value = 4.0f/6.0f;
	rgb[5].color.r = 1.0; rgb[5].color.g = 0.0; rgb[5].color.b = 1.0; rgb[5].value = 5.0f/6.0f;
	rgb[6].color.r = 1.0; rgb[6].color.g = 0.0; rgb[6].color.b = 0.0; rgb[6].value = 6.0f/6.0f;
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(7, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createBlueRedColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(2);
	rgb[0].color.r = 0.0; rgb[0].color.g = 0.0; rgb[0].color.b = 1.0; rgb[0].value = 0.0f;
	rgb[1].color.r = 1.0; rgb[1].color.g = 0.0; rgb[1].color.b = 0.0; rgb[1].value = 1.0f;

	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(2, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createWhiteRedColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(2);
	rgb[0].color.r = 1.0; rgb[0].color.g = 1.0; rgb[0].color.b = 1.0; rgb[0].value = 0.0f;
	rgb[1].color.r = 1.0; rgb[1].color.g = 0.0; rgb[1].color.b = 0.0; rgb[1].value = 1.0f;

	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(2, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}

qfeColorMap qfeColorMaps::createGreenWhiteColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	unsigned int nr_colors = 3;
	rgb.resize(nr_colors);

	rgb[0].color.r =   0.0f/255.0f; rgb[0].color.g = 170.0f/255.0f; rgb[0].color.b =   0.0f/255.0f; rgb[0].value = 0.0/(nr_colors-1);
	rgb[1].color.r =   0.0f/255.0f; rgb[1].color.g = 255.0f/255.0f; rgb[1].color.b =   0.0f/255.0f; rgb[1].value = 1.0/(nr_colors-1);
	rgb[2].color.r = 255.0f/255.0f; rgb[2].color.g = 255.0f/255.0f; rgb[2].color.b = 255.0f/255.0f; rgb[2].value = 2.0/(nr_colors-1);

	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(nr_colors, rgb);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}


//European Broadcasting Union (EBU) color bar test pattern.
qfeColorMap qfeColorMaps::createEBUColorMap()
{
	qfeColorMap colorMap;

	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	rgb.resize(8);
	rgb[0].color.r = 0.749; rgb[0].color.g = 0.749; rgb[0].color.b = 0.749; rgb[0].value = 0.0f/7.0f;
	rgb[1].color.r = 0.749; rgb[1].color.g = 0.749; rgb[1].color.b = 0.000; rgb[1].value = 1.0f/7.0f;
	rgb[2].color.r = 0.000; rgb[2].color.g = 0.749; rgb[2].color.b = 0.749; rgb[2].value = 2.0f/7.0f;
	rgb[3].color.r = 0.000; rgb[3].color.g = 0.749; rgb[3].color.b = 0.000; rgb[3].value = 3.0f/7.0f;
	rgb[4].color.r = 0.749; rgb[4].color.g = 0.000; rgb[4].color.b = 0.749; rgb[4].value = 4.0f/7.0f;
	rgb[5].color.r = 0.749; rgb[5].color.g = 0.000; rgb[5].color.b = 0.000; rgb[5].value = 5.0f/7.0f;
	rgb[6].color.r = 0.000; rgb[6].color.g = 0.000; rgb[6].color.b = 0.749; rgb[6].value = 6.0f/7.0f;
	rgb[7].color.r = 0.000; rgb[7].color.g = 0.000; rgb[7].color.b = 0.000; rgb[7].value = 7.0f/7.0f;

	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	colorMap.qfeSetColorMapRGB(8, rgb);
	colorMap.qfeSetColorMapA(2, o);
	colorMap.qfeSetColorMapG(2, o);
	colorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	colorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	return colorMap;
}