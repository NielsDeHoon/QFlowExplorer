#pragma once

#include "qflowexplorer.h"

#include <iostream>

#include "qfeVolume.h"

#define QFE_VOL_VECTORS 1

/**
* \file   qfeVolumeVector.h
* \author Roy van Pelt
* \class  qfeVolumeVector
* \brief  Implements a 3D raster of voxels.
* \note   Confidential
*
* A volume is a 3D raster of voxels. A volume consists of
* a number of planes or slices; each consisting of a number of rows
* each consisting of a number of voxels. A voxel is a scalar value
* representing a single sample of an image volume.
*/
class qfeVolumeVector : public qfeVolume
{
public:
  qfeVolumeVector();
  qfeVolumeVector(qfeGrid *grid, qfeValueType t, unsigned int ra,
            unsigned int nx, unsigned int ny, unsigned int nz, unsigned int nc,
            void *v, qfeFloat vl, qfeFloat vu);
  ~qfeVolumeVector();

  qfeReturnStatus qfeSetVolumeComponentsPerVoxel(unsigned int nc);
  qfeReturnStatus qfeGetVolumeComponentsPerVoxel(unsigned int &nc);

  qfeReturnStatus qfeUploadVolume();

protected:

  unsigned int numberComponents;

  int qfeGetNextPowerOfTwo(int n);

};
