#pragma once

#include "qflowexplorer.h"

#include <vector>

#include "core/qfeConvertColor.h"
#include "qfeColorMap.h"

/**
* \file   qfeColorMaps.h
* \author Niels de Hoon
* \class  qfeColorMaps
* \brief  Implements a set of color maps
* \note   Confidential
*
* A color map defines a mapping of scalar values to colors.
* A color map contains a number of color mappings. (RGB, HLS)
*/
using namespace std;

class qfeColorMaps
{
public:
	qfeColorMaps() //constructor	
	{
		createColorMaps();

		transferFunctionColorMapChanged = true;
	};
	
	~qfeColorMaps()  //destructor
	{
		removeColorMaps();
	};

	void qfeGetColorMap(const unsigned int id, std::string &name, qfeColorMap &colorMap);
	std::vector<std::string> qfeGetAvailableColorMaps();

	void qfeUpdateTransferFunctionColorMap(qfeColorMap colorMap);
	bool qfeIsTransferFunctionColorMapChanged();
	qfeColorMap qfeGetTransferFunctionColorMap();

	unsigned int qfeGetIDIncreasingColorScheme();
	unsigned int qfeGetIDDivergingColorScheme();
private:
	std::vector<std::pair<std::string, qfeColorMap>> colormaps;

	static bool transferFunctionColorMapChanged;

	static qfeColorMap transferFunctionColorMap;

	void createColorMaps();
	void removeColorMaps();

	std::vector<std::string> colorMapsAvailable;

	qfeColorMap createBlueTurquoiseWhiteColorMap();
	qfeColorMap createBrownOrangeYellowColorMap();
	qfeColorMap createBlackBodyColorMap();
	qfeColorMap createRainbowColorMap();
	qfeColorMap createJetColorMap();
	qfeColorMap createParulaColorMap();
	qfeColorMap createDoppler1ColorMap();
	qfeColorMap createDoppler2ColorMap();	
	qfeColorMap createInvertedColdAndHotColorMap();
	qfeColorMap createColdAndHotColorMap();
	qfeColorMap createDiverging1ColorMap();
	qfeColorMap createDiverging2ColorMap();
	qfeColorMap createDiverging3ColorMap();	
	qfeColorMap createDiverging4ColorMap();
	qfeColorMap createDiverging5ColorMap();
	qfeColorMap createCoolWarmColorMap();
	qfeColorMap createHeatColorMap();
	qfeColorMap createGreyColorMap();
	qfeColorMap createPinkGreyColorMap();
	qfeColorMap createMagmaColorMap();
	qfeColorMap createInfernoColorMap();
	qfeColorMap createPlasmaColorMap();
	qfeColorMap createViridisColorMap();
	qfeColorMap createIsoLColorMap();
	qfeColorMap createIsoAZColorMap();	
	qfeColorMap createIsoLumRainbowColorMap();
	qfeColorMap createIsoLumRainbow2ColorMap();
	qfeColorMap createIsoLumRainbow3ColorMap();
	qfeColorMap createIsoLumViridisColorMap();
	qfeColorMap createIsoLumDivergent1ColorMap();
	qfeColorMap createIsoLumCircularColorMap();
	qfeColorMap createIsoLightViridisColorMap();
	qfeColorMap createIsoLightDarkViridisColorMap();
	qfeColorMap createBasicCircularColorMap();
	qfeColorMap createBlueRedColorMap();
	qfeColorMap createWhiteRedColorMap();
	qfeColorMap createGreenWhiteColorMap();
	qfeColorMap createEBUColorMap();
};