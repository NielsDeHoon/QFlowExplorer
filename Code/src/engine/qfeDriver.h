#pragma once

#define qfeMax(A,B) ((A)>(B) ? (A) : (B))

#include "qflowexplorer.h"

#include <gl/glew.h>

#include <vtkCallbackCommand.h>

#include "qfeDisplay.h"
#include "qfeDriverAide.h"
#include "qfeFrame.h"
#include "qfeGeometry.h"
#include "qfeGLShaderProgram.h"
#include "qfeGLShaderSourceReader.h"
#include "qfeGLSupport.h"
#include "qfeScene.h"
#include "qfeTransform.h"
#include "qfeVector.h"
#include "qfeViewport.h"
#include "qfeVisual.h"
#include "qfeVolume.h"
#include "qfeVtkEngine.h"
#include "qfePostProcessing.h"

enum qfeDriverBuffer
{
  qfeDriverBufferRender,
  qfeDriverBufferSelect
};

struct qfeDriverFrameBufferProps
{
  GLuint texColor;
  GLuint texSelect;
  GLuint texDepth;
  GLint  supersampling;
};

/**
* \file   qfeDriver.h
* \author Roy van Pelt
* \class  qfeDriver
* \brief  Abstract class for visualization drivers
*
* qfeDriver provides an abstract class for different
* visualization drivers. A driver obtains a scene graph,
* which may be rendered by a specific driver.
* Rendering will be stored in an offscreen buffer
*
* \todo Read pixels for display (BGRA format!)
* \todo Implement perspective projection
* \todo Find nice solution for updateWindowSizeFlag
*/

class qfeDriver
{
public:
  qfeDriver(qfeScene *scene);
  virtual ~qfeDriver();

  virtual bool qfeRenderCheck()     = 0;
  virtual void qfeRenderSelect()    = 0;
  virtual void qfeRenderStart()     = 0;
  virtual void qfeRenderWait()      = 0;  
  virtual void qfeRenderStop()      = 0;

  virtual void qfeRenderWait1();
  virtual void qfeRenderWait2();
  virtual void qfeRenderWait3();
  
  virtual void qfeRenderRotate(double angle, bool autoupdate);
  virtual void qfeRenderRotate360(double angleStep, bool autoupdate, bool screenshot);

  void   qfeResetFBO();
  void   qfeResetFBO(int index);

  void   qfeSetSelectColor(qfeColorRGB color); 

  void qfeGetCameraSettings(qfePoint &position, qfePoint &focalPoint, qfeVector &viewUp);
  void qfeSetCameraSettings(qfePoint position, qfePoint focalPoint, qfeVector viewUp);

  qfeVtkEngine * qfeGetEngine()
  {
	  return engine;
  }

protected:
  // VTK variables
  qfeVtkEngine             *engine;
  qfeGLShaderProgram       *shaderProgramSelectColor;

  // QFE variables
  qfeScene                 *scene; 

  qfeMatrix4f               currentModelView;
  qfeMatrix4f               currentProjection;
  
  vector<qfeVisual *>       visuals;

  // OpenGL structures
  GLuint                    frameBuffer;
  qfeViewport              *frameBufferViewport[4];
  qfeFrame                 *frameBufferGeo[4];  
  qfeDriverFrameBufferProps frameBufferProps[4];  

  
  qfeVisualPostProcess			  *visPostProcess;
  qfePostProcessing				  *postProcessor;
 
  virtual void qfeRenderBackground();

  static  void qfeDriverRender(void *caller);
  static  void qfeDriverRender1(void *caller);
  static  void qfeDriverRender2(void *caller);
  static  void qfeDriverRender3(void *caller);
  static  void qfeDriverResize(void *caller);  

  void qfeSetProjection(qfeFrame *geo, qfeViewport *vp, int supersampling);
  void qfeResetProjection();

  void qfeReadDepth(int x, int y, float &depth);
  void qfeReadColor(int x, int y, qfeColorRGB &color);
  void qfeReadSelect(int x, int y, qfeColorRGB &color);
  void qfeGetViewport(int x, int y, int &vpid);
    
  qfeReturnStatus qfeGenerateFBO(GLuint &fbo);  
  qfeReturnStatus qfeDeleteFBO(GLuint &fbo);

  void qfeEnableFBO(GLuint fbo);
  void qfeDisableFBO();
  
  // TODO: support attach without select buffer and single depth buffer
  void qfeAttachFBO(GLuint fbo, GLuint color, GLuint select, GLuint depth);
  void qfeDetachFBO(GLuint fbo);

  void qfeGenerateTextures(GLuint width, GLuint height, GLuint &color, GLuint &select, GLuint &depth);  
  void qfeDeleteTextures(GLuint &color, GLuint &select, GLuint &depth);  
  void qfeDeleteTexture(GLuint &texture);  

  void qfeRenderTextureToScreen(GLuint texture, qfeViewport *vp);

  void qfeUpdate();
  void qfeUpdateWindowSize();
  void qfeUpdateViewGeometry();

  void qfeUpdateCamera(vtkRenderer *renderer, qfeGeometry *geometry);
  void qfeUpdateGeometry(vtkRenderer *renderer, qfeGeometry *geometry);
  void qfeUpdateLight(vtkRenderer *renderer, qfeVector direction); 

  // Time counter
  //void   qfeStartCounter();
  //double qfeGetCounter();

private :
  bool                updateWindowSizeFlag;
  unsigned int        windowsize[2];
  unsigned int        viewportsize[2];   
};