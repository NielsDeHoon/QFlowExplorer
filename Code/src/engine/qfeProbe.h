#pragma once

#include "qflowexplorer.h"

#include "qfeMatrix4f.h"
#include "qfePoint.h"
#include "qfeVector.h"
#include "qfeFrame.h"

enum qfeProbeObjects
{
  qfeObjectProbe,
  qfeObjectProbeHandle,
  qfeObjectPlaneView,
  qfeObjectProbeOrtho,
  qfeObjectPlaneOrtho,
  qfeObjectProbeHandleBaseRadius,
  qfeObjectProbeHandleTopRadius
};

enum qfeProbeInteractionState
{
  probeInteractNone,
  probeInteractTranslate,
  probeInteractRotate,
  probeInteractUpdate,  
  probeInteractAnimate
};

enum qfePlaneInteractionState
{
  planeInteractRotate,
  planeInteractFixed,  
};

enum qfeProbeActionState
{
  probeDefault,
  probeClick,
  probeStroke
};

enum qfeProbeSelectState
{
  probeSelectStart,
  probeSelectFirst,
  probeSelectSecond,
  probeSelectThird,
  probeSelectAbort
};

enum qfeProbeSeedingState
{
  probeSeedingNone,
  probeSeedingInjected,
  probeSeedingUpdate  
};

struct qfeProbeQuantificationItem
{
  std::string         label;
  std::string         labelX;
  std::string         labelY;
  float               rangeX[2];
  float               rangeY[2];
  float               stepsX;
  float               stepsY;
  std::vector<float>  values;
};

/**
* \file   qfeProbe.h
* \author Roy van Pelt
* \class  qfeProbe
* \brief  Abstract probe to explore the data
* \note   Confidential
*
* A virtual probe allows to explore the data.
*/
//----------------------------------------------------------------------------
class qfeProbe
{
public:
  qfeProbe();
  qfeProbe(const qfeProbe &probe);
  ~qfeProbe();  

protected:
  
};

/**
* \file   qfeProbe.h
* \author Roy van Pelt
* \class  qfeProbe2D
* \brief  Implements a 2D probe to explore the data
* \note   Confidential
*
* A 2D probe to explore the data
*/
//----------------------------------------------------------------------------
class qfeProbe2D : public qfeProbe
{
public:
  qfeProbe2D();
  ~qfeProbe2D();

  // flags
  bool selected;
  bool visible;

  // orientation
  qfePoint    origin;
  qfeVector   normal;
  qfeVector   axisX;
  qfeVector   axisY;
  qfeFloat    radius;
  qfeFloat    area;
  qfeColorRGB color;

  // points
  vector<qfePoint> points;

  // quantification
  vector<qfeProbeQuantificationItem> quantification;

};

/**
* \file   qfeProbe.h
* \author Roy van Pelt
* \class  qfeProbe3D
* \brief  Implements a 3D probe to explore the data
* \note   Confidential
*
* A 3D probe to explore the data
*/
//----------------------------------------------------------------------------
class qfeProbe3D : public qfeProbe
{
public:
  qfeProbe3D();
  ~qfeProbe3D();

  void qfeProbeRoll(qfeFloat angle);
  void qfeProbeAzimuth(qfeFloat angle);
  void qfeProbePitch(qfeFloat angle);
  void qfeProbeRotate(qfeVector axis, qfeFloat angle);
  
  // Flags
  bool                     selected;
  bool                     visible;

  // Orientation
  qfePoint                 origin;
  qfeFloat                 length;  
  qfeFloat                 topRadius;
  qfeFloat                 baseRadius;  
  int                      numSlices;
  int                      numStacks;
  qfeVector                axes[3];

  // Appearance
  qfeColorRGBA             color;
  qfeFloat                 borderWidth;  

  // Interactions
  qfeProbeInteractionState interactionState;

  // Additions
  qfeFrameSlice            parallelPlane;
  qfeFrameSlice            orthoPlane;
  qfeFloat                 orthoPlaneOffset;    
};