#pragma once

#include "qflowexplorer.h"
#include "qfeVolume.h"

/**
* \file   qfeHistogram.h
* \author Arjan Broos
* \class  qfeHistogram
* \brief  Implements a histogram
* \note   Confidential
*
* Used to calculate a histogram for a given volume
* with a given number of bins. Only supports float,
* short and unsigned short data types. Only supports
* 1 or 3 components per voxel.
*/
class qfeHistogram {
public:
  qfeHistogram();
  qfeHistogram(const qfeHistogram &h);
  ~qfeHistogram();

  // Set volume to calculate histogram over
  void qfeSetVolume(qfeVolume *volume);
  // Set the number of bins to count values in
  void qfeSetNrOfBins(unsigned nrBins);
  // Get the number of bins
  unsigned qfeGetNrOfBins() const;
  // Set the range of the histogram
  void qfeSetRange(float rangeMin, float rangeMax);
  // Get the range of the histogram
  void qfeGetRange(float &rangeMin, float &rangeMax) const;
  // Returns the bin index for a given value
  unsigned qfeGetBin(float value) const;
  // Returns the count for a given bin
  unsigned qfeGetCount(unsigned bin) const;
  // Returns the maximum count of all bins
  unsigned qfeGetMaxCount() const;

  // Calculates the histogram
  void qfeUpdate();

  // Get the calculated histogram
  const unsigned *qfeGetHistogram();

private:
  // Retrieve float value for given voxel position
  // Only supports float, short and unsigned short data types
  // Only supports 1 or 3 components per voxel
  // Returns 0.f for unsupported things
  float qfeGetValue(unsigned index) const;
  // Returns the value at the given index
  // Does not take number of components into account
  float qfeGetDirectValue(unsigned index) const;
  // Resizes the histogram data if necessary
  void qfeResizeHistogram();

  // Volume to calculate histogram over
  qfeVolume *volume;
  unsigned volWidth, volHeight, volDepth; // Volume dimensions
  void *volData; // Data stored in volume
  qfeValueType volType; // Data type stored in volume
  unsigned volNrComp; // Number of components per voxel in volume

  // Count of values for every bin
  unsigned *histogram;
  // Number of bins for the histogram
  unsigned nrBins;
  // Minimum and maximum values found in volume
  float rangeMin, rangeMax;
  // True if the previously calculated histogram is still up-to-date
  bool upToDate;
  // True if number of bins changed
  bool nrBinsChanged;
  // The maximum number of values assigned to a bin
  unsigned maxCount;
};