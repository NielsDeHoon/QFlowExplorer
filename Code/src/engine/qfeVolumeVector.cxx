#include "qfeVolumeVector.h"

//----------------------------------------------------------------------------
qfeVolumeVector::qfeVolumeVector() : qfeVolume()
{
  this->volumeType = QFE_VOL_VECTORS;
  this->numberComponents = 1;
};

//----------------------------------------------------------------------------
qfeVolumeVector::qfeVolumeVector(qfeGrid *grid, qfeValueType t, unsigned int ra,
                     unsigned int nx, unsigned int ny, unsigned int nz, unsigned int nc,
                     void *v, qfeFloat vl, qfeFloat vu)
                     : qfeVolume(grid, t, ra, nx, ny, nz, v, vl, vu)

{
  qfeVolumeVector();
  this->numberComponents = nc;
};

//----------------------------------------------------------------------------
qfeVolumeVector::~qfeVolumeVector()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolumeVector::qfeSetVolumeComponentsPerVoxel(unsigned int nc)
{
  this->numberComponents = nc;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolumeVector::qfeGetVolumeComponentsPerVoxel(unsigned int &nc)
{
  nc = this->numberComponents;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolumeVector::qfeUploadVolume()
{
  bool resized = false;
  int  resized_dims[3];  

  unsigned char  *data8  = NULL;
  unsigned short *data16 = NULL;
  float          *data32 = NULL;

  // Check if data should be resized to a power-of-two. Leave Z-dimension untouched. 
  //resized_dims[0] = this->GetNextPowerOfTwo(this->numberVoxelsX);
  //resized_dims[1] = this->GetNextPowerOfTwo(this->numberVoxelsY);
  resized_dims[0] = this->numberVoxelsX;
  resized_dims[1] = this->numberVoxelsY;
  resized_dims[2] = this->numberVoxelsZ;

  resized = (resized_dims[0] != this->numberVoxelsX || 
    resized_dims[1] != this->numberVoxelsY || 
    resized_dims[2] != this->numberVoxelsZ) ? true : false;

  unsigned int voxtype  = 0;

  // Store voxel data type
  switch(this->valueType)
  {
  case qfeValueTypeUnsignedInt8  : voxtype = GL_UNSIGNED_BYTE;
    break;
  case qfeValueTypeGrey          : voxtype = GL_UNSIGNED_BYTE;
    break;
  case qfeValueTypeUnsignedInt16 : voxtype = GL_UNSIGNED_SHORT;
    break;
  case qfeValueTypeFloat         : voxtype = GL_FLOAT;
    break;
  default: cout << "qfeVolumeVector::qfeUploadVolume - Unknown data type" << endl;
    return qfeError;
  }

  // Resize data if necessary (padded with zeros)
  if(resized)
  {
    switch(voxtype)
    {
    case GL_UNSIGNED_BYTE :
      data8 = (unsigned char *) calloc(resized_dims[0] * resized_dims[1] * resized_dims[2] * 4, sizeof(unsigned char));
      memcpy(data8, this->voxels, sizeof(unsigned char) * this->numberVoxelsX * this->numberVoxelsY * this->numberVoxelsZ * 4);
      break;
    case GL_UNSIGNED_SHORT :
      data16 = (unsigned short *) calloc(resized_dims[0] * resized_dims[1] * resized_dims[2] * 4, sizeof(unsigned short));
      memcpy(data16, this->voxels, sizeof(unsigned short) * this->numberVoxelsX * this->numberVoxelsY * this->numberVoxelsZ * 4);
      break;
    case GL_FLOAT :
      data32 = (float *) calloc(resized_dims[0] * resized_dims[1] * resized_dims[2] * 4, sizeof(float));
      memcpy(data32, this->voxels, sizeof(float) * this->numberVoxelsX * this->numberVoxelsY * this->numberVoxelsZ * 4);
      break;
    default :
      cout << "qfeVolumeVector::UploadVolume - problem with voxel type" << endl;
      return qfeError;
    }
  }
  else
  {
    switch(voxtype)
    {
    case GL_UNSIGNED_BYTE :
      data8  = (unsigned char *) this->voxels;
      break;
    case GL_UNSIGNED_SHORT :
      data16 = (unsigned short *) this->voxels;
      break;
    case GL_FLOAT :
      data32 = (float *) this->voxels;
      break;
    default :
      cout << "qfeVolumeVector::UploadVolume - problem with voxel type" << endl;
      return qfeError;
    }
  }

  // Allocate texture
  glActiveTexture(GL_TEXTURE0);
  glEnable(GL_TEXTURE_3D);

  unsigned int id;
  glGenTextures(1, &id);
  glBindTexture(GL_TEXTURE_3D, id);
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

  // Upload the texture
  switch(voxtype)
  {
  case GL_UNSIGNED_BYTE :
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA8, resized_dims[0], resized_dims[1], resized_dims[2], 0, 
      GL_RGBA, GL_UNSIGNED_BYTE, data8);
    if(resized) free(data8);
    break;
  case GL_UNSIGNED_SHORT :
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA16, resized_dims[0], resized_dims[1], resized_dims[2], 0, 
      GL_RGBA, GL_UNSIGNED_SHORT, data16);
    if(resized) free(data16);
    break;
  case GL_FLOAT :
    // Note: 32 bits floats will be uploaded as 16 bits floats for memory consumption reasons
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA16F, resized_dims[0], resized_dims[1], resized_dims[2], 0, 
      GL_RGBA, GL_FLOAT, data32);
    if(resized) free(data32);
    //cout << "qfeVolumeVector::UploadVolume - 16 bits floats used instead of 32 bits" << endl;
    break;
  }

  glBindTexture(GL_TEXTURE_3D, 0);
  glDisable(GL_TEXTURE_3D);

  this->texId = id;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
int qfeVolumeVector::qfeGetNextPowerOfTwo(int n)
{
  int i = 1;
  while(i < n)
    i *= 2;
  return i;
}