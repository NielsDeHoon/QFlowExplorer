#include "qfeMeasurement.h"

//----------------------------------------------------------------------------
qfeMeasurement::qfeMeasurement()
{
  this->measurementType = -1;

};

//----------------------------------------------------------------------------
qfeMeasurement::qfeMeasurement(const qfeMeasurement &measurement)
{
  this->measurementType = measurement.measurementType;
};

//----------------------------------------------------------------------------
qfeMeasurement::~qfeMeasurement()
{

};