#include "qfeVolume.h"

//----------------------------------------------------------------------------
qfeVolume::qfeVolume(qfeGrid *grid, qfeValueType t, 
                     unsigned int nx, unsigned int ny, unsigned int nz, void *v, 
                     qfeFloat tt, qfeFloat pd, qfeFloat vl, qfeFloat vu, unsigned int nc)
{
  GLenum err = glewInit();
  if (GLEW_OK != err)
  {
    cout << "qfeVolume::qfeVolume - " << glewGetErrorString(err) << endl;
  }

  this->texId            = 0;

  this->voxels           = v;
  this->grid             = grid;
  this->numberVoxelsX    = nx;
  this->numberVoxelsY    = ny;
  this->numberVoxelsZ    = nz;  
  this->numberComponents = nc;  
  this->lowerVoxelValue  = vl;
  this->upperVoxelValue  = vu;
  this->triggertime      = tt;
  this->phaseduration    = pd;

  this->histogram        = NULL;
  this->histogramsize    = 0;
  this->label            = "";

  if(grid != NULL) this->qfeSetVolumeGrid(grid);
  if(v != NULL)    this->qfeSetVolumeData(t, nx, ny, nz, v);
  this->qfeSetVolumeValueDomain(vl, vu); 
  this->qfeSetVolumeTriggerTime(tt);
  this->qfeSetVolumePhaseDuration(pd);
  this->qfeSetVolumeComponentsPerVoxel(nc);
};


//----------------------------------------------------------------------------
qfeVolume::qfeVolume(const qfeVolume &volume)
{
  this->grid             = new qfeGrid(*volume.grid);
  this->valueType        = volume.valueType;
  this->numberVoxelsX    = volume.numberVoxelsX;
  this->numberVoxelsY    = volume.numberVoxelsY;
  this->numberVoxelsZ    = volume.numberVoxelsZ;
  this->lowerVoxelValue  = volume.lowerVoxelValue;
  this->upperVoxelValue  = volume.upperVoxelValue;
  this->numberComponents = volume.numberComponents;
  this->triggertime      = volume.triggertime;
  this->phaseduration    = volume.phaseduration;
  this->histogramsize    = volume.histogramsize;

  this->texId            = volume.texId;
  this->label            = volume.label;

  // Perform the copying of the histogram
  this->histogram        = NULL;
  this->histogram        = (int *)calloc(volume.histogramsize, sizeof(int));  
  memcpy(this->histogram, volume.histogram, volume.histogramsize*sizeof(int));

  // Perform the copying of the voxel data
  this->voxels           = NULL;
  this->qfeSetVolumeData(volume.valueType, volume.numberVoxelsX, volume.numberVoxelsY, volume.numberVoxelsZ, volume.voxels); // \todo;
};

//----------------------------------------------------------------------------
qfeVolume::~qfeVolume()
{
  if (glIsTexture(this->texId))
  {
    glDeleteTextures(1, (GLuint *)&this->texId);
    this->texId = 0;
  }

  if(this->voxels != NULL)    free(this->voxels);  
  if(this->histogram != NULL) free(this->histogram);

  this->voxels    = NULL;
  this->histogram = NULL;

  delete this->grid;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeSetVolumeGrid(qfeGrid *grid)
{
  delete this->grid;

  this->grid = new qfeGrid(*grid);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeGrid(qfeGrid **grid)
{
  *grid = this->grid;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeSetVolumeData(qfeValueType t,
                                            unsigned int nx, unsigned int ny, unsigned int nz,
                                            void *v)
{
  return qfeSetVolumeData(t, nx, ny, nz, this->numberComponents, v);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeSetVolumeData(qfeValueType t,
                                            unsigned int nx, unsigned int ny, unsigned int nz,
                                            unsigned int c, void *v)
{
  this->valueType        = t;
  this->numberVoxelsX    = nx;
  this->numberVoxelsY    = ny;
  this->numberVoxelsZ    = nz;
  this->numberComponents = c;

  if(this->voxels != NULL) free(this->voxels);
  this->voxels = NULL;

  switch(t)
  {
  case qfeValueTypeInt8  :
  case qfeValueTypeUnsignedInt8 :
  case qfeValueTypeGrey :
    this->voxels        = (unsigned char *)calloc(this->numberComponents*nx*ny*nz, sizeof(unsigned char));  
    memcpy(this->voxels, v, this->numberComponents*nx*ny*nz*sizeof(unsigned char));
    break;
  case qfeValueTypeInt16:
  case qfeValueTypeUnsignedInt16 :
    this->voxels        = (unsigned short *)calloc(this->numberComponents*nx*ny*nz, sizeof(unsigned short));  
    memcpy(this->voxels, v, this->numberComponents*nx*ny*nz*sizeof(unsigned short));
    break;
  case qfeValueTypeFloat :
    this->voxels        = (float *)calloc(this->numberComponents*nx*ny*nz, sizeof(float));  
    memcpy(this->voxels, v, this->numberComponents*nx*ny*nz*sizeof(float));
    break;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeData(qfeValueType& t, 
                                            unsigned int& nx, unsigned int& ny, unsigned int& nz,
                                            void **v)
{
  t  = this->valueType;
  nx = this->numberVoxelsX;
  ny = this->numberVoxelsY;
  nz = this->numberVoxelsZ;
  *v = this->voxels;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeValueType(qfeValueType& t)
{
  t = this->valueType;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeSize(unsigned int& nx, unsigned int& ny, unsigned int& nz)
{
  nx = this->numberVoxelsX;
  ny = this->numberVoxelsY;
  nz = this->numberVoxelsZ;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeSetVolumeValueDomain(qfeFloat vl, qfeFloat vu)
{
  this->lowerVoxelValue = vl;
  this->upperVoxelValue = vu;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeValueDomain(qfeFloat& vl, qfeFloat& vu)
{
  vl = this->lowerVoxelValue;
  vu = this->upperVoxelValue;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeSetVolumeVenc(qfeFloat _vencX, qfeFloat _vencY, qfeFloat _vencZ)
{
  this->vencX = _vencX;
  this->vencY = _vencY;
  this->vencZ = _vencZ;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeVenc(qfeFloat &_vencX, qfeFloat &_vencY, qfeFloat &_vencZ)
{
  _vencX = this->vencX;
  _vencY = this->vencY;
  _vencZ = this->vencZ;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeTextureId(GLuint &tid)
{
  tid = this->texId;

  if(tid == -1) 
    cout << "qfeVolumeGPU::qfeGetVolumeTextureId - Volume not uploaded to GPU" << endl;

  return qfeSuccess;
};

qfeReturnStatus qfeVolume::qfeSetVolumeTextureId(GLuint tid)
{
  texId = tid;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeType(int &t)
{
  if(this->numberComponents == 1)
    t = QFE_VOL_SCALARS;
  else
    t = QFE_VOL_VECTORS;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeSetVolumeComponentsPerVoxel(unsigned int nc)
{
  this->numberComponents = nc;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeComponentsPerVoxel(unsigned int &nc)
{
  nc = this->numberComponents;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeSetVolumeTriggerTime(qfeFloat tt)
{
  this->triggertime = tt;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeTriggerTime(qfeFloat &tt)
{
  tt = this->triggertime;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeSetVolumePhaseDuration(qfeFloat pd)
{
  this->phaseduration = pd;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumePhaseDuration(qfeFloat &pd)
{
  pd = this->phaseduration;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeSetVolumeLabel(string label)
{
  this->label = label;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeLabel(string& label)
{
  label = this->label;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeUpdateVolume()
{
  if (glIsTexture(this->texId))
  {
    glDeleteTextures(1, (GLuint *)&this->texId);
    this->texId = 0;
  }

  this->qfeUploadVolume();

  return qfeSuccess;
}

void qfeVolume::qfeSetHistogramToNull()
{
  this->histogram = nullptr;
  this->histogramsize = 0;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeUploadVolume()
{
  void           *data;
  unsigned int    voxtype  = 0;
  short          *shortVoxels = nullptr;
  unsigned short *uShortVoxels = nullptr;
  unsigned size = numberVoxelsX*numberVoxelsY*numberVoxelsZ*numberComponents;

  if (numberVoxelsX == 0 && numberVoxelsY == 0 && numberVoxelsZ == 0)
    return qfeError;

  switch (valueType) {
  case qfeValueTypeInt8 :
    voxtype = GL_BYTE;
    data = voxels;
    break;
  case qfeValueTypeUnsignedInt8 :  
  case qfeValueTypeGrey :
    voxtype = GL_UNSIGNED_BYTE;
    data    = voxels;
    break;
  case qfeValueTypeInt16:
    // Somehow, when uploading perfectly valid short voxels, nothing seems to get uploaded
    // So now we convert them to floats
    shortVoxels = (short*)voxels;
    data = new float[size];
    for (unsigned i = 0; i < size; i++)
      ((float*)data)[i] = shortVoxels[i];
    voxtype = GL_FLOAT;
    break;
  case qfeValueTypeUnsignedInt16 :
    // Somehow, when uploading perfectly valid short voxels, nothing seems to get uploaded
    // So now we convert them to floats
    uShortVoxels = (unsigned short*)voxels;
    data = new float[size];
    for (unsigned i = 0; i < size; i++)
      ((float*)data)[i] = uShortVoxels[i];
    voxtype = GL_FLOAT;
    break;
  case qfeValueTypeFloat :
    voxtype = GL_FLOAT;
    data    = voxels;
    break;
  default :
    cout << "qfeVolume::UploadVolume - problem with voxel type" << endl;
    return qfeError;
  }

  // Allocate texture
  glActiveTexture(GL_TEXTURE0);
  glEnable(GL_TEXTURE_3D);

  unsigned int id;
  glGenTextures(1, &id);
  glBindTexture(GL_TEXTURE_3D, id);
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

  // Upload the texture
  // Note: 32 bits floats will be uploaded as 16 bits floats for memory consumption reasons
  switch(numberComponents) {
  case 1 :
    glTexImage3D(GL_TEXTURE_3D, 0, GL_R16F, numberVoxelsX, numberVoxelsY, numberVoxelsZ, 0, GL_RED, voxtype, data);
    break;
  case 2 :
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RG16F, numberVoxelsX, numberVoxelsY, numberVoxelsZ, 0, GL_RG, voxtype, data);
    break;
  case 3 :    
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB16F, numberVoxelsX, numberVoxelsY, numberVoxelsZ, 0, GL_RGB, voxtype, data);
    break;
  case 4 :
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA16F, numberVoxelsX, numberVoxelsY, numberVoxelsZ, 0, GL_RGBA, voxtype, data);
    break;
  }

  if (valueType == qfeValueTypeInt16 || valueType == qfeValueTypeInt16)
    delete[] data;

  glBindTexture(GL_TEXTURE_3D, 0);
  glDisable(GL_TEXTURE_3D);

  texId = id;

  if(glGetError() == GL_NO_ERROR)  return qfeSuccess;
  else                             return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeNorm(qfeVolume **norm)
{
  if(this->numberComponents != 3) return qfeError;

  // \todo decent range scaling here

  unsigned short *data = (unsigned short *) calloc(this->numberVoxelsX * this->numberVoxelsY * this->numberVoxelsZ, sizeof(unsigned short));

  switch(this->valueType)
  {
  case qfeValueTypeInt8:
  case qfeValueTypeUnsignedInt8 :
    for(int j=0; j<(int)(numberVoxelsX * numberVoxelsY * numberVoxelsZ); j++)
    {
      data[j] =  (unsigned short) sqrt(pow((float)((unsigned char *)voxels)[j*numberComponents+0],2.0f) + 
                                       pow((float)((unsigned char *)voxels)[j*numberComponents+1],2.0f) + 
                                       pow((float)((unsigned char *)voxels)[j*numberComponents+2],2.0f));      
    }
    break;
  case qfeValueTypeInt16:
  case qfeValueTypeUnsignedInt16 :
    for(int j=0; j<(int)(numberVoxelsX * numberVoxelsY * numberVoxelsZ); j++)
    {
      data[j] = (unsigned short) sqrt(pow((float)((unsigned short *)voxels)[j*numberComponents+0],2.0f) + 
                                      pow((float)((unsigned short *)voxels)[j*numberComponents+1],2.0f) + 
                                      pow((float)((unsigned short *)voxels)[j*numberComponents+2],2.0f));      
    }
    break;
  case qfeValueTypeFloat :
    for(int j=0; j<(int)(numberVoxelsX * numberVoxelsY * numberVoxelsZ); j++)
    {
      data[j] =  (unsigned short) (sqrt(pow((float)((float *)voxels)[j*numberComponents+0],2.0f) + 
                                        pow((float)((float *)voxels)[j*numberComponents+1],2.0f) + 
                                        pow((float)((float *)voxels)[j*numberComponents+2],2.0f)) * pow(2.0f,16.0f));      
    }
    break;
  }

  (*norm)->qfeSetVolumeData(qfeValueTypeUnsignedInt16, numberVoxelsX, numberVoxelsY, numberVoxelsZ, data);
  (*norm)->qfeSetVolumeGrid(new qfeGrid(*this->grid));
  (*norm)->qfeSetVolumeValueDomain(0, (qfeFloat)pow(2.0f,16.0f));

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Assumes granularity of 1 cm/s
qfeReturnStatus qfeVolume::qfeGetVolumeHistogram(int **histogram, int &size)
{
  if(this->voxels == NULL || this->lowerVoxelValue >= this->upperVoxelValue) 
  {
    (*histogram) = NULL;
    size = 0;
    return qfeError;
  }

  float valueX, valueY, valueZ, speed;

  if(this->histogram == NULL || this->histogramsize == 0)
  {
    this->histogramsize = upperVoxelValue - lowerVoxelValue;
    this->histogram = (int *)realloc(this->histogram, this->histogramsize * sizeof(int));
    int offset = (int)lowerVoxelValue;

    for(int i=0; i<this->histogramsize; i++)
    {
      this->histogram[i] = 0;
    }

    // SJ: Catch the special cases for the tMIP histogram calculation
    if(this->valueType == qfeValueTypeUnsignedInt16 && this->numberComponents == 1)
    {
      for(int i=0; i<(int)(this->numberVoxelsX*this->numberVoxelsY*this->numberVoxelsZ); i++)
      { 
        speed        = *((qfeUnsignedInt16*)this->voxels+i);
        (speed < 0)                 ? speed = 0.0f               : speed = speed;
        (speed > (histogramsize-1)) ? speed = histogramsize-1.0f : speed = speed;

        this->histogram[(int)speed-offset]++;
      }
    }
    else if(this->valueType == qfeValueTypeInt16 && this->numberComponents == 1)
    {
      for(int i=0; i<(int)(this->numberVoxelsX*this->numberVoxelsY*this->numberVoxelsZ); i++)
      { 
        speed        = *((qfeInt16*)this->voxels+i);
        (speed < 0)                 ? speed = 0.0f               : speed = speed;
        (speed > (histogramsize-1)) ? speed = histogramsize-1.0f : speed = speed;

        this->histogram[(int)speed-offset]++;
      }
    }
    else if(this->valueType == qfeValueTypeFloat && this->numberComponents == 1)
    {
      for(int i=0; i<(int)(numberVoxelsX*numberVoxelsY*numberVoxelsZ); i++)
      { 
        speed = *((float*)voxels+i);
        (speed < 0)                 ? speed = 0.0f               : speed = speed;
        (speed > (histogramsize-1)) ? speed = histogramsize-1.0f : speed = speed;

        this->histogram[(int)speed-offset]++;
      }
    }
    else  // Calculate the histogram the original way (that is: assuming 3 component float vectors)
    {
      for(int i=0; i<(int)(this->numberVoxelsX*this->numberVoxelsY*this->numberVoxelsZ); i++)
      {   
        float *vox = (float *)voxels;
        float voxel = vox[0];
        valueX       = *((float*)this->voxels + 3*i+0);
        valueY       = *((float*)this->voxels + 3*i+1);
        valueZ       = *((float*)this->voxels + 3*i+2);

        speed        = sqrt(pow(valueX,2) + pow(valueY,2) + pow(valueZ,2));
        (speed < 0)                 ? speed = 0.0f               : speed = speed;
        (speed > (histogramsize-1)) ? speed = histogramsize-1.0f : speed = speed;
        int index = (int)speed - offset;
        if (index >= histogramsize) index = histogramsize-1;

        this->histogram[index]++;
      }
    }
  }

  (*histogram) = this->histogram;
  size         = this->histogramsize;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeScalarAtPosition(int px, int py, int pz, float &v)
{ 
  v = 0.0f;

  if(this->numberComponents != 1 || 
     px < 0 || px >= (int)this->numberVoxelsX || 
     py < 0 || py >= (int)this->numberVoxelsY ||
     pz < 0 || pz >= (int)this->numberVoxelsZ )
  {
    v = 0.0;
    return qfeError;
  }

  int offset = px+numberVoxelsX*(py+numberVoxelsY*pz);
  
  switch (valueType) 
  {
	  case qfeValueTypeInt8 :
	  {
		  char b = *((char*)this->voxels + offset);
		  v = (float)b;
		  v-=CHAR_MIN;
		  v/=(CHAR_MAX-CHAR_MIN);
	  }
	  break;
	  case qfeValueTypeUnsignedInt8 :
	  {
		  unsigned char b = *((unsigned char*)this->voxels + offset);
		  v = (float)b;
		  v /= UCHAR_MAX;
	  }
	  break;
	  case qfeValueTypeGrey :
	  {
		  unsigned char b = *((unsigned char*)this->voxels + offset);
		  v = (float)b;
		  v /= UCHAR_MAX;
	  }
	  break;
	  case qfeValueTypeInt16:
	  {
		  short b = *((short*)this->voxels + offset);
		  v = (float)b;
		  v-=SHRT_MIN;
		  v/=(SHRT_MAX-SHRT_MIN);
	  }
	  break;
	  case qfeValueTypeUnsignedInt16 :
	  {
		  unsigned short b = *((unsigned short*)this->voxels + offset);
		  v = (float)b;
		  v /= USHRT_MAX;
	  }
	  break;
	  case qfeValueTypeFloat :
	  {
		  v = *((float*)this->voxels + offset);
	  }
	  break;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVolume::qfeGetVolumeVectorAtPosition(int px, int py, int pz, qfeVector &v)
{ 
  int offset;

  if(this->numberComponents != 3 || 
     px < 0 || px >= (int)this->numberVoxelsX || 
     py < 0 || py >= (int)this->numberVoxelsY ||
     pz < 0 || pz >= (int)this->numberVoxelsZ )
  {
    v.x = v.y = v.z = 0.0;
    return qfeError;
  }

  offset = (int)px + (int)py*this->numberVoxelsX + (int)pz*numberVoxelsX*numberVoxelsY;

  v.x = *((float*)this->voxels + 3*offset+0);
  v.y = *((float*)this->voxels + 3*offset+1);
  v.z = *((float*)this->voxels + 3*offset+2);
  v.w = 0.0;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// p in voxel coordinates
qfeReturnStatus qfeVolume::qfeGetVolumeVectorAtPositionLinear(qfePoint p, qfeVector &v)
{
  qfeFloat xd = p.x - (qfeFloat)floor(p.x);
  qfeFloat yd = p.y - (qfeFloat)floor(p.y);
  qfeFloat zd = p.z - (qfeFloat)floor(p.z);

  qfeVector va, vb;

  if(this->qfeGetVolumeVectorAtPosition((int)floor(p.x), (int)floor(p.y), (int)floor(p.z), va)) return qfeError;
  if(this->qfeGetVolumeVectorAtPosition((int)floor(p.x), (int)floor(p.y), (int) ceil(p.z), vb)) return qfeError;

  qfeVector i1;
  qfeVector::qfeVectorInterpolateLinearSeparate(va, vb, zd, i1);

  if(this->qfeGetVolumeVectorAtPosition((int)floor(p.x), (int)ceil(p.y), (int)floor(p.z), va)) return qfeError;;
  if(this->qfeGetVolumeVectorAtPosition((int)floor(p.x), (int)ceil(p.y), (int) ceil(p.z), vb)) return qfeError;;

  qfeVector i2;
  qfeVector::qfeVectorInterpolateLinearSeparate(va, vb, zd, i2);

  if(this->qfeGetVolumeVectorAtPosition((int)ceil(p.x), (int)floor(p.y), (int)floor(p.z), va)) return qfeError;;
  if(this->qfeGetVolumeVectorAtPosition((int)ceil(p.x), (int)floor(p.y), (int) ceil(p.z), vb)) return qfeError;;

  qfeVector j1;
  qfeVector::qfeVectorInterpolateLinearSeparate(va, vb, zd, j1);

  if(this->qfeGetVolumeVectorAtPosition((int)ceil(p.x), (int)ceil(p.y), (int)floor(p.z), va)) return qfeError;;
  if(this->qfeGetVolumeVectorAtPosition((int)ceil(p.x), (int)ceil(p.y), (int) ceil(p.z), vb)) return qfeError;;

  qfeVector j2;
  qfeVector::qfeVectorInterpolateLinearSeparate(va, vb, zd, j2);

  qfeVector w1, w2;
  qfeVector::qfeVectorInterpolateLinearSeparate(i1, i2, yd, w1);
  qfeVector::qfeVectorInterpolateLinearSeparate(j1, j2, yd, w2);

  qfeVector::qfeVectorInterpolateLinearSeparate(w1, w2, xd, v);

  return qfeSuccess;
}