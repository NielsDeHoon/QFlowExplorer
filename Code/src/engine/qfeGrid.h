#pragma once

#include "qflowexplorer.h"

enum qfeGridOrientation
{
  qfeGridAxial,
  qfeGridCoronal,
  qfeGridSagittal,
};

/**
* \file   qfeGrid.h
* \author Roy van Pelt
* \class  qfeGrid
* \brief  Implements the geometry for a volume
* \note   Confidential
*
* A grid defines the position (origin), orientation and voxel spacing of
* voxel raster with respect to the visualization coordinate system
*/
class qfeGrid
{
public:
  qfeGrid(const qfeGrid &grid);
  qfeGrid(qfeFloat ox  = 0.0, qfeFloat oy  = 0.0, qfeFloat oz  = 0.0,
          qfeFloat axx = 1.0, qfeFloat axy = 0.0, qfeFloat axz = 0.0,
          qfeFloat ayx = 0.0, qfeFloat ayy = 1.0, qfeFloat ayz = 0.0,
          qfeFloat azx = 0.0, qfeFloat azy = 0.0, qfeFloat azz = 1.0,
          qfeFloat ex  = 1.0, qfeFloat ey  = 1.0, qfeFloat ez  = 1.0);
  ~qfeGrid();

  qfeReturnStatus qfeSetGridOrigin(qfeFloat ox, qfeFloat oy, qfeFloat oz);
  qfeReturnStatus qfeGetGridOrigin(qfeFloat& ox, qfeFloat& oy, qfeFloat& oz);

  qfeReturnStatus qfeSetGridAxes(qfeFloat axx, qfeFloat axy, qfeFloat axz,
                                 qfeFloat ayx, qfeFloat ayy, qfeFloat ayz,
                                 qfeFloat azx, qfeFloat azy, qfeFloat azz);
  qfeReturnStatus qfeGetGridAxes(qfeFloat& axx, qfeFloat& axy, qfeFloat& axz,
                                 qfeFloat& ayx, qfeFloat& ayy, qfeFloat& ayz,
                                 qfeFloat& azx, qfeFloat& azy, qfeFloat& azz);

  qfeReturnStatus qfeSetGridExtent(qfeFloat ex, qfeFloat ey, qfeFloat ez);
  qfeReturnStatus qfeGetGridExtent(qfeFloat& ex, qfeFloat& ey, qfeFloat& ez);

  qfeReturnStatus qfeSetGridOrientation(qfeGridOrientation orientation);
  qfeReturnStatus qfeGetGridOrientation(qfeGridOrientation &orientation);

protected:
  qfeFloat           origin[3];  
  qfeFloat           axis[3][3];
  qfeFloat           extent[3]; 
  qfeGridOrientation orient;

};
