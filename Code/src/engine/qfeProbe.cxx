#include "qfeProbe.h"

//----------------------------------------------------------------------------
// qfeProbe
//----------------------------------------------------------------------------
qfeProbe::qfeProbe()
{  
};

//----------------------------------------------------------------------------
qfeProbe::qfeProbe(const qfeProbe &probe)
{
};

//----------------------------------------------------------------------------
qfeProbe::~qfeProbe()
{ 
};

//----------------------------------------------------------------------------
// qfeProbe2D
//----------------------------------------------------------------------------
qfeProbe2D::qfeProbe2D()
{
  this->origin.qfeSetPointElements(0.0,0.0,0.0);
  this->axisX.qfeSetVectorElements(1.0,0.0,0.0);
  this->axisY.qfeSetVectorElements(0.0,1.0,0.0);
  this->normal.qfeSetVectorElements(0.0,0.0,1.0);  
};

qfeProbe2D::~qfeProbe2D()
{

};

//----------------------------------------------------------------------------
// qfeProbe3D
//----------------------------------------------------------------------------
qfeProbe3D::qfeProbe3D()
{  
  this->selected    = false;
  this->visible     = true;

  this->length      = 70.0f;  
  this->topRadius   = 25.0f; 
  this->baseRadius  = 10.0f;  
  this->numSlices   = 40;
  this->numStacks   = 2;
  this->borderWidth = 3;

  this->origin.qfeSetPointElements(0.0,0.0,0.0);
  this->axes[0].qfeSetVectorElements(1.0,0.0,0.0);
  this->axes[1].qfeSetVectorElements(0.0,1.0,0.0);
  this->axes[2].qfeSetVectorElements(0.0,0.0,1.0);  

  this->orthoPlaneOffset = 0.5f;
};

//----------------------------------------------------------------------------
qfeProbe3D::~qfeProbe3D()
{

};


//----------------------------------------------------------------------------
// angle in radians
void qfeProbe3D::qfeProbeRoll(qfeFloat angle)
{
  this->qfeProbeRotate(this->axes[2], angle);
}

//----------------------------------------------------------------------------
// angle in radians
void qfeProbe3D::qfeProbeAzimuth(qfeFloat angle)
{    
  this->qfeProbeRotate(this->axes[0], angle);
}

//----------------------------------------------------------------------------
// angle in radians
void qfeProbe3D::qfeProbePitch(qfeFloat angle)
{
  this->qfeProbeRotate(this->axes[1], angle);
}

//----------------------------------------------------------------------------
// angle in radians
void qfeProbe3D::qfeProbeRotate(qfeVector axis, qfeFloat angle)
{
  qfeMatrix4f R;

  qfeVector u;
  qfeFloat  phi, c, t, s;

  if(abs(angle) == 0) return;
  
  u    = axis;
  phi  = angle;
  c    = cos(phi);
  t    = 1.0f - cos(phi);
  s    = sin(phi);

  qfeVector::qfeVectorNormalize(u);
  qfeMatrix4f::qfeSetMatrixIdentity(R);  
  R(0,0) = u.x*u.x*t + c;
  R(0,1) = u.x*u.y*t + u.z*s;
  R(0,2) = u.x*u.z*t - u.y*s;
  R(1,0) = u.x*u.y*t - u.z*s;
  R(1,1) = u.y*u.y*t + c;
  R(1,2) = u.y*u.z*t + u.x*s;
  R(2,0) = u.x*u.z*t + u.y*s;
  R(2,1) = u.y*u.z*t - u.x*s;
  R(2,2) = u.z*u.z*t + c;

  // Update the axes
  qfeVector::qfeVectorNormalize(this->axes[0]);
  qfeVector::qfeVectorNormalize(this->axes[2]);
  this->axes[0] = this->axes[0] * R;
  this->axes[2] = this->axes[2] * R;
  qfeVector::qfeVectorNormalize(this->axes[0]);
  qfeVector::qfeVectorNormalize(this->axes[2]);  
  qfeVector::qfeVectorOrthonormalBasis1(this->axes[2], this->axes[0], this->axes[1]); 
}
