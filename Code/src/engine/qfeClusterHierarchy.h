#pragma once

#include "qfeMath.h"
#include "qfeClusterNodeElliptic.h"

#include <vector>
#include <fstream>
#include <time.h>

/**
* \file   qfeClusterHierarchy.h
* \author Sander Jacobs
* \class  -
* \brief  Defines several structs related to hierarchical clustering
* \note   Confidential
*/

// TODO: Make this a proper class?

enum qfeClusterType
{
  qfeClusterTypeL2Norm,
  qfeClusterTypeElliptical,
  qfeClusterTypeElliptical4D,
  qfeClusterTypeLinearModel,
  qfeClusterTypeScalar
};

struct qfeClusterDefinition
{
  qfeClusterDefinition(){}
  qfeClusterDefinition(unsigned int c1, unsigned int c2, qfeFloat d)
  {
    this->c1 = c1;
    this->c2 = c2;
    this->d = d;
  }
  unsigned int c1;
  unsigned int c2;
  qfeFloat     d;
};

struct qfeClusterHierarchy
{
  qfeClusterHierarchy::qfeClusterHierarchy()
  {
    changed = true;

    leaves.clear();
    tree.clear();

    this->dims[0] = 0;
    this->dims[1] = 0;
    this->dims[2] = 0;
    this->phaseCount  = 0;

    this->stepSize    = 0.01;
    this->clusterType = qfeClusterTypeElliptical4D;

    this->clusterTime = 0;
    this->computedDistanceCount = 0;
    this->processedDistanceCount = 0;
  }

  //----------------------------------------------------------------------------
  // TODO: Move this to qfeClusterHierarchy???
  qfeReturnStatus qfeClusterHierarchy::qfeExportClusterData(const char *filePath)
  {
    ofstream os;
    os.open (filePath);

    if(!os.is_open())
      return qfeError;

    time_t secs=time(0);
    tm *t=localtime(&secs);
    char timeStamp[512];
    sprintf(timeStamp,"%04d-%02d-%02d %02d:%02d:%02d",\
      t->tm_year+1900,t->tm_mon,t->tm_mday,t->tm_hour,t->tm_min,t->tm_sec);
    os << "TIME_STAMP\n" << timeStamp << "\n\n";

    os << "CLUSTER_TYPE\n";
    switch(this->clusterType)
    {
    case qfeClusterTypeElliptical:
      os<<"qfeClusterTypeElliptical\n\n"; 
      os<<"ELLIPTICAL_A\n" << qfeClusterNodeElliptic::A<<"\n\n";
      os<<"ELLIPTICAL_B\n" << qfeClusterNodeElliptic::B<<"\n\n"; 
      break;
    case qfeClusterTypeElliptical4D:
      os<<"qfeClusterTypeElliptical4D\n\n";
      os<<"ELLIPTICAL_A\n" << qfeClusterNodeElliptic::A<<"\n\n";
      os<<"ELLIPTICAL_B\n" << qfeClusterNodeElliptic::B<<"\n\n"; 
      break;
    case qfeClusterTypeL2Norm:
      os<<"qfeClusterTypeL2Norm\n\n"; 
      break;
    case qfeClusterTypeLinearModel:
      os<<"qfeClusterTypeLinearModel\n\n"; 
      break;
    default:
      os<<"Unknown\n\n";
    }
    os << "HIERARCHICAL_STEP_SIZE\n" << this->stepSize << "\n\n";

    os << "DIMS\n" << this->dims[0] << "\n" << this->dims[1] << "\n" << this->dims[2] << "\n\n";
    os << "PHASE_COUNT\n" << this->phaseCount << "\n\n";

    os << "LEAF_MASK_SIZE\n" << this->leaves.size() << "\n\n";
    os << "TREE_SIZE\n" <<  this->tree.size() << "\n\n";

    os << "CLUSTER_TIME\n" << this->clusterTime << "\n\n";
    os << "COMPUTED_DISTANCES\n" << this->computedDistanceCount << "\n\n";
    os << "PROCESSED_DISTANCES\n" << this->processedDistanceCount << "\n\n";

    os << "LEAVES\n";
    for(unsigned int i=0; i<this->leaves.size(); i++)
    {
      os << this->leaves[i] << "\n";
    }
    os<<"\n";

    os << "TREE\n";
    for(unsigned int i=0; i<this->tree.size(); i++)
    {
      os << this->tree[i].c1 << "\n";
      os << this->tree[i].c2 << "\n";
      os << this->tree[i].d << "\n";
    }
    os<<"\n";

    os.close();

    return qfeSuccess;
  }

  //----------------------------------------------------------------------------
  qfeReturnStatus qfeClusterHierarchy::qfeImportClusterData(const char *filePath)
  {
    ifstream is;
    is.open (filePath);
    if(!is.is_open())
      return qfeError;
    char buff[512];

    this->leaves.clear();
    this->tree.clear();

    unsigned int treeSize = 0;

    while(is.getline(buff,512))
    {
      //cout << buff << endl;
      if(strcmp(buff,"CLUSTER_TYPE")==0)
      {
        if(!is.getline(buff,512)) return qfeError;
        if(strcmp(buff,"qfeClusterTypeL2Norm")==0)  this->clusterType = qfeClusterTypeL2Norm;
        else if(strcmp(buff,"qfeClusterTypeElliptical")==0)   this->clusterType = qfeClusterTypeElliptical;
        else if(strcmp(buff,"qfeClusterTypeLinearModel")==0)  this->clusterType = qfeClusterTypeLinearModel;
        else if(strcmp(buff,"qfeClusterTypeElliptical4D")==0) this->clusterType = qfeClusterTypeElliptical4D;
        else return qfeError;
      }
      else if(strcmp(buff,"ELLIPTICAL_A")==0)
      {
        if(!is.getline(buff,512)) return qfeError;
        qfeClusterNodeElliptic::A = atof(buff);
      }
      else if(strcmp(buff,"ELLIPTICAL_B")==0)
      {
        if(!is.getline(buff,512)) return qfeError;
        qfeClusterNodeElliptic::B = atof(buff);
      }
      else if(strcmp(buff,"DIMS")==0)
      {
        for(int i=0; i<3; i++)
        {
          if(!is.getline(buff,512)) return qfeError;
          this->dims[i] = atoi(buff);
        }
      }
      else if(strcmp(buff,"PHASE_COUNT")==0)
      {
        if(!is.getline(buff,512)) return qfeError;
        this->phaseCount = atoi(buff);
      }
      else if(strcmp(buff,"HIERARCHICAL_STEP_SIZE")==0)
      {
        if(!is.getline(buff,512)) return qfeError;
        this->stepSize = atof(buff);
      }
      else if(strcmp(buff,"LEAF_MASK_SIZE")==0)
      {
        if(!is.getline(buff,512)) return qfeError;
        this->leaves.resize(atoi(buff));
      }
      else if(strcmp(buff,"TREE_SIZE")==0)
      {
        if(!is.getline(buff,512)) return qfeError;
        treeSize = atoi(buff);
        this->tree.reserve(treeSize);
      }
      else if(strcmp(buff,"LEAVES")==0)
      {
        for(unsigned int i=0; i<this->leaves.size(); i++)
        {
          if(!is.getline(buff,512)) return qfeError;
          this->leaves[i] = atoi(buff);
        }
      }
      else if(strcmp(buff,"TREE")==0)
      {
        unsigned int c1;
        unsigned int c2;
        qfeFloat d;
        for(unsigned int i=0; i<treeSize; i++)
        {
          if(!is.getline(buff,512)) return qfeError; c1 = atoi(buff);
          if(!is.getline(buff,512)) return qfeError; c2 = atoi(buff);
          if(!is.getline(buff,512)) return qfeError; d = (qfeFloat)atof(buff);
          this->tree.push_back(qfeClusterDefinition(c1,c2,d));
        }
      }
    }
    is.close();
    changed = true;
    return qfeSuccess;
  }

  bool changed;

  vector< unsigned int >           leaves;
  vector< qfeClusterDefinition >   tree;

  unsigned int    dims[3];
  unsigned int    phaseCount;

  double          stepSize;
  qfeClusterType  clusterType;

  // Some clustering statistics
  unsigned int clusterTime;
  unsigned int computedDistanceCount;
  unsigned int processedDistanceCount;
};
