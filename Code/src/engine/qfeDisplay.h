#pragma once

#include "qflowexplorer.h"

#include <gl/glew.h>

#include <vtkRenderWindow.h>

#include "qfeGLSupport.h"

enum qfeDisplayBackground
{
  qfeDisplayBackgroundUniform,
  qfeDisplayBackgroundGradient
};

enum qfeDisplayLayout
{
  qfeDisplayLayoutSingle,
  qfeDisplayLayoutQuad,
  qfeDisplayLayoutLeft,
  qfeDisplayLayoutRight,  
  qfeDisplayLayoutUS
};

/**
* \file   qfeDisplay.h
* \author Roy van Pelt
* \class  qfeDisplay
* \brief  Implements a 2D raster of pixels.
* \note   Confidential
*
* A display is a 2D raster of pixels. 
* A display consists of a number of rows, each consisting of
* a number of pixels. A pixel is a scalar value representing a
* single sample of a display image. 
*/

class qfeDisplay
{
public:
  qfeDisplay();
  qfeDisplay(const qfeDisplay &display);
  qfeDisplay(qfeValueType t, unsigned int nx, unsigned int ny);
  ~qfeDisplay();

  qfeReturnStatus qfeGetDisplayValueType(qfeValueType t);
  qfeReturnStatus qfeGetDisplaySize(unsigned int &nx, unsigned int &ny);

  qfeReturnStatus qfeSetDisplayBackgroundColorRGB(qfeFloat r, qfeFloat g, qfeFloat b);
  qfeReturnStatus qfeGetDisplayBackgroundColorRGB(qfeFloat &r, qfeFloat &g, qfeFloat &b);

  qfeReturnStatus qfeSetDisplayBackgroundColorRGBSecondary(qfeFloat r, qfeFloat g, qfeFloat b);
  qfeReturnStatus qfeGetDisplayBackgroundColorRGBSecondary(qfeFloat &r, qfeFloat &g, qfeFloat &b);

  qfeReturnStatus qfeSetDisplayBackgroundType(int type);
  qfeReturnStatus qfeGetDisplayBackgroundType(int &type);

  qfeReturnStatus qfeSetDisplayWindow(vtkRenderWindow *rw);
  qfeReturnStatus qfeGetDisplayWindow(vtkRenderWindow **rw);

  qfeReturnStatus qfeSetDisplayLayout(qfeDisplayLayout layout, bool fullScreen, bool clickable);
  qfeReturnStatus qfeGetDisplayLayout(qfeDisplayLayout &layout, bool &fullScreen, bool &clickable);

  qfeReturnStatus qfeSetDisplaySuperSampling(int factor);
  qfeReturnStatus qfeGetDisplaySuperSampling(int &factor);

private:
  vtkRenderWindow   *renderWindow;
  qfeColorRGBA       backgroundColor;
  qfeColorRGBA       backgroundColorSecondary;
  int                backgroundType;

  qfeDisplayLayout   layout;
  bool               layoutFullScreen;
  bool               layoutClickable;

  unsigned int nx, ny;
  int          supersampling;

};
