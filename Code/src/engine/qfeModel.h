#pragma once

#include "qflowexplorer.h"

#include <gl/glew.h>

#include <iostream>
#include <string>
#include <math.h>

#include <vtkCellArray.h>
#include <vtkCellTreeLocator.h>
#include <vtkExtractEdges.h>
#include <vtkGenericCell.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkSelectEnclosedPoints.h>
#include <vtkSmartPointer.h>
#include <vtkTriangleFilter.h>

#include <vtkIdFilter.h>
#include <vtkBox.h>
#include <vtkClipPolyData.h>

using namespace std;

/**
* \file   qfeModel.h
* \author Roy van Pelt
* \class  qfeModel
* \brief  Implements a geometrical object
* \note   Confidential
*
* For now this is a simple wrapper for a vtkPolyDataObject
*/
class qfeModel
{
public:
  qfeModel();
  qfeModel(const qfeModel &model);
  ~qfeModel();

  qfeReturnStatus DeepCopy(const qfeModel *model);

  qfeReturnStatus qfeSetModelMesh(vtkPolyData *mesh);
  qfeReturnStatus qfeGetModelMesh(vtkPolyData **mesh);
  qfeReturnStatus qfeGetModelMesh(qfeMeshVertices **vertexBuffer, qfeMeshVertices **normalBuffer);    
  qfeReturnStatus qfeGetModelMeshIndicesTriangles(qfeMeshIndices **indexBuffer);  
  qfeReturnStatus qfeGetModelMeshIndicesTriangleStrips(qfeMeshStripsIndices **indexBuffer);  
  qfeReturnStatus qfeGetModelMeshIndicesTriangleStripsAdj(qfeMeshStripsIndices **indexBuffer);  

  qfeReturnStatus qfeSetModelColor(qfeColorRGB col);
  qfeReturnStatus qfeGetModelColor(qfeColorRGB &col);

  qfeReturnStatus qfeSetModelLabel(string  lab);
  qfeReturnStatus qfeGetModelLabel(string &lab);

  qfeReturnStatus qfeGetModelBounds(double &bxmin, double &bxmax, double &bymin, double &bymax, double &bzmin, double &bzmax);
  
  qfeReturnStatus qfeSetModelVisible(bool visible);
  qfeReturnStatus qfeGetModelVisible(bool &visible);

  bool            qfeGetModelContainsPoint(vector<float> p);

  qfeReturnStatus qfeClipMesh(double xMin, double xMax, double yMin, double yMax, double zMin, double zMax);
  
protected:
  vtkPolyData  *mesh;

  qfeMeshVertices      vertices;
  qfeMeshVertices      normals;
  qfeMeshIndices       indicesTriangles;
  qfeMeshStripsIndices indicesTriangleStrips;
  qfeMeshStripsIndices indicesTriangleStripsAdjacency;

  bool          visible;
  qfeColorRGB   color;
  string        label;

  qfeReturnStatus qfeGetMeshBuffers(vtkPolyData *mesh, qfeMeshVertices &vertexBuffer, qfeMeshVertices &normalBuffer);  
  qfeReturnStatus qfeGetMeshIndicesTriangles(vtkPolyData *mesh, qfeMeshIndices &indices);
  qfeReturnStatus qfeGetMeshIndicesTriangleStrips(vtkPolyData *mesh, qfeMeshStripsIndices &indices);
  qfeReturnStatus qfeGetMeshIndicesTriangleStripsAdj(vtkPolyData *mesh, qfeMeshStripsIndices &indices);

  qfeReturnStatus qfeGetMeshEdgeNeighborPoints(vtkPolyData *triangles, vtkIdType p1, vtkIdType p2, vtkIdType p3, vtkIdList *neighborIndices);
  qfeReturnStatus qfeGetMeshConnectedPoints(vtkPolyData *triangles, int id, vtkIdList *neighbors);
};
