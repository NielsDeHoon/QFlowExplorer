#pragma once

#include <iostream>
#include <typeinfo>

#include "qflowexplorer.h"

#include "qfeVector.h"

#include <string>
#include <vector>

using namespace std;

/**
* \brief Structure for gui driven selection lists
*/
struct qfeSelectionList
{
  vector<string> list;
  int            active;
};

/**
* \brief Structure for parameters for vessel cross-section
*/
struct qfeRingParam
{
  bool         visible;
  qfeVector    seedPoint;
  //qfeFloat    dilation;
  qfeColorRGBA color;
};

enum qfeVisualParameterLevel
{
  qfeVisualParameterBasic,
  qfeVisualParameterAdvanced
};

/**
* \file   qfeVisualParameter.h
* \author Roy van Pelt
* \class  qfeVisualParameter
* \brief  Implements a parameter class
* \note   Confidential
*
* Template class for various parameters.
* The type of the parameter can be queried.
*
*/
class qfeVisualParameter
{ 
  public:    
    qfeVisualParameter();
    qfeVisualParameter(const qfeVisualParameter &param);
    ~qfeVisualParameter(){};

    qfeReturnStatus qfeSetVisualParameterLevel(qfeVisualParameterLevel level);
    qfeReturnStatus qfeGetVisualParameterLevel(qfeVisualParameterLevel &level);

    qfeReturnStatus qfeSetVisualParameterLabel(string label);
    qfeReturnStatus qfeGetVisualParameterLabel(string *label);

    qfeReturnStatus qfeSetVisualParameterValue(bool              value);
    qfeReturnStatus qfeSetVisualParameterValue(int               value);
    qfeReturnStatus qfeSetVisualParameterValue(double            value);
    qfeReturnStatus qfeSetVisualParameterValue(string            value);
    qfeReturnStatus qfeSetVisualParameterValue(qfeSelectionList  value);
    qfeReturnStatus qfeSetVisualParameterValue(qfeVector         value);
    qfeReturnStatus qfeSetVisualParameterValue(qfeColorRGBA      value);
    qfeReturnStatus qfeSetVisualParameterValue(qfeRingParam      value);
    qfeReturnStatus qfeSetVisualParameterValue(qfeRange          value);

    qfeReturnStatus qfeGetVisualParameterValue(bool             &value);
    qfeReturnStatus qfeGetVisualParameterValue(int              &value);
    qfeReturnStatus qfeGetVisualParameterValue(double           &value);
    qfeReturnStatus qfeGetVisualParameterValue(string           &value);
    qfeReturnStatus qfeGetVisualParameterValue(qfeSelectionList &value);
    qfeReturnStatus qfeGetVisualParameterValue(qfeVector        &value);
    qfeReturnStatus qfeGetVisualParameterValue(qfeColorRGBA     &value);
    qfeReturnStatus qfeGetVisualParameterValue(qfeRingParam     &value);
    qfeReturnStatus qfeGetVisualParameterValue(qfeRange         &value);

    qfeReturnStatus qfeSetVisualParameterStepSize(float s);
    qfeReturnStatus qfeGetVisualParameterStepSize(float &s);

    qfeReturnStatus qfeSetVisualParameterRange(float rl, float ru);
    qfeReturnStatus qfeGetVisualParameterRange(float &rl, float &ru);

    qfeReturnStatus qfeGetVisualParameterType(qfeParamType &t);

    // Template functions needs to be inline, since export by only few compilers
    template <typename paramtype>
    inline qfeReturnStatus qfeSetVisualParameter(string label, paramtype value, float s, float rl, float ru, qfeVisualParameterLevel level = qfeVisualParameterBasic)
    {      
      this->qfeSetVisualParameterLabel(label);
      this->qfeSetVisualParameterValue(value);
      this->qfeSetVisualParameterStepSize(s);
      this->qfeSetVisualParameterRange(rl, ru);
      this->qfeSetVisualParameterLevel(level);

      return qfeSuccess;
    }

    template <typename paramtype>
    inline qfeReturnStatus qfeSetVisualParameter(string label, paramtype value, qfeVisualParameterLevel level = qfeVisualParameterBasic)
    {
      this->qfeSetVisualParameterLabel(label);
      this->qfeSetVisualParameterValue(value);
      this->qfeSetVisualParameterStepSize(1);
      this->qfeSetVisualParameterRange(0, 1);
      this->qfeSetVisualParameterLevel(level);

      return qfeSuccess;
    }

  protected:
    qfeVisualParameterLevel valueLevel;
    string                  valueLabel;
    qfeParamType            valueType;
    float                   valueStepSize;
    float                   valueRange[2];

    bool                    valueBool;
    int                     valueInt;
    double                  valueDouble;
    string                  valueString;
    qfeSelectionList        valueStringList;   
    qfeVector               valueVector;
    qfeColorRGBA            valueColor;
    qfeRingParam            valueRing;
    qfeRange                valueRangeMinMax;

    qfeReturnStatus qfeClearVisualParameterValues();
    
};
