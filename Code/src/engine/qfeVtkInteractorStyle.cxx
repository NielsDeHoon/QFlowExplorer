#include "qfeVtkInteractorStyle.h"

#include "vtkRenderWindowInteractor.h"
#include "vtkObjectFactory.h"

vtkStandardNewMacro(qfeVtkInteractorStyle);

//----------------------------------------------------------------------------
qfeVtkInteractorStyle::qfeVtkInteractorStyle() 
{
  this->HandleObserversOff();

  this->handleParentMove          = true;
  this->handleParentLeftDown      = true;
  this->handleParentLeftUp        = true;
  this->handleParentRightDown     = true;
  this->handleParentRightUp       = true;
  this->handleParentWheelForward  = true;
  this->handleParentWheelBackward = true;
  this->handleParentKeyPress      = true;

  this->numberOfClicks      = 0;
  this->previousPosition[0] = 0.0;
  this->previousPosition[1] = 0.0;
  this->resetPixelDistance  = 5;

  this->timerCallback = vtkCallbackTimerOneShot::New();
}

//----------------------------------------------------------------------------
qfeVtkInteractorStyle::~qfeVtkInteractorStyle()
{
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnTimer()
{
  vtkInteractorStyleTrackballCamera::OnTimer();

  this->InvokeEvent(vtkCommand::TimerEvent, NULL);
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnParentMouseMove()
{
  vtkInteractorStyleTrackballCamera::OnMouseMove();
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnMouseMove()
{
  if(this->handleParentMove) this->OnParentMouseMove();

  this->InvokeEvent(vtkCommand::MouseMoveEvent, NULL);
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnParentLeftButtonDown()
{
  vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnLeftButtonDown()
{
  if(this->handleParentLeftDown) this->OnParentLeftButtonDown();

  int pickPosition[2];
  this->GetInteractor()->GetEventPosition(pickPosition);

  int xdist = pickPosition[0] - this->previousPosition[0];
  int ydist = pickPosition[1] - this->previousPosition[1];

  this->previousPosition[0] = pickPosition[0];
  this->previousPosition[1] = pickPosition[1];

  int moveDistance = (int)sqrt((double)(xdist*xdist + ydist*ydist));

  // Reset numClicks - If mouse moved further than resetPixelDistance
  int tid;

  this->numberOfClicks++;
  
  if(this->timerCallback->IsOn() && this->timerCallback->IsShot())
  {
   
    this->timerCallback->SetOneShotTimerOff();
    this->GetInteractor()->RemoveObserver(this->timerCallback);    
    this->numberOfClicks = 1; 
  }

  if(this->numberOfClicks == 1)
  {   
    this->GetInteractor()->AddObserver(vtkCommand::TimerEvent, this->timerCallback);
    tid = this->GetInteractor()->CreateOneShotTimer(400); 
    this->timerCallback->SetOneShotTimerId(tid);    
    this->timerCallback->SetOneShotTimerOn();
  }

  if(this->numberOfClicks == 2)
  {  
    if(moveDistance < this->resetPixelDistance)
    {
      this->InvokeEvent(qfeVtkInteractorStyle::DoubleClickEvent, NULL);
    }
   
    this->timerCallback->SetOneShotTimerOff();
    this->GetInteractor()->RemoveObserver(this->timerCallback);    
    this->numberOfClicks = 0;    
  }  

  this->InvokeEvent(vtkCommand::LeftButtonPressEvent, NULL);
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnParentLeftButtonUp()
{
  vtkInteractorStyleTrackballCamera::OnLeftButtonUp();
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnLeftButtonUp()
{
  if(this->handleParentLeftUp) this->OnParentLeftButtonUp();

  this->InvokeEvent(vtkCommand::LeftButtonReleaseEvent, NULL);
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnParentRightButtonDown()
{
  vtkInteractorStyleTrackballCamera::OnRightButtonDown();
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnRightButtonDown()
{
  if(this->handleParentRightDown) this->OnParentRightButtonDown();

  this->InvokeEvent(vtkCommand::RightButtonPressEvent, NULL);
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnParentRightButtonUp()
{
  vtkInteractorStyleTrackballCamera::OnRightButtonUp();
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnRightButtonUp()
{
  if(this->handleParentRightUp) this->OnParentRightButtonUp();

  this->InvokeEvent(vtkCommand::RightButtonReleaseEvent, NULL);
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnMouseWheelForward()
{
  if(this->handleParentWheelForward) vtkInteractorStyleTrackballCamera::OnMouseWheelForward();

  this->InvokeEvent(vtkCommand::MouseWheelForwardEvent, NULL);
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnParentWheelForward()
{
  vtkInteractorStyleTrackballCamera::OnMouseWheelForward();
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnMouseWheelBackward()
{
  if(this->handleParentWheelBackward) vtkInteractorStyleTrackballCamera::OnMouseWheelBackward();

  this->InvokeEvent(vtkCommand::MouseWheelBackwardEvent, NULL);
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnParentWheelBackward()
{
  vtkInteractorStyleTrackballCamera::OnMouseWheelBackward();
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnKeyPress()
{
  if(this->handleParentKeyPress) vtkInteractorStyleTrackballCamera::OnKeyPress();

  this->InvokeEvent(vtkCommand::KeyPressEvent, NULL);
}

//----------------------------------------------------------------------------
void qfeVtkInteractorStyle::OnParentKeyPress()
{
  if(this->handleParentKeyPress) vtkInteractorStyleTrackballCamera::OnKeyPress();
}

////----------------------------------------------------------------------------
//void qfeVtkInteractorStyle::OnParent3DMouseMove()
//{
//  //vtkTDxInteractorStyleCamera::OnMotionEvent();
//}
//
////----------------------------------------------------------------------------
//void qfeVtkInteractorStyle::On3DMouseMove()
//{
//  if(this->handleParent3DMouseMove) this->OnParent3DMouseMove();
//
//  this->InvokeEvent(vtkCommand::TDxMotionEvent, NULL);
//}
