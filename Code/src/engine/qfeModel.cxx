#include "qfeModel.h"

//----------------------------------------------------------------------------
qfeModel::qfeModel()
{
  this->mesh    = vtkPolyData::New();
  this->color.r = this->color.g = this->color.b = 0.5;
  this->label   = "model";
  this->visible = true;
};

//----------------------------------------------------------------------------
qfeModel::qfeModel(const qfeModel &model)
{
  this->mesh    = model.mesh;
  this->color   = model.color;
  this->label   = model.label;
  this->visible = model.visible;

  this->vertices                         = model.vertices;
  this->normals                          = model.normals;
  this->indicesTriangles                 = model.indicesTriangles;
  this->indicesTriangleStrips            = model.indicesTriangleStrips;
  this->indicesTriangleStripsAdjacency   = model.indicesTriangleStrips;
};

//----------------------------------------------------------------------------
qfeModel::~qfeModel()
{
  this->mesh->Delete();
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::DeepCopy(const qfeModel *model)
{
  if(model == NULL)
	return qfeError;
  this->mesh->DeepCopy(model->mesh);
  this->color = model->color;
  this->label = model->label;
  this->visible = model->visible;

  //these vectors contain values, not pointers:
  this->vertices                         = model->vertices;
  this->normals                          = model->normals;
  this->indicesTriangles                 = model->indicesTriangles;
  this->indicesTriangleStrips            = model->indicesTriangleStrips;
  this->indicesTriangleStripsAdjacency   = model->indicesTriangleStrips;

  return qfeSuccess;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeSetModelMesh(vtkPolyData *mesh)
{
  this->mesh->DeepCopy(mesh);

  //this->mesh->BuildCells();
  //this->mesh->BuildLinks();

  this->qfeGetMeshBuffers(this->mesh, this->vertices, this->normals);  
  this->qfeGetMeshIndicesTriangles(this->mesh, this->indicesTriangles);
  this->qfeGetMeshIndicesTriangleStrips(this->mesh, this->indicesTriangleStrips);
  this->qfeGetMeshIndicesTriangleStripsAdj(this->mesh, this->indicesTriangleStripsAdjacency);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetModelMesh(vtkPolyData **mesh)
{
  (*mesh) = this->mesh;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetModelMesh(qfeMeshVertices **vertexBuffer, qfeMeshVertices **normalBuffer)
{
  (*vertexBuffer) = &this->vertices;
  (*normalBuffer) = &this->normals;  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetModelMeshIndicesTriangles(qfeMeshIndices **indexBuffer)
{  
  (*indexBuffer)  = &this->indicesTriangles;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetModelMeshIndicesTriangleStrips(qfeMeshStripsIndices **indexBuffer)
{  
  (*indexBuffer)  = &this->indicesTriangleStrips;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetModelMeshIndicesTriangleStripsAdj(qfeMeshStripsIndices **indexBuffer)
{  
  (*indexBuffer)  = &this->indicesTriangleStripsAdjacency;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeSetModelColor(qfeColorRGB col)
{
  this->color = col;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetModelColor(qfeColorRGB &col)
{
  col = this->color;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeSetModelLabel(string lab)
{
  this->label = lab;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetModelLabel(string &lab)
{

  lab = this->label;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetModelBounds(double &bxmin, double &bxmax, double &bymin, double &bymax, double &bzmin, double &bzmax)
{
  double b[6];
  this->mesh->GetBounds(b);

  bxmin = b[0];
  bxmax = b[1];
  bymin = b[2];
  bymax = b[3];
  bzmin = b[4];
  bzmax = b[5];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeSetModelVisible(bool visible)
{
  this->visible = visible;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetModelVisible(bool &visible)
{
  visible = this->visible;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
bool qfeModel::qfeGetModelContainsPoint(vector< float > p)
{  
  // vtkSelectEnclosedPoints does not seem to work, 
  // perhaps because the mesh is not fully closed.
  vtkSmartPointer<vtkPoints> points =  vtkSmartPointer<vtkPoints>::New();
  points->InsertNextPoint(p[0], p[1], p[2]);

  vtkSmartPointer<vtkPolyData> pointsPolydata = vtkSmartPointer<vtkPolyData>::New();
  pointsPolydata->SetPoints(points);

  vtkSmartPointer<vtkSelectEnclosedPoints> selectEnclosedPoints = 
    vtkSmartPointer<vtkSelectEnclosedPoints>::New();
 
  //selectEnclosedPoints->CheckSurfaceOn();
  selectEnclosedPoints->SetInput(pointsPolydata);
  selectEnclosedPoints->SetSurface(this->mesh);
  selectEnclosedPoints->Update();

  //cout << selectEnclosedPoints->IsInside(0) << ", ";

  std::cout << "Point : " << selectEnclosedPoints->IsInside(0) << std::endl;    
 
  vtkDataArray* insideArray = vtkDataArray::SafeDownCast(selectEnclosedPoints->GetOutput()->GetPointData()->GetArray("SelectedPoints"));
 
  for(vtkIdType i = 0; i < insideArray->GetNumberOfTuples(); i++)
  { 
    std::cout << i << " : " << insideArray->GetComponent(i,0) << std::endl;
  }  

  /*
  vtkSmartPointer<vtkSelectEnclosedPoints> selectEnclosedPoints = 
    vtkSmartPointer<vtkSelectEnclosedPoints>::New();

  selectEnclosedPoints->CheckSurfaceOff();
  selectEnclosedPoints->Initialize(this->mesh);

  cout << selectEnclosedPoints->IsInsideSurface(p[0], p[1], p[2]) << ", ";
  //cout << selectEnclosedPoints->GetCheckSurface() << ", ";
  */

  //this->mesh->BuildCells();
  //this->mesh->BuildLinks();

  /*
  vtkSmartPointer<vtkCellTreeLocator> cellTree = vtkSmartPointer<vtkCellTreeLocator>::New();  
  cellTree->SetDataSet(this->mesh);
  cellTree->BuildLocator();

  double testInside[3] = {p[0], p[1], p[2]};

  vtkIdType cellId;
  double    pcoords[3], weights[3];
  vtkSmartPointer<vtkGenericCell> cell = vtkSmartPointer<vtkGenericCell>::New();

  cellId = cellTree->FindCell(testInside,0, cell, pcoords, weights);

  if(cellId >= 0)
    return true;
  else
    return false;
  */

  /*
  if (cellId>=0)
  {
    std::cout << "First point: in cell " << cellId << ", ";
  }
  else
  {
    std::cout << 0 << ", ";
  }
  */


  /*
  if(selectEnclosedPoints->IsInside(0) == 1)
    return true;
  else
    return false;
  */
   return true;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeClipMesh(double xMin, double xMax, double yMin, double yMax, double zMin, double zMax)
{
  // Define a clipping box
   vtkSmartPointer<vtkBox> clipBox =
      vtkSmartPointer<vtkBox>::New();
  clipBox->SetBounds(xMin, xMax, yMin, yMax, zMin, zMax);
  
  // Clip the source with the box
  vtkSmartPointer<vtkClipPolyData> clipper = 
    vtkSmartPointer<vtkClipPolyData>::New();
  //clipper->SetInput(meshWithIds);
  clipper->SetInput(mesh);
  clipper->SetClipFunction(clipBox); 
  
  clipper->InsideOutOn();
  clipper->Update();
  
  //update current mesh and related info
  qfeSetModelMesh(clipper->GetOutput());
  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetMeshBuffers(vtkPolyData *mesh, qfeMeshVertices &vertexBuffer, qfeMeshVertices &normalBuffer)
{
  if(mesh == NULL)
    return qfeError; 

  vtkCellArray  *strips   = mesh->GetStrips();
  vtkPoints     *points   = mesh->GetPoints();
  vtkDataArray  *normals  = mesh->GetPointData()->GetNormals();  

  if(strips == NULL || points == NULL || normals == NULL)
  {
    cout << "qfeModel::qfeGetMeshBuffers - Could not read mesh information" << endl;
    return qfeError;
  }
 
  vertexBuffer.clear();
  normalBuffer.clear();

  // Get all points
  for(int i=0; i<points->GetNumberOfPoints(); i++)
  {
    double p[3], n[3];
    qfeMeshVertex vv, vn;
    

    points->GetPoint(i, p);
    normals->GetTuple(i, n);

    vv.x = p[0]; vv.y = p[1]; vv.z = p[2];
    vn.x = n[0]; vn.y = n[1]; vn.z = n[2];

    vertexBuffer.push_back(vv);
    normalBuffer.push_back(vn);    
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetMeshIndicesTriangles(vtkPolyData *mesh, qfeMeshIndices &indices)
{   
  if(mesh->GetNumberOfPoints() <= 3)
  {
    cout << "qfeModel::qfeGetMeshIndicesTriangles - Could not read mesh information" << endl;
    return qfeError;
  }

  vtkTriangleFilter *triangleFilter = vtkTriangleFilter::New();
  triangleFilter->SetInput(mesh);
  triangleFilter->Update();

  vtkPolyData  *triangles = triangleFilter->GetOutput();  
  vtkCellArray *polys     = triangles->GetPolys();

  if(triangles->GetNumberOfPoints() <= 3 || triangles->GetNumberOfPolys() <= 0)
  {
    cout << "qfeModel::qfeGetMeshIndicesTriangles - Could not read mesh information" << endl;
    return qfeError;
  }
 
  indices.clear();

  // Build the indices for a triangle strip
  vtkIdType nPts   = 0;
  vtkIdType *ptIds = polys->GetPointer();

  for (polys->InitTraversal(); polys->GetNextCell(nPts,ptIds);)
  {
    qfeMeshIndices  i;    
    
    for (int j = 0; j < nPts; j++)
    {
      indices.push_back((unsigned short)ptIds[j]);      
    }        
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetMeshIndicesTriangleStrips(vtkPolyData *mesh, qfeMeshStripsIndices &indices)
{
  vtkCellArray  *strips   = mesh->GetStrips();
  vtkPoints     *points   = mesh->GetPoints();
  vtkDataArray  *normals  = mesh->GetPointData()->GetNormals();

  if(strips == NULL || points == NULL || normals == NULL)
  {
    cout << "qfeModel::qfeGetMeshIndices - Could not read mesh information" << endl;
    return qfeError;
  }

  indices.clear();

  // Build the indices for a triangle strip
  vtkIdType nPts   = 0;
  vtkIdType *ptIds = strips->GetPointer();

  for (strips->InitTraversal(); strips->GetNextCell(nPts,ptIds);)
  {
    qfeMeshIndices  i;    
    
    for (int j = 0; j < nPts; j++)
    {
      i.push_back((unsigned short)ptIds[j]);
      
    }    
    indices.push_back(i);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetMeshIndicesTriangleStripsAdj(vtkPolyData *mesh, qfeMeshStripsIndices &indices)
{
  if(mesh == NULL)
    return qfeError; 

  vtkCellArray  *strips   = mesh->GetStrips();
  vtkPoints     *points   = mesh->GetPoints();
  vtkDataArray  *normals  = mesh->GetPointData()->GetNormals();
  vtkPolyData   *triangles;

  if(strips == NULL || points == NULL || normals == NULL)
  {
    cout << "qfeModel::qfeGetMeshTriangleStrips - Could not read mesh information" << endl;
    return qfeError;
  }

  vtkTriangleFilter *triangleFilter = vtkTriangleFilter::New();
  triangleFilter->SetInput(mesh);
  triangleFilter->Update();
  triangles = triangleFilter->GetOutput();
 
  indices.clear();

  // Build the indices for a triangle strip
  vtkIdType nPts       = 0;
  vtkIdType *ptIds     = strips->GetPointer();
  
  int index = 0;

  for (strips->InitTraversal(); strips->GetNextCell(nPts,ptIds);)
  {
    qfeMeshIndices  i;       
       
    for (int j = 0; j < nPts; j++)
    {
      vtkIdList *neighbors = vtkIdList::New();

      int third = j;

      if(j == 0)        third = 2;
      if(j == (nPts-1)) third = nPts-3;
           
      this->qfeGetMeshEdgeNeighborPoints(triangles, ptIds[max(j-1,0)], ptIds[min(j+1,nPts-1)], ptIds[third], neighbors);
      
      i.push_back((unsigned short)ptIds[j]);

      
      if(neighbors->GetNumberOfIds() > 0)
      {
        i.push_back((unsigned short)neighbors->GetId(0));
      }
      else
      {
        i.push_back((unsigned short)ptIds[j]);
      }

      neighbors->Delete();         
    }    
    indices.push_back(i);    

    index++;
  }

  triangleFilter->Delete();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetMeshEdgeNeighborPoints(vtkPolyData *triangles, vtkIdType p1, vtkIdType p2, vtkIdType p3, vtkIdList *neighborIndices)
{
  vtkIdList *neighborsP1 = vtkIdList::New();
  vtkIdList *neighborsP2 = vtkIdList::New();

  this->qfeGetMeshConnectedPoints(triangles, p1, neighborsP1);
  this->qfeGetMeshConnectedPoints(triangles, p2, neighborsP2); 

  for(int i=0; i<neighborsP1->GetNumberOfIds(); i++)
  {
    for(int j=0; j<neighborsP2->GetNumberOfIds(); j++)
    {
      // Every shared point that is not the 
      // third vertex of the triangle is added to the list
      if((neighborsP1->GetId(i) == neighborsP2->GetId(j)) &&
         (neighborsP1->GetId(i) != p3)
         )
      {
        neighborIndices->InsertUniqueId(neighborsP1->GetId(i));
      }
    }    
  }

  /*
  cout << p1 << ": ";
  for(int i=0; i<neighborsP1->GetNumberOfIds(); i++)
    cout << neighborsP1->GetId(i) << " ";
  cout << endl;

  cout << p2 << ": ";
  for(int i=0; i<neighborsP2->GetNumberOfIds(); i++)
    cout << neighborsP2->GetId(i) << " ";
  cout << endl;

  cout << p3 << ": exclude" << endl;

  for(int i=0; i<neighborIndices->GetNumberOfIds(); i++)
    cout << neighborIndices->GetId(i) << " ";
  cout << ": mutual" << endl;
  cout << endl;

  cout << endl;
  */

  neighborsP1->Delete();
  neighborsP2->Delete();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeModel::qfeGetMeshConnectedPoints(vtkPolyData *triangles, int id, vtkIdList *neighbors)
{
  vtkIdList   *cellIdList = vtkIdList::New();  

  triangles->GetPointCells(id, cellIdList);
 
  for(vtkIdType i = 0; i < cellIdList->GetNumberOfIds(); i++)
  {
    vtkIdList *pointIdList = vtkIdList::New();
    triangles->GetCellPoints(cellIdList->GetId(i), pointIdList);
 
    for(vtkIdType j = 0; j<pointIdList->GetNumberOfIds(); j++)
    {
      if(pointIdList->GetId(j) != id)
      {
        neighbors->InsertNextId(pointIdList->GetId(j));
      }
    }

    pointIdList->Delete();
  }

  cellIdList->Delete();
 
  return qfeSuccess;
}



