#pragma once

#include "qflowexplorer.h"

#include <gl/glew.h>

#include <algorithm>
#include <iostream>
#include <vector>

#include "core/qfeConvertColor.h"

#define QFE_COLORMAP_INTERPOLATE_NN     0
#define QFE_COLORMAP_INTERPOLATE_LINEAR 1

#define QFE_COLORMAP_SPACE_RGB 0
#define QFE_COLORMAP_SPACE_CIE 1

/**
* \file   qfeColorMap.h
* \author Roy van Pelt
* \class  qfeColorMap
* \brief  Implements a mapping from scalar values to colors
* \note   Confidential
*
* A color map defines a mapping of scalar values to colors.
* A color map contains a number of color mappings. (RGB, HLS)
*/
using namespace std;

class qfeColorMap
{
public:
  qfeColorMap();
  qfeColorMap(const qfeColorMap &colormap);
  ~qfeColorMap();

  qfeReturnStatus qfeSetColorMapRGB(unsigned int n,  vector<qfeRGBMapping>  mRGB);
  qfeReturnStatus qfeGetColorMapRGB(unsigned int &n, vector<qfeRGBMapping> &mRGB);

  qfeReturnStatus qfeSetColorMapA(unsigned int n,  vector<qfeOpacityMapping>  mA);
  qfeReturnStatus qfeGetColorMapA(unsigned int &n, vector<qfeOpacityMapping> &mA);

  qfeReturnStatus qfeSetColorMapG(unsigned int n,  vector<qfeOpacityMapping>  mG);
  qfeReturnStatus qfeGetColorMapG(unsigned int &n, vector<qfeOpacityMapping> &mG);

  qfeReturnStatus qfeGetColorMapRGBSize(unsigned int &n);
  qfeReturnStatus qfeGetColorMapASize(unsigned int &n);

  qfeReturnStatus qfeGetColorMapInterpolation(int &type);
  qfeReturnStatus qfeSetColorMapInterpolation(int type);

  qfeReturnStatus qfeGetColorMapQuantization(int &value);
  qfeReturnStatus qfeSetColorMapQuantization(int value);

  qfeReturnStatus qfeGetColorMapSpace(int &type);
  qfeReturnStatus qfeSetColorMapSpace(int type);

  qfeReturnStatus qfeGetColorMapConstantLightness(bool &l);
  qfeReturnStatus qfeSetColorMapConstantLightness(bool l);  

  qfeReturnStatus qfeGetColorMapPaddingUniform(bool &p);
  qfeReturnStatus qfeSetColorMapPaddingUniform(bool p);  

  qfeReturnStatus qfeCopyColorMap(qfeColorMap map);

  qfeReturnStatus qfeGetColorMapTextureId(GLuint &id);
  qfeReturnStatus qfeGetGradientMapTextureId(GLuint &id);

  qfeReturnStatus qfeUploadColorMap();
  qfeReturnStatus qfeUploadGradientMap();

  qfeReturnStatus qfeGetAllColorsRGB(vector<qfeColorRGB> &result);

protected :

private :
  unsigned int              nRGB;
  vector<qfeRGBMapping>     mRGB;

  unsigned int              nA;
  vector<qfeOpacityMapping> mA;

  unsigned int              nG;
  vector<qfeOpacityMapping> mG;

  int                       interpolation;
  int                       quantization;
  int                       colorspace;
  bool                      constantlightness;
  bool                      paddinguniform;
  
  GLuint                    texId, texGradId;
  bool                      texUploaded, texGradUploaded;

  qfeReturnStatus qfeComputeOpacityLinearGradient(vector< qfeOpacityMapping > map, vector< qfeFloat > &table, int tablesize);
  qfeReturnStatus qfeComputeColorLinearGradient(vector< qfeRGBMapping > map, vector< qfeColorRGB > &table, int tablesize, bool constlight);
};
