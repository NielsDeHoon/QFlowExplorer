#include "qfeVtkEngine.h"

vtkStandardNewMacro(vtkDriverMapper);

qfeVtkEngine* qfeVtkEngine::singleton     = NULL;
bool          qfeVtkEngine::singletonFlag = false;

//----------------------------------------------------------------------------
qfeVtkEngine* qfeVtkEngine::qfeGetInstance()
{
  if(!singletonFlag)
  {
    singleton     = new qfeVtkEngine();
    singletonFlag = true;

    return singleton;
  }
  else
  {
    return singleton;
  }
}

//----------------------------------------------------------------------------
void qfeVtkEngine::qfeDeleteInstance()
{
  singletonFlag  = false;
  delete singleton;
}

//----------------------------------------------------------------------------
qfeVtkEngine::qfeVtkEngine()
{
  // Wrap VTK render engine
  // Dummy data and volume geometry are necessary to initiate the vtk volume mapper
  this->canvas     = vtkSmartPointer<vtkMedicalCanvas>::New();
  this->canvas->SetLayoutToOrientedLeft2();
  this->canvas->SetToFullScreen(this->canvas->GetSubCanvas3D());  

  this->renderer[0]     = this->canvas->GetSubCanvas3D()->GetRenderer();
  this->renderer[1]     = this->canvas->GetSubCanvas2D(0)->GetRenderer();
  this->renderer[2]     = this->canvas->GetSubCanvas2D(1)->GetRenderer();
  this->renderer[3]     = this->canvas->GetSubCanvas2D(2)->GetRenderer();
  
  this->data            = vtkSmartPointer<vtkImageData>::New();
  
  this->layoutClickable = false;

  for(int i=0; i<4; i++)
  {   
    this->mapper[i]   = vtkSmartPointer<vtkDriverMapper>::New();
    this->volume[i]   = vtkSmartPointer<vtkVolume>::New();
    volume[i]->SetScale(1000);

    this->mapper[i]->SetInput(data);
    this->volume[i]->SetMapper(this->mapper[i]);
    this->renderer[i]->AddVolume(this->volume[i]);
  } 
  
  this->cbResize   = NULL;

  const double angleSensitivity=0.02;
  const double translationSensitivity=0.001;
  
  this->interactor      = NULL;
  this->interactorStyle = qfeVtkInteractorStyle::New();  

  this->interactorStyleTDx = static_cast<vtkTDxInteractorStyleCamera *>(this->interactorStyle->GetTDxStyle());
  this->interactorStyleTDx->GetSettings()->SetAngleSensitivity(0.02);
  this->interactorStyleTDx->GetSettings()->SetTranslationXSensitivity(translationSensitivity);
  this->interactorStyleTDx->GetSettings()->SetTranslationYSensitivity(translationSensitivity);
  this->interactorStyleTDx->GetSettings()->SetTranslationZSensitivity(translationSensitivity);  

  this->actionCallbackCommand    = NULL;
  this->eventCallbackCommand     = NULL;
  this->renderCallbackCommand[0] = NULL;
  this->renderCallbackCommand[1] = NULL;
  this->renderCallbackCommand[2] = NULL;
  this->renderCallbackCommand[3] = NULL;
  this->resizeCallbackCommand    = NULL;

  this->functionRender           = NULL;
  this->functionRender1          = NULL;
  this->functionRender2          = NULL;
  this->functionRender3          = NULL;

  this->qfeClearCallbacks();

  // Init orientation marker  
  this->orientationCube = vtkAnnotatedCubeActor::New();  
  this->orientationCube->SetXMinusFaceText("A");
  this->orientationCube->SetXPlusFaceText("P");
  this->orientationCube->SetYMinusFaceText("S");
  this->orientationCube->SetYPlusFaceText("I");    
  this->orientationCube->SetZMinusFaceText("L");
  this->orientationCube->SetZPlusFaceText("R");
  this->orientationCube->SetXFaceTextRotation( 90);
  this->orientationCube->SetYFaceTextRotation(-90);
  this->orientationCube->SetZFaceTextRotation(-90);
  this->orientationCube->SetFaceTextScale(0.65);
  
  this->orientationMarker = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
  this->orientationMarker->SetOrientationMarker(this->orientationCube);
  this->orientationMarker->SetViewport(0.0, 0.0, 0.15, 0.15);  
}

//----------------------------------------------------------------------------
qfeVtkEngine::~qfeVtkEngine()
{
  this->actionCallbackCommand->Delete();
  this->eventCallbackCommand->Delete();  
  this->resizeCallbackCommand->Delete();
  this->renderCallbackCommand[0]->Delete();
  this->renderCallbackCommand[1]->Delete();
  this->renderCallbackCommand[2]->Delete();
  this->renderCallbackCommand[3]->Delete();

  this->interactorStyle->Delete();  

  this->Stop();

  // smart pointers take care of destruction of the pointers
}

//----------------------------------------------------------------------------
void qfeVtkEngine::Attach(vtkRenderWindow *rw)
{
  this->canvas->SetRenderWindow(rw);

  this->interactor      = this->canvas->GetRenderWindow()->GetInteractor();
  
  this->interactor->EnableRenderOff();
  this->interactor->Disable();
}

//----------------------------------------------------------------------------
void qfeVtkEngine::Detach(vtkRenderWindow *rw)
{
  this->interactor->EnableRenderOff();
  this->interactor->Disable();
  this->interactor = NULL;

  this->qfeClearCallbacks();
}

//----------------------------------------------------------------------------
// \todo allow setting the timer externally
void qfeVtkEngine::Start()
{ 
  this->actionCallbackCommand = vtkSmartPointer<vtkCallbackCommand>::New();
  this->actionCallbackCommand->SetClientData((void*)this);
  this->actionCallbackCommand->SetCallback(qfeVtkEngine::processActions);

  this->eventCallbackCommand = vtkSmartPointer<vtkCallbackCommand>::New();
  this->eventCallbackCommand->SetClientData((void*)this); 
  this->eventCallbackCommand->SetCallback(qfeVtkEngine::processEvents);

  this->renderCallbackCommand[0] = vtkSmartPointer<vtkCallbackCommand>::New();
  this->renderCallbackCommand[0]->SetClientData((void*)this); 
  this->renderCallbackCommand[0]->SetCallback(qfeVtkEngine::processRender);
  
  this->renderCallbackCommand[1] = vtkSmartPointer<vtkCallbackCommand>::New();
  this->renderCallbackCommand[1]->SetClientData((void*)this); 
  this->renderCallbackCommand[1]->SetCallback(qfeVtkEngine::processRender1);

  this->renderCallbackCommand[2] = vtkSmartPointer<vtkCallbackCommand>::New();
  this->renderCallbackCommand[2]->SetClientData((void*)this); 
  this->renderCallbackCommand[2]->SetCallback(qfeVtkEngine::processRender2);

  this->renderCallbackCommand[3] = vtkSmartPointer<vtkCallbackCommand>::New();
  this->renderCallbackCommand[3]->SetClientData((void*)this); 
  this->renderCallbackCommand[3]->SetCallback(qfeVtkEngine::processRender3);

  this->resizeCallbackCommand = vtkSmartPointer<vtkCallbackCommand>::New();
  this->resizeCallbackCommand->SetClientData((void*)this);
  this->resizeCallbackCommand->SetCallback(qfeVtkEngine::processResize);

  this->interactorStyle->AddObserver(vtkCommand::TimerEvent                   , this->eventCallbackCommand);
  this->interactorStyle->AddObserver(vtkCommand::MouseMoveEvent               , this->eventCallbackCommand);
  this->interactorStyle->AddObserver(vtkCommand::LeftButtonPressEvent         , this->eventCallbackCommand);
  this->interactorStyle->AddObserver(vtkCommand::LeftButtonReleaseEvent       , this->eventCallbackCommand);
  this->interactorStyle->AddObserver(vtkCommand::RightButtonPressEvent        , this->eventCallbackCommand); 
  this->interactorStyle->AddObserver(vtkCommand::RightButtonReleaseEvent      , this->eventCallbackCommand);
  this->interactorStyle->AddObserver(vtkCommand::MouseWheelForwardEvent       , this->eventCallbackCommand); 
  this->interactorStyle->AddObserver(vtkCommand::MouseWheelBackwardEvent      , this->eventCallbackCommand);
  this->interactorStyle->AddObserver(vtkCommand::KeyPressEvent                , this->eventCallbackCommand);  
  this->interactorStyle->AddObserver(qfeVtkInteractorStyle::DoubleClickEvent  , this->eventCallbackCommand);

  this->interactor->AddObserver(vtkCommand::TDxMotionEvent                    , this->eventCallbackCommand);
 
  // Attaching the observer initiates the rendering
  this->mapper[0]->AddObserver(vtkCommand::ProgressEvent, this->renderCallbackCommand[0]);
  this->mapper[1]->AddObserver(vtkCommand::ProgressEvent, this->renderCallbackCommand[1]);
  this->mapper[2]->AddObserver(vtkCommand::ProgressEvent, this->renderCallbackCommand[2]);
  this->mapper[3]->AddObserver(vtkCommand::ProgressEvent, this->renderCallbackCommand[3]);
  
  this->canvas->GetRenderWindow()->AddObserver(vtkCommand::ModifiedEvent, this->resizeCallbackCommand);
  this->canvas->GetRenderWindow()->AddObserver(vtkCommand::UserEvent,     this->actionCallbackCommand);  

  this->interactor->SetInteractorStyle(this->interactorStyle);

  this->orientationMarker->SetDefaultRenderer(this->renderer[0]);
  this->orientationMarker->SetInteractor(this->interactor);  
  this->orientationMarker->EnabledOn();
  this->orientationMarker->InteractiveOff();
  this->qfeOrientationMarkerInit();  
  this->orientationMarker->EnabledOff();
  
  this->interactor->CreateRepeatingTimer(10);
  this->interactor->Enable();
  this->interactor->EnableRenderOn();   
}

//----------------------------------------------------------------------------
void qfeVtkEngine::Stop()
{ 
  this->orientationMarker->EnabledOff();

  this->interactor->EnableRenderOff();
  this->interactor->Disable();
  this->interactor->DestroyTimer();

  this->interactorStyle->RemoveObserver(this->eventCallbackCommand);    
  this->mapper[0]->RemoveObserver(this->renderCallbackCommand[0]);
  this->mapper[1]->RemoveObserver(this->renderCallbackCommand[1]);
  this->canvas->GetRenderWindow()->RemoveObserver(this->actionCallbackCommand);
  this->canvas->GetRenderWindow()->RemoveObserver(this->resizeCallbackCommand);  
  
  /*
  this->actionCallbackCommand->Delete();
  this->actionCallbackCommand = NULL;

  this->eventCallbackCommand->Delete();
  this->eventCallbackCommand = NULL;

  this->renderCallbackCommand[0]->Delete();
  this->renderCallbackCommand[0] = NULL;

  this->resizeCallbackCommand->Delete();
  this->resizeCallbackCommand = NULL;
  */
}

//----------------------------------------------------------------------------
void qfeVtkEngine::GetViewport3D(qfeViewport **vp)
{
  double *viewp;
  int    *windowsize;
  
  viewp      = this->canvas->GetSubCanvas3D()->GetViewport();
  windowsize = this->canvas->GetRenderWindow()->GetSize();

  this->viewport[0].origin[0] = viewp[0] * windowsize[0];
  this->viewport[0].origin[1] = viewp[1] * windowsize[1];
  this->viewport[0].width     = ceil(viewp[2]*windowsize[0] - viewp[0]*windowsize[0]);
  this->viewport[0].height    = ceil(viewp[3]*windowsize[1] - viewp[1]*windowsize[1]);  

  (*vp) = &viewport[0];
}

//----------------------------------------------------------------------------
void qfeVtkEngine::GetViewport2D(int index, qfeViewport **vp)
{
  double *viewp;
  int    *windowsize;

  if(index<0 || index>3)
  {
    this->viewport[index+1].origin[0] = 0.0;
    this->viewport[index+1].origin[1] = 0.0;
    this->viewport[index+1].width     = 0.0;
    this->viewport[index+1].height    = 0.0;    
    return;
  }
  
  viewp   = this->canvas->GetSubCanvas2D(index)->GetViewport();
  windowsize = this->canvas->GetRenderWindow()->GetSize();

  this->viewport[index+1].origin[0] = viewp[0] * windowsize[0];
  this->viewport[index+1].origin[1] = viewp[1] * windowsize[1];
  this->viewport[index+1].width     = ceil(viewp[2]*windowsize[0] - viewp[0]*windowsize[0]);
  this->viewport[index+1].height    = ceil(viewp[3]*windowsize[1] - viewp[1]*windowsize[1]);  

  (*vp) = &viewport[index+1];
}

//----------------------------------------------------------------------------
void qfeVtkEngine::SetLayout(vtkEngineLayout layout, bool fullScreen, bool clickable)
{
  this->layoutClickable = clickable;

  switch(layout)
  {
  case vtkEngineLayoutSingle : this->canvas->SetLayoutToTiled();
                               this->canvas->SetToFullScreen(this->canvas->GetSubCanvas3D());  
                               break;
  case vtkEngineLayoutQuad   : this->canvas->SetLayoutToTiled();
                               break;
  case vtkEngineLayoutLeft   : this->canvas->SetLayoutToOrientedLeft();
                               break;
  case vtkEngineLayoutRight  : this->canvas->SetLayoutToOrientedRight();
                               break;
  case vtkEngineLayoutUS     : this->canvas->SetLayoutToOrientedLeft2();
                               break;
 
  }

  if(fullScreen)
    this->canvas->SetToFullScreen(this->canvas->GetSubCanvas3D());  
}

//----------------------------------------------------------------------------
void qfeVtkEngine::GetViewUp(double &ux, double &uy, double &uz)
{
  double viewup[3];

  this->renderer[0]->GetActiveCamera()->GetViewUp(viewup);

  ux = viewup[0];
  uy = viewup[1];
  uz = viewup[2];
}

//----------------------------------------------------------------------------
void qfeVtkEngine::Rotate(double angle)
{
  this->renderer[0]->GetActiveCamera()->Azimuth(angle);
}

//----------------------------------------------------------------------------
void qfeVtkEngine::Azimuth(double angle)
{
  this->renderer[0]->GetActiveCamera()->Azimuth(angle);
}

//----------------------------------------------------------------------------
void qfeVtkEngine::Elevation(double angle)
{
  this->renderer[0]->GetActiveCamera()->Elevation(angle);
}

//----------------------------------------------------------------------------
void qfeVtkEngine::Elevation(int viewport, double angle)
{
  if(viewport >= 0 && viewport < 4)
    this->renderer[viewport]->GetActiveCamera()->Elevation(angle);
  else
    this->renderer[0]->GetActiveCamera()->Elevation(angle);
}

//----------------------------------------------------------------------------
void qfeVtkEngine::GetViewUp(qfeVector &viewUp)
{
  double viewup[3];

  this->renderer[0]->GetActiveCamera()->GetViewUp(viewup);

  viewUp.x = (qfeFloat)viewup[0];
  viewUp.y = (qfeFloat)viewup[1];
  viewUp.z = (qfeFloat)viewup[2];
}

//----------------------------------------------------------------------------
void qfeVtkEngine::Refresh()
{	
  this->canvas->GetRenderWindow()->GetInteractor()->Render();  
}

//----------------------------------------------------------------------------
void qfeVtkEngine::OrientationMarkerEnable()
{  
  this->orientationMarker->EnabledOn();
  this->orientationMarker->InteractiveOff();
}

//----------------------------------------------------------------------------
void qfeVtkEngine::OrientationMarkerEnable(float xx, float xy, float xz, float yx, float yy, float yz, float zx, float zy, float zz)
{
  vtkSmartPointer<vtkMatrix4x4> t = vtkSmartPointer<vtkMatrix4x4>::New();

  vtkProp *actor = this->orientationMarker->GetOrientationMarker();

  t->Identity();
  t->SetElement(0,0,xx);
  t->SetElement(0,1,xy);
  t->SetElement(0,2,xz);
  t->SetElement(1,0,yx);
  t->SetElement(1,1,yy);
  t->SetElement(1,2,yz);
  t->SetElement(2,0,zx);
  t->SetElement(2,1,zy);
  t->SetElement(2,2,zz);
  vtkMatrix4x4::Invert(t,t);

  if(actor != NULL)
  {
    //this->orientationCube->SetUserMatrix(t); 
    this->orientationCube->GetAssembly()->SetUserMatrix(t); 
    this->orientationCube->Modified();
    this->orientationMarker->Modified();
  }  

  this->OrientationMarkerEnable();
}

//----------------------------------------------------------------------------
void qfeVtkEngine::OrientationMarkerDisable()
{
  this->orientationMarker->EnabledOff();
}

//----------------------------------------------------------------------------
void qfeVtkEngine::SetStatusMessage(std::string text)
{
  qfeCallData cd;
  cd.stepSize = -2;
  cd.message = (char *)malloc(text.length() * sizeof(char));
  strcpy(cd.message, text.c_str());
  this->canvas->GetRenderWindow()->InvokeEvent(vtkCommand::UpdateEvent, &cd);
}

void qfeVtkEngine::StoreScreenshot()
{
	//Ask the user for the resolution:
	QDialog dlg;
	QVBoxLayout *mainLayout = new QVBoxLayout;
	std::vector<QRadioButton*> radioBoxes;
	std::vector<std::pair<unsigned int, unsigned int>> resolutions;
	
	QDialogButtonBox *buttonBox = new QDialogButtonBox();
	buttonBox->setOrientation(Qt::Vertical);
	buttonBox->setCenterButtons(true);

	int *orig_size = this->canvas->GetRenderWindow()->GetSize();  
	
	int orig_x = orig_size[0];
	int orig_y = orig_size[1];
	
	//generate radioboxes
	QRadioButton *currentRes = new QRadioButton();
	QString currentResLabel = QString("Current resolution (")+QString::number(orig_x)+QString(" x ")+QString::number(orig_y)+QString(")");
	currentRes->setText(currentResLabel);
	currentRes->setChecked(false);	
	radioBoxes.push_back(currentRes);
	resolutions.push_back(std::make_pair(orig_x, orig_y));

	//generate radioboxes
	QRadioButton *fullHD = new QRadioButton();
	fullHD->setText("1080p/Full HD (1920 x 1080)");
	fullHD->setChecked(true);	
	radioBoxes.push_back(fullHD);
	resolutions.push_back(std::make_pair(1920, 1080));

	QRadioButton *UHD = new QRadioButton();
	UHD->setText("4K/UHD (3840 x 2160)");
	UHD->setChecked(false);	
	radioBoxes.push_back(UHD);
	resolutions.push_back(std::make_pair(3840, 2160));

	QRadioButton *eightK = new QRadioButton();
	eightK->setText("8K (7680 x 4320)");
	eightK->setChecked(false);	
	radioBoxes.push_back(eightK);
	resolutions.push_back(std::make_pair(7680, 4320));

	QRadioButton *custom = new QRadioButton();
	custom->setText("Custom");
	custom->setChecked(false);	
	radioBoxes.push_back(custom);
	resolutions.push_back(std::make_pair(0, 0));

	for(unsigned int i = 0; i<radioBoxes.size(); i++)
	{
		buttonBox->addButton(radioBoxes[i], QDialogButtonBox::ActionRole);
	}

	mainLayout->addWidget(buttonBox);
	
	//generate custom input box
	QRegExpValidator* regexPositiveInteger = new QRegExpValidator(QRegExp("\\d*"), 0); // only positive integers are allowed

	QLabel *custom_input_label_x = new QLabel();
	custom_input_label_x->setText("X:");

	QLineEdit *custom_input_x = new QLineEdit();
	custom_input_x->setText("3840");	
	custom_input_x->setValidator(regexPositiveInteger);

	mainLayout->addWidget(custom_input_label_x);
	mainLayout->addWidget(custom_input_x);

	QLabel *custom_input_label_y = new QLabel();
	custom_input_label_y->setText("Y:");

	QLineEdit *custom_input_y = new QLineEdit();
	custom_input_y->setText("2160");	
	custom_input_y->setValidator(regexPositiveInteger);

	mainLayout->addWidget(custom_input_label_y);
	mainLayout->addWidget(custom_input_y);

	QDialogButtonBox *buttonBoxControl = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	QObject::connect(buttonBoxControl, SIGNAL(accepted()), &dlg, SLOT(accept()));
	QObject::connect(buttonBoxControl, SIGNAL(rejected()), &dlg, SLOT(reject()));

	mainLayout->addWidget(buttonBoxControl);

	dlg.setLayout(mainLayout);

	std::pair<unsigned int, unsigned int> screenshot_resolution = std::make_pair(0, 0);

	if(dlg.exec() == QDialog::Accepted)
	{
		for(unsigned int i = 0; i<radioBoxes.size(); i++)
		{
			if(radioBoxes[i]->isChecked())
			{
				screenshot_resolution = resolutions.at(i);
				if(i == radioBoxes.size()-1)//last radiobox is "custom"
				{
					screenshot_resolution.first = custom_input_x->text().toInt();					
					screenshot_resolution.second = custom_input_y->text().toInt();					
				}
			}
		}
	}
	
	if(screenshot_resolution.first != 0 && screenshot_resolution.second != 0)
		this->StoreScreenshot(screenshot_resolution.first, screenshot_resolution.second);
}

void qfeVtkEngine::StoreScreenshot(unsigned int pixel_width, unsigned int pixel_height)
{
  //update and store screen values
  //int border_option = this->canvas->GetRenderWindow()->GetBorders();
  //this->canvas->GetRenderWindow()->SetBorders(0);

  int *orig_size = this->canvas->GetRenderWindow()->GetSize();  

  int orig_x = orig_size[0];
  int orig_y = orig_size[1];

  double *viewPort = this->canvas->GetSubCanvas3D()->GetViewport();

  this->canvas->GetRenderWindow()->SetSize(pixel_width, pixel_height);

  this->canvas->GetRenderWindow()->OffScreenRenderingOn();

  this->canvas->GetRenderWindow()->Render();

  // Screenshot  
   vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = 
    vtkSmartPointer<vtkWindowToImageFilter>::New();
  windowToImageFilter->SetInput(this->canvas->GetRenderWindow());
  //windowToImageFilter->SetMagnification(1); //set the resolution of the output image ("quality" times the current resolution of vtk render window)
  windowToImageFilter->SetInputBufferTypeToRGBA(); //also record the alpha (transparency) channel
  windowToImageFilter->ReadFrontBufferOff(); // read from the back buffer
  windowToImageFilter->Update();
 
  static int screenshot_id = 0;
  std::ostringstream strs;
  if(screenshot_id<10)
	  strs << "00" << screenshot_id;
  else if(screenshot_id<100)
	  strs << "0" << screenshot_id;
  else
	  strs << screenshot_id;
  std::string filename = "screenshot-"+strs.str()+".png";
  screenshot_id++;

  vtkSmartPointer<vtkPNGWriter> writer = 
    vtkSmartPointer<vtkPNGWriter>::New();
  writer->SetFileName(filename.c_str());
  writer->SetInputConnection(windowToImageFilter->GetOutputPort());
  writer->Write();

  //reset screen values:
  //this->canvas->GetRenderWindow()->SetBorders(border_option);
  
  this->canvas->GetRenderWindow()->SetSize(orig_x, orig_y);
  this->canvas->GetRenderWindow()->OffScreenRenderingOff();
  this->canvas->GetRenderWindow()->SetSize(orig_x, orig_y);

  this->canvas->GetRenderWindow()->Render();
  this->Refresh();
  
  std::cout<<"Screenshot: \""<<filename.c_str()<<"\" is saved"<<std::endl;
}

//----------------------------------------------------------------------------
void qfeVtkEngine::processActions(vtkObject* object, unsigned long event, void* clientdata, void* calldata)
{
  qfeVtkEngine* self  = reinterpret_cast<qfeVtkEngine *>( clientdata );

  int actionType = *(reinterpret_cast<int *>(calldata));  

  if(event == vtkCommand::UserEvent)
  {    
    if(self->functionAction != NULL) self->functionAction(self->callerAction, actionType);               
  }
}

//----------------------------------------------------------------------------
void qfeVtkEngine::processEvents(vtkObject* object, unsigned long event, void* clientdata, void* calldata)
{  
  qfeVtkEngine* self  = reinterpret_cast<qfeVtkEngine *>( clientdata );
  vtkRenderWindowInteractor *interactor;  

  interactor      = self->canvas->GetRenderWindow()->GetInteractor();  

  int x, y;
  int v;

  switch(event)
    {
    case vtkCommand::TimerEvent: 
      if(self->functionTimer != NULL) self->functionTimer(self->callerTimer);   
      
      break;

    case vtkCommand::MouseMoveEvent: 
      x = interactor->GetEventPosition()[0];
      y = interactor->GetEventPosition()[1];
      v = self->canvas->GetSubCanvasIndexAt(x,y);
      if(self->functionMouseMove != NULL) self->functionMouseMove(self->callerMouseMove, x, y, v);      
      break;

    case vtkCommand::LeftButtonPressEvent: 
      x = interactor->GetEventPosition()[0];
      y = interactor->GetEventPosition()[1];
      v = self->canvas->GetSubCanvasIndexAt(x,y);
      if(self->functionMouseLeftDown != NULL) self->functionMouseLeftDown(self->callerMouseLeftDown, x, y, v); 
      break;     

    case vtkCommand::LeftButtonReleaseEvent:
      x = interactor->GetEventPosition()[0];
      y = interactor->GetEventPosition()[1];
      v = self->canvas->GetSubCanvasIndexAt(x,y);
      if(self->functionMouseLeftUp != NULL) self->functionMouseLeftUp(self->callerMouseLeftUp, x, y, v); 
      break;

    case vtkCommand::MiddleButtonPressEvent:
      cout << "event middle mouse button" << endl;
      break;

    case vtkCommand::MiddleButtonReleaseEvent:
      cout << "event middle mouse button" << endl;
      break;

    case vtkCommand::RightButtonPressEvent:      
      x = interactor->GetEventPosition()[0];
      y = interactor->GetEventPosition()[1];
      v = self->canvas->GetSubCanvasIndexAt(x,y);
      if(self->functionMouseRightDown != NULL) self->functionMouseRightDown(self->callerMouseRightDown, x, y, v);         
      break;

    case vtkCommand::RightButtonReleaseEvent: 
      x = interactor->GetEventPosition()[0];
      y = interactor->GetEventPosition()[1];
      v = self->canvas->GetSubCanvasIndexAt(x,y);
      if(self->functionMouseRightUp != NULL) self->functionMouseRightUp(self->callerMouseRightUp, x, y, v); 
      break;

    case vtkCommand::MouseWheelForwardEvent:
      x = interactor->GetEventPosition()[0];
      y = interactor->GetEventPosition()[1];
      v = self->canvas->GetSubCanvasIndexAt(x,y);
      if(self->functionWheelForward != NULL) self->functionWheelForward(self->callerWheelForward, x, y, v);
      break;

    case vtkCommand::MouseWheelBackwardEvent: 
      x = interactor->GetEventPosition()[0];
      y = interactor->GetEventPosition()[1];
      v = self->canvas->GetSubCanvasIndexAt(x,y);
      if(self->functionWheelBackward != NULL) self->functionWheelBackward(self->callerWheelBackward, x, y, v);
      break;

    case vtkCommand::KeyPressEvent:      
      if(self->functionKeyPress != NULL) self->functionKeyPress(self->callerKeyPress, self->interactor->GetKeySym());
      break;

    case vtkCommand::KeyReleaseEvent: 
      cout << "event key release" << endl;
      break;

    case vtkCommand::CharEvent:  
      cout << "event char" << endl;
      break;

    case vtkCommand::TDxMotionEvent:
      cout << "motion event" << endl;
      break;

    case qfeVtkInteractorStyle::DoubleClickEvent:
      x = interactor->GetEventPosition()[0];
      y = interactor->GetEventPosition()[1];
      self->handleDoubleClick(x,y);
      break;
   }     
}

//----------------------------------------------------------------------------
void qfeVtkEngine::processRender(vtkObject* object, unsigned long event, void* clientdata, void* calldata)
{
   qfeVtkEngine* self  = reinterpret_cast<qfeVtkEngine *>( clientdata );

   if(event == vtkCommand::ProgressEvent)
   {    
     if(self->functionRender != NULL) self->functionRender(self->callerRender[0]);           
   }
}

//----------------------------------------------------------------------------
void qfeVtkEngine::processRender1(vtkObject* object, unsigned long event, void* clientdata, void* calldata)
{
   qfeVtkEngine* self  = reinterpret_cast<qfeVtkEngine *>( clientdata );

   if(event == vtkCommand::ProgressEvent)
   {    
     if(self->functionRender1 != NULL) self->functionRender1(self->callerRender[1]);           
   }
}

//----------------------------------------------------------------------------
void qfeVtkEngine::processRender2(vtkObject* object, unsigned long event, void* clientdata, void* calldata)
{
   qfeVtkEngine* self  = reinterpret_cast<qfeVtkEngine *>( clientdata );

   if(event == vtkCommand::ProgressEvent)
   {    
     if(self->functionRender2 != NULL) self->functionRender2(self->callerRender[2]);           
   }
}

//----------------------------------------------------------------------------
void qfeVtkEngine::processRender3(vtkObject* object, unsigned long event, void* clientdata, void* calldata)
{
   qfeVtkEngine* self  = reinterpret_cast<qfeVtkEngine *>( clientdata );

   if(event == vtkCommand::ProgressEvent)
   {    
     if(self->functionRender3 != NULL) self->functionRender3(self->callerRender[3]);           
   }
}

//----------------------------------------------------------------------------
void qfeVtkEngine::processResize(vtkObject* object, unsigned long event, void* clientdata, void* calldata)
{
  qfeVtkEngine* self  = reinterpret_cast<qfeVtkEngine *>( clientdata );

  if(event == vtkCommand::ModifiedEvent)
  {    
    if(self->functionResize != NULL) self->functionResize(self->callerResize);           
  }
}

//----------------------------------------------------------------------------
void qfeVtkEngine::qfeOrientationMarkerInit()
{
  int     markerSize;
  int    *windowSize;
  double *viewportSize;
  double  viewport[4], viewportMarker[4];

  if(this->orientationMarker == NULL || this->canvas == NULL) return;

  markerSize = 60;

  windowSize   = this->canvas->GetRenderWindow()->GetSize();
  viewportSize = this->canvas->GetSubCanvas3D()->GetViewport();

  viewport[0] = viewportSize[0] * windowSize[0]; // origin x
  viewport[1] = viewportSize[1] * windowSize[1]; // origin y
  viewport[2] = ceil(viewportSize[2]*windowSize[0] - viewportSize[0]*windowSize[0]); // width
  viewport[3] = ceil(viewportSize[3]*windowSize[1] - viewportSize[1]*windowSize[1]); // height

  viewportMarker[0] = (viewport[0] + viewport[2] - markerSize)/(float)windowSize[0];
  viewportMarker[1] = (viewport[1])                           /(float)windowSize[1];
  viewportMarker[2] = (viewport[0] + viewport[2])             /(float)windowSize[0];
  viewportMarker[3] = (viewport[1] + markerSize)              /(float)windowSize[1];

  this->orientationMarker->SetViewport(viewportMarker[0], viewportMarker[1], viewportMarker[2], viewportMarker[3]);
}

//----------------------------------------------------------------------------
void qfeVtkEngine::SetObserverAction(void *caller, void (*f)(void * c, int t)) 
{
  this->functionAction = f; 
  this->callerAction   = caller;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetObserverRender(void *caller, void (*f)(void * c)) 
{
  this->functionRender  = f; 
  this->callerRender[0] = caller;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetObserverRender1(void *caller, void (*f)(void * c)) 
{
  this->functionRender1 = f; 
  this->callerRender[1] = caller;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetObserverRender2(void *caller, void (*f)(void * c)) 
{
  this->functionRender2 = f; 
  this->callerRender[2] = caller;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetObserverRender3(void *caller, void (*f)(void * c)) 
{
  this->functionRender3 = f; 
  this->callerRender[3] = caller;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetObserverResize(void *caller, void (*f)(void * c)) 
{
  this->functionResize = f; 
  this->callerResize   = caller;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetObserverTimer(void *caller, void (*f)(void * c)) 
{
  this->functionTimer = f; 
  this->callerTimer   = caller;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseMove(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction) 
{
  this->functionMouseMove = f; 
  this->callerMouseMove   = caller;
  this->interactorStyle->handleParentMove = parentInteraction;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseLeftButtonDown(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction) 
{
  this->functionMouseLeftDown = f; 
  this->callerMouseLeftDown   = caller;
  this->interactorStyle->handleParentLeftDown = parentInteraction;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseLeftButtonUp(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction) 
{
  this->functionMouseLeftUp = f; 
  this->callerMouseLeftUp   = caller;
  this->interactorStyle->handleParentLeftUp = parentInteraction;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseRightButtonDown(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction) 
{
  this->functionMouseRightDown = f; 
  this->callerMouseRightDown   = caller;
  this->interactorStyle->handleParentRightDown = parentInteraction;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseRightButtonUp(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction) 
{
  this->functionMouseRightUp = f; 
  this->callerMouseRightUp   = caller;
  this->interactorStyle->handleParentRightUp = parentInteraction;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseWheelForward(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction) 
{
  this->functionWheelForward = f; 
  this->callerWheelForward   = caller;
  this->interactorStyle->handleParentWheelForward = parentInteraction;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseWheelBackward(void *caller, void (*f)(void * c, int x, int y, int v), bool parentInteraction) 
{
  this->functionWheelBackward = f; 
  this->callerWheelBackward   = caller;
  this->interactorStyle->handleParentWheelBackward = parentInteraction;
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionKeyPress(void *caller, void (*f)(void * c, char * k), bool parentInteraction)
{
  this->functionKeyPress = f;
  this->callerKeyPress   = caller;
  this->interactorStyle->handleParentKeyPress = parentInteraction;
}

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseMove(void *caller, void (*f)(void * c, int x, int y, int v)) 
{
  this->SetInteractionMouseMove(caller, f, true);
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseLeftButtonDown(void *caller, void (*f)(void * c, int x, int y, int v)) 
{
  this->SetInteractionMouseLeftButtonDown(caller, f, true);
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseLeftButtonUp(void *caller, void (*f)(void * c, int x, int y, int v)) 
{
  this->SetInteractionMouseLeftButtonUp(caller, f, true);
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseRightButtonDown(void *caller, void (*f)(void * c, int x, int y, int v)) 
{
  this->SetInteractionMouseRightButtonDown(caller, f, true);
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseRightButtonUp(void *caller, void (*f)(void * c, int x, int y, int v)) 
{
  this->SetInteractionMouseRightButtonUp(caller, f, true);
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseWheelForward(void *caller, void (*f)(void * c, int x, int y, int v)) 
{
  this->SetInteractionMouseWheelForward(caller, f, true);
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionMouseWheelBackward(void *caller, void (*f)(void * c, int x, int y, int v)) 
{
  this->SetInteractionMouseWheelBackward(caller, f, true);
};

//----------------------------------------------------------------------------
void qfeVtkEngine::SetInteractionKeyPress(void *caller, void (*f)(void * c, char * k))
{
  this->SetInteractionKeyPress(caller, f, true);
}

//----------------------------------------------------------------------------
void qfeVtkEngine::CallInteractionParentMouseMove()
{
  if(this->interactorStyle == NULL) return;
  this->interactorStyle->OnParentMouseMove();
};

//----------------------------------------------------------------------------
void qfeVtkEngine::CallInteractionParentLeftButtonDown()
{
  if(this->interactorStyle == NULL) return;
  this->interactorStyle->OnParentLeftButtonDown();
};

//----------------------------------------------------------------------------
void qfeVtkEngine::CallInteractionParentLeftButtonUp()
{
  if(this->interactorStyle == NULL) return;
  this->interactorStyle->OnParentLeftButtonUp();
};

//----------------------------------------------------------------------------
void qfeVtkEngine::CallInteractionParentRightButtonDown()
{
  if(this->interactorStyle == NULL) return;
  this->interactorStyle->OnParentRightButtonDown();
};

//----------------------------------------------------------------------------
void qfeVtkEngine::CallInteractionParentRightButtonUp()
{
  if(this->interactorStyle == NULL) return;
  this->interactorStyle->OnParentRightButtonUp();
};

//----------------------------------------------------------------------------
void qfeVtkEngine::CallInteractionParentWheelForward()
{
  if(this->interactorStyle == NULL) return;
  this->interactorStyle->OnParentWheelForward();
};

//----------------------------------------------------------------------------
void qfeVtkEngine::CallInteractionParentWheelBackward()
{
  if(this->interactorStyle == NULL) return;
  this->interactorStyle->OnParentWheelBackward();
};

//----------------------------------------------------------------------------
bool qfeVtkEngine::GetInteractionControlKey() 
{
  if(this->interactor == NULL) return false;
  
  return (this->interactor->GetControlKey() == 1) ? true : false;
}

//----------------------------------------------------------------------------
bool qfeVtkEngine::GetInteractionAltKey() 
{
  if(this->interactor == NULL) return false;
  
  return (this->interactor->GetAltKey() == 1) ? true : false;
}

//----------------------------------------------------------------------------
void qfeVtkEngine::handleDoubleClick(int x, int y)
{
  vtkSubCanvas *subCanvas = this->canvas->GetSubCanvasAt( x, y );

	if( !this->layoutClickable || subCanvas == 0 )
		return;

  int layout = this->canvas->GetLayout();

	if(this->canvas->IsFullScreen())
	  this->canvas->SetLayout(layout);
	else
	  this->canvas->SetToFullScreen(subCanvas);

  this->processResize((vtkObject*)this->canvas->GetRenderWindow(), vtkCommand::ModifiedEvent, (void*)this, NULL);
  //this->processResize((vtkObject*)this->canvas->GetRenderWindow(), vtkCommand::ModifiedEvent, (void*)this, NULL);
  //this->processResize((vtkObject*)this->canvas->GetRenderWindow(), vtkCommand::ModifiedEvent, (void*)this, NULL);
  //this->ren->GetRenderWindow()->Modified();  

  //this->canvas->GetRenderWindow()->InvokeEvent(vtkCommand::UserEvent+101);
  //this->canvas->Modified();
}

//----------------------------------------------------------------------------
void qfeVtkEngine::qfeClearCallbacks()
{
  this->functionTimer          = NULL;  
  this->functionMouseMove      = NULL;
  this->functionMouseLeftDown  = NULL;
  this->functionMouseLeftUp    = NULL;
  this->functionMouseRightDown = NULL;
  this->functionMouseRightUp   = NULL;
  this->functionWheelForward   = NULL;
  this->functionWheelBackward  = NULL;
  this->functionKeyPress       = NULL;

  this->callerRender[0]        = NULL;
  this->callerRender[1]        = NULL;
  this->callerRender[2]        = NULL;
  this->callerRender[3]        = NULL;
  this->callerTimer            = NULL;  
  this->callerMouseMove        = NULL;
  this->callerMouseLeftDown    = NULL;
  this->callerMouseLeftUp      = NULL;
  this->callerMouseRightDown   = NULL;
  this->callerMouseRightUp     = NULL;
  this->callerWheelForward     = NULL;
  this->callerWheelBackward    = NULL;
  this->callerKeyPress         = NULL;  

  this->interactorStyle->handleParentMove          = true;
  this->interactorStyle->handleParentLeftDown      = true;
  this->interactorStyle->handleParentLeftUp        = true;
  this->interactorStyle->handleParentRightDown     = true;
  this->interactorStyle->handleParentRightUp       = true;
  this->interactorStyle->handleParentWheelForward  = true;
  this->interactorStyle->handleParentWheelBackward = true;
  this->interactorStyle->handleParentKeyPress      = true;
}



