#pragma once

#include "qflowexplorer.h"

enum qfeMeasurementType
{  
  qfeMeasurementProbe2D,
  qfeMeasurementProbe3D
};

/**
* \file   qfeMeasurement.h
* \author Roy van Pelt
* \class  qfeMeasurement
* \brief  Abstract class for measurements
* \note   Confidential
*
* An abstract class for various measurements.
* Each patient study can contain a series of measurements.
*/
class qfeMeasurement
{
public:
  qfeMeasurement();
  qfeMeasurement(const qfeMeasurement &measurement);
  ~qfeMeasurement();
  
  virtual qfeReturnStatus qfeGetMeasurementType(int& type);

protected:
  int measurementType;

};

/**
* \file   qfeMeasurement.h
* \author Roy van Pelt
* \class  qfeMeasurement2D
* \brief  Abstract class for 2D measurements
* \note   Confidential
*
* Each patient study can contain a series of measurements.
*/
class qfeMeasurement2D : public qfeMeasurement
{
public:
  qfeMeasurement2D();
  qfeMeasurement2D(const qfeMeasurement2D &measurement);
  ~qfeMeasurement2D();
  
  virtual qfeReturnStatus qfeGetMeasurementType(int& type);

protected:
  int measurementType;

};
