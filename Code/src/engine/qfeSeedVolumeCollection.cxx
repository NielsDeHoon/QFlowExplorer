#include "qfeSeedVolumeCollection.h"

qfeSeedVolumeCollection::qfeSeedVolumeCollection(qfeStudy *_study)
{
	qfeColorMap seedVolumeColorMap;
	qfeColorMaps seedVolumeColorMaps;
	seedVolumeColorMap.qfeUploadColorMap();
	std::string name;
	seedVolumeColorMaps.qfeGetColorMap(21, name, seedVolumeColorMap); 
	seedVolumeColorMap.qfeGetAllColorsRGB(seedVolumeColorMapColors);

	sourceColor = qfeColorRGB(9.0f/255.0f, 141.0f/255.0f, 129.0f/255.0f);
	sinkColor = qfeColorRGB(255.0f/255.0f, 142.0f/255.0f, 47.0f/255.0f);

	this->study = _study;
	
	this->qfeUpdate();
}

qfeColorRGB qfeSeedVolumeCollection::qfeGetSeedVolumeColor(int i)
{
	float index = (float)i/(seedVolumes.size()-1);

	qfeColorRGB col = seedVolumeColorMapColors.at(
		(unsigned int) floor(
		((float)seedVolumeColorMapColors.size()-1) * index
		)
	);

	return col;	
}

qfeColorRGB qfeSeedVolumeCollection::qfeGetSelectedSeedVolumeColor(seedVolumeGuidePoint selected_point)
{
	for(unsigned int i = 0; i<this->seedVolumes.size(); i++)
	{
		if(selected_point.index == seedVolumes[i].unique_id)
		{
			return qfeGetSeedVolumeColor(i);
		}
	}

	return qfeColorRGB(0.5,0.5,0.5);	
}

float qfeSeedVolumeCollection::qfeGetColorIndex(unsigned int _unique_id)
{
	for(unsigned int i = 0; i<this->seedVolumes.size(); i++)
	{
		if(seedVolumes[i].unique_id == _unique_id)
		{
			return (float) i/(float)(this->seedVolumes.size()-1);
		}
	}

	return 0.0f;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeRenderSeedVolumes(vtkCamera *camera, int vp_width, int vp_height, seedVolumeGuidePoint selected_point, bool renderSurface)
{
	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);

	for(unsigned int i = 0; i<this->seedVolumes.size(); i++)
	{
		qfeColorRGB col = this->qfeGetSeedVolumeColor(i);

		if(seedVolumes[i].type != seedVolumeTypeUnknown)
		{
			switch(seedVolumes[i].type)
			{
			case seedVolumeTypeSource:
				col = sourceColor;;
				break;
			case seedVolumeTypeSink:
				col = sinkColor;
				break;
			default:
				col = this->qfeGetSeedVolumeColor(i);
				break;
			}
		}

		seedVolumes[i].volume->qfeSetVtkCamera(camera, vp_width, vp_height);

		bool vol_selected = false;
		if(selected_point.index == seedVolumes[i].unique_id)
		{
			seedVolumes[i].volume->qfeRenderVolume(col, true, true, true);
			vol_selected = true;
		}
		else
		{
			seedVolumes[i].volume->qfeRenderVolume(col, renderSurface, false, true);
		}

		qfePoint _dia1;
		qfePoint _dia2;
		qfePoint _apex;
		seedVolumes[i].volume->qfeGetGuidePoints(_dia1, _dia2, _apex);

		if(vol_selected && selected_point.type == apex)
		{
			qfeRenderCube(_apex, 1.0, true, qfeColorRGB(1.0, 0.0, 0.0));
		}
		else
		{
			qfeRenderCube(_apex, 1.0, true, col);
		}

		//Render the diameter cubes darker so we can distinguish them
		col.r/=2.0;
		col.g/=2.0;
		col.b/=2.0;

		if(vol_selected && selected_point.type == diameter1)
		{
			qfeRenderCube(_dia1, 1.0, true, qfeColorRGB(1.0, 0.0, 0.0));
		}
		else
		{
			qfeRenderCube(_dia1, 1.0, true, col);
		}
		
		if(vol_selected && selected_point.type == diameter2)
		{
			qfeRenderCube(_dia2, 1.0, true, qfeColorRGB(1.0, 0.0, 0.0));
		}
		else
		{
			qfeRenderCube(_dia2, 1.0, true, col);
		}
	}
	
	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeResetCounters()
{
  for(unsigned int i = 0; i < this->seedVolumes.size(); i++)
  {
	  this->seedVolumes[i].counter = 0;
  }

  return qfeSuccess;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeAddSeedVolume(qfePoint dia1, qfePoint dia2, qfePoint apex, seedVolumeShape shape)
{	
	this->qfeAddSeedVolume(dia1, dia2, apex, shape, seedVolumeTypeUnknown);

	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeAddSeedVolume(qfePoint dia1, qfePoint dia2, qfePoint apex, seedVolumeShape shape, seedVolumeType type)
{	
	static unsigned int seedVolumeNextID = 10; //to ensure the ID does not interfere with for example the particle source
	
	seedVolume newSeedVolume;
	switch(shape)
	{
	case EllipsoidType:
		newSeedVolume.volume = new qfeSeedEllipsoid();
		break;
	case CylinderType:
		newSeedVolume.volume = new qfeSeedCylinder();
	default:
		newSeedVolume.volume = new qfeSeedCylinder();
	}

	newSeedVolume.unique_id = seedVolumeNextID;
	newSeedVolume.counter = 0;
	newSeedVolume.volume->qfeSetGuidePoints(dia1, dia2, apex);

	newSeedVolume.volumeShape = shape;
	newSeedVolume.type = type;

	seedVolumes.append(newSeedVolume);

	seedVolumeNextID++;

	this->qfeUpdate();

	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeRemoveSeedVolume(unsigned int index)
{	
	for(unsigned int i = 0; i<this->seedVolumes.size(); i++)
	{
		if(seedVolumes[i].unique_id == index)
		{
			delete seedVolumes.at(i).volume;
			seedVolumes.removeAt(i);

			this->qfeUpdate();

			return qfeSuccess;
		}
	}

	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeUpdateSeedVolumeGuidePoint(unsigned int index, qfePoint point, guidePointType type)
{
	for(unsigned int i = 0; i<this->seedVolumes.size(); i++)
	{
		if(seedVolumes.at(i).unique_id == index)
		{
			seedVolumes[i].counter = 0;

			qfePoint _dia1;
			qfePoint _dia2;
			qfePoint _apex;
			
			this->seedVolumes[i].volume->qfeGetGuidePoints(_dia1, _dia2, _apex);

			if(type == diameter1)
			{
				_dia1 = point;
			}
			if(type == diameter2)
			{
				_dia2 = point;
			}
			if(type == apex)
			{
				_apex = point;
			}

			seedVolumes[i].volume->qfeSetGuidePoints(_dia1, _dia2, _apex);

			this->qfeUpdate();

			return qfeSuccess;
		}
	}
	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeSquishSeedVolume(unsigned int index)
{
	for(unsigned int i = 0; i<this->seedVolumes.size(); i++)
	{
		if(seedVolumes.at(i).unique_id == index)
		{
			seedVolumes[i].volume->qfeSquish();

			this->qfeUpdate();

			return qfeSuccess;
		}
	}

	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeStretchSeedVolume(unsigned int index)
{
	for(unsigned int i = 0; i<this->seedVolumes.size(); i++)
	{
		if(seedVolumes.at(i).unique_id == index)
		{
			seedVolumes[i].volume->qfeStretch();

			this->qfeUpdate();

			return qfeSuccess;
		}
	}

	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeGetAllGuidePoints(std::vector<seedVolumeGuidePoint> &pointList)
{
	for(unsigned int i = 0; i<this->seedVolumes.size(); i++)
	{
		qfePoint _dia1;
		qfePoint _dia2;
		qfePoint _apex;

		this->seedVolumes[i].volume->qfeGetGuidePoints(_dia1, _dia2, _apex);

		seedVolumeGuidePoint point1;
		point1.point = _dia1;
		point1.index = this->seedVolumes[i].unique_id;
		point1.type = diameter1;
			
		seedVolumeGuidePoint point2;
		point2.point = _dia2;
		point2.index = this->seedVolumes[i].unique_id;
		point2.type = diameter2;

		seedVolumeGuidePoint point3;
		point3.point = _apex;
		point3.index = this->seedVolumes[i].unique_id;
		point3.type = apex;

		pointList.push_back(point1);
		pointList.push_back(point2);
		pointList.push_back(point3);
	}

	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeSaveSeedVolumes()
{
	if(seedVolumes.size() == 0)
	{
		return qfeSuccess;
	}

	QFileDialog fileDialog(0, "Save file", QDir::currentPath(), "*.particle_counters");
    fileDialog.selectNameFilter("*.particle_counters");
	fileDialog.setAcceptMode(QFileDialog::AcceptSave);
	fileDialog.setDefaultSuffix("particle_counters");
    fileDialog.exec();

	if(fileDialog.selectedFiles().size() != 0)
	{
		QString fileName = fileDialog.selectedFiles().first();

		QFile file(fileName);
		file.open( QIODevice::WriteOnly );

		file.resize(0); //clear the file

		// store data in f
		QTextStream stream(&file);
		for(unsigned int i = 0; i<this->seedVolumes.size(); i++)
		{
			seedVolume volume = this->seedVolumes.at(i);

			qfePoint _dia1 = volume.volume->qfeGetDia1();
			qfePoint _dia2 = volume.volume->qfeGetDia2();
			qfePoint _apex = volume.volume->qfeGetApex();

			int shape = volume.volumeShape;
			int type = volume.type;

			stream <<_dia1.x<<";"<<_dia1.y<<";"<<_dia1.z<<";";
			stream <<_dia2.x<<";"<<_dia2.y<<";"<<_dia2.z<<";";
			stream <<_apex.x<<";"<<_apex.y<<";"<<_apex.z<<";";
			stream << shape<<";"<<type<<endl;

		}

		file.close();
	}

	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeLoadSeedVolumes()
{
	QFileDialog fileDialog(0, "Load file", QDir::currentPath(), "*.particle_counters");
    fileDialog.selectNameFilter("*.particle_counters");
	fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    fileDialog.exec();

	if(fileDialog.selectedFiles().size() != 0)
	{
		QString fileName = fileDialog.selectedFiles().first();

		QFile file(fileName);
		file.open( QIODevice::ReadOnly );

		// read data in f
		QTextStream stream(&file);
        while(!stream.atEnd())
		{
			QString line = stream.readLine();
			QStringList list = line.split(";");

			if(list.size()!=10 && list.size()!=11)
				continue;

			qfePoint _dia1;
			qfePoint _dia2;
			qfePoint _apex;

			_dia1.x = list[0].toFloat();
			_dia1.y = list[1].toFloat();
			_dia1.z = list[2].toFloat();

			_dia2.x = list[3].toFloat();
			_dia2.y = list[4].toFloat();
			_dia2.z = list[5].toFloat();

			_apex.x = list[6].toFloat();
			_apex.y = list[7].toFloat();
			_apex.z = list[8].toFloat();

			seedVolumeShape shape = (seedVolumeShape)list[9].toInt();

			if(list.size() == 11)
			{
				seedVolumeType type = (seedVolumeType)list[10].toInt();

				this->qfeAddSeedVolume(_dia1, _dia2, _apex, shape, type);
			}
			else
			{
				this->qfeAddSeedVolume(_dia1, _dia2, _apex, shape);
			}
		}

		file.close();
	}

	this->qfeUpdate();

	return qfeSuccess;
}


qfeReturnStatus qfeSeedVolumeCollection::qfeRenderCube(qfePoint pos, float size, bool draw_outline, qfeColorRGB color)
{
  glPushMatrix();

  glTranslatef(pos.x, pos.y, pos.z);
  glScalef(size, size, size);

  glBegin(GL_QUADS);
	glColor3f(color.r, color.g, color.b);    
	glVertex3f( 1.0f, 1.0f,-1.0f);
	glVertex3f(-1.0f, 1.0f,-1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f( 1.0f, 1.0f, 1.0f);
	glVertex3f( 1.0f,-1.0f, 1.0f);
	glVertex3f(-1.0f,-1.0f, 1.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);
	glVertex3f( 1.0f,-1.0f,-1.0f); 
	glVertex3f( 1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f,-1.0f, 1.0f);
	glVertex3f( 1.0f,-1.0f, 1.0f);  
	glVertex3f( 1.0f,-1.0f,-1.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);
	glVertex3f(-1.0f, 1.0f,-1.0f);
	glVertex3f( 1.0f, 1.0f,-1.0f); 
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f,-1.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);
	glVertex3f(-1.0f,-1.0f, 1.0f);   
	glVertex3f( 1.0f, 1.0f,-1.0f);
	glVertex3f( 1.0f, 1.0f, 1.0f);
	glVertex3f( 1.0f,-1.0f, 1.0f);
	glVertex3f( 1.0f,-1.0f,-1.0f);
  glEnd();  

  if(draw_outline)
  {
	  glLineWidth(2.0); 

	  glColor3f(0.0f, 0.0f, 0.0f); 

	  glBegin(GL_LINE_LOOP);	  
		glVertex3f( -1.0f, -1.0f, 1.0f);
		glVertex3f( 1.0f, -1.0f, 1.0f);
		glVertex3f( 1.0f, 1.0f, 1.0f);
		glVertex3f( -1.0f, 1.0f, 1.0f);
	  glEnd();

	  glBegin(GL_LINE_LOOP);	  
		glVertex3f( -1.0f, -1.0f, -1.0f);
		glVertex3f( 1.0f, -1.0f, -1.0f);
		glVertex3f( 1.0f, 1.0f, -1.0f);
		glVertex3f( -1.0f, 1.0f, -1.0f);
	  glEnd();

	  glBegin(GL_LINES);
		glVertex3f( -1.0f, -1.0f, 1.0f);
		glVertex3f( -1.0f, -1.0f, -1.0f);

		glVertex3f( 1.0f, -1.0f, 1.0f);
		glVertex3f( 1.0f, -1.0f, -1.0f);

		glVertex3f( 1.0f, 1.0f, 1.0f);
		glVertex3f( 1.0f, 1.0f, -1.0f);

		glVertex3f( -1.0f, 1.0f, 1.0f);
		glVertex3f( -1.0f, 1.0f, -1.0f);
	  glEnd();
  }

  glPopMatrix();

  return qfeSuccess;
}

qfeReturnStatus qfeSeedVolumeCollection::qfeUpdate()
{
	unsigned int ni,nj,nk;

	study->pcap.at(0)->qfeGetVolumeSize(ni, nj, nk);

	qfeMatrix4f V2P;
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, study->pcap.at(0));
	
	float *data = new float[ni*nj*nk];
	
	//init volume:
	qfeGrid *grid;
	this->study->pcap[0]->qfeGetVolumeGrid(&grid);
	indexVolume.qfeSetVolumeComponentsPerVoxel(1);
	indexVolume.qfeSetVolumeGrid(grid);

	for(unsigned int k = 0; k<nk; k++)
	for(unsigned int j = 0; j<nj; j++)
	for(unsigned int i = 0; i<ni; i++)
	{
		unsigned int offset = ((int)i + (int)j*ni + (int)k*ni*nj);

		qfePoint point = qfePoint(i,j,k)*V2P;
						
		float value = 0.0;

		for(unsigned int v = 0; v<this->seedVolumes.size(); v++)
		{
			if(seedVolumes.at(v).volume->IsInVolume(point)) //this voxel falls within the seed volume, so we label this voxel
			{
				value = seedVolumes[v].unique_id;

				continue;
			}
		}

		data[offset] = value;
	}

	indexVolume.qfeSetVolumeData(qfeValueTypeFloat, ni, nj, nk, data);
	indexVolume.qfeSetVolumeLabel("Seed volume indices");
		
	indexVolume.qfeSetHistogramToNull();

	//can't use indexVolume->qfeUpdate since we need nearest interpolation instead of linear

	GLuint tid;
	this->indexVolume.qfeGetVolumeTextureId(tid);
	if (glIsTexture(tid))
	{
		glDeleteTextures(1, (GLuint *)&tid);
		this->indexVolume.qfeSetVolumeTextureId(0);
	}
	
	GLuint voxtype = GL_FLOAT;

	// Allocate texture
	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_3D);
	
	unsigned int id;
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_3D, id);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	
	// Upload the texture
	// Note: 32 bits floats will be uploaded as 16 bits floats for memory consumption reasons
	glTexImage3D(GL_TEXTURE_3D, 0, GL_R16F, ni, nj, nk, 0, GL_RED, voxtype, data);
	
	glBindTexture(GL_TEXTURE_3D, 0);
	glDisable(GL_TEXTURE_3D);
	
	this->indexVolume.qfeSetVolumeTextureId(id);
			
	delete[] data;
	
	if(glGetError() == GL_NO_ERROR)  
		return qfeSuccess;
	else                             
		return qfeError;
}

qfeReturnStatus qfeSeedVolumeCollection::GetVolumeIndexTextureID(GLuint &tid)
{
	//calling this->indexVolume.qfeUpdateVolume(); is not necessary and might break other (previous) texture bindings
	//glnearest instead of linear
	//	or simply multiply the the stored values and use modulo or round to nearest (correct) value
	return this->indexVolume.qfeGetVolumeTextureId(tid);
}