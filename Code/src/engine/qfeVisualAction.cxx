#include "qfeVisualAction.h"

//----------------------------------------------------------------------------
qfeVisualAction::qfeVisualAction()
{
  this->valueLabel    = (char*)malloc(sizeof(char) * 200);
  this->valueType     = -1;
};

//----------------------------------------------------------------------------
qfeVisualAction::qfeVisualAction(const qfeVisualAction &param)
{
  this->valueLabel            = param.valueLabel;
  this->valueType             = param.valueType;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualAction::qfeSetVisualActionLabel(string label)
{
  this->valueLabel.assign(label);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualAction::qfeGetVisualActionLabel(string &label)
{
  label.assign(this->valueLabel);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualAction::qfeSetVisualActionType(int t)
{
  this->valueType = t;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualAction::qfeGetVisualActionType(int &t)
{
  t = this->valueType;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVisualAction::qfeSetVisualAction(string label, int type)
{
  this->qfeSetVisualActionLabel(label);
  this->qfeSetVisualActionType(type);

  return qfeSuccess;
}

