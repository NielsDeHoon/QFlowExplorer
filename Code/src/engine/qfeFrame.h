#pragma once

#include "qflowexplorer.h"

#include <iostream>

#include "qfeGeometry.h"

#define QFE_FRAME            0
#define QFE_FRAME_SLICE      1
#define QFE_FRAME_PROJECTION 2

using namespace std;

/**
* \file   qfeFrame.h
* \author Roy van Pelt
* \class  qfeFrame
* \brief  Implements a rectangular visualization geometry.
* \note   Confidential
*
* A frame defines a rectangular visualization geometry.
* A frame may define a thin slice, a slab or the parallel projection of 
* an entire volume.
*/
//----------------------------------------------------------------------------
class qfeFrame : public qfeGeometry
{
public:
          qfeFrame();
  virtual ~qfeFrame();

  virtual qfeReturnStatus qfeSetFrameOrigin(qfeFloat ox, qfeFloat oy, qfeFloat oz);
  virtual qfeReturnStatus qfeGetFrameOrigin(qfeFloat& ox, qfeFloat& oy, qfeFloat& oz);

  virtual qfeReturnStatus qfeSetFrameAxes(qfeFloat axx, qfeFloat axy, qfeFloat axz,
                                          qfeFloat ayx, qfeFloat ayy, qfeFloat ayz,
                                          qfeFloat azx, qfeFloat azy, qfeFloat azz);
  virtual qfeReturnStatus qfeGetFrameAxes(qfeFloat& axx, qfeFloat& axy, qfeFloat& axz,
                                          qfeFloat& ayx, qfeFloat& ayy, qfeFloat& ayz,
                                          qfeFloat& azx, qfeFloat& azy, qfeFloat& azz);

  virtual qfeReturnStatus qfeSetFrameExtent(qfeFloat ex, qfeFloat ey, qfeFloat ez=1.0);
  virtual qfeReturnStatus qfeGetFrameExtent(qfeFloat& ex, qfeFloat& ey, qfeFloat &ez);
};

/**
* \file   qfeFrame.h
* \author Roy van Pelt
* \class  qfeFrameSlice
* \brief  Implements a thin slice visualization geometry.
* \note   Confidential
*
* A frame slice defines a thin slice visualization geometry.
*/
//----------------------------------------------------------------------------
class qfeFrameSlice : public qfeFrame
{
public:
  qfeFrameSlice(qfeFloat ox  = 0.0, qfeFloat oy  = 0.0, qfeFloat oz  = 0.0,
                qfeFloat axx = 1.0, qfeFloat axy = 0.0, qfeFloat axz = 0.0,
                qfeFloat ayx = 0.0, qfeFloat ayy = 1.0, qfeFloat ayz = 0.0,
                qfeFloat azx = 0.0, qfeFloat azy = 0.0, qfeFloat azz = 1.0,
                qfeFloat ex  = 1.0, qfeFloat ey  = 1.0, qfeFloat ez  = 1.0);
  virtual ~qfeFrameSlice();
};

/**
* \file   qfeFrame.h
* \author Roy van Pelt
* \class  qfeFrameProjection
* \brief  Implements a projection visualization geometry.
* \note   Confidential
*
* A frame projections defines a projection visualization geometry.
*/
//----------------------------------------------------------------------------
class qfeFrameProjection : public qfeFrame
{
public:
  qfeFrameProjection( qfeFloat ox  = 0.0, qfeFloat oy  = 0.0, qfeFloat oz  = 0.0,
                      qfeFloat axx = 1.0, qfeFloat axy = 0.0, qfeFloat axz = 0.0,
                      qfeFloat ayx = 0.0, qfeFloat ayy = 1.0, qfeFloat ayz = 0.0,
                      qfeFloat azx = 0.0, qfeFloat azy = 0.0, qfeFloat azz = 1.0,
                      qfeFloat ex  = 1.0, qfeFloat ey  = 1.0, qfeFloat ez  = 1.0);
  virtual ~qfeFrameProjection();
};