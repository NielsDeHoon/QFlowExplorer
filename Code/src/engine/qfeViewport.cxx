#include "qfeViewport.h"

//----------------------------------------------------------------------------
qfeViewport::qfeViewport()
{
  this->origin[0]   = 0;
  this->origin[1]   = 0;
  this->width       = 512;
  this->height      = 512;
};

//----------------------------------------------------------------------------
qfeViewport::qfeViewport(const qfeViewport &param)
{
  this->origin[0]   = param.origin[0];
  this->origin[1]   = param.origin[1];
  this->width       = param.width;
  this->height      = param.height;
}

