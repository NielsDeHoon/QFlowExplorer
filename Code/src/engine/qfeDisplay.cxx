#include "qfeDisplay.h"

//----------------------------------------------------------------------------
qfeDisplay::qfeDisplay()
{
  this->renderWindow                = NULL;

  this->backgroundColor.r           = 1.0;
  this->backgroundColor.g           = 1.0;
  this->backgroundColor.b           = 1.0;
  this->backgroundColorSecondary.r  = 200.0f/255.0f;
  this->backgroundColorSecondary.g  = 220.0f/255.0f;
  this->backgroundColorSecondary.b  = 246.0f/255.0f;
  this->backgroundType              = qfeDisplayBackgroundGradient;

  this->layout                      = qfeDisplayLayoutSingle;
  this->layoutFullScreen            = true;
  this->layoutClickable             = false;

  this->nx = 640;
  this->ny = 480;

  this->supersampling = 1;
};

//----------------------------------------------------------------------------
qfeDisplay::qfeDisplay(const qfeDisplay &display)
{
  this->renderWindow               = display.renderWindow;
  this->backgroundColor.r          = display.backgroundColor.r;
  this->backgroundColor.g          = display.backgroundColor.g;
  this->backgroundColor.b          = display.backgroundColor.b;
  this->backgroundColorSecondary.r = display.backgroundColorSecondary.r;
  this->backgroundColorSecondary.g = display.backgroundColorSecondary.g;
  this->backgroundColorSecondary.b = display.backgroundColorSecondary.b;
  this->backgroundType             = display.backgroundType;

  this->nx                         = display.nx;
  this->ny                         = display.ny;
  
  this->layout                     = display.layout;
  this->layoutFullScreen           = display.layoutFullScreen;
  this->layoutClickable            = display.layoutClickable;

  this->supersampling              = display.supersampling;  
};

//----------------------------------------------------------------------------
qfeDisplay::~qfeDisplay()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeSetDisplayBackgroundColorRGB(qfeFloat r, qfeFloat g, qfeFloat b)
{
  this->backgroundColor.r = r;
  this->backgroundColor.g = g;
  this->backgroundColor.b = b;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeGetDisplayBackgroundColorRGB(qfeFloat &r, qfeFloat &g, qfeFloat &b)
{
  r = this->backgroundColor.r;
  b = this->backgroundColor.g;
  g = this->backgroundColor.b;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeSetDisplayBackgroundColorRGBSecondary(qfeFloat r, qfeFloat g, qfeFloat b)
{
  this->backgroundColorSecondary.r = r;
  this->backgroundColorSecondary.g = g;
  this->backgroundColorSecondary.b = b;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeGetDisplayBackgroundColorRGBSecondary(qfeFloat &r, qfeFloat &g, qfeFloat &b)
{
  r = this->backgroundColorSecondary.r;
  g = this->backgroundColorSecondary.g;
  b = this->backgroundColorSecondary.b;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeSetDisplayBackgroundType(int type)
{
  this->backgroundType = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeGetDisplayBackgroundType(int &type)
{
  type = this->backgroundType;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeGetDisplaySize(unsigned int &nx, unsigned int &ny)
{
  int *size = this->renderWindow->GetSize(); 

  this->nx = nx = size[0];
  this->ny = ny = size[1];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeSetDisplayWindow(vtkRenderWindow *rw)
{
  this->renderWindow = rw;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeGetDisplayWindow(vtkRenderWindow **rw)
{
  *rw = this->renderWindow;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeSetDisplayLayout(qfeDisplayLayout layout, bool fullScreen, bool clickable)
{
  this->layout           = layout;
  this->layoutFullScreen = fullScreen;
  this->layoutClickable  = clickable;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeGetDisplayLayout(qfeDisplayLayout &layout, bool &fullScreen, bool &clickable)
{
  layout     = this->layout;
  fullScreen = this->layoutFullScreen;
  clickable  = this->layoutClickable;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeSetDisplaySuperSampling(int factor)
{
  this->supersampling = factor;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDisplay::qfeGetDisplaySuperSampling(int &factor)
{
  factor = this->supersampling;

  return qfeSuccess;
}
