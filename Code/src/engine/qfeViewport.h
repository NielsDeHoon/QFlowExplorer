#pragma once

#include <iostream>
#include <typeinfo>

#include "qflowexplorer.h"

using namespace std;

/**
* \file   qfeViewport.h
* \author Roy van Pelt
* \class  qfeViewport
* \brief  Implements a basic viewport class
* \note   Confidential
*
* Basic viewport setting
*
*/
class qfeViewport
{
  public:
    qfeViewport();
    qfeViewport(const qfeViewport &param);
    ~qfeViewport(){};

    unsigned int origin[2];
    unsigned int width;
    unsigned int height;
};
