#include "qfeHistogram.h"

qfeHistogram::qfeHistogram()
{
  volume = nullptr;
  volWidth = volHeight = volDepth = 0;
  volType = qfeValueTypeFloat;
  volNrComp = 1;
  volData = nullptr;
  histogram = nullptr;
  nrBins = 1024;
  rangeMin = 0.f;
  rangeMax = 1.f;
  upToDate = true;
  nrBinsChanged = false;
  maxCount = 0;
}

qfeHistogram::qfeHistogram(const qfeHistogram &h)
{
  volume = h.volume;
  volWidth = h.volWidth;
  volHeight = h.volHeight;
  volDepth = h.volDepth;
  volType = h.volType;
  volNrComp = h.volNrComp;
  volData = h.volData;
  nrBins = h.nrBins;
  rangeMin = h.rangeMin;
  rangeMax = h.rangeMax;
  upToDate = h.upToDate;
  nrBinsChanged = h.nrBinsChanged;
  maxCount = h.maxCount;
  
  // Copy data in histogram
  // When a copy gets destroyed, we do not want all copies
  // to have their histogram deleted
  histogram = new unsigned[nrBins];
  for (unsigned i = 0; i < nrBins; i++)
    histogram[i] = h.histogram[i];
}

qfeHistogram::~qfeHistogram()
{
  // Don't delete volume, we have no ownership
  if (histogram)
    delete[] histogram;
}

void qfeHistogram::qfeSetVolume(qfeVolume *volume)
{
  this->volume = volume;
  upToDate = false;
}

void qfeHistogram::qfeSetNrOfBins(unsigned nrBins)
{
  this->nrBins = nrBins;
  nrBinsChanged = true;
  upToDate = false;
}

unsigned qfeHistogram::qfeGetNrOfBins() const
{
  return nrBins;
}

void qfeHistogram::qfeSetRange(float rangeMin, float rangeMax)
{
  this->rangeMin = rangeMin;
  this->rangeMax = rangeMax;
  upToDate = false;
}

void qfeHistogram::qfeGetRange(float &rangeMin, float &rangeMax) const
{
  rangeMin = this->rangeMin;
  rangeMax = this->rangeMax;
}

const unsigned *qfeHistogram::qfeGetHistogram()
{  
  // Do not redo calculations unnecessarily
  if (!upToDate) {
    qfeUpdate();
    upToDate = true;
  }

  return histogram;
}

void qfeHistogram::qfeUpdate()
{
  if (!volume || nrBins <= 0)
    return;

  // Retrieve all the required information from the volume
  volume->qfeGetVolumeData(volType, volWidth, volHeight, volDepth, &volData);
  volume->qfeGetVolumeComponentsPerVoxel(volNrComp);

  qfeResizeHistogram();
  
  // Go through all the values in the data
  const unsigned volSize = volWidth*volHeight*volDepth;
  for (unsigned i = 0; i < volSize; i++) {
    float value = qfeGetValue(i);
    unsigned bin = qfeGetBin(value);
    if (bin >= nrBins)
      bin = nrBins - 1;
    // Increase count for bin in which value belongs
    histogram[bin]++;
    if (histogram[bin] > maxCount)
      maxCount = histogram[bin];
  }
}

float qfeHistogram::qfeGetValue(unsigned index) const
{
  if (volNrComp == 1) {
    return qfeGetDirectValue(index);
  } else if (volNrComp == 3) {
    float x = qfeGetDirectValue(index);
    float y = qfeGetDirectValue(index+1);
    float z = qfeGetDirectValue(index+2);
    return sqrtf(x*x + y*y + z*z);
  } else {
    return 0.f;
  }
}

float qfeHistogram::qfeGetDirectValue(unsigned index) const
{
  switch (volType) {
  case qfeValueTypeFloat:
    return ((float*)volData)[index];
  case qfeValueTypeInt16:
    return (float)((short*)volData)[index];
  case qfeValueTypeUnsignedInt16:
    return (float)((unsigned short*)volData)[index];
  }

  return 0.f;
}

void qfeHistogram::qfeResizeHistogram()
{
  if (histogram && nrBinsChanged) {
    delete[] histogram;
    histogram = nullptr;
  }
  if (!histogram) {
    histogram = new unsigned[nrBins];
    for (unsigned i = 0; i < nrBins; i++)
      histogram[i] = 0;
    nrBinsChanged = false;
  }
}

unsigned qfeHistogram::qfeGetBin(float value) const
{
  // +1.f for rounding errors, when value == rangeMax
  float t = (value - rangeMin) / (rangeMax - rangeMin + 1.f);
  return (unsigned)(t * (nrBins));
}

unsigned qfeHistogram::qfeGetCount(unsigned bin) const
{
  return histogram[bin];
}

unsigned qfeHistogram::qfeGetMaxCount() const
{
  return maxCount;
}
