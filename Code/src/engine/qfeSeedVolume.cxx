#include "qfeSeedVolume.h"
#include "qfeMatrix4f.h"

qfeSeedVolume::qfeSeedVolume()
{
  axis1 = qfeVector(1.f, 0.f, 0.f);
  axis2 = qfeVector(0.f, 1.f, 0.f);
  axis3 = qfeVector(0.f, 0.f, 1.f);
  lambda1 = 1.f;
  lambda2 = 1.f;
  lambda3 = 1.f;
  center = qfePoint(0.f, 0.f, 0.f);
  clip = qfeVector(0.f, 0.f, 0.f);
  seedOnClipPlane = false;
  for (int i = 0; i < 4; i++)
	bbCorners[0] = qfePoint(0.f, 0.f, 0.f);
  placed = false;

  //shader.qfeAddShaderFromFile("/shaders/ivr/ivr-clipped-ellipsoid-vert.glsl", QFE_VERTEX_SHADER);  
  //shader.qfeAddShaderFromFile("/shaders/ivr/ivr-clipped-ellipsoid-frag.glsl", QFE_FRAGMENT_SHADER);
  //shader.qfeLink();
}

void qfeSeedVolume::qfeSetGuidePoints(qfePoint _dia1, qfePoint _dia2, qfePoint _apex)
{
  // Center is between diameter guiding points
  center = (_dia1 + _dia2) / 2.f;

  // Y-axis is from center towards apex
  axis2 = _apex - center;
  qfeVector::qfeVectorLength(axis2, lambda2);
  qfeVector::qfeVectorNormalize(axis2);

  // Radius for X and Y-axis is half of distance between diameter points
  clip = _dia1 - center;
  float length;
  qfeVector::qfeVectorLength(_dia2 - _dia1, length);
  length /= 2.f;

  // Z-axis perpendicular to line between diameter points and line from center to apex
  qfeVector::qfeVectorCross(axis2, _dia2 - _dia1, axis3);
  qfeVector::qfeVectorNormalize(axis3);
  lambda3 = length;

  // X-axis is perpendicular to Y and Z-axis
  qfeVector::qfeVectorCross(axis2, axis3, axis1);
  qfeVector::qfeVectorNormalize(axis1);
  lambda1 = length;

  // Save normal of clipping plane
  qfeVector::qfeVectorCross(_dia1 - _dia2, axis3, clipNormal);
  qfeVector::qfeVectorNormalize(clipNormal);
  // Make sure clipping normal faces away from apex guiding point
  if (IsInFrontOfClippingPlane(_apex))
	clipNormal = -clipNormal;

  CalculateTMatrix();

  placed = true;

  dia1 = _dia1;
  dia2 = _dia2;
  apex = _apex;
}

void qfeSeedVolume::qfeGetGuidePoints(qfePoint &_dia1, qfePoint &_dia2, qfePoint &_apex)
{
	_dia1 = this->dia1;
	_dia2 = this->dia2;
	_apex = this->apex;
}

void qfeSeedVolume::qfeSetSeedOnClipPlane(bool on)
{
  seedOnClipPlane = on;
}

void qfeSeedVolume::qfeSetVtkCamera(vtkCamera *camera, int _vp_width, int _vp_height)
{
  this->camera = camera;
  this->vp_width = _vp_width;
  this->vp_height = _vp_height;
}

void qfeSeedVolume::qfeGenerateSurfacePoints()
{
  GenerateUnitSphereSurfacePoints();
  TransformUnitSphereToEllipsoidSurfacePoints();
}

// Generates seed points on the intersection between the clipping plane and the ellipsoid
void qfeSeedVolume::GenerateSeedPointsOnClipPlaneSurface(unsigned nrPoints)
{
	surfacePoints.clear();
	unsigned int steps = nrPoints;
	if(nrPoints != 3 && (nrPoints%2==1 || nrPoints > 3)) //we need a point in the center
	{
		surfacePoints.push_back(center);
		steps-=1;
	}
		
	for(unsigned int step = 0; step < steps; step++)
	{
		float angle = 2.0f*3.14159265358979323846*(float)step/steps;
		qfeVector newSeed = lambda1*this->axis1*cos(angle)+lambda3*this->axis3*sin(angle);
		surfacePoints.push_back(qfePoint(center.x+newSeed.x, center.y+newSeed.y, center.z+newSeed.z));
	}
}

vector<qfePoint>& qfeSeedVolume::qfeGetSurfacePoints()
{
  return surfacePoints;
}

void qfeSeedVolume::qfeReset()
{
  qfeClearSeedPoints();
  surfacePoints.clear();
  axis1 = qfeVector(1.f, 0.f, 0.f);
  axis2 = qfeVector(0.f, 1.f, 0.f);
  axis3 = qfeVector(0.f, 0.f, 1.f);
  lambda1 = 1.f;
  lambda2 = 1.f;
  lambda3 = 1.f;
  center = qfePoint(0.f, 0.f, 0.f);
  clip = qfeVector(0.f, 0.f, 0.f);
  seedOnClipPlane = false;
  for (int i = 0; i < 4; i++)
	bbCorners[0] = qfePoint(0.f, 0.f, 0.f);
  placed = false;
}

void qfeSeedVolume::qfeGenerateSeedPoints(unsigned nrPoints)
{
  seedPoints.clear();

  if (seedOnClipPlane)
	GenerateSeedPointsOnClipPlane(nrPoints);
  else
	GenerateSeedPointsInVolume(nrPoints);
}

void qfeSeedVolume::qfeGenerateSeedPointsNotIn(unsigned nrPoints, qfeSeedVolume *other)
{
  if (other == nullptr)
	return;

  seedPoints.clear();

  // It is possible the other ellipsoid to fully contain this one
  // That would make for an infinite loop
  // Add a maximum number of tries
  int counter = 0;
  int max = 40*nrPoints;

  if (seedOnClipPlane) {
	while (seedPoints.size() < nrPoints) {
	  qfePoint p = GeneratePointInClipRect();
	  if (IsInVolume(p) && !other->IsInVolume(p))
		seedPoints.push_back(p);
	  counter++;
	  if (counter > max)
		return;
	}
  } else {
	while (seedPoints.size() < nrPoints) {
	  qfePoint p = GeneratePointInBoundingBox();
	  if (!IsInFrontOfClippingPlane(p) && IsInVolume(p) && !other->IsInVolume(p))
		seedPoints.push_back(p);
	  counter++;
	  if (counter > max)
		return;
	}
  }
}

void qfeSeedVolume::GenerateSeedPointsInVolume(unsigned nrPoints)
{
  while (seedPoints.size() < nrPoints) {
	qfePoint p = GeneratePointInBoundingBox();
	if (!IsInFrontOfClippingPlane(p) && IsInVolume(p))
	  seedPoints.push_back(p);
  }
}

// Generates seed points on the intersection between the clipping plane and the ellipsoid
void qfeSeedVolume::GenerateSeedPointsOnClipPlane(unsigned nrPoints)
{
  while (seedPoints.size() < nrPoints) {
	qfePoint p = GeneratePointInClipRect();
	if (IsInVolume(p))
	  seedPoints.push_back(p);
  }
}

void qfeSeedVolume::CalculateTMatrix()
{
  // Specify orientation matrix and its transpose
  float fO[16] = {
	axis1.x,  axis2.x,  axis3.x,  0.f,
	axis1.y,  axis2.y,  axis3.y,  0.f,
	axis1.z,  axis2.z,  axis3.z,  0.f,
	0.f,      0.f,      0.f,      1.f};
  qfeMatrix4f O, Ot;
  qfeMatrix4f::qfeSetMatrixElements(O, fO);
  qfeMatrix4f::qfeGetMatrixTranspose(O, Ot);
  
  // Specify radius matrix
  float fLambda[16] = {
	lambda1,  0.f,      0.f,      0.f,
	0.f,      lambda2,  0.f,      0.f,
	0.f,      0.f,      lambda3,  0.f,
	0.f,      0.f,      0.f,      1.f};
  qfeMatrix4f Lambda;
  qfeMatrix4f::qfeSetMatrixElements(Lambda, fLambda);

  // Calculate transformation matrix and its inverse
  T = O*Lambda;
  qfeMatrix4f::qfeGetMatrixInverse(T, TInv);
}

void qfeSeedVolume::CalculateBillboardCorners(const qfePoint &eye, const qfeVector &up)
{
  // Eye point in parameter space
  pEye = WorldToParameter(eye);

  // Calculate two orthogonal vectors in plane of silhouette in parameter space
  qfeVector lookDir(-pEye.x, -pEye.y, -pEye.z);
  qfeVector pUp = WorldToParameter(up);
  qfeVector::qfeVectorNormalize(lookDir);
  qfeVector::qfeVectorNormalize(pUp);
  qfeVector pX, pY;
  qfeVector::qfeVectorCross(lookDir, pUp, pX);
  qfeVector::qfeVectorCross(pX, lookDir, pY);
  // Cross product of two unit vectors turns out not to necessarily be a unit vector..
  // So normalize
  qfeVector::qfeVectorNormalize(pX);
  qfeVector::qfeVectorNormalize(pY);

  // Convert middle point and axes back to world space
  qfePoint pMiddle = qfePoint(0.f, 0.f, 0.f);
  qfePoint middle = ParameterToWorld(pMiddle);
  qfeVector x = ParameterToWorld(pX*2.0);
  qfeVector y = ParameterToWorld(pY*2.0);

  // Calculate bounding box corner points
  bbCorners[0] = middle - x + y;
  bbCorners[1] = middle - x - y;
  bbCorners[2] = middle + x - y;
  bbCorners[3] = middle + x + y;
  for (int i = 0; i < 4; i++)
	pBBCorners[i] = WorldToParameter(bbCorners[i]);

  qfePoint tests[4];
  for (int i = 0; i < 4; i++)
	tests[i] = pBBCorners[i] / sqrtf(pBBCorners[i].x*pBBCorners[i].x + pBBCorners[i].y*pBBCorners[i].y + pBBCorners[i].z*pBBCorners[i].z);

  int i = 0;
}

vector<qfePoint>& qfeSeedVolume::qfeGetSeedPoints()
{
  return seedPoints;
}

void qfeSeedVolume::qfeClearSeedPoints()
{
  seedPoints.clear();
}

void qfeSeedVolume::qfeSquish()
{
  axis3 = axis3 * 0.9f;
  CalculateTMatrix();
}

void qfeSeedVolume::qfeStretch()
{
  axis3 = axis3 * 1.1f;
  CalculateTMatrix();
}

bool qfeSeedVolume::qfeIsPlaced() const
{
  return placed;
}

void qfeSeedVolume::qfeRenderSeedPoints()
{
  glPointSize(8.f);
  glBegin(GL_POINTS);
  glColor3f(0.f, 0.f, 1.f);
  for (unsigned i = 0; i < seedPoints.size(); i++)
	glVertex3f(seedPoints[i].x, seedPoints[i].y, seedPoints[i].z);
  glEnd();
  glPointSize(1.f);
}

void qfeSeedVolume::qfeRenderAxes()
{
  glBegin(GL_LINES);
	glColor3f(1.f, 0.f, 0.f);
	glVertex3f(center.x, center.y, center.z);
	glVertex3f(center.x + axis1.x, center.y + axis1.y, center.z + axis1.z);
	glColor3f(0.f, 1.f, 0.f);
	glVertex3f(center.x, center.y, center.z);
	glVertex3f(center.x + axis2.x, center.y + axis2.y, center.z + axis2.z);
	glColor3f(0.f, 0.f, 1.f);
	glVertex3f(center.x, center.y, center.z);
	glVertex3f(center.x + axis3.x, center.y + axis3.y, center.z + axis3.z);
  glEnd();
}

qfeMatrix4f qfeSeedVolume::qfeGetTMatrix() const
{
  return T;
}

qfeMatrix4f qfeSeedVolume::qfeGetTInverseMatrix() const
{
  return TInv;
}

qfePoint qfeSeedVolume::qfeGetCenter() const
{
  return center;
}

qfeVector qfeSeedVolume::qfeGetAxis1() const
{
  return axis1;
}

qfeVector qfeSeedVolume::qfeGetAxis2() const
{
  return axis2;
}

qfeVector qfeSeedVolume::qfeGetAxis3() const
{
  return axis3;
}

float qfeSeedVolume::qfeGetLambda1() const
{
  return lambda1;
}

float qfeSeedVolume::qfeGetLambda2() const
{
  return lambda2;
}

float qfeSeedVolume::qfeGetLambda3() const
{
  return lambda3;
}

qfePoint qfeSeedVolume::qfeGetDia1() const
{
	return dia1;
}

qfePoint qfeSeedVolume::qfeGetDia2() const
{
	return dia2;
}

qfePoint qfeSeedVolume::qfeGetApex() const
{
	return apex;
}

void qfeSeedVolume::GenerateUnitSphereSurfacePoints()
{
  surfacePoints.clear();

  const float PI = 3.1415192654f;
  for (float phi = 0.f; phi < PI*2.f; phi += PI/8.f) {
	for (float theta = 0.f; theta < PI*2.f; theta += PI/8.f) {
	  float x = sinf(theta) * cosf(phi);
	  float y = sinf(theta) * sinf(phi);
	  float z = cosf(theta);
	  surfacePoints.push_back(qfePoint(x, y, z));
	}
  }
}

void qfeSeedVolume::TransformUnitSphereToEllipsoidSurfacePoints()
{
  for (unsigned i = 0; i < surfacePoints.size(); i++) {
	surfacePoints[i] = ParameterToWorld(surfacePoints[i]);
  }
}

bool qfeSeedVolume::IsInFrontOfClippingPlane(const qfePoint &p)
{
  float dot;
  qfeVector::qfeVectorDot(p - center, clipNormal, dot);
  if (dot > 0.f)
	return true;
  return false;
}

float qfeSeedVolume::GetRandomFloat(float min, float max)
{
  return min + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max-min)));
}

qfePoint qfeSeedVolume::GeneratePointInBoundingBox()
{
  float u1 = GetRandomFloat(-1.f, 1.f);
  float u2 = GetRandomFloat(-1.f, 1.f);
  float u3 = GetRandomFloat(-1.f, 1.f);
  return center + u1*lambda1*axis1 + u2*lambda2*axis2 + u3*lambda3*axis3;
}

qfePoint qfeSeedVolume::GeneratePointInClipRect()
{
  float u1 = GetRandomFloat(-1.5f, 1.5f);
  float u2 = GetRandomFloat(-1.5f, 1.5f);
  return center + u1*clip + u2*lambda3*axis3;
}

qfePoint qfeSeedVolume::WorldToParameter(const qfePoint &p) const
{
  qfePoint q = p - qfeVector(center.x, center.y, center.z);
  return TInv*q;
}

qfeVector qfeSeedVolume::WorldToParameter(const qfeVector &v) const
{
  return TInv*v;
}

qfePoint qfeSeedVolume::ParameterToWorld(const qfePoint &p) const
{
  return T*p + center;
}

qfeVector qfeSeedVolume::ParameterToWorld(const qfeVector &v) const
{
  return T*v;
}

qfeSeedEllipsoid::qfeSeedEllipsoid()
{
  axis1 = qfeVector(1.f, 0.f, 0.f);
  axis2 = qfeVector(0.f, 1.f, 0.f);
  axis3 = qfeVector(0.f, 0.f, 1.f);
  lambda1 = 1.f;
  lambda2 = 1.f;
  lambda3 = 1.f;
  center = qfePoint(0.f, 0.f, 0.f);
  clip = qfeVector(0.f, 0.f, 0.f);
  seedOnClipPlane = false;
  for (int i = 0; i < 4; i++)
	bbCorners[0] = qfePoint(0.f, 0.f, 0.f);
  placed = false;

  shader = new qfeGLShaderProgram();
  shader->qfeAddShaderFromFile("/shaders/ivr/ivr-clipped-ellipsoid-vert.glsl", QFE_VERTEX_SHADER);  
  shader->qfeAddShaderFromFile("/shaders/ivr/ivr-clipped-ellipsoid-frag.glsl", QFE_FRAGMENT_SHADER);
  shader->qfeLink();
}

float qfeSeedEllipsoid::qfeGetVolume()
{
	return 4.0f/3.0f*(3.14159265358979323846*this->lambda1*this->lambda2*this->lambda3);
}

bool qfeSeedEllipsoid::IsInVolume(const qfePoint& p) const
{
  qfePoint q = WorldToParameter(p);
  if (q.x*q.x + q.y*q.y + q.z*q.z <= 1.f)
	return true;
  return false;
}

void qfeSeedEllipsoid::qfeRenderVolume(const qfeColorRGB &color, bool surface, bool selected, bool useClipPlane)
{
  if (!placed)
	return;

  // Retrieve camera position and up vector
  double eyePosition[3];
  double upVector[3];
  camera->GetPosition(eyePosition);
  camera->GetViewUp(upVector);
  qfePoint eye(eyePosition[0], eyePosition[1], eyePosition[2]);
  qfeVector up(upVector[0], upVector[1], upVector[2]);

  // Calculate billboard
  CalculateBillboardCorners(eye, up);

  float fT[16];
  qfeMatrix4f Tt; // transpose T
  qfeMatrix4f::qfeGetMatrixTranspose(T, Tt); // OpenGL takes column major matrices
  qfeMatrix4f::qfeGetMatrixElements(Tt, fT);
  shader->qfeSetUniform4f("pBBTopLeft",      pBBCorners[0].x, pBBCorners[0].y, pBBCorners[0].z, pBBCorners[0].w);
  shader->qfeSetUniform4f("pBBBottomLeft",   pBBCorners[1].x, pBBCorners[1].y, pBBCorners[1].z, pBBCorners[1].w);
  shader->qfeSetUniform4f("pBBBottomRight",  pBBCorners[2].x, pBBCorners[2].y, pBBCorners[2].z, pBBCorners[2].w);
  shader->qfeSetUniform4f("pBBTopRight",     pBBCorners[3].x, pBBCorners[3].y, pBBCorners[3].z, pBBCorners[3].w);
  shader->qfeSetUniform4f("pEye", pEye.x, pEye.y, pEye.z, pEye.w);
  shader->qfeSetUniformMatrix4f("T", 1, fT); // So actually uploading transpose of T
  shader->qfeSetUniform4f("center", center.x, center.y, center.z, center.w);
  shader->qfeSetUniform4f("clipNormal", clipNormal.x, clipNormal.y, clipNormal.z, clipNormal.w);
  shader->qfeSetUniform1i("renderSurface", surface);
  shader->qfeSetUniform1i("renderSelected", selected);
  shader->qfeSetUniform3f("color", color.r, color.g, color.b);
  shader->qfeSetUniform1i("useClipPlane", useClipPlane);

  shader->qfeEnable();
  glBegin(GL_QUADS);
  glColor3f(0.f, 0.5f, 0.5f);
  for (unsigned i = 0; i < 4; i++)
	glVertex3f(bbCorners[i].x, bbCorners[i].y, bbCorners[i].z);
  glEnd();
  shader->qfeDisable();
}


qfeSeedCylinder::qfeSeedCylinder()
{
  axis1 = qfeVector(1.f, 0.f, 0.f);
  axis2 = qfeVector(0.f, 1.f, 0.f);
  axis3 = qfeVector(0.f, 0.f, 1.f);
  lambda1 = 1.f;
  lambda2 = 1.f;
  lambda3 = 1.f;
  center = qfePoint(0.f, 0.f, 0.f);
  clip = qfeVector(0.f, 0.f, 0.f);
  seedOnClipPlane = false;
  for (int i = 0; i < 4; i++)
	bbCorners[0] = qfePoint(0.f, 0.f, 0.f);
  placed = false;

  shader = new qfeGLShaderProgram();
  shader->qfeAddShaderFromFile("/shaders/ivr/ivr-clipped-cylinder-geom.glsl", QFE_GEOMETRY_SHADER);  
  shader->qfeAddShaderFromFile("/shaders/ivr/ivr-clipped-cylinder-vert.glsl", QFE_VERTEX_SHADER);  
  shader->qfeAddShaderFromFile("/shaders/ivr/ivr-clipped-cylinder-frag.glsl", QFE_FRAGMENT_SHADER);
  shader->qfeLink();
}

float qfeSeedCylinder::qfeGetVolume()
{
	return 4.0f/3.0f*(3.14159265358979323846*this->lambda1*this->lambda2*this->lambda3); //TODO FIX
}

bool qfeSeedCylinder::IsInVolume(const qfePoint& p) const
{
  qfePoint q = WorldToParameter(p);
  if(q.y>0.0f && q.y < 1.0f && q.x*q.x + q.z*q.z <= 1.f) 
	  return true;
  return false;
}

void qfeSeedCylinder::qfeRenderVolume(const qfeColorRGB &color, bool surface, bool selected, bool useClipPlane)
{
	this->qfeRenderVolume(color, surface, selected, useClipPlane, false);
}

void qfeSeedCylinder::qfeRenderVolume(const qfeColorRGB &color, bool surface, bool selected, bool useClipPlane, bool planar)
{
  if (!placed)
	return;

  qfeColorRGB col = color;

  /*if(selected)
  {
	  col.r = 0.35f;
	  col.g = 0.35f;
	  col.b = 0.35f;
  }*/

  //note, axis2 points from the center to the apex  
  GLfloat          matrixModelView[16];
  GLfloat          matrixProjection[16];

  glGetFloatv(GL_PROJECTION_MATRIX, matrixProjection);
  glGetFloatv(GL_MODELVIEW_MATRIX, matrixModelView);

  std::array<double,3> viewDir;
  camera->GetDirectionOfProjection(viewDir[0], viewDir[1], viewDir[2]);

  shader->qfeSetUniformMatrix4f("projectionMatrix", 1, matrixProjection);
  shader->qfeSetUniformMatrix4f("modelViewMatrix", 1, matrixModelView);
  shader->qfeSetUniform3f("viewDir", viewDir[0], viewDir[1], viewDir[2]);
  shader->qfeSetUniform3f("color", col.r, col.g, col.b);
  shader->qfeSetUniform4f("center", center.x, center.y, center.z, center.w);
  shader->qfeSetUniform3f("axis1", axis1.x, axis1.y, axis1.z);
  shader->qfeSetUniform3f("axis2", axis2.x, axis2.y, axis2.z);
  shader->qfeSetUniform3f("axis3", axis3.x, axis3.y, axis3.z);
  shader->qfeSetUniform1f("lambda1", lambda1);
  shader->qfeSetUniform1f("lambda2", lambda2);
  shader->qfeSetUniform1f("lambda3", lambda3);
  shader->qfeSetUniform1i("useClipPlane", 0);
  if(useClipPlane)
	  shader->qfeSetUniform1i("useClipPlane", 1);

  shader->qfeSetUniform1i("renderSurface", 0);
  if(surface)
	  shader->qfeSetUniform1i("renderSurface", 1);

  shader->qfeSetUniform1i("selected", 0);
  if(selected)
	  shader->qfeSetUniform1i("selected", 1);

  shader->qfeSetUniform1i("planar", 0);
  if(planar)
	  shader->qfeSetUniform1i("planar", 1);

  shader->qfeEnable();
  glBegin(GL_POINTS);
	glVertex3f(center.x, center.y, center.z);
  glEnd();
  shader->qfeDisable();

  return;
}


qfeReturnStatus qfeSeedVolume::qfeSetUpFBO(GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &fbo_depth_id)
{	
	glGetIntegerv(GL_FRAMEBUFFER_BINDING, &this->current_fbo_id);

	glBindTexture(GL_TEXTURE_2D, 0);                                // unlink all textures

	//create fbo texture
	createTexture(&fbo_texture_id,GL_TEXTURE_MAG_FILTER, GL_TEXTURE_MIN_FILTER, GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_T,
		GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_RGBA16F, GL_BGRA, GL_UNSIGNED_BYTE,vp_width,vp_height,0);
 
	// create a depth texture
	createTexture(&fbo_depth_id, GL_TEXTURE_MAG_FILTER, GL_TEXTURE_MIN_FILTER, GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_T,
	GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE,GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8,vp_width,vp_height,0);

	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	fbo_id = 0;
	glGenFramebuffers(1, &fbo_id);	
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);
	glViewport(0,0,vp_width,vp_height);

	// attach color and depth
	 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo_texture_id, 0);
	 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, fbo_depth_id, 0);

	// Always check that our framebuffer is ok
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		if(GL_FRAMEBUFFER_UNDEFINED == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_UNDEFINED"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER"<<std::endl;

		if(GL_FRAMEBUFFER_UNSUPPORTED == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_UNSUPPORTED"<<std::endl;
		
		if(GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE"<<std::endl;
		
		if(GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE"<<std::endl;
		
		if(GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS"<<std::endl;

		return qfeError;
	}

	// unbind fbo
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolume::qfeDisableFBO()
{
	// reset to base fbo
	glBindFramebuffer(GL_FRAMEBUFFER, current_fbo_id);
	glViewport(0,0,vp_width,vp_height);

	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolume::qfeEnableFBO(GLuint &fbo_id)
{
	glViewport (0, 0, vp_width, vp_height);                         // set The Current Viewport to the fbo size
	
	// bind the framebuffer as the output framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);
		
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);            // Clear Screen And Depth Buffer on the fbo

	glEnable(GL_DEPTH_TEST);
	
	// define the index array for the outputs
	GLuint attachments[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1,  attachments);

	glViewport(0,0,vp_width,vp_height);

	return qfeSuccess;
}

qfeReturnStatus qfeSeedVolume::qfeDeleteFBO(GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &fbo_depth_id)
{
	glDeleteFramebuffers(1, &fbo_id);

	// reset to base fbo
	glBindFramebuffer(GL_FRAMEBUFFER, current_fbo_id);

	//delete textures:
	glDeleteTextures(1, &fbo_texture_id);
	glDeleteTextures(1, &fbo_depth_id);
	
	return qfeSuccess;
}