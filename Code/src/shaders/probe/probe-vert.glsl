#version 330

uniform mat4 matrixModelViewProjection;

in  vec3 qfe_Vertex;
in  vec3 qfe_Normal;

out vec3 normal;

//----------------------------------------------------------------------------
void main(void)
{  
  normal = qfe_Normal;

  gl_Position = matrixModelViewProjection * vec4(qfe_Vertex,1.0);
}