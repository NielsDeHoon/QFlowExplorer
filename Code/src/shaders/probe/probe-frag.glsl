#version 330

uniform vec4 light;
uniform vec4 lightDirection;
uniform vec4 color;
uniform int  appearance;
uniform mat4 matrixModelViewInverse;

in vec3 normal;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s) ;

//----------------------------------------------------------------------------
vec4 Glass(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in vec2 s);

//----------------------------------------------------------------------------
vec4 Metal(in vec4 col, in vec3 l, in vec3 p);

//----------------------------------------------------------------------------
void main(void)
{
  vec4 col;
  vec3 lightDir;
  vec3 eye;  
  vec3 pcurv;
    
  eye      = (matrixModelViewInverse*vec4(0.0,0.0,1.0,0.0)).xyz;
  lightDir = (matrixModelViewInverse*-lightDirection).xyz;
  pcurv    = cross(normal, vec3(0.0,1.0,0.0)); 
  
  if(appearance == 0)
  {
    col = Glass(vec4(0.5,0.5,0.6,0.25), normalize(lightDir), normalize(normal), normalize(eye), vec2(2.0, 1.0)); 
  }
  else if(appearance == 1)
  {
    col = Metal(color, normalize(lightDir), normalize(pcurv)); 
    col = Phong(col, normalize(lightDir), normalize(normal), normalize(eye), light.x, light.y, vec2(light.z, light.w));   
      
    if( all(equal(abs(normal),vec3(0.0,0.0,1.0))) || all(equal(abs(normal),vec3(0.0,1.0,0.0))))  
    {
      col = color;
    }      
  }
  else if(appearance == 3)
  {
    col = vec4(0.0);
  }
  else if(appearance == 4)
  {
    col = color;    
  }
  else
  {
    col = Phong(color, normalize(lightDir), normalize(normal), normalize(eye), light.x, light.y, vec2(light.z, light.w));     
  }
  
  qfe_FragColor = col; 
}