// default-frag.glsl
uniform sampler3D volumeData;
uniform sampler2D rayStart;
uniform sampler2D rayEnd;
uniform sampler1D transferFunction;
uniform vec2      scalarRange;
uniform float     stepSize;

//----------------------------------------------------------------------------
void main()
{

  vec3 position  = gl_TexCoord[0].xyz;

  vec4 frag      = texture3D(volumeData, position);

  gl_FragColor = vec4(frag.xyz*16.0, 1.0);

}