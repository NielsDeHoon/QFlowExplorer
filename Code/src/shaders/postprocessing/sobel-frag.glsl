#version 430

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D sceneTex;

uniform float znear;
uniform float zfar;

vec3 luminanceWeighting = vec3(0.2125, 0.7154, 0.0721);

float readLuminance(in vec2 coord)
{
	return dot(texture2D(sceneTex,coord).rgb, luminanceWeighting);
}

float readDepth( in vec2 coord ) 
{
	return (2.0 * znear) / (zfar + znear - texture2D( depthTex, coord ).x * (zfar - znear));	
}

vec3 readNormal( in vec2 coord ) 
{
	//return texture2D(normalTex, coord).xyz;
	
	vec3 normal = texture2D(normalTex, coord).xyz * 2.0 - 1.0;
	if(length(normal)<0.000001)
		normal = vec3(0.0, 0.0, 1.0);
	return normalize(normal);
}
 
void main()
{	
	ivec2 tex_size = textureSize2D(sceneTex, 0).xy;
	
	float dxtex = 1.0 / tex_size.x /*image width*/;
	float dytex = 1.0 / tex_size.y /*image height*/;
	
	vec3 color = texture2D(sceneTex,textureCoord.xy).rgb;
	
	vec3 ownNormal = texture2D(normalTex, textureCoord.xy).rgb;
	
	vec2 coord = vec2(0.0, 0.0);
	//upper left:
	coord = textureCoord.xy + vec2(-dxtex, -dytex);
	float sul = readLuminance(coord);
	float dul = readDepth(coord);
	float nul = dot(ownNormal, readNormal(coord));
	
	//left:
	coord = textureCoord.xy + vec2(-dxtex, 0.0);
	float sl = readLuminance(coord);
	float dl = readDepth(coord);
	float nl = dot(ownNormal, readNormal(coord));
	
	//lower left:
	coord = textureCoord.xy + vec2(-dxtex, dytex);
	float sll = readLuminance(coord);
	float dll = readDepth(coord);
	float nll = dot(ownNormal, readNormal(coord));
	
	//up:
	coord = textureCoord.xy + vec2(0.0, -dytex);
	float su = readLuminance(coord);
	float du = readDepth(coord);
	float nu = dot(ownNormal, readNormal(coord));
	
	//down:
	coord = textureCoord.xy + vec2(0.0, dytex);
	float sd = readLuminance(coord);
	float dd = readDepth(coord);
	float nd = dot(ownNormal, readNormal(coord));
 
 	//upper right:
	coord = textureCoord.xy + vec2(dxtex, -dytex);
	float sur = readLuminance(coord);
	float dur = readDepth(coord);
	float nur = dot(ownNormal, readNormal(coord));
	
	//right:
	coord = textureCoord.xy + vec2(dxtex, 0.0);
	float sr = readLuminance(coord);
	float dr = readDepth(coord);
	float nr = dot(ownNormal, readNormal(coord));
	
	//lower right:
	coord = textureCoord.xy + vec2(dxtex, dytex);
	float slr = readLuminance(coord);
	float dlr = readDepth(coord);
	float nlr = dot(ownNormal, readNormal(coord));
		
	//https://en.wikipedia.org/wiki/Sobel_operator
	float Gx = 1.0 * (sul + dul + nul) //upper left
			+  2.0 * (sl  + dl  + nl) //left
			+  1.0 * (sll + dll + nll) // lower left
			+ -1.0 * (sur + dur + nur) //upper right
			+ -2.0 * (sr  + dr  + nr) // right
			+ -1.0 * (slr + dlr + nlr); //lower right
		
	float Gy = 1.0 * (sul + dul + nul) //upper left
			+  2.0 * (su  + du  + nu) //up
			+  1.0 * (sur + dur + nur) //upper right
			+ -1.0 * (sll + dll + nll) //lower left
			+ -2.0 * (sd  + dd  + nd) //down
			+ -1.0 * (slr + dlr + nlr); //lower right
			
	// divide by 3 because we have 3 [0,1] range values
	float G = sqrt(pow(Gx/3.0, 2.0) + pow(Gy/3.0, 2.0));
 
	if(G>0.9) // edge
	{
		color.rgb = vec3(0.0);
	}
 
	fragColor = color.rgb;
}