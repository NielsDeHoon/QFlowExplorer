#version 400

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D sceneTex;
uniform sampler2D depthTex;

uniform float znear;
uniform float zfar;

uniform float depth_darkening_power;
uniform int kernel_size;
uniform int blend_with_scene;

float gaussianFilter(sampler2D tex, vec2 texcoord, int size, float std=-1)
{
    if(std==-1)
		std=size*size;
		
	ivec2 screenSize = textureSize2D(tex, 0).xy;
		
	float step_x=1.0f/(screenSize.x);
	float step_y=1.0f/(screenSize.y);

	float arg=0;
	float h=0;
	float sum=0;
	float pixel = 0.0;   
   
	if(size==0)
	{
	   pixel=texture(tex,texcoord.xy).r;
	   sum=1;
	}
				  
	for(int i=0;i<size;i++)
	{
	   for(int j=0;j<size;j++)
	   {
		   float x=-(float(size)-1)/2.0f+i;
		   float y=-(float(size)-1)/2.0f+j;
		   arg=-(x*x+y*y)/(2*std*std);
		   h=exp(arg);
		   sum+=h;
		  		   
		   float depth   = texture(tex, texcoord.xy+vec2(x*step_x,y*step_y)).r;
			
		   if (depth > 0.0)
				pixel+=h*depth;
		   else
				pixel+=h*1;
	   }
	}
	return pixel/sum;
}

float linearDepth(float depthSample)
{
	float z_n = 2.0 * depthSample - 1.0;
    return 2.0 * znear * zfar / (zfar + znear - z_n * (zfar - znear));
}
 
void main()
{          
	ivec2 tex_size = textureSize2D(sceneTex, 0).xy;

	vec4 tex_color = texture(sceneTex, textureCoord);

	float depthFF = texture(depthTex, textureCoord.xy).x;
	
	fragColor = tex_color.rgb;
	
	if(depthFF>0.99) //background
		return;
		
	if(blend_with_scene<1)
		fragColor = vec3(1.0);
		
	float f_depth = 0.00;

	float pseudodepthFF = gaussianFilter(depthTex, textureCoord.xy, kernel_size);
	float deltaD = pseudodepthFF - depthFF;
			
	float shadow = 0.0;
	if(deltaD<0.000)
	{
		shadow = deltaD*50.0*depth_darkening_power;
	}
	fragColor.rgb += vec3(shadow);
}