#version 430

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D sceneTex;

//https://igortrindade.wordpress.com/2010/04/23/fun-with-opengl-and-shaders/

//sharpening weights:
// 0 -1  0
//-1  5 -1
// 0 -1  0

void main()
{
	vec3 sum = vec3(0.0);
	
	ivec2 screensize = textureSize2D(sceneTex, 0).xy;
 
	float pw = 1.0 / float(screensize.x);
	float ph = 1.0 / float(screensize.y);

	sum = 5.0*texture2D(sceneTex, textureCoord).rgb;
	
	sum -= texture2D(sceneTex, textureCoord + vec2(0.0, ph)).rgb;
	sum -= texture2D(sceneTex, textureCoord + vec2(0.0, -ph)).rgb;
	sum -= texture2D(sceneTex, textureCoord + vec2(pw, 0.0)).rgb;
	sum -= texture2D(sceneTex, textureCoord + vec2(-pw, 0.0)).rgb;
		
	fragColor = sum;
}