#version 430

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D sceneTex;

//https://www.shadertoy.com/view/lslGzl

float gamma = 2.2;

vec3 Uncharted2ToneMapping(vec3 color)
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;
	float W = 11.2;
	float exposure = 2.;
	color *= exposure;
	color = ((color * (A * color + C * B) + D * E) / (color * (A * color + B) + D * F)) - E / F;
	float white = ((W * (A * W + C * B) + D * E) / (W * (A * W + B) + D * F)) - E / F;
	color /= white;
	color = pow(color, vec3(1. / gamma));
	return color;
}

vec3 FilmicToneMapping(vec3 color)
{
	color = max(vec3(0.), color - vec3(0.004));
	color = (color * (6.2 * color + .5)) / (color * (6.2 * color + 1.7) + 0.06);
	color = pow(color, vec3(gamma));
	return color;
}
 
void main()
{   
	vec3 color = texture2D(sceneTex, textureCoord.xy).rgb;
	
	vec3 out_color = FilmicToneMapping(color);//Uncharted2ToneMapping(color);
  
	fragColor = out_color;
}