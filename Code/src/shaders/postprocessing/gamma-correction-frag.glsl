#version 430

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D sceneTex;

uniform float gamma;

//http://www.geeks3d.com/20101001/tutorial-gamma-correction-a-story-of-linearity/
 
void main()
{   	
	vec3 color = texture2D(sceneTex, textureCoord).rgb;

    fragColor = pow(color, vec3(1.0 / gamma));
}