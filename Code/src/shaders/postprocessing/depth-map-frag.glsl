#version 430

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D depthTex;
uniform sampler2D sceneTex;

uniform float znear;
uniform float zfar;

float readDepth( in vec2 coord )
{
	return (2 * znear) / (zfar + znear - texture2D( depthTex, textureCoord ).x * (zfar - znear));
}

void main() 
{ 
	//alternatively use:
	//http://www.gamedev.net/topic/529926-terrain-contour-lines-using-pixel-shader/
	
	fragColor = vec3(0.0);
	
	ivec2 tex_size = textureSize2D(depthTex, 0).xy;

	float depth = readDepth(textureCoord);
	
	fragColor = texture2D(sceneTex, textureCoord).rgb;
	
	if(depth>0.99)//background
		return;
	else if(3.0*znear >= zfar-1)
	{
		depth = (depth - 0.63) * 32.0; //some manual correction for the small range the depth is actually in
		depth = 1.0-depth; //swap distance to make closer whiter
	}
	
	fragColor = vec3(depth);
}