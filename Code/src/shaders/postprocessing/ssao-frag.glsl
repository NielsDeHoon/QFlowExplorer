#version 430
#extension GL_EXT_gpu_shader4 : enable

//SSAO code from
//http://www.opengl.org/discussion_boards/ubbthreads.php?ubb=showflat&Number=236805#Post236805

// Ouput data
out vec3 fragColor;

in vec2 textureCoord;

uniform sampler2D depthTex;
uniform sampler2D sceneTex;

uniform float znear;
uniform float zfar;

uniform float ao_power;

uniform int blend_with_scene;

//local:
float depth;
float d = 0.0;
 
float pw;
float ph;

float ao;
 
float aoMultiplier;

float aoscale;

float readDepth( in vec2 coord ) {
	return (2.0 * znear) / (zfar + znear - texture2D( depthTex, coord ).x * (zfar - znear));	
}
 
float compareDepths( in float depth1, in float depth2 ) {
	float aoCap = 1.0;
	//float aoMultiplier=10000.0;
	float depthTolerance=0.000;
	float aorange = 1.0*(ao_power*4.0);// units in space the AO effect extends to (this gets divided by the camera far range
	float diff = sqrt( clamp(1.0-(depth1-depth2) / (aorange/(zfar-znear)),0.0,1.0) );
	float _ao = min(aoCap,max(0.0,depth1-depth2-depthTolerance) * aoMultiplier) * diff;
	return _ao;
}

void compute_single_pass()
{
	d=readDepth( vec2(textureCoord.x+pw,textureCoord.y+ph));
	ao+=compareDepths(depth,d)/aoscale;
 
	d=readDepth( vec2(textureCoord.x-pw,textureCoord.y+ph));
	ao+=compareDepths(depth,d)/aoscale;
 
	d=readDepth( vec2(textureCoord.x+pw,textureCoord.y-ph));
	ao+=compareDepths(depth,d)/aoscale;
 
	d=readDepth( vec2(textureCoord.x-pw,textureCoord.y-ph));
	ao+=compareDepths(depth,d)/aoscale;
 
	d=readDepth( vec2(textureCoord.x+pw,textureCoord.y));
	ao+=compareDepths(depth,d)/aoscale;
 
	d=readDepth( vec2(textureCoord.x-pw,textureCoord.y));
	ao+=compareDepths(depth,d)/aoscale;
 
	d=readDepth( vec2(textureCoord.x,textureCoord.y+ph));
	ao+=compareDepths(depth,d)/aoscale;
 
	d=readDepth( vec2(textureCoord.x,textureCoord.y-ph));
	ao+=compareDepths(depth,d)/aoscale;
 
	pw*=2.0;
	ph*=2.0;
	aoMultiplier/=2.0;
	aoscale*=1.2;
}
 
void main(void)
{	
	ivec2 screensize = textureSize2D(depthTex, 0).xy;

	depth = readDepth( textureCoord );
	d;
 
	pw = 1.0 / float(screensize.x);
	ph = 1.0 / float(screensize.y);
 
	//float aoCap = 1.0;
 
	ao = 0.0;
 
	aoMultiplier=10000.0*ao_power;
 
	float depthTolerance = 0.001;
 
	aoscale=1.0;
	
	int loops = 8;
	
	for(int i = 0; i<loops; i++)
	{
		compute_single_pass();
 
		compute_single_pass();
 
		compute_single_pass();
 
		compute_single_pass();
	}
 
	ao/=16.0/ao_power;
	
	ao = pow(ao,ao_power);
	
	clamp(ao,0.25,0.5);
	
	if(readDepth(textureCoord) > 0.99) //background
	{
		fragColor = vec3(1.0-ao) * texture2D(sceneTex,textureCoord).rgb;
	}
	else
	{
		fragColor = vec3(1.0-ao);
		
		if(blend_with_scene == 1)
		{
			fragColor *= texture2D(sceneTex,textureCoord).rgb;
		}		
	}
}