#version 430
layout (location = 0) in vec2 Vertex;

out vec2 textureCoord;

const vec2 madd=vec2(0.5,0.5);

void main()
{
	textureCoord = Vertex.xy*madd+madd; // scale vertex attribute to [0-1] range
   
	gl_Position = vec4(Vertex.xy,0.0,1.0);
}