#version 400

in vec4 Vertex;
uniform mat4 modelViewMatrix;

uniform float particle_size;

uniform int particle_type;

uniform float znear;
uniform float zfar;

uniform int depth_darkening;

out vec3 viewVec;

void main()
{
	vec4 eyePos = inverse(modelViewMatrix) * vec4(0.0,0.0,0.0,1.0);
	viewVec = vec3(normalize(eyePos.xyz - Vertex.xyz));
		
	gl_Position = vec4(Vertex.xyz, 1.0); 
}