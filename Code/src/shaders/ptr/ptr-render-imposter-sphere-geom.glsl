#version 330

uniform mat4  modelViewProjection;
uniform mat4  modelViewProjectionInverse;

uniform int   particleSize;

layout(points )                        in;
layout(triangle_strip, max_vertices=4) out;

in particleAttrib
{
  vec4 position;
  vec4 attribs;
  
} particle[];

out imposterAttrib
{ 
  vec4 texcoord;
  vec4 attribs;
} imposter;

//----------------------------------------------------------------------------
void main(void)
{ 
  vec2  radius;
  vec4  x, y;
  vec4  p[4];
  
  imposter.attribs = particle[0].attribs;   
  
  radius   = vec2(float(particleSize)*0.5); 

  // zombie state
  if(imposter.attribs.x < -1.0) 
    radius = vec2(1.0);          
   
  x       = radius.x * normalize(modelViewProjectionInverse*vec4(1.0,0.0,0.0,0.0)); 
  y       = radius.y * normalize(modelViewProjectionInverse*vec4(0.0,1.0,0.0,0.0));   

  p[0] = vec4(particle[0].position.xyz,1.0) - x - y;
  p[1] = vec4(particle[0].position.xyz,1.0) - x + y;
  p[2] = vec4(particle[0].position.xyz,1.0) + x - y;
  p[3] = vec4(particle[0].position.xyz,1.0) + x + y;
   
  imposter.texcoord     = vec4(0.0,0.0,0.0,1.0);
  gl_Position           = modelViewProjection * p[0];
  EmitVertex();

  imposter.texcoord     = vec4(0.0,1.0,0.0,1.0);
  gl_Position           = modelViewProjection * p[1];
  EmitVertex();

  imposter.texcoord     = vec4(1.0,0.0,0.0,1.0);
  gl_Position           = modelViewProjection * p[2];
  EmitVertex();

  imposter.texcoord     = vec4(1.0,1.0,0.0,1.0);
  gl_Position           = modelViewProjection * p[3];
  EmitVertex();   
   
  EndPrimitive();
}
