#version 330

uniform sampler3D volumeData0; 
uniform sampler3D volumeData1; 
uniform sampler3D volumeData2; 

uniform mat4  projection;
uniform mat4  modelViewProjection;
uniform mat4  modelViewProjectionInverse;
uniform mat4  patientVoxelMatrix;

uniform int   particleSize;
uniform ivec3 volumeDimensions;
uniform float phaseCurrent;

layout(points )                        in;
layout(triangle_strip, max_vertices=4) out;

in particleAttrib
{
  vec4 position;
  vec4 attribs;
  
} particle[];

out imposterAttrib
{ 
  vec2 texcoord;
  vec4 attribs;
  mat2 rotation;
} imposter;

//----------------------------------------------------------------------------
vec3 TransformPosition(vec3 pos, mat4 matrix);

//----------------------------------------------------------------------------
vec3 FetchVelocityTemporalLinear(sampler3D vol1, sampler3D vol2, vec3 pos, float posTime, mat4 v2t);

//----------------------------------------------------------------------------
void OrthonormalBasis2(in vec3 v, out vec3 u, out vec3 w)
{
  vec3 s, sabs;
  
  s    = normalize(v);
  sabs = abs(s);
  u    = s;
  
  if((sabs.x <= sabs.y) && (sabs.x <= sabs.z))  
    u = vec3(0.0, -s.z, s.y); 
  else
  if((sabs.y <= sabs.x) && (sabs.y <= sabs.z))  
    u = vec3(-s.z, 0.0, s.x);  
  else
  if((sabs.z <= sabs.x) && (sabs.z <= sabs.y))  
    u = vec3(-s.y, s.x, 0.0);  
    
  u = normalize(u);  
  
  w = cross(s, u); 
}

//----------------------------------------------------------------------------
void main(void)
{   
  float particleTime;
  vec4  particlePatient;  
  vec4  particleScreen;  
  vec3  velocity, v[3], lambda;  
  vec4  vp[3];
  vec2  radius;
  vec2  axes[2];
  vec4  x, y, c;
  vec4  p[4];
  mat4  p2v, v2t;
  float venc;
  float s, t;
  
  venc    = 200.0;
   
  imposter.attribs = particle[0].attribs;   
  
  p2v = patientVoxelMatrix;
  v2t = mat4(1.0/float(volumeDimensions[0]), 0.0, 0.0,0.0,
             0.0, 1.0/float(volumeDimensions[1]), 0.0, 0.0,
             0.0, 0.0, 1.0/float(volumeDimensions[2]), 0.0,
             0.0, 0.0, 0.0, 1.0);
             
  particlePatient = vec4(particle[0].position.xyz, 1.0);  
  
  particleTime    = particle[0].position.w;
  velocity       = FetchVelocityTemporalLinear(volumeData1, volumeData2, TransformPosition(particlePatient.xyz, p2v), particleTime, v2t);  
  
  // determine the scaling of the glyph
  lambda.x = 0.5*particleSize + particleSize*(length(velocity)/(venc/2.0));
  lambda.y = 0.5*particleSize;
  lambda.z = 0.5*particleSize;  
  
  // in case of a zombie state
  if(imposter.attribs.x < -1.0) 
  {
    lambda = vec3(1.0);
  }
 
  // get the orthonormal basis of the ellipsoid in worldspace  
  OrthonormalBasis2(normalize(velocity),v[1],v[2]);  
  v[0]     = normalize(velocity) * lambda.x;  
  v[1]     = normalize(v[1])     * lambda.y;
  v[2]     = normalize(v[2])     * lambda.z;  
  
  // projection of the velocity ellipsoid in screen coordinates
  vp[0]    = (modelViewProjection*vec4(v[0],0.0));
  vp[1]    = (modelViewProjection*vec4(v[1],0.0));
  vp[2]    = (modelViewProjection*vec4(v[2],0.0)); 
  
  // determine the center in screen space
  c        = (modelViewProjection*particlePatient); 
  
  // determine the radius in screen coordinates  
  if(length(vp[0]) > length(vp[1]))
  {
    x        = vp[0];    
    y        = vp[1];
    radius.x = length(vp[0]);
    radius.y = length(vp[1]);
  }
  else
  {
    x        = vp[1];    
    y        = vp[0];
    radius.x = length(vp[1]);
    radius.y = length(vp[0]);
  }    
  
  if(length(vp[2]) > radius.x)  
  {
    y         = x;
    radius. y = radius.x;
    x         = vp[2];
    radius.x  = length(vp[2]);
  }
  else
  if(length(vp[2]) > radius.y)
  {
    y          = vp[2];
    radius.y   = length(vp[2]);    
  }
  
  // determine triangle winding variables
  s = t = 1.0;  
  if(x.x < 0.0) s = -1.0;
  if(y.y < 0.0) t = -1.0;    
  
  // determine the quad vertex positions in screen space
  p[0] = c - s*x - t*y;
  p[1] = c - s*x + t*y;
  p[2] = c + s*x - t*y;
  p[3] = c + s*x + t*y;
  
  // determine the rotation matrix
  imposter.rotation  = mat2(normalize(p[2]-p[0]).x, normalize(p[2]-p[0]).y,
                            normalize(p[1]-p[0]).x, normalize(p[1]-p[0]).y);
     
  imposter.texcoord  = vec2(0.0,0.0);
  gl_Position        = p[0];
  EmitVertex();

  imposter.texcoord  = vec2(0.0,1.0);
  gl_Position        = p[1];
  EmitVertex();

  imposter.texcoord  = vec2(1.0,0.0);
  gl_Position        = p[2];
  EmitVertex();

  imposter.texcoord  = vec2(1.0,1.0);
  gl_Position        = p[3];
  EmitVertex();   
   
  EndPrimitive();
}
