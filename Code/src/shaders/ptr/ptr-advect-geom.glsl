#version 330

uniform mat4 modelViewProjectionInverse;

uniform sampler3D volumeData0; //-2
uniform sampler3D volumeData1; //-1
uniform sampler3D volumeData2; // 0
uniform sampler3D volumeData3; // 1
uniform sampler3D volumeData4; // 2
uniform ivec3     volumeDimensions;

uniform mat4      patientVoxelMatrix;

uniform float     phaseCurrent;       // current phase at start of integration
uniform int       phaseCount;         // total phases

uniform int       traceSteps;         // steps count per trace
uniform float     traceStepSize;      // step size in phases
uniform float     traceStepDuration;  // duration of a full phase in ms
uniform int       traceDirection;
uniform int       traceScheme;        // 0 RK1 / 1 RK2 / 2 RK4
uniform int       traceSpeed;

layout(points)    	        in;
layout(points, max_vertices=20) out;

in particleAttrib
{
  vec4 position;
  vec4 attribs;
  
} particle[];

out vec4 particlePos;
out vec4 particleAttribs;

// attribs.x - age in phases (-1 = dead)
// attribs.y - current velocity (set during advection)
// attribs.z - color index
// attribs.w - birth age

//----------------------------------------------------------------------------
// Fetch curl
vec3 FetchCurl(sampler3D vol, vec3 pos, vec3 stepsize)
{
  vec3  Fdx, Fdy, Fdz;
  float F3dy, F2dz, F1dz, F3dx, F2dx, F1dy;
  
  Fdx =   texture(vol, pos + vec3(stepsize.x, 0.0, 0.0)).xyz -           
          texture(vol, pos - vec3(stepsize.x, 0.0, 0.0)).xyz;  
  Fdy =   texture(vol, pos + vec3(0.0, stepsize.y, 0.0)).xyz -
          texture(vol, pos - vec3(0.0, stepsize.y, 0.0)).xyz;  
  Fdz =   texture(vol, pos + vec3(0.0, 0.0, stepsize.z)).xyz -
          texture(vol, pos - vec3(0.0, 0.0, stepsize.z)).xyz;
                
  F3dy  = Fdy.z /2.0;
  F2dz  = Fdz.y /2.0;
  F1dz  = Fdz.x /2.0;
  F3dx  = Fdx.z /2.0;
  F2dx  = Fdx.y /2.0;
  F1dy  = Fdy.x /2.0;
                 
  return vec3(F3dy-F2dz, F1dz-F3dx, F2dx-F1dy);
}

//----------------------------------------------------------------------------
vec3 TransformPosition(vec3 pos, mat4 matrix);

//----------------------------------------------------------------------------
vec3 FetchVelocityLinearTemporalLinear(sampler3D d0, sampler3D d1, sampler3D d2, sampler3D d3, sampler3D d4, vec3 pos, float posTime, float seedTime, mat4 v2t);

//----------------------------------------------------------------------------
vec4 IntegrateRungeKutta1(sampler3D d0,sampler3D d1,sampler3D d2,sampler3D d3,sampler3D d4, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t);
vec4 IntegrateRungeKutta2(sampler3D d0,sampler3D d1,sampler3D d2,sampler3D d3,sampler3D d4, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t);
vec4 IntegrateRungeKutta4(sampler3D d0,sampler3D d1,sampler3D d2,sampler3D d3,sampler3D d4, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t);

//----------------------------------------------------------------------------
void main() {

  vec4  particlePatient;    
  vec3  particleTexture;
  vec4  pos;  
  vec3  velocity;  
  vec3  curl;
  vec4  attribs; 
  vec3  dims;
  float posTimeNext;
      
  dims      = vec3(volumeDimensions);
  
  // initialize transformations
  mat4 p2v; // patient to voxel   coordinates
  mat4 v2t; // voxel   to texture coordinates
  
  p2v = patientVoxelMatrix;
  v2t = mat4(1.0/dims.x, 0.0, 0.0,0.0,
             0.0, 1.0/dims.y, 0.0, 0.0,
             0.0, 0.0, 1.0/dims.z, 0.0,
             0.0, 0.0, 0.0, 1.0);
             
  // get the particle position  
  particlePatient   = particle[0].position;  
  attribs           = particle[0].attribs;
  
  // pos is the intermediate advected position 
  // pos.xyz - patient coordinates
  // pos.w   - seed time in phases
  pos         = particlePatient;
  posTimeNext = particlePatient.w + traceDirection*traceSteps*traceStepSize;  
  posTimeNext = mod(posTimeNext, phaseCount);
   
  // particle is alive and the loaded volume corresponds to the current particle time
  if((attribs.x >= 0.0) && (int(posTimeNext) == floor(phaseCurrent)))
  {    
    // perform the trace integration
    for(int i=0; i<traceSteps; i++)
    {
      if(traceScheme == 0)
        pos = IntegrateRungeKutta1(volumeData0, volumeData1, volumeData2, volumeData3, volumeData4, 
                                   pos, phaseCurrent, traceStepSize, traceStepDuration, traceDirection, traceSpeed, p2v, v2t);    
      else
      if(traceScheme == 1)
        pos = IntegrateRungeKutta2(volumeData0, volumeData1, volumeData2, volumeData3, volumeData4, 
                                   pos, phaseCurrent, traceStepSize, traceStepDuration, traceDirection, traceSpeed, p2v, v2t);    
      else
        pos = IntegrateRungeKutta4(volumeData0, volumeData1, volumeData2, volumeData3, volumeData4, 
                                   pos, phaseCurrent, traceStepSize, traceStepDuration, traceDirection, traceSpeed, p2v, v2t);  
    }

    // make sure the particle has arrived at the right point in time
    pos.w        = mod(phaseCurrent,phaseCount);    

    // get the current velocity    
    //velocity     = texture(volumeData2, TransformPosition(pos.xyz, v2t*p2v)).xyz;
    velocity    = FetchVelocityLinearTemporalLinear(volumeData0, volumeData1, volumeData2, volumeData3, volumeData4, 
                                                    TransformPosition(pos.xyz, p2v), pos.w, phaseCurrent, v2t);
    
    // get the current curl
    curl         = FetchCurl(volumeData2, TransformPosition(pos.xyz, v2t*p2v), vec3(1.0/vec3(volumeDimensions)));
  
    // update the particle attributes
    attribs.x = attribs.x + traceDirection*(traceStepSize*traceSteps); // age
    attribs.y = length(velocity);                                      // speed
    //attribs.z = length(curl);                                        // vorticity

    if(attribs.x < 0.0) attribs.x = 0.0;
  }
  
  // particle hit the boundaries
  if(attribs.x < -2.0)  
  {
    attribs.x = attribs.x + traceDirection*(traceStepSize*traceSteps); // age    
    attribs.y = 0.0;
    
    // recovered from the fading steps, set ready for reincarnation
    if(attribs.x >= -2.0) attribs.x = -2.0;
  }

  // assign the new position and emit  
  particlePos     = pos;
  particleAttribs = attribs;    

  EmitVertex(); 
  
  EndPrimitive();
} 
 