#version 330

in vec4 qfe_ParticlePosition;
in vec4 qfe_ParticleAttribute;

out particleAttrib
{
  vec4 position;
  vec4 attribs;
  
} particle;

//----------------------------------------------------------------------------
void main(void)
{
   particle.position  = qfe_ParticlePosition;
   particle.attribs   = vec4(qfe_ParticleAttribute);  
}
