#version 330

uniform sampler1D transferFunction;
uniform vec2      dataRange;
uniform vec4      light;
uniform vec4      lightDirection;

uniform int       surfaceShading;

uniform mat4      matrixModelViewInverse;

in  vec4  frontColor;
in  vec4  normal;
in  vec2  texCoord;
in  float speed;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Cel(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s) ;

//----------------------------------------------------------------------------
void main()
{   
  const float PI = 3.141592;
  vec4  color, shaded;
  vec3  eye, n, lightDir; 
  float ndotv;
  float speedIndex;
  
  float scaleT, frac1, frac2;
  float fuzz   = 0.1;
  float width  = 0.2;
    
  eye       = normalize(matrixModelViewInverse * vec4(0.0,0.0,1.0,0.0)).xyz;
  lightDir  = normalize(matrixModelViewInverse * -lightDirection).xyz;   
  ndotv     = dot(normal.xyz, eye.xyz);
  
  // fix normals for thin structure
  if(ndotv < 0.0)
    n = -normal.xyz;
  else
    n = normal.xyz;
  
  speedIndex    = speed / max(abs(dataRange.x), abs(dataRange.y));
  
  color = texture(transferFunction, speedIndex);
 
  if(surfaceShading == 0) // cel
  {
    shaded = Cel(color, lightDir, n.xyz);    
  }
  else
  if(surfaceShading == 1) // cel
  {
    shaded = Phong(color, lightDir, n.xyz, eye, light.x, light.y, vec2(light.z, light.w));  
  }
  else
  if(surfaceShading == 2) // procedural stripes
  {    
    scaleT = fract(texCoord.y * 10.0);
    frac1  = clamp(scaleT/fuzz,0.0,1.0);
    frac2  = clamp((scaleT - width)/fuzz, 0.0, 1.0);
      
    frac1 = frac1 * (1.0-frac2);
    frac1 = frac1 * frac1 * (3.0 - (2.0*frac1));
    
    color  = mix(vec4(0.4,0.4,0.4,1.0), color, frac1);
       
    shaded = Phong(color, lightDir, n.xyz, eye, light.x, light.y, vec2(light.z, light.w)); 
  } 
  else  
  {
    shaded = color;
  }
    
  if(texCoord.x > 0.95)                      shaded = vec4(vec3(0.0),1.0);
  if(texCoord.y > 0.45 && texCoord.y < 0.55) shaded = vec4(vec3(0.0),1.0);
  if(texCoord.y < 0.1  || texCoord.y > 0.9)  shaded = vec4(vec3(0.0),1.0);
  
  if(shaded.a   == 0.0)       discard;
  
  qfe_FragColor = shaded;
}