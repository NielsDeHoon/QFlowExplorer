#version 330

uniform sampler3D volumeData0; //-2
uniform sampler3D volumeData1; //-1
uniform sampler3D volumeData2; // 0
uniform sampler3D volumeData3; // 1
uniform sampler3D volumeData4; // 2
uniform ivec3     volumeDimensions;

uniform mat4      voxelPatientMatrix;
uniform mat4      patientVoxelMatrix;

uniform float     phaseStart;         // start phase of the line trace
uniform float     phaseCurrent;       // current phase at start of integration (not necessarily the current viewed phase!)

uniform int       traceSamples;       // amount of points to generate on the line
uniform int       traceSteps;         // steps count per trace
uniform float     traceStepSize;      // step size in phases
uniform float     traceStepDuration;  // duration of a full phase in ms
uniform int       traceDirection;
uniform int       traceScheme;        // 0 RK1 / 1 RK2 / 2 RK4
uniform int       traceSpeed;
uniform vec2      texRange;

layout(points)                   in;
layout(points, max_vertices=128) out; // GL_MAX_GEOMETRY_OUTPUT_VERTICES

in  vec4  seedPosition[1];

out vec4  qfe_SurfacePoints;
out vec4  qfe_SurfaceAttribs; // x = velocity, y, z = tex coord 0 (pathline), w = tex coord 1 (timeline)

//----------------------------------------------------------------------------
vec3 TransformPosition(vec3 pos, mat4 matrix);

//----------------------------------------------------------------------------
vec3 FetchVelocityLinearTemporalLinear(sampler3D d0, sampler3D d1, sampler3D d2, sampler3D d3, sampler3D d4, vec3 pos, float posTime, float seedTime, mat4 v2t);

//----------------------------------------------------------------------------
vec4 IntegrateRungeKutta1(sampler3D d0,sampler3D d1,sampler3D d2,sampler3D d3,sampler3D d4, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t);
vec4 IntegrateRungeKutta2(sampler3D d0,sampler3D d1,sampler3D d2,sampler3D d3,sampler3D d4, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t);
vec4 IntegrateRungeKutta4(sampler3D d0,sampler3D d1,sampler3D d2,sampler3D d3,sampler3D d4, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t);

//----------------------------------------------------------------------------
void main() 
{  
  vec4  seedPatient;   
  vec4  posTexture;
  vec4  pos;    
  int   steps;
  vec3  velocity;
  vec3  tangent;
  float endtime;
  
  // initialize transformations
  mat4 p2v; // patient to voxel   coordinates
  mat4 v2t; // voxel   to texture coordinates
  
  p2v = patientVoxelMatrix;
  v2t = mat4(1.0/volumeDimensions[0], 0.0, 0.0,0.0,
             0.0, 1.0/volumeDimensions[1], 0.0, 0.0,
             0.0, 0.0, 1.0/volumeDimensions[2], 0.0,
             0.0, 0.0, 0.0, 1.0);
             
  // get the seed position  
  seedPatient   = seedPosition[0];  
  
  // pos is the intermediate advected position 
  // pos.xyz - patient coordinates
  // pos.w   - current seed time in phases
  //           tricky part here, apply the time shift for adjacency
  pos.xyz = seedPatient.xyz;
  pos.w   = phaseCurrent; 
  
  // perform the trace integration   (for n samples, we need n-1 steps)
  steps = int(max(traceSamples-3, traceSteps)/float(traceSamples-3));
  
  // get the initial velocity  
  velocity    = FetchVelocityLinearTemporalLinear(volumeData0, volumeData1, volumeData2, volumeData3, volumeData4, 
                                                  TransformPosition(pos.xyz, p2v), pos.w, phaseCurrent, v2t);
  
  qfe_SurfacePoints   = pos;  
  qfe_SurfaceAttribs  = vec4(0.0, gl_PrimitiveIDIn, 0.0, 0.0);

  // we deal with the adjacency information here
  for(int i=1; i<traceSamples-2; i++)
  {    
    vec4 currentPos, previousPos; 
    vec3 currentVel, previousVel; 
    
    previousPos = pos;   
    previousVel = velocity;
      
    for(int j=0; j<steps; j++)
    {
      if(traceScheme == 0)
        pos = IntegrateRungeKutta1(volumeData0, volumeData1, volumeData2, volumeData3, volumeData4, 
                                   pos, phaseCurrent, traceStepSize, traceStepDuration, traceDirection, traceSpeed, p2v, v2t);    
      else
      if(traceScheme == 1)
        pos = IntegrateRungeKutta2(volumeData0, volumeData1, volumeData2, volumeData3, volumeData4,
                                   pos, phaseCurrent, traceStepSize, traceStepDuration, traceDirection, traceSpeed, p2v, v2t);    
      else
        pos = IntegrateRungeKutta4(volumeData0, volumeData1, volumeData2, volumeData3, volumeData4, 
                                   pos, phaseCurrent, traceStepSize, traceStepDuration, traceDirection, traceSpeed, p2v, v2t);        
    }        
          
    // check the bounding box condition
    posTexture = (patientVoxelMatrix*vec4(pos.xyz,1.0)) / vec4(vec3(volumeDimensions),1.0);
    if( any(lessThanEqual(posTexture.xyz,vec3(0.0))) || any(greaterThanEqual(posTexture.xyz,vec3(1.0))) )    
      pos.xyz = previousPos.xyz;    
      
    // set the current position and velocity
    currentPos  = pos;
    //currentVel  = texture(volumeData2, TransformPosition(pos.xyz, v2t*p2v)).xyz;
    //currentVel  = FetchVelocityTemporalLinear(volumeData2, volumeData3, TransformPosition(pos.xyz, p2v), pos.w, v2t);
    currentVel    = FetchVelocityLinearTemporalLinear(volumeData0, volumeData1, volumeData2, volumeData3, volumeData4, 
                                                      TransformPosition(pos.xyz, p2v), pos.w, phaseCurrent, v2t);
    velocity      = currentVel;
         
    if(i == 1)     
    {      
      tangent = currentPos.xyz-previousPos.xyz;
      
      qfe_SurfacePoints.xyz = previousPos.xyz - tangent;      
      qfe_SurfacePoints.w   = previousPos.w - (currentPos.w - previousPos.w);   
      qfe_SurfaceAttribs    = vec4(length(previousVel),0.0, -1.0/texRange.x, gl_PrimitiveIDIn/texRange.y);
      EmitVertex();
      
      qfe_SurfacePoints     = previousPos;      
      qfe_SurfaceAttribs    = vec4(length(previousVel), 0.0, 0.0/texRange.x, gl_PrimitiveIDIn/texRange.y);
      EmitVertex();
    }
    else
    if(i == traceSamples-3)
    {
      tangent = currentPos.xyz-previousPos.xyz;
      endtime = phaseCurrent + float(traceSteps)*traceStepSize;
      
      qfe_SurfacePoints   = previousPos;      
      qfe_SurfaceAttribs  = vec4(length(previousVel), 0.0, (i-1)/texRange.x, gl_PrimitiveIDIn/texRange.y);
      EmitVertex();
          
      qfe_SurfacePoints   = currentPos;    
      qfe_SurfacePoints.w = endtime;
      qfe_SurfaceAttribs  = vec4(length(currentVel), 0.0, i/texRange.x, gl_PrimitiveIDIn/texRange.y);
      EmitVertex();
      
      qfe_SurfacePoints.xyz = currentPos.xyz + tangent;      
      qfe_SurfacePoints.w   = endtime+(endtime - previousPos.w);      
      qfe_SurfaceAttribs    = vec4(length(currentVel), 0.0, (i+1)/texRange.x, gl_PrimitiveIDIn/texRange.y);
      EmitVertex();
    }
    else
    {
      qfe_SurfacePoints   = previousPos;      
      //qfe_SurfacePoints.w = max(pos.w,0.0); //?
      qfe_SurfaceAttribs  = vec4(length(previousVel), 0.0, (i-1)/texRange.x, gl_PrimitiveIDIn/texRange.y);
      EmitVertex();
    }      
    
  }    
  
  EndPrimitive();  
} 
 