#version 330

uniform float traceSamples;

layout(lines_adjacency)        in;
layout(points, max_vertices=3) out; 

in  vec4  line[4];
in  vec4  lineLeft[4];
in  vec4  lineRight[4];

out vec4  qfe_SurfaceNormals;

//----------------------------------------------------------------------------
void main() {
  vec3 center;
  vec3 neighbors[4]; // top bottom left right    
  vec3 normal = vec3(0.0);

  center       = line[1].xyz;
  neighbors[0] = line[2].xyz;  
  neighbors[1] = line[0].xyz;  
  neighbors[2] = lineLeft[1].xyz;
  neighbors[3] = lineRight[1].xyz;
    
  // compute vertex normal for smooth rendering
  normal = normal + cross(neighbors[3]-center, neighbors[0]-center);
  normal = normal - cross(neighbors[2]-center, neighbors[0]-center);
  normal = normal - cross(neighbors[3]-center, neighbors[1]-center);
  normal = normal + cross(neighbors[2]-center, neighbors[1]-center);  
  normal = normalize(normal);
  
  // fill out the line strip adjacency with zero normals
  if(gl_PrimitiveIDIn == 0)
  {
    qfe_SurfaceNormals = vec4(0.0);
    EmitVertex();  
  }  
    
  qfe_SurfaceNormals = vec4(normal,1.0);
  EmitVertex();  
  
  // fill out the line strip adjacency with zero normals
  if(gl_PrimitiveIDIn == int(traceSamples-4.0)) // -4 for adjacency
  {
    qfe_SurfaceNormals = vec4(normal,1.0);  
    EmitVertex();  
  
    qfe_SurfaceNormals = vec4(0.0);        
    EmitVertex();    
  }  
} 
 