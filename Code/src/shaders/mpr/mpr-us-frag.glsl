// mpr-us-frag
#version 330

uniform sampler3D planeDataSet1;
uniform sampler3D planeDataSet2;
uniform int       planeDataContext;
uniform int       planeDataFocus;
uniform int       planeDataComponentCount;
uniform int       planeDataRepresentation;  // Context
uniform int       planeDataRepresentation2; // Focus
uniform vec2      planeDataRange1;
uniform vec2      planeDataRange2;
uniform vec3      planeNormal;
uniform vec3      planeDirection;
uniform vec3      voxelSize;
uniform sampler1D transferFunction;
uniform sampler1D transferFunctionUS;
uniform sampler1D transferFunctionUS2;
uniform float     triggerTime;
uniform int       scale;
uniform int       balance;
uniform int       speedThreshold;
uniform int       viewAngle;
uniform float     contrast;
uniform float     brightness;
uniform float     opacity;

in vec3 texCoord[];

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
float NormalizeRange(float value, vec2 range);

//----------------------------------------------------------------------------
vec3  NormalizeRange(vec3 value, vec2 range);

//----------------------------------------------------------------------------
float FetchScalarValue(sampler3D vol, vec3 pos);

//----------------------------------------------------------------------------
vec3  FetchVectorValue(sampler3D vol, vec3 pos);

//----------------------------------------------------------------------------
vec3  FetchTransferFunction(sampler1D tf, float value);

//----------------------------------------------------------------------------
float FetchDivergence(sampler3D vol, vec3 pos, vec3 stepsize);

//----------------------------------------------------------------------------
vec3  FetchCurl(sampler3D vol, vec3 pos, vec3 stepsize);

//----------------------------------------------------------------------------
float FetchLPC(sampler3D vol, vec3 pos, vec3 stepsize);

//----------------------------------------------------------------------------
float FetchEDC(sampler3D vol, vec3 pos, vec3 stepsize);

//----------------------------------------------------------------------------
float FetchAngleDerivative(sampler3D vol, vec3 pos, vec3 direction, vec3 normal, vec3 stepsize);

//----------------------------------------------------------------------------
mat3  FetchStructureTensor(sampler3D vol, vec3 pos);

//----------------------------------------------------------------------------
mat3  FetchStructureTensorAvg(sampler3D vol, vec3 pos, vec3 stepsize, int neighbourlevel);

//----------------------------------------------------------------------------
vec3  FetchEigenValuesSymmetric(mat3 m);

//----------------------------------------------------------------------------
// Note: second data set is always pca-p
//----------------------------------------------------------------------------
void main()
{
  vec4  color, color1, color2;
  vec2  dataRange1, dataRange2;
  vec3  samplePos1, samplePos2; 
  vec3  sampleVector1, sampleVector2, sampleVectorTmp;
  float sampleScalar1 = 0.0, sampleScalar2 = 0.0;   
  float fraction;
  
  float viewAngleCurrent, viewAngleBand;
  float speed, angle, angleDerivative, coherence;
  vec3  velocity, velocityInPlane;
  float sampleUS;  
  
  float PI = 3.14159265358979323846264;

  color    = vec4(0.0);
  color1   = vec4(0.0);
  color2   = vec4(0.0);
  fraction = float(balance) / 100.0;
 
  dataRange1.xy = planeDataRange1.xy;
  dataRange2.xy = planeDataRange2.xy;
  if(planeDataContext == 0)
  {
    dataRange1 = vec2(0.0, max(abs(planeDataRange1.x),abs(planeDataRange1.y))); 
    dataRange2 = vec2(0.0, max(abs(planeDataRange2.x),abs(planeDataRange2.y))); 
  }  
      
  samplePos1    = texCoord[0].xyz;   
  samplePos2    = texCoord[1].xyz;   
    
  // Determine the selected dataset rendering  
  if(planeDataComponentCount == 3 || planeDataComponentCount == 9)
  {
    sampleVector1 = NormalizeRange(FetchVectorValue(planeDataSet1, samplePos1), dataRange1);     
  
    if(planeDataContext == 0) // magnitude
      sampleScalar1 = length(sampleVector1);  
    if(planeDataContext == 1) // x-component
      sampleScalar1 = sampleVector1.r;       
    if(planeDataContext == 2) // y-component
      sampleScalar1 = sampleVector1.g;   
    if(planeDataContext == 3) // z-component
      sampleScalar1 = sampleVector1.b;       
    if(planeDataContext == 4) // divergence
      sampleScalar1 = NormalizeRange(FetchDivergence(planeDataSet1, samplePos1, voxelSize), dataRange1);      
    if(planeDataContext == 5) // curl x
      sampleScalar1 = NormalizeRange(FetchCurl(planeDataSet1, samplePos1, voxelSize).x, dataRange1);    
    if(planeDataContext == 6) // curl y
      sampleScalar1 = NormalizeRange(FetchCurl(planeDataSet1, samplePos1, voxelSize).y, dataRange1);      
    if(planeDataContext == 7) // curl z
      sampleScalar1 = NormalizeRange(FetchCurl(planeDataSet1, samplePos1, voxelSize).z, dataRange1);      
    if(planeDataContext == 8) // coherence (LPC)
      sampleScalar1 = FetchLPC(planeDataSet1, samplePos1, voxelSize);      
    if(planeDataContext == 9) // coherence (EDC)         
      sampleScalar1 = FetchEDC(planeDataSet1, samplePos1, voxelSize);
  }
  else 
    sampleScalar1 = NormalizeRange(FetchScalarValue(planeDataSet1, samplePos1), dataRange1); 
    
  // Determine the pca-p dataset rendering
  sampleVectorTmp = FetchVectorValue(planeDataSet2, samplePos2)*(float(scale)/100.0); 
  sampleVector2   = NormalizeRange(sampleVectorTmp, dataRange2); 
  
  if(planeDataContext == 0) // magnitude
    sampleScalar2 = length(sampleVector2);  
  if(planeDataContext == 1) // x-component
    sampleScalar2 = sampleVector2.r;       
  if(planeDataContext == 2) // y-component
    sampleScalar2 = sampleVector2.g;   
  if(planeDataContext == 3) // z-component
    sampleScalar2 = sampleVector2.b; 
  if(planeDataContext == 4) // divergence
    sampleScalar2 = NormalizeRange(FetchDivergence(planeDataSet2, samplePos2, voxelSize), dataRange2);
  if(planeDataContext == 5) // curl x
    sampleScalar2 = NormalizeRange(FetchCurl(planeDataSet2, samplePos2, voxelSize).x, dataRange2);
  if(planeDataContext == 6) // curl y
    sampleScalar2 = NormalizeRange(FetchCurl(planeDataSet2, samplePos2, voxelSize).y, dataRange2);
  if(planeDataContext == 7) // curl z
    sampleScalar2 = NormalizeRange(FetchCurl(planeDataSet2, samplePos2, voxelSize).z, dataRange2);
  if(planeDataContext == 8) // coherence (LPC)
    sampleScalar2 = FetchLPC(planeDataSet2, samplePos2, voxelSize);
  if(planeDataContext == 9) // coherence (EDC)
    sampleScalar2 = FetchEDC(planeDataSet2, samplePos2, voxelSize);  

  // Get the appearance right
  if(planeDataRepresentation == 0) // gray
  {
    color1 = vec4(vec3(sampleScalar1), 1.0);    
    color2 = vec4(vec3(sampleScalar2), 1.0);    
  }    
  if(planeDataRepresentation == 1) // color
  {
    color1 = vec4(FetchTransferFunction(transferFunction,sampleScalar1), 1.0);
    color2 = vec4(FetchTransferFunction(transferFunction,sampleScalar2), 1.0);
  }     
  
  // Mix the fragment colors
  color    = vec4((1.0-fraction)*color1 + fraction*color2);  
  color.a  = opacity;
    
  // Mimic Doppler for blood-flow  
  viewAngleCurrent = 1.0-(float(viewAngle)/180.0);
  viewAngleBand    = 1.0-(max(0.0,float(viewAngle-10))/180.0);
  speed            = length(sampleVectorTmp);  
  

  if(speed >= float(speedThreshold))  
  {
    if(all(equal(planeNormal, planeDirection)))
      velocityInPlane = sampleVectorTmp;
    else
      velocityInPlane = sampleVectorTmp - (dot(sampleVectorTmp,planeNormal))*planeNormal;
      
    angle           = dot(normalize(velocityInPlane),normalize(planeDirection));
    
    sampleUS        = NormalizeRange(length(velocityInPlane),dataRange2);
        
    if(planeDataFocus == 1) // vorticity / curl
      sampleUS = 2.0*NormalizeRange(length(FetchCurl(planeDataSet2, samplePos2, voxelSize)), dataRange2);
    if(planeDataFocus == 2) // angle  
      sampleUS = 0.5*abs(angle);
    if(planeDataFocus == 3) // angle derivative
      sampleUS = 0.5*FetchAngleDerivative(planeDataSet2, samplePos2, planeDirection, planeNormal, voxelSize);
    if(planeDataFocus == 4) // coherence
      sampleUS = 0.5*FetchLPC(planeDataSet2, samplePos2, voxelSize);

    sampleUS        = 0.5*(sign(angle)*sampleUS) +0.5;
    
    if(planeDataRepresentation2 == 0) // gray
      color = vec4(vec3(sampleUS), 1.0);  
    if(planeDataRepresentation2 == 1) // transfer function       
      color = vec4(FetchTransferFunction(transferFunction, sampleUS), 1.0);      
    if(planeDataRepresentation2 == 2) // doppler 1
      color = vec4(FetchTransferFunction(transferFunctionUS, sampleUS), 1.0);    
    if(planeDataRepresentation2 == 3) // doppler 2
      color = vec4(FetchTransferFunction(transferFunctionUS2, sampleUS), 1.0);            
    if(planeDataRepresentation2 == 4) // variance by coherence
    {
      vec4  green     = vec4(0.0, 1.0, 0.351, 1.0);
      float coherence = FetchLPC(planeDataSet2, samplePos2, voxelSize);
      float frac      = abs((sampleUS*2.0)-1.0);      
      
      color = vec4(FetchTransferFunction(transferFunctionUS, sampleUS), 1.0);    
      color = mix(color, green, frac*(1.0-coherence));      
    }
      
    
    if(angle >= viewAngleCurrent)
    {
      if(angle < viewAngleBand)
      {
        float frac = 1.0-((angle-viewAngleCurrent)/(viewAngleBand-viewAngleCurrent));                                               
        color = (1.0-frac)*color2 + frac*color;      
      }
      else
        color = color2;          
    }    
        
    // Set higher opacity for velocity part
    color.a  = min(opacity*1.5, 1.0);
  }
  
  // Render black when outside of the volume
  if(any(lessThan(texCoord[0].xyz, vec3(0.0))) || any(greaterThan(texCoord[0].xyz, vec3(1.0))))
    color = vec4(vec3(0.0),opacity);
  
  // Final adjustments    
  // Apply contrast
  float c;
  c = (1.0/(1.0-contrast));
  
  color.rgb = ((color.rgb - 0.5) * c) + 0.5;  
  
  // Apply brightness
  color.rgb += brightness;     
    
  qfe_FragColor = color;
  

}