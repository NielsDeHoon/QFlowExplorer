// mpr-stat-frag.glsl
#version 330

uniform sampler3D planeDataSet1; // 2D + time data
uniform sampler3D planeDataSet2; // 3D data at current time
uniform int       planeDataComponent;
uniform int       planeDataRepresentation;
uniform vec2      planeDataRange1;
uniform vec2      planeDataRange2;
uniform vec3      planeNormal;
uniform sampler1D transferFunction;
uniform float     triggerTime;  // Normalized [0,1] for automatic linear interpolation
uniform int       balance;
uniform float     contrast;
uniform float     brightness;
uniform float     opacity;

in vec3 texCoord[];

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
float NormalizeRange(float value, vec2 range);

//----------------------------------------------------------------------------
vec3 NormalizeRange(vec3 value, vec2 range);

//----------------------------------------------------------------------------
float FetchScalarValue(sampler3D vol, vec3 pos);

//----------------------------------------------------------------------------
vec3 FetchVectorValue(sampler3D vol, vec3 pos);

//----------------------------------------------------------------------------
vec3 FetchTransferFunction(sampler1D tf, float value);

//----------------------------------------------------------------------------
void main()
{  
  vec2  dataRange;
  vec3  samplePos2D, samplePos3D; 
  float sampleScalar2D        = 0.0;  
  float sampleScalar3D        = 0.0;  
  vec3  sampleVector3D;
  float sampleScalar3DOpacity = 1.0;
  vec4  color2D;
  vec4  color3D;
  vec4  color;
  float fraction;
 
  color          = vec4(0.0);
  fraction       = float(balance) / 100.0;
  
  // Fix the range for some visual styles of the 3D vectors
  dataRange = planeDataRange2;
  if(planeDataComponent == 1)
    dataRange    = vec2(0.0, max(abs(planeDataRange2.x),abs(planeDataRange2.y)));  
  
  // Determine the data values
  samplePos2D    = vec3(texCoord[0].xy, triggerTime);   
  sampleScalar2D = NormalizeRange(FetchScalarValue(planeDataSet1, samplePos2D), planeDataRange1.xy);   
  
  samplePos3D    = vec3(texCoord[1].xyz); 
  sampleVector3D = NormalizeRange(FetchVectorValue(planeDataSet2, samplePos3D), dataRange.xy);   
    
  // Determine the way to render the 3D vector
  sampleScalar3D = sampleVector3D.z;  
  
  if(planeDataComponent == 0)
  {  
    vec3 velocity  = (sampleVector3D*2.0)-1.0;
    sampleScalar3D = dot(normalize(planeNormal), velocity);
    sampleScalar3D = (sampleScalar3D+1.0)/2.0;
  }
  
  if(planeDataComponent == 1)
      sampleScalar3D = length(sampleVector3D);  
  
  if(planeDataComponent == 2)
    sampleScalar3D = sampleVector3D.x;
    
  if(planeDataComponent == 3)
    sampleScalar3D = sampleVector3D.y;
    
  if(planeDataComponent == 4)
    sampleScalar3D = sampleVector3D.z;
  
  // Now determine the color      
  if(any(lessThan(texCoord[1].xyz, vec3(0.0))) || any(greaterThan(texCoord[1].xyz, vec3(1.0))))
    sampleScalar3DOpacity = 0.0;        
    
  color2D = vec4(vec3(sampleScalar2D),1.0);  
  color3D = vec4(vec3(sampleScalar3D),sampleScalar3DOpacity); 
    
  // Depict the fetched scalar    
  if(planeDataRepresentation == 0) // gray    
    color = (1.0-fraction)*color2D + fraction*color3D;        
    
  if(planeDataRepresentation == 1) // color
    color = vec4(FetchTransferFunction(transferFunction,(1.0-fraction)*sampleScalar2D + fraction*sampleScalar3D), (1.0-fraction)+fraction*sampleScalar3DOpacity);  
    
  if(planeDataRepresentation == 2) // red-blue  
  {
    float angle;
    vec3  vec;
    
    // Color for 3D vector
    vec    = NormalizeRange(FetchVectorValue(planeDataSet2, samplePos3D), dataRange.xy);
    vec    = (vec*2.0)-1.0;
    angle  = dot(planeNormal.xyz, vec);
          
    if(angle > 0.0)
    {
      color3D.r = angle;  color3D.b = 0.0;
    }
    else
    {
      color3D.r = 0.0; color3D.b = (-1.0 * angle);
    }        
    color3D.g = 0.0;    
    
    // Color for 2D vector
    if(sampleScalar2D > 0.5)
    {
      color2D.r = (sampleScalar2D-0.5)*2.0; color2D.b = 0.0;
    }
    else
    {
      color2D.r = 0.0; color2D.b = (1.0-sampleScalar2D*2.0);
    }
    color2D.g = 0.0;    

    color = vec4((1.0-fraction)*color2D.rgb + fraction*color3D.rgb,1.0);  
  }
  
  // Final adjustments    
  // Apply contrast
  float c;
  c = (1.0/(1.0-contrast));
  
  color.rgb = ((color.rgb - 0.5) * c) + 0.5;
  
  // Apply brightness
  color.rgb += brightness;
  
  color.a    = opacity * color.a;
  
  
  qfe_FragColor = color;

}