// mpr-func.glsl
#version 330

//----------------------------------------------------------------------------
// Matrix: Trace
float trace(mat3 m)
{
  return (m[0][0] + m[1][1] + m[2][2]);
} 

//----------------------------------------------------------------------------
// Matrix: Determinant
float det(mat3 m)
{
  float result;
  result = m[0][0]*m[1][1]*m[2][2] + 
           m[1][0]*m[2][1]*m[0][2] + 
           m[2][0]*m[0][1]*m[1][2] - 
           m[0][0]*m[2][1]*m[1][2] - 
           m[1][0]*m[0][1]*m[2][2] - 
           m[2][0]*m[1][1]*m[0][2];
  return result;
} 

//----------------------------------------------------------------------------
// Matrix: Identity matrix
mat3 eye()
{
  mat3 m;
  
  m[0][0] = m[1][1] = m[2][2] = 1.0;
  m[0][1] = m[0][2] = 0.0;
  m[1][0] = m[1][2] = 0.0;
  m[2][0] = m[2][1] = 0.0;
  
  return m;
} 

//----------------------------------------------------------------------------
// Compute structure tensor
mat3 ComputeStructureTensor(vec3 vec)
{
  mat3 st; // [column][row]
   
  st[0][0] = vec.x * vec.x;
  st[0][1] = vec.x * vec.y;
  st[0][2] = vec.x * vec.z;
  
  st[1][0] = vec.x * vec.y;
  st[1][1] = vec.y * vec.y;
  st[1][2] = vec.y * vec.z;
  
  st[2][0] = vec.x * vec.z;
  st[2][1] = vec.y * vec.z;
  st[2][2] = vec.z * vec.z;
  
  return st;
}

//----------------------------------------------------------------------------
vec3 ComputeRoots (mat3 m)
{  
  float inv3, root3;
  float c0, c1, c2;
  float c2Div3, aDiv3, mbDiv2;
  float q;
  float magnitude, angle, cs, sn, tmp;  
  vec3  ev;
  
  inv3  = 1.0/3.0;
  root3 = sqrt(float(3.0));  
  
  c0 = m[0][0] * m[1][1] * m[2][2] + 2.0*m[1][0] * m[2][0] * m[2][1] - m[0][0] * m[2][1] * m[2][1] - m[1][1] * m[2][0] * m[2][0] - m[2][2] * m[1][0] * m[1][0];
  c1 = m[0][0] * m[1][1] - m[1][0] *     m[1][0] + m[0][0] * m[2][2] - m[2][0] * m[2][0] + m[1][1] * m[2][2] - m[2][1] * m[2][1];
  c2 = m[0][0] + m[1][1] + m[2][2];
  
  c2Div3 = c2*inv3;
  aDiv3  = (c1 - c2*c2Div3)*inv3;

  if (aDiv3 > 0.0) aDiv3 = 0.0;
  
  mbDiv2 = 0.5*(c0 + c2Div3*(2.0*c2Div3*c2Div3 - c1));
  q      = mbDiv2*mbDiv2 + aDiv3*aDiv3*aDiv3;
  
  if (q > 0.0) q = 0.0;
  
  magnitude = sqrt(-aDiv3);
  angle     = atan(sqrt(-q),mbDiv2)*inv3;
  cs        = cos(angle);
  sn        = sin(angle);
  
  ev.x = c2Div3 + 2.0*magnitude*cs;
  ev.y = c2Div3 - magnitude*(cs + root3*sn);
  ev.z = c2Div3 - magnitude*(cs - root3*sn);
  
  // Sort the roots here to obtain root[0] <= root[1] <= root[2].
  tmp = min(ev.x, ev.y); ev.x = ev.x + ev.y - tmp; ev.y = tmp;
  tmp = min(ev.y, ev.z); ev.y = ev.y + ev.z - tmp; ev.z = tmp;
    
  return ev;
}


//----------------------------------------------------------------------------
// Fetch a vector
vec3 FetchVectorValue(sampler3D vol, vec3 pos)
{
  return texture(vol, pos).rgb;
}

//----------------------------------------------------------------------------
// Fetch a scalar
float FetchScalarValue(sampler3D vol, vec3 pos)
{
  return texture(vol, pos).r;
}

//----------------------------------------------------------------------------
// Fetch from transfer function
vec3 FetchTransferFunction(sampler1D tf, float value)
{
  return texture(tf, value).rgb;
}

//----------------------------------------------------------------------------
// Fetch divergence
float FetchDivergence(sampler3D vol, vec3 pos, vec3 stepsize)
{
  float F1dx  = FetchVectorValue(vol, pos + vec3(stepsize.x, 0.0, 0.0)).x -
                FetchVectorValue(vol, pos - vec3(stepsize.x, 0.0, 0.0)).x;
  float F2dy  = FetchVectorValue(vol, pos + vec3(0.0, stepsize.y, 0.0)).y -
                FetchVectorValue(vol, pos - vec3(0.0, stepsize.y, 0.0)).y;
  float F3dz  = FetchVectorValue(vol, pos + vec3(0.0, 0.0, stepsize.z)).z -
                FetchVectorValue(vol, pos - vec3(0.0, 0.0, stepsize.z)).z;
                 
  return (F1dx/2.0 + F2dy/2.0 + F3dz/2.0);
}

//----------------------------------------------------------------------------
// Fetch curl
vec3 FetchCurl(sampler3D vol, vec3 pos, vec3 stepsize)
{
  vec3  Fdx, Fdy, Fdz;
  float F3dy, F2dz, F1dz, F3dx, F2dx, F1dy;
  
  Fdx =    FetchVectorValue(vol, pos + vec3(stepsize.x, 0.0, 0.0)) -
           FetchVectorValue(vol, pos - vec3(stepsize.x, 0.0, 0.0));  
  Fdy =    FetchVectorValue(vol, pos + vec3(0.0, stepsize.y, 0.0)) -
           FetchVectorValue(vol, pos - vec3(0.0, stepsize.y, 0.0));  
  Fdz =    FetchVectorValue(vol, pos + vec3(0.0, 0.0, stepsize.z)) -
           FetchVectorValue(vol, pos - vec3(0.0, 0.0, stepsize.z));
                
  F3dy  = Fdy.z /2.0;
  F2dz  = Fdz.y /2.0;
  F1dz  = Fdz.x /2.0;
  F3dx  = Fdx.z /2.0;
  F2dx  = Fdx.y /2.0;
  F1dy  = Fdy.x /2.0;
                 
  return vec3(F3dy-F2dz, F1dz-F3dx, F2dx-F1dy);
}

//----------------------------------------------------------------------------
// Fetch structure tensor
mat3 FetchStructureTensor(sampler3D vol, vec3 pos)
{
  vec3 v;
  mat3 st; // [column][row]
  
  v  = FetchVectorValue(vol, pos);
  st = ComputeStructureTensor(v);
  
  return st; 
}

//----------------------------------------------------------------------------
// Fetch structure tensor, averaged over a specified region
mat3 FetchStructureTensorAvg(sampler3D vol, vec3 pos, vec3 stepsize, int neighbourlevel)
{
  vec3  step, vec;
  mat3  avg, st; // [column][row]
  float fraction;    
    
  avg   = mat3(0.0);
  
  fraction = 1.0 / pow(((2.0*float(neighbourlevel))+1.0),3.0);

  for(int u=-1*neighbourlevel; u<=neighbourlevel; u++)
  {
    for(int v=-1*neighbourlevel; v<=neighbourlevel; v++)
    {
      for(int w=-1*neighbourlevel; w<=neighbourlevel; w++)
      {
        step = vec3(float(u)*stepsize.x, float(v)*stepsize.y, float(w)*stepsize.z);
        vec  = FetchVectorValue(vol, pos + step);
        st   = ComputeStructureTensor(vec);
        avg  = avg + fraction*st;        
      }
    }
  }  
  
  return avg;
}

//----------------------------------------------------------------------------
// Fetch eigenvalues of 3x3 matrix 
vec3 FetchEigenValues(mat3 m)
{
  return ComputeRoots(m);
}

//----------------------------------------------------------------------------
// Fetch LPC (Local Phase Coherence)
float FetchLPC(sampler3D vol, vec3 pos, vec3 stepsize)
{
  vec3  center, step;
  float result = 0.0;
  
  center = normalize(FetchVectorValue(vol, pos));

  for(int u=-1; u<2; u++)
  {
    for(int v=-1; v<2; v++)
    {
      for(int w=-1; w<2; w++)
      {
        step = vec3(float(u)*stepsize.x, float(v)*stepsize.y, float(w)*stepsize.z);
        result = result + dot(center, normalize(FetchVectorValue(vol, pos + step)));
      }
    }
  }
                 
  return (result/26.0);
}

//----------------------------------------------------------------------------
// Fetch EDC (Eigenvalue Discontinuity Coherence)
float FetchEDC(sampler3D vol, vec3 pos, vec3 stepsize)
{
  mat3  structuretensor;
  vec3  lambda;
  float r, c;
  
  structuretensor = FetchStructureTensorAvg(vol, pos, stepsize, 1);
  lambda          = FetchEigenValues(structuretensor);
  
  //lambda.x = abs(lambda.x);
  //lambda.y = abs(lambda.y);
  lambda.x = lambda.x * 100.0;
  lambda.y = lambda.y * 100.0;  
  
  c  = lambda.x + lambda.y;
  r  = 1.0 - ((4.0*lambda.x*lambda.y) / (c*c));
  
  //r = pow((lambda.x - lambda.y)/(lambda.x + lambda.y), 2.0);   
  
  //r = (4.0*lambda.x*lambda.y) / pow(lambda.x + lambda.y, 2.0);
  
  return r;    
}

//----------------------------------------------------------------------------
// Fetch Angle Derivative
float FetchAngleDerivative(sampler3D vol, vec3 pos, vec3 direction, vec3 normal, vec3 stepsize)
{                
  vec3  gradient;
  vec3  posTex[6]; 
  vec3  v[6];
  float a[6];
    
  //if(all(greaterThan(pos, stepsize)) && all(lessThan(pos, -1.0*stepsize)))
  //{
    posTex[0] = pos + vec3( stepsize.x, 0.0,  0.0);
    posTex[1] = pos + vec3(-stepsize.x, 0.0,  0.0);
    posTex[2] = pos + vec3( 0.0, stepsize.y,  0.0);
    posTex[3] = pos + vec3( 0.0,-stepsize.y,  0.0);
    posTex[4] = pos + vec3( 0.0,  0.0, stepsize.z);
    posTex[5] = pos + vec3( 0.0,  0.0,-stepsize.z);
    
    for(int i =0; i<6; i++)
    {
      v[i] = normalize(FetchVectorValue(vol, posTex[i]));
      a[i] = dot(v[i] - (dot(v[i],normal))*normal, direction);
    }
      
    gradient.x = 0.5 * a[0] - a[1];  
    gradient.y = 0.5 * a[2] - a[3];  
    gradient.z = 0.5 * a[4] - a[5];  
  //}
  //else
  //  gradient = vec3(0.0);  
  
  return length(gradient);
}

//----------------------------------------------------------------------------
// Normalize to [0,1] range
// r = (d-c)/(b-a) (here we map to (0,1), so (d-c) = (1 - 0) = 1.0
// y = (x-a)*r+c
vec3 NormalizeRange(vec3 value, vec2 range)
{
  vec3 result;

  result.r = (value.r - range.x) * ( 1.0 / (range.y - range.x) );
  result.g = (value.g - range.x) * ( 1.0 / (range.y - range.x) );
  result.b = (value.b - range.x) * ( 1.0 / (range.y - range.x) );

  return result;

}

//----------------------------------------------------------------------------
// Normalize to [0,1] range
// r = (d-c)/(b-a) (here we map to (0,1), so (d-c) = (1 - 0) = 1.0
// y = (x-a)*r+c
float NormalizeRange(float value, vec2 range)
{
  float result;

  result = (value - range.x) * ( 1.0 / (range.y - range.x) );
  
  return result;
}