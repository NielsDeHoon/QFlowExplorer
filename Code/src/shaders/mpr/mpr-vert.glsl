// mpr-vert.c
#version 330

uniform mat4 matrixModelViewProjection;

void main()
{
  gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;
  gl_Position    = matrixModelViewProjection * gl_Vertex;
}