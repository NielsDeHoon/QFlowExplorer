// mpr-stat-vert.c
#version 330

uniform mat4 matrixModelViewProjection;

in vec3 qfe_Vertex;
in vec3 qfe_TexCoord0;
in vec3 qfe_TexCoord1;

out vec3 texCoord[2];

void main()
{
  //gl_TexCoord[0] = gl_MultiTexCoord0;
  //gl_TexCoord[1] = gl_MultiTexCoord1;
  //gl_Position    = matrixModelViewProjection * gl_Vertex;
  //gl_TexCoord[0] = gl_MultiTexCoord0;
  
  texCoord[0] = qfe_TexCoord0;
  texCoord[1] = qfe_TexCoord1;  
  gl_Position = matrixModelViewProjection*vec4(qfe_Vertex,1.0);
}