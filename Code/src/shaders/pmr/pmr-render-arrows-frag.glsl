#version 330

uniform sampler1D lineTransferFunction;

uniform vec4      light;
uniform vec4      lightDirection;
uniform int       lineShading;
uniform mat4      matrixModelView;
uniform mat4      matrixModelViewInverse;

in arrowAttrib
{
  float radiusDepth;
  vec3  normal;
  vec2  texcoord; // (x = arclength, y = left-right)
  vec4  attribs;
  vec4  color;
} arrow;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
void main()
{ 
  const float delta = 0.05;
  
  vec4  color, shaded;
  float depth, yCoordNorm1, yCoordNorm2;    
  float speed;
  
  // determine if completely transparent
  if(arrow.color.a <= delta) discard;
  
  // initialize vectors  
  vec3 n = normalize(arrow.normal);
  vec3 l = normalize(-lightDirection.xyz);
  vec3 e = vec3(0.0,0.0,1.0);
  
  // apply shading (we bump the color a bit to highlight the arrow heads)
  shaded = arrow.color;
  if(lineShading < 3)
    shaded = Phong(shaded, l, n, e, light.x, light.y, vec2(light.z,light.w));  
  shaded.a = arrow.color.a;    

  qfe_FragColor = shaded;  
}