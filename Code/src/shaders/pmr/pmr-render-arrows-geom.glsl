#version 330

uniform sampler1D lineTransferFunction;

uniform float phaseSpan;
uniform int   phaseVisible;
uniform float phaseCurrent;
uniform mat4  patientVoxelMatrix;
uniform float lineWidth;
uniform float arrowScale;
uniform mat4  matrixModelView;
uniform mat4  matrixModelViewInverse;
uniform mat4  matrixModelViewProjection;
uniform mat4  matrixProjection;

uniform vec2  dataRange;
uniform int   colorScale;
uniform int   colorType;
uniform vec3  colorUni;

layout(lines_adjacency )                in;
layout(triangle_strip, max_vertices=38) out;

in seedAttrib
{
  vec4 position; // in patient coordinates (x,y,z,t)
  vec4 attribs;  // x = seedPhase, y = speed, z = z-val, w = cluster id
} seed[];

out arrowAttrib
{
  float radiusDepth;
  vec3  normal;
  vec2  texcoord; // (x = arclength, y = left-right)
  vec4  attribs;
  vec4  color;
} arrow;

//----------------------------------------------------------------------------
// Copyright (c) 2012 Corey Tabaka
vec4 hsv_to_rgb(float h, float s, float v, float a)
{
	float c = v * s;
	h = mod((h * 6.0), 6.0);
	float x = c * (1.0 - abs(mod(h, 2.0) - 1.0));
	vec4 color;
 
	if (0.0 <= h && h < 1.0) {
		color = vec4(c, x, 0.0, a);
	} else if (1.0 <= h && h < 2.0) {
		color = vec4(x, c, 0.0, a);
	} else if (2.0 <= h && h < 3.0) {
		color = vec4(0.0, c, x, a);
	} else if (3.0 <= h && h < 4.0) {
		color = vec4(0.0, x, c, a);
	} else if (4.0 <= h && h < 5.0) {
		color = vec4(x, 0.0, c, a);
	} else if (5.0 <= h && h < 6.0) {
		color = vec4(c, 0.0, x, a);
	} else {
		color = vec4(0.0, 0.0, 0.0, a);
	}
 
	color.rgb += v - c;
 
	return color;
}

//----------------------------------------------------------------------------
void orthonormalBasis2(in vec3 v, out vec3 u, out vec3 w)
{
  vec3 s, sabs;
  
  s    = normalize(v);
  sabs = abs(s);
  u    = s;
  
  if((sabs.x <= sabs.y) && (sabs.x <= sabs.z))  
    u = vec3(0.0, -s.z, s.y); 
  else
  if((sabs.y <= sabs.x) && (sabs.y <= sabs.z))  
    u = vec3(-s.z, 0.0, s.x);  
  else
  if((sabs.z <= sabs.x) && (sabs.z <= sabs.y))  
    u = vec3(-s.y, s.x, 0.0);  
    
  u = normalize(u);  
  
  w = cross(s, u); 
}

//----------------------------------------------------------------------------
void renderCone(vec3 start, vec3 end, float radius, vec4 color)
{
  const float twoPi    = 2.0 * 3.14159;
  const int   sections = 8;
  vec3  p[2*sections+3], n[2*sections+3];
  vec3  lineTangent, u, v;
  vec3  base;
  float theta;
  
  mat4  matrixNormal   = inverse(matrixModelViewInverse);  
  
  // Initialize
  lineTangent = end-start;  
  base        = end - lineTangent;
  theta       = twoPi / float(sections);  
  
  orthonormalBasis2(normalize(lineTangent), u, v);
   
  // Generate the cone
  p[0] = end;
  for(int i = 0; i <= sections; i++) 
  { 
    p[2*i+1] = vec3(base + ( sin(float(i)*theta)*radius*u ) + ( cos(float(i)*theta)*radius*v ));
    p[2*i+2] = end;   
    
    n[2*i+1] = normalize(p[2*i+1]-base);
    n[2*i+2] = normalize(lineTangent);
    
  }  
  
  // Render the cone
  for(int i=0; i < 2*sections+3; i++)
  {
    arrow.normal  = (matrixNormal * vec4(n[i],0.0)).xyz;
    arrow.color   = color; 
    gl_Position   = matrixModelViewProjection*vec4(p[i],1.0);
    EmitVertex();
  }
  
  EndPrimitive();
}

//----------------------------------------------------------------------------
void main()
{   
  const float delta = 0.001;
  const float ratio = 3.0;

  vec3  eye, eyeWorld;
  vec4  posScreen;
  vec3  lineTangent;
  float lineLength;
  vec3  linePoints[2];
  vec4  vertexColor;
  
  float speed, speedScale;  
  float phaseSeed;
  float goldenRatio; 
    
  // initialize
  eye            = vec3(0.0,0.0,1.0);        
  eyeWorld       = normalize( matrixModelViewInverse*vec4(0.0,0.0,1.0,0.0)).xyz;   

  phaseSeed      = seed[1].attribs.x;
  goldenRatio    = (1.0 + sqrt(5.0))/2.0;
  speed          = seed[1].attribs.y / max(abs(dataRange.x), abs(dataRange.y));
  speedScale     = max(1.0,3.0*speed);
 
  // compute color
  vertexColor = vec4(colorUni,1.0);  
 
  if(colorType == 1) // Pattern Type
  {
    vertexColor = texture(lineTransferFunction, (seed[1].attribs.z+1.0)/2.0);      
  }
  
  if(colorType == 2) // Pattern Cluster ID
  {
    float value = seed[1].attribs.z*goldenRatio;
    float h     = value - floor(value);    
    vertexColor = hsv_to_rgb(h, 0.5, 0.95, 1.0);
  }  
  
  if(colorType == 3) // Speed  
  {     
    vertexColor = texture(lineTransferFunction, speed);      
  }  
  
  bool isInsideSpan = ((phaseSpan != 0.0) && (seed[1].position.w > phaseSeed - 0.5*phaseSpan) && (seed[2].position.w < phaseSeed + 0.5*phaseSpan));
  bool isAtCurrent  = (seed[1].position.w < phaseCurrent) && (seed[2].position.w >= phaseCurrent);
  bool isVisible;
    
  if(phaseVisible == -1)
    isVisible = true;
  else 
    isVisible = (phaseVisible == round(seed[1].attribs.x));
    
  // Render arrow at the current phase, within the user-defined span
  if(isInsideSpan && isAtCurrent && isVisible)
  { 
    // render arrow
    lineTangent         = normalize(seed[2].position.xyz - seed[1].position.xyz);
    lineLength          = length(seed[2].position.xyz - seed[1].position.xyz);    
    linePoints[0]       = seed[1].position.xyz + 0.5*lineLength*lineTangent - 0.5*ratio*arrowScale*speedScale*lineWidth*lineTangent;
    linePoints[1]       = seed[1].position.xyz + 0.5*lineLength*lineTangent + 0.5*ratio*arrowScale*speedScale*lineWidth*lineTangent;
            
    renderCone(linePoints[0], linePoints[1], arrowScale*speedScale*lineWidth, vertexColor);      
    
    /*
    // render arrow shadow
    lineTangent         = normalize(seed[1].position.xyz - seed[0].position.xyz);
    lineLength          = length(seed[1].position.xyz - seed[0].position.xyz);    
    linePoints[0]       = seed[0].position.xyz + 0.5*lineLength*lineTangent - 0.5*ratio*speedScale*lineWidth*lineTangent;
    linePoints[1]       = seed[0].position.xyz + 0.5*lineLength*lineTangent + 0.5*ratio*speedScale*lineWidth*lineTangent;
        
    vertexColor.a = 0.5;
    renderCone(linePoints[0], linePoints[1], speedScale*lineWidth, vertexColor);          
    */
  }
    
}