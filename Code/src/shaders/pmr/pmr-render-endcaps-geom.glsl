#version 330

uniform sampler1D lineTransferFunction;

uniform float phaseSpan;
uniform int   phaseVisible;
uniform mat4  patientVoxelMatrix;
uniform float lineWidth;
uniform mat4  matrixModelViewProjection;
uniform mat4  matrixModelViewInverse;
uniform mat4  matrixProjection;

uniform vec2  dataRange;
uniform int   colorScale;
uniform int   colorType;
uniform vec3  colorUni;

layout(lines_adjacency )                in;
layout(triangle_strip, max_vertices=4) out;

in seedAttrib
{
  vec4 position; // in patient coordinates (x,y,z,t)
  vec4 attribs;  // x = seedPhase, y = speed, z = z-val, w = cluster id
} seed[];

out capAttrib
{
  float radiusDepth;
  vec3  normal;
  vec2  texcoord; // (x = arclength, y = left-right)
  vec4  attribs;
  vec4  color;
} cap;

//----------------------------------------------------------------------------
// Copyright (c) 2012 Corey Tabaka
vec4 hsv_to_rgb(float h, float s, float v, float a)
{
	float c = v * s;
	h = mod((h * 6.0), 6.0);
	float x = c * (1.0 - abs(mod(h, 2.0) - 1.0));
	vec4 color;
 
	if (0.0 <= h && h < 1.0) {
		color = vec4(c, x, 0.0, a);
	} else if (1.0 <= h && h < 2.0) {
		color = vec4(x, c, 0.0, a);
	} else if (2.0 <= h && h < 3.0) {
		color = vec4(0.0, c, x, a);
	} else if (3.0 <= h && h < 4.0) {
		color = vec4(0.0, x, c, a);
	} else if (4.0 <= h && h < 5.0) {
		color = vec4(x, 0.0, c, a);
	} else if (5.0 <= h && h < 6.0) {
		color = vec4(c, 0.0, x, a);
	} else {
		color = vec4(0.0, 0.0, 0.0, a);
	}
 
	color.rgb += v - c;
 
	return color;
}

//----------------------------------------------------------------------------
void main()
{   
  const float delta = 0.001;

  vec3  eye, eyeWorld;
  vec4  posScreen;
  vec4  strip[4];
  vec3  lineTangent;
  vec3  capStart, capNormal, capX, capY;
  vec4  vertexColor;
  mat4  matrixNormal;
  
  float speed;  
  float phaseSeed;
  float goldenRatio; 
    
  // initialize
  eye            = vec3(0.0,0.0,1.0);        
  eyeWorld       = normalize( matrixModelViewInverse*vec4(0.0,0.0,1.0,0.0)).xyz;   
  
  phaseSeed      = seed[1].attribs.x;
  goldenRatio    = (1.0 + sqrt(5.0))/2.0;
  
  matrixNormal   = inverse(matrixModelViewInverse);  
 
  // compute color
  vertexColor = vec4(colorUni,1.0);  
 
  if(colorType == 1) // Pattern Type
  {
    vertexColor = texture(lineTransferFunction, (seed[1].attribs.z+1.0)/2.0);      
  }
  
  if(colorType == 2) // Pattern Cluster ID
  {
    float value = seed[1].attribs.z*goldenRatio;
    float h     = value - floor(value);    
    vertexColor = hsv_to_rgb(h, 0.5, 0.95, 1.0);
  }  
  
  if(colorType == 3) // Speed  
  {    
    speed       = seed[1].attribs.y / max(abs(dataRange.x), abs(dataRange.y));
    //speed     = speed * (float(colorScale)/100.0);
    
    vertexColor = texture(lineTransferFunction, speed);      
  }
  
  bool isStartCap = (phaseSpan != 0.0) && (seed[1].position.w <= phaseSeed - 0.5*phaseSpan) && (seed[2].position.w >= phaseSeed - 0.5*phaseSpan);
  bool isEndCap   = (phaseSpan != 0.0) && (seed[1].position.w <= phaseSeed + 0.5*phaseSpan) && (seed[2].position.w >= phaseSeed + 0.5*phaseSpan);  
  bool isVisible;
      
  if(phaseVisible == -1)
    isVisible = true;
  else 
    isVisible = (phaseVisible == round(seed[1].attribs.x));    
    
  // Start cap
  if((isStartCap || isEndCap) && isVisible)
  {     
    // compute geometry
    if(isStartCap)    
    {
      lineTangent        = normalize(seed[3].position.xyz - seed[2].position.xyz);           
      capStart           = seed[2].position.xyz;
      capNormal          = (matrixNormal * vec4(-lineTangent.xyz,0.0)).xyz;
    }
    else    
    {
      lineTangent        = normalize(seed[1].position.xyz - seed[0].position.xyz);            
      capStart           = seed[1].position.xyz;
      capNormal          = (matrixNormal * vec4(lineTangent.xyz,0.0)).xyz;
    }
    
    
    // build the basis    
    capX               = normalize(cross(eyeWorld, lineTangent));
    capY               = cross(lineTangent, capX);
    
    // create the strip
    strip[0] = vec4(capStart - 0.5*lineWidth*capX - 0.5*lineWidth*capY, 1.0);
    strip[1] = vec4(capStart - 0.5*lineWidth*capX + 0.5*lineWidth*capY, 1.0);
    strip[2] = vec4(capStart + 0.5*lineWidth*capX - 0.5*lineWidth*capY, 1.0);
    strip[3] = vec4(capStart + 0.5*lineWidth*capX + 0.5*lineWidth*capY, 1.0);
  
    // render the cap  
    //posScreen         = matrixModelViewProjection * (strip[0] + vec4(0.0,0.0,0.5*lineWidth,0.0));
    //cap.radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
    cap.normal        = capNormal;
    cap.texcoord      = vec2(0.0,0.0);    
    cap.attribs       = seed[1].attribs;
    cap.color         = vertexColor;
    gl_Position       = matrixModelViewProjection * strip[0];     
    EmitVertex();

    //posScreen         = matrixModelViewProjection * (strip[1] + vec4(0.0,0.0,0.5*lineWidth,0.0));
    //cap.radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
    cap.normal        = capNormal;
    cap.texcoord      = vec2(0.0,1.0);
    cap.attribs       = seed[1].attribs;
    cap.color         = vertexColor;
    gl_Position       = matrixModelViewProjection * strip[1];     
    EmitVertex();

    //posScreen         = matrixModelViewProjection * (strip[2] + vec4(0.0,0.0,0.5*lineWidth,0.0));
    //cap.radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
    cap.normal        = capNormal;
    cap.texcoord      = vec2(1.0,0.0);
    cap.attribs       = seed[2].attribs;
    cap.color         = vertexColor;
    gl_Position       = matrixModelViewProjection * strip[2];     
    EmitVertex();

    //posScreen         = matrixModelViewProjection * (strip[3] + vec4(0.0,0.0,0.5*lineWidth,0.0));
    //cap.radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
    cap.normal        = capNormal;
    cap.texcoord      = vec2(1.0,1.0);
    cap.attribs       = seed[2].attribs;
    cap.color         = vertexColor;
    gl_Position       = matrixModelViewProjection * strip[3];     
    EmitVertex();    
  }
    
}