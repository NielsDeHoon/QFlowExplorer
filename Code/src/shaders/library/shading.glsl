#version 330

float metal[20] = float[20](0.0, 0.0, 0.140, 0.242, 0.102, 0.0, 0.032, 0.164, 0.006, 0.0, 0.0, 0.040, 0.055, 0.172, 0.211, 0.143, 0.133, 0.206, 0.0224, 0.14);

//----------------------------------------------------------------------------
float diffuse(in vec3 l, in vec3 n) 
{
  float ldotn = dot(l, n);
  float diff  = max(ldotn, 0.0);

  return diff; 
}

//----------------------------------------------------------------------------
float diffuseLine(in vec3 l, in vec3 t) 
{
  float ldott = dot(l, t);
  float ndotl = sqrt(1.0 - pow(ldott, 2.0));

  return ndotl; 
}


//----------------------------------------------------------------------------
float specular(in vec3 h, in vec3 n, in float s) 
{

  float hdotn = max(dot(h, n),0.0);
  float spec  = pow(hdotn, s);
  
  return spec;
}

//----------------------------------------------------------------------------
float specularLine(in vec3 v, in vec3 l, in vec3 t, in float n) 
{
  float ldott = dot(l, t);
  float vdott = dot(v, t);
  float ndotl = sqrt(1.0 - pow(ldott, 2.0));
  float rdotv = max(0.0, ndotl * sqrt(1.0-pow(vdott, 2.0)) - ldott*vdott);
  
  float spec;
  
  if(ndotl < 0.01)
    spec = 0.0;
  else
    spec = pow(rdotv, n);
  
  return spec;
}

//----------------------------------------------------------------------------
// col = color
// l   = light vector
// n   = normal vector
//----------------------------------------------------------------------------
vec4 Cel(in vec4 col, in vec3 l, in vec3 n) 
{
  float intensity;
  vec4  color;
  
  intensity = dot(n,l);
  
  color = col;
  
  if(intensity > 0.9)
    color = vec4(col.rgb + vec3(0.4), col.a);
  else if (intensity > 0.5)
    color = vec4(col.rgb + vec3(0.1), col.a);
  else if (intensity > 0.25)
    color = vec4(col.rgb - vec3(0.1), col.a);
  else 
    color = vec4(col.rgb - vec3(0.2), col.a);  
  
  return color;
}

//----------------------------------------------------------------------------
// col = color
// l   = light vector
// n   = normal vector
//----------------------------------------------------------------------------
vec4 Cel2(in vec4 col, in vec3 l, in vec3 n) 
{
  float intensity;
  vec4  color;
  
  intensity = dot(n,l);
  
  color = col;      
  
  if(intensity > 0.95)
    color = vec4(col.rgb + vec3(0.15), col.a);
  else if (intensity > 0.5)
    color = vec4(col.rgb + vec3(0.1), col.a);
  else if (intensity < 0.2)
    color = vec4(col.rgb - vec3(0.05), col.a);
  
  return color;
}

//----------------------------------------------------------------------------
// col = color
// l   = light vector
// n   = normal vector
// v   = eye vector
// v   = reflection vector
// a   = ambient  contribution [0,1]
// d   = diffuse  contribution [0,1]
// s.x = specular contribution [0,1]
// s.y = specular power
//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s) 
{
  vec4  color;  
  vec3  h = normalize(l+v);
  
  float ambientLight  = 0.3;  
  float diffuseLight  = diffuse(l, n);
  float specularLight = specular(h, n, s.y);  
    
  if(diffuseLight <= 0.0) specularLight = 0.0;
    
  vec3  ambient  = vec3(a)   * ambientLight;
  vec3  diffuse  = vec3(d)   * diffuseLight;
  vec3  specular = vec3(s.x) * specularLight;
  
  if(all(equal(n, vec3(0.0))))
    color = col;
  else  
    color = vec4(col.rgb + ambient + diffuse + specular, col.a);
    
  return color;
}

//----------------------------------------------------------------------------
// col = color
// l   = light vector
// n   = normal vector
// v   = eye vector
// v   = reflection vector
// a   = ambient  contribution [0,1]
// d   = diffuse  contribution [0,1]
// s.x = specular contribution [0,1]
// s.y = specular power
//----------------------------------------------------------------------------
vec4 PhongMultiplicative(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s) 
{
  vec4  color;  
  vec3  h = normalize(l+v);
  
  float ambientLight  = 0.3;  
  float diffuseLight  = diffuse(l, n);
  float specularLight = specular(h, n, s.y);  
    
  if(diffuseLight <= 0.0) specularLight = 0.0;
    
  vec3  ambient  = vec3(a)   * ambientLight;
  vec3  diffuse  = vec3(d)   * diffuseLight;
  vec3  specular = vec3(s.x) * specularLight;
  
  if(all(equal(n, vec3(0.0))))
    color = col;
  else  
    color = vec4(ambient + col.rgb*diffuse + specular, col.a);
    
  return color;
}

//----------------------------------------------------------------------------
// col = color
// l   = light vector
// n   = normal vector
// v   = eye vector
// v   = reflection vector
// a   = ambient  contribution [0,1]
// d   = diffuse  contribution [0,1]
// s.x = specular contribution [0,1]
// s.y = specular power
//----------------------------------------------------------------------------
vec4 PhongLine(in vec4 col, in vec3 l, in vec3 t, in vec3 v, in float a, in float d, in vec2 s) 
{
  vec4  color;
  
  float ambientLight  = 0.3;  
  float diffuseLight  = max(diffuseLine(l, t), 0.0);
  float specularLight = specularLine(v, l, t, s.y);
  
  if(diffuseLight <= 0.0) specularLight = 0.0;
  
  vec3  ambient  = vec3(a)   * ambientLight;
  vec3  diffuse  = vec3(d)   * diffuseLight;
  vec3  specular = vec3(s.x) * specularLight;
    
  color = vec4(col.rgb + (ambient + diffuse + specular), col.a);
  
  return color;
}

//----------------------------------------------------------------------------
// col = color
// l   = light vector
// n   = normal vector
// v   = eye vector
// s.x = specular factor 1
// s.y = specular factor 2
//----------------------------------------------------------------------------
vec4 Glass(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in vec2 s) 
{
  vec4  color;
  vec3  reflectDir;
  float spec;
  
  reflectDir = -reflect(l, n);
    
  spec = max(dot(v, reflectDir), 0.0);
  
  // Compute two rings of specular effect.
  spec = spec * spec;
 
  // Basic glass color + first specular effect
  color = col + s.x * spec * vec4(0.2, 0.2, 0.15, 0.3);

  // Second specular effect  
  spec   = pow(spec, 8.0) * s.y;  
  color += spec * vec4(0.15, 0.15, 0.1, 0.25);
    
  //return vec4(min(color.rgb, vec3(1.0)), col.a);
  return min(color, vec4(1.0));
}    

//----------------------------------------------------------------------------
// col = color
// l   = light vector
// p   = principal curvature
//----------------------------------------------------------------------------
vec4 Metal(in vec4 col, in vec3 l, in vec3 p) 
{
  vec4  color;
  
  float ldotp;
  int   index;
  
  ldotp = dot(normalize(l), normalize(p));
  index = int(floor(ldotp*20.0));  
  
  color = vec4(vec3(mix(float(metal[index]), float(metal[index+1]), abs(ldotp))), col.a);
  
  return color;
} 

//----------------------------------------------------------------------------
// col = color
// v   = eye vector
// n   = normal
//----------------------------------------------------------------------------
vec4 XRay(in vec4 col, in vec3 v, in vec3 n) 
{
  vec4 color;
  
  float edgeFallOff = 0.25; //lower is sharper/"less-clear" edge
  
  float opacity = dot(normalize(n), normalize(-v));
  opacity = abs(opacity);
  opacity = 1.0 - pow(opacity, edgeFallOff);
  
  //color = opacity * col;  
  
  //select white or black x-ray depending on the color of the mesh
  float gray_value = (col.r + col.g + col.b)/3.0;
    
  float out_color = (1.0-opacity)*(gray_value) + opacity*(1.0-gray_value);
  
  color.rgb = vec3(out_color, out_color, out_color);
  
  color.a = 1.0;//opacity;
    
  return color;
} 

//----------------------------------------------------------------------------
// col = color
// l   = light vector
// n   = normal vector
// v   = eye vector
//----------------------------------------------------------------------------
vec4 Gooch(in vec4 col, in vec3 l, in vec3 n, in vec3 v) 
{
  vec4 color;
  
  vec3 WarmColor = vec3(0.6, 0.6, 0.0);
  vec3 CoolColor = vec3(0.0, 0.0, 0.6);
  float DiffuseWarm = 0.45;
  float DiffuseCool = 0.45;
  
  float NdotL = dot(n, l);

  vec3 kcool    = min(CoolColor + DiffuseCool * col.rgb, 1.0);
  vec3 kwarm    = min(WarmColor + DiffuseWarm * col.rgb, 1.0); 
  vec3 kfinal   = mix(kcool, kwarm, NdotL);
  
  vec3 nreflect = normalize(reflect(-l, n));
  vec3 nview    = normalize(v);
  
  float spec    = max(dot(nreflect, nview), 0.0);
  spec          = pow(spec, 32.0);
  
  color = vec4(min(kfinal + spec, 1.0), 1.0);
    
  return color;
} 