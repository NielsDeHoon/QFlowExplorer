// vfr-func.glsl
//----------------------------------------------------------------------------
// Fetch Angle Derivative
vec3 FetchAngleDerivative(sampler3D vol, vec3 pos, vec3 direction, vec3 stepsize)
{                
  vec3  gradient;
  vec3  posTex[6]; 
  vec3  v[6];
  float a[6];
    
  posTex[0] = pos + vec3( stepsize.x, 0.0,  0.0);
  posTex[1] = pos + vec3(-stepsize.x, 0.0,  0.0);
  posTex[2] = pos + vec3( 0.0, stepsize.y,  0.0);
  posTex[3] = pos + vec3( 0.0,-stepsize.y,  0.0);
  posTex[4] = pos + vec3( 0.0,  0.0, stepsize.z);
  posTex[5] = pos + vec3( 0.0,  0.0,-stepsize.z);
    
  for(int i =0; i<6; i++)
  {
    v[i] = normalize(texture3D(vol, posTex[i]).rgb);
    a[i] = dot(v[i], direction);
  }
     
  gradient.x = 0.5 * a[0] - a[1];  
  gradient.y = 0.5 * a[2] - a[3];  
  gradient.z = 0.5 * a[4] - a[5];  
  
  return gradient;
}

//----------------------------------------------------------------------------
// Fetch curl
vec3 FetchCurl(sampler3D vol, vec3 pos, vec3 stepsize)
{
  vec3  Fdx, Fdy, Fdz;
  float F3dy, F2dz, F1dz, F3dx, F2dx, F1dy;
  
  Fdx =    texture3D(vol, pos + vec3(stepsize.x, 0.0, 0.0)).rgb -
           texture3D(vol, pos - vec3(stepsize.x, 0.0, 0.0)).rgb;  
  Fdy =    texture3D(vol, pos + vec3(0.0, stepsize.y, 0.0)).rgb -
           texture3D(vol, pos - vec3(0.0, stepsize.y, 0.0)).rgb;  
  Fdz =    texture3D(vol, pos + vec3(0.0, 0.0, stepsize.z)).rgb -
           texture3D(vol, pos - vec3(0.0, 0.0, stepsize.z)).rgb;
                
  F3dy  = Fdy.z /2.0;
  F2dz  = Fdz.y /2.0;
  F1dz  = Fdz.x /2.0;
  F3dx  = Fdx.z /2.0;
  F2dx  = Fdx.y /2.0;
  F1dy  = Fdy.x /2.0;
                 
  return vec3(F3dy-F2dz, F1dz-F3dx, F2dx-F1dy);
}

//----------------------------------------------------------------------------
// Fetch LPC (Local Phase Coherence)
float FetchLPC(sampler3D vol, vec3 pos, vec3 stepsize)
{
  vec3  center, step;
  float result = 0.0;
  
  center = normalize(texture3D(vol, pos).rgb);

  for(int u=-1; u<2; u++)
  {
    for(int v=-1; v<2; v++)
    {
      for(int w=-1; w<2; w++)
      {
        step = vec3(float(u)*stepsize.x, float(v)*stepsize.y, float(w)*stepsize.z);
        result = result + dot(center, normalize(texture3D(vol, pos + step).rgb));
      }
    }
  }
                 
  return (result/26.0);
}
