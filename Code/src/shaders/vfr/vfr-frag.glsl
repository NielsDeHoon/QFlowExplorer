// vfr-frag.glsl
uniform sampler3D vfrDataSetFlow;
uniform sampler3D vfrDataSetContext;
uniform vec2      vfrDataRange;
uniform int       vfrDataComponent;
uniform vec3      vfrDirection;
uniform int       vfrDataFocus;
uniform int       vfrDataContext;
uniform sampler1D transferFunction;
uniform sampler1D transferFunctionUS; // doppler color map
uniform sampler1D transferFunctionCW; // cool-warm color map
uniform sampler1D transferGradient;
uniform sampler2D rayEnd;
uniform sampler2D rayEndDepth;
uniform vec3      scaling;   // dimensions * voxel size (mm)
uniform float     stepSize;  // minimum of voxel size (mm)
uniform float     threshold; // velocity threshold (cm/s)
uniform int       mode; 
uniform float     quality;
uniform vec4      light;
uniform vec4      lightDirection;
uniform int       style;

uniform mat4      voxelPatientMatrix;

// This shader only deals with scalar data
// For vector data is will use the magnitudes (alpha component)
const   int       SCALARS  = 0;
const   int       VECTOR   = 1;

varying vec4 posTexture;
varying vec4 posScreen;

//----------------------------------------------------------------------------
vec3 FetchVectorGradient(sampler3D vol, vec3 pos, vec3 step);

//----------------------------------------------------------------------------
vec3 NormalizeRange(vec3 value, vec2 range);

//----------------------------------------------------------------------------
vec4 CompositeScalarFrontToBack(vec4 src, vec4 dst);

//----------------------------------------------------------------------------
// Fetch Angle Derivative
vec3 FetchAngleDerivative(sampler3D vol, vec3 pos, vec3 direction, vec3 stepsize);

//----------------------------------------------------------------------------
// Fetch curl
vec3 FetchCurl(sampler3D vol, vec3 pos, vec3 stepsize);

//----------------------------------------------------------------------------
// Fetch LPC (Local Phase Coherence)
float FetchLPC(sampler3D vol, vec3 pos, vec3 stepsize);

//----------------------------------------------------------------------------
void main()
{
  vec4  dataSample;
  float dataValue; 
  vec4  dataVelocity;
  float dataSpeed;
  vec3  dataGradient;
  vec2  dataRange;  
  float normalizedValue;
  vec3  classifiedValue;
  float stepSizeLocal;  
  
  vec3  ray;  
  vec3  rayVector;
  vec3  rayDir;
  float rayMaxLength; 
  float rayOffset;
  
  vec3  lightDir;
  
  vec4  src;
  vec4  dst;
  float angle;
  
  vec4  color;  
  
  // Initialize gradient
  dataGradient = vec3(0.0);
  
  // Initialize src and dst
  src = vec4(0.0,0.0,0.0,0.0);
  dst = vec4(0.0,0.0,0.0,0.0);
  
  stepSizeLocal = stepSize / quality;  
  
  // Initialize the data range
  dataRange.xy = vfrDataRange.xy;
  if(mode == VECTOR && vfrDataComponent == 0) 
    dataRange = vec2(0.0, max(abs(vfrDataRange.x),abs(vfrDataRange.y))); 
  
  // Compute normalized device coordinate
  vec2 texCoord =  ((posScreen.xy / posScreen.w) + 1.0) / 2.0;
  
  //--------------------------
  // Ray Setup
  //--------------------------
  // Determine start and end of rays  
  vec3 start      = posTexture.rgb;
  vec3 end        = texture2D(rayEnd, texCoord.xy).rgb;
  
  //if(any(lessThan(   start.xyz, vec3(0.0))) || 
  //   any(greaterThan(start.xyz, vec3(1.0))))
  //  discard; 
    
  //start           = clamp(start, 0.0, 1.0) * scaling;
  //end             = clamp(end,   0.0, 1.0) * scaling;
  start           = start * scaling;
  end             = end   * scaling;
    
  ray          = start;    
  rayVector    = end - start;
  rayDir       = normalize(rayVector);
  rayMaxLength = length(rayVector);  
  rayOffset    = 0.0;    
  
  // If there is no ray, discard the fragment
  if(rayMaxLength <= 0.01) discard;
    
  //--------------------------
  // Ray Propagation
  //--------------------------  
  int i;
  for(i=0; i <= int(rayMaxLength/stepSizeLocal); i++)
  {     
  
    // Early ray termination
    // Determine if ray is within bounding box 
    // Determine if ray is fully opaque
    if((rayOffset >= rayMaxLength) || (dst.a == 1.0))
    {
      break;
    }        
    
    // Outside of bounding box
    if( any(lessThan(ray.xyz, vec3(0.0))) || any(greaterThan(ray.xyz, vec3(1.0*scaling))) )      
    {
      ray        = ray      + rayDir * stepSizeLocal;
      rayOffset += stepSizeLocal;
          
      continue;
    }
    
    //--------------------------
    // Sample velocities
    //--------------------------
    dataVelocity = texture3D(vfrDataSetFlow, ray/scaling);  
    dataSpeed    = length(dataVelocity.rgb);   
      
    //--------------------------
    // Sampling & Classification
    //--------------------------       
    // Post-interpolative transfer function
    
    // Focus
    if(threshold <= dataSpeed)
    {    
      angle           = dot(normalize(dataVelocity.rgb),normalize(vfrDirection.rgb));      
      normalizedValue = 0.5;
      
      
      if(vfrDataFocus == 0) // doppler
      {
        dataValue       = dataSpeed*sign(angle);  
        normalizedValue = NormalizeRange(vec3(dataValue), vfrDataRange.xy).r;  
        src             = texture1D(transferFunctionUS, normalizedValue);         
      }      
      if(vfrDataFocus == 1) // angle
      {
        dataValue       = angle;  
        normalizedValue = NormalizeRange(vec3(dataValue/2.0), vec2(-1.0,1.0)).r;        
        src             = texture1D(transferFunctionUS, normalizedValue); 
      }      
      if(vfrDataFocus == 2) // angle derivative
      {
        dataValue       = length(FetchAngleDerivative(vfrDataSetFlow, ray/scaling, normalize(vfrDirection.rgb), vec3(1.0)/scaling));
        dataValue       = dataValue*sign(angle);
        normalizedValue = NormalizeRange(vec3(dataValue/2.0), vec2(-1.0,1.0)).r;        
        src             = texture1D(transferFunctionUS, normalizedValue); 
      }
      if(vfrDataFocus == 3) // coherence
      {
        dataValue       = length(FetchLPC(vfrDataSetFlow, ray/scaling, vec3(1.0)/scaling));
        dataValue       = (1.0-dataValue)*sign(angle);
        normalizedValue = NormalizeRange(vec3(dataValue), vec2(-1.0,1.0)).r;        
        src             = texture1D(transferFunctionUS, normalizedValue); 
      } 
      if(vfrDataFocus == 4) // vorticity
      {
        dataValue       = length(FetchCurl(vfrDataSetFlow, ray/scaling, vec3(1.0)/scaling));
        dataValue       = dataValue*sign(angle);
        normalizedValue = NormalizeRange(vec3(dataValue), vfrDataRange.xy).r;        
        src             = texture1D(transferFunctionUS, normalizedValue); 
      }    
      
      src.a             = texture1D(transferGradient, normalizedValue).a;
       
    }
    // Context
    else
    {
      dataSample      = texture3D(vfrDataSetContext, ray/scaling);     
      dataValue       = length(dataSample.rgb);  
      normalizedValue = NormalizeRange(vec3(dataValue), dataRange).r;    
      
      // Post-interpolative transfer function
      src   = texture1D(transferFunction, normalizedValue);             
      src.a = 0.0;
    }   
         
    //--------------------------
    // Compositing
    //--------------------------
    // Opacity correction           
    src.a   = 1.0 - pow((1.0 - src.a), (stepSizeLocal/stepSize));
    
    // Front-to-back compositing
    dst = CompositeScalarFrontToBack(src,dst);    
    
    ray        = ray      + rayDir * stepSizeLocal;
    rayOffset += stepSizeLocal;
  }  
    
  // Determine the final color
  color = dst;   
   
  gl_FragColor = color;     

}
