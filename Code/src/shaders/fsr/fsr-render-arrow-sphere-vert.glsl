#version 330

in vec3 qfe_ParticlePosition;
in vec3 qfe_ParticleVelocity1;
in vec3 qfe_ParticleVelocity2;

out particleAttrib
{
  vec3 position;  
  vec3 velocity1;
  vec3 velocity2;
} particle;

//----------------------------------------------------------------------------
void main(void)
{
   particle.position  = qfe_ParticlePosition;     
   particle.velocity1 = qfe_ParticleVelocity1;
   particle.velocity2 = qfe_ParticleVelocity2;
}
