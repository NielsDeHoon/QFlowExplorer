#version 330

uniform sampler1D lineTransferFunction;

uniform vec4      light;
uniform vec4      lightDirection;
uniform int       colorType;
uniform int       highlight;
uniform mat4      matrixModelView;
uniform mat4      matrixModelViewInverse;

in arrowAttrib
{
  vec3  normal;  
  vec4  attribs;
  vec4  color;
  float height;  
  float total_height;
  float curvature;
  vec3  curve_direction;
  float radius;
} arrow;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 Desaturate(const vec4 color, const float desaturation)
{
  //const vec3 gray_conv = vec3(0.30, 0.59, 0.11);  
  const vec3 gray_conv = vec3(0.30, 0.3, 0.3);  
  vec3 gray = vec3(dot(gray_conv, color.rgb));
  
  return vec4(vec3(mix(color.rgb, gray, desaturation)),1.0);
}

//----------------------------------------------------------------------------
void main()
{ 
  const float delta = 0.05;
  
  vec4  color, shaded;
  float depth, yCoordNorm1, yCoordNorm2;    
  float speed;
  
  // determine if completely transparent
  if(arrow.color.a <= delta) discard;
  
  // initialize vectors  
  vec3 n = normalize(arrow.normal);
  vec3 l = normalize(-lightDirection.xyz);
  vec3 e = vec3(0.0,0.0,1.0);
  
  // set color
  color = vec4(0.0,0.25,0.6,1.0);  
  
  // check if we need to hightlight this pathline
  if(highlight == 1)
      color = vec4(0.9,0.45,0.0,1.0);
    //color = vec4(0.2,0.2,0.2,1.0);  
		  
  // desaturate
  color =  Desaturate(color, 0.6);

  // apply shading (we bump the color a bit to highlight the arrow heads)  
  shaded = Phong(color, l, n, e, light.x, light.y, vec2(light.z,light.w));  
  shaded.a = arrow.color.a;    
  
  qfe_FragColor = shaded;  
    
  float kv = arrow.curvature / arrow.height;
  
  kv = abs(dot(e,arrow.curve_direction) ) * kv;
   
  float T = 0.50; //contour thickness
  
  if(kv>1/T)
	kv = 1/T;
  
  float threshold = sqrt(T*kv*(2.0-T*kv));
  
  if(abs(dot(e,n)) <= threshold)
	qfe_FragColor.xyz = vec3(0);

  if(arrow.height >= arrow.total_height-T/4 || arrow.height <= T/2)
  {
	qfe_FragColor.xyz = vec3(0);
  }
}