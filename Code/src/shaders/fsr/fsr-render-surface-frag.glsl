#version 330

uniform sampler1D lineTransferFunction;
uniform float     lineWidth;

uniform int       colorType;
uniform vec4      light;
uniform vec4      lightDirection;

uniform mat4      matrixProjection;

in imposterAttrib
{  
  vec3  normal;
  vec2  texcoord; // (x = arclength, y = left-right)
  vec4  attribs;  
} imposter;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
void main()
{ 
  vec3  eye, lightDir, normal;
  vec4  color, shaded;  
  float yw, zw, ndotv;
      
  yw          = (imposter.texcoord.y*2.0)-1.0;  
  zw          = 1.0 - abs(yw);
  
  // determine the tapering
  eye         = vec3(0.0,0.0,1.0);   
  lightDir    = normalize(-lightDirection.xyz);  
  normal      = normalize(imposter.normal);
  ndotv       = dot(normal.xyz, eye.xyz);  
       
  
  // fetch the color
  color  = vec4(1.0,0.0,0.0,1.0);

  if(colorType == 0) // speed
    color  = texture(lineTransferFunction, imposter.attribs.x);  

  if(colorType == 1) // distance local
    color  = texture(lineTransferFunction, imposter.attribs.y);  

  if(colorType == 2) // distance mean
    color  = texture(lineTransferFunction, imposter.attribs.z);  

  if(colorType == 3) // distance Hausdorff
    color  = texture(lineTransferFunction, imposter.attribs.w);  

  // appy the shading 
  //shaded = Phong(color, lightDir.xyz, normal.xyz, eye.xyz, light.x, light.y, vec2(light.z,light.w));  
       
  //qfe_FragColor = vec4(color.rgb,1.0);
  //qfe_FragColor = vec4(imposter.normal,1.0);
  
  qfe_FragColor = vec4(color.rgb,1.0);  
  
}