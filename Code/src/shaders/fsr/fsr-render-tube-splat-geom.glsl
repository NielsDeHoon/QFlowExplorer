#version 330

uniform float lineWidth;
uniform float distanceFilter;
uniform mat4  matrixModelView;
uniform mat4  matrixProjection;
uniform int   colorType;

layout(lines_adjacency )                in;
layout(triangle_strip, max_vertices=12) out;

in seedAttrib
{
  vec4 position; // in patient coordinates (x,y,z,t)
  vec4 attribs;
} seed[];

out imposterAttrib
{ 
  vec4  position;
  vec3  binormal;
  vec2  texcoord; // (x = arclength, y = left-right)
  vec4  attribs;
} imposter;

//----------------------------------------------------------------------------
void main()
{    
  vec4  lineEye[4], strip[4];
  vec3  lineTangent, lineTangentAdj[2];
  vec3  lineNormalAdj[2];  
  vec3  eye;
  bool  passFilter;
  
  const float delta = 0.0001;
      
  // initialize
  eye                  = vec3(0.0,0.0,1.0);      
  imposter.binormal    = vec3(0.0);  
  passFilter           = true;
  
  if(colorType == 3) // Hausdorff  
    passFilter = (seed[1].attribs.w > (1.0 - distanceFilter));  
  else
    passFilter = (seed[1].attribs.z > (1.0 - distanceFilter));
  
  if(passFilter)
  {
     // -----------------------------
     // compute the strip
     // -----------------------------

    // we assume adjacency information, hence we have 4 vertices  
    // we calculate the imposters in screen space  
    lineEye[0] = matrixModelView * vec4(seed[0].position.xyz,1.0);
    lineEye[1] = matrixModelView * vec4(seed[1].position.xyz,1.0);
    lineEye[2] = matrixModelView * vec4(seed[2].position.xyz,1.0);
    lineEye[3] = matrixModelView * vec4(seed[3].position.xyz,1.0);

    // compute tangent          
    lineTangentAdj[0] = normalize(lineEye[1].xyz - lineEye[0].xyz); 
    lineTangentAdj[1] = normalize(lineEye[2].xyz - lineEye[1].xyz); 

    // check start of the line
    if(length(lineEye[1].xyz - lineEye[0].xyz) < delta)
      lineTangentAdj[0] = lineTangentAdj[1];

    // compute normal  
    lineNormalAdj[0]  = normalize(cross(eye, lineTangentAdj[0]));
    lineNormalAdj[1]  = normalize(cross(eye, lineTangentAdj[1]));

    strip[0] = vec4(lineEye[1].xyz - 0.5 *lineWidth*lineNormalAdj[0], 1.0);
    strip[1] = vec4(lineEye[1].xyz + 0.5 *lineWidth*lineNormalAdj[0], 1.0);
    strip[2] = vec4(lineEye[2].xyz - 0.5 *lineWidth*lineNormalAdj[1], 1.0);
    strip[3] = vec4(lineEye[2].xyz + 0.5 *lineWidth*lineNormalAdj[1], 1.0);

    // -----------------------------
    // render the strip
    // -----------------------------

    imposter.position    = strip[0];
    imposter.binormal    = lineNormalAdj[0];
    imposter.texcoord    = vec2(0.0,0.0);      
    imposter.attribs     = seed[1].attribs;
    gl_Position          = matrixProjection * strip[0];     
    EmitVertex();

    imposter.position    = strip[1];
    imposter.binormal    = lineNormalAdj[0];
    imposter.texcoord    = vec2(0.0,1.0);
    imposter.attribs     = seed[1].attribs;
    gl_Position          = matrixProjection * strip[1];     
    EmitVertex();

    imposter.position    = strip[2];
    imposter.binormal    = lineNormalAdj[1];
    imposter.texcoord    = vec2(0.0,0.0);  
    imposter.attribs     = seed[2].attribs;
    gl_Position          = matrixProjection * strip[2];     
    EmitVertex();

    imposter.position    = strip[3];
    imposter.binormal    = lineNormalAdj[1];
    imposter.texcoord    = vec2(0.0,1.0);  
    imposter.attribs     = seed[2].attribs;
    gl_Position          = matrixProjection * strip[3];     
    EmitVertex();  

    EndPrimitive();
  }
}