#version 330

in vec4 qfe_ParticlePosition;
in vec3 qfe_ParticleVelocity;

out particleAttrib
{
  vec4 position;
  vec3 velocity;
  
} particle;

//----------------------------------------------------------------------------
void main(void)
{
   particle.position  = qfe_ParticlePosition;
   particle.velocity  = qfe_ParticleVelocity;  
}
