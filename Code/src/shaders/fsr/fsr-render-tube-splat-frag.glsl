#version 330

uniform sampler1D lineTransferFunction;
uniform float     lineWidth;

uniform int       colorType;
uniform int       highlight;
uniform vec4      light;
uniform vec4      lightDirection;

uniform mat4      matrixProjection;

in imposterAttrib
{
  vec4  position;
  vec3  binormal;
  vec2  texcoord; // (x = arclength, y = left-right)
  vec4  attribs;
} imposter;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Cel(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 Desaturate(const vec4 color, const float desaturation)
{
  //const vec3 gray_conv = vec3(0.30, 0.59, 0.11);  
  const vec3 gray_conv = vec3(0.30, 0.3, 0.3);  
  vec3 gray = vec3(dot(gray_conv, color.rgb));
  
  return vec4(vec3(mix(color.rgb, gray, desaturation)),1.0);
}

//----------------------------------------------------------------------------
void main()
{ 
  vec3  eye, lightDir, normal;
  vec4  color, shaded;  
  float yw, zw, ndotv;
  
  yw          = (imposter.texcoord.y*2.0)-1.0;  
  zw          = 1.0 - abs(yw);
  
  // determine the tapering
  eye         = vec3(0.0,0.0,1.0);   
  lightDir    = normalize(-lightDirection.xyz);  
  normal      = normalize(vec3(imposter.binormal.xy * yw, zw));
  ndotv       = dot(normal.xyz, eye.xyz);  
        
  color = vec4(0.0,0.25,0.6,1.0);  
  
  // check if we need to hightlight this pathline
  if(highlight == 1)
      color = vec4(0.9,0.45,0.0,1.0);
    //color = vec4(0.2,0.2,0.2,1.0);  
		  
  // desaturate
  color =  Desaturate(color, 0.6);

  // appy the shading 
  shaded = Phong(color, lightDir.xyz, normal.xyz, eye.xyz, light.x, light.y, vec2(light.z,light.w));
  //shaded   = Cel(color, lightDir.xyz, normal.xyz);  
  
  // adjust the depth
  vec4 screenpos  = vec4(imposter.position.xyz,1.0);
  screenpos.z    += normal.z*0.5*lineWidth;  
  screenpos       = matrixProjection * screenpos;

  float far       = gl_DepthRange.far; 
  float near      = gl_DepthRange.near;  
  float ndc_depth = screenpos.z / screenpos.w;  
  float depth     = (((far-near) * ndc_depth) + near + far) / 2.0;
    
  gl_FragDepth    = depth; 
  
  float contour = 0.20;
  float dist = imposter.texcoord.y;
  if(dist>1.0-contour)
	dist = 1.0-dist;
	    
  if(dist < contour)
  {
    shaded.xyz = vec3(0.0, 0.0, 0.0);
  }
		
  qfe_FragColor = shaded;
}