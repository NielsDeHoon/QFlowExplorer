#version 330

uniform mat4  matrixModelView;
uniform mat4  matrixModelViewProjection;
uniform mat4  matrixModelViewProjectionInverse;
uniform int   angleFilter;

uniform float particleSize;

layout(points )                        in;
layout(triangle_strip, max_vertices=4) out;

in particleAttrib
{
  vec3 position;  
  vec3 velocity1;
  vec3 velocity2;
} particle[];

out imposterAttrib
{ 
  vec4 position;
  vec4 texcoord;    
} imposter;

//----------------------------------------------------------------------------
void main(void)
{ 
  bool  passFilter;
  float angle;
  vec2  radius;
  vec4  x, y;
  vec4  p[4];    
  
  angle       = dot(normalize(particle[0].velocity1),normalize(particle[0].velocity2));
  passFilter  = (2.0-(1.0-angle)) <= (float(angleFilter)/90.0);  
   
  if(passFilter)
  {  
    radius   = vec2(particleSize*0.5); 

    x        = radius.x * normalize(matrixModelViewProjectionInverse*vec4(1.0,0.0,0.0,0.0)); 
    y        = radius.y * normalize(matrixModelViewProjectionInverse*vec4(0.0,1.0,0.0,0.0));     

    p[0] = vec4(particle[0].position.xyz,1.0) - x - y;
    p[1] = vec4(particle[0].position.xyz,1.0) - x + y;
    p[2] = vec4(particle[0].position.xyz,1.0) + x - y;
    p[3] = vec4(particle[0].position.xyz,1.0) + x + y;

    imposter.position     = matrixModelView * vec4(particle[0].position.xyz,1.0);      

    imposter.texcoord     = vec4(-1.0,-1.0,0.0,1.0);
    gl_Position           = matrixModelViewProjection * p[0];
    EmitVertex();

    imposter.texcoord     = vec4(-1.0,1.0,0.0,1.0);
    gl_Position           = matrixModelViewProjection * p[1];
    EmitVertex();

    imposter.texcoord     = vec4(1.0,-1.0,0.0,1.0);
    gl_Position           = matrixModelViewProjection * p[2];
    EmitVertex();

    imposter.texcoord     = vec4(1.0,1.0,0.0,1.0);
    gl_Position           = matrixModelViewProjection * p[3];
    EmitVertex();   

    EndPrimitive();
  }
}
