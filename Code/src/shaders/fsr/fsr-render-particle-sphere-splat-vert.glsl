#version 330

in vec4 qfe_ParticlePosition;
in vec3 qfe_ParticleAttribs;

out particleAttrib
{
  vec4 position;  
  vec3 attribs;  
} particle;

//----------------------------------------------------------------------------
void main(void)
{
   particle.position  = qfe_ParticlePosition;   
   particle.attribs   = qfe_ParticleAttribs;
}
