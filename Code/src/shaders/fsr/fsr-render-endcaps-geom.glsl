#version 330

uniform sampler1D lineTransferFunction;

uniform float lineWidth;
uniform float distanceFilter;
uniform mat4  matrixModelView;
uniform mat4  matrixModelViewProjection;
uniform mat4  matrixModelViewInverse;

layout(lines_adjacency )                in;
layout(triangle_strip, max_vertices=4) out;

in seedAttrib
{
  vec4 position; // in patient coordinates (x,y,z,t)
  vec4 attribs;  // x = speed, y = vertex id, z = distance local, w = distance global
} seed[];

out capAttrib
{  
  vec4  position;
  vec3  normal;
  vec2  texcoord; // (x = arclength, y = left-right)    
  vec4  attribs;
} cap;


//----------------------------------------------------------------------------
void main()
{   
  const float delta = 0.001;

  vec3  eye, eyeWorld;
  vec4  posScreen;
  vec4  strip[4];
  vec3  lineTangent;
  vec3  capStart, capNormal, capX, capY;  
  mat4  matrixNormal;
  
  float speed;  
    
  if(seed[1].attribs.z > (1.0 - distanceFilter))
  {
    
    // initialize
    eye             = vec3(0.0,0.0,1.0);        
    eyeWorld        = normalize( matrixModelViewInverse*vec4(eye,0.0)).xyz;     
    matrixNormal    = inverse(matrixModelViewInverse);  
    
    bool isStartCap = length(seed[1].position.xyz - seed[0].position.xyz) < delta;
    bool isEndCap   = length(seed[3].position.xyz - seed[2].position.xyz) < delta;

    // Start cap
    if(isStartCap || isEndCap)
    {     
      // compute geometry    
      if(isStartCap)    
      {
        lineTangent        = normalize(seed[2].position.xyz - seed[1].position.xyz);           
        capStart           = seed[1].position.xyz;
        capNormal          = lineTangent.xyz;
      }
      else    
      {
        lineTangent        = normalize(seed[2].position.xyz - seed[1].position.xyz);            
        capStart           = seed[2].position.xyz;
        capNormal          = lineTangent.xyz;
      }    

      // build the basis    
      capX               = normalize(cross(eyeWorld, capNormal));
      capY               = cross(capNormal, capX);

      // create the strip
      strip[0] = vec4(capStart - 0.5*lineWidth*capX - 0.5*lineWidth*capY, 1.0);
      strip[1] = vec4(capStart - 0.5*lineWidth*capX + 0.5*lineWidth*capY, 1.0);
      strip[2] = vec4(capStart + 0.5*lineWidth*capX - 0.5*lineWidth*capY, 1.0);
      strip[3] = vec4(capStart + 0.5*lineWidth*capX + 0.5*lineWidth*capY, 1.0);

      // render the cap  
      cap.position      = matrixModelView * strip[0];      
      cap.normal        = (matrixNormal * vec4(capNormal.xyz,0.0)).xyz;
      cap.texcoord      = vec2(0.0,0.0);            
      cap.attribs       = seed[1].attribs;
      gl_Position       = matrixModelViewProjection * strip[0];     
      EmitVertex();

      cap.position      = matrixModelView * strip[1];
      cap.normal        = (matrixNormal * vec4(capNormal.xyz,0.0)).xyz;
      cap.texcoord      = vec2(0.0,1.0);  
      cap.attribs       = seed[1].attribs;
      gl_Position       = matrixModelViewProjection * strip[1];     
      EmitVertex();

      cap.position      = matrixModelView * strip[2];
      cap.normal        = (matrixNormal * vec4(capNormal.xyz,0.0)).xyz;
      cap.texcoord      = vec2(1.0,0.0);        
      cap.attribs       = seed[2].attribs;
      gl_Position       = matrixModelViewProjection * strip[2];     
      EmitVertex();

      cap.position      = matrixModelView * strip[3];
      cap.normal        = (matrixNormal * vec4(capNormal.xyz,0.0)).xyz;
      cap.texcoord      = vec2(1.0,1.0);        
      cap.attribs       = seed[2].attribs;
      gl_Position       = matrixModelViewProjection * strip[3];     
      EmitVertex();    
    }
  }    
}