#version 330

uniform sampler1D particleTransferFunction;
uniform mat4      matrixModelView;
uniform mat4      matrixModelViewProjection;
uniform vec4      light;
uniform vec4      lightDirection;
uniform int       particleShading;
uniform int       particleContour;
uniform float     particleSize;

in imposterAttrib
{ 
  vec3 parametercoord;
  vec3 parameternormal;  
  mat4 quadric;
  mat4 transform;
  vec3 velocity;  
} imposter;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 PhongMultiplicative(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 Cel(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec2 GetRayQuadricIntersection(in vec3 rayOrigin, in vec3 rayDir, in mat4 quadric)
{
  // coeffs structure
  // | a d e g |
  // | d b f h |
  // | e f c i |
  // | g h i j |
  // ax^2 + by^2 + cz^2 + 2dxy + 2exz + 2fyz + 2gx + 2hy + 2iz + j = 0
  
  float a, b, c;
  float d, t1, t2;
  
  a   = b = c = 0.0;  

  a = dot(quadric * vec4(rayDir,0.0)   , vec4(rayDir,0.0));
  b = dot(quadric * vec4(rayOrigin,1.0), vec4(rayDir,0.0)) * 2.0;
  c = dot(quadric * vec4(rayOrigin,1.0), vec4(rayOrigin,1.0));
              
  d  = b*b - 4.0*a*c;
  
  if(d < 0.0)
  {    
    return vec2(0.0);
  }
  
  d   = sqrt(d);    
  t1  = (-b-d)/(2.0*a);
  t2  = (-b+d)/(2.0*a);  
  
  return vec2(t1,t2);
}

//----------------------------------------------------------------------------
void main()
{ 
  bool  hit;
  vec3  color, lightDir;  
  vec3  rayOrigin, rayDirection;    
  vec2  intersect;
  vec3  parameterpos;
  vec4  screenpos;
  vec4  eye, eyenormal;
  float ndote, speed;
  vec4  colorfrag;     
  
  // perform the raycasting
  rayOrigin    = imposter.parametercoord;  
  rayDirection = imposter.parameternormal;
    
  intersect    = GetRayQuadricIntersection(rayOrigin, rayDirection, imposter.quadric);
  
  // discard positions without intersection
  if(intersect.x <= 0) discard;
  
  // computer the intersection point
  parameterpos = rayOrigin + intersect.x * rayDirection;  
  screenpos    = matrixModelViewProjection * imposter.transform * vec4(parameterpos,1.0); 
    
  // shading characteristics  
  eyenormal    = transpose(inverse(matrixModelView * imposter.transform)) * normalize(vec4(parameterpos,0.0));
  eye          = vec4(0.0,0.0,1.0,0.0);
  ndote        = dot(eyenormal,eye); 
  lightDir     = normalize(-lightDirection).xyz;    
  
  // set the color    
  speed   = length(imposter.velocity.xyz);  
  color   = texture(particleTransferFunction, speed).rgb;   
  
  // apply shading
  if(particleShading == 1)      
    colorfrag = Phong(vec4(color,1.0), lightDir, normalize(eyenormal.xyz), eye.xyz, light.x, light.y, vec2(light.z, light.w));    
  else
    colorfrag = Cel(vec4(color,1.0), lightDir, normalize(eyenormal.xyz));  
    
  // apply a contour
  if(particleContour == 1)
    if(ndote < 0.4) 
      colorfrag = vec4(0.2,0.2,0.2,1.0);  
  
  qfe_FragColor = vec4(colorfrag.rgb,1.0);

  // adjust the depth
  float far       = gl_DepthRange.far; 
  float near      = gl_DepthRange.near;  
  float ndc_depth = screenpos.z / screenpos.w;  
  float depth     = (((far-near) * ndc_depth) + near + far) / 2.0;
  
  gl_FragDepth    = depth;
}