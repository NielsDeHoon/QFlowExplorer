#version 130
#extension GL_EXT_gpu_shader4 : enable
#extension GL_EXT_geometry_shader4 : enable

uniform sampler3D volumeData0;     //-2
uniform sampler3D volumeData1;     //-1
uniform sampler3D volumeData2;     // 0
uniform sampler3D volumeData3;     // 1
uniform sampler3D volumeData4;     // 2
uniform sampler3D volumeClusters0; //-2
uniform sampler3D volumeClusters1; //-1
uniform sampler3D volumeClusters2; // 0
uniform sampler3D volumeClusters3; // 1
uniform sampler3D volumeClusters4; // 2
uniform ivec3     volumeDimensions;

uniform float     phaseCurrent;

uniform mat4      voxelPatientMatrix;
uniform mat4      patientVoxelMatrix;

uniform int       traceSteps;         // steps count per trace
uniform float     traceStepSize;      // step size in phases
uniform float     traceStepDuration;  // duration of a full phase in ms
uniform int       traceScheme;        // 0 RK1 / 1 RK2 / 2 RK4
uniform float     maxAngle;

in  vec4  seedPosition[1];

out vec4  qfe_LinePoints;
out vec4  qfe_LineAttribs;  // x = velocity, y = inside cluster (bool), z = trace length (mm), w = start / end position
                            // start and end position are encoded as: start 1, 2, 3 and end 4, 5, 6 
                            // 2,5 and 3, 4 are without and with cluster masking respectively 
                            // 1 and 6 always reside withing the cluster

sampler3D[5] series;
sampler3D[5] clusters;

//----------------------------------------------------------------------------
vec3 FetchVelocityLinearTemporalLinear(sampler3D[5] series, vec3 pos, float posTime, float seedTime, mat4 v2t);

//----------------------------------------------------------------------------
vec3 TransformPosition(vec3 pos, mat4 matrix);

//----------------------------------------------------------------------------
vec4 IntegrateRungeKutta1(sampler3D[5] series, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t);
vec4 IntegrateRungeKutta2(sampler3D[5] series, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t);
vec4 IntegrateRungeKutta4(sampler3D[5] series, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t);

//----------------------------------------------------------------------------
float FetchClusterLabelTemporalNearest(sampler3D[5] clusters, vec3 posTex, float posTime, float currentTime)
{
  float label = 0.0;

  // The uniform samplers proved a range of volumes
  // We need to compute the phase for the data to index the samples
  int phaseData = (int(posTime)-int(currentTime)+2);
  
  if(phaseData == 0) label = texture(clusters[0], posTex).r;  
  if(phaseData == 1) label = texture(clusters[1], posTex).r;  
  if(phaseData == 2) label = texture(clusters[2], posTex).r;
  if(phaseData == 3) label = texture(clusters[3], posTex).r;
  if(phaseData == 4) label = texture(clusters[4], posTex).r;  

  return label;

}

//----------------------------------------------------------------------------
void main() {

  vec4  seedPatient;   
  vec4  posTexture;
  vec4  pos;  
  int   steps;
  vec3  velocity;
  float clusterLabel, currentLabel;  
  
  bool        insideFlag    = true;  
  float       insideCluster = 1.0;
  float       traceLength   = 0.0;
  const float delta         = 0.0005;
  const int   traceSamples  = 29;
  const int   centerSample  = (traceSamples-1)/2;
  vec4        vertexPositions[traceSamples];
  vec4        vertexAttribs[traceSamples];  
  
  qfe_LinePoints  = vec4(0.0);
  qfe_LineAttribs = vec4(0.0);
    
  // initalize the array & clusters
  series[0]   = volumeData0;
  series[1]   = volumeData1;
  series[2]   = volumeData2;
  series[3]   = volumeData3;
  series[4]   = volumeData4;
  
  clusters[0] = volumeClusters0;
  clusters[1] = volumeClusters1;
  clusters[2] = volumeClusters2;
  clusters[3] = volumeClusters3;
  clusters[4] = volumeClusters4;
  
  // get the seed position  
  seedPatient   = seedPosition[0];  
  
  // trace the pathline, if the seed phase corresponds to the current phase
  if(int(seedPatient.w) == int(phaseCurrent))
  {   
    // initialize transformations
    mat4 p2v; // patient to voxel   coordinates
    mat4 v2t; // voxel   to texture coordinates

    p2v = patientVoxelMatrix;
    v2t = mat4(1.0/volumeDimensions[0], 0.0, 0.0,0.0,
               0.0, 1.0/volumeDimensions[1], 0.0, 0.0,
               0.0, 0.0, 1.0/volumeDimensions[2], 0.0,
               0.0, 0.0, 0.0, 1.0);             

    // pos is the intermediate advected position 
    // pos.xyz - patient coordinates
    // pos.w   - current seed time in phases
    pos.xyz      = seedPatient.xyz;
    pos.w        = seedPatient.w;

    velocity     = FetchVelocityLinearTemporalLinear(series, TransformPosition(pos.xyz, p2v), pos.w, phaseCurrent, v2t);

    posTexture   = vec4(TransformPosition(pos.xyz, p2v) / vec3(volumeDimensions), 1.0);
    clusterLabel = texture(clusters[2], posTexture.xyz).r;

    // perform the trace integration   (for n samples, we need n-1 steps)
    steps = int(max(traceSamples-3, traceSteps)/float(traceSamples-3));    

    // initialize varying variables
    qfe_LinePoints   = pos;  
    qfe_LineAttribs  = vec4(0.0, 0.0, 0.0, 0.0);

    // -- Center position
    vertexPositions[centerSample] = pos;
    vertexAttribs[centerSample]   = vec4(length(velocity),1.0,0.0,0.0);

    // -- Trace backward  
    insideFlag    = true;
    insideCluster = 1.0;
    for(int i=1; i<(traceSamples-1)/2; i++)
    {           
      vec4  previousPos    = pos; 
      float previousInside = insideCluster;    

      for(int j=0; j<steps; j++)
      {
        vec4 posTmp = pos;
      
        if(traceScheme == 0)
          pos = IntegrateRungeKutta1(series, pos, phaseCurrent, traceStepSize, traceStepDuration, -1, 100, p2v, v2t);    
        else
        if(traceScheme == 1)
          pos = IntegrateRungeKutta2(series, pos, phaseCurrent, traceStepSize, traceStepDuration, -1, 100, p2v, v2t);    
        else
          pos = IntegrateRungeKutta4(series, pos, phaseCurrent, traceStepSize, traceStepDuration, -1, 100, p2v, v2t);        
          
        // accumulate the trace length
        traceLength = traceLength + length(pos-posTmp);
      }    

      // -- Check conditions
      posTexture   = vec4(TransformPosition(pos.xyz, p2v) / vec3(volumeDimensions), 1.0);

      // check the bounding box condition    
      if( any(lessThanEqual(posTexture.xyz,vec3(0.0))) || any(greaterThanEqual(posTexture.xyz,vec3(1.0))) )    
        pos.xyz = previousPos.xyz;   

      // check cluster bounding condition   
      insideCluster = 0.0;
      currentLabel = FetchClusterLabelTemporalNearest(clusters, posTexture.xyz, pos.w, phaseCurrent);
      if((insideFlag == true) && (currentLabel > clusterLabel-delta) && (currentLabel < clusterLabel+delta))         
        insideCluster = 1.0;   
      else
      {
        insideFlag    = false;      
        vertexAttribs[centerSample-i+1].y = 0.0;
        //vertexAttribs[centerSample-i+2].y = 0.0;
      }
      
      // determine start and end points
      if(previousInside - insideCluster == 1.0)
        vertexAttribs[centerSample-i+1].w = 3.0;

      // set the current position and velocity    
      velocity = FetchVelocityLinearTemporalLinear(series, TransformPosition(pos.xyz, p2v), pos.w, phaseCurrent, v2t);
      
      // add to the sample array
      vertexPositions[centerSample-i] = pos;
      vertexAttribs[centerSample-i]   = vec4(length(velocity),insideCluster,0.0,0.0);              
    }
    
    // adjacency information
    vertexPositions[0]              = vertexPositions[1];
    vertexAttribs[0]                = vertexAttribs[1];
    vertexAttribs[1].w              = 2.0;     

    if(insideFlag == true)
      vertexAttribs[1].w = 1.0;

    // -- Trace forward
    pos           = vertexPositions[centerSample];
    insideFlag    = true;
    insideCluster = 1.0;
    for(int i=1; i<(traceSamples-1)/2; i++)
    {      
      vec4  previousPos    = pos;
      float previousInside = insideCluster;      

      for(int j=0; j<steps; j++)
      {
        vec4 posTmp = pos;
              
        if(traceScheme == 0)
          pos = IntegrateRungeKutta1(series, pos, phaseCurrent, traceStepSize, traceStepDuration, 1, 100, p2v, v2t);    
        else
        if(traceScheme == 1)
          pos = IntegrateRungeKutta2(series, pos, phaseCurrent, traceStepSize, traceStepDuration, 1, 100, p2v, v2t);    
        else
          pos = IntegrateRungeKutta4(series, pos, phaseCurrent, traceStepSize, traceStepDuration, 1, 100, p2v, v2t);        
          
        // accumulate the trace length  
        traceLength = traceLength + length(pos-posTmp);
      }        

      // -- Check conditions
      posTexture   = vec4(TransformPosition(pos.xyz, p2v) / vec3(volumeDimensions), 1.0);

      // check the bounding box condition    
      if( any(lessThanEqual(posTexture.xyz,vec3(0.0))) || any(greaterThanEqual(posTexture.xyz,vec3(1.0))) )    
        pos.xyz = previousPos.xyz;   
        
      // check cluster bounding condition
      insideCluster = 0.0;
      currentLabel = FetchClusterLabelTemporalNearest(clusters, posTexture.xyz, pos.w, phaseCurrent);
      if((insideFlag == true) && (currentLabel > clusterLabel-delta) && (currentLabel < clusterLabel+delta))              
        insideCluster = 1.0;      
      else
      {
        insideFlag    = false;      
        vertexAttribs[centerSample+i-1].y = 0.0;
      }

      // determine start and end points
      if(previousInside - insideCluster == 1.0)      
        vertexAttribs[centerSample+i-1].w = 4.0;
        
      // set the current position and velocity    
      velocity = FetchVelocityLinearTemporalLinear(series, TransformPosition(pos.xyz, p2v), pos.w, phaseCurrent, v2t);

      // add to the sample array
      vertexPositions[centerSample+i] = pos;
      vertexAttribs[centerSample+i]   = vec4(length(velocity),insideCluster,0.0,0.0);   
   
    }
    
    // adjacency information   
    vertexPositions[traceSamples-1] = vertexPositions[traceSamples-2];
    vertexAttribs[traceSamples-1]   = vec4(vertexAttribs[traceSamples-2].xyz,0.0);    
    vertexAttribs[traceSamples-2].w = 5.0;    
    
    if(insideFlag == true)
      vertexAttribs[traceSamples-2].w = 6.0;
    
    // -- Emit vertices
    for(int i=0; i<traceSamples; i++)
    {
      vertexAttribs[i].z = traceLength;
      
      qfe_LinePoints     = vertexPositions[i];      
      qfe_LineAttribs    = vertexAttribs[i];
      EmitVertex();  
    }
  } // - check seed phase with current phase
  
  EndPrimitive();  
}  