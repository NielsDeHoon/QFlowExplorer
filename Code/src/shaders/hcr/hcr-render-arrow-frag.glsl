#version 120

uniform sampler1D lineTransferFunction;

uniform int       clusterMasking;
uniform float     lengthMasking;
uniform vec4      light;
uniform vec4      lightDirection;
uniform vec2      dataRange;
uniform int       colorScale;
uniform int       colorType;
uniform int       lineShading;

varying in float  radiusDepth;
varying in vec3   normal;
varying in vec2   texcoord;
varying in vec4   attribs;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
void main()
{ 
  const float delta = 0.1;
  
  vec4  eye, lightDir, color, shaded;
  float depth, yCoordNorm1, yCoordNorm2;    
  float speed;
  
  // initialize vectors
  eye         = normalize( gl_ModelViewMatrixInverse*vec4(0.0,0.0,1.0,0.0));   
  lightDir    = normalize( vec4((gl_ModelViewMatrix * -lightDirection).xyz, 0.0) );    
  
  // apply shading
  shaded = gl_Color;
  if(lineShading < 3)
    shaded = Phong(gl_Color, lightDir.xyz, normal.xyz, eye.xyz, light.x, light.y, vec2(light.z,light.w));  
  shaded.a = 1.0;  
  
  // fix for static representation
  //if(colorType == 3) discard; 
  
  gl_FragColor = shaded;  
}