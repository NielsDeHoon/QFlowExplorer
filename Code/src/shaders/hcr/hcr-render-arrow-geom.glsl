#version 120
#extension GL_EXT_gpu_shader4 : enable
#extension GL_EXT_geometry_shader4 : enable

uniform sampler1D lineTransferFunction;

uniform int   clusterMasking;
uniform float lengthMasking;

uniform float phaseCurrent;
uniform float phaseStart;
uniform float phaseEnd;
uniform int   phaseCount;
uniform mat4  patientVoxelMatrix;
uniform vec3  volumeDimensions;
uniform float lineWidth;

uniform vec2  dataRange;
uniform int   colorScale;
uniform int   colorType;
uniform vec3  colorUni;

varying in vec4 position[4];
varying in vec4 attributes[4];

varying out float radiusDepth;
varying out vec3  normal;
varying out vec2  texcoord; // (x = arclength, y = left-right)
varying out vec4  attribs;

//----------------------------------------------------------------------------
int InTemporalWindow(float timePoint, float current, float scope, int phaseCount)
{
  int result = 0;
  
  float t  = timePoint;
  float c  = current;
  float s  = scope;
  float pc = float(phaseCount);
  
  // map current phase in the temporal range
  if(t < 0.0) t = pc - mod(abs(t),pc);
  if(t > pc)  t = mod(t, pc);
  
  // check wrap around 0
  if((c>=0) && (c<0.5*s))
  {
    if(((t >  pc+c-0.5*s) && (t <= pc)) ||
       ((t >= 0.0)        && (t <  c+0.5*s)) )
       result = 1;
  }
  else
  // check wrap around at phase count
  if((c>pc-0.5*s) && (c<=pc))
  {
    if(((t >  c-0.5*s) && (t <= pc)) ||
       ((t >= 0.0)     && (t <  c+0.5*s-pc)) )
       result = 1;
  }
  else
  // check normal window behavior    
  if((t > c-0.5*s) && (t < c+0.5*s))
    result = 1;
    
  return result;
}

//----------------------------------------------------------------------------
vec3 InterpolateEndCap(vec4 pos1, vec4 pos2, float current, float scope, int phaseCount)
{  
  float t  = current - 0.5*scope;
  float pc = float(phaseCount);
  
  // map current phase in the temporal range
  if(t < 0.0) t = pc - mod(abs(t),pc);
  if(t > pc)  t = mod(t, pc);
  
  float tl = pos1.w;
  float tr = pos2.w;
  float f  = abs(t - tl)/abs(tr-tl);
  
  
  vec4  v  = vec4(pos2.xyz-pos1.xyz,0.0);
  vec4  r  = vec4(pos1.xyz,1.0) + f*v;
  
  return r.xyz;  
}

//----------------------------------------------------------------------------
void OrthonormalBasis2(in vec3 v, out vec3 u, out vec3 w)
{
  vec3 s, sabs;
  
  s    = normalize(v);
  sabs = abs(s);
  u    = s;
  
  if((sabs.x <= sabs.y) && (sabs.x <= sabs.z))  
    u = vec3(0.0, -s.z, s.y); 
  else
  if((sabs.y <= sabs.x) && (sabs.y <= sabs.z))  
    u = vec3(-s.z, 0.0, s.x);  
  else
  if((sabs.z <= sabs.x) && (sabs.z <= sabs.y))  
    u = vec3(-s.y, s.x, 0.0);  
    
  u = normalize(u);  
  
  w = cross(s, u); 
}

//----------------------------------------------------------------------------
void qfeRenderCone(vec3 start, vec3 end, float radius, vec3 color)
{
  const float twoPi    = 2.0 * 3.14159;
  const int   sections = 8;
  vec3  p[2*sections+3], n[2*sections+3];
  vec3  lineTangent, u, v;
  vec3  base;
  float theta;
  
  // Initialize
  lineTangent = end-start;  
  base        = end - lineTangent;
  theta       = twoPi / float(sections);  
  
  OrthonormalBasis2(normalize(lineTangent), u, v);
   
  // Generate the cone
  p[0] = end;
  for(int i = 0; i <= sections; i++) 
  { 
    p[2*i+1] = vec3(base + ( sin(float(i)*theta)*radius*u ) + ( cos(float(i)*theta)*radius*v ));
    p[2*i+2] = end;   
    
    n[2*i+1] = normalize(p[2*i+1]-base);
    n[2*i+2] = normalize(lineTangent);
    
  }
  
  // Render the base plate
  for(int i=0; i < 2*sections+3; i++)
  {
    gl_FrontColor = vec4(color,1.0); 
    if((i!=0) && (mod(i,0)==0))
    {
      normal = -1.0*n[i];
      gl_Position  = gl_ModelViewProjectionMatrix*vec4(base,1.0);
    }
    else
    {
      normal = n[i];
      gl_Position   = gl_ModelViewProjectionMatrix*vec4(p[i],1.0);
    }
    EmitVertex();
  }
  
  
  // Render the cone
  for(int i=0; i < 2*sections+3; i++)
  {
    normal        = n[i];
    gl_FrontColor = vec4(color,1.0); 
    gl_Position   = gl_ModelViewProjectionMatrix*vec4(p[i],1.0);
    EmitVertex();
  }
}

//----------------------------------------------------------------------------
void qfeRenderDisc(vec3 p, vec3 n, float radius, vec3 color)
{
  const float twoPi    = 2.0 * 3.14159;
  const int   sections = 8;
  vec3  b[2*sections+3];
  vec3  lineTangent, u, v;
  vec3  base;
  float theta;
  
  // Initialize   
  theta       = twoPi / float(sections);  
  
  OrthonormalBasis2(normalize(n), u, v);
   
  // Generate the disc
  b[0] = p;
  for(int i = 0; i <= sections; i++) 
  {     
    b[2*i+1] = vec3(p + ( sin(float(i)*theta)*radius*u ) + ( cos(float(i)*theta)*radius*v ));
    b[2*i+2] = p;        
  }
  
  // Render the disc
  for(int i=0; i < 2*sections+3; i++)
  {
    normal        = n;
    gl_FrontColor = vec4(color,1.0);     
    gl_Position   = gl_ModelViewProjectionMatrix*vec4(b[i],1.0);    
    EmitVertex();
  } 
}

//----------------------------------------------------------------------------
void main()
{ 
  vec3  eye;  
  vec3  lineTangent;  
    
  float speed;  
  vec4  vertexColor;
  
  // -- initialize
  eye            = vec3(0.0,0.0,1.0);        
  
  // we assume to get a line with adjacency information
  lineTangent    = normalize(position[2].xyz - position[1].xyz);     
  
  // -- compute color            
  vertexColor = vec4(colorUni,1.0);    

  if(colorType == 1 || colorType == 3) // speed
  {
    speed       = attributes[2].x / max(abs(dataRange.x), abs(dataRange.y));
    speed       = speed * (float(colorScale)/100.0);

    vertexColor = texture1D(lineTransferFunction, speed);      
  } 
  if(colorType == 2) // time
  {
    vertexColor = texture1D(lineTransferFunction, position[2].w/float(phaseCount));      
  }
  
  if(colorType == 4) // static saturation
  {
    speed       = attributes[2].x / max(abs(dataRange.x), abs(dataRange.y));
    speed       = speed * (float(colorScale)/100.0);

    if(InTemporalWindow(position[2].w, phaseCurrent, 2.0, phaseCount) == 1)
        vertexColor = texture1D(lineTransferFunction, speed);      
    else
        vertexColor = vec4(0.5,0.5,0.5,1.0);
  }
  
  if(colorType == 3) // static transparent
  {
    // this coloring requires different positions for arrowhead and caps
    // this is a hideous piece of shader, and has some flaws
    // it will have to do for now...
    int p1In, p2In;
     
    p1In = InTemporalWindow(position[1].w, phaseCurrent, 2.0, phaseCount);
    p2In = InTemporalWindow(position[2].w, phaseCurrent, 2.0, phaseCount);
    
    // -- check if we have an end point  
    if((p1In == 1) && (p2In == 0))
    {      
      qfeRenderCone(position[1].xyz, position[1].xyz+(3.0*lineWidth*lineTangent.xyz), lineWidth, vertexColor.rgb);   
    }
    else    
    if( ((p1In == 1) || (p2In == 1)) &&
        ((attributes[2].w == 5.0 && clusterMasking == 0) ||
         (attributes[2].w == 6.0)) ) 
    {
      qfeRenderCone(position[2].xyz, position[2].xyz+(3.0*lineWidth*lineTangent.xyz), lineWidth, vertexColor.rgb);   
    }  
      
    // -- check if we have a start point  
    if( ((p1In == 0) && (p2In == 1)) )
    {
      // render the cap
      vec3 endPos = InterpolateEndCap(position[1], position[2], phaseCurrent, 2.0, phaseCount);
      
      //qfeRenderDisc(endPos, -1.0*lineTangent, 0.5*lineWidth, vertexColor.rgb);     
      qfeRenderDisc(position[2].xyz, -1.0*lineTangent, 0.5*lineWidth, vertexColor.rgb);     
    }
    else
    if( ((p1In == 1) || (p2In == 1)) && 
        ((attributes[1].w == 2.0 && clusterMasking == 0) ||
         (attributes[1].w == 1.0))  )
    {             
      qfeRenderDisc(position[1].xyz, -1.0*lineTangent, 0.5*lineWidth, vertexColor.rgb);     
    }  
  }
  else
  {
    // -- check if we have an end point  
    if(  attributes[2].z > lengthMasking &&
       ((attributes[2].w == 4.0 && clusterMasking == 1) || 
        (attributes[2].w == 5.0 && clusterMasking == 0) ||
        (attributes[2].w == 6.0) ) 
      )
    {
      // render the cone
      qfeRenderCone(position[2].xyz, position[2].xyz+(3.0*lineWidth*lineTangent.xyz), lineWidth, vertexColor.rgb);   
    }  

    // -- check if we have an start point  
    if(  attributes[1].z > lengthMasking &&
       ((attributes[1].w == 3.0 && clusterMasking == 1) || 
        (attributes[1].w == 2.0 && clusterMasking == 0) ||
        (attributes[1].w == 1.0) ) 
      )
    {              
        // render the cap
        qfeRenderDisc(position[1].xyz, -1.0*lineTangent, 0.5*lineWidth, vertexColor.rgb);     
    }  
  }
        
}