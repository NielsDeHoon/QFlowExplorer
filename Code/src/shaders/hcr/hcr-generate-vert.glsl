#version 130
#extension GL_EXT_gpu_shader4 : enable

in  vec4 qfe_SeedPoints;
in  vec4 qfe_SeedAttributes;

out vec4 seedPosition;

void main(void)
{     
   seedPosition   = qfe_SeedPoints;
}
