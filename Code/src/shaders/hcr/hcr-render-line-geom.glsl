#version 120
#extension GL_EXT_gpu_shader4 : enable
#extension GL_EXT_geometry_shader4 : enable

uniform float phaseCurrent; // keep counting after wrap-around
uniform float phaseStart;
uniform float phaseEnd;
uniform int   seedingType;
uniform mat4  patientVoxelMatrix;
uniform vec3  volumeDimensions;

varying in vec4 position[4];
varying in vec4 attributes[4];

varying out vec3 tangent;
varying out vec4 attribs;

//----------------------------------------------------------------------------
void main()
{   
  const float delta = 0.005;
  
  vec4  posPatient, posTexture; 
  vec4  posScreen;
  vec3  lineTangent;
  float posPhase;     
  
  // we assume to get a line with adjacency information
  // at this point we mainly compute the tangent for lighting
  // and pass on the vertex attributes
  
  gl_FrontColor = vec4(1.0,0.0,0.0,1.0);
  posPatient    = vec4(position[1].xyz, 1.0);
  posScreen     = gl_ModelViewProjectionMatrix * posPatient;
  tangent       = normalize(0.5*(position[1].xyz - position[0].xyz) + 0.5*(position[2].xyz - position[1].xyz)); 
  attribs       = attributes[1];
  gl_Position   = posScreen; 
  EmitVertex();
  
  gl_FrontColor = vec4(0.0,1.0,0.0,1.0);
  posPatient    = vec4(position[2].xyz, 1.0);
  posScreen     = gl_ModelViewProjectionMatrix * posPatient;
  tangent       = normalize(0.5*(position[3].xyz - position[2].xyz) + 0.5*(position[2].xyz - position[1].xyz)); 
  attribs       = attributes[2];
  gl_Position   = posScreen; 
  EmitVertex();
    
}