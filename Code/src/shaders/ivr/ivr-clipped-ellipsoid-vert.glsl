#version 120
#extension GL_EXT_gpu_shader4 : enable

// Parameter space billboard corners
uniform vec4 pBBTopLeft;
uniform vec4 pBBBottomLeft;
uniform vec4 pBBBottomRight;
uniform vec4 pBBTopRight;

// Parameter to world space matrix
uniform mat4 T;

// Center of ellipsoid
uniform vec4 center;

// Parameter space position of fragment on billboard
varying vec4 pPosBB;

void main() {	
  switch (gl_VertexID) {
  case 0:   pPosBB = pBBTopLeft;        break;
  case 1:   pPosBB = pBBBottomLeft;     break;
  case 2:   pPosBB = pBBBottomRight;    break;
  case 3:   pPosBB = pBBTopRight;       break;
  default:  pPosBB = vec4(vec3(0), 1);  break;
  }
  
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}