#version 120

// Eye position in parameter space
uniform vec4 pEye;
// Parameter to world space matrix
uniform mat4 T;
// Center of ellipsoid in world space
// Also a point on the clipping plane
uniform vec4 center;
uniform vec4 clipNormal;

// Render whole surface or just contours
uniform int renderSurface;
// Render highlighted because it was selected
uniform int renderSelected;
// Use clipping plane or not
uniform int useClipPlane;

// Color of ellipsoid
uniform vec3 color;

// Parameter space position of fragment on billboard
varying vec4 pPosBB;

void main() {
  // Outside of circle in parameter space means outside of ellipsoid
  float lSq = pPosBB.x*pPosBB.x + pPosBB.y*pPosBB.y + pPosBB.z*pPosBB.z;
  if (lSq > 1)
    discard;
    
  // Determine real position on ellipsoid surface and the normal there
  float h = sqrt(1 - lSq);
  vec3 pLookDir = normalize(-pEye.xyz);
  vec3 lookDir = normalize((T*vec4(pLookDir, 0)).xyz);
  vec4 pPos = vec4(pPosBB.xyz - h*pLookDir, 1);
  vec4 pos = vec4((T*pPos).xyz + center.xyz, 1);
  vec4 normal = vec4(normalize((T*pPos).xyz), 0);
  
  // Determine if positioned in front of clip plane
  float dotClip = dot(pos - center, clipNormal);
  float backMult = 1; // Color multiplier for viewing backside/inside of ellipsoid
  if (useClipPlane == 1 && dotClip > 0) {
    backMult = 0.5;
    
    // Determine if backside of ellipsoid is in front of clip plane
    pPos = vec4(pPosBB.xyz + h*pLookDir, 1);
    pos = vec4((T*pPos).xyz + center.xyz, 1);
    normal = -vec4(normalize((T*pPos).xyz), 0); // Invert normal, because backside
    dotClip = dot(pos - center, clipNormal);
    
    // Discard fragment if backside is in front of clip plane as well
    if (dotClip > 0)
      discard;
  }
  
  // Do depth correction
  vec4 clipPos = gl_ModelViewProjectionMatrix * pos;
  float ndcDepth = clipPos.z / clipPos.w;
  gl_FragDepth = ((gl_DepthRange.diff * ndcDepth) + gl_DepthRange.near + gl_DepthRange.far) / 2.0;
  
  float dotNLook = dot(normal.xyz, -lookDir);
  vec3 endColor = (0.5 + 0.5*dotNLook) * backMult * color;
  gl_FragColor = vec4(endColor, 1);
  
  // Add contour
  if (lSq > 0.95 || (useClipPlane == 1 && dotClip > -1.5)) {
    if (renderSurface == 1)
      gl_FragColor = vec4(0, 0, 0, 1);
    else
      gl_FragColor = vec4(endColor, 1);
  } else if (renderSurface == 0) {
    discard;
  }
}