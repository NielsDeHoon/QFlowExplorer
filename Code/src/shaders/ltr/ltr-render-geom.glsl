#version 330

uniform float phaseCurrent; // keep counting after wrap-around
uniform float phaseStart;
uniform float phaseEnd;
uniform int   seedingType;
uniform vec3  volumeDimensions;
uniform mat4  patientVoxelMatrix;
uniform mat4  matrixModelViewProjection;

layout(lines_adjacency )           in;
layout(line_strip, max_vertices=2) out;

in seedAttrib
{
  vec4 position;
  vec4 attribs;
} seed[];

out lineAttrib
{
  vec3 tangent;
  vec4 attribs;
} line;

//varying in vec4 position[4];
//varying in vec4 attributes[4];

//varying out vec3 tangent;
//varying out vec4 attribs;

//----------------------------------------------------------------------------
void main()
{    
  vec4  posPatient, posTexture; 
  vec4  posScreen;
  vec3  lineTangent;
  float posPhase; 
  
  // compute tangent        
  lineTangent = normalize(seed[2].position.xyz - seed[1].position.xyz); 
    
  // we assume adjacency information, hence we have 4 vertices  
  for(int i = 1; i <=2; ++i) 
  { 
    posPatient    = vec4(seed[i].position.xyz, 1.0);
    posTexture    = vec4((patientVoxelMatrix * posPatient).xyz / volumeDimensions, 1.0);
    posScreen     = matrixModelViewProjection * posPatient;
    posPhase      = seed[i].position.w;              
  
  /*
    if((posPhase >= phaseStart) && (posPhase < phaseCurrent) && (posPhase < phaseEnd)) 
      //gl_FrontColor = vec4(1.0,0.0,0.0,1.0);
      //gl_FrontColor = vec4(vec3(posPhase/(phaseCount*2.0)),1.0);
      gl_FrontColor = vec4(1.0,0.0,0.0,1.0);
    else
      gl_FrontColor = vec4(1.0,0.0,0.0,0.0);
      
    if(seedingType == 0) // Dynamic seeding always visible
      gl_FrontColor = vec4(1.0,0.0,0.0,1.0);    
   */
          
    line.tangent  = lineTangent;  
    line.attribs  = seed[i].attribs;
    gl_Position   = posScreen; 
    
    EmitVertex();
  }
}