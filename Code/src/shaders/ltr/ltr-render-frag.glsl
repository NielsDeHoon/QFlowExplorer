#version 330

uniform sampler1D lineTransferFunction;

uniform vec4      light;
uniform vec4      lightDirection;
uniform vec2      dataRange;
uniform int       colorScale;
uniform mat4      matrixModelView;
uniform mat4      matrixModelViewInverse;

in lineAttrib
{
  vec3 tangent;
  vec4 attribs;
} line;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 PhongLine(in vec4 col, in vec3 l, in vec3 t, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
void main()
{ 
  vec4  eye, lightDir, color, shaded;
  float speed;
  
  //if(gl_Color.a == 0.0) discard;
  
  eye         = normalize( matrixModelViewInverse*vec4(0.0,0.0,1.0,0.0)); 
  lightDir    = vec4(normalize( matrixModelView * -lightDirection).xyz, 0.0);  

  speed    = line.attribs.x / max(abs(dataRange.x), abs(dataRange.y));
  speed    = speed * (float(colorScale)/100.0);
  
  color    = texture(lineTransferFunction, speed);
  
  shaded   = PhongLine(color, lightDir.xyz, line.tangent.xyz, eye.xyz, light.x, light.y, vec2(light.z, light.w));
  shaded.a = 1.0;
  
  qfe_FragColor = shaded;  
}