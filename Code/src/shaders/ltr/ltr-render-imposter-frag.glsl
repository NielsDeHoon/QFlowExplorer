#version 330

uniform sampler1D lineTransferFunction;

uniform vec4      light;
uniform vec4      lightDirection;
uniform vec2      dataRange;
uniform int       colorScale;
uniform int       lineShading;
uniform int       lineContour;
uniform int       lineHalo;
uniform int       vertexCount;
uniform int       totalCount;

in imposterAttrib
{
  float radiusDepth;
  vec3  binormal;
  vec3  texcoord; // (x = arclength, y = left-right, z = type)
  vec4  attribs;
} imposter;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Cel(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 PhongMultiplicative(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
void main()
{ 
  vec3  eye, lightDir, normal;
  vec4  color, shaded;
  float speed, depth;  
  float ndotv, yw, zw, sw, dw;
  
  float s, r, l, fuzz, frac1, frac2, frac3;
  
  float taperStart  = 0.8;
  float taperFactor = 1.0;  
  
  float stripWidth;
  float tubeWidth;
  float contourWidth;
  
  vec4  haloColor;
  
  if(imposter.texcoord.z < 0) discard;
  
  stripWidth   = 1.0;
  tubeWidth    = 0.8;
  contourWidth = 0.6;
  
  // set tapering and fuzziness
  taperFactor = 1.0;
  taperStart  = 0.95;
  fuzz        = 0.05;    
  
  // determine distance to center
  s           = stripWidth*abs(imposter.texcoord.y-0.5);    
  
  yw          = (imposter.texcoord.y*2.0)-1.0;
  sw          = yw*(taperFactor*stripWidth + abs(taperFactor*stripWidth - taperFactor*tubeWidth) + 2.0*fuzz); 
  zw          = 1.0 - abs(sw);
  dw          = 1.0 - abs(yw);
 
  // determine the tapering
  r           = (0.0-1.0)/(1.0-taperStart);
  l           = imposter.attribs.z/ float(totalCount);
  
  taperFactor = (l-taperStart)*r + 1.0;
  taperFactor = clamp(taperFactor, 0.0, 1.0);  
    
  eye         = vec3(0.0,0.0,1.0);   
  lightDir    = normalize(-lightDirection.xyz);  
  normal      = vec3(imposter.binormal.xy * yw, zw);
  depth       = gl_FragDepth;
  ndotv       = dot(normal.xyz, eye.xyz);
  
  frac1       = smoothstep(taperFactor*tubeWidth -fuzz,  taperFactor*tubeWidth,    2.0*s);
  frac2       = smoothstep(taperFactor*stripWidth-fuzz,  taperFactor*stripWidth,   2.0*s);  
  frac3       = smoothstep(taperFactor*contourWidth-fuzz,taperFactor*contourWidth, 2.0*s);
  
  speed       = imposter.attribs.x / max(abs(dataRange.x), abs(dataRange.y));
  speed       = speed * (float(colorScale)/100.0);  

  // set the depth  
  depth = gl_FragCoord.z + (1.0-dw)*(imposter.radiusDepth-gl_FragCoord.z);
    
  // fetch the color
  color  = texture(lineTransferFunction, speed);

  // appy the shading
  if(lineShading == 1)
    shaded = Phong(color, lightDir.xyz, normal.xyz, eye.xyz, light.x, light.y, vec2(light.z,light.w));
  else
  if(lineShading == 2)
    shaded = PhongMultiplicative(color, lightDir.xyz, normal.xyz, eye.xyz, light.x, light.y, vec2(light.z,light.w));
  else
  if(lineShading == 3)
    shaded = color;
  else
    shaded   = Cel(color, lightDir.xyz, normal.xyz);  
   
  // apply depth correction
  gl_FragDepth = depth;
  
  // todo: get rid of ugly conditions here (= anti-aliasing trick)
  if(s < 0.5*taperFactor*stripWidth)
  {    
    if(s < 0.5*taperFactor*tubeWidth)          
    {
       // apply the contour
      if(lineContour == 1)    
      {
        if(s < 0.5*taperFactor*contourWidth)         
          shaded  = mix(shaded, vec4(vec3(0.0),1.0), frac3); 
        else
          shaded  = mix(vec4(vec3(0.0),1.0), vec4(vec3(0.5),1.0), frac1);         
      }      
      else
        shaded  = mix(shaded, vec4(vec3(0.5),1.0), frac1);   
    }
    else
    {
      // apply the halo
      if(lineHalo == 1)      
        shaded  = mix(vec4(1.0), vec4(vec3(0.8),1.0), frac2);   
      else
        discard;
    }
  }
  else discard;
   
  qfe_FragColor = shaded;   
}