#version 120

uniform int       useDataSet0;
uniform int       useDataSet1;
uniform sampler3D dataSet0;
uniform vec2      range0;
uniform sampler3D dataSet1;
uniform vec2      range1;
uniform sampler1D tf0;
uniform sampler1D tf1;
uniform sampler2D rayEnd;
uniform sampler2D rayEndDepth;
uniform sampler2D intersectColor;
uniform sampler2D intersectDepth;
uniform int       intersectAvailable;
uniform vec3      scaling;   // dimensions * voxel size (mm)
uniform float     stepSize;  // minimum of voxel size (mm)
uniform int       depthToneMapping;
uniform float     depthToneMappingFactor;
uniform mat4      voxelPatientMatrix;

// Use clipping volume of interest or not
uniform int useVoI;
uniform mat4 voiTInv;
uniform vec4 voiCenter;

uniform vec3 dimensions;

varying vec4 posTexture;
varying vec4 posScreen;

vec4 CompositeScalarFrontToBack(vec4 src, vec4 dst);

vec3 GetDepthColor(float depth) {
  return mix(vec3(1.0, 0.5, 0.5), vec3(0.2, 0.2, 0.5), depth);
}

bool DiscardOutsideVoI(vec4 p) {
  vec4 pp = transpose(voiTInv)*(p - voiCenter);
  if (pp.x*pp.x + pp.y*pp.y + pp.z*pp.z > 1)
    return true;
  else
    return false;
}

void main() {    
  // Compute normalized device coordinate
  vec2 texCoord = (posScreen.xy/posScreen.w + 1.0) / 2.0;
              
  // Determine start and end of rays  
  vec3 start = posTexture.xyz;
  vec3 end = texture2D(rayEnd, texCoord.xy).xyz;
    
  start = clamp(start, 0.0, 1.0)*scaling;
  end = clamp(end, 0.0, 1.0)*scaling;
  
  vec4 dst = vec4(0.0);
  
  float depthFront = gl_FragCoord.z;
  float depthBack = texture2D(rayEndDepth, texCoord.xy).r;
  
  // If front is further than back, swap
  if (depthFront > depthBack) {
    float temp = depthFront;
    depthFront = depthBack;
    depthBack = temp;
    vec3 temp2 = start;
    start = end;
    end = temp2;
  }
  
  // Intersection with opaque geometry
  if (intersectAvailable == 1) {
    // Determine if ray hit opaque geometry
    float intersectD = texture2D(intersectDepth, texCoord.xy).r;
    if (intersectD != 1.0) {
      vec4 intersectC = texture2D(intersectColor, texCoord.xy);
      float intersectOffset = abs(clamp(intersectD, depthFront, depthBack) - depthFront)/abs(depthBack-depthFront);
      vec3 intersectPos = start + intersectOffset*(end - start);
      end = intersectPos;
    }
  }
  
  vec3 ray = start;
  vec3 rayVector = end - start;
  vec3 rayDir = normalize(rayVector);
  float rayMaxLength = length(rayVector);
  float rayOffset = 0.0;
  
  // If there is no ray, discard the fragment
  if (rayMaxLength <= 0.01)
    discard;

  float stepSizeLocal = stepSize/3.0;
  for (int i=0; i <= int(rayMaxLength/stepSizeLocal); i++) {
    // Early ray termination if out of bounding box or ray is fully opaque
    if (rayOffset >= rayMaxLength || dst.a >= 1.0)
      break;
      
    // Clipping outside of volume of interest
    vec4 patientPos = voxelPatientMatrix * vec4( ray/dimensions, 1);
    if (useVoI == 1 && DiscardOutsideVoI(patientPos)) {
      ray += rayDir * stepSizeLocal;
      rayOffset += stepSizeLocal;
      continue;
    }
    
    // Calculate probability
    float probability0 = 1;
    if (useDataSet0 == 1) {
      vec4 sample0 = texture3D(dataSet0, ray/scaling);
      float sampleValue0 = sample0.r;
      float sampleNorm0 = (sampleValue0 - range0.x) / (range0.y - range0.x);
      probability0 = texture1D(tf0, sampleNorm0).r;
    }
    
    float probability1 = 1;
    if (useDataSet1 == 1) {
      vec4 sample1 = texture3D(dataSet1, ray/scaling);
      float sampleValue1 = sample1.r;
      float sampleNorm1 = (sampleValue1 - range1.x) / (range1.y - range1.x);
      probability1 = texture1D(tf1, sampleNorm1).r;
    }
    
    // Calculate opacity
    float opacity = probability0 * probability1;
    vec4 sampleColor = vec4(1, 0, 1, opacity);
    
    // Skip sample if it is too transparent
    if (sampleColor.a < 0.01) {
      ray += rayDir * stepSizeLocal;
      rayOffset += stepSizeLocal;
      continue;
    }

    // Retrieve depth color
    float depth = rayOffset / rayMaxLength;
    vec3 depthColor = GetDepthColor(depth);
    
    if (depthToneMapping == 1)
      sampleColor.rgb = mix(sampleColor.rgb, depthColor.rgb, depthToneMappingFactor);
    
    // Front-to-back compositing
    dst = CompositeScalarFrontToBack(sampleColor, dst);
    
    ray += rayDir * stepSizeLocal;
    rayOffset += stepSizeLocal;
  }  
  
  if (intersectAvailable == 1)
    dst = mix(dst, texture2D(intersectColor, texCoord.xy), 1.0 - dst.a);
    
  // Make this visualization transparent
  dst.a *= 0.5;
	
  gl_FragColor = dst;
}