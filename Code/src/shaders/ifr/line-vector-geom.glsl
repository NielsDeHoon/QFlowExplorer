// line-velocity-geom
#version 330

uniform sampler3D volumeData;
uniform ivec3     volumeDimensions;

uniform sampler1D transferFunction;

uniform mat4      matrixVoxelPatient;
uniform mat4      matrixPatientVoxel;
uniform mat4      matrixModelViewProjection;

uniform vec3      lineColor;
uniform int       lineColorMode;

uniform float     phaseDuration;

layout(points) in;
layout(line_strip, max_vertices=2) out;

in  vec3  seedPatientCoord[1];
out vec4  lineCol;
out vec3  lineTangentNorm;

//----------------------------------------------------------------------------
vec3 transPatientToVoxel(vec3 p)
{
  return (matrixPatientVoxel * vec4(p,1.0)).xyz;
}

//----------------------------------------------------------------------------
// seed in patient coordinates
// tf is a 1D transfer function
vec4 getColorBySpeed(vec3 seed, sampler1D tf)
{
  float speed;
  vec3  texcoord;
  vec4  velocity, color;
  
  texcoord = transPatientToVoxel(seed) / vec3(volumeDimensions);
  velocity = texture(volumeData, texcoord); 
  speed    = length(velocity.xyz);
  color    = texture(tf, speed/200.0); 
  
  return color;
}

//----------------------------------------------------------------------------
// index in the linestrip
// tf is a 1D transfer function
vec4 getColorByPropagation(int index, sampler1D tf)
{
  float time;
  vec4  color;
  
  color  = texture(tf, index); 
  
  return color;
}

//----------------------------------------------------------------------------
// seed in patient coordinates
// tf is a 1D transfer function
vec4 getColorByDistanceToVoxel(vec3 seed, sampler1D tf)
{
  float distance;
  vec3  voxelcoord;
  ivec3 voxelpos[8];
  vec4  velocity, color;
  
  voxelcoord  = transPatientToVoxel(seed);
  voxelpos[0] = ivec3(voxelcoord) + ivec3(0,0,0);
  voxelpos[1] = ivec3(voxelcoord) + ivec3(1,0,0);
  voxelpos[2] = ivec3(voxelcoord) + ivec3(1,1,0);
  voxelpos[3] = ivec3(voxelcoord) + ivec3(0,1,0);
  voxelpos[4] = ivec3(voxelcoord) + ivec3(0,0,1);
  voxelpos[5] = ivec3(voxelcoord) + ivec3(1,0,1);
  voxelpos[6] = ivec3(voxelcoord) + ivec3(1,1,1);
  voxelpos[7] = ivec3(voxelcoord) + ivec3(0,1,1);
  
  
  distance = 2.0;
  for(int i=0; i<8; i++)
  {
    float current = length(voxelcoord - vec3(voxelpos[i]));
    if(current < distance) distance = current;
  }
  
  color    = texture(tf, distance*2.0); 
  
  return color;
}

//----------------------------------------------------------------------------
// seed in patient coordinates
// index is the line strip [0, n]
// tf is a 1D transfer function
vec4 getColor(vec3 seed, int index, sampler1D tf)
{
  vec4 color = vec4(0.8,0.0,0.0,1.0);
  
  if(lineColorMode == 0) color = getColorBySpeed(seed, tf);
  if(lineColorMode == 1) color = getColorByPropagation(index, tf);
  if(lineColorMode == 2) color = getColorByDistanceToVoxel(seed, tf);
  if(lineColorMode == 3) color = vec4(lineColor,1.0);
  
  return color;
}

//----------------------------------------------------------------------------
void main(void)
{   
  int  i, j;
  vec4 beginPos, endPos, velocity;
  
  velocity = texture(volumeData, transPatientToVoxel(seedPatientCoord[0]) / vec3(volumeDimensions));
 
  beginPos = vec4(seedPatientCoord[0], 1.0);
  endPos   = beginPos + velocity*(phaseDuration/1000.0);
  
  lineTangentNorm = normalize(endPos - beginPos).xyz;
  
  lineCol   = getColor(beginPos.xyz, 0, transferFunction);  
  gl_Position = matrixModelViewProjection*vec4(beginPos.xyz, 1.0);
  EmitVertex();
  
  lineCol   = getColor(endPos.xyz, 1, transferFunction); 
  gl_Position = matrixModelViewProjection*vec4(endPos.xyz, 1.0);
  EmitVertex();

  EndPrimitive();

}
 