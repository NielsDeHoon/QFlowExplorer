// silhouette-dilate-frag.glsl
#version 330

uniform vec3 color;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
int diagonalPattern(int x, int y, int n)
{
  float fract = float(x + 1 - mod(y, 2*n)) / float(n);
  
  return int( mod(int(ceil(fract)), 2) );
}

//----------------------------------------------------------------------------
void main()
{  
  int   offset    = 5;
  float colFrac   = 0.4;
  
  qfe_FragColor = vec4(clamp(color,0.0,1.0),1.0);  
  
  //if( diagonalPattern(int(gl_FragCoord.x),int(gl_FragCoord.y),offset) == 1 )
  //  //qfe_FragColor = vec4(vec3(0.6),1.0);
  //  qfe_FragColor = vec4(mix(vec3(0.7),color,colFrac),1.0);
  qfe_FragColor = vec4(0.0f,0.0f,0.0f,1.0f);
}