// arrow-vert
#version 330

uniform mat4 voxelPatientMatrix;

in  vec3 qfe_Vertex;

out vec3 seedPatientCoord;

void main()
{
  seedPatientCoord = (voxelPatientMatrix * vec4(qfe_Vertex,1.0)).xyz;
}