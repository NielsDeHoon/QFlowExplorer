#version 120
#extension GL_EXT_geometry_shader4 : enable

varying in  vec3 vertexNormal[3];
varying out vec3 normal;

//----------------------------------------------------------------------------
void main()
{ 
  vec3  triangleNormal, eye;
  float ndote; 
  
  // Initialize
  triangleNormal = vec3(0.0);
  eye            = vec3(0.0,0.0,-1.0);

  // Determine the triangle normal  
  for(int i = 0; i < 3; ++i)
  {
    triangleNormal = triangleNormal + 1.0/3.0*vertexNormal[i]; 
  }
  
  // Compute the angle
  ndote = dot(triangleNormal, eye);
  
  // Render the front facing triangles
  if(ndote < 0.0)
  {
    // Render the triangle
    for(int i = 0; i < gl_VerticesIn; ++i)
    {       
      normal        = vertexNormal[i];
      gl_FrontColor = gl_FrontColorIn[i]; 
  
      gl_Position = gl_PositionIn[i];  
  
      EmitVertex();
    }  
  }
}