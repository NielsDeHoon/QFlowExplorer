uniform sampler3D volumeData;
uniform sampler1D TransferFunction;
uniform int       DataNormalized;
uniform vec3      PlaneNormal;
uniform vec2      Range;

int VectorColorMode = 1;

vec4 eyePos;
vec4 eyeVec;

vec3 recPlaneNormal;


//----------------------------------------------------------------------------
void main()
{
  //eyePos = gl_ModelViewMatrixInverse * vec4(0.0, 0.0, 0.0, 1.0);
  //eyeVec = eyePos.xyz - gl_Vertex.xyz;
  vec4 sample_coord = gl_TexCoord[0];
  vec4 sample_color = texture3D(volumeData, sample_coord.xyz);
  
  sample_color.a = 1.0;
  
  if(sample_coord.x < 0.0 || sample_coord.x > 1.0 ||
     sample_coord.y < 0.0 || sample_coord.y > 1.0 ||
     sample_coord.z < 0.0 || sample_coord.z > 1.0)
     discard;
  
  //gl_FragColor = vec4(1.0,0.0,0.0,1.0);
  
  // Deprecated: Direct texture mapping, without transfer function
  //gl_FragColor = vec4(sample_color.r * 25.0, sample_color.r * 25.0,sample_color.r * 25.0,1.0);
  //gl_FragColor = vec4(sample_color.r * 200.0, sample_color.r * 200.0,sample_color.r * 200.0,1.0);
  gl_FragColor = vec4(sample_color.rgb * 25.0,sample_color.a);
  
  float val;
  
  // x - component
  //val = ((sample_color.r+200.0)/400.0)*1.2;
  
  // y - component
  //val = ((sample_color.g+200.0)/400.0)*1.2;
  
  // z - component
  //val = ((sample_color.b+200.0)/400.0)*1.2;
  
  // magnitude
  val = sqrt(pow(sample_color.r,2.0)+pow(sample_color.g,2.0)+pow(sample_color.b,2.0))/200.0;
  
  gl_FragColor = vec4(val, val, val, sample_color.a);
  
  //gl_FragColor = vec4(((sample_color.rgb+200.0)/400.0)*1.2,sample_color.a);
  

}