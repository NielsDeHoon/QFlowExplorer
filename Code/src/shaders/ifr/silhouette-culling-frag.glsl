// silhouette-culling-frag.glsl
#version 330

uniform int       silhouetteShadingMode;
uniform sampler1D lightTex;
uniform vec3      lightDir;
uniform vec4      lightSpec;
uniform vec3      color;
uniform int       culling;
uniform int       wss;

in  vec3 normal;
out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Cel(in vec4 col, in vec3 l, in vec3 n);
vec4 Cel2(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 XRay(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec4 Gooch(in vec4 col, in vec3 l, in vec3 n, in vec3 v);

//----------------------------------------------------------------------------
void main()
{
  float ndotl, ndote, index, diffuse;
  vec4 col;
  vec3 n = normalize(normal);
  vec3 l = normalize(-lightDir);
  vec3 e = vec3(0.0,0.0,1.0);

  // We are rendering backfaces, get the right angle (-n * l)  
  if(culling == 1)
    n = normalize(-normal);  
    
  ndotl = dot(n,l);  
  ndote = dot(n,-e);
  col   = vec4(color,1.0);
  /*
  if(wss == 1)
  {
  //vec4 wssColor = vec4(0.43,0.48,1.0,1.0);
  vec4 wssColor = vec4(0.1,0.1,1.0,1.0);
  if(gl_Color.a > 0.75)
    color = mix(color, wssColor, 1.0);
  else if (gl_Color.a > 0.5)
    color = mix(color, wssColor, 0.5);
  else if (gl_Color.a > 0.25)
    color = mix(color, wssColor, 0.2);
  }
  */
  
  if(silhouetteShadingMode == 0) // Toon shading 1
  {
    col = Cel2(col, l.xyz, n.xyz);    
  }
  else  
  if(silhouetteShadingMode == 1) // Toon shading 2
  {
    index = max(ndotl,0.0);  
    col = texture(lightTex, index);
    col = col * vec4(color,1.0);   
  }
  else
  //silhouetteShadingMode == 2 // Fresnel (different shader)
  if(silhouetteShadingMode == 3) // Diffuse shading
  {
    col = Phong(col, l.xyz, n.xyz, e.xyz, lightSpec.x, lightSpec.y, vec2(lightSpec.z,lightSpec.w));
  }  
  else
  if(silhouetteShadingMode == 4) // Flat shading
  {
	col = col;
  }
  else
  if(silhouetteShadingMode == 5) // X-Ray shading
  {
	col = XRay(col, e, n);
  }
  else
  if(silhouetteShadingMode == 6) // Gooch shading
  {
	col = Gooch(col, l.xyz, n.xyz, e.xyz);
  }
  
  col.r = 1.0;
  
  qfe_FragColor = col;
}