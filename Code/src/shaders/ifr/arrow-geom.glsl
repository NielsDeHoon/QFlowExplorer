// arrow-geom
#version 330

layout(points) in;
layout(line_strip, max_vertices=9) out;

uniform mat4      voxelPatientMatrix;
uniform mat4      patientVoxelMatrix;

uniform mat4      matrixModelViewProjection;
uniform mat4      matrixModelViewInverse;

uniform vec3      vector;
uniform vec3      axisX;

uniform int       phaseDuration;
uniform float     scaleFactor;

uniform int       trailMax;
uniform int       trailCount;

out vec4          color;
in vec3           seedPatientCoord[1];

//----------------------------------------------------------------------------
vec3 transPatientToVoxel(vec3 p)
{
  return (patientVoxelMatrix * vec4(p,1.0)).xyz;
}

//----------------------------------------------------------------------------
// origin in patient coordinates
// direction in patient coordinates
// scale factor between base and head in x and y direction
void drawArrow(vec3 origin, vec3 axisX, vec3 axisY, vec2 proportions, float length, float scale)
{
   vec3 p[8];
  
   normalize(axisX);
   normalize(axisY);
   
   float baseWidth  = 1.0*proportions.x;
   float baseHeight = 2.0*proportions.y*length*scale;
   float headWidth  = 2.0*proportions.x;
   float headHeight = 1.0*proportions.y*length*scale;
    
   p[0] = origin;
   p[1] = origin  + axisX*(baseWidth/2.0);
   p[2] = p[1]    + axisY*(baseHeight);
   p[3] = p[2]    + axisX*(headWidth/2.0 - baseWidth/2.0);
   p[4] = origin  + axisY*(baseHeight    + headHeight);
   p[5] = p[3]    - axisX*(headWidth);
   p[6] = p[2]    - axisX*(baseWidth);
   p[7] = origin  - axisX*(baseWidth/2.0);
  
   for(int i=0; i<=8; i++)
   {
     gl_Position = matrixModelViewProjection*vec4(p[int(i%8)], 1.0);
     EmitVertex();
   }
   
   EndPrimitive();
  
}


//----------------------------------------------------------------------------
void main(void)
{   
  vec4  x, y, e, u, v;
  float l, c;
  
  x = normalize( voxelPatientMatrix*vec4(axisX,0.0) );
  e = normalize( matrixModelViewInverse*vec4(0.0,0.0,1.0,0.0)); 
  
  v = normalize( vec4(vector, 0.0) );
  u = normalize( vec4( cross(e.xyz, v.xyz) , 0.0) );    
  
  l = length(vector);
  
  c = trailCount / float(trailMax);
  c = c * 0.9; // Eliminate white arrows
  
  color = vec4(c, c, c, 1.0-c);
  
  drawArrow(seedPatientCoord[0], u.xyz, v.xyz, vec2(3.0,3.0), l, (phaseDuration/1000.0)*scaleFactor);

}
 