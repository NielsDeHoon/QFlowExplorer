// silhouette-dilate-geom.glsl
#version 330

uniform int   culling;
uniform float vertexOffset;

uniform mat4  matrixModelViewProjection;

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in  vec3 vertexPosition[3];
in  vec3 vertexNormal[3];

//----------------------------------------------------------------------------
void main()
{   
  // Enlarge the mesh in object space
  gl_Position   = matrixModelViewProjection * vec4(vertexPosition[0]+(vertexOffset*vertexNormal[0]),1.0);  
  EmitVertex();
    
  gl_Position   = matrixModelViewProjection * vec4(vertexPosition[1]+(vertexOffset*vertexNormal[1]),1.0);  
  EmitVertex();
   
  gl_Position   = matrixModelViewProjection * vec4(vertexPosition[2]+(vertexOffset*vertexNormal[2]),1.0);  
  EmitVertex(); 
      
  EndPrimitive();  
}