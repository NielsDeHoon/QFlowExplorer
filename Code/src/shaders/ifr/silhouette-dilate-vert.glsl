// silhouette-dilate-vert.glsl
#version 330

uniform mat4 matrixModelViewProjection;

in  vec3 qfe_Vertex;
in  vec3 qfe_Normal;

out vec3 vertexPosition;
out vec3 vertexNormal;

//----------------------------------------------------------------------------
void main()
{  
  vertexNormal   = normalize(qfe_Normal);
  vertexPosition = qfe_Vertex;
}