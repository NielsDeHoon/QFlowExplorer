// line-path-geom
#version 330

uniform sampler3D volumeData; // volume at the seedTime phase of the cardiac cycle
uniform sampler3D volumeData1;
uniform sampler3D volumeData2;
uniform sampler3D volumeData3;
uniform sampler3D volumeData4;
uniform ivec3     volumeDimensions;

uniform sampler1D transferFunction;

uniform mat4      matrixVoxelPatient;
uniform mat4      matrixPatientVoxel;
uniform mat4      matrixModelViewProjection;

uniform float     traceStep;
uniform int       traceIterations;
uniform int       traceCounter;
uniform int       traceDirection; //[-1,1]
uniform float     traceSeedTime;

uniform float     phaseDuration;

uniform vec3      lineColor;
uniform int       lineColorMode;

layout(points) in;
layout(line_strip, max_vertices=90) out;

in  vec3  seedPatientCoord[1];
out vec4  lineCol;
out float lineTexCoord;
out vec3  lineTangentNorm;


//----------------------------------------------------------------------------
vec3 transPatientToVoxel(vec3 p)
{
  return (matrixPatientVoxel * vec4(p,1.0)).xyz;
}

//----------------------------------------------------------------------------
vec3 vectorInterpolateSphericalLinear(vec3 v1, vec3 v2, float t)
{
  vec3  v1n, v2n, u;
  float alpha;

  v1n = normalize(v1);
  v2n = normalize(v2);

  alpha = acos(dot(v1n,v2n));

  u = v1 * (sin((1.0-t)*alpha)/sin(alpha)) +
      v2 * (sin(     t *alpha)/sin(alpha));
      
  return u;
}

//----------------------------------------------------------------------------
//! Point p in voxel coordinates
//! Time  t in phases (0,1,...)
vec4 seriesGetVectorAtPosition(vec3 p, int t)
{
  vec4 v = vec4(0.0,0.0,0.0,0.0);
  
  // Volume are updated with the seedtime
  // Return the correct volume by shifting the time
  // with the seedTime
  int dataPhase = (t-int(traceSeedTime));
  
  if(dataPhase == 0) v = texture(volumeData,  p);
  if(dataPhase == 1) v = texture(volumeData1, p);
  if(dataPhase == 2) v = texture(volumeData2, p);
  if(dataPhase == 3) v = texture(volumeData3, p);
  if(dataPhase == 4) v = texture(volumeData4, p);
  
  return v;
}

//----------------------------------------------------------------------------
//! Point p in voxel coordinates
//! Time  t in phases (0,1,...)
vec3 seriesGetVectorAtPositionLinear(vec3 p, float t)
{
  vec4 v, yl, yr;

  int   tl = int(t); // treated as a floor by glsl
  int   tr = (tl+1);
  float ti = t - tl;
  
  yl = seriesGetVectorAtPosition(transPatientToVoxel(p) / vec3(volumeDimensions), tl);
  yr = seriesGetVectorAtPosition(transPatientToVoxel(p) / vec3(volumeDimensions), tr);

  // Spherical linear interpolation
  v = vec4(vectorInterpolateSphericalLinear(yl.xyz, yr.xyz, ti),0.0);
  
  // Linear interpolation
  //v = mix(yl, yr, ti);
  
  return v.xyz;
}

//----------------------------------------------------------------------------
// seed     in patient coordinates
// seedTime in phases
// step     in patient coordinates (mm)
// volume texture
// returns new point in patient coordinates (with point.w is new time in phases)
vec4 computeRungeKuttaTemporal(vec4 seed, float step, int dir)
{
  vec3  vi, vi_1, vi_2, vi_3;
  vec3  s;
  float t;
  float stepSize;
  
  // Set the trace direction
  if(dir >= 0) dir = 1;
  else         dir = -1;
  
  // Phase image velocities is defined in cm/s
  // Step size in phases
  stepSize = step / phaseDuration;
  
  // Determine initial time
  s = seed.xyz;
  t = seed.w;

  // Get the velocity (requires texture coordinates)
  vi      = dir * seriesGetVectorAtPositionLinear(seed.xyz, traceSeedTime);

  vec3  k1 = vi   * stepSize;  
  vec3  p1 = s    + k1*0.5;  
  float t1 = t    + stepSize*0.5;

  vi_1    = dir * seriesGetVectorAtPositionLinear(p1, t1);

  vec3  k2 = vi_1 * stepSize;
  vec3  p2 = s    + k2*0.5;
  float t2 = t    + stepSize*0.5;
  
  vi_2    = dir * seriesGetVectorAtPositionLinear(p2, t2);

  vec3  k3 = vi_2 * stepSize;
  vec3  p3 = s    + k3;
  float t3 = t    + stepSize;
  
  vi_3    = dir * seriesGetVectorAtPositionLinear(p3, t3);

  vec3 k4 = vi_3 * stepSize;

  // New position in voxel coordinates
  s = s + (k1 + k2*2.0 + k3*2.0 + k4)*(1.0/6.0);
  t = t + stepSize;
  
  return vec4(s, t);
  
}

//----------------------------------------------------------------------------
// seed in patient coordinates
// tf is a 1D transfer function
vec4 getColorBySpeed(vec3 seed, sampler1D tf)
{
  float speed;
  vec3  texcoord;
  vec4  velocity, color;
  
  texcoord = transPatientToVoxel(seed) / vec3(volumeDimensions);
  velocity = texture(volumeData, texcoord); 
  speed    = length(velocity.xyz);
  color    = texture(tf, speed/200.0); 
  
  return color;
}

//----------------------------------------------------------------------------
// index in the linestrip
// tf is a 1D transfer function
vec4 getColorByPropagation(int index, sampler1D tf)
{
  float time;
  vec4  color;
  
  time   = index / float(traceIterations);
  color  = texture(tf, time); 
  
  return color;
}

//----------------------------------------------------------------------------
// for pathlines we only take the distance between timesteps into account
// seed in patient coordinates
// tf is a 1D transfer function
vec4 getColorByDistanceToVoxel(vec4 seed, sampler1D tf)
{
  float distance;
  vec4  color;
  
  distance = 2.0*min(seed.w - int(seed.w), (int(seed.w)+1) - seed.w);
  
  color    = texture(tf, distance); 
  
  return color;
}

//----------------------------------------------------------------------------
// seed in patient coordinates
// index is the line strip [0, n]
// tf is a 1D transfer function
vec4 getColor(vec4 seed, int index, sampler1D tf)
{
  vec4 color = vec4(0.8,0.0,0.0,1.0);
  
  if(lineColorMode == 0) color = getColorBySpeed(seed.xyz, tf);
  if(lineColorMode == 1) color = getColorByPropagation(index, tf);
  if(lineColorMode == 2) color = getColorByDistanceToVoxel(seed, tf);
  if(lineColorMode == 3) color = vec4(lineColor,1.0);
  
  return color;
}


//----------------------------------------------------------------------------
void main(void)
{   
  int  i, j, highlightCounter;
  vec4 nextPos, currPos;
  vec4 highlightColor = vec4(0.9,0.9,0.9,1.0);
  
  currPos         = vec4(seedPatientCoord[0], traceSeedTime);
  lineTangentNorm = normalize(computeRungeKuttaTemporal(currPos, traceStep, traceDirection) - currPos).xyz;
  lineCol         = getColor(currPos, 0, transferFunction);
  gl_Position     = matrixModelViewProjection*vec4(currPos.xyz,1.0);
  EmitVertex();


  nextPos = vec4(seedPatientCoord[0],traceSeedTime);   
  for(int j=1; j<=traceIterations; j++)  
  { 
    lineCol = getColor(nextPos, j, transferFunction);
    
    if(traceDirection < 0 ) highlightCounter = traceIterations - (traceCounter - 1);    
    else                    highlightCounter = traceCounter    - 1;
    
    if(j == highlightCounter ) lineCol.xyz = highlightColor.xyz;         

    nextPos         = computeRungeKuttaTemporal(nextPos, traceStep, traceDirection);     
    lineTangentNorm = normalize(nextPos - currPos).xyz;
    currPos         = nextPos;
    gl_Position     = matrixModelViewProjection*vec4(currPos.xyz,1.0);

    EmitVertex();
  }
  

  EndPrimitive();  


}
 
 