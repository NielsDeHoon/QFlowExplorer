// contour-frag.glsl

varying float t_ndote;
varying float t_kr;

uniform vec3 lightDir;
uniform vec4 lightSpec;

//----------------------------------------------------------------------------
void main()
{  
  float delta;
  
  delta = 0.01;
  
  if(((t_ndote*t_ndote)*(1.0-t_kr)) > delta) discard;
  
  gl_FragColor = gl_Color;
}
