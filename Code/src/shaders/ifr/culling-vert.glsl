// culling-vert.glsl
out vec3 vertexNormal;

//----------------------------------------------------------------------------
void main()
{  
  vertexNormal  = gl_NormalMatrix  * gl_Normal;   
  
  //gl_FrontColor = gl_Color;
  
  gl_Position   = ftransform();
}