#include "qfeDriverVPR.h"

//----------------------------------------------------------------------------
qfeDriverVPR::qfeDriverVPR(qfeScene *scene) : qfeDriver(scene)
{  
  this->visProbe3D        = NULL;  
  this->visOrtho          = NULL;
  this->visMPR            = NULL;  
  this->visMPRP           = NULL;
  this->visParticleTrace  = NULL;
  this->visLineTrace      = NULL;
  this->visSurfaceTrace   = NULL;
  this->visDVR            = NULL;
  this->visMIP            = NULL;  
  this->visSurface        = NULL;
  this->visAides          = NULL;

  this->intersectTexColor     = 0;
  this->intersectTexDepth     = 0;
  this->clippingTexColor      = 0;
  this->clippingFrontTexDepth = 0;
  this->clippingBackTexDepth  = 0;

  // Initialize plane   
  this->planeX = new qfeFrameSlice();      
  this->planeX->qfeSetFrameExtent(1,1);
  this->planeX->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeX->qfeSetFrameAxes(0.0,0.0,1.0, 0.0,1.0,0.0, 1.0,0.0,0.0);  

  this->planeY = new qfeFrameSlice();      
  this->planeY->qfeSetFrameExtent(1,1);
  this->planeY->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeY->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,0.0,1.0, 0.0,1.0,0.0);  
  
  this->planeZ = new qfeFrameSlice();      
  this->planeZ->qfeSetFrameExtent(1,1);
  this->planeZ->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeZ->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0);  

  this->planeSelected         = qfePlaneNone;

  this->planeColorDefault.r   = 0.952;
  this->planeColorDefault.g   = 0.674;
  this->planeColorDefault.b   = 0.133;

  this->planeColorHighlight.r = 0.356;
  this->planeColorHighlight.g = 0.623;
  this->planeColorHighlight.b = 0.396;

  this->planeProbeOrthogonalSelected = -1;

  this->algorithmProbe           = new qfeFlowProbe3D();  
  this->algorithmOctree          = new qfeFlowOctree();
  this->algorithmMPR             = new qfePlanarReformat();   
  this->algorithmParticleTrace   = new qfeFlowParticleTrace();  
  this->algorithmLineTrace       = new qfeFlowLineTrace();    
  this->algorithmSurfaceTrace    = new qfeFlowSurfaceTrace();
  this->algorithmDVR             = new qfeRayCasting();  
  this->algorithmMIP             = new qfeMaximumProjection();   
  this->algorithmSurfaceShading  = new qfeSurfaceShading();
  this->algorithmSurfaceContours = new qfeSurfaceContours();

  // Initialize probe
  this->probeActionState        = probeDefault;
  this->probeClickState         = probeSelectStart;
  this->probeStrokeState        = probeSelectStart;
  this->probeAnimateCount       = 0;
  this->probeClickOffset.x      = 0.0;
  this->probeClickOffset.y      = 0.0;
  this->probeClickOffset.z      = 0.0;

  // Initialize probe color set
  this->probeColor[0].r = 251.0/256.0; // red
  this->probeColor[0].g = 128.0/256.0;
  this->probeColor[0].b = 114.0/256.0;
  this->probeColor[1].r = 128.0/256.0; // blue
  this->probeColor[1].g = 177.0/256.0; 
  this->probeColor[1].b = 211.0/256.0;
  this->probeColor[2].r = 190.0/256.0; // purple
  this->probeColor[2].g = 186.0/256.0;
  this->probeColor[2].b = 218.0/256.0;  
  this->probeColor[3].r = 141.0/256.0; // green
  this->probeColor[3].g = 211.0/256.0;
  this->probeColor[3].b = 199.0/256.0;  
  this->probeColor[4].r = 253.0/256.0; // orange
  this->probeColor[4].g = 180.0/256.0;
  this->probeColor[4].b =  98.0/256.0;    
  this->probeColor[5].r = 255.0/256.0; // yellow
  this->probeColor[5].g = 255.0/256.0;
  this->probeColor[5].b = 179.0/256.0;    
  
  this->surfaceStyle            = qfeFlowSurfaceTube;
  this->surfaceRadius           = 10;
  this->surfaceCount            = 1;

  this->dvrQuality              = 2.0;
    
  this->prevTime                = 0.0;

  this->probeShadowList.clear();  

  this->engine->SetObserverAction((void*)this, this->qfeOnAction);
  this->engine->SetObserverResize((void*)this, this->qfeOnResize);
  this->engine->SetInteractionMouseLeftButtonDown((void *)this, this->qfeOnMouseLeftButtonDown, false);
  this->engine->SetInteractionMouseLeftButtonUp((void *)this, this->qfeOnMouseLeftButtonUp);
  this->engine->SetInteractionMouseMove((void *)this, this->qfeOnMouseMove);
  this->engine->SetInteractionMouseWheelForward((void*)this, this->qfeOnWheelForward, false);
  this->engine->SetInteractionMouseWheelBackward((void*)this, this->qfeOnWheelBackward, false);
  this->engine->SetInteractionKeyPress((void*)this, this->qfeOnKeyPress, true);
};

//----------------------------------------------------------------------------
qfeDriverVPR::~qfeDriverVPR()
{
  delete this->algorithmProbe;
  delete this->algorithmOctree;
  delete this->algorithmMPR;  
  delete this->algorithmParticleTrace;
  delete this->algorithmLineTrace;  
  delete this->algorithmSurfaceTrace;
  delete this->algorithmDVR;
  delete this->algorithmMIP;  
  delete this->algorithmSurfaceShading;
  delete this->algorithmSurfaceContours;

  delete this->planeX;
  delete this->planeY;
  delete this->planeZ;

  for(int i=0; i<(int)this->planeProbeOrthogonal.size(); i++)
  {
    delete this->planeProbeOrthogonal[i];
    this->planeProbeOrthogonal[i] = NULL;
  }
  this->planeProbeOrthogonal.clear();  
  this->planeProbeOrthogonalOffset.clear();
    
  for(int i=0; i<(int)this->planeProbeParallel.size(); i++)
  {
    delete this->planeProbeParallel[i];
    this->planeProbeParallel[i] = NULL;
  }
  this->planeProbeParallel.clear();
  
  this->qfeDeleteTextures();

  this->qfeRenderStop();
};

//----------------------------------------------------------------------------
bool qfeDriverVPR::qfeRenderCheck()
{
  int t;
  for(int i=0; i<(int)this->visuals.size(); i++)
  {
    this->visuals[i]->qfeGetVisualType(t);

    switch(t)
    {
    case visualReformatOrtho : 
      this->visOrtho     = static_cast<qfeVisualReformatOrtho *>(this->visuals[i]);
      break;
    case visualReformatOblique : 
      this->visMPR     = static_cast<qfeVisualReformatOblique *>(this->visuals[i]);
      break;
    case visualReformatObliqueProbe:
      this->visMPRP    = static_cast<qfeVisualReformatObliqueProbe *>(this->visuals[i]);
      break;
    case visualMaximum :
      this->visMIP     = static_cast<qfeVisualMaximum *>(this->visuals[i]);
      break;
    case visualRaycast :
      this->visDVR     = static_cast<qfeVisualRaycast *>(this->visuals[i]);
      break;
    case visualFlowProbe3D :
      this->visProbe3D = static_cast<qfeVisualFlowProbe3D *>(this->visuals[i]);
      break;
    case visualFlowParticles :
      this->visParticleTrace = static_cast<qfeVisualFlowParticleTrace *>(this->visuals[i]);
      break;
    case visualFlowLines :
      this->visLineTrace = static_cast<qfeVisualFlowLineTrace *>(this->visuals[i]);
      break;
    case visualFlowSurfaces :
      this->visSurfaceTrace = static_cast<qfeVisualFlowSurfaceTrace *>(this->visuals[i]);
      break;
    case visualAnatomyIllustrative :    
      this->visSurface = static_cast<qfeVisualAnatomyIllustrative *>(this->visuals[i]);
      break;
    case visualAides :
      this->visAides  = static_cast<qfeVisualAides *>(this->visuals[i]);
      break;
    default : cout << "qfeDriverVPR - Undefined visual type" << endl;
    }
  }

  return true;
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeRenderStart()
{
  // On a resize, the start will be called again, 
  // setting up a texture equal to the viewport size
  qfeViewport         *vp3D;
  int                 supersampling;

  qfeSelectionList    paramShadingList;

  this->engine->GetViewport3D(&vp3D);
  supersampling = this->frameBufferProps[0].supersampling;

  if(this->visMIP != NULL)
  {
    qfeVisualParameter *p;
    this->visMIP->qfeGetVisualParameter("MIP visible", &p);
    p->qfeSetVisualParameterValue(false);
    
    this->algorithmMIP->qfeSetViewportSize(vp3D->width, vp3D->height, supersampling);    
  }
  if(this->visDVR != NULL)
  {
    qfeVisualParameter *p;
    this->visDVR->qfeGetVisualParameter("DVR visible", &p);
    p->qfeSetVisualParameterValue(false);

    this->algorithmDVR->qfeSetViewportSize(vp3D->width, vp3D->height, supersampling);
  }

  // Initialize Ortho
  if(this->visOrtho != NULL)
  {
    qfeStudy *study;   

    this->visOrtho->qfeGetVisualStudy(&study);

    if((int)study->pcap.size() > 0)
    {
      this->qfeGetDataSlice(study->pcap.front(), qfePlaneSagittal, &this->planeX);  
      this->qfeGetDataSlice(study->pcap.front(), qfePlaneCoronal,  &this->planeY);
      this->qfeGetDataSlice(study->pcap.front(), qfePlaneAxial,    &this->planeZ);
    }
  }

  // Initialize MPR
  if(this->visMPR != NULL)
  { 
    qfeStudy *study;   
    qfeVisualParameter *p;

    this->visMPR->qfeGetVisualStudy(&study); 

    this->visMPR->qfeGetVisualParameter("MPR visible", &p);
    p->qfeSetVisualParameterValue(true); 
  }  

  // Initialize particle trace
  if(this->visParticleTrace != NULL)
  {
    qfeStudy *study;
    qfeVisualParameter p;

    this->visParticleTrace->qfeGetVisualStudy(&study); 

    p.qfeSetVisualParameter("Particles lifetime", (int)study->pcap.size(), 1, 1, 500);
    this->visParticleTrace->qfeAddVisualParameter(p);

    this->scene->qfeSceneUpdate();
  }

  // Initialize line trace
  if(this->visLineTrace != NULL)
  {
    qfeStudy *study;
    qfeVisualParameter p;

    this->visLineTrace->qfeGetVisualStudy(&study); 

    p.qfeSetVisualParameter("Lines phases static", (int)study->pcap.size(), 1, 1, 500);
    this->visLineTrace->qfeAddVisualParameter(p);

    this->scene->qfeSceneUpdate();
  }

  // Initialize surface trace
  if(this->visSurfaceTrace != NULL)
  {
    qfeVisualParameter *p;
     
    this->visSurfaceTrace->qfeGetVisualParameter("Surface radius (mm)", &p);
    if(p != NULL) p->qfeGetVisualParameterValue(this->surfaceRadius); 

    this->visSurfaceTrace->qfeGetVisualParameter("Surface tube count", &p);
    if(p != NULL) p->qfeGetVisualParameterValue(this->surfaceCount); 
  }
  
  // Initialize probe 
  if(this->visProbe3D != NULL)
  {
    qfeStudy    *study;
    qfeViewport *viewport;
    int          current;

    //Obtain the original texture and depth buffer
    this->engine->GetViewport3D(&viewport);

    this->visProbe3D->qfeGetVisualStudy(&study); 
    this->visProbe3D->qfeGetVisualActiveVolume(current);    

    this->algorithmProbe->qfeSetProbeSuperSamplingFactor(supersampling);

    // Create alternative color and depth texture 
    this->qfeCreateTextures(viewport, supersampling);    

    // Intialize probe properties
    this->probeInteractionState.clear();    
    this->planeInteractionState      = planeInteractRotate;
    this->probeSeedingStateParticles = probeSeedingNone;
    this->probeSeedingStateLines     = probeSeedingNone;
    this->probeSeedingStateSurfaces  = probeSeedingNone;
    this->planeProbeParallel.clear();
    this->planeProbeOrthogonal.clear();
    this->planeProbeOrthogonalOffset.clear();

    for(int i=0; i<(int)study->probe3D.size(); i++)
    {
      this->probeShadowList.push_back(study->probe3D[i]);
      this->probeInteractionState.push_back(probeInteractNone);      
      this->planeProbeParallel.push_back(new qfeFrameSlice());
      this->planeProbeOrthogonal.push_back(new qfeFrameSlice());
      this->planeProbeOrthogonalOffset.push_back(0.0);
    }

    // Compute the octree, use tmop if available
    if((int)study->tmop != NULL)
    {
      this->algorithmOctree->qfeBuildFlowOctree(study->tmop);
    }
    else
    if((int)study->pcap.size() > 0)
    {
      this->algorithmOctree->qfeBuildFlowOctree(study->pcap[current]);
    }
    else
    {
      this->algorithmOctree->qfeBuildFlowOctree(NULL);
      cout << "qfeDriverVPR::qfeRenderStart - Could not initialize octree" << endl;
    }    

    this->algorithmOctree->qfeWriteTest();
  }  

  if(this->visSurface != NULL)
  {
    qfeVisualParameter *p;
    this->visSurface->qfeGetVisualParameter("Surface shading", &p);
    if(p != NULL) p->qfeGetVisualParameterValue(paramShadingList);
    paramShadingList.active = 3;
    p->qfeSetVisualParameterValue(paramShadingList);
  }
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeRenderWait()
{
  int supersampling = this->frameBufferProps[0].supersampling;

  // Initialize OpenGL state
  //glEnable(GL_DEPTH_TEST);
  glDisable(GL_LIGHTING);

  // Initialize color map
  qfeColorMap *colormap;
  if(this->visOrtho != NULL)
  {
    this->visOrtho->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  if(this->visMPR != NULL) 
  {
    this->visMPR->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();   
  }  
  if(this->visMPRP != NULL) 
  {
    this->visMPRP->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();   
  }    
  if(this->visMIP != NULL) 
  {
    this->visMIP->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();   
  }
  if(this->visDVR != NULL) 
  {
    this->visDVR->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();   
    colormap->qfeUploadGradientMap();   
  }
  if(this->visParticleTrace != NULL)
  {
    this->visParticleTrace->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap(); 
  }
  if(this->visLineTrace != NULL)
  {
    this->visLineTrace->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap(); 
  }  
  if(this->visSurfaceTrace != NULL)
  {
    this->visSurfaceTrace->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap(); 
  }

  // Clear the intersection buffer
  this->qfeEnableIntersectBuffer();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  this->qfeDisableIntersectBuffer();

  // Clear the front and back clipping buffer
  this->qfeEnableClippingBuffer(this->clippingFrontTexDepth);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  this->qfeDisableClippingBuffer();

  this->qfeEnableClippingBuffer(this->clippingBackTexDepth);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  this->qfeDisableClippingBuffer();

  // Note strict order, first geometry, than volume renderings

  // Render Particles
  if(this->visParticleTrace != NULL)
  {
     this->qfeRenderVisualParticleTrace(this->visParticleTrace, supersampling);     
  }

  // Render Lines
  if(this->visLineTrace != NULL)
  {
     this->qfeRenderVisualLineTrace(this->visLineTrace, supersampling);     
  }

  // Render Surfaces  
  if(this->visSurfaceTrace != NULL)
  {
     this->qfeRenderVisualSurfaceTrace(this->visSurfaceTrace, supersampling);     
  }
  
  // Render Probe (Needs to be before MPR, because of stencil mask)
  if(this->visProbe3D != NULL)
  {
     this->qfeRenderVisualProbe(this->visProbe3D, supersampling);     
  }

  // Render Planar Reformat
  if(this->visOrtho != NULL)
  {
     this->qfeRenderVisualOrtho(this->visOrtho, supersampling);
  }
  if(this->visMPR != NULL)
  {
     this->qfeRenderVisualMPR(this->visMPR, supersampling);     
  }
  if(this->visMPRP != NULL)
  {
    this->qfeRenderVisualMPRP(this->visMPRP, supersampling);
  }

  // Render Visual Aides
  if(this->visAides != NULL)
  {
    this->qfeRenderVisualAides(this->visAides, supersampling);
  }

  // Render the Illustrative Surface
  if(this->visSurface != NULL)
  {
    this->qfeRenderVisualAnatomyIllustrative(this->visSurface, supersampling);
  }
  
  // Render DVR (Last to blend with geometry)
   if(this->visDVR != NULL)
  {    
    this->qfeRenderVisualDVR(this->visDVR, supersampling); 
  }
  
  // Render MIP (Last to blend with geometry)
  if(this->visMIP != NULL)
  {    
    this->qfeRenderVisualMIP(this->visMIP, supersampling); 
  }

};

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeRenderSelect()
{
  qfeColorRGB color;   
  qfeVolume  *volume;
  qfeStudy   *study;    
  qfeFloat    vl, vu;

  qfeVector   eyeVec;
  qfeMatrix4f mvInv;
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);

  // Get eye vector in patient coordinates;
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;

  // Selection objects for the probe
  if(this->visProbe3D != NULL)
  {    
    this->visProbe3D->qfeGetVisualStudy(&study);     

    for(int i=0; i<(int)study->probe3D.size(); i++)
    {      
      // Render the rotation handle
      color.r = color.g = i / 255.0f;
      color.b = SELECT_PROBE_HANDLE;
      this->qfeSetSelectColor(color);
      this->algorithmProbe->qfeRenderProbeHandleRotation(&study->probe3D[i], color);  

      // Render the probe
      color.r = color.g = i / 255.0f;
      color.b = SELECT_PROBE;      

      this->qfeSetSelectColor(color);

      if(this->probeInteractionState[i] == probeInteractNone)     
        this->algorithmProbe->qfeGetProbeViewAligned(study->probe3D[i], eyeVec);  

      this->algorithmProbe->qfeSetProbeAppearance(qfeFlowProbe3D::qfeFlowProbeFlat);
      this->algorithmProbe->qfeRenderProbe(&study->probe3D[i]);

      // Render the probe clip plane to the stencil buffer
      // Disable updates in the color and depth attachments;
      glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
      glDepthMask(GL_FALSE);
   
      glEnable(GL_STENCIL_TEST);
      
      // Always pass a one to the stencil buffer where we draw
      glStencilFunc(GL_ALWAYS, 1, 0xFFFFFFFF);
      glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);

      // Render the probe clip plane to the stencil buffer
      this->algorithmProbe->qfeRenderProbeClipPlane(&study->probe3D[i]); 

      // Turn the color and depth buffers back on
      glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
      glDepthMask(GL_TRUE);

      glDisable(GL_STENCIL_TEST);        
    
    }
  }

  // Selection objects for ortho planes
  if(this->visOrtho != NULL)
  { 
    qfeVisualParameter *param;
    bool                paramShowX, paramShowY, paramShowZ;
    qfeColorRGB         color;  

    // Get the visual parameters
    this->visOrtho->qfeGetVisualParameter("Ortho plane RL (X)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);    

    this->visOrtho->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);    

    this->visOrtho->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);   

    // Process the visual parameters
    this->visOrtho->qfeGetVisualStudy(&study);
    
    if(((int)study->pcap.size() > 0) && ((int)study->probe3D.size() == 0))
    {
      if(paramShowX)
      {
        color.r = color.g = 1.0f / 255.0f;
        color.b = SELECT_PLANE_ORTHO;
        this->qfeSetSelectColor(color);
        this->algorithmMPR->qfeRenderPolygon3D(study->pcap.front(), this->planeX, NULL);
      }

      if(paramShowY)
      {
        color.r = color.g = 2.0f / 255.0f;
        color.b = SELECT_PLANE_ORTHO;
        this->qfeSetSelectColor(color);
        this->algorithmMPR->qfeRenderPolygon3D(study->pcap.front(), this->planeY, NULL);
      }
      
      if(paramShowZ)
      {
        color.r = color.g = 3.0f / 255.0f;
        color.b = SELECT_PLANE_ORTHO;
        this->qfeSetSelectColor(color);
        this->algorithmMPR->qfeRenderPolygon3D(study->pcap.front(), this->planeZ, NULL);
      }
    }   
  }

  // Selection objects for reformatting
  if(this->visMPR != NULL)
  {
    qfeVisualParameter *param;
    qfeSelectionList    paramDataList;
    bool                paramShowPlane;

    this->visMPR->qfeGetVisualStudy(&study);   

    // Get parameters
    this->visMPR->qfeGetVisualParameter("MPR visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowPlane);    
    
    this->visMPR->qfeGetVisualParameter("MPR data set", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);      
    this->qfeGetDataSet((qfeDataTypeIndex)paramDataList.active, this->visMPR, 0, &volume, vl, vu);        

    // Assign a color    
    color.r = color.g = color.b = SELECT_PLANE;
    this->qfeSetSelectColor(color);
    
    // Render the planes    
    glEnable(GL_STENCIL_TEST);

    // Until stencil test is disabled, only write to areas where the stencil buffer equals one. 
    glStencilFunc(GL_EQUAL, 0, 0xFFFFFFFF);    
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);    

    int probeCount = (int)study->probe3D.size();
    if(this->probeActionState == probeClick) 
      probeCount = 0;
     
    if(paramShowPlane)
    {
      for(int i=0; i<(int)study->probe3D.size(); i++)
        this->algorithmMPR->qfeRenderPolygon3D(volume, this->planeProbeParallel[i], NULL);      
    }
  
    glDisable(GL_STENCIL_TEST);
  }

  // Selection objects for reformatting
  if(this->visMPRP != NULL)
  {
    qfeVisualParameter *param;
    qfeSelectionList    paramDataList;
    bool                paramVisible;

    this->visMPRP->qfeGetVisualStudy(&study);   

    // Get parameters
    this->visMPRP->qfeGetVisualParameter("MPRP visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);    
    
    this->visMPRP->qfeGetVisualParameter("MPRP data set", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);      
    this->qfeGetDataSet((qfeDataTypeIndex)paramDataList.active, this->visMPRP, 0, &volume, vl, vu);        

    // Assign a color        
    if(paramVisible)
    {      
      for(int i=0; i<(int)study->probe3D.size(); i++)
      {
        color.r = color.g = i / 255.0f;
        color.b = SELECT_PLANE_PROBE;
        this->qfeSetSelectColor(color);    
    
        this->algorithmMPR->qfeRenderCircle3D(volume, volume, this->planeProbeOrthogonal[i], NULL);            
      }
    }    
  }
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeRenderStop()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeRenderVisualProbe(qfeVisualFlowProbe3D *visual, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *volume;  
  qfeLightSource     *light;
  int                 current;
  qfeMatrix4f         P2V, V2T, P2T;

  qfeVisualParameter *param;
  qfeSelectionList    paramListAppearance;
  bool                paramProbeVisible;
  bool                paramContourVisible;
  //bool                paramCenterline;
  bool                paramAxes;
  bool                paramOctreeVisible;
  qfeSelectionList    paramOctreeType;
  double              paramOctreeCoherence;
  int                 paramOctreeLevel;

  this->scene->qfeGetSceneLightSource(&light);

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);  

  if(study == NULL) return qfeError;  
  if((int)study->pcap.size() <= 0) return qfeError;

  this->visProbe3D->qfeGetVisualParameter("Probe visible", &param);    
  if(param != NULL) param->qfeGetVisualParameterValue(paramProbeVisible); 

  this->visProbe3D->qfeGetVisualParameter("Probe appearance", &param);    
  if(param != NULL) param->qfeGetVisualParameterValue(paramListAppearance);

    this->visProbe3D->qfeGetVisualParameter("Probe contour", &param);    
  if(param != NULL) param->qfeGetVisualParameterValue(paramContourVisible); 

  //this->visProbe3D->qfeGetVisualParameter("Probe centerline", &param);    
  //if(param != NULL) param->qfeGetVisualParameterValue(paramCenterline);

  this->visProbe3D->qfeGetVisualParameter("Probe axes", &param);    
  if(param != NULL) param->qfeGetVisualParameterValue(paramAxes);

  this->visProbe3D->qfeGetVisualParameter("Octree visible", &param);    
  if(param != NULL) param->qfeGetVisualParameterValue(paramOctreeVisible);

  this->visProbe3D->qfeGetVisualParameter("Octree type", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramOctreeType);

  this->visProbe3D->qfeGetVisualParameter("Octree coherence level", &param);    
  if(param != NULL) param->qfeGetVisualParameterValue(paramOctreeCoherence);

  this->visProbe3D->qfeGetVisualParameter("Octree hierarchy level", &param);    
  if(param != NULL) param->qfeGetVisualParameterValue(paramOctreeLevel);

  volume = study->pcap[current];

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  P2T = P2V*V2T;

  // Get the view vector
  qfeMatrix4f mvInv;   
  qfeVector   eyeVec;
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);
  eyeVec.qfeSetVectorElements(0.0,0.0,-1.0);  
  eyeVec = eyeVec*mvInv;

  // Prepare octree rendering  
  qfePoint *center = new qfePoint[5];

  glEnable(GL_DEPTH_TEST);

  if(this->probeActionState == probeClick) 
    paramProbeVisible = false;    

  // Render the probes
  if(paramProbeVisible)
  {   
    qfeVector   probeNormal;
    qfeVector   eyeVecInterpolated;         
    qfeFloat    fraction;     

    for(int i=0; i<(int)study->probe3D.size(); i++)
    {      
      probeNormal = study->probe3D[i].axes[0];

      this->algorithmProbe->qfeGetProbeCenterline(&study->probe3D[i], 5, &center);

      // Check the current interaction state, we might need to animate
      switch(this->probeInteractionState[i])
      {
        case probeInteractAnimate :          
          // Interpolate between the plane normal and the eye vector
          fraction = (ANIMATION_FRAME_COUNT-this->probeAnimateCount)/(float)ANIMATION_FRAME_COUNT;

          qfeVector::qfeVectorInterpolateLinear(-1.0f*probeNormal, eyeVec, fraction, eyeVecInterpolated);                    
          this->algorithmProbe->qfeGetProbeViewAligned(study->probe3D[i], eyeVecInterpolated);     

          // Make sure the normal does not flip by checking the direction
          // This should never happen, otherwise we look at the back of the probe
          if(probeNormal*study->probe3D[i].axes[0] < 0)
          {            
            study->probe3D[i].axes[0] = -1.0f*study->probe3D[i].axes[0];
            qfeVector::qfeVectorCross(study->probe3D[i].axes[0], study->probe3D[i].axes[2], study->probe3D[i].axes[1]);
          }

          this->probeAnimateCount--;          

          if(this->probeAnimateCount <= 0)
          {
            this->probeAnimateCount        = 0;
            this->probeInteractionState[i] = probeInteractNone;
          }
          break;
        case probeInteractNone :                  
          this->algorithmProbe->qfeGetProbeViewAligned(study->probe3D[i], eyeVec);     
          break;      
      }      

      // Render the probe to the intersect buffer
      this->qfeEnableIntersectBuffer();
      this->algorithmProbe->qfeRenderProbe(&study->probe3D[i]);  
      this->qfeDisableIntersectBuffer(); 

      this->algorithmProbe->qfeSetProbeAppearance((qfeFlowProbe3D::qfeFlowProbe3DAppearance)paramListAppearance.active);
      this->algorithmProbe->qfeSetProbeContour(paramContourVisible);
      this->algorithmProbe->qfeSetProbeLightSource(light);

      this->algorithmProbe->qfeRenderProbe(&study->probe3D[i]);  
      
      //if(paramCenterline) this->algorithmProbe->qfeRenderProbeCenterline(&study->probe3D[i], 2.0);   
      if(paramAxes)       this->algorithmProbe->qfeRenderProbeAxes(&study->probe3D[i], 2.0);     
    }    
  }
  
  // Render the octree
  if(paramOctreeVisible)
  {
    switch(paramOctreeType.active)
    {
      case 0: this->algorithmOctree->qfeRenderOctree((qfeFloat)paramOctreeCoherence);
              break;
      case 1: this->algorithmOctree->qfeRenderOctree((int)paramOctreeLevel);
              break;
      case 2: for(int i=0; i<5; i++)
                this->algorithmOctree->qfeRenderOctree(center[i], (int)paramOctreeLevel);
              break;
    }      
  }

  // Render workflow steps
  qfePoint probePointVisible[2];
  qfeFloat delta = 0.1;

  probePointVisible[0] = this->probePoints[0] - delta*eyeVec;
  probePointVisible[1] = this->probePoints[1] - delta*eyeVec;

  glDepthFunc(GL_ALWAYS);

  glPointSize(2.0*supersampling);

  if(this->probeActionState == probeClick && this->probeClickState == probeSelectSecond)
    this->qfeDrawPoint(probePointVisible[0], supersampling);

  if(this->probeActionState == probeStroke && this->probeStrokeState == probeSelectSecond)
    this->qfeDrawLine(probePointVisible[0], probePointVisible[1], supersampling);

  glDepthFunc(GL_LESS);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeRenderVisualOrtho(qfeVisualReformat *visual, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *volumeBounding;
  qfeVolume          *volumeTexture;
  qfeGrid            *grid;
  qfeColorMap        *colormap;
  qfePoint            origin;
  qfePoint            extent;
  unsigned int        dims[3];
  qfeFloat            size, depth;
  
  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  int                 paramInterp;
  double              paramBrightness;
  double              paramContrast;
  qfeFloat            paramRange[2];
  bool                paramShowX, paramShowY, paramShowZ;
  bool                paramProbeVisible;
  bool                paramDVRVisible;  
  int                 current;
  qfeColorRGB         colorX, colorY, colorZ;
  qfeFloat            tt;
  qfeVector           x,xp,y,yp,z,n,d;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  // Get visual parameters
  visual->qfeGetVisualParameter("Ortho data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("Ortho data vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("Ortho data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("Ortho data interpolation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInterpList);  
  paramInterp = paramInterpList.active;

  visual->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);    

  visual->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);    

  visual->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);  

  visual->qfeGetVisualParameter("Ortho contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);   

  visual->qfeGetVisualParameter("Ortho brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);   

  // Check if the probe is visible
  if(this->visProbe3D != NULL)
  {
    this->visProbe3D->qfeGetVisualParameter("Probe visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramProbeVisible);
  }
  else paramProbeVisible = false;

  // Check if the volume rendering is visible
  if(this->visDVR != NULL)
  {
    this->visDVR->qfeGetVisualParameter("DVR visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDVRVisible);
  }
  else paramDVRVisible = false;
 
  // Process parameters  
  this->qfeGetDataSet((qfeDataTypeIndex)dataTypePCAP, visual, current, &volumeBounding, paramRange[0], paramRange[1]); // PCA-P
  this->qfeGetDataSet((qfeDataTypeIndex)paramData,    visual, current, &volumeTexture,  paramRange[0], paramRange[1]); // Selected volume

  if(volumeBounding != NULL)
  {
    volumeBounding->qfeGetVolumeGrid(&grid);
    volumeBounding->qfeGetVolumeTriggerTime(tt);
    volumeBounding->qfeGetVolumeSize(dims[0], dims[1], dims[2]);
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);   
    grid->qfeGetGridExtent(extent.x, extent.y, extent.z);
  }
  else 
  {
    volumeBounding = volumeTexture;
    origin.x = origin.y = origin.z = 0.0;  
    extent.x = extent.y = extent.z = 1.0;
    dims[0] = dims[1] = dims[2] = 1;
  }

  size  = max(max(dims[0]*extent.x, dims[1]*extent.y), dims[2]*extent.z) * sqrt(2.0f);
  depth = sqrt(pow(dims[0]*extent.x, 2.0f) + pow(dims[1]*extent.y, 2.0f) + pow(dims[2]*extent.y, 2.0f));
  
  // Determine line color
  colorX = colorY = colorZ = this->planeColorDefault;
  if(this->planeSelected == qfePlaneX)  colorX = this->planeColorHighlight;
  if(this->planeSelected == qfePlaneY)  colorY = this->planeColorHighlight;
  if(this->planeSelected == qfePlaneZ)  colorZ = this->planeColorHighlight;

  // Update algorithm parameters    
  this->algorithmMPR->qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
  this->algorithmMPR->qfeSetPlaneDataComponent(paramComp);
  this->algorithmMPR->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmMPR->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  this->algorithmMPR->qfeSetPlaneTriggerTime(tt); 
  this->algorithmMPR->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  this->algorithmMPR->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmMPR->qfeSetPlaneContrast(paramContrast);

  int probeCount = (int)study->probe3D.size();
  if (this->probeActionState == probeClick)
    probeCount = 0;

  // If there is no probe defined: render the planes
  if(probeCount <= 0)
  {
    if(paramShowX)
    {
      this->algorithmMPR->qfeSetPlaneBorderColor(colorX);
      this->algorithmMPR->qfeRenderPolygon3D(volumeTexture, this->planeX, colormap);      
    }

    if(paramShowY) 
    { 
      this->algorithmMPR->qfeSetPlaneBorderColor(colorY);
      this->algorithmMPR->qfeRenderPolygon3D(volumeTexture, this->planeY, colormap);      
    }

    if(paramShowZ)
    {  
      this->algorithmMPR->qfeSetPlaneBorderColor(colorZ);
      this->algorithmMPR->qfeRenderPolygon3D(volumeTexture, this->planeZ, colormap);      
    }
  } 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeRenderVisualMPR(qfeVisualReformat *visual, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *volumeBounding;  
  qfeVolume          *volumeTexture;  
  qfeColorMap        *colormap;
  qfePoint            origin;
  qfePoint            extent;
  unsigned int        dims[3];  

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  int                 paramInterp;
  int                 paramBalance;
  qfeFloat            paramRange[2];
  qfeFloat            paramRange2[2];
  bool                paramShowPlane;
  bool                paramShowContour;
  bool                paramProbeVisible;
  bool                paramDVRVisible;
  double              paramBrightness;
  double              paramContrast;
  double              paramOpacity1;
  int                 current;
  qfeColorRGB         color;
  qfeFloat            tt;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  
  // Get visual parameters
  visual->qfeGetVisualParameter("MPR visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowPlane);    

  visual->qfeGetVisualParameter("MPR contour", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowContour);   

  visual->qfeGetVisualParameter("MPR data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("MPR data vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("MPR data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("MPR data interpolation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInterpList);  
  paramInterp = paramInterpList.active;

  visual->qfeGetVisualParameter("MPR mix (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBalance);  

  visual->qfeGetVisualParameter("MPR opacity 1", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramOpacity1);  

  visual->qfeGetVisualParameter("MPR brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);  

  visual->qfeGetVisualParameter("MPR contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);  
  
  if(this->visProbe3D != NULL)
  {
    this->visProbe3D->qfeGetVisualParameter("Probe visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramProbeVisible);  
  }
  else paramProbeVisible = false;

  if(this->visDVR != NULL)
  {
    this->visDVR->qfeGetVisualParameter("DVR visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDVRVisible);  
  }
  else paramDVRVisible = false;
  
  // Process parameters
  this->qfeGetDataSet((qfeDataTypeIndex)dataTypePCAP, visual, current, &volumeBounding, paramRange[0], paramRange[1]); // PCA-P
  this->qfeGetDataSet((qfeDataTypeIndex)paramData,    visual, current, &volumeTexture, paramRange[0], paramRange[1]); // Selected volume 

  if(volumeBounding == NULL)
    volumeBounding = volumeTexture;

  study->qfeGetStudyRangePCAP(paramRange2[0], paramRange2[1]);
  
  // Determine the origin of the active volume and size of clipping planes
  qfeFloat size, depth;
  qfeGrid *grid;

  volumeBounding->qfeGetVolumeGrid(&grid);
  volumeBounding->qfeGetVolumeSize(dims[0], dims[1], dims[2]);
  grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  grid->qfeGetGridExtent(extent.x, extent.y, extent.z);

  size  = max(max(dims[0]*extent.x, dims[1]*extent.y), dims[2]*extent.z) * sqrt(2.0f);
  depth = sqrt(pow(dims[0]*extent.x, 2.0f) + pow(dims[1]*extent.y, 2.0f) + pow(dims[2]*extent.y, 2.0f));  

  // Determine line color
  color = this->planeColorDefault;
  tt = 0.0;

  if(this->algorithmProbe == NULL) return qfeError;

  // Update algorithm parameters  
  this->algorithmMPR->qfeSetPlaneBorderWidth(3.5*supersampling);
  this->algorithmMPR->qfeSetPlaneDataComponent(paramComp);
  this->algorithmMPR->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmMPR->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  this->algorithmMPR->qfeSetPlaneTriggerTime(tt); 
  this->algorithmMPR->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  this->algorithmMPR->qfeSetPlaneDataRange2(paramRange2[0], paramRange2[1]);
  this->algorithmMPR->qfeSetPlaneBorderColor(color);
  this->algorithmMPR->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmMPR->qfeSetPlaneContrast(paramContrast);  
  this->algorithmMPR->qfeSetPlaneBalance(paramBalance);
  this->algorithmMPR->qfeSetPlaneContour(paramShowContour);
  this->algorithmMPR->qfeSetPlaneOpacity(paramOpacity1);

  int probeCount = (int)study->probe3D.size();

  if (this->probeActionState == probeClick) 
    probeCount = 0;

  glEnable(GL_BLEND);

  // For each probe, render a probe aligned plane  
  for(int i=0; i<probeCount; i++)
  {    
    // Create a static list of the probe, so we can fix the planes if necessary
    if(this->planeInteractionState != planeInteractFixed)
      this->probeShadowList[i] = study->probe3D[i];   

    // Update the plane position and orientation
    this->planeProbeParallel[i]->qfeSetFrameExtent(size, size);
    this->planeProbeParallel[i]->qfeSetFrameOrigin(this->probeShadowList[i].origin.x, this->probeShadowList[i].origin.y, this->probeShadowList[i].origin.z);
    this->planeProbeParallel[i]->qfeSetFrameAxes(this->probeShadowList[i].axes[1].x, this->probeShadowList[i].axes[1].y, this->probeShadowList[i].axes[1].z,
                                                 this->probeShadowList[i].axes[2].x, this->probeShadowList[i].axes[2].y, this->probeShadowList[i].axes[2].z,
                                                 this->probeShadowList[i].axes[0].x, this->probeShadowList[i].axes[0].y, this->probeShadowList[i].axes[0].z);
    

    // Enable stencil buffer
    glClear(GL_STENCIL_BUFFER_BIT);
    glEnable(GL_STENCIL_TEST);

    if(study->probe3D[i].selected && paramProbeVisible)
    {
      qfeProbe3D p;

      // Disable updates in the color and depth attachments;
      glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
      glDepthMask(GL_FALSE);

      // Always pass a one to the stencil buffer where we draw
      glStencilFunc(GL_ALWAYS, 1, 0xFFFFFFFF);
      glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);

      // Render the probe clip plane to the stencil buffer         
      //p = study->probe3D[i];
      p = this->probeShadowList[i];
      p.topRadius  = p.topRadius  * 1.15;
      p.baseRadius = p.baseRadius * 1.15;
      p.length     = p.length     * 1.15;
      this->algorithmProbe->qfeRenderProbeClipPlane(&p); 

      // turn the color and depth buffers back on
      glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
      glDepthMask(GL_TRUE);

      // Until stencil test is disabled, only write to areas where the stencil buffer equals one. 
      glStencilFunc(GL_EQUAL, 0, 0xFFFFFFFF);    
      glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    }   
         
    // Render the planes
    if(study->probe3D[i].selected && paramShowPlane) 
    {   
      if(paramDVRVisible)
      {        
        // For this convex opaque geometry, we only need the backfaces
        // The front faces are always outside of the boundingbox
        this->qfeEnableClippingBuffer(this->clippingBackTexDepth);                
          glEnable(GL_CULL_FACE);  
          glCullFace(GL_FRONT);
          this->algorithmMPR->qfeRenderClipBox3D(this->planeProbeParallel[i], size, depth); 
          glDisable(GL_CULL_FACE);
        this->qfeDisableClippingBuffer();

        this->algorithmMPR->qfeRenderPolygonLine3D(volumeBounding, this->planeProbeParallel[i]);        
      }
      else  
      { 
        this->algorithmMPR->qfeRenderPolygon3D(volumeBounding, volumeTexture, this->planeProbeParallel[i], colormap);          
      }
    }      
  }

  // Clean up
  glDisable(GL_STENCIL_TEST);
  glDisable(GL_BLEND);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeRenderVisualMPRP(qfeVisualReformat *visual, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *volumeBounding;  
  qfeVolume          *volumeTexture;  
  qfeColorMap        *colormap;

  qfeVisualParameter *param;    
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  qfeSelectionList    paramShapeList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  int                 paramInterp;
  int                 paramBalance;
  //int                 paramShape;
  int                 paramScale;
  qfeFloat            paramRange[2];
  qfeFloat            paramRange2[2];
  bool                paramShowPlane;
  bool                paramProbeVisible;  
  double              paramBrightness;
  double              paramContrast;
  int                 current;
  qfeColorRGB         color;
  qfeFloat            tt;
 

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);  

  // Get visual parameters
  visual->qfeGetVisualParameter("MPRP visible", &param);  
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowPlane);    

  visual->qfeGetVisualParameter("MPRP data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("MPRP data vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("MPRP data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("MPRP data interpolation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInterpList);  
  paramInterp = paramInterpList.active;

  /*
  visual->qfeGetVisualParameter("MPRP shape", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShapeList);  
  paramShape = paramInterpList.active;
  */

  visual->qfeGetVisualParameter("MPRP scale (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramScale);  

  visual->qfeGetVisualParameter("MPRP mix (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBalance);  
  

  visual->qfeGetVisualParameter("MPRP brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);  

  visual->qfeGetVisualParameter("MPRP contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);  
  
  if(this->visProbe3D != NULL)
  {
    this->visProbe3D->qfeGetVisualParameter("Probe visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramProbeVisible);  
  }
  else paramProbeVisible = false;
  
  // Process parameters
  this->qfeGetDataSet((qfeDataTypeIndex)dataTypePCAP, visual, current, &volumeBounding, paramRange[0], paramRange[1]); // PCA-P
  this->qfeGetDataSet((qfeDataTypeIndex)paramData,    visual, current, &volumeTexture,  paramRange[0], paramRange[1]); // Selected volume 

  study->qfeGetStudyRangePCAP(paramRange2[0], paramRange2[1]);
    
  // Determine line color
  color = this->planeColorDefault;
  tt = 0.0;

  if(this->algorithmProbe == NULL) return qfeError;

  // Update algorithm parameters  
  this->algorithmMPR->qfeSetPlaneBorderWidth(3.5*supersampling);
  this->algorithmMPR->qfeSetPlaneDataComponent(paramComp);
  this->algorithmMPR->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmMPR->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  this->algorithmMPR->qfeSetPlaneTriggerTime(tt); 
  this->algorithmMPR->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  this->algorithmMPR->qfeSetPlaneDataRange2(paramRange2[0], paramRange2[1]);
  this->algorithmMPR->qfeSetPlaneBorderColor(color);
  this->algorithmMPR->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmMPR->qfeSetPlaneContrast(paramContrast);  
  this->algorithmMPR->qfeSetPlaneBalance(paramBalance);

  int probeCount = (int)study->probe3D.size();

  if (this->probeActionState == probeClick) 
    probeCount = 0;

  // For each probe, render a probe aligned plane  
  for(int i=0; i<probeCount; i++)
  {    
    qfeFloat radius;
    qfePoint originPlane;
    qfePoint originBase;
    qfeFloat offset;

    // Set the plane origin and linearly interpolate the radius in the probe
    offset = 0;
    if(this->planeProbeOrthogonalOffset[i] >= 0)
    {
      if(this->planeProbeOrthogonalOffset[i] >=  study->probe3D[i].length)
        offset      = study->probe3D[i].length;
      else
        offset      = this->planeProbeOrthogonalOffset[i];
    }

    originBase  = study->probe3D[i].origin - 0.5*study->probe3D[i].length*study->probe3D[i].axes[2];
    originPlane = originBase + offset*study->probe3D[i].axes[2];    

    offset      = offset / study->probe3D[i].length;
    radius      = (offset * study->probe3D[i].topRadius) + ((1.0f-offset) * study->probe3D[i].baseRadius);
    radius      = radius * (paramScale/100.0f);

    // Update the plane position and orientation    
    this->planeProbeOrthogonal[i]->qfeSetFrameOrigin(originPlane.x, originPlane.y, originPlane.z);
    this->planeProbeOrthogonal[i]->qfeSetFrameAxes(study->probe3D[i].axes[0].x, study->probe3D[i].axes[0].y, study->probe3D[i].axes[0].z,
                                                   study->probe3D[i].axes[1].x, study->probe3D[i].axes[1].y, study->probe3D[i].axes[1].z,
                                                   study->probe3D[i].axes[2].x, study->probe3D[i].axes[2].y, study->probe3D[i].axes[2].z);
    this->planeProbeOrthogonal[i]->qfeSetFrameExtent(2.0*radius,2.0*radius);  
         
    // Render the planes
    if(paramShowPlane) 
    {       
      this->qfeEnableIntersectBuffer();
      this->algorithmMPR->qfeRenderCircle3D(volumeBounding, volumeTexture, this->planeProbeOrthogonal[i], colormap);            
      this->qfeDisableIntersectBuffer();

      this->algorithmMPR->qfeRenderCircle3D(volumeBounding, volumeTexture, this->planeProbeOrthogonal[i], colormap);            
    }      
  }
  
  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeRenderVisualParticleTrace(qfeVisualFlowParticleTrace *visual, int supersampling)
{
  qfeStudy       *study;
  qfeLightSource *light;
  qfeColorMap    *colormap;
  qfeFloat        current;
  qfeDisplay     *display;

  qfeSeeds     seeds;
  int          seedsCount;

  qfeVisualParameter *param;
  qfeSelectionList    paramInterpolationList;
  qfeSelectionList    paramIntegrationList;
  qfeSelectionList    paramParticleStyle;
  qfeSelectionList    paramParticleShading;
  qfeSelectionList    paramParticleTrail;
  qfeSelectionList    paramParticleColor;
  bool                paramParticleContour;
  int                 paramCount;
  int                 paramSize;
  int                 paramSpeed;
  int                 paramLifeTime;  
  int                 paramColorScale;
  int                 paramIntegrationType;
  int                 paramIntegrationSteps;
  bool                paramVisible;
  double              paramTrailLifeTime;  
  
  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);  
  this->scene->qfeGetSceneDisplay(&display);
  this->scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("Particles visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("Particles style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramParticleStyle);

  visual->qfeGetVisualParameter("Particles shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramParticleShading); 

  visual->qfeGetVisualParameter("Particles color", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramParticleColor); 

  visual->qfeGetVisualParameter("Particles trail", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramParticleTrail); 

  visual->qfeGetVisualParameter("Particles contour", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramParticleContour); 

  visual->qfeGetVisualParameter("Particles count", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCount);  

  visual->qfeGetVisualParameter("Particles size (mm)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSize);  

  visual->qfeGetVisualParameter("Particles lifetime", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLifeTime);  

  visual->qfeGetVisualParameter("Particles color scale (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramColorScale);  

  visual->qfeGetVisualParameter("Particles speed (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSpeed);  

  visual->qfeGetVisualParameter("Particles integration", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramIntegrationList);  
  paramIntegrationType = paramIntegrationList.active;

  visual->qfeGetVisualParameter("Particles steps/sample", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramIntegrationSteps); 

  visual->qfeGetVisualParameter("Particles trail lifetime", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTrailLifeTime);  

  seedsCount = paramCount;
  
  if(paramVisible != true) return qfeError;  
  
  // Set particle trace parameters
  qfeFloat phaseDuration, traceDuration;
  int      traceDirection;
  study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);
  
  this->algorithmParticleTrace->qfeSetTraceSteps(paramIntegrationSteps);   
  this->algorithmParticleTrace->qfeSetTraceScheme((qfeFlowParticleTrace::qfeFlowParticlesIntegrationScheme)paramIntegrationType);
  this->algorithmParticleTrace->qfeSetTraceSpeed(paramSpeed);
  this->algorithmParticleTrace->qfeSetTraceLifeTime(paramLifeTime);
  this->algorithmParticleTrace->qfeSetPhaseDuration(phaseDuration);
  this->algorithmParticleTrace->qfeSetParticleStyle((qfeFlowParticleTrace::qfeFlowParticlesStyle)paramParticleStyle.active);
  this->algorithmParticleTrace->qfeSetParticleColor((qfeFlowParticleTrace::qfeFlowParticlesColor)paramParticleColor.active);    
  this->algorithmParticleTrace->qfeSetParticleShading((qfeFlowParticleTrace::qfeFlowParticlesShading)paramParticleShading.active);    
  this->algorithmParticleTrace->qfeSetParticleContour(paramParticleContour);
  this->algorithmParticleTrace->qfeSetParticleSize(paramSize);  
  this->algorithmParticleTrace->qfeSetLightSource(light);
  this->algorithmParticleTrace->qfeSetColorScale(paramColorScale);
  this->algorithmParticleTrace->qfeSetTrailStyle((qfeFlowParticleTrace::qfeFlowParticlesTrailType)paramParticleTrail.active);  
  this->algorithmParticleTrace->qfeSetTrailLifeTime(paramTrailLifeTime);
  
  // Update the probe seeding
  this->qfeUpdateProbeSeeds();
    
  // Evolve and advect the particles if the time has changed  
  if(current != this->prevTime)
  {        
    // Advect the particles    
    traceDirection =  1;
    if((current < this->prevTime) && (abs(current - this->prevTime) < 1.0))      
      traceDirection = -1;
    if((current > this->prevTime) && (abs(current - this->prevTime) > 1.0))      
      traceDirection = -1;      
      
    traceDuration = phaseDuration * abs(current - this->prevTime);
    if(traceDirection ==  1 && current == 0.0)
    {
      traceDuration = phaseDuration * abs((int)study->pcap.size() - this->prevTime);
    }
    if(traceDirection == -1 && this->prevTime == 0.0)
    {
      traceDuration = phaseDuration * abs(current - (int)study->pcap.size());
    }
     
    this->algorithmParticleTrace->qfeSetTraceDirection(traceDirection);

    this->algorithmParticleTrace->qfeAdvectParticles(study->pcap, current, traceDuration);

    // Evolve the particles
    this->algorithmParticleTrace->qfeEvolveParticles(); 
  
    // Update global time flag
    this->prevTime = current;
  }    

  // Attach the alternative color and depth textures
  // Revert FBO to original setting for further rendering
  this->qfeEnableIntersectBuffer();
  this->algorithmParticleTrace->qfeRenderParticles(colormap);  
  this->qfeDisableIntersectBuffer();

  // Render particles
  this->algorithmParticleTrace->qfeRenderParticles(colormap);
  

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeRenderVisualLineTrace(qfeVisualFlowLineTrace *visual, int supersampling)
{
  qfeStudy       *study;
  qfeColorMap    *colormap;
  qfeLightSource *light;
  qfeFloat        current;
  qfeDisplay     *display;

  int             seedsCount;

  qfeVisualParameter *param;
  qfeSelectionList    paramInterpolationList;
  qfeSelectionList    paramIntegrationList;
  qfeSelectionList    paramStyleList;
  qfeSelectionList    paramShadingList;
  qfeSelectionList    paramSeedingBehavior;
  qfeSelectionList    paramSeedingType;
  int                 paramCount;
  //int                 paramInterpolationType;
  int                 paramIntegrationType;
  int                 paramIntegrationSteps;
  bool                paramVisible;
  int                 paramPhasesDynamic;
  int                 paramPhasesStatic;
  double              paramLineWidth;
  int                 paramColorScale;
  bool                paramContour;
  bool                paramHalo;
  int                 paramMaxAngle;
  
  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  this->scene->qfeGetSceneDisplay(&display);
  this->scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("Lines visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("Lines count", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCount);  
  
  visual->qfeGetVisualParameter("Lines phases dynamic", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramPhasesDynamic);  

  visual->qfeGetVisualParameter("Lines phases static", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramPhasesStatic);   

  visual->qfeGetVisualParameter("Lines maximum angle", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramMaxAngle);   

  //visual->qfeGetVisualParameter("Lines interpolation", &param);
  //if(param != NULL) param->qfeGetVisualParameterValue(paramInterpolationList);
  //paramInterpolationType = paramInterpolationList.active;

  visual->qfeGetVisualParameter("Lines style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramStyleList);  

  visual->qfeGetVisualParameter("Lines shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShadingList);   

  visual->qfeGetVisualParameter("Lines width", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLineWidth);   

  visual->qfeGetVisualParameter("Lines contour", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContour);   

  visual->qfeGetVisualParameter("Lines halo", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramHalo);   

  visual->qfeGetVisualParameter("Lines color scale (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramColorScale);  

  visual->qfeGetVisualParameter("Lines integration", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramIntegrationList);  
  paramIntegrationType = paramIntegrationList.active;

  visual->qfeGetVisualParameter("Lines steps/sample", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramIntegrationSteps);  

  visual->qfeGetVisualParameter("Lines seeding behavior", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSeedingBehavior);  

  visual->qfeGetVisualParameter("Lines seeding type", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSeedingType); 
     
  seedsCount = paramCount;
  
  if(paramVisible != true) return qfeError;  
  
  // Set line trace parameters
  qfeFloat               phaseDuration;
  int                    traceDirection;
  qfeFlowSeedingBehavior seedingBehavior;
  
  study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);    
  traceDirection =  1;  

  switch(paramSeedingBehavior.active)
  {
  case 0  : seedingBehavior = qfeFlowLinesPathDynamic;
            break;
  case 1  : seedingBehavior = qfeFlowLinesPathStatic;
            break;
  default : seedingBehavior = qfeFlowLinesPathStatic;
  }    
 
  this->algorithmLineTrace->qfeSetStyle((qfeFlowLinesStyle)paramStyleList.active);
  this->algorithmLineTrace->qfeSetTraceSteps(paramIntegrationSteps);   
  this->algorithmLineTrace->qfeSetTraceScheme((qfeFlowLinesIntegrationScheme)paramIntegrationType);  
  this->algorithmLineTrace->qfeSetTraceDirection(traceDirection);
  this->algorithmLineTrace->qfeSetTracePhasesStatic(paramPhasesStatic);
  this->algorithmLineTrace->qfeSetTracePhasesDynamic(paramPhasesDynamic);  
  //this->algorithmLineTrace->qfeSetTraceMaximumAngle(paramMaxAngle);
  this->algorithmLineTrace->qfeSetPhase(current);
  this->algorithmLineTrace->qfeSetPhaseDuration(phaseDuration);
  this->algorithmLineTrace->qfeSetLineShading((qfeFlowLinesShading)paramShadingList.active);
  this->algorithmLineTrace->qfeSetLineWidth((float)paramLineWidth);
  this->algorithmLineTrace->qfeSetLineContour(paramContour);
  this->algorithmLineTrace->qfeSetLineHalo(paramHalo);
  this->algorithmLineTrace->qfeSetLightSource(light);
  this->algorithmLineTrace->qfeSetColorScale(paramColorScale);
  this->algorithmLineTrace->qfeSetSuperSampling(supersampling);
 
  // Update the probe seeding
  this->qfeUpdateProbeSeeds();

  // Attach the alternative color and depth textures
  // Revert FBO to original setting for further rendering
  this->qfeEnableIntersectBuffer();
  this->algorithmLineTrace->qfeRenderPathlines(NULL, seedingBehavior);  
  this->qfeDisableIntersectBuffer();

  // Render particles
  this->algorithmLineTrace->qfeRenderPathlines(colormap, seedingBehavior);    

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeRenderVisualSurfaceTrace(qfeVisualFlowSurfaceTrace *visual, int supersampling)
{
  qfeStudy       *study;
  qfeColorMap    *colormap;
  qfeLightSource *light;
  qfeFloat        current;
  qfeDisplay     *display;

  qfeVisualParameter *param;  
  qfeSelectionList    paramIntegrationList;
  qfeSelectionList    paramStyleList;
  qfeSelectionList    paramShadingList;
  int                 paramIntegrationType;  
  double              paramPhases;
  bool                paramVisible;  
  double              paramRadius;
  int                 paramCount;
  
  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  this->scene->qfeGetSceneDisplay(&display);
  this->scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("Surface visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("Surface style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramStyleList);  

  visual->qfeGetVisualParameter("Surface shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShadingList); 

  visual->qfeGetVisualParameter("Surface radius (mm)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramRadius);  

  visual->qfeGetVisualParameter("Surface tube count", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCount);  

  visual->qfeGetVisualParameter("Surface trace phases", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramPhases);  

  visual->qfeGetVisualParameter("Surface integration", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramIntegrationList);  
  paramIntegrationType = paramIntegrationList.active;

  if(paramVisible != true) return qfeError;  
  
  // Set line trace parameters
  qfeFloat phaseDuration;

  study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);    
  
  this->algorithmSurfaceTrace->qfeSetSurfaceStyle((qfeFlowSurfaceStyle)paramStyleList.active);
  this->algorithmSurfaceTrace->qfeSetSurfaceShading((qfeFlowSurfaceShading)paramShadingList.active);
  this->algorithmSurfaceTrace->qfeSetSurfaceTracePhases(paramPhases);
  this->algorithmSurfaceTrace->qfeSetVolumeSeries(study->pcap);  
  this->algorithmSurfaceTrace->qfeSetPhase(current);
  this->algorithmSurfaceTrace->qfeSetLightSource(light);

  // Re-inject on parameter change
  if(((paramRadius != this->surfaceRadius) || (paramCount != this->surfaceCount) || ((qfeFlowSurfaceStyle)paramStyleList.active != this->surfaceStyle)) && (this->probeSeedingStateSurfaces == probeSeedingInjected))
  {
    this->surfaceRadius = paramRadius;
    this->surfaceCount  = paramCount;
    this->surfaceStyle  = (qfeFlowSurfaceStyle)paramStyleList.active;
    
    this->probeSeedingStateSurfaces = probeSeedingUpdate;
  }
 
  // Update the probe seeding
  this->qfeUpdateProbeSeeds();

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Attach the alternative color and depth textures
  // Revert FBO to original setting for further rendering
  this->qfeEnableIntersectBuffer();
  this->algorithmSurfaceTrace->qfeRenderSurface(colormap);    
  this->qfeDisableIntersectBuffer();

  // Render particles
  this->algorithmSurfaceTrace->qfeRenderSurface(colormap);    

  glDisable(GL_BLEND);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeRenderVisualDVR(qfeVisualRaycast *visual, int supersampling)
{
  qfeStudy       *study;
  qfeVolume      *textureVolume;
  qfeVolume      *boundingVolume;
  qfeColorMap    *colormap;
  qfeLightSource *light;
  int             current;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;  
  qfeSelectionList    paramQualList;
  qfeSelectionList    paramStyleList;
  bool                paramVisible;
  int                 paramData;
  int                 paramComp;  
  int                 paramQuality;
  bool                paramShading;
  bool                paramShadingMultiRes;
  qfeFloat            paramRange[2];
  int                 paramMode;
  qfeRange            paramClipX, paramClipY, paramClipZ;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  this->scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("DVR visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);

  visual->qfeGetVisualParameter("DVR data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("DVR data component", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("DVR shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShading);

  visual->qfeGetVisualParameter("DVR shading style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramStyleList);

  visual->qfeGetVisualParameter("DVR shading quality", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramQualList);
  switch(paramQualList.active)
  {
  case 0  : paramQuality = 1; break;
  case 1  : paramQuality = 2; break;
  case 2  : paramQuality = 4; break;
  case 3  : paramQuality = 8; break;
  default : paramQuality = 2; break;
  }    

  visual->qfeGetVisualParameter("DVR shading multires", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShadingMultiRes);

  visual->qfeGetVisualParameter("DVR clipping x", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipX);

    visual->qfeGetVisualParameter("DVR clipping y", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipY);

    visual->qfeGetVisualParameter("DVR clipping z", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipZ);

  if(paramVisible != true) return qfeError;
  
  // Get the data set and compute the data range
  this->qfeGetDataSet((qfeDataTypeIndex)paramData, visual, current, &textureVolume, paramRange[0], paramRange[1]);
  
  if(textureVolume == NULL) return qfeError;  

  textureVolume->qfeGetVolumeType(paramMode);

  // if there is PCA-P data loaded, use this as bounding volume
  if((int)study->pcap.size() > 0)  
    boundingVolume = study->pcap.front();  
  else
    boundingVolume = textureVolume;

  // Multi-resolution dvr - check the global state
  if(paramShadingMultiRes)
    paramQuality = std::min((float)paramQuality, this->dvrQuality);

  // Update algorithm parameters
  this->algorithmDVR->qfeSetIntersectionBuffers(this->intersectTexColor, this->intersectTexDepth);
  this->algorithmDVR->qfeSetConvexClippingBuffers(this->clippingFrontTexDepth, this->clippingBackTexDepth);  
  //this->algorithmDVR->qfeSetObliqueClippingPlane(this->planeProbeParallel.front());  
  this->algorithmDVR->qfeSetOrthoClippingRanges(paramClipX, paramClipY, paramClipZ);
  this->algorithmDVR->qfeSetDataComponent(paramComp);  
  this->algorithmDVR->qfeSetDataRange(paramRange[0], paramRange[1]);
  this->algorithmDVR->qfeSetShading(paramShading);
  this->algorithmDVR->qfeSetLightSource(light);
  this->algorithmDVR->qfeSetQuality(paramQuality);
  this->algorithmDVR->qfeSetStyle(paramStyleList.active);  

  this->algorithmDVR->qfeRenderRaycasting(this->frameBufferProps[0].texColor, this->frameBufferProps[0].texDepth, boundingVolume, textureVolume, colormap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeRenderVisualMIP(qfeVisualMaximum *visual, int supersampling)
{  
  qfeStudy    *study;
  qfeVolume   *volume;
  qfeColorMap *colormap;
  int          current;
  qfeDisplay  *display;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  bool                paramVisible;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  double              paramC;
  double              paramB;
  qfeFloat            paramRange[2];
  int                 paramMode;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  this->scene->qfeGetSceneDisplay(&display);

  // Fetch the parameters
  visual->qfeGetVisualParameter("MIP visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);  
  
  visual->qfeGetVisualParameter("MIP data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("MIP data component", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("MIP data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("MIP brightness", &param);
  param->qfeGetVisualParameterValue(paramB);

  visual->qfeGetVisualParameter("MIP contrast",   &param);
  param->qfeGetVisualParameterValue(paramC);

  if(paramVisible != true) return qfeError;  

  // Get the data set and compute the data range
  this->qfeGetDataSet((qfeDataTypeIndex)paramData, visual, current, &volume, paramRange[0], paramRange[1]);
  
  if(volume == NULL) return qfeError;  

  volume->qfeGetVolumeType(paramMode);
  
  // Update algorithm parameters  
  this->algorithmMIP->qfeSetGeometryBuffers(this->intersectTexColor, this->intersectTexDepth);
  this->algorithmMIP->qfeSetDataComponent(paramComp);
  this->algorithmMIP->qfeSetDataRepresentation(paramRepr);
  this->algorithmMIP->qfeSetDataRange(paramRange[0], paramRange[1]);
  this->algorithmMIP->qfeSetContrast(paramC);
  this->algorithmMIP->qfeSetBrightness(paramB);

  this->algorithmMIP->qfeRenderMaximumIntensityProjection(this->frameBufferProps[0].texColor, volume, colormap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual, int supersampling)
{ 
  qfeStudy        *study;
  qfeVolume       *volume;
  qfeGrid         *grid;
  qfeLightSource  *light;
  qfeModel        *model;  
  vtkPolyData     *mesh; 
  unsigned int     nx, ny, nz;
  qfeFloat         ex, ey, ez, ox, oy, oz;
  int              current;

  this->scene->qfeGetSceneLightSource(&light);
  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);

  volume = study->pcap[current];
  volume->qfeGetVolumeSize(nx, ny, nz);
  volume->qfeGetVolumeGrid(&grid);

  grid->qfeGetGridExtent(ex,ey,ez);
  grid->qfeGetGridOrigin(ox,oy,oz);

  if(volume == NULL)
  {
    cout << "qfeDriverVPR::qfeRenderVisualAnatomyIllustrative - Incorrect volume data" << endl;
    return qfeError;
  }

  if(study == NULL)
  {
    cout << "qfeDriverVPR::qfeRenderVisualAnatomyIllustrative - Incorrect study" << endl;
    return qfeError;
  }

  // Compute the view vector
  qfeVector   eyeVec;
  qfeMatrix4f mvInv;
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);

  qfeMatrix4f P2V, V2P;
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume); 
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume); 

   // Get eye vector in patient coordinates;
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;

  // Get the parameters
  qfeSelectionList    paramSurfaceShadingList;
  bool                paramSurfaceContour;
  bool                paramSurfaceClipping;

  qfeVisualParameter *param;  

  visual->qfeGetVisualParameter("Surface shading", &param);    
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceShadingList); 

  visual->qfeGetVisualParameter("Surface clipping", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceClipping); 

  visual->qfeGetVisualParameter("Surface contour", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceContour); 

  // Set visual parameters
  this->algorithmSurfaceShading->qfeSetLightSource(light);

  for(int i=0; i<(int)study->segmentation.size(); i++)
  {
    stringstream        paramLabel;   
    bool                visible;
    int                 surfaceShading;
    qfeColorRGB         colorSilhouette;
    qfeColorRGBA        colorContour;
    qfeColorRGBA        colorContourHidden;
    
    // Get the model    
    model = study->segmentation[i];
    
    // Get the parameters
    model->qfeGetModelColor(colorSilhouette);    

    paramLabel << "Model " << (i+1) << " visible";

    visual->qfeGetVisualParameter(paramLabel.str(), &param);
    if(param != NULL) param->qfeGetVisualParameterValue(visible);

    // Process the parameter structures   
    colorContour.r       = 0.3; 
    colorContour.g       = 0.3;
    colorContour.b       = 0.3;
    colorContourHidden.r = 0.7;
    colorContourHidden.g = 0.7;
    colorContourHidden.b = 0.7;

    surfaceShading       = paramSurfaceShadingList.active; 

    // Get the mesh    
    model->qfeGetModelMesh(&mesh);

    double *center;
    center = mesh->GetCenter();

    // Draw the mesh
    if(mesh != NULL && visible)
    {     
      this->algorithmSurfaceShading->qfeRenderMeshCulling(mesh, volume, colorSilhouette, paramSurfaceClipping, surfaceShading);
      
      if(paramSurfaceContour) 
      {       
        this->algorithmSurfaceContours->qfeRenderContour(mesh, volume, colorContour, 2.5*this->frameBufferProps[0].supersampling);
      }
    }    
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeRenderVisualAides(qfeVisualAides *visual, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *volume;
  int                 current;

  qfeVisualParameter *param;
  bool                paramSupportBoundingBox = true;
  bool                paramSupportAxes        = false;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  volume = study->pcap[current];

  visual->qfeGetVisualParameter("Support bounding box", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportBoundingBox); 

  visual->qfeGetVisualParameter("Support axes", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportAxes); 

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  if(paramSupportBoundingBox == true) qfeDriverAide::qfeRenderBoundingBoxDashed(volume, this->currentModelView, 0.8*supersampling);
  if(paramSupportAxes        == true) qfeDriverAide::qfeRenderVolumeAxes(volume, 10.0, 1.6*supersampling);

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeWorkflowProbeClick(qfePoint *p)
{    
  switch(this->probeClickState)
  {
    case probeSelectStart : 
      //cout << "start" << endl;
      if(this->probeActionState != probeDefault) return qfeError;
      //cout << "start ok" << endl;
      this->scene->qfeSetSceneCursor(cursorCross);
      this->probeActionState  = probeClick;
      this->probeClickState   = probeSelectFirst;
      break;
    case probeSelectFirst :        
      //cout << "first" << endl;
      this->probePoints[0]  = *p;      
      this->probeClickState = probeSelectSecond;     
      break;
    case probeSelectSecond :
      //cout << "second" << endl;
      this->probePoints[1]  = *p;      

      this->qfeAddProbe(this->probePoints[0], this->probePoints[1]);
            
      this->scene->qfeSceneUpdate();
      this->scene->qfeSetSceneCursor(cursorArrow);
      this->probeClickState  = probeSelectStart;
      this->probeActionState = probeDefault;
      break;
    case probeSelectAbort :
      //cout << "abort" << endl;
      this->scene->qfeSetSceneCursor(cursorArrow);
      this->probeClickState  = probeSelectStart;
      this->probeActionState = probeDefault;
      break;
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeWorkflowProbeStroke(qfePoint *p)
{
  qfeFloat length = 0.0;
  switch(this->probeStrokeState)
  {
    case probeSelectStart : 
      if(this->probeActionState != probeDefault) return qfeError;
      this->scene->qfeSetSceneCursor(cursorCross);
      this->probeActionState  = probeStroke;
      this->probeStrokeState  = probeSelectFirst;
      break;
    case probeSelectFirst :        
      this->probePoints[0]   = *p;      
      this->probeStrokeState = probeSelectSecond;     
      break;
    case probeSelectSecond :
      this->probePoints[1]  = *p;      

      qfeVector::qfeVectorLength(this->probePoints[1]-this->probePoints[0], length);

      if(length > 1.0)
        this->qfeAddProbe(this->probePoints[0], this->probePoints[1]);
            
      this->scene->qfeSceneUpdate();
      this->scene->qfeSetSceneCursor(cursorArrow);
      this->probeStrokeState = probeSelectStart;
      this->probeActionState = probeDefault;
      break;
    case probeSelectAbort :
      this->scene->qfeSetSceneCursor(cursorArrow);
      this->probeStrokeState  = probeSelectStart;
      this->probeActionState = probeDefault;
      break;
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeWorkflowProbeFit()
{  
  qfeStudy   *study;   
  qfeDisplay *display;
  qfePoint    p[2];
  qfeVector   eyeVec;
  qfeMatrix4f mvInv;

  if(this->algorithmOctree == NULL || this->visProbe3D == NULL) return qfeError;  

  this->scene->qfeGetSceneDisplay(&display);
  this->visProbe3D->qfeGetVisualStudy(&study);   
    
  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    //qfeStartCounter();
    this->algorithmProbe->qfeGetProbeFitLocal(study->probe3D[i], this->algorithmOctree, this->currentModelView);      
    //this->algorithmProbe->qfeGetProbeFitGlobal(study->probe3D[i], study->pcap[0], this->algorithmOctree, this->currentModelView);      
    //cout << "Probe fit time: " << qfeGetCounter() << " ms" << endl;

    this->probeInteractionState[i] = probeInteractUpdate;      
  }  

  if(this->probeSeedingStateParticles == probeSeedingInjected)
    this->probeSeedingStateParticles = probeSeedingUpdate;
  if(this->probeSeedingStateLines == probeSeedingInjected)
    this->probeSeedingStateLines = probeSeedingUpdate;  
  if(this->probeSeedingStateSurfaces == probeSeedingInjected)
    this->probeSeedingStateSurfaces = probeSeedingUpdate;

  this->qfeSetProbeAnimation(); 
  for(int i=0; i<ANIMATION_FRAME_COUNT; i++)
    this->engine->Refresh();
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeAddProbe(qfeProbe3D probe)
{
  if(this->visProbe3D == NULL) return qfeError;

  qfeStudy           *study;   
  qfeMatrix4f         P2V;
  qfePoint            originV;
  unsigned int        dims[3];
  qfeValueType        t;
  void               *v;
  qfeVector           eyeVec;
  qfeMatrix4f         mvInv;

  // Get eye vector in patient coordinates;
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;
  
  this->visProbe3D->qfeGetVisualStudy(&study); 

  // Check if the probe is positioned within the volume
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, study->pcap.front());
  study->pcap.front()->qfeGetVolumeData(t, dims[0], dims[1], dims[2], &v);

  originV = probe.origin*P2V;

  if(! ((originV.x >= 0) && (originV.x < dims[0]) &&
        (originV.y >= 0) && (originV.y < dims[1]) &&
        (originV.z >= 0) && (originV.z < dims[2])))
    return qfeError;  

  // Set a specific color
  int colIndex = min((int)study->probe3D.size(),6);
  probe.color.r = this->probeColor[colIndex].r;
  probe.color.g = this->probeColor[colIndex].g;
  probe.color.b = this->probeColor[colIndex].b;
  probe.color.a = 1.0;

  // Add and select the current probe
  study->probe3D.push_back(probe);
  this->probeShadowList.push_back(probe);

  // Set the current state
  this->probeInteractionState.push_back(probeInteractNone);

  this->qfeSetSelectedProbe(this->visProbe3D, study->probe3D.size()-1);
  
  // Probe specific planes
  this->planeProbeParallel.push_back(new qfeFrameSlice());      
  this->planeProbeParallel.back()->qfeSetFrameExtent(1,1);
  this->planeProbeParallel.back()->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeProbeParallel.back()->qfeSetFrameAxes(0.0,0.0,-1.0, 0.0,1.0,0.0, 1.0,0.0,0.0);  

  this->planeProbeOrthogonal.push_back(new qfeFrameSlice());      
  this->planeProbeOrthogonal.back()->qfeSetFrameExtent(1,1);
  this->planeProbeOrthogonal.back()->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeProbeOrthogonal.back()->qfeSetFrameAxes(0.0,0.0,-1.0, 0.0,1.0,0.0, 1.0,0.0,0.0);  
  this->planeProbeOrthogonalOffset.push_back(probe.length/2.0f);

  // Initial placement here
  //this->algorithmProbe->qfeGetProbeFitGlobal(study->probe3D.back(), study->pcap.front(), this->algorithmOctree, this->currentModelView);
  this->algorithmProbe->qfeGetProbeViewAligned(study->probe3D.back(), eyeVec); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeAddProbe(qfePoint p1, qfePoint p2)
{
  if(this->visProbe3D == NULL) return qfeError;

  //qfeStudy           *study;  
  qfeProbe3D          probe;
  qfePoint            origin;
  qfeVector           axisZ;
  qfeFloat            length;
    
  origin = p1 + 0.5*(p2-p1);
  axisZ  = p2-p1; 

  qfeVector::qfeVectorLength(p2-p1, length);
  qfeVector::qfeVectorNormalize(axisZ);

  probe.origin  = origin;
  probe.length  = length;
  probe.axes[2] = axisZ;  

  this->qfeAddProbe(probe);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeUpdateProbeSeeds()
{    
  // Update the particle trace seeds
  if((visParticleTrace != NULL) && (this->probeSeedingStateParticles == probeSeedingUpdate))
  { 
    this->qfeClearSeeds(this->visParticleTrace);
    this->qfeInjectSeeds(this->visParticleTrace);

    this->probeSeedingStateParticles = probeSeedingInjected;    
  } 

  // Update line trace seeds and generate new lines
  if((visLineTrace != NULL) && (this->probeSeedingStateLines == probeSeedingUpdate))
  { 
    //this->qfeClearSeeds(this->visLineTrace);
    this->qfeInjectSeeds(this->visLineTrace);    

    this->probeSeedingStateLines = probeSeedingInjected;   
  } 

  // Update line trace seeds and generate new lines
  if((visSurfaceTrace != NULL) && (this->probeSeedingStateSurfaces == probeSeedingUpdate))
  { 
    this->qfeClearSeeds(this->visSurfaceTrace);
    this->qfeInjectSeeds(this->visSurfaceTrace);

    this->probeSeedingStateSurfaces = probeSeedingInjected;   
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeDeleteProbe(qfeProbe3D *probe)
{
  qfeStudy *study;

  if(this->visProbe3D == NULL) return qfeError;

  this->visProbe3D->qfeGetVisualStudy(&study); 

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(probe == &study->probe3D[i])
    {
      study->probe3D.erase(study->probe3D.begin() + i);
      if(i < (int)this->probeShadowList.size()) this->probeShadowList.erase(this->probeShadowList.begin() + i);
      this->planeProbeOrthogonal.erase(this->planeProbeOrthogonal.begin() + i);
      this->planeProbeOrthogonalOffset.erase(this->planeProbeOrthogonalOffset.begin() + i);
      this->planeProbeParallel.erase(this->planeProbeParallel.begin() + i);
      if(this->visParticleTrace != NULL) this->qfeClearSeeds(this->visParticleTrace);
      if(this->visLineTrace != NULL)     this->qfeClearSeeds(this->visLineTrace);
      if(this->visSurfaceTrace != NULL)  this->qfeClearSeeds(this->visSurfaceTrace);
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeGetSelectedProbe(qfeVisualFlowProbe3D *visual, qfeProbe3D **probe)
{
  qfeStudy           *study;

  *probe = NULL;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study); 

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].selected == true)
    {
      *probe = &study->probe3D[i];

      return qfeSuccess;
    }  
  }

  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeSetSelectedProbe(qfeVisualFlowProbe3D *visual, qfeProbe3D  *probe)
{
  qfeStudy           *study;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study); 

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(&study->probe3D[i] == probe)
    {
      study->probe3D[i].selected = true;

      return qfeSuccess;
    }  
  }

  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeGetProbeIndex(qfeVisualFlowProbe3D *visual, qfeProbe3D  *probe, int &index)
{
  qfeStudy           *study;

  index = -1;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study); 

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(&study->probe3D[i] == probe)
    {
      index = i;

      return qfeSuccess;
    }  
  }

  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeGetSelectedProbe(qfeVisualFlowProbe3D *visual, int &index)
{
  qfeStudy           *study;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study); 

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].selected == true)
    {
      index = i;
      return qfeSuccess;
    }  
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeSetSelectedProbe(qfeVisualFlowProbe3D *visual, int index)
{
  qfeStudy           *study;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study); 

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(i == index) 
      study->probe3D[i].selected = true;
    else
      study->probe3D[i].selected = false;    
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// angle - angle in radians [0,2pi]
qfeReturnStatus qfeDriverVPR::qfeRotateProbe(int index, qfeFloat angle)
{
  qfeStudy           *study;

  if((this->visProbe3D) == NULL || (this->algorithmProbe == NULL)) return qfeError;

  this->visProbe3D->qfeGetVisualStudy(&study); 

  if((index < 0) || (index > (int)study->probe3D.size())) return qfeError;

  this->algorithmProbe->qfeGetProbeRotation(study->probe3D[index], angle);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeSetProbeAnimation()
{
  qfeStudy *study;

  if(this->visProbe3D == NULL) return qfeError;

  this->visProbe3D->qfeGetVisualStudy(&study);

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(this->probeInteractionState[i] == probeInteractUpdate)
    {
      // animate here
      this->probeAnimateCount = ANIMATION_FRAME_COUNT;
      this->probeInteractionState[i] = probeInteractAnimate;      
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeInjectSeeds(qfeVisualFlowParticleTrace *visual)
{  
  qfeStudy           *study;
  qfeVisualParameter *param;
  bool                visible;
  qfeSeeds            seeds;
  int                 seedsCount;
  qfeSelectionList    seedsType;  
  bool                orthoPlaneVisible = false;
  float               current = 0;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);        
  visual->qfeGetVisualActiveVolume(current);     

  visual->qfeGetVisualParameter("Particles visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(visible);    

  visual->qfeGetVisualParameter("Particles count", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(seedsCount);    

  visual->qfeGetVisualParameter("Particles seeding", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(seedsType);   

  if(!visible) return qfeSuccess;

  if(this->visMPRP != NULL)
  {
    this->visMPRP->qfeGetVisualParameter("MPRP visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(orthoPlaneVisible);   
  }    
    
  // Add seed positions for each probe
  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(this->probeSeedingStateParticles == probeSeedingInjected) 
      this->qfeClearSeeds(visual);

    if(orthoPlaneVisible && ((int)this->planeProbeOrthogonal.size() > 0)) 
    {
      switch(seedsType.active)
      {
      case (0) : qfeSeeding::qfeGetSeedsRandomUniform(this->planeProbeOrthogonal[i], seedsCount, seeds);      
                 break;             
      case (1) : qfeSeeding::qfeGetSeedsRandomPoissonDisk(this->planeProbeOrthogonal[i], seedsCount, seeds);      
                 break;
      }
    }
    else
    {
      if(study->probe3D.size() <= 0) return qfeError;

      switch(seedsType.active)
      {
        case (0) : qfeSeeding::qfeGetSeedsRandomUniform(&study->probe3D[i], seedsCount, seeds);    
                   break;             
        case (1) : qfeSeeding::qfeGetSeedsRandomPoissonDisk(&study->probe3D[i], seedsCount, seeds);          
                   break;
      }
    }

    // add the seed positions
    if((int)seeds.size() > 0)
    {    
      this->algorithmParticleTrace->qfeAddSeedPositions(seeds, current, study->probe3D[i].color);       
    }
  } 

  this->probeSeedingStateParticles = probeSeedingInjected;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeInjectFileSeeds(qfeVisualFlowParticleTrace *visual)
{  
  qfeStudy           *study;
  qfeVisualParameter *param;
  bool                visible;
  float               current = 0;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);        
  visual->qfeGetVisualActiveVolume(current);     

  if(study == NULL || study->seeds.size() <= 0) return qfeError;

  visual->qfeGetVisualParameter("Particles visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(visible);    

  if(!visible) return qfeSuccess;
    
  // Add seed positions for each probe
  if(this->probeSeedingStateParticles == probeSeedingInjected) 
      this->qfeClearSeeds(visual);

  // add the seed positions
  if((int)study->seeds.size() > 0)
  {    
    this->algorithmParticleTrace->qfeAddSeedPositions(study->seeds);       
  }

  this->probeSeedingStateParticles = probeSeedingInjected;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeClearSeeds(qfeVisualFlowParticleTrace *visual)
{
  if(this->visParticleTrace != NULL)
    this->algorithmParticleTrace->qfeClearSeedPositions();

  this->probeSeedingStateParticles = probeSeedingNone;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeInjectSeeds(qfeVisualFlowLineTrace *visual)
{  
  qfeStudy           *study;
  qfeVisualParameter *param;
  bool                visible;
  qfeSeeds            seeds;
  int                 seedsCount;
  qfeSelectionList    seedingType;
  qfeSelectionList    seedingBehavior;
  bool                orthoPlaneVisible = false;
  float               current = 0;

  if(visual == NULL) return qfeError;

  // get the study and active volume
  visual->qfeGetVisualStudy(&study);        
  visual->qfeGetVisualActiveVolume(current);    

  visual->qfeGetVisualParameter("Lines visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(visible); 

  visual->qfeGetVisualParameter("Lines count", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(seedsCount);  

  visual->qfeGetVisualParameter("Lines seeding behavior", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(seedingBehavior); 

  visual->qfeGetVisualParameter("Lines seeding type", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(seedingType); 

  if(!visible) return qfeSuccess;

  if(this->visMPRP != NULL)
  {
    this->visMPRP->qfeGetVisualParameter("MPRP visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(orthoPlaneVisible);   
  }
  
  //for(int i=0; i<(int)this->planeProbeOrthogonal.size(); i++)
  //{
  //if(this->probeSeedingStateLines == probeSeedingInjected) 
  //  this->qfeClearSeeds(visual);
  
  if(orthoPlaneVisible && ((int)this->planeProbeOrthogonal.size() > 0)) 
  {
    switch(seedingType.active)
    {
    case (0) : qfeSeeding::qfeGetSeedsRandomUniform(this->planeProbeOrthogonal.front(), seedsCount, seeds);      
               //qfeSeeding::qfeGetSeedsRandomUniform(this->planeProbeOrthogonal.front(), 4, seeds);      
               break;             
    case (1) : qfeSeeding::qfeGetSeedsRandomPoissonDisk(this->planeProbeOrthogonal.front(), seedsCount, seeds);      
               break;
    }
  }
  else
  {
    if(study->probe3D.size() <= 0) return qfeError;

    switch(seedingType.active)
    {
      case (0) : qfeSeeding::qfeGetSeedsRandomUniform(&study->probe3D.front(), seedsCount, seeds);    
                 break;             
      case (1) : qfeSeeding::qfeGetSeedsRandomPoissonDisk(&study->probe3D.front(), seedsCount, seeds);          
                 break;
    }
  }

  if((int)seeds.size() <= 0) return qfeError;

  // add the seed positions
  if(this->probeSeedingStateLines == probeSeedingUpdate) 
    this->algorithmLineTrace->qfeUpdateSeedPositions(seeds, current);      
  else
  {
    this->qfeClearSeeds(visual);
    this->algorithmLineTrace->qfeAddSeedPositions(seeds, current);      
  }

  // generate the lines
  if(seedingBehavior.active == (int)qfeFlowLinesPathDynamic)
    this->algorithmLineTrace->qfeGeneratePathlines(study->pcap, current, qfeFlowLinesPathDynamic);
  else
    this->algorithmLineTrace->qfeGeneratePathlines(study->pcap, current);

  //}
  this->probeSeedingStateLines = probeSeedingInjected;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeInjectFileSeeds(qfeVisualFlowLineTrace *visual)
{  
  qfeStudy           *study;
  qfeVisualParameter *param;
  bool                visible;
  qfeSelectionList    seedingBehavior;
  bool                orthoPlaneVisible = false;
  float               current = 0;

  if(visual == NULL) return qfeError;

  // get the study and active volume
  visual->qfeGetVisualStudy(&study);        
  visual->qfeGetVisualActiveVolume(current);    

  if(study == NULL || study->seeds.size() <= 0) return qfeError;

  visual->qfeGetVisualParameter("Lines visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(visible); 

  visual->qfeGetVisualParameter("Lines seeding behavior", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(seedingBehavior); 

  if(!visible) return qfeSuccess;

  // add the seed positions
  if((int)study->seeds.size() > 0)
  {
    this->qfeClearSeeds(visual);
    this->algorithmLineTrace->qfeAddSeedPositions(study->seeds);      
  }

  // generate the lines
  if(seedingBehavior.active == (int)qfeFlowLinesPathDynamic)
    this->algorithmLineTrace->qfeGeneratePathlines(study->pcap, current, qfeFlowLinesPathDynamic);
  else
    this->algorithmLineTrace->qfeGeneratePathlines(study->pcap, current);

  this->probeSeedingStateLines = probeSeedingInjected;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeClearSeeds(qfeVisualFlowLineTrace *visual)
{
  if(this->visLineTrace != NULL)
    this->algorithmLineTrace->qfeClearSeedPositions();

  this->probeSeedingStateLines = probeSeedingNone;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeInjectSeeds(qfeVisualFlowSurfaceTrace *visual)
{  
  qfeStudy           *study;
  qfeVisualParameter *param;
  qfeSelectionList    style;
  bool                visible;
  qfeSeeds            seeds;
  double              radius = 10.0;
  int                 count;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);        

  visual->qfeGetVisualParameter("Surface visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(visible); 

  visual->qfeGetVisualParameter("Surface style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(style); 

  visual->qfeGetVisualParameter("Surface radius (mm)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(radius); 

  visual->qfeGetVisualParameter("Surface tube count", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(count);

  if(!visible) return qfeSuccess;

  //for(int i=0; i<(int)this->planeProbeOrthogonal.size(); i++)
  //{
  if(this->probeSeedingStateSurfaces == probeSeedingInjected) 
    this->qfeClearSeeds(visual);

  if(study->probe3D.size() <= 0) return qfeError;

  qfeSeeds  s1, s2;
  qfeVector v1, v2;
  qfePoint  origin;
  qfeVector normal, x, y;
  int       itemCount = 0;
  int       seedCount = 40;

  if((int)study->probe3D.size() > 0)
  {    
    switch(style.active)
    {
    case qfeFlowSurfaceTube :       

      origin = study->probe3D.front().origin;
      normal = study->probe3D.front().axes[2];

      float currentRadius;

      s1.clear();
      seeds.clear();      

      for(int i=1; i<=count; i++)
      {
        currentRadius = i * (radius / (float)count);

        qfeSeeding::qfeGetSeedsCircle(origin, normal, currentRadius, seedCount, s1); 

        seeds.insert( seeds.end(), s1.begin(), s1.end() );

        s1.clear();

        itemCount++;
      }
      break;
    case qfeFlowSurfaceStrip :             
      origin = study->probe3D.front().origin;      
      y      = study->probe3D.front().axes[1];
      
      qfeSeeding::qfeGetSeedsLine(origin, y, radius, seedCount, seeds);      
      itemCount++;
      break;
    case qfeFlowSurfaceCross :  
      origin = study->probe3D.front().origin;
      x      = study->probe3D.front().axes[0];
      y      = study->probe3D.front().axes[1];

      qfeSeeding::qfeGetSeedsLine(origin, x, radius, seedCount, s1);
      qfeSeeding::qfeGetSeedsLine(origin, y, radius, seedCount, s2);
      seeds.insert( seeds.end(), s1.begin(), s1.end() );
      seeds.insert( seeds.end(), s2.begin(), s2.end() );
      itemCount+=2;      
      break;
    default :
      qfeSeeding::qfeGetSeedsCircle(&study->probe3D.front(), seedCount, seeds); 
      break;
    }
  }

  if((int)seeds.size() <= 0) return qfeError;

  // add the seed positions
  this->algorithmSurfaceTrace->qfeAddSeedPositions(seeds, itemCount);      
 
  //} 

  this->probeSeedingStateSurfaces = probeSeedingInjected;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeClearSeeds(qfeVisualFlowSurfaceTrace *visual)
{
  if(this->visSurfaceTrace != NULL)
    this->algorithmSurfaceTrace->qfeClearSeedPositions();

  this->probeSeedingStateSurfaces = probeSeedingNone;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeDrawPoint(qfePoint p, int supersampling)
{
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glColor3f(0.0,1.0,0.0);
  glPointSize(2.0*supersampling);
  
  glBegin(GL_POINTS);
    glVertex3f(p.x, p.y, p.z);    
  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeDrawLine(qfePoint p1, qfePoint p2, int supersampling)
{
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glColor3f(0.0,1.0,0.0);
  glLineWidth(2.0*supersampling);
  
  glBegin(GL_LINES);
    glVertex3f(p1.x, p1.y, p1.z);    
    glVertex3f(p2.x, p2.y, p2.z);    
  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeCreateTextures(qfeViewport *vp, int supersampling)
{
  if(supersampling <= 0) return qfeError;

  glGenTextures(1, &this->intersectTexColor);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexColor);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F,
    vp->width*supersampling, vp->height*supersampling, 0, GL_BGRA, GL_FLOAT, NULL);

  glGenTextures(1, &this->intersectTexDepth);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 
     vp->width*supersampling, vp->height*supersampling, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0);  

  glGenTextures(1, &this->clippingTexColor);
  glBindTexture(GL_TEXTURE_2D, this->clippingTexColor);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F,
    vp->width*supersampling, vp->height*supersampling, 0, GL_BGRA, GL_FLOAT, NULL);

  glGenTextures(1, &this->clippingFrontTexDepth);
  glBindTexture(GL_TEXTURE_2D, this->clippingFrontTexDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 
     vp->width*supersampling, vp->height*supersampling, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0);  
  
  glGenTextures(1, &this->clippingBackTexDepth);
  glBindTexture(GL_TEXTURE_2D, this->clippingBackTexDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 
     vp->width*supersampling, vp->height*supersampling, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeDeleteTextures()
{
  if (glIsTexture(this->intersectTexColor))     glDeleteTextures(1, (GLuint *)&this->intersectTexColor);
  if (glIsTexture(this->intersectTexDepth))     glDeleteTextures(1, (GLuint *)&this->intersectTexDepth);
  if (glIsTexture(this->clippingTexColor))      glDeleteTextures(1, (GLuint *)&this->clippingTexColor);
  if (glIsTexture(this->clippingFrontTexDepth)) glDeleteTextures(1, (GLuint *)&this->clippingFrontTexDepth);
  if (glIsTexture(this->clippingBackTexDepth))  glDeleteTextures(1, (GLuint *)&this->clippingBackTexDepth);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeBindTextures()
{
  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexColor);

  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexDepth);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeEnableIntersectBuffer()
{
  this->qfeAttachFBO(this->frameBuffer, this->intersectTexColor, this->intersectTexColor, this->intersectTexDepth);
  this->qfeEnableFBO(this->frameBuffer);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeDisableIntersectBuffer()
{
  this->qfeResetFBO();
  this->qfeEnableFBO(this->frameBuffer);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeEnableClippingBuffer(GLuint depthBuffer)
{
  this->qfeAttachFBO(this->frameBuffer, this->clippingTexColor, this->clippingTexColor, depthBuffer);
  this->qfeEnableFBO(this->frameBuffer);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeDisableClippingBuffer()
{
  this->qfeResetFBO();
  this->qfeEnableFBO(this->frameBuffer);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeGetDataSet(qfeDataTypeIndex index, qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu)
{
  qfeStudy           *study;
  int                 current;

  *volume = NULL;
  vl = vu = 0.0;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(dataTypePCAP) : // PCA-P
    if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time))
    {
      *volume = study->pcap[time];
      study->qfeGetStudyRangePCAP(vl, vu);
    }
    break;
  case(dataTypePCAM) : // PCA-M
    if(((int)study->pcam.size() > 0) && ((int)study->pcam.size() > time))
    {
      *volume = study->pcam[time];
      study->qfeGetStudyRangePCAM(vl, vu);
    }
    break;
  case(dataTypeSSFP) : // SSFP
    if((int)study->ssfp3D.size() > 0)
    {
      // If there is also pcap data loaded, check for the trigger time and load
      // accompanying ssfp data at the right time slot
      if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time))
      {
        qfeFloat att, ptt;
        qfeFloat distance = 9000;
        (int)study->pcap[time]->qfeGetVolumeTriggerTime(ptt);
        for(int i=0; i<(int)study->ssfp3D.size(); i++)
        {
          (int)study->ssfp3D[i]->qfeGetVolumeTriggerTime(att);
          if(abs(att-ptt) < distance)
          {
            distance = abs(att-ptt);
            *volume = study->ssfp3D[i];
          }
        }
      }
      else
        *volume = study->ssfp3D[0];

      study->qfeGetStudyRangeSSFP(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(dataTypeTMIP) : // T-MIP
    if(study->tmip != NULL)
    {
      *volume = study->tmip;
      study->tmip->qfeGetVolumeValueDomain(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(dataTypeFFE) : // Flow anatomy FFE
    if(((int)study->ffe.size() > 0) && ((int)study->ffe.size() > time))
    {
      *volume = study->ffe[time];
      study->qfeGetStudyRangeFFE(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeGetDataSlice(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice)
{
  qfeMatrix4f P2V, V2P;
  qfeVector   axisX, axisY, axisZ, axisXabs, axisYabs, axisZabs, x, y, z;
  qfePoint    o;
  qfeGrid    *grid;

  if(volume == NULL) return qfeError;

  // Get the patient to voxel transformation
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);

  // Set the plane normal direction in patient coordinates
  switch(dir)
  {
  case qfePlaneAxial       : axisX.qfeSetVectorElements(1.0,0.0,0.0);
                             axisY.qfeSetVectorElements(0.0,1.0,0.0);
                             axisZ.qfeSetVectorElements(0.0,0.0,1.0);                              
                             break;
  case qfePlaneSagittal    : axisX.qfeSetVectorElements(0.0,0.0,1.0);
                             axisY.qfeSetVectorElements(0.0,1.0,0.0);
                             axisZ.qfeSetVectorElements(1.0,0.0,0.0); 
                             break;
  case qfePlaneCoronal     : axisX.qfeSetVectorElements(1.0,0.0,0.0);
                             axisY.qfeSetVectorElements(0.0,0.0,1.0);
                             axisZ.qfeSetVectorElements(0.0,1.0,0.0); 
                             break;
  }  

  // Convert the axis to voxel coordinates and find dominant direction
  axisX = axisX*P2V;
  axisY = axisY*P2V;
  axisZ = axisZ*P2V;

  axisXabs.qfeSetVectorElements(abs(axisX.x), abs(axisX.y), abs(axisX.z));
  axisYabs.qfeSetVectorElements(abs(axisY.x), abs(axisY.y), abs(axisY.z));
  axisZabs.qfeSetVectorElements(abs(axisZ.x), abs(axisZ.y), abs(axisZ.z));

  if(axisZabs.x == max(max(axisZabs.x, axisYabs.x), axisXabs.x))
  {
    x.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
    y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
    z.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
  }
  if(axisZabs.y == max(max(axisZabs.y, axisYabs.y), axisXabs.y))
  {
    x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
    y.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
    z.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
  }
  if(axisZabs.z == max(max(axisZabs.z, axisYabs.z), axisXabs.z))
  {
    x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
    y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
    z.qfeSetVectorElements( 0.0f,  0.0f,  1.0f);    
  }

  // Convert back to patient coordinates
  x = x*V2P;
  y = y*V2P;
  z = z*V2P;

  // Get the plane origin
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridOrigin(o.x, o.y, o.z);

  // Build the plane
  (*slice)->qfeSetFrameExtent(1,1);
  (*slice)->qfeSetFrameOrigin(o.x, o.y, o.z);
  (*slice)->qfeSetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverVPR::qfeGetSelectedObject(qfeColorRGB color, int &id, int &nr)
{
  id = -1;
  nr =  0;

  if((color.b >= SELECT_PROBE-0.01f) && 
     (color.b <= SELECT_PROBE+0.01f) && 
     (color.r==color.g))
  {
    id = qfeObjectProbe;
    nr = (int)ceil(color.r*255.0f);    
  }
  if((color.b >= SELECT_PROBE_HANDLE-0.01f) && 
     (color.b <= SELECT_PROBE_HANDLE+0.01f) && 
     (color.r==color.g))
  {
    id = qfeObjectProbeHandle;
    nr = (int)ceil(color.r*255.0f);    
  }
  if((color.r >= SELECT_PLANE-0.01f) && 
     (color.r <= SELECT_PLANE+0.01f) && 
     (color.g >= SELECT_PLANE-0.01f) && 
     (color.g <= SELECT_PLANE+0.01f) &&
     (color.b >= SELECT_PLANE-0.01f) && 
     (color.b <= SELECT_PLANE+0.01f))
  {
    id = qfeObjectPlaneView;
    nr = 0;
  }
  if((color.b >= SELECT_PLANE_PROBE-0.01f) && 
     (color.b <= SELECT_PLANE_PROBE+0.01f) && 
     (color.r==color.g))
  {
    id = qfeObjectProbeOrtho;
    nr = (int)ceil(color.r*255.0f);
  }
  if((color.b >= SELECT_PLANE_ORTHO-0.01f) &&
     (color.b <= SELECT_PLANE_ORTHO+0.01f) &&
     (color.r==color.g))
  {
    id = qfeObjectPlaneOrtho;
    nr = (int)ceil(color.r*255.0f);
  } 
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{ 
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;  
  qfeMatrix4f  P2V, V2T, T2P;
  
  if(volume == NULL) return;
  
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin + (mm*normal);
  else
    origin = origin - (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  originTex.x = std::max(originTex.x, 0.0f);
  originTex.x = std::min(originTex.x, 1.0f);
  originTex.y = std::max(originTex.y, 0.0f);
  originTex.y = std::min(originTex.y, 1.0f);
  originTex.z = std::max(originTex.z, 0.0f);
  originTex.z = std::min(originTex.z, 1.0f);  

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;  
  qfeMatrix4f  P2V, V2T, T2P;

  if(volume == NULL) return;
  
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin - (mm*normal);
  else
    origin = origin + (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  originTex.x = std::max(originTex.x, 0.0f);
  originTex.x = std::min(originTex.x, 1.0f);
  originTex.y = std::max(originTex.y, 0.0f);
  originTex.y = std::min(originTex.y, 1.0f);
  originTex.z = std::max(originTex.z, 0.0f);
  originTex.z = std::min(originTex.z, 1.0f);  

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeResetPlane()
{
  qfeStudy *study;
  qfeGrid  *grid;
  qfePoint  origin;

  if(this->visMPR == NULL) return;

  this->visMPR->qfeGetVisualStudy(&study);

  study->pcap.front()->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeOnAction(void *caller, int t)
{
  qfeDriverVPR *self = reinterpret_cast<qfeDriverVPR *>(caller);
  qfeStudy *study;

  switch(t)
  {
  case qfeVisualFlowProbe3D::actionNewProbeClick  :   
         self->probeClickState  = probeSelectStart;
         self->probeActionState = probeDefault;
         self->qfeWorkflowProbeClick(NULL);         
         break;
  //case qfeVisualFlowProbe3D::actionNewProbeStroke :          
  //       self->probeStrokeState = probeSelectStart;
  //       self->probeActionState = probeDefault;
  //       self->qfeWorkflowProbeStroke(NULL);
  //       break;
  case qfeVisualFlowProbe3D::actionProbeFit :          
         self->qfeWorkflowProbeFit();
         break;
  case qfeVisualFlowProbe3D::actionProbeUndo :
         if(self->visProbe3D == NULL) return;
         self->visProbe3D->qfeGetVisualStudy(&study);
         for(int i=0; i<(int)study->probe3D.size(); i++)
         {
           if(study->probe3D[i].selected)
             self->algorithmProbe->qfeGetProbeFitUndo(study->probe3D[i]);
         }         
         break;
  case qfeVisualFlowParticleTrace::actionInjectSeeds :
         self->qfeInjectSeeds(self->visParticleTrace);
         self->qfeInjectSeeds(self->visLineTrace);
         self->qfeInjectSeeds(self->visSurfaceTrace);
         break;
  case qfeVisualFlowParticleTrace::actionInjectSeedsFile :         
         self->qfeInjectFileSeeds(self->visParticleTrace);
         self->qfeInjectFileSeeds(self->visLineTrace);
         break;
  case qfeVisualFlowParticleTrace::actionClearSeeds :
         self->qfeClearSeeds(self->visParticleTrace);
         self->qfeClearSeeds(self->visLineTrace);
         self->qfeClearSeeds(self->visSurfaceTrace);
         break;
  case qfeVisualReformatOblique::actionResetMPR:
         self->qfeResetPlane();
         break;
  }  

  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeOnResize(void *caller)
{
  qfeDriverVPR *self = reinterpret_cast<qfeDriverVPR *>(caller);
  qfeViewport  *vp;

  qfeDriver::qfeDriverResize(caller);

  self->engine->GetViewport3D(&vp);

  // Refresh size of local geometry texturs
  self->qfeUnbindTextures();
  self->qfeDeleteTextures();
  self->qfeCreateTextures(vp, self->frameBufferProps[0].supersampling);
  self->qfeUnbindTextures();

  // Refresh size of the MIP textures
  self->algorithmMIP->qfeSetViewportSize(vp->width, vp->height, self->frameBufferProps[0].supersampling);
  self->algorithmDVR->qfeSetViewportSize(vp->width, vp->height, self->frameBufferProps[0].supersampling);
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v)
{
  qfeDriverVPR *self = reinterpret_cast<qfeDriverVPR *>(caller);

  qfeStudy    *study;
  qfeViewport *vp3D;
  int          objId, objNr;
  qfeColorRGB  color;
  qfeFloat     depth;
  qfePoint     windowCoord, patientCoord, visibleCoord;
  qfeFloat     delta;  

  qfeVector   eyeVec;
  qfeMatrix4f mvInv;
  qfeMatrix4f::qfeGetMatrixInverse(self->currentModelView, mvInv);

  // Initialize
  delta = 1.0;
  objId = -1;
  objNr =  0;

  // Get eye vector in patient coordinates;
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;
  
  self->engine->GetViewport3D(&vp3D);
  self->qfeReadSelect(x-vp3D->origin[0], y-vp3D->origin[1], color);
  self->qfeReadDepth(x-vp3D->origin[0], y-vp3D->origin[1], depth);
  
  //cout << "viewport: " << vp3D.origin[0] << ", " << vp3D.origin[1] << ", " << vp3D.width << ", " << vp3D.height << endl;
  //cout << "color:    " << color.r << ", " << color.g << ", " << color.b << endl;
  //cout << "position: " << x << ", " << y << ", " << depth << endl << endl;
  
  windowCoord.qfeSetPointElements(x, y, depth);
  qfeTransform::qfeGetCoordinateUnproject(windowCoord, patientCoord, self->currentModelView, self->currentProjection, vp3D);

  visibleCoord = patientCoord - delta*eyeVec;

  self->visProbe3D->qfeGetVisualStudy(&study);
  
  // Reset selections
  if(self->engine->GetInteractionControlKey())
  {
    switch(self->probeActionState)
    {
      case probeClick   : 
        self->qfeWorkflowProbeClick(&patientCoord);        
        break;
      case probeStroke   : 
        self->qfeWorkflowProbeStroke(&patientCoord);        
        break;
    }
  }
  else
  {    
    // Reset selections    
    self->planeSelected = qfePlaneNone;

    // Select objects
    self->qfeGetSelectedObject(color, objId, objNr);

    // Prepare dragging operation
    qfePoint displacementCoord;
    displacementCoord.qfeSetPointElements(x, y, 0.0);
    qfeTransform::qfeGetCoordinateUnproject(displacementCoord, self->prevPoint, self->currentModelView, self->currentProjection, vp3D);
   
    // Handle mouse left click
    switch(objId)
    {
      case qfeObjectProbe     : //cout << "probe" << endl;   
             if((objNr < 0) || (objNr > (int)self->probeInteractionState.size())) break;
             
             if(self->probeInteractionState[objNr] == probeInteractNone ||
                self->probeInteractionState[objNr] == probeInteractUpdate)
             {
               self->qfeSetSelectedProbe(self->visProbe3D, objNr);
               self->probeInteractionState[objNr] = probeInteractTranslate;    
               self->qfeMouseMoveOffset(&study->probe3D[objNr], self->prevPoint, eyeVec, self->probeClickOffset);
             }             
             break;
      case qfeObjectProbeHandle :
             if((objNr < 0) || (objNr > (int)self->probeInteractionState.size())) break;

             if(self->probeInteractionState[objNr] == probeInteractNone ||
                self->probeInteractionState[objNr] == probeInteractUpdate)
             {
               self->qfeSetSelectedProbe(self->visProbe3D, objNr);             
               self->probeInteractionState[objNr] = probeInteractRotate;                              
             }             
             break;
      case qfeObjectPlaneOrtho :
             if((objNr < 1) || (objNr > 3)) break;
             self->planeSelected = objNr;             
             self->engine->CallInteractionParentLeftButtonDown();
             break;           
      case qfeObjectProbeOrtho :             
             if((objNr < 0) || (objNr > (int)self->probeInteractionState.size())) break;
             self->planeProbeOrthogonalSelected = objNr;
             break;
      default :           
             // Default camera interaction
             self->engine->CallInteractionParentLeftButtonDown();
             self->qfeSetProbeAnimation();          
    }   
  }
  
  // Deal with the volume rendering - multiresolution
  self->dvrQuality = 1.0;
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v)
{
  qfeDriverVPR *self = reinterpret_cast<qfeDriverVPR *>(caller);

  qfeViewport *vp3D;
  qfeFloat     depth;
  qfePoint     windowCoord, patientCoord;  
  
  self->qfeReadDepth(x, y, depth);
  self->engine->GetViewport3D(&vp3D);

  windowCoord.qfeSetPointElements(x, y, depth);
  qfeTransform::qfeGetCoordinateUnproject(windowCoord, patientCoord, self->currentModelView, self->currentProjection, vp3D);

  // Reset selections
  self->planeProbeOrthogonalSelected = -1;
  self->probeClickOffset.qfeSetVectorElements(0.0,0.0,0.0);

  // Update workflows
  switch(self->probeActionState)   
  {
    case probeStroke   : 
      if(self->probeStrokeState == probeSelectSecond) 
        self->qfeWorkflowProbeStroke(&patientCoord);        
      //else
      //  self->probeStrokeState = probeSelectStart;
      break;
  }

  // Update probe interaction states
  for(int i=0; i<(int)self->probeInteractionState.size(); i++)
  {
    if(self->probeInteractionState[i] != probeInteractNone)
    {      
      self->probeInteractionState[i] = probeInteractUpdate;

      if(self->probeSeedingStateParticles == probeSeedingInjected)      
        self->probeSeedingStateParticles = probeSeedingUpdate;      
      if(self->probeSeedingStateLines     == probeSeedingInjected)      
        self->probeSeedingStateLines     = probeSeedingUpdate;      
      if(self->probeSeedingStateSurfaces  == probeSeedingInjected)      
        self->probeSeedingStateSurfaces  = probeSeedingUpdate;      
    }
  }

  // Deal with the volume rendering - multiresolution
  self->dvrQuality = 4.0;

  self->engine->Refresh();
  return;
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeOnMouseMove(void *caller, int x, int y, int v)
{
  qfeDriverVPR *self = reinterpret_cast<qfeDriverVPR *>(caller);
  
  qfeStudy    *study;
  qfeViewport *vp3D;
  qfePoint     windowCoord, patientCoord;
  qfeVector    probeDisplacement; 
  qfeFloat     depth;
  
  qfeVector   eyeVec;
  qfeMatrix4f mvInv;
  qfeMatrix4f::qfeGetMatrixInverse(self->currentModelView, mvInv);

  // Get eye vector in patient coordinates;
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;

  qfeVisualParameter *param;
  qfeSelectionList    paramInteractionList;  

  self->qfeReadDepth(x, y, depth);
  self->engine->GetViewport3D(&vp3D);
    
  if(self->visProbe3D == NULL) return;

  self->visProbe3D->qfeGetVisualStudy(&study);
  self->visProbe3D->qfeGetVisualParameter("Probe interaction", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInteractionList);     

  windowCoord.qfeSetPointElements(x, y, 0.0);
  qfeTransform::qfeGetCoordinateUnproject(windowCoord, self->nextPoint, self->currentModelView, self->currentProjection, vp3D);
  
  // Deal with workflow
  if(self->probeActionState == probeStroke)
  {
    // Update the second probe point
    windowCoord.qfeSetPointElements(x, y, depth);
    qfeTransform::qfeGetCoordinateUnproject(windowCoord, patientCoord, self->currentModelView, self->currentProjection, vp3D);
    self->probePoints[1] = patientCoord;
  }

  // Deal with probe interaction
  for(int i=0; i<(int)self->probeInteractionState.size(); i++)
  {
    if(self->probeInteractionState[i] == probeInteractTranslate)
    {
       // Update the origin
       probeDisplacement   = self->nextPoint - self->prevPoint;       
       probeDisplacement.w = 0.0f;

       if(paramInteractionList.active == 1)
         self->qfeMouseMoveProbe3DOF(&study->probe3D[i], probeDisplacement);
       else         
         self->qfeMouseMoveProbe2DOF(&study->probe3D[i], study->pcap.front(), self->nextPoint, self->probeClickOffset, eyeVec);   
       
       // Update for the next move event
       self->prevPoint     = self->nextPoint;   
    }

    if(self->probeInteractionState[i] == probeInteractRotate)
    {
      qfeFloat  angle;
      qfeVector v1, v2;  
      qfePoint  posInPlane;
      bool      inside;

      self->qfeGetIntersectionLineProbePlane(&study->probe3D[i], self->nextPoint, eyeVec, posInPlane);
      self->qfeGetInsideBox(study->pcap.front(), posInPlane, inside);
    
      v1 = study->probe3D[i].axes[2];
      v2 = posInPlane - study->probe3D[i].origin;

      qfeVector::qfeVectorNormalize(v1);
      qfeVector::qfeVectorNormalize(v2);

      angle = v1*v2;
      
      self->qfeRotateProbe(i, angle);

      // Update for the next move event
      self->prevPoint     = self->nextPoint; 
    }
  }

  // Deal with probe orthogonal planes
  if(self->planeProbeOrthogonalSelected >= 0)
  {
    qfeFloat offset;
    int      index = self->planeProbeOrthogonalSelected;
    self->qfeMouseMoveSlice(&study->probe3D[index], self->nextPoint, eyeVec, offset);
    self->planeProbeOrthogonalOffset[index] = offset;
  }

  // Deal with interactive pathlines and surfaces 
  for(int i=0; i<(int)self->probeInteractionState.size(); i++)
  {    
     if(self->probeInteractionState[i] == probeInteractTranslate ||
        self->probeInteractionState[i] == probeInteractRotate)     
     {
       if(self->probeSeedingStateLines == probeSeedingInjected)
         self->probeSeedingStateLines = probeSeedingUpdate;
       if(self->probeSeedingStateSurfaces == probeSeedingInjected)
         self->probeSeedingStateSurfaces = probeSeedingUpdate;
     }  
        
  }

  // Render the scene
  self->engine->Refresh();
  return;
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeOnWheelForward(void *caller, int x, int y, int v)
{
  qfeDriverVPR *self = reinterpret_cast<qfeDriverVPR *>(caller);

  qfeStudy           *study;  
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if(self->visOrtho == NULL) return;

  // Obtain the volume
  self->visOrtho->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visOrtho->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);

  if(self->planeSelected == qfePlaneX) self->qfeMovePlaneForward(study->pcap.front(), &self->planeX, scrollStep);
  if(self->planeSelected == qfePlaneY) self->qfeMovePlaneForward(study->pcap.front(), &self->planeY, scrollStep);
  if(self->planeSelected == qfePlaneZ) self->qfeMovePlaneForward(study->pcap.front(), &self->planeZ, scrollStep);

  // If there are probe selected, allow movement
  qfeFloat offset = 2.0f;
  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].selected)
    {    
      qfeVector displacement;

      displacement = -offset * study->probe3D[i].axes[0];

      self->qfeKeyMoveProbe(&study->probe3D[i], displacement);

      if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;   
      if(self->probeSeedingStateLines == probeSeedingInjected)
        self->probeSeedingStateLines = probeSeedingUpdate;
      if(self->probeSeedingStateSurfaces == probeSeedingInjected)
        self->probeSeedingStateSurfaces = probeSeedingUpdate;
    }
  }

  self->engine->Refresh();

}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeOnWheelBackward(void *caller, int x, int y, int v)
{
  qfeDriverVPR *self = reinterpret_cast<qfeDriverVPR *>(caller);

  qfeStudy           *study;  
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if(self->visOrtho == NULL) return;

  // Obtain the volume
  self->visOrtho->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visOrtho->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);

  if(self->planeSelected == qfePlaneX) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeX, scrollStep);
  if(self->planeSelected == qfePlaneY) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeY, scrollStep);
  if(self->planeSelected == qfePlaneZ) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeZ, scrollStep);

  // If there are probe selected, allow movement
  qfeFloat offset = 2.0f;
  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].selected)
    {    
      qfeVector displacement;
      
      displacement = offset * study->probe3D[i].axes[0];

      self->qfeKeyMoveProbe(&study->probe3D[i], displacement);

      if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate; 
      if(self->probeSeedingStateLines == probeSeedingInjected)
        self->probeSeedingStateLines = probeSeedingUpdate;
      if(self->probeSeedingStateSurfaces == probeSeedingInjected)
        self->probeSeedingStateSurfaces = probeSeedingUpdate;
    }
  }

  self->engine->Refresh(); 
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeOnKeyPress(void *caller, char *keysym)
{
  qfeDriverVPR *self = reinterpret_cast<qfeDriverVPR *>(caller); 

  qfeProbe3D *probe;
  qfeVector   displacement;   
  qfeFloat    offset = 2.0;  

  self->qfeGetSelectedProbe(self->visProbe3D, &probe);

  string key = keysym;

  if(key.compare("Delete") == 0)
  {
    if(probe == NULL) return;
    self->qfeDeleteProbe(probe);
    self->scene->qfeSceneUpdate();
    self->engine->Refresh();
  }
  if(key.compare("Left") == 0)
  {   
    if(probe == NULL) return;
    displacement = -offset * probe->axes[1];
    
    self->qfeKeyMoveProbe(probe, displacement);  

    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines == probeSeedingInjected)
        self->probeSeedingStateLines = probeSeedingUpdate;
    if(self->probeSeedingStateSurfaces == probeSeedingInjected)
        self->probeSeedingStateSurfaces = probeSeedingUpdate;
    
    self->engine->Refresh(); 
  }
  if(key.compare("Right") == 0)
  {       
    if(probe == NULL) return;
    displacement = offset * probe->axes[1];
    
    self->qfeKeyMoveProbe(probe, displacement);  
    
    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines == probeSeedingInjected)
        self->probeSeedingStateLines = probeSeedingUpdate;
    if(self->probeSeedingStateSurfaces == probeSeedingInjected)
        self->probeSeedingStateSurfaces = probeSeedingUpdate;

    
    self->engine->Refresh(); 
  }
  if(key.compare("Up") == 0)
  {        
    if(probe == NULL) return;
    displacement = offset * probe->axes[2];
    
    self->qfeKeyMoveProbe(probe, displacement);  
    
    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines == probeSeedingInjected)
        self->probeSeedingStateLines = probeSeedingUpdate;
    if(self->probeSeedingStateSurfaces == probeSeedingInjected)
        self->probeSeedingStateSurfaces = probeSeedingUpdate;

    self->engine->Refresh(); 
  }
  if(key.compare("Down") == 0)
  {    
    if(probe == NULL) return;
    displacement = -offset * probe->axes[2];
    
    self->qfeKeyMoveProbe(probe, displacement); 
    
    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines == probeSeedingInjected)
        self->probeSeedingStateLines = probeSeedingUpdate;
    if(self->probeSeedingStateSurfaces == probeSeedingInjected)
        self->probeSeedingStateSurfaces = probeSeedingUpdate;

    self->engine->Refresh(); 
  }
  if(key.compare("plus") == 0)
  {   
    if(probe == NULL) return;
    displacement = -offset * probe->axes[0];
    
    self->qfeKeyMoveProbe(probe, displacement); 
    
    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines == probeSeedingInjected)
        self->probeSeedingStateLines = probeSeedingUpdate;
    if(self->probeSeedingStateSurfaces == probeSeedingInjected)
        self->probeSeedingStateSurfaces = probeSeedingUpdate;

    self->engine->Refresh(); 
  }
  if(key.compare("minus") == 0)
  {
    if(probe == NULL) return;
    displacement = offset * probe->axes[0];
    
    self->qfeKeyMoveProbe(probe, displacement); 
    
    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines == probeSeedingInjected)
        self->probeSeedingStateLines = probeSeedingUpdate;
    if(self->probeSeedingStateSurfaces == probeSeedingInjected)
        self->probeSeedingStateSurfaces = probeSeedingUpdate;

    self->engine->Refresh(); 
  }
  if(key.compare("x") == 0)
  {
    if(self->planeInteractionState == planeInteractRotate)    
      self->planeInteractionState = planeInteractFixed;      
    else    
       self->planeInteractionState = planeInteractRotate;    
  }
  if(key.compare("Escape") == 0)
  {
    self->probeClickState  = probeSelectAbort;
    self->probeStrokeState = probeSelectAbort;
    self->qfeWorkflowProbeClick(NULL);
    self->qfeWorkflowProbeStroke(NULL);
  }
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeOnWiiPress(void *caller, char *wiikey)
{
  qfeDriverVPR *self = reinterpret_cast<qfeDriverVPR *>(caller); 

  string key = wiikey;

  if(key.compare("WII_PLUS") == 0)
  {
    cout << "+" << endl;        
    //self->engine->Refresh();
  }

  if(key.compare("WII_MINUS") == 0)
  {
    cout << "-" << endl;    
    //self->engine->Refresh();
  }

  if(key.compare("WII_LEFT") == 0)
  {
    cout << "left" << endl;    
    self->engine->Azimuth(5);    
    self->engine->Refresh();
  }

  if(key.compare("WII_RIGHT") == 0)
  {
    cout << "right" << endl;    
    self->engine->Azimuth(-5);    
    self->engine->Refresh();
  }

  if(key.compare("WII_UP") == 0)
  {
    cout << "up" << endl;    
    self->engine->Elevation(-5);    
    self->engine->Refresh();
  }

  if(key.compare("WII_DOWN") == 0)
  {
    cout << "down" << endl;    
    self->engine->Elevation(5);    
    self->engine->Refresh();
  }
  
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeMouseMoveProbe3DOF(qfeProbe3D *probe, qfeVector displacement)
{
  probe->origin   = probe->origin + displacement;
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeMouseMoveProbe2DOF(qfeProbe3D *probe, qfeVector displacement)
{
  qfeVector   displacementInPlane;
  qfeVector   probeNormal;

  probeNormal = probe->axes[0];
  qfeVector::qfeVectorNormalize(probeNormal);  

  displacementInPlane = displacement - (displacement*probeNormal)*probeNormal;

  probe->origin   = probe->origin + displacementInPlane;  
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeMouseMoveProbe2DOF(qfeProbe3D *probe, qfeVolume *volume, qfePoint mousePatientPos, qfeVector offset, qfeVector eye)
{
  bool inside;
  qfePoint origin;

  qfeDriverVPR::qfeGetIntersectionLineProbePlane(probe, mousePatientPos, eye, origin);
  qfeDriverVPR::qfeGetInsideBox(volume, origin, inside);

  if(inside)
    probe->origin = origin - offset;
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeMouseMoveOffset(qfeProbe3D *probe, qfePoint mousePatientPos, qfeVector eye, qfeVector &offset)
{
  qfePoint p;

  qfeDriverVPR::qfeGetIntersectionLineProbePlane(probe, mousePatientPos, eye, p);

  offset = p - probe->origin;    
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeKeyMoveProbe(qfeProbe3D *probe, qfeVector displacement)
{
  probe->origin   = probe->origin + displacement;
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeMouseMoveSlice(qfeProbe3D *probe, qfePoint mousePatientPos, qfeVector eye, qfeFloat &offset)
{
  qfePoint base;
  qfePoint mousePatientInPlane;
  qfeVector ab, ap;
 
  if(probe == NULL ) return;

  qfeDriverVPR::qfeGetIntersectionLineProbePlane(probe, mousePatientPos, eye, mousePatientInPlane);

  base   = probe->origin - 0.5f*probe->length*probe->axes[2];
  ab     = probe->axes[2];
  ap     = mousePatientInPlane - base;

  qfeVector::qfeVectorNormalize(ab);

  // Project mouse position on the probe axis
  offset  = ab*ap;  
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeGetIntersectionLineProbePlane(qfeProbe3D *probe, qfePoint p, qfeVector e, qfePoint &pos)
{
  qfeFloat  s, nom, den;
  qfePoint  o;
  qfeVector n, d;
  qfePoint  origin;

  if(probe == NULL) return;

  // ray plane intersection
  o   = probe->origin;  
  n   = probe->axes[0];
  d   = o-p;  

  qfeVector::qfeVectorNormalize(n);
  qfeVector::qfeVectorNormalize(e);

  nom = n*d;
  den = n*e;

  if(den != 0)
    s = nom / den;
  else
    s = -1;
  
  if(s >= 0)
    pos = p + s*e;
  else
    pos = p;
}

//----------------------------------------------------------------------------
void qfeDriverVPR::qfeGetInsideBox(qfeVolume *volume, qfePoint p, bool &inside)
{
  qfeMatrix4f      P2V;
  qfeGrid         *grid;
  qfePoint         pVoxel;
  unsigned int     size[3];

  if(volume == NULL) return;

  // Get transformation patient to voxel
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  // Get volume properties
  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(size[0],size[1],size[2]);

  // Transform the coordinate
  pVoxel = p * P2V;

  // Check if the point is in the bounding box
  inside = false;

  if((ceil(pVoxel.x) >=0) && (floor(pVoxel.x) <= size[0]) &&
     (ceil(pVoxel.y) >=0) && (floor(pVoxel.y) <= size[1]) &&
     (ceil(pVoxel.z) >=0) && (floor(pVoxel.z) <= size[2]))
  {    
    inside = true;
  }
  
}

