#include "qfeDriverFilter.h"

//----------------------------------------------------------------------------
qfeDriverFilter::qfeDriverFilter(qfeScene *scene) : qfeDriver(scene) {
  visualFilter = nullptr;
  currentTransferFunctionId = 0;
  previousInvertedParam = false;
  previousMagFilterParam = false;
  previousMagFilterThresholdParam = 0.09f;
  previousTMIPFilterParam = false;
  previousTMIPFilterThresholdParam = 0.09f;
  previousSdFilterMagParam = false;
  previousSdFilterVelParam = false;
  previousSdFilterBelowParam = false;
  previousSdFilterThresholdParam = 0.09f;
  previousGradFilterParam = false;
  previousVMFilterParam = false;

  engine->SetObserverResize(this, qfeOnResize);
  engine->SetObserverAction(this, qfeOnAction);
};

//----------------------------------------------------------------------------
qfeDriverFilter::~qfeDriverFilter() {
  this->qfeRenderStop();
};

//----------------------------------------------------------------------------
bool qfeDriverFilter::qfeRenderCheck() {
  int t;
  for (int i = 0; i < (int)visuals.size(); i++)   {    
    visuals[i]->qfeGetVisualType(t);
    if (t == visualFilterType)
      visualFilter = static_cast<qfeVisualFilter*>(visuals[i]);
  }
  return true;
}

//----------------------------------------------------------------------------
void qfeDriverFilter::qfeRenderStart() {
  qfeViewport *vp3D;
  engine->GetViewport3D(&vp3D);
   
  if (visualFilter) {
    qfeOnResize(this);
    qfeStudy *study = nullptr;
    visualFilter->qfeGetVisualStudy(&study);

    for (int i = 0; i < (int)study->pcam.size(); i++) {
      qfeVolume *vol = new qfeVolume(*study->pcam[i]);
      vol->qfeSetVolumeTextureId(-1);
      filteredMagnitude.push_back(vol);
    }
    for (int i = 0; i < (int)study->pcap.size(); i++) {
      qfeVolume *vol = new qfeVolume(*study->pcap[i]);
      vol->qfeSetVolumeTextureId(-1);
      filteredVelocity.push_back(vol);
    }

    // Set up magnitude and stddev filters
    algorithmSdFilter.qfeSetData(&filteredVelocity);
    if ((int)study->pcam.size() > 0) {
      algorithmMagFilter.qfeSetData(&study->pcam);
      algorithmMagFilter.qfeSetThreshold(0.09f);
      algorithmSdFilter.qfeSetData(&filteredMagnitude);
      algorithmSdFilter.qfeSetThreshold(0.3f);
      algorithmGradFilter.qfeSetData(&study->pcam);
    }
    if ((int)study->pcap.size() > 0) {
      algorithmVMFilter.qfeSetData(study->pcap.front());
    }
    // Set up tmip filter
    if (study->tmip) {
      algorithmTMIPFilter.qfeSetData(study->tmip);
      algorithmTMIPFilter.qfeSetThreshold(0.09f);
      algorithmTMIPFilter.qfeUpdate();
    }

    qfeUpdateMagnitude();
    qfeUpdateVelocity();
  }

  engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverFilter::qfeRenderWait() {
  // Initialize OpenGL state
  glDisable(GL_DITHER);
  glEnable(GL_BLEND);
  glDisable(GL_NORMALIZE);
  glDisable(GL_LIGHTING);
  glDisable(GL_COLOR_MATERIAL);
  glEnable(GL_DEPTH_TEST);

  // Initialize color map and render MIP
  qfeColorMap *colormap;
  if (visualFilter) {
    visualFilter->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();   
    qfeRenderVisualFilter(visualFilter); 
  }
};

//----------------------------------------------------------------------------
void qfeDriverFilter::qfeRenderStop() {
  for (int i = 0; i < (int)filteredMagnitude.size(); i++)
    delete filteredMagnitude[i];
  for (int i = 0; i < (int)filteredVelocity.size(); i++)
    delete filteredVelocity[i];
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFilter::qfeRenderVisualFilter(qfeVisualFilter *visual) {  
  qfeStudy    *study;
  qfeVolume   *volume;
  qfeColorMap *colormap;
  int          current;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  bool                paramVisible;
  bool                paramGradient;
  bool                paramTransparency;
  bool                paramInverted;
  bool                paramMagnitudeFilter;
  double              paramMagFilterThreshold;
  bool                paramTMIPFilter;
  double              paramTMIPFilterThreshold;
  bool                paramSdVelFilter;
  double              paramSdVelExponent = -2.f;
  bool                paramSdMagFilter;
  double              paramSdMagExponent = -2.f;
  int                 paramSdPhaseSpan;
  double              paramSdFilterThreshold;
  bool                paramGradFilter;
  bool                paramVMFilter;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  double              paramC;
  double              paramB;
  qfeFloat            paramRange[2];
  int                 paramMode;

  if (!visual)
    return qfeError;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  // Fetch the parameters
  visual->qfeGetVisualParameter("MIP visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("MIP data set", &param);
  if (param) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("MIP data component", &param);
  if (param) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("MIP data representation", &param);
  if (param) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("MIP gradient", &param);
  if (param) param->qfeGetVisualParameterValue(paramGradient);    

  visual->qfeGetVisualParameter("MIP transparency", &param);
  if (param) param->qfeGetVisualParameterValue(paramTransparency);    

  visual->qfeGetVisualParameter("MIP brightness", &param);
  param->qfeGetVisualParameterValue(paramB);

  visual->qfeGetVisualParameter("MIP contrast",   &param);
  if (param) param->qfeGetVisualParameterValue(paramC);

  visual->qfeGetVisualParameter("Invert filters", &param);
  if (param) param->qfeGetVisualParameterValue(paramInverted);
  if (paramInverted != previousInvertedParam) {
    previousInvertedParam = paramInverted;
    qfeUpdateMagnitude();
    qfeUpdateVelocity();
  }

  visual->qfeGetVisualParameter("Magnitude filter", &param);
  if (param) param->qfeGetVisualParameterValue(paramMagnitudeFilter);
  if (paramMagnitudeFilter != previousMagFilterParam) {
    previousMagFilterParam = paramMagnitudeFilter;
    qfeUpdateVelocity();
  }

  visual->qfeGetVisualParameter("Magnitude filter threshold", &param);
  if (param) param->qfeGetVisualParameterValue(paramMagFilterThreshold);
  if (paramMagFilterThreshold != previousMagFilterThresholdParam) {
    previousMagFilterThresholdParam = paramMagFilterThreshold;
    algorithmMagFilter.qfeSetThreshold(paramMagFilterThreshold);
    algorithmMagFilter.qfeUpdate();
    qfeUpdateVelocity();
  }

  visual->qfeGetVisualParameter("tMIP filter", &param);
  if (param) param->qfeGetVisualParameterValue(paramTMIPFilter);
  if (paramTMIPFilter != previousTMIPFilterParam) {
    previousTMIPFilterParam = paramTMIPFilter;
    qfeUpdateMagnitude();
  }

  visual->qfeGetVisualParameter("tMIP filter threshold", &param);
  if (param) param->qfeGetVisualParameterValue(paramTMIPFilterThreshold);
  if (paramTMIPFilterThreshold != previousTMIPFilterThresholdParam) {
    previousTMIPFilterThresholdParam = paramTMIPFilterThreshold;
    algorithmTMIPFilter.qfeSetThreshold(paramTMIPFilterThreshold);
    algorithmTMIPFilter.qfeUpdate();
    qfeUpdateMagnitude();
  }

  visual->qfeGetVisualParameter("StdDev velocity filter", &param);
  if (param) param->qfeGetVisualParameterValue(paramSdVelFilter);
  if (paramSdVelFilter != previousSdFilterVelParam) {
    previousSdFilterVelParam = paramSdVelFilter;
    qfeUpdateVelocity();
  }

  visual->qfeGetVisualParameter("StdDev velocity exponent", &param);
  static double velExponent;
  if (param) param->qfeGetVisualParameterValue(paramSdVelExponent);
  if (velExponent != paramSdVelExponent) {
    velExponent = paramSdVelExponent;
    algorithmSdFilter.qfeSetVelocityFilterExponent(velExponent);
    qfeUpdateVelocity();
  }

  visual->qfeGetVisualParameter("StdDev magnitude filter", &param);
  if (param) param->qfeGetVisualParameterValue(paramSdMagFilter);
  if (paramSdMagFilter != previousSdFilterMagParam) {
    previousSdFilterMagParam = paramSdMagFilter;
    qfeUpdateMagnitude();
  }

  visual->qfeGetVisualParameter("StdDev magnitude exponent", &param);
  static double magExponent;
  if (param) param->qfeGetVisualParameterValue(paramSdMagExponent);
  if (magExponent != paramSdMagExponent) {
    magExponent = paramSdMagExponent;
    algorithmSdFilter.qfeSetMagnitudeFilterExponent(magExponent);
    qfeUpdateMagnitude();
  }

  visual->qfeGetVisualParameter("StdDev phase span", &param);
  static int phaseSpan;
  if (param) param->qfeGetVisualParameterValue(paramSdPhaseSpan);
  if (phaseSpan != paramSdPhaseSpan) {
    phaseSpan = paramSdPhaseSpan;
    algorithmSdFilter.qfeSetPhaseSpan(phaseSpan);
    qfeUpdateMagnitude();
    qfeUpdateVelocity();
  }

  visual->qfeGetVisualParameter("StdDev threshold", &param);
  if (param) param->qfeGetVisualParameterValue(paramSdFilterThreshold);
  if (paramSdFilterThreshold != previousSdFilterThresholdParam) {
    previousSdFilterThresholdParam = paramSdFilterThreshold;
    algorithmSdFilter.qfeSetThreshold(paramSdFilterThreshold);
    algorithmSdFilter.qfeUpdate();
    qfeUpdateMagnitude();
    qfeUpdateVelocity();
  }

  visual->qfeGetVisualParameter("Gradient filter", &param);
  if (param) param->qfeGetVisualParameterValue(paramGradFilter);
  if (paramGradFilter != previousGradFilterParam) {
    previousGradFilterParam = paramGradFilter;
    qfeUpdateMagnitude();
  }

  visual->qfeGetVisualParameter("Vector median filter", &param);
  if (param) param->qfeGetVisualParameterValue(paramVMFilter);
  if (paramVMFilter != previousVMFilterParam) {
    previousVMFilterParam = paramVMFilter;
    qfeUpdateVelocity();
  }

  if (!paramVisible) return qfeError;  

  // Get the data set and compute the data range
  qfeGetDataSet(paramData, visual, current, &volume);

  switch (paramData) {
  case 0 : study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
           volume = filteredVelocity[current];
           break;
  case 1 : study->qfeGetStudyRangePCAM(paramRange[0], paramRange[1]);
           volume = filteredMagnitude[current];
           break;
  case 2 : study->qfeGetStudyRangeSSFP(paramRange[0], paramRange[1]);
           break;
  case 3 : if (study->tmip)
           study->tmip->qfeGetVolumeValueDomain(paramRange[0], paramRange[1]);
           // Non-float texture are clamped [0,1]
           //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
           //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
           break;
  case 4 : study->qfeGetStudyRangeFFE(paramRange[0], paramRange[1]);
           // Non-float texture are clamped [0,1]
           //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
           //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
           break;
  case 5:  study->qfeGetStudyRangeFTLE(paramRange[0], paramRange[1]);
           break;
  default: study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
           break;  
  }  

  if (!volume) return qfeError;  

  volume->qfeGetVolumeType(paramMode);

  // Update algorithm parameters  
  algorithmMIP.qfeSetDataComponent(paramComp);
  algorithmMIP.qfeSetDataRepresentation(paramRepr);
  algorithmMIP.qfeSetDataRange(paramRange[0], paramRange[1]);
  algorithmMIP.qfeSetGradient(paramGradient);
  algorithmMIP.qfeSetTransparency(paramTransparency);
  algorithmMIP.qfeSetContrast(paramC);
  algorithmMIP.qfeSetBrightness(paramB);

  algorithmMIP.qfeRenderMaximumIntensityProjection(this->frameBufferProps[0].texColor, volume, colormap);

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFilter::qfeGetDataSet(int index, qfeVisual *visual, int time, qfeVolume **volume) {
  qfeStudy           *study;

  *volume = NULL;
  visual->qfeGetVisualStudy(&study); 

  if (!study) return qfeError;

  switch(index)   {
  case(0) : // PCA-P
    if ((int)study->pcap.size() > 0 && (int)study->pcap.size() > time) 
      *volume = study->pcap[time];
    break;
  case(1) : // PCA-M
    if ((int)study->pcam.size() > 0 && (int)study->pcam.size() > time) 
      *volume = study->pcam[time];
    break;
  case(2) : // SSFP
    if ((int)study->ssfp3D.size() > 0) {
      // If there is also pcap data loaded, check for the trigger time and load
      // accompanying ssfp data at the right time slot 
      if ((int)study->pcap.size() > 0 && (int)study->pcap.size() > time) {
        qfeFloat att, ptt;
        qfeFloat distance = 9000;
        (int)study->pcap[time]->qfeGetVolumeTriggerTime(ptt);
        for (int i=0; i < (int)study->ssfp3D.size(); i++)         {
          (int)study->ssfp3D[i]->qfeGetVolumeTriggerTime(att);
          if(abs(att-ptt) < distance) {
            distance = abs(att-ptt);
            *volume = study->ssfp3D[i];
          }
        }
      }
      else {
        *volume = study->ssfp3D[0];
      }
    }
    break;
    case(3) : // T-MIP
      if (study->tmip) 
        *volume = study->tmip;
      break;
    case(4) : // FFE
    if ((int)study->ffe.size() > 0 && (int)study->ffe.size() > time) 
      *volume = study->ffe[time];
    break;
    case(5) : // FTLE
      if ((int)study->ftle.size() > 0 && (int)study->ftle.size() > time)
        *volume = study->ftle[time];
      break;
  }

  return qfeSuccess;
}

void qfeDriverFilter::qfeUpdateMagnitude() {
  if (filteredMagnitude.size() == 0)
    return;

  unsigned width, height, depth;
  filteredMagnitude[0]->qfeGetVolumeSize(width, height, depth);
  unsigned size = width * height * depth;
  unsigned nrFrames = (unsigned)filteredMagnitude.size();
  qfeStudy *study = nullptr;
  visualFilter->qfeGetVisualStudy(&study);

  // Reset filteredMagnitude
  for (unsigned fi = 0; fi < nrFrames; fi++) {
    delete filteredMagnitude[fi];
    filteredMagnitude[fi] = new qfeVolume(*study->pcam[fi]);
    filteredMagnitude[fi]->qfeSetVolumeTextureId(-1);
  }

  if (previousTMIPFilterParam)
    algorithmTMIPFilter.qfeApplyFilter(&filteredMagnitude, previousInvertedParam);
  if (previousGradFilterParam)
    algorithmGradFilter.qfeApplyFilter(&filteredMagnitude, previousInvertedParam);
  if (previousSdFilterMagParam)
    algorithmSdFilter.qfeApplyFilterMagnitude(&filteredMagnitude, previousInvertedParam);
  if (previousSdFilterVelParam)
    algorithmSdFilter.qfeApplyFilterVelocity(&filteredVelocity, previousInvertedParam);

  for (unsigned fi = 0; fi < nrFrames; fi++)
    filteredMagnitude[fi]->qfeUpdateVolume();
}

void qfeDriverFilter::qfeUpdateVelocity() {
  if (filteredVelocity.size() == 0)
    return;

  unsigned width, height, depth;
  filteredVelocity[0]->qfeGetVolumeSize(width, height, depth);
  unsigned size = width * height * depth;
  unsigned nrFrames = (unsigned)filteredVelocity.size();
  qfeStudy *study = nullptr;
  visualFilter->qfeGetVisualStudy(&study);

  // Reset filteredVelocity
  for (unsigned fi = 0; fi < nrFrames; fi++) {
    delete filteredVelocity[fi];
    filteredVelocity[fi] = new qfeVolume(*study->pcap[fi]);
    filteredVelocity[fi]->qfeSetVolumeTextureId(-1);
  }

  if (previousMagFilterParam)
    algorithmMagFilter.qfeApplyFilter(&filteredVelocity, previousInvertedParam);
  if (previousSdFilterVelParam)
    algorithmSdFilter.qfeApplyFilterVelocity(&filteredVelocity, previousInvertedParam);
  if (previousVMFilterParam)
    algorithmVMFilter.qfeApplyFilter(&filteredVelocity, previousInvertedParam);

  for (unsigned fi = 0; fi < nrFrames; fi++) {
    filteredVelocity[fi]->qfeUpdateVolume();
  }
}

void qfeDriverFilter::WriteFilteredData() {
  qfeImageData* magVTK = qfeImageData::New();
  qfeImageData* velVTK = qfeImageData::New();

  for (int fi = 0; fi < (int)filteredMagnitude.size(); fi++) {
    qfeConvert::qfeVolumeToVtkImageData(filteredMagnitude[fi], magVTK);
    qfeConvert::qfeVolumeToVtkImageData(filteredVelocity[fi], velVTK);
    vtkDataArray *array = velVTK->GetPointData()->GetArray("qflow velocities");
    magVTK->GetPointData()->AddArray(array);

    stringstream ss;
    ss << "filtered_" << setw(2) << setfill('0') << fi << ".vti";
    string fileName = ss.str();

    qfeVTIImageDataWriter *writer = qfeVTIImageDataWriter::New();
    writer->SetInput(magVTK);
    writer->SetFileName(fileName.c_str());
    writer->SetDataModeToBinary();
    writer->Write(); 
    writer->Delete();
  }

  magVTK->Delete();
  velVTK->Delete();
}

void qfeDriverFilter::MmToCmVel(qfeVolume *volume)
{
  qfeValueType t;
  unsigned width, height, depth;
  unsigned nrComp;
  float *data;
  volume->qfeGetVolumeData(t, width, height, depth, (void**)&data);
  volume->qfeGetVolumeComponentsPerVoxel(nrComp);

  for (unsigned i = 0; i < width*height*depth*nrComp; i++)
    data[i] /= 10.f;

  float rangeMin, rangeMax;
  volume->qfeGetVolumeValueDomain(rangeMin, rangeMax);
  rangeMin /= 10.f;
  rangeMax /= 10.f;
  volume->qfeSetVolumeValueDomain(rangeMin, rangeMax);
}

void qfeDriverFilter::ReverseZ(qfeVolume *volume)
{
  qfeValueType t;
  unsigned width, height, depth;
  short *data;
  volume->qfeGetVolumeData(t, width, height, depth, (void**)&data);

  for (unsigned z = 0; z < depth/2; z++) {
    for (unsigned y = 0; y < height; y++) {
      for (unsigned x = 0; x < width; x++) {
        unsigned index1 = z*width*height + y*width + x;
        unsigned index2 = (depth - z - 1)*width*height + y*width + x;
        short temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
      }
    }
  }
}

void qfeDriverFilter::ReverseVelX(qfeVolume *volume)
{
  qfeValueType t;
  unsigned width, height, depth;
  unsigned nrComp;
  float *data;
  volume->qfeGetVolumeData(t, width, height, depth, (void**)&data);
  volume->qfeGetVolumeComponentsPerVoxel(nrComp);

  for (unsigned z = 0; z < depth; z++) {
    for (unsigned y = 0; y < height; y++) {
      for (unsigned x = 0; x < width; x++) {
        unsigned index = (z*width*height + y*width + x)*nrComp;
        data[index] = -data[index];
      }
    }
  }
}

void qfeDriverFilter::ReverseVelY(qfeVolume *volume)
{
  qfeValueType t;
  unsigned width, height, depth;
  unsigned nrComp;
  float *data;
  volume->qfeGetVolumeData(t, width, height, depth, (void**)&data);
  volume->qfeGetVolumeComponentsPerVoxel(nrComp);

  for (unsigned z = 0; z < depth; z++) {
    for (unsigned y = 0; y < height; y++) {
      for (unsigned x = 0; x < width; x++) {
        unsigned index = (z*width*height + y*width + x)*nrComp;
        data[index+1] = -data[index+1];
      }
    }
  }
}

void qfeDriverFilter::ReverseVelZ(qfeVolume *volume)
{
  qfeValueType t;
  unsigned width, height, depth;
  unsigned nrComp;
  float *data;
  volume->qfeGetVolumeData(t, width, height, depth, (void**)&data);
  volume->qfeGetVolumeComponentsPerVoxel(nrComp);

  for (unsigned z = 0; z < depth; z++) {
    for (unsigned y = 0; y < height; y++) {
      for (unsigned x = 0; x < width; x++) {
        unsigned index = (z*width*height + y*width + x)*nrComp;
        data[index+2] = -data[index+2];
      }
    }
  }
}

void qfeDriverFilter::SwapXY(qfeVolume *volume)
{
  qfeValueType t;
  unsigned width, height, depth;
  unsigned nrComp;
  float *data;
  volume->qfeGetVolumeData(t, width, height, depth, (void**)&data);
  volume->qfeGetVolumeComponentsPerVoxel(nrComp);

  for (unsigned z = 0; z < depth; z++) {
    for (unsigned y = 0; y < height; y++) {
      for (unsigned x = 0; x < width; x++) {
        unsigned index = (z*width*height + y*width + x)*nrComp;
        float temp = data[index];
        data[index] = data[index+1];
        data[index+1] = temp;
      }
    }
  }
}

void qfeDriverFilter::SwapXZ(qfeVolume *volume)
{
  qfeValueType t;
  unsigned width, height, depth;
  unsigned nrComp;
  float *data;
  volume->qfeGetVolumeData(t, width, height, depth, (void**)&data);
  volume->qfeGetVolumeComponentsPerVoxel(nrComp);

  for (unsigned z = 0; z < depth; z++) {
    for (unsigned y = 0; y < height; y++) {
      for (unsigned x = 0; x < width; x++) {
        unsigned index = (z*width*height + y*width + x)*nrComp;
        float temp = data[index];
        data[index] = data[index+2];
        data[index+2] = temp;
      }
    }
  }
}

void qfeDriverFilter::SwapYZ(qfeVolume *volume)
{
  qfeValueType t;
  unsigned width, height, depth;
  unsigned nrComp;
  float *data;
  volume->qfeGetVolumeData(t, width, height, depth, (void**)&data);
  volume->qfeGetVolumeComponentsPerVoxel(nrComp);

  for (unsigned z = 0; z < depth; z++) {
    for (unsigned y = 0; y < height; y++) {
      for (unsigned x = 0; x < width; x++) {
        unsigned index = (z*width*height + y*width + x)*nrComp;
        float temp = data[index+2];
        data[index+2] = data[index+1];
        data[index+1] = temp;
      }
    }
  }
}

void qfeDriverFilter::qfeOnResize(void *caller) {
  qfeDriverFilter *self = reinterpret_cast<qfeDriverFilter *>(caller);
  qfeViewport *vp;
  self->engine->GetViewport3D(&vp);
  qfeDriver::qfeDriverResize(caller);
  self->algorithmMIP.qfeSetViewportSize(vp->width, vp->height, self->frameBufferProps[0].supersampling);
}

void qfeDriverFilter::qfeOnAction(void *caller, int t) {
  qfeDriverFilter *self = reinterpret_cast<qfeDriverFilter *>(caller);

  switch(t) { 
  case qfeVisualFilter::actionSaveFilteredData :         
    self->WriteFilteredData();       
    break;
  }  

  self->engine->Refresh();
}