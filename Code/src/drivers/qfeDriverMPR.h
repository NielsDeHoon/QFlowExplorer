#pragma once

#include "qflowexplorer.h"

#include "qfeDriver.h"
#include "qfeFrame.h"
#include "qfeMatrix4f.h"
#include "qfePlanarReformat.h"
#include "qfeTransform.h"

#include "vtkCamera.h"
#include "vtkMatrix4x4.h"

/**
* \file   qfeDriverMPR.h
* \author Roy van Pelt
* \class  qfeDriverMPR
* \brief  Implements a MPR driver
*
* qfeDriverMPR derives from qfeDriver, and implements
* a GPU-based Multi Planar Reformat
*/

class qfeDriverMPR : public qfeDriver
{
public:
  qfeDriverMPR(qfeScene *scene);
  ~qfeDriverMPR();

  bool qfeRenderCheck();
  void qfeRenderSelect();
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();

  enum qfePlaneSelected
  {
    qfePlaneNone,
    qfePlaneX,
    qfePlaneY,
    qfePlaneZ
  };

  enum qfePlaneDirection
  {
    qfePlaneAxial,
    qfePlaneSagittal,
    qfePlaneCoronal,
  };

private:  
  qfeVisualReformatOrtho     *visOrtho;
  qfeVisualReformatFixed     *visQflow;
  qfeVisualPlanesArrowHeads  *visArrows;
  qfeVisualAides             *visAides;
  
  qfeMatrix4f                currentModelView;

  qfePlanarReformat         *algorithmOrtho;
  qfePlanarReformat         *algorithmOblique;

  int                        selectedPlane;
  qfeFrameSlice             *planeX;
  qfeFrameSlice             *planeY;
  qfeFrameSlice             *planeZ;
  qfeFrameSlice             *planeQ;

  qfeColorRGB                colorDefault;
  qfeColorRGB                colorHighlight;
  
  qfeReturnStatus qfeRenderVisualPlanesOrtho(qfeVisualReformat *visual);
  qfeReturnStatus qfeRenderVisualPlanesOblique(qfeVisualReformat *visual);
  qfeReturnStatus qfeRenderVisualPlanesArrows(qfeVisualReformat *visual);
  qfeReturnStatus qfeRenderVisualAides(qfeVisualAides *visual);

  qfeReturnStatus qfeGetDataSetOrtho(int index, qfeVisual *visual, qfeVolume **volume);  
  qfeReturnStatus qfeGetDataSetOblique(int index, int slice, qfeVisual *visual, qfeVolume **volume);
  qfeReturnStatus qfeGetDataCountOblique(int index, qfeVisual *visual, int &count);
  qfeReturnStatus qfeGetDataPhasesOblique(int index, qfeVisual *visual, int &phases);

  qfeReturnStatus qfeGetPlaneOrtho(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice);
  qfeReturnStatus qfeGetPlaneOblique(qfeVolume *volume, qfeFrameSlice **slice);
  
  void qfeMovePlaneForward(qfeFrameSlice **slice);
  void qfeMovePlaneBackward(qfeFrameSlice **slice);
  void qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm);
  void qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm);
  
  static void qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v);
  static void qfeOnWheelForward(void *caller, int x, int y, int v);
  static void qfeOnWheelBackward(void *caller, int x, int y, int v);
};
