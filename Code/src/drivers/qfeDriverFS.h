#pragma once

#include "qflowexplorer.h"

#include "qfeDriver.h"
#include "qfeSimulation.h"

#include <math.h>

#include "qfeFrame.h"
#include "qfeMatrix4f.h"
#include "qfeModel.h"
#include "qfeRaycasting.h"
#include "qfeSimulationParticleTrace.h"
#include "qfeSimulationLinesComparison.h"
#include "qfeSimulationBoundaryComparison.h"
#include "qfeSurfaceShading.h" 
#include "qfeTransform.h"
#include "qfeFluidInteractorCollection.h"
#include "qfeCenterline.h"
#include "qfeColorMaps.h"
#include "qfe2DBasicRendering.h"


#include "qfeQuantification.h"

// TODO NHD - I typically avoid QT stuff in my backend. 
// Not a big deal though
#include <QTime>
#include <QMessageBox>
#include <QProgressDialog>

using namespace std;

/**
* \file   qfeDriverFS.h
* \author Niels de Hoon
* \class  qfeDriverFS
* \brief  Implements a FS driver
*
* qfeDriverFS derives from qfeDriver, and implements
* a driver for a FLIP based Fluid Simulation 
*/

class qfeDriverFS : public qfeDriver
{
public:
  qfeDriverFS(qfeScene *scene);
  ~qfeDriverFS();

  bool qfeRenderCheck();
  void qfeRenderSelect(){};
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();

private:  
  qfeVisualFluid			      *visFluidSim;
  qfeVisualAnatomyIllustrative	  *visAnatomy;
  qfeVisualSimParticleTrace       *visParticles;
  qfeVisualSimLinesComparison     *visLines;
  qfeVisualSimBoundaryComparison  *visBoundary;
  qfeVisualAides                  *visAides;
  qfeVisualMetrics				  *visMetrics;

  qfeFluidInteractorCollection	  fluidInteractors;
  qfeCenterline					  *centerline;

  qfeColorMaps					  colorMaps;

  bool							  source_selection_enabled;
  bool							  sink_selection_enabled;
  bool							  inspector_enabled;

  qfeSimulation					  *algorithmFluidSim;
  qfeSimulationParticleTrace      *algorithmFluidSimParticles;
  qfeSimulationLinesComparison    *algorithmFluidSimLines;
  qfeSimulationBoundaryComparison *algorithmFluidSimBoundary;
  qfeSurfaceShading               *algorithmSurfaceShading;

  vtkPolyData					  *mesh;
  bool errorWritingDone;

  bool                             simulationUpdated;  
  vector<vector<float>>			   simulationParticlePositions; 
  vector<vector<float>>	           simulationParticleVelocities; 

  int							   simulationSpacing;
  
  int                              seedCount;  
  vector< vector<float> >          randomParticlePositions;

  bool                             firstRender;

  qfeReturnStatus				   qfeRemoveFluidInteractor();
  qfeReturnStatus                  qfePreprocessFluidSimulation();
  qfeReturnStatus                  qfeInjectSeeds(qfeVisualSimLinesComparison *visual);
  qfeReturnStatus                  qfeClearSeeds(qfeVisualSimLinesComparison  *visual);
  qfeReturnStatus                  qfeInjectSeeds(qfeVisualSimBoundaryComparison *visual);
  qfeReturnStatus                  qfeClearSeeds(qfeVisualSimBoundaryComparison  *visual);

  qfeReturnStatus                  qfeRenderVisualFluidSim(qfeVisualFluid *visual);
  qfeReturnStatus                  qfeRenderVisualFluidSimParticles(qfeVisualSimParticleTrace *visual);
  qfeReturnStatus                  qfeRenderVisualFluidSimLines(qfeVisualSimLinesComparison *visual);
  qfeReturnStatus                  qfeRenderVisualFluidSimBoundary(qfeVisualSimBoundaryComparison *visual);
  qfeReturnStatus                  qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual);
  qfeReturnStatus                  qfeRenderVisualAides(qfeVisualAides *visual);  
  qfeReturnStatus				   qfeRenderVisualMetrics(qfeVisualMetrics *visual);

  qfeReturnStatus				   qfeRetrieveInspectionImage(qfeFluidInteractor *interactor, string label, float currentPhase, GLfloat* &image, unsigned int size, unsigned int colorMapId, float &range_min, float &range_max);
  qfeReturnStatus				   qfeCreateIsoSurface(Array3f *input, const float isoValue);

  void glEnable2D();
  void glDisable2D();

  float bezierWeighting(float t);

  static void qfeOnAction(void *caller, int t);
  static void qfeOnResize(void *caller);  
  static void qfeOnMouseMove(void *caller, int x, int y, int v);  
  static void qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v); 
  static void qfeOnMouseRightButtonDown(void *caller, int x, int y, int v); 
  static void qfeOnMouseRightButtonUp(void *caller, int x, int y, int v); 

  qfeReturnStatus qfePickPoint(int windowX, int windowY, qfeViewport *viewport, qfeVolume *volume, float &value, qfePoint &simPos);
  unsigned int qfeSelectInteractor(int windowX, int windowY, qfeViewport *viewport, qfeVolume *volume, qfePoint &simPos);
  qfeReturnStatus qfeGetIntersectionsBoxLine(qfeVolume *volume, qfePoint p1, qfePoint p2, qfePoint **intersections, unsigned int &count);
  qfeReturnStatus qfeGetInsideBox(qfeVolume *volume, qfePoint p, bool &inside);

  qfeReturnStatus qfeRunCoupling(int timeDirection, float stepSize, bool merge_needed);

  void qfeComputeFlux(qfeVisualFluid *visual);
  void qfePlotFlux(qfeStudy *study, qfeVolumeSeries *series, QString data_label);
  
  QList<Array3Vec3> forwardSimVelocityFields;
  QList<Array3Vec3> backwardSimVelocityFields;

  bool rightMouseDown;
  int mouse_x, mouse_y;

  qfeVector rotateVectorAroundAxis(qfeVector input, qfeVector axis, float angle);

  bool isoMeshUpToDate;
  bool isoMeshCreated;

  float currentIsoValue;
  float currentIsoPhase;
  std::string currentIsoLabel;

  qfe2DBasicRendering *q2DRenderer;

  Array3Vec3 negateVelocityField(Array3Vec3 velocityField);
};
