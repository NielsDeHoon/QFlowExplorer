#include "qfeDriverMIP.h"

//----------------------------------------------------------------------------
qfeDriverMIP::qfeDriverMIP(qfeScene *scene) : qfeDriver(scene)
{
  this->visualMIP     = NULL;

  this->currentTransferFunctionId = 0;

  this->algorithmMIP    = new qfeMaximumProjection(); 

  this->engine->SetObserverResize((void*)this, this->qfeOnResize);
  this->engine->SetInteractionMouseMove((void*)this, this->qfeOnMouseMove, false);
  this->engine->SetInteractionMouseRightButtonDown((void*)this, this->qfeOnMouseRightButtonDown, false);
};

//----------------------------------------------------------------------------
qfeDriverMIP::~qfeDriverMIP()
{
  delete this->algorithmMIP;

  this->qfeRenderStop();
};

//----------------------------------------------------------------------------
bool qfeDriverMIP::qfeRenderCheck()
{
  int t;
  for(int i=0; i<(int)this->visuals.size(); i++)
  {    
    this->visuals[i]->qfeGetVisualType(t);
    if(t == visualMaximum)
    {
      this->visualMIP = static_cast<qfeVisualMaximum *>(this->visuals[i]);
    }
  }
  return true;
}

//----------------------------------------------------------------------------
void qfeDriverMIP::qfeRenderStart()
{
  qfeViewport *vp3D;
  this->engine->GetViewport3D(&vp3D);

  if(this->visualMIP != NULL)
  {
    //this->algorithmMIP->qfeSetViewportSize(vp3D->width, vp3D->width, this->frameBufferProps[0].supersampling);
    this->qfeOnResize(this);
  }

  this->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverMIP::qfeRenderWait()
{
  // Initialize OpenGL state
  glDisable(GL_DITHER);
  glEnable(GL_BLEND);
  glDisable(GL_NORMALIZE);
  glDisable(GL_LIGHTING);
  glDisable(GL_COLOR_MATERIAL);
  glEnable(GL_DEPTH_TEST);

  // Initialize color map
  qfeColorMap *colormap;
  if(this->visualMIP != NULL) 
  {
    this->visualMIP->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();   
  }

  // Render MIP
  if(this->visualMIP != NULL)
  {    
    this->qfeRenderVisualMIP(this->visualMIP); 
  }
};

//----------------------------------------------------------------------------
void qfeDriverMIP::qfeRenderStop()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMIP::qfeRenderVisualMIP(qfeVisualMaximum *visual)
{  
  qfeStudy    *study;
  qfeVolume   *volume;
  qfeColorMap *colormap;
  int          current;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  bool                paramVisible;
  bool                paramGradient;
  bool                paramTransparency;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  double              paramC;
  double              paramB;
  qfeFloat            paramRange[2];
  int                 paramMode;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  // Fetch the parameters
  visual->qfeGetVisualParameter("MIP visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("MIP data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("MIP data component", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("MIP data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("MIP gradient", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramGradient);    

  visual->qfeGetVisualParameter("MIP transparency", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTransparency);    

  visual->qfeGetVisualParameter("MIP brightness", &param);
  param->qfeGetVisualParameterValue(paramB);

  visual->qfeGetVisualParameter("MIP contrast",   &param);
  param->qfeGetVisualParameterValue(paramC);

  if(paramVisible != true) return qfeError;  

  // Get the data set and compute the data range
  this->qfeGetDataSet(paramData, visual, current, &volume);
  
  if(volume == NULL) return qfeError;  

  switch(paramData)
  {
  case 0 : study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
           break;
  case 1 : study->qfeGetStudyRangePCAM(paramRange[0], paramRange[1]);
           break;
  case 2 : study->qfeGetStudyRangeSSFP(paramRange[0], paramRange[1]);
           break;
  case 3 : if(study->tmip != NULL)
           study->tmip->qfeGetVolumeValueDomain(paramRange[0], paramRange[1]);
           // Non-float texture are clamped [0,1]
           //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
          // paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
           break;
  case 4 : study->qfeGetStudyRangeFFE(paramRange[0], paramRange[1]);
           // Non-float texture are clamped [0,1]
           //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
           //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
           break;
  case 5:  study->qfeGetStudyRangeFTLE(paramRange[0], paramRange[1]);
           break;
  default: study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
           break;  
  }  

  volume->qfeGetVolumeType(paramMode);

  // Update algorithm parameters  
  this->algorithmMIP->qfeSetDataComponent(paramComp);
  this->algorithmMIP->qfeSetDataRepresentation(paramRepr);
  this->algorithmMIP->qfeSetDataRange(paramRange[0], paramRange[1]);
  this->algorithmMIP->qfeSetGradient(paramGradient);
  this->algorithmMIP->qfeSetTransparency(paramTransparency);
  this->algorithmMIP->qfeSetContrast(paramC);
  this->algorithmMIP->qfeSetBrightness(paramB);

  this->algorithmMIP->qfeRenderMaximumIntensityProjection(this->frameBufferProps[0].texColor, volume, colormap);

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMIP::qfeGetDataSet(int index, qfeVisual *visual, int time, qfeVolume **volume)
{
  qfeStudy           *study;

  *volume = NULL;
  visual->qfeGetVisualStudy(&study); 

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(0) : // PCA-P
    if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time)) 
      *volume = study->pcap[time];
    break;
  case(1) : // PCA-M
    if(((int)study->pcam.size() > 0) && ((int)study->pcam.size() > time)) 
      *volume = study->pcam[time];
    break;
  case(2) : // SSFP
    if((int)study->ssfp3D.size() > 0) 
    {
      // If there is also pcap data loaded, check for the trigger time and load
      // accompanying ssfp data at the right time slot 
      if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time)) 
      {
        qfeFloat att, ptt;
        qfeFloat distance = 9000;
        (int)study->pcap[time]->qfeGetVolumeTriggerTime(ptt);
        for(int i=0; i<(int)study->ssfp3D.size(); i++)
        {
          (int)study->ssfp3D[i]->qfeGetVolumeTriggerTime(att);
          if(abs(att-ptt) < distance)
          {
            distance = abs(att-ptt);
            *volume = study->ssfp3D[i];
          }
        }
      }
      else
        *volume = study->ssfp3D[0];
    }
    break;
    case(3) : // T-MIP
      if(study->tmip != NULL) 
      *volume = study->tmip;
      break;
    case(4) : // FFE
    if(((int)study->ffe.size() > 0) && ((int)study->ffe.size() > time)) 
      *volume = study->ffe[time];
    break;
    case(5) : // FTLE
      if(((int)study->ftle.size() > 0) && ((int)study->ftle.size() > time))
      {
        *volume = study->ftle[time];
      }
      break;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeDriverMIP::qfeOnResize(void *caller)
{
  qfeDriverMIP *self = reinterpret_cast<qfeDriverMIP *>(caller);

  qfeViewport *vp;

  self->engine->GetViewport3D(&vp);

  qfeDriver::qfeDriverResize(caller);

  self->algorithmMIP->qfeSetViewportSize(vp->width, vp->height, self->frameBufferProps[0].supersampling);
}

//----------------------------------------------------------------------------
void qfeDriverMIP::qfeOnMouseMove(void *caller, int x, int y, int v)
{
  qfeDriverMIP *self = reinterpret_cast<qfeDriverMIP *>(caller);

  if(self->engine->GetInteractionControlKey())
  {
    cout << "window level stuff" << x << ", " << y << endl;
  }
  else
  {
    self->engine->CallInteractionParentMouseMove();
  }
  
}

//----------------------------------------------------------------------------
void qfeDriverMIP::qfeOnMouseRightButtonDown(void *caller, int x, int y, int v)
{
  qfeDriverMIP *self = reinterpret_cast<qfeDriverMIP *>(caller);

  if(self->engine->GetInteractionControlKey())
  {
    cout << "window level stuff" << endl;
  }
  else
  {
    self->engine->CallInteractionParentRightButtonDown();
  }
  
}

//----------------------------------------------------------------------------
void qfeDriverMIP::qfeSetDynamicUniformLocations()
{
  /*
  qfeStudy           *study;
  qfeVolume          *volume;
  qfeFloat            vl, vu;  
  int                 current, m;
  double              c, b;

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;

  vl = 0.0;
  vu = 255.0;

  paramData = 0;
  paramComp = 0;
  paramRepr = 0;

  this->visualMIP->qfeGetVisualStudy(&study);
  this->visualMIP->qfeGetVisualActiveVolume(current);

  // Fetch the parameters
  this->visualMIP->qfeGetVisualParameter("MIP data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  this->visualMIP->qfeGetVisualParameter("MIP data component", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  this->visualMIP->qfeGetVisualParameter("MIP data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  this->visualMIP->qfeGetVisualParameter("MIP brightness", &param);
  param->qfeGetVisualParameterValue(b);

  this->visualMIP->qfeGetVisualParameter("MIP contrast",   &param);
  param->qfeGetVisualParameterValue(c);

  // Process parameters
  this->qfeGetDataSet(paramData, &volume);

  if(volume == NULL) return;

  if(paramData == 0)
    study->qfeGetStudyRangePCAP(vl, vu);
  else
  if(paramData == 1)
    study->qfeGetStudyRangePCAM(vl, vu);
  else
  if(paramData == 2)
    study->qfeGetStudyRangeSSFP(vl, vu);

  volume->qfeGetVolumeType(m);

  // Provide parameters to the shader
  this->shaderProgram->qfeSetUniform1i("mipDataSet"            , paramData+1);
  this->shaderProgram->qfeSetUniform1i("mipDataComponent"      , paramComp);
  this->shaderProgram->qfeSetUniform1i("mipDataRepresentation" , paramRepr);
  this->shaderProgram->qfeSetUniform2f("mipDataRange"          , vl, vu);
  this->shaderProgram->qfeSetUniform1f("stepSize"              , 1.0/250.0);
  this->shaderProgram->qfeSetUniform1f("scalarSize"            , 16.0);
  this->shaderProgram->qfeSetUniform1i("mode"                  , m);
  this->shaderProgram->qfeSetUniform1f("contrast"              , c);
  this->shaderProgram->qfeSetUniform1f("brightness"            , b);
*/
}
