#pragma once

#include "qflowexplorer.h"

#include "qfeDriver.h"
#include "qfeRayCasting.h"

using namespace std;

/**
* \file   qfeDriverDVR.h
* \author Roy van Pelt
* \class  qfeDriverDVR
* \brief  Implements a MIP driver
*
* qfeDriverDVR derives from qfeDriver, and implements
* a GPU-based Raycaster
*/

class qfeDriverDVR : public qfeDriver
{
public:
  qfeDriverDVR(qfeScene *scene);
  ~qfeDriverDVR();

  bool qfeRenderCheck();
  void qfeRenderSelect(){};
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();

private:
  qfeVisualRaycast     *visualDVR;

  qfeRayCasting        *algorithmDVR;

  GLuint                currentTransferFunctionId;
  
  float                 quality;

  GLuint texRayStart;
  GLuint texRayEnd;

  qfeReturnStatus qfeRenderVisualDVR(qfeVisualRaycast *visual);
  qfeReturnStatus qfeGetDataSet(int index, qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu);

  void qfeSetDynamicUniformLocations();

  static void qfeOnResize(void *caller);
  static void qfeOnMouseMove(void *caller, int x, int y, int v);
  static void qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v);
  static void qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v);
  static void qfeOnMouseRightButtonDown(void *caller, int x, int y, int v);

};
