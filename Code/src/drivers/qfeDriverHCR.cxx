#include "qfeDriverHCR.h"

//----------------------------------------------------------------------------
qfeDriverHCR::qfeDriverHCR(qfeScene *scene) : qfeDriver(scene)
{
  this->visCluster            = NULL;
  this->visClusterArrows      = NULL;
  this->visOrtho              = NULL;
  this->visSurface            = NULL;
  this->visDVR                = NULL;
  this->visAides              = NULL;

  this->intersectTexColor     = 0;
  this->intersectTexDepth     = 0;
  this->clippingTexColor      = 0;
  this->clippingFrontTexDepth = 0;
  this->clippingBackTexDepth  = 0;

  // Initialize plane line colors
  this->colorDefault.r        = 0.952;
  this->colorDefault.g        = 0.674;
  this->colorDefault.b        = 0.133;

  this->colorHighlight.r      = 0.356;
  this->colorHighlight.g      = 0.623;
  this->colorHighlight.b      = 0.396;

  this->dvrQuality            = 2.0;

  // Initialize the planes (in patient coordinates)
  this->planeX = new qfeFrameSlice();
  this->planeX->qfeSetFrameExtent(1,1,1);
  this->planeX->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeX->qfeSetFrameAxes(0.0,0.0,-1.0, 0.0,1.0,0.0, 1.0,0.0,0.0);

  this->planeY = new qfeFrameSlice();
  this->planeY->qfeSetFrameExtent(1,1,1);
  this->planeY->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeY->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,0.0,-1.0, 0.0,1.0,0.0);

  this->planeZ = new qfeFrameSlice();
  this->planeZ->qfeSetFrameExtent(1,1,1);
  this->planeZ->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeZ->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,-1.0);

  this->selectedPlane = qfePlaneNone;

  this->algorithmOrtho           = new qfePlanarReformat();
  this->algorithmDVR             = new qfeRayCasting();
  this->algorithmLines           = new qfeFlowLineTraceCluster();
  this->algorithmSurfaceShading  = new qfeSurfaceShading();
  this->algorithmSurfaceContours = new qfeSurfaceContours();

  this->hierarchicalClustering = new qfeHierarchicalClustering();
  this->clusterCenters.clear();

  this->engine->SetObserverAction((void*)this, this->qfeOnAction);
  this->engine->SetObserverResize((void*)this, this->qfeOnResize);
  this->engine->SetInteractionMouseLeftButtonUp((void*)this, this->qfeOnMouseLeftButtonUp);
  this->engine->SetInteractionMouseLeftButtonDown((void*)this, this->qfeOnMouseLeftButtonDown);
  this->engine->SetInteractionMouseWheelForward((void*)this, this->qfeOnWheelForward, false);
  this->engine->SetInteractionMouseWheelBackward((void*)this, this->qfeOnWheelBackward, false);
};

//----------------------------------------------------------------------------
qfeDriverHCR::~qfeDriverHCR()
{
  this->qfeRenderStop();

  delete this->planeX;
  delete this->planeY;
  delete this->planeZ;

  delete this->algorithmOrtho;
  delete this->algorithmDVR;
  delete this->algorithmLines;
  delete this->algorithmSurfaceShading;
  delete this->algorithmSurfaceContours;

  delete this->hierarchicalClustering;
  
  this->clusterCenters.clear();
};

//----------------------------------------------------------------------------
bool qfeDriverHCR::qfeRenderCheck()
{
  int t;
  for(int i=0; i<(int)this->visuals.size(); i++)
  {
    this->visuals[i]->qfeGetVisualType(t);
    
    if(t == visualFlowClustering)
    {
      this->visCluster = static_cast<qfeVisualClustering *>(this->visuals[i]);
    }
    if(t == visualFlowClusterArrows)
    {
      this->visClusterArrows = static_cast<qfeVisualClusterArrowTrace *>(this->visuals[i]);
    }
    if(t == visualReformatOrtho)
    {
      this->visOrtho = static_cast<qfeVisualReformatOrtho *>(this->visuals[i]);
    }
    if(t == visualAnatomyIllustrative)
    {
      this->visSurface = static_cast<qfeVisualAnatomyIllustrative *>(this->visuals[i]);
    }
    if(t == visualRaycast)
    {
      this->visDVR = static_cast<qfeVisualRaycast *>(this->visuals[i]);
    }
    if(t == visualAides)
    {
      this->visAides = static_cast<qfeVisualAides *>(this->visuals[i]);
    }
  }
  return true;
}

//----------------------------------------------------------------------------
void qfeDriverHCR::qfeRenderSelect()
{
  qfeVolume    *volume;

  qfeVisualParameter *param;

  // Render ortho planes
  if(this->visOrtho != NULL)
  {
    qfeSelectionList    paramDataList;
    int                 paramData;
    bool                paramShowX, paramShowY, paramShowZ;
    qfeFloat            vu, vl;
    qfeColorRGB         color;

    paramShowX = paramShowY = paramShowZ = true;

    // Get the visual parameters
    this->visOrtho->qfeGetVisualParameter("Ortho data set", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
    paramData = paramDataList.active;

    this->visOrtho->qfeGetVisualParameter("Ortho plane RL (X)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);

    this->visOrtho->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);

    this->visOrtho->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);

    // Process the visual parameters  
    this->qfeGetDataSet((qfeDataTypeIndex)paramData, this->visOrtho, 0, &volume, vl, vu);

    if(paramShowX)
    {
      color.r = 1.0; color.g = 0.0; color.b = 0.0;
      this->qfeSetSelectColor(color);
      this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeX, NULL);
    }

    if(paramShowY)
    {
      color.r = 0.0; color.g = 1.0; color.b = 0.0;
      this->qfeSetSelectColor(color);
      this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeY, NULL);
    }

    if(paramShowZ)
    {
      color.r = 0.0; color.g = 0.0; color.b = 1.0;
      this->qfeSetSelectColor(color);
      this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeZ, NULL);
    }
  }
}


//----------------------------------------------------------------------------
void qfeDriverHCR::qfeRenderStart()
{
  qfeVolume          *volume = NULL;
  qfeGrid            *grid   = NULL;
  qfePoint            origin;
  qfeViewport        *viewport;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramShadingList;
  int                 paramData;
  qfeFloat            vu, vl;

  if(this->visOrtho != NULL)
  {
    this->visOrtho->qfeGetVisualParameter("Ortho data set", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
    paramData = paramDataList.active;
    
    this->qfeGetDataSet((qfeDataTypeIndex)paramData, this->visOrtho, 0, &volume, vl, vu);

    this->visOrtho->qfeGetVisualParameter("Ortho plane RL (X)", &param);
    param->qfeSetVisualParameterValue(false);

    this->visOrtho->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
    param->qfeSetVisualParameterValue(true);

    this->visOrtho->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
    param->qfeSetVisualParameterValue(false);
  }

  if(this->visSurface)
  {
    cout << "load the surface" << endl;
  }

  if(this->visDVR != NULL)
  {
    qfeVisualParameter *p;
    this->visDVR->qfeGetVisualParameter("DVR visible", &p);
    p->qfeSetVisualParameterValue(false);

    this->engine->GetViewport3D(&viewport);
    this->algorithmDVR->qfeSetViewportSize(viewport->width, viewport->height, this->frameBufferProps[0].supersampling);    
    this->qfeCreateTextures(viewport, this->frameBufferProps[0].supersampling);    
  }

  if(this->visSurface != NULL)
  {
    qfeVisualParameter *p;
    this->visSurface->qfeGetVisualParameter("Surface shading", &p);
    if(p != NULL) p->qfeGetVisualParameterValue(paramShadingList);
    paramShadingList.active = 3;
    p->qfeSetVisualParameterValue(paramShadingList);
  }

  if(volume != NULL)
  {
    this->qfeGetDataSlice(volume, qfePlaneCoronal,   &this->planeX);
    this->qfeGetDataSlice(volume, qfePlaneSagittal,  &this->planeY);
    this->qfeGetDataSlice(volume, qfePlaneAxial,     &this->planeZ);
  }
}

//----------------------------------------------------------------------------
void qfeDriverHCR::qfeRenderWait()
{
  // Start rendering
  glDisable(GL_LIGHTING);

  // Save the current model-view matrix
  GLfloat modelview[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f::qfeSetMatrixElements(this->currentModelView, modelview);

  // Initialize color map
  qfeColorMap *colormap;
  if(this->visOrtho != NULL)
  {
    this->visOrtho->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  if(this->visDVR != NULL)
  {
    this->visDVR->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
    colormap->qfeUploadGradientMap();
  }
  if(this->visClusterArrows != NULL)
  {
    this->visClusterArrows->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }

  // Clear the intersection buffer
  this->qfeEnableIntersectBuffer();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  this->qfeDisableIntersectBuffer();

  // Render the clustering
  if(this->visCluster != NULL)
  {
    this->qfeRenderVisualClustering(this->visCluster);
  }

  // Render the cluster arrow trace
  if(this->visClusterArrows != NULL)
  {
    this->qfeRenderVisualClusterArrows(this->visClusterArrows);
  }

  // Render the reformat
  if(this->visOrtho != NULL)
  {
    this->qfeRenderVisualPlanesOrtho(this->visOrtho);
  }

  // Render the surface
  if(this->visSurface != NULL)
  {
    this->qfeRenderVisualAnatomyIllustrative(this->visSurface);
  }

  // Render the raycasting
  if(this->visDVR != NULL)
  {
    this->qfeRenderVisualDVR(this->visDVR);
  }

  // Render visual aides
  if(this->visAides != NULL)
  {
    this->qfeRenderVisualAides(this->visAides);
  }
};

//----------------------------------------------------------------------------
void qfeDriverHCR::qfeRenderStop()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeRenderVisualClustering(qfeVisualClustering *visual)
{
  // SJ - for now, this is more of a stub that really a visualization
  //      at least you can catch the parameters here

  qfeStudy           *study;
  int                 current;
  qfeVisualParameter *param;
  qfeSelectionList    paramCostMeasureList;
  double              paramLevel;  

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL || study->hierarchy == NULL) return qfeError;

  // Initialize cluster label volumes if hierarchy changed
  if(study->hierarchy->changed)
  {
    study->hierarchy->changed = false;

    if(study->hierarchy->clusterType == qfeClusterTypeScalar) 
      this->qfeInitializeClusterLabelVolumes(&study->clusters, &study->ftle);
    else this->qfeInitializeClusterLabelVolumes(&study->clusters, &study->pcap);

    this->hierarchicalClustering->qfeGetInitialClusterLabels(study->hierarchy,&study->clusters);

    // Set hierarchy level to zero
    visCluster->qfeGetVisualParameter("Hierarchy level (%)", &param);
    param->qfeSetVisualParameterValue(0.0);
    visCluster->qfeAddVisualParameter(*param);
    this->currentLevel = 0.0;

    // Load cluster type from hierarchy data
    qfeSelectionList costMeasure;
    visCluster->qfeGetVisualParameter("Clustering cost", &param);
    param->qfeGetVisualParameterValue(costMeasure);

    // Make sure these definitions are "synchronized" properly
    switch(study->hierarchy->clusterType)
    {
      default:
      case qfeClusterTypeL2Norm:      costMeasure.active = 0; break;
      case qfeClusterTypeElliptical:  costMeasure.active = 1; break;
      case qfeClusterTypeElliptical4D:costMeasure.active = 2; break;
      case qfeClusterTypeLinearModel: costMeasure.active = 3; break;
      case qfeClusterTypeScalar:      costMeasure.active = 4; break;
    }
    param->qfeSetVisualParameterValue(costMeasure);
    visCluster->qfeAddVisualParameter(*param); 

    // Load hierarchical step size from hierarchy data
    visCluster->qfeGetVisualParameter("Hierarchical step size", &param);
    param->qfeSetVisualParameterValue(study->hierarchy->stepSize);
    visCluster->qfeAddVisualParameter(*param); 

    /*
    // Load leaf percentage from hierarchy data
    visCluster->qfeGetVisualParameter("Data selection (%)", &param);
    param->qfeSetVisualParameterValue(100.0*study->hierarchy->leaves.size() \
      / (study->hierarchy->dims[0]*study->hierarchy->dims[1]*study->hierarchy->dims[2]));
    visCluster->qfeAddVisualParameter(*param); 
    */
    
    this->scene->qfeSceneUpdate();
  }

  if(study->clusters.empty())
    return qfeError;

  // Continue rendering
  visual->qfeGetVisualParameter("Hierarchy level (%)", &param);
  param->qfeGetVisualParameterValue(paramLevel);

  if(this->currentLevel != paramLevel)
  {
    this->clusterVoxels.clear();
    
    this->hierarchicalClustering->qfeGetClusterLabels(study->hierarchy, &study->clusters, (int)(study->hierarchy->tree.size()*(paramLevel/100)));
    this->hierarchicalClustering->qfeGetClusterVoxelPositions(study->hierarchy, study->clusters.front(), study->clusters.size(), (int)(study->hierarchy->tree.size()*(paramLevel/100)), clusterVoxels);
    
    this->currentLevel    = paramLevel;
    this->currentSeedType = -1;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeRenderVisualClusterArrows(qfeVisualClusterArrowTrace *visual)
{
  qfeStudy           *study;
  qfeVisualParameter *param;
  float               current;
  qfeColorMap        *colormap;  
  qfeLightSource     *light;

  qfeFloat            phaseDuration;

  qfeSelectionList    paramLineType;
  qfeSelectionList    paramSeedType;
  qfeSelectionList    paramColorType;
  double              paramLineWidth;
  bool                paramArrowVisible;
  bool                paramCentersVisible;
  bool                paramClusterMask;
  bool                paramShading;
  double              paramLengthMask;
  int                 paramTraceSteps;
  int                 paramColorScale;
  qfeColorRGBA        paramColor;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  this->scene->qfeGetSceneLightSource(&light);

  if(study == NULL || study->hierarchy == NULL) return qfeError;  

  study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);   

  // Get visual parameters
  visual->qfeGetVisualParameter("HCR centers visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCentersVisible);

  visual->qfeGetVisualParameter("HCR arrows visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramArrowVisible);

  visual->qfeGetVisualParameter("HCR cluster mask", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClusterMask);

  visual->qfeGetVisualParameter("HCR length mask (mm)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLengthMask);  

  visual->qfeGetVisualParameter("HCR seed style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSeedType);

  visual->qfeGetVisualParameter("HCR line style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLineType);

  visual->qfeGetVisualParameter("HCR line width (mm)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLineWidth); 

  visual->qfeGetVisualParameter("HCR color type", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramColorType); 

  visual->qfeGetVisualParameter("HCR color uniform", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramColor); 
 
  visual->qfeGetVisualParameter("HCR color scale (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramColorScale);  

  visual->qfeGetVisualParameter("HCR shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShading);    

  visual->qfeGetVisualParameter("HCR steps/sample", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTraceSteps);

  this->algorithmLines->qfeSetPhase(current);
  this->algorithmLines->qfeSetTraceSteps(paramTraceSteps);
  this->algorithmLines->qfeSetPhaseDuration(phaseDuration);
  this->algorithmLines->qfeSetClusterMask(paramClusterMask);
  this->algorithmLines->qfeSetLengthMask(paramLengthMask);
  this->algorithmLines->qfeSetStyle((qfeFlowLinesStyle)paramLineType.active);
  this->algorithmLines->qfeSetColorType((qfeFlowLineTraceCluster::qfeFlowLinesColorType)paramColorType.active);
  this->algorithmLines->qfeSetColorScale(paramColorScale); 
  this->algorithmLines->qfeSetColor(paramColor);  
  this->algorithmLines->qfeSetLineWidth(paramLineWidth);
  this->algorithmLines->qfeSetLightSource(light);

  // Get the right cluster centers
  if(paramSeedType.active != this->currentSeedType)
  {
    this->clusterCenters.clear();

    switch(paramSeedType.active)
    {
    case 0 : //3D
      this->hierarchicalClustering->qfeGetClusterCenterPositions(&this->clusterVoxels, qfeHierarchicalClustering::qfeCenterStochastic3D, 200, this->clusterCenters);        
      break;
    case 1 : //4D dynamic
      this->hierarchicalClustering->qfeGetClusterCenterPositions(&this->clusterVoxels, qfeHierarchicalClustering::qfeCenterStochastic4D, 200, this->clusterCenters);        
      break;
    case 2 :// 4D static
      this->hierarchicalClustering->qfeGetClusterCenterPositions(&this->clusterVoxels, qfeHierarchicalClustering::qfeCenterStochastic4D, 200, this->clusterCenters);        
      //this->algorithmLines->qfeGeneratePathLinesStatic(study->pcap, study->clusters, (qfeSeeds *)&this->clusterCenters);    // COMMENTED TO DEBUG REMOVE COMMENT FOR NORMAL FUNCTIONALITY
      break;
    default:
      this->hierarchicalClustering->qfeGetClusterCenterPositions(&this->clusterVoxels, qfeHierarchicalClustering::qfeCenterStochastic4D, 200, this->clusterCenters);        
    }  

    this->currentSeedType = paramSeedType.active;
  }
  if((int)this->clusterCenters.size() <= 0)     return qfeError;

  // Process other visualization related parameters
  if(paramShading)
    this->algorithmLines->qfeSetLineShading(qfeFlowLinesPhongAdditive);
  else
    this->algorithmLines->qfeSetLineShading(qfeFlowLinesNone);

  if(paramArrowVisible)
  {
    // static 
    if(paramSeedType.active == 2) 
    {       
      this->algorithmLines->qfeRenderClusterPathlinesStatic(colormap);
    }
    // dynamic
    else 
    {      
      this->algorithmLines->qfeGeneratePathLinesDynamic(study->pcap, study->clusters, (qfeSeeds *)&this->clusterCenters, current);    
      this->algorithmLines->qfeRenderClusterPathlinesDynamic(colormap);

      this->qfeEnableIntersectBuffer();
      this->algorithmLines->qfeRenderClusterPathlinesDynamic(colormap); 
      this->qfeDisableIntersectBuffer(); 
    }    
  }

  if(paramCentersVisible)  
  {    
    this->hierarchicalClustering->qfeRenderClusterCenters(study->clusters.front(), &this->clusterCenters, current);
  }

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeRenderVisualPlanesOrtho(qfeVisualReformat *visual)
{
  qfeStudy           *study;
  qfeVolume          *volume;
  qfeGrid            *grid;
  qfeColorMap        *colormap;
  qfePoint            origin;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  int                 paramInterp;
  double              paramBrightness;
  double              paramContrast;
  int                 paramTrans;
  qfeFloat            paramRange[2];
  bool                paramShowX, paramShowY, paramShowZ;
  int                 current;
  qfeColorRGB         colorX, colorY, colorZ;
  qfeFloat            tt;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  // Get visual parameters
  visual->qfeGetVisualParameter("Ortho data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("Ortho data vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("Ortho data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("Ortho data interpolation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInterpList);
  paramInterp = paramInterpList.active;

  visual->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);

  visual->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);

  visual->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);

  visual->qfeGetVisualParameter("Ortho transparency", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTrans);

  visual->qfeGetVisualParameter("Ortho contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);

  visual->qfeGetVisualParameter("Ortho brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);

  // Process parameters  
  this->qfeGetDataSet((qfeDataTypeIndex)paramData, visual, current, &volume, paramRange[0], paramRange[1]);

  if(volume != NULL)
  {
    volume->qfeGetVolumeGrid(&grid);
    volume->qfeGetVolumeTriggerTime(tt);
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  }
  else origin.x = origin.y = origin.z = 0.0;

  // Determine line color
  colorX = colorY = colorZ = this->colorDefault;
  if(this->selectedPlane == qfePlaneX)  colorX = this->colorHighlight;
  if(this->selectedPlane == qfePlaneY)  colorY = this->colorHighlight;
  if(this->selectedPlane == qfePlaneZ)  colorZ = this->colorHighlight;

  // Update algorithm parameters
  this->algorithmOrtho->qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
  this->algorithmOrtho->qfeSetPlaneDataComponent(paramComp);
  this->algorithmOrtho->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmOrtho->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  this->algorithmOrtho->qfeSetPlaneTriggerTime(tt);
  this->algorithmOrtho->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  this->algorithmOrtho->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmOrtho->qfeSetPlaneContrast(paramContrast);
  this->algorithmOrtho->qfeSetPlaneOpacity(1.0-(paramTrans/100.0));

  // Render the planes
  if(paramShowX)
  {
    this->algorithmOrtho->qfeSetPlaneBorderColor(colorX);
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeX, colormap);

    this->qfeEnableIntersectBuffer();
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeX, colormap);
    this->qfeDisableIntersectBuffer();
  }

  if(paramShowY)
  {
    this->algorithmOrtho->qfeSetPlaneBorderColor(colorY);
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeY, colormap);

    this->qfeEnableIntersectBuffer();
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeY, colormap);
    this->qfeDisableIntersectBuffer();
  }

  if(paramShowZ)
  {
    this->algorithmOrtho->qfeSetPlaneBorderColor(colorZ);
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeZ, colormap);

    this->qfeEnableIntersectBuffer();
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeZ, colormap);
    this->qfeDisableIntersectBuffer();
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual)
{ 
  qfeStudy        *study;
  qfeVolume       *volume;
  qfeGrid         *grid;
  qfeLightSource  *light;
  qfeModel        *model;  
  vtkPolyData     *mesh; 
  unsigned int     nx, ny, nz;
  qfeFloat         ex, ey, ez, ox, oy, oz;
  int              current;

  this->scene->qfeGetSceneLightSource(&light);
  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);

  volume = study->pcap[current];
  volume->qfeGetVolumeSize(nx, ny, nz);
  volume->qfeGetVolumeGrid(&grid);

  grid->qfeGetGridExtent(ex,ey,ez);
  grid->qfeGetGridOrigin(ox,oy,oz);

  if(volume == NULL)
  {
    cout << "qfeDriverHCR::qfeRenderVisualAnatomyIllustrative - Incorrect volume data" << endl;
    return qfeError;
  }

  if(study == NULL)
  {
    cout << "qfeDriverHCR::qfeRenderVisualAnatomyIllustrative - Incorrect study" << endl;
    return qfeError;
  }

  // Compute the view vector
  qfeVector   eyeVec;
  qfeMatrix4f mvInv;
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);

  qfeMatrix4f P2V, V2P;
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume); 
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume); 

   // Get eye vector in patient coordinates;
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;

  // Get the parameters
  qfeSelectionList    paramSurfaceShadingList;
  bool                paramSurfaceContour;
  bool                paramSurfaceClipping;

  qfeVisualParameter *param;  

  visual->qfeGetVisualParameter("Surface shading", &param);    
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceShadingList); 

  visual->qfeGetVisualParameter("Surface clipping", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceClipping); 

  visual->qfeGetVisualParameter("Surface contour", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceContour); 

  // Set visual parameters
  this->algorithmSurfaceShading->qfeSetLightSource(light);

  for(int i=0; i<(int)study->segmentation.size(); i++)
  {
    bool                visible;
    int                 surfaceShading;
    qfeColorRGB         colorSilhouette;
    qfeColorRGBA        colorContour;
    qfeColorRGBA        colorContourHidden;
    
    // Get the model    
    model = study->segmentation[i];
    
    // Get the parameters
    model->qfeGetModelColor(colorSilhouette);    

    // Process the parameter structures   
    colorContour.r       = 0.3; 
    colorContour.g       = 0.3;
    colorContour.b       = 0.3;
    colorContourHidden.r = 0.7;
    colorContourHidden.g = 0.7;
    colorContourHidden.b = 0.7;

    surfaceShading       = paramSurfaceShadingList.active; 

    // Get the mesh    
    model->qfeGetModelMesh(&mesh);
    model->qfeGetModelVisible(visible);

    double *center;
    center = mesh->GetCenter();

    // Draw the mesh
    if(mesh != NULL && visible)
    {     
      this->algorithmSurfaceShading->qfeRenderMeshCulling(mesh, volume, colorSilhouette, paramSurfaceClipping, surfaceShading);
      //this->algorithmSurfaceShading->qfeRenderMesh(mesh, volume, colorSilhouette, paramSurfaceClipping, surfaceShading);
      
      if(paramSurfaceContour) 
      {
        //if(mouseDrag)                 
        //  this->algorithmContours->qfeRenderContourHidden(mesh, volume, colorContourHidden, 1.5*this->frameBufferProps[0].supersampling);
     
        this->algorithmSurfaceContours->qfeRenderContour(mesh, volume, colorContour, 2.5*this->frameBufferProps[0].supersampling);
      }
    }    
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeRenderVisualDVR(qfeVisualRaycast* visual)
{
  qfeStudy           *study;
  qfeVolume          *textureVolume;
  qfeVolume          *boundingVolume;
  qfeColorMap        *colormap;
  qfeLightSource     *light;
  int                 current;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramQualList;
  qfeSelectionList    paramStyleList;
  bool                paramVisible;
  int                 paramData;
  int                 paramComp;
  int                 paramQuality;
  bool                paramGradient;
  bool                paramShading;
  bool                paramMultiRes;
  qfeFloat            paramRange[2];
  int                 paramMode;
  qfeRange            paramClipX, paramClipY, paramClipZ;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  this->scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("DVR visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);

  visual->qfeGetVisualParameter("DVR data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("DVR data component", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("DVR gradient", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramGradient);

  visual->qfeGetVisualParameter("DVR shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShading);

  visual->qfeGetVisualParameter("DVR shading style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramStyleList);

  visual->qfeGetVisualParameter("DVR shading quality", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramQualList);
  switch(paramQualList.active)
  {
  case 0  : paramQuality = 1; break;
  case 1  : paramQuality = 2; break;
  case 2  : paramQuality = 4; break;
  case 3  : paramQuality = 8; break;
  default : paramQuality = 2; break;
  }

  visual->qfeGetVisualParameter("DVR shading multires", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramMultiRes);

  visual->qfeGetVisualParameter("DVR clipping x", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipX);

  visual->qfeGetVisualParameter("DVR clipping y", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipY);

    visual->qfeGetVisualParameter("DVR clipping z", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipZ);

  if(paramVisible != true) return qfeError;

  // Get the data set and compute the data range
  this->qfeGetDataSet((qfeDataTypeIndex)paramData, visual, current, &textureVolume, paramRange[0], paramRange[1]);

  if(textureVolume == NULL) return qfeError;

  textureVolume->qfeGetVolumeType(paramMode);

  // if there is PCA-P data loaded, use this as bounding volume
  if((int)study->pcap.size() > 0)
    boundingVolume = study->pcap.front();
  else
    boundingVolume = textureVolume;

  // Multi-resolution dvr - check the global state
  if(paramMultiRes)
    paramQuality = std::min((float)paramQuality, this->dvrQuality);

  // Update algorithm parameters
  this->algorithmDVR->qfeSetIntersectionBuffers(this->intersectTexColor, this->intersectTexDepth);
  //this->algorithmDVR->qfeSetConvexClippingBuffers(this->clippingFrontTexDepth[0], this->clippingBackTexDepth[0]);
  this->algorithmDVR->qfeSetOrthoClippingRanges(paramClipX, paramClipY, paramClipZ);
  this->algorithmDVR->qfeSetDataComponent(paramComp);
  this->algorithmDVR->qfeSetDataRange(paramRange[0], paramRange[1]);
  this->algorithmDVR->qfeSetGradientRendering(paramGradient);
  this->algorithmDVR->qfeSetShading(paramShading);
  this->algorithmDVR->qfeSetLightSource(light);
  this->algorithmDVR->qfeSetQuality(paramQuality);
  this->algorithmDVR->qfeSetStyle(paramStyleList.active);

  this->algorithmDVR->qfeRenderRaycasting(this->frameBufferProps[0].texColor, this->frameBufferProps[0].texDepth, boundingVolume, textureVolume, colormap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeRenderVisualAides(qfeVisualAides *visual)
{
  qfeStudy           *study;
  qfeVolume          *volume;
  int                 current;

  qfeVisualParameter *param;
  bool                paramSupportBoundingBox = true;
  bool                paramSupportAxes        = false;

  this->visAides->qfeGetVisualStudy(&study);
  this->visAides->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  if(study->pcap.size() > 0)
    volume = study->pcap[current];
  else return qfeError;

  this->visAides->qfeGetVisualParameter("Support bounding box", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportBoundingBox);

  this->visAides->qfeGetVisualParameter("Support axes", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportAxes);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  if(paramSupportBoundingBox == true) qfeDriverAide::qfeRenderBoundingBoxDashed(volume, this->currentModelView, 0.8*this->frameBufferProps[0].supersampling);
  if(paramSupportAxes        == true) qfeDriverAide::qfeRenderVolumeAxes(volume, 10.0, 1.6*this->frameBufferProps[0].supersampling);

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeDriverHCR::qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if(volume == NULL) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin + (mm*normal);
  else
    origin = origin - (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = std::max(originTex.x, 0.0f+delta);
  originTex.x = std::min(originTex.x, 1.0f-delta);
  originTex.y = std::max(originTex.y, 0.0f+delta);
  originTex.y = std::min(originTex.y, 1.0f-delta);
  originTex.z = std::max(originTex.z, 0.0f+delta);
  originTex.z = std::min(originTex.z, 1.0f-delta);

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z); 
}

//----------------------------------------------------------------------------
void qfeDriverHCR::qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{ 
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if(volume == NULL) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin - (mm*normal);
  else
    origin = origin + (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = std::max(originTex.x, 0.0f+delta);
  originTex.x = std::min(originTex.x, 1.0f-delta);
  originTex.y = std::max(originTex.y, 0.0f+delta);
  originTex.y = std::min(originTex.y, 1.0f-delta);
  originTex.z = std::max(originTex.z, 0.0f+delta);
  originTex.z = std::min(originTex.z, 1.0f-delta);

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeGetDataSet(qfeDataTypeIndex index, qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu)
{
  qfeStudy           *study;
  int                 current;

  *volume = NULL;
  vl = vu = 0.0;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(dataTypePCAP) : // PCA-P
    if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time))
    {
      *volume = study->pcap[time];
      study->qfeGetStudyRangePCAP(vl, vu);
    }
    break;
  case(dataTypePCAM) : // PCA-M
    if(((int)study->pcam.size() > 0) && ((int)study->pcam.size() > time))
    {
      *volume = study->pcam[time];
      study->qfeGetStudyRangePCAM(vl, vu);
    }
    break;
  case(dataTypeSSFP) : // SSFP
    if((int)study->ssfp3D.size() > 0)
    {
      // If there is also pcap data loaded, check for the trigger time and load
      // accompanying ssfp data at the right time slot
      if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time))
      {
        qfeFloat att, ptt;
        qfeFloat distance = 9000;
        (int)study->pcap[time]->qfeGetVolumeTriggerTime(ptt);
        for(int i=0; i<(int)study->ssfp3D.size(); i++)
        {
          (int)study->ssfp3D[i]->qfeGetVolumeTriggerTime(att);
          if(abs(att-ptt) < distance)
          {
            distance = abs(att-ptt);
            *volume = study->ssfp3D[i];
          }
        }
      }
      else
        *volume = study->ssfp3D[0];

      study->qfeGetStudyRangeSSFP(vl, vu);
      // Non-float texture are clamped [0,1]
      vl = vl / pow(2.0f,16.0f);
      vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(dataTypeTMIP) : // T-MIP
    if(study->tmip != NULL)
    {
      *volume = study->tmip;
      study->tmip->qfeGetVolumeValueDomain(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(dataTypeFFE) : // Flow anatomy FFE
    if(((int)study->ffe.size() > 0) && ((int)study->ffe.size() > time))
    {
      *volume = study->ffe[time];
      study->qfeGetStudyRangeFFE(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(dataTypeCLUST) : // Clusters
    if(((int)study->clusters.size() > 0) && ((int)study->clusters.size() > time))
    {
      *volume = study->clusters[time];
      (*volume)->qfeGetVolumeValueDomain(vu, vl);
    }
    break;
  case(dataTypeFTLE) : // FTLE
    if(((int)study->ftle.size() > 0) && ((int)study->ftle.size() > time))
    {
      *volume = study->ftle[time];
      (*volume)->qfeGetVolumeValueDomain(vu, vl);
    }
    break;
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeGetDataSetOrtho(int index, qfeVisual *visual, qfeVolume **volume)
{
  qfeStudy           *study;
  int                 current;

  *volume = NULL;
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(0) : // PCA-P
            if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > current))
              *volume = study->pcap[current];
            break;
  case(1) : // PCA-M
            if(((int)study->pcam.size() > 0) && ((int)study->pcam.size() > current))
              *volume = study->pcam[current];
            break;
  case(2) : // SSFP
            if(((int)study->ssfp3D.size() > 0) && ((int)study->ssfp3D.size() > current))
              *volume = study->ssfp3D[current];
            break;
  case(3) : // TMIP
            if((int)study->tmip != NULL)
              *volume = study->tmip;
            break;
  case(4) : // FFE
            break;
  case(5) : // Clusters
            if(((int)study->clusters.size() > 0) && ((int)study->clusters.size() > current))
              *volume = study->clusters[current];
            break;

  // SJ, this may be a bit tricky. Just try to add a new type CLUSTERS to select the study->clusters as input
  // - Done
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeGetDataSlice(qfeVolume *volume, qfeClusterPlaneDirection dir, qfeFrameSlice **slice)
{
  qfeMatrix4f P2V, V2P;
  qfeVector   axisX, axisY, axisZ, axisXabs, axisYabs, axisZabs, x, y, z;
  qfePoint    o;
  qfeGrid    *grid;

  if(volume == NULL) return qfeError;

  // Get the patient to voxel transformation
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);

  // Set the plane normal direction in patient coordinates
  switch(dir)
  {
  case qfePlaneAxial       : axisX.qfeSetVectorElements(1.0,0.0,0.0);
                             axisY.qfeSetVectorElements(0.0,1.0,0.0);
                             axisZ.qfeSetVectorElements(0.0,0.0,1.0);                              
                             break;
  case qfePlaneSagittal    : axisX.qfeSetVectorElements(0.0,0.0,1.0);
                             axisY.qfeSetVectorElements(0.0,1.0,0.0);
                             axisZ.qfeSetVectorElements(1.0,0.0,0.0); 
                             break;
  case qfePlaneCoronal     : axisX.qfeSetVectorElements(1.0,0.0,0.0);
                             axisY.qfeSetVectorElements(0.0,0.0,1.0);
                             axisZ.qfeSetVectorElements(0.0,1.0,0.0); 
                             break;
  }  

  // Convert the axis to voxel coordinates and find dominant direction
  axisX = axisX*P2V;
  axisY = axisY*P2V;
  axisZ = axisZ*P2V;

  axisXabs.qfeSetVectorElements(abs(axisX.x), abs(axisX.y), abs(axisX.z));
  axisYabs.qfeSetVectorElements(abs(axisY.x), abs(axisY.y), abs(axisY.z));
  axisZabs.qfeSetVectorElements(abs(axisZ.x), abs(axisZ.y), abs(axisZ.z));

  if(axisZabs.x == max(max(axisZabs.x, axisYabs.x), axisXabs.x))
  {
    x.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
    y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
    z.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
  }
  if(axisZabs.y == max(max(axisZabs.y, axisYabs.y), axisXabs.y))
  {
    x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
    y.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
    z.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
  }
  if(axisZabs.z == max(max(axisZabs.z, axisYabs.z), axisXabs.z))
  {
    x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
    y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
    z.qfeSetVectorElements( 0.0f,  0.0f,  1.0f);    
  }

  // Convert back to patient coordinates
  x = x*V2P;
  y = y*V2P;
  z = z*V2P;

  // Get the plane origin
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridOrigin(o.x, o.y, o.z);

  // Build the plane
  (*slice)->qfeSetFrameExtent(1,1,1);
  (*slice)->qfeSetFrameOrigin(o.x, o.y, o.z);
  (*slice)->qfeSetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeShowClusters(qfeVisualReformatOrtho *visual)
{
  qfeVisualParameter *p;
  qfeSelectionList    l;

  this->visOrtho->qfeGetVisualParameter("Ortho data set", &p);
  p->qfeGetVisualParameterValue(l);  
  l.active = (int)dataTypeCLUST;
  p->qfeSetVisualParameterValue(l);    

  this->visOrtho->qfeGetVisualParameter("Ortho data representation", &p);
  p->qfeSetVisualParameterValue(l); 
  l.active = 1;
  p->qfeSetVisualParameterValue(l); 

  this->visOrtho->qfeGetVisualParameter("Ortho data interpolation", &p);
  p->qfeSetVisualParameterValue(l); 
  l.active = 0;
  p->qfeSetVisualParameterValue(l); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeGenerateClusters()
{
  if(this->visCluster == NULL) return qfeError;
  qfeStudy *study;
  this->visCluster->qfeGetVisualStudy(&study);
  if(study == NULL) return qfeError;
  
  qfeClusterHierarchy *hierarchy = study->hierarchy;


  qfeVisualParameter *param;
  qfeSelectionList    paramCostMeasureList;
  qfeSelectionList    paramSourceSelection;
  int                 paramCost;
  int                 paramSrc;
  double              dataPercentage        = 5.0;
  qfeVolumeSeries    *clusterSource;
  
  // TODO: Make this user selected? TODO: FTLE as source
  visCluster->qfeGetVisualParameter("Source", &param);
  if(param != NULL){
    param->qfeGetVisualParameterValue(paramSourceSelection);
  }
  paramSrc = paramSourceSelection.active;

  switch(paramSrc){
  case(0):  //pca-p
            clusterSource = &study->pcap;
            break;
  case(1):  //ftle
            clusterSource = &study->ftle;
            break;
  }
  
  visCluster->qfeGetVisualParameter("Clustering cost", &param);
  if(param != NULL) 
    param->qfeGetVisualParameterValue(paramCostMeasureList);
  paramCost = paramCostMeasureList.active;

  switch(paramCost)
  {
  case(0):  // Eucledian distance (L2)
            hierarchy->clusterType    = qfeClusterTypeL2Norm;
            break;
  case(1):  // Elliptical distance 
            hierarchy->clusterType    = qfeClusterTypeElliptical;

            visCluster->qfeGetVisualParameter("Elliptical - A", &param);
            if(param != NULL)
              param->qfeGetVisualParameterValue(qfeClusterNodeElliptic::A);
            cout << "qfeClusterNodeElliptic::A = " << qfeClusterNodeElliptic::A << endl;

            visCluster->qfeGetVisualParameter("Elliptical - B", &param);
            if(param != NULL)
              param->qfeGetVisualParameterValue(qfeClusterNodeElliptic::B);
            cout << "qfeClusterNodeElliptic::B = " << qfeClusterNodeElliptic::B << endl;

            if(clusterSource->size()>0)
            {
              qfeGrid *grid;
              unsigned int nx,ny,nz;
              qfeFloat ex,ey,ez;

              clusterSource->front()->qfeGetVolumeSize(nx,ny,nz);
              clusterSource->front()->qfeGetVolumeGrid(&grid);
              grid->qfeGetGridExtent(ex,ey,ez);

              double dx = ex*nx;
              double dy = ey*ny;
              double dz = ez*nz;

              qfeClusterNodeElliptic::maxSpatialDistance = sqrt(dx*dx+dy*dy+dz*dz)/10;
            }
            break;
  case(2):  // Elliptical distance 4D
            hierarchy->clusterType    = qfeClusterTypeElliptical4D;

            visCluster->qfeGetVisualParameter("Elliptical - A", &param);
            if(param != NULL)
              param->qfeGetVisualParameterValue(qfeClusterNodeElliptic::A);
            cout << "qfeClusterNodeElliptic::A = " << qfeClusterNodeElliptic::A << endl;

            visCluster->qfeGetVisualParameter("Elliptical - B", &param);
            if(param != NULL)
              param->qfeGetVisualParameterValue(qfeClusterNodeElliptic::B);
            cout << "qfeClusterNodeElliptic::B = " << qfeClusterNodeElliptic::B << endl;

            if(clusterSource->size()>0)
            {
              qfeGrid *grid;
              unsigned int nx,ny,nz;
              qfeFloat ex,ey,ez;

              clusterSource->front()->qfeGetVolumeSize(nx,ny,nz);
              clusterSource->front()->qfeGetVolumeGrid(&grid);
              grid->qfeGetGridExtent(ex,ey,ez);

              double dx = ex*nx;
              double dy = ey*ny;
              double dz = ez*nz;

              qfeClusterNodeElliptic::maxSpatialDistance = sqrt(dx*dx+dy*dy+dz*dz)/10;
            }
            break;
  case(3):  // Linear model
            hierarchy->clusterType = qfeClusterTypeLinearModel; 
            break;  
  case(4):  // Scalar model
            hierarchy->clusterType = qfeClusterTypeScalar;

            visCluster->qfeGetVisualParameter("Elliptical - A", &param);
            if(param != NULL)
              param->qfeGetVisualParameterValue(qfeClusterNodeElliptic::A);
            cout << "qfeClusterNodeElliptic::A = " << qfeClusterNodeElliptic::A << endl;

            //Get maximum FTLE value
            for(int t = 0; t < study->ftle.size(); t++){
              qfeFloat lowerVal;
              qfeFloat upperVal;
              study->ftle[t]->qfeGetVolumeValueDomain(lowerVal, upperVal);
              if(upperVal > qfeClusterNodeScalar::maxScalarVal){
                qfeClusterNodeScalar::maxScalarVal = upperVal;
              }
            }
            
            break;
  default:
            cout << "qfeDriverHCR::qfeGenerateClusters - Unknown clustering cost selected" << endl; 
            return qfeError;
  }

  visCluster->qfeGetVisualParameter("Data selection (%)", &param);
  if(param != NULL)
    param->qfeGetVisualParameterValue(dataPercentage);

  visCluster->qfeGetVisualParameter("Hierarchical step size", &param);
  if(param != NULL)
    param->qfeGetVisualParameterValue(hierarchy->stepSize);

  if(hierarchy->clusterType == qfeClusterTypeScalar){
    this->hierarchicalClustering->qfeSelectLeavesByVolume(hierarchy,clusterSource->front());
  }
  else if(study->tmip != NULL)
    this->hierarchicalClustering->qfeSelectLeavesByPercentage(hierarchy,study->tmip,dataPercentage);
  else
  {
    if(clusterSource->size()==1)
    {
      cout << "Using cluster source dataset for leaf selection" << endl;
      this->hierarchicalClustering->qfeSelectLeavesByPercentage(hierarchy,clusterSource->front(),dataPercentage);
    }
    else
    {
      cout << "Please generate tMIP data for leaf selection" << endl;
      return qfeError;
    }
  }
    
  this->hierarchicalClustering->qfeInitializeLeafNodes(hierarchy, *clusterSource);
  this->hierarchicalClustering->qfeBuildTree(hierarchy);

  hierarchy->changed = true;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeInitializeClusterLabelVolumes(qfeVolumeSeries *clusterLabels, qfeVolumeSeries *clusterSource)
{
  // Clear existing cluster label volumes
  qfeVolumeSeries::iterator i; 
  for(i = clusterLabels->begin(); i < clusterLabels->end(); i++) {delete *i; *i = NULL;}; 
  clusterLabels->clear();

  // Initialize zero filled cluster label volumes
  qfeGrid     *grid;
  unsigned int nx, ny, nz, nx_check, ny_check, nz_check;
  qfeFloat     tt, pd;

  clusterSource->front()->qfeGetVolumeSize(nx, ny, nz);
  float       *voxels = (float *)calloc(nx*ny*nz, sizeof(float));

  for(int i=0; i<(int)clusterSource->size(); i++)
  {
    clusterSource->at(i)->qfeGetVolumeSize(nx_check, ny_check, nz_check);

    if(nx != nx_check || ny != ny_check || nz != nz_check)
    {
      cout << "qfeDriverHCR::qfeInitializeClusterLabels - Inconsistent volume dimensions" << endl;
      return qfeError;
    }

    clusterSource->at(i)->qfeGetVolumeGrid(&grid);
    clusterSource->at(i)->qfeGetVolumeTriggerTime(tt);
    clusterSource->at(i)->qfeGetVolumePhaseDuration(pd);

    clusterLabels->push_back(new qfeVolume());

    clusterLabels->back()->qfeSetVolumeGrid(grid);
    clusterLabels->back()->qfeSetVolumePhaseDuration(pd);
    clusterLabels->back()->qfeSetVolumeTriggerTime(tt);
    clusterLabels->back()->qfeSetVolumeValueDomain(0.0,0.0);
    clusterLabels->back()->qfeSetVolumeComponentsPerVoxel(1);
    clusterLabels->back()->qfeSetVolumeData(qfeValueTypeFloat, nx, ny, nz, voxels);
    clusterLabels->back()->qfeUploadVolume();
  }
  free(voxels);
  voxels = NULL;

  return qfeSuccess;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeCreateTextures(qfeViewport *vp, int supersampling)
{
   if(supersampling <= 0) return qfeError;

  glGenTextures(1, &this->intersectTexColor);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexColor);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F,
    vp->width*supersampling, vp->height*supersampling, 0, GL_BGRA, GL_FLOAT, NULL);

  glGenTextures(1, &this->intersectTexDepth);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 
     vp->width*supersampling, vp->height*supersampling, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0);  

  glGenTextures(1, &this->clippingTexColor);
  glBindTexture(GL_TEXTURE_2D, this->clippingTexColor);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F,
    vp->width*supersampling, vp->height*supersampling, 0, GL_BGRA, GL_FLOAT, NULL);

  glGenTextures(1, &this->clippingFrontTexDepth);
  glBindTexture(GL_TEXTURE_2D, this->clippingFrontTexDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 
     vp->width*supersampling, vp->height*supersampling, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0);  
  
  glGenTextures(1, &this->clippingBackTexDepth);
  glBindTexture(GL_TEXTURE_2D, this->clippingBackTexDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 
     vp->width*supersampling, vp->height*supersampling, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeDeleteTextures()
{
  if (glIsTexture(this->intersectTexColor))     glDeleteTextures(1, (GLuint *)&this->intersectTexColor);
  if (glIsTexture(this->intersectTexDepth))     glDeleteTextures(1, (GLuint *)&this->intersectTexDepth);
  if (glIsTexture(this->clippingTexColor))      glDeleteTextures(1, (GLuint *)&this->clippingTexColor);
  if (glIsTexture(this->clippingFrontTexDepth)) glDeleteTextures(1, (GLuint *)&this->clippingFrontTexDepth);
  if (glIsTexture(this->clippingBackTexDepth))  glDeleteTextures(1, (GLuint *)&this->clippingBackTexDepth);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeBindTextures()
{
  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexColor);

  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexDepth);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeEnableIntersectBuffer()
{
  this->qfeAttachFBO(this->frameBuffer, this->intersectTexColor, this->intersectTexColor, this->intersectTexDepth);
  this->qfeEnableFBO(this->frameBuffer);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverHCR::qfeDisableIntersectBuffer()
{
  this->qfeResetFBO();
  this->qfeEnableFBO(this->frameBuffer);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeDriverHCR::qfeOnAction(void *caller, int t)
{
  qfeDriverHCR *self = reinterpret_cast<qfeDriverHCR *>(caller);

  switch(t)
  {
  case qfeVisualClustering::actionGenerateClusters  :
    self->qfeGenerateClusters();  
    if(self->visOrtho != NULL)
    {      
      self->qfeShowClusters(self->visOrtho);      
    }
    break;
  }  
  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverHCR::qfeOnResize(void *caller)
{
  qfeDriverHCR *self = reinterpret_cast<qfeDriverHCR *>(caller);
  qfeViewport  *vp;

  qfeDriver::qfeDriverResize(caller);

  self->engine->GetViewport3D(&vp);

  // Refresh size of local geometry texturs
  self->qfeUnbindTextures();
  self->qfeDeleteTextures();
  self->qfeCreateTextures(vp, self->frameBufferProps[0].supersampling);

  // Refresh size of the MIP textures
  self->algorithmDVR->qfeSetViewportSize(vp->width, vp->height, self->frameBufferProps[0].supersampling);
}

//----------------------------------------------------------------------------
void qfeDriverHCR::qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v)
{
  qfeDriverHCR *self = reinterpret_cast<qfeDriverHCR *>(caller);

  // Deal with the volume rendering - multiresolution
  self->dvrQuality = 4.0;

  self->engine->Refresh();
  return;
}

//----------------------------------------------------------------------------
void qfeDriverHCR::qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v)
{
  qfeDriverHCR *self = reinterpret_cast<qfeDriverHCR *>(caller);

  qfeColorRGB color;
  float       depth;

  self->qfeReadSelect(x, y, color);
  self->qfeReadDepth(x, y, depth);

  self->selectedPlane = qfePlaneNone;

  if(color.r == 1.0 && color.g == 0.0 && color.b==0.0)
    self->selectedPlane = qfePlaneX;

  if(color.r == 0.0 && color.g == 1.0 && color.b==0.0)
    self->selectedPlane = qfePlaneY;

  if(color.r == 0.0 && color.g == 0.0 && color.b==1.0)
    self->selectedPlane = qfePlaneZ;
  
  // Deal with the volume rendering - multiresolution
  self->dvrQuality = 1.0;
}

//----------------------------------------------------------------------------
void qfeDriverHCR::qfeOnWheelForward(void *caller, int x, int y, int v)
{
  qfeDriverHCR *self = reinterpret_cast<qfeDriverHCR *>(caller);

  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if(self->visOrtho == NULL) return;

  // Obtain the volume
  self->visOrtho->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visOrtho->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);

  if((int)study->pcap.size() <= 0) return;
  
  if(self->selectedPlane == qfePlaneX) self->qfeMovePlaneForward(study->pcap.front(), &self->planeX, scrollStep);
  if(self->selectedPlane == qfePlaneY) self->qfeMovePlaneForward(study->pcap.front(), &self->planeY, scrollStep);
  if(self->selectedPlane == qfePlaneZ) self->qfeMovePlaneForward(study->pcap.front(), &self->planeZ, scrollStep);
  
  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverHCR::qfeOnWheelBackward(void *caller, int x, int y, int v)
{
  qfeDriverHCR *self = reinterpret_cast<qfeDriverHCR *>(caller);

  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if(self->visOrtho == NULL) return;

  // Obtain the volume
  self->visOrtho->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visOrtho->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);
  
  if((int)study->pcap.size() <= 0) return;  

  if(self->selectedPlane == qfePlaneX) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeX, scrollStep);
  if(self->selectedPlane == qfePlaneY) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeY, scrollStep);
  if(self->selectedPlane == qfePlaneZ) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeZ, scrollStep);

  self->engine->Refresh();
}

