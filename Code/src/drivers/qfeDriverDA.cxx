#include "qfeDriverDA.h"

qfeDriverDA::qfeDriverDA(qfeScene *scene) : qfeDriver(scene)
{
	//Initialize visuals with empty pointer
	this->visualSeedVols			= NULL;
	this->visualReformat          = NULL;
	this->visualDA				= NULL;
	this->visAides				= NULL;
	this->visualInk1			= NULL;
	this->visualInk2			= NULL;

	//Initialize algorithms
	this->algorithmReformat	= new qfePlanarReformat();
	
	this->algorithmSurfaceShading = new qfeSurfaceShading();

	this->algorithmIP1			= new qfeInkParticles();

	this->algorithmIP2			= new qfeInkParticles();

	//Setup engine observers/interaction
	this->engine->SetObserverAction((void*)this, this->qfeOnAction);
	this->engine->SetObserverResize((void*)this, this->qfeOnResize);
	
	this->engine->SetInteractionMouseMove((void*)this, this->qfeOnMouseMove, false);  
	this->engine->SetInteractionMouseLeftButtonDown((void*)this, this->qfeOnMouseLeftButtonDown);
	this->engine->SetInteractionMouseRightButtonUp((void*)this, this->qfeOnMouseRightButtonUp);
	this->engine->SetInteractionMouseRightButtonDown((void*)this, this->qfeOnMouseRightButtonDown, false);
	this->engine->SetInteractionMouseWheelForward((void*)this, this->qfeOnWheelForward, false);
	this->engine->SetInteractionMouseWheelBackward((void*)this, this->qfeOnWheelBackward, false);
		
	rightMouseDown = false;

	//Initialize reformat
	//Initialize the planes (in patient coordinates)
	this->planeX = new qfeFrameSlice();      
	this->planeX->qfeSetFrameExtent(1,1);
	this->planeX->qfeSetFrameOrigin(0.0,0.0,0.0);
	this->planeX->qfeSetFrameAxes(0.0,0.0,1.0, 0.0,1.0,0.0, 1.0,0.0,0.0);  
	
	this->planeY = new qfeFrameSlice();      
	this->planeY->qfeSetFrameExtent(1,1);
	this->planeY->qfeSetFrameOrigin(0.0,0.0,0.0);
	this->planeY->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,0.0,1.0, 0.0,1.0,0.0);  
	
	this->planeZ = new qfeFrameSlice();     
	this->planeZ->qfeSetFrameExtent(1,1);
	this->planeZ->qfeSetFrameOrigin(0.0,0.0,0.0);
	this->planeZ->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0);  
	
	this->selectedPlane = qfePlaneNone;
	// Initialize plane line colors
	this->colorReformatDefault.r = 0.952;
	this->colorReformatDefault.g = 0.674;
	this->colorReformatDefault.b = 0.133;
	
	this->colorReformatHighlight.r = 0.356;
	this->colorReformatHighlight.g = 0.623;
	this->colorReformatHighlight.b = 0.396;

	this->additional_data_computed = false;

	//Initialize seed volume collection
	flowSourceAndSinkGuidePointSelected = false;
	newFlowSourceAndSink = false; 
}

qfeDriverDA::~qfeDriverDA()
{
  delete this->planeX;
  delete this->planeY;
  delete this->planeZ;

  delete this->algorithmSurfaceShading;
  delete this->algorithmReformat;
  delete this->algorithmIP1;
  delete this->algorithmIP2;

  this->qfeRenderStop();
}

bool qfeDriverDA::qfeRenderCheck()
{
	int t;
		
	for(int i=0; i<(int)this->visuals.size(); i++)
	{    
		this->visuals[i]->qfeGetVisualType(t);

		if(t == visualInkVis)
		{	  	  
			this->visualInk1 = static_cast<qfeVisualInkVis *>(this->visuals[i]);

			//remove some parameters that we don't need
			this->visualInk1->qfeRemoveVisualParameter("Uncertainty");
			this->visualInk1->qfeRemoveVisualParameter("Show graph");
			this->visualInk1->qfeRemoveVisualParameter("Graph data");
			this->visualInk1->qfeRemoveVisualParameter("Invert graph");
			this->visualInk1->qfeRemoveVisualParameter("Volumetric seeding");
			this->visualInk1->qfeRemoveVisualParameter("Planar seeding");
			this->visualInk1->qfeRemoveVisualParameter("Seed from voxel center");
			this->visualInk1->qfeRemoveVisualParameter("Seed from fixed positions");
			this->visualInk1->qfeRemoveVisualParameter("Seed throughout mesh");
			this->visualInk1->qfeRemoveVisualParameter("Clip seed outside mesh");
			this->visualInk1->qfeRemoveVisualParameter("Clip outside mesh");
			this->visualInk1->qfeRemoveVisualParameter("Clipping margin (mm)");
			this->visualInk1->qfeRemoveVisualParameter("Clip seed volume");
			this->visualInk1->qfeRemoveVisualParameter("Particle opacity");
			this->visualInk1->qfeRemoveVisualParameter("Map opacity to uncertainty");
			this->visualInk1->qfeRemoveVisualParameter("Number of lines");
			this->visualInk1->qfeRemoveVisualParameter("CFL scaling");
			this->visualInk1->qfeRemoveVisualParameter("Particle type");
			this->visualInk1->qfeRemoveVisualParameter("Render GPU friendly");
			this->visualInk1->qfeRemoveVisualParameter("Depth enhancement");
			this->visualInk1->qfeRemoveVisualParameter("Depth enhancement amount");
			this->visualInk1->qfeRemoveVisualParameter("Depth enhancement light");
			this->visualInk1->qfeRemoveVisualParameter("Chroma depth");
			this->visualInk1->qfeRemoveVisualParameter("Aerial perspective");
			this->visualInk1->qfeRemoveVisualParameter("Spatial gap amount 1");
			this->visualInk1->qfeRemoveVisualParameter("Spatial gap width 1");
			this->visualInk1->qfeRemoveVisualParameter("Spatial gap amount 2");
			this->visualInk1->qfeRemoveVisualParameter("Spatial gap width 2");			
			this->visualInk1->qfeRemoveVisualParameter("Temporal gap amount");
			this->visualInk1->qfeRemoveVisualParameter("Temporal gap width");
			this->visualInk1->qfeRemoveVisualParameter("Render colorscheme");
			this->visualInk1->qfeRemoveVisualParameter("Render seeding");
			this->visualInk1->qfeRemoveVisualParameter("Render iso surface");
			this->visualInk1->qfeRemoveVisualParameter("Iso value");

			this->visualInk1->qfeRemoveVisualAction("OpenFile");
			this->visualInk1->qfeRemoveVisualAction("Save");
			this->visualInk1->qfeRemoveVisualAction("Center");
			this->visualInk1->qfeRemoveVisualAction("Plot");
			this->visualInk1->qfeRemoveVisualAction("Seed Points Feature-based");

			//set some parameters to more suitable values:
			qfeVisualParameter *param;

			this->visualInk1->qfeGetVisualParameter("Fixed seed positions multiplier", &param);
			if(param)param->qfeSetVisualParameterValue(10.0); 	
			
			this->visualInk1->qfeGetVisualParameter("Particle multiplier", &param);
			if(param)param->qfeSetVisualParameterValue(0.5); 	
			
			this->visualInk1->qfeGetVisualParameter("Particle lifetime (phases)", &param);
			if(param)param->qfeSetVisualParameterValue(1.0); 

			this->visualInk1->qfeGetVisualParameter("Animation step size (phases)", &param);
			if(param)param->qfeSetVisualParameterValue(0.1); 

			this->visualInk1->qfeGetVisualParameter("Animation length (phases)", &param);
			if(param)param->qfeSetVisualParameterValue(1.0); 

			this->visualInk1->qfeGetVisualParameter("Particle size", &param);
			if(param)param->qfeSetVisualParameterValue(1.75); 

			this->visualInk1->qfeGetVisualParameter("Glyph type", &param);
			qfeSelectionList    paramList;
			if(param) param->qfeGetVisualParameterValue(paramList);
			if(param) paramList.active = 2;
			if(param) param->qfeSetVisualParameterValue(paramList); 

			this->visualInk1->qfeGetVisualParameter("Glyph frequency", &param);
			if(param)param->qfeSetVisualParameterValue(1); 

			//initialize the algorithm
			
			qfeStudy *study;

			this->visuals[i]->qfeGetVisualStudy(&study); 
			
			algorithmIP1->qfeInitialize(study, &study->pcap);
		}
		
		if(t == visualInkVisSecondary)
		{	  	  
			this->visualInk2 = static_cast<qfeVisualInkVisSecondary *>(this->visuals[i]);
			
			qfeStudy *study;

			this->visuals[i]->qfeGetVisualStudy(&study); 

			if(study->sim_velocities.size() == 0)
			{
				visualInk2_initialized = false;

				algorithmIP2->qfeInitialize(study, &study->pcap);
			}
			else
			{
				algorithmIP2->qfeInitialize(study, &study->sim_velocities);
			
				visualInk2_initialized = true;
			}
		}
		
		if(t == visualSeedVolumesType)
		{
			this->visualSeedVols = static_cast<qfeVisualSeedVolumes *>(this->visuals[i]);

			qfeStudy *study;

			this->visuals[i]->qfeGetVisualStudy(&study);  

			flowSourceAndSinks = new qfeSeedVolumeCollection(study);
		}
		if(t == visualAnatomyIllustrative)
		{
			this->visualAnatomy = static_cast<qfeVisualAnatomyIllustrative *>(this->visuals[i]);  
		}
		if(t == visualDataAssimilation)
		{
			this->visualDA = static_cast<qfeVisualDataAssimilation *>(this->visuals[i]);
			
			unsigned w = 1, h = 1, d = 1;

			qfeStudy *study;
			this->visuals[i]->qfeGetVisualStudy(&study); 
			if(study->pcap.size()>0)
				study->pcap.front()->qfeGetVolumeSize(w, h, d);

			//set cropping parameters to the correct domain:
			qfeVisualParameter *param;

			this->visualDA->qfeGetVisualParameter("Crop X1", &param);
			if(param)param->qfeSetVisualParameterRange(0, (int)w);
			if(param)param->qfeSetVisualParameterValue(0); 	
			this->visualDA->qfeGetVisualParameter("Crop X2", &param);
			if(param)param->qfeSetVisualParameterRange(0, (int)w);
			if(param)param->qfeSetVisualParameterValue((int)w); 	
			this->visualDA->qfeGetVisualParameter("Crop Y1", &param);
			if(param)param->qfeSetVisualParameterRange(0, (int)h);
			if(param)param->qfeSetVisualParameterValue(0); 	
			this->visualDA->qfeGetVisualParameter("Crop Y2", &param);
			if(param)param->qfeSetVisualParameterRange(0, (int)h);
			if(param)param->qfeSetVisualParameterValue((int)h); 	
			this->visualDA->qfeGetVisualParameter("Crop Z1", &param);
			if(param)param->qfeSetVisualParameterRange(0, (int)d);
			if(param)param->qfeSetVisualParameterValue(0); 	
			this->visualDA->qfeGetVisualParameter("Crop Z2", &param);
			if(param)param->qfeSetVisualParameterRange(0, (int)d);
			if(param)param->qfeSetVisualParameterValue((int)d); 	

			ROI_min.x = 0; 
			ROI_max.x = w;
			ROI_min.y = 0;
			ROI_max.y = h;
			ROI_min.z = 0;
			ROI_max.z = d;
		}
		if(t == visualAides)
		{
			this->visAides = static_cast<qfeVisualAides *>(this->visuals[i]);
		}
		if (t == visualReformatOrtho)
		{
			qfeStudy *study;

			this->visuals[i]->qfeGetVisualStudy(&study);  

			bool mesh_exists = study->segmentation.size()>0;
			
			//change parameters to different default values:
			qfeVisualParameter p;
			
			visualReformat = static_cast<qfeVisualReformatOrtho *>(visuals[i]);
			
			/*
			this->visualReformat->qfeRemoveVisualParameter("Ortho data set");
			qfeSelectionList dataSet;
			dataSet.list.push_back("pca-p");
			dataSet.list.push_back("pca-m");
			dataSet.list.push_back("ssfp");
			dataSet.list.push_back("t-mip");
			dataSet.list.push_back("ffe");
			dataSet.list.push_back("ftle");
			dataSet.list.push_back("SNR");
			dataSet.list.push_back("Signal strength");
			dataSet.list.push_back("Mesh levelset");
			dataSet.list.push_back("WSS");
			dataSet.active = 0;
			if(study->pcam.size()!=0)
				dataSet.active = 1;
			if(study->tmip!=NULL)
				dataSet.active = 3;
			p.qfeSetVisualParameter("Ortho data set", dataSet);  
			this->visualReformat->qfeAddVisualParameter(p);
			*/

			qfeVisualParameter *param;
			
			this->visualReformat->qfeGetVisualParameter("Ortho data set", &param);
			if(param)
			{
				qfeSelectionList dataSet;
				param->qfeGetVisualParameterValue(dataSet);
				
				qfeStudy           *study;
				this->visualReformat->qfeGetVisualStudy(&study); 
 
				std::vector<qfeStudy::metric_descriptor> scalar_metrics;
				study->qfeGetScalarMetricList(scalar_metrics);
				
				for(unsigned int m = 0; m<scalar_metrics.size(); m++)
				{
					if(study->qfeScalarMetricExists(scalar_metrics.at(m).name))
					{
						dataSet.list.push_back(scalar_metrics.at(m).name);
					}
				}

				std::vector<qfeStudy::metric_descriptor> vector_metrics;
				study->qfeGetVectorMetricList(vector_metrics);
				
				for(unsigned int m = 0; m<vector_metrics.size(); m++)
				{
					if(study->qfeVectorMetricExists(vector_metrics.at(m).name))
					{
						dataSet.list.push_back(vector_metrics.at(m).name);
					}
				}
				
				param->qfeSetVisualParameterValue(dataSet); 
			}
		}
		if (t == visualPostProcess)
		{
			qfeVisualParameter *param;

			this->visPostProcess->qfeGetVisualParameter("SSAO", &param);
			if(param)param->qfeSetVisualParameterValue(true); 	
		}
	}

	return true;
}

void qfeDriverDA::qfeRenderStart()
{
	qfeVolume          *volume = NULL;
	qfeGrid            *grid   = NULL;
	qfePoint            origin;
	
	qfeVisualParameter *param;  
	qfeSelectionList    paramDataList;
	int                 paramData;
	std::string			paramName;
	
	if(this->visualReformat != NULL) 
	{
		this->visualReformat->qfeGetVisualParameter("Ortho data set", &param);
		if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
		paramData = paramDataList.active;
		paramName = paramDataList.list.at(paramData);
		
		int current;
		this->visualReformat->qfeGetVisualActiveVolume(current);
		this->qfeReformatGetDataSet(paramData, paramName, this->visualReformat, current, &volume);
	}
	
	if(volume != NULL)
	{
		this->qfeGetPlaneOrtho(volume, qfePlaneSagittal, &this->planeX);  
		this->qfeGetPlaneOrtho(volume, qfePlaneCoronal,  &this->planeY);
		this->qfeGetPlaneOrtho(volume, qfePlaneAxial,    &this->planeZ);
	}
	
	qfeViewport *vp3D;
	this->engine->GetViewport3D(&vp3D);
		
	this->engine->Refresh();
}

void qfeDriverDA::qfeRenderWait()
{
	//THE ORDER OF RENDERING IS IMPORTANT
	
	qfeColorMap *colormap;
	
	if(this->visualReformat != NULL) 
	{
		this->visualReformat->qfeGetVisualColorMap(&colormap);
		colormap->qfeUploadColorMap();   
	}
	
	// Render the reformat
	if(this->visualReformat != NULL)
	{
		this->qfeRenderVisualPlanesOrtho(this->visualReformat);    		
	}  
	
	//render seed volumes
	if(this->visualSeedVols != NULL)
	{
		this->qfeRenderVisualSeedVolumes(this->visualSeedVols);
	}

	//render Data Assimilation helpers
	if(this->visualDA != NULL)
	{
		this->qfeRenderVisualDataAssimilation(this->visualDA);
	}

	if(this->visualAnatomy != NULL)
	{
		this->qfeRenderVisualAnatomyIllustrative(this->visualAnatomy);
	}

	//render ink
	if(this->visualInk1 != NULL && this->visualInk2 != NULL)
	{
		this->qfeRenderVisualInk(this->visualDA, this->visualInk1, this->visualInk2);
	}
	// Render visual aides
	if(this->visAides != NULL)
	{
		this->qfeRenderVisualAides(this->visAides);
	}
		
	this->engine->Refresh();
};

void qfeDriverDA::qfeRenderSelect()
{
	qfeVolume    *volume;
	
	qfeVisualParameter *param;  
	qfeSelectionList    paramDataList;
	int                 paramData;
	std::string			paramName;
	bool                paramShowX, paramShowY, paramShowZ;
	
	qfeColorRGB   color;
	
	// Get the visual parameters
	this->visualReformat->qfeGetVisualParameter("Ortho data set", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
	paramData = paramDataList.active;
	paramName = paramDataList.list.at(paramData);
	
	this->visualReformat->qfeGetVisualParameter("Ortho plane RL (X)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);  
	
	this->visualReformat->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);    
	
	this->visualReformat->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);   
	
	// Process the visual parameters  
	int current;
	this->visualReformat->qfeGetVisualActiveVolume(current);
	this->qfeReformatGetDataSet(paramData, paramName, this->visualReformat, current, &volume);    
	
	if(paramShowX)
	{
		color.r = 1.0; color.g = 0.0; color.b = 0.0;
		this->qfeSetSelectColor(color);
		this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeX, NULL);
	}
	
	if(paramShowY)
	{
		color.r = 0.0; color.g = 1.0; color.b = 0.0;
		this->qfeSetSelectColor(color);
		this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeY, NULL);
	}
	
	if(paramShowZ)
	{
		color.r = 0.0; color.g = 0.0; color.b = 1.0;
		this->qfeSetSelectColor(color);
		this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeZ, NULL);
	}
}

//----------------------------------------------------------------------------
void qfeDriverDA::qfeRenderStop()
{
};

qfeReturnStatus qfeDriverDA::qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual)
{ 
	qfeStudy              *study;
	qfeVolume             *volume;
	qfeLightSource        *light;
	vtkPolyData           *mesh; 
	qfeMeshVertices       *vertices         = NULL;
	qfeMeshVertices       *normals          = NULL;
	qfeMeshIndices        *indicesTriangles = NULL;
	int                    current;

	visual->qfeGetVisualStudy(&study);  
	visual->qfeGetVisualActiveVolume(current);  

	if(study->segmentation.size()<=0)
		return qfeSuccess;

	qfeModel			  *model = new qfeModel();
	model->DeepCopy(study->segmentation[0]);

	this->scene->qfeGetSceneLightSource(&light);

	volume = study->pcap[current];

	if(volume == NULL)
	{
		cout << "qfeDriverFS::qfeRenderVisualAnatomyIllustrative - Incorrect volume data" << endl;
		return qfeError;
	}

	if(study == NULL)
	{
		cout << "qfeDriverFS::qfeRenderVisualAnatomyIllustrative - Incorrect study" << endl;
		return qfeError;
	}
		
	bool                surfaceClipping;

	// Get the parameters
	qfeSelectionList    paramSurfaceShadingList;
	int                 paramSurfaceShading;
	bool                paramSurfaceContour;

	qfeVisualParameter *param;  

	visual->qfeGetVisualParameter("Surface shading", &param);    
	if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceShadingList); 
	paramSurfaceShading       = paramSurfaceShadingList.active; 

	visual->qfeGetVisualParameter("Surface clipping", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(surfaceClipping);

	visual->qfeGetVisualParameter("Surface contour", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceContour); 

	bool                visible;  
	qfeColorRGB         colorSilhouette;
	qfeColorRGBA        colorContour;
	qfeColorRGBA        colorContourHidden;    

	// Process the parameter structures   
	colorContour.r       = 0.3; 
	colorContour.g       = 0.3;
	colorContour.b       = 0.3;
	colorContourHidden.r = 0.7;
	colorContourHidden.g = 0.7;
	colorContourHidden.b = 0.7;   

	// Set general variables for all meshes
	this->algorithmSurfaceShading->qfeSetLightSource(light);

	bool enableSurfaceClipping = surfaceClipping;
			
	model->qfeClipMesh(ROI_min.x, ROI_max.x, ROI_min.y, ROI_max.y, ROI_min.z, ROI_max.z);

	this->algorithmSurfaceShading->qfeSetSurfaceShading((qfeSurfaceShadingType)paramSurfaceShading);

	// Get the parameters
	model->qfeGetModelColor(colorSilhouette);        

	// Get the mesh    		
	model->qfeGetModelMesh(&mesh);    
	model->qfeGetModelMesh(&vertices, &normals);
		
	if(vertices->size() == 0)
		return qfeSuccess;
	
	model->qfeGetModelMeshIndicesTriangles(&indicesTriangles);
	model->qfeGetModelVisible(visible);

	enableSurfaceClipping = surfaceClipping;
		
	// Draw the mesh
	if(vertices != NULL && vertices->size() >= 3 && visible)
	{
		// Update the surface parameters      
		this->algorithmSurfaceShading->qfeSetSurfaceColor(colorSilhouette);
					if(paramSurfaceContour) 
		{
			this->algorithmSurfaceShading->qfeSetSurfaceCulling(true);
			this->algorithmSurfaceShading->qfeRenderMeshDilate(vertices, normals, indicesTriangles, 0.2, volume);
		}  
					
		// Render the mesh
			
		this->algorithmSurfaceShading->qfeSetSurfaceCulling(enableSurfaceClipping);
		this->algorithmSurfaceShading->qfeRenderMesh(vertices, normals, indicesTriangles, volume);            
		//this->algorithmSurfaceShading->qfeDrawMeshCurvature(mesh);
	
	}

	return qfeSuccess;
}

qfeReturnStatus qfeDriverDA::qfeRenderVisualSeedVolumes(qfeVisualSeedVolumes *visual)
{
	qfeViewport *vp3D;
	this->engine->GetViewport3D(&vp3D);

	vtkCamera *vtkcamera = this->engine->GetRenderer()->GetActiveCamera();

	qfeVisualParameter *param;	  

	bool visible = false;
	visual->qfeGetVisualParameter("Seed Volumes Visible", &param);
	if(param != NULL) 
		param->qfeGetVisualParameterValue(visible);
	
	if(!visible)
		return qfeSuccess;

	int current;

	qfeStudy		   *study;
	qfeVolume          *volume;	
	qfeGrid			   *grid;
	
	visual->qfeGetVisualStudy(&study);  
	visual->qfeGetVisualActiveVolume(current);
	
	if(study == NULL) return qfeError;

	if(study->pcap.size() > 0) 
		volume = study->pcap[current];
	else 
		return qfeError;

	if(volume == NULL) return qfeError;

	study->pcap[0]->qfeGetVolumeGrid(&grid);

	if(grid == NULL) return qfeError;

	std::array<float, 3> spacing;
	grid->qfeGetGridExtent(spacing[0],spacing[1],spacing[2]);

	float dx = max(spacing[0], max(spacing[1], spacing[2]));

	bool renderSurfaces = false;
	visual->qfeGetVisualParameter("Surfaces visible", &param);
	if(param != NULL) 
		param->qfeGetVisualParameterValue(renderSurfaces);

	this->flowSourceAndSinks->qfeRenderSeedVolumes(vtkcamera, vp3D->width, vp3D->height, this->pointSelected, renderSurfaces);

	if(this->newFlowSourceAndSink)
	{
		for(unsigned int i = 0; i<newFlowSourceAndSinkPoints.size(); i++)
		{			
			qfePoint p = newFlowSourceAndSinkPoints.at(i);

			this->flowSourceAndSinks->qfeRenderCube(p, dx/2.0f, true, qfeColorRGB(1.0f,0.5f,0.5f)); 
		}
	}

	return qfeSuccess;
}

qfeReturnStatus qfeDriverDA::qfeRenderVisualDataAssimilation(qfeVisualDataAssimilation *visual)
{
	qfeVisualParameter *param;

	bool paramVisible = false;

	visual->qfeGetVisualParameter("Visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);    

	if(!paramVisible)
		return qfeSuccess;
	
	qfeStudy		   *study;
	qfeVolume          *volume;
	
	qfeMatrix4f P2V, V2P;
	qfePoint    o;
	qfeGrid    *grid;
	int current;

	visual->qfeGetVisualStudy(&study);  
	visual->qfeGetVisualActiveVolume(current);

	if(study == NULL) return qfeError;

	if(study->pcap.size() > 0) 
		volume = study->pcap[current];
	else 
		return qfeError;

	if(volume == NULL) return qfeError;

	study->pcap[0]->qfeGetVolumeGrid(&grid);

	if(grid == NULL) return qfeError;
			
	std::array<float, 3> spacing;
	grid->qfeGetGridExtent(spacing[0],spacing[1],spacing[2]);

	float dx = max(spacing[0], max(spacing[1], spacing[2]));
	
	// Get the patient to voxel transformation
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
		
	unsigned int ni = this->sourceSinkVolume.ni;
	unsigned int nj = this->sourceSinkVolume.nj;
	unsigned int nk = this->sourceSinkVolume.nk;

	if(ni < 2 || nj < 2 || nk < 2)
	{
		std::cout<<"Source Sink Volume not defined"<<std::endl;
		return qfeError;
	}

	if(mesh_level_set.ni < 2 || mesh_level_set.nj < 2 || mesh_level_set.nk < 2)
	{
		std::cout<<"Mesh level set not defined"<<std::endl;
		return qfeError;
	}

	
	//get ROI:
	unsigned w = 1, h = 1, d = 1;

	if(study->pcap.size()>0)
		study->pcap.front()->qfeGetVolumeSize(w, h, d);

	int x0 = 0; int x1 = w;
	int y0 = 0; int y1 = h;
	int z0 = 0; int z1 = d;
	
	visual->qfeGetVisualParameter("Crop X1", &param); if(param != NULL) param->qfeGetVisualParameterValue(x0);
	visual->qfeGetVisualParameter("Crop X2", &param); if(param != NULL) param->qfeGetVisualParameterValue(x1);
	visual->qfeGetVisualParameter("Crop Y1", &param); if(param != NULL) param->qfeGetVisualParameterValue(y0);
	visual->qfeGetVisualParameter("Crop Y2", &param); if(param != NULL) param->qfeGetVisualParameterValue(y1);
	visual->qfeGetVisualParameter("Crop Z1", &param); if(param != NULL) param->qfeGetVisualParameterValue(z0);
	visual->qfeGetVisualParameter("Crop Z2", &param); if(param != NULL) param->qfeGetVisualParameterValue(z1);

	ROI_min.x = (unsigned int) x0;
	ROI_max.x = (unsigned int) x1;
	ROI_min.y = (unsigned int) y0;
	ROI_max.y = (unsigned int) y1;
	ROI_min.z = (unsigned int) z0;
	ROI_max.z = (unsigned int) z1;

	if(ROI_min.x > ROI_max.x) ROI_min.x = ROI_max.x;
	if(ROI_min.y > ROI_max.y) ROI_min.y = ROI_max.y;
	if(ROI_min.z > ROI_max.z) ROI_min.z = ROI_max.z;
	
	for(unsigned int k = ROI_min.z; k<ROI_max.z; k++)
	for(unsigned int j = ROI_min.y; j<ROI_max.y; j++)
	for(unsigned int i = ROI_min.x; i<ROI_max.x; i++)
	{
		bool inside_mesh = mesh_level_set.at(i,j,k) >= 0.0f;

		//check if special voxel
		if(sourceSinkVolume(i,j,k) == 0)
		{
			continue;
		}
		else //this voxel should be marked
		{
			qfeColorRGB col = this->flowSourceAndSinks->sourceColor;
			
			if(sourceSinkVolume(i,j,k) == seedVolumeTypeSource)
			{
				col = this->flowSourceAndSinks->sourceColor;
			}
			
			if(sourceSinkVolume(i,j,k) == seedVolumeTypeSink)
			{
				col = this->flowSourceAndSinks->sinkColor;
			}

			if(inside_mesh) //darken color
			{
				col.r = col.r/1.5f;
				col.g = col.g/1.5f;			
				col.b = col.b/1.5f;
			}
			
			qfePoint vp = qfePoint(i,j,k);
			qfePoint position = vp*V2P;

			this->flowSourceAndSinks->qfeRenderCube(position, dx/2.0f, false, col); 
		}
	}

	bool renderCropping = false;
	visual->qfeGetVisualParameter("Render cropping", &param); if(param != NULL) param->qfeGetVisualParameterValue(renderCropping);

	if(!renderCropping)
		return qfeSuccess;

	//render ROI
	qfePoint position000 = qfePoint(ROI_min.x, ROI_min.y, ROI_min.z)*V2P;	
	qfePoint position001 = qfePoint(ROI_min.x, ROI_min.y, ROI_max.z)*V2P;
	qfePoint position010 = qfePoint(ROI_min.x, ROI_max.y, ROI_min.z)*V2P;
	qfePoint position011 = qfePoint(ROI_min.x, ROI_max.y, ROI_max.z)*V2P;
	
	qfePoint position100 = qfePoint(ROI_max.x, ROI_min.y, ROI_min.z)*V2P;
	qfePoint position101 = qfePoint(ROI_max.x, ROI_min.y, ROI_max.z)*V2P;
	qfePoint position110 = qfePoint(ROI_max.x, ROI_max.y, ROI_min.z)*V2P;
	qfePoint position111 = qfePoint(ROI_max.x, ROI_max.y, ROI_max.z)*V2P;

	this->flowSourceAndSinks->qfeRenderCube(position000, dx/2.0f, false, qfeColorRGB(1.0, 1.0, 1.0));
	this->flowSourceAndSinks->qfeRenderCube(position001, dx/2.0f, false, qfeColorRGB(1.0, 1.0, 1.0));
	this->flowSourceAndSinks->qfeRenderCube(position010, dx/2.0f, false, qfeColorRGB(1.0, 1.0, 1.0));
	this->flowSourceAndSinks->qfeRenderCube(position011, dx/2.0f, false, qfeColorRGB(1.0, 1.0, 1.0));
	
	this->flowSourceAndSinks->qfeRenderCube(position100, dx/2.0f, false, qfeColorRGB(1.0, 1.0, 1.0));
	this->flowSourceAndSinks->qfeRenderCube(position101, dx/2.0f, false, qfeColorRGB(1.0, 1.0, 1.0));
	this->flowSourceAndSinks->qfeRenderCube(position110, dx/2.0f, false, qfeColorRGB(1.0, 1.0, 1.0));
	this->flowSourceAndSinks->qfeRenderCube(position111, dx/2.0f, false, qfeColorRGB(1.0, 1.0, 1.0));

	glLineWidth(3.0); 

	glColor4f(1.0f, 1.0f, 1.0f, 0.25f); 

	glBegin(GL_LINE_LOOP);	  
		glVertex3f(position001.x,position001.y,position001.z);
		glVertex3f(position101.x,position101.y,position101.z);
		glVertex3f(position111.x,position111.y,position111.z);
		glVertex3f(position011.x,position011.y,position011.z);
	  glEnd();

	  glBegin(GL_LINE_LOOP);	  
		glVertex3f(position000.x,position000.y,position000.z);
		glVertex3f(position100.x,position100.y,position100.z);
		glVertex3f(position110.x,position110.y,position110.z);
		glVertex3f(position010.x,position010.y,position010.z);
	  glEnd();

	  glBegin(GL_LINES);
		glVertex3f(position001.x,position001.y,position001.z);
		glVertex3f(position000.x,position000.y,position000.z);

		glVertex3f(position101.x,position101.y,position101.z);
		glVertex3f(position100.x,position100.y,position100.z);

		glVertex3f(position111.x,position111.y,position111.z);
		glVertex3f(position110.x,position110.y,position110.z);

		glVertex3f(position011.x,position011.y,position011.z);
		glVertex3f(position010.x,position010.y,position010.z);
	  glEnd();

	return qfeSuccess;
}

qfeReturnStatus qfeDriverDA::qfeRenderVisualPlanesOrtho(qfeVisualReformat *visual)
{
	qfeStudy		   *study;
	qfeVolume          *volume;
	qfeGrid            *grid;
	qfeColorMap        *colormap;
	qfePoint            origin;
	
	qfeVisualParameter *param;  
	qfeSelectionList    paramDataList;
	qfeSelectionList    paramCompList;
	qfeSelectionList    paramReprList;
	qfeSelectionList    paramInterpList;
	int                 paramData;
	std::string			paramName;
	int                 paramComp;
	int                 paramRepr;
	int                 paramInterp;
	double              paramBrightness;
	double              paramContrast;
	int                 paramTransparency;
	qfeFloat            paramRange[2];
	bool                paramShowX, paramShowY, paramShowZ;
	int                 current;
	qfeColorRGB         colorX, colorY, colorZ;
	qfeFloat            tt = 0.0f;
	
	visual->qfeGetVisualStudy(&study);
	visual->qfeGetVisualActiveVolume(current);
	visual->qfeGetVisualColorMap(&colormap);
	
	visual->qfeGetVisualStudy(&study);
	visual->qfeGetVisualActiveVolume(current);

	if(!additional_data_computed)
	{
		this->qfeComputeMeshLevelsetVolume();

		this->qfeComputeSNRData();
		this->qfeComputeSNRVolumes();
		this->qfeComputeSignalStrengthVolumes();

		this->qfeComputeWSSData();
		this->qfeComputeWSSVolumes();

		this->additional_data_computed = true;
	}
	
	// Get visual parameters
	visual->qfeGetVisualParameter("Ortho data set", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
	paramData = paramDataList.active;
	paramName = paramDataList.list.at(paramData);
	
	visual->qfeGetVisualParameter("Ortho data vector", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
	paramComp = paramCompList.active;
	
	visual->qfeGetVisualParameter("Ortho data representation", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
	paramRepr = paramReprList.active;
	
	visual->qfeGetVisualParameter("Ortho data interpolation", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramInterpList);  
	paramInterp = paramInterpList.active;
	
	visual->qfeGetVisualParameter("Ortho plane RL (X)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);   
	
	visual->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);    

	visual->qfeGetVisualParameter("Ortho transparency", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramTransparency);    
	
	visual->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);  
	
	visual->qfeGetVisualParameter("Ortho contrast", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);   

	visual->qfeGetVisualParameter("Ortho brightness", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);   

	// Process parameters
	visual->qfeGetVisualActiveVolume(current);
	this->qfeReformatGetDataSet(paramData, paramName, visual, current, &volume);  
	
	if(volume != NULL)
	{
		volume->qfeGetVolumeGrid(&grid);
		volume->qfeGetVolumeTriggerTime(tt);
		grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);    

		//volume->qfeGetVolumeType(paramComp);
		volume->qfeSetVolumeTextureId(-1);
		volume->qfeUpdateVolume();
	}
	else
	{
		origin.x = origin.y = origin.z = 0.0;
	}

	switch(paramData)
	{
	case 0 : 
		study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
		break;
	case 1 : 
		study->qfeGetStudyRangePCAM(paramRange[0], paramRange[1]);
		break;
	case 2 : 
		study->qfeGetStudyRangeSSFP(paramRange[0], paramRange[1]);
		break;
	case 3 : 
		if(study->tmip != NULL)
			study->tmip->qfeGetVolumeValueDomain(paramRange[0], paramRange[1]);
		break;
	case 4 : 
		study->qfeGetStudyRangeFFE(paramRange[0], paramRange[1]);
		break;
	case 5:  
		study->qfeGetStudyRangeFTLE(paramRange[0], paramRange[1]);
		break;
	default:
		volume->qfeGetVolumeValueDomain(paramRange[0], paramRange[1]);
		break;
	/*case 6:  
		paramRange[0] = SNR_Range[0];
		paramRange[1] = SNR_Range[1];
		break;
	case 7:  
		paramRange[0] = Signal_Strength_Range[0];
		paramRange[1] = Signal_Strength_Range[1];
		break;
	
	case(8) : // segmentation levelset
		paramRange[0] = meshLevelSetRange[0];
		paramRange[1] = meshLevelSetRange[1];
		break;
	default: 
		study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
		break;  
		*/
	}  

	// Determine line color
	colorX = colorY = colorZ = this->colorReformatDefault;
	if(this->selectedPlane == qfePlaneX)  colorX = this->colorReformatHighlight;
	if(this->selectedPlane == qfePlaneY)  colorY = this->colorReformatHighlight;
	if(this->selectedPlane == qfePlaneZ)  colorZ = this->colorReformatHighlight;
	
	// Update algorithm parameters    
	this->algorithmReformat->qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
	this->algorithmReformat->qfeSetPlaneDataComponent(paramComp);
	this->algorithmReformat->qfeSetPlaneDataRepresentation(paramRepr);
	this->algorithmReformat->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
	this->algorithmReformat->qfeSetPlaneTriggerTime(tt); 
	this->algorithmReformat->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
	this->algorithmReformat->qfeSetPlaneBrightness(paramBrightness);
	this->algorithmReformat->qfeSetPlaneContrast(paramContrast);
	this->algorithmReformat->qfeSetPlaneOpacity(1.0-(paramTransparency/100.0));
	
	// Render the planes
	if(paramShowX)
	{
		this->algorithmReformat->qfeSetPlaneBorderColor(colorX);
		this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeX, colormap);      
	}

	if(paramShowY) 
	{ 
		this->algorithmReformat->qfeSetPlaneBorderColor(colorY);
		this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeY, colormap);      
	}

	if(paramShowZ)
	{  
		this->algorithmReformat->qfeSetPlaneBorderColor(colorZ);
		this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeZ, colormap);   
	}  

	return qfeSuccess;
}

float qfeDriverDA::qfeGetProbability(float value, float rangeMin, float rangeMax, QVector<QPair<double, double>> &tf)
{
  float valueNorm = (value - rangeMin) / (rangeMax - rangeMin);
  float before = tf.first().first;
  float pBefore = tf.first().second;
  float after;
  float pAfter;
  for (unsigned i = 1; i != tf.size(); i++) {
	if (tf[i].first >= valueNorm) {
	  after = tf[i].first;
	  pAfter = tf[i].second;
	  break;
	}
	before = tf[i].first;
	pBefore = tf[i].second;
  }

  float t = (valueNorm - before) / (after - before);
  return pBefore + t * (pAfter - pBefore);
}

void qfeDriverDA::qfeComputeSeeding(qfeVisualDataAssimilation *visual_da)
{
	particle seed;

	std::random_device rd;
	std::mt19937 generator(rd());
	std::uniform_real_distribution<> distribution(0, 1);

	QTransferFunctionOpacity tf = scene->qfeGetSceneFeatureTF(0);

	qfeVisualParameter *param;	
	qfeSelectionList    paramList;

	static int selectedMetric = -1;

	//start setting up both visualizations with the settings of visual1
	visual_da->qfeGetVisualParameter("Feature", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramList);
	std::string metricName = paramList.list.at(paramList.active);
	
	int currentTime;
	visual_da->qfeGetVisualActiveVolume(currentTime);

	qfeStudy *study = new qfeStudy();
	visual_da->qfeGetVisualStudy(&study);

	bool exists;
	float range_min, range_max;

	Array3f metric;

	bool useFeature;
	visual_da->qfeGetVisualParameter("Use feature based", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(useFeature);

	static bool alreadyComputed = false;
	static bool usedFeature = false;

	if(seeds.size() == 0)
		alreadyComputed = false;

	//if(this->algorithmIP1->qfeNumberOfParticles() == 0)
		//alreadyComputed = false;

	if(useFeature != usedFeature)
	{
		usedFeature = useFeature;
		alreadyComputed = false;
	}

	if(useFeature && paramList.active != selectedMetric)
	{
		selectedMetric = paramList.active;
		
		alreadyComputed = false;
	}

	if(useFeature && !alreadyComputed)
	{
		seeds.clear();

		study->qfeGetScalarMetric(metricName, currentTime, exists, metric, range_min, range_max);

		if(exists == false)
		{
			seeds.clear();
			return;
		}

		float color_id = 0;

		qfeMatrix4f V2P;
		qfeTransform::qfeGetMatrixVoxelToPatient(V2P, study->pcap[0]);

		for(unsigned int k = 0; k<metric.nk; k++)
		{
			for(unsigned int j = 0; j<metric.nj; j++)
			{
				for(unsigned int i = 0; i<metric.ni; i++)
				{
					float sample = metric(i,j,k);

					float probability = distribution(generator);

					float value = this->qfeGetProbability(sample, range_min, range_max, tf);

					//to avoid adding seeds everywhere, we exclude where the probability was 0 to begin with
					//for the remaining voxels we use the tf
					if(sample>0.0f && value>probability)
					{
						seed.position = qfePoint((float)i,(float)j,(float)k)* V2P;
						seed.age = 0.0f;
					
						seed.seed_time = this->currentTime;
						seed.seed_id1 = 0.0f;
						seed.seed_id2 = 1.0f;
						seed.deviation = 0.0f;
				
						seeds.push_back(seed);
					}
				}
			}
		}

		std::cout<<"Feature-based seed amount: "<<seeds.size()<<std::endl;
	}

	if(!useFeature && !alreadyComputed)
	{
		seeds.clear();
		this->algorithmIP1->qfeClearParticlesFeatureBased();

		//this->algorithmIP1->qfeClearParticles();

		double seedFromFixedPositionMultiplier = 1.0;
		visual_da->qfeGetVisualParameter("Fixed seed positions multiplier", &param);
			if(param != NULL) param->qfeGetVisualParameterValue(seedFromFixedPositionMultiplier);	

		this->algorithmIP1->qfeSeedParticlesWholeMesh(false, true, (float)seedFromFixedPositionMultiplier);

		seeds = *(this->algorithmIP1->getFixedSeedPositions());

		std::cout<<"Regular whole mesh seed amount: "<<seeds.size()<<std::endl;
	}

	alreadyComputed = true;
}

qfeReturnStatus qfeDriverDA::qfeRenderVisualInk(qfeVisualDataAssimilation *visual_da, qfeVisualInkVis *visual1, qfeVisualInkVisSecondary *visual2)
{
	qfeViewport *vp3D;
	this->engine->GetViewport3D(&vp3D);

	std::array<float, 3> spacing;
	qfeStudy *study = new qfeStudy();
	visual1->qfeGetVisualStudy(&study);
	qfeGrid *grid = new qfeGrid();
	study->pcap[0]->qfeGetVolumeGrid(&grid);
	grid->qfeGetGridExtent(spacing[0],spacing[1],spacing[2]);

	int visualInkVis = 0;
	qfeVisualParameter *param;	  
	qfeSelectionList    paramList;

	qfeColorMap *colormap1;
	qfeColorMap *colormap2;

	qfeComputeSeeding(visual_da);

	visual1->qfeGetVisualColorMap(&colormap1);
	this->algorithmIP1->qfeUpdateColorMap(colormap1);

	visual2->qfeGetVisualColorMap(&colormap2);
	this->algorithmIP2->qfeUpdateColorMap(colormap2);

	bool render1 = false;
	visual1->qfeGetVisualParameter("Ink visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(render1);

	bool render2 = false;
	visual2->qfeGetVisualParameter("Ink2 visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(render2);

	render2 = render2 && this->visualInk2_initialized;

	//start setting up both visualizations with the settings of visual1
	visual1->qfeGetVisualParameter("Advection type", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramList);

	this->algorithmIP1->qfeSetAdvectionType((advection_type) paramList.active);
	this->algorithmIP2->qfeSetAdvectionType((advection_type) paramList.active);

	bool clip_outside_mesh = false;
	this->algorithmIP1->qfeClipOutsideMesh(clip_outside_mesh, 0.0f);
	this->algorithmIP2->qfeClipOutsideMesh(clip_outside_mesh, 0.0f);

	double particle_size = 1.0;
	visual1->qfeGetVisualParameter("Particle size", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(particle_size);

	particle_size = (particle_size*spacing[2])/2.5f;

	double particle_multiplier = 1.0;
	visual1->qfeGetVisualParameter("Particle multiplier", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(particle_multiplier);	
	//this->algorithmIP1->qfeSetParticleMultiplier((float)particle_multiplier);
	//this->algorithmIP2->qfeSetParticleMultiplier((float)particle_multiplier);

	double particle_life = 1.0;
	visual1->qfeGetVisualParameter("Particle lifetime (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(particle_life);  
	this->algorithmIP1->qfeSetParticleLifeTime(particle_life);
	this->algorithmIP2->qfeSetParticleLifeTime(particle_life);

	double step_size = 0.0;
	visual1->qfeGetVisualParameter("Animation step size (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(step_size);
	this->algorithmIP1->qfeSetTimeStepSize(step_size);
	this->algorithmIP2->qfeSetTimeStepSize(step_size);
	
	double animation_length = 0.0;
	visual1->qfeGetVisualParameter("Animation length (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(animation_length);  
	animation_duration = animation_length;	  

	double stop_seeding_after = 0.0;
	visual1->qfeGetVisualParameter("Stop seeding (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(stop_seeding_after);  
	this->algorithmIP1->qfeSetStopSeedingTime(stop_seeding_after);
	this->algorithmIP2->qfeSetStopSeedingTime(stop_seeding_after);

	bool inverse_colorscheme = false;
	visual1->qfeGetVisualParameter("Inverse colorscheme", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(inverse_colorscheme);
	this->algorithmIP1->qfeInverseColorMap(inverse_colorscheme);
	this->algorithmIP2->qfeInverseColorMap(inverse_colorscheme);
	
	visual1->qfeGetVisualParameter("Color setting", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramList);
	this->algorithmIP1->qfeSetColorSetting(paramList.active);
	this->algorithmIP2->qfeSetColorSetting(paramList.active);

	int glyph_type = 0;
	visual1->qfeGetVisualParameter("Glyph type", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramList);
	glyph_type = paramList.active;

	int glyph_frequency = 0;
	visual1->qfeGetVisualParameter("Glyph frequency", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(glyph_frequency); 

	visual1->qfeGetVisualActiveVolume(currentTime);
	this->algorithmIP1->qfeSetPhase(currentTime);
	this->algorithmIP2->qfeSetPhase(currentTime);

	vtkCamera *vtkcamera = this->engine->GetRenderer()->GetActiveCamera();
	std::array<double,3> camera;
	vtkcamera->GetPosition(camera[0], camera[1], camera[2]);
	
	std::array<double,3> viewUp;
	vtkcamera->GetViewUp(viewUp[0], viewUp[1], viewUp[2]);
	
	std::array<double,3> viewDir;
	vtkcamera->GetDirectionOfProjection(viewDir[0], viewDir[1], viewDir[2]);

	this->algorithmIP1->setFixedSeedPositions(&seeds);
	this->algorithmIP2->setFixedSeedPositions(&seeds);

	if(render1)
		this->algorithmIP1->qfeRenderParticlesGPUFriendly((float)particle_size, viewDir, false, glyph_type, ceil(glyph_frequency*particle_life));	
	
	if(render2)
		this->algorithmIP2->qfeRenderParticlesGPUFriendly((float)particle_size, viewDir, false, glyph_type, ceil(glyph_frequency*particle_life));

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverDA::qfeRenderVisualAides(qfeVisualAides *visual)
{
	qfeStudy           *study;
	qfeVolume          *volume;
	int                 current;

	qfeVisualParameter *param;
	bool                paramSupportBoundingBox = true;
	bool                paramSupportAxes        = false;

	this->visAides->qfeGetVisualStudy(&study);  
	this->visAides->qfeGetVisualActiveVolume(current);

	if(study == NULL) return qfeError;

	if(study->pcap.size() > 0) 
		volume = study->pcap[current];
	else return qfeError;

	this->visAides->qfeGetVisualParameter("Support bounding box", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramSupportBoundingBox); 

	this->visAides->qfeGetVisualParameter("Support axes", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramSupportAxes); 

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LINE_SMOOTH);

	if(paramSupportBoundingBox == true) 
		qfeDriverAide::qfeRenderBoundingBoxDashed(volume, this->currentModelView, 0.8*this->frameBufferProps[0].supersampling);
	if(paramSupportAxes        == true) qfeDriverAide::qfeRenderVolumeAxes(volume, 10.0, 1.6*this->frameBufferProps[0].supersampling);

	glDisable(GL_LINE_SMOOTH);
	glDisable(GL_BLEND); 

	return qfeSuccess;
}

//get SNR volume data:
qfeReturnStatus qfeDriverDA::qfeComputeSNRData()
{
	qfeStudy *study;
	if(this->visualReformat != NULL) 
	{
		this->visualReformat->qfeGetVisualStudy(&study);  
	}
	else
	{
		return qfeError;
	}

	Array3f result;
	float sigma = 0.0f;
	float range_min = 0.0f; 
	float range_max = 0.0f;
	
	for(unsigned int t = 0; t<study->pcap.size(); t++)
	{
		study->qfeGetSNR((float)t, result, sigma, range_min, range_max);
		
		SNR_data.push_back(result);
		sigma_data.push_back(sigma);
	}
}

//get mesh levelset volume:
qfeReturnStatus qfeDriverDA::qfeComputeMeshLevelsetVolume()
{	
	std::cout<<"Pre computing mesh levelset"<<std::endl;

	qfeStudy *study;
	if(this->visualReformat != NULL) 
	{
		this->visualReformat->qfeGetVisualStudy(&study);  
	}
	else
	{
		return qfeError;
	}
	
	if(study->segmentation.size()>0) //load the mesh
	{
		bool exists;
		float range_min, range_max;
		study->qfeGetScalarMetric("Solid SDF", 0.0f, exists, this->mesh_level_set, range_min, range_max);

		if(!exists)
			return qfeError;

		this->qfeUpdateSourceSinkVolume();
	}
	else
	{
		return qfeError;
	}

	std::cout<<"Computed mesh levelset"<<std::endl;

	//init volume:
	qfeGrid *grid;
	study->pcap[0]->qfeGetVolumeGrid(&grid);
	meshLevelsetVolume.qfeSetVolumeComponentsPerVoxel(3);
	meshLevelsetVolume.qfeSetVolumeGrid(grid);

	unsigned int ni = this->mesh_level_set.ni;
	unsigned int nj = this->mesh_level_set.nj;
	unsigned int nk = this->mesh_level_set.nk;

	float *data = new float[3 * ni * nj * nk];

	float max = 0.0f;
	
	for(unsigned int k = 0; k<nk; k++)
	for(unsigned int j = 0; j<nj; j++)
	for(unsigned int i = 0; i<ni; i++)
	{
		unsigned int offset = 3 * ((int)i + (int)j*ni + (int)k*ni*nj);
						
		float value = this->mesh_level_set.at(i,j,k);

		data[offset+0] = value;
		data[offset+1] = 0.0f;
		data[offset+2] = 0.0f;

		if(abs(value) > max)
			max = abs(value);
	}

	
	meshLevelSetRange[0] = -max;
	meshLevelSetRange[1] = max;

	meshLevelsetVolume.qfeSetVolumeData(qfeValueTypeFloat, ni, nj, nk, data);
	meshLevelsetVolume.qfeSetVolumeLabel("Mesh levelset");
	meshLevelsetVolume.qfeSetVolumeValueDomain(-max, max);
		
	meshLevelsetVolume.qfeSetHistogramToNull();

	meshLevelsetVolume.qfeUpdateVolume();

		
	delete[] data;

	return qfeSuccess;
}

//get SNR volume data:
qfeReturnStatus qfeDriverDA::qfeComputeWSSData()
{
	qfeStudy *study;
	if(this->visualReformat != NULL) 
	{
		this->visualReformat->qfeGetVisualStudy(&study);  
	}
	else
	{
		return qfeError;
	}

	Array3Vec3 result;
	
	for(unsigned int t = 0; t<study->pcap.size(); t++)
	{		
		bool exists = false;

		study->qfeGetVectorMetric("WSS 1", &this->mesh_level_set, (float)t, exists, result);
		
		qfeVolume volume;
		qfeGrid *grid;
		study->pcap.at(0)->qfeGetVolumeGrid(&grid);
		
		WSS_data.push_back(result);
	}
}

//get SNR volume data:
qfeReturnStatus qfeDriverDA::qfeComputeSNRVolumes()
{	
	qfeStudy *study;
	if(this->visualReformat != NULL) 
	{
		this->visualReformat->qfeGetVisualStudy(&study);  
	}
	else
	{
		return qfeError;
	}

	this->SNR_Volumes.clear();

	float max = 0.0f;
	
	for(unsigned int id = 0; id<SNR_data.size(); id++)
	{
		float *data = new float[3 * SNR_data[id].ni * SNR_data[id].nj * SNR_data[id].nk];

		qfeVolume SNRVolume;
		
		//init volume:		
		qfeGrid *grid;
		study->pcap[0]->qfeGetVolumeGrid(&grid);
		SNRVolume.qfeSetVolumeComponentsPerVoxel(3);
		SNRVolume.qfeSetVolumeGrid(grid);
		
		for(unsigned int k = 0; k<SNR_data[id].nk; k++)
		for(unsigned int j = 0; j<SNR_data[id].nj; j++)
		for(unsigned int i = 0; i<SNR_data[id].ni; i++)
		{
			unsigned int offset = 3*((int)i + (int)j*SNR_data[id].ni + (int)k*SNR_data[id].ni*SNR_data[id].nj);
						
			if(SNR_data[id].at(i,j,k)>max)
				max = SNR_data[id].at(i,j,k);
			
			data[offset+0] = SNR_data[id].at(i,j,k);
			data[offset+1] = 0.0f;
			data[offset+2] = 0.0f;
		}

		SNRVolume.qfeSetVolumeData(qfeValueTypeFloat, SNR_data[id].ni, SNR_data[id].nj, SNR_data[id].nk, data);
		SNRVolume.qfeSetVolumeLabel("SNR");
		
		SNRVolume.qfeUpdateVolume();

		this->SNR_Volumes.push_back(SNRVolume);
		
		delete[] data;
	}

	SNR_Range[0] = 0.0f;
	SNR_Range[1] = max;

	return qfeSuccess;
}

//get signal strength volume data:
qfeReturnStatus qfeDriverDA::qfeComputeSignalStrengthVolumes()
{
	qfeStudy *study;
	if(this->visualReformat != NULL) 
	{
		this->visualReformat->qfeGetVisualStudy(&study);  
	}
	else
	{
		return qfeError;
	}

	Signal_Strength_Volumes.clear();
	
	float max = 0.0f;
	
	for(unsigned int id = 0; id<SNR_data.size(); id++)
	{
		float *data = new float[3 * SNR_data[id].ni * SNR_data[id].nj * SNR_data[id].nk];

		qfeVolume SignalVolume;

		//init volume:		
		qfeGrid *grid;
		study->pcap[0]->qfeGetVolumeGrid(&grid);
		SignalVolume.qfeSetVolumeComponentsPerVoxel(3);
		SignalVolume.qfeSetVolumeGrid(grid);
		
		for(unsigned int k = 0; k<SNR_data[id].nk; k++)
		for(unsigned int j = 0; j<SNR_data[id].nj; j++)
		for(unsigned int i = 0; i<SNR_data[id].ni; i++)
		{
			unsigned int offset = 3*((int)i + (int)j*SNR_data[id].ni + (int)k*SNR_data[id].ni*SNR_data[id].nj);
						
			if(SNR_data[id].at(i,j,k) * this->sigma_data[id]>max)
				max = SNR_data[id].at(i,j,k) * this->sigma_data[id];

			data[offset+0] = SNR_data[id].at(i,j,k) * this->sigma_data[id]; //convert to signal strength
			data[offset+1] = 0.0f;
			data[offset+2] = 0.0f;
		}

		SignalVolume.qfeSetVolumeData(qfeValueTypeFloat, SNR_data[id].ni, SNR_data[id].nj, SNR_data[id].nk, data);
		SignalVolume.qfeSetVolumeLabel("Signal strength");
		
		SignalVolume.qfeUpdateVolume();

		Signal_Strength_Volumes.push_back(SignalVolume);
		
		delete[] data;
	}

	Signal_Strength_Range[0] = 0.0f;
	Signal_Strength_Range[1] = max;

	return qfeSuccess;
}

//get SNR volume data:
qfeReturnStatus qfeDriverDA::qfeComputeWSSVolumes()
{	
	qfeStudy *study;
	if(this->visualReformat != NULL) 
	{
		this->visualReformat->qfeGetVisualStudy(&study);  
	}
	else
	{
		return qfeError;
	}

	this->WSS_Volumes.clear();
		
	for(unsigned int id = 0; id<WSS_data.size(); id++)
	{
		float *data = new float[3 * WSS_data[id].ni * WSS_data[id].nj * WSS_data[id].nk];

		qfeVolume WSSVolume;
		
		//init volume:		
		qfeGrid *grid;
		study->pcap[0]->qfeGetVolumeGrid(&grid);
		WSSVolume.qfeSetVolumeComponentsPerVoxel(3);
		WSSVolume.qfeSetVolumeGrid(grid);
		
		for(unsigned int k = 0; k<WSS_data[id].nk; k++)
		for(unsigned int j = 0; j<WSS_data[id].nj; j++)
		for(unsigned int i = 0; i<WSS_data[id].ni; i++)
		{
			unsigned int offset = 3*((int)i + (int)j*WSS_data[id].ni + (int)k*WSS_data[id].ni*WSS_data[id].nj);
			
			data[offset+0] = WSS_data[id].at(i,j,k)[0];
			data[offset+1] = WSS_data[id].at(i,j,k)[1];
			data[offset+2] = WSS_data[id].at(i,j,k)[2];
		}

		WSSVolume.qfeSetVolumeData(qfeValueTypeFloat, WSS_data[id].ni, WSS_data[id].nj, WSS_data[id].nk, data);
		WSSVolume.qfeSetVolumeLabel("WSS");
		
		WSSVolume.qfeUpdateVolume();

		this->WSS_Volumes.push_back(WSSVolume);
		
		delete[] data;
	}

	return qfeSuccess;
}

qfeReturnStatus qfeDriverDA::qfeReformatGetDataSet(int index, std::string name, qfeVisual *visual, int time, qfeVolume **volume)
{
	*volume = NULL;
	
	qfeStudy *study;
	visual->qfeGetVisualStudy(&study);  
	
	if(study == NULL) 
		return qfeError;
	
	switch(index)
	{
	case(0) : // PCA-P
		if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time)) 
			*volume = study->pcap[time];
		break;
	case(1) : // PCA-M
		if(((int)study->pcam.size() > 0) && ((int)study->pcam.size() > time)) 
			*volume = study->pcam[time];
		break;
	case(2) : // SSFP
		if((int)study->ssfp3D.size() > 0) 
		{
			// If there is also pcap data loaded, check for the trigger time and load
			// accompanying ssfp data at the right time slot 
			if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time)) 
			{
				qfeFloat att, ptt;
				qfeFloat distance = 9000;
				(int)study->pcap[time]->qfeGetVolumeTriggerTime(ptt);
				for(int i=0; i<(int)study->ssfp3D.size(); i++)
				{
					(int)study->ssfp3D[i]->qfeGetVolumeTriggerTime(att);
					if(abs(att-ptt) < distance)
					{
						distance = abs(att-ptt);
						*volume = study->ssfp3D[i];
					}
				}
			}
			else
				*volume = study->ssfp3D[0];
		}
		break;
	case(3) : // T-MIP
		if(study->tmip != NULL) 
			*volume = study->tmip;
		break;
	case(4) : // FFE
		if(((int)study->ffe.size() > 0) && ((int)study->ffe.size() > time)) 
			*volume = study->ffe[time];
		break;
	case(5) : // FTLE
		if(((int)study->ftle.size() > 0) && ((int)study->ftle.size() > time))
		{
			*volume = study->ftle[time];
		}
	default : // metric
		if(study->qfeScalarMetricExists(name))
		{
			bool existing_metric = true;
			study->qfeGetScalarVolume(name, time, existing_metric, volume);
		}
		else if(study->qfeVectorMetricExists(name))
		{
			bool existing_metric = true;
			study->qfeGetVectorVolume(name, time, existing_metric, volume);
		}
		else if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time))
			*volume = study->pcap[time];
		break;
		/*
	case(6) : // SNR
		if(((int)SNR_Volumes.size() > 0) && ((int)SNR_Volumes.size() > time))
		{
			*volume = &SNR_Volumes[time];
		}
		break;
	case(7) : // Signal strength
		if(((int)Signal_Strength_Volumes.size() > 0) && ((int)Signal_Strength_Volumes.size() > time))
		{
			*volume = &Signal_Strength_Volumes[time];
		}
		break;
	case(8) : // Mesh levelset
		if(study->segmentation.size()>0)
		{			
			*volume = &meshLevelsetVolume;
		}
		break;
	case(9) : // WSS
		if(study->segmentation.size()>0)
		{			
			*volume = &WSS_Volumes[time];
		}
		break;
	default : // PCA-P
		if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time)) 
			*volume = study->pcap[time];
		break;
		*/
  }

	if(*volume == NULL)
	{
		*volume = study->pcap[time];
		std::cout<<"Could not load data to display"<<std::endl;
	}

  return qfeSuccess;
}

qfeReturnStatus qfeDriverDA::qfeGetPlaneOrtho(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice)
{
	qfeMatrix4f P2V, V2P;
	qfeVector   axisX, axisY, axisZ, axisXabs, axisYabs, axisZabs, x, y, z;
	qfePoint    o;
	qfeGrid    *grid;
	
	if(volume == NULL) return qfeError;
	
	// Get the patient to voxel transformation
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
	
	// Set the plane normal direction in patient coordinates
	switch(dir)
	{
	case qfePlaneAxial       :	axisX.qfeSetVectorElements(1.0,0.0,0.0);
								axisY.qfeSetVectorElements(0.0,1.0,0.0);
								axisZ.qfeSetVectorElements(0.0,0.0,1.0);      
								break;
	case qfePlaneSagittal    :	axisX.qfeSetVectorElements(0.0,0.0,1.0);
								axisY.qfeSetVectorElements(0.0,1.0,0.0);
								axisZ.qfeSetVectorElements(1.0,0.0,0.0); 
								break;
	case qfePlaneCoronal     :	axisX.qfeSetVectorElements(1.0,0.0,0.0);
								axisY.qfeSetVectorElements(0.0,0.0,1.0);
								axisZ.qfeSetVectorElements(0.0,1.0,0.0); 
								break;
	}  
	
	// Convert the axis to voxel coordinates and find dominant direction
	axisX = axisX*P2V;
	axisY = axisY*P2V;
	axisZ = axisZ*P2V;
	
	axisXabs.qfeSetVectorElements(abs(axisX.x), abs(axisX.y), abs(axisX.z));
	axisYabs.qfeSetVectorElements(abs(axisY.x), abs(axisY.y), abs(axisY.z));
	axisZabs.qfeSetVectorElements(abs(axisZ.x), abs(axisZ.y), abs(axisZ.z));
	
	if(axisZabs.x == max(max(axisZabs.x, axisYabs.x), axisXabs.x))
	{
		x.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
		y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
		z.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
	}
	if(axisZabs.y == max(max(axisZabs.y, axisYabs.y), axisXabs.y))
	{
		x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
		y.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
		z.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
	}
	if(axisZabs.z == max(max(axisZabs.z, axisYabs.z), axisXabs.z))
	{
		x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
		y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
		z.qfeSetVectorElements( 0.0f,  0.0f,  1.0f);    
	}
	
	// Convert back to patient coordinates
	x = x*V2P;
	y = y*V2P;
	z = z*V2P;
	
	// Get the plane origin
	volume->qfeGetVolumeGrid(&grid);
	grid->qfeGetGridOrigin(o.x, o.y, o.z);
	
	// Build the plane
	(*slice)->qfeSetFrameExtent(1,1);
	(*slice)->qfeSetFrameOrigin(o.x, o.y, o.z);
	(*slice)->qfeSetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);
	
	return qfeSuccess;
}

void qfeDriverDA::qfeOnAction(void *caller, int t)
{
	qfeDriverDA *self = reinterpret_cast<qfeDriverDA *>(caller);

	qfeVisualParameter *p;
	
	switch(t)
	{		
		case qfeVisualSeedVolumes::actionAddSeedVolume  :  
		{
			self->newFlowSourceAndSink = !self->newFlowSourceAndSink;
			self->newFlowSourceAndSinkPoints.clear();

			self->qfeUpdateSourceSinkVolume();
			break;
		}
		case qfeVisualSeedVolumes::actionRemoveSeedVolume  :  
		{
			if(self->pointSelected.index != 0)
			{
				self->flowSourceAndSinks->qfeRemoveSeedVolume(self->pointSelected.index);

				self->flowSourceAndSinkGuidePointSelected = false;

				self->pointSelected.index = 0;

				self->qfeUpdateSourceSinkVolume();

				self->engine->Refresh();
			}
			break;
		}
		case qfeVisualSeedVolumes::actionSaveSeedVolumes :
		{
			self->flowSourceAndSinks->qfeSaveSeedVolumes();

			self->qfeUpdateSourceSinkVolume();

			break;
		}
		case qfeVisualSeedVolumes::actionLoadSeedVolumes :
		{
			self->flowSourceAndSinks->qfeLoadSeedVolumes();

			self->qfeUpdateSourceSinkVolume();

			self->engine->Refresh();

			break;
		}
		case qfeVisualDataAssimilation::actionApplyDataAssimilation:
			self->qfeApplyDataAssimilation(self->visualDA);
			break;
		case qfeVisualInkVis::actionAnimateInk  :  
		{
			if(self->visualInk1 != NULL)
				self->visualInk1->qfeGetVisualActiveVolume(self->currentTime);
			self->algorithmIP1->qfeSetPhase(self->currentTime);
			self->algorithmIP2->qfeSetPhase(self->currentTime);
			
			qfeVisualParameter *param;

			bool seedFromFixedPosition = true;

			double seedFromFixedPositionMultiplier = 1.0;
			self->visualInk1->qfeGetVisualParameter("Fixed seed positions multiplier", &param);
			if(param != NULL) param->qfeGetVisualParameterValue(seedFromFixedPositionMultiplier);				
									
			bool seedPlanar = false;
			bool seedVolume = false;

			bool seedWholeMesh = true;
			
			bool storeScreenshots = false;
			self->visualInk1->qfeGetVisualParameter("Store Screenshots", &p);
			if(p != NULL) p->qfeGetVisualParameterValue(storeScreenshots);	
					
			double time = 0.0f;
			double step_size = self->algorithmIP1->qfeGetTimeStepSize();

			if(storeScreenshots)
				self->engine->StoreScreenshot(1920, 1080);
			
			while(time < self->animation_duration)
			{				
				if(time + step_size >  self->animation_duration)
				{
					self->algorithmIP1->qfeSetTimeStepSize(self->animation_duration-time);
					self->algorithmIP2->qfeSetTimeStepSize(self->animation_duration-time);
				}

				self->algorithmIP1->qfeComputeNextTimeStep(seedVolume, false, seedFromFixedPosition, (float)seedFromFixedPositionMultiplier, seedPlanar, seedWholeMesh);
				if(self->visualInk2_initialized)
					self->algorithmIP2->qfeComputeNextTimeStep(seedVolume, false, seedFromFixedPosition, (float)seedFromFixedPositionMultiplier, seedPlanar, seedWholeMesh);
				self->engine->Refresh();
				time += step_size;
				
				if(storeScreenshots)
					self->engine->StoreScreenshot(1920, 1080);
			}
			
			self->algorithmIP1->qfeSetTimeStepSize(step_size);//reset time step sizes
			self->algorithmIP2->qfeSetTimeStepSize(step_size);//reset time step sizes

			self->engine->Refresh();
		}
			break;  
		case qfeVisualInkVis::actionRemoveInk  :  
		{
			self->algorithmIP1->qfeClearParticles();
			self->algorithmIP2->qfeClearParticles();
			self->engine->Refresh();
			break;
		}
	}
}

void qfeDriverDA::qfeOnResize(void *caller)
{
	qfeDriverDA *self = reinterpret_cast<qfeDriverDA *>(caller);
	
	qfeViewport *vp;
	
	self->engine->GetViewport3D(&vp);
	
	qfeDriver::qfeDriverResize(caller);	
}

void qfeDriverDA::qfeOnMouseMove(void *caller, int x, int y, int v)
{
	qfeDriverDA *self = reinterpret_cast<qfeDriverDA *>(caller);
	
	if(self->engine->GetInteractionControlKey() && self->rightMouseDown && self->flowSourceAndSinkGuidePointSelected) 
	{//a point was already selected so we have to update the point selector
		qfePoint p(0.0f,0.0f,0.0f);
		bool found = self->qfeGetPointOnSlices(x, y, p);
		
		//get nearest seeding point:
		if(found)
		{
			self->flowSourceAndSinks->qfeUpdateSeedVolumeGuidePoint(self->pointSelected.index, p, self->pointSelected.type);
			self->pointSelected.point = p;

			self->qfeUpdateSourceSinkVolume();
		}
		else
		{
			//skip
		}
		
		self->engine->Refresh();
	}
	if(!self->engine->GetInteractionControlKey())
	{
		//unselect the point
		//self->particleSinkGuidePointSelected = false;
	}
}

void qfeDriverDA::qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v)
{
	qfeDriverDA *self = reinterpret_cast<qfeDriverDA *>(caller);
	
	qfeColorRGB color;
	float       depth;
	
	self->qfeReadSelect(x, y, color);
	self->qfeReadDepth(x, y, depth);
	
	self->selectedPlane = qfePlaneNone;
	
	if(!self->engine->GetInteractionControlKey())
	{
		if(color.r == 1.0 && color.g == 0.0 && color.b==0.0)
			self->selectedPlane = qfePlaneX;
		
		if(color.r == 0.0 && color.g == 1.0 && color.b==0.0)
			self->selectedPlane = qfePlaneY;
		
		if(color.r == 0.0 && color.g == 0.0 && color.b==1.0)
			self->selectedPlane = qfePlaneZ;
		
		//see if a particle sink point was clicked  
		std::vector<seedVolumeGuidePoint> points;
		
		vtkCamera *vtkcamera = self->engine->GetRenderer()->GetActiveCamera();
		std::array<double,3> viewDir;
		vtkcamera->GetDirectionOfProjection(viewDir[0], viewDir[1], viewDir[2]);
		
		qfePoint p(0.0f,0.0f,0.0f);
		bool found = self->qfeGetPointOnSlices(x, y, p);
		
		// Convert mouse click to ray
		qfePoint rayStart, rayEnd;
		self->qfeClickToRay(x, y, rayStart, rayEnd);
		
		if(!found)
		{
			//point was not found, though we might have select a point:
			
			p = rayEnd;
			
			found = true;
		}
		
		//to make sure that we cannot select points that are not in behind a visible slice
		points.push_back(self->flowSourceAndSinks->seedVolumeGuidePointConstructor(
			//move the found point to allow selection of points that are on the slice:
			qfePoint(p.x+viewDir[0]*0.025, p.y+viewDir[1]*0.025, p.z+viewDir[2]*0.025), 
			0, other_point_type));
		
		self->flowSourceAndSinks->qfeGetAllGuidePoints(points);
		
		int index = self->qfePointsOnLine(rayStart, rayEnd, points);
		if(index > 0)//a point was clicked (note index 0 indicates a click on a plane not a point)
		{			
			self->pointSelected = points[index];
		}
	}
	
	self->flowSourceAndSinkGuidePointSelected = false;
}

void qfeDriverDA::qfeOnMouseRightButtonUp(void *caller, int x, int y, int v)
{
	qfeDriverDA *self = reinterpret_cast<qfeDriverDA *>(caller);

	self->rightMouseDown = false;
	
	if(self->engine->GetInteractionControlKey() && self->flowSourceAndSinkGuidePointSelected)
	{
		qfePoint p(0.0f,0.0f,0.0f);
		
		bool found = self->qfeGetPointOnSlices(x, y, p);
		
		if(found) //update the point position
		{			
			self->flowSourceAndSinks->qfeUpdateSeedVolumeGuidePoint(self->pointSelected.index, p, self->pointSelected.type);

			self->qfeUpdateSourceSinkVolume();
		}
		else
		{
			self->flowSourceAndSinkGuidePointSelected = false;
		}
	}

	self->engine->Refresh();
}

void qfeDriverDA::qfeOnMouseRightButtonDown(void *caller, int x, int y, int v)
{
	qfeDriverDA *self = reinterpret_cast<qfeDriverDA *>(caller);

	self->rightMouseDown = true;
	
	self->pointSelected.index = 0;

	if(self->engine->GetInteractionControlKey())
	{ 
		//no point is selected, so we might have to select one
		qfePoint p(0.0f,0.0f,0.0f);
		bool found = self->qfeGetPointOnSlices(x, y, p);
		
		// Convert mouse click to ray
		qfePoint rayStart, rayEnd;
		self->qfeClickToRay(x, y, rayStart, rayEnd);

		if(!found)
		{
			//point was not found, though we might have select a point:

			p = rayEnd;
			
			found = true;
		}
		
		//get nearby seeding point:
		if(found)
		{
			self->flowSourceAndSinkGuidePointSelected = false;

			std::vector<seedVolumeGuidePoint> points;

			vtkCamera *vtkcamera = self->engine->GetRenderer()->GetActiveCamera();
			std::array<double,3> viewDir;
			vtkcamera->GetDirectionOfProjection(viewDir[0], viewDir[1], viewDir[2]);
			
			//to make sure that we cannot select points that are not in behind a visible slice
			points.push_back(self->flowSourceAndSinks->seedVolumeGuidePointConstructor(
				//move the found point to allow selection of points that are on the slice:
				qfePoint(p.x+viewDir[0]*0.025, p.y+viewDir[1]*0.025, p.z+viewDir[2]*0.025), 
				0, other_point_type));

			self->flowSourceAndSinks->qfeGetAllGuidePoints(points);

			int index = self->qfePointsOnLine(rayStart, rayEnd, points);
			if(index > 0)//a point was clicked (note index 0 indicates a click on a plane not a point)
			{
				self->flowSourceAndSinkGuidePointSelected = true;
				self->pointSelected = points[index];
			}
			else
			{
				if(self->newFlowSourceAndSink)
				{
					self->newFlowSourceAndSinkPoints.push_back(p);

					if(self->newFlowSourceAndSinkPoints.size() == 3) //we have enough points for a particle sink
					{
						QDialog dlg;
						QVBoxLayout *mainLayout = new QVBoxLayout;	
						QDialogButtonBox *buttonBoxControl = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

						buttonBoxControl->button(QDialogButtonBox::Ok)->setText("Source");
						buttonBoxControl->button(QDialogButtonBox::Ok)->setIcon(QIcon());
						buttonBoxControl->button(QDialogButtonBox::Cancel)->setText("Sink");
						buttonBoxControl->button(QDialogButtonBox::Cancel)->setIcon(QIcon());

						QObject::connect(buttonBoxControl, SIGNAL(accepted()), &dlg, SLOT(accept()));
						QObject::connect(buttonBoxControl, SIGNAL(rejected()), &dlg, SLOT(reject()));
						
						mainLayout->addWidget(buttonBoxControl);
						
						dlg.setLayout(mainLayout);

						int dialog_result = dlg.exec();
						
						if(dialog_result == QDialog::Rejected)
						{
							self->flowSourceAndSinks->qfeAddSeedVolume(
								self->newFlowSourceAndSinkPoints.at(0),
								self->newFlowSourceAndSinkPoints.at(1),
								self->newFlowSourceAndSinkPoints.at(2), 
								CylinderType, seedVolumeTypeSink);
						}
						else
						{
							self->flowSourceAndSinks->qfeAddSeedVolume(
								self->newFlowSourceAndSinkPoints.at(0),
								self->newFlowSourceAndSinkPoints.at(1),
								self->newFlowSourceAndSinkPoints.at(2), 
								CylinderType, seedVolumeTypeSource);
						}

						
						self->newFlowSourceAndSinkPoints.clear();
						self->newFlowSourceAndSink = false;
					}
				}
			}

			self->qfeUpdateSourceSinkVolume();
		}
		else //no point on the planes was found
		{
			self->flowSourceAndSinkGuidePointSelected = false;
		}
	}
	else
	{
		//control is not pressed:
		self->flowSourceAndSinkGuidePointSelected = false;

		self->engine->CallInteractionParentRightButtonDown();
	}
	
	self->engine->Refresh();
}

void qfeDriverDA::qfeOnWheelForward(void *caller, int x, int y, int v)
{
  qfeDriverDA *self = reinterpret_cast<qfeDriverDA *>(caller);

  if(self->flowSourceAndSinkGuidePointSelected)
  {
	  //we don't allow squeezing (yet) because it is not stored
	  //self->flowSourceAndSinks->qfeSquishSeedVolume(self->pointSelected.index);

	  self->engine->Refresh();

	  return;
  }  
  
  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if(self->visualReformat == NULL) return;

  // Obtain the volume
  self->visualReformat->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visualReformat->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);

  if((int)study->pcap.size() <= 0) return;
  
  if(self->selectedPlane == qfePlaneX) self->qfeMovePlaneForward(study->pcap.front(), &self->planeX, scrollStep);
  if(self->selectedPlane == qfePlaneY) self->qfeMovePlaneForward(study->pcap.front(), &self->planeY, scrollStep);
  if(self->selectedPlane == qfePlaneZ) self->qfeMovePlaneForward(study->pcap.front(), &self->planeZ, scrollStep);

  if(self->selectedPlane == qfePlaneNone) self->engine->CallInteractionParentWheelForward();
  
  self->engine->Refresh();
}

void qfeDriverDA::qfeOnWheelBackward(void *caller, int x, int y, int v)
{
  qfeDriverDA *self = reinterpret_cast<qfeDriverDA *>(caller);

  if(self->flowSourceAndSinkGuidePointSelected)
  {
	  //we don't allow stretching (yet) because it is not stored
	  //self->flowSourceAndSinks->qfeStretchSeedVolume(self->pointSelected.index);

	  self->engine->Refresh();

	  return;
  }  
  
  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if(self->visualReformat == NULL) return;

  // Obtain the volume
  self->visualReformat->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visualReformat->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);

  if((int)study->pcap.size() <= 0) return;
  
  if(self->selectedPlane == qfePlaneX) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeX, scrollStep);
  if(self->selectedPlane == qfePlaneY) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeY, scrollStep);
  if(self->selectedPlane == qfePlaneZ) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeZ, scrollStep);

  if(self->selectedPlane == qfePlaneNone) self->engine->CallInteractionParentWheelBackward();
  
  self->engine->Refresh();
}

void qfeDriverDA::qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if(volume == NULL) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
	origin = origin + (mm*normal);
  else
	origin = origin - (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = max((double)originTex.x, 0.0+delta);
  originTex.x = min((double)originTex.x, 1.0-delta);
  originTex.y = max((double)originTex.y, 0.0+delta);
  originTex.y = min((double)originTex.y, 1.0-delta);
  originTex.z = max((double)originTex.z, 0.0+delta);
  originTex.z = min((double)originTex.z, 1.0-delta);

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

void qfeDriverDA::qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if(volume == NULL) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
	origin = origin - (mm*normal);
  else
	origin = origin + (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = max((double)originTex.x, 0.0+delta);
  originTex.x = min((double)originTex.x, 1.0-delta);
  originTex.y = max((double)originTex.y, 0.0+delta);
  originTex.y = min((double)originTex.y, 1.0-delta);
  originTex.z = max((double)originTex.z, 0.0+delta);
  originTex.z = min((double)originTex.z, 1.0-delta);

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

bool qfeDriverDA::qfeGetPointOnSlices(int x, int y, qfePoint &p)
{
  // Convert mouse click to ray
  qfePoint rayStart, rayEnd;
  qfeClickToRay(x, y, rayStart, rayEnd);

  // Determine slices to check
  qfeVisualParameter *param;  
  bool                paramShowX, paramShowY, paramShowZ;

  visualReformat->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowX);    
  visualReformat->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowY);    
  visualReformat->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowZ);   

  // Find closest slice intersection
  qfePoint intersection, closestIntersection;
  float distance;
  float smallestDistance = 1e30f;

  if (paramShowX && qfeGetRaySliceIntersection(rayStart, rayEnd, planeX, intersection, distance) && distance < smallestDistance) 
  {
	smallestDistance = distance;
	closestIntersection = intersection;
  }

  if (paramShowY && qfeGetRaySliceIntersection(rayStart, rayEnd, planeY, intersection, distance) && distance < smallestDistance) 
  {
	smallestDistance = distance;
	closestIntersection = intersection;
  }

  if (paramShowZ && qfeGetRaySliceIntersection(rayStart, rayEnd, planeZ, intersection, distance) && distance < smallestDistance) 
  {
	smallestDistance = distance;
	closestIntersection = intersection;
  }

  // If there was no intersection
  if (smallestDistance == 1e30f)
	return false;

  p = closestIntersection;
  return true;
}

void qfeDriverDA::qfeClickToRay(int x, int y, qfePoint &start, qfePoint &end) 
{
  qfeViewport *vp3D;
  engine->GetViewport3D(&vp3D);

  // Take window coordinates of mouseclick on near plane, unproject to patient coordinates
  // Then do the same for the far plane
  qfeTransform::qfeGetCoordinateUnproject(qfePoint((float)x, (float)y, 0.f), start, currentModelView, currentProjection, vp3D);
  qfeTransform::qfeGetCoordinateUnproject(qfePoint((float)x, (float)y, 1.f), end, currentModelView, currentProjection, vp3D);
}

//checks whether any of the points in vector "points" is on the line, if not return -1, otherwise return the index of the closest point
int qfeDriverDA::qfePointsOnLine(qfePoint start, qfePoint end, std::vector<seedVolumeGuidePoint> points) 
{
  int index = -1;

  float distBestAC = 1e6; //closest distance found so far
  float distAC; //distance from ray start to point "C"
  float distBC; //distance from ray end to point "C"

  float distAB; //distance from ray start to ray end

  qfePoint::qfePointDistance(start, end, distAB);
  
  for(unsigned int i = 0; i<points.size(); i++)
  {
	  qfePoint::qfePointDistance(start, points[i].point, distAC);
	  qfePoint::qfePointDistance(points[i].point, end, distBC);

	  if(fabs(distAC + distBC - distAB)<0.0015) //the point is on the line
	  {//explanation of why can be found here: http://stackoverflow.com/questions/17692922/check-is-a-point-x-y-is-between-two-points-drawn-on-a-straight-line
		  if(distAC < distBestAC)
		  {//best point on the line so far
			  distBestAC = distAC;
			  index = i;
		  }
	  }
  }

  return index;
}

bool qfeDriverDA::qfeGetRaySliceIntersection(qfePoint rayStart, qfePoint rayEnd, qfeFrameSlice *slice, qfePoint &intersection, float &distance)
{
  // Define slice as a rectangle by point P0 and sides S1, S2, with normal N
  qfePoint *sliceCorners;
  qfeGetSliceCorners(slice, &sliceCorners);
  qfePoint P0 = sliceCorners[0];
  qfeVector S1 = sliceCorners[1] - P0;
  qfeVector S2 = sliceCorners[3] - P0;
  qfeVector N;
  qfeVector::qfeVectorCross(S1, S2, N);
  qfeVector::qfeVectorNormalize(N);

  // Define ray as R0 + t*D
  qfePoint R0 = rayStart;
  qfeVector D = rayEnd - rayStart;
  qfeVector::qfeVectorNormalize(D);

  float DdotN;
  qfeVector::qfeVectorDot(D, N, DdotN);
  if (DdotN == 0.f) // If looking directly at the side of the slice
	return false;

  float P0minR0dotN;
  qfeVector::qfeVectorDot(P0 - R0, N, P0minR0dotN);
  float a = P0minR0dotN / DdotN;

  qfePoint P = R0 + a * D;; // Intersection point between ray and plane in which slice resides

  // Q1 and Q2 are projections of P - P0 onto S1 and S2 respectively
  qfeVector P0P = P - P0;
  qfeVector S1U = S1;
  qfeVector S2U = S2;
  qfeVector::qfeVectorNormalize(S1U);
  qfeVector::qfeVectorNormalize(S2U);
  float q1, q2;
  qfeVector::qfeVectorDot(P0P, S1U, q1);
  qfeVector::qfeVectorDot(P0P, S2U, q2);
  qfeVector Q1 = S1U * q1;
  qfeVector Q2 = S2U * q2;

  // Check if ray-plane intersection is inside rectangle
  float ls1, ls2, lq1, lq2;
  qfeVector::qfeVectorLength(S1, ls1);
  qfeVector::qfeVectorLength(S2, ls2);
  qfeVector::qfeVectorLength(Q1, lq1);
  qfeVector::qfeVectorLength(Q2, lq2);
  if (lq1 <= ls1 && lq2 <= ls2) {
	intersection = P;
	distance = a;
	return true;
  }

  return false;
}

void qfeDriverDA::qfeGetSliceCorners(qfeFrameSlice *slice, qfePoint **corners)
{
  qfeVolume *volume;
  int paramData;
  std::string paramName;
  qfeVisualParameter *param;
  qfeSelectionList paramDataList;

  visualReformat->qfeGetVisualParameter("Ortho data set", &param);
  
  if (param) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;
  paramName = paramDataList.list.at(paramData);

  int current;
  this->visualReformat->qfeGetVisualActiveVolume(current);
  qfeReformatGetDataSet(paramData, paramName, visualReformat, current, &volume);

  int nrVertices;
  algorithmReformat->qfeComputePolygon(volume, slice, corners, nrVertices);
}

void qfeDriverDA::qfeUpdateSourceSinkVolume()
{
	qfeMatrix4f P2V, V2P;
	qfePoint    o;
	qfeGrid    *grid;
	int current;

	qfeStudy		   *study;
	qfeVolume          *volume;

	if(this->visualDA == NULL) 
		return;
	
	this->visualDA->qfeGetVisualStudy(&study);  
	this->visualDA->qfeGetVisualActiveVolume(current);

	if(study == NULL) 
		return;

	if(study->pcap.size() > 0) 
		volume = study->pcap[current];
	else 
		return;

	if(volume == NULL) 
		return;

	// Get the patient to voxel transformation
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);

	study->pcap[0]->qfeGetVolumeGrid(&grid);

	if(grid == NULL) 
		return;
	
	std::array<float, 3> spacing;
	grid->qfeGetGridExtent(spacing[0],spacing[1],spacing[2]);

	float dx = max(spacing[0], max(spacing[1], spacing[2]));

	unsigned int ni = this->mesh_level_set.ni;
	unsigned int nj = this->mesh_level_set.nj;
	unsigned int nk = this->mesh_level_set.nk;

	if(ni < 2 || nj < 2 || nk < 2)
	{
		return;
	}

	sourceSinkVolume.resize(ni, nj, nk);
	sourceSinkVolume.assign(0);
	
	for(unsigned int k = 0; k<nk; k++)
	for(unsigned int j = 0; j<nj; j++)
	for(unsigned int i = 0; i<ni; i++)
	{
		sourceSinkVolume(i,j,k) = 0;

		//check if near volume boundary
		bool near_volume_boundary = false;
		if(i<2 || j<2 || k<2 || i>ni-3 || j>nj-3 || k>nk-3)
			near_volume_boundary = true;

		//check if near mesh boundary
		if(!near_volume_boundary && fabs(mesh_level_set.at(i,j,k)) > dx)
		{
			continue;
		}

		qfePoint vp = qfePoint(i,j,k);
		qfePoint position = vp*V2P;

		unsigned int nrSourceSinks = this->flowSourceAndSinks->size();
		for(unsigned int ss = 0; ss<nrSourceSinks; ss++)
		{
			//check if in seed volume
			if(this->flowSourceAndSinks->at(ss)->volume->IsInVolume(position))
			{
				sourceSinkVolume(i,j,k) = this->flowSourceAndSinks->at(ss)->type;
			}
		}
	}

	return;
}

std::string qfeDriverDA::qfeWriteData(qfeVisualDataAssimilation *visual, unsigned int t)
{
	qfeStudy *study;
	qfeGrid *grid;
	visual->qfeGetVisualStudy(&study);
	
	study->pcap[0]->qfeGetVolumeGrid(&grid);
	
	bool subsample = false;

	qfeVisualParameter *param; 
	visual->qfeGetVisualParameter("Subsample", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(subsample); 

	std::fstream filestrTime;
	
	Array3Vec3 vectorField;
	
	float spacing_x = 0.0f;
	float spacing_y = 0.0f;
	float spacing_z = 0.0f;
			
	grid->qfeGetGridExtent(spacing_x,spacing_y,spacing_z);

	if(!additional_data_computed)
	{
		this->qfeComputeMeshLevelsetVolume();

		this->qfeComputeSNRData();
		this->qfeComputeSNRVolumes();
		this->qfeComputeSignalStrengthVolumes();

		this->qfeComputeWSSData();
		this->qfeComputeWSSVolumes();

		this->additional_data_computed = true;
	}
	
		
	int source_count = 0;
	int sink_count = 0;

	unsigned phases = study->pcap.size();

	std::ostringstream strs;
	if(t<10)
		strs << '0' << t;
	else
		strs << t;
	
	std::string folderName = study->qfeGetSeriesDataPath() + "data_assim/";
	
	//create the folder:
	_mkdir(folderName.c_str());
	
	std::string data_path = folderName + "DA_"+strs.str()+".dat";
	
	filestrTime.open (data_path, fstream::in | ios::trunc);
	filestrTime.clear();  
	filestrTime.close();
	filestrTime.open (data_path, std::fstream::app);  
	
	//cout << "Progress (" << t+1 << "/" << phases << ")" << endl;
	
	bool existingMetric = false;

	if(t >= phases || t < 0) //wrap around
	{
		t = 0;
	}

	study->qfeGetVectorMetric("Velocity 1", &mesh_level_set, t , existingMetric, vectorField);	
	if(!existingMetric)
	{
		std::cout<<"Velocity field not found"<<std::endl;
		return "";
	}

	if(vectorField.ni != mesh_level_set.ni || vectorField.nj != mesh_level_set.nj || vectorField.nk != mesh_level_set.nk)
	{
		std::cout<<"Error, non-matching dimensions:"<<std::endl;
		std::cout<<vectorField.ni<<" "<<mesh_level_set.ni<<std::endl;
		std::cout<<vectorField.nj<<" "<<mesh_level_set.nj<<std::endl;
		std::cout<<vectorField.nk<<" "<<mesh_level_set.nk<<std::endl;
		return "";
	}		

	//volume dimensions
	filestrTime<<"Dimensions:\n";
	//filestrTime<<mesh_level_set.ni<<" "<<mesh_level_set.nj<<" "<<mesh_level_set.nk<<"\n";
	int dimX = ROI_max.x-ROI_min.x;
	int dimY = ROI_max.y-ROI_min.y;
	int dimZ = ROI_max.z-ROI_min.z;

	if(subsample)
	{
		dimX = ceil((double)dimX/2.0); dimY = ceil((double)dimY/2.0); dimZ = ceil((double)dimZ/2.0);
		spacing_x = spacing_x*2.0; spacing_y = spacing_y * 2.0; spacing_z = spacing_z * 2.0;
	}

	filestrTime<<dimX<<" "<<dimY<<" "<<dimZ<<"\n";
	 
	//cell spacing
	filestrTime<<"Voxel spacing (mm):\n";
	filestrTime<<spacing_x<<" "<<spacing_y<<" "<<spacing_z<<"\n";
				
	qfeFloat venc[3] = {0.0, 0.0, 0.0};
	study->pcap[t]->qfeGetVolumeVenc(venc[0], venc[1], venc[2]);

	//meta data
	filestrTime<<"Meta data [#phases phaseDuration phaseTriggerTime vencX vencY vencZ sigma]:\n";
	filestrTime<<phases<<" "<<study->phaseDuration<<" "<<t*study->phaseDuration<<" "<<venc[0]<<" "<<venc[1]<<" "<<venc[2]<<" "<<sigma_data.at(t)<<"\n";
	
	filestrTime<<"Data [vx vy vz level_set SNR cell_type]:\n";
	for(unsigned int z = ROI_min.z; z<ROI_max.z; z++)
	{
		for(unsigned int y = ROI_min.y; y<ROI_max.y; y++)
		{
			for(unsigned int x = ROI_min.x; x<ROI_max.x; x++)
			{
				bool write = true;
				if(subsample)
				{
					//only write if all indices are even
					int xd = x-ROI_min.x;
					int yd = y-ROI_min.y;
					int zd = z-ROI_min.z;
					if(xd % 2 != 0) write = false;
					if(yd % 2 != 0) write = false;
					if(zd % 2 != 0) write = false;
				}
				if(!write) //skip this voxel
					continue;

				std::array<float,3> v = vectorField.at(x,y,z);
				
				int cell_type = 0;

				if(sourceSinkVolume.at(x,y,z) == seedVolumeTypeSource)
				{
					cell_type = 1;
					source_count++;
				}

				if(sourceSinkVolume.at(x,y,z) == seedVolumeTypeSink)
				{
					cell_type = 2;
					sink_count++;
				}
					
				filestrTime<<
					v[0]<<" "<<						
					v[1]<<" "<<
					v[2]<<" "<<
					mesh_level_set.at(x,y,z)<<" "<<
					SNR_data.at(t).at(x,y,z)<<" "<<
					cell_type<<
					"\n";
			}
		}
	}
		
	filestrTime.close();

	return data_path;
}


void qfeDriverDA::qfeApplyDataAssimilation(qfeVisualDataAssimilation *visual)
{
	this->qfeUpdateSourceSinkVolume();

	qfeStudy *study;
	visual->qfeGetVisualStudy(&study);

	if(study->pcap.size()<1)
		return;

	if(mesh_level_set.ni < 2 || mesh_level_set.nj < 2 || mesh_level_set.nk < 2)
	{
		std::cout<<"Mesh not found"<<std::endl;
		return;
	}

	unsigned phases = study->pcap.size();
	
	const unsigned int max_instances = 2;
	
	study->qfeGetProgressBar("Appling data assimilation... This can take a while", phases+2);

	//get all the parameters
	qfeVisualParameter *assimApproach;  
	qfeSelectionList assimApproachList;
	int selectedAssimApproach = 0;
	visual->qfeGetVisualParameter("Assimilation approach", &assimApproach);
	if(assimApproach != NULL) assimApproach->qfeGetVisualParameterValue(assimApproachList); 
	selectedAssimApproach = assimApproachList.active;

	qfeVisualParameter *filterFunction;  
	qfeSelectionList filterFunctionList;
	int selectedFilterFunction = 0;
	visual->qfeGetVisualParameter("Filter function", &filterFunction);
	if(filterFunction != NULL) filterFunction->qfeGetVisualParameterValue(filterFunctionList); 
	selectedFilterFunction = filterFunctionList.active;

	double alpha = 1.0;
	double beta = 1.0;
	double gamma = 1.0;
	double m_delta = 1.0;
	double m_epsilon = 1.0;
	int max_iter = 1000;

	bool spatial_interpolation = false;
	bool temporal_interpolation = false;

	bool verbose = false;
	bool use_uncertainty = true;
	bool inject_noise = false;
	int noise_percentage = 5;
	bool keep_intermediate_files = false;
	
	qfeVisualParameter *param; 
	visual->qfeGetVisualParameter("Alpha", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(alpha); 

	visual->qfeGetVisualParameter("Beta", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(beta); 

	visual->qfeGetVisualParameter("Gamma", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(gamma); 

	visual->qfeGetVisualParameter("Delta (minimizer)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(m_delta); 
	m_delta *=1e-3;

	visual->qfeGetVisualParameter("Epsilon (minimizer)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(m_epsilon); 
	m_epsilon *= 1e-6;

	visual->qfeGetVisualParameter("Maximum iterations", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(max_iter); 

	visual->qfeGetVisualParameter("Spatial interpolation", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(spatial_interpolation); 

	visual->qfeGetVisualParameter("Temporal interpolation", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(temporal_interpolation); 
	
	visual->qfeGetVisualParameter("Verbose", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(verbose); 

	visual->qfeGetVisualParameter("Keep intermediate files", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(keep_intermediate_files); 

	visual->qfeGetVisualParameter("Use uncertainty", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(use_uncertainty); 

	visual->qfeGetVisualParameter("Inject noise", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(inject_noise); 

	visual->qfeGetVisualParameter("Noise percentage", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(noise_percentage);

	
	//Write input data to a file
	std::vector<std::string> data_files;
	for (unsigned t = 0; t <= phases; t++) 
	{
		std::string data_path = this->qfeWriteData(visual, t);
		data_files.push_back(data_path);
	}

	for (unsigned t = 0; t < phases; t++) 
	{
		if(study->qfeWasCancelledProgressBar())
		{
			break;
		}
		
		study->qfeUpdateProgressBar(t+2);

		//Prepare execution of DA application
		char *cwd = (char *)calloc(_MAX_PATH, sizeof(char));
		_getcwd(cwd, _MAX_PATH);
		std::string executable_path = cwd;
  
		executable_path = executable_path + "/FluidControl/FluidControl.exe";

		const QString app = QDir::toNativeSeparators(QString::fromStdString(executable_path));		
		
		const QString data_location = QDir::toNativeSeparators(QString::fromStdString(data_files.at(t)));

		if(data_location.size() == 0)
			continue;


		//Prepare parameters for the DA application
		QStringList params;
		params.push_back(data_location);
  
		params.push_back("-approach");
		params.push_back(QString::number(selectedAssimApproach));
				
		params.push_back("-function");
		params.push_back(QString::number(selectedFilterFunction));

		if(spatial_interpolation) params.push_back("-s_interpolation");
		if(temporal_interpolation) params.push_back("-t_interpolation");

		params.push_back("-alpha"); params.push_back(QString::number(alpha));
		params.push_back("-beta"); params.push_back(QString::number(beta));
		params.push_back("-gamma"); params.push_back(QString::number(gamma));

		params.push_back("-delta"); params.push_back(QString::number(m_delta));
		params.push_back("-epsilon"); params.push_back(QString::number(m_epsilon));

		params.push_back("-max_iterations"); params.push_back(QString::number(max_iter));

		if(verbose)				params.push_back("-verbose");
		if(!use_uncertainty)	params.push_back("-no_uncertainty");
		if(inject_noise)		params.push_back("-inject_noise");
		if(inject_noise)		params.push_back(QString::number(noise_percentage));

		qDebug()<<params;

		//Start the process
		QProcess process;

		//process->startDetached(app, params); doesn't work (not enough resources available to run multiple instances)
		process.execute(app, params);

		//wait for the process to be started:
		//process->waitForStarted(-1); we wait for one to be finished to continue since we run them one by one

		study->qfeUpdateProgressBar(t+3);
	}

	
	//delete intermediate data files:
	if(!keep_intermediate_files)
	{
		for (unsigned t = 0; t < data_files.size(); t++) 
		{
			std::string data_path = data_files.at(t);
			remove(data_path.c_str());
		}
	}

	study->qfeUpdateProgressBar(phases+4);
	
	study->qfeClearProgressBar();

	std::cout<<"Finished"<<std::endl;
	
}