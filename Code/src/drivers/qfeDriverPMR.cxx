#include "qfeDriverPMR.h"

//----------------------------------------------------------------------------
qfeDriverPMR::qfeDriverPMR(qfeScene *scene) : qfeDriver(scene)
{
  this->firstRenderLineTrace    = false;
  this->visPatternMatching      = NULL;  
  this->visDVR                  = NULL;
  this->visAnatomy              = NULL;
  this->visAides                = NULL;

  this->intersectTexColor       = 0;
  this->intersectTexDepth       = 0;

  this->algorithmDVR            = new qfeRayCasting();
  this->algorithmLineTrace      = new qfeFlowLineTraceMatching();
  this->algorithmSurfaceShading = new qfeSurfaceShading();

  this->engine->SetObserverAction((void*)this, this->qfeOnAction);
  this->engine->SetObserverResize((void*)this, this->qfeOnResize);
  this->engine->SetInteractionMouseLeftButtonUp((void*)this, this->qfeOnMouseLeftButtonUp);
  this->engine->SetInteractionMouseLeftButtonDown((void*)this, this->qfeOnMouseLeftButtonDown);
};

//----------------------------------------------------------------------------
qfeDriverPMR::~qfeDriverPMR()
{
  this->qfeRenderStop();

  delete this->algorithmDVR;
  delete this->algorithmLineTrace;
  delete this->algorithmSurfaceShading;

  this->seedsPatternMatching.clear();
  this->attribsPatternMatching.clear();
};

//----------------------------------------------------------------------------
bool qfeDriverPMR::qfeRenderCheck()
{
  int t;
  for(int i=0; i<(int)this->visuals.size(); i++)
  {
    this->visuals[i]->qfeGetVisualType(t);

    if(t == visualFlowPatternMatching)
    {
      this->visPatternMatching = static_cast<qfeVisualPatternMatching *>(this->visuals[i]);
    }
    if(t == visualRaycast)
    {
      this->visDVR = static_cast<qfeVisualRaycast *>(this->visuals[i]);
    }
    if(t == visualAnatomyIllustrative)
    {
      this->visAnatomy = static_cast<qfeVisualAnatomyIllustrative *>(this->visuals[i]);      
    } 
    if(t == visualAides)
    {
      this->visAides = static_cast<qfeVisualAides *>(this->visuals[i]);
    }
  }
  return true;
}

//----------------------------------------------------------------------------
void qfeDriverPMR::qfeRenderSelect()
{

}


//----------------------------------------------------------------------------
void qfeDriverPMR::qfeRenderStart()
{
  qfeVolume          *volume = NULL;
  qfeGrid            *grid   = NULL;
  qfePoint            origin;
  qfeViewport        *viewport;
  int                 supersampling;

  if(this->visDVR != NULL)
  {
    qfeVisualParameter *p;
    this->visDVR->qfeGetVisualParameter("DVR visible", &p);
    p->qfeSetVisualParameterValue(false);
    
    this->visDVR->qfeGetVisualParameter("DVR gradient", &p);
    p->qfeSetVisualParameterValue(true);

    this->engine->GetViewport3D(&viewport);
    this->algorithmDVR->qfeSetViewportSize(viewport->width, viewport->height, this->frameBufferProps[0].supersampling);
    this->qfeCreateTextures(viewport, this->frameBufferProps[0].supersampling);
  }

  if(this->visPatternMatching != NULL)
  {
    // Adjust the line span to the current phase duration
    qfeStudy       *study;
    qfeLightSource *light;
    qfeFloat        phaseDuration;    

    this->visPatternMatching ->qfeGetVisualStudy(&study);
    study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);     

    qfeVisualParameter *p;
    this->visPatternMatching->qfeGetVisualParameter("PMR lines span (ms)", &p);
    p->qfeSetVisualParameterRange(0.5*phaseDuration, 4.0*phaseDuration);
    p->qfeSetVisualParameterValue(2.0*phaseDuration); 

    this->visPatternMatching->qfeGetVisualParameter("PMR lines width (mm)", &p);    
    p->qfeSetVisualParameterValue(0.5);

    qfeSelectionList visiblePhases;
    visiblePhases.list.push_back("all");
    for(int i=0; i<(int)study->pcap.size(); i++)
    {
      visiblePhases.list.push_back(static_cast<ostringstream*>(&(ostringstream() << i))->str());
    }
    visiblePhases.active = 0;
    this->visPatternMatching->qfeGetVisualParameter("PMR phase visible", &p);
    p->qfeSetVisualParameterValue(visiblePhases);  

    // Change the lighting conditions for the lines
    this->scene->qfeGetSceneLightSource(&light);
    light->qfeSetLightSourceAmbientContribution(0.10f);

    // Create alternative color and depth texture  
    this->engine->GetViewport3D(&viewport);
    supersampling = this->frameBufferProps[0].supersampling;
    this->qfeCreateTextures(viewport, supersampling);  
  }
}

//----------------------------------------------------------------------------
void qfeDriverPMR::qfeRenderWait()
{
  // Start rendering
  glDisable(GL_LIGHTING);

  // Initialize color map
  qfeColorMap *colormap;   
  if(this->visPatternMatching != NULL)
  {
    this->visPatternMatching->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  if(this->visDVR != NULL)
  {
    this->visDVR->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
    colormap->qfeUploadGradientMap();
  }

  // Clear the intersection buffer
  this->qfeEnableIntersectBuffer();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  this->qfeDisableIntersectBuffer();

  // Render the pattern matching
  if(this->visPatternMatching != NULL)
  {
    this->qfeRenderVisualPatternMatching(this->visPatternMatching);
  }

  // Render the raycasting
  if(this->visDVR != NULL)
  {
    this->qfeRenderVisualDVR(this->visDVR);
  }

  // Render the surface anatomy   
  if(this->visAnatomy != NULL)
  {
    this->qfeRenderVisualAnatomyIllustrative(this->visAnatomy);
  }

  // Render visual aides
  if(this->visAides != NULL)
  {
    this->qfeRenderVisualAides(this->visAides);
  }
};

//----------------------------------------------------------------------------
void qfeDriverPMR::qfeRenderStop()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeRenderVisualPatternMatching(qfeVisualPatternMatching *visual)
{
  qfeStudy           *study;
  float               current;
  qfeVisualParameter *param;
  qfeColorMap        *colormap;  
  qfeLightSource     *light;
  qfeColorRGBA        color;

  qfeFloat            phaseDuration;

  bool                paramVisibleSeeds;
  bool                paramVisibleLines;
  bool                paramVisibleArrows;
  double              paramPhaseSpan;
  double              paramLineWidth;
  qfeSelectionList    paramLineColorList;
  int                 paramLineColor;
  bool                paramLineShading;
  qfeSelectionList    paramPhaseVisibleList;
  int                 paramPhaseVisible;
  int                 paramArrowSize;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  this->scene->qfeGetSceneLightSource(&light);  

  if(study == NULL) return qfeError;

  study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);     

  // Fetch the parameters
  visual->qfeGetVisualParameter("PMR lines visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisibleLines);

  visual->qfeGetVisualParameter("PMR arrows visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisibleArrows);

  visual->qfeGetVisualParameter("PMR seeds visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisibleSeeds);

  visual->qfeGetVisualParameter("PMR phase visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramPhaseVisibleList);
  paramPhaseVisible = paramPhaseVisibleList.active - 1;

  visual->qfeGetVisualParameter("PMR lines shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLineShading);

  visual->qfeGetVisualParameter("PMR lines span (ms)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramPhaseSpan);  

  visual->qfeGetVisualParameter("PMR lines width (mm)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLineWidth);  

  visual->qfeGetVisualParameter("PMR arrow size (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramArrowSize);    
  
  visual->qfeGetVisualParameter("PMR color type", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLineColorList);
  paramLineColor = paramLineColorList.active;

  color.r = color.a = 1.0;
  color.g = color.b = 0.0;

  this->algorithmLineTrace->qfeSetPhase(current); 
  this->algorithmLineTrace->qfeSetPhaseSpan(paramPhaseSpan);
  this->algorithmLineTrace->qfeSetPhaseVisible(paramPhaseVisible);
  this->algorithmLineTrace->qfeSetPhaseDuration(phaseDuration);  
  this->algorithmLineTrace->qfeSetArrowScale(paramArrowSize);
  this->algorithmLineTrace->qfeSetColorType((qfeFlowLineTraceMatching::qfeFlowLinesColorType)paramLineColor);
  this->algorithmLineTrace->qfeSetLineWidth(paramLineWidth);
  this->algorithmLineTrace->qfeSetLightSource(light);
  this->algorithmLineTrace->qfeSetColor(color);

  if(paramLineShading) 
    this->algorithmLineTrace->qfeSetLineShading(qfeFlowLinesPhongAdditive);
  else
    this->algorithmLineTrace->qfeSetLineShading(qfeFlowLinesNone);

  if(this->firstRenderLineTrace == false)
  {
    if((int)study->seeds.size() > 0) 
      this->qfeInjectFileSeeds();  

    this->firstRenderLineTrace = true;
  }

  if(paramVisibleLines)
  {
    this->algorithmLineTrace->qfeRenderMatchingPathlinesStatic(colormap);

    this->qfeEnableIntersectBuffer();
    this->algorithmLineTrace->qfeRenderMatchingPathlinesStatic(colormap);
    this->qfeDisableIntersectBuffer();
  }

  if(paramVisibleArrows)
  {
    this->algorithmLineTrace->qfeRenderMatchingPathlineArrows(colormap);
  }

  if(paramVisibleSeeds)
  {
    this->algorithmLineTrace->qfeRenderMatchingSeeds();
  }

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeRenderVisualDVR(qfeVisualRaycast* visual)
{
  qfeStudy           *study;
  qfeVolume          *textureVolume;
  qfeVolume          *boundingVolume;
  qfeColorMap        *colormap;
  qfeLightSource     *light;
  int                 current;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramQualList;
  qfeSelectionList    paramStyleList;
  bool                paramVisible;
  int                 paramData;
  int                 paramComp;
  int                 paramQuality;
  bool                paramGradient;
  bool                paramShading;
  bool                paramMultiRes;
  qfeFloat            paramRange[2];
  int                 paramMode;
  qfeRange            paramClipX, paramClipY, paramClipZ;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  this->scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("DVR visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);

  visual->qfeGetVisualParameter("DVR data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("DVR data component", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("DVR gradient", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramGradient);

  visual->qfeGetVisualParameter("DVR shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShading);

  visual->qfeGetVisualParameter("DVR shading style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramStyleList);

  visual->qfeGetVisualParameter("DVR shading quality", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramQualList);
  switch(paramQualList.active)
  {
  case 0  : paramQuality = 1; break;
  case 1  : paramQuality = 2; break;
  case 2  : paramQuality = 4; break;
  case 3  : paramQuality = 8; break;
  default : paramQuality = 2; break;
  }

  visual->qfeGetVisualParameter("DVR shading multires", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramMultiRes);

  visual->qfeGetVisualParameter("DVR clipping x", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipX);

  visual->qfeGetVisualParameter("DVR clipping y", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipY);

    visual->qfeGetVisualParameter("DVR clipping z", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipZ);

  if(paramVisible != true) return qfeError;

  // Get the data set and compute the data range
  this->qfeGetDataSet((qfeDataTypeIndex)paramData, visual, current, &textureVolume, paramRange[0], paramRange[1]);

  if(textureVolume == NULL) return qfeError;

  textureVolume->qfeGetVolumeType(paramMode);

  // if there is PCA-P data loaded, use this as bounding volume
  if((int)study->pcap.size() > 0)
    boundingVolume = study->pcap.front();
  else
    boundingVolume = textureVolume;

  // Multi-resolution dvr - check the global state
  if(paramMultiRes)
    paramQuality = std::min((float)paramQuality, this->dvrQuality);

  // Update algorithm parameters
  this->algorithmDVR->qfeSetIntersectionBuffers(this->intersectTexColor, this->intersectTexDepth);
  this->algorithmDVR->qfeSetOrthoClippingRanges(paramClipX, paramClipY, paramClipZ);
  this->algorithmDVR->qfeSetDataComponent(paramComp);
  this->algorithmDVR->qfeSetDataRange(paramRange[0], paramRange[1]);
  this->algorithmDVR->qfeSetGradientRendering(paramGradient);
  this->algorithmDVR->qfeSetShading(paramShading);
  this->algorithmDVR->qfeSetLightSource(light);
  this->algorithmDVR->qfeSetQuality(paramQuality);
  this->algorithmDVR->qfeSetStyle(paramStyleList.active);

  this->algorithmDVR->qfeRenderRaycasting(this->frameBufferProps[0].texColor, this->frameBufferProps[0].texDepth, boundingVolume, textureVolume, colormap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual)
{ 
  qfeStudy              *study;
  qfeVolume             *volume;
  qfeLightSource        *light;
  qfeModel              *model;  
  vtkPolyData           *mesh; 
  qfeMeshVertices       *vertices         = NULL;
  qfeMeshVertices       *normals          = NULL;
  qfeMeshIndices        *indicesTriangles = NULL;
  int                    current;
  
  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);  

  this->scene->qfeGetSceneLightSource(&light);

  volume = study->pcap[current];

  if(volume == NULL)
  {
    cout << "qfeDriverPMR::qfeRenderVisualAnatomyIllustrative - Incorrect volume data" << endl;
    return qfeError;
  }

  if(study == NULL)
  {
    cout << "qfeDriverPMR::qfeRenderVisualAnatomyIllustrative - Incorrect study" << endl;
    return qfeError;
  }

  // Get the parameters
  qfeSelectionList    paramSurfaceShadingList;
  int                 paramSurfaceShading;
  bool                paramSurfaceClipping;
  bool                paramSurfaceContour;

  qfeVisualParameter *param;  

  visual->qfeGetVisualParameter("Surface shading", &param);    
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceShadingList); 
  paramSurfaceShading       = paramSurfaceShadingList.active; 

  visual->qfeGetVisualParameter("Surface clipping", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceClipping);

  visual->qfeGetVisualParameter("Surface contour", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceContour); 

  bool                visible;  
  qfeColorRGB         colorSilhouette;
  qfeColorRGBA        colorContour;
  qfeColorRGBA        colorContourHidden;    

  // Process the parameter structures   
  colorContour.r       = 0.3; 
  colorContour.g       = 0.3;
  colorContour.b       = 0.3;
  colorContourHidden.r = 0.7;
  colorContourHidden.g = 0.7;
  colorContourHidden.b = 0.7;   

  // Set general variables for all meshes
  this->algorithmSurfaceShading->qfeSetLightSource(light);
  this->algorithmSurfaceShading->qfeSetSurfaceCulling(paramSurfaceClipping);
  this->algorithmSurfaceShading->qfeSetSurfaceShading((qfeSurfaceShadingType)paramSurfaceShading);
   
  for(int i=0; i<(int)study->segmentation.size(); i++)
  {    
    // Get the model    
    model = study->segmentation[i];
    
    // Get the parameters
    model->qfeGetModelColor(colorSilhouette);        

    // Get the mesh    
    model->qfeGetModelMesh(&mesh);    
    model->qfeGetModelMesh(&vertices, &normals);
    model->qfeGetModelMeshIndicesTriangles(&indicesTriangles);
    model->qfeGetModelVisible(visible);

    // Draw the mesh
    if(vertices != NULL && visible)
    {
      // Update the surface parameters      
      this->algorithmSurfaceShading->qfeSetSurfaceColor(colorSilhouette);
      
      // Render the mesh
      this->algorithmSurfaceShading->qfeRenderMesh(vertices, normals, indicesTriangles, volume);            

      if(paramSurfaceContour) 
      {
        this->algorithmSurfaceShading->qfeRenderMeshDilate(vertices, normals, indicesTriangles, 0.6, volume);
      }  
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeRenderVisualAides(qfeVisualAides *visual)
{
  qfeStudy           *study;
  qfeVolume          *volume;
  int                 current;

  qfeVisualParameter *param;
  bool                paramSupportBoundingBox = true;
  bool                paramSupportAxes        = false;

  this->visAides->qfeGetVisualStudy(&study);
  this->visAides->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  if(study->pcap.size() > 0)
    volume = study->pcap[current];
  else return qfeError;

  this->visAides->qfeGetVisualParameter("Support bounding box", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportBoundingBox);

  this->visAides->qfeGetVisualParameter("Support axes", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportAxes);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  if(paramSupportBoundingBox == true) qfeDriverAide::qfeRenderBoundingBoxDashed(volume, this->currentModelView, 0.8*this->frameBufferProps[0].supersampling);
  if(paramSupportAxes        == true) qfeDriverAide::qfeRenderVolumeAxes(volume, 10.0, 1.6*this->frameBufferProps[0].supersampling);

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeInjectFileSeeds()
{  
  qfeStudy           *study;
  float               current = 0;

  qfePoint            currentSeed;
  qfePoint            transformedSeed;
  qfePoint            blank;
  qfeSeeds            attribsTemp;
  bool                attribsAvailable;
  bool                seedBlank;

  if(this->visPatternMatching == NULL) return qfeError;

  this->visPatternMatching->qfeGetVisualStudy(&study);        
  this->visPatternMatching->qfeGetVisualActiveVolume(current);     

  if(study == NULL) return qfeError;

  qfeMatrix4f P2V;
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, study->pcap.front()); 

  blank.x = blank.y = blank.z = blank.w = 0.0;
   
  // add the seed positions
  if((int)study->seeds.size() > 0)
  {  
    this->seedsPatternMatching.clear();
    this->attribsPatternMatching.clear();

    // Check the seed attributes
    if((int)study->seedAttribs.size() == (int)study->seeds.size())         
      attribsAvailable = true;    
    else
      attribsAvailable = false;
    
    // Get the seeds, only if the data is available at it's seed time
    for(int i=0; i<(int)study->seeds.size(); i++)
    { 
      currentSeed       = study->seeds[i];
      transformedSeed.qfeSetPointElements(currentSeed.x, currentSeed.y, currentSeed.z, 1.0);
      transformedSeed   = transformedSeed * P2V;      
      transformedSeed.w = currentSeed.w;

      seedBlank = false;
      if(currentSeed == blank)
        seedBlank = true;

      if(!seedBlank && attribsAvailable && ((int)study->seeds[i].w < (int)study->pcap.size()))  
      {        
        this->seedsPatternMatching.push_back(transformedSeed);      
        this->attribsPatternMatching.push_back(study->seedAttribs[i]);
      }
      else if(!seedBlank && !attribsAvailable && ((int)study->seeds[i].w < (int)study->pcap.size()))
      {
        this->seedsPatternMatching.push_back(transformedSeed);      
        this->attribsPatternMatching.push_back(blank);
      }      
    }

    this->algorithmLineTrace->qfeGenerateSeeds(study->pcap, &this->seedsPatternMatching);
    this->algorithmLineTrace->qfeGeneratePathLinesStatic(study->pcap, &this->seedsPatternMatching, &this->attribsPatternMatching);
  }   

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeClearSeeds()
{
  this->seedsPatternMatching.clear();

  this->algorithmLineTrace->qfeClearSeedPositions();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeGetDataSet(qfeDataTypeIndex index, qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu)
{
  qfeStudy           *study;
  int                 current;

  *volume = NULL;
  vl = vu = 0.0;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(dataTypePCAP) : // PCA-P
    if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time))
    {
      *volume = study->pcap[time];
      study->qfeGetStudyRangePCAP(vl, vu);
    }
    break;
  case(dataTypePCAM) : // PCA-M
    if(((int)study->pcam.size() > 0) && ((int)study->pcam.size() > time))
    {
      *volume = study->pcam[time];
      study->qfeGetStudyRangePCAM(vl, vu);
    }
    break;
  case(dataTypeSSFP) : // SSFP
    if((int)study->ssfp3D.size() > 0)
    {
      // If there is also pcap data loaded, check for the trigger time and load
      // accompanying ssfp data at the right time slot
      if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time))
      {
        qfeFloat att, ptt;
        qfeFloat distance = 9000;
        (int)study->pcap[time]->qfeGetVolumeTriggerTime(ptt);
        for(int i=0; i<(int)study->ssfp3D.size(); i++)
        {
          (int)study->ssfp3D[i]->qfeGetVolumeTriggerTime(att);
          if(abs(att-ptt) < distance)
          {
            distance = abs(att-ptt);
            *volume = study->ssfp3D[i];
          }
        }
      }
      else
        *volume = study->ssfp3D[0];

      study->qfeGetStudyRangeSSFP(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(dataTypeTMIP) : // T-MIP
    if(study->tmip != NULL)
    {
      *volume = study->tmip;
      study->tmip->qfeGetVolumeValueDomain(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(dataTypeFFE) : // Flow anatomy FFE
    if(((int)study->ffe.size() > 0) && ((int)study->ffe.size() > time))
    {
      *volume = study->ffe[time];
      study->qfeGetStudyRangeFFE(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;  
  }

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeCreateTextures(qfeViewport *vp, int supersampling)
{
  if(supersampling <= 0) return qfeError;

  glGenTextures(1, &this->intersectTexColor);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexColor);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F,
    vp->width*supersampling, vp->height*supersampling, 0, GL_BGRA, GL_FLOAT, NULL);

  glGenTextures(1, &this->intersectTexDepth);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 
     vp->width*supersampling, vp->height*supersampling, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeDeleteTextures()
{
  if (glIsTexture(this->intersectTexColor))     glDeleteTextures(1, (GLuint *)&this->intersectTexColor);
  if (glIsTexture(this->intersectTexDepth))     glDeleteTextures(1, (GLuint *)&this->intersectTexDepth);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeBindTextures()
{
  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexColor);

  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_2D, this->intersectTexDepth);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeEnableIntersectBuffer()
{
  this->qfeAttachFBO(this->frameBuffer, this->intersectTexColor, this->intersectTexColor, this->intersectTexDepth);
  this->qfeEnableFBO(this->frameBuffer);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverPMR::qfeDisableIntersectBuffer()
{
  this->qfeResetFBO();
  this->qfeEnableFBO(this->frameBuffer);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeDriverPMR::qfeOnAction(void *caller, int t)
{
  qfeDriverPMR *self = reinterpret_cast<qfeDriverPMR *>(caller);

  switch(t)
  { 
  case qfeVisualPatternMatching::actionInjectSeedsFile :         
         self->qfeInjectFileSeeds();       
         break;
  case qfeVisualPatternMatching::actionClearSeeds :
         self->qfeClearSeeds();
         break;
  }  

  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverPMR::qfeOnResize(void *caller)
{
  qfeDriverPMR *self = reinterpret_cast<qfeDriverPMR *>(caller);
  qfeViewport  *vp;

  qfeDriver::qfeDriverResize(caller);

  self->engine->GetViewport3D(&vp);

  // Refresh size of local geometry texturs
  self->qfeUnbindTextures();
  self->qfeDeleteTextures();
  self->qfeCreateTextures(vp, self->frameBufferProps[0].supersampling);
  self->qfeUnbindTextures();

  // Refresh size of the MIP textures
  self->algorithmDVR->qfeSetViewportSize(vp->width, vp->height, self->frameBufferProps[0].supersampling);
}

//----------------------------------------------------------------------------
void qfeDriverPMR::qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v)
{
  qfeDriverPMR *self = reinterpret_cast<qfeDriverPMR *>(caller);

  // Deal with the volume rendering - multiresolution
  self->dvrQuality = 4.0;

  self->engine->Refresh();
  return;
}

//----------------------------------------------------------------------------
void qfeDriverPMR::qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v)
{
  qfeDriverPMR *self = reinterpret_cast<qfeDriverPMR *>(caller);

  qfeColorRGB color;
  float       depth;

  self->qfeReadSelect(x, y, color);
  self->qfeReadDepth(x, y, depth);

  // Deal with the volume rendering - multiresolution
  self->dvrQuality = 1.0;
}
