#include "qfeDriverIV.h"

//----------------------------------------------------------------------------
qfeDriverIV::qfeDriverIV(qfeScene *scene) : qfeDriver(scene)
{
  this->visualMIP				= NULL;
  this->visualInkVisP			= NULL;
  this->visualSeedVols			= NULL;
  this->visualReformat          = NULL;
  this->visAides				= NULL;

  // Initialize plane line colors
  this->colorReformatDefault.r = 0.952;
  this->colorReformatDefault.g = 0.674;
  this->colorReformatDefault.b = 0.133;

  this->colorReformatHighlight.r = 0.356;
  this->colorReformatHighlight.g = 0.623;
  this->colorReformatHighlight.b = 0.396;

  this->currentTransferFunctionId = 0;

  this->algorithmReformat	= new qfePlanarReformat();

  this->algorithmMIP		= new qfeMaximumProjection();

  this->algorithmIP			= new qfeInkParticles();

  this->algorithmSurfaceShading = new qfeSurfaceShading();
  // Initialize the planes (in patient coordinates)
  this->planeX = new qfeFrameSlice();      
  this->planeX->qfeSetFrameExtent(1,1);
  this->planeX->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeX->qfeSetFrameAxes(0.0,0.0,1.0, 0.0,1.0,0.0, 1.0,0.0,0.0);  

  this->planeY = new qfeFrameSlice();      
  this->planeY->qfeSetFrameExtent(1,1);
  this->planeY->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeY->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,0.0,1.0, 0.0,1.0,0.0);  
  
  this->planeZ = new qfeFrameSlice();      
  this->planeZ->qfeSetFrameExtent(1,1);
  this->planeZ->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeZ->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0);  

  planeQ = new qfeFrameSlice();
  planeQ->qfeSetFrameExtent(1,1);
  planeQ->qfeSetFrameOrigin(0.0,0.0,0.0);
  planeQ->qfeSetFrameAxes(0.0,0.0,1.0, 0.0,1.0,0.0, 1.0,0.0,0.0); 

  this->selectedPlane = qfePlaneNone;

  this->engine->SetObserverAction((void*)this, this->qfeOnAction);
  this->engine->SetObserverResize((void*)this, this->qfeOnResize);

  this->engine->SetInteractionMouseMove((void*)this, this->qfeOnMouseMove, false);  
  this->engine->SetInteractionMouseLeftButtonDown((void*)this, this->qfeOnMouseLeftButtonDown);
  this->engine->SetInteractionMouseRightButtonUp((void*)this, this->qfeOnMouseRightButtonUp);
  this->engine->SetInteractionMouseRightButtonDown((void*)this, this->qfeOnMouseRightButtonDown, false);
  this->engine->SetInteractionMouseWheelForward((void*)this, this->qfeOnWheelForward, false);
  this->engine->SetInteractionMouseWheelBackward((void*)this, this->qfeOnWheelBackward, false);
  
  currentTime = 0.0f;

  selected_seeding_point= seedingPointNone;  
  particleSinkGuidePointSelected = false;
  newParticleSink = false;

  rightMouseDown = false;

  filteringGraphRendered = false;

  screen_size_x = 1;
  screen_size_y = 1;

  brush_active = false;
  brush_position1 = Vec2f(-1,-1);
  brush_position2 = Vec2f(-1,-1);
};

//----------------------------------------------------------------------------
qfeDriverIV::~qfeDriverIV()
{
	//remove iso surfaces if they exists
	std::string label = "Iso surface";
	std::string lab = "";
	for(unsigned int mesh_id = 0; mesh_id<study->segmentation.size(); mesh_id++)
	{
		study->segmentation.at(mesh_id)->qfeGetModelLabel(lab);
		if(label.compare(lab) == 0)
		{
			study->segmentation.erase(study->segmentation.begin()+mesh_id);
		}
	}

  delete this->planeX;
  delete this->planeY;
  delete this->planeZ;
  delete this->planeQ;

  delete this->algorithmMIP;
  delete this->algorithmIP;
  delete this->algorithmSurfaceShading;
  delete this->algorithmReformat;

  this->qfeRenderStop();
};

//----------------------------------------------------------------------------
bool qfeDriverIV::qfeRenderCheck()
{
  int t;

  for(int i=0; i<(int)this->visuals.size(); i++)
  {    
	this->visuals[i]->qfeGetVisualType(t);

	if(t == visualInkVis)
	{	  	  
	  this->visualInkVisP = static_cast<qfeVisualInkVis *>(this->visuals[i]);

	  this->visuals[i]->qfeGetVisualStudy(&study);  

	  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, study->pcap.at(0));
	  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, study->pcap.at(0));
	  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, study->pcap.at(0));
	  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, study->pcap.at(0));
	  
	  study->pcap.at(0)->qfeGetVolumeSize(ni, nj, nk);

	  seedPosition1 = qfePoint(0.4*ni,0.5*nj,0.5*nk);
	  seedPosition2 = qfePoint(0.6*ni,0.5*nj,0.5*nk);
	  seedPosition3 = qfePoint(0.5*ni,0.6*nj,0.5*nk);

	  //user study:
	  //seedPosition1 = qfePoint(0.25*ni,0.5*nj,0.05*nk);
	  //seedPosition2 = qfePoint(0.75*ni,0.5*nj,0.05*nk);
	  //seedPosition3 = qfePoint(0.5*ni,0.5*nj,0.1*nk);

	  seedPosition3_enabled = false;

	  //VCBM VIS award approx seed settings:
	  //- full dissection data
	  //- streak visualization
	  //- animation length 14 from
	  //- start phase 0
	  //- no uncertainty	  
	  //seedPosition1 = qfePoint(21, 63, 47);
	  //seedPosition2 = qfePoint(38, 63, 47);

	  //Uncertainty vs certainty: vorticity dissection
	  //- dissection data at phase 11
	  //- stream visualization
	  //- animation length 10 phases
	  //seedPosition1 = qfePoint(42.5, 20, 38);
	  //seedPosition2 = qfePoint(42.5, 20, 30);

	  //volumetric seeding healthy
	  //seedPosition1 = qfePoint(5, 45, 23);
	  //seedPosition2 = qfePoint(23, 45, 21);
	  //seedPosition3 = qfePoint(16, 76, 15);

	  //line seeding healthy
	  //comparison streak 10 phases (age)
	  //seedPosition1 = qfePoint(13, 60, 18);
	  //seedPosition2 = qfePoint(19, 60, 18);
	  //seedPosition3 = qfePoint(14.5, 54, 20);

	  //line seeding healthy2
	  //phase 2, stream
	  //seedPosition1 = qfePoint(12.5, 60, 18);
	  //seedPosition2 = qfePoint(19, 60, 18);
	  //seedPosition3 = qfePoint(14.5, 54, 20);

	  //point seeding healthy
	  //- phase 2
	  //- stream visualization
	  //- animation length 5
	  //seedPosition1 = qfePoint(15.7, 64, 15.3);
	  //seedPosition2 = qfePoint(15.9, 64, 15.4);

	  //point seeding healthy, influence uncertainty 70/30 probability for "certain" vis
	  //- phase 2
	  //- stream visualization
	  //- animation length 5
	  //seedPosition1 = qfePoint(16.6, 62, 15.1);
	  //seedPosition2 = qfePoint(16.8, 62, 15.3);

	   //point seeding healthy
	  //- phase 1
	  //- path visualization
	  //- animation length 6
	  //seedPosition1 = qfePoint(13.0, 64, 15.0);
	  //seedPosition2 = qfePoint(13.2, 64, 15.2);

	  //volume seeding unhealthy helix core:
	  //phase 3, stream
	  //seedPosition1 = qfePoint(32.8906, 76.0003, 37.8072);
	  //seedPosition2 = qfePoint(29.6769, 76.0003, 36.9517);
	  //seedPosition3 = qfePoint(30.053, 58.0005, 45.4825);
	  
	  //line version is average of seedpoint 1 and 2
	  //seedPosition1 = qfePoint((seedPosition1.x+seedPosition2.x)/2.0f, (seedPosition1.y+seedPosition2.y)/2.0f, (seedPosition1.z+seedPosition2.z)/2.0f);
	  //seedPosition2 = seedPosition3;

	  //volume unsteady unhealthy flow:
	  //phase 4, any visualization type
	  //duration 10 phases
	  //seedPosition1 = qfePoint(34, 59, 44);
	  //seedPosition2 = qfePoint(27, 59, 44);
	  //seedPosition3 = qfePoint(30.5, 44, 44);

	  //single point path/streak
	  //dissection: phase 9, length 6
	  //seed from voxel center
	  //seedPosition1 = qfePoint(36.5, 25.1, 36.8);
	  //seedPosition2 = qfePoint(36.5, 25.3, 36.3);
	  //seedPosition3 = qfePoint(36.2, 25.2, 36.5);

	  //dissection ring ascending:
	  //seedPosition1 = qfePoint(18.0, 47.0, 48);
	  //seedPosition2 = qfePoint(39.0, 47.0, 48);
	  //seedPosition3 = qfePoint(28.5, 54.0, 48);

	  //dissection ring ascending vortex (path, phase 4, length 6-7 phases, to show "false" certainty 10 phases and the graph):
	  //seedPosition1 = qfePoint(31.0, 42.0, 48);
	  //seedPosition2 = qfePoint(36.0, 42.0, 48);
	  //seedPosition3 = qfePoint(34.0, 53.0, 48);

	  algorithmIP->qfeInitialize(study);

	  algorithmIP->qfeGetSNRVolumes(this->SNR_Volumes, this->SNR_Range, 3);
	  algorithmIP->qfeGetSignalStrengthVolumes(this->Signal_Strength_Volumes, this->Signal_Strength_Range, 3);
	  algorithmIP->qfeGetParticleDensityVolume(this->particleDensityVolume, 3, this->seedPosition3_enabled);
	  if(study->segmentation.size()>0)
		algorithmIP->qfeGetMeshLevelsetVolume(this->meshLevelsetVolume, meshLevelSetRange, 1);

	}
	if(t == visualSeedVolumesType)
	{
		this->visualSeedVols = static_cast<qfeVisualSeedVolumes *>(this->visuals[i]);
		
		//in this case, algorythmSP is owner of the SeedVolumeCollection
		//therefore we don't have to do more here
	}
	if(t == visualMaximum)
	{	  	
	  this->visualMIP = static_cast<qfeVisualMaximum *>(this->visuals[i]);

	  //change parameters to different default values:
	  qfeVisualParameter p;

	  this->visualMIP->qfeRemoveVisualParameter("MIP visible");
	  p.qfeSetVisualParameter("MIP visible", false);  
	  this->visualMIP->qfeAddVisualParameter(p);

	  this->visualMIP->qfeRemoveVisualParameter("MIP gradient");
	  p.qfeSetVisualParameter("MIP gradient", false);  
	  this->visualMIP->qfeAddVisualParameter(p);

	  this->visualMIP->qfeRemoveVisualParameter("MIP transparency");
	  p.qfeSetVisualParameter("MIP transparency", true);  
	  this->visualMIP->qfeAddVisualParameter(p);

	  this->visualMIP->qfeRemoveVisualParameter("MIP data set");
	  qfeSelectionList dataSet;
	  dataSet.list.push_back("pca-p");
	  dataSet.list.push_back("pca-m");
	  dataSet.list.push_back("ssfp");
	  dataSet.list.push_back("t-mip");
	  dataSet.list.push_back("ffe");
	  dataSet.list.push_back("ftle");
	  dataSet.list.push_back("SNR");
	  dataSet.list.push_back("Signal strength");
	  dataSet.list.push_back("Particle density");
	  dataSet.list.push_back("Seeding origin");
	  dataSet.list.push_back("Seeding origins");
	  dataSet.list.push_back("Leaving mesh");
	  if(study->segmentation.size()>0)
		  dataSet.list.push_back("Solid levelset");
	  dataSet.active = 0;
	  if(study->pcam.size()!=0)
		dataSet.active = 1;
	  if(study->tmip!=NULL)
		  dataSet.active = 3;
	  p.qfeSetVisualParameter("MIP data set", dataSet);  
	  this->visualMIP->qfeAddVisualParameter(p);

	  this->visuals[i]->qfeGetVisualStudy(&study);  
	}
	if(t == visualAnatomyIllustrative)
	{
		this->visualAnatomy = static_cast<qfeVisualAnatomyIllustrative *>(this->visuals[i]);   
	}
	if(t == visualAides)
	{
	  this->visAides = static_cast<qfeVisualAides *>(this->visuals[i]);
	}
	if (t == visualReformatOrtho)
	{
		bool mesh_exists = study->segmentation.size()>0;

		//change parameters to different default values:
		qfeVisualParameter p;

		visualReformat = static_cast<qfeVisualReformatOrtho *>(visuals[i]);
		
		this->visualReformat->qfeRemoveVisualParameter("Ortho data set");
		qfeSelectionList dataSet;
		dataSet.list.push_back("pca-p");
		dataSet.list.push_back("pca-m");
		dataSet.list.push_back("ssfp");
		dataSet.list.push_back("t-mip");
		dataSet.list.push_back("ffe");
		dataSet.list.push_back("ftle");
		dataSet.list.push_back("SNR");
		dataSet.list.push_back("Signal strength");
		dataSet.list.push_back("Particle density");
		dataSet.list.push_back("Seeding origin");
		dataSet.list.push_back("Seeding origins");
		dataSet.list.push_back("Leaving mesh");
		if(study->segmentation.size()>0)
		  dataSet.list.push_back("Solid levelset");
		dataSet.active = 0;
		if(study->pcam.size()!=0)
			dataSet.active = 1;
		if(study->tmip!=NULL)
			dataSet.active = 3;
		if(study->ffe.size()!=0)
			dataSet.active = 4;
		p.qfeSetVisualParameter("Ortho data set", dataSet);  
		this->visualReformat->qfeAddVisualParameter(p);
	}
	if (t == visualPlaneOrthoViewType)
	{
		this->visualPOV = static_cast<qfeVisualPlaneOrthoView *>(this->visuals[i]);
	}
  }

  return true;
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeRenderStart()
{
  qfeVolume          *volume = NULL;
  qfeGrid            *grid   = NULL;
  qfePoint            origin;

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  int                 paramData;

  if(this->visualReformat != NULL) 
  {
	this->visualReformat->qfeGetVisualParameter("Ortho data set", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
	paramData = paramDataList.active;

	int current;
	this->visualReformat->qfeGetVisualActiveVolume(current);
	this->qfeGetDataSet(paramData, this->visualReformat, current, &volume);
  }

  if(volume != NULL)
  {
	this->qfeGetPlaneOrtho(volume, qfePlaneSagittal, &this->planeX);  
	this->qfeGetPlaneOrtho(volume, qfePlaneCoronal,  &this->planeY);
	this->qfeGetPlaneOrtho(volume, qfePlaneAxial,    &this->planeZ);

	qfeGrid *grid;
    qfePoint o;
    volume->qfeGetVolumeGrid(&grid);
    grid->qfeGetGridOrigin(o.x, o.y, o.z);
    planeQ->qfeSetFrameOrigin(o.x, o.y, o.z);
    qfeGetPlaneViewOrthogonal(volume, &planeQ);
  }

  qfeViewport *vp3D;
  this->engine->GetViewport3D(&vp3D);

  this->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeRenderWait()
{
  //THE ORDER OF RENDERING IS IMPORTANT
  
  if(this->visualAnatomy != NULL)
  {
	  this->qfeRenderVisualAnatomyIllustrative(this->visualAnatomy);
  }

  qfeColorMap *colormap1;
  qfeColorMap *colormap2;
  qfeColorMap *colormap3;

  if(this->visualReformat != NULL) 
  {
	this->visualReformat->qfeGetVisualColorMap(&colormap2);
	colormap2->qfeUploadColorMap();   
  }
  
  // Render the reformat
  if(this->visualReformat != NULL)
  {
	this->qfeRenderVisualPlanesOrtho(this->visualReformat);        
  }

  if(this->visualPOV != NULL)
  {
    qfeRenderPlaneOrthoView(visualPOV, this->visualReformat);
  }

  //render seed volumes
  if(this->visualSeedVols != NULL)
  {
	  this->qfeRenderVisualSeedVolumes(this->visualSeedVols);
  }

  //render ink
  if(this->visualInkVisP != NULL)
  {
	  bool MIP_visible = false;
	  if(this->visualMIP != NULL)
	  {
		  qfeVisualParameter *param;	
		  visualMIP->qfeGetVisualParameter("MIP visible", &param);
		  if(param != NULL) param->qfeGetVisualParameterValue(MIP_visible); 
	  }

	  if(MIP_visible)
		  glDisable(GL_DEPTH_TEST);

	  this->qfeRenderVisualInk(this->visualInkVisP);

	  if(MIP_visible) 
		  glEnable(GL_DEPTH_TEST);
  }

  // Render visual aides
  if(this->visAides != NULL)
  {
	this->qfeRenderVisualAides(this->visAides);
  }

  // Initialize OpenGL state
  glDisable(GL_DITHER);
  glEnable(GL_BLEND);
  glDisable(GL_NORMALIZE);
  glDisable(GL_LIGHTING);
  glDisable(GL_COLOR_MATERIAL);
  glEnable(GL_DEPTH_TEST);  

  // Initialize color map
  if(this->visualMIP != NULL) 
  {
	this->visualMIP->qfeGetVisualColorMap(&colormap1);
	colormap1->qfeUploadColorMap();   
  }

  // Render MIP
  if(this->visualMIP != NULL)
  {    
	this->qfeRenderVisualMIP(this->visualMIP); 
  } 

  glDisable(GL_DITHER);
  glDisable(GL_NORMALIZE);
  glDisable(GL_LIGHTING);
  glDisable(GL_COLOR_MATERIAL);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Render visual aides
  if(this->visAides != NULL)
  {
	this->qfeRenderVisualAides(this->visAides);
  }

  //render colorscheme
  if(this->visualInkVisP != NULL)
  {
	  qfeVisualParameter *param;
	  bool renderColorScheme = false;
	  
	  this->visualInkVisP->qfeGetVisualParameter("Render colorscheme", &param);
	  if(param != NULL) param->qfeGetVisualParameterValue(renderColorScheme);

	  this->visualInkVisP->qfeGetVisualColorMap(&colormap3);
	  colormap3->qfeUploadColorMap();
	  
	  if(renderColorScheme)
	  {
		  qfeViewport *vp3D;
		  this->engine->GetViewport3D(&vp3D);
		  this->algorithmIP->qfeRenderColormapToScreen(vp3D->width, vp3D->height);
	  }
  }
  
  this->engine->Refresh();
};

void qfeDriverIV::qfeRenderSelect()
{
	  qfeVolume    *volume;

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  int                 paramData;
  bool                paramShowX, paramShowY, paramShowZ, paramShowQ;

  qfeColorRGB   color;
  
  // Get the visual parameters
  this->visualReformat->qfeGetVisualParameter("Ortho data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  this->visualReformat->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);    

  this->visualReformat->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);    

  this->visualReformat->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);   

  visualPOV->qfeGetVisualParameter("Show view orthogonal plane", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowQ);   

  // Process the visual parameters  
  int current;
  this->visualReformat->qfeGetVisualActiveVolume(current);
  this->qfeGetDataSet(paramData, this->visualReformat, current, &volume);    

  if(paramShowX)
  {
	color.r = 1.0; color.g = 0.0; color.b = 0.0;
	this->qfeSetSelectColor(color);
	this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeX, NULL);
  }

  if(paramShowY)
  {
	color.r = 0.0; color.g = 1.0; color.b = 0.0;
	this->qfeSetSelectColor(color);
	this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeY, NULL);
  }
  
  if(paramShowZ)
  {
	color.r = 0.0; color.g = 0.0; color.b = 1.0;
	this->qfeSetSelectColor(color);
	this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeZ, NULL);
  }

  if (paramShowQ) 
  {
    qfeSetSelectColor(qfeColorRGB(0.5f, 0.5f, 0.5f));
    this->algorithmReformat->qfeRenderPolygon3D(volume, planeQ, NULL);
  }
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeRenderStop()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIV::qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual)
{ 
	qfeStudy              *study;
	qfeVolume             *volume;
	qfeLightSource        *light;
	qfeModel              *model;  
	vtkPolyData           *mesh; 
	qfeMeshVertices       *vertices         = NULL;
	qfeMeshVertices       *normals          = NULL;
	qfeMeshIndices        *indicesTriangles = NULL;
	int                    current;

	visual->qfeGetVisualStudy(&study);  
	visual->qfeGetVisualActiveVolume(current);  

	this->scene->qfeGetSceneLightSource(&light);

	volume = study->pcap[current];

	if(volume == NULL)
	{
		cout << "qfeDriverFS::qfeRenderVisualAnatomyIllustrative - Incorrect volume data" << endl;
		return qfeError;
	}

	if(study == NULL)
	{
		cout << "qfeDriverFS::qfeRenderVisualAnatomyIllustrative - Incorrect study" << endl;
		return qfeError;
	}
		
	bool                surfaceClipping;

	// Get the parameters
	qfeSelectionList    paramSurfaceShadingList;
	int                 paramSurfaceShading;
	bool                paramSurfaceContour;

	qfeVisualParameter *param;  

	visual->qfeGetVisualParameter("Surface shading", &param);    
	if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceShadingList); 
	paramSurfaceShading       = paramSurfaceShadingList.active; 

	visual->qfeGetVisualParameter("Surface clipping", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(surfaceClipping);

	visual->qfeGetVisualParameter("Surface contour", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceContour); 

	bool                visible;  
	qfeColorRGB         colorSilhouette;
	qfeColorRGBA        colorContour;
	qfeColorRGBA        colorContourHidden;    

	// Process the parameter structures   
	colorContour.r       = 0.3; 
	colorContour.g       = 0.3;
	colorContour.b       = 0.3;
	colorContourHidden.r = 0.7;
	colorContourHidden.g = 0.7;
	colorContourHidden.b = 0.7;   

	// Set general variables for all meshes
	this->algorithmSurfaceShading->qfeSetLightSource(light);

	bool enableSurfaceClipping = surfaceClipping;

	for(int i=0; i<(int)study->segmentation.size(); i++)
	{   
		model = study->segmentation[i];
		
		this->algorithmSurfaceShading->qfeSetSurfaceShading((qfeSurfaceShadingType)paramSurfaceShading);

		// Get the parameters
		model->qfeGetModelColor(colorSilhouette);        

		// Get the mesh    
		model->qfeGetModelMesh(&mesh);    
		model->qfeGetModelMesh(&vertices, &normals);

		if(vertices->size() == 0)
			continue;

		model->qfeGetModelMeshIndicesTriangles(&indicesTriangles);
		model->qfeGetModelVisible(visible);

		enableSurfaceClipping = surfaceClipping;
				
		// Draw the mesh
		if(vertices != NULL && vertices->size() >= 3 && visible)
		{
			// Update the surface parameters      
			this->algorithmSurfaceShading->qfeSetSurfaceColor(colorSilhouette);

			if(paramSurfaceContour) 
			{
				this->algorithmSurfaceShading->qfeSetSurfaceCulling(true);
				this->algorithmSurfaceShading->qfeRenderMeshDilate(vertices, normals, indicesTriangles, 0.2, volume);
			}  

			// Render the mesh
			
			this->algorithmSurfaceShading->qfeSetSurfaceCulling(enableSurfaceClipping);
			this->algorithmSurfaceShading->qfeRenderMesh(vertices, normals, indicesTriangles, volume);            
		}
	}

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIV::qfeRenderVisualSeedVolumes(qfeVisualSeedVolumes *visual)
{
	qfeViewport *vp3D;
	this->engine->GetViewport3D(&vp3D);

	vtkCamera *vtkcamera = this->engine->GetRenderer()->GetActiveCamera();

	qfeVisualParameter *param;	  

	bool visible = false;
	visual->qfeGetVisualParameter("Seed Volumes Visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(visible);
	
	if(!visible)
		return qfeSuccess;

	bool renderSurfaces = false;
	visual->qfeGetVisualParameter("Surfaces visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(renderSurfaces);

	this->algorithmIP->particleSinks->qfeRenderSeedVolumes(vtkcamera, vp3D->width, vp3D->height, this->pointSelected, renderSurfaces);

	if(this->newParticleSink)
	{
		for(unsigned int i = 0; i<newParticleSinkPoints.size(); i++)
		{			
			qfePoint p = newParticleSinkPoints.at(i);

			this->algorithmIP->qfeRenderCube(p, 1, true, qfeColorRGB(1.0f,0.5f,0.5f)); 
		}
	}
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIV::qfeRenderVisualInk(qfeVisualInkVis *visual)
{
	qfeViewport *vp3D;
	this->engine->GetViewport3D(&vp3D);

	int visualInkVisP = 0;
	qfeVisualParameter *param;	  
	qfeSelectionList    paramList;

	qfeColorMap *colormap;
	visual->qfeGetVisualColorMap(&colormap);
	this->algorithmIP->qfeUpdateColorMap(colormap);
	
	bool depthDarkening = true;
	double depthDarkeningAmount = 0.0;
	bool depthDarkeningLight = false;
	bool chromaDepth = true;
	bool aerialPerspective = true;

	bool clip_seed_outside_mesh = true;

	bool clip_outside_mesh = false;
	double clipping_margin = 0.0;

	bool clip_seed_volume = false;

	double particle_size = 1.0;
	int particle_type = 0;

	bool seedPlanar = false;

	std::array<float, 3> spacing;
	qfeGrid *grid = new qfeGrid();
	study->pcap[0]->qfeGetVolumeGrid(&grid);
	grid->qfeGetGridExtent(spacing[0],spacing[1],spacing[2]);

	bool render = false;
	visual->qfeGetVisualParameter("Ink visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(render);
	if(!render)
		return qfeSuccess;
	
	visual->qfeGetVisualParameter("Advection type", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramList);
	this->algorithmIP->qfeSetAdvectionType((advection_type) paramList.active);
	
	visual->qfeGetVisualParameter("Volumetric seeding", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(seedPosition3_enabled);
	
	visual->qfeGetVisualParameter("Planar seeding", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(seedPlanar);	
	
	if(!seedPosition3_enabled && seedPlanar)
		seedPosition3_enabled = true;
	if(!seedPosition3_enabled && !seedPlanar)
		seedPosition3_enabled = false;
	
	double particle_opacity = 0.0f;
	visual->qfeGetVisualParameter("Particle opacity", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(particle_opacity);
	this->algorithmIP->qfeSetParticleOpacity((float) particle_opacity);

	bool mapOpacityToUncertainty = false;
	visual->qfeGetVisualParameter("Map opacity to uncertainty", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(mapOpacityToUncertainty);
	
	bool use_uncertainty = true;
	visual->qfeGetVisualParameter("Uncertainty", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(use_uncertainty);
	this->algorithmIP->qfeUseUncertainty(use_uncertainty);
	
	visual->qfeGetVisualParameter("Depth enhancement", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(depthDarkening);

	visual->qfeGetVisualParameter("Depth enhancement amount", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(depthDarkeningAmount);

	visual->qfeGetVisualParameter("Depth enhancement light", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(depthDarkeningLight);

	visual->qfeGetVisualParameter("Chroma depth", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(chromaDepth);

	visual->qfeGetVisualParameter("Aerial perspective", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(aerialPerspective);

	visual->qfeGetVisualParameter("Clip seed outside mesh", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(clip_seed_outside_mesh);
	this->algorithmIP->qfeClipSeedOutsideMesh(clip_seed_outside_mesh);

	visual->qfeGetVisualParameter("Clip outside mesh", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(clip_outside_mesh);
	visual->qfeGetVisualParameter("Clipping margin (mm)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(clipping_margin);
	this->algorithmIP->qfeClipOutsideMesh(clip_outside_mesh, (float)clipping_margin);

	visual->qfeGetVisualParameter("Clip seed volume", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(clip_seed_volume);
		
	visual->qfeGetVisualParameter("Particle size", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(particle_size);

	particle_size = (particle_size*spacing[2])/2.5f;
	
	double particle_life = 0.0f;
	
	int number_of_lines = 0;
	visual->qfeGetVisualParameter("Number of lines", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(number_of_lines);	  	
	this->algorithmIP->qfeSetNumberOfLines((unsigned int) number_of_lines);

	//int number_of_color_bands = 0;
	//visual->qfeGetVisualParameter("Number of color bands", &param);
	//if(param != NULL) param->qfeGetVisualParameterValue(number_of_color_bands);
	
	//this->algorithmIP->qfeSetNumberOfColorBands(number_of_color_bands);
	
	double particle_multiplier = 0.0f;
	visual->qfeGetVisualParameter("Particle multiplier", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(particle_multiplier);	
	this->algorithmIP->qfeSetParticleMultiplier((float)particle_multiplier);
	
	visual->qfeGetVisualParameter("Particle lifetime (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(particle_life);  
	this->algorithmIP->qfeSetParticleLifeTime(particle_life);
	
	double step_size = 0.0f;
	visual->qfeGetVisualParameter("Animation step size (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(step_size);
	this->algorithmIP->qfeSetTimeStepSize(step_size);
	
	double animation_length = 0.0f;
	visual->qfeGetVisualParameter("Animation length (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(animation_length);  
	animation_duration = animation_length;	  

	double stop_seeding_after = 0.0;
	visual->qfeGetVisualParameter("Stop seeding (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(stop_seeding_after);  
	this->algorithmIP->qfeSetStopSeedingTime(stop_seeding_after);
		
	double CFL_scale = 1.0f;
	visual->qfeGetVisualParameter("CFL scaling", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(CFL_scale);  
	this->algorithmIP->qfeComputeCFL(CFL_scale);

	bool inverse_colorscheme = false;
	visual->qfeGetVisualParameter("Inverse colorscheme", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(inverse_colorscheme);
	this->algorithmIP->qfeInverseColorMap(inverse_colorscheme);
	
	visual->qfeGetVisualParameter("Color setting", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramList);
	this->algorithmIP->qfeSetColorSetting(paramList.active);

	visual->qfeGetVisualParameter("Particle type", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramList);
	particle_type = paramList.active;

	int spatialGapAmount1;
	int spatialGapAmount2;
	int temporalGapAmount;
	double spatialGapWidth1;
	double spatialGapWidth2;
	double temporalGapWidth;

	visual->qfeGetVisualParameter("Spatial gap amount 1", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(spatialGapAmount1);
	visual->qfeGetVisualParameter("Spatial gap amount 2", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(spatialGapAmount2);
	visual->qfeGetVisualParameter("Temporal gap amount", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(temporalGapAmount);
	visual->qfeGetVisualParameter("Spatial gap width 1", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(spatialGapWidth1);
	visual->qfeGetVisualParameter("Spatial gap width 2", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(spatialGapWidth2);
	visual->qfeGetVisualParameter("Temporal gap width", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(temporalGapWidth);

	int glyph_type = 0;
	int glyph_frequency = 0;

	visual->qfeGetVisualParameter("Glyph type", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramList);
	glyph_type = paramList.active;

	visual->qfeGetVisualParameter("Glyph frequency", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(glyph_frequency);
	
	this->algorithmIP->qfeSetGapSettings(spatialGapAmount1, spatialGapAmount2, temporalGapAmount, spatialGapWidth1, spatialGapWidth2, temporalGapWidth);

	visual->qfeGetVisualActiveVolume(currentTime);
	this->algorithmIP->qfeSetPhase(currentTime);
	
	vtkCamera *vtkcamera = this->engine->GetRenderer()->GetActiveCamera();
	std::array<double,3> camera;
	vtkcamera->GetPosition(camera[0], camera[1], camera[2]);
	
	std::array<double,3> viewUp;
	vtkcamera->GetViewUp(viewUp[0], viewUp[1], viewUp[2]);
	
	std::array<double,3> viewDir;
	vtkcamera->GetDirectionOfProjection(viewDir[0], viewDir[1], viewDir[2]);
	
	//draw the seeding points
	qfePoint p = (seedPosition1 * V2P);
	p.x += -viewDir[0]*0.01f; p.z += -viewDir[1]*0.01f;p.y += -viewDir[2]*0.01f;

	//change depth darkening based on opacity:	
	depthDarkening *= (1.0f-particle_opacity/2);

	
	if(selected_seeding_point != seedingPointNone)
	{
		p = seedPosition1;	
				
		if(selected_seeding_point == seedingPointTwo)
		{
			p = seedPosition2;
		}
		else if(selected_seeding_point == seedingPointThree)
		{
			p = seedPosition3;
		}
		p = p * V2P;
		p.x += -viewDir[0]*0.01f; p.y += -viewDir[1]*0.01f;p.z += -viewDir[2]*0.01f;
		
		//draw the selected points contour
		glPointSize(10.0f);
		glColor3f(0.0f,0.0f,0.0f);
		glBegin(GL_POINTS);
			glVertex3f(p.x,p.y,p.z);	
		glEnd();
		
		//draw the updated position point
		p = newSeedPosition * V2P;
		p.x += -viewDir[0]*0.01f; p.y += -viewDir[1]*0.01f; p.z += -viewDir[2]*0.01f;

		glPointSize(10.0f);

		if(selected_seeding_point == seedingPointOne)
			glColor3f(1.0f,0.5f,0.5f);
		if(selected_seeding_point == seedingPointTwo)
			glColor3f(0.5f,1.0f,0.5f);
		if(selected_seeding_point == seedingPointThree)
			glColor3f(0.5f,0.5f,1.0f);

		glBegin(GL_POINTS);
			glVertex3f(p.x,p.y,p.z);	
		glEnd();

		if(pointSelected.index>1)
		{
			glColor3f(0.0f,1.0f,0.0f);

			this->algorithmIP->qfeRenderCube(pointSelected.point, 1.5, true, qfeColorRGB(1.0f,0.0f,0.0f)); 
		}
	}

	bool render_seeding_position = true;
	visual->qfeGetVisualParameter("Render seeding", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(render_seeding_position);
	
	if(render_seeding_position)
	{
		if(!seedPosition3_enabled)
		{
			InkLine inkline;
			inkline[0] = seedPosition1;
			inkline[0].x += -viewDir[0]*0.01f; inkline[0].y += -viewDir[1]*0.01f; inkline[0].z += -viewDir[2]*0.01f;
			inkline[1] = seedPosition2;
			inkline[1].x += -viewDir[0]*0.01f; inkline[1].y += -viewDir[1]*0.01f; inkline[1].z += -viewDir[2]*0.01f;

			this->algorithmIP->qfeRenderSeedingLine(inkline);
		}
		else
		{
			InkVolume volume;
			volume[0] = seedPosition1;
			volume[1] = seedPosition2;
			volume[2] = seedPosition3;
			this->algorithmIP->qfeRenderSeedingVolume(volume, vtkcamera, vp3D->width, vp3D->height, seedPlanar);
		}
	}
	
	this->algorithmIP->qfeRenderGuideLines(viewDir);
	
	screen_size_x = vp3D->width;
	screen_size_y = vp3D->height;

	double znear, zfar;
	vtkcamera->GetClippingRange(znear,zfar);

	Vec2f age_selection;
	age_selection[0] = min(brush_position1[0],brush_position2[0])*particle_life;
	age_selection[1] = max(brush_position1[0],brush_position2[0])*particle_life;
		
	Vec2f filter_selection;
	filter_selection[0] = min(brush_position1[1],brush_position2[1]);
	filter_selection[1] = max(brush_position1[1],brush_position2[1]);
	
	bool invert_graph = false;
	visual->qfeGetVisualParameter("Invert graph", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(invert_graph);

	this->algorithmIP->qfeSetSelectionRange(age_selection, filter_selection, invert_graph);

	bool render_gpu_friendly = false;
	visual->qfeGetVisualParameter("Render GPU friendly", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(render_gpu_friendly);
		
	if(render_gpu_friendly)
		this->algorithmIP->qfeRenderParticlesGPUFriendly((float)particle_size, viewDir, clip_seed_volume, glyph_type, glyph_frequency);
	else
		this->algorithmIP->qfeRenderParticles(camera, viewUp, viewDir, (float)znear ,(float)zfar, vp3D->width, vp3D->height, mapOpacityToUncertainty, depthDarkening, depthDarkeningAmount, depthDarkeningLight, chromaDepth, aerialPerspective, (float)particle_size, particle_type, this->frameBuffer, glyph_type, glyph_frequency, clip_seed_volume);

	bool renderGraph = false;
	visual->qfeGetVisualParameter("Show graph", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(renderGraph);

	if(renderGraph)
	{
		graph_data_type graph_data_id = UncertaintyGraph;
		visual->qfeGetVisualParameter("Graph data", &param);
		if(param != NULL) 
		{
			param->qfeGetVisualParameterValue(paramList);
			graph_data_id = (graph_data_type) paramList.active;
		}

		bool invert_graph = false;
		visual->qfeGetVisualParameter("Invert graph", &param);
		if(param != NULL) param->qfeGetVisualParameterValue(invert_graph);

		this->algorithmIP->qfeRenderGraph(vp3D->height/2, vp3D->height/2, (graph_data_type) graph_data_id, invert_graph);

		filteringGraphRendered = true;
		
		glDisable(GL_DEPTH_TEST);
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glOrtho(0.0, vp3D->width, 0.0, vp3D->height, -1.0, 1.0);
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		
		glLoadIdentity();
		glDisable(GL_LIGHTING);

		/*
		glColor4f(0.0f, 1.0f, 0.0f, 0.1f);
		glBegin(GL_POLYGON); // for the interior
			glVertex2f(brush_position1[0]*(screen_size_y/2), brush_position1[1]*(screen_size_y/2));
			glVertex2f(brush_position1[0]*(screen_size_y/2), brush_position2[1]*(screen_size_y/2));
			glVertex2f(brush_position2[0]*(screen_size_y/2), brush_position2[1]*(screen_size_y/2));
			glVertex2f(brush_position2[0]*(screen_size_y/2), brush_position1[1]*(screen_size_y/2));
		glEnd();
		*/

		glColor4f(0.0f, 1.0f, 0.0f, 0.5f);
		glLineWidth(5.0f);
		glBegin(GL_LINE_LOOP); // for just the outline
			glVertex2f(brush_position1[0]*(screen_size_y/2), brush_position1[1]*(screen_size_y/2));
			glVertex2f(brush_position1[0]*(screen_size_y/2), brush_position2[1]*(screen_size_y/2));
			glVertex2f(brush_position2[0]*(screen_size_y/2), brush_position2[1]*(screen_size_y/2));
			glVertex2f(brush_position2[0]*(screen_size_y/2), brush_position1[1]*(screen_size_y/2));
		glEnd();
				
		glPopMatrix();
		
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();

		glMatrixMode(GL_MODELVIEW);
		glEnable(GL_DEPTH_TEST);
	}
	else
	{
		brush_position1 = Vec2f(-1,-1);
		brush_position2 = Vec2f(-1,-1);

		filteringGraphRendered = false;

	}

	bool renderIsoSurface = false;
	visual->qfeGetVisualParameter("Render iso surface", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(renderIsoSurface);

	//remove previous iso surface if it exists
	std::string label = "Iso surface";
	std::string lab = "";
	for(unsigned int mesh_id = 0; mesh_id<study->segmentation.size(); mesh_id++)
	{
		study->segmentation.at(mesh_id)->qfeGetModelLabel(lab);
		if(label.compare(lab) == 0)
		{
			study->segmentation.erase(study->segmentation.begin()+mesh_id);
			
			continue;
		}
	}

	if(renderIsoSurface)
	{
		unsigned int selected_sink = 0;
		if(this->pointSelected.index > 0) //a particle sink is selected:
		{
			selected_sink = this->pointSelected.index;
		}

		double iso_value = 0.0;
		visual->qfeGetVisualParameter("Iso value", &param);
		if(param != NULL) param->qfeGetVisualParameterValue(iso_value);

		qfeVolume volume;
		algorithmIP->qfeGetSeedingOriginVolume(volume, 3, selected_sink);
	
		qfeModel *model;
		vtkPolyData           *mesh; 
		qfeMeshVertices       *vertices         = NULL;
		qfeMeshVertices       *normals          = NULL;
		qfeMeshIndices        *indicesTriangles = NULL;
	
		study->qfeGetIsoSurface(&volume, iso_value, &model);
	
		qfeColorRGB color = algorithmIP->particleSinks->qfeGetSelectedSeedVolumeColor(pointSelected);
		
		bool visible = true;
	
		model->qfeSetModelColor(color); 
	
		// Get the mesh    
		model->qfeGetModelMesh(&mesh);
		model->qfeGetModelMesh(&vertices, &normals);
	
		model->qfeSetModelLabel(label);
	
		if(vertices->size()>0)
		{	
			model->qfeGetModelMeshIndicesTriangles(&indicesTriangles);
			model->qfeGetModelVisible(visible);
		
			study->segmentation.push_back(model);		
		}
	}
	
	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIV::qfeRenderVisualMIP(qfeVisualMaximum *visual)
{  
  qfeVolume   *volume;
  qfeColorMap *colormap;
  int          current;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  bool                paramVisible;
  bool                paramGradient;
  bool                paramTransparency;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  double              paramC;
  double              paramB;
  qfeFloat            paramRange[2];
  int                 paramMode;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  // Fetch the parameters
  visual->qfeGetVisualParameter("MIP visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("MIP data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("MIP data component", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("MIP data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("MIP gradient", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramGradient);    

  visual->qfeGetVisualParameter("MIP transparency", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTransparency);    

  visual->qfeGetVisualParameter("MIP brightness", &param);
  param->qfeGetVisualParameterValue(paramB);

  visual->qfeGetVisualParameter("MIP contrast",   &param);
  param->qfeGetVisualParameterValue(paramC);

  if(paramVisible != true) return qfeError;  

  // Get the data set and compute the data range
  this->qfeGetDataSet(paramData, visual, current, &volume);

  switch(paramData)
  {
  case 0 : study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
		   break;
  case 1 : study->qfeGetStudyRangePCAM(paramRange[0], paramRange[1]);
		   break;
  case 2 : study->qfeGetStudyRangeSSFP(paramRange[0], paramRange[1]);
		   break;
  case 3 : if(study->tmip != NULL)
		   study->tmip->qfeGetVolumeValueDomain(paramRange[0], paramRange[1]);
		   // Non-float texture are clamped [0,1]
		   //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
		  // paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
		   break;
  case 4 : study->qfeGetStudyRangeFFE(paramRange[0], paramRange[1]);
		   // Non-float texture are clamped [0,1]
		   //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
		   //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
		   break;
  case 5:  study->qfeGetStudyRangeFTLE(paramRange[0], paramRange[1]);
		   break;
  case 6:  paramRange[0] = SNR_Range[0];
		   paramRange[1] = SNR_Range[1];
		   break;
  case 7:  paramRange[0] = Signal_Strength_Range[0];
		   paramRange[1] = Signal_Strength_Range[1];
		   break;
  case(8) : // Particle density
			paramRange[0] = 0.0f;
			paramRange[1] = 1.0f;
			break;
  case(9) : // Seeding origin
			paramRange[0] = 0.0f;
			paramRange[1] = 1.0f;
			break;
  case(10) : // Seeding origins
			paramRange[0] = 0.0f;
			paramRange[1] = this->algorithmIP->particleSinks->size()+1;
			break;
  case(11) : // Leaving Mesh
			paramRange[0] = 0.0f;
			paramRange[1] = 1.0;
			break;
  case(12) : // segmentation levelset
			paramRange[0] = meshLevelSetRange[0];
			paramRange[1] = meshLevelSetRange[1];
			break;
  default: study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
		   break;  
  }  

  if(volume == NULL) return qfeError;  

  volume->qfeGetVolumeType(paramMode);
  volume->qfeSetVolumeTextureId(-1);
  volume->qfeUpdateVolume();

  // Update algorithm parameters  
  this->algorithmMIP->qfeSetDataComponent(paramComp);
  this->algorithmMIP->qfeSetDataRepresentation(paramRepr);
  this->algorithmMIP->qfeSetDataRange(paramRange[0], paramRange[1]);
  this->algorithmMIP->qfeSetGradient(paramGradient);
  this->algorithmMIP->qfeSetTransparency(paramTransparency);
  this->algorithmMIP->qfeSetContrast(paramC);
  this->algorithmMIP->qfeSetBrightness(paramB);

  this->algorithmMIP->qfeRenderMaximumIntensityProjection(this->frameBufferProps[0].texColor, volume, colormap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIV::qfeRenderVisualPlanesOrtho(qfeVisualReformat *visual)
{
  qfeVolume          *volume;
  qfeGrid            *grid;
  qfeColorMap        *colormap;
  qfePoint            origin;

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  int                 paramInterp;
  double              paramBrightness;
  double              paramContrast;
  int                 paramTransparency;
  qfeFloat            paramRange[2];
  bool                paramShowX, paramShowY, paramShowZ;
  int                 current;
  qfeColorRGB         colorX, colorY, colorZ;
  qfeFloat            tt = 0.0f;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  // Get visual parameters
  visual->qfeGetVisualParameter("Ortho data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("Ortho data vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("Ortho data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("Ortho data interpolation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInterpList);  
  paramInterp = paramInterpList.active;

  visual->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);    

  visual->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);    

  visual->qfeGetVisualParameter("Ortho transparency", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTransparency);    

  visual->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);  

  visual->qfeGetVisualParameter("Ortho contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);   

  visual->qfeGetVisualParameter("Ortho brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);   

  // Process parameters
  visual->qfeGetVisualActiveVolume(current);
  this->qfeGetDataSet(paramData, visual, current, &volume);  

  if(volume != NULL)
  {
	volume->qfeGetVolumeGrid(&grid);
	volume->qfeGetVolumeTriggerTime(tt);
	grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);    
	
	//volume->qfeGetVolumeType(paramComp);
	volume->qfeSetVolumeTextureId(-1);
	volume->qfeUpdateVolume();
  }
  else origin.x = origin.y = origin.z = 0.0;

  switch(paramData)
  {
  case 0 : study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
		   break;
  case 1 : study->qfeGetStudyRangePCAM(paramRange[0], paramRange[1]);
		   break;
  case 2 : study->qfeGetStudyRangeSSFP(paramRange[0], paramRange[1]);
		   break;
  case 3 : if(study->tmip != NULL)
		   study->tmip->qfeGetVolumeValueDomain(paramRange[0], paramRange[1]);
		   // Non-float texture are clamped [0,1]
		   //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
		  // paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
		   break;
  case 4 : study->qfeGetStudyRangeFFE(paramRange[0], paramRange[1]);
		   // Non-float texture are clamped [0,1]
		   //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
		   //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
		   break;
  case 5:  study->qfeGetStudyRangeFTLE(paramRange[0], paramRange[1]);
		   break;
  case 6:  paramRange[0] = SNR_Range[0];
		   paramRange[1] = SNR_Range[1];
		   break;
  case 7:  paramRange[0] = Signal_Strength_Range[0];
		   paramRange[1] = Signal_Strength_Range[1];
		   break;
  case 8:  paramRange[0] = 0.0f;
		   paramRange[1] = 1.0f;
		   break;
  case(9) : // Seeding origin
			paramRange[0] = 0.0f;
			paramRange[1] = 1.0f;
			break;
  case(10) : // Seeding origins
			paramRange[0] = 0.0f;
			paramRange[1] =  this->algorithmIP->qfeGetSeedVolumeCount();
			colormap = this->algorithmIP->qfeGetSeedVolumeColorMap();
			break;
  case(11) : // Leaving mesh
			paramRange[0] = 0.0f;
			paramRange[1] = 1.0f;
			break;
  case(12) : // segmentation levelset
			paramRange[0] = meshLevelSetRange[0];
			paramRange[1] = meshLevelSetRange[1];
			break;
  default: study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
		   break;  
  }  
  
  // Determine line color
  colorX = colorY = colorZ = this->colorReformatDefault;
  if(this->selectedPlane == qfePlaneX)  colorX = this->colorReformatHighlight;
  if(this->selectedPlane == qfePlaneY)  colorY = this->colorReformatHighlight;
  if(this->selectedPlane == qfePlaneZ)  colorZ = this->colorReformatHighlight;

  // Update algorithm parameters    
  this->algorithmReformat->qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
  this->algorithmReformat->qfeSetPlaneDataComponent(paramComp);
  this->algorithmReformat->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmReformat->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  this->algorithmReformat->qfeSetPlaneTriggerTime(tt); 
  this->algorithmReformat->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  this->algorithmReformat->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmReformat->qfeSetPlaneContrast(paramContrast);
  this->algorithmReformat->qfeSetPlaneOpacity(1.0-(paramTransparency/100.0));
 
  // Render the planes
  if(paramShowX)
  {
	this->algorithmReformat->qfeSetPlaneBorderColor(colorX);
	this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeX, colormap);      
  }

  if(paramShowY) 
  { 
	this->algorithmReformat->qfeSetPlaneBorderColor(colorY);
	this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeY, colormap);      
  }

  if(paramShowZ)
  {  
	this->algorithmReformat->qfeSetPlaneBorderColor(colorZ);
	this->algorithmReformat->qfeRenderPolygon3D(volume, this->planeZ, colormap);   
  }  

  return qfeSuccess;
}

qfeReturnStatus qfeDriverIV::qfeRenderPlaneOrthoView(qfeVisualPlaneOrthoView *visual, qfeVisualReformat *visMPR)
{
  qfeStudy *study;
  qfeColorMap *colormap;
  int current;
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  int                 paramInterp;
  double              paramBrightness;
  double              paramContrast;
  int                 paramTransparency;

  // Get visual parameters from multi-planar reformat
  visMPR->qfeGetVisualParameter("Ortho data set", &param);
  if (param) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;
  visMPR->qfeGetVisualParameter("Ortho data vector", &param);
  if (param) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;
  visMPR->qfeGetVisualParameter("Ortho data representation", &param);
  if (param) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;
  visMPR->qfeGetVisualParameter("Ortho data interpolation", &param);
  if (param) param->qfeGetVisualParameterValue(paramInterpList);  
  paramInterp = paramInterpList.active;
  visMPR->qfeGetVisualParameter("Ortho transparency", &param);
  if (param) param->qfeGetVisualParameterValue(paramTransparency);    
  visMPR->qfeGetVisualParameter("Ortho contrast", &param);
  if (param) param->qfeGetVisualParameterValue(paramContrast);   
  visMPR->qfeGetVisualParameter("Ortho brightness", &param);
  if (param) param->qfeGetVisualParameterValue(paramBrightness);   

  qfeVolume *volume;
  // Process parameters
  visMPR->qfeGetVisualActiveVolume(current);
  this->qfeGetDataSet(paramData, visMPR, current, &volume);  

  bool paramShow, paramLock;
  visual->qfeGetVisualParameter("Show view orthogonal plane", &param);
  if (param) param->qfeGetVisualParameterValue(paramShow);
  visual->qfeGetVisualParameter("Lock plane", &param);
  if (param) param->qfeGetVisualParameterValue(paramLock);

  qfeColorRGB colorQ = colorReformatDefault;
  if (selectedPlane == qfePlaneQ)
    colorQ = colorReformatHighlight;

  qfeGrid *grid;
  float tt;
  qfePoint origin;
  if (volume) {
    volume->qfeGetVolumeGrid(&grid);
    volume->qfeGetVolumeTriggerTime(tt);   
  }

  float paramRange[2];
  switch(paramData)
  {
  case 0 : study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
		   break;
  case 1 : study->qfeGetStudyRangePCAM(paramRange[0], paramRange[1]);
		   break;
  case 2 : study->qfeGetStudyRangeSSFP(paramRange[0], paramRange[1]);
		   break;
  case 3 : if(study->tmip != NULL)
		   study->tmip->qfeGetVolumeValueDomain(paramRange[0], paramRange[1]);
		   // Non-float texture are clamped [0,1]
		   //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
		  // paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
		   break;
  case 4 : study->qfeGetStudyRangeFFE(paramRange[0], paramRange[1]);
		   // Non-float texture are clamped [0,1]
		   //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
		   //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
		   break;
  case 5:  study->qfeGetStudyRangeFTLE(paramRange[0], paramRange[1]);
		   break;
  case 6:  paramRange[0] = SNR_Range[0];
		   paramRange[1] = SNR_Range[1];
		   break;
  case 7:  paramRange[0] = Signal_Strength_Range[0];
		   paramRange[1] = Signal_Strength_Range[1];
		   break;
  case 8:  paramRange[0] = 0.0f;
		   paramRange[1] = 1.0f;
		   break;
  case(9) : // Seeding origin
			paramRange[0] = 0.0f;
			paramRange[1] = 1.0f;
			break;
  case(10) : // Seeding origins
			paramRange[0] = 0.0f;
			paramRange[1] =  this->algorithmIP->qfeGetSeedVolumeCount();
			colormap = this->algorithmIP->qfeGetSeedVolumeColorMap();
			break;
  case(11) : // Leaving mesh
			paramRange[0] = 0.0f;
			paramRange[1] = 1.0f;
			break;
  case(12) : // segmentation levelset
			paramRange[0] = meshLevelSetRange[0];
			paramRange[1] = meshLevelSetRange[1];
			break;
  default: study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
		   break;  
  }  

  this->algorithmReformat->qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
  this->algorithmReformat->qfeSetPlaneDataComponent(paramComp);
  this->algorithmReformat->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmReformat->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  this->algorithmReformat->qfeSetPlaneTriggerTime(tt); 
  this->algorithmReformat->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  this->algorithmReformat->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmReformat->qfeSetPlaneContrast(paramContrast);
  this->algorithmReformat->qfeSetPlaneOpacity(1.0-(paramTransparency/100.0));

  if (paramShow) {
    if (!paramLock)
      qfeGetPlaneViewOrthogonal(volume, &planeQ);
    this->algorithmReformat->qfeSetPlaneBorderColor(colorQ);
    this->algorithmReformat->qfeRenderPolygon3D(volume, planeQ, colormap);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIV::qfeRenderVisualAides(qfeVisualAides *visual)
{
  qfeStudy           *study;
  qfeVolume          *volume;
  int                 current;

  qfeVisualParameter *param;
  bool                paramSupportBoundingBox = true;
  bool                paramSupportAxes        = false;
	
  this->visAides->qfeGetVisualStudy(&study);  
  this->visAides->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;
  
  if(study->pcap.size() > 0) 
	volume = study->pcap[current];
  else return qfeError;

  this->visAides->qfeGetVisualParameter("Support bounding box", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportBoundingBox); 

  this->visAides->qfeGetVisualParameter("Support axes", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportAxes); 

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  if(paramSupportBoundingBox == true) qfeDriverAide::qfeRenderBoundingBoxDashed(volume, this->currentModelView, 0.8*this->frameBufferProps[0].supersampling);
  if(paramSupportAxes        == true) qfeDriverAide::qfeRenderVolumeAxes(volume, 10.0, 1.6*this->frameBufferProps[0].supersampling);

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIV::qfeGetDataSet(int index, qfeVisual *visual, int time, qfeVolume **volume)
{
  *volume = NULL;

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(0) : // PCA-P
	if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time)) 
	  *volume = study->pcap[time];
	break;
  case(1) : // PCA-M
	if(((int)study->pcam.size() > 0) && ((int)study->pcam.size() > time)) 
	  *volume = study->pcam[time];
	break;
  case(2) : // SSFP
	if((int)study->ssfp3D.size() > 0) 
	{
	  // If there is also pcap data loaded, check for the trigger time and load
	  // accompanying ssfp data at the right time slot 
	  if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time)) 
	  {
		qfeFloat att, ptt;
		qfeFloat distance = 9000;
		(int)study->pcap[time]->qfeGetVolumeTriggerTime(ptt);
		for(int i=0; i<(int)study->ssfp3D.size(); i++)
		{
		  (int)study->ssfp3D[i]->qfeGetVolumeTriggerTime(att);
		  if(abs(att-ptt) < distance)
		  {
			distance = abs(att-ptt);
			*volume = study->ssfp3D[i];
		  }
		}
	  }
	  else
		*volume = study->ssfp3D[0];
	}
	break;
	case(3) : // T-MIP
	  if(study->tmip != NULL) 
	  *volume = study->tmip;
	  break;
	case(4) : // FFE
	if(((int)study->ffe.size() > 0) && ((int)study->ffe.size() > time)) 
	  *volume = study->ffe[time];
	break;
	case(5) : // FTLE
	  if(((int)study->ftle.size() > 0) && ((int)study->ftle.size() > time))
	  {
		*volume = study->ftle[time];
	  }
	case(6) : // SNR
		if(((int)SNR_Volumes.size() > 0) && ((int)SNR_Volumes.size() > time))
		{
			*volume = &SNR_Volumes[time];
		}
		break;
	case(7) : // Signal strength
		if(((int)Signal_Strength_Volumes.size() > 0) && ((int)Signal_Strength_Volumes.size() > time))
		{
			*volume = &Signal_Strength_Volumes[time];
		}
		break;
	case(8) : // Particle density
		if(((int)Signal_Strength_Volumes.size() > 0) && ((int)Signal_Strength_Volumes.size() > time))
		{
			*volume = &particleDensityVolume;
		}
		break;
	case(9) : // Seeding origin
		{
			unsigned int selected_sink = 0;
			if(this->pointSelected.index > 0) //a particle sink is selected:
			{
				selected_sink = this->pointSelected.index;
			}

			*volume = new qfeVolume();
			algorithmIP->qfeGetSeedingOriginVolume(**volume, 3, selected_sink);
		}
		break;
	case(10) : // Seeding origins
		{
			unsigned int selected_sink = 0;
			if(this->pointSelected.index > 0) //a particle sink is selected:
			{
				selected_sink = this->pointSelected.index;
			}

			*volume = new qfeVolume();
			algorithmIP->qfeGetAllSeedingOriginVolume(**volume, 3);
		}
		break;
	case(11) : // Leaving mesh
		{
			*volume = new qfeVolume();
			algorithmIP->qfeGetParticlesLeavingMeshVolume(**volume, 3);
		}
		break;
	case(12) : // Solid levelset
		if(study->segmentation.size()>0)
		{			
			*volume = &meshLevelsetVolume;
		}
		break;
	default : // PCA-P
		if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time)) 
			*volume = study->pcap[time];
		break;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIV::qfeGetPlaneOrtho(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice)
{
  qfeMatrix4f P2V, V2P;
  qfeVector   axisX, axisY, axisZ, axisXabs, axisYabs, axisZabs, x, y, z;
  qfePoint    o;
  qfeGrid    *grid;

  if(volume == NULL) return qfeError;

  // Get the patient to voxel transformation
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);

  // Set the plane normal direction in patient coordinates
  switch(dir)
  {
  case qfePlaneAxial       : axisX.qfeSetVectorElements(1.0,0.0,0.0);
							 axisY.qfeSetVectorElements(0.0,1.0,0.0);
							 axisZ.qfeSetVectorElements(0.0,0.0,1.0);                              
							 break;
  case qfePlaneSagittal    : axisX.qfeSetVectorElements(0.0,0.0,1.0);
							 axisY.qfeSetVectorElements(0.0,1.0,0.0);
							 axisZ.qfeSetVectorElements(1.0,0.0,0.0); 
							 break;
  case qfePlaneCoronal     : axisX.qfeSetVectorElements(1.0,0.0,0.0);
							 axisY.qfeSetVectorElements(0.0,0.0,1.0);
							 axisZ.qfeSetVectorElements(0.0,1.0,0.0); 
							 break;
  }  

  // Convert the axis to voxel coordinates and find dominant direction
  axisX = axisX*P2V;
  axisY = axisY*P2V;
  axisZ = axisZ*P2V;

  axisXabs.qfeSetVectorElements(abs(axisX.x), abs(axisX.y), abs(axisX.z));
  axisYabs.qfeSetVectorElements(abs(axisY.x), abs(axisY.y), abs(axisY.z));
  axisZabs.qfeSetVectorElements(abs(axisZ.x), abs(axisZ.y), abs(axisZ.z));

  if(axisZabs.x == max(max(axisZabs.x, axisYabs.x), axisXabs.x))
  {
	x.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
	y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
	z.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
  }
  if(axisZabs.y == max(max(axisZabs.y, axisYabs.y), axisXabs.y))
  {
	x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
	y.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
	z.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
  }
  if(axisZabs.z == max(max(axisZabs.z, axisYabs.z), axisXabs.z))
  {
	x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
	y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
	z.qfeSetVectorElements( 0.0f,  0.0f,  1.0f);    
  }

  // Convert back to patient coordinates
  x = x*V2P;
  y = y*V2P;
  z = z*V2P;

  // Get the plane origin
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridOrigin(o.x, o.y, o.z);

  // Build the plane
  (*slice)->qfeSetFrameExtent(1,1);
  (*slice)->qfeSetFrameOrigin(o.x, o.y, o.z);
  (*slice)->qfeSetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);

  return qfeSuccess;
}

qfeReturnStatus qfeDriverIV::qfeGetPlaneViewOrthogonal(qfeVolume *volume, qfeFrameSlice **slice)
{
  qfeGrid * grid;
  double upVec[3];
  engine->GetRenderer()->GetActiveCamera()->GetViewUp(upVec);
  qfeVector up(upVec[0], upVec[1], upVec[2]);

  if (volume == NULL) return qfeError;

  unsigned int s[3];
  qfePoint  o, e;
  qfeVector x, y, z;    

  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(s[0], s[1], s[2]);
  grid->qfeGetGridAxes(x.x,x.y,x.z, y.x,y.y,y.z, z.x,z.y,z.z);
  grid->qfeGetGridExtent(e.x, e.y, e.z);

  qfeVector lookDir = currentModelView * qfeVector(0.f, 0.f, -1.f);
  qfeVector::qfeVectorNormalize(lookDir);
  qfeVector::qfeVectorNormalize(up);
  z = -lookDir;
  qfeVector::qfeVectorCross(up, z, x);
  qfeVector::qfeVectorNormalize(x);
  qfeVector::qfeVectorCross(z, x, y);
  qfeVector::qfeVectorNormalize(y);

  (*slice)->qfeSetFrameAxes(x.x,x.y,x.z, y.x,y.y,y.z, z.x,z.y,z.z);
  (*slice)->qfeSetFrameExtent(s[0]*e.x, s[1]*e.y);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeOnAction(void *caller, int t)
{
	qfeDriverIV *self = reinterpret_cast<qfeDriverIV *>(caller);

	qfeVisualParameter *p;
	bool storeScreenshots = false;
	
	if(self->visualInkVisP == NULL) return;
	// Obtain the parameter value
	self->visualInkVisP->qfeGetVisualParameter("Store Screenshots", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(storeScreenshots);

	switch(t)
	{
		case qfeVisualInkVis::actionLoadInkSeed  :  
		{
			self->qfeLoadSeedPositions();
			self->algorithmIP->qfeClearParticles();

			if(!self->seedPosition3_enabled)
			{
				InkLine line;
				line[0] = self->seedPosition1;
				line[1] = self->seedPosition2;

				self->algorithmIP->qfeSetSeedLine(line);
			}
			else
			{
				InkVolume volume;
				volume[0] = self->seedPosition1;
				volume[1] = self->seedPosition2;
				volume[2] = self->seedPosition3;

				self->algorithmIP->qfeSetSeedVolume(volume);
			}

			self->engine->Refresh();
			break;
		}
		case qfeVisualInkVis::actionSaveInkSeed  :  
		{
			self->qfeSaveSeedPositions();
			self->engine->Refresh();
			break;
		}
		case qfeVisualInkVis::actionCenterInkCamera  :  
		{
			static int click_count = 0;

			qfePoint position;
			qfePoint focalPoint;
			qfeVector viewUp;
			self->qfeGetCameraSettings(position, focalPoint, viewUp);

			qfeVector a1; qfeVector a2; qfeVector a3; qfePoint c;
			self->algorithmIP->qfeGetSeedVolumeDetails(a1, a2, a3, c);

			click_count++;
			if(click_count>2)
				click_count = 0;
			
			if(click_count == 0) //use original settings
				self->qfeSetCameraSettings(position, self->algorithmIP->volumeCenter, viewUp);
				// to reset everything: self->qfeSetCameraSettings(self->initial_camera_position, self->initial_camera_focalPoint, self->initial_camera_viewUp);
			if(click_count == 1) //pan to  seeding positions
				self->qfeSetCameraSettings(position, c, a2);
			if(click_count == 2) //pan to  seeding positions (flipped normal)
				self->qfeSetCameraSettings(position, c, -a2);

			self->engine->Refresh();

			break;
		}
		case qfeVisualInkVis::actionAnimateInk  :  
		{
			if(self->visualInkVisP != NULL)
				self->visualInkVisP->qfeGetVisualActiveVolume(self->currentTime);
			self->algorithmIP->qfeSetPhase(self->currentTime);
			
			qfeVisualParameter *param;
			int number_of_lines = 0;
			self->visualInkVisP->qfeGetVisualParameter("Number of lines", &param);
			if(param != NULL) param->qfeGetVisualParameterValue(number_of_lines);	  

			bool seedFromVoxelCenter = false;
			self->visualInkVisP->qfeGetVisualParameter("Seed from voxel center", &param);
			if(param != NULL) param->qfeGetVisualParameterValue(seedFromVoxelCenter);	

			bool seedFromFixedPosition = false;
			self->visualInkVisP->qfeGetVisualParameter("Seed from fixed positions", &param);
			if(param != NULL) param->qfeGetVisualParameterValue(seedFromFixedPosition);	

			double seedFromFixedPositionMultiplier = 1.0f;
			self->visualInkVisP->qfeGetVisualParameter("Fixed seed positions multiplier", &param);
			if(param != NULL) param->qfeGetVisualParameterValue(seedFromFixedPositionMultiplier);				
									
			bool seedPlanar = false;
			self->visualInkVisP->qfeGetVisualParameter("Planar seeding", &param);
			if(param != NULL) param->qfeGetVisualParameterValue(seedPlanar);

			bool seedWholeMesh = false;
			self->visualInkVisP->qfeGetVisualParameter("Seed throughout mesh", &param);
			if(param != NULL) param->qfeGetVisualParameterValue(seedWholeMesh);

			if(!self->seedPosition3_enabled)
			{
				InkLine line;
				line[0] = self->seedPosition1;
				line[1] = self->seedPosition2;

				self->algorithmIP->qfeSetSeedLine(line);
			}
			else
			{
				InkVolume volume;
				volume[0] = self->seedPosition1;
				volume[1] = self->seedPosition2;
				volume[2] = self->seedPosition3;

				self->algorithmIP->qfeSetSeedVolume(volume);
			}

			double time = 0.0f;
			double step_size = self->algorithmIP->qfeGetTimeStepSize();

			if(storeScreenshots)
				self->engine->StoreScreenshot(1920, 1080);
			
			std::vector<std::vector<statistic>> statistics_table;
			while(time < self->animation_duration)
			{				
				if(time + step_size >  self->animation_duration)
				{
					self->algorithmIP->qfeSetTimeStepSize(self->animation_duration-time);
				}

				self->algorithmIP->qfeComputeNextTimeStep(self->seedPosition3_enabled, seedFromVoxelCenter, seedFromFixedPosition, (float)seedFromFixedPositionMultiplier, seedPlanar, seedWholeMesh);
				self->engine->Refresh();
				time += step_size;
				
				if(storeScreenshots)
					self->engine->StoreScreenshot(1920, 1080);

				std::vector<statistic> stats = self->algorithmIP->qfeGetStatistics();
				if(stats.size()>0)
					statistics_table.push_back(stats);
				
				//std::cout<<"Progress: "<<(int)(t/self->animation_duration*100)<<"% using  "<<self->algorithmIP->qfeGetNumberOfInkParticles()<<" particles"<<std::endl;
			}
			
			self->algorithmIP->qfeSetTimeStepSize(step_size);//reset time step sizes

			self->algorithmIP->qfeGetParticleDensityVolume(self->particleDensityVolume, 3, self->seedPosition3_enabled);
			self->engine->Refresh();

			//print statistics table:
			if(statistics_table.size() > 0)
			{
				std::fstream filestrStatistics;
				filestrStatistics.open ("statistics.txt", fstream::in | ios::trunc);
				filestrStatistics.clear();  
				filestrStatistics.close();
				filestrStatistics.open ("statistics.txt", std::fstream::app);  

				//print labels
				for(unsigned int i = 0; i<statistics_table[0].size(); i++)
				{
					filestrStatistics<<statistics_table[0].at(i).label<<";";
					std::cout<<statistics_table[0].at(i).label<<";";
				}
				filestrStatistics<<std::endl;
				std::cout<<endl;

				//print values
				for(unsigned int i = 0; i<statistics_table.size(); i++)
				{
					for(unsigned int j = 0; j<statistics_table[i].size(); j++)
					{
						filestrStatistics<<self->valueToPrettyString(statistics_table[i].at(j).value)<<";";
						std::cout<<self->valueToPrettyString(statistics_table[i].at(j).value)<<";";
					}
					filestrStatistics<<std::endl;
					std::cout<<endl;
				}
				
				filestrStatistics.close();	
			}		
		}
			break;  
		case qfeVisualInkVis::actionRemoveInk  :  
		{
			self->algorithmIP->qfeClearParticles();
			self->algorithmIP->qfeGetParticleDensityVolume(self->particleDensityVolume, 3, self->seedPosition3_enabled);
			self->engine->Refresh();
			break;
		}
		case qfeVisualInkVis::actionShowTimeLine :
		{
			self->algorithmIP->showTimeLinePlot();
			break;
		}
		case qfeVisualInkVis::actionPlaceFeatureSeedPointsInk :
		{			
			self->qfeFeatureBasedSeeding();
			break;
		}
		case qfeVisualSeedVolumes::actionAddSeedVolume  :  
		{
			self->newParticleSink = !self->newParticleSink;
			self->newParticleSinkPoints.clear();
			break;
		}
		case qfeVisualSeedVolumes::actionRemoveSeedVolume  :  
		{
			if(self->pointSelected.index != 0)
			{
				self->algorithmIP->particleSinks->qfeRemoveSeedVolume(self->pointSelected.index);

				self->particleSinkGuidePointSelected = false;

				self->pointSelected.index = 0;

				self->engine->Refresh();
			}
			break;
		}
		case qfeVisualSeedVolumes::actionSaveSeedVolumes :
		{
			self->algorithmIP->particleSinks->qfeSaveSeedVolumes();

			break;
		}
		case qfeVisualSeedVolumes::actionLoadSeedVolumes :
		{
			self->algorithmIP->particleSinks->qfeLoadSeedVolumes();

			self->engine->Refresh();

			break;
		}
	}
}

float qfeDriverIV::qfeGetProbability(float value, float rangeMin, float rangeMax, QVector<QPair<double, double>> &tf)
{
  float valueNorm = (value - rangeMin) / (rangeMax - rangeMin);
  float before = tf.first().first;
  float pBefore = tf.first().second;
  float after;
  float pAfter;
  for (unsigned i = 1; i != tf.size(); i++) {
	if (tf[i].first >= valueNorm) {
	  after = tf[i].first;
	  pAfter = tf[i].second;
	  break;
	}
	before = tf[i].first;
	pBefore = tf[i].second;
  }

  float t = (valueNorm - before) / (after - before);
  return pBefore + t * (pAfter - pBefore);
}

void qfeDriverIV::qfeFeatureBasedSeeding()
{
	if(visualInkVisP == NULL)
		return;

	unsigned int selected_sink = 0;
	if(this->pointSelected.index > 0) //a particle sink is selected:
	{
		selected_sink = this->pointSelected.index;
	}

	std::cout<<"Selected Sink: "<<selected_sink<<std::endl;
	qfeVolume volume;
	algorithmIP->qfeGetSeedingOriginVolume(volume, 3, selected_sink);

	float color_id = this->algorithmIP->particleSinks->qfeGetColorIndex(selected_sink);

	std::cout<<"Color id: "<<color_id<<std::endl;

	unsigned int nx, ny, nz;
	volume.qfeGetVolumeSize(nx, ny, nz);
	
	std::vector<featureSeedPoint> seeds;
	featureSeedPoint seed;

	std::random_device rd;
	std::mt19937 generator(rd());
	std::uniform_real_distribution<> distribution(0, 1);

	QTransferFunctionOpacity tf = scene->qfeGetSceneFeatureTF(0);

	for(unsigned int k = 0; k<nz; k++)
	{
		for(unsigned int j = 0; j<ny; j++)
		{
			for(unsigned int i = 0; i<nx; i++)
			{
				qfeVector vector;
				volume.qfeGetVolumeVectorAtPosition(i,j,k, vector);
				float sample = vector.x;

				float probability = distribution(generator);

				float value = this->qfeGetProbability(sample, 0.0f, 1.0f, tf);

				//to avoid adding seeds everywhere, we exclude where the probability was 0 to begin with
				//for the remaining voxels we use the tf
				if(sample>0.0f && value>probability)
				{
					seed.seedPoint = qfePoint((float)i,(float)j,(float)k);
					seed.color_id = color_id;
				
					seeds.push_back(seed);
				}
			}
		}
	}

	this->algorithmIP->qfeSetFeatureBasedSeeds(seeds);

	this->algorithmIP->qfeClearParticlesFeatureBased();

	double stepSize = 0.0;
	double lifeTime = 0.0;
	double animationLength = 0.0;
	double stopSeeding = 0.0;

	qfeVisualParameter *param;

	this->visualInkVisP->qfeGetVisualParameter("Particle lifetime (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(lifeTime);  
	this->algorithmIP->qfeSetParticleLifeTime(lifeTime);

	this->visualInkVisP->qfeGetVisualParameter("Animation step size (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(stepSize);
	this->algorithmIP->qfeSetTimeStepSize(stepSize);
	
	this->visualInkVisP->qfeGetVisualParameter("Animation length (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(animationLength);  
	animation_duration = animationLength;	  

	this->visualInkVisP->qfeGetVisualParameter("Stop seeding (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(stopSeeding);  
	this->algorithmIP->qfeSetStopSeedingTime(stopSeeding);

	double duration = animationLength;
	if(stopSeeding >= 0.0)
		duration = stopSeeding + lifeTime;

	this->algorithmIP->qfeSetParticleLifeTime(duration);
	this->algorithmIP->qfeSetStopSeedingTime(stopSeeding);
	this->algorithmIP->qfeSetTimeStepSize(stepSize);
	
	bool storeScreenshots = false;
	this->visualInkVisP->qfeGetVisualParameter("Store Screenshots", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(storeScreenshots);

	if(storeScreenshots)
		this->engine->StoreScreenshot();
	
	double time = 0.0;

	while(time < duration)
	{				
		if(time + stepSize >  duration)
		{
			this->algorithmIP->qfeSetTimeStepSize(duration-time);
		}
		
		this->algorithmIP->qfeComputeNextTimeStep(false, false, false, 1.0f, false, false);
		this->engine->Refresh();
		time += stepSize;
				
		if(storeScreenshots)
			this->engine->StoreScreenshot();
	}
			
	this->algorithmIP->qfeSetTimeStepSize(stepSize);//reset time step sizes

	std::cout<<seeds.size()<<" Seeding positions"<<std::endl;
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeOnResize(void *caller)
{
  qfeDriverIV *self = reinterpret_cast<qfeDriverIV *>(caller);

  qfeViewport *vp;

  self->engine->GetViewport3D(&vp);

  qfeDriver::qfeDriverResize(caller);

  self->algorithmMIP->qfeSetViewportSize(vp->width, vp->height, self->frameBufferProps[0].supersampling);
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeOnMouseMove(void *caller, int x, int y, int v)
{
	qfeDriverIV *self = reinterpret_cast<qfeDriverIV *>(caller);
	
	if(self->filteringGraphRendered && self->brush_active)
	{
		if(x < 0)
			x = 0;
		if(y < 0)
			y = 0;

		if(x > self->screen_size_y/2)
			x = self->screen_size_y/2;
		if(y > self->screen_size_y/2)
			y = self->screen_size_y/2;

		self->brush_position2 = Vec2f((float)x/(self->screen_size_y/2.0f),(float)y/(self->screen_size_y/2.0f));

		self->engine->Refresh();

		return;
	}
	
	if(self->engine->GetInteractionControlKey() && self->rightMouseDown && (self->selected_seeding_point != seedingPointNone || self->particleSinkGuidePointSelected)) 
	{//a point was already selected so we have to update the point selector
		qfePoint p(0.0f,0.0f,0.0f);
		bool found = self->qfeGetPointOnSlices(x, y, p);
		
		//get nearest seeding point:
		if(found)
		{
			if(self->selected_seeding_point != seedingPointNone)
			{
				self->newSeedPosition = p * self->P2V;
			}
			if(self->particleSinkGuidePointSelected)
			{
				self->algorithmIP->particleSinks->qfeUpdateSeedVolumeGuidePoint(self->pointSelected.index, p, self->pointSelected.type);
				self->pointSelected.point = p;
			}
		}
		else
			self->selected_seeding_point = seedingPointNone;
		
		self->engine->Refresh();
	}
	if(!self->engine->GetInteractionControlKey())
	{//unselect the point
		self->selected_seeding_point = seedingPointNone;
		//self->particleSinkGuidePointSelected = false;
	}
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v)
{
  qfeDriverIV *self = reinterpret_cast<qfeDriverIV *>(caller);

  qfeColorRGB color;
  float       depth;

  self->qfeReadSelect(x, y, color);
  self->qfeReadDepth(x, y, depth);

  self->selectedPlane = qfePlaneNone;

  if(!self->engine->GetInteractionControlKey())
  {
	  if(color.r == 1.0 && color.g == 0.0 && color.b==0.0)
		self->selectedPlane = qfePlaneX;

	  if(color.r == 0.0 && color.g == 1.0 && color.b==0.0)
		self->selectedPlane = qfePlaneY;

	  if(color.r == 0.0 && color.g == 0.0 && color.b==1.0)
		self->selectedPlane = qfePlaneZ;

	  if (color.r == 0.5f && color.g == 0.5f && color.b == 0.5f)
		  self->selectedPlane = qfePlaneQ;

	  //see if a particle sink point was clicked  
	  std::vector<seedVolumeGuidePoint> points;
	  
	  vtkCamera *vtkcamera = self->engine->GetRenderer()->GetActiveCamera();
	  std::array<double,3> viewDir;
	  vtkcamera->GetDirectionOfProjection(viewDir[0], viewDir[1], viewDir[2]);
	  
	  qfePoint p(0.0f,0.0f,0.0f);
	  bool found = self->qfeGetPointOnSlices(x, y, p);
	  
	  // Convert mouse click to ray
	  qfePoint rayStart, rayEnd;
	  self->qfeClickToRay(x, y, rayStart, rayEnd);
	  
	  if(!found)
	  {
		  //point was not found, though we might have select a point:
		  
		  p = rayEnd;
		  
		  found = true;
	  }
	  
	  //to make sure that we cannot select points that are not in behind a visible slice
	  points.push_back(self->algorithmIP->particleSinks->seedVolumeGuidePointConstructor(
		  //move the found point to allow selection of points that are on the slice:
		  qfePoint(p.x+viewDir[0]*0.025, p.y+viewDir[1]*0.025, p.z+viewDir[2]*0.025), 
		  0, other_point_type));
	  
	  self->algorithmIP->particleSinks->qfeGetAllGuidePoints(points);
	  
	  int index = self->qfePointsOnLine(rayStart, rayEnd, points);
	  if(index > 0)//a point was clicked (note index 0 indicates a click on a plane not a point)
	  {
			self->selected_seeding_point = seedingPointNone;
			
			self->pointSelected = points[index];
	  }
  }

  self->selected_seeding_point = seedingPointNone;
  self->particleSinkGuidePointSelected = false;
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeOnMouseRightButtonUp(void *caller, int x, int y, int v)
{
	qfeDriverIV *self = reinterpret_cast<qfeDriverIV *>(caller);

	self->rightMouseDown = false;

	self->brush_active = false;
	
	if(self->engine->GetInteractionControlKey() && (self->selected_seeding_point != seedingPointNone || self->particleSinkGuidePointSelected))
	{
		qfePoint p(0.0f,0.0f,0.0f);
		
		bool found = self->qfeGetPointOnSlices(x, y, p);
		
		if(found) //update the point position
		{			
			if(self->selected_seeding_point == seedingPointOne)
				self->seedPosition1 = p * self->P2V;
			else if(self->selected_seeding_point == seedingPointTwo)
				self->seedPosition2 = p * self->P2V;				
			else if(self->seedPosition3_enabled && self->selected_seeding_point == seedingPointThree)
				self->seedPosition3 = p * self->P2V;	
			else if(self->particleSinkGuidePointSelected)
			{
				self->algorithmIP->particleSinks->qfeUpdateSeedVolumeGuidePoint(self->pointSelected.index, p, self->pointSelected.type);
			}
		}
		else
		{
			self->selected_seeding_point = seedingPointNone;
			self->particleSinkGuidePointSelected = false;
		}
	}
	self->selected_seeding_point = seedingPointNone;

	self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeOnMouseRightButtonDown(void *caller, int x, int y, int v)
{
	qfeDriverIV *self = reinterpret_cast<qfeDriverIV *>(caller);

	self->rightMouseDown = true;
	
	if(self->filteringGraphRendered && x < self->screen_size_y/2 && y < self->screen_size_y/2)
	{
		self->brush_active = true;

		self->brush_position1 = Vec2f((float)x/(self->screen_size_y/2.0f),(float)y/(self->screen_size_y/2.0f));

		return;
	}

	self->pointSelected.index = 0;

	if(self->engine->GetInteractionControlKey() && (self->selected_seeding_point == seedingPointNone))
	{ //no point is selected, so we might have to select one
		qfePoint p(0.0f,0.0f,0.0f);
		bool found = self->qfeGetPointOnSlices(x, y, p);
		
		// Convert mouse click to ray
		qfePoint rayStart, rayEnd;
		self->qfeClickToRay(x, y, rayStart, rayEnd);

		if(!found)
		{
			//point was not found, though we might have select a point:

			p = rayEnd;
			
			found = true;
		}
		
		//get nearby seeding point:
		if(found)
		{
			self->selected_seeding_point = seedingPointNone;
			self->particleSinkGuidePointSelected = false;

			float dist1, dist2, dist3 = 0.0f;

				
			std::vector<seedVolumeGuidePoint> points;

			vtkCamera *vtkcamera = self->engine->GetRenderer()->GetActiveCamera();
			std::array<double,3> viewDir;
			vtkcamera->GetDirectionOfProjection(viewDir[0], viewDir[1], viewDir[2]);
			
			//to make sure that we cannot select points that are not in behind a visible slice
			points.push_back(self->algorithmIP->particleSinks->seedVolumeGuidePointConstructor(
				//move the found point to allow selection of points that are on the slice:
				qfePoint(p.x+viewDir[0]*0.025, p.y+viewDir[1]*0.025, p.z+viewDir[2]*0.025), 
				0, other_point_type));

			points.push_back(self->algorithmIP->particleSinks->seedVolumeGuidePointConstructor(self->seedPosition1*self->V2P, 1, diameter1));
			points.push_back(self->algorithmIP->particleSinks->seedVolumeGuidePointConstructor(self->seedPosition2*self->V2P, 1, diameter2));
			
			if(self->seedPosition3_enabled)
			{
				points.push_back(self->algorithmIP->particleSinks->seedVolumeGuidePointConstructor(self->seedPosition3*self->V2P, 1, apex));
			}

			self->algorithmIP->particleSinks->qfeGetAllGuidePoints(points);

			int index = self->qfePointsOnLine(rayStart, rayEnd, points);
			if(index > 0)//a point was clicked (note index 0 indicates a click on a plane not a point)
			{
				if(index == 1)
					self->selected_seeding_point = seedingPointOne;
				else if(index == 2)
					self->selected_seeding_point = seedingPointTwo;
				else if(self->seedPosition3_enabled && index == 3)
					self->selected_seeding_point = seedingPointThree;
				else //particle sink point selected
				{
					self->selected_seeding_point = seedingPointNone;

					self->particleSinkGuidePointSelected = true;
					self->pointSelected = points[index];
				}
			}
			else
			{
				if(self->newParticleSink)
				{
					self->newParticleSinkPoints.push_back(p);

					if(self->newParticleSinkPoints.size() == 3) //we have enough points for a particle sink
					{
						self->algorithmIP->particleSinks->qfeAddSeedVolume(
							self->newParticleSinkPoints.at(0),
							self->newParticleSinkPoints.at(1),
							self->newParticleSinkPoints.at(2), CylinderType);
						
						self->newParticleSinkPoints.clear();
						self->newParticleSink = false;
					}
				}
			}
		}
		else //no point on the planes was found
		{
			self->selected_seeding_point = seedingPointNone;
			self->particleSinkGuidePointSelected = false;
		}
	}
	else if(self->engine->GetInteractionControlKey() && (self->selected_seeding_point != seedingPointNone)) 
	{//a point was already selected so we have to update the point selector
		qfePoint p(0.0f,0.0f,0.0f);
		bool found = self->qfeGetPointOnSlices(x, y, p);
		
		//get nearest seeding point:
		if(found)
		{
			self->newSeedPosition = p * self->P2V;
		}
		else
		{
			self->selected_seeding_point = seedingPointNone;
			self->particleSinkGuidePointSelected = false;
		}
	}
	else
	{//control is not pressed:
		self->selected_seeding_point = seedingPointNone;
		self->particleSinkGuidePointSelected = false;

		self->engine->CallInteractionParentRightButtonDown();
	}
	
	self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeOnWheelForward(void *caller, int x, int y, int v)
{
  qfeDriverIV *self = reinterpret_cast<qfeDriverIV *>(caller);

  if(self->particleSinkGuidePointSelected)
  {
	  self->algorithmIP->particleSinks->qfeSquishSeedVolume(self->pointSelected.index);

	  self->engine->Refresh();

	  return;
  }  
  
  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if(self->visualReformat == NULL) return;

  // Obtain the volume
  self->visualReformat->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visualReformat->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);

  if((int)study->pcap.size() <= 0) return;
  
  if(self->selectedPlane == qfePlaneX) self->qfeMovePlaneForward(study->pcap.front(), &self->planeX, scrollStep);
  if(self->selectedPlane == qfePlaneY) self->qfeMovePlaneForward(study->pcap.front(), &self->planeY, scrollStep);
  if(self->selectedPlane == qfePlaneZ) self->qfeMovePlaneForward(study->pcap.front(), &self->planeZ, scrollStep);
  if(self->selectedPlane == qfePlaneQ) self->qfeMovePlaneForward(study->pcap.front(), &self->planeQ, scrollStep);

  if(self->selectedPlane == qfePlaneNone) self->engine->CallInteractionParentWheelForward();
  
  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeOnWheelBackward(void *caller, int x, int y, int v)
{
  qfeDriverIV *self = reinterpret_cast<qfeDriverIV *>(caller);

  if(self->particleSinkGuidePointSelected)
  {
	  self->algorithmIP->particleSinks->qfeStretchSeedVolume(self->pointSelected.index);

	  self->engine->Refresh();

	  return;
  }  
  
  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if(self->visualReformat == NULL) return;

  // Obtain the volume
  self->visualReformat->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visualReformat->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);

  if((int)study->pcap.size() <= 0) return;
  
  if(self->selectedPlane == qfePlaneX) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeX, scrollStep);
  if(self->selectedPlane == qfePlaneY) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeY, scrollStep);
  if(self->selectedPlane == qfePlaneZ) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeZ, scrollStep);
  if(self->selectedPlane == qfePlaneQ) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeQ, scrollStep);

  if(self->selectedPlane == qfePlaneNone) self->engine->CallInteractionParentWheelBackward();
  
  self->engine->Refresh();
}
//----------------------------------------------------------------------------
void qfeDriverIV::qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if(volume == NULL) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
	origin = origin + (mm*normal);
  else
	origin = origin - (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = MAX(originTex.x, 0.0+delta);
  originTex.x = MIN(originTex.x, 1.0-delta);
  originTex.y = MAX(originTex.y, 0.0+delta);
  originTex.y = MIN(originTex.y, 1.0-delta);
  originTex.z = MAX(originTex.z, 0.0+delta);
  originTex.z = MIN(originTex.z, 1.0-delta);

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

//----------------------------------------------------------------------------
void qfeDriverIV::qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if(volume == NULL) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
	origin = origin - (mm*normal);
  else
	origin = origin + (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = MAX(originTex.x, 0.0+delta);
  originTex.x = MIN(originTex.x, 1.0-delta);
  originTex.y = MAX(originTex.y, 0.0+delta);
  originTex.y = MIN(originTex.y, 1.0-delta);
  originTex.z = MAX(originTex.z, 0.0+delta);
  originTex.z = MIN(originTex.z, 1.0-delta);

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

bool qfeDriverIV::qfeGetPointOnSlices(int x, int y, qfePoint &p)
{
  // Convert mouse click to ray
  qfePoint rayStart, rayEnd;
  qfeClickToRay(x, y, rayStart, rayEnd);

  // Determine slices to check
  qfeVisualParameter *param;  
  bool                paramShowX, paramShowY, paramShowZ, paramShowQ;

  visualReformat->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowX);    
  visualReformat->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowY);    
  visualReformat->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowZ);   
  visualPOV->qfeGetVisualParameter("Show view orthogonal plane", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowQ); 

  // Find closest slice intersection
  qfePoint intersection, closestIntersection;
  float distance;
  float smallestDistance = 1e30f;

  if (paramShowX && qfeGetRaySliceIntersection(rayStart, rayEnd, planeX, intersection, distance) && distance < smallestDistance) 
  {
	smallestDistance = distance;
	closestIntersection = intersection;
  }

  if (paramShowY && qfeGetRaySliceIntersection(rayStart, rayEnd, planeY, intersection, distance) && distance < smallestDistance) 
  {
	smallestDistance = distance;
	closestIntersection = intersection;
  }

  if (paramShowZ && qfeGetRaySliceIntersection(rayStart, rayEnd, planeZ, intersection, distance) && distance < smallestDistance) 
  {
	smallestDistance = distance;
	closestIntersection = intersection;
  }

  if (paramShowQ && qfeGetRaySliceIntersection(rayStart, rayEnd, planeQ, intersection, distance) && distance < smallestDistance) 
  {
	smallestDistance = distance;
	closestIntersection = intersection;
  }

  // If there was no intersection
  if (smallestDistance == 1e30f)
	return false;

  p = closestIntersection;
  return true;
}

void qfeDriverIV::qfeClickToRay(int x, int y, qfePoint &start, qfePoint &end) 
{
  qfeViewport *vp3D;
  engine->GetViewport3D(&vp3D);

  // Take window coordinates of mouseclick on near plane, unproject to patient coordinates
  // Then do the same for the far plane
  qfeTransform::qfeGetCoordinateUnproject(qfePoint((float)x, (float)y, 0.f), start, currentModelView, currentProjection, vp3D);
  qfeTransform::qfeGetCoordinateUnproject(qfePoint((float)x, (float)y, 1.f), end, currentModelView, currentProjection, vp3D);
}

//checks whether any of the points in vector "points" is on the line, if not return -1, otherwise return the index of the closest point
int qfeDriverIV::qfePointsOnLine(qfePoint start, qfePoint end, std::vector<seedVolumeGuidePoint> points) 
{
  int index = -1;

  float distBestAC = 1e6; //closest distance found so far
  float distAC; //distance from ray start to point "C"
  float distBC; //distance from ray end to point "C"

  float distAB; //distance from ray start to ray end

  qfePoint::qfePointDistance(start, end, distAB);
  
  for(unsigned int i = 0; i<points.size(); i++)
  {
	  qfePoint::qfePointDistance(start, points[i].point, distAC);
	  qfePoint::qfePointDistance(points[i].point, end, distBC);

	  if(fabs(distAC + distBC - distAB)<0.0015) //the point is on the line
	  {//explanation of why can be found here: http://stackoverflow.com/questions/17692922/check-is-a-point-x-y-is-between-two-points-drawn-on-a-straight-line
		  if(distAC < distBestAC)
		  {//best point on the line so far
			  distBestAC = distAC;
			  index = i;
		  }
	  }
  }

  return index;
}

bool qfeDriverIV::qfeGetRaySliceIntersection(qfePoint rayStart, qfePoint rayEnd, qfeFrameSlice *slice, qfePoint &intersection, float &distance)
{
  // Define slice as a rectangle by point P0 and sides S1, S2, with normal N
  qfePoint *sliceCorners;
  qfeGetSliceCorners(slice, &sliceCorners);
  qfePoint P0 = sliceCorners[0];
  qfeVector S1 = sliceCorners[1] - P0;
  qfeVector S2 = sliceCorners[3] - P0;
  qfeVector N;
  qfeVector::qfeVectorCross(S1, S2, N);
  qfeVector::qfeVectorNormalize(N);

  // Define ray as R0 + t*D
  qfePoint R0 = rayStart;
  qfeVector D = rayEnd - rayStart;
  qfeVector::qfeVectorNormalize(D);

  float DdotN;
  qfeVector::qfeVectorDot(D, N, DdotN);
  if (DdotN == 0.f) // If looking directly at the side of the slice
	return false;

  float P0minR0dotN;
  qfeVector::qfeVectorDot(P0 - R0, N, P0minR0dotN);
  float a = P0minR0dotN / DdotN;

  qfePoint P = R0 + a * D;; // Intersection point between ray and plane in which slice resides

  // Q1 and Q2 are projections of P - P0 onto S1 and S2 respectively
  qfeVector P0P = P - P0;
  qfeVector S1U = S1;
  qfeVector S2U = S2;
  qfeVector::qfeVectorNormalize(S1U);
  qfeVector::qfeVectorNormalize(S2U);
  float q1, q2;
  qfeVector::qfeVectorDot(P0P, S1U, q1);
  qfeVector::qfeVectorDot(P0P, S2U, q2);
  qfeVector Q1 = S1U * q1;
  qfeVector Q2 = S2U * q2;

  // Check if ray-plane intersection is inside rectangle
  float ls1, ls2, lq1, lq2;
  qfeVector::qfeVectorLength(S1, ls1);
  qfeVector::qfeVectorLength(S2, ls2);
  qfeVector::qfeVectorLength(Q1, lq1);
  qfeVector::qfeVectorLength(Q2, lq2);
  if (lq1 <= ls1 && lq2 <= ls2) {
	intersection = P;
	distance = a;
	return true;
  }

  return false;
}

void qfeDriverIV::qfeGetSliceCorners(qfeFrameSlice *slice, qfePoint **corners)
{
  qfeVolume *volume;
  int paramData;
  qfeVisualParameter *param;
  qfeSelectionList paramDataList;

  visualReformat->qfeGetVisualParameter("Ortho data set", &param);

  if (param) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  int current;
  this->visualReformat->qfeGetVisualActiveVolume(current);
  qfeGetDataSet(paramData, visualReformat, current, &volume);

  int nrVertices;
  algorithmReformat->qfeComputePolygon(volume, slice, corners, nrVertices);
}

std::string qfeDriverIV::valueToPrettyString(float value)
{
	if(value == 0.00000)
		return "0";

	std::ostringstream strs;
	strs << value;

	std::string str = strs.str();
	
	//remove all trailing zeros:
	/* this does not work well, it removes the zeros from for example 100.0 too
	str.erase(str.find_last_not_of('0') + 1, std::string::npos);

	if(str.length()>0 && str.at(str.length()-1) == '.')//if last character is a dot we can remove it (the float was actually an integer)
	{
		str.pop_back();
	}
	*/

	return str;
}

qfeReturnStatus qfeDriverIV::qfeSaveSeedPositions()
{
	QFileDialog fileDialog(0, "Save file", QDir::currentPath(), "*.ink_seed");
	fileDialog.selectNameFilter("*.ink_seed");
	fileDialog.setAcceptMode(QFileDialog::AcceptSave);
	fileDialog.setDefaultSuffix("ink_seed");
	fileDialog.exec();

	if(fileDialog.selectedFiles().size() != 0)
	{
		QString fileName = fileDialog.selectedFiles().first();

		QFile file(fileName);
		file.open( QIODevice::WriteOnly );

		file.resize(0); //clear the file

		// store data in f
		QTextStream stream(&file);

		qfePoint _dia1 = this->seedPosition1;
		qfePoint _dia2 = this->seedPosition2;
		qfePoint _apex = this->seedPosition3;
		
		stream <<_dia1.x<<";"<<_dia1.y<<";"<<_dia1.z<<";";
		stream <<_dia2.x<<";"<<_dia2.y<<";"<<_dia2.z<<";";
		stream <<_apex.x<<";"<<_apex.y<<";"<<_apex.z<<";" << endl;

		file.close();
	}

	return qfeSuccess;
}

qfeReturnStatus qfeDriverIV::qfeLoadSeedPositions()
{
	QFileDialog fileDialog(0, "Load file", QDir::currentPath(), "*.ink_seed;;*.smoke_seed");
	fileDialog.selectNameFilter("*.ink_seed;;*.smoke_seed");
	fileDialog.setAcceptMode(QFileDialog::AcceptOpen);
	fileDialog.exec();

	if(fileDialog.selectedFiles().size() != 0)
	{
		QString fileName = fileDialog.selectedFiles().first();

		QFile file(fileName);
		file.open( QIODevice::ReadOnly );
		
		// read data in f
		QTextStream stream(&file);
		while(!stream.atEnd())
		{
			QString line = stream.readLine();
			QStringList list = line.split(";");

			if(list.size()<9)
				continue;

			qfePoint _dia1;
			qfePoint _dia2;
			qfePoint _apex;

			_dia1.x = list[0].toFloat();
			_dia1.y = list[1].toFloat();
			_dia1.z = list[2].toFloat();

			_dia2.x = list[3].toFloat();
			_dia2.y = list[4].toFloat();
			_dia2.z = list[5].toFloat();

			_apex.x = list[6].toFloat();
			_apex.y = list[7].toFloat();
			_apex.z = list[8].toFloat();

			this->seedPosition1 = _dia1;
			this->seedPosition2 = _dia2;
			this->seedPosition3 = _apex;
			
			break;
		}

		file.close();
	}

	this->qfeUpdate();

	return qfeSuccess;
}