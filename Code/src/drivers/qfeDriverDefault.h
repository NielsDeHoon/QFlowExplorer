#pragma once

#include "qflowexplorer.h"

#include "qfeDriver.h"

#include <vtkPolyDataMapper.h>
#include <vtkSphereSource.h>

/**
* \file   qfeDriverDefault.h
* \author Roy van Pelt
* \class  qfeDriverDefault
* \brief  Implements a default driver
*
* qfeDriverDefault derives from qfeDriver, and implements
* a default background driver
*/

class qfeDriverDefault : public qfeDriver
{
public:
  qfeDriverDefault(qfeScene *scene);
  ~qfeDriverDefault();

  bool qfeRenderCheck();
  void qfeRenderSelect(){};
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();

private:

};
