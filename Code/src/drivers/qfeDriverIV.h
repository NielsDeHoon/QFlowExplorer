#pragma once

#include <memory>

#include "qflowexplorer.h"

#include "qfeDriver.h"
#include "qfeMaximumProjection.h"
#include "qfePlanarReformat.h"
#include "qfeInkParticles.h"
#include "qfeSurfaceShading.h" 
#include "qtTransferFunctionWidget.h"

#include <array>
#include "fluidsim/array3.h"

/**
* \file   qfeDriverIV.h
* \author Niels de Hoon
* \class  qfeDriverIV
* \brief  Implements functions for ink visualization
*
* qfeDriverIV implements various functions
* for ink-like visualizations
*/

class qfeDriverIV : public qfeDriver
{
public:
  qfeDriverIV(qfeScene *scene);
  ~qfeDriverIV();

  bool qfeRenderCheck();
  void qfeRenderSelect();
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();
  
  enum qfePlaneDirection
  {
    qfePlaneAxial,
    qfePlaneSagittal,
    qfePlaneCoronal,
  };

  enum qfePlaneSelected
  {
    qfePlaneNone,
    qfePlaneX,
    qfePlaneY,
    qfePlaneZ,
	qfePlaneQ
  };

  enum qfeSeedingPointState {
  seedingPointNone = 0,
  seedingPointOne,
  seedingPointTwo,
  seedingPointThree
};

private:
  bool particleSinkGuidePointSelected;
  seedVolumeGuidePoint pointSelected;
  bool newParticleSink;
  std::vector<qfePoint> newParticleSinkPoints;

  qfePoint seedPosition1, seedPosition2, seedPosition3;
  bool seedPosition3_enabled;
  qfePoint newSeedPosition;
  unsigned int selected_seeding_point;
  bool rightMouseDown;

  bool filteringGraphRendered;

  float qfeGetProbability(float value, float rangeMin, float rangeMax, QVector<QPair<double, double>> &tf);
  void qfeFeatureBasedSeeding();

  qfeMatrix4f P2V, T2V, V2P, V2T;
  qfeStudy    *study;
  unsigned int ni,nj,nk;
  float currentTime;

  unsigned int screen_size_x;
  unsigned int screen_size_y;

  bool brush_active;
  Vec2f brush_position1;
  Vec2f brush_position2;

  float animation_duration;

  qfeVisualInkVis			*visualInkVisP;
  qfeVisualSeedVolumes		*visualSeedVols;
  qfeVisualReformatOrtho	*visualReformat;
  qfeVisualPlaneOrthoView   *visualPOV;
  qfeVisualAnatomyIllustrative *visualAnatomy;
  qfeVisualMaximum			*visualMIP;
  qfeVisualAides			*visAides;

  qfePlanarReformat *algorithmReformat;
  qfeMaximumProjection *algorithmMIP;
  qfeInkParticles	*algorithmIP;
  qfeSurfaceShading *algorithmSurfaceShading;

  int                        selectedPlane;
  qfeFrameSlice             *planeX;
  qfeFrameSlice             *planeY;
  qfeFrameSlice             *planeZ;
  qfeFrameSlice             *planeQ;
  
  GLuint              currentTransferFunctionId;

  GLuint texRayStart;
  GLuint texRayEnd;

  qfeReturnStatus qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual);
  qfeReturnStatus qfeRenderInk(qfeVisualInkVis *visual);
  qfeReturnStatus qfeRenderVisualMIP(qfeVisualMaximum *visual);
  qfeReturnStatus qfeRenderVisualSeedVolumes(qfeVisualSeedVolumes *visual);
  qfeReturnStatus qfeRenderVisualInk(qfeVisualInkVis *visual);
  qfeReturnStatus qfeRenderVisualPlanesOrtho(qfeVisualReformat *visual);
  qfeReturnStatus qfeRenderPlaneOrthoView(qfeVisualPlaneOrthoView *visual, qfeVisualReformat *visMPR);
  qfeReturnStatus qfeRenderVisualAides(qfeVisualAides *visual);

  qfeColorRGB                colorReformatDefault;
  qfeColorRGB                colorReformatHighlight;

  qfeReturnStatus qfeGetDataSet(int index, qfeVisual *visual, int time, qfeVolume **volume);

  qfeReturnStatus qfeGetPlaneOrtho(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice);
  qfeReturnStatus qfeGetPlaneViewOrthogonal(qfeVolume *volume, qfeFrameSlice **slice);


  void qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm);
  void qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm);

  std::vector<qfeVolume> SNR_Volumes;
  std::array<float, 2> SNR_Range;
  std::vector<qfeVolume> Signal_Strength_Volumes;
  std::array<float, 2> Signal_Strength_Range;
  qfeVolume particleDensityVolume;
  qfeVolume meshLevelsetVolume;
  std::array<float, 2> meshLevelSetRange;

  static void qfeOnAction(void *caller, int t);
  static void qfeOnResize(void *caller);  
  static void qfeOnMouseMove(void *caller, int x, int y, int v);
  static void qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v);
  static void qfeOnMouseRightButtonUp(void *caller, int, int y, int v);
  static void qfeOnMouseRightButtonDown(void *caller, int x, int y, int v);  
  static void qfeOnWheelForward(void *caller, int x, int y, int v);
  static void qfeOnWheelBackward(void *caller, int x, int y, int v);

  bool qfeGetPointOnSlices(int x, int y, qfePoint &p);
  void qfeClickToRay(int x, int y, qfePoint &start, qfePoint &end);
  int qfePointsOnLine(qfePoint start, qfePoint end, std::vector<seedVolumeGuidePoint> points);
  bool qfeGetRaySliceIntersection(qfePoint rayStart, qfePoint rayEnd, qfeFrameSlice *slice, qfePoint &intersection, float &distance);
  void qfeGetSliceCorners(qfeFrameSlice *slice, qfePoint **corners);

  qfeReturnStatus qfeSaveSeedPositions();
  qfeReturnStatus qfeLoadSeedPositions();

  std::string valueToPrettyString(float value);
};