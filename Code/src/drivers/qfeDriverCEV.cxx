#include "qfeDriverCEV.h"
#include "qfeKernel.h"

//----------------------------------------------------------------------------
qfeDriverCEV::qfeDriverCEV(qfeScene *scene) : qfeDriver(scene) {
  visualCEV         = nullptr;
  visParticleTrace  = nullptr;
  visLineTrace      = nullptr;
  visAides          = nullptr;
  visOrtho          = nullptr;
  visSE             = nullptr;
  visPOV            = nullptr;
  visFS             = nullptr;
  visVOI            = nullptr;
  visPDVR           = nullptr;

  // Initialize plane line colors
  colorDefault.r = 0.952;
  colorDefault.g = 0.674;
  colorDefault.b = 0.133;

  colorHighlight.r = 0.356;
  colorHighlight.g = 0.623;
  colorHighlight.b = 0.396;

  // Initialize the planes (in patient coordinates)
  planeX = new qfeFrameSlice();      
  planeX->qfeSetFrameExtent(1,1);
  planeX->qfeSetFrameOrigin(0.0,0.0,0.0);
  planeX->qfeSetFrameAxes(0.0,0.0,1.0, 0.0,1.0,0.0, 1.0,0.0,0.0);  

  planeY = new qfeFrameSlice();      
  planeY->qfeSetFrameExtent(1,1);
  planeY->qfeSetFrameOrigin(0.0,0.0,0.0);
  planeY->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,0.0,1.0, 0.0,1.0,0.0);  

  planeZ = new qfeFrameSlice();      
  planeZ->qfeSetFrameExtent(1,1);
  planeZ->qfeSetFrameOrigin(0.0,0.0,0.0);
  planeZ->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0);  

  planeQ = new qfeFrameSlice();
  planeQ->qfeSetFrameExtent(1,1);
  planeQ->qfeSetFrameOrigin(0.0,0.0,0.0);
  planeQ->qfeSetFrameAxes(0.0,0.0,1.0, 0.0,1.0,0.0, 1.0,0.0,0.0); 

  selectedPlane = qfePlaneNone;

  defaultColorMap = qfeGetDefaultColorMap();

  currentTransferFunctionId = 0;

  seedingStateLines = probeSeedingNone;
  seedingStateParticles = probeSeedingNone;
  lvGuidingPointState = guidingPointNone;
  rvGuidingPointState = guidingPointNone;
  voiGuidingPointState = guidingPointNone;
  lvSelectedGuidingPoint = guidingPointNone;
  rvSelectedGuidingPoint = guidingPointNone;
  voiSelectedGuidingPoint = guidingPointNone;
  lvEllipsoidSelected = false;
  rvEllipsoidSelected = false;
  voiEllipsoidSelected = false;
  tMipAvailable = false;

  volumeSize0 = 0;
  volumeSize1 = 0;

  clipAllData = false;
  previousClipAllData = false;

  engine->SetObserverAction((void*)this, qfeOnAction);
  engine->SetObserverResize(this, qfeOnResize);
  engine->SetInteractionMouseMove(this, qfeOnMouseMove, false);
  engine->SetInteractionMouseLeftButtonDown(this, qfeOnMouseLeftButtonDown, false);
  engine->SetInteractionMouseLeftButtonUp(this, qfeOnMouseLeftButtonUp, true);
  engine->SetInteractionMouseRightButtonDown(this, qfeOnMouseRightButtonDown, false);
  engine->SetInteractionMouseWheelForward(this, qfeOnWheelForward, false);
  engine->SetInteractionMouseWheelBackward(this, qfeOnWheelBackward, false);
}

//----------------------------------------------------------------------------
qfeDriverCEV::~qfeDriverCEV() {
  qfeRenderStop();
}

//----------------------------------------------------------------------------
bool qfeDriverCEV::qfeRenderCheck() {
  int t;
  for (int i=0; i<(int)visuals.size(); i++)   {
    visuals[i]->qfeGetVisualType(t);
    if (t == visualCEVType)
      visualCEV = static_cast<qfeVisualCEV *>(visuals[i]);
    if (t == visualFlowLines)
      visLineTrace = static_cast<qfeVisualFlowLineTrace *>(visuals[i]);
    if (t == visualFlowParticles)
      visParticleTrace = static_cast<qfeVisualFlowParticleTrace *>(visuals[i]);
    if (t == visualAides)
      visAides = static_cast<qfeVisualAides *>(visuals[i]);
    if (t == visualReformatOrtho)
      visOrtho = static_cast<qfeVisualReformatOrtho *>(visuals[i]);
    if (t == visualSeedEllipsoidType)
      visSE = static_cast<qfeVisualSeedEllipsoid *>(visuals[i]);
    if (t == visualPlaneOrthoViewType)
      visPOV = static_cast<qfeVisualPlaneOrthoView *>(visuals[i]);
    if (t == visualFeatureSeedingType)
      visFS = static_cast<qfeVisualFeatureSeeding *>(visuals[i]);
    if (t == visualVolumeOfInterest)
      visVOI = static_cast<qfeVisualVolumeOfInterest *>(visuals[i]);
    if (t == visualProbabilityDVR)
      visPDVR = static_cast<qfeVisualProbabilityDVR *>(visuals[i]);
  }
  return true;
}

void qfeDriverCEV::qfeRenderSelect() {
  qfeVolume    *volume;

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  int                 paramData;
  bool                paramShowX, paramShowY, paramShowZ, paramShowQ;
  bool                paramShowGuidePoints;

  // Get the visual parameters
  visOrtho->qfeGetVisualParameter("Ortho data set", &param);
  if (param) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visOrtho->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowX);

  visOrtho->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowY);    

  visOrtho->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowZ); 

  visPOV->qfeGetVisualParameter("Show view orthogonal plane", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowQ);   

  // Process the visual parameters  
  qfeGetDataSetOrtho(paramData, visOrtho, &volume);    

  if (paramShowX) {
    qfeSetSelectColor(qfeColorRGB(1.f, 0.f, 0.f));
    algorithmOrtho.qfeRenderPolygon3D(volume, planeX, NULL);
  }

  if (paramShowY) {
    qfeSetSelectColor(qfeColorRGB(0.f, 1.f, 0.f));
    algorithmOrtho.qfeRenderPolygon3D(volume, planeY, NULL);
  }

  if (paramShowZ) {
    qfeSetSelectColor(qfeColorRGB(0.f, 0.f, 1.f));
    algorithmOrtho.qfeRenderPolygon3D(volume, planeZ, NULL);
  }

  if (paramShowQ) {
    qfeSetSelectColor(qfeColorRGB(0.5f, 0.5f, 0.5f));
    algorithmOrtho.qfeRenderPolygon3D(volume, planeQ, NULL);
  }

  visSE->qfeGetVisualParameter("Guide points visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowGuidePoints);
  if (paramShowGuidePoints) {
    qfeSetSelectColor(qfeColorRGB(0.f, 1.f, 1.f));
    qfeRenderGuidingPoint(0, SE_Left);
    qfeSetSelectColor(qfeColorRGB(1.f, 0.f, 1.f));
    qfeRenderGuidingPoint(1, SE_Left);
    qfeSetSelectColor(qfeColorRGB(1.f, 1.f, 0.f));
    qfeRenderGuidingPoint(2, SE_Left);

    qfeSetSelectColor(qfeColorRGB(0.f, 0.5f, 0.5f));
    qfeRenderGuidingPoint(0, SE_Right);
    qfeSetSelectColor(qfeColorRGB(0.5f, 0.f, 0.5f));
    qfeRenderGuidingPoint(1, SE_Right);
    qfeSetSelectColor(qfeColorRGB(0.5f, 0.5f, 0.f));
    qfeRenderGuidingPoint(2, SE_Right);
  }
  visVOI->qfeGetVisualParameter("Guide points visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowGuidePoints);
  if (paramShowGuidePoints) {
    qfeSetSelectColor(qfeColorRGB(0.f, 0.5f, 1.f));
    qfeRenderGuidingPoint(0, SE_VoI);
    qfeSetSelectColor(qfeColorRGB(0.5f, 0.f, 1.f));
    qfeRenderGuidingPoint(1, SE_VoI);
    qfeSetSelectColor(qfeColorRGB(0.5f, 1.f, 0.f));
    qfeRenderGuidingPoint(2, SE_VoI);
  }
}

//----------------------------------------------------------------------------
void qfeDriverCEV::qfeRenderStart() {
  qfeViewport *vp3D;
  engine->GetViewport3D(&vp3D);

  if (visualCEV) {
    algorithmIVR.qfeSetViewportSize(vp3D->width, vp3D->height, frameBufferProps[0].supersampling);
    algorithmPDVR.qfeSetViewportSize(vp3D->width, vp3D->height, frameBufferProps[0].supersampling);
    algorithmIVR.qfeCreateDotMap();

    qfeStudy *study;
    visualCEV->qfeGetVisualStudy(&study);

    study->pcap[0]->qfeGetVolumeSize(width, height, depth);
    nrFrames = study->pcap.size();

    texGradients = new GLuint[nrFrames];
    if (study->pcam.size() > 0) {
      algorithmGradient.qfeSetData(&study->pcam);
      gradients = algorithmGradient.qfeCalculateGradients();
      maxGradLength = algorithmGradient.qfeGetMaxGradientLength();
      algorithmIVR.qfeSetMaxGradientLength(maxGradLength);
    }

    if (study->tmip) {
      tMipAvailable = true;
      qfeVolumeSeries series;
      series.push_back(study->tmip);
      algorithmGradient.qfeSetData(&series);
      tMipGradients = algorithmGradient.qfeCalculateGradients()[0];
      maxGradLengthTMip = algorithmGradient.qfeGetMaxGradientLength();
    }
  }

  if (visParticleTrace) {
    qfeStudy *study;
    qfeVisualParameter p;

    visParticleTrace->qfeGetVisualStudy(&study); 

    p.qfeSetVisualParameter("Particles lifetime", (int)study->pcap.size() / 3, 1, 1, 500);
    visParticleTrace->qfeAddVisualParameter(p);

    scene->qfeSceneUpdate();
  }

  if (visLineTrace) {
    qfeStudy *study;
    qfeVisualParameter p;

    visLineTrace->qfeGetVisualStudy(&study); 

    p.qfeSetVisualParameter("Lines phases static", (int)study->pcap.size(), 1, 1, 500);
    visLineTrace->qfeAddVisualParameter(p);

    scene->qfeSceneUpdate();
  }

  if (visSE) {
    lvEllipsoid.qfeSetVtkCamera(engine->GetRenderer()->GetActiveCamera(), vp3D->width, vp3D->height);
    rvEllipsoid.qfeSetVtkCamera(engine->GetRenderer()->GetActiveCamera(), vp3D->width, vp3D->height);
  }
  if (visVOI)
    voiEllipsoid.qfeSetVtkCamera(engine->GetRenderer()->GetActiveCamera(), vp3D->width, vp3D->height);

  qfeVolume *volume;
  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  int                 paramData;
  if (visOrtho) {
    visOrtho->qfeGetVisualParameter("Ortho data set", &param);
    if (param) param->qfeGetVisualParameterValue(paramDataList);  
    paramData = paramDataList.active;

    qfeGetDataSetOrtho(paramData, visOrtho, &volume);
  }

  if (volume) {
    qfeGetPlaneOrtho(volume, qfePlaneSagittal, &planeX);  
    qfeGetPlaneOrtho(volume, qfePlaneCoronal,  &planeY);
    qfeGetPlaneOrtho(volume, qfePlaneAxial,    &planeZ);

    qfeGrid *grid;
    qfePoint o;
    volume->qfeGetVolumeGrid(&grid);
    grid->qfeGetGridOrigin(o.x, o.y, o.z);
    planeQ->qfeSetFrameOrigin(o.x, o.y, o.z);
    qfeGetPlaneViewOrthogonal(volume, &planeQ);
  }

  qfeCopyVolumeSeries();

  qfeCreateTextures(vp3D, frameBufferProps[0].supersampling);
}

//----------------------------------------------------------------------------
void qfeDriverCEV::qfeRenderWait() {
  qfeStudy *study;
  int current;
  visualCEV->qfeGetVisualStudy(&study);
  visualCEV->qfeGetVisualActiveVolume(current);
  qfeVolume *volume = study->pcap[current];

  // Initialize OpenGL state
  glDisable(GL_DITHER);
  glDisable(GL_NORMALIZE);
  glDisable(GL_LIGHTING);
  glDisable(GL_COLOR_MATERIAL);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Initialize color map
  qfeColorMap *colormap;
  if (visualCEV) {
   // visualCEV->qfeSetVisualColorMap(defaultColorMap);
    visualCEV->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
    colormap->qfeUploadGradientMap();
  }
  if (visParticleTrace) {
    visParticleTrace->qfeGetVisualColorMap(&colormap);
    vector<qfeRGBMapping> colors = qfeGetFlowVisColorMap();
    colormap->qfeSetColorMapRGB(colors.size(), colors);
    colormap->qfeUploadColorMap(); 
  }
  if (visLineTrace) {
    visLineTrace->qfeGetVisualColorMap(&colormap);
    vector<qfeRGBMapping> colors = qfeGetFlowVisColorMap();
    colormap->qfeSetColorMapRGB(colors.size(), colors);
    colormap->qfeUploadColorMap(); 
  }

  qfeEnableIntersectBuffer();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  qfeDisableIntersectBuffer();

  // If volume rendering is set to invisible, do not enable intersect buffer
  qfeVisualParameter *param;
  bool                paramVisible;
  if (visualCEV) visualCEV->qfeGetVisualParameter("IVR visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramVisible);

  // Note order: opaque rendering first, then volume rendering
  if (visAides)
    qfeRenderVisualAides(visAides, frameBufferProps[0].supersampling);
  if (paramVisible)
    qfeEnableIntersectBuffer();
  if (visOrtho)
    qfeRenderVisualPlanesOrtho(visOrtho);
  if (visParticleTrace)
    qfeRenderVisualParticleTrace(visParticleTrace, frameBufferProps[0].supersampling);
  if (visLineTrace)
    qfeRenderVisualLineTrace(visLineTrace, frameBufferProps[0].supersampling);
  if (visSE)
    qfeRenderSeedEllipsoid(visSE);
  if (visVOI)
    qfeRenderVolumeOfInterest(visVOI);
  qfeRenderFeatureSeedPoints();
  if (visPOV)
    qfeRenderPlaneOrthoView(visPOV, visOrtho);
  if (paramVisible)
    qfeDisableIntersectBuffer();
  if (visualCEV)
    qfeRenderVisualCEV(visualCEV, visVOI);
  if (visPDVR)
    qfeRenderProbabilityDVR(visPDVR, visVOI);
}

//----------------------------------------------------------------------------
void qfeDriverCEV::qfeRenderStop() {
  for (unsigned i = 0; i < nrFrames; i++)
    delete[] gradients[i];
  delete[] gradients;
  gradients = nullptr;

  delete planeX;
  delete planeY;
  delete planeZ;
  delete planeQ;

  qfeDeleteTextures();
  delete[] texGradients;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverCEV::qfeRenderVisualCEV(qfeVisualCEV *visual, qfeVisualVolumeOfInterest *voiVisual) {
  qfeStudy       *study;
  qfeVolume      *volume;
  qfeColorMap    *colormap;
  qfeLightSource *light;
  int             current;

  qfeVisualParameter *param;
  bool                paramVisible;
  qfeFloat            paramRange[2];
  double              paramDirTransparency;
  bool                paramDepthTransparency;
  bool                paramLitSphereMapRendering;
  qfeSelectionList    paramLsmPresetList;
  int                 paramLsmPreset;
  qfeSelectionList    paramContextList;
  int                 paramContext;
  bool                paramGradientSampling;
  bool                paramDepthToneMapping;
  double              paramDepthToneMappingFactor;
  int                 paramLsmNrDots;
  double              paramLsmLambda;
  bool                paramClipVoI;
  double              paramSimpleOpacity;
  bool                paramClipSeedPoints = false;

  if (!visual) return qfeError;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  scene->qfeGetSceneLightSource(&light);

  voiVisual->qfeGetVisualParameter("Clip seed points", &param);
  if (param) param->qfeGetVisualParameterValue(paramClipSeedPoints);
  if (paramClipSeedPoints)
    qfeClipSeedPoints();

  // Fetch the parameters
  visual->qfeGetVisualParameter("IVR visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramVisible);

  if (paramVisible != true) return qfeError;

  visual->qfeGetVisualParameter("Directional transparency", &param);
  if (param) param->qfeGetVisualParameterValue(paramDirTransparency);

  visual->qfeGetVisualParameter("Depth transparency", &param);
  if (param) param->qfeGetVisualParameterValue(paramDepthTransparency);

  visual->qfeGetVisualParameter("Lit Sphere Map rendering", &param);
  if (param) param->qfeGetVisualParameterValue(paramLitSphereMapRendering);

  visual->qfeGetVisualParameter("Lit Sphere Map preset", &param);
  if (param) param->qfeGetVisualParameterValue(paramLsmPresetList);
  paramLsmPreset = paramLsmPresetList.active;

  visual->qfeGetVisualParameter("Context data", &param);
  if (param) param->qfeGetVisualParameterValue(paramContextList);
  paramContext = paramContextList.active;

  visual->qfeGetVisualParameter("Sample gradient", &param);
  if (param) param->qfeGetVisualParameterValue(paramGradientSampling);

  visual->qfeGetVisualParameter("Depth Tone Mapping", &param);
  if (param) param->qfeGetVisualParameterValue(paramDepthToneMapping);

  visual->qfeGetVisualParameter("Depth Tone Mapping factor", &param);
  if (param) param->qfeGetVisualParameterValue(paramDepthToneMappingFactor);

  visual->qfeGetVisualParameter("Dot LSM: Number of dots", &param);
  if (param) param->qfeGetVisualParameterValue(paramLsmNrDots);

  visual->qfeGetVisualParameter("Dot LSM: Lambda", &param);
  if (param) param->qfeGetVisualParameterValue(paramLsmLambda);

  voiVisual->qfeGetVisualParameter("Clip context visualization", &param);
  if (param) param->qfeGetVisualParameterValue(paramClipVoI);

  visual->qfeGetVisualParameter("Simple opacity", &param);
  if (param) param->qfeGetVisualParameterValue(paramSimpleOpacity);

  // Get the data set and compute the data range
  qfeGetDataSet(visual, current, &volume, paramRange[0], paramRange[1], paramContext);

  if (!volume) return qfeError;  

  // Update algorithm parameters
  algorithmIVR.qfeSetDataRange(paramRange[0], paramRange[1]);
  algorithmIVR.qfeSetLightSource(light);
  algorithmIVR.qfeSetDirectionalTransparencyExponent(paramDirTransparency);
  algorithmIVR.qfeSetDepthTransparency(paramDepthTransparency);
  algorithmIVR.qfeSetLitSphereMapRendering(paramLitSphereMapRendering);
  algorithmIVR.qfeSetGradientSampling(paramGradientSampling);
  algorithmIVR.qfeSetLitSphereMapPreset(paramLsmPreset);
  algorithmIVR.qfeSetDepthToneMapping(paramDepthToneMapping);
  algorithmIVR.qfeSetDepthToneMappingFactor(paramDepthToneMappingFactor);
  algorithmIVR.qfeSetLsmNrDots(paramLsmNrDots);
  algorithmIVR.qfeSetLsmLambda(paramLsmLambda);
  algorithmIVR.qfeSetVoITInv(voiEllipsoid.qfeGetTInverseMatrix());
  algorithmIVR.qfeSetVoICenter(voiEllipsoid.qfeGetCenter());
  algorithmIVR.qfeSetClipVoI(paramClipVoI);
  algorithmIVR.qfeSetSimpleOpacity(paramSimpleOpacity);
  double eye[3];
  engine->GetRenderer()->GetActiveCamera()->GetPosition(eye);
  algorithmIVR.qfeSetEyePosition(qfePoint(eye[0], eye[1], eye[2]));

  glActiveTexture(GL_TEXTURE22);
  if (paramContext == 0) {
    glBindTexture(GL_TEXTURE_3D, texGradients[current]);
    algorithmIVR.qfeSetMaxGradientLength(maxGradLength);
  } else if (paramContext == 1 && tMipAvailable) {
    glBindTexture(GL_TEXTURE_3D, texTMipGradients);
    algorithmIVR.qfeSetMaxGradientLength(maxGradLengthTMip);
  }
              
  algorithmIVR.qfeRenderRaycasting(frameBufferProps[0].texColor, frameBufferProps[0].texDepth, volume, colormap);

  glBindTexture(GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeRenderVisualParticleTrace(qfeVisualFlowParticleTrace *visual, int supersampling) {
  qfeStudy       *study;
  qfeLightSource *light;
  qfeColorMap    *colormap;
  qfeFloat        current;
  qfeDisplay     *display;

  qfeSeeds     seeds;
  int          seedsCount;

  qfeVisualParameter *param;
  qfeSelectionList    paramInterpolationList;
  qfeSelectionList    paramIntegrationList;
  qfeSelectionList    paramParticleStyle;
  qfeSelectionList    paramParticleShading;
  qfeSelectionList    paramParticleTrail;
  qfeSelectionList    paramParticleColor;
  bool                paramParticleContour;
  int                 paramCount;
  int                 paramSize;
  int                 paramSpeed;
  int                 paramLifeTime;  
  int                 paramColorScale;
  int                 paramIntegrationType;
  int                 paramIntegrationSteps;
  bool                paramVisible;
  double              paramTrailLifeTime;  

  if (!visual) return qfeError;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);  
  scene->qfeGetSceneDisplay(&display);
  scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("Particles visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("Particles style", &param);
  if (param) param->qfeGetVisualParameterValue(paramParticleStyle);

  visual->qfeGetVisualParameter("Particles shading", &param);
  if (param) param->qfeGetVisualParameterValue(paramParticleShading); 

  visual->qfeGetVisualParameter("Particles color", &param);
  if (param) param->qfeGetVisualParameterValue(paramParticleColor); 

  visual->qfeGetVisualParameter("Particles trail", &param);
  if (param) param->qfeGetVisualParameterValue(paramParticleTrail); 

  visual->qfeGetVisualParameter("Particles contour", &param);
  if (param) param->qfeGetVisualParameterValue(paramParticleContour); 

  visual->qfeGetVisualParameter("Particles count", &param);
  if (param) param->qfeGetVisualParameterValue(paramCount);  

  visual->qfeGetVisualParameter("Particles size (mm)", &param);
  if (param) param->qfeGetVisualParameterValue(paramSize);  

  visual->qfeGetVisualParameter("Particles lifetime", &param);
  if (param) param->qfeGetVisualParameterValue(paramLifeTime);  

  visual->qfeGetVisualParameter("Particles color scale (%)", &param);
  if (param) param->qfeGetVisualParameterValue(paramColorScale);  

  visual->qfeGetVisualParameter("Particles speed (%)", &param);
  if (param) param->qfeGetVisualParameterValue(paramSpeed);  

  visual->qfeGetVisualParameter("Particles integration", &param);
  if (param) param->qfeGetVisualParameterValue(paramIntegrationList);  
  paramIntegrationType = paramIntegrationList.active;

  visual->qfeGetVisualParameter("Particles steps/sample", &param);
  if (param) param->qfeGetVisualParameterValue(paramIntegrationSteps); 

  visual->qfeGetVisualParameter("Particles trail lifetime", &param);
  if (param) param->qfeGetVisualParameterValue(paramTrailLifeTime);  

  seedsCount = paramCount;

  if (!paramVisible) return qfeError;  

  // Set particle trace parameters
  qfeFloat phaseDuration, traceDuration;
  int      traceDirection;
  study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);

  algorithmParticleTrace.qfeSetTraceSteps(paramIntegrationSteps);   
  algorithmParticleTrace.qfeSetTraceScheme((qfeFlowParticleTrace::qfeFlowParticlesIntegrationScheme)paramIntegrationType);
  algorithmParticleTrace.qfeSetTraceSpeed(paramSpeed);
  algorithmParticleTrace.qfeSetTraceLifeTime(paramLifeTime);
  algorithmParticleTrace.qfeSetPhaseDuration(phaseDuration);
  algorithmParticleTrace.qfeSetParticleStyle((qfeFlowParticleTrace::qfeFlowParticlesStyle)paramParticleStyle.active);
  algorithmParticleTrace.qfeSetParticleColor((qfeFlowParticleTrace::qfeFlowParticlesColor)paramParticleColor.active);    
  algorithmParticleTrace.qfeSetParticleShading((qfeFlowParticleTrace::qfeFlowParticlesShading)paramParticleShading.active);    
  algorithmParticleTrace.qfeSetParticleContour(paramParticleContour);
  algorithmParticleTrace.qfeSetParticleSize(paramSize);  
  algorithmParticleTrace.qfeSetLightSource(light);
  algorithmParticleTrace.qfeSetColorScale(paramColorScale);
  algorithmParticleTrace.qfeSetTrailStyle((qfeFlowParticleTrace::qfeFlowParticlesTrailType)paramParticleTrail.active);  
  algorithmParticleTrace.qfeSetTrailLifeTime(paramTrailLifeTime);

  // Update the probe seeding
  qfeUpdateSeeds();

  // Evolve and advect the particles if the time has changed  
  if (current != prevTime) {        
    // Advect the particles    
    traceDirection =  1;
    if ((current < prevTime) && (abs(current - prevTime) < 1.0))      
      traceDirection = -1;
    if ((current > prevTime) && (abs(current - prevTime) > 1.0))      
      traceDirection = -1;      

    traceDuration = phaseDuration * abs(current - prevTime);
    if (traceDirection ==  1 && current == 0.0)
      traceDuration = phaseDuration * abs((int)study->pcap.size() - prevTime);
    if (traceDirection == -1 && prevTime == 0.0)
      traceDuration = phaseDuration * abs(current - (int)study->pcap.size());

    algorithmParticleTrace.qfeSetTraceDirection(traceDirection);

    algorithmParticleTrace.qfeAdvectParticles(study->pcap, current, traceDuration);

    // Evolve the particles
    algorithmParticleTrace.qfeEvolveParticles(); 

    // Update global time flag
    prevTime = current;
  }    

  // Render particles
  algorithmParticleTrace.qfeRenderParticles(colormap);

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeRenderVisualLineTrace(qfeVisualFlowLineTrace *visual, int supersampling) {
  qfeStudy       *study;
  qfeColorMap    *colormap;
  qfeLightSource *light;
  qfeFloat        current;
  qfeDisplay     *display;

  int             seedsCount;

  qfeVisualParameter *param;
  qfeSelectionList    paramInterpolationList;
  qfeSelectionList    paramIntegrationList;
  qfeSelectionList    paramStyleList;
  qfeSelectionList    paramShadingList;
  qfeSelectionList    paramSeedingBehavior;
  qfeSelectionList    paramSeedingType;
  int                 paramCount;
  //int                 paramInterpolationType;
  int                 paramIntegrationType;
  int                 paramIntegrationSteps;
  bool                paramVisible;
  int                 paramPhasesDynamic;
  int                 paramPhasesStatic;
  double              paramLineWidth;
  int                 paramColorScale;
  bool                paramContour;
  bool                paramHalo;
  int                 paramMaxAngle;
  qfeSelectionList    paramTraceDirList;
  int                 paramTraceDir = 1;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  scene->qfeGetSceneDisplay(&display);
  scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("Lines visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("Lines count", &param);
  if (param) param->qfeGetVisualParameterValue(paramCount);  

  visual->qfeGetVisualParameter("Lines phases dynamic", &param);
  if (param) param->qfeGetVisualParameterValue(paramPhasesDynamic);  

  visual->qfeGetVisualParameter("Lines phases static", &param);
  if (param) param->qfeGetVisualParameterValue(paramPhasesStatic);   

  visual->qfeGetVisualParameter("Lines maximum angle", &param);
  if (param) param->qfeGetVisualParameterValue(paramMaxAngle);   

  //visual->qfeGetVisualParameter("Lines interpolation", &param);
  //if (param) param->qfeGetVisualParameterValue(paramInterpolationList);
  //paramInterpolationType = paramInterpolationList.active;

  visual->qfeGetVisualParameter("Lines style", &param);
  if (param) param->qfeGetVisualParameterValue(paramStyleList);  

  visual->qfeGetVisualParameter("Lines shading", &param);
  if (param) param->qfeGetVisualParameterValue(paramShadingList);   

  visual->qfeGetVisualParameter("Lines width", &param);
  if (param) param->qfeGetVisualParameterValue(paramLineWidth);   

  visual->qfeGetVisualParameter("Lines contour", &param);
  if (param) param->qfeGetVisualParameterValue(paramContour);   

  visual->qfeGetVisualParameter("Lines halo", &param);
  if (param) param->qfeGetVisualParameterValue(paramHalo);   

  visual->qfeGetVisualParameter("Lines color scale (%)", &param);
  if (param) param->qfeGetVisualParameterValue(paramColorScale);  

  visual->qfeGetVisualParameter("Lines integration", &param);
  if (param) param->qfeGetVisualParameterValue(paramIntegrationList);  
  paramIntegrationType = paramIntegrationList.active;

  visual->qfeGetVisualParameter("Lines steps/sample", &param);
  if (param) param->qfeGetVisualParameterValue(paramIntegrationSteps);  

  visual->qfeGetVisualParameter("Lines seeding behavior", &param);
  if (param) param->qfeGetVisualParameterValue(paramSeedingBehavior);  

  visual->qfeGetVisualParameter("Lines seeding type", &param);
  if (param) param->qfeGetVisualParameterValue(paramSeedingType); 

  visualCEV->qfeGetVisualParameter("Trace direction", &param);
  if (param) param->qfeGetVisualParameterValue(paramTraceDirList);  
  paramTraceDir = paramTraceDirList.active;

  seedsCount = paramCount;

  if (!paramVisible) return qfeError;  

  // Set line trace parameters
  qfeFloat               phaseDuration;
  int                    traceDirection;
  qfeFlowSeedingBehavior seedingBehavior;

  study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);    
  if (paramTraceDir == 0)
    traceDirection = 1;
  else
    traceDirection = -1;

  switch(paramSeedingBehavior.active) {
  case 0  : seedingBehavior = qfeFlowLinesPathDynamic;
    break;
  case 1  : seedingBehavior = qfeFlowLinesPathStatic;
    break;
  default : seedingBehavior = qfeFlowLinesPathStatic;
  }    

  algorithmLineTrace.qfeSetStyle((qfeFlowLinesStyle)paramStyleList.active);
  algorithmLineTrace.qfeSetTraceSteps(paramIntegrationSteps);   
  algorithmLineTrace.qfeSetTraceScheme((qfeFlowLinesIntegrationScheme)paramIntegrationType);  
  algorithmLineTrace.qfeSetTraceDirection(traceDirection);
  algorithmLineTrace.qfeSetTracePhasesStatic(paramPhasesStatic);
  algorithmLineTrace.qfeSetTracePhasesDynamic(paramPhasesDynamic);  
  //this->algorithmLineTrace->qfeSetTraceMaximumAngle(paramMaxAngle);
  algorithmLineTrace.qfeSetPhase(current);
  algorithmLineTrace.qfeSetPhaseDuration(phaseDuration);
  algorithmLineTrace.qfeSetLineShading((qfeFlowLinesShading)paramShadingList.active);
  algorithmLineTrace.qfeSetLineWidth((float)paramLineWidth);
  algorithmLineTrace.qfeSetLineContour(paramContour);
  algorithmLineTrace.qfeSetLineHalo(paramHalo);
  algorithmLineTrace.qfeSetLightSource(light);
  algorithmLineTrace.qfeSetColorScale(paramColorScale);
  algorithmLineTrace.qfeSetSuperSampling(supersampling);

  // Update the probe seeding
  //qfeUpdateSeeds();

  // Render particles
  algorithmLineTrace.qfeRenderPathlines(colormap, seedingBehavior);    

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeRenderVisualAides(qfeVisualAides *visual, int supersampling) {
  qfeStudy           *study;
  qfeVolume          *volume;
  int                 current;

  qfeVisualParameter *param;
  bool                paramSupportBoundingBox = true;
  bool                paramSupportAxes        = false;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);

  if (!study) return qfeError;

  volume = study->pcap[current];

  visual->qfeGetVisualParameter("Support bounding box", &param);
  if (param)
    param->qfeGetVisualParameterValue(paramSupportBoundingBox); 

  visual->qfeGetVisualParameter("Support axes", &param);
  if (param)
    param->qfeGetVisualParameterValue(paramSupportAxes); 

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  if (paramSupportBoundingBox)
    qfeDriverAide::qfeRenderBoundingBoxDashed(volume, currentModelView, 0.8*frameBufferProps[0].supersampling);
  if (paramSupportAxes)
    qfeDriverAide::qfeRenderVolumeAxes(volume, 10.0, 1.6*frameBufferProps[0].supersampling);

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND); 

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeRenderVisualPlanesOrtho(qfeVisualReformat *visual) {
  qfeStudy           *study;
  qfeVolume          *volume;
  qfeGrid            *grid;
  qfeColorMap        *colormap;
  qfePoint            origin;

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  int                 paramInterp;
  double              paramBrightness;
  double              paramContrast;
  int                 paramTransparency;
  qfeFloat            paramRange[2];
  bool                paramShowX, paramShowY, paramShowZ;
  int                 current;
  qfeColorRGB         colorX, colorY, colorZ, colorQ;
  qfeFloat            tt;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  // Get visual parameters
  visual->qfeGetVisualParameter("Ortho data set", &param);
  if (param) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("Ortho data vector", &param);
  if (param) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("Ortho data representation", &param);
  if (param) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("Ortho data interpolation", &param);
  if (param) param->qfeGetVisualParameterValue(paramInterpList);  
  paramInterp = paramInterpList.active;

  visual->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowX);    

  visual->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowY);    

  visual->qfeGetVisualParameter("Ortho transparency", &param);
  if (param) param->qfeGetVisualParameterValue(paramTransparency);    

  visual->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowZ);  

  visual->qfeGetVisualParameter("Ortho contrast", &param);
  if (param) param->qfeGetVisualParameterValue(paramContrast);   

  visual->qfeGetVisualParameter("Ortho brightness", &param);
  if (param) param->qfeGetVisualParameterValue(paramBrightness);   

  // Process parameters
  qfeGetDataSetOrtho(paramData, visual, &volume);  

  if (volume) {
    volume->qfeGetVolumeGrid(&grid);
    volume->qfeGetVolumeTriggerTime(tt);
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);    
  } else {
    origin.x = origin.y = origin.z = 0.0;
  }

  switch (paramData) {
  case 0 : study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
    break;  
  case 1 : study->qfeGetStudyRangePCAM(paramRange[0], paramRange[1]);
    break;
  case 2 : study->qfeGetStudyRangeSSFP(paramRange[0], paramRange[1]);
    // Non-float texture are clamped [0,1]
    //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
    //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
    break;
  case 3:
    if (study->tmip)
      study->tmip->qfeGetVolumeValueDomain(paramRange[0], paramRange[1]);
    // Non-float texture are clamped [0,1]
    //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
    //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
    break;
  case 6:  study->qfeGetStudyRangeFTLE(paramRange[0], paramRange[1]);
    break;
  }

  // Determine line color
  colorX = colorY = colorZ = colorQ = colorDefault;
  if (selectedPlane == qfePlaneX)  colorX = colorHighlight;
  if (selectedPlane == qfePlaneY)  colorY = colorHighlight;
  if (selectedPlane == qfePlaneZ)  colorZ = colorHighlight;
  if (selectedPlane == qfePlaneQ)  colorQ = colorHighlight;

  // Update algorithm parameters    
  algorithmOrtho.qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
  algorithmOrtho.qfeSetPlaneDataComponent(paramComp);
  algorithmOrtho.qfeSetPlaneDataRepresentation(paramRepr);
  algorithmOrtho.qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  algorithmOrtho.qfeSetPlaneTriggerTime(tt); 
  algorithmOrtho.qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  algorithmOrtho.qfeSetPlaneBrightness(paramBrightness);
  algorithmOrtho.qfeSetPlaneContrast(paramContrast);
  algorithmOrtho.qfeSetPlaneOpacity(1.0-(paramTransparency/100.0));

  // Render the planes
  if (paramShowX) {
    algorithmOrtho.qfeSetPlaneBorderColor(colorX);
    algorithmOrtho.qfeRenderPolygon3D(volume, planeX, colormap);      
  }

  if (paramShowY) { 
    algorithmOrtho.qfeSetPlaneBorderColor(colorY);
    algorithmOrtho.qfeRenderPolygon3D(volume, planeY, colormap);      
  }

  if (paramShowZ) {  
    algorithmOrtho.qfeSetPlaneBorderColor(colorZ);
    algorithmOrtho.qfeRenderPolygon3D(volume, planeZ, colormap);      
  }

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeRenderGuidingPoints(bool voi)
{
  if (!voi) {
    for (unsigned i = 0; i < lvGuidingPoints.size(); i++)
      qfeRenderGuidingPoint(i, SE_Left);
    for (unsigned i = 0; i < rvGuidingPoints.size(); i++)
      qfeRenderGuidingPoint(i, SE_Right);
  } else {
    for (unsigned i = 0; i < voiGuidingPoints.size(); i++)
      qfeRenderGuidingPoint(i, SE_VoI);
  }

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeRenderGuidingPoint(unsigned index, SE_Type type)
{
  if (type == SE_Left) {
    if (index >= lvGuidingPoints.size())
      return qfeError;

    if (index + 1 == (int)lvSelectedGuidingPoint) // If rendering selected guide point
      qfeRenderCube(lvGuidingPoints[index], 1.f, qfeColorRGB(1.f, 0.f, 1.f));
    else if (lvEllipsoidSelected)
      qfeRenderCube(lvGuidingPoints[index], 1.f, qfeColorRGB(0.f, 1.f, 0.f));
    else
      qfeRenderCube(lvGuidingPoints[index], 1.f, qfeColorRGB(1.f, 0.f, 0.f));
  } else if (type == SE_Right) {
    if (index >= rvGuidingPoints.size())
      return qfeError;

    if (index + 1 == (int)rvSelectedGuidingPoint) // If rendering selected guide point
      qfeRenderCube(rvGuidingPoints[index], 1.f, qfeColorRGB(1.f, 0.f, 1.f));
    else if (rvEllipsoidSelected)
      qfeRenderCube(rvGuidingPoints[index], 1.f, qfeColorRGB(0.f, 1.f, 0.f));
    else
      qfeRenderCube(rvGuidingPoints[index], 1.f, qfeColorRGB(1.f, 0.f, 0.f));
  } else { // type == SE_VoI
    if (index >= voiGuidingPoints.size())
      return qfeError;

    if (index + 1 == (int)voiSelectedGuidingPoint) // If rendering selected guide point
      qfeRenderCube(voiGuidingPoints[index], 1.f, qfeColorRGB(1.f, 0.f, 1.f));
    else if (voiEllipsoidSelected)
      qfeRenderCube(voiGuidingPoints[index], 1.f, qfeColorRGB(0.f, 1.f, 0.f));
    else
      qfeRenderCube(voiGuidingPoints[index], 1.f, qfeColorRGB(1.f, 0.f, 0.f));
  }
  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeRenderCube(qfePoint pos, float size, qfeColorRGB color)
{
  glPushMatrix();

  glTranslatef(pos.x, pos.y, pos.z);
  glScalef(size, size, size);

  glBegin(GL_QUADS);
    glColor3f(color.r, color.g, color.b);    
    glVertex3f( 1.0f, 1.0f,-1.0f);
    glVertex3f(-1.0f, 1.0f,-1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f( 1.0f, 1.0f, 1.0f);
    glVertex3f( 1.0f,-1.0f, 1.0f);
    glVertex3f(-1.0f,-1.0f, 1.0f);
    glVertex3f(-1.0f,-1.0f,-1.0f);
    glVertex3f( 1.0f,-1.0f,-1.0f); 
    glVertex3f( 1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f,-1.0f, 1.0f);
    glVertex3f( 1.0f,-1.0f, 1.0f);  
    glVertex3f( 1.0f,-1.0f,-1.0f);
    glVertex3f(-1.0f,-1.0f,-1.0f);
    glVertex3f(-1.0f, 1.0f,-1.0f);
    glVertex3f( 1.0f, 1.0f,-1.0f); 
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f,-1.0f);
    glVertex3f(-1.0f,-1.0f,-1.0f);
    glVertex3f(-1.0f,-1.0f, 1.0f);   
    glVertex3f( 1.0f, 1.0f,-1.0f);
    glVertex3f( 1.0f, 1.0f, 1.0f);
    glVertex3f( 1.0f,-1.0f, 1.0f);
    glVertex3f( 1.0f,-1.0f,-1.0f);
  glEnd();  

  glPopMatrix();

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeRenderSeedEllipsoid(qfeVisualSeedEllipsoid *visual)
{
  bool paramEllipsoid = true;
  bool paramSurface = true;
  bool paramGuidePoints = true;
  bool paramSeedPoints = false;
  bool paramSeedOnClipPlane = false;
  int paramNrSeedPoints = 250;
  static bool previousSeedOnClipPlane = false;

  qfeVisualParameter *param;
  visual->qfeGetVisualParameter("Ellipsoid visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramEllipsoid);
  visual->qfeGetVisualParameter("Surface visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramSurface);
  visual->qfeGetVisualParameter("Guide points visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramGuidePoints);
  visual->qfeGetVisualParameter("Seed points visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramSeedPoints);
  visual->qfeGetVisualParameter("Seed on clip plane", &param);
  if (param) param->qfeGetVisualParameterValue(paramSeedOnClipPlane);

  if (paramSeedOnClipPlane != previousSeedOnClipPlane) {
    previousSeedOnClipPlane = paramSeedOnClipPlane;
    lvEllipsoid.qfeSetSeedOnClipPlane(paramSeedOnClipPlane);
    rvEllipsoid.qfeSetSeedOnClipPlane(paramSeedOnClipPlane);
    qfeGenerateEllipsoidPoints();
    qfeInjectEllipsoidSeedPoints();
  }

  if (paramEllipsoid) {
    lvEllipsoid.qfeRenderVolume(qfeColorRGB(0.f, 1.f, 0.f), paramSurface, lvEllipsoidSelected, true);
    rvEllipsoid.qfeRenderVolume(qfeColorRGB(0.f, 0.f, 1.f), paramSurface, rvEllipsoidSelected, true);
  }
  if (paramGuidePoints)
    qfeRenderGuidingPoints(false);
  if (paramSeedPoints) {
    lvEllipsoid.qfeRenderSeedPoints();
    rvEllipsoid.qfeRenderSeedPoints();
  }

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeRenderVolumeOfInterest(qfeVisualVolumeOfInterest *visual)
{
  bool paramVoI = true;
  bool paramSurface = true;
  bool paramGuidePoints = true;

  qfeVisualParameter *param;
  visual->qfeGetVisualParameter("VoI visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramVoI);
  visual->qfeGetVisualParameter("Surface visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramSurface);
  visual->qfeGetVisualParameter("Guide points visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramGuidePoints);
  visual->qfeGetVisualParameter("Clip all data", &param);
  if (param) param->qfeGetVisualParameterValue(clipAllData);

  if (clipAllData != previousClipAllData)
    qfeUpdateClipData();
  previousClipAllData = clipAllData;

  if (paramVoI)
    voiEllipsoid.qfeRenderVolume(qfeColorRGB(1.f, 0.f, 0.f), paramSurface, voiEllipsoidSelected, false);
  if (paramGuidePoints)
    qfeRenderGuidingPoints(true);

  return qfeSuccess;
}

void qfeDriverCEV::qfeUpdateClipData()
{
  qfeStudy *study;
  unsigned w, h, d;
  visualCEV->qfeGetVisualStudy(&study);
  if (study->pcap.size() <= 0)
    return;

  if (clipAllData) {
    study->pcap[0]->qfeGetVolumeSize(w, h, d);

    float *mask = new float[w*h*d];
    qfeCreateClipMask(mask);

    for (unsigned i = 0; i < study->pcap.size(); i++) {
      if (flowSpeedCopy.size() > 0) qfeClipVolume(flowSpeedCopy[i], study->flowSpeed[i], mask);
      if (pcamCopy.size() > 0) qfeClipVolume(pcamCopy[i], study->pcam[i], mask);
      if (curlCopy.size() > 0) qfeClipVolume(curlCopy[i], study->curl[i], mask);
      if (lambda2Copy.size() > 0) qfeClipVolume(lambda2Copy[i], study->lambda2[i], mask);
      if (divergenceCopy.size() > 0) qfeClipVolume(divergenceCopy[i], study->divergence[i], mask);
    }
    if (tmipCopy) qfeClipVolume(tmipCopy, study->tmip, mask);

    delete[] mask;
  } else {
    qfeCopyVolumeSeriesBack();
  }
}

void qfeDriverCEV::qfeCreateClipMask(float *mask)
{
  qfeStudy *study;
  unsigned w, h, d;
  qfeMatrix4f V2P;
  visualCEV->qfeGetVisualStudy(&study);
  study->pcap[0]->qfeGetVolumeSize(w, h, d);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, study->pcap[0]);

  for (unsigned z = 0; z < d; z++) {
    for (unsigned y = 0; y < h; y++) {
      for (unsigned x = 0; x < w; x++) {
        qfePoint patientCoord = qfePoint(x, y, z) * V2P;
        unsigned index = z*w*h + y*w + x;
        if (voiEllipsoid.IsInVolume(patientCoord))
          mask[index] = 1.f;
        else
          mask[index] = 0.f;
      }
    }
  }
}

void qfeDriverCEV::qfeClipVolume(qfeVolume *original, qfeVolume *toClip, float *mask)
{
  qfeValueType t;
  unsigned w, h, d;
  void *originalData;
  void *toClipData;
  original->qfeGetVolumeData(t, w, h, d, &originalData);
  toClip->qfeGetVolumeData(t, w, h, d, &toClipData);

  float min = 1e30f;
  float max = -1e30f;
  for (unsigned z = 0; z < d; z++) {
    for (unsigned y = 0; y < h; y++) {
      for (unsigned x = 0; x < w; x++) {
        unsigned index = z*w*h + y*w + x;
        float value = qfeGetValue(originalData, t, index, 1) * mask[index];
        if (value < min) min = value;
        if (value > max) max = value;
        qfeAssignValue(toClipData, t, index, value);
      }
    }
  }

  toClip->qfeSetVolumeValueDomain(min, max);
  toClip->qfeUploadVolume();
}

qfeReturnStatus qfeDriverCEV::qfeRenderPlaneOrthoView(qfeVisualPlaneOrthoView *visual, qfeVisualReformat *visMPR)
{
  qfeStudy *study;
  qfeColorMap *colormap;
  int current;
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  int                 paramInterp;
  double              paramBrightness;
  double              paramContrast;
  int                 paramTransparency;

  // Get visual parameters from multi-planar reformat
  visMPR->qfeGetVisualParameter("Ortho data set", &param);
  if (param) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;
  visMPR->qfeGetVisualParameter("Ortho data vector", &param);
  if (param) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;
  visMPR->qfeGetVisualParameter("Ortho data representation", &param);
  if (param) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;
  visMPR->qfeGetVisualParameter("Ortho data interpolation", &param);
  if (param) param->qfeGetVisualParameterValue(paramInterpList);  
  paramInterp = paramInterpList.active;
  visMPR->qfeGetVisualParameter("Ortho transparency", &param);
  if (param) param->qfeGetVisualParameterValue(paramTransparency);    
  visMPR->qfeGetVisualParameter("Ortho contrast", &param);
  if (param) param->qfeGetVisualParameterValue(paramContrast);   
  visMPR->qfeGetVisualParameter("Ortho brightness", &param);
  if (param) param->qfeGetVisualParameterValue(paramBrightness);   

  qfeVolume *volume;
  qfeGetDataSetOrtho(paramData, visMPR, &volume);  

  bool paramShow, paramLock;
  visual->qfeGetVisualParameter("Show view orthogonal plane", &param);
  if (param) param->qfeGetVisualParameterValue(paramShow);
  visual->qfeGetVisualParameter("Lock plane", &param);
  if (param) param->qfeGetVisualParameterValue(paramLock);

  qfeColorRGB colorQ = colorDefault;
  if (selectedPlane == qfePlaneQ)
    colorQ = colorHighlight;

  qfeGrid *grid;
  float tt;
  qfePoint origin;
  if (volume) {
    volume->qfeGetVolumeGrid(&grid);
    volume->qfeGetVolumeTriggerTime(tt);   
  }

  float paramRange[2];
  switch (paramData) {
  case 0 : study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
    break;  
  case 1 : study->qfeGetStudyRangePCAM(paramRange[0], paramRange[1]);
    break;
  case 2 : study->qfeGetStudyRangeSSFP(paramRange[0], paramRange[1]);
    // Non-float texture are clamped [0,1]
    //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
    //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
    break;
  case 3:
    if (study->tmip)
      study->tmip->qfeGetVolumeValueDomain(paramRange[0], paramRange[1]);
    // Non-float texture are clamped [0,1]
    //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
    //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
    break;
  case 6:  study->qfeGetStudyRangeFTLE(paramRange[0], paramRange[1]);
    break;
  }

  algorithmOrtho.qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
  algorithmOrtho.qfeSetPlaneDataComponent(paramComp);
  algorithmOrtho.qfeSetPlaneDataRepresentation(paramRepr);
  algorithmOrtho.qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  algorithmOrtho.qfeSetPlaneTriggerTime(tt); 
  algorithmOrtho.qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  algorithmOrtho.qfeSetPlaneBrightness(paramBrightness);
  algorithmOrtho.qfeSetPlaneContrast(paramContrast);
  algorithmOrtho.qfeSetPlaneOpacity(1.0-(paramTransparency/100.0));

  if (paramShow) {
    if (!paramLock)
      qfeGetPlaneViewOrthogonal(volume, &planeQ);
    algorithmOrtho.qfeSetPlaneBorderColor(colorQ);
    algorithmOrtho.qfeRenderPolygon3D(volume, planeQ, colormap);
  }

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeRenderProbabilityDVR(qfeVisualProbabilityDVR *visual, qfeVisualVolumeOfInterest *voiVisual)
{
  if (!visual)
    return qfeError;

  qfeVisualParameter *param;
  bool                paramVisible;
  bool                paramDepthToneMapping;
  double              paramDepthToneMappingFactor;
  bool                paramVoIClipping;

  qfeStudy       *study;
  int             current;
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  // Fetch the parameters
  visual->qfeGetVisualParameter("Probability visible", &param);
  if (param) param->qfeGetVisualParameterValue(paramVisible);

  if (!paramVisible)
    return qfeError;

  visual->qfeGetVisualParameter("Depth Tone Mapping", &param);
  if (param) param->qfeGetVisualParameterValue(paramDepthToneMapping);

  visual->qfeGetVisualParameter("Depth Tone Mapping factor", &param);
  if (param) param->qfeGetVisualParameterValue(paramDepthToneMappingFactor);

  voiVisual->qfeGetVisualParameter("Clip probability visualization", &param);
  if (param) param->qfeGetVisualParameterValue(paramVoIClipping);

  QTransFunc tf0 = scene->qfeGetSceneFeatureTF(0);
  QTransFunc tf1 = scene->qfeGetSceneFeatureTF(1);
  int dataType0 = scene->qfeGetSceneFeatureDataType(0);
  int dataType1 = scene->qfeGetSceneFeatureDataType(1);
  float range0[2];
  float range1[2];
  qfeVolume *volume0 = qfeGetVolumeForDataType(dataType0, current, range0[0], range0[1]);
  qfeVolume *volume1 = qfeGetVolumeForDataType(dataType1, current, range1[0], range1[1]);

  if (!volume0 && !volume1)
    return qfeError;  

  // Update algorithm parameters
  algorithmPDVR.qfeSetRange0(range0[0], range0[1]);
  algorithmPDVR.qfeSetRange1(range1[0], range1[1]);
  algorithmPDVR.qfeSetDepthToneMapping(paramDepthToneMapping);
  algorithmPDVR.qfeSetDepthToneMappingFactor(paramDepthToneMappingFactor);
  algorithmPDVR.qfeSetVoITInv(voiEllipsoid.qfeGetTInverseMatrix());
  algorithmPDVR.qfeSetVoICenter(voiEllipsoid.qfeGetCenter());
  algorithmPDVR.qfeSetClipVoI(paramVoIClipping);

  algorithmPDVR.qfeRenderRaycasting(frameBufferProps[0].texColor, frameBufferProps[0].texDepth, volume0, volume1, tf0, tf1);

  glBindTexture(GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverCEV::qfeGetDataSet(qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu, int contextIndex) {
  qfeStudy           *study;
  int                 current;

  *volume = NULL;
  vl = vu = 0.0;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  if (!study) return qfeError;

  if (contextIndex == 0) {
    if ((int)study->pcam.size() > 0 && (int)study->pcam.size() > time) {
      *volume = study->pcam[time];
      study->qfeGetStudyRangePCAM(vl, vu);
    }
  } else if (contextIndex == 1) {
    if (study->tmip != nullptr) {
      *volume = study->tmip;
      (*volume)->qfeGetVolumeValueDomain(vl, vu);
    }
  }

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeGetDataSetOrtho(int index, qfeVisual *visual, qfeVolume **volume) {
  qfeStudy           *study;  
  int                 current;

  *volume = NULL;
  visual->qfeGetVisualStudy(&study); 
  visual->qfeGetVisualActiveVolume(current);

  if (!study) return qfeError;

  switch (index) {
  case(0) : // PCA-P
    if ((int)study->pcap.size() > 0 && (int)study->pcap.size() > current) 
      *volume = study->pcap[current];
    break;
  case(1) : // PCA-M
    if ((int)study->pcam.size() > 0 && (int)study->pcam.size() > current) 
      *volume = study->pcam[current];
    break;
  case(2) : // SSFP
    if ((int)study->ssfp3D.size() > 0 && (int)study->ssfp3D.size() > current)
      *volume = study->ssfp3D[current];
    break;
  case(3) : // TMIP
    if ((int)study->tmip != NULL) 
      *volume = study->tmip;
    break;
  case(4) : // FFE
    break;
  case(5) : // Clusters
    break;
  case(6) : // ftle
    if ((int)study->ftle.size() > 0 && (int)study->ftle.size() > current)
      *volume = study->ftle[current];
    break;
  }

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeGetPlaneOrtho(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice) {
  qfeMatrix4f P2V, V2P;
  qfeVector   axisX, axisY, axisZ, axisXabs, axisYabs, axisZabs, x, y, z;
  qfePoint    o;
  qfeGrid    *grid;

  if (!volume) return qfeError;

  // Get the patient to voxel transformation
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);

  // Set the plane normal direction in patient coordinates
  switch (dir) {
  case qfePlaneAxial :
    axisX.qfeSetVectorElements(1.0,0.0,0.0);
    axisY.qfeSetVectorElements(0.0,1.0,0.0);
    axisZ.qfeSetVectorElements(0.0,0.0,1.0);                              
    break;
  case qfePlaneSagittal :
    axisX.qfeSetVectorElements(0.0,0.0,1.0);
    axisY.qfeSetVectorElements(0.0,1.0,0.0);
    axisZ.qfeSetVectorElements(1.0,0.0,0.0); 
    break;
  case qfePlaneCoronal :
    axisX.qfeSetVectorElements(1.0,0.0,0.0);
    axisY.qfeSetVectorElements(0.0,0.0,1.0);
    axisZ.qfeSetVectorElements(0.0,1.0,0.0); 
    break;
  }  

  // Convert the axis to voxel coordinates and find dominant direction
  axisX = axisX*P2V;
  axisY = axisY*P2V;
  axisZ = axisZ*P2V;

  axisXabs.qfeSetVectorElements(abs(axisX.x), abs(axisX.y), abs(axisX.z));
  axisYabs.qfeSetVectorElements(abs(axisY.x), abs(axisY.y), abs(axisY.z));
  axisZabs.qfeSetVectorElements(abs(axisZ.x), abs(axisZ.y), abs(axisZ.z));

  if (axisZabs.x == max(max(axisZabs.x, axisYabs.x), axisXabs.x)) {
    x.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
    y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
    z.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
  }
  if (axisZabs.y == max(max(axisZabs.y, axisYabs.y), axisXabs.y)) {
    x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
    y.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
    z.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
  }
  if (axisZabs.z == max(max(axisZabs.z, axisYabs.z), axisXabs.z)) {
    x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
    y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
    z.qfeSetVectorElements( 0.0f,  0.0f,  1.0f);    
  }

  // Convert back to patient coordinates
  x = x*V2P;
  y = y*V2P;
  z = z*V2P;

  // Get the plane origin
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridOrigin(o.x, o.y, o.z);

  // Build the plane
  (*slice)->qfeSetFrameExtent(1,1);
  (*slice)->qfeSetFrameOrigin(o.x, o.y, o.z);
  (*slice)->qfeSetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeGetPlaneViewOrthogonal(qfeVolume *volume, qfeFrameSlice **slice)
{
  qfeGrid * grid;
  double upVec[3];
  engine->GetRenderer()->GetActiveCamera()->GetViewUp(upVec);
  qfeVector up(upVec[0], upVec[1], upVec[2]);

  if (volume == NULL) return qfeError;

  unsigned int s[3];
  qfePoint  o, e;
  qfeVector x, y, z;    

  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(s[0], s[1], s[2]);
  grid->qfeGetGridAxes(x.x,x.y,x.z, y.x,y.y,y.z, z.x,z.y,z.z);
  grid->qfeGetGridExtent(e.x, e.y, e.z);

  qfeVector lookDir = currentModelView * qfeVector(0.f, 0.f, -1.f);
  qfeVector::qfeVectorNormalize(lookDir);
  qfeVector::qfeVectorNormalize(up);
  z = -lookDir;
  qfeVector::qfeVectorCross(up, z, x);
  qfeVector::qfeVectorNormalize(x);
  qfeVector::qfeVectorCross(z, x, y);
  qfeVector::qfeVectorNormalize(y);

  (*slice)->qfeSetFrameAxes(x.x,x.y,x.z, y.x,y.y,y.z, z.x,z.y,z.z);
  (*slice)->qfeSetFrameExtent(s[0]*e.x, s[1]*e.y);

  return qfeSuccess;
}

void qfeDriverCEV::qfeMovePlaneForward(qfeFrameSlice **slice) {
  qfePoint     origin;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;  

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin + (1.0*normal);
  else
    origin = origin - (1.0*normal);

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

void qfeDriverCEV::qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm) {
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if (!volume) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin + (mm*normal);
  else
    origin = origin - (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = originTex.x > delta     ? originTex.x : delta;
  originTex.x = originTex.x < 1.0-delta ? originTex.x : 1.0-delta;
  originTex.y = originTex.y > delta     ? originTex.y : delta;
  originTex.y = originTex.y < 1.0-delta ? originTex.y : 1.0-delta;
  originTex.z = originTex.z > delta     ? originTex.z : delta;
  originTex.z = originTex.z < 1.0-delta ? originTex.z : 1.0-delta;

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

void qfeDriverCEV::qfeMovePlaneBackward(qfeFrameSlice **slice) {
  qfePoint     origin;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;  

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin - (1.0*normal);
  else
    origin = origin + (1.0*normal);

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

void qfeDriverCEV::qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm) {
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if (!volume) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin - (mm*normal);
  else
    origin = origin + (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = originTex.x > delta     ? originTex.x : delta;
  originTex.x = originTex.x < 1.0-delta ? originTex.x : 1.0-delta;
  originTex.y = originTex.y > delta     ? originTex.y : delta;
  originTex.y = originTex.y < 1.0-delta ? originTex.y : 1.0-delta;
  originTex.z = originTex.z > delta     ? originTex.z : delta;
  originTex.z = originTex.z < 1.0-delta ? originTex.z : 1.0-delta;

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

qfeReturnStatus qfeDriverCEV::qfeInjectSeeds(qfeVisualFlowParticleTrace *visual) {
  qfeStudy *study;
  float current;
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current); 

  qfeGrid *grid;
  study->pcap[0]->qfeGetVolumeGrid(&grid);
  float ox, oy, oz;
  grid->qfeGetGridOrigin(ox, oy, oz);

  qfeSeeds seeds;
  seeds.push_back(qfePoint(ox, oy, oz));
  seeds.push_back(qfePoint(ox + 10.f, oy, oz));
  seeds.push_back(qfePoint(ox, oy + 10.f, oz));
  seeds.push_back(qfePoint(ox, oy, oz + 10.f));
  seeds.push_back(qfePoint(ox - 10.f, oy, oz));
  seeds.push_back(qfePoint(ox, oy - 10.f, oz));
  seeds.push_back(qfePoint(ox, oy, oz - 10.f));

  algorithmParticleTrace.qfeAddSeedPositions(seeds); 

  seedingStateParticles = probeSeedingInjected;

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeInjectSeeds(qfeVisualFlowLineTrace *visual) {
  qfeStudy *study;
  float current;
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current); 

  qfeGrid *grid;
  study->pcap[0]->qfeGetVolumeGrid(&grid);
  float ox, oy, oz;
  grid->qfeGetGridOrigin(ox, oy, oz);

  qfeSeeds seeds;
  seeds.push_back(qfePoint(ox, oy, oz));
  seeds.push_back(qfePoint(ox + 10.f, oy, oz));
  seeds.push_back(qfePoint(ox, oy + 10.f, oz));
  seeds.push_back(qfePoint(ox, oy, oz + 10.f));
  seeds.push_back(qfePoint(ox - 10.f, oy, oz));
  seeds.push_back(qfePoint(ox, oy - 10.f, oz));
  seeds.push_back(qfePoint(ox, oy, oz - 10.f));

  algorithmLineTrace.qfeAddSeedPositions(seeds); 
  algorithmLineTrace.qfeGeneratePathlines(study->pcap, current, qfeFlowLinesPathDynamic);

  seedingStateLines = probeSeedingInjected;

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeClearSeeds(qfeVisualFlowParticleTrace *visual) {
  algorithmLineTrace.qfeClearSeedPositions();

  seedingStateParticles = probeSeedingNone;

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeClearSeeds(qfeVisualFlowLineTrace *visual) {
  algorithmLineTrace.qfeClearSeedPositions();

  seedingStateLines = probeSeedingNone;

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeUpdateSeeds() {
  if (visParticleTrace && (seedingStateParticles == probeSeedingUpdate)) { 
    qfeClearSeeds(visParticleTrace);
    qfeInjectSeeds(visParticleTrace);
    seedingStateParticles = probeSeedingInjected;    
  } 

  if (visLineTrace && (seedingStateLines == probeSeedingUpdate)) { 
    qfeClearSeeds(visLineTrace);
    qfeInjectSeeds(visLineTrace);    
    seedingStateLines = probeSeedingInjected;   
  }

  return qfeSuccess;
}

void qfeDriverCEV::qfeClickToRay(int x, int y, qfePoint &start, qfePoint &end) {
  qfeViewport *vp3D;
  engine->GetViewport3D(&vp3D);

  // Take window coordinates of mouseclick on near plane, unproject to patient coordinates
  // Then do the same for the far plane
  qfeTransform::qfeGetCoordinateUnproject(qfePoint((float)x, (float)y, 0.f), start, currentModelView, currentProjection, vp3D);
  qfeTransform::qfeGetCoordinateUnproject(qfePoint((float)x, (float)y, 1.f), end, currentModelView, currentProjection, vp3D);
}

void qfeDriverCEV::qfePlaceGuidingPoint(int x, int y, SE_Type type)
{
  qfePoint p;
  if (!qfeGetPointOnSlices(x, y, p))
    return;

  int state;
  if (type == SE_Left) {
    lvGuidingPoints.push_back(p);
    state = (int)lvGuidingPointState;
  } else if (type == SE_Right) {
    rvGuidingPoints.push_back(p);
    state = (int)rvGuidingPointState;
  } else { // type == SE_VoI
    voiGuidingPoints.push_back(p);
    state = (int)voiGuidingPointState;
  }

  // Move to next state
  state++;
  if (state > 3) {
    if (type == SE_Left) {
      lvEllipsoid.qfeSetGuidePoints(lvGuidingPoints[0], lvGuidingPoints[1], lvGuidingPoints[2]);
      lvEllipsoidSelected = true;
      qfeGenerateEllipsoidPoints();
      qfeInjectEllipsoidSeedPoints();
    } else if (type == SE_Right) {
      rvEllipsoid.qfeSetGuidePoints(rvGuidingPoints[0], rvGuidingPoints[1], rvGuidingPoints[2]);
      rvEllipsoidSelected = true;
      qfeGenerateEllipsoidPoints();
      qfeInjectEllipsoidSeedPoints();
    } else { // type == SE_VoI
      voiEllipsoid.qfeSetGuidePoints(voiGuidingPoints[0], voiGuidingPoints[1], voiGuidingPoints[2]);
      voiEllipsoidSelected = true;
    }
    state = 0;
  }
  if (type == SE_Left)
    lvGuidingPointState = (qfeGuidingPointState)state;
  else if (type == SE_Right)
    rvGuidingPointState = (qfeGuidingPointState)state;
  else // type == SE_VoI
    voiGuidingPointState = (qfeGuidingPointState)state;
}

void qfeDriverCEV::qfeMoveGuidingPoint(unsigned index, int x, int y, SE_Type type)
{
  qfePoint p;
  if (!qfeGetPointOnSlices(x, y, p))
    return;

  if (type == SE_Left) {
    lvGuidingPoints[index] = p;
    lvEllipsoid.qfeSetGuidePoints(lvGuidingPoints[0], lvGuidingPoints[1], lvGuidingPoints[2]);
  } else if (type == SE_Right) {
    rvGuidingPoints[index] = p;
    rvEllipsoid.qfeSetGuidePoints(rvGuidingPoints[0], rvGuidingPoints[1], rvGuidingPoints[2]);
  } else { // type == SE_VoI
    voiGuidingPoints[index] = p;
    voiEllipsoid.qfeSetGuidePoints(voiGuidingPoints[0], voiGuidingPoints[1], voiGuidingPoints[2]);
  }
}

void qfeDriverCEV::qfeInjectEllipsoidSeedPoints()
{
  vector<qfePoint> &lvSeedPoints = lvEllipsoid.qfeGetSeedPoints();
  vector<qfePoint> &rvSeedPoints = rvEllipsoid.qfeGetSeedPoints();

  qfeStudy *study;
  float current;
  visualCEV->qfeGetVisualStudy(&study);
  visualCEV->qfeGetVisualActiveVolume(current);

  bool continuous;
  qfeVisualParameter *param;
  visSE->qfeGetVisualParameter("Continuous seeding", &param);
  if (param) param->qfeGetVisualParameterValue(continuous);

  if (continuous) {
    algorithmParticleTrace.qfeClearSeedPositions();
    for (unsigned i = 0; i < study->pcap.size(); i++) {
      qfeGenerateEllipsoidPoints();
      lvSeedPoints = lvEllipsoid.qfeGetSeedPoints();
      rvSeedPoints = rvEllipsoid.qfeGetSeedPoints();
      algorithmParticleTrace.qfeAddSeedPositions(lvSeedPoints, (float)i);
      algorithmParticleTrace.qfeAddSeedPositions(rvSeedPoints, (float)i);
    }
  }
  else {
    algorithmParticleTrace.qfeClearSeedPositions();
    algorithmParticleTrace.qfeAddSeedPositions(lvSeedPoints, current);
    algorithmParticleTrace.qfeAddSeedPositions(rvSeedPoints, current);
  }

  algorithmLineTrace.qfeClearSeedPositions();
  algorithmLineTrace.qfeAddSeedPositions(lvSeedPoints, current);
  algorithmLineTrace.qfeAddSeedPositions(rvSeedPoints, current); 
  algorithmLineTrace.qfeGeneratePathlines(study->pcap, current, qfeFlowLinesPathDynamic);
}

void qfeDriverCEV::qfeGenerateEllipsoidPoints()
{
  int nrOfPointsLV = 100;
  int nrOfPointsRV = 100;
  bool seedLV = true;
  bool seedRV = true;
  bool subtract = true;
  qfeVisualParameter *param;
  visSE->qfeGetVisualParameter("Number of seed points LV", &param);
  if (param) param->qfeGetVisualParameterValue(nrOfPointsLV);
  visSE->qfeGetVisualParameter("Number of seed points RV", &param);
  if (param) param->qfeGetVisualParameterValue(nrOfPointsRV);
  visSE->qfeGetVisualParameter("Seed left ventricle", &param);
  if (param) param->qfeGetVisualParameterValue(seedLV);
  visSE->qfeGetVisualParameter("Seed right ventricle", &param);
  if (param) param->qfeGetVisualParameterValue(seedRV);
  visSE->qfeGetVisualParameter("Subtract LVE from RVE", &param);
  if (param) param->qfeGetVisualParameterValue(subtract);


  if (lvEllipsoid.qfeIsPlaced() && seedLV)
    lvEllipsoid.qfeGenerateSeedPoints(nrOfPointsLV);
  if (!seedLV)
    lvEllipsoid.qfeClearSeedPoints();
  if (rvEllipsoid.qfeIsPlaced() && lvEllipsoid.qfeIsPlaced() && seedRV && subtract)
    rvEllipsoid.qfeGenerateSeedPointsNotIn(nrOfPointsRV, &lvEllipsoid);
  else if (rvEllipsoid.qfeIsPlaced() && seedRV)
    rvEllipsoid.qfeGenerateSeedPoints(nrOfPointsRV);
  if (!seedRV)
    rvEllipsoid.qfeClearSeedPoints();
}

bool qfeDriverCEV::qfeGetRaySliceIntersection(qfePoint rayStart, qfePoint rayEnd, qfeFrameSlice *slice, qfePoint &intersection, float &distance)
{
  // Define slice as a rectangle by point P0 and sides S1, S2, with normal N
  qfePoint *sliceCorners;
  qfeGetSliceCorners(slice, &sliceCorners);
  qfePoint P0 = sliceCorners[0];
  qfeVector S1 = sliceCorners[1] - P0;
  qfeVector S2 = sliceCorners[3] - P0;
  qfeVector N;
  qfeVector::qfeVectorCross(S1, S2, N);
  qfeVector::qfeVectorNormalize(N);

  // Define ray as R0 + t*D
  qfePoint R0 = rayStart;
  qfeVector D = rayEnd - rayStart;
  qfeVector::qfeVectorNormalize(D);

  float DdotN;
  qfeVector::qfeVectorDot(D, N, DdotN);
  if (DdotN == 0.f) // If looking directly at the side of the slice
    return false;

  float P0minR0dotN;
  qfeVector::qfeVectorDot(P0 - R0, N, P0minR0dotN);
  float a = P0minR0dotN / DdotN;

  qfePoint P = R0 + a * D;; // Intersection point between ray and plane in which slice resides

  // Q1 and Q2 are projections of P - P0 onto S1 and S2 respectively
  qfeVector P0P = P - P0;
  qfeVector S1U = S1;
  qfeVector S2U = S2;
  qfeVector::qfeVectorNormalize(S1U);
  qfeVector::qfeVectorNormalize(S2U);
  float q1, q2;
  qfeVector::qfeVectorDot(P0P, S1U, q1);
  qfeVector::qfeVectorDot(P0P, S2U, q2);
  qfeVector Q1 = S1U * q1;
  qfeVector Q2 = S2U * q2;

  // Check if ray-plane intersection is inside rectangle
  float ls1, ls2, lq1, lq2;
  qfeVector::qfeVectorLength(S1, ls1);
  qfeVector::qfeVectorLength(S2, ls2);
  qfeVector::qfeVectorLength(Q1, lq1);
  qfeVector::qfeVectorLength(Q2, lq2);
  if (lq1 <= ls1 && lq2 <= ls2) {
    intersection = P;
    distance = a;
    return true;
  }

  return false;
}

bool qfeDriverCEV::qfeGetPointOnSlices(int x, int y, qfePoint &p)
{
  // Convert mouse click to ray
  qfePoint rayStart, rayEnd;
  qfeClickToRay(x, y, rayStart, rayEnd);

  // Determine slices to check
  qfeVisualParameter *param;  
  bool                paramShowX, paramShowY, paramShowZ, paramShowQ;
  visOrtho->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowX);    
  visOrtho->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowY);    
  visOrtho->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowZ);   
  visPOV->qfeGetVisualParameter("Show view orthogonal plane", &param);
  if (param) param->qfeGetVisualParameterValue(paramShowQ);

  // Find closest slice intersection
  qfePoint intersection, closestIntersection;
  float distance;
  float smallestDistance = 1e30f;
  if (paramShowX && qfeGetRaySliceIntersection(rayStart, rayEnd, planeX, intersection, distance) && distance < smallestDistance) {
    smallestDistance = distance;
    closestIntersection = intersection;
  }
  if (paramShowY && qfeGetRaySliceIntersection(rayStart, rayEnd, planeY, intersection, distance) && distance < smallestDistance) {
    smallestDistance = distance;
    closestIntersection = intersection;
  }
  if (paramShowZ && qfeGetRaySliceIntersection(rayStart, rayEnd, planeZ, intersection, distance) && distance < smallestDistance) {
    smallestDistance = distance;
    closestIntersection = intersection;
  }
  if (paramShowQ && qfeGetRaySliceIntersection(rayStart, rayEnd, planeQ, intersection, distance) && distance < smallestDistance) {
    smallestDistance = distance;
    closestIntersection = intersection;
  }

  // If there was no intersection
  if (smallestDistance == 1e30f)
    return false;

  p = closestIntersection;
  return true;
}

void qfeDriverCEV::qfeGetSliceCorners(qfeFrameSlice *slice, qfePoint **corners)
{
  qfeVolume *volume;
  int paramData;
  qfeVisualParameter *param;
  qfeSelectionList paramDataList;
  visOrtho->qfeGetVisualParameter("Ortho data set", &param);
  if (param) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;
  qfeGetDataSetOrtho(paramData, visOrtho, &volume);

  int nrVertices;
  algorithmOrtho.qfeComputePolygon(volume, slice, corners, nrVertices);
}

void qfeDriverCEV::qfeCreateTextures(qfeViewport *vp, int supersampling) {
  glGenTextures(nrFrames, texGradients);

  qfeStudy *study;
  visualCEV->qfeGetVisualStudy(&study);

  if (study->pcam.size() > 0) {
    for (unsigned i = 0; i < nrFrames; i++) {
      glBindTexture(GL_TEXTURE_3D, texGradients[i]);
      glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB32F, width, height, depth, 0, GL_RGB, GL_FLOAT, gradients[i]);
    }
  }

  if (tMipAvailable) {
    glGenTextures(1, &texTMipGradients);
    glBindTexture(GL_TEXTURE_3D, texTMipGradients);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB32F, width, height, depth, 0, GL_RGB, GL_FLOAT, tMipGradients);
  }

  if (supersampling <= 0) return;

  glGenTextures(1, &intersectTexColor);
  glBindTexture(GL_TEXTURE_2D, intersectTexColor);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F,
    vp->width*supersampling, vp->height*supersampling, 0, GL_BGRA, GL_FLOAT, NULL);

  glGenTextures(1, &intersectTexDepth);
  glBindTexture(GL_TEXTURE_2D, intersectTexDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 
    vp->width*supersampling, vp->height*supersampling, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 0);

  algorithmIVR.qfeSetIntersectionBuffers(intersectTexColor, intersectTexDepth);

  glBindTexture(GL_TEXTURE_3D, 0);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void qfeDriverCEV::qfeDeleteTextures() {
  for (unsigned i = 0; i < nrFrames; i++) {
    if (glIsTexture(texGradients[i]))
      glDeleteTextures(1, &texGradients[i]);
  }
  if (glIsTexture(texTMipGradients))
    glDeleteTextures(1, &texTMipGradients);
  if (glIsTexture(intersectTexColor))
    glDeleteTextures(1, &intersectTexColor);
  if (glIsTexture(intersectTexDepth))
    glDeleteTextures(1, &intersectTexDepth);
}

void qfeDriverCEV::qfePlaceFeatureBasedSeedPoints(int currentPhase)
{
  algorithmLineTrace.qfeClearSeedPositions();
  algorithmParticleTrace.qfeClearSeedPositions();
  featureSeedPoints.clear();

  QVector<QPair<double, double>> tf0 = scene->qfeGetSceneFeatureTF(0);
  QVector<QPair<double, double>> tf1 = scene->qfeGetSceneFeatureTF(1);
  int dataType0 = scene->qfeGetSceneFeatureDataType(0);
  int dataType1 = scene->qfeGetSceneFeatureDataType(1);

  double baseP;
  qfeVisualParameter *param;
  visFS->qfeGetVisualParameter("Base probability", &param);
  if (param) param->qfeGetVisualParameterValue(baseP);

  float range0[2];
  float range1[2];
  qfeVolume *vol0 = qfeGetVolumeForDataType(dataType0, currentPhase, range0[0], range0[1]);
  qfeVolume *vol1 = qfeGetVolumeForDataType(dataType1, currentPhase, range1[0], range1[1]);

  if (!qfePrepareProbabilities(vol0, vol1, tf0, tf1))
    return;

  qfeCalculateProbabilities(vol0, tf0, 0, range0[0], range0[1]);
  qfeCalculateProbabilities(vol1, tf1, 1, range1[0], range1[1]);

  unsigned w, h, d;
  qfeMatrix4f V2P;
  if (vol0) {
    vol0->qfeGetVolumeSize(w, h, d);
    qfeTransform::qfeGetMatrixVoxelToPatient(V2P, vol0);
  }
  if (vol1) {
    vol1->qfeGetVolumeSize(w, h, d);
    qfeTransform::qfeGetMatrixVoxelToPatient(V2P, vol1);
  }

  for (unsigned z = 0; z < d; z++) {
    for (unsigned y = 0; y < h; y++) {
      for (unsigned x = 0; x < w; x++) {
        unsigned index = qfeGetIndex(w, h, x, y, z);

        float p = baseP;
        if (vol0) p *= probabilities0[index];
        if (vol1) p *= probabilities1[index];

        float r = GetRandomFloat(0.00f, 1.f);
        if (r < p) {
          qfePoint pos((float)x, (float)y, (float)z);
          pos = pos*V2P;
          featureSeedPoints.push_back(pos);
        }
      }
    }
  }

  qfeStudy *study;
  float current;
  visualCEV->qfeGetVisualStudy(&study);
  visualCEV->qfeGetVisualActiveVolume(current);
  algorithmParticleTrace.qfeAddSeedPositions(featureSeedPoints, current);
  algorithmLineTrace.qfeAddSeedPositions(featureSeedPoints, current);
  algorithmLineTrace.qfeGeneratePathlines(study->pcap, current, qfeFlowLinesPathDynamic);
}

qfeVolume *qfeDriverCEV::qfeGetVolumeForDataType(int dataType, int currentPhase, float &rangeMin, float &rangeMax)
{
  qfeStudy *study;
  visFS->qfeGetVisualStudy(&study);
  qfeVolumeSeries *series;

  rangeMin = 1e30f;
  rangeMax = -1e30f;

  switch (dataType) {
  case 0: // None
    series = nullptr; break;
  case 1: // Flow speed (PCA-P)
    series = &study->flowSpeed; break;
  case 2: // Magnitude (PCA-M)
    series = &study->pcam; break;
  case 3: // Curl
    series = &study->curl; break;
  //case 4: // FTLE
  //  return study->ftle.at(currentPhase); break;
  case 4: // Lambda2
    series = &study->lambda2; break;
  case 5: // Divergence
    series = &study->divergence; break;
  case 6: // Q-Criterion
    series = &study->qcriterion; break;
  }

  if (!series)
    return nullptr;

  for (unsigned i = 0; i < series->size(); i++) {
    float rmin, rmax;
    series->at(i)->qfeGetVolumeValueDomain(rmin, rmax);
    if (rmin < rangeMin) rangeMin = rmin;
    if (rmax > rangeMax) rangeMax = rmax;
  }

  return series->at(currentPhase);
}

unsigned qfeDriverCEV::qfeGetIndex(unsigned width, unsigned height, unsigned x, unsigned y, unsigned z)
{
  return z*width*height + y*width + x;
}

void qfeDriverCEV::qfeResizeProbabilities(qfeVolume *vol, int id)
{
  if (id == 0) {
    unsigned w, h, d;
    vol->qfeGetVolumeSize(w, h, d);
    if (volumeSize0 != w*h*d) {
      if (volumeSize0 != 0)
        delete probabilities0;
      volumeSize0 = w*h*d;
      probabilities0 = new float[volumeSize0];
    }
  } else {
    unsigned w, h, d;
    vol->qfeGetVolumeSize(w, h, d);
    if (volumeSize1 != w*h*d) {
      if (volumeSize1 != 0)
        delete probabilities1;
      volumeSize1 = w*h*d;
      probabilities1 = new float[volumeSize1];
    }
  }
}

bool qfeDriverCEV::qfePrepareProbabilities(qfeVolume *vol0, qfeVolume *vol1, QVector<QPair<double, double>> &tf0, QVector<QPair<double, double>> &tf1)
{
  // If there are no volumes
  if (!vol0 && !vol1)
    return false;

  // If there are two volumes, but they have unequal size
  if (vol0 && vol1) {
    unsigned w1, h1, d1, w2, h2, d2;
    vol0->qfeGetVolumeSize(w1, h1, d1);
    vol1->qfeGetVolumeSize(w2, h2, d2);
    if (w1 != w2 || h1 != h2 || d1 != d2)
      return false;
  }

  // Resize probabilities for either of the volume series
  if (vol0)
    qfeResizeProbabilities(vol0, 0);
  if (vol1)
    qfeResizeProbabilities(vol1, 1);
  
  return true;
}

void qfeDriverCEV::qfeCalculateProbabilities(qfeVolume *vol, QVector<QPair<double, double>> &tf, int id, float rangeMin, float rangeMax)
{
  float *probabilities = id == 0 ? probabilities0 : probabilities1;
  if (vol) {
    qfeValueType t;
    void *data;
    unsigned w, h, d;
    unsigned nrComp;
    vol->qfeGetVolumeData(t, w, h, d, &data);
    vol->qfeGetVolumeComponentsPerVoxel(nrComp);

    for (unsigned z = 0; z < d; z++) {
      for (unsigned y = 0; y < h; y++) {
        for (unsigned x = 0; x < w; x++) {
          unsigned index = qfeGetIndex(w, h, x, y, z);
          float value = qfeGetValue(data, t, index, nrComp);
          probabilities[index] = qfeGetProbability(value, rangeMin, rangeMax, tf);
        }
      }
    }
  }
}

float qfeDriverCEV::qfeGetValue(void *data, qfeValueType t, unsigned index, unsigned nrComp)
{
  switch (t) {
  case qfeValueTypeFloat:
    return ((float*)data)[index];
    break;
  case qfeValueTypeInt16:
    return (float)((short*)data)[index];
    break;
  case qfeValueTypeUnsignedInt16:
    return (float)((unsigned short*)data)[index];
    break;
  }
  return -1.f;
}

float qfeDriverCEV::qfeGetProbability(float value, float rangeMin, float rangeMax, QVector<QPair<double, double>> &tf)
{
  float valueNorm = (value - rangeMin) / (rangeMax - rangeMin);
  float before = tf.first().first;
  float pBefore = tf.first().second;
  float after;
  float pAfter;
  for (unsigned i = 1; i != tf.size(); i++) {
    if (tf[i].first >= valueNorm) {
      after = tf[i].first;
      pAfter = tf[i].second;
      break;
    }
    before = tf[i].first;
    pBefore = tf[i].second;
  }

  float t = (valueNorm - before) / (after - before);
  return pBefore + t * (pAfter - pBefore);
}

float qfeDriverCEV::GetRandomFloat(float min, float max) {
  return min + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(max-min)));
}

void qfeDriverCEV::qfeRenderFeatureSeedPoints()
{
  glPointSize(8.f);
  glBegin(GL_POINTS);
  glColor3f(0.f, 0.f, 1.f);
  for (unsigned i = 0; i < featureSeedPoints.size(); i++)
    glVertex3f(featureSeedPoints[i].x, featureSeedPoints[i].y, featureSeedPoints[i].z);
  glEnd();
  glPointSize(1.f);
}

qfeReturnStatus qfeDriverCEV::qfeEnableIntersectBuffer() {
  qfeAttachFBO(frameBuffer, intersectTexColor, intersectTexColor, intersectTexDepth);
  qfeEnableFBO(frameBuffer);

  return qfeSuccess;
}

qfeReturnStatus qfeDriverCEV::qfeDisableIntersectBuffer() {
  qfeResetFBO();
  qfeEnableFBO(frameBuffer);

  return qfeSuccess;
}

qfeColorMap qfeDriverCEV::qfeGetDefaultColorMap() {
  qfeColorMap map;
  vector<qfeRGBMapping>     rgb;
  vector<qfeOpacityMapping> a;
  vector<qfeOpacityMapping> g;

  qfeRGBMapping rgbm;
  qfeColorRGB bright; bright.r = 1.0; bright.g = 0.4; bright.b = 0.4;
  qfeColorRGB dark; dark.r = 0.5; dark.g = 0.5; dark.b = 0.7;
  rgbm.value = 0.0;
  rgbm.color = bright;
  rgb.push_back(rgbm);
  rgbm.value = 1.0;
  rgbm.color = dark;
  rgb.push_back(rgbm);

  qfeOpacityMapping om;
  om.value = 0.0;
  om.opacity = 0.0;
  a.push_back(om);
  om.value = 0.15;
  om.opacity = 0.0;
  a.push_back(om);
  om.value = 0.2;
  om.opacity = 0.03;
  a.push_back(om);
  om.value = 0.25;
  om.opacity = 0.0;
  a.push_back(om);
  om.value = 1.0;
  om.opacity = 0.0;
  a.push_back(om);

  om.value = 0.0;
  om.opacity = 0.0;
  g.push_back(om);
  om.value = 1.0;
  om.opacity = 0.0;
  g.push_back(om);

  map.qfeSetColorMapInterpolation(1);
  map.qfeSetColorMapQuantization(7);
  map.qfeSetColorMapSpace(1);
  map.qfeSetColorMapConstantLightness(false);
  map.qfeSetColorMapPaddingUniform(false);
  map.qfeSetColorMapRGB((int)rgb.size(), rgb);
  map.qfeSetColorMapA((int)a.size(), a);
  map.qfeSetColorMapG((int)g.size(), g);

  return map;
}

vector<qfeRGBMapping> qfeDriverCEV::qfeGetFlowVisColorMap()
{
  vector<qfeRGBMapping> rgb;
  qfeColorRGB blue(0.23f, 0.30f, 0.75f);
  qfeColorRGB red(0.71, 0.02f, 0.15f);
  qfeColorRGB white(0.87f, 0.87f, 0.87f);
  qfeRGBMapping mapping;
  mapping.color = blue;
  mapping.value = 0.f;
  rgb.push_back(mapping);
  mapping.color = white;
  mapping.value = 0.5f;
  rgb.push_back(mapping);
  mapping.color = red;
  mapping.value = 1.f;
  rgb.push_back(mapping);
  return rgb;
}

void qfeDriverCEV::qfeGenerateSphereSet()
{
  qfeStudy *study;
  visualCEV->qfeGetVisualStudy(&study);
  qfeVolume *volume = study->pcam.front();
  qfeValueType t;
  unsigned w, h, d;
  short *data;
  volume->qfeGetVolumeData(t, w, h, d, (void**)&data);
  unsigned radius = min(w, min(h, d)) / 2;
  short low = 10000;
  short high = -10000;
  for (unsigned z = 0; z < d; z++) {
    for (unsigned y = 0; y < h; y++) {
      for (unsigned x = 0; x < w; x++) {
        short value = (x - radius)*(x - radius) + (y - radius)*(y-radius) + (z - radius)*(z - radius);
        data[z*w*h + y*w + x] = value;
        if (value > high) high = value;
        if (value < low) low = value;
      }
    }
  }
  volume->qfeSetVolumeValueDomain(low, high);
  study->pcap.front()->qfeSetVolumeValueDomain(low, high);
  qfeImageData* magVTK = qfeImageData::New();
  qfeImageData* velVTK = qfeImageData::New();

  qfeConvert::qfeVolumeToVtkImageData(study->pcam.front(), magVTK);
  qfeConvert::qfeVolumeToVtkImageData(study->pcap.front(), velVTK);
  vtkDataArray *array = velVTK->GetPointData()->GetArray("qflow velocities");
  magVTK->GetPointData()->AddArray(array);

  qfeVTIImageDataWriter *writer = qfeVTIImageDataWriter::New();
  writer->SetInput(magVTK);
  writer->SetFileName("implicitSphere.vti");
  writer->SetDataModeToBinary();
  writer->Write(); 
  writer->Delete();

  magVTK->Delete();
  velVTK->Delete();
}

void qfeDriverCEV::qfeCopyVolumeSeries()
{
  qfeStudy *study;
  visualCEV->qfeGetVisualStudy(&study);
  if (study->pcap.size() <= 0)
    return;

  for (unsigned i = 0; i < study->pcap.size(); i++) {
    if (study->flowSpeed.size() > 0) flowSpeedCopy.push_back(new qfeVolume(*study->flowSpeed[i]));
    if (study->pcam.size() > 0) pcamCopy.push_back(new qfeVolume(*study->pcam[i]));
    if (study->curl.size() > 0) curlCopy.push_back(new qfeVolume(*study->curl[i]));
    if (study->lambda2.size() > 0) lambda2Copy.push_back(new qfeVolume(*study->lambda2[i]));
    if (study->divergence.size() > 0) divergenceCopy.push_back(new qfeVolume(*study->divergence[i]));
    if (study->qcriterion.size() > 0) qcriterionCopy.push_back(new qfeVolume(*study->qcriterion[i]));
  }
  if (study->tmip) tmipCopy = new qfeVolume(*study->tmip);
}

void qfeDriverCEV::qfeCopyVolumeSeriesBack()
{
  qfeStudy *study;
  visualCEV->qfeGetVisualStudy(&study);
  for (unsigned i = 0; i < study->pcap.size(); i++) {
    if (flowSpeedCopy.size() > 0) { delete study->flowSpeed[i]; study->flowSpeed[i] = new qfeVolume(*flowSpeedCopy[i]); }
    if (pcamCopy.size() > 0) { delete study->pcam[i]; study->pcam[i] = new qfeVolume(*pcamCopy[i]); }
    if (curlCopy.size() > 0) { delete study->curl[i]; study->curl[i] = new qfeVolume(*curlCopy[i]); }
    if (lambda2Copy.size() > 0) { delete study->lambda2[i]; study->lambda2[i] = new qfeVolume(*lambda2Copy[i]); }
    if (divergenceCopy.size() > 0) { delete study->divergence[i]; study->divergence[i] = new qfeVolume(*divergenceCopy[i]); }
    if (qcriterionCopy.size() > 0) { delete study->qcriterion[i]; study->qcriterion[i] = new qfeVolume(*qcriterionCopy[i]); }
  }
  if (tmipCopy) { delete study->tmip; study->tmip = new qfeVolume(*tmipCopy); }
}

void qfeDriverCEV::qfeAssignValue(void *data, qfeValueType t, unsigned index, float value)
{
  switch (t) {
  case qfeValueTypeFloat:
    ((float*)data)[index] = value;
    break;
  case qfeValueTypeInt16:
    ((short*)data)[index] = (short)value;
    break;
  case qfeValueTypeUnsignedInt16:
    ((unsigned short*)data)[index] = (unsigned short)value;
    break;
  }
}

void qfeDriverCEV::qfeOnAction(void *caller, int t) {
  qfeDriverCEV *self = reinterpret_cast<qfeDriverCEV *>(caller);
  qfeStudy *study;

  switch (t) {
  case qfeVisualFlowParticleTrace::actionInjectSeeds:
    self->qfeInjectSeeds(self->visParticleTrace);
    self->qfeInjectSeeds(self->visLineTrace);
    break;
  case qfeVisualFlowParticleTrace::actionClearSeeds:
    self->qfeClearSeeds(self->visParticleTrace);
    self->qfeClearSeeds(self->visLineTrace);
    self->featureSeedPoints.clear();
    break;
  case qfeVisualSeedEllipsoid::actionPlaceLVGuidingPoints:
    self->lvEllipsoid.qfeReset();
    self->lvGuidingPoints.clear();
    self->lvGuidingPointState = guidingPointOne;
    self->rvGuidingPointState = guidingPointNone;
    self->voiGuidingPointState = guidingPointNone;
    break;
  case qfeVisualSeedEllipsoid::actionPlaceRVGuidingPoints:
    self->rvEllipsoid.qfeReset();
    self->rvGuidingPoints.clear();
    self->lvGuidingPointState = guidingPointNone;
    self->rvGuidingPointState = guidingPointOne;
    self->voiGuidingPointState = guidingPointNone;
    break;
  case qfeVisualVolumeOfInterest::actionPlaceVolumeOfInterest:
    self->voiEllipsoid.qfeReset();
    self->voiGuidingPoints.clear();
    self->lvGuidingPointState = guidingPointNone;
    self->rvGuidingPointState = guidingPointNone;
    self->voiGuidingPointState = guidingPointOne;
    break;
  case qfeVisualFeatureSeeding::actionPlaceFeatureSeedPoints:
    int current;
    self->visualCEV->qfeGetVisualActiveVolume(current);
    self->qfePlaceFeatureBasedSeedPoints(current);
    break;
  }  

  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverCEV::qfeOnResize(void *caller) {
  qfeDriverCEV *self = reinterpret_cast<qfeDriverCEV *>(caller);
  qfeViewport *vp3D;

  self->engine->GetViewport3D(&vp3D);

  qfeDriver::qfeDriverResize(caller);

  // Refresh size of local geometry textures  
  self->qfeDeleteTextures();
  self->qfeCreateTextures(vp3D,  self->frameBufferProps[0].supersampling);

  self->algorithmIVR.qfeSetViewportSize(vp3D->width, vp3D->height, self->frameBufferProps[0].supersampling);
  self->algorithmPDVR.qfeSetViewportSize(vp3D->width, vp3D->height, self->frameBufferProps[0].supersampling);
}

//----------------------------------------------------------------------------
void qfeDriverCEV::qfeOnMouseMove(void *caller, int x, int y, int v) {
  qfeDriverCEV *self = reinterpret_cast<qfeDriverCEV *>(caller);

  if (self->lvSelectedGuidingPoint != guidingPointNone
    && self->engine->GetInteractionControlKey()) {
    self->qfeMoveGuidingPoint(((int)self->lvSelectedGuidingPoint - 1), x, y, SE_Left);
    self->engine->Refresh();
  }

  if (self->rvSelectedGuidingPoint != guidingPointNone
    && self->engine->GetInteractionControlKey()) {
      self->qfeMoveGuidingPoint(((int)self->rvSelectedGuidingPoint - 1), x, y, SE_Right);
      self->engine->Refresh();
  }

  if (self->voiSelectedGuidingPoint != guidingPointNone
    && self->engine->GetInteractionControlKey()) {
      self->qfeMoveGuidingPoint(((int)self->voiSelectedGuidingPoint - 1), x, y, SE_VoI);
      self->engine->Refresh();
  }

  if (!self->engine->GetInteractionControlKey())
    self->engine->CallInteractionParentMouseMove();
}

//----------------------------------------------------------------------------
void qfeDriverCEV::qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v) {
  qfeDriverCEV *self = reinterpret_cast<qfeDriverCEV *>(caller);

  if (self->lvGuidingPointState != guidingPointNone) {
    if (self->engine->GetInteractionControlKey()) {
      self->qfePlaceGuidingPoint(x, y, SE_Left);
    }
  }

  if (self->rvGuidingPointState != guidingPointNone) {
    if (self->engine->GetInteractionControlKey()) {
      self->qfePlaceGuidingPoint(x, y, SE_Right);
    }
  }

  if (self->voiGuidingPointState != guidingPointNone) {
    if (self->engine->GetInteractionControlKey()) {
      self->qfePlaceGuidingPoint(x, y, SE_VoI);
    }
  }

  qfeColorRGB color;
  self->qfeReadSelect(x, y, color);
  self->selectedPlane = qfePlaneNone;
  self->lvSelectedGuidingPoint = guidingPointNone;
  self->rvSelectedGuidingPoint = guidingPointNone;
  self->voiSelectedGuidingPoint = guidingPointNone;
  self->lvEllipsoidSelected = false;
  self->rvEllipsoidSelected = false;
  self->voiEllipsoidSelected = false;
  if (color.r == 1.f && color.g == 0.f && color.b == 0.f)
    self->selectedPlane = qfePlaneX;
  if (color.r == 0.f && color.g == 1.f && color.b == 0.f)
    self->selectedPlane = qfePlaneY;
  if (color.r == 0.f && color.g == 0.f && color.b == 1.f)
    self->selectedPlane = qfePlaneZ;
  if (color.r == 0.5f && color.g == 0.5f && color.b == 0.5f)
    self->selectedPlane = qfePlaneQ;
  if (color.r == 0.f && color.g == 1.f && color.b == 1.f)
    self->lvSelectedGuidingPoint = guidingPointOne;
  if (color.r == 1.f && color.g == 0.f && color.b == 1.f)
    self->lvSelectedGuidingPoint = guidingPointTwo;
  if (color.r == 1.f && color.g == 1.f && color.b == 0.f)
    self->lvSelectedGuidingPoint = guidingPointThree;
  if (color.r == 0.f && color.g == 0.5f && color.b == 0.5f)
    self->rvSelectedGuidingPoint = guidingPointOne;
  if (color.r == 0.5f && color.g == 0.f && color.b == 0.5f)
    self->rvSelectedGuidingPoint = guidingPointTwo;
  if (color.r == 0.5f && color.g == 0.5f && color.b == 0.f)
    self->rvSelectedGuidingPoint = guidingPointThree;
  if (color.r == 0.f && color.g == 0.5f && color.b == 1.f)
    self->voiSelectedGuidingPoint = guidingPointOne;
  if (color.r == 0.5f && color.g == 0.f && color.b == 1.f)
    self->voiSelectedGuidingPoint = guidingPointTwo;
  if (color.r == 0.5f && color.g == 1.f && color.b == 0.f)
    self->voiSelectedGuidingPoint = guidingPointThree;

  // Selecting a guiding point also selects ellipsoid
  if (self->lvSelectedGuidingPoint != guidingPointNone)
    self->lvEllipsoidSelected = true;
  if (self->rvSelectedGuidingPoint != guidingPointNone)
    self->rvEllipsoidSelected = true;
  if (self->voiSelectedGuidingPoint != guidingPointNone)
    self->voiEllipsoidSelected = true;

  if (!self->engine->GetInteractionControlKey())
    self->engine->CallInteractionParentLeftButtonDown();
}

//----------------------------------------------------------------------------
void qfeDriverCEV::qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v) {
  qfeDriverCEV *self = reinterpret_cast<qfeDriverCEV *>(caller);

  // Refresh ellipsoid if we just moved a guiding point
  if (self->lvSelectedGuidingPoint != guidingPointNone
    && self->lvGuidingPoints.size() == 3) {
    self->qfeGenerateEllipsoidPoints();
    self->qfeInjectEllipsoidSeedPoints();

    // Refresh right ventricle too, since it depends on left ventricle
    if (self->rvEllipsoid.qfeIsPlaced()) {
      self->qfeGenerateEllipsoidPoints();
      self->qfeInjectEllipsoidSeedPoints();
    }
  }
  self->lvSelectedGuidingPoint = guidingPointNone;

  // Refresh ellipsoid if we just moved a guiding point
  if (self->rvSelectedGuidingPoint != guidingPointNone
    && self->rvGuidingPoints.size() == 3) {
      self->qfeGenerateEllipsoidPoints();
      self->qfeInjectEllipsoidSeedPoints();
      if (self->clipAllData)
        self->qfeUpdateClipData();
  }
  self->rvSelectedGuidingPoint = guidingPointNone;

  self->voiSelectedGuidingPoint = guidingPointNone;

  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverCEV::qfeOnMouseRightButtonDown(void *caller, int x, int y, int v) {
  qfeDriverCEV *self = reinterpret_cast<qfeDriverCEV *>(caller);

  if (self->engine->GetInteractionControlKey())
    cout << "window level stuff" << endl;
  else
    self->engine->CallInteractionParentRightButtonDown();
}

void qfeDriverCEV::qfeOnWheelForward(void *caller, int x, int y, int v) {
  qfeDriverCEV *self = reinterpret_cast<qfeDriverCEV *>(caller);

  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if (!self->visOrtho) return;

  // Obtain the volume
  self->visOrtho->qfeGetVisualStudy(&study);
  if (study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visOrtho->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if (p) p->qfeGetVisualParameterValue(scrollStep);

  if ((int)study->pcap.size() <= 0) return;

  if (self->selectedPlane == qfePlaneX) self->qfeMovePlaneForward(study->pcap.front(), &self->planeX, scrollStep);
  if (self->selectedPlane == qfePlaneY) self->qfeMovePlaneForward(study->pcap.front(), &self->planeY, scrollStep);
  if (self->selectedPlane == qfePlaneZ) self->qfeMovePlaneForward(study->pcap.front(), &self->planeZ, scrollStep);
  if (self->selectedPlane == qfePlaneQ) self->qfeMovePlaneForward(study->pcap.front(), &self->planeQ, scrollStep);

  if (self->lvEllipsoidSelected) {
    self->lvEllipsoid.qfeSquish();
    self->qfeGenerateEllipsoidPoints();
    self->qfeInjectEllipsoidSeedPoints();
  }
  if (self->rvEllipsoidSelected) {
    self->rvEllipsoid.qfeSquish();
    self->qfeGenerateEllipsoidPoints();
    self->qfeInjectEllipsoidSeedPoints();
  }
  if (self->voiEllipsoidSelected) {
    self->voiEllipsoid.qfeSquish();
  }

  self->engine->Refresh();
}

void qfeDriverCEV::qfeOnWheelBackward(void *caller, int x, int y, int v) {
  qfeDriverCEV *self = reinterpret_cast<qfeDriverCEV *>(caller);

  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if (!self->visOrtho) return;

  // Obtain the volume
  self->visOrtho->qfeGetVisualStudy(&study);
  if (study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visOrtho->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if (p) p->qfeGetVisualParameterValue(scrollStep);

  if ((int)study->pcap.size() <= 0) return;

  if (self->selectedPlane == qfePlaneX) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeX, scrollStep);
  if (self->selectedPlane == qfePlaneY) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeY, scrollStep);
  if (self->selectedPlane == qfePlaneZ) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeZ, scrollStep);
  if (self->selectedPlane == qfePlaneQ) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeQ, scrollStep);

  if (self->lvEllipsoidSelected) {
    self->lvEllipsoid.qfeStretch();
    self->qfeGenerateEllipsoidPoints();
    self->qfeInjectEllipsoidSeedPoints();
  }
  if (self->rvEllipsoidSelected) {
    self->rvEllipsoid.qfeStretch();
    self->qfeGenerateEllipsoidPoints();
    self->qfeInjectEllipsoidSeedPoints();
  }
  if (self->voiEllipsoidSelected) {
    self->voiEllipsoid.qfeStretch();
  }

  self->engine->Refresh();
}

void qfeDriverCEV::qfeClipSeedPoints()
{
  qfeStudy *study;
  float current;
  visualCEV->qfeGetVisualStudy(&study);
  visualCEV->qfeGetVisualActiveVolume(current);

  algorithmLineTrace.qfeClearSeedPositions();
  algorithmParticleTrace.qfeClearSeedPositions();

  std::vector<qfePoint> points;
  for (unsigned i = 0; i < featureSeedPoints.size(); i++) {
    qfePoint point = featureSeedPoints[i];
    if (voiEllipsoid.IsInVolume(point))
      points.push_back(point);
  }
  featureSeedPoints.clear();
  featureSeedPoints = points;

  algorithmLineTrace.qfeAddSeedPositions(featureSeedPoints);
  algorithmLineTrace.qfeGeneratePathlines(study->pcap, current, qfeFlowLinesPathDynamic);
  algorithmParticleTrace.qfeAddSeedPositions(featureSeedPoints);
}
