#include "qfeDriverAide.h"

//----------------------------------------------------------------------------
void qfeDriverAide::qfeRenderBoundingBox(qfeVolume *volume, qfeFloat lineWidth)
{
  qfeColorRGB color;
  color.r = color.g = color.b = 0.6;

  qfeRenderBoundingBox(volume, color, lineWidth);
}

//----------------------------------------------------------------------------
void qfeDriverAide::qfeRenderBoundingBox(qfeVolume *volume, qfeColorRGB color, qfeFloat lineWidth)
{
  qfeMatrix4f   V2P, T2V, M;
  GLfloat       matrix[16];

  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

  M = T2V*V2P;

  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(&matrix[0]);  // Voxel to World Coordinates

  glLineWidth(lineWidth);
  glColor3f(color.r,color.g,color.b);

  qfeDriverAide::qfeDrawBoundingBox();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

};

//----------------------------------------------------------------------------
void qfeDriverAide::qfeRenderBoundingBoxDashed(qfeVolume *volume, qfeMatrix4f modelview, qfeFloat lineWidth)
{
  qfeColorRGB color;
  color.r = color.g = color.b = 0.6;

  qfeRenderBoundingBoxDashed(volume, modelview, color, lineWidth);
}

//----------------------------------------------------------------------------
void qfeDriverAide::qfeRenderBoundingBoxDashed(qfeVolume *volume, qfeMatrix4f modelview, qfeColorRGB color, qfeFloat lineWidth)
{
  qfeMatrix4f   V2P, T2V, M, MVinv;
  GLfloat       matrix[16];
  qfePoint      points[8];
  qfeVector     normals[6];
  int           topologyEdges[12][2];
  int           topologyNormals[12][2];
  qfeVector     view;
  qfePoint      minima;

  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

  // Coordinate transformation: Texture to Patient
  M = T2V*V2P; 

  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  // Compute view direction in patient space
  qfeMatrix4f::qfeGetMatrixInverse(modelview, MVinv);
  view.qfeSetVectorElements(0.0,0.0,-1.0,0.0);
  qfeMatrix4f::qfeMatrixPreMultiply(view, MVinv, view);
  qfeVector::qfeVectorNormalize(view);

  // Points in patient space
  //points[0] = M * qfePoint(-0.5f,-0.5f,-0.5f);
  //points[1] = M * qfePoint( 0.5f,-0.5f,-0.5f);
  //points[2] = M * qfePoint( 0.5f, 0.5f,-0.5f);
  //points[3] = M * qfePoint(-0.5f, 0.5f,-0.5f);
  //points[4] = M * qfePoint(-0.5f,-0.5f, 0.5f);
  //points[5] = M * qfePoint( 0.5f,-0.5f, 0.5f);
  //points[6] = M * qfePoint( 0.5f, 0.5f, 0.5f);
  //points[7] = M * qfePoint(-0.5f, 0.5f, 0.5f);

  points[0] = qfePoint( 0.0f, 0.0f, 0.0f) * M;
  points[1] = qfePoint( 1.0f, 0.0f, 0.0f) * M;
  points[2] = qfePoint( 1.0f, 1.0f, 0.0f) * M;
  points[3] = qfePoint( 0.0f, 1.0f, 0.0f) * M;
  points[4] = qfePoint( 0.0f, 0.0f, 1.0f) * M;
  points[5] = qfePoint( 1.0f, 0.0f, 1.0f) * M;
  points[6] = qfePoint( 1.0f, 1.0f, 1.0f) * M;
  points[7] = qfePoint( 0.0f, 1.0f, 1.0f) * M;

  // Normals in patient space
  normals[0] = qfeVector(-1.0f,  0.0f,  0.0f) * M;  // left   face
  normals[1] = qfeVector( 0.0f,  0.0f,  1.0f) * M;  // front  face
  normals[2] = qfeVector( 1.0f,  0.0f,  0.0f) * M;  // right  face
  normals[3] = qfeVector( 0.0f,  0.0f, -1.0f) * M;  // back   face
  normals[4] = qfeVector( 0.0f,  1.0f,  0.0f) * M;  // top    face
  normals[5] = qfeVector( 0.0f, -1.0f,  0.0f) * M;  // bottom face

  for(int i=0; i<6; i++) qfeVector::qfeVectorNormalize(normals[i]);

  // Define bounding box topology (index to the points of the edges)
  topologyEdges[ 0][0] = 0; topologyEdges[ 0][1] = 1; // e 0 1 
  topologyEdges[ 1][0] = 1; topologyEdges[ 1][1] = 2; // e 1 2
  topologyEdges[ 2][0] = 2; topologyEdges[ 2][1] = 3; // e 2 3
  topologyEdges[ 3][0] = 3; topologyEdges[ 3][1] = 0; // e 3 0
  topologyEdges[ 4][0] = 4; topologyEdges[ 4][1] = 5; // e 4 5
  topologyEdges[ 5][0] = 5; topologyEdges[ 5][1] = 6; // e 5 6
  topologyEdges[ 6][0] = 6; topologyEdges[ 6][1] = 7; // e 6 7
  topologyEdges[ 7][0] = 7; topologyEdges[ 7][1] = 4; // e 7 4
  topologyEdges[ 8][0] = 0; topologyEdges[ 8][1] = 4; // e 0 4
  topologyEdges[ 9][0] = 1; topologyEdges[ 9][1] = 5; // e 1 5
  topologyEdges[10][0] = 2; topologyEdges[10][1] = 6; // e 2 6
  topologyEdges[11][0] = 3; topologyEdges[11][1] = 7; // e 3 7

  // Define to with surfaces the edge connects, we index the normals
  topologyNormals[ 0][0] = 1; topologyNormals[ 0][1] = 4; // n front top
  topologyNormals[ 1][0] = 1; topologyNormals[ 1][1] = 0; // n front left
  topologyNormals[ 2][0] = 1; topologyNormals[ 2][1] = 5; // n front bottom
  topologyNormals[ 3][0] = 1; topologyNormals[ 3][1] = 2; // n front right
  topologyNormals[ 4][0] = 3; topologyNormals[ 4][1] = 4; // n back  top
  topologyNormals[ 5][0] = 3; topologyNormals[ 5][1] = 0; // n back  left
  topologyNormals[ 6][0] = 3; topologyNormals[ 6][1] = 5; // n back  bottom
  topologyNormals[ 7][0] = 3; topologyNormals[ 7][1] = 2; // n back  right
  topologyNormals[ 8][0] = 2; topologyNormals[ 8][1] = 4; // n right top
  topologyNormals[ 9][0] = 0; topologyNormals[ 9][1] = 4; // n left  top
  topologyNormals[10][0] = 0; topologyNormals[10][1] = 5; // n left  bottom
  topologyNormals[11][0] = 2; topologyNormals[11][1] = 5; // n right bottom

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLineWidth(lineWidth);  
  glColor3f(0.6,0.6,0.6);

  for(int i=0; i<12; i++)
  {
    qfeFloat a1, a2;
    a1 = view * normals[topologyNormals[i][0]];
    a2 = view * normals[topologyNormals[i][1]];

    if(a1 < 0 && a2 < 0){
      glEnable(GL_LINE_STIPPLE);  
      glLineStipple(1.0, 0x00FF);
    }

    // Immediate mode rendering for each pass is not efficient
    // GL_LINE_STIPPLE cannot be changed during the render call
    glBegin(GL_LINES);
      glVertex3f(points[topologyEdges[i][0]].x, points[topologyEdges[i][0]].y, points[topologyEdges[i][0]].z);
      glVertex3f(points[topologyEdges[i][1]].x, points[topologyEdges[i][1]].y, points[topologyEdges[i][1]].z);
    glEnd();

    glDisable(GL_LINE_STIPPLE);
  }

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
}

//----------------------------------------------------------------------------
void qfeDriverAide::qfeRenderVolumeAxes(qfeVolume *volume, qfeFloat scale, qfeFloat lineWidth)
{  
  qfeMatrix4f   T2V, V2P, M;
  qfeGrid      *grid;
  GLfloat       matrix[16];

  qfePoint  ov(-0.5f,-0.5f,-0.5f);
  qfePoint  op( 0.0f, 0.0f, 0.0f);
  qfePoint  ow( 0.0f, 0.0f, 0.0f);
  qfeVector x( 1.0f, 0.0f, 0.0f);
  qfeVector y( 0.0f, 1.0f, 0.0f);
  qfeVector z( 0.0f, 0.0f, 1.0f);

  if(volume == NULL) return;

  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);

  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridOrigin(op.x, op.y, op.z);

  // Coordinate transformation: Voxel to Patient
  M = V2P; 

  // Move the origin to voxel coordinates
  //ov = T2V*ov;

  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLineWidth(lineWidth);

  // Draw world coordinate system axis  
  //qfeDrawAxes(ow, x, y, z, scale);

  // Draw patient coordinate system axis  
  qfeDrawAxes(op, x, y, z, scale);

  // Draw voxel coordinate system axes
  glMultMatrixf(&matrix[0]); 
  qfeDrawAxes(ov, x, y, z, scale);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();  

}

//----------------------------------------------------------------------------
void qfeDriverAide::qfeDrawBoundingBox()
{
  glBegin(GL_LINE_STRIP);
  glVertex3f(-0.5f,-0.5f,-0.5f);
  glVertex3f( 0.5f,-0.5f,-0.5f);
  glVertex3f( 0.5f, 0.5f,-0.5f);
  glVertex3f(-0.5f, 0.5f,-0.5f);
  glVertex3f(-0.5f,-0.5f,-0.5f);
  glVertex3f(-0.5f,-0.5f, 0.5f);
  glVertex3f(-0.5f, 0.5f, 0.5f);
  glVertex3f( 0.5f, 0.5f, 0.5f);
  glVertex3f( 0.5f,-0.5f, 0.5f);
  glVertex3f( 0.5f,-0.5f,-0.5f);
  glEnd();

  glBegin(GL_LINES);
  glVertex3f(-0.5f, 0.5f,-0.5f);
  glVertex3f(-0.5f, 0.5f, 0.5f);

  glVertex3f( 0.5f, 0.5f,-0.5f);
  glVertex3f( 0.5f, 0.5f, 0.5f);

  glVertex3f(-0.5f,-0.5f, 0.5f);
  glVertex3f( 0.5f,-0.5f, 0.5f);
  glEnd();
};

//----------------------------------------------------------------------------
void qfeDriverAide::qfeDrawAxes(qfePoint origin, qfeVector x, qfeVector y, qfeVector z, qfeFloat scale)
{
  qfeVector axisX, axisY, axisZ;
  axisX = x;
  axisY = y;
  axisZ = z;

  qfeVector::qfeVectorNormalize(axisX);
  qfeVector::qfeVectorNormalize(axisY);
  qfeVector::qfeVectorNormalize(axisZ);

  axisX = scale * axisX;
  axisY = scale * axisY;
  axisZ = scale * axisZ;

  glBegin(GL_LINES);

  glColor3f(1.0,0.0,0.0);
  glVertex3f(origin.x,origin.y,origin.z);
  glVertex3f(origin.x + axisX.x, origin.y + axisX.y, origin.z + axisX.z);

  glColor3f(0.0,1.0,0.0);
  glVertex3f(origin.x,origin.y,origin.z);
  glVertex3f(origin.x + axisY.x, origin.y + axisY.y, origin.z + axisY.z);

  glColor3f(0.0,0.0,1.0);
  glVertex3f(origin.x,origin.y,origin.z);
  glVertex3f(origin.x + axisZ.x, origin.y + axisZ.y, origin.z + axisZ.z);

  glEnd();
}
