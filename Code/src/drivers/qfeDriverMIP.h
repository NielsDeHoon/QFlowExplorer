#pragma once

#include "qflowexplorer.h"

#include "qfeDriver.h"
#include "qfeMaximumProjection.h"

using namespace std;

/**
* \file   qfeDriverMIP.h
* \author Roy van Pelt
* \class  qfeDriverMIP
* \brief  Implements a MIP driver
*
* qfeDriverMIP derives from qfeDriver, and implements
* a GPU-based Maximum Intensity Projection
*/

class qfeDriverMIP : public qfeDriver
{
public:
  qfeDriverMIP(qfeScene *scene);
  ~qfeDriverMIP();

  bool qfeRenderCheck();
  void qfeRenderSelect(){};
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();

private:
  qfeVisualMaximum     *visualMIP;

  qfeMaximumProjection *algorithmMIP;
  
  GLuint              currentTransferFunctionId;

  GLuint texRayStart;
  GLuint texRayEnd;

  qfeReturnStatus qfeRenderVisualMIP(qfeVisualMaximum *visual);
  qfeReturnStatus qfeGetDataSet(int index, qfeVisual *visual, int time, qfeVolume **volume);

  void qfeSetDynamicUniformLocations();

  static void qfeOnResize(void *caller);  
  static void qfeOnMouseMove(void *caller, int x, int y, int v);
  static void qfeOnMouseRightButtonDown(void *caller, int x, int y, int v);
  
};
