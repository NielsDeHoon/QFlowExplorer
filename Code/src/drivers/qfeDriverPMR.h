#pragma once

#include "qflowexplorer.h"

#include "qfeDriver.h"
#include "qfeFrame.h"
#include "qfeMatrix4f.h"
#include "qfeRaycasting.h"
#include "qfeSurfaceShading.h"
#include "qfeFlowLineTraceMatching.h"

#include "qfeTransform.h"

#include "vtkCamera.h"
#include "vtkMatrix4x4.h"

/**
* \file   qfeDriverPMR.h
* \author Roy van Pelt
* \class  qfeDriverPMR
* \brief  Implements a driver for the pattern matching result rendering
*
* qfeDriverPMR derives from qfeDriver, and implements
* a GPU-based pattern matching result visualization
*/

class qfeDriverPMR : public qfeDriver
{
public:
  qfeDriverPMR(qfeScene *scene);
  ~qfeDriverPMR();

  bool qfeRenderCheck();
  void qfeRenderSelect();
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();

  enum qfeDataTypeIndex
  {
    dataTypePCAP,
    dataTypePCAM,
    dataTypeSSFP,
    dataTypeTMIP,
    dataTypeFFE,
    dataTypeCLUST
  };

private:
  qfeVisualPatternMatching      *visPatternMatching;
  qfeVisualRaycast              *visDVR;
  qfeVisualAnatomyIllustrative  *visAnatomy;
  qfeVisualAides                *visAides;

  qfeRayCasting                 *algorithmDVR;
  qfeFlowLineTraceMatching      *algorithmLineTrace;
  qfeSurfaceShading             *algorithmSurfaceShading;

  qfeSeeds                       seedsPatternMatching;
  qfeSeeds                       attribsPatternMatching;

  float                          dvrQuality;

  bool                           firstRenderLineTrace;

  GLuint                         intersectTexColor;
  GLuint                         intersectTexDepth;

  qfeReturnStatus qfeRenderVisualPatternMatching(qfeVisualPatternMatching *visual);
  qfeReturnStatus qfeRenderVisualDVR(qfeVisualRaycast* visual);
  qfeReturnStatus qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual);
  qfeReturnStatus qfeRenderVisualAides(qfeVisualAides *visual);

  qfeReturnStatus qfeGetDataSet(qfeDataTypeIndex index, qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu);

  qfeReturnStatus qfeInjectFileSeeds();
  qfeReturnStatus qfeClearSeeds();

  qfeReturnStatus qfeCreateTextures(qfeViewport *vp, int supersampling);
  qfeReturnStatus qfeDeleteTextures();
  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeEnableIntersectBuffer();
  qfeReturnStatus qfeDisableIntersectBuffer();

  static void qfeOnAction(void *caller, int t);
  static void qfeOnResize(void *caller);
  static void qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v);
  static void qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v);

};
