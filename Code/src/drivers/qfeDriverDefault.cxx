#include "qfeDriverDefault.h"

//----------------------------------------------------------------------------
qfeDriverDefault::qfeDriverDefault(qfeScene *scene) : qfeDriver(scene)
{
};

//----------------------------------------------------------------------------
qfeDriverDefault::~qfeDriverDefault()
{
  this->qfeRenderStop();
};

//----------------------------------------------------------------------------
bool qfeDriverDefault::qfeRenderCheck()
{
  return true;
}


//----------------------------------------------------------------------------
void qfeDriverDefault::qfeRenderStart()
{
}

//----------------------------------------------------------------------------
void qfeDriverDefault::qfeRenderWait()
{  

};

//----------------------------------------------------------------------------
void qfeDriverDefault::qfeRenderStop()
{

};
