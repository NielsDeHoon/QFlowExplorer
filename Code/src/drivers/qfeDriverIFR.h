#pragma once

#include "qflowexplorer.h"

#include "qfeFlowArrows.h"
#include "qfeFlowLines.h"
#include "qfeFlowPlanes.h"
#include "qfeFlowProbe2D.h"
#include "qfeMaximumProjection.h"
#include "qfeSurfaceContours.h"
#include "qfeSurfaceShading.h"

#include "qfeConvert.h"
#include "qfeDriver.h"
#include "qfeSeeding.h"
#include "qfeViewport.h"

#include <vtkActor.h>
#include <vtkCellArray.h>
#include <vtkCurvatures.h>
#include <vtkPoints.h>
#include <vtkPolyDataMapper.h>
#include <vtkTriangleFilter.h>

#include <sstream>
#include <direct.h>  // for getcwd
#include <stdlib.h>  // for MAX_PATH

typedef vector< qfePoint >         qfeSeeds;              // Per ring
typedef vector< qfeSeeds >         qfeSeedsGroup;         // Per volume

#define  SELECT_PROBE        0.4

enum qfeSelectObjects
{
  qfeObjectProbe2D
};

/**
* \file   qfeDriverIFR.h
* \author Roy van Pelt
* \class  qfeDriverIFR
* \brief  Implements an illustrative flow rendering driver
*
* qfeDriverIFR derives from qfeDriver, and implements
* an Illustative Flow Rendering driver
*/

class qfeDriverIFR : public qfeDriver
{
public:
  qfeDriverIFR(qfeScene *scene);
  ~qfeDriverIFR();

  bool qfeRenderCheck();
  void qfeRenderSelect();
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();

private:
  qfeVisualAnatomyIllustrative       *visAnatomyIllustrative;
  qfeVisualMaximum                   *visMIP;
  qfeVisualFlowProbe2D               *visFlowProbe;
  qfeVisualFlowLines                 *visFlowLines;
  qfeVisualFlowPlanes                *visFlowPlanes;
  qfeVisualFlowArrowTrails           *visFlowArrowTrails;
  qfeVisualAides                     *visAides;

  qfeFlowProbe2D                     *algorithmProbe2D;
  qfeSurfaceContours                 *algorithmContours;
  qfeSurfaceShading                  *algorithmSurfaceShading;
  qfeFlowLines                       *algorithmFlowLines;
  qfeFlowArrows                      *algorithmFlowArrows;
  qfeFlowPlanes                      *algorithmFlowPlanes;
  qfeMaximumProjection               *algorithmMIP;
   
  qfeSeedsGroup                       probeSeeds;
  int                                 probeSeedsStrategy;
  int                                 probeSeedsTemplate;
  double                              probeSeedsDensity;
  qfeFloat                            probeTraceStep;
  int                                 probeTraceIterations;
  bool                                probeCountChanged;
  int                                 probeArrowMaxPerc;
  vector< qfeProbeInteractionState >  probeInteractionState;  // Current interaction per probe
  vector< qfeColorRGB >               probeColor;
  qfeProbeSelectState                 probeClickState;        // Current state for adding with 2-clicks

  int                                 paramTraceCounter;
  bool                                paramLineHighlightVisible;

  int                                 paramSeedStrategy;
  int                                 paramSeedTransferFunction;
  double                              paramSeedDensity;
 
  bool                                mouseDrag;
  qfePoint                            mouseCache;

  vector<qfePoint>                    tempPoint;

  // Global render function for supported visuals
  qfeReturnStatus qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual);  
  qfeReturnStatus qfeRenderVisualMaximumProjection(qfeVisualMaximum *visual); 
  qfeReturnStatus qfeRenderVisualFlowProbe(qfeVisualFlowProbe2D *visual);
  qfeReturnStatus qfeRenderVisualFlowLines(qfeVisualFlowLines *visual);
  qfeReturnStatus qfeRenderVisualFlowPlanes(qfeVisualFlowPlanes *visual);
  qfeReturnStatus qfeRenderVisualFlowArrowTrails(qfeVisualFlowArrowTrails *visual);
  qfeReturnStatus qfeRenderVisualAides(qfeVisualAides *visual);

  // Probe related functions
  qfeReturnStatus qfeAddProbe(qfePoint seed, qfeStudy *study);
  qfeReturnStatus qfeDeleteProbe(int probeId, qfeStudy *study);
  qfeReturnStatus qfeUpdateProbe(qfePoint seed, qfeStudy *study, int probeId);

  // Pre-computation functions
  qfeReturnStatus qfePrecomputeSeeds(qfeProbe2DSeries probe, int seedStrategy, int seedTransferFunction, double seedDensity, qfeSeedsGroup &seeds);
  qfeReturnStatus qfeUpdateSeeds(qfeProbe2D probe, int seedStrategy, int seedTransferFunction, double seedDensity, qfeSeeds &seeds);
 
  // Computation support functions
  void qfeVolumeSeriesGetVectorAtPositionLinear(qfeVolumeSeries series, qfePoint p, qfeFloat t, qfeVector &v);
  bool qfeUpdateSeedsRequired(int seedStrategy, double seedDensity, int seedTemplate);
  bool qfeUpdateLinesRequired(qfeFloat traceStep, int traceIterations);
  bool qfeUpdateArrowsRequired(int arrowMaxPerc);

  // Interaction
  qfeReturnStatus qfeWorkflowProbeClick(qfePoint *p);
  qfeReturnStatus qfeGetSelectedObject(qfeColorRGB color, int &id, int &nr); 
  qfeReturnStatus qfeSetSelectedProbe(qfeVisualFlowProbe2D *visual, int index);
  qfeReturnStatus qfeGetSelectedProbe(qfeVisualFlowProbe2D *visual, int &index);

  qfeReturnStatus qfePickMesh(int windowX, int windowY, qfeViewport *viewport, qfePoint &patientPos);
  qfeReturnStatus qfePickMaximum(int windowX, int windowY, qfeViewport *viewport, qfeVolume *volume, qfePoint &patientPos);
  
  static void qfeOnTimer(void *caller);  
  static void qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v);
  static void qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v);
  static void qfeOnMouseMove(void *caller, int x, int y, int v);
  static void qfeOnKeyPress(void *caller, char *keysym);
  static void qfeOnResize(void *caller);
  static void qfeOnAction(void *caller, int t);

  qfeReturnStatus qfeGetDataSet(int index, qfeVisual *visual, int time, qfeVolume **volume);
  qfeReturnStatus qfeGetIntersectionsBoxLine(qfeVolume *volume, qfePoint p1, qfePoint p2, qfePoint **intersections, int &count);
  qfeReturnStatus qfeGetInsideBox(qfeVolume *volume, qfePoint p, bool &inside);



};
