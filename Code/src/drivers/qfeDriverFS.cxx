#include "qfeDriverFS.h"

/**
* \core functionality
*/

#include <iostream>
#include <iomanip>
#include <ctime>
void printCurrentTime()
{
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);
	std::cout << std::put_time(&tm, "%d-%m-%Y %H-%M-%S") << std::endl;
}

//----------------------------------------------------------------------------
qfeDriverFS::qfeDriverFS(qfeScene *scene) : qfeDriver(scene)
{
	this->visFluidSim  = NULL;
	this->visAnatomy   = NULL;
	this->visParticles = NULL;
	this->visLines     = NULL;
	this->visBoundary  = NULL;
	this->visAides     = NULL;
	this->visMetrics   = NULL;

	this->fluidInteractors.disableCreator();
	this->centerline = new qfeCenterline();

	this->firstRender       = false;    
	this->simulationUpdated = false;
	this->simulationParticlePositions.clear();
	this->simulationParticleVelocities.clear(); 

	this->seedCount         = 100;

	this->algorithmFluidSim          = new qfeSimulation(); 
	this->algorithmFluidSimParticles = new qfeSimulationParticleTrace();
	this->algorithmFluidSimLines     = new qfeSimulationLinesComparison();
	this->algorithmFluidSimBoundary  = new qfeSimulationBoundaryComparison();
	this->algorithmSurfaceShading    = new qfeSurfaceShading();

	this->engine->SetObserverAction((void*)this, this->qfeOnAction);
	this->engine->SetObserverResize((void*)this, this->qfeOnResize);
	this->engine->SetInteractionMouseMove((void*)this, this->qfeOnMouseMove, false);
	this->engine->SetInteractionMouseLeftButtonDown((void*)this, this->qfeOnMouseLeftButtonDown, false);
	this->engine->SetInteractionMouseRightButtonDown((void*)this, this->qfeOnMouseRightButtonDown, false);
	this->engine->SetInteractionMouseRightButtonUp((void*)this, this->qfeOnMouseRightButtonUp, false);
	
	errorWritingDone = false;

	this->rightMouseDown = false;

	forwardSimVelocityFields.clear();
	backwardSimVelocityFields.clear();

	isoMeshUpToDate = false;
	isoMeshCreated = false;

	currentIsoValue = 0.0f;
	currentIsoLabel = "";
	currentIsoPhase = 0.0f;

	simulationSpacing = 1;

	q2DRenderer = new qfe2DBasicRendering(this->engine->GetRenderer());
};

//----------------------------------------------------------------------------
qfeDriverFS::~qfeDriverFS()
{
	delete this->algorithmFluidSim;
	delete this->algorithmFluidSimParticles;
	delete this->algorithmFluidSimLines;
	delete this->algorithmFluidSimBoundary;
	delete this->algorithmSurfaceShading;

	this->q2DRenderer->qfeClear2DText();
	delete this->q2DRenderer;
	delete this->centerline;

	this->qfeRenderStop();
};

//----------------------------------------------------------------------------
bool qfeDriverFS::qfeRenderCheck()
{	
	int       t;

	for(int i=0; i<(int)this->visuals.size(); i++)
	{    
		this->visuals[i]->qfeGetVisualType(t);
		if(t == visualFluid)
		{
			qfeStudy      *study;
			qfeVolume     *volume   = NULL;
			int				     current;

			//with noise:
			//algorithmFluidSim->noise = 10.0;

			this->visFluidSim = static_cast<qfeVisualFluid *>(this->visuals[i]);

			this->visFluidSim->qfeGetVisualStudy(&study); 
			this->visFluidSim->qfeGetVisualActiveVolume(current);

			if(study == NULL) 
				return qfeError;

			volume = study->pcap[current];

			if(volume == NULL) 
				return qfeError;

			this->algorithmFluidSim->qfeSetVolume(volume);
			this->algorithmFluidSim->qfeSetVelocityField(study->pcap);

			unsigned int nxi, nxj, nxz;
			volume->qfeGetVolumeSize(nxi,nxj,nxz);
			unsigned int gridWidth = nxi/simulationSpacing;
			if(simulationSpacing>1)
			{
				float res = 1.0f/(float)simulationSpacing;
				std::cout<<"--------------------------------\nSimulation runs on "<<res<<" resolution!\n--------------------------------"<<std::endl;
			}

			this->algorithmFluidSim->qfeInitFluidSimulation(gridWidth,volume,study->segmentation,study);	

			//std::cout<<"TODO disable no-stick"<<std::endl;
			//this->algorithmFluidSim->fluidsim->grid.no_stick = true;

			if(study->segmentation.size() == 0)
			{
				QMessageBox msgBox;
				msgBox.setText("No mesh was found, please load a mesh.");
				msgBox.exec();
				return false;
			}

			this->algorithmFluidSim->qfeUpdateTime(current);
			if(!this->algorithmFluidSim->fluidsim->firstRun)
			{
				this->algorithmFluidSim->qfeUpdateSerie(current);
			}

			cout<<"Simulation initialized"<<std::endl;
		}
		if(t == visualSimParticles)
		{
			this->visParticles = static_cast<qfeVisualSimParticleTrace *>(this->visuals[i]);      
		}
		if(t == visualSimLinesComparison)
		{
			this->visLines = static_cast<qfeVisualSimLinesComparison *>(this->visuals[i]);      
		}
		if(t == visualSimBoundaryComparison)
		{
			this->visBoundary = static_cast<qfeVisualSimBoundaryComparison *>(this->visuals[i]);      
		}
		if(t == visualAnatomyIllustrative)
		{
			this->visAnatomy = static_cast<qfeVisualAnatomyIllustrative *>(this->visuals[i]);      
		}  
		if(t == visualAides)
		{
			this->visAides   = static_cast<qfeVisualAides *>(this->visuals[i]);
		}
		if(t == visualMetrics)
		{
			this->visMetrics   = static_cast<qfeVisualMetrics *>(this->visuals[i]);
		}
	}

	return true;
}

//----------------------------------------------------------------------------
void qfeDriverFS::qfeRenderStart()
{
	source_selection_enabled = false;
	sink_selection_enabled   = false;
	inspector_enabled		 = false;

	qfeMatrix4f V2P,V2T,T2V;

	qfeStudy      *study;
	qfeVolume     *volume;
	int				     current;

	// get the data 
	volume = NULL;
	this->visFluidSim->qfeGetVisualStudy(&study); 
	if(study == NULL) return;
	this->visFluidSim->qfeGetVisualActiveVolume(current);
	volume = study->pcap[current];
	if(volume == NULL) return;
	if(this->mesh == NULL) return;

	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);	
	
	//this->algorithmFluidSim->qfeGenerateSyntheticData();	
	//this->algorithmFluidSim->qfeGenerateEnrightData();
	/*
	if(study->pcap.size()<1)
		return;
	
	study->pcap[0]->qfeGetVolumeGrid(&grid);

	if(study->segmentation.size()<1)
	{
		std::cout<<"No mesh was provided"<<std::endl;
		return;
	}
		
    bool existingMetric;	
	Array3f solid;

	float range_min,range_max;
	study->qfeGetScalarMetric("Solid SDF", 0.0f, existingMetric, solid, range_min, range_max);
	if(existingMetric == false || solid.ni < 2 || solid.nj < 2 || solid.nk < 2)
	{
		std::cout<<"Error: Mesh not found"<<std::endl;
		return;
	}
	*/
	
	fluidInteractors.init(this->algorithmFluidSim->fluidsim->getSolidSurface(), this->algorithmFluidSim->dx, V2P, V2T, T2V);

	std::cout<<"Compute centerline"<<std::endl;
	centerline->qfeInit(this->algorithmFluidSim->fluidsim->getSolidSurface(), this->algorithmFluidSim->dx, V2P, V2T, T2V);

	qfeViewport *vp3D;
	this->engine->GetViewport3D(&vp3D);

	if(this->visFluidSim != NULL)
	{
		this->algorithmFluidSim->removeNearBoundary = true;
		this->algorithmFluidSim->qfeSetViewportSize(vp3D->width, vp3D->height, this->frameBufferProps[0].supersampling);
		std::cout<<"Seed fluid particles"<<std::endl;
		this->algorithmFluidSim->qfeSeedParticles();

		qfeStudy           *study   = NULL;
		qfeVolume          *volume  = NULL;
		int				          current;

		// get the data 
		volume = NULL;
		this->visFluidSim->qfeGetVisualStudy(&study); 
		this->visFluidSim->qfeGetVisualActiveVolume(current);

		if(study == NULL) return;

		volume = study->pcap[current];

		if(volume == NULL) return;

		//algorithmFluidSim->qfeWriteErrors();
		//algorithmFluidSim->qfeInterpolationErrors();
		//algorithmFluidSim->qfeNoiseErrors();

		//algorithmFluidSim->qfeStoreSimulationData(volume,(int) current);
		//this->algorithmFluidSim->qfeGenerateSyntheticData();
		fluidInteractors.init(this->algorithmFluidSim->fluidsim->getSolidSurface(), this->algorithmFluidSim->dx, V2P, V2T, T2V);
	}

	if(this->visLines != NULL)
	{
		this->algorithmFluidSimLines->qfeSetLinesWidth(this->algorithmFluidSim->dx*5.0);
	}

	//
	// Add hardcoded interactors
	//

	//*ANEURYSM
	qfeFluidInteractor source;
	source.Initialize(
		qfePoint(54, 46.3855, 49.2754),				//center
		-0.974876,									//radius
		qfeVector(-0.0042, 0.99998, 0.00338129),	//normal
		qfeVector(0.39567, -0.00037, 0.604697),		//axis1
		qfeVector(-0.6047, -0.00390, 0.395662),		//axis2
		qfeFluidInteractor::SOURCE, 				//type
		0.02,										//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	qfeFluidInteractor sink1;
	sink1.Initialize(
		qfePoint(18, 1.20482, 51.2464),				//center
		-0.974876,									//radius
		qfeVector(0.0777401, -0.99173, -0.102115),	//normal
		qfeVector(0.509661, -0.0117, 0.50203),		//axis1
		qfeVector(0.499072, 0.0910721, -0.504538),	//axis2
		qfeFluidInteractor::SINK, 					//type
		0.02,										//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	qfeFluidInteractor sink2;
	sink2.Initialize(
		qfePoint(82, 1.20482, 51.24646),			//center
		-0.974876,									//radius
		qfeVector(-0.0001431, -1, -4.299e-5),		//normal
		qfeVector(-2.22725, 0.000182, 3.22707),		//axis1
		qfeVector(3.22707, -0.000556, 2.22725),		//axis2
		qfeFluidInteractor::SINK, 					//type
		0.02,										//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	fluidInteractors.add(source);
	fluidInteractors.add(sink1);
	fluidInteractors.add(sink2);
	//*/
	/*DISSECTION
	qfeFluidInteractor source;
	source.Initialize(
		qfePoint(28.35, 64, 45),					//center
		-1,											//radius
		qfeVector(0.103742, 0.865443, -0.49015),	//normal
		qfeVector(0.283257, 0.237466, 0.479277),	//axis1
		qfeVector(-0.53118, 0.18856, 0.220508),		//axis2
		qfeFluidInteractor::SOURCE, 				//type
		0.208333,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface());
	qfeFluidInteractor sink1;
	sink1.Initialize(
		qfePoint(30.375, 24, 51.6667),				//center
		-1,											//radius
		qfeVector(-0.0678264, 0.997328, -0.0271258),//normal
		qfeVector(0.824367, 0.0592313, 0.116402),	//axis1
		qfeVector(-0.117698, 0.0144665, 0.286182),	//axis2
		qfeFluidInteractor::SINK,					//type
		0.421875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface());
	qfeFluidInteractor sink2;
	sink2.Initialize(
		qfePoint(44.55, 122, 30),					//center
		-1,											//radius
		qfeVector(-0.548329, 0.792514, 0.266942),	//normal
		qfeVector(0.939953, 0.950298, -0.890251),	//axis1
		qfeVector(0.95921, 0.237237, 1.266),		//axis2
		qfeFluidInteractor::SINK,					//type
		0.421875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface());

	fluidInteractors.add(source);
	fluidInteractors.add(sink1);
	fluidInteractors.add(sink2);
	//*/

	/*
	//HIGH RES LUMC data
	qfeFluidInteractor source(qfePoint(110,86,11),      -1,qfeVector(-0.378587,-0.911344,0.16163), qfeVector(-5.16735,2.75184,3.4155),    qfeVector(3.55748,-0.457867,5.75104),   qfeFluidInteractor::SOURCE,0.3125,V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface());
	qfeFluidInteractor  sink1(qfePoint(124,234,12.2222),-1,qfeVector(0.176686,-0.967357,0.181666), qfeVector(1.04577,0.153581,-0.199355), qfeVector(-0.164947,-0.225204,-1.03877),qfeFluidInteractor::SINK,0.3125,V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface());
	//additional small sinks
	qfeFluidInteractor  sink2(qfePoint(120,198,11),		-1,qfeVector(-0.80389,0.581808,-0.123529), qfeVector(0.29173,0.456512,-0.251759), qfeVector(-0.202868,-0.166349,0.536716),qfeFluidInteractor::SINK,0.3125,V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface());
	qfeFluidInteractor  sink3(qfePoint(124,30,8.55556), -1,qfeVector(-0.137738,-0.984482,0.108732),qfeVector(-0.481414,0.208024,1.27339), qfeVector(1.27625,-0.123048,0.502596),  qfeFluidInteractor::SINK,0.3125,V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface());
	qfeFluidInteractor  sink4(qfePoint(112,30,9.77778), -1,qfeVector(0.150546,-0.97463,-0.165627), qfeVector(0.231405,-0.114291,0.882886),qfeVector(0.879417,0.171242,-0.208328), qfeFluidInteractor::SINK,0.3125,V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface());
	//inspectors:
	//ascending aorta
	qfeFluidInteractor inspc1(qfePoint(106,72,13.4444), -1,qfeVector(-0.113077,-0.966881,0.215743),qfeVector(5.64617,-1.38402,-3.26215),  qfeVector(-3.46249,-0.849247,-5.63261), qfeFluidInteractor::INSPECTOR,0.3125,V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface());
	//aortic arc
	qfeFluidInteractor inspc2(qfePoint(128,46,9.77778),	-1,qfeVector(-0.978415,-0.206649,0.000309132),qfeVector(-0.20317,0.962305,0.240865),qfeVector(0.0500718,-0.235603,0.983519),qfeFluidInteractor::INSPECTOR,0.3125,V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface());
	//descending aorta
	qfeFluidInteractor inspc3(qfePoint(146,82,13.4444), -1,qfeVector(0.0477369,0.984178,-0.170632),qfeVector(1.01878,-0.0448865,0.026106),qfeVector(-0.0180339,0.175083,1.0048),  qfeFluidInteractor::INSPECTOR,0.3125,V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface());

	std::cout<<"Add hardcoded interactors"<<std::endl;
	fluidInteractors.add(source);
	fluidInteractors.add(sink1);
	fluidInteractors.add(sink2);
	fluidInteractors.add(sink3);
	fluidInteractors.add(sink4);
	fluidInteractors.add(inspc1);
	fluidInteractors.add(inspc2);
	fluidInteractors.add(inspc3);
	//*/

	/*Intracranial data
	qfeFluidInteractor source1;
	source1.Initialize(
		qfePoint(187.0, 195.0, 6.51163),			//center
		-1,											//radius
		qfeVector(0.0297695, 0.0129326, 0.999473),	//normal
		qfeVector(0.496964, 0.524594, -0.0215584),	//axis1
		qfeVector(0.524597, -0.497344, -0.00918984),//axis2
		qfeFluidInteractor::SOURCE, 				//type
		0.046875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	qfeFluidInteractor source2;
	source2.Initialize(
		qfePoint(144.0, 174.0, 15.814),				//center
		-1,											//radius
		qfeVector(0.95121, -0.224579, 0.211574),	//normal
		qfeVector(0.78921, 1.8233, -1.61251),		//axis1
		qfeVector(0.0236267, -1.70081, -1.91158),	//axis2
		qfeFluidInteractor::SOURCE, 				//type
		0.046875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	qfeFluidInteractor source3;
	source3.Initialize(
		qfePoint(143.0, 217.0, 11.1628),				//center
		-1,											//radius
		qfeVector(-0.989294, -0.01163259, -0.145472),//normal
		qfeVector(0.150482, 2.03581, -1.18629),		//axis1
		qfeVector(-0.309952, 1.19548, 2.01226),		//axis2
		qfeFluidInteractor::SOURCE, 				//type
		0.046875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	
	qfeFluidInteractor sink1;
	sink1.Initialize(
		qfePoint(145.0, 138.0, 29.7674),			//center
		-1,											//radius
		qfeVector(-0.285492, -0.957041, 0.0506721),	//normal
		qfeVector(1.33066, -0.393623, 0.0629601),	//axis1
		qfeVector(0.0403096, -0.0854021, -1.38587),	//axis2
		qfeFluidInteractor::SINK, 					//type
		0.046875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	qfeFluidInteractor sink2;
	sink2.Initialize(
		qfePoint(209.0, 161.0, 25.1163),			//center
		-1,											//radius
		qfeVector(-0.927008, -0.00571699, 0.374998),//normal
		qfeVector(-3.5856, 13.2469, -8.66132),		//axis1
		qfeVector(4.91805, 9.37371, 12.3005),		//axis2
		qfeFluidInteractor::SINK, 					//type
		0.046875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	qfeFluidInteractor sink3;
	sink3.Initialize(
		qfePoint(137.0, 202.0, 34.4186),			//center
		-1,											//radius
		qfeVector(0.703485, 0.334488, -0.627078),	//normal
		qfeVector(-0.288352, 1.05115, 0.237205),	//axis1
		qfeVector(-0.738493, -0.0139487, -0.925916),//axis2
		qfeFluidInteractor::SINK, 					//type
		0.046875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	qfeFluidInteractor sink4;
	sink4.Initialize(
		qfePoint(133.0, 195.0, 32.5581),			//center
		-1,											//radius
		qfeVector(0.717021, -0.231283, -0.657563),	//normal
		qfeVector(-0.913329, 4.48767, -2.57434),	//axis1
		qfeVector(-3.54632, -2.44643, -3.00652),	//axis2
		qfeFluidInteractor::SINK, 					//type
		0.046875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	qfeFluidInteractor sink5;
	sink5.Initialize(
		qfePoint(211.0, 228.0, 18.6047),			//center
		-1,											//radius
		qfeVector(-0.869927, -0.43235, 0.237277),	//normal
		qfeVector(-0.576398, 1.30714, 0.269261),	//axis1
		qfeVector(0.426568, -0.0974721, 1.38632),	//axis2
		qfeFluidInteractor::SINK, 					//type
		0.046875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	qfeFluidInteractor sink6;
	sink6.Initialize(
		qfePoint(160.0, 241.0, 27.907),				//center
		-1,											//radius
		qfeVector(-0.373303, 0.89975, -0.226039),	//normal
		qfeVector(1.74024, 0.428413, -1.16865),		//axis1
		qfeVector(0.954654, 0.829622, 1.72571),		//axis2
		qfeFluidInteractor::SINK, 					//type
		0.046875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	qfeFluidInteractor sink7;
	sink7.Initialize(
		qfePoint(184.0, 211.0, 1.86047),			//center
		-1,											//radius
		qfeVector(0.300516, -0.714626, 0.631665),	//normal
		qfeVector(-0.0417646, 0.47946, 0.562304),	//axis1
		qfeVector(0.704695, 0.195362, -0.114239),	//axis2
		qfeFluidInteractor::SINK, 					//type
		0.046875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	qfeFluidInteractor sink8;
	sink8.Initialize(
		qfePoint(189.0, 175.0, 9.30233),			//center
		-1,											//radius
		qfeVector(0.110924, 0.979483, 0.168255),	//normal
		qfeVector(-6.65247, -0.677511, 8.33),		//axis1
		qfeVector(-8.27308, 2.04331, -6.44084),		//axis2
		qfeFluidInteractor::SINK, 					//type
		0.046875,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
	);

	fluidInteractors.add(source1);
	fluidInteractors.add(source2);
	fluidInteractors.add(source3);

	fluidInteractors.add(sink1);
	fluidInteractors.add(sink2);
	fluidInteractors.add(sink3);
	fluidInteractors.add(sink4);
	fluidInteractors.add(sink5);
	fluidInteractors.add(sink6);
	fluidInteractors.add(sink7);
	fluidInteractors.add(sink8);
	//*/
	/*Healthy data
	qfeFluidInteractor source;
	source.Initialize(
		qfePoint(15.0, 53.6276, 20.0),				//center
		-1,											//radius
		qfeVector(-0.0541759, -0.751137, 0.65792),	//normal
		qfeVector(-4.73915, 2.86205, 2.8771),		//axis1
		qfeVector(4.0441, 2.96211, 3.71481),		//axis2
		qfeFluidInteractor::SOURCE, 				//type
		0.208333,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
		);

	qfeFluidInteractor sink1;
	sink1.Initialize(
		qfePoint(18.0, 13.9034, 26.1538),			//center
		-1,											//radius
		qfeVector(-0.172982, 0.751586, -0.63655),	//normal
		qfeVector(-0.425151, 0.706274, 0.718877),	//axis1
		qfeVector(-0.989877, -0.146277, -0.44171),	//axis2
		qfeFluidInteractor::SINK, 					//type
		0.208333,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
		);

	qfeFluidInteractor sink2;
	sink2.Initialize(
		qfePoint(18.0, 8.93793, 14.6154),			//center
		-1,											//radius
		qfeVector(-0.328569, -0.847099, -0.417691),	//normal
		qfeVector(7.29914, 0.542247, -6.84139),		//axis1
		qfeVector(-6.02182, 5.29666, -6.00493),		//axis2
		qfeFluidInteractor::SINK, 					//type
		0.208333,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
		);

	qfeFluidInteractor sink3;
	sink3.Initialize(
		qfePoint(24, 12.9103, 12.3077),				//center
		-1,											//radius
		qfeVector(-0.344466, -0.924639, -0.162441),	//normal
		qfeVector(-6.05581, 1.23311, 5.8227),		//axis1
		qfeVector(5.18358,-2.98943,6.0242),		//axis2
		qfeFluidInteractor::SINK, 					//type
		0.208333,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
		);

	qfeFluidInteractor sink4;
	sink4.Initialize(
		qfePoint(30, 123.145, 13.8462),				//center
		-1,											//radius
		qfeVector(0.296988,-0.952751,-0.0637494),	//normal
		qfeVector(-0.300273,-0.193543,1.49382),		//axis1
		qfeVector(1.43557,0.424503,0.343565),		//axis2
		qfeFluidInteractor::SINK, 					//type
		0.208333,									//dx
		V2P,V2T,T2V,&this->algorithmFluidSim->fluidsim->getSolidSurface()
		);

	fluidInteractors.add(source);

	fluidInteractors.add(sink1);
	fluidInteractors.add(sink2);
	fluidInteractors.add(sink3);
	fluidInteractors.add(sink4);
	//*/
}

//----------------------------------------------------------------------------
void qfeDriverFS::qfeRenderWait()
{
	// Initialize OpenGL state
	glDisable(GL_DITHER);
	glEnable(GL_BLEND);
	glDisable(GL_NORMALIZE);
	glDisable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);

	// Initialize color map
	qfeColorMap *colormap;
	if(this->visParticles != NULL)
	{
		this->visParticles->qfeGetVisualColorMap(&colormap);
		colormap->qfeUploadColorMap();
	}
	if(this->visLines != NULL)
	{
		this->visLines->qfeGetVisualColorMap(&colormap);
		colormap->qfeUploadColorMap();
	}
	if(this->visBoundary != NULL)
	{
		this->visBoundary->qfeGetVisualColorMap(&colormap);
		colormap->qfeUploadColorMap();
	}

	// Render the surface anatomy   
	if(this->visAnatomy != NULL)
	{
		this->qfeRenderVisualAnatomyIllustrative(this->visAnatomy);
	}

	// Render the fluid simulation
	// Important: this visual must be first, as it keeps the simulation state
	if(this->visFluidSim != NULL)
	{    
		this->qfeRenderVisualFluidSim(this->visFluidSim); 
		//reset the color to white to get a consistent background:
		glColor3f(1.0f,1.0f,1.0f);
	} 

	// Render the fluid simulation particles
	if(this->visParticles != NULL)
	{    
		this->qfeRenderVisualFluidSimParticles(this->visParticles); 	  
	}

	// Render the fluid simulation comparative lines
	if(this->visLines != NULL)
	{    
		this->qfeRenderVisualFluidSimLines(this->visLines); 	  
	}

	// Render the fluid simulation boundary comparison
	if(this->visBoundary != NULL)
	{    
		this->qfeRenderVisualFluidSimBoundary(this->visBoundary); 	  
	}

	// Render visual aides
	if(this->visAides != NULL)
	{
		this->qfeRenderVisualAides(this->visAides);
	}  

	// Render visual aides
	if(this->visMetrics != NULL)
	{
		this->qfeRenderVisualMetrics(this->visMetrics);
	}  

	this->fluidInteractors.renderAll();
	this->centerline->qfeRenderCenterline();
};

//----------------------------------------------------------------------------
void qfeDriverFS::qfeRenderStop()
{
};

qfeReturnStatus qfeDriverFS::qfeRemoveFluidInteractor()
{
	unsigned int index = this->fluidInteractors.getSelectedInteractorIndex();

	if(index == 0)
	{
		this->engine->SetStatusMessage("Please select a fluid interactor first");
	}
	else
	{
		qfeFluidInteractor *interactor = this->fluidInteractors.qfeGetFluidInteractor(index);

		Array3b cells = this->fluidInteractors.qfeGetFluidInteractorCells(interactor);

		for(unsigned int k = 0; k<cells.nk; k++) for(unsigned int j = 0; j<cells.nj; j++)  for(unsigned int i = 0; i<cells.ni; i++)
		{
			if(cells(i,j,k)) //if the cell is covered by the interactor we have to reset it:
			{
				this->algorithmFluidSim->fluidsim->removeSinkCell(i,j,k);
				this->algorithmFluidSim->fluidsim->removeSourceCell(i,j,k);
			}
		}

		this->fluidInteractors.removeSelected();
	}
	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFS::qfePreprocessFluidSimulation()
{
	std::cout<<"Computing simulation volume"<<std::endl;
	//TODO do something with the result: but mainly when starting the simulation
	Array3b validCells = this->fluidInteractors.qfeGetValidFluidCells();

	while(this->algorithmFluidSim->fluidsim->particles.size() != 0)
	{
		this->algorithmFluidSim->fluidsim->particles.remove_particle(this->algorithmFluidSim->fluidsim->particles.size()-1);
	}

	this->algorithmFluidSim->fluidsim->firstRun = true;


	std::cout<<"Seeding simulation particles"<<std::endl;
	this->algorithmFluidSim->qfeSeedParticles(); //to reset the particles

	unsigned int p = this->algorithmFluidSim->fluidsim->particles.size();
	while(p>=1)
	{
		p--;
		Vec3ui cell = this->algorithmFluidSim->fluidsim->particles.get_cell_of_particle(p);

		if(!validCells(cell[0], cell[1], cell[2]))
		{
			//for the synthetic data this should be skipped:
			this->algorithmFluidSim->fluidsim->particles.remove_particle(p);
		}
	}

	this->algorithmFluidSim->fluidsim->setFillHolesCells(validCells);

	int count = 0;
	for(unsigned int i = 0; i<validCells.size(); i++)
	{
		if(validCells.a.at(i))
			count ++;
	}
	std::cout<<"Particles left: "<<this->algorithmFluidSim->fluidsim->getNumberOfParticles()<<" # valid cells: "<<count<<std::endl;
	if(this->algorithmFluidSim->fluidsim->getNumberOfParticles() == 0)
	{
		std::cout<<"Please set a source and sink"<<std::endl;
		QMessageBox *msgBox = new QMessageBox(0); //not setting a parent is not so smart, but there is no QWidget parent accessable
		msgBox->setText("Please set a source and sink first.");
		//msgBox->setWindowModality(Qt::NonModal);
		//msgBox->setInformativeText("Please set a source and sink first.");
		msgBox->setStandardButtons(QMessageBox::Ok);
		msgBox->setDefaultButton(QMessageBox::Ok);
		int ret = msgBox->exec();
		return qfeError;
	}

	this->forwardSimVelocityFields.clear();
	this->backwardSimVelocityFields.clear();

	this->algorithmFluidSim->measurementsWritten = 0;
	//simulation should be initialized

	qfeStudy      *study;
	qfeVolume     *volume;
	int			   current;

	// get the data 
	volume = NULL;
	this->visFluidSim->qfeGetVisualStudy(&study); 
	if(study == NULL) return qfeError;
	this->visFluidSim->qfeGetVisualActiveVolume(current);
	volume = study->pcap[current];
	if(volume == NULL) return qfeError;

	//get the current phase
	float currentPhase;

	if(this->visFluidSim == NULL) return qfeError;
	this->visFluidSim->qfeGetVisualActiveVolume(currentPhase);  

	int outputCount = 0;
	string bar; //progressbar

	// fetch the parameters  
	bool paramForwardSim  = true;
	bool paramBackwardSim = true;
	bool paramUseViscosity = true;
	int paramNumberOfStepsPerMeasurement = 0;
	qfeVisualParameter *param;

	this->visFluidSim->qfeGetVisualParameter("FS forward simulated coupling", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramForwardSim);    

	this->visFluidSim->qfeGetVisualParameter("FS backward simulated coupling", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramBackwardSim);  

	this->visFluidSim->qfeGetVisualParameter("FS use viscosity", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramUseViscosity); 

	this->visFluidSim->qfeGetVisualParameter("FS steps per measurement", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramNumberOfStepsPerMeasurement);

	if(paramNumberOfStepsPerMeasurement<1)
		return qfeError;

	this->algorithmFluidSim->qfeUseViscosity(paramUseViscosity);

	float stepSize = 1.0f/(float)paramNumberOfStepsPerMeasurement;

	std::cout<<"Output temporal resolution: "<<study->phaseDuration * stepSize<<"ms, "<<paramNumberOfStepsPerMeasurement<<"x the original temporal resolution"<<std::endl;

	std::cout<<""<<std::endl;
	qfeReturnStatus status = qfeSuccess;
	if(paramForwardSim)
	{
		std::cout<<"Starting forward simulated coupling..."<<std::endl;
		printCurrentTime();

		this->algorithmFluidSim->measurementsWritten = 0;
		status = this->qfeRunCoupling(1, stepSize, paramForwardSim && paramBackwardSim);
	}

	if(status == qfeSuccess && paramBackwardSim)
	{
		std::cout<<"Starting backward simulated coupling..."<<std::endl;
		printCurrentTime();

		this->algorithmFluidSim->measurementsWritten = 0;
		status = this->qfeRunCoupling(-1, stepSize, paramForwardSim && paramBackwardSim);
	}

	if(paramForwardSim && paramBackwardSim)
	{
		if(this->forwardSimVelocityFields.size() != this->backwardSimVelocityFields.size())
		{
			std::cout<<"Forward and backward simulation resulted in different simulation steps..."<<std::endl;
			status = qfeError;
		}
	}

	if(status != qfeSuccess)
	{
		std::cout<<"Something went wrong..."<<std::endl;
		return status;
	}

	unsigned int phaseCount = 1.0/stepSize * study->pcap.size();
	float phaseDuration = stepSize * study->phaseDuration;

	this->algorithmFluidSim->negative_time_step = false;

	QList<Array3Vec3> resultSimVelocityFields;

	/*
	if(paramForwardSim && !paramBackwardSim)//forward only
	{
		//we should move the forward sim in the result sim data set to output it
		resultSimVelocityFields.clear();
		for(unsigned int index = 0; index<this->forwardSimVelocityFields.size(); index++)
		{
			resultSimVelocityFields.append(
				this->forwardSimVelocityFields.at(index)
			);
		}
	}
	*/

	/*
	if(!paramForwardSim && paramBackwardSim)//backward only
	{
		//we should move the backward sim in the result sim data set to output it
		resultSimVelocityFields.clear();
		for(unsigned int index = 0; index<this->backwardSimVelocityFields.size(); index++)
		{
			resultSimVelocityFields.append(
				this->backwardSimVelocityFields.at(index)
			);
		}
	}
	*/

	//if merge required
	if(paramForwardSim && paramBackwardSim)
	{
		std::cout<<"Merge forward and backward simulations..."<<std::endl;
		printCurrentTime();
		std::cout<<"In total we have "<<this->forwardSimVelocityFields.size()<<" simulation steps"<<std::endl;

		for(unsigned int index = 0; index<this->forwardSimVelocityFields.size(); index++)
		{
			float phase = index*stepSize;
			float linear_weight = phase-floor(phase);
			std::cout<<"Linear weight "<<linear_weight<<std::endl;
			float forward_weight = 1.0f-linear_weight;//bezierWeighting(linear_weight);

			if(forward_weight < 0.0f) forward_weight = 0.0f;
			if(forward_weight > 1.0f) forward_weight = 1.0f;

			float backward_weight = 1.0f-forward_weight;

			std::cout<<"Phase "<<phase<<" forward simulation weight "<<forward_weight<<" backward simulation weight "<<backward_weight<<std::endl;
			Array3Vec3 forwardSim  = this->forwardSimVelocityFields.at(  index );
			Array3Vec3 backwardSim = this->backwardSimVelocityFields.at( index );

			Array3Vec3 resultSim(forwardSim.ni, forwardSim.nj, forwardSim.nk);

			//merge the velocity field based on the last time it was coupled (older -> lower weight)

			for(unsigned int k = 0; k<backwardSim.nk; k++)
				for(unsigned int j = 0; j<backwardSim.nj; j++)
					for(unsigned int i = 0; i<backwardSim.ni; i++)
			{
				resultSim(i,j,k)[0] = 
					forward_weight  * forwardSim(i,j,k)[0] + 
					backward_weight * backwardSim(i,j,k)[0];

				resultSim(i,j,k)[1] =
					forward_weight  * forwardSim(i,j,k)[1] + 
					backward_weight * backwardSim(i,j,k)[1];

				resultSim(i,j,k)[2] =  
					forward_weight  * forwardSim(i,j,k)[2] + 
					backward_weight * backwardSim(i,j,k)[2];
			}			

			resultSimVelocityFields.append(resultSim);
		}
	}
		
	this->algorithmFluidSim->measurementsWritten = 0;
	std::cout<<"TODO LOAD THE DATA"<<std::endl;
	/*
	this->visFluidSim->qfeGetVisualStudy(&study); 
	qfeStudy *lineStudy;
	this->visLines->qfeGetVisualStudy(&lineStudy);
	qfeStudy *particleStudy;
	this->visParticles->qfeGetVisualStudy(&particleStudy);

	study->sim_velocities.clear();
	lineStudy->sim_velocities.clear();
	particleStudy->sim_velocities.clear();
	*/

	//if merge was necessary we still ahve to write the merged result:
	if(paramForwardSim && paramBackwardSim)
	{
		//write the simulated data to file and load it in the study
		std::cout<<"Write the simulated data to file..."<<std::endl;
		printCurrentTime();
		
		for(unsigned int index = 0; index<resultSimVelocityFields.size(); index++)
		{
			float triggerTime = stepSize * index;

			if(!paramForwardSim && paramBackwardSim)//backward sim only
			{
				triggerTime = phaseCount - triggerTime;
			}

			volume = this->algorithmFluidSim->WriteSimulationVelocityFieldToVolume(index,phaseCount,"qflow_sim",phaseDuration,triggerTime,resultSimVelocityFields.at(index));

			/*
			study->sim_velocities.push_back(volume);
			lineStudy->sim_velocities.push_back(volume);
			particleStudy->sim_velocities.push_back(volume);
			*/
		}
	}

	//TODO update visuals
	//this->algorithmFluidSimLines->qfeGeneratePathlines();

	// TODO NDH
	cout << endl << "Simulation Done" << endl;
	printCurrentTime();
	/*
	qfeStudy      *study;
	qfeVolume     *volume;
	int				     current;

	// get the data 
	volume = NULL;
	this->visFluidSim->qfeGetVisualStudy(&study); 
	if(study == NULL) return qfeError;
	this->visFluidSim->qfeGetVisualActiveVolume(current);
	volume = study->pcap[current];
	if(volume == NULL) return qfeError;

	algorithmFluidSim->qfeStoreSimulationData(volume, 0);
	*/
	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFS::qfeInjectSeeds(qfeVisualSimLinesComparison *visual)
{
	std::cout<<"Inject seeds"<<std::endl;
	if(this->simulationParticlePositions.size() == 0)
	{
		std::cout<<"No simulation particles available"<<std::endl;
		return qfeError;
	}

	qfeStudy           *study   = NULL;
	qfeVolume          *volume  = NULL;
	qfeModel           *model   = NULL;
	qfeMatrix4f         P2V, V2T, P2T;
	qfeSeeds            seeds;
	int                 current;
	int                 phases;

	if(visual == NULL) return qfeError;

	visual->qfeGetVisualStudy(&study);  
	visual->qfeGetVisualActiveVolume(current);

	if(study == NULL) return qfeError;

	phases = (int) study->pcap.size();
	volume = study->pcap[current];  
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
	P2T = P2V * V2T;

	if((int)study->segmentation.size() > 0)
		model  = study->segmentation.front();

	/*
	// TODO RvP Issues to select particles within the mesh
	// vtkSelectEnclosedPoints not working.
	if(model != NULL)
	{
	this->randomParticlePositions.clear();

	qfeSeeding::qfeGetSeedsRandomUniform(model, 200, seeds);

	qfePoint p;
	for(int i=0; i<(int)seeds.size(); i++)
	{
	vector< float > currentSeed;

	p = seeds[i] * V2T;

	currentSeed.push_back(p.x);
	currentSeed.push_back(p.y);
	currentSeed.push_back(p.z);
	currentSeed.push_back(-1);

	this->randomParticlePositions.push_back(currentSeed);
	}
	}*/

	/*
	for(int i=0; i<this->seedCount; i++)
	{  }
	*/

	//vec2.reserve( vec1.size() );
	//copy(vec1.begin(), vec1.end(), std::back_inserter(vec2.begin()));

	vector< vector<float> >   selectedParticlePositions;
	vector< vector<float> >   injectedParticlePositions;

	selectedParticlePositions.clear();
	injectedParticlePositions.clear();  

	std::random_shuffle(this->simulationParticlePositions.begin(), this->simulationParticlePositions.end());
	std::copy(this->simulationParticlePositions.begin(), this->simulationParticlePositions.begin()+this->seedCount, std::back_inserter(selectedParticlePositions));

	//for(int i=0; i<selectedParticlePositions.size(); i++)
	//{
	//  cout << selectedParticlePositions[i][0] << " " << selectedParticlePositions[i][1] << " " << selectedParticlePositions[i][2] << " 6 0 0 1" << endl;
	//}

	injectedParticlePositions.reserve(phases * sizeof(selectedParticlePositions));

	for(int phase=0; phase<phases; phase++)
	{
		for(int j=0; j<(int)selectedParticlePositions.size(); j++)
		{
			selectedParticlePositions[j][3] = phase;           
		}

		std::copy(selectedParticlePositions.begin(), selectedParticlePositions.end(), std::back_inserter(injectedParticlePositions));
	}

	this->algorithmFluidSimLines->qfeAddSeedPositions(injectedParticlePositions, volume);    
	this->algorithmFluidSimLines->qfeGeneratePathlines();

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFS::qfeClearSeeds(qfeVisualSimLinesComparison  *visual)
{
	this->randomParticlePositions.clear();

	this->algorithmFluidSimLines->qfeClearSeedPositions();
	this->algorithmFluidSimLines->qfeClearPathlines();

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus  qfeDriverFS::qfeInjectSeeds(qfeVisualSimBoundaryComparison *visual)
{
	qfeStudy      *study   = NULL;  
	qfeVolume     *volume  = NULL;
	int            current;
	const float    particleOffset = 0.6f;

	if(visual == NULL) return qfeError;

	visual->qfeGetVisualStudy(&study);
	visual->qfeGetVisualActiveVolume(current);

	if(study == NULL) return qfeError;

	volume = study->pcap[current];  

	if((int)study->segmentation.size() > 0)
	{
		this->algorithmFluidSimBoundary->qfeAddSeedsFromModel(study->segmentation.front(), particleOffset, volume);    
	}

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus  qfeDriverFS::qfeClearSeeds(qfeVisualSimBoundaryComparison  *visual)
{
	this->algorithmFluidSimBoundary->qfeClearSeeds();

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFS::qfeRenderVisualFluidSim(qfeVisualFluid *visual)
{  
	QTime time = QTime::QTime(0,0,0,0);
	time.start();  

	qfeStudy           *study  = NULL;
	qfeVolume          *volume = NULL;
	qfeLightSource     *light  = NULL;
	float               simulationPhase, currentPhase;

	int frameTimeElap   = 0;
	int updateTimeElap  = 0;  

	qfeVisualParameter *param;
	bool                paramCouplingRealTime;
	qfeSelectionList    paramDataList;  
	int                 paramData;
	bool                paramGridVisible;  

	if(visual == NULL) return qfeError;

	visual->qfeGetVisualStudy(&study); 
	visual->qfeGetVisualActiveVolume(currentPhase);  

	if(study == NULL) return qfeError;

	this->scene->qfeGetSceneLightSource(&light);

	volume = study->pcap[(int)floor(currentPhase)];

	if(volume == NULL) return qfeError;

	// fetch the parameters  
	visual->qfeGetVisualParameter("FS real-time coupling", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramCouplingRealTime);    

	visual->qfeGetVisualParameter("FS grid visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramGridVisible);       

	visual->qfeGetVisualParameter("Render data", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
	paramData = paramDataList.active;

	// Get the current time of the simulation
	simulationPhase = (float)this->algorithmFluidSim->qfeGetSimulationTime();

	// Reset the state variable
	this->simulationUpdated = false;

	if(!this->firstRender)
	{
		this->algorithmFluidSim->qfeGetParticlePositions(this->simulationParticlePositions);
		this->algorithmFluidSim->qfeGetParticleVelocities(this->simulationParticleVelocities);

		this->simulationUpdated = true;
		this->firstRender       = true;
	}

	// If real-time coupling is enabled and the animation is playing,
	// we have to advance the fluid simulation.
	if(paramCouplingRealTime && (simulationPhase != currentPhase)) 
	{
		// Compute the time we need to advance in the simulation
		double difference = currentPhase-simulationPhase;
		if (difference < 0) 
		{      
			difference = currentPhase+study->pcap.size()-simulationPhase;
			errorWritingDone = true;
		}

		// Advance one frame
		QTime frameTime = QTime::QTime(0,0,0,0);	
		frameTime.start();

		//phase duration is stored in simulation
		this->algorithmFluidSim->qfeAdvanceOneFrame(difference);

		frameTimeElap = frameTime.elapsed();	  

		QTime updateTime = QTime::QTime(0,0,0,0);	
		updateTime.start();

		this->algorithmFluidSim->qfeUpdateTime(currentPhase);

		this->simulationUpdated = true;

		// Obtain the current particle set
		this->simulationParticlePositions.clear();

		this->algorithmFluidSim->qfeGetParticlePositions(this->simulationParticlePositions);
		this->algorithmFluidSim->qfeGetParticleVelocities(this->simulationParticleVelocities);

		// If the simulation arrives at a timepoint with new measured data,
		// provide the right index into the data
		if((int)floor(currentPhase) == currentPhase) 
		{

			//this->algorithmFluidSim->qfeStoreSimulationData(volume,(int)currentPhase);
			this->algorithmFluidSim->qfeUpdateSerie((int)floor(currentPhase));      
		}

		updateTimeElap = frameTime.elapsed();	  
	}

	QTime renderTime = QTime::QTime(0,0,0,0);
	renderTime.start();

	int elapsed = time.elapsed();
	int renderTimeElap = renderTime.elapsed();

	// Perform support rendering
	if(paramGridVisible)
	{
		this->algorithmFluidSim->qfeRenderGrid(volume);  
	}

	//if (currentPhase-simulationPhase != 0)
	//  algorithmFluidSim->qfeWriteErrors();

	//if(elapsed>0) cout<<"Total:  "<<((float)elapsed/1000.0)<<endl;
	//if(algorithmFluidSim->fluidsim->grid.solverTimeNeeded>0) cout<<"Solver: "<<algorithmFluidSim->fluidsim->grid.solverTimeNeeded<<endl;
	//cout<<"Render: "<<(float)renderTimeElap/1000.0<<endl;
	//if(frameTimeElap>0) cout<<"Frame:  "<<(float)frameTimeElap/1000.0<<endl;
	//if(updateTimeElap>0) cout<<"Update: "<<(float)updateTimeElap/1000.0<<endl;
	//cout<<"----\n"<<endl;

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFS::qfeRenderVisualFluidSimParticles(qfeVisualSimParticleTrace *visual)
{  
	qfeStudy           *study;
	qfeLightSource     *light;
	qfeVolume          *volume;
	qfeColorMap        *colormap;
	int                 current;

	if(visual == NULL) return qfeError;

	visual->qfeGetVisualStudy(&study);  
	visual->qfeGetVisualActiveVolume(current);
	visual->qfeGetVisualColorMap(&colormap);
	this->scene->qfeGetSceneLightSource(&light);

	if(study == NULL) return qfeError;

	volume = study->pcap[current];

	// fetch the parameters  
	qfeVisualParameter *param;
	bool                paramParticlesVisible;
	int                 paramParticlesPercentage;
	double              paramParticleSize;
	qfeSelectionList    paramParticleStyleList;
	int                 paramParticleStyle;
	qfeSelectionList    paramParticleShadingList;
	int                 paramParticleShading;
	bool                paramParticleContour;

	visual->qfeGetVisualParameter("FS particles visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramParticlesVisible);    

	visual->qfeGetVisualParameter("FS particles percentage", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramParticlesPercentage);   

	visual->qfeGetVisualParameter("FS particles size (mm)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramParticleSize);   

	visual->qfeGetVisualParameter("FS particles style", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramParticleStyleList);  
	paramParticleStyle = paramParticleStyleList.active;    

	visual->qfeGetVisualParameter("FS particles shading", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramParticleShadingList);  
	paramParticleShading = paramParticleShadingList.active;    

	visual->qfeGetVisualParameter("FS particles contour", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramParticleContour);  

	this->algorithmFluidSimParticles->qfeSetParticlePercentage(paramParticlesPercentage);
	this->algorithmFluidSimParticles->qfeSetParticleSize(paramParticleSize);
	this->algorithmFluidSimParticles->qfeSetParticleStyle((qfeSimulationParticleTrace::qfeFlowParticlesStyle)paramParticleStyle);
	this->algorithmFluidSimParticles->qfeSetParticleShading((qfeSimulationParticleTrace::qfeFlowParticlesShading)paramParticleShading);
	this->algorithmFluidSimParticles->qfeSetParticleContour(paramParticleContour);
	this->algorithmFluidSimParticles->qfeSetLightSource(light);

	if(this->simulationUpdated)
	{ 
		this->algorithmFluidSimParticles->qfeAddSeedPositions(this->simulationParticlePositions, volume);
		this->algorithmFluidSimParticles->qfeAddSeedVelocities(this->simulationParticleVelocities);    
	}

	if(paramParticlesVisible)
	{  
		this->algorithmFluidSimParticles->qfeRenderParticles(colormap);
	}

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFS::qfeRenderVisualFluidSimLines(qfeVisualSimLinesComparison *visual)
{
	qfeStudy           *study;
	qfeLightSource     *light;  
	qfeVolume          *volume;
	qfeColorMap        *colormap;
	int                 current;
	float               currentTime;

	if(visual == NULL) return qfeError;

	visual->qfeGetVisualStudy(&study);  
	visual->qfeGetVisualActiveVolume(current);
	visual->qfeGetVisualActiveVolume(currentTime);
	visual->qfeGetVisualColorMap(&colormap);
	this->scene->qfeGetSceneLightSource(&light);

	if(study == NULL) return qfeError;

	volume = study->pcap[current];

	// fetch the parameters  
	qfeVisualParameter *param;
	qfeSelectionList    paramColorList;
	bool                paramSurfaceVisible; 
	bool                paramLinesFirstVisible;  
	bool                paramLinesSecondVisible;  
	bool                paramSeedsVisible; 
	bool                paramHighlight;
	int                 paramSeedsCount;
	int                 paramDistanceFilter;
	int                 paramColorType;
	double				paramLineLength;

	visual->qfeGetVisualParameter("FS seeds visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramSeedsVisible);   

	visual->qfeGetVisualParameter("FS line length (phases)", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramLineLength);   

	visual->qfeGetVisualParameter("FS seeds count", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramSeedsCount);   

	visual->qfeGetVisualParameter("FS lines measurement visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramLinesFirstVisible);    

	visual->qfeGetVisualParameter("FS lines simulation visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramLinesSecondVisible);    

	visual->qfeGetVisualParameter("FS surface visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceVisible);    

	visual->qfeGetVisualParameter("FS distance filter", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramDistanceFilter);

	visual->qfeGetVisualParameter("FS color type", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramColorList);    
	paramColorType = paramColorList.active;

	visual->qfeGetVisualParameter("FS highlight measurement", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramHighlight);  

	this->algorithmFluidSimLines->qfeSetVolumeSeries(study->pcap, study->sim_velocities);
	this->algorithmFluidSimLines->qfeSetLinesLength(paramLineLength);
	this->algorithmFluidSimLines->qfeSetSeedsVisible(paramSeedsVisible);  
	this->algorithmFluidSimLines->qfeSetLinesFirstVisible(paramLinesFirstVisible);
	this->algorithmFluidSimLines->qfeSetLinesSecondVisible(paramLinesSecondVisible);
	this->algorithmFluidSimLines->qfeSetSurfaceVisible(paramSurfaceVisible);
	this->algorithmFluidSimLines->qfeSetDistanceFilter(paramDistanceFilter);
	this->algorithmFluidSimLines->qfeSetColorType((qfeSimulationLinesComparison::qfeComparisonColorType)paramColorType);
	this->algorithmFluidSimLines->qfeSetLinesFirstHighlight(paramHighlight);
	this->algorithmFluidSimLines->qfeSetLightSource(light);
	
	if(paramSeedsCount != this->seedCount)
	{    
		this->seedCount = paramSeedsCount;
		this->qfeInjectSeeds(this->visLines);
	}

	//if(this->simulationUpdated)
	//{ 
	//this->algorithmFluidSimLines->qfeAddSeedPositions(this->simulationParticlePositions, volume);  
	//this->algorithmFluidSimLines->qfeGeneratePathlines();
	//}

	if(paramLinesFirstVisible || paramLinesSecondVisible || paramSeedsVisible || paramSurfaceVisible)
	{  
		this->algorithmFluidSimLines->qfeRenderPathlineComparison(colormap, currentTime);    
	}

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFS::qfeRenderVisualFluidSimBoundary(qfeVisualSimBoundaryComparison *visual)
{
	qfeStudy           *study;
	qfeLightSource     *light;  
	qfeVolume          *volume;
	qfeColorMap        *colormap;
	int                 current;
	float               currentTime;

	if(visual == NULL) return qfeError;

	visual->qfeGetVisualStudy(&study);  
	visual->qfeGetVisualActiveVolume(current);
	visual->qfeGetVisualActiveVolume(currentTime);
	visual->qfeGetVisualColorMap(&colormap);
	this->scene->qfeGetSceneLightSource(&light);

	if(study == NULL) return qfeError;

	volume = study->pcap[current];

	// fetch the parameters  
	qfeVisualParameter *param;
	bool                paramArrowsVisible;
	int                 paramAngleFilter;

	visual->qfeGetVisualParameter("FS arrows visible", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramArrowsVisible);   

	visual->qfeGetVisualParameter("FS angle filter", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramAngleFilter);   

	this->algorithmFluidSimBoundary->qfeSetVolumeSeries(study->pcap, study->sim_velocities);
	this->algorithmFluidSimBoundary->qfeSetArrowsVisible(paramArrowsVisible);  
	this->algorithmFluidSimBoundary->qfeSetAngleFilter(paramAngleFilter);  
	this->algorithmFluidSimBoundary->qfeSetLightSource(light);

	if(paramArrowsVisible)
	{  
		this->algorithmFluidSimBoundary->qfeRenderArrowsComparison(colormap, currentTime);
	}

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFS::qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual)
{ 
	qfeStudy              *study;
	qfeVolume             *volume;
	qfeLightSource        *light;
	qfeModel              *model;  
	vtkPolyData           *mesh; 
	qfeMeshVertices       *vertices         = NULL;
	qfeMeshVertices       *normals          = NULL;
	qfeMeshIndices        *indicesTriangles = NULL;
	int                    current;

	visual->qfeGetVisualStudy(&study);  
	visual->qfeGetVisualActiveVolume(current);  

	this->scene->qfeGetSceneLightSource(&light);

	volume = study->pcap[current];

	if(volume == NULL)
	{
		cout << "qfeDriverFS::qfeRenderVisualAnatomyIllustrative - Incorrect volume data" << endl;
		return qfeError;
	}

	if(study == NULL)
	{
		cout << "qfeDriverFS::qfeRenderVisualAnatomyIllustrative - Incorrect study" << endl;
		return qfeError;
	}
		
	bool                surfaceClipping;

	// Get the parameters
	qfeSelectionList    paramSurfaceShadingList;
	int                 paramSurfaceShading;
	bool                paramSurfaceContour;

	qfeVisualParameter *param;  

	visual->qfeGetVisualParameter("Surface shading", &param);    
	if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceShadingList); 
	paramSurfaceShading       = paramSurfaceShadingList.active; 

	visual->qfeGetVisualParameter("Surface clipping", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(surfaceClipping);

	visual->qfeGetVisualParameter("Surface contour", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceContour); 

	bool                visible;  
	qfeColorRGB         colorSilhouette;
	qfeColorRGBA        colorContour;
	qfeColorRGBA        colorContourHidden;    

	// Process the parameter structures   
	colorContour.r       = 0.3; 
	colorContour.g       = 0.3;
	colorContour.b       = 0.3;
	colorContourHidden.r = 0.7;
	colorContourHidden.g = 0.7;
	colorContourHidden.b = 0.7;   

	// Set general variables for all meshes
	this->algorithmSurfaceShading->qfeSetLightSource(light);

	for(int i=0; i<(int)study->segmentation.size(); i++)
	{    
		// Get the model    
		if(i!=0)
		{
			model = study->segmentation[i];
			
			surfaceClipping = false;

			this->algorithmSurfaceShading->qfeSetSurfaceShading((qfeSurfaceShadingType)1);
		}
		else
		{
			model = new qfeModel();
			this->algorithmFluidSim->qfeGetSimulationModel(*model);

			qfeColorRGB color;
			std::string label;
			bool visible = true;

			study->segmentation[0]->qfeGetModelColor(color);    
			study->segmentation[0]->qfeGetModelLabel(label);
			study->segmentation[0]->qfeGetModelVisible(visible);

			model->qfeSetModelColor(color);
			model->qfeSetModelLabel(label);
			model->qfeSetModelVisible(visible);

			visual->qfeGetVisualParameter("Surface clipping", &param);
			if(param != NULL) 
				param->qfeGetVisualParameterValue(surfaceClipping);
			
			this->algorithmSurfaceShading->qfeSetSurfaceShading((qfeSurfaceShadingType)paramSurfaceShading);
		}

		// Get the parameters
		model->qfeGetModelColor(colorSilhouette);        

		// Get the mesh    
		model->qfeGetModelMesh(&mesh);    
		model->qfeGetModelMesh(&vertices, &normals);
		model->qfeGetModelMeshIndicesTriangles(&indicesTriangles);
		model->qfeGetModelVisible(visible);
		
		// Draw the mesh
		if(vertices != NULL && vertices->size() != 0 && visible)
		{
			// Update the surface parameters      
			this->algorithmSurfaceShading->qfeSetSurfaceColor(colorSilhouette);

			if(paramSurfaceContour) 
			{
				this->algorithmSurfaceShading->qfeSetSurfaceCulling(true);
				this->algorithmSurfaceShading->qfeRenderMeshDilate(vertices, normals, indicesTriangles, 0.2, volume);
			}  

			// Render the mesh
			
			this->algorithmSurfaceShading->qfeSetSurfaceCulling(surfaceClipping);
			this->algorithmSurfaceShading->qfeRenderMesh(vertices, normals, indicesTriangles, volume);            
		}
	}

	return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFS::qfeRenderVisualAides(qfeVisualAides *visual)
{
	qfeStudy           *study;
	qfeVolume          *volume;
	int                 current;

	qfeVisualParameter *param;
	bool                paramSupportBoundingBox = true;
	bool                paramSupportAxes        = false;

	visual->qfeGetVisualStudy(&study);  
	visual->qfeGetVisualActiveVolume(current);

	if(study == NULL) return qfeError;

	volume = study->pcap[current];

	visual->qfeGetVisualParameter("Support bounding box", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramSupportBoundingBox); 

	visual->qfeGetVisualParameter("Support axes", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramSupportAxes); 

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LINE_SMOOTH);

	if(paramSupportBoundingBox == true) qfeDriverAide::qfeRenderBoundingBoxDashed(volume, this->currentModelView, 0.8*this->frameBufferProps[0].supersampling);
	if(paramSupportAxes        == true) qfeDriverAide::qfeRenderVolumeAxes(volume, 10.0, 1.6*this->frameBufferProps[0].supersampling);

	glDisable(GL_LINE_SMOOTH);
	glDisable(GL_BLEND); 

	return qfeSuccess;
}

void qfeDriverFS::glEnable2D()
{
	int vPort[4];

	glGetIntegerv(GL_VIEWPORT, vPort);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glOrtho(0, vPort[2], 0, vPort[3], -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
}

void qfeDriverFS::glDisable2D()
{
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();   
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();	
}

qfeReturnStatus qfeDriverFS::qfeRenderVisualMetrics(qfeVisualMetrics *visual)
{		
	qfeViewport *viewport = new qfeViewport();
	this->engine->GetViewport3D(&viewport);
	unsigned int size = viewport->height/4;
	unsigned int vp_height = viewport->height;
	unsigned int vp_width  = viewport->width;
	unsigned int offset = 10;

	unsigned int fontSize = q2DRenderer->qfeGetFontSize();

	float currentPhase;
	this->visFluidSim->qfeGetVisualActiveVolume(currentPhase);  

	q2DRenderer->qfeClear2DText();

	qfeStudy *study;
	this->visFluidSim->qfeGetVisualStudy(&study);

	bool existing = true;
	float total = 0.0f;
	float average = 0.0f;	
	std::ostringstream stream;

	//*
	static bool computed = false;

	if(!computed)
	{
		/* dump all available metrics:
		unsigned int fileNumber = 0;
		for(unsigned int i = 0; i<study->pcap.size(); i++)
		{
			unsigned int steps = 1;
			for(unsigned int j = 0; j<steps; j++)
			{
				float phase = (float)i+((float)j/1);

				metrics->qfeStoreAllMetricData(study,&this->algorithmFluidSim->fluidsim->getSolidSurface(),phase,fileNumber);

				fileNumber++;
			}				
		}
		//*/

		/*
		for(unsigned int i = 0; i<study->pcap.size(); i++)
		{
			float average = 0.0f;
			float total = 0.0f;
			metrics->qfeGetScalarMetricStatistics("Velocity magnitude 1",study,&this->algorithmFluidSim->fluidsim->getSolidSurface(),i,existing,total,average);

			std::cout<<"Average velocity in mesh at phase "<<i<<" is "<<average<<std::endl;
		}
		*/

		/*
		for(unsigned int i = 0; i<study->pcap.size(); i++)
		{
			metrics->qfeStoreAllMetricData(study, &this->algorithmFluidSim->fluidsim->getSolidSurface(), i, i);
		}
		*/
		/*
		for(unsigned int i = 0; i<=study->pcap.size()-1; i++)
		{
			unsigned int steps = 10;
			for(unsigned int j = 0; j<steps; j++)
			{
				if(i==study->pcap.size()-1 && j>0)
					break;
				float offset = (float)j/(float)steps;

				float phase = (float)i+offset;

				metrics->qfeGetScalarMetricStatistics("Vel angle diff",study,&this->algorithmFluidSim->fluidsim->getSolidSurface(),phase,existing,total,average);
				std::cout<<"Angle diff at phase: "<<phase<<" average = "<<average<<std::endl;
			}
		}
		//*/
		/*
		for(unsigned int i = 0; i<=study->pcap.size()-1; i++)
		{
			unsigned int steps = 10;
			for(unsigned int j = 0; j<steps; j++)
			{
				if(i==study->pcap.size()-1 && j>0)
					break;
				float offset = (float)j/(float)steps;

				float phase = (float)i+offset;

				//metrics->qfeGetScalarMetricStatistics("Vel magnitude comparison",study,&this->algorithmFluidSim->fluidsim->getSolidSurface(),phase,existing,total,average);

				//std::cout<<"Magni diff at phase: "<<phase<<" = "<<average<<std::endl;

				
				float average1 = 0.0f; float average2 = 0.0f;
				float total1 = 0.0f; float total2 = 0.0f;
				metrics->qfeGetScalarMetricStatistics("Velocity 1",study,&this->algorithmFluidSim->fluidsim->getSolidSurface(),phase,existing,total1,average1);
				metrics->qfeGetScalarMetricStatistics("Velocity 2",study,&this->algorithmFluidSim->fluidsim->getSolidSurface(),phase,existing,total2,average2);

				std::cout<<"Average 1 = "<<average1<<" Average 2 = "<<average2<<" percent off = "<<(fabs(average1-average2))/average1<<std::endl;
				
				/*
				Array3Vec vel1;
				Array3Vec vel2;
				metrics->qfeGetVectorMetric("Velocity 1", study,&this->algorithmFluidSim->fluidsim->getSolidSurface(),phase,existing, vel1);
				metrics->qfeGetVectorMetric("Velocity 2", study,&this->algorithmFluidSim->fluidsim->getSolidSurface(),phase,existing, vel2);
				
				unsigned int count = this->algorithmFluidSim->fluidsim->particles.numberOfParticles;

				average = 0.0f;

				for(unsigned int p = 0; p<count; p++)
				{
					Vec3f sLocat = this->algorithmFluidSim->fluidsim->particles.x.at(p);
					
					std::vector<float> loc(3);
					loc[0] = vel1.ni * sLocat[0]/this->algorithmFluidSim->fluidsim->grid.ni;
					loc[1] = vel1.nj * sLocat[1]/this->algorithmFluidSim->fluidsim->grid.nj;
					loc[2] = vel1.nk * sLocat[2]/this->algorithmFluidSim->fluidsim->grid.nk;

					std::vector<float> velM = vel1.interpolate_value(loc);
					std::vector<float> velS = vel2.interpolate_value(loc);

					float magnM = vel1.vector_length(&velM);
					float magnS = vel1.vector_length(&velS);

					average+= magnM - magnS;
				}

				average /= (float)count;
				float average1 = 0.0f;
				float total1 = 0.0f;
				metrics->qfeGetScalarMetricStatistics("Velocity 1",study,&this->algorithmFluidSim->fluidsim->getSolidSurface(),phase,existing,total1,average1);
				std::cout<<phase<<" average: "<<average<<" metric "<<average<<" total "<<total<<std::endl;
				
			}
		}
		//*/

		computed = true;
	}
	//*/

	/*
	qfe2DBasicRendering::plot_data pdata;
	pdata.x_label = "time";
	pdata.y_label = "average velocity";
	metrics->qfeGetAverageOverTime("Velocity 1", study, &this->algorithmFluidSim->fluidsim->getSolidSurface(), pdata.data);
	q2DRenderer->qfeAdd2DPlot(0,0,0.5,pdata);

	std::cout<<"Start"<<std::endl;
	for(unsigned int i = 0; i<pdata.data.size(); i++)
	{
		std::cout<<pdata.data[i]<<std::endl;
	}
	std::cout<<"End"<<std::endl;
	*/

	/*
	metrics->qfeGetScalarMetricStatistics("Velocity 2", study, &this->algorithmFluidSim->fluidsim->getSolidSurface(), currentPhase, existing, total, average);
	stream<<"Total = "<<total<<" average = "<<average<<"\n";	
	q2DRenderer->qfeAdd2DText(vp_width/2.0f,viewport->height-15,&stream);

	stream.clear();
	metrics->qfeGetScalarMetricStatistics("Divergence 2", study, &this->algorithmFluidSim->fluidsim->getSolidSurface(), currentPhase, existing, total, average);
	stream<<"Total = "<<total<<" average = "<<average<<"\n";	
	q2DRenderer->qfeAdd2DText(vp_width/2.0f,viewport->height-30,&stream);
	*/

	//metrics->qfeRenderVectorMetric("Velocity 1",study, &this->algorithmFluidSim->fluidsim->getSolidSurface(), currentPhase);
	
	//metrics->qfeRenderVectorMetric("Velocity 2",study,&this->algorithmFluidSim->fluidsim->getSolidSurface(),currentPhase);	
	//metrics->qfeRenderScalarMetric("WSS magnitude 1", study, &this->algorithmFluidSim->fluidsim->getSolidSurface(), currentPhase);

	/*
	if(this->fluidInteractors.getNumberOfInteractors()>1)
	{
		Array3b validCells = this->fluidInteractors.qfeGetValidFluidCells();
		qfeMatrix4f V2P,T2V;
		qfeTransform::qfeGetMatrixVoxelToPatient(V2P, study->pcap[0]);
		qfeTransform::qfeGetMatrixTextureToVoxel(T2V, study->pcap[0]);
		for(unsigned int k = 0; k<validCells.nk; k++)
			for(unsigned int j = 0; j<validCells.nj; j++)
				for(unsigned int i = 0; i<validCells.ni; i++)
				{
					float solid = this->algorithmFluidSim->fluidsim->getSolidSurface().at(i,j,k);
					if(solid>0.0f && !validCells(i,j,k))
					{
						qfePoint center((float)i/validCells.ni,(float)j/validCells.nj,(float)k/validCells.nk);
						qfePoint point = (center * T2V) * V2P;
					
						glPointSize(3.0f);
					
						glBegin(GL_POINTS);
							glColor3f(1.0f, 0.0f, 0.0f);
							glVertex3f(point.x, point.y, point.z);
						glEnd();
					}
				}	
	}
	*/

	/*
	static float last_checked_phase = -1;
	if(last_checked_phase != currentPhase)
	{
		last_checked_phase = currentPhase;

		Array3b validCells = this->fluidInteractors.qfeGetValidFluidCells();

		Array3f vel1;
		Array3f vel2;

		Array3f solid = this->algorithmFluidSim->fluidsim->grid.solid_phi;
		bool existing_metric = false;
		float range_min = 0.0f; float range_max = 0.0f;

		metrics->qfeGetScalarMetric("Velocity 1", study, &solid, currentPhase, existing_metric, vel1, range_min, range_max);	
		metrics->qfeGetScalarMetric("Velocity 2", study, &solid, currentPhase, existing_metric, vel2, range_min, range_max);	

		float total1 = 0.0f;
		float total2 = 0.0f;
		float average1 = 0.0f;
		float average2 = 0.0f;

		unsigned int valid_cells = 0;

		for(unsigned int k = 0; k<vel1.nk; k++)
		for(unsigned int j = 0; j<vel1.nj; j++)
		for(unsigned int i = 0; i<vel1.ni; i++)
		{
			float si = (float)i/vel1.ni * (float)solid.ni;
			float sj = (float)j/vel1.nj * (float)solid.nj;
			float sk = (float)k/vel1.nk * (float)solid.nk;
		
			if(solid.interpolate_value(si,sj,sk) > 0 && validCells(si,sj,sk)) //we are not in the solid, so this cell should be counted
			{
				valid_cells++;

				total1 += vel1.at(i,j,k);
				total2 += vel2.at(i,j,k);
			}
		}

		average1 = total1/(float)valid_cells;
		average2 = total2/(float)valid_cells;

		std::cout<<"Velocity 1"<<" has an average value of "<<average1<<" and a total of "<<total1<<" within the valid region"<<std::endl;
		std::cout<<"Velocity 2"<<" has an average value of "<<average2<<" and a total of "<<total2<<" within the valid region"<<std::endl;
	}
	*/

	qfeFluidInteractor *interactor = this->fluidInteractors.qfeGetFluidInteractorSelector();
	if(interactor != NULL && interactor->getType() == qfeFluidInteractor::INSPECTOR)
	{			
		interactor->setSolid(&this->algorithmFluidSim->fluidsim->getSolidSurface());

		unsigned int image_count = 0;

		qfeVisualParameter *param;

		int param_count = 0;
		visual->qfeGetVisualParameterCount(param_count);

		qfeSelectionList paramList;

		visual->qfeGetVisualParameter(0, &param);

		param->qfeGetVisualParameterValue(paramList);

		unsigned int selectedMetricID = paramList.active;
		std::string main_metric_label = paramList.list.at(paramList.active);
		
		GLfloat *image;
		
		if(this->fluidInteractors.creatorEnabled() && study->qfeScalarMetricExists(main_metric_label))
		{
			image_count = 1;
						
			unsigned int color_scheme_id = 0;

			if(study->qfeGetScalarMetricProperties(main_metric_label).diverging)
				color_scheme_id = colorMaps.qfeGetIDDivergingColorScheme();
			else
				color_scheme_id = colorMaps.qfeGetIDIncreasingColorScheme();

			float range_min = 0.0f;
			float range_max = 0.0f;

			this->qfeRetrieveInspectionImage(interactor, main_metric_label, currentPhase, image, size, color_scheme_id, range_min, range_max);

			/*TODO enable again
			q2DRenderer->qfeAdd2DText(25,	  15,		range_min);
			q2DRenderer->qfeAdd2DText(size-25,15,		range_max);
			q2DRenderer->qfeAdd2DText(size/2, size-15,	main_metric_label);
			*/
		}
		if(!this->fluidInteractors.creatorEnabled()) //an existing selector is chosen, so we can show all metrics selected
		{
			static float previousPhase = -1.0f;
			if(previousPhase != currentPhase)
			{
				previousPhase = currentPhase;

				std::vector<float> normal(3);
				normal[0] = interactor->getNormal().x;
				normal[1] = interactor->getNormal().y;
				normal[2] = interactor->getNormal().z;
				bool existing_metric = false;

				/*
				float flux = 0.0f;
				metrics->qfeGetMetricFluxInCells("Velocity 1", study, 
					&this->algorithmFluidSim->fluidsim->getSolidSurface(), 
					&fluidInteractors.qfeGetFluidInteractorCells(interactor), 
					normal,
					currentPhase, existing_metric, flux);
				std::cout<<"Flux data set 1 through this interactor "<<flux<<std::endl;

				flux = 0.0f;
				metrics->qfeGetMetricFluxInCells("Velocity 2", study, 
					&this->algorithmFluidSim->fluidsim->getSolidSurface(), 
					&fluidInteractors.qfeGetFluidInteractorCells(interactor), 
					normal,
					currentPhase, existing_metric, flux);
				std::cout<<"Flux data set 2 through this interactor "<<flux<<std::endl;
				*/
			}

			std::vector<qfeStudy::metric_descriptor> metricList;
			
			study->qfeGetScalarMetricList(metricList);

			unsigned int param_id = 0;

			std::vector<std::pair<unsigned int, unsigned int>> metricsToDraw;

			for(unsigned int p = 0; p<(unsigned int)param_count; p++)
			{
				visual->qfeGetVisualParameter(p, &param);
				
				param->qfeGetVisualParameterValue(paramList);

				if(paramList.list.size()>0 && paramList.list[0].compare("Disable") == 0) //if the param is a metric
				{
					if(paramList.active != 0) //if it should be drawn
					{
						std::string label = metricList.at(param_id).name;
						if(study->qfeScalarMetricExists(label)) // if the metric exists
						{
							std::pair<unsigned int, unsigned int> pair;
							pair.first = param_id;				//metric ID
							pair.second = paramList.active-1;	//color map ID
							metricsToDraw.push_back(pair);
						}
					}
					param_id++;
				}
			}

			image_count = (unsigned int) metricsToDraw.size();

			image = new GLfloat[image_count*size*size*4]();

			GLfloat *image_local;

			unsigned int offset = size*size*4;

			for(unsigned int p = 0; p<image_count; p++)
			{
				unsigned int metricID = metricsToDraw.at(p).first;
				unsigned int color_scheme_id = metricsToDraw.at(p).second;

				std::string label = metricList.at(metricID).name;

				float range_min = 0.0f;
				float range_max = 0.0f;
				
				this->qfeRetrieveInspectionImage(interactor, label, currentPhase, image_local, size, color_scheme_id, range_min, range_max);

				/*
				float total = 0.0f;
				bool existing_metric = false;
				metrics->qfeGetMetricTotalInCells(label, study, 
					&this->algorithmFluidSim->fluidsim->grid.solid_phi,
					&fluidInteractors.qfeGetFluidInteractorCells(interactor), currentPhase, existing_metric, total);
				if(existing_metric)
					std::cout<<label<<" of interactor "<<interactor->getID()<<" has a total value of "<<total<<std::endl;

				string velocityLabel = "Velocity";
				if(velocityLabel.compare(label.substr(0,8)) == 0)
				{
					std::vector<float> normal(3);
					normal[0] = interactor->getNormal().x;
					normal[1] = interactor->getNormal().y;
					normal[2] = interactor->getNormal().z;

					metrics->qfeGetMetricFluxInCells(label, study, 
						&this->algorithmFluidSim->fluidsim->grid.solid_phi,
						&fluidInteractors.qfeGetFluidInteractorCells(interactor), 
						normal,
						currentPhase, existing_metric, total);
					if(existing_metric)
						std::cout<<"The oriented magnitude of interactor "<<interactor->getID()<<" has a total value of "<<total<<std::endl;
				}
				//*/

				/*TODO enable again
				q2DRenderer->qfeAdd2DText(25,	  15+p*size,		range_min);
				q2DRenderer->qfeAdd2DText(size-25,15+p*size,		range_max);
				q2DRenderer->qfeAdd2DText(size/2, size-15+p*size,	label);
				*/
				
				for(unsigned int i = 0; i<size; i++)//height
				{
					for(unsigned int j = 0; j<size; j++)//width 
					{
						image[offset*p+0+4*(i+size*j)] = image_local[0+4*(i+size*j)];
						image[offset*p+1+4*(i+size*j)] = image_local[1+4*(i+size*j)];
						image[offset*p+2+4*(i+size*j)] = image_local[2+4*(i+size*j)];
						image[offset*p+3+4*(i+size*j)] = image_local[3+4*(i+size*j)];
					}
				}

				delete[size*size*4] image_local;
			}
		}

		unsigned int id = 0;

		if(image_count>0)
		{
			glEnable( GL_ALPHA_TEST );
			glDrawPixels(size,image_count*size,GL_RGBA,GL_FLOAT,image);
			glDisable( GL_ALPHA_TEST );
			
			delete[image_count*size*size*4] image;
		}
	}

	qfeVisualParameter *param;
	int paramValueInt = 0;
	visual->qfeGetVisualParameter("Iso value (%)",&param);
	if(param != NULL) 
		param->qfeGetVisualParameterValue(paramValueInt); //given in percentages
	float paramValue = (float)paramValueInt/100.0f;

	qfeSelectionList paramList;
	visual->qfeGetVisualParameter("Iso surface", &param);
	if(param != NULL) param->qfeGetVisualParameterValue(paramList); 
	
	std::string str_label = paramList.list.at(paramList.active);
	
	char* iso_label = &str_label[0u];

	if(currentIsoPhase != currentPhase)
	{
		currentIsoPhase = currentPhase;
		isoMeshUpToDate = false;
	}

	if(currentIsoValue != paramValue || currentIsoLabel.compare(str_label)!=0)
	{
		currentIsoValue = paramValue;
		currentIsoLabel = str_label;
		isoMeshUpToDate = false;
	}

	if(!isoMeshUpToDate)
	{
		Array3f input;

		float range_min = 0.0f;
		float range_max = 0.0f;

		bool existing_metric = false;

		qfeStudy *study;
		this->visFluidSim->qfeGetVisualStudy(&study);
	
		study->qfeGetScalarMetric(iso_label, &this->algorithmFluidSim->fluidsim->getSolidSurface(), currentPhase, existing_metric, input, range_min, range_max);

		if(existing_metric)
		{
			float isoValue = (range_max-range_min)*currentIsoValue+range_min;

			std::ostringstream buff;
			buff<<"Used iso value is "<<isoValue;

			this->engine->SetStatusMessage(buff.str());

			this->qfeCreateIsoSurface(&input, isoValue);
		}
		else
			this->qfeCreateIsoSurface(&input, 0.0f); //empty iso field
	}

	return qfeSuccess;
}

qfeReturnStatus qfeDriverFS::qfeRetrieveInspectionImage(qfeFluidInteractor *interactor, string label, float currentPhase, GLfloat* &image, unsigned int size, unsigned int colorMapId, float &range_min, float &range_max)
{
	bool exists = interactor->textureExists(label,currentPhase,image,size,range_min,range_max,colorMapId);
	if(exists)
	{
		return qfeSuccess;
	}
		
	Array3f input;

	qfeColorMap colorMap;
	string colorMapName;
	colorMaps.qfeGetColorMap(colorMapId, colorMapName, colorMap);
	
	qfeStudy *study;
	this->visFluidSim->qfeGetVisualStudy(&study);
	
	bool existing_metric = study->qfeScalarMetricExists(label);
		
	if(existing_metric)
	{
		study->qfeGetScalarMetric(label, &this->algorithmFluidSim->fluidsim->getSolidSurface(), currentPhase, existing_metric, input, range_min, range_max);
		Array3f solid = this->algorithmFluidSim->fluidsim->getSolidSurface();
		interactor->setSolid(&solid);
		interactor->mapTo2D(&input,label,currentPhase,image,size,range_min,range_max,&colorMap,colorMapId);

		/*
		float total = 0.0f;
		metrics->qfeGetMetricTotalInCells(label, study, 
			&this->algorithmFluidSim->fluidsim->grid.solid_phi, 
			&fluidInteractors.qfeGetFluidInteractorCells(interactor), 
			currentPhase, existing_metric, total);
		std::cout<<label<<" total amount in this interactor "<<total<<std::endl;
		*/
	}
	else //make it clear the data is not there
	{
		image = new GLfloat[size*size*4]();

		for(unsigned int i = 0; i<size; i++)//height
		{				
			for(unsigned int j = 0; j<size; j++)//width 
			{
				image[0+4*(i+size*j)] = 1.0f;
				image[1+4*(i+size*j)] = 1.0f;
				image[2+4*(i+size*j)] = 1.0f;

				if((i+j)%20<10 || ((size-i)+j)%20<10) //lines
				{
					image[0+4*(i+size*j)] = (i+j)%10<5;
					image[1+4*(i+size*j)] = 0.0f;
					image[2+4*(i+size*j)] = 0.0f;
				}
				image[3+4*(i+size*j)] = i%2 ==  j%2;
			}
		}

		range_min = 0.0f;
		range_max = 0.0f;
	}

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFS::qfeCreateIsoSurface(Array3f *input, const float isoValue)
{ 
	qfeVolume             *volume;
	qfeLightSource        *light;
	qfeModel              *model;  
	vtkPolyData           *mesh; 
	qfeMeshVertices       *vertices         = NULL;
	qfeMeshVertices       *normals          = NULL;
	qfeMeshIndices        *indicesTriangles = NULL;
	int                    current;

	qfeStudy     *study;

	this->visFluidSim->qfeGetVisualStudy(&study);

	qfeVolumeSeries *volser1 = &study->pcap;

	this->visFluidSim->qfeGetVisualActiveVolume(current);  

	this->scene->qfeGetSceneLightSource(&light);

	volume = study->pcap[current];

	if(volume == NULL)
	{
		cout << "qfeDriverFS::qfeRenderIsoSurface - Incorrect volume data" << endl;
		return qfeError;
	}

	if(study == NULL)
	{
		cout << "qfeDriverFS::qfeRenderIsoSurface - Incorrect study" << endl;
		return qfeError;
	}

	// Get the parameters
	qfeSelectionList    paramSurfaceShadingList;
	int                 paramSurfaceShading;
	bool                paramSurfaceClipping;
	bool                paramSurfaceContour;

	paramSurfaceShading       = 3; 

	paramSurfaceClipping = false;
	paramSurfaceContour = true; 

	qfeColorRGB         colorSilhouette;
	qfeColorRGBA        colorContour;
	qfeColorRGBA        colorContourHidden;    

	// Process the parameter structures   
	colorContour.r       = 0.3; 
	colorContour.g       = 0.3;
	colorContour.b       = 0.3;
	colorContourHidden.r = 0.7;
	colorContourHidden.g = 0.7;
	colorContourHidden.b = 0.7;   

	// Set general variables for all meshes
	this->algorithmSurfaceShading->qfeSetLightSource(light);
	this->algorithmSurfaceShading->qfeSetSurfaceCulling(paramSurfaceClipping);
	this->algorithmSurfaceShading->qfeSetSurfaceShading((qfeSurfaceShadingType)paramSurfaceShading);

	// Get the model    
	study->qfeGetIsoSurface(volume,&this->algorithmFluidSim->fluidsim->getSolidSurface(),input,isoValue,&model);
	
	qfeColorRGB color;
	std::string label = "Iso surface";
	bool visible = true;
	
	color.r = 0.49f;    
	color.g = 1.00f;
	color.b = 0.62f;
		
	// Get the parameters
	model->qfeGetModelColor(colorSilhouette); 
		
	// Get the mesh    
	model->qfeGetModelMesh(&mesh);   
	model->qfeGetModelMesh(&vertices, &normals);
	model->qfeGetModelMeshIndicesTriangles(&indicesTriangles);
	model->qfeGetModelVisible(visible);
	
	if(isoMeshCreated)
	{
		unsigned int index = (unsigned int) study->segmentation.size()-1;

		study->segmentation[index]->qfeGetModelColor(color);
		study->segmentation[index]->qfeGetModelLabel(label);
		study->segmentation[index]->qfeGetModelVisible(visible);

		model->qfeSetModelColor(color);  
		model->qfeSetModelLabel(label);
		model->qfeSetModelVisible(visible);

		study->segmentation.at(study->segmentation.size()-1) = model;
	}
	else
	{
		model->qfeSetModelColor(color);
		model->qfeSetModelLabel(label);
		model->qfeSetModelVisible(visible);

		study->segmentation.push_back(model);
		isoMeshCreated = true;
	}

	isoMeshUpToDate = true;

	return qfeSuccess;
}

//----------------------------------------------------------------------------
float qfeDriverFS::bezierWeighting(float t)
{
	assert(t>=0.0f && t<=1.0f);

	float b04 = 1.0f * pow(t,0.0f) * pow((1-t),4);
	float b14 = 4.0f * pow(t,1.0f) * pow((1-t),3);
	float b24 = 6.0f * pow(t,2.0f) * pow((1-t),2);
	float b34 = 4.0f * pow(t,3.0f) * pow((1-t),1);
	float b44 = 1.0f * pow(t,4.0f) * pow((1-t),0);

	float w0 = 0;
	float w1 = 0;
	float w2 = 0.5;
	float w3 = 1;
	float w4 = 1;

	float result = w0*b04 + w1*b14 + w2*b24 + w3*b34 + w4*b44;

	assert(result>= 0.0f && result<=1.0f);

	return result;
}

//----------------------------------------------------------------------------
void qfeDriverFS::qfeOnAction(void *caller, int t)
{
	qfeDriverFS *self = reinterpret_cast<qfeDriverFS *>(caller);

	switch(t)
	{
	case qfeVisualFluid::actionFluidSimPreprocess  :   
		self->qfePreprocessFluidSimulation();
		break;  
	case qfeVisualFluid::actionFluidSimSetSource   :
		self->source_selection_enabled = !self->source_selection_enabled;
		self->sink_selection_enabled = false;
		self->inspector_enabled = false;
		break;
	case qfeVisualFluid::actionFluidSimSetSink   :
		self->sink_selection_enabled = !self->sink_selection_enabled;
		self->source_selection_enabled = false;
		self->inspector_enabled = false;
		break;
	case qfeVisualFluid::actionFluidSimInspect   :
		self->inspector_enabled = !self->inspector_enabled;
		self->source_selection_enabled = false;
		self->sink_selection_enabled = false;
	case qfeVisualFluid::actionFluidSimRemoveInteractor :
		self->source_selection_enabled = false;
		self->sink_selection_enabled = false;
		self->qfeRemoveFluidInteractor();
		break;	
	case qfeVisualFluid::actionFluidSimComputeFlux :
		self->qfeComputeFlux(self->visFluidSim);
		break;  
	case qfeVisualSimLinesComparison::actionInjectSeeds :
		//self->qfeInjectSeeds(self->visLines);
		self->qfeInjectSeeds(self->visBoundary);
		break;
	case qfeVisualSimLinesComparison::actionClearSeeds :
		//self->qfeClearSeeds(self->visLines);
		self->qfeClearSeeds(self->visBoundary);
		break;  
	}  

	if(self->source_selection_enabled)	 
		self->engine->SetStatusMessage("Press control and click to set a fluid source");
	else if(self->sink_selection_enabled)
		self->engine->SetStatusMessage("Press control and click to set a fluid sink");
	else if(self->inspector_enabled)
		self->engine->SetStatusMessage("Press control and click inpect the fluid");

	self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverFS::qfeOnResize(void *caller)
{
	qfeDriverFS *self = reinterpret_cast<qfeDriverFS *>(caller);

	qfeViewport *vp;

	self->engine->GetViewport3D(&vp);

	qfeDriver::qfeDriverResize(caller);

	self->algorithmFluidSim->qfeSetViewportSize(vp->width, vp->height, self->frameBufferProps[0].supersampling);
}

//----------------------------------------------------------------------------
void qfeDriverFS::qfeOnMouseMove(void *caller, int x, int y, int v)
{
	qfeDriverFS *self = reinterpret_cast<qfeDriverFS *>(caller);

	qfeStudy     *study;
	qfeColorRGB   color;
	qfeViewport  *vp3D;
	qfeVolume    *volume;
	qfeMatrix4f   P2V, T2V, V2P, V2T;
	unsigned int  nx, ny, nz;
	int           current;

	self->engine->GetViewport3D(&vp3D);
	self->qfeReadSelect(x-vp3D->origin[0], y-vp3D->origin[1], color);

	self->visFluidSim->qfeGetVisualStudy(&study);
	self->visFluidSim->qfeGetVisualActiveVolume(current);

	if(study == NULL) return;

	volume = study->pcap[current];
	volume->qfeGetVolumeSize(nx, ny, nz);

	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);  
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);

	if(
		(self->engine->GetInteractionControlKey() && (self->source_selection_enabled || self->sink_selection_enabled || self->inspector_enabled)) || 
		(self->rightMouseDown && self->fluidInteractors.getSelectedInteractorIndex() != 0)
		) 
	{
		unsigned int selectedIndex = self->fluidInteractors.getSelectedInteractorIndex();
		bool interaction = self->rightMouseDown && selectedIndex != 0;

		qfePoint simCoord;

		float value = 0.0f;

		self->qfePickPoint(x,y,vp3D,volume, value, simCoord);

		if(value > 0.0f)   
		{
			qfeFluidInteractor *interactor = new qfeFluidInteractor();

			qfeFluidInteractor::InteractorType type;
			if(self->source_selection_enabled)
				type = qfeFluidInteractor::SOURCE_SELECTION;
			else if(self->sink_selection_enabled)
				type = qfeFluidInteractor::SINK_SELECTION;
			else if(self->inspector_enabled)
				type = qfeFluidInteractor::INSPECTOR;

			qfeReturnStatus status = qfeSuccess;

			if(!interaction) 
				status = self->algorithmFluidSim->qfeComputeFluidInteractor(simCoord, type, *interactor);
			else //use the existing interactor
				interactor = self->fluidInteractors.qfeGetFluidInteractor(self->fluidInteractors.getSelectedInteractorIndex());

			if(status == qfeSuccess)
			{
				if(interaction && interactor != NULL)
				{
					// Get the view vector
					qfeMatrix4f Minv;
					qfeMatrix4f::qfeGetMatrixInverse(self->currentModelView, Minv);

					qfeVector eye;
					eye.qfeSetVectorElements(0.0,0.0,-1.0);
					eye = eye*Minv;
					qfeVector::qfeVectorNormalize(eye);

					qfeVector rotationAxis = eye; //find better vector
					qfeVector::qfeVectorNormalize(rotationAxis);

					qfeVector normal = interactor->getNormal();
					qfeVector axis1 = interactor->getAxis1();
					qfeVector axis2 = interactor->getAxis2();

					//TODO get view up somehow...
					float angle = (self->mouse_x-x + self->mouse_y-y);

					normal = self->rotateVectorAroundAxis(normal, rotationAxis, angle);
					axis1 = self->rotateVectorAroundAxis(axis1, rotationAxis, angle);
					axis2 = self->rotateVectorAroundAxis(axis2, rotationAxis, angle);

					interactor->setNormal(normal);
					interactor->setAxis(axis1, axis2);
				}
				Vec3ui new_center(0,0,0);
				Vec3f normal(0,0,0);
				self->centerline->qfeGetNearestCenterlinePoint(Vec3ui(simCoord.x,simCoord.y,simCoord.z),new_center);
				//convert to voxel coordinates for convenience
				qfePoint texCoord;
				texCoord.x = (float)new_center[0]/self->algorithmFluidSim->fluidsim->grid.ni;
				texCoord.y = (float)new_center[1]/self->algorithmFluidSim->fluidsim->grid.nj;
				texCoord.z = (float)new_center[2]/self->algorithmFluidSim->fluidsim->grid.nk;	
				qfePoint voxCoord = (texCoord * T2V);
				interactor->setOrigin(voxCoord);

				interactor->setV2PMatrix(V2P);
				interactor->setV2TMatrix(V2T);
				interactor->setT2VMatrix(T2V); 
				interactor->setSolid(&self->algorithmFluidSim->fluidsim->getSolidSurface());

				self->fluidInteractors.enableCreator(interactor);
				self->q2DRenderer->qfeClear2DText();

				self->scene->qfeSelectionUpdate();
			}
		}
		self->engine->Refresh();
	}
	else
	{
		if(self->fluidInteractors.qfeGetFluidInteractorSelector() != NULL)
		{
			//self->fluidInteractors.disableCreator();
			//self->q2DText->qfeClear2DText();
			self->scene->qfeSelectionUpdate();
			self->engine->Refresh();
		}
		self->engine->CallInteractionParentMouseMove();
	}
}

//----------------------------------------------------------------------------
void qfeDriverFS::qfeOnMouseRightButtonDown(void *caller, int x, int y, int v)
{
	qfeDriverFS *self = reinterpret_cast<qfeDriverFS *>(caller);

	self->rightMouseDown = true;

	self->mouse_x = x;
	self->mouse_y = y;

	if(self->engine->GetInteractionControlKey())
	{
	}
	else
	{
	}  
}

//----------------------------------------------------------------------------
void qfeDriverFS::qfeOnMouseRightButtonUp(void *caller, int x, int y, int v)
{
	qfeDriverFS *self = reinterpret_cast<qfeDriverFS *>(caller);

	if(self->rightMouseDown)
	{
		qfeFluidInteractor *interactor = self->fluidInteractors.qfeGetFluidInteractorSelector();
		if(interactor != NULL)
		{
			self->qfeRemoveFluidInteractor(); //remove the selected interactor

			self->fluidInteractors.add(*interactor);

			Array3b cells = self->fluidInteractors.qfeGetFluidInteractorCells(interactor);
			for(unsigned int k = 0; k<cells.nk; k++) for(unsigned int j = 0; j<cells.nj; j++) for(unsigned int i = 0; i<cells.ni; i++)
			{
				if(cells(i,j,k) == true) //add cell to simulation
				{
					if(interactor->getType() == qfeFluidInteractor::SOURCE)
					{
						self->algorithmFluidSim->fluidsim->setSourceCell(i,j,k);
					}
					if(interactor->getType() == qfeFluidInteractor::SINK)
					{
						self->algorithmFluidSim->fluidsim->setSinkCell(i,j,k);
					}
				}
			}

			self->fluidInteractors.selectLastCreated();

			self->engine->SetStatusMessage("Click the delete button to remove the selected fluid interactor");
			self->engine->Refresh();
		}
	}

	self->rightMouseDown = false;

	if(self->engine->GetInteractionControlKey())
	{
	}
	else
	{
	}  
}

//----------------------------------------------------------------------------
void qfeDriverFS::qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v)
{
	qfeDriverFS *self = reinterpret_cast<qfeDriverFS *>(caller); 

	self->fluidInteractors.disableCreator();

	qfeStudy     *study;
	qfeColorRGB   color;
	qfeViewport  *vp3D;
	qfeVolume    *volume;
	qfeMatrix4f   P2V;
	qfeMatrix4f	V2P;
	qfeMatrix4f   T2V;
	qfeMatrix4f	V2T;
	unsigned int  nx, ny, nz;
	int           current;

	self->engine->GetViewport3D(&vp3D);
	self->qfeReadSelect(x-vp3D->origin[0], y-vp3D->origin[1], color);

	self->visFluidSim->qfeGetVisualStudy(&study);
	self->visFluidSim->qfeGetVisualActiveVolume(current);

	if(study == NULL) return;

	volume = study->pcap[current];
	volume->qfeGetVolumeSize(nx, ny, nz);

	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);  
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);

	qfePoint simCoord;

	float value = 0.0f;

	if(self->engine->GetInteractionControlKey() && (self->source_selection_enabled || self->sink_selection_enabled || self->inspector_enabled)) 
	{
		self->qfePickPoint(x,y,vp3D,volume, value, simCoord);

		qfeFluidInteractor *interactor = new qfeFluidInteractor();

		if(value > 0.0f)  
		{
			qfeFluidInteractor::InteractorType type;
			if(self->source_selection_enabled)
				type = qfeFluidInteractor::SOURCE;
			else if(self->sink_selection_enabled)
				type = qfeFluidInteractor::SINK;
			else if(self->inspector_enabled)
				type = qfeFluidInteractor::INSPECTOR;

			qfeReturnStatus status = self->algorithmFluidSim->qfeComputeFluidInteractor(simCoord, type, *interactor);

			Vec3ui new_center(0,0,0);
			self->centerline->qfeGetNearestCenterlinePoint(Vec3ui(simCoord.x,simCoord.y,simCoord.z),new_center);
			//convert to voxel coordinates for convenience
			qfePoint texCoord;
			texCoord.x = (float)new_center[0]/self->algorithmFluidSim->fluidsim->grid.ni;
			texCoord.y = (float)new_center[1]/self->algorithmFluidSim->fluidsim->grid.nj;
			texCoord.z = (float)new_center[2]/self->algorithmFluidSim->fluidsim->grid.nk;		  
			qfePoint voxCoord = (texCoord * T2V);
			interactor->setOrigin(voxCoord);

			interactor->setV2PMatrix(V2P);
			interactor->setV2TMatrix(V2T);
			interactor->setT2VMatrix(T2V);
			interactor->setSolid(&self->algorithmFluidSim->fluidsim->getSolidSurface());

			if(status == qfeSuccess)
			{			  
				self->fluidInteractors.add(*interactor);
				
				//for hardcoding interactors:
				std::cout<<"----------------------------------------"<<std::endl;
				qfePoint center = interactor->getOrigin();
				std::cout<<"Center: ("<<center.x<<","<<center.y<<","<<center.z<<std::endl;
				std::cout<<"Radius: "<<interactor->getRadius()<<std::endl;
				qfeVector normal = interactor->getNormal();
				std::cout<<"Normal: ("<<normal.x<<","<<normal.y<<","<<normal.z<<std::endl;
				qfeVector axis = interactor->getAxis1();
				std::cout<<"Axis 1: ("<<axis.x<<","<<axis.y<<","<<axis.z<<std::endl;
				axis = interactor->getAxis2();
				std::cout<<"Axis 2: ("<<axis.x<<","<<axis.y<<","<<axis.z<<std::endl;
				std::cout<<"Type  : "<<interactor->getType()<<std::endl;
				std::cout<<"DX    : "<<interactor->getdx();
				std::cout<<"----------------------------------------"<<std::endl;

				Array3b cells = self->fluidInteractors.qfeGetFluidInteractorCells(interactor);
				for(unsigned int k = 0; k<cells.nk; k++) for(unsigned int j = 0; j<cells.nj; j++) for(unsigned int i = 0; i<cells.ni; i++)
				{
					if(cells(i,j,k) == true) //add cell to simulation
					{
						if(interactor->getType() == qfeFluidInteractor::SOURCE)
						{
							self->algorithmFluidSim->fluidsim->setSourceCell(i,j,k);
						}
						if(interactor->getType() == qfeFluidInteractor::SINK)
						{
							self->algorithmFluidSim->fluidsim->setSinkCell(i,j,k);
						}
					}
				}

				self->fluidInteractors.selectLastCreated();

				self->engine->SetStatusMessage("Click the delete button to remove the selected fluid interactor");

				self->scene->qfeSelectionUpdate();
			}
		}
		self->engine->Refresh();
	}
	else
	{	  
		unsigned int id = self->qfeSelectInteractor(x,y,vp3D,volume, simCoord);
		if(id != 0)
		{
			self->fluidInteractors.setSelectedInteractorIndex(id);
			self->engine->SetStatusMessage("Click the delete button to remove the selected fluid interactor");
			self->engine->Refresh();
		}
		else
		{
			self->fluidInteractors.setSelectedInteractorIndex(0); // make sure none is selected		  
			self->engine->SetStatusMessage("");
		}

		self->engine->CallInteractionParentLeftButtonDown(); 
	}
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverFS::qfePickPoint(int windowX, int windowY, qfeViewport *viewport, qfeVolume *volume, float &value, qfePoint &simPos)
{
	qfeMatrix4f P2V, V2T, Minv;
	qfePoint    windowPos, worldPos, patientPos, voxelPos, texPos;
	qfePoint   *intersectPos;
	qfePoint    q, p1f, p1b, swap;
	unsigned int         count;
	float       depth;
	float       depthDelta = 0.01;
	float       stepSize;
	unsigned int         steps;

	float maximum = -1e12f;

	Array3f	  solidSurface = this->algorithmFluidSim->fluidsim->getSolidSurface();
	unsigned int i,j,k;
	float fx,fy,fz;

	if(viewport == NULL || volume == NULL) return qfeError;

	// Get the volume data 
	//volume->qfeGetVolumeData(t, dims[0], dims[1], dims[2], &data);

	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);

	// Get the view vector
	qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, Minv);

	qfeVector eye;
	eye.qfeSetVectorElements(0.0,0.0,-1.0);
	eye = eye*Minv;
	qfeVector::qfeVectorNormalize(eye);

	// Unproject the center in patient coordinates to window coordinates
	// We don't know the depth a priori, but we need a reasonable estimate
	// before we determine the patient position by searching the maximum
	qfePoint o, windowOrigin;
	o.qfeSetPointElements(0.0,0.0,0.0);
	qfeTransform::qfeGetCoordinateProject(o, windowOrigin, this->currentModelView, this->currentProjection, viewport);

	// Obtain the coordinates
	windowPos.x = windowX;
	windowPos.y = windowY;  
	windowPos.z = windowOrigin.z;

	// Unproject to patient (/world) coordinates
	qfeTransform::qfeGetCoordinateUnproject(windowPos, worldPos, this->currentModelView, this->currentProjection, viewport);

	worldPos.x = worldPos.x/worldPos.w;
	worldPos.y = worldPos.y/worldPos.w;
	worldPos.z = worldPos.z/worldPos.w;

	// Compute the intersections with the bounding box
	this->qfeGetIntersectionsBoxLine(volume, worldPos, worldPos+eye, &intersectPos, count);

	if(count < 2) return qfeError;

	// Assign the intersection point and sort them according to their depth
	p1f = intersectPos[0];
	p1b = intersectPos[1];

	if((p1f*this->currentModelView).z < (p1b*this->currentModelView).z)
	{
		swap = p1f;
		p1f  = p1b;
		p1b  = swap;
	}

	patientPos = p1f;

	// Initialize parameters
	qfeVector::qfeVectorLength(p1f-p1b, depth);
	stepSize      = 0.3f;  
	steps         = floor(abs(depth) / stepSize);  

	for(unsigned int step=0; step<steps; step++)
	{      
		float currentValue = -1e-12f;

		q          = p1f + step*stepSize*eye;   
		voxelPos   = q * P2V;
		texPos	   = voxelPos * V2T;

		//get the solid_phi value from the simulation
		i = floor(texPos.x * solidSurface.ni);
		j = floor(texPos.y * solidSurface.nj);
		k = floor(texPos.z * solidSurface.nk);

		fx = texPos.x * solidSurface.ni - i;
		fy = texPos.y * solidSurface.nj - j;
		fz = texPos.z * solidSurface.nk - k;

		if(i>0 && i<solidSurface.ni-1 && j>0 && j<solidSurface.nj-1 && k>0 && k<solidSurface.nk-1) //inside grid
		{
			currentValue = solidSurface.gridtrilerp(i,j,k,fx,fy,fz);
		}

		// Find the maximum
		if(currentValue > maximum)
		{
			patientPos = q;
			simPos	 = qfePoint(i+fx, j+fy, k+fz);
			value		 = currentValue;
			maximum    = currentValue;

			if(maximum>=this->algorithmFluidSim->fluidsim->grid.dx*2.0f)
				break;
		}    
	}

	delete [] intersectPos;

	return qfeSuccess;
}

unsigned int qfeDriverFS::qfeSelectInteractor(int windowX, int windowY, qfeViewport *viewport, qfeVolume *volume, qfePoint &simPos)
{
	qfeMatrix4f P2V, V2T, Minv;
	qfePoint    windowPos, worldPos, patientPos, voxelPos, texPos;
	qfePoint   *intersectPos;
	qfePoint    q, p1f, p1b, swap;
	unsigned int         count;
	float       depth;
	float       depthDelta = 0.01;
	float       stepSize;
	unsigned int         steps;

	Array3f	  solidSurface = this->algorithmFluidSim->fluidsim->getSolidSurface();
	unsigned int i,j,k;

	if(viewport == NULL || volume == NULL) return 0;

	// Get the volume data 
	//volume->qfeGetVolumeData(t, dims[0], dims[1], dims[2], &data);

	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);

	// Get the view vector
	qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, Minv);

	qfeVector eye;
	eye.qfeSetVectorElements(0.0,0.0,-1.0);
	eye = eye*Minv;
	qfeVector::qfeVectorNormalize(eye);

	// Unproject the center in patient coordinates to window coordinates
	// We don't know the depth a priori, but we need a reasonable estimate
	// before we determine the patient position by searching the maximum
	qfePoint o, windowOrigin;
	o.qfeSetPointElements(0.0,0.0,0.0);
	qfeTransform::qfeGetCoordinateProject(o, windowOrigin, this->currentModelView, this->currentProjection, viewport);

	// Obtain the coordinates
	windowPos.x = windowX;
	windowPos.y = windowY;  
	windowPos.z = windowOrigin.z;

	// Unproject to patient (/world) coordinates
	qfeTransform::qfeGetCoordinateUnproject(windowPos, worldPos, this->currentModelView, this->currentProjection, viewport);

	worldPos.x = worldPos.x/worldPos.w;
	worldPos.y = worldPos.y/worldPos.w;
	worldPos.z = worldPos.z/worldPos.w;

	// Compute the intersections with the bounding box
	this->qfeGetIntersectionsBoxLine(volume, worldPos, worldPos+eye, &intersectPos, count);

	if(count < 2) return 0;

	// Assign the intersection point and sort them according to their depth
	p1f = intersectPos[0];
	p1b = intersectPos[1];

	if((p1f*this->currentModelView).z < (p1b*this->currentModelView).z)
	{
		swap = p1f;
		p1f  = p1b;
		p1b  = swap;
	}

	patientPos = p1f;

	// Initialize parameters
	qfeVector::qfeVectorLength(p1f-p1b, depth);
	stepSize      = 0.3f;  
	steps         = floor(abs(depth) / stepSize);  

	for(unsigned int step=0; step<steps; step++)
	{      		
		q          = p1f + step*stepSize*eye;  		
		voxelPos   = q * P2V;
		texPos	   = voxelPos * V2T;

		//get the solid_phi value from the simulation
		i = floor(texPos.x * solidSurface.ni);
		j = floor(texPos.y * solidSurface.nj);
		k = floor(texPos.z * solidSurface.nk);

		if(!solidSurface.inRange(i,j,k))
		{
			break;
		}

		if(fluidInteractors.qfeCellContainsFluidInteractor(i,j,k) != 0)
		{		
			return fluidInteractors.qfeCellContainsFluidInteractor(i,j,k);
		}
	}

	delete [] intersectPos;

	return 0;
}

//----------------------------------------------------------------------------
// p1, p2 - points on a line in patient coordinates
qfeReturnStatus qfeDriverFS::qfeGetIntersectionsBoxLine(qfeVolume *volume, qfePoint p1, qfePoint p2, qfePoint **intersections, unsigned int &count)
{
	const int        boxSides = 6;

	qfeVector        normals[boxSides];
	qfePoint         origins[boxSides];
	vector<qfePoint> intersect;

	qfeGrid         *grid;
	unsigned int     size[3];
	qfePoint         o, p;
	qfeVector        e, axes[3];
	qfeFloat         nom, den, u;
	bool             inside;

	if(volume == NULL) return qfeError;

	qfeMatrix4f P2V;
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

	// Get volume properties
	volume->qfeGetVolumeGrid(&grid);
	volume->qfeGetVolumeSize(size[0],size[1],size[2]);
	grid->qfeGetGridOrigin(o.x, o.y, o.z);
	grid->qfeGetGridExtent(e.x, e.y, e.z);
	grid->qfeGetGridAxes(axes[0].x, axes[0].y, axes[0].z, axes[1].x, axes[1].y, axes[1].z, axes[2].x, axes[2].y, axes[2].z);

	// Compute the normals of the bounding box sides
	qfeVector::qfeVectorCross(axes[0], axes[1], normals[0]);
	qfeVector::qfeVectorCross(axes[0], axes[2], normals[2]);
	qfeVector::qfeVectorCross(axes[1], axes[2], normals[4]);
	normals[1] = -1.0f * normals[0];
	normals[3] = -1.0f * normals[2];
	normals[5] = -1.0f * normals[4];

	for(int i=0; i<boxSides; i++)
		qfeVector::qfeVectorNormalize(normals[i]);

	// Compute the origins of the bounding box sides
	origins[0] = o + e.z*0.5f*size[2]*axes[2];
	origins[1] = o - e.z*0.5f*size[2]*axes[2];
	origins[2] = o - e.y*0.5f*size[1]*axes[1];
	origins[3] = o + e.y*0.5f*size[1]*axes[1];
	origins[4] = o + e.x*0.5f*size[0]*axes[0];
	origins[5] = o - e.x*0.5f*size[0]*axes[0];

	// Compute the intersections between the line p1-p2 and the planes
	for(int i=0; i<boxSides; i++)
	{
		den = normals[i] * (p2-p1);
		// Check if the line is parallel to the plane
		if(den != 0)
		{
			nom = normals[i] * (origins[i] - p1);
			u   = nom / den;
			p   = p1 + u*(p2-p1);

			this->qfeGetInsideBox(volume, p, inside);

			if(inside) 
				intersect.push_back(p);
		}
	} 

	// Output the intersection
	count = (int)intersect.size();

	(*intersections) = new qfePoint[count];
	for(unsigned int i=0; i<count; i++)
		(*intersections)[i] = intersect[i];

	return qfeSuccess;
}

//----------------------------------------------------------------------------
// p - patient coordinates
qfeReturnStatus qfeDriverFS::qfeGetInsideBox(qfeVolume *volume, qfePoint p, bool &inside)
{
	qfeMatrix4f      P2V;
	qfeGrid         *grid;
	qfePoint         pVoxel;
	unsigned int     size[3];

	if(volume == NULL) return qfeError; 

	// Get transformation patient to voxel
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

	// Get volume properties
	volume->qfeGetVolumeGrid(&grid);
	volume->qfeGetVolumeSize(size[0],size[1],size[2]);

	// Transform the coordinate
	pVoxel = p * P2V;

	// Check if the point is in the bounding box
	inside = false;

	if((ceil(pVoxel.x) >=0) && (floor(pVoxel.x) <= size[0]) &&
		(ceil(pVoxel.y) >=0) && (floor(pVoxel.y) <= size[1]) &&
		(ceil(pVoxel.z) >=0) && (floor(pVoxel.z) <= size[2]))
	{    
		inside = true;
	}

	return qfeSuccess;
}

qfeReturnStatus qfeDriverFS::qfeRunCoupling(int timeDirection, float stepSize, bool merge_needed)
{	
	assert(timeDirection == 1 || timeDirection == -1);

	std::cout<<""<<std::endl;
	std::cout<<"0% Initializing...";

	this->algorithmFluidSim->negative_time_step = (timeDirection == -1);

	float currentPhase;

	this->visFluidSim->qfeGetVisualActiveVolume(currentPhase);
	
	qfeStudy *study;

	this->visFluidSim->qfeGetVisualStudy(&study);

	//std::cout<<"Initialization"<<std::endl;
	
	if(timeDirection == 1)
	{
		this->algorithmFluidSim->negative_time_step = false;

		this->algorithmFluidSim->setMeasurementVelocityField(0.0);
		currentPhase = 0;
		this->algorithmFluidSim->qfeUpdateTime(currentPhase);
	}
	else if (timeDirection == -1)
	{		
		this->algorithmFluidSim->negative_time_step = true;

		this->algorithmFluidSim->setMeasurementVelocityField(currentPhase<=study->pcap.size()-1);
		currentPhase = study->pcap.size()-1;
		this->algorithmFluidSim->qfeUpdateTime(currentPhase);
	}
	else
	{
		std::cout<<"ERROR: Coupling can only be done one measurement at a time..."<<std::endl;
		return qfeError;
	}
	
	float multiplier = 1.0/stepSize;

	unsigned int number_of_steps = (1.0f/stepSize)*(study->pcap.size()-1);

	//std::cout<<"Number of simulation steps: "<<number_of_steps<<std::endl;
	//std::cout<<"Store initial velocity field"<<std::endl;
	this->algorithmFluidSim->qfeUpdateSerie(currentPhase); 
	
	Array3Vec3 velocity;
	this->algorithmFluidSim->fluidsim->getVelocityField(velocity);

	if(merge_needed)
	{	
		if(timeDirection == 1)
		{
			forwardSimVelocityFields.append(velocity);
		}
		if(timeDirection == -1)
		{
			backwardSimVelocityFields.prepend(velocity);
		}
	}

	int modulo = lround(1.0f/stepSize);
	
	//output intermediate results:
	float phaseDuration = stepSize * study->phaseDuration;
	float triggerTime = 0.0f;

	string fileName = "intermediate_forward";
	if(timeDirection == -1)
		fileName = "intermediate_backward";

	if(!merge_needed)
		fileName = "qflow_sim";

	this->algorithmFluidSim->WriteSimulationVelocityFieldToVolume(0,number_of_steps,fileName,phaseDuration,triggerTime,velocity);

	const QString dialog_title = "Running coupling...";
	const QString abort_title = "Cancel";
	QProgressDialog *progress = new QProgressDialog(dialog_title, abort_title, 0, number_of_steps+1);
	progress->setWindowModality(Qt::WindowModal);
	progress->show();

	for(unsigned int current_step = 1; current_step<=number_of_steps; current_step++)
	{		
		//input: int id, int phaseCount, float phaseDuration, float phaseTriggerTime
		int phaseCount = number_of_steps;
			
		triggerTime = stepSize * current_step;

		float nextPhase = currentPhase + timeDirection * stepSize;

		int percent = (float)(current_step)/number_of_steps*100;

		progress->setValue(current_step);
		progress->update();
		
		if (progress->wasCanceled())
		{
			return qfeError;
		}

		//if next step is coupling, we don't have to advance the simulation
		bool new_serie = (current_step % modulo) == 0;
		if(!new_serie)
		{
			std::cout<<"\r"<<percent<<"% "<<" ("<<current_step<<"/"<<number_of_steps<<") --> Advance from phase: "<<currentPhase<<" to "<<nextPhase<<" based on measurement "<<this->algorithmFluidSim->serie<<"       ";
		
			//std::cout<<"Update phase"<<std::endl;
			this->algorithmFluidSim->qfeUpdateTime(currentPhase);

			// advance the simulation	
			//std::cout<<"Start simulation"<<std::endl;
			this->algorithmFluidSim->qfeAdvanceOneFrame(timeDirection*stepSize);
		}

		// If the simulation arrives at a timepoint with new measured data,
		// provide the right index into the data
		
		if(new_serie && currentPhase != study->pcap.size()) 
		{
			int serie_id = (int)(current_step / modulo);
			if(timeDirection<0)
				serie_id = (study->pcap.size()-1) - serie_id;

			std::cout<<"\r"<<percent<<"% "<<" ("<<current_step<<"/"<<number_of_steps<<") --> Apply coupling with measurement: " << serie_id<<"                                     ";

			this->algorithmFluidSim->qfeUpdateSerie(serie_id); 

			this->algorithmFluidSim->qfeUpdateTime(serie_id);
		}	 

		this->simulationUpdated = true;
		currentPhase = nextPhase;

		//std::cout<<"Store velocity field"<<std::endl;
		this->algorithmFluidSim->fluidsim->getVelocityField(velocity);

		if(merge_needed)
		{
			if(timeDirection == 1)
			{
				forwardSimVelocityFields.append(velocity);
			}
			if(timeDirection == -1)
			{			
				velocity = this->negateVelocityField(velocity);

				backwardSimVelocityFields.prepend(velocity);
			}
		}

		this->algorithmFluidSim->WriteSimulationVelocityFieldToVolume(current_step,number_of_steps,fileName,phaseDuration,triggerTime,velocity);
		//done
	}

	progress->setValue(number_of_steps+1);
	progress->close();

	if(timeDirection == 1)
		std::cout<<"\r"<<"Forward coupling finished                                                          "<<std::endl;
	if(timeDirection == -1)
		std::cout<<"\r"<<"Backward coupling finished                                                         "<<std::endl;

	return qfeSuccess;
}

void qfeDriverFS::qfeComputeFlux(qfeVisualFluid *visual)
{
	qfeStudy      *study;
	qfeVolume     *volume   = NULL;

	visual->qfeGetVisualStudy(&study); 

	if(study->pcap.size()>0)
	{
		qfePlotFlux(study, &study->pcap, "Measured ");
	}

	if(study->sim_velocities.size()>0)
	{
		qfePlotFlux(study, &study->sim_velocities, "Simulated ");
	}	
}

void qfeDriverFS::qfePlotFlux(qfeStudy *study, qfeVolumeSeries *series, QString data_label)
{
	if(series->size() == 0)
		return;

	unsigned int sink_count = 0;
	unsigned int source_count = 0;

	QVector<QString> labels;
	QList<QVector<QPair<double,double>>> dataSets;

	qfeMatrix4f V2P;

	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, series->at(0));

	for(unsigned int i = 0; i<this->fluidInteractors.size(); i++)
	{
		qfeFluidInteractor probe = fluidInteractors.at(i);

		qfeQuantification quantification;
		//convert axis and origin to patient coords (somehow qfeQuantification relies on patient coords...)
		probe.origin = probe.origin * V2P;
		probe.axisX = probe.axisX * V2P;
		probe.axisY = probe.axisY * V2P;
		quantification.qfeGetFlowRate(series, probe);
		
		for(unsigned int q = 0; q<probe.quantification.size(); q++)
		{
			QString quant_name = "Flow rate";
			std::cout<<probe.quantification.at(q).label<<std::endl;
			if(probe.quantification.at(q).label.compare(quant_name.toStdString()) == 0)
			{
				QString label = "";
				if(probe.getType() == qfeFluidInteractor::SOURCE || probe.getType() == qfeFluidInteractor::SOURCE_SELECTION)
				{
					label = data_label+QString(" ")+quant_name+" Source "+QString::number(source_count);
					source_count++;
				}
				if(probe.getType() == qfeFluidInteractor::SINK || probe.getType() == qfeFluidInteractor::SINK_SELECTION)
				{
					label = data_label+QString(" ")+quant_name+" Sink "+QString::number(sink_count);
					sink_count++;
				}

				float step_size = probe.quantification.at(q).stepsX;

				QVector<QPair<double,double>> dataSet;
				for(unsigned int j = 0; j<probe.quantification.at(q).values.size(); j++)
				{
					QPair<double, double> linePoint(0.0, 0.0);
					linePoint.first = j*step_size*0.001; //s to ms
					linePoint.second = probe.quantification.at(q).values.at(j);

					std::cout<<linePoint.first<<", "<<linePoint.second<<std::endl;
					
					dataSet.push_back(linePoint);
				}

				labels.push_back(label);
				dataSets.push_back(dataSet);
				break;
			}
		}
	}
	
	std::cout<<labels.size()<<" "<<dataSets.size()<<std::endl;

	study->qfePlotTemporalGraph(labels, dataSets);
}

qfeVector qfeDriverFS::rotateVectorAroundAxis(qfeVector input, qfeVector axis, float angle)
{
	qfeVector::qfeVectorNormalize(axis);

	float u = axis.x;  float v = axis.y;  float w = axis.z;
	float x = input.x; float y = input.y; float z = input.z;

	float new_x = u * (u*x + v*y + w*z) * (1.0f - cos(angle)) + x*cos(angle) + (-w*y + v*z)*sin(angle);
	float new_y = v * (u*x + v*y + w*z) * (1.0f - cos(angle)) + y*cos(angle) + ( w*x - u*z)*sin(angle);
	float new_z = w * (u*x + v*y + w*z) * (1.0f - cos(angle)) + z*cos(angle) + (-v*x + u*y)*sin(angle);

	return qfeVector(new_x, new_y, new_z);
}

Array3Vec3 qfeDriverFS::negateVelocityField(Array3Vec3 velocityField)
{
	Array3Vec3 negatedVelocityField(velocityField.ni, velocityField.nj, velocityField.nk);

	for(unsigned int k = 0; k<velocityField.nk; k++)
	for(unsigned int j = 0; j<velocityField.nj; j++)
	for(unsigned int i = 0; i<velocityField.ni; i++)
	{
		negatedVelocityField(i,j,k)[0] = -velocityField(i,j,k)[0];
		negatedVelocityField(i,j,k)[1] = -velocityField(i,j,k)[1];
		negatedVelocityField(i,j,k)[2] = -velocityField(i,j,k)[2];
	}
	return negatedVelocityField;
}