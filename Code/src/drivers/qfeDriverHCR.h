#pragma once

#include "qflowexplorer.h"

#include "qfeDriver.h"
#include "qfeFlowLineTraceCluster.h"
#include "qfeFrame.h"
#include "qfeMatrix4f.h"
#include "qfePlanarReformat.h"
#include "qfeRaycasting.h"
#include "qfeSurfaceShading.h" 
#include "qfeSurfaceContours.h" 
#include "qfeTransform.h"

// SJ - add your algorithms here
#include "qfeHierarchicalClustering.h"

#include "vtkCamera.h"
#include "vtkMatrix4x4.h"

/**
* \file   qfeDriverHCR.h
* \author Roy van Pelt
* \class  qfeDriverHCR
* \brief  Implements a driver for the hierarchical clustering rendering
*
* qfeDriverHCR derives from qfeDriver, and implements
* a GPU-based hierarchical clustering visualization
*/

class qfeDriverHCR : public qfeDriver
{
public:
  qfeDriverHCR(qfeScene *scene);
  ~qfeDriverHCR();

  bool qfeRenderCheck();
  void qfeRenderSelect();
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();

  enum qfeClusterPlaneSelected
  {
    qfePlaneNone,
    qfePlaneX,
    qfePlaneY,
    qfePlaneZ
  };

  enum qfeClusterPlaneDirection
  {
    qfePlaneAxial,
    qfePlaneSagittal,
    qfePlaneCoronal,
  };

  enum qfeDataTypeIndex
  {
    dataTypePCAP,
    dataTypePCAM,
    dataTypeSSFP,
    dataTypeTMIP,
    dataTypeFFE,
    dataTypeCLUST,
    dataTypeFTLE
  };

private:
  qfeVisualClustering          *visCluster;
  qfeVisualClusterArrowTrace   *visClusterArrows;
  qfeVisualReformatOrtho       *visOrtho;
  qfeVisualAnatomyIllustrative *visSurface;
  qfeVisualRaycast             *visDVR;
  qfeVisualAides               *visAides;

  qfeMatrix4f                   currentModelView;

  qfePlanarReformat            *algorithmOrtho;
  qfeRayCasting                *algorithmDVR;
  qfeFlowLineTraceCluster      *algorithmLines;
  qfeSurfaceShading            *algorithmSurfaceShading;
  qfeSurfaceContours           *algorithmSurfaceContours;

  //SJ - your fancy algorithms here
  //     be aware of the nasty copy constructors!
  qfeHierarchicalClustering *hierarchicalClustering;
  
  qfeVoxelSeries              clusterVoxels;
  qfeVoxelArray               clusterCenters;

  double                      currentLevel;
  int                         currentSeedType;

  int                         selectedPlane;
  qfeFrameSlice              *planeX;
  qfeFrameSlice              *planeY;
  qfeFrameSlice              *planeZ;

  qfeColorRGB                 colorDefault;
  qfeColorRGB                 colorHighlight;
  
  float                       dvrQuality;

  GLuint                      intersectTexColor;
  GLuint                      intersectTexDepth;
  GLuint                      clippingTexColor;
  GLuint                      clippingFrontTexDepth;
  GLuint                      clippingBackTexDepth;

  qfeReturnStatus qfeGenerateClusters();
  qfeReturnStatus qfeInitializeClusterLabelVolumes(qfeVolumeSeries *clusterLabels, qfeVolumeSeries *clusterSource);

  qfeReturnStatus qfeRenderVisualClustering(qfeVisualClustering *visual);
  qfeReturnStatus qfeRenderVisualClusterArrows(qfeVisualClusterArrowTrace *visual);
  qfeReturnStatus qfeRenderVisualPlanesOrtho(qfeVisualReformat *visual);
  qfeReturnStatus qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual);
  qfeReturnStatus qfeRenderVisualDVR(qfeVisualRaycast* visual);
  qfeReturnStatus qfeRenderVisualAides(qfeVisualAides *visual);

  qfeReturnStatus qfeGetDataSetOrtho(int index, qfeVisual *visual, qfeVolume **volume);
  qfeReturnStatus qfeGetDataSet(qfeDataTypeIndex index, qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu);
  qfeReturnStatus qfeGetDataSlice(qfeVolume *volume, qfeClusterPlaneDirection dir, qfeFrameSlice **slice);

  qfeReturnStatus qfeShowClusters(qfeVisualReformatOrtho *visual);

  qfeReturnStatus qfeCreateTextures(qfeViewport *vp, int supersampling);
  qfeReturnStatus qfeDeleteTextures();
  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeEnableIntersectBuffer();
  qfeReturnStatus qfeDisableIntersectBuffer();

  void qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm);
  void qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm);

  static void qfeOnAction(void *caller, int t);
  static void qfeOnResize(void *caller);  
  static void qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v);
  static void qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v);
  static void qfeOnWheelForward(void *caller, int x, int y, int v);
  static void qfeOnWheelBackward(void *caller, int x, int y, int v);
};
