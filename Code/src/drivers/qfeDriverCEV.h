#pragma once

#include "qflowexplorer.h"

#include "qfeDriver.h"
#include "qfeIllustrativeVolumeRendering.h"
#include "qfeGradient.h"
#include "qfeFlowLineTrace.h"
#include "qfeFlowParticleTrace.h"
#include "qfeSeedVolume.h"
#include "qtTransferFunctionWidget.h"
#include "qfePlanarReformat.h"
#include "qfeProbabilityDVR.h"

using namespace std;

/**
* \file   qfeDriverCEV.h
* \author Arjan Broos
* \class  qfeDriverCEV
* \brief  Implements a driver for cardiac exploration
*
*/

class qfeDriverCEV : public qfeDriver {
public:
  qfeDriverCEV(qfeScene *scene);
  ~qfeDriverCEV();

  bool qfeRenderCheck();
  void qfeRenderSelect();
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();

  enum qfePlaneSelected
  {
    qfePlaneNone,
    qfePlaneX,
    qfePlaneY,
    qfePlaneZ,
    qfePlaneQ
  };

  enum qfePlaneDirection
  {
    qfePlaneAxial,
    qfePlaneSagittal,
    qfePlaneCoronal,
  };

private:
  qfeVisualCEV                *visualCEV;
  qfeVisualFlowParticleTrace  *visParticleTrace;
  qfeVisualFlowLineTrace      *visLineTrace;
  qfeVisualAides              *visAides;
  qfeVisualReformatOrtho      *visOrtho;
  qfeVisualSeedEllipsoid      *visSE;
  qfeVisualPlaneOrthoView     *visPOV;
  qfeVisualFeatureSeeding     *visFS;
  qfeVisualVolumeOfInterest   *visVOI;
  qfeVisualProbabilityDVR     *visPDVR;

  qfeGradient                     algorithmGradient;
  qfeIllustrativeVolumeRendering  algorithmIVR;
  qfeFlowParticleTrace            algorithmParticleTrace;  
  qfeFlowLineTrace                algorithmLineTrace; 
  qfePlanarReformat               algorithmOrtho;
  qfeProbabilityDVR               algorithmPDVR;

  int                        selectedPlane;
  qfeFrameSlice             *planeX;
  qfeFrameSlice             *planeY;
  qfeFrameSlice             *planeZ;
  qfeFrameSlice             *planeQ;

  qfeColorRGB                colorDefault;
  qfeColorRGB                colorHighlight;

  qfeColorMap defaultColorMap;
  qfeRGBMapping flowVisColorMap;

  bool tMipAvailable;

  float **gradients;      // PCA-M Gradients
  float *tMipGradients;
  float maxGradLength;
  float maxGradLengthTMip;

  GLuint currentTransferFunctionId;

  unsigned nrFrames;
  unsigned width, height, depth;

  GLuint texRayStart;
  GLuint texRayEnd;
  GLuint *texGradients;
  GLuint texTMipGradients;
  GLuint intersectTexColor;
  GLuint intersectTexDepth;

  qfeFloat prevTime;

  qfeSeedEllipsoid lvEllipsoid;
  qfeSeedEllipsoid rvEllipsoid;
  qfeGuidingPointState lvGuidingPointState;
  qfeGuidingPointState rvGuidingPointState;
  bool lvEllipsoidSelected;
  bool rvEllipsoidSelected;
  std::vector<qfePoint> lvGuidingPoints;
  std::vector<qfePoint> rvGuidingPoints;
  qfeGuidingPointState lvSelectedGuidingPoint;
  qfeGuidingPointState rvSelectedGuidingPoint;
  enum SE_Type {
    SE_Left,
    SE_Right,
    SE_VoI
  };

  qfeSeedEllipsoid voiEllipsoid;
  qfeGuidingPointState voiGuidingPointState;
  bool voiEllipsoidSelected;
  std::vector<qfePoint> voiGuidingPoints;
  qfeGuidingPointState voiSelectedGuidingPoint;

  bool clipAllData;
  bool previousClipAllData;

  qfeProbeSeedingState seedingStateParticles;
  qfeProbeSeedingState seedingStateLines;

  qfeReturnStatus qfeRenderVisualCEV(qfeVisualCEV *visual, qfeVisualVolumeOfInterest *voiVisual);
  qfeReturnStatus qfeRenderVisualParticleTrace(qfeVisualFlowParticleTrace *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualLineTrace(qfeVisualFlowLineTrace *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualAides(qfeVisualAides *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualPlanesOrtho(qfeVisualReformat *visual);
  qfeReturnStatus qfeRenderGuidingPoints(bool voi);
  qfeReturnStatus qfeRenderGuidingPoint(unsigned index, SE_Type type);
  qfeReturnStatus qfeRenderCube(qfePoint pos, float size, qfeColorRGB color);
  qfeReturnStatus qfeRenderSeedEllipsoid(qfeVisualSeedEllipsoid *visual);
  qfeReturnStatus qfeRenderVolumeOfInterest(qfeVisualVolumeOfInterest *visual);
  qfeReturnStatus qfeRenderPlaneOrthoView(qfeVisualPlaneOrthoView *visual, qfeVisualReformat *visMPR);
  qfeReturnStatus qfeRenderProbabilityDVR(qfeVisualProbabilityDVR *visual, qfeVisualVolumeOfInterest *voiVisual);

  qfeReturnStatus qfeGetDataSet(qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu, int contextIndex);
  qfeReturnStatus qfeGetDataSetOrtho(int index, qfeVisual *visual, qfeVolume **volume);  
  qfeReturnStatus qfeGetPlaneOrtho(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice);
  qfeReturnStatus qfeGetPlaneViewOrthogonal(qfeVolume *volume, qfeFrameSlice **slice);

  void qfeMovePlaneForward(qfeFrameSlice **slice);
  void qfeMovePlaneBackward(qfeFrameSlice **slice);
  void qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm);
  void qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm);

  qfeReturnStatus qfeInjectSeeds(qfeVisualFlowParticleTrace *visual);
  qfeReturnStatus qfeClearSeeds(qfeVisualFlowParticleTrace *visual);
  qfeReturnStatus qfeInjectSeeds(qfeVisualFlowLineTrace *visual);
  qfeReturnStatus qfeClearSeeds(qfeVisualFlowLineTrace *visual);
  qfeReturnStatus qfeUpdateSeeds();

  void qfeClickToRay(int x, int y, qfePoint &start, qfePoint &end);
  void qfePlaceGuidingPoint(int x, int y, SE_Type type);
  void qfeMoveGuidingPoint(unsigned index, int x, int y, SE_Type type);
  void qfeInjectEllipsoidSeedPoints();
  void qfeGenerateEllipsoidPoints();
  bool qfeGetRaySliceIntersection(qfePoint rayStart, qfePoint rayEnd, qfeFrameSlice *slice, qfePoint &intersection, float &distance);
  bool qfeGetPointOnSlices(int x, int y, qfePoint &p);
  void qfeGetSliceCorners(qfeFrameSlice *slice, qfePoint **corners);

  void qfeCreateTextures(qfeViewport *vp, int supersampling);
  void qfeDeleteTextures();

  void qfePlaceFeatureBasedSeedPoints(int currentPhase);
  qfeVolume *qfeGetVolumeForDataType(int dataType, int currentPhase, float &rangeMin, float &rangeMax);
  unsigned volumeSize0;
  unsigned volumeSize1;
  float *probabilities0;
  float *probabilities1;
  inline unsigned qfeGetIndex(unsigned width, unsigned height, unsigned x, unsigned y, unsigned z);
  void qfeResizeProbabilities(qfeVolume *vol, int id);
  bool qfePrepareProbabilities(qfeVolume *vol0, qfeVolume *vol1, QVector<QPair<double, double>> &tf0, QVector<QPair<double, double>> &tf1);
  void qfeCalculateProbabilities(qfeVolume *vol, QVector<QPair<double, double>> &tf, int id, float rangeMin, float rangeMax);
  float qfeGetValue(void *data, qfeValueType t, unsigned index, unsigned nrComp);
  float qfeGetProbability(float value, float rangeMin, float rangeMax, QVector<QPair<double, double>> &tf);
  float GetRandomFloat(float min, float max);
  std::vector<qfePoint> featureSeedPoints;
  void qfeRenderFeatureSeedPoints();

  qfeReturnStatus qfeEnableIntersectBuffer();
  qfeReturnStatus qfeDisableIntersectBuffer();

  qfeColorMap qfeGetDefaultColorMap();
  vector<qfeRGBMapping> qfeGetFlowVisColorMap();

  void qfeGenerateSphereSet();

  void qfeUpdateClipData();
  void qfeCreateClipMask(float *mask);
  void qfeClipVolume(qfeVolume *original, qfeVolume *toClip, float *mask);
  void qfeCopyVolumeSeries();
  void qfeCopyVolumeSeriesBack();
  void qfeAssignValue(void *data, qfeValueType t, unsigned index, float value);
  qfeVolumeSeries flowSpeedCopy;
  qfeVolumeSeries pcamCopy;
  qfeVolumeSeries curlCopy;
  qfeVolumeSeries lambda2Copy;
  qfeVolumeSeries divergenceCopy;
  qfeVolumeSeries qcriterionCopy;
  qfeVolume *tmipCopy;

  static void qfeOnAction(void *caller, int t);
  static void qfeOnResize(void *caller);
  static void qfeOnMouseMove(void *caller, int x, int y, int v);
  static void qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v);
  static void qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v);
  static void qfeOnMouseRightButtonDown(void *caller, int x, int y, int v);
  static void qfeOnWheelForward(void *caller, int x, int y, int v);
  static void qfeOnWheelBackward(void *caller, int x, int y, int v);
  void qfeClipSeedPoints();
};
