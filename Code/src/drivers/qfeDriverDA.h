#pragma once

#include "qflowexplorer.h"

#include <stdlib.h>

#include "qfeMatrix4f.h"
#include "qfeTransform.h"
#include "qfeVolume.h"
#include "qfeDriver.h"

//algorithms
#include "qfePlanarReformat.h"
#include "qfeSurfaceShading.h" 
#include "qfeSeedVolumeCollection.h"

#include "qfeInkParticles.h"

#include <QPushButton>
#include <qdialog.h>


#include "qtTransferFunctionWidget.h"

/**
* \file   qfeDriverAide.h
* \author Roy van Pelt
* \class  qfeDriverAide
* \brief  Implements functions to support drivers
*
* qfeDriverAide implements various functions that
* are valuable to a wide range of drivers
*/

class qfeDriverDA : public qfeDriver
{
public:
  qfeDriverDA(qfeScene *scene);
  ~qfeDriverDA();

  bool qfeRenderCheck();
  void qfeRenderSelect();
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();
  
  enum qfePlaneDirection
  {
    qfePlaneAxial,
    qfePlaneSagittal,
    qfePlaneCoronal,
  };

  enum qfePlaneSelected
  {
    qfePlaneNone,
    qfePlaneX,
    qfePlaneY,
    qfePlaneZ
  };
protected:

private:
	//visuals
	qfeVisualSeedVolumes		*visualSeedVols;
	qfeVisualReformatOrtho	*visualReformat;
	qfeVisualAnatomyIllustrative *visualAnatomy;
	qfeVisualDataAssimilation *visualDA;
	qfeVisualInkVis			*visualInk1;
	qfeVisualInkVisSecondary *visualInk2;
	bool visualInk2_initialized;
	qfeVisualAides			*visAides;
	
	//algorithms
	qfePlanarReformat *algorithmReformat;
	qfeSurfaceShading *algorithmSurfaceShading;
	qfeSeedVolumeCollection *flowSourceAndSinks;
	qfeInkParticles	*algorithmIP1;
	qfeInkParticles	*algorithmIP2;

	//render functions
	qfeReturnStatus qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual);
	qfeReturnStatus qfeRenderVisualSeedVolumes(qfeVisualSeedVolumes *visual);
	qfeReturnStatus qfeRenderVisualDataAssimilation(qfeVisualDataAssimilation *visual);
	qfeReturnStatus qfeRenderVisualPlanesOrtho(qfeVisualReformat *visual);
	qfeReturnStatus qfeRenderVisualInk(qfeVisualDataAssimilation *visual_da, qfeVisualInkVis *visual1, qfeVisualInkVisSecondary *visual2);
	qfeReturnStatus qfeRenderVisualAides(qfeVisualAides *visual);
		
	//engine observer/interaction functions:
	static void qfeOnAction(void *caller, int t);
	static void qfeOnResize(void *caller);  
	static void qfeOnMouseMove(void *caller, int x, int y, int v);
	static void qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v);
	static void qfeOnMouseRightButtonUp(void *caller, int, int y, int v);
	static void qfeOnMouseRightButtonDown(void *caller, int x, int y, int v);  
	static void qfeOnWheelForward(void *caller, int x, int y, int v);
	static void qfeOnWheelBackward(void *caller, int x, int y, int v);
		
	bool rightMouseDown;

	//additional volumes:
	bool additional_data_computed;
	qfeReturnStatus qfeComputeSNRData();
	qfeReturnStatus qfeComputeMeshLevelsetVolume();
	qfeReturnStatus qfeComputeWSSData();
	qfeReturnStatus qfeComputeSNRVolumes();
	qfeReturnStatus qfeComputeSignalStrengthVolumes();
	qfeReturnStatus qfeComputeWSSVolumes();

	float qfeGetProbability(float value, float rangeMin, float rangeMax, QVector<QPair<double, double>> &tf);
	void qfeComputeSeeding(qfeVisualDataAssimilation *visual_da);

	std::vector<Array3f> SNR_data; 
	std::vector<float> sigma_data;

	std::vector<Array3Vec3> WSS_data; 

	Array3f mesh_level_set;

	Array3ui sourceSinkVolume;

	std::vector<qfeVolume> SNR_Volumes;
	std::array<float, 2> SNR_Range;
	std::vector<qfeVolume> Signal_Strength_Volumes;
	std::array<float, 2> Signal_Strength_Range;
	qfeVolume meshLevelsetVolume;
	std::array<float, 2> meshLevelSetRange;
	std::vector<qfeVolume> WSS_Volumes;

	//additional variables/functions
	//reformat
	int                        selectedPlane;
	qfeFrameSlice             *planeX;
	qfeFrameSlice             *planeY;
	qfeFrameSlice             *planeZ;

	qfeColorRGB                colorReformatDefault;
	qfeColorRGB                colorReformatHighlight;

	qfeReturnStatus qfeReformatGetDataSet(int index, std::string name, qfeVisual *visual, int time, qfeVolume **volume);
	qfeReturnStatus qfeGetPlaneOrtho(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice);
	void qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm);
	void qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm);

	bool qfeGetPointOnSlices(int x, int y, qfePoint &p);
	void qfeClickToRay(int x, int y, qfePoint &start, qfePoint &end);
	int qfePointsOnLine(qfePoint start, qfePoint end, std::vector<seedVolumeGuidePoint> points);
	bool qfeGetRaySliceIntersection(qfePoint rayStart, qfePoint rayEnd, qfeFrameSlice *slice, qfePoint &intersection, float &distance);
	void qfeGetSliceCorners(qfeFrameSlice *slice, qfePoint **corners);

	//seed volume collection
	bool flowSourceAndSinkGuidePointSelected;
	seedVolumeGuidePoint pointSelected;
	bool newFlowSourceAndSink;
	std::vector<qfePoint> newFlowSourceAndSinkPoints;

	std::string qfeWriteData(qfeVisualDataAssimilation *visual, unsigned int t);

	void qfeApplyDataAssimilation(qfeVisualDataAssimilation *visual);

	void qfeUpdateSourceSinkVolume();

	//ink vis variables
	float animation_duration;
	float currentTime;

	//Box shaped ROI in voxel coordinates x1, x2, y1, y2, z1, z2
	qfePoint ROI_min;
	qfePoint ROI_max;

	std::vector<particle> seeds;
};
