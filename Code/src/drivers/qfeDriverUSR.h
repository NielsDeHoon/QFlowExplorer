#pragma once

#include "qflowexplorer.h"

#include "qfeDriver.h"
#include "qfeFlowLineTrace.h"
#include "qfeFlowOctree.h"
#include "qfeFlowProbe3D.h"
#include "qfeFlowParticleTrace.h"
#include "qfeMaximumProjection.h"
#include "qfePlanarReformat.h"
#include "qfeRaycasting.h"
#include "qfeRaycastingVector.h"
#include "qfeSeeding.h"

#define  SELECT_PLANE         0.4
#define  SELECT_PLANE_PROBE   0.6
#define  SELECT_PLANE_ORTHO   0.2
#define  SELECT_PROBE         0.5
#define  SELECT_PROBE_HANDLE  0.7
#define  SELECT_PROBE_ORTHO   0.9

using namespace std;

/**
* \file   qfeDriverUSR.h
* \author Roy van Pelt
* \class  qfeDriverUSR
* \brief  Implements a Ultrasound rendering (USR) driver
*
* qfeDriverUSR derives from qfeDriver, and implements
* a GPU-based Ultrasound-inspired rendering for 4D PC-MRI blood-flow
*/

class qfeDriverUSR : public qfeDriver
{
public:
  qfeDriverUSR(qfeScene *scene);
  ~qfeDriverUSR();

  bool qfeRenderCheck();
  void qfeRenderSelect();
  void qfeRenderPositions();
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderWait1();
  void qfeRenderWait2();
  void qfeRenderWait3();
  void qfeRenderStop();

private:
  enum qfeDataTypeIndex
  {
    dataTypePCAP,
    dataTypePCAM,
    dataTypeSSFP,
    dataTypeTMIP,
    dataTypeFFE
  };

  enum qfePlaneSelected
  {
    qfePlaneNone,
    qfePlaneX,
    qfePlaneY,
    qfePlaneZ
  };

  enum qfePlaneDirection
  {
    qfePlaneAxial,
    qfePlaneSagittal,
    qfePlaneCoronal,
  };

  enum qfePlaneProbe
  {
    qfePlaneProbeOrthogonal,
    qfePlaneProbeParallel    
  };

  enum qfePlaneInteractionDirection
  {
    qfePlaneInteractionNone,
    qfePlaneInteractionHorizontal,
    qfePlaneInteractionVertical
  };


  qfeVisualFlowProbe3D                     *visProbe3D;
  qfeVisualReformatOrtho                   *visOrtho;
  qfeVisualReformatOblique                 *visMPR;
  qfeVisualReformatObliqueProbe            *visMPRP;
  qfeVisualReformatUltrasound              *visMPRUS;
  qfeVisualReformatFixed                   *visPlane;
  qfeVisualPlanesArrowHeads                *visArrows;
  qfeVisualFlowLineTrace                   *visLineTrace;
  qfeVisualFlowParticleTrace               *visParticleTrace;    
  qfeVisualRaycast                         *visDVR;
  qfeVisualRaycastVector                   *visVFR;
  qfeVisualMaximum                         *visMIP;
  qfeVisualAides                           *visAides;

  qfeFlowProbe3D                           *algorithmProbe;
  qfeFlowOctree                            *algorithmOctree;
  qfePlanarReformat                        *algorithmMPR;
  qfePlanarReformat                        *algorithmPlane;
  qfeFlowLineTrace                         *algorithmLineTrace;
  qfeFlowParticleTrace                     *algorithmParticleTrace;    
  qfeRayCasting                            *algorithmDVR;
  qfeRayCastingVector                      *algorithmVFR;
  qfeMaximumProjection                     *algorithmMIP;  

  // FBO variables
  GLuint                                    fboIntersectClip;  

  GLuint                                    intersectTexColor[2];
  GLuint                                    intersectTexDepth[2];
  GLuint                                    clippingTexColor[2];
  GLuint                                    clippingFrontTexDepth[2];
  GLuint                                    clippingBackTexDepth[2];

  qfeFrameSlice                            *planeX, *planeY, *planeZ, *planeQ;
  int                                       planeSelected;
  int                                       planeProbeOrthogonalSelected;
  int                                       planeViewAngle;
  bool                                      planeViewFlip;
  bool                                      planeOverlayVisible;
  qfeColorRGB                               planeColorDefault;
  qfeColorRGB                               planeColorHighlight;
  qfeFloat                                  planeOpacity;

  qfePoint                                  prevPoint;
  qfePoint                                  nextPoint;
  qfeFloat                                  prevTime;  
    
  qfePlaneInteractionState                  planeInteractionState;
  qfePlaneInteractionDirection              planeInteractionDirection;
  qfeProbeSeedingState                      probeSeedingStateParticles;
  qfeProbeSeedingState                      probeSeedingStateLines;
  qfeProbeSeedingState                      probeSeedingStateSurfaces;

  bool                                      viewportMouseButtonDown[4];
  qfePoint                                  viewportMouseCurrentPoint;

  qfeProbeActionState                       probeActionState;       // Current workflow for adding a new probe
  qfeProbeSelectState                       probeClickState;        // Current state for adding with 2-clicks  
  qfePoint                                  probePoints[3];

  qfeVector                                 probeClickOffset;  

  int                                       probeVolumePitch;
  float                                     probePitch;

  float                                     dvrQuality;

  // Render geometry
  qfeReturnStatus qfeRenderVisualProbe(qfeVisualFlowProbe3D *visual, qfeViewport *viewport, int supersampling);
  qfeReturnStatus qfeRenderVisualOrtho(qfeVisualReformat *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualMPR(qfeVisualReformat *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualMPRP(qfeVisualReformat *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualMPRUS(qfeVisualReformat *visual, qfePlaneProbe type, int supersampling);
  qfeReturnStatus qfeRenderVisualPlane(qfeVisualReformat *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualPlanesArrows(qfeVisualReformat *visual, qfePlaneProbe type, int supersampling);
  qfeReturnStatus qfeRenderVisualParticleTrace(qfeVisualFlowParticleTrace *visual, int supersampling);    
  qfeReturnStatus qfeRenderVisualLineTrace(qfeVisualFlowLineTrace *visual, int supersampling);

  // Render volumes
  qfeReturnStatus qfeRenderVisualMIP(qfeVisualMaximum *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualDVR(qfeVisualRaycast *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualVFR(qfeVisualRaycastVector *visual, int supersampling);

  // Render visual aides
  qfeReturnStatus qfeRenderVisualAides(qfeVisualAides *visual, int supersampling);

  // Workflow steps
  qfeReturnStatus qfeWorkflowProbeThreeClick(qfePoint *p);  
  qfeReturnStatus qfeWorkflowProbeFit();

  // Workflow related functions
  qfeReturnStatus qfeAddProbe(qfeProbe3D probe);
  qfeReturnStatus qfeAddProbe(qfePoint p1, qfePoint p2);
  qfeReturnStatus qfeAddProbe(qfePoint p1, qfePoint p2, qfePoint p3);
  qfeReturnStatus qfeDeleteProbe(qfeProbe3D *probe);
  qfeReturnStatus qfeUpdateProbeSeeds();

  qfeReturnStatus qfeSetSelectedProbe(qfeVisualFlowProbe3D *visual, qfeProbe3D  *probe);
  qfeReturnStatus qfeGetSelectedProbe(qfeVisualFlowProbe3D *visual, qfeProbe3D **probe);
  qfeReturnStatus qfeGetSelectedProbe(qfeVisualFlowProbe3D *visual, int &index);
  qfeReturnStatus qfeSetSelectedProbe(qfeVisualFlowProbe3D *visual, int index);
  qfeReturnStatus qfeGetProbeIndex(qfeVisualFlowProbe3D *visual, qfeProbe3D  *probe, int &index);

  qfeReturnStatus qfeInjectSeeds(qfeVisualFlowParticleTrace *visual);
  qfeReturnStatus qfeClearSeeds(qfeVisualFlowParticleTrace *visual);
  qfeReturnStatus qfeInjectSeeds(qfeVisualFlowLineTrace *visual);
  qfeReturnStatus qfeClearSeeds(qfeVisualFlowLineTrace *visual);
  
  qfeReturnStatus qfeDrawPoint(qfePoint p, int supersampling, qfeColorRGBA color);
  qfeReturnStatus qfeDrawLine(qfePoint p1, qfePoint p2, int supersampling);

  // Get the appropriate dataset or selection
  qfeReturnStatus qfeGetDataSet(qfeDataTypeIndex index, qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu);
  qfeReturnStatus qfeGetDataPlane(int index, int slice, qfeVisual *visual, qfeVolume **volume);
  qfeReturnStatus qfeGetDataSlice(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice);
  qfeReturnStatus qfeGetSliceOblique(qfeVolume *volume, qfeFrameSlice **slice);
  qfeReturnStatus qfeGetSelectedObject(qfeColorRGB color, int &id, int &nr);

  // Buffer functions (multi-tex)
  qfeReturnStatus qfeEnableBuffer(GLuint texColor, GLuint texDepth);
  qfeReturnStatus qfeDisableBuffer(GLint index);

  // Texture functions  
  qfeReturnStatus qfeCreateTexturesIntersect(qfeViewport *vp, int supersampling, GLuint& texColor, GLuint &texDepth);
  qfeReturnStatus qfeCreateTexturesClipping(qfeViewport *vp, int supersampling, GLuint &texColor, GLuint &texFront, GLuint &texBack);
  qfeReturnStatus qfeDeleteTextures();
  qfeReturnStatus qfeUnbindTextures();

  // MPR interaction
  void qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm);
  void qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm);
  void qfeResetPlane();

  // MPR ortho interaction
  static void qfeMouseMoveSlice(qfeProbe3D *probe, qfeFrameSlice *plane, qfePoint mousePatientPos, qfeVector eye, qfeFloat &offset);

  // Probe interaction
  static void qfeMouseMoveProbe3DOF(qfeProbe3D    *probe, qfeVector displacement);
  static void qfeMouseMoveProbe2DOF(qfeProbe3D *probe, qfeFrameSlice *plane, qfeVolume* volume, qfePoint mousePatientPos, qfeVector offset, qfeVector eye);
  static void qfeMouseMoveOffset(qfeProbe3D *probe, qfeFrameSlice *plane, qfePoint mousePatientPos, qfeVector eye, qfeVector &offset);

  static void qfeKeyMoveProbe(qfeProbe3D *probe, qfeVector displacement);

  static void qfeGetIntersectionLineProbePlane(qfeProbe3D *probe, qfePoint p, qfeVector e, qfePoint &pos);
  static void qfeGetIntersectionLineProbePlane(qfeFrameSlice *plane, qfePoint p, qfeVector e, qfePoint &pos);
  static void qfeGetInsideBox(qfeVolume *volume, qfePoint p, bool &inside);
  static void qfeGetDistancePointPlane(qfeFrameSlice *plane, qfePoint p, qfeFloat &d);

  // Interaction callbacks
  static void qfeOnAction(void *caller, int t);
  static void qfeOnResize(void *caller);
  static void qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v);
  static void qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v);
  static void qfeOnMouseMove(void *caller, int x, int y, int v);
  static void qfeOnWheelForward(void *caller, int x, int y, int v);
  static void qfeOnWheelBackward(void *caller, int x, int y, int v);
  static void qfeOnKeyPress(void *caller, char *keysym);
  static void qfeOnWiiPress(void *caller, char *wiikey);

  // Interaction handles
  void qfeOnMouseLeftButtonDown3D(int x, int y);
  void qfeOnMouseLeftButtonDown2D1(int x, int y);
  void qfeOnMouseLeftButtonDown2D2(int x, int y);
  void qfeOnMouseLeftButtonDown2D3(int x, int y);

  void qfeOnWheelForward3D(int x, int y);
  void qfeOnWheelForward2D1(int x, int y);
  void qfeOnWheelForward2D2(int x, int y);
  void qfeOnWheelForward2D3(int x, int y);

  void qfeOnWheelBackward3D(int x, int y);
  void qfeOnWheelBackward2D1(int x, int y);
  void qfeOnWheelBackward2D2(int x, int y);
  void qfeOnWheelBackward2D3(int x, int y);

  void qfeOnMouseMove3D(int x, int y);
  void qfeOnMouseMove2D1(int x, int y);
  void qfeOnMouseMove2D2(int x, int y);
  void qfeOnMouseMove2D3(int x, int y);
 };
