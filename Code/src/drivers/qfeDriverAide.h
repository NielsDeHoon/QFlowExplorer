#pragma once

#include "qflowexplorer.h"

#include "qfeMatrix4f.h"
#include "qfeTransform.h"
#include "qfeVolume.h"

/**
* \file   qfeDriverAide.h
* \author Roy van Pelt
* \class  qfeDriverAide
* \brief  Implements functions to support drivers
*
* qfeDriverAide implements various functions that
* are valuable to a wide range of drivers
*/

class qfeDriverAide
{
public:
  // Render functions
  static void qfeRenderBoundingBox(qfeVolume *volume, qfeFloat lineWidth);  
  static void qfeRenderBoundingBox(qfeVolume *volume, qfeColorRGB color, qfeFloat lineWidth);  

  static void qfeRenderBoundingBoxDashed(qfeVolume *volume, qfeMatrix4f modelview, qfeFloat lineWidth);
  static void qfeRenderBoundingBoxDashed(qfeVolume *volume, qfeMatrix4f modelview, qfeColorRGB color, qfeFloat lineWidth);  

  static void qfeRenderVolumeAxes(qfeVolume *volume, qfeFloat scale, qfeFloat lineWidth);

  // Draw functions
  static void qfeDrawBoundingBox();
  static void qfeDrawAxes(qfePoint origin, qfeVector x, qfeVector y, qfeVector z, qfeFloat scale);

protected:
  qfeDriverAide(){};
  ~qfeDriverAide(){};

private:

};
