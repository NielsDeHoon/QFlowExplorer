#include "qfeDriverUSR.h"

//----------------------------------------------------------------------------
qfeDriverUSR::qfeDriverUSR(qfeScene *scene) : qfeDriver(scene)
{
  this->visProbe3D                = NULL;
  this->visOrtho                  = NULL;
  this->visMPR                    = NULL;
  this->visMPRP                   = NULL;
  this->visMPRUS                  = NULL;
  this->visPlane                  = NULL;
  this->visLineTrace              = NULL;
  this->visParticleTrace          = NULL;
  this->visDVR                    = NULL;
  this->visVFR                    = NULL;
  this->visMIP                    = NULL;
  this->visAides                  = NULL;
 
  this->intersectTexColor[0]      = 0;
  this->intersectTexColor[1]      = 0;
  this->intersectTexDepth[0]      = 0;
  this->intersectTexDepth[1]      = 0;
  this->clippingTexColor[0]       = 0;
  this->clippingTexColor[1]       = 0;
  this->clippingFrontTexDepth[0]  = 0;
  this->clippingFrontTexDepth[1]  = 0;
  this->clippingBackTexDepth[0]   = 0;
  this->clippingBackTexDepth[1]   = 0;

  // Initialize plane
   this->planeX = new qfeFrameSlice();      
  this->planeX->qfeSetFrameExtent(1,1);
  this->planeX->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeX->qfeSetFrameAxes(0.0,0.0,1.0, 0.0,1.0,0.0, 1.0,0.0,0.0);  

  this->planeY = new qfeFrameSlice();      
  this->planeY->qfeSetFrameExtent(1,1);
  this->planeY->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeY->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,0.0,1.0, 0.0,1.0,0.0);  
  
  this->planeZ = new qfeFrameSlice();      
  this->planeZ->qfeSetFrameExtent(1,1);
  this->planeZ->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeZ->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0);  

  this->planeQ = new qfeFrameSlice();      
  this->planeQ->qfeSetFrameExtent(1,1);
  this->planeQ->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeQ->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,-1.0);  

  this->planeSelected             = qfePlaneNone;
  this->planeInteractionDirection = qfePlaneInteractionNone;

  this->planeColorDefault.r   = 0.952;
  this->planeColorDefault.g   = 0.674;
  this->planeColorDefault.b   = 0.133;

  this->planeColorHighlight.r = 0.356;
  this->planeColorHighlight.g = 0.623;
  this->planeColorHighlight.b = 0.396;

  this->planeProbeOrthogonalSelected  = -1;
  this->planeViewAngle                = 0;
  this->planeViewFlip                 = true;
  this->planeOverlayVisible           = false; 
  this->planeOpacity                  = 0.6;

  this->algorithmProbe                = new qfeFlowProbe3D();
  this->algorithmOctree               = new qfeFlowOctree();
  this->algorithmMPR                  = new qfePlanarReformat();
  this->algorithmPlane                = new qfePlanarReformat(); 
  this->algorithmLineTrace            = new qfeFlowLineTrace();
  this->algorithmParticleTrace        = new qfeFlowParticleTrace();
  this->algorithmDVR                  = new qfeRayCasting();
  this->algorithmVFR                  = new qfeRayCastingVector();
  this->algorithmMIP                  = new qfeMaximumProjection();

  this->probeActionState              = probeDefault;
  this->probeClickState               = probeSelectStart;    
  this->probeClickOffset.x            = 0.0;
  this->probeClickOffset.y            = 0.0;
  this->probeClickOffset.z            = 0.0;

  this->probePitch                    = 0.0; // For the probe in general
  this->probeVolumePitch              = -40; // For the separate US probe with VFR  

  this->viewportMouseButtonDown[0]    = false;
  this->viewportMouseButtonDown[1]    = false;
  this->viewportMouseButtonDown[2]    = false;
  this->viewportMouseButtonDown[3]    = false;
  
  this->dvrQuality                    = 2.0;

  this->prevTime                      = 0.0;  

  this->engine->SetObserverAction((void*)this, this->qfeOnAction);
  this->engine->SetObserverResize((void*)this, this->qfeOnResize);
  this->engine->SetInteractionMouseLeftButtonDown((void *)this, this->qfeOnMouseLeftButtonDown, false);
  this->engine->SetInteractionMouseLeftButtonUp((void *)this, this->qfeOnMouseLeftButtonUp);
  this->engine->SetInteractionMouseMove((void *)this, this->qfeOnMouseMove, false);
  this->engine->SetInteractionMouseWheelForward((void*)this, this->qfeOnWheelForward, false);
  this->engine->SetInteractionMouseWheelBackward((void*)this, this->qfeOnWheelBackward, false);
  this->engine->SetInteractionKeyPress((void*)this, this->qfeOnKeyPress, true);
};

//----------------------------------------------------------------------------
qfeDriverUSR::~qfeDriverUSR()
{
  delete this->algorithmProbe;
  delete this->algorithmOctree;
  delete this->algorithmMPR;
  delete this->algorithmPlane;
  delete this->algorithmLineTrace;
  delete this->algorithmParticleTrace;
  delete this->algorithmDVR;
  delete this->algorithmVFR;
  delete this->algorithmMIP;

  delete this->planeX;
  delete this->planeY;
  delete this->planeZ;    
  
  this->qfeDeleteTextures();  

  this->qfeRenderStop();
};

//----------------------------------------------------------------------------
bool qfeDriverUSR::qfeRenderCheck()
{
  int t;
  for(int i=0; i<(int)this->visuals.size(); i++)
  {
    this->visuals[i]->qfeGetVisualType(t);

    switch(t)
    {
    case visualReformatOrtho :
      this->visOrtho   = static_cast<qfeVisualReformatOrtho *>(this->visuals[i]);
      break;
    case visualReformatOblique :
      this->visMPR     = static_cast<qfeVisualReformatOblique *>(this->visuals[i]);
      break;
    case visualReformatObliqueProbe:
      this->visMPRP    = static_cast<qfeVisualReformatObliqueProbe *>(this->visuals[i]);
      break;
    case visualReformatFixed :
      this->visPlane   = static_cast<qfeVisualReformatFixed *>(this->visuals[i]);
      break;
    case visualReformatUltrasound:
      this->visMPRUS   = static_cast<qfeVisualReformatUltrasound *>(this->visuals[i]);      
      break;
    case(visualPlanesArrowHeads):
      this->visArrows = static_cast<qfeVisualPlanesArrowHeads *>(this->visuals[i]);      
      break;    
    case visualMaximum :
      this->visMIP     = static_cast<qfeVisualMaximum *>(this->visuals[i]);
      break;
    case visualRaycast :
      this->visDVR     = static_cast<qfeVisualRaycast *>(this->visuals[i]);
      break;
    case visualRaycastVector :
      this->visVFR     = static_cast<qfeVisualRaycastVector *>(this->visuals[i]);
      break;
    case visualFlowProbe3D :
      this->visProbe3D = static_cast<qfeVisualFlowProbe3D *>(this->visuals[i]);
      break;
    case visualFlowParticles :
      this->visParticleTrace = static_cast<qfeVisualFlowParticleTrace *>(this->visuals[i]);
      break;
    case visualFlowLines :
      this->visLineTrace = static_cast<qfeVisualFlowLineTrace *>(this->visuals[i]);
      break;
    case visualAides :
      this->visAides  = static_cast<qfeVisualAides *>(this->visuals[i]);
      break;
    default : cout << "qfeDriverUSR - Undefined visual type" << endl;
    }
  }

  return true;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeRenderStart()
{
  // On a resize, the start will be called again,
  // setting up a texture equal to the viewport size      
  qfeViewport *vp3D;
  qfeViewport *vp2D3;
  this->engine->GetViewport3D(&vp3D);
  this->engine->GetViewport2D(1, &vp2D3);  

  this->qfeOnResize(this);

  // Initialize viewport scale
  this->frameBufferGeo[1]->origin.qfeSetPointElements(0.0,0.0,0.0);
  this->frameBufferGeo[1]->axis[0].qfeSetVectorElements(1.0,0.0,0.0);
  this->frameBufferGeo[1]->axis[1].qfeSetVectorElements(0.0,1.0,0.0);
  this->frameBufferGeo[1]->axis[2].qfeSetVectorElements(0.0,0.0,1.0);
  this->frameBufferGeo[1]->extent[1] = 80.0;
  this->frameBufferGeo[2]->origin.qfeSetPointElements(0.0,0.0,0.0);
  this->frameBufferGeo[2]->axis[0].qfeSetVectorElements(1.0,0.0,0.0);
  this->frameBufferGeo[2]->axis[1].qfeSetVectorElements(0.0,1.0,0.0);
  this->frameBufferGeo[2]->axis[2].qfeSetVectorElements(0.0,0.0,1.0);
  this->frameBufferGeo[2]->extent[1] = 80.0;
  this->frameBufferGeo[3]->origin.qfeSetPointElements(0.0,0.0,0.0);
  this->frameBufferGeo[3]->axis[0].qfeSetVectorElements(1.0,0.0,0.0);
  this->frameBufferGeo[3]->axis[1].qfeSetVectorElements(0.0,1.0,0.0);
  this->frameBufferGeo[3]->axis[2].qfeSetVectorElements(0.0,0.0,1.0);
  this->frameBufferGeo[3]->extent[1] = 80.0;

  // Initialize supersampling
  this->frameBufferProps[1].supersampling = 2;
  this->frameBufferProps[2].supersampling = 2;
  
  // Intialize MIP
  if(this->visMIP != NULL)
  {
    qfeStudy           *study;       
    qfeVisualParameter *p;
    qfeSelectionList    l;     

    this->visMIP->qfeGetVisualStudy(&study);
    
    this->visMIP->qfeGetVisualParameter("MIP visible", &p);
    p->qfeSetVisualParameterValue(false);

    this->visMIP->qfeGetVisualParameter("MIP gradient", &p);
    p->qfeSetVisualParameterValue(true);

    this->visMIP->qfeGetVisualParameter("MIP transparency", &p);
    p->qfeSetVisualParameterValue(true);
   
    this->visMIP->qfeGetVisualParameter("MIP brightness", &p);
    p->qfeSetVisualParameterValue(0.05);
    
    this->visMIP->qfeGetVisualParameter("MIP data set", &p);
    p->qfeGetVisualParameterValue(l);
    if(study->tmip != NULL)
      l.active = 3;
    else
      l.active = 1;
    p->qfeSetVisualParameterValue(l);      

    this->algorithmMIP->qfeSetViewportSize(vp2D3->width, vp2D3->height, this->frameBufferProps[3].supersampling);
  }

  // Initialize DVR
  if(this->visDVR != NULL)
  {
    qfeVisualParameter *p;
    this->visDVR->qfeGetVisualParameter("DVR visible", &p);
    p->qfeSetVisualParameterValue(false);

    this->algorithmDVR->qfeSetViewportSize(vp3D->width, vp3D->height, this->frameBufferProps[0].supersampling);
  }

  // Initialize VFR
  if(this->visVFR != NULL)
  {    
    this->algorithmVFR->qfeSetViewportSize(vp2D3->width, vp2D3->height, this->frameBufferProps[3].supersampling);
  }

  // Initialize Ortho
  if(this->visOrtho != NULL)
  {
    qfeStudy *study;   

    this->visOrtho->qfeGetVisualStudy(&study);

    if((int)study->pcap.size() > 0)
    {
      this->qfeGetDataSlice(study->pcap.front(), qfePlaneSagittal, &this->planeX);  
      this->qfeGetDataSlice(study->pcap.front(), qfePlaneCoronal,  &this->planeY);
      this->qfeGetDataSlice(study->pcap.front(), qfePlaneAxial,    &this->planeZ);
    }
  }

  // Initialize MPR
  if(this->visMPR != NULL)
  {
    qfeVisualParameter *p;
    
    this->visMPR->qfeGetVisualParameter("MPR visible", &p);
    p->qfeSetVisualParameterValue(true);

    this->visMPR->qfeGetVisualParameter("MPR opacity 1", &p);
    p->qfeSetVisualParameterValue(1.0);

    this->visMPR->qfeGetVisualParameter("MPR opacity 2", &p);
    p->qfeSetVisualParameterValue(0.6);
  }

  // Initialize MPRP
  if(this->visMPRP != NULL)
  {
    qfeVisualParameter *p;
    qfeSelectionList    dataRepresentation;
    
    this->visMPRP->qfeGetVisualParameter("MPRP data representation", &p);
    p->qfeGetVisualParameterValue(dataRepresentation);
    dataRepresentation.active = 2;
    p->qfeSetVisualParameterValue(dataRepresentation);
  }

  // Initialize MPR US
  if(this->visMPRUS != NULL)
  {
    qfeVisualParameter *p;
    qfeSelectionList    l;       

    this->visMPRUS->qfeGetVisualParameter("MPRUS data context repr.", &p);
    p->qfeGetVisualParameterValue(l);
    l.active = 0;
    p->qfeSetVisualParameterValue(l);

    this->visMPRUS->qfeGetVisualParameter("MPRUS data set", &p);
    p->qfeGetVisualParameterValue(l);
    l.active = 4;
    p->qfeSetVisualParameterValue(l);    
  }

  // Initialize MPR arrowheads
  if(this->visArrows != NULL)
  {
    qfeVisualParameter *p;
    qfeSelectionList    l;       

    this->visArrows->qfeGetVisualParameter("Arrows spacing (mm)", &p);
    p->qfeSetVisualParameterValue(2.5);

    this->visArrows->qfeGetVisualParameter("Arrows scaling factor", &p);
    p->qfeSetVisualParameterValue(2.0);
    
    this->visArrows->qfeGetVisualParameter("Arrows data representation", &p);
    p->qfeGetVisualParameterValue(l);
    l.active = 1;
    p->qfeSetVisualParameterValue(l);
  }

  // Initialize fixed plane
  if(this->visPlane != NULL)
  {
    qfeVisualParameter *p;
    qfeSelectionList    dataSet;
    
    this->visPlane->qfeGetVisualParameter("Plane data set", &p);
    p->qfeGetVisualParameterValue(dataSet);
    dataSet.active = 3;
    p->qfeSetVisualParameterValue(dataSet);
  }

  // Initialize particle trace
  if(this->visParticleTrace != NULL)
  {
    qfeStudy *study;
    qfeVisualParameter p;

    this->visParticleTrace->qfeGetVisualStudy(&study);

    p.qfeSetVisualParameter("Particles visible", true);    
    this->visParticleTrace->qfeAddVisualParameter(p);

    p.qfeSetVisualParameter("Particles lifetime", (int)study->pcap.size(), 1, 1, 500);
    this->visParticleTrace->qfeAddVisualParameter(p);

    this->scene->qfeSceneUpdate();
  }

  // Initialize line trace
  if(this->visLineTrace != NULL)
  {
    qfeStudy *study;
    qfeVisualParameter p;

    this->visLineTrace->qfeGetVisualStudy(&study); 

    p.qfeSetVisualParameter("Lines phases static", (int)study->pcap.size(), 1, 1, 500);
    this->visLineTrace->qfeAddVisualParameter(p);

    this->scene->qfeSceneUpdate();
  }
  
  // Initialize probe
  if(this->visProbe3D != NULL)
  {
    qfeStudy    *study;
    int          current;  
        
    this->visProbe3D->qfeGetVisualStudy(&study);
    this->visProbe3D->qfeGetVisualActiveVolume(current);

    this->algorithmProbe->qfeSetProbeSuperSamplingFactor(this->frameBufferProps[0].supersampling);

    // Intialize probe properties    
    this->planeInteractionState      = planeInteractRotate;
    this->probeSeedingStateParticles = probeSeedingNone;    
    this->probeSeedingStateLines     = probeSeedingNone;
    
    for(int i=0; i<(int)study->probe3D.size(); i++)
    {
      study->probe3D[i].interactionState = probeInteractNone;            
      study->probe3D[i].orthoPlaneOffset = 0.5f;            
    }

    // Compute the octree, use tmop if available
    if((int)study->tmop != NULL)
    {
      this->algorithmOctree->qfeBuildFlowOctree(study->tmop);
    }
    else
    if((int)study->pcap.size() > 0)
    {
      this->algorithmOctree->qfeBuildFlowOctree(study->pcap[current]);
    }
    else
    {
      this->algorithmOctree->qfeBuildFlowOctree(NULL);
      cout << "qfeDriverUSR::qfeRenderStart - Could not initialize octree" << endl;
    }

    //this->algorithmOctree->qfeWriteTest();
  }
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeRenderWait()
{
  qfeViewport *vp3D;
  int supersampling = this->frameBufferProps[0].supersampling;

  this->engine->GetViewport3D(&vp3D);

  // Initialize OpenGL state
  glEnable(GL_DEPTH_TEST);

  // Initialize color map
  qfeColorMap *colormap;
  if(this->visOrtho != NULL)
  {
    this->visOrtho->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  if(this->visMPR != NULL)
  {
    this->visMPR->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  if(this->visMPRP != NULL)
  {
    this->visMPRP->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();    
  }
  if(this->visPlane != NULL)
  {
    this->visPlane->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  if(this->visMIP != NULL)
  {
    this->visMIP->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  if(this->visDVR != NULL)
  {
    this->visDVR->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
    colormap->qfeUploadGradientMap();
  }
  if(this->visParticleTrace != NULL)
  {
    this->visParticleTrace->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  if(this->visLineTrace != NULL)
  {
    this->visLineTrace->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  
  // Clear the intersection buffer
  this->qfeEnableBuffer(this->intersectTexColor[0], this->intersectTexDepth[0]);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); 
  
  // Clear the front and back clipping buffer
  this->qfeEnableBuffer(this->clippingTexColor[0], this->clippingFrontTexDepth[0]);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  this->qfeEnableBuffer(this->clippingTexColor[0], this->clippingBackTexDepth[0]);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  this->qfeDisableBuffer(0);

  // Note strict order, first geometry, than volume renderings

  // Render Particles
  if(this->visParticleTrace != NULL)
  {
     this->qfeRenderVisualParticleTrace(this->visParticleTrace, supersampling);
  }

  // Render Lines
  if(this->visLineTrace != NULL)
  {
     this->qfeRenderVisualLineTrace(this->visLineTrace, supersampling);     
  }

  // Render Planar Reformat    
  if(this->visOrtho != NULL)
  {
     this->qfeRenderVisualOrtho(this->visOrtho, supersampling);
  }
  if(this->visMPRP != NULL)
  {     
    this->qfeRenderVisualMPRP(this->visMPRP, supersampling);       
  }
  if(this->visMPR != NULL)
  {
     this->qfeRenderVisualMPR(this->visMPR, supersampling);
  }  
  if(this->visPlane != NULL)
  {
    this->qfeRenderVisualPlane(this->visPlane, supersampling);
  }

  // Render Probe
  if(this->visProbe3D != NULL)
  {
     this->qfeRenderVisualProbe(this->visProbe3D, vp3D, supersampling);
  }

  // Render Visual Aides
  if(this->visAides != NULL)
  {
    this->qfeRenderVisualAides(this->visAides, supersampling);
  }

  // Render DVR (Last to blend with geometry)
  if(this->visDVR != NULL)
  {
    this->qfeRenderVisualDVR(this->visDVR, supersampling);
  }
};

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeRenderWait1()
{
  int supersampling = this->frameBufferProps[1].supersampling;

  // Initialize OpenGL state
  glEnable(GL_DEPTH_TEST);

  // Initialize color map
  qfeColorMap *colormap;
  if(this->visMPRUS != NULL)
  {
    this->visMPRUS->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }    
  if(this->visArrows != NULL)
  {
    this->visArrows->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  
  // Render Planar Reformat
  if(this->visMPRUS != NULL)
  {
    this->qfeRenderVisualMPRUS(this->visMPRUS, qfePlaneProbeParallel, supersampling);
  }   

  // Arrow heads
  if(this->visArrows != NULL)
  {
    glEnable(GL_BLEND);
    this->qfeRenderVisualPlanesArrows(this->visArrows, qfePlaneProbeParallel, supersampling);        
    glDisable(GL_BLEND);
  } 
};

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeRenderWait2()
{
  int supersampling = this->frameBufferProps[2].supersampling;

  // Initialize OpenGL state
  glEnable(GL_DEPTH_TEST);

  // Initialize color map
  qfeColorMap *colormap;
  if(this->visMPRUS != NULL)
  {
    this->visMPRUS->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }     
  if(this->visArrows != NULL)
  {
    this->visArrows->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  
  // Render Planar Reformat
  if(this->visMPRUS != NULL)
  {
    this->qfeRenderVisualMPRUS(this->visMPRUS, qfePlaneProbeOrthogonal, supersampling);
  } 
  // Arrow heads
  if(this->visArrows != NULL)
  {
    glEnable(GL_BLEND);
    this->qfeRenderVisualPlanesArrows(this->visArrows, qfePlaneProbeOrthogonal, supersampling);        
    glDisable(GL_BLEND);
  } 
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeRenderWait3()
{
  int supersampling = this->frameBufferProps[3].supersampling;

  // Initialize OpenGL state
  glEnable(GL_DEPTH_TEST);

  // Initialize color map
  qfeColorMap *colormap;
  if(this->visVFR != NULL)
  {
    this->visVFR->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
    colormap->qfeUploadGradientMap();
  }

  // Clear the intersection buffer
  this->qfeEnableBuffer(this->intersectTexColor[1], this->intersectTexDepth[1]);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);  

  // Clear the front and back clipping buffer
  this->qfeEnableBuffer(this->clippingTexColor[1], this->clippingFrontTexDepth[1]);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
  this->qfeEnableBuffer(this->clippingTexColor[1], this->clippingBackTexDepth[1]);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  this->qfeDisableBuffer(3);
 
  // Render Planar Reformat
  if(this->visVFR != NULL)
  {
    this->qfeRenderVisualVFR(this->visVFR, supersampling);
  }  

  // Render MIP (blend on top)
  if(this->visMIP != NULL)
  {
    this->qfeRenderVisualMIP(this->visMIP, supersampling);
  }
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeRenderSelect()
{
  qfeColorRGB color;  
  qfeStudy   *study;
  qfeVolume  *volume;
  qfeFloat    vl, vu;

  qfeVector   eyeVec;
  qfeMatrix4f mvInv;
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);

  // Get eye vector in patient coordinates;
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;

  // Selection objects for the probe
  if(this->visProbe3D != NULL)
  {
    this->visProbe3D->qfeGetVisualStudy(&study);

    for(int i=0; i<(int)study->probe3D.size(); i++)
    {
       qfeProbe3D p;
       qfeVector  x, y, z, n, d;

      // Render the rotation handle
      color.r = color.g = i / 255.0f;
      color.b = SELECT_PROBE_HANDLE;
      this->qfeSetSelectColor(color);
      this->algorithmProbe->qfeRenderProbeHandleRotation(&study->probe3D[i], color);

      // Render the probe
      color.r = color.g = i / 255.0f;
      color.b = SELECT_PROBE;

      this->qfeSetSelectColor(color);
      this->algorithmProbe->qfeSetProbeAppearance(qfeFlowProbe3D::qfeFlowProbeFlat);
      this->algorithmProbe->qfeRenderProbe(&study->probe3D[i]);  

      // A local probe is created, which is aligned with the plane normal.
      p = study->probe3D[i];        
      p.topRadius  = p.topRadius  * 1.15;
      p.baseRadius = p.baseRadius * 1.15;
      p.length     = p.length     * 1.15;

      study->probe3D[i].parallelPlane.qfeGetFrameAxes(d.x, d.y, d.z, d.x, d.y, d.z, n.x, n.y, n.z);
      
      x = n;
      z.qfeSetVectorElements(p.axes[2][0], p.axes[2][1], p.axes[2][2]);
      qfeVector::qfeVectorNormalize(x);
      qfeVector::qfeVectorNormalize(z); 
      qfeVector::qfeVectorOrthonormalBasis1(z,x,y);
      qfeVector::qfeVectorNormalize(y);

      p.axes[0][0] = x.x; p.axes[0][1] = x.y; p.axes[0][2] = x.z;
      p.axes[1][0] = y.x; p.axes[1][1] = y.y; p.axes[1][2] = y.z;
      p.axes[2][0] = z.x; p.axes[2][1] = z.y; p.axes[2][2] = z.z;      

      // Render the probe clip plane to the stencil buffer
      // Disable updates in the color and depth attachments;
      glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
      glDepthMask(GL_FALSE);

      glEnable(GL_STENCIL_TEST);

      // Always pass a one to the stencil buffer where we draw
      glStencilFunc(GL_ALWAYS, 1, 0xFFFFFFFF);
      glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);

      this->algorithmProbe->qfeRenderProbeClipPlane(&p);

      // Turn the color and depth buffers back on
      glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
      glDepthMask(GL_TRUE);

      glDisable(GL_STENCIL_TEST);
    }
  }

  // Selection objects for ortho planes
  if(this->visOrtho != NULL)
  { 
    qfeVisualParameter *param;
    bool                paramShowX, paramShowY, paramShowZ;
    qfeColorRGB         color;  

    // Get the visual parameters
    this->visOrtho->qfeGetVisualParameter("Ortho plane RL (X)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);    

    this->visOrtho->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);    

    this->visOrtho->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);   

    // Process the visual parameters
    this->visOrtho->qfeGetVisualStudy(&study);
    
    if(((int)study->pcap.size() > 0) && ((int)study->probe3D.size() == 0))
    {
      if(paramShowX)
      {
        color.r = color.g = 1.0f / 255.0f;
        color.b = SELECT_PLANE_ORTHO;
        this->qfeSetSelectColor(color);
        this->algorithmMPR->qfeRenderPolygon3D(study->pcap.front(), this->planeX, NULL);
      }

      if(paramShowY)
      {
        color.r = color.g = 2.0f / 255.0f;
        color.b = SELECT_PLANE_ORTHO;
        this->qfeSetSelectColor(color);
        this->algorithmMPR->qfeRenderPolygon3D(study->pcap.front(), this->planeY, NULL);
      }
      
      if(paramShowZ)
      {
        color.r = color.g = 3.0f / 255.0f;
        color.b = SELECT_PLANE_ORTHO;
        this->qfeSetSelectColor(color);
        this->algorithmMPR->qfeRenderPolygon3D(study->pcap.front(), this->planeZ, NULL);
      }
    }   
  }

  // Selection objects for reformatting
  if(this->visMPRP != NULL)
  {
    qfeVisualParameter *param;
    qfeSelectionList    paramDataList;
    bool                paramVisible;

    this->visMPRP->qfeGetVisualStudy(&study);

    // Get parameters
    this->visMPRP->qfeGetVisualParameter("MPRP visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);

    this->visMPRP->qfeGetVisualParameter("MPRP data set", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
    this->qfeGetDataSet((qfeDataTypeIndex)paramDataList.active, this->visMPRP, 0, &volume, vl, vu);

    // Assign a color
    if(paramVisible)
    {
      for(int i=0; i<(int)study->probe3D.size(); i++)
      {
        color.r = color.g = i / 255.0f;
        color.b = SELECT_PROBE_ORTHO;
        this->qfeSetSelectColor(color);

        this->algorithmMPR->qfeRenderCircle3D(volume, volume, &study->probe3D[i].orthoPlane, NULL);
      }
    }
  }
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeRenderStop()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualProbe(qfeVisualFlowProbe3D *visual, qfeViewport *viewport, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *volume;  
  qfeLightSource     *light;
  int                 current;
  qfeMatrix4f         P2V, V2T, P2T;

  qfeVisualParameter *param;
  qfeSelectionList    paramListAppearance;
  bool                paramProbeVisible;
  bool                paramContourVisible;
  bool                paramAxes;
  bool                paramOctreeVisible;
  qfeSelectionList    paramOctreeType;
  double              paramOctreeCoherence;
  int                 paramOctreeLevel;
  bool                paramPlaneVisible[3];

  this->scene->qfeGetSceneLightSource(&light);

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;
  if((int)study->pcap.size() <= 0) return qfeError;

  this->visProbe3D->qfeGetVisualParameter("Probe visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramProbeVisible);

  this->visProbe3D->qfeGetVisualParameter("Probe appearance", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramListAppearance);

  this->visProbe3D->qfeGetVisualParameter("Probe contour", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContourVisible);

  this->visProbe3D->qfeGetVisualParameter("Probe axes", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramAxes);

  this->visProbe3D->qfeGetVisualParameter("Octree visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramOctreeVisible);

  this->visProbe3D->qfeGetVisualParameter("Octree type", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramOctreeType);

  this->visProbe3D->qfeGetVisualParameter("Octree coherence level", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramOctreeCoherence);

  this->visProbe3D->qfeGetVisualParameter("Octree hierarchy level", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramOctreeLevel);

  volume = study->pcap[current];

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  P2T = P2V*V2T;

  // Get the view vector
  qfeMatrix4f mvInv;
  qfeVector   eyeVec;
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);
  eyeVec.qfeSetVectorElements(0.0,0.0,-1.0);
  eyeVec = eyeVec*mvInv;

  // Prepare octree rendering
  qfePoint *center = new qfePoint[5];

  glEnable(GL_DEPTH_TEST);

  if(this->probeActionState == probeClick)
    paramProbeVisible = false;


  // Render the probes
  if(paramProbeVisible)
  {
    qfeVector   probeNormal;
    qfeVector   eyeVecInterpolated;

    for(int i=0; i<(int)study->probe3D.size(); i++)
    {
      study->probe3D[i].color.r =
        study->probe3D[i].color.g =
          study->probe3D[i].color.b = 0.4;
      study->probe3D[i].color.a    = 1.0;

      probeNormal = study->probe3D[i].axes[0];

      this->algorithmProbe->qfeGetProbeCenterline(&study->probe3D[i], 5, &center);
      
      // Render the probe to the intersect buffer
      this->qfeEnableBuffer(this->intersectTexColor[0], this->intersectTexDepth[0]);      
      this->algorithmProbe->qfeRenderProbe(&study->probe3D[i]);      
      this->qfeDisableBuffer(0);

      this->algorithmProbe->qfeSetProbeAppearance((qfeFlowProbe3D::qfeFlowProbe3DAppearance)paramListAppearance.active);
      this->algorithmProbe->qfeSetProbeContour(paramContourVisible);
      this->algorithmProbe->qfeSetProbeLightSource(light);

      this->algorithmProbe->qfeRenderProbe(&study->probe3D[i]);      

      if(paramAxes)       this->algorithmProbe->qfeRenderProbeAxes(&study->probe3D[i], 2.0);
    }
  }

  // Render the octree
  if(paramOctreeVisible)
  {
    switch(paramOctreeType.active)
    {
      case 0: this->algorithmOctree->qfeRenderOctree((qfeFloat)paramOctreeCoherence);
              break;
      case 1: this->algorithmOctree->qfeRenderOctree((int)paramOctreeLevel);
              break;
      case 2: for(int i=0; i<5; i++)
                this->algorithmOctree->qfeRenderOctree(center[i], (int)paramOctreeLevel);
              break;
    }
  }

  // Render workflow steps
  if(this->visOrtho != NULL)
  {
    this->visProbe3D->qfeGetVisualParameter("Ortho plane RL (X)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramPlaneVisible[0]);

    this->visProbe3D->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramPlaneVisible[1]);

    this->visProbe3D->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramPlaneVisible[2]);
  }

  qfePoint     probePointVisible[2];
  qfeFloat     probePointDistance[2];
  qfeFloat     deltaView    = 0.1;
  qfeFloat     deltaVisible = 6.0;
  qfeFloat     dx, dy, dz;
  qfeColorRGBA colorVisible, colorHidden;

  colorVisible.r = 0.0; colorVisible.g = 1.0; colorVisible.b = 0.0; colorVisible.a = 1.0;
  colorHidden.r  = 0.0; colorHidden.g  = 1.0; colorHidden.b  = 0.0; colorHidden.a  = 0.4;

  probePointVisible[0] = this->probePoints[0] - deltaView*eyeVec;
  probePointVisible[1] = this->probePoints[1] - deltaView*eyeVec;
 
  dx = dy = dz = 65536.0f;
  if(paramPlaneVisible[0]) this->qfeGetDistancePointPlane(this->planeX, this->probePoints[0], dx);
  if(paramPlaneVisible[1]) this->qfeGetDistancePointPlane(this->planeY, this->probePoints[0], dy);
  if(paramPlaneVisible[2]) this->qfeGetDistancePointPlane(this->planeZ, this->probePoints[0], dz);
  probePointDistance[0] = min(dx, min(dy,dz));

  dx = dy = dz = 65536.0f;
  if(paramPlaneVisible[0]) this->qfeGetDistancePointPlane(this->planeX, this->probePoints[1], dx);
  if(paramPlaneVisible[1]) this->qfeGetDistancePointPlane(this->planeY, this->probePoints[1], dy);
  if(paramPlaneVisible[2]) this->qfeGetDistancePointPlane(this->planeZ, this->probePoints[1], dz);
  probePointDistance[1] = min(dx, min(dy,dz));

  if(this->probeActionState == probeClick)
  {
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    if((this->probeClickState == probeSelectSecond))
    {
      if(probePointDistance[0] <= deltaVisible) this->qfeDrawPoint(this->probePoints[0], supersampling, colorHidden);
    }
    if(this->probeClickState == probeSelectThird)
    {
      if(probePointDistance[0] <= deltaVisible) this->qfeDrawPoint(this->probePoints[0], supersampling, colorHidden);
      if(probePointDistance[1] <= deltaVisible) this->qfeDrawPoint(this->probePoints[1], supersampling, colorHidden);      
    }
    glEnable(GL_DEPTH_TEST);
    if((this->probeClickState == probeSelectSecond))
    {
      if(probePointDistance[0] <= deltaVisible) this->qfeDrawPoint(probePointVisible[0], supersampling, colorVisible);
    }
    if(this->probeClickState == probeSelectThird)
    {
      if(probePointDistance[0] <= deltaVisible) this->qfeDrawPoint(probePointVisible[0], supersampling, colorVisible);
      if(probePointDistance[1] <= deltaVisible) this->qfeDrawPoint(probePointVisible[1], supersampling, colorVisible);      
    }
	  glDisable(GL_BLEND);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualOrtho(qfeVisualReformat *visual, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *volumeBounding;
  qfeVolume          *volumeTexture;
  qfeGrid            *grid;
  qfeColorMap        *colormap;
  qfePoint            origin;
  qfePoint            extent;
  unsigned int        dims[3];
  qfeFloat            size, depth;
  
  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  int                 paramInterp;
  double              paramBrightness;
  double              paramContrast;
  qfeFloat            paramRange[2];
  bool                paramShowX, paramShowY, paramShowZ;
  bool                paramProbeVisible;
  bool                paramDVRVisible;  
  int                 current;
  qfeColorRGB         colorX, colorY, colorZ;
  qfeFloat            tt;
  qfeVector           x,xp,y,yp,z,n,d;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  // Get visual parameters
  visual->qfeGetVisualParameter("Ortho data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("Ortho data vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("Ortho data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("Ortho data interpolation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInterpList);  
  paramInterp = paramInterpList.active;

  visual->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);    

  visual->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);    

  visual->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);  

  visual->qfeGetVisualParameter("Ortho contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);   

  visual->qfeGetVisualParameter("Ortho brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);   

  // Check if the probe is visible
  if(this->visProbe3D != NULL)
  {
    this->visProbe3D->qfeGetVisualParameter("Probe visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramProbeVisible);
  }
  else paramProbeVisible = false;

  // Check if the volume rendering is visible
  if(this->visDVR != NULL)
  {
    this->visDVR->qfeGetVisualParameter("DVR visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDVRVisible);
  }
  else paramDVRVisible = false;
 
  // Process parameters  
  this->qfeGetDataSet((qfeDataTypeIndex)dataTypePCAP, visual, current, &volumeBounding, paramRange[0], paramRange[1]); // PCA-P
  this->qfeGetDataSet((qfeDataTypeIndex)paramData,    visual, current, &volumeTexture,  paramRange[0], paramRange[1]); // Selected volume

  if(volumeBounding != NULL)
  {
    volumeBounding->qfeGetVolumeGrid(&grid);
    volumeBounding->qfeGetVolumeTriggerTime(tt);
    volumeBounding->qfeGetVolumeSize(dims[0], dims[1], dims[2]);
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);   
    grid->qfeGetGridExtent(extent.x, extent.y, extent.z);
  }
  else 
  {
    volumeBounding = volumeTexture;
    origin.x = origin.y = origin.z = 0.0;  
    extent.x = extent.y = extent.z = 1.0;
    dims[0] = dims[1] = dims[2] = 1;
  }

  size  = max(max(dims[0]*extent.x, dims[1]*extent.y), dims[2]*extent.z) * sqrt(2.0f);
  depth = sqrt(pow(dims[0]*extent.x, 2.0f) + pow(dims[1]*extent.y, 2.0f) + pow(dims[2]*extent.y, 2.0f));
  
  // Determine line color
  colorX = colorY = colorZ = this->planeColorDefault;
  if(this->planeSelected == qfePlaneX)  colorX = this->planeColorHighlight;
  if(this->planeSelected == qfePlaneY)  colorY = this->planeColorHighlight;
  if(this->planeSelected == qfePlaneZ)  colorZ = this->planeColorHighlight;

  // Update algorithm parameters    
  this->algorithmMPR->qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
  this->algorithmMPR->qfeSetPlaneDataComponent(paramComp);
  this->algorithmMPR->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmMPR->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  this->algorithmMPR->qfeSetPlaneTriggerTime(tt); 
  this->algorithmMPR->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  this->algorithmMPR->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmMPR->qfeSetPlaneContrast(paramContrast);

  int probeCount = (int)study->probe3D.size();
  if (this->probeActionState == probeClick)
    probeCount = 0;

  // If there is no probe defined: render the planes
  if(probeCount <= 0)
  {
    if(paramShowX)
    {
      this->algorithmMPR->qfeSetPlaneBorderColor(colorX);
      this->algorithmMPR->qfeRenderPolygon3D(volumeTexture, this->planeX, colormap);      
    }

    if(paramShowY) 
    { 
      this->algorithmMPR->qfeSetPlaneBorderColor(colorY);
      this->algorithmMPR->qfeRenderPolygon3D(volumeTexture, this->planeY, colormap);      
    }

    if(paramShowZ)
    {  
      this->algorithmMPR->qfeSetPlaneBorderColor(colorZ);
      this->algorithmMPR->qfeRenderPolygon3D(volumeTexture, this->planeZ, colormap);      
    }
  } 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualMPR(qfeVisualReformat *visual, int supersampling)
{  
  qfeStudy           *study;
  qfeLightSource     *light;
  qfeVolume          *volumeBounding;
  qfeVolume          *volumeTexture;
  qfeGrid            *grid;
  qfeColorMap        *colormap;
  qfePoint            origin;
  qfePoint            extent;
  unsigned int        dims[3];
  qfeFloat            size, depth;
  
  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  int                 paramInterp;
  int                 paramBalance;
  double              paramBrightness;
  double              paramContrast;
  qfeFloat            paramRange[2];  
  bool                paramProbeVisible;
  bool                paramDVRVisible;
  bool                paramShowPlane;
  double              paramOpacity1;
  double              paramOpacity2;
  int                 current;  
  qfeFloat            tt;
  qfeVector           x,xp,y,yp,z,n,d;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  this->scene->qfeGetSceneLightSource(&light);

  // Get visual parameters
  visual->qfeGetVisualParameter("MPR visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowPlane);    

  visual->qfeGetVisualParameter("MPR data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("MPR data vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("MPR data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("MPR data interpolation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInterpList);  
  paramInterp = paramInterpList.active;

  visual->qfeGetVisualParameter("MPR mix (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBalance);  

  visual->qfeGetVisualParameter("MPR opacity 1", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramOpacity1);  

  visual->qfeGetVisualParameter("MPR opacity 2", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramOpacity2);  

  visual->qfeGetVisualParameter("MPR contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);   

  visual->qfeGetVisualParameter("MPR brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);   

  // Check if the probe is visible
  if(this->visProbe3D != NULL)
  {
    this->visProbe3D->qfeGetVisualParameter("Probe visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramProbeVisible);
  }
  else paramProbeVisible = false;

  // Check if the volume rendering is visible
  if(this->visDVR != NULL)
  {
    this->visDVR->qfeGetVisualParameter("DVR visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDVRVisible);
  }
  else paramDVRVisible = false;

  // Process parameters 
  this->qfeGetDataSet((qfeDataTypeIndex)dataTypePCAP, visual, current, &volumeBounding, paramRange[0], paramRange[1]); // PCA-P
  this->qfeGetDataSet((qfeDataTypeIndex)paramData,    visual, current, &volumeTexture,  paramRange[0], paramRange[1]); // Selected volume

  if(volumeBounding != NULL)
  {
    volumeBounding->qfeGetVolumeGrid(&grid);
    volumeBounding->qfeGetVolumeTriggerTime(tt);
    volumeBounding->qfeGetVolumeSize(dims[0], dims[1], dims[2]);
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);   
    grid->qfeGetGridExtent(extent.x, extent.y, extent.z);
  }
  else 
  {
    volumeBounding = volumeTexture;
    origin.x = origin.y = origin.z = 0.0;  
    extent.x = extent.y = extent.z = 1.0;
    dims[0] = dims[1] = dims[2] = 1;
  }

  size  = max(max(dims[0]*extent.x, dims[1]*extent.y), dims[2]*extent.z) * sqrt(2.0f);
  depth = sqrt(pow(dims[0]*extent.x, 2.0f) + pow(dims[1]*extent.y, 2.0f) + pow(dims[2]*extent.y, 2.0f));
  
  // Update algorithm parameters    
  this->algorithmMPR->qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
  this->algorithmMPR->qfeSetPlaneDataComponent(paramComp);
  this->algorithmMPR->qfeSetPlaneDataComponent2(paramComp);
  this->algorithmMPR->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmMPR->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  this->algorithmMPR->qfeSetPlaneTriggerTime(tt); 
  this->algorithmMPR->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  this->algorithmMPR->qfeSetPlaneBalance(paramBalance);
  this->algorithmMPR->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmMPR->qfeSetPlaneContrast(paramContrast);
  this->algorithmMPR->qfeSetPlaneOpacity(paramOpacity1);

  int probeCount = (int)study->probe3D.size();
  if (this->probeActionState == probeClick)
    probeCount = 0;

  for(int i=0; i<probeCount; i++)
  {      
    // Update the plane position and orientation
    study->probe3D[i].parallelPlane.qfeSetFrameExtent(2.0*max(study->probe3D[i].baseRadius, study->probe3D[i].topRadius), study->probe3D[i].length);
    study->probe3D[i].parallelPlane.qfeSetFrameOrigin(study->probe3D[i].origin.x, study->probe3D[i].origin.y, study->probe3D[i].origin.z);

    if(study->probe3D[i].selected && paramProbeVisible && paramShowPlane)
    {
      qfeProbe3D   p;  
      
      // Create a local probe
      p = study->probe3D[i];        
      p.topRadius  = p.topRadius  * 1.15;
      p.baseRadius = p.baseRadius * 1.15;
      p.length     = p.length     * 1.15;

      study->probe3D[i].parallelPlane.qfeGetFrameAxes(d.x, d.y, d.z, d.x, d.y, d.z, n.x, n.y, n.z);
      
      x = n;
      z.qfeSetVectorElements(p.axes[2][0], p.axes[2][1], p.axes[2][2]);
      qfeVector::qfeVectorNormalize(x);
      qfeVector::qfeVectorNormalize(z); 
      qfeVector::qfeVectorOrthonormalBasis1(z,x,y);
      qfeVector::qfeVectorNormalize(y);

      p.axes[0][0] = x.x; p.axes[0][1] = x.y; p.axes[0][2] = x.z;
      p.axes[1][0] = y.x; p.axes[1][1] = y.y; p.axes[1][2] = y.z;
      p.axes[2][0] = z.x; p.axes[2][1] = z.y; p.axes[2][2] = z.z;  
  
      // Render the clip plane, computed as the intersecting projection between probe and plane
      glEnable(GL_STENCIL_TEST);      

      glClear(GL_STENCIL_BUFFER_BIT);           
      glStencilFunc(GL_ALWAYS, 1, 0xFFFFFFFF);
      glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE); 
      glDepthMask(GL_FALSE);
      glColorMask(GL_FALSE,GL_FALSE,GL_FALSE,GL_FALSE); 

      this->algorithmProbe->qfeRenderProbeClipPlane(&study->probe3D[i], &study->probe3D[i].parallelPlane);

      glColorMask(GL_TRUE,GL_TRUE,GL_TRUE,GL_TRUE);      
      glDepthMask(GL_TRUE);           

      // Until stencil test is disabled, only write to areas where the stencil buffer equals one.
      glStencilFunc(GL_EQUAL, 0, 0xFFFFFFFF);
      glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    }
      
    // Render the planes
    if(study->probe3D[i].selected && paramShowPlane)
    {
      if(paramDVRVisible)
      {
        qfeVector           eyeVec, d, norm;
        qfeMatrix4f         mvInv;

        // Get eye vector in patient coordinates;
        qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);
        eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
        eyeVec = eyeVec*mvInv;
        qfeVector::qfeVectorNormalize(eyeVec);

        study->probe3D[i].parallelPlane.qfeGetFrameAxes(d.x,d.y,d.z, d.x,d.y,d.z, norm.x,norm.y,norm.z);

        if(norm*eyeVec >= 0)
        {
          this->qfeEnableBuffer(this->clippingTexColor[0], this->clippingBackTexDepth[0]);
            glEnable(GL_CULL_FACE);
            glCullFace(GL_FRONT);
              this->algorithmMPR->qfeRenderClipBox3D(volumeBounding, &study->probe3D[i].parallelPlane, size, depth);
            glDisable(GL_CULL_FACE);
          this->qfeDisableBuffer(0);                 
        }
        else
        {
          this->qfeEnableBuffer(this->clippingTexColor[0], this->clippingFrontTexDepth[0]);
            glEnable(GL_CULL_FACE);
            glCullFace(GL_BACK);
              this->algorithmMPR->qfeRenderClipBox3D(volumeBounding, &study->probe3D[i].parallelPlane, size, depth);
            glDisable(GL_CULL_FACE);
          this->qfeDisableBuffer(0);                 
        }
      }
      else
      {
        // Render the polygon         
        glEnable(GL_BLEND);
        this->algorithmMPR->qfeRenderPolygon3D(volumeBounding, volumeTexture, &study->probe3D[i].parallelPlane, colormap);                         
      }
      glDisable(GL_STENCIL_TEST);     
      this->algorithmMPR->qfeRenderPolygonLine3D(volumeBounding, &study->probe3D[i].parallelPlane);
      
      // Render the translucent ultrasound plane in the probe
      if(!paramDVRVisible)
      {      
        qfeFrameSlice currentPlane;          

        currentPlane.qfeSetFrameExtent(2.0*max(study->probe3D[i].baseRadius, study->probe3D[i].topRadius), study->probe3D[i].length);
        currentPlane.qfeSetFrameOrigin(study->probe3D[i].origin.x, study->probe3D[i].origin.y, study->probe3D[i].origin.z);
        currentPlane.qfeSetFrameAxes(study->probe3D[i].axes[1].x, study->probe3D[i].axes[1].y, study->probe3D[i].axes[1].z,
                                     study->probe3D[i].axes[2].x, study->probe3D[i].axes[2].y, study->probe3D[i].axes[2].z,
                                     study->probe3D[i].axes[0].x, study->probe3D[i].axes[0].y, study->probe3D[i].axes[0].z);
        
        if(paramOpacity2 > this->planeOpacity) 
          paramOpacity2 = this->planeOpacity;
        if(paramOpacity2 != 1.0) 
        
        glDepthMask(GL_FALSE);
        this->algorithmMPR->qfeSetPlaneOpacity(paramOpacity2);
        this->algorithmMPR->qfeRenderPolygonUS3D(volumeBounding, volumeTexture, &currentPlane, study->probe3D[i].baseRadius, study->probe3D[i].topRadius, colormap);
        this->algorithmMPR->qfeSetPlaneOpacity(1.0);
        glDepthMask(GL_TRUE);      
      }      
    }
  }  

  // Clean up
  glDepthFunc(GL_LESS);
  glDisable(GL_STENCIL_TEST);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualMPRP(qfeVisualReformat *visual, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *volumeBounding;
  qfeVolume          *volumeTexture;
  qfeColorMap        *colormap;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramCompUSList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  qfeSelectionList    paramShapeList;
  int                 paramData;
  int                 paramDataUS;
  int                 paramComp;
  int                 paramContext;
  int                 paramFocus;
  int                 paramRepr;
  int                 paramInterp;
  int                 paramBalance;
  //int                 paramShape;
  int                 paramScale;
  qfeFloat            paramRange[2];
  qfeFloat            paramRange2[2];
  bool                paramShowPlane;
  bool                paramProbeVisible;
  double              paramBrightness;
  double              paramContrast;
  int                 paramThreshold;
  int                 current;
  qfeColorRGB         color;
  qfeFloat            tt;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  // Get visual parameters
  visual->qfeGetVisualParameter("MPRP visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowPlane);

  visual->qfeGetVisualParameter("MPRP data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("MPRP data vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("MPRP data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("MPRP data interpolation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInterpList);
  paramInterp = paramInterpList.active;

  visual->qfeGetVisualParameter("MPRP scale (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramScale);

  visual->qfeGetVisualParameter("MPRP mix (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBalance);

  visual->qfeGetVisualParameter("MPRP brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);

  visual->qfeGetVisualParameter("MPRP contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);

  if(this->visProbe3D != NULL)
  {
    this->visProbe3D->qfeGetVisualParameter("Probe visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramProbeVisible);
  }
  else paramProbeVisible = false;

  if(this->visMPRUS != NULL)
  {
    this->visMPRUS->qfeGetVisualParameter("MPRUS data set", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
    paramDataUS = paramDataList.active;

    this->visMPRUS->qfeGetVisualParameter("MPRUS data context", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramCompUSList);
    paramContext = paramCompList.active;  

    this->visMPRUS->qfeGetVisualParameter("MPRUS data focus", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramCompUSList);
    paramFocus = paramCompList.active;  

    this->visMPRUS->qfeGetVisualParameter("MPRUS speed thr. (cm/s)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramThreshold);
  }
  if(paramRepr == 2)
  {
    paramData = paramDataUS; 
  }
  else  
    paramContext = paramComp;    

  // Process parameters
  this->qfeGetDataSet((qfeDataTypeIndex)dataTypePCAP, visual, current, &volumeBounding, paramRange2[0], paramRange2[1]); // PCA-P
  this->qfeGetDataSet((qfeDataTypeIndex)paramData,    visual, current, &volumeTexture,  paramRange[0], paramRange[1]); // Selected volume  
 
  // Determine line color
  color = this->planeColorDefault;
  tt = 0.0;

  if(this->algorithmProbe == NULL) return qfeError;

  // Update algorithm parameters
  this->algorithmMPR->qfeSetPlaneBorderWidth(3.5*supersampling);
  this->algorithmMPR->qfeSetPlaneDataComponent(paramContext);
  this->algorithmMPR->qfeSetPlaneDataComponent2(paramFocus);
  this->algorithmMPR->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmMPR->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  this->algorithmMPR->qfeSetPlaneTriggerTime(tt);
  this->algorithmMPR->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  this->algorithmMPR->qfeSetPlaneDataRange2(paramRange2[0], paramRange2[1]);
  this->algorithmMPR->qfeSetPlaneBorderColor(color);
  this->algorithmMPR->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmMPR->qfeSetPlaneContrast(paramContrast);
  this->algorithmMPR->qfeSetPlaneBalance(paramBalance);  
  this->algorithmMPR->qfeSetPlaneOpacity(1.0);
  this->algorithmMPR->qfeSetPlaneSpeedThreshold(paramThreshold);
  
  int probeCount = (int)study->probe3D.size();

  if (this->probeActionState == probeClick)
    probeCount = 0;

  // For each probe, render a probe aligned plane
  for(int i=0; i<probeCount; i++)
  {
    qfeFloat radius;
    qfePoint originPlane;
    qfePoint originBase;
    qfeFloat offset;

    // Set the plane origin and linearly interpolate the radius in the probe
    offset      = study->probe3D[i].orthoPlaneOffset*study->probe3D[i].length;    

    originBase  = study->probe3D[i].origin - 0.5*study->probe3D[i].length*study->probe3D[i].axes[2];
    originPlane = originBase + offset*study->probe3D[i].axes[2];

    offset      = offset / study->probe3D[i].length;
    radius      = (offset * study->probe3D[i].topRadius) + ((1.0f-offset) * study->probe3D[i].baseRadius);
    radius      = radius * (paramScale/100.0f);

    // Update the plane position and orientation
    study->probe3D[i].orthoPlane.qfeSetFrameOrigin(originPlane.x, originPlane.y, originPlane.z);
    study->probe3D[i].orthoPlane.qfeSetFrameAxes(study->probe3D[i].axes[0].x, study->probe3D[i].axes[0].y, study->probe3D[i].axes[0].z,
                                                   study->probe3D[i].axes[1].x, study->probe3D[i].axes[1].y, study->probe3D[i].axes[1].z,
                                                   study->probe3D[i].axes[2].x, study->probe3D[i].axes[2].y, study->probe3D[i].axes[2].z);
    study->probe3D[i].orthoPlane.qfeSetFrameExtent(2.0*radius,2.0*radius);

    // Render the planes
    if(paramShowPlane)
    {   
      // Render the orthoplane            
      this->qfeEnableBuffer(this->intersectTexColor[0], this->intersectTexDepth[0]);
      this->algorithmMPR->qfeRenderCircle3D(volumeBounding, volumeTexture, &study->probe3D[i].orthoPlane, colormap);
      this->qfeDisableBuffer(0);

      this->algorithmMPR->qfeRenderCircle3D(volumeBounding, volumeTexture, &study->probe3D[i].orthoPlane, colormap);      
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualMPRUS(qfeVisualReformat *visual, qfePlaneProbe type, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *volumeBounding;
  qfeVolume          *volumeTexture;
  qfeColorMap        *colormap;
  qfePoint            origin;
  qfePoint            extent;
  unsigned int        dims[3];

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  int                 paramData;
  int                 paramContext;
  int                 paramFocus;
  int                 paramContextRepr;
  int                 paramFocusRepr;
  int                 paramInterp;
  int                 paramScale;
  int                 paramBalance;
  qfeFloat            paramRange[2];
  qfeFloat            paramRange2[2];
  bool                paramShowPlane;
  bool                paramShowContour;
  bool                paramProbeVisible;
  double              paramBrightness;
  double              paramContrast;
  bool                paramFlip;
  int                 paramSpeedThreshold;
  int                 paramViewAngle;
  int                 current;
  qfeColorRGB         color;
  qfeFloat            tt;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  // Get visual parameters
  visual->qfeGetVisualParameter("MPRUS visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowPlane);

  visual->qfeGetVisualParameter("MPRUS contour", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowContour);

  visual->qfeGetVisualParameter("MPRUS data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("MPRUS data context", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);
  paramContext = paramCompList.active;

  visual->qfeGetVisualParameter("MPRUS data focus", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);
  paramFocus = paramCompList.active;

  visual->qfeGetVisualParameter("MPRUS data context repr.", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);
  paramContextRepr = paramReprList.active;

  visual->qfeGetVisualParameter("MPRUS data focus repr.", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);
  paramFocusRepr = paramReprList.active;

  visual->qfeGetVisualParameter("MPRUS data interpolation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInterpList);
  paramInterp = paramInterpList.active;

  visual->qfeGetVisualParameter("MPRUS speed thr. (cm/s)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSpeedThreshold);  

  visual->qfeGetVisualParameter("MPRUS horizontal flip", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramFlip);

  visual->qfeGetVisualParameter("MPRUS scale (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramScale);

  visual->qfeGetVisualParameter("MPRUS mix (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBalance);

  visual->qfeGetVisualParameter("MPRUS brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);

  visual->qfeGetVisualParameter("MPRUS contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);

  if(this->visProbe3D != NULL)
  {
    this->visProbe3D->qfeGetVisualParameter("Probe visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramProbeVisible);
  }
  else paramProbeVisible = false;

  // Process parameters
  this->qfeGetDataSet((qfeDataTypeIndex)dataTypePCAP, visual, current, &volumeBounding, paramRange2[0], paramRange2[1]); // PCA-P
  this->qfeGetDataSet((qfeDataTypeIndex)paramData,    visual, current, &volumeTexture,  paramRange[0], paramRange[1]); // Selected volume

  if(volumeBounding == NULL)
    volumeBounding = volumeTexture;

  if(type == qfePlaneProbeParallel)
    paramViewAngle = this->planeViewAngle;
  else
    paramViewAngle = 0;

  this->planeViewFlip = paramFlip;

  // Determine the origin of the active volume and size of clipping planes
  qfeFloat depth;
  qfeGrid *grid;

  volumeBounding->qfeGetVolumeGrid(&grid);
  volumeBounding->qfeGetVolumeSize(dims[0], dims[1], dims[2]);
  grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  grid->qfeGetGridExtent(extent.x, extent.y, extent.z);
  
  depth = sqrt(pow(dims[0]*extent.x, 2.0f) + pow(dims[1]*extent.y, 2.0f) + pow(dims[2]*extent.y, 2.0f));

  // Determine line color
  color = this->planeColorDefault;
  tt = 0.0;

  if(this->algorithmProbe == NULL) return qfeError;

  // Update algorithm parameters
  this->algorithmMPR->qfeSetPlaneBorderWidth(3.5*supersampling);
  this->algorithmMPR->qfeSetPlaneDataComponent(paramContext);
  this->algorithmMPR->qfeSetPlaneDataComponent2(paramFocus);
  this->algorithmMPR->qfeSetPlaneDataRepresentation(paramContextRepr);
  this->algorithmMPR->qfeSetPlaneDataRepresentation2(paramFocusRepr);
  this->algorithmMPR->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  this->algorithmMPR->qfeSetPlaneTriggerTime(tt);
  this->algorithmMPR->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  this->algorithmMPR->qfeSetPlaneDataRange2(paramRange2[0], paramRange2[1]);
  this->algorithmMPR->qfeSetPlaneBorderColor(color);
  this->algorithmMPR->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmMPR->qfeSetPlaneContrast(paramContrast);
  this->algorithmMPR->qfeSetPlaneScale(paramScale);
  this->algorithmMPR->qfeSetPlaneBalance(paramBalance);
  this->algorithmMPR->qfeSetPlaneContour(paramShowContour);
  this->algorithmMPR->qfeSetPlaneHorizontalFlip(paramFlip);
  this->algorithmMPR->qfeSetPlaneSpeedThreshold(paramSpeedThreshold); 
  this->algorithmMPR->qfeSetPlaneViewAngle(paramViewAngle);
  this->algorithmMPR->qfeSetPlaneOpacity(1.0);

  int probeCount = (int)study->probe3D.size();

  if (this->probeActionState == probeClick)
    probeCount = 0;

  // For each probe, render a probe aligned plane
  for(int i=0; i<probeCount; i++)
  {   
    qfeFrameSlice *currentParallel = new qfeFrameSlice();

    // Update the plane position and orientation
    study->probe3D[i].parallelPlane.qfeSetFrameExtent(2.0*max(study->probe3D[i].baseRadius, study->probe3D[i].topRadius), study->probe3D[i].length);
    study->probe3D[i].parallelPlane.qfeSetFrameOrigin(study->probe3D[i].origin.x, study->probe3D[i].origin.y, study->probe3D[i].origin.z);
 
    currentParallel->qfeSetFrameExtent(2.0*max(study->probe3D[i].baseRadius, study->probe3D[i].topRadius), study->probe3D[i].length);
    currentParallel->qfeSetFrameOrigin(study->probe3D[i].origin.x, study->probe3D[i].origin.y, study->probe3D[i].origin.z);
    currentParallel->qfeSetFrameAxes(study->probe3D[i].axes[1].x, study->probe3D[i].axes[1].y, study->probe3D[i].axes[1].z,
                                    study->probe3D[i].axes[2].x, study->probe3D[i].axes[2].y, study->probe3D[i].axes[2].z,
                                    study->probe3D[i].axes[0].x, study->probe3D[i].axes[0].y, study->probe3D[i].axes[0].z);
    
    // Render the planes
    if(study->probe3D[i].selected && paramShowPlane)
    {       
      if(type == qfePlaneProbeParallel)
      {        
        this->algorithmMPR->qfeRenderPolygonUS2D(volumeBounding, volumeTexture, currentParallel, study->probe3D[i].baseRadius, study->probe3D[i].topRadius, colormap);      
        if(this->planeOverlayVisible)
          this->algorithmMPR->qfeRenderOverlayUS2D(volumeBounding, currentParallel, study->probe3D[i].baseRadius, study->probe3D[i].topRadius, float(this->planeViewAngle));
      }
      else
        this->algorithmMPR->qfeRenderCircleUS2D(volumeBounding, volumeTexture, &study->probe3D[i].orthoPlane, colormap);      
    }

    delete currentParallel;
  }
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualPlane(qfeVisualReformat *visual,  int supersampling)
{  
  qfeStudy           *study       = NULL;
  qfeVolume          *volumeSlice = NULL;
  qfeVolume          *volumeFlow  = NULL;
  qfeGrid            *grid        = NULL;
  qfeColorMap        *colormap;
  qfePoint            origin;

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList1;
  qfeSelectionList    paramDataList2;  
  qfeSelectionList    paramDataVector;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  bool                paramVisible;
  bool                paramBorderVisible;
  int                 paramData1;
  int                 paramData2;
  int                 paramRepr;
  int                 paramComponent;
  qfeFloat            paramRange1[2];
  qfeFloat            paramRange2[2];
  int                 paramTransparency;
  int                 paramBalance;
  double              paramBrightness;
  double              paramContrast;
  int                 current;
  qfeColorRGB         colorQ;
  qfeFloat            tt;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  // Get visual parameters
  visual->qfeGetVisualParameter("Planes visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("Plane data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList1);  
  paramData1 = paramDataList1.active;

  visual->qfeGetVisualParameter("Plane border visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBorderVisible);  

  visual->qfeGetVisualParameter("Plane transparency", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTransparency);  
  
  visual->qfeGetVisualParameter("4D flow data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList2);  
  paramData2 = paramDataList2.active;  

  visual->qfeGetVisualParameter("4D flow visibility (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBalance);  

  visual->qfeGetVisualParameter("4D flow vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataVector);  
  paramComponent = paramDataVector.active;

  visual->qfeGetVisualParameter("Data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("Brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);  

  visual->qfeGetVisualParameter("Contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);  

  // Process parameters
  // If there is data for the ortho reader, synchronize the trigger time.
  origin.x = origin.y = origin.z = 0.0;
  tt  = 0.0;
  if(this->visOrtho != NULL)
  {    
    this->qfeGetDataSet((qfeDataTypeIndex)0, this->visOrtho, current, &volumeSlice, paramRange1[0], paramRange1[1]);

    if(volumeSlice != NULL)
    {
      volumeSlice->qfeGetVolumeGrid(&grid);
      volumeSlice->qfeGetVolumeTriggerTime(tt);
      grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);    
    }
  }

  // Select the right data range
  switch(paramData1) // QFlow data
  {
  case 0 : study->qfeGetStudyRangePCAP(paramRange1[0], paramRange1[1]);
           break;  
  case 1 : study->qfeGetStudyRangePCAM(paramRange1[0], paramRange1[1]);
           break;
  case 2 : study->qfeGetStudyRangeFFE(paramRange1[0], paramRange1[1]);
           // Non-float texture are clamped [0,1]
           //paramRange1[0] = paramRange1[0] / pow(2.0f,16.0f);
           //paramRange1[1] = paramRange1[1] / pow(2.0f,16.0f);
           break;
  case 3 : study->qfeGetStudyRangeSSFP(paramRange1[0], paramRange1[1]);
           // Non-float texture are clamped [0,1]
           //paramRange1[0] = paramRange1[0] / pow(2.0f,16.0f);
           //paramRange1[1] = paramRange1[1] / pow(2.0f,16.0f);
           break;
  }

  switch(paramData2) // 4D flow data (fix different combobox orders)
  {
  case 0 : paramData2 = 0; break;
  case 1 : paramData2 = 1; break;          
  case 2 : paramData2 = 4; break;
  case 3 : paramData2 = 2; break;           
  }
  
  // Determine line color
  colorQ.r = 0.55;
  colorQ.g = 0.55;
  colorQ.b = 0.55;

  // Update algorithm parameters  
  this->algorithmPlane->qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
  this->algorithmPlane->qfeSetPlaneDataComponent(paramComponent);
  this->algorithmPlane->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmPlane->qfeSetPlaneTriggerTime(tt);  
  this->algorithmPlane->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmPlane->qfeSetPlaneContrast(paramContrast);
  this->algorithmPlane->qfeSetPlaneBalance(paramBalance);
  this->algorithmPlane->qfeSetPlaneOpacity((100-paramTransparency)/100.0f);
  this->algorithmPlane->qfeSetPlaneContour(paramBorderVisible);
 
  if(paramVisible)
  {
    // render the planes
    for(int i=0; i<(int)study->ssfp2D.size();i++)
    {
      this->qfeGetDataPlane(paramData1, i, visual, &volumeSlice);  
      this->qfeGetDataSet((qfeDataTypeIndex)paramData2, visual, current, &volumeFlow, paramRange2[0], paramRange2[1]);

      this->qfeGetSliceOblique(volumeSlice, &this->planeQ);   
      
      this->algorithmPlane->qfeSetPlaneDataRange(paramRange1[0], paramRange1[1]);
      this->algorithmPlane->qfeSetPlaneDataRange2(paramRange2[0], paramRange2[1]);
      this->algorithmPlane->qfeSetPlaneBorderColor(colorQ);
      this->algorithmPlane->qfeRenderPlane3D(volumeSlice, volumeFlow, study->ssfp2D_phases, this->planeQ, colormap);  
    }
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualPlanesArrows(qfeVisualReformat *visual, qfePlaneProbe type, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *volume;
  qfeColorMap        *colormap;
  qfePoint            origin;

  qfeVisualParameter *param;  
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  int                 paramComp;
  int                 paramRepr;
  double              paramSpacing;
  double              paramScaling;
  int                 paramSpeedThreshold;
  bool                paramVisible, paramShowPlane;
  int                 paramTransparency;
  qfeFloat            paramDataRange[2];
  int                 current;
  qfeFrameSlice       currentParallel;
  qfeMatrix4f         R, T, M;
  GLfloat             m[16];
  

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  // Get the data set
  if(study != NULL)
  {
    /*if((int)study->pcap.size() > current)
      volume = study->pcap[current];
    else 
    if((int)study->pcap.size() > 0)
      volume = study->pcap.front();      
    else 
      return qfeError;*/
    this->qfeGetDataSet((qfeDataTypeIndex)dataTypePCAP, visual, current, &volume, paramDataRange[0], paramDataRange[1]);
  }
  else return qfeError;
  
  // Get visual parameters
  visual->qfeGetVisualParameter("Arrows visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);    

  visual->qfeGetVisualParameter("Arrows data vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("Arrows data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active; 

  visual->qfeGetVisualParameter("Arrows spacing (mm)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSpacing); 

  visual->qfeGetVisualParameter("Arrows scaling factor", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramScaling); 

  paramTransparency = 50;

  // Process parameters
  if(this->visMPRUS != NULL)
  {
    this->visMPRUS->qfeGetVisualParameter("MPRUS visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowPlane);   

    this->visMPRUS->qfeGetVisualParameter("MPRUS speed thr. (cm/s)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramSpeedThreshold);         
  }

  // Set the arrow head properties  
  this->algorithmMPR->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmMPR->qfeSetPlaneDataRange(paramDataRange[0], paramDataRange[1]);
  this->algorithmMPR->qfeSetPlaneSpeedThreshold(paramSpeedThreshold);

  // Set up the plane
  if(paramVisible)
  { 
    for(int i=0; i<(int)study->probe3D.size(); i++)
    {
      if(study->probe3D[i].selected && paramShowPlane)
      {
        qfePoint  o;
        qfeVector x, y, z;
        
        qfeMatrix4f::qfeSetMatrixIdentity(T);
        qfeMatrix4f::qfeSetMatrixIdentity(R);
        
        if(type == qfePlaneProbeParallel)
        {
          o = study->probe3D[i].origin;
          x = study->probe3D[i].axes[0];
          y = study->probe3D[i].axes[1];
          z = study->probe3D[i].axes[2];

          currentParallel.qfeSetFrameExtent(2.0*max(study->probe3D[i].baseRadius, study->probe3D[i].topRadius), study->probe3D[i].length);
          currentParallel.qfeSetFrameOrigin(o.x, o.y, o.z);
          currentParallel.qfeSetFrameAxes(y.x,y.y,y.z, z.x,z.y,z.z, x.x,x.y,x.z);

          T(3,0) = o.x; T(3,1) = o.y; T(3,2) = o.z;

          R(0,0) = y.x; R(1,0) = z.x; R(2,0) = x.x;
          R(0,1) = y.y; R(1,1) = z.y; R(1,2) = z.z;
          R(0,2) = y.z; R(2,1) = x.y; R(2,2) = x.z;
        }
        else
        {
          qfeVector t;
          
          study->probe3D[i].orthoPlane.qfeGetFrameOrigin(o.x, o.y, o.z);
          study->probe3D[i].orthoPlane.qfeGetFrameAxes(x.x,x.y,x.z, y.x,y.y,y.z, z.x,z.y,z.z);          

          t = y;
          y = -1.0*x;
          x = t;

          T(3,0) = o.x; T(3,1) = o.y; T(3,2) = o.z;

          R(0,0) = x.x; R(1,0) = y.x; R(2,0) = z.x;
          R(0,1) = x.y; R(1,1) = y.y; R(1,2) = y.z;
          R(0,2) = x.z; R(2,1) = z.y; R(2,2) = z.z;
        }

        qfeMatrix4f::qfeGetMatrixInverse(R*T, M);
        qfeMatrix4f::qfeGetMatrixElements(M, m);

        // First render the MPR-US plane to the stencil buffer to create a mask
        glEnable(GL_STENCIL_TEST);      

        glClear(GL_STENCIL_BUFFER_BIT);           
        glStencilFunc(GL_ALWAYS, 1, 0xFFFFFFFF);
        glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE); 
        glDepthMask(GL_FALSE);
        glColorMask(GL_FALSE,GL_FALSE,GL_FALSE,GL_FALSE); 

        if(type == qfePlaneProbeParallel)
          this->algorithmMPR->qfeRenderPolygonUS2D(volume, volume, &currentParallel, study->probe3D[i].baseRadius, study->probe3D[i].topRadius, NULL);      
        else
          this->algorithmMPR->qfeRenderCircleUS2D(volume, volume, &study->probe3D[i].orthoPlane, NULL);      

        glColorMask(GL_TRUE,GL_TRUE,GL_TRUE,GL_TRUE);      
        glDepthMask(GL_TRUE);           

        // Render the arrow head where the stencil mask allows it
        glStencilFunc(GL_EQUAL, 1, 0xFFFFFFFF);
        glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
        
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();

        glMultMatrixf(m);        
        
        if(type == qfePlaneProbeParallel)
          this->algorithmMPR->qfeRenderArrowHeads3D(volume, &currentParallel, paramSpacing, paramScaling, colormap, paramTransparency);  
        else
        {
          this->algorithmMPR->qfeSetPlaneHorizontalFlip(false);
          this->algorithmMPR->qfeRenderArrowHeads3D(volume, &study->probe3D[i].orthoPlane, paramSpacing, paramScaling, colormap, paramTransparency);  
        }

        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();

        glDisable(GL_STENCIL_TEST);
              
      }
    }
  } 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualParticleTrace(qfeVisualFlowParticleTrace *visual, int supersampling)
{
  qfeStudy       *study;
  qfeLightSource *light;
  qfeColorMap    *colormap;
  qfeFloat        current;  

  qfeSeeds     seeds;
  int          seedsCount;

  qfeVisualParameter *param;
  qfeSelectionList    paramInterpolationList;
  qfeSelectionList    paramIntegrationList;
  qfeSelectionList    paramParticleStyle;
  qfeSelectionList    paramParticleShading;
  qfeSelectionList    paramParticleTrail;
  qfeSelectionList    paramParticleColor;
  bool                paramParticleContour;
  int                 paramCount;
  int                 paramSize;
  int                 paramSpeed;
  int                 paramLifeTime;
  int                 paramColorScale;
  int                 paramIntegrationType;
  int                 paramIntegrationSteps;
  bool                paramVisible;
  double              paramTrailLifeTime;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);  
  this->scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("Particles visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);

  visual->qfeGetVisualParameter("Particles style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramParticleStyle);

  visual->qfeGetVisualParameter("Particles shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramParticleShading);

  visual->qfeGetVisualParameter("Particles color", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramParticleColor);

  visual->qfeGetVisualParameter("Particles trail", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramParticleTrail);

  visual->qfeGetVisualParameter("Particles contour", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramParticleContour);

  visual->qfeGetVisualParameter("Particles count", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCount);

  visual->qfeGetVisualParameter("Particles size (mm)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSize);

  visual->qfeGetVisualParameter("Particles lifetime", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLifeTime);

  visual->qfeGetVisualParameter("Particles color scale (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramColorScale);

  visual->qfeGetVisualParameter("Particles speed (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSpeed);

  visual->qfeGetVisualParameter("Particles integration", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramIntegrationList);
  paramIntegrationType = paramIntegrationList.active;

  visual->qfeGetVisualParameter("Particles steps/sample", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramIntegrationSteps);

  visual->qfeGetVisualParameter("Particles trail lifetime", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTrailLifeTime);

  seedsCount = paramCount;

  if(paramVisible != true) return qfeError;

  // Set particle trace parameters
  qfeFloat phaseDuration, traceDuration;
  int      traceDirection;
  study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);

  this->algorithmParticleTrace->qfeSetTraceSteps(paramIntegrationSteps);
  this->algorithmParticleTrace->qfeSetTraceScheme((qfeFlowParticleTrace::qfeFlowParticlesIntegrationScheme)paramIntegrationType);
  this->algorithmParticleTrace->qfeSetTraceSpeed(paramSpeed);
  this->algorithmParticleTrace->qfeSetTraceLifeTime(paramLifeTime);
  this->algorithmParticleTrace->qfeSetPhaseDuration(phaseDuration);
  this->algorithmParticleTrace->qfeSetParticleStyle((qfeFlowParticleTrace::qfeFlowParticlesStyle)paramParticleStyle.active);
  this->algorithmParticleTrace->qfeSetParticleColor((qfeFlowParticleTrace::qfeFlowParticlesColor)paramParticleColor.active);
  this->algorithmParticleTrace->qfeSetParticleShading((qfeFlowParticleTrace::qfeFlowParticlesShading)paramParticleShading.active);
  this->algorithmParticleTrace->qfeSetParticleContour(paramParticleContour);
  this->algorithmParticleTrace->qfeSetParticleSize(paramSize);
  this->algorithmParticleTrace->qfeSetLightSource(light);
  this->algorithmParticleTrace->qfeSetColorScale(paramColorScale);
  this->algorithmParticleTrace->qfeSetTrailStyle((qfeFlowParticleTrace::qfeFlowParticlesTrailType)paramParticleTrail.active);
  this->algorithmParticleTrace->qfeSetTrailLifeTime(paramTrailLifeTime);

  // Update the probe seeding
  this->qfeUpdateProbeSeeds();

  // Evolve and advect the particles if the time has changed
  if(current != this->prevTime)
  {
    // Advect the particles
    traceDirection =  1;
    if((current < this->prevTime) && (abs(current - this->prevTime) < 1.0))
      traceDirection = -1;
    if((current > this->prevTime) && (abs(current - this->prevTime) > 1.0))
      traceDirection = -1;

    traceDuration = phaseDuration * abs(current - this->prevTime);
    if(traceDirection ==  1 && current == 0.0)
    {
      traceDuration = phaseDuration * abs((int)study->pcap.size() - this->prevTime);
    }
    if(traceDirection == -1 && this->prevTime == 0.0)
    {
      traceDuration = phaseDuration * abs(current - (int)study->pcap.size());
    }

    this->algorithmParticleTrace->qfeSetTraceDirection(traceDirection);

    this->algorithmParticleTrace->qfeAdvectParticles(study->pcap, current, traceDuration);

    // Evolve the particles
    this->algorithmParticleTrace->qfeEvolveParticles();

    // Update global time flag
    this->prevTime = current;
  }

  // Attach the alternative color and depth textures
  // Revert FBO to original setting for further rendering
  this->qfeEnableBuffer(this->intersectTexColor[0], this->intersectTexDepth[0]);
  this->algorithmParticleTrace->qfeRenderParticles(colormap);
  this->qfeDisableBuffer(0);

  // Render particles
  this->algorithmParticleTrace->qfeRenderParticles(colormap);


  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualLineTrace(qfeVisualFlowLineTrace *visual, int supersampling)
{
  qfeStudy       *study;
  qfeColorMap    *colormap;
  qfeLightSource *light;
  qfeFloat        current;
  qfeDisplay     *display;

  int             seedsCount;

  qfeVisualParameter *param;
  qfeSelectionList    paramInterpolationList;
  qfeSelectionList    paramIntegrationList;
  qfeSelectionList    paramStyleList;
  qfeSelectionList    paramShadingList;
  qfeSelectionList    paramSeedingBehavior;
  qfeSelectionList    paramSeedingType;
  int                 paramCount;
  //int                 paramInterpolationType;
  int                 paramIntegrationType;
  int                 paramIntegrationSteps;
  bool                paramVisible;
  int                 paramPhasesDynamic;
  int                 paramPhasesStatic;
  double              paramLineWidth;
  int                 paramColorScale;
  bool                paramContour;
  bool                paramHalo;
  int                 paramMaxAngle;
  
  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  this->scene->qfeGetSceneDisplay(&display);
  this->scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("Lines visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("Lines count", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCount);  
  
  visual->qfeGetVisualParameter("Lines phases dynamic", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramPhasesDynamic);  

  visual->qfeGetVisualParameter("Lines phases static", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramPhasesStatic);   

  visual->qfeGetVisualParameter("Lines maximum angle", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramMaxAngle);   

  //visual->qfeGetVisualParameter("Lines interpolation", &param);
  //if(param != NULL) param->qfeGetVisualParameterValue(paramInterpolationList);
  //paramInterpolationType = paramInterpolationList.active;

  visual->qfeGetVisualParameter("Lines style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramStyleList);  

  visual->qfeGetVisualParameter("Lines shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShadingList);   

  visual->qfeGetVisualParameter("Lines width", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLineWidth);   

  visual->qfeGetVisualParameter("Lines contour", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContour);   

  visual->qfeGetVisualParameter("Lines halo", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramHalo);   

  visual->qfeGetVisualParameter("Lines color scale (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramColorScale);  

  visual->qfeGetVisualParameter("Lines integration", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramIntegrationList);  
  paramIntegrationType = paramIntegrationList.active;

  visual->qfeGetVisualParameter("Lines steps/sample", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramIntegrationSteps);  

  visual->qfeGetVisualParameter("Lines seeding behavior", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSeedingBehavior);  

  visual->qfeGetVisualParameter("Lines seeding type", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSeedingType); 
     
  seedsCount = paramCount;
  
  if(paramVisible != true) return qfeError;  
  
  // Set line trace parameters
  qfeFloat               phaseDuration;
  int                    traceDirection;
  qfeFlowSeedingBehavior seedingBehavior;
  
  study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);    
  traceDirection =  1;  

  switch(paramSeedingBehavior.active)
  {
  case 0  : seedingBehavior = qfeFlowLinesPathDynamic;
            break;
  case 1  : seedingBehavior = qfeFlowLinesPathStatic;
            break;
  default : seedingBehavior = qfeFlowLinesPathStatic;
  }    
 
  this->algorithmLineTrace->qfeSetStyle((qfeFlowLinesStyle)paramStyleList.active);
  this->algorithmLineTrace->qfeSetTraceSteps(paramIntegrationSteps);   
  this->algorithmLineTrace->qfeSetTraceScheme((qfeFlowLinesIntegrationScheme)paramIntegrationType);  
  this->algorithmLineTrace->qfeSetTraceDirection(traceDirection);
  this->algorithmLineTrace->qfeSetTracePhasesStatic(paramPhasesStatic);
  this->algorithmLineTrace->qfeSetTracePhasesDynamic(paramPhasesDynamic);  
  //this->algorithmLineTrace->qfeSetTraceMaximumAngle(paramMaxAngle);
  this->algorithmLineTrace->qfeSetPhase(current);
  this->algorithmLineTrace->qfeSetPhaseDuration(phaseDuration);
  this->algorithmLineTrace->qfeSetLineShading((qfeFlowLinesShading)paramShadingList.active);
  this->algorithmLineTrace->qfeSetLineWidth((float)paramLineWidth);
  this->algorithmLineTrace->qfeSetLineContour(paramContour);
  this->algorithmLineTrace->qfeSetLineHalo(paramHalo);
  this->algorithmLineTrace->qfeSetLightSource(light);
  this->algorithmLineTrace->qfeSetColorScale(paramColorScale);
  this->algorithmLineTrace->qfeSetSuperSampling(supersampling);
 
  // Update the probe seeding
  this->qfeUpdateProbeSeeds();

  // Attach the alternative color and depth textures
  // Revert FBO to original setting for further rendering
  this->qfeEnableBuffer(this->intersectTexColor[0], this->intersectTexDepth[0]);
  this->algorithmLineTrace->qfeRenderPathlines(NULL, seedingBehavior);  
  this->qfeDisableBuffer(0);

  // Render particles
  this->algorithmLineTrace->qfeRenderPathlines(colormap, seedingBehavior);    

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualDVR(qfeVisualRaycast *visual, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *textureVolume;
  qfeVolume          *boundingVolume;
  qfeColorMap        *colormap;
  qfeLightSource     *light;
  int                 current;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramQualList;
  qfeSelectionList    paramStyleList;
  bool                paramVisible;
  int                 paramData;
  int                 paramComp;
  int                 paramQuality;
  bool                paramGradient;
  bool                paramShading;
  bool                paramMultiRes;
  qfeFloat            paramRange[2];
  int                 paramMode;
  qfeRange            paramClipX, paramClipY, paramClipZ;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  this->scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("DVR visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);

  visual->qfeGetVisualParameter("DVR data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("DVR data component", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("DVR gradient", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramGradient);

  visual->qfeGetVisualParameter("DVR shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShading);

  visual->qfeGetVisualParameter("DVR shading style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramStyleList);

  visual->qfeGetVisualParameter("DVR shading quality", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramQualList);
  switch(paramQualList.active)
  {
  case 0  : paramQuality = 1; break;
  case 1  : paramQuality = 2; break;
  case 2  : paramQuality = 4; break;
  case 3  : paramQuality = 8; break;
  default : paramQuality = 2; break;
  }

  visual->qfeGetVisualParameter("DVR shading multires", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramMultiRes);

  visual->qfeGetVisualParameter("DVR clipping x", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipX);

  visual->qfeGetVisualParameter("DVR clipping y", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipY);

    visual->qfeGetVisualParameter("DVR clipping z", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipZ);

  if(paramVisible != true) return qfeError;

  // Get the data set and compute the data range
  this->qfeGetDataSet((qfeDataTypeIndex)paramData, visual, current, &textureVolume, paramRange[0], paramRange[1]);

  if(textureVolume == NULL) return qfeError;

  textureVolume->qfeGetVolumeType(paramMode);

  // if there is PCA-P data loaded, use this as bounding volume
  if((int)study->pcap.size() > 0)
    boundingVolume = study->pcap.front();
  else
    boundingVolume = textureVolume;

  // Multi-resolution dvr - check the global state
  if(paramMultiRes)
    paramQuality = std::min((float)paramQuality, this->dvrQuality);

  // Update algorithm parameters
  this->algorithmDVR->qfeSetIntersectionBuffers(this->intersectTexColor[0], this->intersectTexDepth[0]);
  this->algorithmDVR->qfeSetConvexClippingBuffers(this->clippingFrontTexDepth[0], this->clippingBackTexDepth[0]);
  this->algorithmDVR->qfeSetOrthoClippingRanges(paramClipX, paramClipY, paramClipZ);
  this->algorithmDVR->qfeSetDataComponent(paramComp);
  this->algorithmDVR->qfeSetDataRange(paramRange[0], paramRange[1]);
  this->algorithmDVR->qfeSetGradientRendering(paramGradient);
  this->algorithmDVR->qfeSetShading(paramShading);
  this->algorithmDVR->qfeSetLightSource(light);
  this->algorithmDVR->qfeSetQuality(paramQuality);
  this->algorithmDVR->qfeSetStyle(paramStyleList.active);

  this->algorithmDVR->qfeRenderRaycasting(this->frameBufferProps[0].texColor, this->frameBufferProps[0].texDepth, boundingVolume, textureVolume, colormap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualVFR(qfeVisualRaycastVector *visual, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *textureVolume;
  qfeVolume          *boundingVolume;
  qfeColorMap        *colormap;
  qfeLightSource     *light;
  int                 current;  

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramQualList;
  qfeSelectionList    paramStyleList;
  qfeSelectionList    paramDataFocus;
  qfeSelectionList    paramDataContext;
  bool                paramVisible;
  int                 paramData;
  int                 paramQuality;
  bool                paramMultiRes;
  qfeFloat            paramRange[2];
  int                 paramMode;
  int                 paramThreshold;
  qfeRange            paramClipX, paramClipY, paramClipZ;

  qfeMatrix4f         T;
  GLfloat             matrix[16];

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  this->scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("VFR visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);

  visual->qfeGetVisualParameter("VFR data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("VFR velocity thr. (cm/s)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramThreshold);

  visual->qfeGetVisualParameter("VFR data focus", &param);  
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataFocus);

  visual->qfeGetVisualParameter("VFR data context", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataContext);

/*
  visual->qfeGetVisualParameter("VFR shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShading);

  visual->qfeGetVisualParameter("VFR shading style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramStyleList);
*/
  visual->qfeGetVisualParameter("VFR quality", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramQualList);
  switch(paramQualList.active)
  {
  case 0  : paramQuality = 1; break;
  case 1  : paramQuality = 2; break;
  case 2  : paramQuality = 4; break;
  case 3  : paramQuality = 8; break;
  default : paramQuality = 2; break;
  }

  visual->qfeGetVisualParameter("VFR multires", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramMultiRes);

  visual->qfeGetVisualParameter("VFR clipping x", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipX);

  visual->qfeGetVisualParameter("VFR clipping y", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipY);

  visual->qfeGetVisualParameter("VFR clipping z", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipZ);

  if(paramVisible != true) return qfeError;

  // Get the data set and compute the data range
  this->qfeGetDataSet((qfeDataTypeIndex)paramData, visual, current, &textureVolume, paramRange[0], paramRange[1]);

  if(textureVolume == NULL) return qfeError;

  textureVolume->qfeGetVolumeType(paramMode);

  // if there is PCA-P data loaded, use this as bounding volume
  if((int)study->pcap.size() > current)
    boundingVolume = study->pcap[current];
  else
    boundingVolume = textureVolume;

  // Multi-resolution dvr - check the global state
  if(paramMultiRes)
    paramQuality = std::min((float)paramQuality, this->dvrQuality);

  // Update algorithm parameters
  this->algorithmVFR->qfeSetIntersectionBuffers(this->intersectTexColor[1], this->intersectTexDepth[1]);
  this->algorithmVFR->qfeSetConvexClippingBuffers(this->clippingFrontTexDepth[1], this->clippingBackTexDepth[1]);
  this->algorithmVFR->qfeSetOrthoClippingRanges(paramClipX, paramClipY, paramClipZ);
  this->algorithmVFR->qfeSetDataRange(paramRange[0], paramRange[1]);
  this->algorithmVFR->qfeSetLightSource(light);
  this->algorithmVFR->qfeSetQuality(paramQuality);
  this->algorithmVFR->qfeSetStyle(paramStyleList.active);
  this->algorithmVFR->qfeSetSpeedThreshold(paramThreshold);
  this->algorithmVFR->qfeSetDataRepresentationFocus((qfeRayCastingVector::qfeRaycastingVectorFocus)paramDataFocus.active);
  this->algorithmVFR->qfeSetDataRepresentationContext((qfeRayCastingVector::qfeRaycastingVectorContext)paramDataContext.active);

  int probeCount = (int)study->probe3D.size();

  if (this->probeActionState == probeClick)
    probeCount = 0;

  // For each probe, render a probe aligned plane
  for(int i=0; i<probeCount; i++)
  {    
    // Render the cylinder
    if(study->probe3D[i].selected)
    {     
      qfeFloat angle = this->probeVolumePitch * (float(PI)/180.0f);
      qfeMatrix4f::qfeSetMatrixIdentity(T);
      T(1,1) = cos(angle);
      T(1,2) = sin(angle) * -1.0f;
      T(2,1) = sin(angle);
      T(2,2) = cos(angle);

      qfeMatrix4f::qfeGetMatrixElements(T, &matrix[0]);

      glMatrixMode(GL_MODELVIEW);
      glPushMatrix();

      glMultMatrixf(&matrix[0]);

      this->algorithmVFR->qfeRenderRaycasting(this->frameBufferProps[3].texColor, this->frameBufferProps[3].texDepth, boundingVolume, textureVolume, &study->probe3D[i], colormap); 
      
      glMatrixMode(GL_MODELVIEW);
      glPopMatrix();      
    }
  }

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualMIP(qfeVisualMaximum *visual, int supersampling)
{
  qfeStudy    *study;
  qfeVolume   *volume;
  qfeColorMap *colormap;
  int          current;  

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  bool                paramVisible;
  bool                paramGradient;
  bool                paramTransparency;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  double              paramC;
  double              paramB;
  qfeFloat            paramRange[2];
  int                 paramMode;

  qfeMatrix4f         R, R1, R2, T, C1, C2, M;
  GLfloat             matrix[16];
  qfeFloat            arcRadius, theta;
  qfeVector           x, y, z;
  qfePoint            o;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);  

  // Fetch the parameters
  visual->qfeGetVisualParameter("MIP visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);

  visual->qfeGetVisualParameter("MIP data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("MIP data component", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("MIP data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);
  paramRepr = paramReprList.active;
  
  visual->qfeGetVisualParameter("MIP gradient", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramGradient);    

  visual->qfeGetVisualParameter("MIP transparency", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTransparency);    

  visual->qfeGetVisualParameter("MIP brightness", &param);
  param->qfeGetVisualParameterValue(paramB);

  visual->qfeGetVisualParameter("MIP contrast",   &param);
  param->qfeGetVisualParameterValue(paramC);

  if(paramVisible != true) return qfeError;

  // Get the data set and compute the data range
  this->qfeGetDataSet((qfeDataTypeIndex)paramData, visual, current, &volume, paramRange[0], paramRange[1]);

  if(volume == NULL) return qfeError;

  volume->qfeGetVolumeType(paramMode);

  // Update algorithm parameters
  this->algorithmMIP->qfeSetGeometryBuffers(this->intersectTexColor[1], this->intersectTexDepth[1]);
  this->algorithmMIP->qfeSetDataComponent(paramComp);
  this->algorithmMIP->qfeSetDataRepresentation(paramRepr);
  this->algorithmMIP->qfeSetDataRange(paramRange[0], paramRange[1]);
  this->algorithmMIP->qfeSetGradient(paramGradient);
  this->algorithmMIP->qfeSetTransparency(paramTransparency);
  this->algorithmMIP->qfeSetContrast(paramC);
  this->algorithmMIP->qfeSetBrightness(paramB);

  int probeCount = (int)study->probe3D.size();

  // Transform into the space of the VFR probe
  qfeMatrix4f::qfeSetMatrixIdentity(R1); 
  theta   = (float(PI) / 2.0f);
  R1(0,0) =  cos(theta); 
  R1(0,1) =  sin(theta)*-1.0f;  
  R1(1,0) =  sin(theta); 
  R1(1,1) =  cos(theta);    

  qfeMatrix4f::qfeSetMatrixIdentity(R2);
  theta   = (float(PI) / 2.0f);
  R2(1,1) =  cos(theta);
  R2(1,2) =  sin(theta)*-1.0f;  
  R2(2,1) =  sin(theta);
  R2(2,2) =  cos(theta);

  // Take the local pitch into account
  qfeFloat angle = this->probeVolumePitch * (float(PI)/180.0f);
  qfeMatrix4f::qfeSetMatrixIdentity(C2);
  C2(1,1) = cos(angle);
  C2(1,2) = sin(angle) * -1.0f;
  C2(2,1) = sin(angle);
  C2(2,2) = cos(angle);

  glEnable(GL_BLEND);

  // For each probe, render a probe aligned plane
  for(int i=0; i<probeCount; i++)
  {    
    // Render the cylinder
    if(study->probe3D[i].selected)
    { 
      x = study->probe3D[i].axes[0];
      y = study->probe3D[i].axes[1];
      z = study->probe3D[i].axes[2];
      o = study->probe3D[i].origin;

      // Small correction for VFR cylinder
      qfeMatrix4f::qfeSetMatrixIdentity(C1);  
      arcRadius = sqrt(pow(0.5f*study->probe3D[i].length, 2.0f) + pow(study->probe3D[i].topRadius, 2.0f));
      C1(3,2)   = 0.25f*study->probe3D[i].length - 0.5f*arcRadius;

      // Transform back to the basic cylinder coordinates
      qfeMatrix4f::qfeSetMatrixIdentity(R);
      R(0,0) = x.x;  R(1,0) = y.x;  R(2,0) = z.x;
      R(0,1) = x.y;  R(1,1) = y.y;  R(2,1) = z.y;
      R(0,2) = x.z;  R(1,2) = y.z;  R(2,2) = z.z;    

      qfeMatrix4f::qfeSetMatrixIdentity(T);
      T(3,0) = o.x;
      T(3,1) = o.y;
      T(3,2) = o.z;

      qfeMatrix4f::qfeSetMatrixIdentity(M);
      M = R*T;
      qfeMatrix4f::qfeGetMatrixInverse(M,M);
  
      qfeMatrix4f::qfeGetMatrixElements(M*C1*R1*R2*C2, &matrix[0]);

      glMatrixMode(GL_MODELVIEW);
      glPushMatrix();

      glMultMatrixf(&matrix[0]);

      this->algorithmMIP->qfeRenderMaximumIntensityProjection(this->frameBufferProps[3].texColor, volume, colormap);
      
      glMatrixMode(GL_MODELVIEW);
      glPopMatrix();      
    }
  }
    
  glDisable(GL_BLEND);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeRenderVisualAides(qfeVisualAides *visual, int supersampling)
{
  qfeStudy           *study;
  qfeVolume          *volume;
  qfeGrid            *grid;
  int                 current;
  qfeVector           ax, ay, az;

  qfeVisualParameter *param;
  bool                paramSupportBoundingBox       = true;
  bool                paramSupportOrientationMarker = true;
  bool                paramSupportAxes              = false;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  
  if(study == NULL) return qfeError;

  volume = study->pcap[current];
  volume->qfeGetVolumeGrid(&grid);

  grid->qfeGetGridAxes(ax.x, ax.y, ax.z, ay.x, ay.y, ay.z, az.x, az.y, az.z);

  visual->qfeGetVisualParameter("Support bounding box", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportBoundingBox);

  visual->qfeGetVisualParameter("Support orientation marker", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportOrientationMarker);

  visual->qfeGetVisualParameter("Support axes", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportAxes);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  if(paramSupportBoundingBox == true)       qfeDriverAide::qfeRenderBoundingBoxDashed(volume, this->currentModelView, 0.8*supersampling);  
  if(paramSupportAxes        == true)       qfeDriverAide::qfeRenderVolumeAxes(volume, 10.0, 1.6*supersampling);

  if(paramSupportOrientationMarker == true) this->engine->OrientationMarkerEnable(ax.x, ax.y, ax.z, ay.x, ay.y, ay.z, az.x, az.y, az.z);
  else                                      this->engine->OrientationMarkerDisable();

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND);

 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeWorkflowProbeThreeClick(qfePoint *p)
{
  switch(this->probeClickState)
  {
    case probeSelectStart :
      //cout << "start" << endl;
      if(this->probeActionState != probeDefault) return qfeError;

      this->probeActionState  = probeClick;
      this->probeClickState   = probeSelectFirst;
      
      this->scene->qfeSetSceneCursor(cursorCross);      
      break;
    case probeSelectFirst :
      //cout << "first" << endl;
      this->probePoints[0]  = *p;
      this->probeClickState = probeSelectSecond;
      break;
    case probeSelectSecond :
      //cout << "second" << endl;
      this->probePoints[1]  = *p;
      this->probeClickState = probeSelectThird;
      break;
    case probeSelectThird :
      //cout << "third" << endl;
      this->probePoints[2]  = *p;

      this->qfeAddProbe(this->probePoints[0], this->probePoints[1], this->probePoints[2]);

      this->scene->qfeSceneUpdate();
      this->scene->qfeSetSceneCursor(cursorArrow);      
      
      this->probeClickState  = probeSelectStart;
      this->probeActionState = probeDefault;      

      break;
    case probeSelectAbort :
      //cout << "abort" << endl;
      this->scene->qfeSetSceneCursor(cursorArrow);
      this->probeClickState  = probeSelectStart;
      this->probeActionState = probeDefault;
      break;
  }

  this->engine->Refresh();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeWorkflowProbeFit()
{
  qfeStudy   *study;  
  qfePoint    p[2];
  qfeVector   eyeVec;
  qfeMatrix4f mvInv;

  if(this->algorithmOctree == NULL || this->visProbe3D == NULL) return qfeError;
  
  this->visProbe3D->qfeGetVisualStudy(&study);

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].selected)
    {
      this->algorithmProbe->qfeGetProbeFitLocal(study->probe3D[i], this->algorithmOctree, this->currentModelView);

      // Update the parallel plane
      study->probe3D[i].parallelPlane.qfeSetFrameExtent(2.0*max(study->probe3D[i].baseRadius, study->probe3D[i].topRadius), study->probe3D[i].length);
      study->probe3D[i].parallelPlane.qfeSetFrameOrigin(study->probe3D[i].origin.x, study->probe3D[i].origin.y, study->probe3D[i].origin.z);
      study->probe3D[i].parallelPlane.qfeSetFrameAxes(study->probe3D[i].axes[1].x, study->probe3D[i].axes[1].y, study->probe3D[i].axes[1].z,
                                        study->probe3D[i].axes[2].x, study->probe3D[i].axes[2].y, study->probe3D[i].axes[2].z,
                                        study->probe3D[i].axes[0].x, study->probe3D[i].axes[0].y, study->probe3D[i].axes[0].z);

      study->probe3D[i].interactionState = probeInteractUpdate;
    }
  }

  if(this->probeSeedingStateParticles == probeSeedingInjected)
    this->probeSeedingStateParticles = probeSeedingUpdate;
  if(this->probeSeedingStateLines == probeSeedingInjected)
    this->probeSeedingStateLines = probeSeedingUpdate;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeAddProbe(qfeProbe3D probe)
{
  if(this->visProbe3D == NULL) return qfeError;

  qfeStudy           *study;
  qfeDisplay         *display;
  qfeDisplayLayout    layout;
  bool                dummy;
  qfeMatrix4f         P2V;
  qfePoint            originV;
  unsigned int        dims[3];
  qfeValueType        t;
  void               *v;
  qfeVector           eyeVec;
  qfeMatrix4f         mvInv;

  // Get eye vector in patient coordinates;
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;

  this->visProbe3D->qfeGetVisualStudy(&study);

  // Check if the probe is positioned within the volume
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, study->pcap.front());
  study->pcap.front()->qfeGetVolumeData(t, dims[0], dims[1], dims[2], &v);

  originV = probe.origin * P2V;

  if(! ((originV.x >= 0) && (originV.x < dims[0]) &&
        (originV.y >= 0) && (originV.y < dims[1]) &&
        (originV.z >= 0) && (originV.z < dims[2])))
    return qfeError;

  // Add and select the current probe
  study->probe3D.push_back(probe);  

  // Set the current state
  study->probe3D.back().interactionState = probeInteractNone;

  this->qfeSetSelectedProbe(this->visProbe3D, study->probe3D.size()-1);

  // Probe specific planes  
  study->probe3D.back().parallelPlane.qfeSetFrameExtent(2.0*max(probe.baseRadius, probe.topRadius), probe.length);
  study->probe3D.back().parallelPlane.qfeSetFrameOrigin(probe.origin.x, probe.origin.y, probe.origin.z);
  study->probe3D.back().parallelPlane.qfeSetFrameAxes(probe.axes[1].x, probe.axes[1].y, probe.axes[1].z,
                                                    probe.axes[2].x, probe.axes[2].y, probe.axes[2].z,
                                                    probe.axes[0].x, probe.axes[0].y, probe.axes[0].z);   

  
  study->probe3D.back().orthoPlane.qfeSetFrameExtent(1,1);
  study->probe3D.back().orthoPlane.qfeSetFrameOrigin(0.0,0.0,0.0);
  study->probe3D.back().orthoPlane.qfeSetFrameAxes(0.0,0.0,-1.0, 0.0,1.0,0.0, 1.0,0.0,0.0);
  study->probe3D.back().orthoPlaneOffset = 0.5f;  
  
  // Fix the probe axis
  qfeVector probeNormal;
  probeNormal.qfeSetVectorElements(probe.axes[0].x, probe.axes[0].y, probe.axes[0].z);
  this->algorithmProbe->qfeGetProbeViewAligned(study->probe3D.back(), probeNormal);

  // Set the probe cache
  study->probe3DCache.push_back(study->probe3D.back());

  // Change the viewport layout
  this->scene->qfeGetSceneDisplay(&display);
  if(display != NULL)
    display->qfeGetDisplayLayout(layout, dummy, dummy);
  else
    layout = qfeDisplayLayoutLeft;

  if(study->probe3D.size() == 1)
  {
    this->engine->SetLayout((vtkEngineLayout)layout, false, true);  
    this->qfeOnResize(this);
  }


  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeAddProbe(qfePoint p1, qfePoint p2)
{
  if(this->visProbe3D == NULL) return qfeError;

  //qfeStudy           *study;
  qfeProbe3D          probe;
  qfePoint            origin;
  qfeVector           axisZ;
  qfeFloat            length;

  origin = p1 + 0.5*(p2-p1);
  axisZ  = p2-p1;

  qfeVector::qfeVectorLength(p2-p1, length);
  qfeVector::qfeVectorNormalize(axisZ);

  probe.origin  = origin;
  probe.length  = length;
  probe.axes[2] = axisZ;

  this->qfeAddProbe(probe);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeAddProbe(qfePoint p1, qfePoint p2, qfePoint p3)
{
  if(this->visProbe3D == NULL) return qfeError;
  
  qfeProbe3D          probe;
  qfePoint            origin;
  qfeVector           axisX, axisY, axisZ;
  qfeFloat            length;

  origin = p1 + 0.5*(p2-p1);
  axisY  = p3-p1;
  axisZ  = p2-p1;

  qfeVector::qfeVectorLength(p2-p1, length);
  qfeVector::qfeVectorNormalize(axisY);
  qfeVector::qfeVectorNormalize(axisZ);

  qfeVector::qfeVectorOrthonormalBasis1(axisZ, axisY, axisX);
  //qfeVector::qfeVectorCross(axisZ, axisY, axisX);  
  //qfeVector::qfeVectorNormalize(axisX);  
  //qfeVector::qfeVectorNormalize(axisV); 

  probe.origin  = origin;
  probe.length  = length;
  probe.axes[0] = axisX;
  probe.axes[1] = axisY;
  probe.axes[2] = axisZ;

  this->qfeAddProbe(probe);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeUpdateProbeSeeds()
{
  // Update the particle trace seeds
  if((visParticleTrace != NULL) && (this->probeSeedingStateParticles == probeSeedingUpdate))
  {
    this->qfeInjectSeeds(this->visParticleTrace);

    this->probeSeedingStateParticles = probeSeedingInjected;
  }

  // Update line trace seeds and generate new lines
  if((visLineTrace != NULL) && (this->probeSeedingStateLines == probeSeedingUpdate))
  { 
    //this->qfeClearSeeds(this->visLineTrace);
    this->qfeInjectSeeds(this->visLineTrace);    

    this->probeSeedingStateLines = probeSeedingInjected;   
  } 
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeDeleteProbe(qfeProbe3D *probe)
{
  qfeStudy        *study;
  qfeDisplay      *display;
  qfeDisplayLayout layout;
  bool             dummy;

  if(this->visProbe3D == NULL) return qfeError;

  this->visProbe3D->qfeGetVisualStudy(&study);

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(probe == &study->probe3D[i])
    {
      study->probe3D.erase(study->probe3D.begin() + i);      
      if((int)study->probe3DCache.size() > i)
        study->probe3DCache.erase(study->probe3DCache.begin() + i);      
      
      if(this->visParticleTrace != NULL) this->qfeClearSeeds(this->visParticleTrace);            
      if(this->visLineTrace != NULL)     this->qfeClearSeeds(this->visLineTrace);            
    }
  }

  // Change the viewport layout
  this->scene->qfeGetSceneDisplay(&display);
  if(display != NULL)
    display->qfeGetDisplayLayout(layout, dummy, dummy);
  else
    layout = qfeDisplayLayoutLeft;

  if(study->probe3D.size() <= 0)
  {
    this->engine->SetLayout((vtkEngineLayout)layout, true, false);  
    this->qfeOnResize(this);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeGetSelectedProbe(qfeVisualFlowProbe3D *visual, qfeProbe3D **probe)
{
  qfeStudy           *study;

  *probe = NULL;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].selected == true)
    {
      *probe = &study->probe3D[i];

      return qfeSuccess;
    }
  }

  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeSetSelectedProbe(qfeVisualFlowProbe3D *visual, qfeProbe3D  *probe)
{
  qfeStudy           *study;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(&study->probe3D[i] == probe)
    {
      study->probe3D[i].selected = true;

      return qfeSuccess;
    }
  }

  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeGetProbeIndex(qfeVisualFlowProbe3D *visual, qfeProbe3D  *probe, int &index)
{
  qfeStudy           *study;

  index = -1;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(&study->probe3D[i] == probe)
    {
      index = i;

      return qfeSuccess;
    }
  }

  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeGetSelectedProbe(qfeVisualFlowProbe3D *visual, int &index)
{
  qfeStudy           *study;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].selected == true)
    {
      index = i;
      return qfeSuccess;
    }
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeSetSelectedProbe(qfeVisualFlowProbe3D *visual, int index)
{
  qfeStudy           *study;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(i == index)
      study->probe3D[i].selected = true;
    else
      study->probe3D[i].selected = false;
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeInjectSeeds(qfeVisualFlowParticleTrace *visual)
{
  qfeStudy           *study;
  qfeVisualParameter *param;
  bool                visible;
  qfeSeeds            seeds;
  int                 seedsCount;
  qfeSelectionList    seedsType;
  bool                orthoPlaneVisible = false;
  float               current = 0;

  cout << "injecting" << endl;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  visual->qfeGetVisualParameter("Particles visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(visible);

  visual->qfeGetVisualParameter("Particles count", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(seedsCount);

  visual->qfeGetVisualParameter("Particles seeding", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(seedsType);

  if(!visible) return qfeSuccess;

  if(this->visMPRP != NULL)
  {
    this->visMPRP->qfeGetVisualParameter("MPRP visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(orthoPlaneVisible);
  }
  
  if(this->probeSeedingStateParticles == probeSeedingInjected)
    this->qfeClearSeeds(visual);

  if(orthoPlaneVisible && ((int)study->probe3D.size() > 0))
  {
    switch(seedsType.active)
    {
    case (0) : qfeSeeding::qfeGetSeedsRandomUniform(&study->probe3D.front().orthoPlane, seedsCount, seeds);
               break;
    case (1) : qfeSeeding::qfeGetSeedsRandomPoissonDisk(&study->probe3D.front().orthoPlane, seedsCount, seeds);
               break;
    }
  }
  else
  {
    if(study->probe3D.size() <= 0) return qfeError;

    switch(seedsType.active)
    {
      case (0) : qfeSeeding::qfeGetSeedsRandomUniform(&study->probe3D.front(), seedsCount, seeds);
                 break;
      case (1) : qfeSeeding::qfeGetSeedsRandomPoissonDisk(&study->probe3D.front(), seedsCount, seeds);
                 break;
    }
  }

  if((int)seeds.size() <= 0) return qfeError;

  // add the seed positions
  this->algorithmParticleTrace->qfeClearSeedPositions();
  this->algorithmParticleTrace->qfeAddSeedPositions(seeds, current);

  this->probeSeedingStateParticles = probeSeedingInjected;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeClearSeeds(qfeVisualFlowParticleTrace *visual)
{
  if(this->visParticleTrace != NULL)
    this->algorithmParticleTrace->qfeClearSeedPositions();

  this->probeSeedingStateParticles = probeSeedingNone;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeInjectSeeds(qfeVisualFlowLineTrace *visual)
{  
  qfeStudy           *study;
  qfeVisualParameter *param;
  bool                visible;
  qfeSeeds            seeds;
  int                 seedsCount;
  qfeSelectionList    seedingType;
  qfeSelectionList    seedingBehavior;
  bool                orthoPlaneVisible = false;
  float               current = 0;

  if(visual == NULL) return qfeError;

  // get the study and active volume
  visual->qfeGetVisualStudy(&study);        
  visual->qfeGetVisualActiveVolume(current);    

  visual->qfeGetVisualParameter("Lines visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(visible); 

  visual->qfeGetVisualParameter("Lines count", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(seedsCount);  

  visual->qfeGetVisualParameter("Lines seeding behavior", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(seedingBehavior); 

  visual->qfeGetVisualParameter("Lines seeding type", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(seedingType); 

  if(!visible) return qfeSuccess;

  if(this->visMPRP != NULL)
  {
    this->visMPRP->qfeGetVisualParameter("MPRP visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(orthoPlaneVisible);   
  }

  if(orthoPlaneVisible && ((int)study->probe3D.size() > 0)) 
  {
    switch(seedingType.active)
    {
    case (0) : qfeSeeding::qfeGetSeedsRandomUniform(&study->probe3D[current].orthoPlane, seedsCount, seeds);                     
               break;             
    case (1) : qfeSeeding::qfeGetSeedsRandomPoissonDisk(&study->probe3D.front().orthoPlane, seedsCount, seeds);      
               break;
    }
  }
  else
  {
    if(study->probe3D.size() <= 0) return qfeError;

    switch(seedingType.active)
    {
      case (0) : qfeSeeding::qfeGetSeedsRandomUniform(&study->probe3D.front(), seedsCount, seeds);    
                 break;             
      case (1) : qfeSeeding::qfeGetSeedsRandomPoissonDisk(&study->probe3D.front(), seedsCount, seeds);          
                 break;
    }
  }

  if((int)seeds.size() <= 0) return qfeError;

  // add the seed positions
  if(this->probeSeedingStateLines == probeSeedingUpdate) 
    this->algorithmLineTrace->qfeUpdateSeedPositions(seeds, current);      
  else
  {
    this->qfeClearSeeds(visual);
    this->algorithmLineTrace->qfeAddSeedPositions(seeds, current);      
  }

  // generate the lines
  if(seedingBehavior.active == (int)qfeFlowLinesPathDynamic)
    this->algorithmLineTrace->qfeGeneratePathlines(study->pcap, current, qfeFlowLinesPathDynamic);
  else
    this->algorithmLineTrace->qfeGeneratePathlines(study->pcap, current);

  //}
  this->probeSeedingStateLines = probeSeedingInjected;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeClearSeeds(qfeVisualFlowLineTrace *visual)
{
  if(this->visLineTrace != NULL)
    this->algorithmLineTrace->qfeClearSeedPositions();

  this->probeSeedingStateLines = probeSeedingNone;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeDrawPoint(qfePoint p, int supersampling, qfeColorRGBA color)
{
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glEnable(GL_POINT_SMOOTH);

  glColor4f(color.r, color.g, color.b, color.a);
  glPointSize(4.0*supersampling);

  glBegin(GL_POINTS);
    glVertex3f(p.x, p.y, p.z);
  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeDrawLine(qfePoint p1, qfePoint p2, int supersampling)
{
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glColor3f(0.0,1.0,0.0);
  glLineWidth(2.0*supersampling);

  glBegin(GL_LINES);
    glVertex3f(p1.x, p1.y, p1.z);
    glVertex3f(p2.x, p2.y, p2.z);
  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeEnableBuffer(GLuint texColor, GLuint texDepth)
{
  this->qfeAttachFBO(this->frameBuffer, texColor, texColor, texDepth);
  this->qfeEnableFBO(this->frameBuffer);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeDisableBuffer(GLint index)
{
  this->qfeResetFBO(index);
  this->qfeEnableFBO(this->frameBuffer);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeCreateTexturesIntersect(qfeViewport *vp, int supersampling, GLuint& texColor, GLuint &texDepth)
{
  int ss = supersampling;
  if(ss <= 0) ss = 1;

  glGenTextures(1, &texColor);
  glBindTexture(GL_TEXTURE_2D, texColor);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F,
    vp->width*ss, vp->height*ss, 0, GL_BGRA, GL_FLOAT, NULL);

  glGenTextures(1, &texDepth);
  glBindTexture(GL_TEXTURE_2D, texDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8_EXT, 
    vp->width*ss, vp->height*ss, 0, GL_DEPTH_STENCIL_EXT, GL_UNSIGNED_INT_24_8_EXT, 0);  
  
  glFinish();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeCreateTexturesClipping(qfeViewport *vp, int supersampling, GLuint &texColor, GLuint &texFront, GLuint &texBack)
{
  int ss = supersampling;
  if(ss <= 0) ss = 1;

  glGenTextures(1, &texColor);
  glBindTexture(GL_TEXTURE_2D, texColor);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F,
    vp->width*ss, vp->height*ss, 0, GL_BGRA, GL_FLOAT, NULL);

  glGenTextures(1, &texFront);
  glBindTexture(GL_TEXTURE_2D, texFront);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8_EXT, 
    vp->width*ss, vp->height*ss, 0, GL_DEPTH_STENCIL_EXT, GL_UNSIGNED_INT_24_8_EXT, 0);  

  glGenTextures(1, &texBack);
  glBindTexture(GL_TEXTURE_2D, texBack);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8_EXT, 
    vp->width*ss, vp->height*ss, 0, GL_DEPTH_STENCIL_EXT, GL_UNSIGNED_INT_24_8_EXT, 0);  

  glFinish();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeDeleteTextures()
{
  for(int i=0; i<2; i++)
  {
    if (glIsTexture(this->intersectTexColor[i]))     glDeleteTextures(1, (GLuint *)&this->intersectTexColor[i]);
    if (glIsTexture(this->intersectTexDepth[i]))     glDeleteTextures(1, (GLuint *)&this->intersectTexDepth[i]);
    if (glIsTexture(this->clippingTexColor[i]))      glDeleteTextures(1, (GLuint *)&this->clippingTexColor[i]);
    if (glIsTexture(this->clippingFrontTexDepth[i])) glDeleteTextures(1, (GLuint *)&this->clippingFrontTexDepth[i]);
    if (glIsTexture(this->clippingBackTexDepth[i]))  glDeleteTextures(1, (GLuint *)&this->clippingBackTexDepth[i]);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeGetDataSet(qfeDataTypeIndex index, qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu)
{
  qfeStudy           *study;
  int                 current;

  *volume = NULL;
  vl = vu = 0.0;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(dataTypePCAP) : // PCA-P
    if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time))
    {
      *volume = study->pcap[time];
      study->qfeGetStudyRangePCAP(vl, vu);
    }
    break;
  case(dataTypePCAM) : // PCA-M
    if(((int)study->pcam.size() > 0) && ((int)study->pcam.size() > time))
    {
      *volume = study->pcam[time];
      study->qfeGetStudyRangePCAM(vl, vu);
    }
    break;
  case(dataTypeSSFP) : // SSFP
    if((int)study->ssfp3D.size() > 0)
    {
      // If there is also pcap data loaded, check for the trigger time and load
      // accompanying ssfp data at the right time slot
      if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time))
      {
        qfeFloat att, ptt;
        qfeFloat distance = 9000;
        (int)study->pcap[time]->qfeGetVolumeTriggerTime(ptt);
        for(int i=0; i<(int)study->ssfp3D.size(); i++)
        {
          (int)study->ssfp3D[i]->qfeGetVolumeTriggerTime(att);
          if(abs(att-ptt) < distance)
          {
            distance = abs(att-ptt);
            *volume = study->ssfp3D[i];
          }
        }
      }
      else
        *volume = study->ssfp3D[0];

      study->qfeGetStudyRangeSSFP(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(dataTypeTMIP) : // T-MIP
    if(study->tmip != NULL)
    {
      *volume = study->tmip;
      study->tmip->qfeGetVolumeValueDomain(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(dataTypeFFE) : // Flow anatomy FFE
    if(((int)study->ffe.size() > 0) && ((int)study->ffe.size() > time))
    {
      *volume = study->ffe[time];
      study->qfeGetStudyRangeFFE(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeGetDataPlane(int index, int slice, qfeVisual *visual, qfeVolume **volume)
{
  qfeStudy           *study;  
  int                 current;

  *volume = NULL;
  visual->qfeGetVisualStudy(&study); 
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(0) : // PCA-P
            if(((int)study->flow2D_pcap.size() > 0) && ((int)study->flow2D_pcap.size() >= slice)) 
              *volume = study->flow2D_pcap[slice];
            break;
  case(1) : // PCA-M
            if(((int)study->flow2D_pcam.size() > 0) && ((int)study->flow2D_pcam.size() >= slice)) 
              *volume = study->flow2D_pcam[slice];
            break;
  case(2) : // FFE
            if(((int)study->flow2D_ffe.size() > 0) && ((int)study->flow2D_ffe.size() >= slice)) 
              *volume = study->flow2D_ffe[slice];
            break;
  case(3) : // SSFP
            if(((int)study->ssfp2D.size() > 0) && ((int)study->ssfp2D.size() >= slice)) 
              *volume = study->ssfp2D[slice];
            break;
  }
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeGetDataSlice(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice)
{
  qfeMatrix4f P2V, V2P;
  qfeVector   axisX, axisY, axisZ, axisXabs, axisYabs, axisZabs, x, y, z;
  qfePoint    o;
  qfeGrid    *grid;

  if(volume == NULL) return qfeError;

  // Get the patient to voxel transformation
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);

  // Set the plane normal direction in patient coordinates
  switch(dir)
  {
  case qfePlaneAxial       : axisX.qfeSetVectorElements(1.0,0.0,0.0);
                             axisY.qfeSetVectorElements(0.0,1.0,0.0);
                             axisZ.qfeSetVectorElements(0.0,0.0,1.0);                              
                             break;
  case qfePlaneSagittal    : axisX.qfeSetVectorElements(0.0,0.0,1.0);
                             axisY.qfeSetVectorElements(0.0,1.0,0.0);
                             axisZ.qfeSetVectorElements(1.0,0.0,0.0); 
                             break;
  case qfePlaneCoronal     : axisX.qfeSetVectorElements(1.0,0.0,0.0);
                             axisY.qfeSetVectorElements(0.0,0.0,1.0);
                             axisZ.qfeSetVectorElements(0.0,1.0,0.0); 
                             break;
  }  

  // Convert the axis to voxel coordinates and find dominant direction
  axisX = axisX*P2V;
  axisY = axisY*P2V;
  axisZ = axisZ*P2V;

  axisXabs.qfeSetVectorElements(abs(axisX.x), abs(axisX.y), abs(axisX.z));
  axisYabs.qfeSetVectorElements(abs(axisY.x), abs(axisY.y), abs(axisY.z));
  axisZabs.qfeSetVectorElements(abs(axisZ.x), abs(axisZ.y), abs(axisZ.z));

  if(axisZabs.x == max(max(axisZabs.x, axisYabs.x), axisXabs.x))
  {
    x.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
    y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
    z.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
  }
  if(axisZabs.y == max(max(axisZabs.y, axisYabs.y), axisXabs.y))
  {
    x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
    y.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
    z.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
  }
  if(axisZabs.z == max(max(axisZabs.z, axisYabs.z), axisXabs.z))
  {
    x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
    y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
    z.qfeSetVectorElements( 0.0f,  0.0f,  1.0f);    
  }

  // Convert back to patient coordinates
  x = x*V2P;
  y = y*V2P;
  z = z*V2P;

  // Get the plane origin
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridOrigin(o.x, o.y, o.z);

  // Build the plane
  (*slice)->qfeSetFrameExtent(1,1);
  (*slice)->qfeSetFrameOrigin(o.x, o.y, o.z);
  (*slice)->qfeSetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeGetSliceOblique(qfeVolume *volume, qfeFrameSlice **slice)
{
  qfeGrid * grid;

  if(volume == NULL) return qfeError;

  unsigned int s[3];
  qfePoint  o, e;
  qfeVector x, y, z;    

  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(s[0], s[1], s[2]);
  grid->qfeGetGridOrigin(o.x,o.y,o.z);
  grid->qfeGetGridAxes(x.x,x.y,x.z, y.x,y.y,y.z, z.x,z.y,z.z);
  grid->qfeGetGridExtent(e.x, e.y, e.z);

  (*slice)->qfeSetFrameOrigin(o.x,o.y,o.z);
  (*slice)->qfeSetFrameAxes(x.x,x.y,x.z, y.x,y.y,y.z, z.x,z.y,z.z);
  (*slice)->qfeSetFrameExtent(s[0]*e.x, s[1]*e.y);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverUSR::qfeGetSelectedObject(qfeColorRGB color, int &id, int &nr)
{
  id = -1;
  nr =  0;

  if((color.b >= SELECT_PROBE-0.01f) &&
     (color.b <= SELECT_PROBE+0.01f) &&
     (color.r==color.g))
  {
    id = qfeObjectProbe;
    nr = (int)ceil(color.r*255.0f);
  }
  if((color.b >= SELECT_PROBE_HANDLE-0.01f) &&
     (color.b <= SELECT_PROBE_HANDLE+0.01f) &&
     (color.r==color.g))
  {
    id = qfeObjectProbeHandle;
    nr = (int)ceil(color.r*255.0f);
  }
  if((color.r >= SELECT_PLANE-0.01f) &&
     (color.r <= SELECT_PLANE+0.01f) &&
     (color.g >= SELECT_PLANE-0.01f) &&
     (color.g <= SELECT_PLANE+0.01f) &&
     (color.b >= SELECT_PLANE-0.01f) &&
     (color.b <= SELECT_PLANE+0.01f))
  {
    id = qfeObjectPlaneView;
    nr = 0;
  }
  if((color.b >= SELECT_PROBE_ORTHO-0.01f) &&
     (color.b <= SELECT_PROBE_ORTHO+0.01f) &&
     (color.r==color.g))
  {
    id = qfeObjectProbeOrtho;
    nr = (int)ceil(color.r*255.0f);
  }
  if((color.b >= SELECT_PLANE_ORTHO-0.01f) &&
     (color.b <= SELECT_PLANE_ORTHO+0.01f) &&
     (color.r==color.g))
  {
    id = qfeObjectPlaneOrtho;
    nr = (int)ceil(color.r*255.0f);
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if(volume == NULL) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin + (mm*normal);
  else
    origin = origin - (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = std::max(originTex.x, 0.0f+delta);
  originTex.x = std::min(originTex.x, 1.0f-delta);
  originTex.y = std::max(originTex.y, 0.0f+delta);
  originTex.y = std::min(originTex.y, 1.0f-delta);
  originTex.z = std::max(originTex.z, 0.0f+delta);
  originTex.z = std::min(originTex.z, 1.0f-delta);

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if(volume == NULL) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin - (mm*normal);
  else
    origin = origin + (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = std::max(originTex.x, 0.0f+delta);
  originTex.x = std::min(originTex.x, 1.0f-delta);
  originTex.y = std::max(originTex.y, 0.0f+delta);
  originTex.y = std::min(originTex.y, 1.0f-delta);
  originTex.z = std::max(originTex.z, 0.0f+delta);
  originTex.z = std::min(originTex.z, 1.0f-delta);

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeResetPlane()
{
  qfeStudy *study;
  qfeGrid  *grid;
  qfePoint  origin;

  if(this->visOrtho == NULL) return;

  this->visOrtho->qfeGetVisualStudy(&study);

  study->pcap.front()->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);

  // Reset the ortho planes
  this->planeX->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  this->planeY->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  this->planeZ->qfeSetFrameOrigin(origin.x, origin.y, origin.z);

  // Reset the probes and associated planes
  for(int i=0; i<(int)study->probe3DCache.size(); i++)
  {
    study->probe3D[i].axes[0] = study->probe3DCache[i].axes[0];
    study->probe3D[i].axes[1] = study->probe3DCache[i].axes[1];
    study->probe3D[i].axes[2] = study->probe3DCache[i].axes[2];
    study->probe3D[i].origin  = study->probe3DCache[i].origin;

    study->probe3D[i].parallelPlane = study->probe3DCache[i].parallelPlane;
    study->probe3D[i].orthoPlane    = study->probe3DCache[i].orthoPlane;
  }

  // Reset the pitch angle
  this->probePitch = 0.0f;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnAction(void *caller, int t)
{
  qfeDriverUSR *self = reinterpret_cast<qfeDriverUSR *>(caller);
  qfeStudy *study; 

  switch(t)
  {
  case qfeVisualFlowProbe3D::actionNewProbeClick  :
         self->probeClickState  = probeSelectStart;
         self->probeActionState = probeDefault;
         self->qfeWorkflowProbeThreeClick(NULL);
         break;  
  case qfeVisualFlowProbe3D::actionProbeFit :
         self->qfeWorkflowProbeFit();
         break;
  case qfeVisualFlowProbe3D::actionProbeUndo :
         if(self->visProbe3D == NULL) return;
         self->visProbe3D->qfeGetVisualStudy(&study);
         for(int i=0; i<(int)study->probe3D.size(); i++)
         {
           if(study->probe3D[i].selected)
             self->algorithmProbe->qfeGetProbeFitUndo(study->probe3D[i]);
         }         
         break;  
  case qfeVisualFlowParticleTrace::actionInjectSeeds :
         self->qfeInjectSeeds(self->visParticleTrace);
         self->qfeInjectSeeds(self->visLineTrace);         
         break;
  case qfeVisualFlowParticleTrace::actionClearSeeds :
         self->qfeClearSeeds(self->visParticleTrace);
         self->qfeClearSeeds(self->visLineTrace);
         break;
  case qfeVisualReformatOrtho::actionResetMPR:
         self->qfeResetPlane();
         break;
  }

  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnResize(void *caller)
{
  qfeDriverUSR *self = reinterpret_cast<qfeDriverUSR *>(caller);
  qfeViewport  *vp3D, *vp2D3;  

  self->engine->GetViewport3D(&vp3D);
  self->engine->GetViewport2D(2, &vp2D3);

  qfeDriver::qfeDriverResize(caller);

  // Refresh size of local geometry texturs  
  self->qfeUnbindTextures();
  self->qfeDeleteTextures();
  
  self->qfeCreateTexturesIntersect(vp3D,  self->frameBufferProps[0].supersampling, self->intersectTexColor[0], self->intersectTexDepth[0]);
  self->qfeCreateTexturesClipping(vp3D,  self->frameBufferProps[0].supersampling, self->clippingTexColor[0], self->clippingFrontTexDepth[0], self->clippingBackTexDepth[0]);

  self->qfeCreateTexturesIntersect(vp2D3,  self->frameBufferProps[3].supersampling, self->intersectTexColor[1], self->intersectTexDepth[1]);
  self->qfeCreateTexturesClipping(vp2D3,  self->frameBufferProps[3].supersampling, self->clippingTexColor[1], self->clippingFrontTexDepth[1], self->clippingBackTexDepth[1]);

  // Refresh size of the DVR and MIP textures  
  self->algorithmDVR->qfeSetViewportSize(vp3D->width,  vp3D->height,  self->frameBufferProps[0].supersampling);
  self->algorithmVFR->qfeSetViewportSize(vp2D3->width, vp2D3->height, self->frameBufferProps[3].supersampling);
  self->algorithmMIP->qfeSetViewportSize(vp2D3->width, vp2D3->height, self->frameBufferProps[3].supersampling); 
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v)
{
  qfeDriverUSR *self = reinterpret_cast<qfeDriverUSR *>(caller);
  
  switch(v)
  {
  case(0): self->qfeOnMouseLeftButtonDown3D(x,y);
           break;
  case(1): self->qfeOnMouseLeftButtonDown2D1(x,y);
           break;
  case(2): self->qfeOnMouseLeftButtonDown2D2(x,y);
           break;
  case(3): self->qfeOnMouseLeftButtonDown2D3(x,y);
           break;
  default: self->qfeOnMouseLeftButtonDown3D(x,y);
  }
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v)
{
  qfeDriverUSR *self = reinterpret_cast<qfeDriverUSR *>(caller);

  qfeStudy    *study;
  qfeViewport *vp3D;
  qfeFloat     depth;
  qfePoint     windowCoord, patientCoord;    

  if(self->visProbe3D == NULL) return;
  
  self->visProbe3D->qfeGetVisualStudy(&study);
  self->qfeReadDepth(x, y, depth);
  self->engine->GetViewport3D(&vp3D);

  windowCoord.qfeSetPointElements(x, y, depth);
  qfeTransform::qfeGetCoordinateUnproject(windowCoord, patientCoord, self->currentModelView, self->currentProjection, vp3D);

  // Reset mouse press
  self->viewportMouseButtonDown[0] = false;
  self->viewportMouseButtonDown[1] = false;
  self->viewportMouseButtonDown[2] = false;
  self->viewportMouseButtonDown[3] = false;

  // Reset selections
  self->planeProbeOrthogonalSelected = -1;
  self->planeInteractionDirection    = qfePlaneInteractionNone;
  self->probeClickOffset.qfeSetVectorElements(0.0,0.0,0.0);

  // Update probe interaction states
  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].interactionState != probeInteractNone)
    {
      study->probe3D[i].interactionState = probeInteractNone;

      if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;  
      if(self->probeSeedingStateLines     == probeSeedingInjected)      
        self->probeSeedingStateLines     = probeSeedingUpdate;   
    }
  }

  // Deal with the volume rendering - multiresolution
  self->dvrQuality = 4.0;

  self->engine->Refresh();
  return;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnMouseMove(void *caller, int x, int y, int v)
{
  qfeDriverUSR *self = reinterpret_cast<qfeDriverUSR *>(caller);

  switch(v)
  {
  case(0): self->qfeOnMouseMove3D(x,y);
           break;
  case(1): self->qfeOnMouseMove2D1(x,y);
           break;
  case(2): self->qfeOnMouseMove2D2(x,y);
           break;
  case(3): self->qfeOnMouseMove2D3(x,y);
           break;
  default: self->qfeOnMouseMove3D(x,y);
  }
  
  // Render the scene
  self->engine->Refresh();
  return;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnWheelForward(void *caller, int x, int y, int v)
{
  qfeDriverUSR *self = reinterpret_cast<qfeDriverUSR *>(caller);

  switch(v)
  {
  case(0): self->qfeOnWheelForward3D(x,y);
           break;
  case(1): self->qfeOnWheelForward2D1(x,y);
           break;
  case(2): self->qfeOnWheelForward2D2(x,y);
           break;
  case(3): self->qfeOnWheelForward2D3(x,y);
           break;
  default: self->qfeOnWheelForward3D(x,y);
  }
  
  self->engine->Refresh();

}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnWheelBackward(void *caller, int x, int y, int v)
{
  qfeDriverUSR *self = reinterpret_cast<qfeDriverUSR *>(caller);

  switch(v)
  {
  case(0): self->qfeOnWheelBackward3D(x,y);
           break;
  case(1): self->qfeOnWheelBackward2D1(x,y);
           break;
  case(2): self->qfeOnWheelBackward2D2(x,y);
           break;
  case(3): self->qfeOnWheelBackward2D3(x,y);
           break;
  default: self->qfeOnWheelBackward3D(x,y);
  }
 
  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnKeyPress(void *caller, char *keysym)
{
  qfeDriverUSR *self = reinterpret_cast<qfeDriverUSR *>(caller);

  qfeProbe3D *probe;
  qfeVector   displacement;
  qfeFloat    offset = 2.0;

  self->qfeGetSelectedProbe(self->visProbe3D, &probe);

  string key = keysym;

  if(key.compare("Delete") == 0)
  {
    if(probe == NULL) return;
    self->qfeDeleteProbe(probe);
    self->scene->qfeSceneUpdate();
    self->engine->Refresh();
  }
  if(key.compare("Left") == 0)
  {
    if(probe == NULL) return;
    displacement = -offset * probe->axes[1];

    self->qfeKeyMoveProbe(probe, displacement);	

    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines     == probeSeedingInjected)      
        self->probeSeedingStateLines     = probeSeedingUpdate; 
    
    self->engine->Refresh();
  }
  if(key.compare("Right") == 0)
  {
    if(probe == NULL) return;
    displacement = offset * probe->axes[1];

    self->qfeKeyMoveProbe(probe, displacement);

    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines     == probeSeedingInjected)      
        self->probeSeedingStateLines     = probeSeedingUpdate; 
    
    self->engine->Refresh();
  }
  if(key.compare("Up") == 0)
  {
    if(probe == NULL) return;
    displacement = offset * probe->axes[2];

    self->qfeKeyMoveProbe(probe, displacement);

    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines     == probeSeedingInjected)      
        self->probeSeedingStateLines     = probeSeedingUpdate; 
    
    self->engine->Refresh();
  }
  if(key.compare("Down") == 0)
  {
    if(probe == NULL) return;
    displacement = -offset * probe->axes[2];

    self->qfeKeyMoveProbe(probe, displacement);

    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines     == probeSeedingInjected)      
        self->probeSeedingStateLines     = probeSeedingUpdate; 
    
    self->engine->Refresh();
  }
  if(key.compare("plus") == 0)
  {
    if(probe == NULL) return;
    displacement = -offset * probe->axes[0];

    self->qfeKeyMoveProbe(probe, displacement);

    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines     == probeSeedingInjected)      
        self->probeSeedingStateLines     = probeSeedingUpdate; 
    
    self->engine->Refresh();
  }
  if(key.compare("minus") == 0)
  {
    if(probe == NULL) return;
    displacement = offset * probe->axes[0];

    self->qfeKeyMoveProbe(probe, displacement);

    if(self->probeSeedingStateParticles == probeSeedingInjected)
        self->probeSeedingStateParticles = probeSeedingUpdate;
    if(self->probeSeedingStateLines     == probeSeedingInjected)      
        self->probeSeedingStateLines     = probeSeedingUpdate; 
    
    self->engine->Refresh();
  }
  if(key.compare("x") == 0)
  {
    if(self->planeInteractionState == planeInteractRotate)
      self->planeInteractionState = planeInteractFixed;
    else
       self->planeInteractionState = planeInteractRotate;
  }
  if(key.compare("Escape") == 0)
  {
    self->probeClickState  = probeSelectAbort;    
    self->qfeWorkflowProbeThreeClick(NULL);    
  }
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnWiiPress(void *caller, char *wiikey)
{
  qfeDriverUSR *self = reinterpret_cast<qfeDriverUSR *>(caller);

  string key = wiikey;

  if(key.compare("WII_PLUS") == 0)
  {
    cout << "+" << endl;
    //self->engine->Refresh();
  }

  if(key.compare("WII_MINUS") == 0)
  {
    cout << "-" << endl;
    //self->engine->Refresh();
  }

  if(key.compare("WII_LEFT") == 0)
  {
    cout << "left" << endl;
    self->engine->Azimuth(5);
    self->engine->Refresh();
  }

  if(key.compare("WII_RIGHT") == 0)
  {
    cout << "right" << endl;
    self->engine->Azimuth(-5);
    self->engine->Refresh();
  }

  if(key.compare("WII_UP") == 0)
  {
    cout << "up" << endl;
    self->engine->Elevation(-5);
    self->engine->Refresh();
  }

  if(key.compare("WII_DOWN") == 0)
  {
    cout << "down" << endl;
    self->engine->Elevation(5);
    self->engine->Refresh();
  }

}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeMouseMoveProbe3DOF(qfeProbe3D *probe, qfeVector displacement)
{
  probe->origin   = probe->origin + displacement;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeMouseMoveProbe2DOF(qfeProbe3D *probe, qfeFrameSlice *plane, qfeVolume *volume, qfePoint mousePatientPos, qfeVector offset, qfeVector eye)
{
  bool inside;
  qfePoint intersection;

  qfeDriverUSR::qfeGetIntersectionLineProbePlane(plane, mousePatientPos, eye, intersection);
  qfeDriverUSR::qfeGetInsideBox(volume, intersection, inside);

  if(inside)
    probe->origin = intersection - offset;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeMouseMoveOffset(qfeProbe3D *probe, qfeFrameSlice *plane, qfePoint mousePatientPos, qfeVector eye, qfeVector &offset)
{
  qfePoint p;

  qfeDriverUSR::qfeGetIntersectionLineProbePlane(plane, mousePatientPos, eye, p);

  offset = p - probe->origin;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeKeyMoveProbe(qfeProbe3D *probe, qfeVector displacement)
{
  probe->origin   = probe->origin + displacement;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeMouseMoveSlice(qfeProbe3D *probe, qfeFrameSlice *plane, qfePoint mousePatientPos, qfeVector eye, qfeFloat &offset)
{
  qfePoint base;
  qfePoint mousePatientInPlane;
  qfeVector ab, ap;

  if(probe == NULL ) return;

  qfeDriverUSR::qfeGetIntersectionLineProbePlane(plane, mousePatientPos, eye, mousePatientInPlane);

  base   = probe->origin - 0.5f*probe->length*probe->axes[2];
  ab     = probe->axes[2];
  ap     = mousePatientInPlane - base;

  qfeVector::qfeVectorNormalize(ab);

  // Project mouse position on the probe axis
  offset  = ab*ap;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeGetIntersectionLineProbePlane(qfeProbe3D *probe, qfePoint p, qfeVector e, qfePoint &pos)
{
  qfeFloat  s, nom, den;
  qfePoint  o;
  qfeVector n, d;
  qfePoint  origin;

  if(probe == NULL) return;

  // ray plane intersection
  o   = probe->origin;
  n   = probe->axes[0];
  d   = o-p;

  qfeVector::qfeVectorNormalize(n);
  qfeVector::qfeVectorNormalize(e);

  nom = n*d;
  den = n*e;

  if(den != 0)
    s = nom / den;
  else
    s = -1;

  if(s >= 0)
    pos = p + s*e;
  else
    pos = p;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeGetIntersectionLineProbePlane(qfeFrameSlice *plane, qfePoint p, qfeVector e, qfePoint &pos)
{
  qfeFloat  s, nom, den;
  qfePoint  o;
  qfeVector x, y, n, d;
  qfePoint  origin;

  if(plane == NULL) return;

  // ray plane intersection
  plane->qfeGetFrameOrigin(o.x,o.y, o.z);
  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, n.x, n.y, n.z);  
  d   = o-p;

  qfeVector::qfeVectorNormalize(n);
  qfeVector::qfeVectorNormalize(e);

  nom = n*d;
  den = n*e;

  if(den != 0)
    s = nom / den;
  else
    s = -1;

  if(s >= 0)
    pos = p + s*e;
  else
    pos = p;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeGetInsideBox(qfeVolume *volume, qfePoint p, bool &inside)
{
  qfeMatrix4f      P2V;
  qfeGrid         *grid;
  qfePoint         pVoxel;
  unsigned int     size[3];

  if(volume == NULL) return;

  // Get transformation patient to voxel
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  // Get volume properties
  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(size[0],size[1],size[2]);

  // Transform the coordinate
  pVoxel = p * P2V;

  // Check if the point is in the bounding box
  inside = false;

  if((ceil(pVoxel.x) >=0) && (floor(pVoxel.x) <= size[0]) &&
     (ceil(pVoxel.y) >=0) && (floor(pVoxel.y) <= size[1]) &&
     (ceil(pVoxel.z) >=0) && (floor(pVoxel.z) <= size[2]))
  {
    inside = true;
  }
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeGetDistancePointPlane(qfeFrameSlice *plane, qfePoint p, qfeFloat &d)
{
  qfeVector t, n;
  qfePoint  o;

  plane->qfeGetFrameAxes(t.x,t.y,t.z, t.x,t.y,t.z, n.x,n.y,n.z);
  plane->qfeGetFrameOrigin(o.x, o.y, o.z);

  qfeMath::qfeComputeDistancePointPlane(n, o, p, d); 
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnMouseLeftButtonDown3D(int x, int y)
{
  qfeStudy    *study;
  qfeViewport *vp3D;
  int          objId, objNr;
  qfeColorRGB  color;
  qfeFloat     depth;
  qfePoint     windowCoord, patientCoord, visibleCoord;
  qfeFloat     delta;  

  qfeVector   eyeVec;
  qfeMatrix4f mvInv;
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);

  this->viewportMouseButtonDown[0] = true;
  
  if(this->visProbe3D == NULL) return;

  // Initialize
  delta = 1.0;
  objId = -1;
  objNr =  0;

  // Get eye vector in patient coordinates;
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;

  this->engine->GetViewport3D(&vp3D);  
  this->qfeReadSelect(x, y, color);
  this->qfeReadDepth(x, y, depth);
  
  windowCoord.qfeSetPointElements(x, y, depth);
  qfeTransform::qfeGetCoordinateUnproject(windowCoord, patientCoord, this->currentModelView, this->currentProjection, vp3D);

  visibleCoord = patientCoord - delta*eyeVec;

  this->visProbe3D->qfeGetVisualStudy(&study);

  if(this->engine->GetInteractionControlKey())
  {
    switch(this->probeActionState)
    {
      case probeClick   :
        this->qfeWorkflowProbeThreeClick(&patientCoord);
        this->engine->Refresh();
        break;      
    }
  }
  else
  {
    // Reset selections    
    this->planeSelected = qfePlaneNone;

    // Obtain the selected object
    this->qfeGetSelectedObject(color, objId, objNr);

    // Prepare dragging operation
    qfePoint displacementCoord;
    displacementCoord.qfeSetPointElements(x, y, 0.0);
    qfeTransform::qfeGetCoordinateUnproject(displacementCoord, this->prevPoint, this->currentModelView, this->currentProjection, vp3D);

    //cout << objId << endl;
    
    // Handle mouse left click
    switch(objId)
    {
      case qfeObjectProbe     : //cout << "probe" << endl;
             if((objNr < 0) || (objNr > (int)study->probe3D.size())) break;

             if(study->probe3D[objNr].interactionState == probeInteractNone ||
                study->probe3D[objNr].interactionState == probeInteractUpdate)
             {
               this->qfeSetSelectedProbe(this->visProbe3D, objNr);
               study->probe3D[objNr].interactionState = probeInteractTranslate;
               this->qfeMouseMoveOffset(&study->probe3D[objNr], &study->probe3D[objNr].parallelPlane, this->prevPoint, eyeVec, this->probeClickOffset);
             }
             break;
      case qfeObjectProbeHandle :
             if((objNr < 0) || (objNr > (int)study->probe3D.size())) break;
             //cout << "handle" << endl;
             if(study->probe3D[objNr].interactionState == probeInteractNone ||
                study->probe3D[objNr].interactionState == probeInteractUpdate)
             {
               this->qfeSetSelectedProbe(this->visProbe3D, objNr);
               study->probe3D[objNr].interactionState = probeInteractRotate;
             }
             break;      
      case qfeObjectPlaneOrtho :
             if((objNr < 1) || (objNr > 3)) break;
             this->planeSelected = objNr;             
             this->engine->CallInteractionParentLeftButtonDown();
             break;     
      case qfeObjectProbeOrtho :
             if((objNr < 0) || (objNr > (int)study->probe3D.size())) break;             
             this->planeProbeOrthogonalSelected = objNr;             
             break;      
      default :
             // Default camera interaction
             this->engine->CallInteractionParentLeftButtonDown();             
    }
  }

  // Deal with the volume rendering - multiresolution
  this->dvrQuality = 1.0;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnMouseLeftButtonDown2D1(int x, int y)
{  
  this->viewportMouseButtonDown[1] = true;
  
  // Set the current point
  this->viewportMouseCurrentPoint.x = x;
  this->viewportMouseCurrentPoint.y = y;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnMouseLeftButtonDown2D2(int x, int y)
{  
  this->viewportMouseButtonDown[2] = true;
  
  // Set the current point
  this->viewportMouseCurrentPoint.x = x;
  this->viewportMouseCurrentPoint.y = y;
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnMouseLeftButtonDown2D3(int x, int y)
{  
  this->viewportMouseButtonDown[3] = true;

  // Set the current point
  this->viewportMouseCurrentPoint.x = x;
  this->viewportMouseCurrentPoint.y = y;

  // Deal with the volume rendering - multiresolution
  this->dvrQuality = 1.0;  
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnWheelForward3D(int x, int y)
{
  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;
  qfeVector           eyeVec;
  qfeMatrix4f         mvInv;

  if(this->visOrtho == NULL) return;

  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);

  // Get eye vector in patient coordinates;
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;

  // Obtain the volume
  this->visOrtho->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  this->visOrtho->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);
  
  if(this->planeSelected == qfePlaneX) this->qfeMovePlaneForward(study->pcap.front(), &this->planeX, scrollStep);
  if(this->planeSelected == qfePlaneY) this->qfeMovePlaneForward(study->pcap.front(), &this->planeY, scrollStep);
  if(this->planeSelected == qfePlaneZ) this->qfeMovePlaneForward(study->pcap.front(), &this->planeZ, scrollStep);

  // If there are probe selected, allow movement
  qfeFloat offset = 2.0f;
  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].selected)
    {    
      qfeVector x, y, z, displacement;
      qfeFloat  dir;
      
      study->probe3D[i].parallelPlane.qfeGetFrameAxes(x.x,x.y,x.z,y.x,y.y,y.z,z.x,z.y,z.z);
      
      if(eyeVec*z > 0) dir =  1.0f;
      else             dir = -1.0f;

      displacement = dir * offset * z;

      this->qfeKeyMoveProbe(&study->probe3D[i], displacement);

      if(this->probeSeedingStateParticles == probeSeedingInjected)
          this->probeSeedingStateParticles = probeSeedingUpdate;           
    }
  }
  this->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnWheelForward2D1(int x, int y)
{
  if(this->planeViewFlip)
    this->planeViewAngle = min(this->planeViewAngle+4, 360);  
  else
    this->planeViewAngle = max(this->planeViewAngle-4, 0);  
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnWheelForward2D2(int x, int y)
{
  qfeStudy     *study;
  int           index;
  qfeFloat      current;
  
  if(this->visProbe3D == NULL) return;

  this->visProbe3D->qfeGetVisualStudy(&study);  
  
  this->qfeGetSelectedProbe(this->visProbe3D, index);

  if((index >= 0 ) && (index < (int)study->probe3D.size()))
  {    
    current  = study->probe3D[index].orthoPlaneOffset;
    study->probe3D[index].orthoPlaneOffset = std::max(std::min((current*study->probe3D[index].length-2)/study->probe3D[index].length,1.0f),0.0f);  
  }
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnWheelForward2D3(int x, int y)
{
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnWheelBackward3D(int x, int y)
{
  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;
   qfeVector           eyeVec;
  qfeMatrix4f         mvInv;

  if(this->visOrtho == NULL) return;

  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);

  // Get eye vector in patient coordinates;
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;

  // Obtain the volume
  this->visOrtho->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  this->visOrtho->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);

  if(this->planeSelected == qfePlaneX) this->qfeMovePlaneBackward(study->pcap.front(), &this->planeX, scrollStep);
  if(this->planeSelected == qfePlaneY) this->qfeMovePlaneBackward(study->pcap.front(), &this->planeY, scrollStep);
  if(this->planeSelected == qfePlaneZ) this->qfeMovePlaneBackward(study->pcap.front(), &this->planeZ, scrollStep);

  // If there are probe selected, allow movement
  qfeFloat offset = 2.0f;
  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].selected)
    {    
      qfeVector x, y, z, displacement;
      qfeFloat  dir;
      
      study->probe3D[i].parallelPlane.qfeGetFrameAxes(x.x,x.y,x.z,y.x,y.y,y.z,z.x,z.y,z.z);
      
      if(eyeVec*z > 0) dir = -1.0f;
      else             dir =  1.0f;

      displacement = dir * offset * z;

      this->qfeKeyMoveProbe(&study->probe3D[i], displacement);

      if(this->probeSeedingStateParticles == probeSeedingInjected)
          this->probeSeedingStateParticles = probeSeedingUpdate;           
    }
  }
  this->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnWheelBackward2D1(int x, int y)
{  
  if(this->planeViewFlip)
    this->planeViewAngle = max(this->planeViewAngle-4, 0);
  else
    this->planeViewAngle = min(this->planeViewAngle+4, 360);
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnWheelBackward2D2(int x, int y)
{
  qfeStudy     *study;
  int           index;
  qfeFloat      current;
  
  if(this->visProbe3D == NULL) return;

  this->visProbe3D->qfeGetVisualStudy(&study);  
  
  this->qfeGetSelectedProbe(this->visProbe3D, index);

  if((index >= 0 ) && (index < (int)study->probe3D.size()))
  {    
    current  = study->probe3D[index].orthoPlaneOffset;
    study->probe3D[index].orthoPlaneOffset = std::max(std::min((current*study->probe3D[index].length+2)/study->probe3D[index].length,1.0f),0.0f);  
  }
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnWheelBackward2D3(int x, int y)
{
}


//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnMouseMove3D(int x, int y)
{
  qfeStudy    *study;
  qfeViewport *vp3D;
  qfePoint     windowCoord, patientCoord;
  qfeVector    probeDisplacement;
  qfeFloat     depth;     
  qfeColorRGB  color;
  int          objId, objNr;

  qfeVector   eyeVec;
  qfeMatrix4f mvInv;
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, mvInv);

  // Get eye vector in patient coordinates;
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv;

  qfeVisualParameter *param;
  qfeSelectionList    paramInteractionList;

  this->qfeReadDepth(x, y, depth);
  this->qfeReadSelect(x, y, color);
  this->engine->GetViewport3D(&vp3D);

  if(this->visProbe3D == NULL) return;

  this->visProbe3D->qfeGetVisualStudy(&study);
  this->visProbe3D->qfeGetVisualParameter("Probe interaction", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInteractionList);

  windowCoord.qfeSetPointElements(x, y, 0.0);
  qfeTransform::qfeGetCoordinateUnproject(windowCoord, this->nextPoint, this->currentModelView, this->currentProjection, vp3D);

  // Obtain the selected object
  this->qfeGetSelectedObject(color, objId, objNr);

  if(objId == qfeObjectProbe || objId == qfeObjectProbeOrtho) 
    this->planeOpacity = 0.3;
  else
    this->planeOpacity = 1.0;

  // Perform parent interaction
  this->engine->CallInteractionParentMouseMove();
  
  // Deal with probe interaction
  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].interactionState == probeInteractTranslate)
    {
       // Update the origin
       probeDisplacement   = this->nextPoint - this->prevPoint;
       probeDisplacement.w = 0.0f;

       if(paramInteractionList.active == 1)
         this->qfeMouseMoveProbe3DOF(&study->probe3D[i], probeDisplacement);
       else
         this->qfeMouseMoveProbe2DOF(&study->probe3D[i], &study->probe3D[i].parallelPlane, study->pcap.front(), this->nextPoint, this->probeClickOffset, eyeVec);

       // Update for the next move event
       this->prevPoint     = this->nextPoint;
    }

    if(study->probe3D[i].interactionState == probeInteractRotate)
    {
      qfeFloat  angle;
      qfeVector v1, v2;
      qfePoint  posInPlane;
      bool      inside;

      this->qfeGetIntersectionLineProbePlane(&study->probe3D[i].parallelPlane, this->nextPoint, eyeVec, posInPlane);
      this->qfeGetInsideBox(study->pcap.front(), posInPlane, inside);

      v1 = study->probe3D[i].axes[2];
      v2 = posInPlane - study->probe3D[i].origin;

      qfeVector::qfeVectorNormalize(v1);
      qfeVector::qfeVectorNormalize(v2);

      angle = v1*v2;

      study->probe3D[i].qfeProbeAzimuth(angle);

      // Update for the next move event
      this->prevPoint     = this->nextPoint;
    }
  }

  // Deal with probe orthogonal planes
  if(this->planeProbeOrthogonalSelected >= 0)
  {
    qfeFloat offset;
    int      index = this->planeProbeOrthogonalSelected;
    this->qfeMouseMoveSlice(&study->probe3D[index], &study->probe3D[index].parallelPlane, this->nextPoint, eyeVec, offset);
    study->probe3D[index].orthoPlaneOffset = std::max(std::min(offset/study->probe3D[index].length,1.0f),0.0f);
  }

  // Deal with interactive pathlines and surfaces 
  for(int i=0; i<(int)study->probe3D.size(); i++)
  {    
     if(study->probe3D[i].interactionState == probeInteractTranslate ||
        study->probe3D[i].interactionState == probeInteractRotate)     
     {
       if(this->probeSeedingStateLines == probeSeedingInjected)
          this->probeSeedingStateLines = probeSeedingUpdate;
     }          
  }
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnMouseMove2D1(int x, int y)
{
  qfeStudy    *study;
  qfeFloat     depth;
  qfeViewport *vp2D1;  

  this->engine->GetViewport2D(0, &vp2D1);
  this->qfeReadDepth(x, y, depth);

   // Reset the overlay
  this->planeOverlayVisible = false;     

  if(this->visProbe3D == NULL) return;
  this->visProbe3D->qfeGetVisualStudy(&study);  

  if(this->viewportMouseButtonDown[1] == true)
  {
	  qfeFloat diffX, diffY;
    qfeFloat signX, signY;
    qfeFloat accelerateX, accelerateY;
	
	  diffX = x - this->viewportMouseCurrentPoint.x;
    diffY = y - this->viewportMouseCurrentPoint.y;

    if(diffX >=0 ) signX =  1.0f;
    else           signX = -1.0f;

    if(diffY >=0 ) signY =  1.0f;
    else           signY = -1.0f;
    
    accelerateX = min((abs(diffX)/vp2D1->width)*PI, 0.1f*PI);
    accelerateY = min((abs(diffY)/vp2D1->height)*PI, 0.1f*PI);

    if(this->planeInteractionDirection == qfePlaneInteractionNone)
    {
      if(abs(diffX) > abs(diffY))
        this->planeInteractionDirection = qfePlaneInteractionHorizontal;
      else
        this->planeInteractionDirection = qfePlaneInteractionVertical;
    }


    for(int i=0; i<(int)study->probe3D.size(); i++)
    {
      if(study->probe3D[i].selected)
      {
        qfeVector planeX, planeY, planeZ;
        
        if(this->planeInteractionDirection == qfePlaneInteractionHorizontal)
        { 
          // Azimuth about the original x axis from the probe cache
          study->probe3D[i].qfeProbeRotate(study->probe3DCache[i].axes[0], -1.0f*signX*accelerateX);
        }
        else        
        {
          // Pitch about the original y-axis from the probe cache
          qfeVector d, npr, npl, ncrossn;

          npr = study->probe3D[i].axes[2];
          study->probe3D[i].parallelPlane.qfeGetFrameAxes(d.x,   d.y,   d.z,
                                                          d.x,   d.y,   d.z,
                                                          npl.x, npl.y, npl.z);  
          qfeVector::qfeVectorNormalize(npr);
          qfeVector::qfeVectorNormalize(npl);
          qfeVector::qfeVectorOrthonormalBasis1(npl, npr, ncrossn);

          accelerateY = min(accelerateY, 0.04f*float(PI));

          this->probePitch += signY*accelerateY;
          this->probePitch  = max(this->probePitch, -0.45f*float(PI));
          this->probePitch  = min(this->probePitch,  0.45f*float(PI));

          //cout << this->probePitch << endl;
          
          if((probePitch < 0.45f*float(PI)) && (probePitch > -0.45f*float(PI)))
            study->probe3D[i].qfeProbeRotate(ncrossn, -1.0f*signY*accelerateY);

          /*
          // Update the probe parallel plane
          planeX = study->probe3DCache[i].axes[1];
          planeY = study->probe3D[i].axes[2];
          qfeVector::qfeVectorNormalize(planeX);
          qfeVector::qfeVectorNormalize(planeY);
          qfeVector::qfeVectorOrthonormalBasis1(planeX, planeY, planeZ);
          study->probe3D[i].parallelPlane.qfeSetFrameAxes(planeX.x, planeY.y, planeY.z,
                                                          planeY.x, planeY.y, planeY.z,
                                                          planeZ.x, planeZ.y, planeZ.z);        
           */
        }
      }
    } 
  }
  else
  {
    // If we are not interacting, and on top of the plane
	  // Show the overlay
    if(depth != 1)
      this->planeOverlayVisible = true;  
  }

  // Set the current point
  this->viewportMouseCurrentPoint.x = x;
  this->viewportMouseCurrentPoint.y = y;

  // Perform parent interaction
  if(this->viewportMouseButtonDown[0] == false)
    this->engine->CallInteractionParentMouseMove();  
}

//----------------------------------------------------------------------------  
void qfeDriverUSR::qfeOnMouseMove2D2(int x, int y)
{
  qfeStudy *study;
 
  if(this->visProbe3D == NULL) return;
  this->visProbe3D->qfeGetVisualStudy(&study);

  if(this->viewportMouseButtonDown[2] == true)
  {
	  qfeFloat diffX, diffY, d;
	
	  diffX = x - this->viewportMouseCurrentPoint.x;  
    diffY = y - this->viewportMouseCurrentPoint.y; 

    if(abs(diffX) >= abs(diffY))
      d = diffX;
    else
      d = diffY;
    
    for(int i=0; i<(int)study->probe3D.size(); i++)
    {
      if(study->probe3D[i].selected)
      {       
        // Roll about the probe z axis
        study->probe3D[i].qfeProbeRotate(study->probe3D[i].axes[2],-1.0f*d*0.05f);
      }
    } 
  }
 
  // Set the current point
  this->viewportMouseCurrentPoint.x = x;
  this->viewportMouseCurrentPoint.y = y;

  // Perform parent interaction
  if(this->viewportMouseButtonDown[0] == false)
    this->engine->CallInteractionParentMouseMove();  
}

//----------------------------------------------------------------------------
void qfeDriverUSR::qfeOnMouseMove2D3(int x, int y)
{
  qfeStudy    *study;
  qfeFloat     depth;
  qfeViewport *vp2D3;  

  this->engine->GetViewport2D(2, &vp2D3);
  this->qfeReadDepth(x, y, depth);

  // Reset the overlay
  this->planeOverlayVisible = false;     

  if(this->visProbe3D == NULL) return;
  this->visProbe3D->qfeGetVisualStudy(&study);

  if(this->viewportMouseButtonDown[3] == true)
  {    
    qfeFloat diffX, diffY;
    qfeFloat signX, signY;
    qfeFloat accelerateX, accelerateY;
  	
    diffX = x - this->viewportMouseCurrentPoint.x;
    diffY = y - this->viewportMouseCurrentPoint.y;

    if(diffX >=0 ) signX =  1.0f;
    else           signX = -1.0f;

    if(diffY >=0 ) signY =  1.0f;
    else           signY = -1.0f;
      
    accelerateX = min((abs(diffX)/vp2D3->width)*PI, 0.1f*PI);
    accelerateY = min((abs(diffY)/vp2D3->height)*PI, 0.1f*PI);

    if(this->planeInteractionDirection == qfePlaneInteractionNone)
    {
      if(abs(diffX) > abs(diffY))
        this->planeInteractionDirection = qfePlaneInteractionHorizontal;
      else
        this->planeInteractionDirection = qfePlaneInteractionVertical;
    }

    for(int i=0; i<(int)study->probe3D.size(); i++)
    {
      if(study->probe3D[i].selected)
      {
        qfeVector planeX, planeY, planeZ;
          
        if(this->planeInteractionDirection == qfePlaneInteractionHorizontal)
        { 
          // Roll about the probe z axis
          study->probe3D[i].qfeProbeRotate(study->probe3D[i].axes[2], -1.0f*diffX*0.05f);
        }
        else        
        {
          // Restricted picth of the probe
          this->probeVolumePitch += int(signY)*accelerateY*90;
          this->probeVolumePitch  = min(this->probeVolumePitch, 90);
          this->probeVolumePitch  = max(this->probeVolumePitch,-90);
        }
      }
    }  
  }
  else this->engine->CallInteractionParentMouseMove(); 

  // Set the current point
  this->viewportMouseCurrentPoint.x = x;
  this->viewportMouseCurrentPoint.y = y;
}
