#pragma once

#include "qflowexplorer.h"

#include "qfeDriver.h"
#include "qfeFlowLineTrace.h"
#include "qfeFlowOctree.h"
#include "qfeFlowProbe3D.h"
#include "qfeFlowParticleTrace.h"
#include "qfeFlowSurfaceTrace.h"
#include "qfeMaximumProjection.h"
#include "qfePlanarReformat.h"
#include "qfeRaycasting.h"
#include "qfeSeeding.h"
#include "qfeSurfaceShading.h" 
#include "qfeSurfaceContours.h" 

//#include "time.h"
#include <windows.h>

#define  SELECT_PLANE        0.2
#define  SELECT_PLANE_ORTHO  0.4
#define  SELECT_PLANE_PROBE  0.6
#define  SELECT_PROBE        0.5
#define  SELECT_PROBE_HANDLE 0.7

#define  ANIMATION_FRAME_COUNT 10

using namespace std;

/**
* \file   qfeDriverVPR.h
* \author Roy van Pelt
* \class  qfeDriverVPR
* \brief  Implements a Vector Field Probing (VPR) driver
*
* qfeDriverVPR derives from qfeDriver, and implements
* a GPU-based Vector Field Probing approach
*/

class qfeDriverVPR : public qfeDriver
{
public:
  qfeDriverVPR(qfeScene *scene);
  ~qfeDriverVPR();

  bool qfeRenderCheck();
  void qfeRenderSelect();
  void qfeRenderPositions();
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();

private:
  qfeVisualFlowProbe3D           *visProbe3D;
  qfeVisualReformatOrtho         *visOrtho;
  qfeVisualReformatOblique       *visMPR;
  qfeVisualReformatObliqueProbe  *visMPRP;
  qfeVisualFlowParticleTrace     *visParticleTrace;
  qfeVisualFlowLineTrace         *visLineTrace;  
  qfeVisualFlowSurfaceTrace      *visSurfaceTrace; 
  qfeVisualRaycast               *visDVR;  
  qfeVisualMaximum               *visMIP;  
  qfeVisualAnatomyIllustrative   *visSurface;
  qfeVisualAides                 *visAides;

  qfeFlowProbe3D                 *algorithmProbe;
  qfeFlowOctree                  *algorithmOctree;
  qfePlanarReformat              *algorithmMPR;      
  qfeFlowParticleTrace           *algorithmParticleTrace;  
  qfeFlowLineTrace               *algorithmLineTrace;  
  qfeFlowSurfaceTrace            *algorithmSurfaceTrace;  
  qfeRayCasting                  *algorithmDVR;  
  qfeMaximumProjection           *algorithmMIP; 
  qfeSurfaceShading              *algorithmSurfaceShading;
  qfeSurfaceContours             *algorithmSurfaceContours;

  vector< qfeProbe3D >            probeShadowList;
    
  GLuint                          intersectTexColor;
  GLuint                          intersectTexDepth;
  GLuint                          clippingTexColor;
  GLuint                          clippingFrontTexDepth;
  GLuint                          clippingBackTexDepth;

  qfeFrameSlice                  *planeX, *planeY, *planeZ, *planeQ;
  int                             planeSelected;
  vector< qfeFrameSlice *>        planeProbeParallel;
  vector< qfeFrameSlice *>        planeProbeOrthogonal;
  vector< qfeFloat >              planeProbeOrthogonalOffset;
  int                             planeProbeOrthogonalSelected;
  qfeColorRGB                     planeColorDefault;
  qfeColorRGB                     planeColorHighlight;

  qfeFlowSurfaceStyle             surfaceStyle;
  double                          surfaceRadius;  
  int                             surfaceCount;

  qfePoint                        prevPoint;
  qfePoint                        nextPoint;
  qfeFloat                        prevTime;

  vector< qfeProbeInteractionState >        probeInteractionState;  // Current interaction per probe
  qfePlaneInteractionState                  planeInteractionState;
  qfeProbeSeedingState                      probeSeedingStateParticles;
  qfeProbeSeedingState                      probeSeedingStateLines;
  qfeProbeSeedingState                      probeSeedingStateSurfaces;

  qfeProbeActionState                       probeActionState;       // Current workflow for adding a new probe 
  qfeProbeSelectState                       probeClickState;        // Current state for adding with 2-clicks
  qfeProbeSelectState                       probeStrokeState;       // Curren  state for adding with single stroke
  qfePoint                                  probePoints[2];  
  qfeColorRGB                               probeColor[6];

  qfeVector                                 probeClickOffset;
  int                                       probeAnimateCount;

  float                                     dvrQuality;

  enum qfeDataTypeIndex
  {
    dataTypePCAP,
    dataTypePCAM,
    dataTypeSSFP,
    dataTypeTMIP, 
    dataTypeFFE
  };

  enum qfePlaneDirection
  {
    qfePlaneAxial,
    qfePlaneSagittal,
    qfePlaneCoronal,
  };

  enum qfePlaneSelected
  {
    qfePlaneNone,
    qfePlaneX,
    qfePlaneY,
    qfePlaneZ
  };
  
  // Render geometry
  qfeReturnStatus qfeRenderVisualProbe(qfeVisualFlowProbe3D *visual, int supersampling);  
  qfeReturnStatus qfeRenderVisualOrtho(qfeVisualReformat *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualMPR(qfeVisualReformat *visual, int supersampling);  
  qfeReturnStatus qfeRenderVisualMPRP(qfeVisualReformat *visual, int supersampling);  
  qfeReturnStatus qfeRenderVisualParticleTrace(qfeVisualFlowParticleTrace *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualLineTrace(qfeVisualFlowLineTrace *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualSurfaceTrace(qfeVisualFlowSurfaceTrace *visual, int supersampling);

  // Render volumes
  qfeReturnStatus qfeRenderVisualMIP(qfeVisualMaximum *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualDVR(qfeVisualRaycast *visual, int supersampling);
  qfeReturnStatus qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual, int supersampling);
  
  // Render visual aides
  qfeReturnStatus qfeRenderVisualAides(qfeVisualAides *visual, int supersampling);

  // Workflow steps
  qfeReturnStatus qfeWorkflowProbeClick(qfePoint *p);
  qfeReturnStatus qfeWorkflowProbeStroke(qfePoint *p);
  qfeReturnStatus qfeWorkflowProbeFit();
  
  // Workflow related functions
  qfeReturnStatus qfeAddProbe(qfeProbe3D probe);
  qfeReturnStatus qfeAddProbe(qfePoint p1, qfePoint p2);  
  qfeReturnStatus qfeDeleteProbe(qfeProbe3D *probe);  
  qfeReturnStatus qfeUpdateProbeSeeds();  
    
  qfeReturnStatus qfeSetSelectedProbe(qfeVisualFlowProbe3D *visual, qfeProbe3D  *probe);
  qfeReturnStatus qfeGetSelectedProbe(qfeVisualFlowProbe3D *visual, qfeProbe3D **probe);
  qfeReturnStatus qfeGetSelectedProbe(qfeVisualFlowProbe3D *visual, int &index);
  qfeReturnStatus qfeSetSelectedProbe(qfeVisualFlowProbe3D *visual, int index);  
  qfeReturnStatus qfeGetProbeIndex(qfeVisualFlowProbe3D *visual, qfeProbe3D  *probe, int &index);

  qfeReturnStatus qfeRotateProbe(int index, qfeFloat angle);
  qfeReturnStatus qfeSetProbeAnimation();

  qfeReturnStatus qfeInjectSeeds(qfeVisualFlowParticleTrace *visual);
  qfeReturnStatus qfeInjectFileSeeds(qfeVisualFlowParticleTrace *visual);
  qfeReturnStatus qfeClearSeeds(qfeVisualFlowParticleTrace *visual);
  qfeReturnStatus qfeInjectSeeds(qfeVisualFlowLineTrace *visual);
  qfeReturnStatus qfeInjectFileSeeds(qfeVisualFlowLineTrace *visual);
  qfeReturnStatus qfeClearSeeds(qfeVisualFlowLineTrace *visual);
  qfeReturnStatus qfeInjectSeeds(qfeVisualFlowSurfaceTrace *visual);
  qfeReturnStatus qfeClearSeeds(qfeVisualFlowSurfaceTrace *visual);
  
  qfeReturnStatus qfeDrawPoint(qfePoint p, int supersampling);
  qfeReturnStatus qfeDrawLine(qfePoint p1, qfePoint p2, int supersampling);
  
  // Get the appropriate dataset or selection   
  qfeReturnStatus qfeGetDataSet(qfeDataTypeIndex index, qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu);
  qfeReturnStatus qfeGetDataSlice(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice);
  qfeReturnStatus qfeGetSelectedObject(qfeColorRGB color, int &id, int &nr);  

  // Texture functions
  qfeReturnStatus qfeCreateTextures(qfeViewport *vp, int supersampling);
  qfeReturnStatus qfeDeleteTextures();
  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeEnableIntersectBuffer();
  qfeReturnStatus qfeDisableIntersectBuffer();  
  qfeReturnStatus qfeEnableClippingBuffer(GLuint depthBuffer);
  qfeReturnStatus qfeDisableClippingBuffer();  

  // MPR interaction
  void qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm);
  void qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm);
  void qfeResetPlane();

  // MPR ortho interaction
  static void qfeMouseMoveSlice(qfeProbe3D *probe, qfePoint mousePatientPos, qfeVector eye, qfeFloat &offset);  
  
  // Probe interaction
  static void qfeMouseMoveProbe3DOF(qfeProbe3D *probe, qfeVector displacement);
  static void qfeMouseMoveProbe2DOF(qfeProbe3D *probe, qfeVector displacement);
  static void qfeMouseMoveProbe2DOF(qfeProbe3D *probe, qfeVolume* volume, qfePoint mousePatientPos, qfeVector offset, qfeVector eye);  
  static void qfeMouseMoveOffset(qfeProbe3D *probe, qfePoint mousePatientPos, qfeVector eye, qfeVector &offset);

  static void qfeKeyMoveProbe(qfeProbe3D *probe, qfeVector displacement);
   
  static void qfeGetIntersectionLineProbePlane(qfeProbe3D *probe, qfePoint p, qfeVector e, qfePoint &pos);
  static void qfeGetInsideBox(qfeVolume *volume, qfePoint p, bool &inside);
  
  // Interaction callbacks
  static void qfeOnAction(void *caller, int t);  
  static void qfeOnResize(void *caller);  
  static void qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v);
  static void qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v);
  static void qfeOnMouseMove(void *caller, int x, int y, int v);
  static void qfeOnWheelForward(void *caller, int x, int y, int v);
  static void qfeOnWheelBackward(void *caller, int x, int y, int v);
  static void qfeOnKeyPress(void *caller, char *keysym);
  static void qfeOnWiiPress(void *caller, char *wiikey);

 };
