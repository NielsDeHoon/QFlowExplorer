#pragma once

#include "qflowexplorer.h"

#include "qfeDriver.h"
#include "qfeMaximumProjection.h"
#include "qfeMagnitudeFilter.h"
#include "qfeTMIPFilter.h"
#include "qfeStdDevFilter.h"
#include "qfeGradientMagnitudeFilter.h"
#include "qfeVectorMedianFilter.h"
#include "qfeConvert.h"
#include "qfeVTIImageDataWriter.h"

using namespace std;

/**
* \file   qfeDriverFilter.h
* \author Arjan Broos
* \class  qfeDriverFilter
* \brief  Driver for different noise filters
*
* Longer description.
*/

class qfeDriverFilter : public qfeDriver {
public:
  qfeDriverFilter(qfeScene *scene);
  ~qfeDriverFilter();

  bool qfeRenderCheck();
  void qfeRenderSelect() {};
  void qfeRenderStart();
  void qfeRenderWait();
  void qfeRenderStop();

private:
  qfeVisualFilter *visualFilter;
  qfeMaximumProjection algorithmMIP;
  qfeMagnitudeFilter algorithmMagFilter;
  qfeTMIPFilter algorithmTMIPFilter;
  qfeStdDevFilter algorithmSdFilter;
  qfeGradientMagnitudeFilter algorithmGradFilter;
  qfeVectorMedianFilter algorithmVMFilter;

  GLuint currentTransferFunctionId;

  GLuint texRayStart;
  GLuint texRayEnd;

  qfeVolumeSeries filteredMagnitude;
  qfeVolumeSeries filteredVelocity;

  bool previousInvertedParam;
  bool previousMagFilterParam;
  double previousMagFilterThresholdParam;
  bool previousTMIPFilterParam;
  double previousTMIPFilterThresholdParam;
  bool previousSdFilterMagParam;
  bool previousSdFilterVelParam;
  bool previousSdFilterBelowParam;
  double previousSdFilterThresholdParam;
  bool previousGradFilterParam;
  bool previousVMFilterParam;

  qfeReturnStatus qfeRenderVisualFilter(qfeVisualFilter *visual);
  qfeReturnStatus qfeGetDataSet(int index, qfeVisual *visual, int time, qfeVolume **volume);

  void qfeUpdateMagnitude();
  void qfeUpdateVelocity();

  void WriteFilteredData();
  void MmToCmVel(qfeVolume *volume);
  void ReverseZ(qfeVolume *volume);
  void ReverseVelX(qfeVolume *volume);
  void ReverseVelY(qfeVolume *volume);
  void ReverseVelZ(qfeVolume *volume);
  void SwapXY(qfeVolume *volume);
  void SwapXZ(qfeVolume *volume);
  void SwapYZ(qfeVolume *volume);

  static void qfeOnResize(void *caller);
  static void qfeOnAction(void *caller, int t);
};
