#include "qfeDriverMPR.h"

//----------------------------------------------------------------------------
qfeDriverMPR::qfeDriverMPR(qfeScene *scene) : qfeDriver(scene)
{
  this->visOrtho      = NULL;
  this->visQflow      = NULL;
  this->visArrows     = NULL;
  this->visAides      = NULL;

  // Initialize plane line colors
  this->colorDefault.r = 0.952;
  this->colorDefault.g = 0.674;
  this->colorDefault.b = 0.133;

  this->colorHighlight.r = 0.356;
  this->colorHighlight.g = 0.623;
  this->colorHighlight.b = 0.396;

  // Initialize the planes (in patient coordinates)
  this->planeX = new qfeFrameSlice();      
  this->planeX->qfeSetFrameExtent(1,1);
  this->planeX->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeX->qfeSetFrameAxes(0.0,0.0,1.0, 0.0,1.0,0.0, 1.0,0.0,0.0);  

  this->planeY = new qfeFrameSlice();      
  this->planeY->qfeSetFrameExtent(1,1);
  this->planeY->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeY->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,0.0,1.0, 0.0,1.0,0.0);  
  
  this->planeZ = new qfeFrameSlice();      
  this->planeZ->qfeSetFrameExtent(1,1);
  this->planeZ->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeZ->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0);  

  this->planeQ = new qfeFrameSlice();      
  this->planeQ->qfeSetFrameExtent(1,1);
  this->planeQ->qfeSetFrameOrigin(0.0,0.0,0.0);
  this->planeQ->qfeSetFrameAxes(1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,-1.0); 

  this->selectedPlane = qfePlaneNone;
  
  this->algorithmOrtho   = new qfePlanarReformat(); 
  this->algorithmOblique = new qfePlanarReformat(); 

  this->engine->SetInteractionMouseLeftButtonDown((void*)this, this->qfeOnMouseLeftButtonDown);
  this->engine->SetInteractionMouseWheelForward((void*)this, this->qfeOnWheelForward, false);
  this->engine->SetInteractionMouseWheelBackward((void*)this, this->qfeOnWheelBackward, false);
};

//----------------------------------------------------------------------------
qfeDriverMPR::~qfeDriverMPR()
{
  this->qfeRenderStop();

  delete this->planeX;
  delete this->planeY;
  delete this->planeZ;
  delete this->planeQ;

  delete this->algorithmOrtho;
  delete this->algorithmOblique;
};

//----------------------------------------------------------------------------
bool qfeDriverMPR::qfeRenderCheck()
{
  int t;
  for(int i=0; i<(int)this->visuals.size(); i++)
  {    
    this->visuals[i]->qfeGetVisualType(t);
    if(t == visualReformatOrtho)
    {
      this->visOrtho = static_cast<qfeVisualReformatOrtho *>(this->visuals[i]);
    }
    if(t == visualReformatFixed)
    {
      this->visQflow = static_cast<qfeVisualReformatFixed *>(this->visuals[i]);
    }
    if(t == visualPlanesArrowHeads)
    {
      this->visArrows = static_cast<qfeVisualPlanesArrowHeads *>(this->visuals[i]);      
    }
    if(t == visualAides)
    {
      this->visAides = static_cast<qfeVisualAides *>(this->visuals[i]);
    }
  }
  return true;
}

//----------------------------------------------------------------------------
void qfeDriverMPR::qfeRenderSelect()
{
  qfeVolume    *volume;

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  int                 paramData;
  bool                paramShowX, paramShowY, paramShowZ;

  qfeColorRGB   color;
  
  // Get the visual parameters
  this->visOrtho->qfeGetVisualParameter("Ortho data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  this->visOrtho->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);    

  this->visOrtho->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);    

  this->visOrtho->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);   

  // Process the visual parameters  
  this->qfeGetDataSetOrtho(paramData, this->visOrtho, &volume);    

  if(paramShowX)
  {
    color.r = 1.0; color.g = 0.0; color.b = 0.0;
    this->qfeSetSelectColor(color);
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeX, NULL);
  }

  if(paramShowY)
  {
    color.r = 0.0; color.g = 1.0; color.b = 0.0;
    this->qfeSetSelectColor(color);
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeY, NULL);
  }
  
  if(paramShowZ)
  {
    color.r = 0.0; color.g = 0.0; color.b = 1.0;
    this->qfeSetSelectColor(color);
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeZ, NULL);
  }
}


//----------------------------------------------------------------------------
void qfeDriverMPR::qfeRenderStart()
{
  qfeVolume          *volume = NULL;
  qfeGrid            *grid   = NULL;
  qfePoint            origin;

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  int                 paramData;

  if(this->visOrtho != NULL) 
  {
    this->visOrtho->qfeGetVisualParameter("Ortho data set", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
    paramData = paramDataList.active;

    this->qfeGetDataSetOrtho(paramData, this->visOrtho, &volume);
  }

  if(volume != NULL)
  {
    this->qfeGetPlaneOrtho(volume, qfePlaneSagittal, &this->planeX);  
    this->qfeGetPlaneOrtho(volume, qfePlaneCoronal,  &this->planeY);
    this->qfeGetPlaneOrtho(volume, qfePlaneAxial,    &this->planeZ);
  }
 
}

//----------------------------------------------------------------------------
void qfeDriverMPR::qfeRenderWait()
{
  // Start rendering
  glDisable(GL_LIGHTING);

  // Save the current model-view matrix
  GLfloat modelview[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f::qfeSetMatrixElements(this->currentModelView, modelview);

  // Initialize color map
  qfeColorMap *colormap;
  if(this->visOrtho != NULL) 
  {
    this->visOrtho->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();   
  }
  if(this->visQflow != NULL) 
  {
    this->visQflow->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();   
  }
  if(this->visArrows != NULL)
  {
    this->visArrows->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }

  // Render the reformat
  if(this->visOrtho != NULL)
  {
    this->qfeRenderVisualPlanesOrtho(this->visOrtho);        
  }  
  if(this->visQflow != NULL)
  {
    this->qfeRenderVisualPlanesOblique(this->visQflow);        
  }  
  if(this->visArrows != NULL)
  {
    this->qfeRenderVisualPlanesArrows(this->visArrows);        
  } 

  // Render visual aides
  if(this->visAides != NULL)
  {
    this->qfeRenderVisualAides(this->visAides);
  }
};

//----------------------------------------------------------------------------
void qfeDriverMPR::qfeRenderStop()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMPR::qfeRenderVisualAides(qfeVisualAides *visual)
{
  qfeStudy           *study;
  qfeVolume          *volume;
  int                 current;

  qfeVisualParameter *param;
  bool                paramSupportBoundingBox = true;
  bool                paramSupportAxes        = false;
    
  this->visAides->qfeGetVisualStudy(&study);  
  this->visAides->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  if(study->pcap.size() > 0) 
    volume = study->pcap[current];
  else return qfeError;

  this->visAides->qfeGetVisualParameter("Support bounding box", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportBoundingBox); 

  this->visAides->qfeGetVisualParameter("Support axes", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportAxes); 

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  if(paramSupportBoundingBox == true) qfeDriverAide::qfeRenderBoundingBoxDashed(volume, this->currentModelView, 0.8*this->frameBufferProps[0].supersampling);
  if(paramSupportAxes        == true) qfeDriverAide::qfeRenderVolumeAxes(volume, 10.0, 1.6*this->frameBufferProps[0].supersampling);

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMPR::qfeRenderVisualPlanesOrtho(qfeVisualReformat *visual)
{
  qfeStudy           *study;
  qfeVolume          *volume;
  qfeGrid            *grid;
  qfeColorMap        *colormap;
  qfePoint            origin;

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  qfeSelectionList    paramInterpList;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  int                 paramInterp;
  double              paramBrightness;
  double              paramContrast;
  int                 paramTransparency;
  qfeFloat            paramRange[2];
  bool                paramShowX, paramShowY, paramShowZ;
  int                 current;
  qfeColorRGB         colorX, colorY, colorZ;
  qfeFloat            tt = 0.0f;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  // Get visual parameters
  visual->qfeGetVisualParameter("Ortho data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("Ortho data vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("Ortho data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("Ortho data interpolation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramInterpList);  
  paramInterp = paramInterpList.active;

  visual->qfeGetVisualParameter("Ortho plane RL (X)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);    

  visual->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);    

  visual->qfeGetVisualParameter("Ortho transparency", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTransparency);    

  visual->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);  

  visual->qfeGetVisualParameter("Ortho contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);   

  visual->qfeGetVisualParameter("Ortho brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);   

  // Process parameters
  this->qfeGetDataSetOrtho(paramData, visual, &volume);  

  if(volume != NULL)
  {
    volume->qfeGetVolumeGrid(&grid);
    volume->qfeGetVolumeTriggerTime(tt);
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);    
  }
  else origin.x = origin.y = origin.z = 0.0;

  switch(paramData)
  {
  case 0 : study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
           break;  
  case 1 : study->qfeGetStudyRangePCAM(paramRange[0], paramRange[1]);
           break;
  case 2 : study->qfeGetStudyRangeSSFP(paramRange[0], paramRange[1]);
           // Non-float texture are clamped [0,1]
           //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
           //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
           break;
  case 3:  if(study->tmip != NULL)
           study->tmip->qfeGetVolumeValueDomain(paramRange[0], paramRange[1]);
           // Non-float texture are clamped [0,1]
           //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
           //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
           break;
  case 6:  study->qfeGetStudyRangeFTLE(paramRange[0], paramRange[1]);
           break;
  }
  
  // Determine line color
  colorX = colorY = colorZ = this->colorDefault;
  if(this->selectedPlane == qfePlaneX)  colorX = this->colorHighlight;
  if(this->selectedPlane == qfePlaneY)  colorY = this->colorHighlight;
  if(this->selectedPlane == qfePlaneZ)  colorZ = this->colorHighlight;

  // Update algorithm parameters    
  this->algorithmOrtho->qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
  this->algorithmOrtho->qfeSetPlaneDataComponent(paramComp);
  this->algorithmOrtho->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmOrtho->qfeSetPlaneDataInterpolation((qfePlanarReformatInterpolation)paramInterp);
  this->algorithmOrtho->qfeSetPlaneTriggerTime(tt); 
  this->algorithmOrtho->qfeSetPlaneDataRange(paramRange[0], paramRange[1]);
  this->algorithmOrtho->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmOrtho->qfeSetPlaneContrast(paramContrast);
  this->algorithmOrtho->qfeSetPlaneOpacity(1.0-(paramTransparency/100.0));
 
  // Render the planes
  if(paramShowX)
  {
    this->algorithmOrtho->qfeSetPlaneBorderColor(colorX);
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeX, colormap);      
  }

  if(paramShowY) 
  { 
    this->algorithmOrtho->qfeSetPlaneBorderColor(colorY);
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeY, colormap);      
  }

  if(paramShowZ)
  {  
    this->algorithmOrtho->qfeSetPlaneBorderColor(colorZ);
    this->algorithmOrtho->qfeRenderPolygon3D(volume, this->planeZ, colormap);      
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMPR::qfeRenderVisualPlanesOblique(qfeVisualReformat *visual)
{
  qfeStudy           *study       = NULL;
  qfeVolume          *volumeSlice = NULL;
  qfeVolume          *volumeFlow  = NULL;  
  qfeGrid            *grid        = NULL;
  qfeColorMap        *colormap;
  qfePoint            origin;

  qfeVisualParameter *param;  
  qfeSelectionList    paramDataList1;
  qfeSelectionList    paramDataList2;
  qfeSelectionList    paramDataVector;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  bool                paramVisible;
  bool                paramBorderVisible;
  int                 paramData1;
  int                 paramData2;
  int                 paramRepr;
  int                 paramComponent;
  qfeFloat            paramRange1[2];
  qfeFloat            paramRange2[2];
  int                 paramBalance;
  double              paramBrightness;
  double              paramContrast;
  int                 paramTransparency;
  int                 current;
  qfeColorRGB         colorQ;
  qfeFloat            tt;
  int                 count, phases;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  // Get visual parameters
  visual->qfeGetVisualParameter("Planes visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("Plane data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList1);  
  paramData1 = paramDataList1.active;

  visual->qfeGetVisualParameter("Plane border visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBorderVisible); 

  visual->qfeGetVisualParameter("Plane transparency", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTransparency);  
  
  visual->qfeGetVisualParameter("4D flow data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList2);  
  paramData2 = paramDataList2.active;  

  visual->qfeGetVisualParameter("4D flow visibility (%)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBalance);  

  visual->qfeGetVisualParameter("4D flow vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataVector);  
  paramComponent = paramDataVector.active;

  visual->qfeGetVisualParameter("Data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("Brightness", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramBrightness);  

  visual->qfeGetVisualParameter("Contrast", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramContrast);  

  // Process parameters

  // If there is data for the ortho reader, synchronize the trigger time.
  origin.x = origin.y = origin.z = 0.0;
  tt  = 0.0;
  if(this->visOrtho != NULL)
  {
    this->qfeGetDataSetOrtho(0, this->visOrtho, &volumeSlice);  

    if(volumeSlice != NULL)
    {
      volumeSlice->qfeGetVolumeGrid(&grid);
      volumeSlice->qfeGetVolumeTriggerTime(tt);
      grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);       
    }  
  }

  // Select the right data range
  switch(paramData1) // QFlow / Anatomy data
  {
  case 0 : study->qfeGetStudyRangePCAP(paramRange1[0], paramRange1[1]);          
           break;  
  case 1 : study->qfeGetStudyRangePCAM(paramRange1[0], paramRange1[1]);           
           break;
  case 2 : study->qfeGetStudyRangeFFE(paramRange1[0], paramRange1[1]);           
           // Non-float texture are clamped [0,1]
           //paramRange1[0] = paramRange1[0] / pow(2.0f,16.0f);
           //paramRange1[1] = paramRange1[1] / pow(2.0f,16.0f);
           break;
  case 3 : study->qfeGetStudyRangeSSFP(paramRange1[0], paramRange1[1]);           
           // Non-float texture are clamped [0,1]
           //paramRange1[0] = paramRange1[0] / pow(2.0f,16.0f);
           //paramRange1[1] = paramRange1[1] / pow(2.0f,16.0f);
           break;
  }

  switch(paramData2) // 4D flow data
  {
  case 0 : study->qfeGetStudyRangePCAP(paramRange2[0], paramRange2[1]);
           break;  
  case 1 : study->qfeGetStudyRangePCAM(paramRange2[0], paramRange2[1]);
           break;
  case 2 : study->qfeGetStudyRangeFFE(paramRange2[0], paramRange2[1]);
           // Non-float texture are clamped [0,1]
           //paramRange2[0] = paramRange2[0] / pow(2.0f,16.0f);
           //paramRange2[1] = paramRange2[1] / pow(2.0f,16.0f);
           break;
  case 3 : if(study->tmip != NULL)
           { 
             study->tmip->qfeGetVolumeValueDomain(paramRange2[0], paramRange2[1]);
             // Non-float texture are clamped [0,1]
             //paramRange2[0] = paramRange2[0] / pow(2.0f,16.0f);
             //paramRange2[1] = paramRange2[1] / pow(2.0f,16.0f);        
           }
           else
           {
             paramRange2[0] = 0.0;
             paramRange2[1] = 1.0;        
           }
           break;
  case 4 : study->qfeGetStudyRangeFTLE(paramRange2[0], paramRange2[1]);
    break;  
  }
  
  // Determine line color
  colorQ.r = 0.55;
  colorQ.g = 0.55;
  colorQ.b = 0.55;

  // Update algorithm parameters  
  this->algorithmOblique->qfeSetPlaneBorderWidth(3.5*this->frameBufferProps[0].supersampling);
  this->algorithmOblique->qfeSetPlaneDataComponent(paramComponent);
  this->algorithmOblique->qfeSetPlaneDataRepresentation(paramRepr);
  this->algorithmOblique->qfeSetPlaneTriggerTime(tt);  
  this->algorithmOblique->qfeSetPlaneBrightness(paramBrightness);
  this->algorithmOblique->qfeSetPlaneContrast(paramContrast);
  this->algorithmOblique->qfeSetPlaneBalance(paramBalance);
  this->algorithmOblique->qfeSetPlaneOpacity(1.0-(paramTransparency/100.0));
  this->algorithmOblique->qfeSetPlaneContour(paramBorderVisible);
 
  if(paramVisible)
  {    
    count = 0;
    phases = 0;
    this->qfeGetDataCountOblique(paramData1, visual, count);
    this->qfeGetDataPhasesOblique(paramData1, visual, phases);

    // render the 2D slices
    for(int i=0; i<(int)count;i++)
    {      
      this->qfeGetDataSetOrtho(paramData2, visual, &volumeFlow);
      this->qfeGetDataSetOblique(paramData1, i, visual, &volumeSlice);  

      if(volumeFlow == NULL || volumeSlice == NULL) continue;

      this->qfeGetPlaneOblique(volumeSlice, &this->planeQ);   
      
      this->algorithmOblique->qfeSetPlaneDataRange(paramRange1[0], paramRange1[1]);
      this->algorithmOblique->qfeSetPlaneDataRange2(paramRange2[0], paramRange2[1]);
      this->algorithmOblique->qfeSetPlaneBorderColor(colorQ);
      this->algorithmOblique->qfeRenderPlane3D(volumeSlice, volumeFlow, phases, this->planeQ, colormap);  
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMPR::qfeRenderVisualPlanesArrows(qfeVisualReformat *visual)
{
  qfeStudy           *study;
  qfeVolume          *volume;
  qfeColorMap        *colormap;
  qfePoint            origin;

  qfeVisualParameter *param;  
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  int                 paramComp;
  int                 paramRepr;
  double              paramSpacing;
  double              paramScaling;
  bool                paramVisible, paramShowX, paramShowY, paramShowZ;
  int                 current;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);
  
  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  // Get visual parameters
  visual->qfeGetVisualParameter("Arrows visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);    

  visual->qfeGetVisualParameter("Arrows data vector", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("Arrows data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active; 

  visual->qfeGetVisualParameter("Arrows spacing (mm)", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSpacing); 

  visual->qfeGetVisualParameter("Arrows scaling factor", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramScaling); 

  // Process parameters
  if(this->visOrtho != NULL)
  {
    this->qfeGetDataSetOrtho(0, visOrtho, &volume);  

    visOrtho->qfeGetVisualParameter("Ortho plane RL (X)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowX);    

    visOrtho->qfeGetVisualParameter("Ortho plane AP (Y)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowY);    

    visOrtho->qfeGetVisualParameter("Ortho plane FH (Z)", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramShowZ);  
  }
  else
  {
    qfeStudy *study;
    int       current;
    visual->qfeGetVisualActiveVolume(current);
    visual->qfeGetVisualStudy(&study);

    if(study != NULL)
    {
      if((int)study->pcap.size() > current)
        volume = study->pcap[current];
      else 
        return qfeError;
    }
    paramShowX = paramShowY = paramShowZ = true;
  }
 
  // Render the planes
  if(paramVisible)
  {       
    if(paramShowX) this->algorithmOrtho->qfeRenderArrowHeads3D(volume, this->planeX, paramSpacing, paramScaling, colormap, 0);  
    if(paramShowY) this->algorithmOrtho->qfeRenderArrowHeads3D(volume, this->planeY, paramSpacing, paramScaling, colormap, 0);  
    if(paramShowZ) this->algorithmOrtho->qfeRenderArrowHeads3D(volume, this->planeZ, paramSpacing, paramScaling, colormap, 0);  
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeDriverMPR::qfeMovePlaneForward(qfeFrameSlice **slice)
{ 
  qfePoint     origin;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;  

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin + (1.0*normal);
  else
    origin = origin - (1.0*normal);

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

//----------------------------------------------------------------------------
void qfeDriverMPR::qfeMovePlaneForward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if(volume == NULL) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin + (mm*normal);
  else
    origin = origin - (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = std::max(originTex.x, 0.0f+delta);
  originTex.x = std::min(originTex.x, 1.0f-delta);
  originTex.y = std::max(originTex.y, 0.0f+delta);
  originTex.y = std::min(originTex.y, 1.0f-delta);
  originTex.z = std::max(originTex.z, 0.0f+delta);
  originTex.z = std::min(originTex.z, 1.0f-delta);

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

//----------------------------------------------------------------------------
void qfeDriverMPR::qfeMovePlaneBackward(qfeFrameSlice **slice)
{
  qfePoint     origin;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;  

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin - (1.0*normal);
  else
    origin = origin + (1.0*normal);

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}

//----------------------------------------------------------------------------
void qfeDriverMPR::qfeMovePlaneBackward(qfeVolume *volume, qfeFrameSlice **slice, int mm)
{
  qfePoint     origin, originTex;
  qfeVector    normal, n, v;
  qfeFloat     dummy, ndotv;
  qfeMatrix4f  P2V, V2T, T2P;
  qfeFloat     delta;

  if(volume == NULL) return;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V*V2T, T2P);

  (*slice)->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  (*slice)->qfeGetFrameAxes(dummy, dummy, dummy, dummy, dummy, dummy, normal.x, normal.y, normal.z );

  // eye vector in world space
  v.x =  0.0;
  v.y =  0.0;
  v.z = -1.0;

  // normal vector in world space
  n = normal*currentModelView;

  ndotv = n * v;

  if(ndotv >= 0)
    origin = origin - (mm*normal);
  else
    origin = origin + (mm*normal);

  // check if the plane is still within the volume
  originTex = origin * (P2V*V2T);

  delta       = 0.005f;
  originTex.x = std::max(originTex.x, 0.0f+delta);
  originTex.x = std::min(originTex.x, 1.0f-delta);
  originTex.y = std::max(originTex.y, 0.0f+delta);
  originTex.y = std::min(originTex.y, 1.0f-delta);
  originTex.z = std::max(originTex.z, 0.0f+delta);
  originTex.z = std::min(originTex.z, 1.0f-delta);

  origin = originTex * T2P;

  (*slice)->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMPR::qfeGetDataSetOrtho(int index, qfeVisual *visual, qfeVolume **volume)
{
  qfeStudy           *study;  
  int                 current;

  *volume = NULL;
  visual->qfeGetVisualStudy(&study); 
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(0) : // PCA-P
            if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > current)) 
              *volume = study->pcap[current];
            break;
  case(1) : // PCA-M
            if(((int)study->pcam.size() > 0) && ((int)study->pcam.size() > current)) 
              *volume = study->pcam[current];
            break;
  case(2) : // SSFP
            if(((int)study->ssfp3D.size() > 0) && ((int)study->ssfp3D.size() > current)) 
              *volume = study->ssfp3D[current];
            break;
  case(3) : // TMIP
            if((int)study->tmip != NULL) 
              *volume = study->tmip;
            break;
  case(4) : // FFE
            break;
  case(5) : // Clusters
            break;
  case(6) : // ftle
            if(((int)study->ftle.size() > 0) && ((int)study->ftle.size() > current)) 
              *volume = study->ftle[current];
            break;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMPR::qfeGetDataSetOblique(int index, int slice, qfeVisual *visual, qfeVolume **volume)
{
  qfeStudy           *study;  
  int                 current;

  *volume = NULL;
  visual->qfeGetVisualStudy(&study); 
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(0) : // PCA-P
            if(((int)study->flow2D_pcap.size() > 0) && ((int)study->flow2D_pcap.size() >= slice)) 
              *volume = study->flow2D_pcap[slice];
            break;
  case(1) : // PCA-M
            if(((int)study->flow2D_pcam.size() > 0) && ((int)study->flow2D_pcam.size() >= slice)) 
              *volume = study->flow2D_pcam[slice];
            break;
  case(2) : // FFE
            if(((int)study->flow2D_ffe.size() > 0) && ((int)study->flow2D_ffe.size() >= slice)) 
              *volume = study->flow2D_ffe[slice];
            break;
  case(3) : // SSFP
            if(((int)study->ssfp2D.size() > 0) && ((int)study->ssfp2D.size() >= slice)) 
              *volume = study->ssfp2D[slice];
            break;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMPR::qfeGetDataCountOblique(int index, qfeVisual *visual, int &count)
{
  qfeStudy           *study;  
  unsigned int        c;

  visual->qfeGetVisualStudy(&study); 

  c = 0;

  switch(index)
  {
  case(0) : // PCA-P
            if((int)study->flow2D_pcap.size() > 0)  c = study->flow2D_pcap.size();
            break;
  case(1) : // PCA-M
            if((int)study->flow2D_pcam.size() > 0)  c = study->flow2D_pcam.size();
            break;
  case(2) : // FFE
            if((int)study->flow2D_ffe.size() > 0)   c = study->flow2D_ffe.size();
            break;
  case(3) : // SSFP
            if((int)study->ssfp2D.size() > 0)       c = study->ssfp2D.size();
            break;
  }

  count = (int) c;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMPR::qfeGetDataPhasesOblique(int index, qfeVisual *visual, int &phases)
{
  qfeStudy           *study;  
  unsigned int        x, y, p;

  visual->qfeGetVisualStudy(&study); 

  p = 0;

  switch(index)
  {
  case(0) : // PCA-P
            if((int)study->flow2D_pcap.size() > 0)  phases = study->flow2D_pcap.front()->qfeGetVolumeSize(x,y,p);
            break;
  case(1) : // PCA-M
            if((int)study->flow2D_pcam.size() > 0)  phases = study->flow2D_pcam.front()->qfeGetVolumeSize(x,y,p);
            break;
  case(2) : // FFE
            if((int)study->flow2D_ffe.size() > 0)   phases = study->flow2D_ffe.front()->qfeGetVolumeSize(x,y,p);
            break;
  case(3) : // SSFP
            if((int)study->ssfp2D.size() > 0)       phases = study->ssfp2D.front()->qfeGetVolumeSize(x,y,p);
            break;
  }

  phases = (int) p;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMPR::qfeGetPlaneOrtho(qfeVolume *volume, qfePlaneDirection dir, qfeFrameSlice **slice)
{
  qfeMatrix4f P2V, V2P;
  qfeVector   axisX, axisY, axisZ, axisXabs, axisYabs, axisZabs, x, y, z;
  qfePoint    o;
  qfeGrid    *grid;

  if(volume == NULL) return qfeError;

  // Get the patient to voxel transformation
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);

  // Set the plane normal direction in patient coordinates
  switch(dir)
  {
  case qfePlaneAxial       : axisX.qfeSetVectorElements(1.0,0.0,0.0);
                             axisY.qfeSetVectorElements(0.0,1.0,0.0);
                             axisZ.qfeSetVectorElements(0.0,0.0,1.0);                              
                             break;
  case qfePlaneSagittal    : axisX.qfeSetVectorElements(0.0,0.0,1.0);
                             axisY.qfeSetVectorElements(0.0,1.0,0.0);
                             axisZ.qfeSetVectorElements(1.0,0.0,0.0); 
                             break;
  case qfePlaneCoronal     : axisX.qfeSetVectorElements(1.0,0.0,0.0);
                             axisY.qfeSetVectorElements(0.0,0.0,1.0);
                             axisZ.qfeSetVectorElements(0.0,1.0,0.0); 
                             break;
  }  

  // Convert the axis to voxel coordinates and find dominant direction
  axisX = axisX*P2V;
  axisY = axisY*P2V;
  axisZ = axisZ*P2V;

  axisXabs.qfeSetVectorElements(abs(axisX.x), abs(axisX.y), abs(axisX.z));
  axisYabs.qfeSetVectorElements(abs(axisY.x), abs(axisY.y), abs(axisY.z));
  axisZabs.qfeSetVectorElements(abs(axisZ.x), abs(axisZ.y), abs(axisZ.z));

  if(axisZabs.x == max(max(axisZabs.x, axisYabs.x), axisXabs.x))
  {
    x.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
    y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
    z.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
  }
  if(axisZabs.y == max(max(axisZabs.y, axisYabs.y), axisXabs.y))
  {
    x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
    y.qfeSetVectorElements( 0.0f,  0.0f, -1.0f);
    z.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
  }
  if(axisZabs.z == max(max(axisZabs.z, axisYabs.z), axisXabs.z))
  {
    x.qfeSetVectorElements( 1.0f,  0.0f,  0.0f);
    y.qfeSetVectorElements( 0.0f,  1.0f,  0.0f);
    z.qfeSetVectorElements( 0.0f,  0.0f,  1.0f);    
  }

  // Convert back to patient coordinates
  x = x*V2P;
  y = y*V2P;
  z = z*V2P;

  // Get the plane origin
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridOrigin(o.x, o.y, o.z);

  // Build the plane
  (*slice)->qfeSetFrameExtent(1,1);
  (*slice)->qfeSetFrameOrigin(o.x, o.y, o.z);
  (*slice)->qfeSetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverMPR::qfeGetPlaneOblique(qfeVolume *volume, qfeFrameSlice **slice)
{
  qfeGrid * grid;

  if(volume == NULL) return qfeError;

  unsigned int s[3];
  qfePoint  o, e;
  qfeVector x, y, z;    

  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(s[0], s[1], s[2]);
  grid->qfeGetGridOrigin(o.x,o.y,o.z);
  grid->qfeGetGridAxes(x.x,x.y,x.z, y.x,y.y,y.z, z.x,z.y,z.z);
  grid->qfeGetGridExtent(e.x, e.y, e.z);

  (*slice)->qfeSetFrameOrigin(o.x,o.y,o.z);
  (*slice)->qfeSetFrameAxes(x.x,x.y,x.z, y.x,y.y,y.z, z.x,z.y,z.z);
  (*slice)->qfeSetFrameExtent(s[0]*e.x, s[1]*e.y);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeDriverMPR::qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v)
{
  qfeDriverMPR *self = reinterpret_cast<qfeDriverMPR *>(caller);

  qfeColorRGB color;
  float       depth;

  self->qfeReadSelect(x, y, color);
  self->qfeReadDepth(x, y, depth);

  self->selectedPlane = qfePlaneNone;

  if(color.r == 1.0 && color.g == 0.0 && color.b==0.0)
    self->selectedPlane = qfePlaneX;

  if(color.r == 0.0 && color.g == 1.0 && color.b==0.0)
    self->selectedPlane = qfePlaneY;

  if(color.r == 0.0 && color.g == 0.0 && color.b==1.0)
    self->selectedPlane = qfePlaneZ;
}

//----------------------------------------------------------------------------
void qfeDriverMPR::qfeOnWheelForward(void *caller, int x, int y, int v)
{
  qfeDriverMPR *self = reinterpret_cast<qfeDriverMPR *>(caller);
  
  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if(self->visOrtho == NULL) return;

  // Obtain the volume
  self->visOrtho->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visOrtho->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);

  if((int)study->pcap.size() <= 0) return;
  
  if(self->selectedPlane == qfePlaneX) self->qfeMovePlaneForward(study->pcap.front(), &self->planeX, scrollStep);
  if(self->selectedPlane == qfePlaneY) self->qfeMovePlaneForward(study->pcap.front(), &self->planeY, scrollStep);
  if(self->selectedPlane == qfePlaneZ) self->qfeMovePlaneForward(study->pcap.front(), &self->planeZ, scrollStep);
  
  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverMPR::qfeOnWheelBackward(void *caller, int x, int y, int v)
{
  qfeDriverMPR *self = reinterpret_cast<qfeDriverMPR *>(caller);
  
  qfeStudy           *study;
  qfeVisualParameter *p;
  int                 scrollStep = 1;

  if(self->visOrtho == NULL) return;

  // Obtain the volume
  self->visOrtho->qfeGetVisualStudy(&study);
  if(study->pcap.size() < 0) return;

  // Obtain the scroll step size in mm
  self->visOrtho->qfeGetVisualParameter("Ortho scroll step (mm)", &p);
  if(p != NULL) p->qfeGetVisualParameterValue(scrollStep);

  if((int)study->pcap.size() <= 0) return;
  
  if(self->selectedPlane == qfePlaneX) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeX, scrollStep);
  if(self->selectedPlane == qfePlaneY) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeY, scrollStep);
  if(self->selectedPlane == qfePlaneZ) self->qfeMovePlaneBackward(study->pcap.front(), &self->planeZ, scrollStep);
  
  self->engine->Refresh();
}

