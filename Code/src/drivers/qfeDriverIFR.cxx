#include "qfeDriverIFR.h"

//----------------------------------------------------------------------------
qfeDriverIFR::qfeDriverIFR(qfeScene *scene) : qfeDriver(scene)
{
  this->visAnatomyIllustrative    = NULL;
  this->visMIP                    = NULL;
  this->visFlowProbe              = NULL;
  this->visFlowLines              = NULL;
  this->visFlowPlanes             = NULL;
  this->visFlowArrowTrails        = NULL;
  this->visAides                  = NULL;

  this->probeTraceStep            = -1;
  this->probeTraceIterations      = -1;
  this->probeSeedsStrategy        = -1;
  this->probeSeedsTemplate        = -1;
  this->probeSeedsDensity         = -1;
  this->probeCountChanged         =  false;
  this->probeArrowMaxPerc         =  0;
  this->probeClickState           =  probeSelectStart;

  this->paramTraceCounter         =  0;
  this->paramSeedDensity          = 1.0;
  this->paramSeedStrategy         =  0;
  this->paramSeedTransferFunction =  0;

  // Create a list of default probe colors
  qfeColorRGB col;
  this->probeColor.clear();

  col.r = 1.0; col.g = 0.0; col.b = 0.0;
  this->probeColor.push_back(col);
  col.r = 0.0; col.g = 1.0; col.b = 0.0;
  this->probeColor.push_back(col);
  col.r = 0.0; col.g = 0.0; col.b = 1.0;
  this->probeColor.push_back(col);

  // Set up the interactor
  this->mouseDrag = false; 

  this->engine->SetObserverTimer((void *)this, this->qfeOnTimer);
  this->engine->SetObserverResize((void*)this, this->qfeOnResize);
  this->engine->SetObserverAction((void*)this, this->qfeOnAction);
  this->engine->SetInteractionMouseLeftButtonDown((void *)this, this->qfeOnMouseLeftButtonDown, false);
  this->engine->SetInteractionMouseLeftButtonUp((void *)this, this->qfeOnMouseLeftButtonUp);
  this->engine->SetInteractionMouseMove((void *)this, this->qfeOnMouseMove);
  this->engine->SetInteractionKeyPress((void*)this, this->qfeOnKeyPress, true);

  // Construct algorithms 
  this->algorithmSurfaceShading  = new qfeSurfaceShading();
  this->algorithmContours        = new qfeSurfaceContours();

  this->algorithmProbe2D         = new qfeFlowProbe2D();
  this->algorithmFlowArrows      = new qfeFlowArrows();
  this->algorithmFlowLines       = new qfeFlowLines();
  this->algorithmFlowPlanes      = new qfeFlowPlanes();

  this->algorithmMIP             = new qfeMaximumProjection();
};

//----------------------------------------------------------------------------
qfeDriverIFR::~qfeDriverIFR()
{
  // Destruct algorithms
  delete this->algorithmContours;
  delete this->algorithmSurfaceShading;
  delete this->algorithmFlowArrows;
  delete this->algorithmFlowLines;
  delete this->algorithmFlowPlanes;
  delete this->algorithmMIP;

  // Stop the rendering
  this->qfeRenderStop();
};

//----------------------------------------------------------------------------
bool qfeDriverIFR::qfeRenderCheck()
{
  int t;
  for(int i=0; i<(int)this->visuals.size(); i++)
  {
    this->visuals[i]->qfeGetVisualType(t);
    if(t == visualMaximum)
    {
      this->visMIP                 = static_cast<qfeVisualMaximum *>(this->visuals[i]);      
    }
    if(t == visualAnatomyIllustrative)
    {
      this->visAnatomyIllustrative = static_cast<qfeVisualAnatomyIllustrative *>(this->visuals[i]);
    }
    if(t == visualFlowProbe2D)
    {
      this->visFlowProbe           = static_cast<qfeVisualFlowProbe2D *>(this->visuals[i]);
    }
    if(t == visualFlowLinesDepr)
    {
      this->visFlowLines           = static_cast<qfeVisualFlowLines *>(this->visuals[i]);
    }
    if(t == visualFlowPlanes)
    {      
      this->visFlowPlanes          = static_cast<qfeVisualFlowPlanes *>(this->visuals[i]);
    }
    if(t == visualFlowTrails)
    {
      this->visFlowArrowTrails     = static_cast<qfeVisualFlowArrowTrails *>(this->visuals[i]); 
    }
    if(t == visualAides)
    {
      this->visAides               = static_cast<qfeVisualAides *>(this->visuals[i]);
    }
  }

  return true;
}

//----------------------------------------------------------------------------
void qfeDriverIFR::qfeRenderStart()
{
  qfeStudy *study;

  // Initialize MIP
  if(this->visMIP != NULL)
  {
    qfeVisualParameter *param;
    qfeSelectionList    paramDataList;

    this->visMIP->qfeGetVisualParameter("MIP data set", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
    paramDataList.active = 3;
    param->qfeSetVisualParameterValue(paramDataList);

    this->qfeOnResize(this);
  }

  // Initialize probe 
  if(this->visFlowProbe != NULL)
  {
    this->visFlowProbe->qfeGetVisualStudy(&study);

    this->probeInteractionState.clear(); 

    for(int i=0; i<(int)study->probe2D.size(); i++)
    {
      this->probeInteractionState.push_back(probeInteractNone);      
    }
  }

}

//----------------------------------------------------------------------------
void qfeDriverIFR::qfeRenderSelect()
{
  qfeStudy    *study;
  qfeColorRGB  color;

  // Initialize OpenGL state
  glDisable(GL_DITHER);
  glDisable(GL_BLEND);
  glDisable(GL_NORMALIZE);
  glDisable(GL_LIGHTING);
  glDisable(GL_COLOR_MATERIAL);
  glEnable(GL_DEPTH_TEST);

  // Render silhouette
  if(this->visAnatomyIllustrative != NULL)
  {
    this->qfeRenderVisualAnatomyIllustrative(this->visAnatomyIllustrative);
  }

  if(this->visFlowProbe != NULL)
  {
    qfeVisualParameter *param;
    bool                paramVisible;

    this->visFlowProbe->qfeGetVisualStudy(&study);   

    // Get parameters
    this->visFlowProbe->qfeGetVisualParameter("Probe visible", &param);
    if(param != NULL) param->qfeGetVisualParameterValue(paramVisible); 

    if(paramVisible)
    {
      // Render all probes
      for(int i=0; i<(int)study->probe2D.size(); i++)
      {
        // Start rendering
        if(study->probe2D[i].visible) 
        {      
          color.r = color.g = i / 255.0f;
          color.b = SELECT_PROBE;
          this->qfeSetSelectColor(color);

          this->algorithmProbe2D->qfeRenderProbePlane(study->probe2D[i]); 
        }
      } 
    }   
  }
}

//----------------------------------------------------------------------------
void qfeDriverIFR::qfeRenderWait()
{
  // Initialize OpenGL state
  glDisable(GL_DITHER);
  glDisable(GL_BLEND);
  glDisable(GL_NORMALIZE);
  glDisable(GL_LIGHTING);
  glDisable(GL_COLOR_MATERIAL);
  glEnable(GL_DEPTH_TEST);

  // Save the current model-view matrix
  qfeColorMap *colormap;
  if(this->visMIP != NULL)
  {
    this->visMIP->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
  }
  if(this->visFlowLines != NULL) 
  {
    this->visFlowLines->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();   
  }  

  // Render silhouette
  if(this->visAnatomyIllustrative != NULL)
  {
    this->qfeRenderVisualAnatomyIllustrative(this->visAnatomyIllustrative);
  }

  // Render MIP
  if(this->visMIP != NULL)
  {
    this->qfeRenderVisualMaximumProjection(this->visMIP);
  }

  // Render flow probe
  if(this->visFlowProbe != NULL)
  {
    this->qfeRenderVisualFlowProbe(this->visFlowProbe);
  }

  // Render flow lines
  if(this->visFlowLines != NULL)
  {
    this->qfeRenderVisualFlowLines(this->visFlowLines);
  }

   // Render flow planes
  if(this->visFlowPlanes != NULL)
  {    
    this->qfeRenderVisualFlowPlanes(this->visFlowPlanes);
  }

  // Render flow arrow trails
  if(this->visFlowArrowTrails != NULL)
  {
    this->qfeRenderVisualFlowArrowTrails(this->visFlowArrowTrails);
  }

  // Render visual aides
  if(this->visAides != NULL)
  {
    this->qfeRenderVisualAides(this->visAides);
  }

  this->probeCountChanged = false;
};

//----------------------------------------------------------------------------
void qfeDriverIFR::qfeRenderStop()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeRenderVisualAnatomyIllustrative(qfeVisualAnatomyIllustrative *visual)
{ 
  qfeStudy              *study;
  qfeVolume             *volume;
  qfeLightSource        *light;
  qfeModel              *model;  
  vtkPolyData           *mesh; 
  qfeMeshVertices       *vertices   = NULL;
  qfeMeshVertices       *normals    = NULL;
  qfeMeshIndices        *indicesTriangles         = NULL;
  qfeMeshStripsIndices  *indicesTriangleStrips    = NULL;
  qfeMeshStripsIndices  *indicesTriangleStripsAdj = NULL;
  int                    current;
  
  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);

  this->scene->qfeGetSceneLightSource(&light);

  volume = study->pcap[current];

  if(volume == NULL)
  {
    cout << "qfeDriverIFR::qfeRenderVisualAnatomyIllustrative - Incorrect volume data" << endl;
    return qfeError;
  }

  if(study == NULL)
  {
    cout << "qfeDriverIFR::qfeRenderVisualAnatomyIllustrative - Incorrect study" << endl;
    return qfeError;
  }

  // Get the parameters
  qfeSelectionList    paramSurfaceShadingList;
  bool                paramSurfaceClipping;
  bool                paramSurfaceContour;

  qfeVisualParameter *param;  

  visual->qfeGetVisualParameter("Surface shading", &param);    
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceShadingList); 

  visual->qfeGetVisualParameter("Surface clipping", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceClipping);

  visual->qfeGetVisualParameter("Surface contour", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSurfaceContour); 
   
  for(int i=0; i<(int)study->segmentation.size(); i++)
  {
    bool                visible;
    int                 surfaceShading;
    qfeColorRGB         colorSilhouette;
    qfeColorRGBA        colorContour;
    qfeColorRGBA        colorContourHidden;
    
    // Get the model    
    model = study->segmentation[i];
    
    // Get the parameters
    model->qfeGetModelColor(colorSilhouette);        

    // Process the parameter structures   
    colorContour.r       = 0.3; 
    colorContour.g       = 0.3;
    colorContour.b       = 0.3;
    colorContourHidden.r = 0.7;
    colorContourHidden.g = 0.7;
    colorContourHidden.b = 0.7;

    surfaceShading       = paramSurfaceShadingList.active; 

    // Get the mesh    
    model->qfeGetModelMesh(&mesh);    
    model->qfeGetModelMesh(&vertices, &normals);
    model->qfeGetModelMeshIndicesTriangles(&indicesTriangles);
    model->qfeGetModelMeshIndicesTriangleStrips(&indicesTriangleStrips);
    model->qfeGetModelMeshIndicesTriangleStripsAdj(&indicesTriangleStripsAdj);
    model->qfeGetModelVisible(visible);

    //cout << indicesTriangleStrips->size() << endl;
    //cout << indicesTriangleStripsAdj->size() << endl;
/*
    // Draw the mesh
    if(vertices != NULL && visible)
    {
      glDisable(GL_DEPTH_TEST);
      
      if(paramSurfaceContour) 
      {
        this->algorithmSurfaceShading->qfeRenderMeshDilate(vertices, normals, indicesTriangles, 1.0, volume);
      }      

      glEnable(GL_DEPTH_TEST);

      // Update the surface parameters
      this->algorithmSurfaceShading->qfeSetLightSource(light);
      this->algorithmSurfaceShading->qfeSetSurfaceColor(colorSilhouette);
      this->algorithmSurfaceShading->qfeSetSurfaceCulling(paramSurfaceClipping);
      this->algorithmSurfaceShading->qfeSetSurfaceShading((qfeSurfaceShadingType)surfaceShading);

      // Render the mesh
      //this->algorithmSurfaceShading->qfeRenderMesh(mesh, volume, colorSilhouette, paramSurfaceClipping, surfaceShading);
      //this->algorithmSurfaceShading->qfeRenderMesh(mesh, volume);      
      this->algorithmSurfaceShading->qfeRenderMesh(vertices, normals, indicesTriangles, volume);            
      //this->algorithmSurfaceShading->qfeRenderDebug(mesh, volume);
      //this->algorithmSurfaceShading->qfeRenderDebug(vertices, normals, indices, volume);

      
      //if(paramSurfaceContour) 
      //{
      //  if(mouseDrag)                 
      //    this->algorithmContours->qfeRenderContourHidden(mesh, volume, colorContourHidden, 1.5*this->frameBufferProps[0].supersampling);
     
      //  this->algorithmContours->qfeRenderContour(mesh, volume, colorContour, 2.5*this->frameBufferProps[0].supersampling);
      //}
      
    }
  */

    // Draw the mesh
    if(vertices != NULL && visible)
    {
      // Update the surface parameters
      this->algorithmSurfaceShading->qfeSetLightSource(light);
      this->algorithmSurfaceShading->qfeSetSurfaceColor(colorSilhouette);
      this->algorithmSurfaceShading->qfeSetSurfaceCulling(paramSurfaceClipping);
      this->algorithmSurfaceShading->qfeSetSurfaceShading((qfeSurfaceShadingType)surfaceShading);

      // Render the mesh
      this->algorithmSurfaceShading->qfeRenderMesh(vertices, normals, indicesTriangles, volume);            

      if(paramSurfaceContour) 
      {
        this->algorithmSurfaceShading->qfeRenderMeshDilate(vertices, normals, indicesTriangles, 1.0, volume);
      }  
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeRenderVisualMaximumProjection(qfeVisualMaximum *visual)
{
  qfeStudy    *study;
  qfeVolume   *volume;
  qfeColorMap *colormap;
  int          current;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramReprList;
  bool                paramVisible;
  bool                paramGradient;
  bool                paramTransparency;
  int                 paramData;
  int                 paramComp;
  int                 paramRepr;
  double              paramC;
  double              paramB;
  qfeFloat            paramRange[2];
  int                 paramMode;

  if(visual == NULL) return qfeError;
  
  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  if(this->visAnatomyIllustrative != NULL)
  {
    // If there is a segmentation, and it's visible, don't use the MIP
    for(int i=0; i<(int)study->segmentation.size(); i++)
    {
      bool visible;

      study->segmentation[i]->qfeGetModelVisible(visible);

      if(visible) return qfeSuccess;
    }
  }

  // Fetch the parameters
  visual->qfeGetVisualParameter("MIP visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);  

  visual->qfeGetVisualParameter("MIP data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);  
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("MIP data component", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);  
  paramComp = paramCompList.active;

  visual->qfeGetVisualParameter("MIP data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);  
  paramRepr = paramReprList.active;

  visual->qfeGetVisualParameter("MIP gradient", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramGradient);    

  visual->qfeGetVisualParameter("MIP transparency", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTransparency);    

  visual->qfeGetVisualParameter("MIP brightness", &param);
  param->qfeGetVisualParameterValue(paramB);

  visual->qfeGetVisualParameter("MIP contrast",   &param);
  param->qfeGetVisualParameterValue(paramC);

  if(paramVisible != true) return qfeError;  

  // Get the data set and compute the data range
  this->qfeGetDataSet(paramData, visual, current, &volume);

  switch(paramData)
  {
  case 0 : study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
           break;
  case 1 : study->qfeGetStudyRangePCAM(paramRange[0], paramRange[1]);
           break;
  case 2 : study->qfeGetStudyRangeSSFP(paramRange[0], paramRange[1]);
           break;
  case 3 : if(study->tmip != NULL)
           study->tmip->qfeGetVolumeValueDomain(paramRange[0], paramRange[1]);
           // Non-float texture are clamped [0,1]
           //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
           //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
           break;
  case 4 : study->qfeGetStudyRangeFFE(paramRange[0], paramRange[1]);
           // Non-float texture are clamped [0,1]
           //paramRange[0] = paramRange[0] / pow(2.0f,16.0f);
           //paramRange[1] = paramRange[1] / pow(2.0f,16.0f);
           break;
  default: study->qfeGetStudyRangePCAP(paramRange[0], paramRange[1]);
           break;  
  }  

  if(volume == NULL) return qfeError;  

  volume->qfeGetVolumeType(paramMode);

  // Update algorithm parameters  
  this->algorithmMIP->qfeSetDataComponent(paramComp);
  this->algorithmMIP->qfeSetDataRepresentation(paramRepr);
  this->algorithmMIP->qfeSetDataRange(paramRange[0], paramRange[1]);
  this->algorithmMIP->qfeSetGradient(paramGradient);
  this->algorithmMIP->qfeSetTransparency(paramTransparency);
  this->algorithmMIP->qfeSetContrast(paramC);
  this->algorithmMIP->qfeSetBrightness(paramB);

  this->algorithmMIP->qfeRenderMaximumIntensityProjection(this->frameBufferProps[0].texColor, volume, colormap);

  //glClearDepth(1.0);
  glClear(GL_DEPTH_BUFFER_BIT);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeRenderVisualFlowProbe(qfeVisualFlowProbe2D *visual)
{
  qfeStudy           *study;
  qfeColorMap        *colormap;
  qfeColorRGBA        colorHighlight, colorNormal;
  qfeLightSource     *light;
  float               phase;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualColorMap(&colormap);  
  visual->qfeGetVisualActiveVolume(phase);

  this->scene->qfeGetSceneLightSource(&light);

  if(study == NULL) return qfeError;

  colorHighlight.r = 0.356;
  colorHighlight.g = 0.623;
  colorHighlight.b = 0.396;
  colorHighlight.a = 1.0;

  colorNormal.r = colorNormal.g = colorNormal.b = 0.3;
  colorNormal.a = 1.0;

  // Get all global parameters
  qfeVisualParameter *param;
  bool                paramVisible; 
  
  qfeSelectionList    paramSeedStrategy;
  double              paramSeedDensity     = 1.0;
  qfeSelectionList    paramSeedTemplate;

  visual->qfeGetVisualParameter("Probe visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);

  //glPointSize(8.0);
  //glBegin(GL_POINTS);
  //  glColor3f(1.0,0.0,0.0);
  //  for(int i=0; i<(int)this->tempPoint.size(); i++)
  //  {
  //    glVertex3f(this->tempPoint[i].x,this->tempPoint[i].y,this->tempPoint[i].z);
  //  }
  //glEnd();

  if(paramVisible == false) return qfeSuccess;

  // Render all probes
  for(int i=0; i<(int)study->probe2D.size(); i++)
  {
    // Start rendering
    if(study->probe2D[i].visible) 
    {         
      if(study->probe2D[i].selected)
        this->algorithmProbe2D->qfeSetProbeColor(colorHighlight);
      else
        this->algorithmProbe2D->qfeSetProbeColor(colorNormal);

      this->algorithmProbe2D->qfeRenderProbe(study->probe2D[i]); 
    }
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeRenderVisualFlowLines(qfeVisualFlowLines *visual)
{
  qfeStudy           *study;
  qfeColorMap        *colormap;
  qfeLightSource     *light;
  float               phase;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualColorMap(&colormap);  
  visual->qfeGetVisualActiveVolume(phase);

  this->scene->qfeGetSceneLightSource(&light);

  // Get all global parameters
  qfeVisualParameter *param;
  qfeSelectionList    paramLinesList;
  qfeSelectionList    paramLinesColorList;
  qfeSelectionList    paramLinesShadingList;
  qfeSelectionList    paramLinesTraceDirList;
  int                 paramLineShading;
  int                 paramLineColor;
  int                 paramTraceDirection;
  double              paramTraceStep;
  int                 paramTraceIterations;
  
  qfeSelectionList    paramSeedStrategyList;
  qfeSelectionList    paramSeedTransferFunctionList;
  

  paramTraceStep                  = 0.1;
  paramTraceIterations            = 20;
  this->paramLineHighlightVisible = false;

  visual->qfeGetVisualParameter("Line type", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLinesList);

  visual->qfeGetVisualParameter("Line seed strategy", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSeedStrategyList);
  this->paramSeedStrategy = paramSeedStrategyList.active;

  visual->qfeGetVisualParameter("Line seed transfer", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSeedTransferFunctionList);
  this->paramSeedTransferFunction = paramSeedTransferFunctionList.active;

  visual->qfeGetVisualParameter("Line seed density", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(this->paramSeedDensity);

  visual->qfeGetVisualParameter("Line trace direction", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLinesTraceDirList);
  if(paramLinesTraceDirList.active == 1) paramTraceDirection = -1;
  else                                   paramTraceDirection =  1;

  visual->qfeGetVisualParameter("Line trace step", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTraceStep);

  visual->qfeGetVisualParameter("Line trace iterations", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramTraceIterations);  

  visual->qfeGetVisualParameter("Line color", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLinesColorList); 
  paramLineColor = paramLinesColorList.active;  

  visual->qfeGetVisualParameter("Line shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramLinesShadingList); 
  paramLineShading = paramLinesShadingList.active;  

  visual->qfeGetVisualParameter("Line highlight visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(this->paramLineHighlightVisible);    

  // Pre-compute necessary line
  if(study != NULL)
  {
    bool newSeeds  = qfeUpdateSeedsRequired(this->paramSeedStrategy, this->paramSeedDensity, this->paramSeedTransferFunction);

    if(newSeeds == true)  
    {
      this->qfePrecomputeSeeds(study->probe2D, this->paramSeedStrategy, this->paramSeedTransferFunction, this->paramSeedDensity, this->probeSeeds);
    }
  } 

  // Prevent counter overload
  this->paramTraceCounter =  this->paramTraceCounter%paramTraceIterations;

  // Update algorithm parameters  
  this->algorithmFlowLines->qfeSetPhase(phase);
  this->algorithmFlowLines->qfeSetPhaseDuration(study->phaseDuration);
  this->algorithmFlowLines->qfeSetLineTrace(paramTraceDirection, paramTraceStep, paramTraceIterations);
  this->algorithmFlowLines->qfeSetLineSeedTime(phase);
  this->algorithmFlowLines->qfeSetLineHighlight(this->paramTraceCounter);
  this->algorithmFlowLines->qfeSetLineColorMode(paramLineColor);
  this->algorithmFlowLines->qfeSetLineShadingMode(paramLineShading);
  this->algorithmFlowLines->qfeSetLineWidth(2.5 * this->frameBufferProps[0].supersampling);
  this->algorithmFlowLines->qfeSetLightSource(light);  


  // Render all probes
  for(int i=0; i<(int)study->probe2D.size(); i++)
  {
    // Depth sort the seeds before rendering
    // qfeSeeding::qfeComputeSeedsDepthSort(this->currentModelView, this->probeSeeds[i]);
   
    // Start rendering
    if(study->probe2D[i].visible) 
    {         
      // Render line primitives
      if(study != NULL && probeCountChanged==false)
      {
        this->algorithmFlowLines->qfeSetLineColor(study->probe2D[i].color);
        switch(paramLinesList.active)
        {
        case 1 : this->algorithmFlowLines->qfeRenderVectorLines(this->probeSeeds[i], study->pcap[floor(phase)], colormap);       
                 break;
        case 2:  this->algorithmFlowLines->qfeRenderStreamLines(this->probeSeeds[i], study->pcap[floor(phase)], colormap);
                 break;
        case 3 : this->algorithmFlowLines->qfeRenderPathLines(this->probeSeeds[i], &study->pcap, colormap);
                 break;
        } 
      }      
    }
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeRenderVisualFlowPlanes(qfeVisualFlowPlanes *visual)
{
  qfeStudy           *study;
  int                 phase;

  this->visFlowPlanes->qfeGetVisualActiveVolume(phase);
  this->visFlowPlanes->qfeGetVisualStudy(&study);

  qfeVisualParameter *param;
  qfeSelectionList    paramPlaneTypeList;
  qfeSelectionList    paramPlaneDataList;
  double              paramPlaneSize       = 0.0;
  qfeVector           paramViewUp;

  this->visFlowPlanes->qfeGetVisualParameter("Plane type", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramPlaneTypeList);  

  this->visFlowPlanes->qfeGetVisualParameter("Plane data", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramPlaneDataList);  

  this->visFlowPlanes->qfeGetVisualParameter("Plane size", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramPlaneSize); 

  this->engine->GetViewUp(paramViewUp);

  // Update algorithm parameters
  this->algorithmFlowPlanes->qfeSetPlaneType(paramPlaneTypeList.active);
  this->algorithmFlowPlanes->qfeSetPlaneData(paramPlaneDataList.active);
  this->algorithmFlowPlanes->qfeSetPlaneSize(paramPlaneSize);
  this->algorithmFlowPlanes->qfeSetCameraViewUp(paramViewUp);

  // Render all probes
  for(int i=0; i<(int)study->probe2D.size(); i++)
  {
    // Start rendering
    if(study->probe2D[i].visible) 
    {
      if(study != NULL && probeCountChanged==false)
      {
        this->algorithmFlowPlanes->qfeRenderFlowPlanes(&study->probe2D[i], study->pcap[phase]);
      }      
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeRenderVisualFlowArrowTrails(qfeVisualFlowArrowTrails *visual)
{
  qfeStudy           *study;
  int                 phase;

  this->visFlowArrowTrails->qfeGetVisualActiveVolume(phase);
  this->visFlowArrowTrails->qfeGetVisualStudy(&study);

  qfeVisualParameter *param;
  bool                paramArrowVisible    = false;
  bool                paramArrowTrails     = false;
  double              paramArrowScale      = 1.0;
  qfeColorRGBA        paramArrowColor;
  int                 paramArrowMaxPerc    = 20;
  
  this->visFlowArrowTrails->qfeGetVisualParameter("Arrow visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramArrowVisible); 

  this->visFlowArrowTrails->qfeGetVisualParameter("Arrow trail", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramArrowTrails); 

  this->visFlowArrowTrails->qfeGetVisualParameter("Arrow maximum perc.", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramArrowMaxPerc); 

  this->visFlowArrowTrails->qfeGetVisualParameter("Arrow scale", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramArrowScale); 

  this->visFlowArrowTrails->qfeGetVisualParameter("Arrow color", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramArrowColor); 

  // Pre-compute necessary line
  if(study != NULL)
  {
     bool newArrows = qfeUpdateArrowsRequired(paramArrowMaxPerc);
   
    if(newArrows == true)
    {
      //this->qfePrecomputeBigArrows(study->probe, study->pcap, paramArrowMaxPerc, this->bigArrowDirections, this->bigArrowPositions);
    }
  }

  // Render all probes
  for(int i=0; i<(int)study->probe2D.size(); i++)
  {
    // Start rendering
    if(study->probe2D[i].visible) 
    {      
      //if(paramArrowVisible) this->algorithmFlowArrows->qfeRenderBigArrowGPU(&study->probe[i], 
      //  this->qfeRenderBigArrowGPU(i, phase, paramArrowTrails);
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeRenderVisualAides(qfeVisualAides *visual)
{
  qfeStudy           *study;
  qfeVolume          *volume;
  int                 current;

  qfeVisualParameter *param;
  bool                paramSupportBoundingBox = true;
  bool                paramSupportAxes        = false;

  visual->qfeGetVisualStudy(&study);  
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  volume = study->pcap[current];

  visual->qfeGetVisualParameter("Support bounding box", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportBoundingBox); 

  visual->qfeGetVisualParameter("Support axes", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramSupportAxes); 

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  if(paramSupportBoundingBox == true) qfeDriverAide::qfeRenderBoundingBoxDashed(volume, this->currentModelView, 0.8*this->frameBufferProps[0].supersampling);
  if(paramSupportAxes        == true) qfeDriverAide::qfeRenderVolumeAxes(volume, 10.0, 1.6*this->frameBufferProps[0].supersampling);

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfePrecomputeSeeds(qfeProbe2DSeries probe, int seedStrategy, int seedTransferFunction, double seedDensity, qfeSeedsGroup &seeds)
{
  seeds.clear();

  for(int i=0; i<(int)probe.size(); i++)
  {
      qfeSeeds seedsPerProbe;
      qfeSeeding::qfeGetSeeds(&probe[i], (qfeSeedType)seedStrategy, seedDensity, seedsPerProbe);
      qfeSeeding::qfeComputeSeedsTransferFunction(&probe[i], (qfeSeedTransferFunction)seedTransferFunction, seedsPerProbe);
      seeds.push_back(seedsPerProbe);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeUpdateSeeds(qfeProbe2D probe, int seedStrategy, int seedTransferFunction, double seedDensity, qfeSeeds &seeds)
{
  qfeSeeding::qfeGetSeeds(&probe, (qfeSeedType)seedStrategy, seedDensity, seeds);
  qfeSeeding::qfeComputeSeedsTransferFunction(&probe, (qfeSeedTransferFunction)seedTransferFunction, seeds);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
bool qfeDriverIFR::qfeUpdateSeedsRequired(int seedStrategy, double seedDensity, int seedTemplate)
{
  if(this->probeCountChanged)    return true;
  
  if(seedStrategy != this->probeSeedsStrategy)
  {
    this->probeSeedsStrategy = seedStrategy;
    return true;
  }

  if(seedDensity != this->probeSeedsDensity)
  {
    this->probeSeedsDensity = seedDensity;
    return true;
  }

  if(seedTemplate != this->probeSeedsTemplate)
  {
    this->probeSeedsTemplate = seedTemplate;
    return true;
  }

  return false;
}

//----------------------------------------------------------------------------
bool qfeDriverIFR::qfeUpdateLinesRequired(qfeFloat traceStep, int traceIterations)
{
  if(this->probeCountChanged) return true;

  if(traceStep != this->probeTraceStep)
  {
    this->probeTraceStep = traceStep;
    return true;
  }

  if(traceIterations != this->probeTraceIterations)
  {
    this->probeTraceIterations = traceIterations;
    return true;
  }

  return false;
}

//----------------------------------------------------------------------------
bool qfeDriverIFR::qfeUpdateArrowsRequired(int arrowMaxPerc)
{
  if(this->probeCountChanged) return true;

  if(arrowMaxPerc != this->probeArrowMaxPerc)
  {
    this->probeArrowMaxPerc = arrowMaxPerc;
    return true;
  }

  return false;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeWorkflowProbeClick(qfePoint *p)
{    
  qfeStudy *study;

  switch(this->probeClickState)
  {
    case probeSelectStart : 
      //cout << "start" << endl;
      this->scene->qfeSetSceneCursor(cursorCross);
      this->probeClickState   = probeSelectFirst;
      this->engine->SetStatusMessage("Hold CTRL key and click left mouse button to select...");
      break;
    case probeSelectFirst :        
      //cout << "first" << endl; 
      if(this->visFlowProbe != NULL)
        this->visFlowProbe->qfeGetVisualStudy(&study);
      else
        break;

      this->qfeAddProbe(*p, study);
            
      this->scene->qfeSceneUpdate();
      this->scene->qfeSetSceneCursor(cursorArrow);
      this->probeClickState  = probeSelectStart;
      this->engine->SetStatusMessage("");
      break;
    case probeSelectAbort :
      //cout << "abort" << endl;
      this->scene->qfeSetSceneCursor(cursorArrow);
      this->probeClickState  = probeSelectStart;
      break;
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeGetSelectedObject(qfeColorRGB color, int &id, int &nr)
{
  float delta = 0.01;

  id = -1;
  nr =  0;  

  if((color.b >= SELECT_PROBE-delta) && 
     (color.b <= SELECT_PROBE+delta) && 
     (color.r==color.g))
  {
    id = qfeObjectProbe2D;
    nr = (int)ceil(color.r*255.0f);    
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeSetSelectedProbe(qfeVisualFlowProbe2D *visual, int index)
{
  qfeStudy           *study;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study); 

  for(int i=0; i<(int)study->probe2D.size(); i++)
  {
    if(i == index) 
      study->probe2D[i].selected = true;
    else
      study->probe2D[i].selected = false;    
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeGetSelectedProbe(qfeVisualFlowProbe2D *visual, int &index)
{
  qfeStudy           *study;

  index = -1;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study); 

  for(int i=0; i<(int)study->probe2D.size(); i++)
  {
    if(study->probe2D[i].selected == true) 
      index = i;    
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfePickMesh(int windowX, int windowY, qfeViewport *viewport, qfePoint &patientPos)
{
  qfeMatrix4f Minv;
  qfePoint    windowPos, worldPos;
  float       depth;

  // Compute the view vector
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, Minv);

  qfeVector eye;
  eye.qfeSetVectorElements(0.0,0.0,1.0);
  eye = eye*Minv;
  qfeVector::qfeVectorNormalize(eye);

  // Obtain the coordinates
  this->qfeReadDepth(windowX, windowY, depth);
  windowPos.x = windowX;
  windowPos.y = windowY;  
  windowPos.z = depth;

  // Unproject to patient (/world) coordinates
  qfeTransform::qfeGetCoordinateUnproject(windowPos, worldPos, this->currentModelView, this->currentProjection, viewport);

  worldPos   = worldPos + 5.0*eye;
  worldPos.x = worldPos.x/worldPos.w;
  worldPos.y = worldPos.y/worldPos.w;
  worldPos.z = worldPos.z/worldPos.w;

  patientPos = worldPos;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfePickMaximum(int windowX, int windowY, qfeViewport *viewport, qfeVolume *volume, qfePoint &patientPos)
{
  qfeMatrix4f P2V, Minv;
  qfePoint    windowPos, worldPos, voxelPos;
  qfePoint   *intersectPos;
  qfePoint    q, p1f, p1b, swap;
  int         count;
  float       depth;
  float       depthDelta = 0.01;
  float       stepSize;
  int         steps;
  
  unsigned int  dims[3];
  unsigned int  nc;
  qfeValueType  t;
  void         *data;
  int           readIndex;

  if(viewport == NULL || volume == NULL) return qfeError;

  // Get the volume data
  volume->qfeGetVolumeComponentsPerVoxel(nc);  
  volume->qfeGetVolumeData(t, dims[0], dims[1], dims[2], &data);

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  if(nc != 1) return qfeError;

  // Get the view vector
  qfeMatrix4f::qfeGetMatrixInverse(this->currentModelView, Minv);

  qfeVector eye;
  eye.qfeSetVectorElements(0.0,0.0,-1.0);
  eye = eye*Minv;
  qfeVector::qfeVectorNormalize(eye);

  // Unproject the center in patient coordinates to window coordinates
  // We don't know the depth a priori, but we need a reasonable estimate
  // before we determine the patient position by searching the maximum
  qfePoint o, windowOrigin;
  o.qfeSetPointElements(0.0,0.0,0.0);
  qfeTransform::qfeGetCoordinateProject(o, windowOrigin, this->currentModelView, this->currentProjection, viewport);

  // Obtain the coordinates
  windowPos.x = windowX;
  windowPos.y = windowY;  
  windowPos.z = windowOrigin.z;

  // Unproject to patient (/world) coordinates
  qfeTransform::qfeGetCoordinateUnproject(windowPos, worldPos, this->currentModelView, this->currentProjection, viewport);

  worldPos.x = worldPos.x/worldPos.w;
  worldPos.y = worldPos.y/worldPos.w;
  worldPos.z = worldPos.z/worldPos.w;

  // Compute the intersections with the bounding box
  this->qfeGetIntersectionsBoxLine(volume, worldPos, worldPos+eye, &intersectPos, count);

  if(count < 2) return qfeError;

  // Assign the intersection point and sort them according to their depth
  p1f = intersectPos[0];
  p1b = intersectPos[1];
  
  if((p1f*this->currentModelView).z < (p1b*this->currentModelView).z)
  {
    swap = p1f;
    p1f  = p1b;
    p1b  = swap;
  }

  patientPos = p1f;

  // Initialize parameters
  qfeVector::qfeVectorLength(p1f-p1b, depth);
  stepSize      = 1.0f;  
  steps         = floor(abs(depth) / stepSize);  

  unsigned short  maximum = 0.0;

  for(int i=0; i<steps; i++)
  {      
    unsigned short currentValue;

    q          = p1f + i*stepSize*eye;   
    voxelPos   = q * P2V;

    // Take the scalar from the data and compute structure tensors
    readIndex = voxelPos.x  + (voxelPos.y *dims[0]) + (voxelPos.z *dims[0]*dims[1]);

    if(readIndex >= 0 && readIndex < (int)(dims[0]*dims[1]*dims[2]))
    {
      currentValue = *((unsigned short*)data + readIndex);
    }

    // Find the maximum
    if(currentValue > maximum)
    {
      patientPos = q;
      maximum    = currentValue;
    }    
  }

  delete [] intersectPos;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
void qfeDriverIFR::qfeOnTimer(void *caller)
{
  qfeDriverIFR *self = reinterpret_cast<qfeDriverIFR *>(caller);
  
  if(self->paramLineHighlightVisible == true)
  {
    self->paramTraceCounter++;
  }
  else
  {
    self->paramTraceCounter =  0;
  }  
}

//----------------------------------------------------------------------------
void qfeDriverIFR::qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v)
{
  qfeDriverIFR *self = reinterpret_cast<qfeDriverIFR *>(caller);
  
  qfeStudy     *study;
  qfeColorRGB   color;
  qfeViewport  *vp3D;
  qfeVolume    *volume;
  qfeMatrix4f   P2V;
  unsigned int  nx, ny, nz;
  int           current;
    
  self->mouseDrag = true;
  self->mouseCache.x = (float) x;
  self->mouseCache.y = (float) y;

  self->engine->GetViewport3D(&vp3D);
  self->qfeReadSelect(x-vp3D->origin[0], y-vp3D->origin[1], color);

  self->visFlowProbe->qfeGetVisualStudy(&study);
  self->visFlowProbe->qfeGetVisualActiveVolume(current);

  if(study == NULL) return;

  volume = study->pcap[current];
  volume->qfeGetVolumeSize(nx, ny, nz);

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);  

  if(self->engine->GetInteractionControlKey()) 
  {
    qfePoint   patientCoord, voxelCoord; 
    bool       mesh = false;

    if(self->probeClickState == probeSelectFirst)
    {
      // Determine if we have a mesh available
      if((int)study->segmentation.size() > 0)
      {
        bool visible;
        for(int i=0; i<(int)study->segmentation.size(); i++)
          study->segmentation[i]->qfeGetModelVisible(visible);

        if(visible == true) mesh = true;
      }

      if(mesh)
        self->qfePickMesh(x,y,vp3D,patientCoord);
      else
        self->qfePickMaximum(x,y,vp3D,study->tmip,patientCoord);

      voxelCoord = patientCoord * P2V;
      
      // Add the 2D probe now
      if(voxelCoord.x >= 0 && voxelCoord.x < nx && 
         voxelCoord.y >= 0 && voxelCoord.y < ny && 
         voxelCoord.z >= 0 && voxelCoord.z < nz)
      {
        self->qfeWorkflowProbeClick(&voxelCoord);
      }    

      // Update the quantification measures
      for(int i=0; i<(int)study->probe2D.size(); i++)
      {
        self->algorithmProbe2D->qfeQuantifyProbe(&study->pcap, study->probe2D[i]);
        self->scene->qfeSelectionUpdate();                 
      }
    }
  }
  else
  {
    int objId, objNr;

    self->qfeGetSelectedObject(color, objId, objNr);

    // Handle mouse left click
    switch(objId)
    {
      case qfeObjectProbe     : //cout << "probe" << endl;   
             if((objNr < 0) || (objNr > (int)self->probeInteractionState.size())) break;
 
             if(self->probeInteractionState[objNr] == probeInteractNone)
             {         
               self->qfeSetSelectedProbe(self->visFlowProbe, objNr);
               self->probeInteractionState[objNr] = probeInteractTranslate; 
                      
               //self->engine->Refresh();
               //self->qfeMouseMoveOffset(&study->probe3D[objNr], self->prevPoint, eyeVec, self->probeClickOffset);
             }             
             break;
      default :           
             // Default camera interaction
             self->qfeSetSelectedProbe(self->visFlowProbe, -1);
             self->engine->CallInteractionParentLeftButtonDown();       
    }
  }

}

//----------------------------------------------------------------------------
void qfeDriverIFR::qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v)
{
  qfeDriverIFR *self = reinterpret_cast<qfeDriverIFR *>(caller);

  qfeStudy    *study;
  int          delta = 1; // pixel difference
  bool         mouseMovement;

  self->visFlowProbe->qfeGetVisualStudy(&study);

  mouseMovement = abs((int)self->mouseCache.x-x)>delta && abs((int)self->mouseCache.y-y)>delta;

  // Update probe interaction states
  for(int i=0; i<(int)self->probeInteractionState.size(); i++)
  {
    //cout << "up: " << i << endl;
    if((self->probeInteractionState[i] == probeInteractTranslate))
    {
      qfeMatrix4f P2V;
      qfePoint    patientCoord, voxelCoord; 

      self->probeInteractionState[i] = probeInteractNone;    
    }
  }
  
  if(study != NULL)
  {
    for(int i=0; i<(int)study->probe2D.size(); i++)
    {
      self->algorithmProbe2D->qfeQuantifyProbe(&study->pcap, study->probe2D[i]);
      self->scene->qfeSelectionUpdate();
    }
  }

  self->mouseDrag = false;
  self->engine->Refresh();
  return;
}

//----------------------------------------------------------------------------
void qfeDriverIFR::qfeOnMouseMove(void *caller, int x, int y, int v)
{
  qfeDriverIFR *self = reinterpret_cast<qfeDriverIFR *>(caller);

  qfeStudy    *study;
  qfeViewport *vp3D;
  unsigned int nx, ny, nz;
  int          delta = 1; // pixel difference
  bool         mouseMovement;

  self->visFlowProbe->qfeGetVisualStudy(&study);
  self->engine->GetViewport3D(&vp3D);
  
  study->pcap.front()->qfeGetVolumeSize(nx, ny, nz);

  mouseMovement = abs((int)self->mouseCache.x-x)>delta && abs((int)self->mouseCache.y-y)>delta;

  // Update probe interaction states
  for(int i=0; i<(int)self->probeInteractionState.size(); i++)
  {
    if((self->probeInteractionState[i] == probeInteractTranslate) && mouseMovement)
    {
      qfeMatrix4f P2V;
      qfePoint    patientCoord, voxelCoord; 
       bool       mesh = false;

      // Determine if we have a mesh available
      if((int)study->segmentation.size() > 0)
      {
        bool visible;
        for(int i=0; i<(int)study->segmentation.size(); i++)
          study->segmentation[i]->qfeGetModelVisible(visible);

        if(visible == true) mesh = true;
      }

      if(mesh)
        self->qfePickMesh(x,y,vp3D,patientCoord);
      else
        self->qfePickMaximum(x,y,vp3D,study->tmip,patientCoord);

      qfeTransform::qfeGetMatrixPatientToVoxel(P2V, study->pcap.front());  
      voxelCoord = patientCoord * P2V;

      if(voxelCoord.x > 0 && voxelCoord.x < nx && 
         voxelCoord.y > 0 && voxelCoord.y < ny && 
         voxelCoord.z > 0 && voxelCoord.z < nz)
      { 
        self->qfeUpdateProbe(voxelCoord, study, i);

        self->qfeUpdateSeeds(study->probe2D[i], self->paramSeedDensity, self->paramSeedTransferFunction, self->paramSeedDensity, self->probeSeeds[i]);
      }
    }
  }

  // Render the scene
  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverIFR::qfeOnKeyPress(void *caller, char *keysym)
{
  qfeDriverIFR *self = reinterpret_cast<qfeDriverIFR *>(caller); 

  qfeStudy   *study;
  qfeMatrix4f P2V;
  int probeId;

  self->visFlowProbe->qfeGetVisualStudy(&study);
  self->qfeGetSelectedProbe(self->visFlowProbe, probeId);
  
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, study->pcap.front());

  if(probeId < 0) return;

  string key = keysym;

  if(key.compare("Delete") == 0 )
  {
    self->qfeDeleteProbe(probeId, study);
    self->scene->qfeSceneUpdate();
    self->engine->Refresh();    
  } 
  if(key.compare("Up") == 0)
  {        
    qfeVector n = study->probe2D[probeId].normal;
    qfePoint  p = study->probe2D[probeId].origin + 2*n;

    self->qfeUpdateProbe(p*P2V, study, probeId);

    self->qfeUpdateSeeds(study->probe2D[probeId], self->paramSeedDensity, self->paramSeedTransferFunction, self->paramSeedDensity, self->probeSeeds[probeId]);

    self->algorithmProbe2D->qfeQuantifyProbe(&study->pcap, study->probe2D[probeId]);
    self->scene->qfeSelectionUpdate();

    self->engine->Refresh(); 
  }
  if(key.compare("Down") == 0)
  {    
    qfeVector n = study->probe2D[probeId].normal;
    qfePoint  p = study->probe2D[probeId].origin - 2*n;

    self->qfeUpdateProbe(p*P2V, study, probeId);

    self->qfeUpdateSeeds(study->probe2D[probeId], self->paramSeedDensity, self->paramSeedTransferFunction, self->paramSeedDensity, self->probeSeeds[probeId]);

    self->algorithmProbe2D->qfeQuantifyProbe(&study->pcap, study->probe2D[probeId]);
    self->scene->qfeSelectionUpdate();

    self->engine->Refresh(); 
  }
}

//----------------------------------------------------------------------------
void qfeDriverIFR::qfeOnResize(void *caller)
{
  qfeDriverIFR *self = reinterpret_cast<qfeDriverIFR *>(caller);

  qfeViewport *vp;

  self->engine->GetViewport3D(&vp);

  qfeDriver::qfeDriverResize(caller);

  self->algorithmMIP->qfeSetViewportSize(vp->width, vp->height, self->frameBufferProps[0].supersampling);
}

//----------------------------------------------------------------------------
void qfeDriverIFR::qfeOnAction(void *caller, int t)
{
  int probeId;
  qfeStudy *study;

  qfeDriverIFR *self = reinterpret_cast<qfeDriverIFR *>(caller);
  
  self->visFlowProbe->qfeGetVisualStudy(&study);
  self->qfeGetSelectedProbe(self->visFlowProbe, probeId);

  switch(t)
  {
  case qfeVisualFlowProbe3D::actionNewProbeClick  :
    self->probeClickState  = probeSelectStart;
    self->qfeWorkflowProbeClick(NULL);         
    break;  
  case qfeVisualFlowProbe3D::actionDeleteProbe :      
    if(probeId < 0) break;
    self->qfeDeleteProbe(probeId, study);
    self->scene->qfeSceneUpdate();
    break;
  }

  self->engine->Refresh();
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeAddProbe(qfePoint seed, qfeStudy *study)
{
  qfeProbe2D        probe;  
  qfeGrid          *grid;

  unsigned int      nx, ny, nz;
  qfeFloat          sx, sy, sz;


  if(study == NULL)       return qfeError;
  if(study->tmip == NULL) return qfeError;

  // Check if the seed is within the bounding box
  study->tmip->qfeGetVolumeSize(nx,ny,nz);
  study->tmip->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(sx, sy, sz);

  if(seed.x < 0 || seed.x > nx || 
     seed.y < 0 || seed.y > ny || 
     seed.z < 0 || seed.z > nz)
    return qfeError;

  // A series of volumes is loaded
  if(this->algorithmProbe2D->qfeLocateProbe(seed, study, probe) == qfeSuccess)
  {
    if(probe.radius > 2.0 * min(min(sx,sy),sz))
    {
      probe.color = this->probeColor[(study->probe2D.size())%this->probeColor.size()];
      probe.selected = true;

      for(int i=0; i < (int)study->probe2D.size(); i++)
      {
        study->probe2D[i].selected = false;
      }

      study->probe2D.push_back(probe);  
      this->probeInteractionState.push_back(probeInteractNone);
     
    }
  }
  else return qfeError;
 
  this->probeCountChanged = true;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeDeleteProbe(int probeId, qfeStudy *study)
{
  if(probeId >= 0 && probeId < (int)study->probe2D.size())
  {
    study->probe2D.erase(study->probe2D.begin() + probeId);
    this->probeSeeds.erase(this->probeSeeds.begin() + probeId);
  }
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeUpdateProbe(qfePoint seed, qfeStudy *study, int probeId)
{
  qfeProbe2D        probe;  
  qfeGrid          *grid;
 
  unsigned int      nx, ny, nz;
  qfeFloat          sx, sy, sz;

  if(study == NULL)       return qfeError;
  if(study->tmip == NULL) return qfeError;
  if(probeId < 0 || probeId > (int)study->probe2D.size()) return qfeError;

  // Check if the seed is within the bounding box
  study->tmip->qfeGetVolumeSize(nx,ny,nz);
  study->tmip->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(sx, sy, sz);

  if(seed.x < 0 || seed.x > nx || 
     seed.y < 0 || seed.y > ny || 
     seed.z < 0 || seed.z > nz)
    return qfeError;

  // A series of volumes is loaded
  if(this->algorithmProbe2D->qfeLocateProbe(seed, study, probe) == qfeSuccess)
  {
    bool visible       = study->probe2D[probeId].visible;
    bool selection     = study->probe2D[probeId].selected;
    qfeColorRGB color  = study->probe2D[probeId].color;

    probe.visible  = visible;
    probe.selected = selection;
    probe.color    = color;

    //cout << "origin: "; qfePoint::qfePointPrint(probe.origin); cout << endl;
    //cout << "radius: " << probe.radius; cout << endl;
    //cout << "area: " << probe.area; cout << endl;
    //cout << "axis X: "; qfeVector::qfeVectorPrint(probe.axisX); cout << endl;
    //cout << "axis Y: "; qfeVector::qfeVectorPrint(probe.axisY); cout << endl;
    //cout << "axis Z: "; qfeVector::qfeVectorPrint(probe.normal); cout << endl;
    //cout << endl;

    if(probe.radius > 2.0 * min(min(sx,sy),sz))
      study->probe2D[probeId] = probe;  
  }
  else return qfeError;


  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverIFR::qfeGetDataSet(int index, qfeVisual *visual, int time, qfeVolume **volume)
{
  qfeStudy           *study;

  *volume = NULL;
  visual->qfeGetVisualStudy(&study); 

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(0) : // PCA-P
    if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time)) 
      *volume = study->pcap[time];
    break;
  case(1) : // PCA-M
    if(((int)study->pcam.size() > 0) && ((int)study->pcam.size() > time)) 
      *volume = study->pcam[time];
    break;
  case(2) : // SSFP
    if((int)study->ssfp3D.size() > 0) 
    {
      // If there is also pcap data loaded, check for the trigger time and load
      // accompanying ssfp data at the right time slot 
      if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time)) 
      {
        qfeFloat att, ptt;
        qfeFloat distance = 9000;
        (int)study->pcap[time]->qfeGetVolumeTriggerTime(ptt);
        for(int i=0; i<(int)study->ssfp3D.size(); i++)
        {
          (int)study->ssfp3D[i]->qfeGetVolumeTriggerTime(att);
          if(abs(att-ptt) < distance)
          {
            distance = abs(att-ptt);
            *volume = study->ssfp3D[i];
          }
        }
      }
      else
        *volume = study->ssfp3D[0];
    }
    break;
    case(3) : // T-MIP
      if(study->tmip != NULL) 
      *volume = study->tmip;
      break;
    case(4) : // FFE
    if(((int)study->ffe.size() > 0) && ((int)study->ffe.size() > time)) 
      *volume = study->ffe[time];
    break;
  }

  return qfeSuccess;
}


//----------------------------------------------------------------------------
// p1, p2 - points on a line in patient coordinates
qfeReturnStatus qfeDriverIFR::qfeGetIntersectionsBoxLine(qfeVolume *volume, qfePoint p1, qfePoint p2, qfePoint **intersections, int &count)
{
  const int        boxSides = 6;

  qfeVector        normals[boxSides];
  qfePoint         origins[boxSides];
  vector<qfePoint> intersect;

  qfeGrid         *grid;
  unsigned int     size[3];
  qfePoint         o, p;
  qfeVector        e, axes[3];
  qfeFloat         nom, den, u;
  bool             inside;

  if(volume == NULL) return qfeError;

  qfeMatrix4f P2V;
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  // Get volume properties
  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(size[0],size[1],size[2]);
  grid->qfeGetGridOrigin(o.x, o.y, o.z);
  grid->qfeGetGridExtent(e.x, e.y, e.z);
  grid->qfeGetGridAxes(axes[0].x, axes[0].y, axes[0].z, axes[1].x, axes[1].y, axes[1].z, axes[2].x, axes[2].y, axes[2].z);

  // Compute the normals of the bounding box sides
  qfeVector::qfeVectorCross(axes[0], axes[1], normals[0]);
  qfeVector::qfeVectorCross(axes[0], axes[2], normals[2]);
  qfeVector::qfeVectorCross(axes[1], axes[2], normals[4]);
  normals[1] = -1.0f * normals[0];
  normals[3] = -1.0f * normals[2];
  normals[5] = -1.0f * normals[4];

  for(int i=0; i<boxSides; i++)
    qfeVector::qfeVectorNormalize(normals[i]);

  // Compute the origins of the bounding box sides
  origins[0] = o + e.z*0.5f*size[2]*axes[2];
  origins[1] = o - e.z*0.5f*size[2]*axes[2];
  origins[2] = o - e.y*0.5f*size[1]*axes[1];
  origins[3] = o + e.y*0.5f*size[1]*axes[1];
  origins[4] = o + e.x*0.5f*size[0]*axes[0];
  origins[5] = o - e.x*0.5f*size[0]*axes[0];

  // Compute the intersections between the line p1-p2 and the planes
  for(int i=0; i<boxSides; i++)
  {
    den = normals[i] * (p2-p1);
    // Check if the line is parallel to the plane
    if(den != 0)
    {
      nom = normals[i] * (origins[i] - p1);
      u   = nom / den;
      p   = p1 + u*(p2-p1);

      this->qfeGetInsideBox(volume, p, inside);

      if(inside) 
        intersect.push_back(p);
    }
  } 

  // Output the intersection
  count = (int)intersect.size();
  
  (*intersections) = new qfePoint[count];
  for(int i=0; i<count; i++)
    (*intersections)[i] = intersect[i];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// p - patient coordinates
qfeReturnStatus qfeDriverIFR::qfeGetInsideBox(qfeVolume *volume, qfePoint p, bool &inside)
{
  qfeMatrix4f      P2V;
  qfeGrid         *grid;
  qfePoint         pVoxel;
  unsigned int     size[3];

  if(volume == NULL) return qfeError; 

  // Get transformation patient to voxel
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  // Get volume properties
  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(size[0],size[1],size[2]);

  // Transform the coordinate
  pVoxel = p * P2V;

  // Check if the point is in the bounding box
  inside = false;

  if((ceil(pVoxel.x) >=0) && (floor(pVoxel.x) <= size[0]) &&
     (ceil(pVoxel.y) >=0) && (floor(pVoxel.y) <= size[1]) &&
     (ceil(pVoxel.z) >=0) && (floor(pVoxel.z) <= size[2]))
  {    
    inside = true;
  }
  
  return qfeSuccess;
}