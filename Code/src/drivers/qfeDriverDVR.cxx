#include "qfeDriverDVR.h"

//----------------------------------------------------------------------------
qfeDriverDVR::qfeDriverDVR(qfeScene *scene) : qfeDriver(scene)
{
  this->visualDVR     = NULL;

  this->currentTransferFunctionId = 0;

  this->quality       = 2.0;

  this->algorithmDVR  = new qfeRayCasting();

  this->engine->SetObserverResize((void*)this, this->qfeOnResize);
  this->engine->SetInteractionMouseMove((void*)this, this->qfeOnMouseMove, false);
  this->engine->SetInteractionMouseLeftButtonDown((void*)this, this->qfeOnMouseLeftButtonDown, true);
  this->engine->SetInteractionMouseLeftButtonUp((void*)this, this->qfeOnMouseLeftButtonUp, true);
  this->engine->SetInteractionMouseRightButtonDown((void*)this, this->qfeOnMouseRightButtonDown, false);
};

//----------------------------------------------------------------------------
qfeDriverDVR::~qfeDriverDVR()
{
  delete this->algorithmDVR;

  this->qfeRenderStop();
};

//----------------------------------------------------------------------------
bool qfeDriverDVR::qfeRenderCheck()
{
  int t;
  for(int i=0; i<(int)this->visuals.size(); i++)
  {
    this->visuals[i]->qfeGetVisualType(t);
    if(t == visualRaycast)
    {
      this->visualDVR = static_cast<qfeVisualRaycast *>(this->visuals[i]);
    }
  }
  return true;
}

//----------------------------------------------------------------------------
void qfeDriverDVR::qfeRenderStart()
{
  qfeViewport *vp3D;
  this->engine->GetViewport3D(&vp3D);

  if(this->visualDVR != NULL)
  {
    this->algorithmDVR->qfeSetViewportSize(vp3D->width, vp3D->height, this->frameBufferProps[0].supersampling);
  }
}

//----------------------------------------------------------------------------
void qfeDriverDVR::qfeRenderWait()
{
  // Initialize OpenGL state
  glDisable(GL_DITHER);
  glDisable(GL_NORMALIZE);
  glDisable(GL_LIGHTING);
  glDisable(GL_COLOR_MATERIAL);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Initialize color map
  qfeColorMap *colormap;
  if(this->visualDVR != NULL)
  {
    this->visualDVR->qfeGetVisualColorMap(&colormap);
    colormap->qfeUploadColorMap();
    colormap->qfeUploadGradientMap();
  }

  // Render DVR
  if(this->visualDVR != NULL)
  {
    this->qfeRenderVisualDVR(this->visualDVR);
  }
};

//----------------------------------------------------------------------------
void qfeDriverDVR::qfeRenderStop()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverDVR::qfeRenderVisualDVR(qfeVisualRaycast *visual)
{
  qfeStudy       *study;
  qfeVolume      *volume;
  qfeColorMap    *colormap;
  qfeLightSource *light;
  int             current;

  qfeVisualParameter *param;
  qfeSelectionList    paramDataList;
  qfeSelectionList    paramCompList;
  qfeSelectionList    paramQualList;
  qfeSelectionList    paramStyleList;
  bool                paramVisible;
  int                 paramData;
  int                 paramComp;
  int                 paramQuality;
  bool                paramGradient;
  bool                paramShading;  
  bool                paramMultiRes;
  qfeFloat            paramRange[2];  
  double              paramMIDAgamma;
  qfeRange            paramClipX, paramClipY, paramClipZ;

  if(visual == NULL) return qfeError;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);
  visual->qfeGetVisualColorMap(&colormap);

  this->scene->qfeGetSceneLightSource(&light);

  // Fetch the parameters
  visual->qfeGetVisualParameter("DVR visible", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramVisible);

  visual->qfeGetVisualParameter("DVR data set", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramDataList);
  paramData = paramDataList.active;

  visual->qfeGetVisualParameter("DVR data component", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramCompList);
  paramComp = paramCompList.active;
/*
  visual->qfeGetVisualParameter("DVR data representation", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramReprList);
  paramRepr = paramReprList.active;
*/
  visual->qfeGetVisualParameter("DVR gradient", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramGradient);

  visual->qfeGetVisualParameter("DVR shading", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramShading);

  visual->qfeGetVisualParameter("DVR shading style", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramStyleList);

  visual->qfeGetVisualParameter("DVR shading quality", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramQualList);
  switch(paramQualList.active)
  {
  case 0  : paramQuality = 1; break;
  case 1  : paramQuality = 2; break;
  case 2  : paramQuality = 4; break;
  case 3  : paramQuality = 8; break;
  default : paramQuality = 2; break;
  }

  visual->qfeGetVisualParameter("DVR shading multires", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramMultiRes);

  visual->qfeGetVisualParameter("DVR MIDA gamma", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramMIDAgamma);

  visual->qfeGetVisualParameter("DVR clipping x", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipX);

  visual->qfeGetVisualParameter("DVR clipping y", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipY);

  visual->qfeGetVisualParameter("DVR clipping z", &param);
  if(param != NULL) param->qfeGetVisualParameterValue(paramClipZ);  

  if(paramVisible != true) return qfeError;

  // Get the data set and compute the data range
  this->qfeGetDataSet(paramData, visual, current, &volume, paramRange[0], paramRange[1]);

  if(volume == NULL) return qfeError;  

  // Multi-resolution dvr - check the global state
  if(paramMultiRes)
    paramQuality = std::min((float)paramQuality, this->quality);

  // Update algorithm parameters
  this->algorithmDVR->qfeSetOrthoClippingRanges(paramClipX, paramClipY, paramClipZ);
  this->algorithmDVR->qfeSetDataComponent(paramComp);
  this->algorithmDVR->qfeSetDataRange(paramRange[0], paramRange[1]);
  this->algorithmDVR->qfeSetGradientRendering(paramGradient);
  this->algorithmDVR->qfeSetShading(paramShading);
  this->algorithmDVR->qfeSetLightSource(light);
  this->algorithmDVR->qfeSetQuality(paramQuality);
  this->algorithmDVR->qfeSetStyle(paramStyleList.active);
  this->algorithmDVR->qfeSetMIDAgamma(paramMIDAgamma);

  this->algorithmDVR->qfeRenderRaycasting(this->frameBufferProps[0].texColor, this->frameBufferProps[0].texDepth, volume, colormap);

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeDriverDVR::qfeGetDataSet(int index, qfeVisual *visual, int time, qfeVolume **volume, qfeFloat &vl, qfeFloat &vu)
{
  qfeStudy           *study;
  int                 current;

  *volume = NULL;
  vl = vu = 0.0;

  visual->qfeGetVisualStudy(&study);
  visual->qfeGetVisualActiveVolume(current);

  if(study == NULL) return qfeError;

  switch(index)
  {
  case(0) : // PCA-P
    if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time))
    {
      *volume = study->pcap[time];
      study->qfeGetStudyRangePCAP(vl, vu);
    }
    break;
  case(1) : // PCA-M
    if(((int)study->pcam.size() > 0) && ((int)study->pcam.size() > time))
    {
      *volume = study->pcam[time];
      study->qfeGetStudyRangePCAM(vl, vu);
    }
    break;
  case(2) : // SSFP
    if((int)study->ssfp3D.size() > 0)
    {
      // If there is also pcap data loaded, check for the trigger time and load
      // accompanying ssfp data at the right time slot
      if(((int)study->pcap.size() > 0) && ((int)study->pcap.size() > time))
      {
        qfeFloat att, ptt;
        qfeFloat distance = 9000;
        (int)study->pcap[time]->qfeGetVolumeTriggerTime(ptt);
        for(int i=0; i<(int)study->ssfp3D.size(); i++)
        {
          (int)study->ssfp3D[i]->qfeGetVolumeTriggerTime(att);
          if(abs(att-ptt) < distance)
          {
            distance = abs(att-ptt);
            *volume = study->ssfp3D[i];
          }
        }
      }
      else
        *volume = study->ssfp3D[0];

      study->qfeGetStudyRangeSSFP(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(3) : // T-MIP
    if(study->tmip != NULL)
    {
      *volume = study->tmip;
      study->tmip->qfeGetVolumeValueDomain(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(4) : // Flow anatomy FFE
    if(((int)study->ffe.size() > 0) && ((int)study->ffe.size() > time))
    {
      *volume = study->ffe[time];
      study->qfeGetStudyRangeFFE(vl, vu);
      // Non-float texture are clamped [0,1]
      //vl = vl / pow(2.0f,16.0f);
      //vu = vu / pow(2.0f,16.0f);
    }
    break;
  case(5) : // FTLE
    if(((int)study->ftle.size() > 0) && ((int)study->ftle.size() > time))
    {
      *volume = study->ftle[time];
      study->qfeGetStudyRangeFTLE(vl, vu);
    }
    break;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeDriverDVR::qfeOnResize(void *caller)
{
  qfeDriverDVR *self = reinterpret_cast<qfeDriverDVR *>(caller);
  qfeViewport *vp3D;

  self->engine->GetViewport3D(&vp3D);

  qfeDriver::qfeDriverResize(caller);

  self->algorithmDVR->qfeSetViewportSize(vp3D->width, vp3D->height, self->frameBufferProps[0].supersampling);
}

//----------------------------------------------------------------------------
void qfeDriverDVR::qfeOnMouseMove(void *caller, int x, int y, int v)
{
  qfeDriverDVR *self = reinterpret_cast<qfeDriverDVR *>(caller);

  if(self->engine->GetInteractionControlKey())
  {
    cout << "window level stuff" << x << ", " << y << endl;
  }
  else
  {
    self->engine->CallInteractionParentMouseMove();
  }

}

//----------------------------------------------------------------------------
void qfeDriverDVR::qfeOnMouseLeftButtonDown(void *caller, int x, int y, int v)
{
  qfeDriverDVR *self = reinterpret_cast<qfeDriverDVR *>(caller);

  // Deal with the volume rendering - multiresolution
  self->quality = 1.0;
}

//----------------------------------------------------------------------------
void qfeDriverDVR::qfeOnMouseLeftButtonUp(void *caller, int x, int y, int v)
{
  qfeDriverDVR *self = reinterpret_cast<qfeDriverDVR *>(caller);

  // Deal with the volume rendering - multiresolution
  self->quality = 4.0;

  self->engine->Refresh();
}

//----------------------------------------------------------------------------
void qfeDriverDVR::qfeOnMouseRightButtonDown(void *caller, int x, int y, int v)
{
  qfeDriverDVR *self = reinterpret_cast<qfeDriverDVR *>(caller);

  if(self->engine->GetInteractionControlKey())
  {
    cout << "window level stuff" << endl;
  }
  else
  {
    self->engine->CallInteractionParentRightButtonDown();
  }
}
