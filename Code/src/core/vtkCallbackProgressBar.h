/**
* \file   vtkCallbackProgressBar.h
* \author Roy van Pelt
* \class  vtkCallbackProgressBar
* \brief  Implements a callback function to update a progress bar
*/

#ifndef _vtkCallbackProgressBar_h
#define _vtkCallbackProgressBar_h

#include <vtkCommand.h>

#include "qfeCallBackProgressBar.h"

class vtkCallbackProgressBar : public vtkCommand
{
public:
  static vtkCallbackProgressBar* New();

  virtual void SetProgressClass(qfeCallBackProgressBar *cb);
  virtual void Execute(vtkObject *, unsigned long, void *);

private:
  qfeCallBackProgressBar *callbackClass;

};

#endif // _vtkCallbackProgressBar_h