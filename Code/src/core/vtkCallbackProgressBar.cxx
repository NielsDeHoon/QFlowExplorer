#include "vtkCallbackProgressBar.h"

//----------------------------------------------------------------------------
vtkCallbackProgressBar* vtkCallbackProgressBar::New()
{
  vtkCallbackProgressBar *cb = new vtkCallbackProgressBar;
  return cb;
}

//----------------------------------------------------------------------------
void vtkCallbackProgressBar::Execute(vtkObject *caller, unsigned long eventId, void *callData)
{
  this->callbackClass->Execute(*((qfeCallData*)callData));
}

//----------------------------------------------------------------------------
void vtkCallbackProgressBar::SetProgressClass(qfeCallBackProgressBar *cb)
{
  this->callbackClass = cb;
}

