#include "qfeTransform.h"

//----------------------------------------------------------------------------
qfeTransform::qfeTransform()
{
}

//----------------------------------------------------------------------------
qfeTransform::~qfeTransform()
{

}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetMatrixWorldToViewportOrthogonal(qfeMatrix4f &W2VP, qfeFrame *geo, qfeViewport *vp)
{
  qfeFloat extent[3];
  double   scale, range[2], distance;  
  double   xmin, xmax, ymin, ymax;
  double   aspect;

  if(vp->width <= 0 || vp->height <= 0)
    aspect = 1;
  else
    aspect = vp->width/vp->height;

  geo->qfeGetFrameExtent(extent[0], extent[1], extent[2]);
  distance = (double)LARGEST_DISTANCE; 
  range[0] = distance;
  range[1] = distance * 3.0;
  scale    = extent[1]/2.0;

  xmin = -1.0*scale*aspect; ymin = -1.0*scale;
  xmax =  1.0*scale*aspect; ymax =  1.0*scale;

  qfeMatrix4f::qfeSetMatrixIdentity(W2VP);

  W2VP(0,0) =  2.0 / (xmax - xmin);
  W2VP(1,1) =  2.0 / (ymax - ymin);
  W2VP(2,2) = -2.0 / (range[1] - range[0]);

  W2VP(0,3) = -(xmin + xmax)/(xmax - xmin);
  W2VP(1,3) = -(ymin + ymax)/(ymax - ymin);
  W2VP(2,3) = -(range[0] + range[1])/(range[1] - range[0]);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetMatrixWorldToViewportPerspective(qfeMatrix4f &W2VP, qfeFrame *geo, double aspect)
{
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetMatrixViewportToWorldOrthogonal(qfeMatrix4f &VP2W, qfeFrame *geo, qfeViewport *vp)
{
  qfeGetMatrixWorldToViewportOrthogonal(VP2W, geo, vp);
  qfeMatrix4f::qfeGetMatrixInverse(VP2W, VP2W);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetMatrixViewportToWorldPerspective(qfeMatrix4f &VP2W, qfeFrame *geo, double aspect)
{
  qfeGetMatrixWorldToViewportPerspective(VP2W, geo, aspect);
  qfeMatrix4f::qfeGetMatrixInverse(VP2W, VP2W);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetMatrixPatientToWorld(qfeMatrix4f &P2W)
{
  // The matrix obtained from OpenGL is column-major.
  qfeFloat mat[16];
  glGetFloatv(GL_MODELVIEW_MATRIX , mat);
  qfeMatrix4f::qfeSetMatrixElements(P2W, mat);

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetMatrixWorldToPatient(qfeMatrix4f &W2P)
{
  qfeGetMatrixPatientToWorld(W2P);
  qfeMatrix4f::qfeGetMatrixInverse(W2P, W2P);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetMatrixVoxelToPatient(qfeMatrix4f &V2P, qfeVolume *volume)
{
  qfeMatrix4f   translateorigin, translate, rotate, scale;
  qfeGrid      *grid;

  unsigned int  s[3];
  qfePoint      o;
  qfeVector     e;
  GLfloat       g[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1};

  if(volume == NULL) return qfeError;

  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(s[0], s[1], s[2]);
  
  grid->qfeGetGridOrigin(o.x,o.y,o.z);
  grid->qfeGetGridExtent(e.x,e.y,e.z);
  grid->qfeGetGridAxes(g[0],g[1],g[2],g[4],g[5],g[6],g[8],g[9],g[10]);

  qfeMatrix4f::qfeSetMatrixIdentity(translateorigin);
  translateorigin(3,0) = -0.5*s[0]; 
  translateorigin(3,1) = -0.5*s[1]; 
  translateorigin(3,2) = -0.5*s[2];

  qfeMatrix4f::qfeSetMatrixIdentity(translate);
  translate(3,0) = o.x; 
  translate(3,1) = o.y; 
  translate(3,2) = o.z;

  qfeMatrix4f::qfeSetMatrixIdentity(rotate);
  qfeMatrix4f::qfeSetMatrixElements(rotate, &g[0]);

  qfeMatrix4f::qfeSetMatrixIdentity(scale);  
  scale(0,0) = e.x;
  scale(1,1) = e.y;
  scale(2,2) = e.z;

  qfeMatrix4f::qfeSetMatrixIdentity(V2P);
  
  V2P = translateorigin*scale*rotate*translate;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetMatrixPatientToVoxel(qfeMatrix4f &P2V, qfeVolume *volume)
{
  qfeGetMatrixVoxelToPatient(P2V, volume);
  qfeMatrix4f::qfeGetMatrixInverse(P2V, P2V);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetMatrixTextureToVoxel(qfeMatrix4f &T2V, qfeVolume *volume)
{
  unsigned int  s[3];
  qfeVector     e;

  volume->qfeGetVolumeSize(s[0], s[1], s[2]);

  qfeMatrix4f::qfeSetMatrixIdentity(T2V);

  // Scale
  T2V(0,0) = s[0];
  T2V(1,1) = s[1];
  T2V(2,2) = s[2];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetMatrixVoxelToTexture(qfeMatrix4f &V2T, qfeVolume *volume)
{
  qfeGetMatrixTextureToVoxel(V2T, volume);
  qfeMatrix4f::qfeGetMatrixInverse(V2T, V2T);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetCoordinateUnproject(qfePoint windowCoord, qfePoint &patientCoord, qfeMatrix4f mv, qfeMatrix4f p, qfeViewport *vp)
{
  qfePoint    coord;
  qfeMatrix4f mvpinv;

  qfeMatrix4f::qfeGetMatrixInverse(mv*p , mvpinv);

  coord.x = ((2.0f *(windowCoord.x-(float)vp->origin[0]))/(float)vp->width) -1;
  coord.y = ((2.0f *(windowCoord.y-(float)vp->origin[1]))/(float)vp->height) -1;
  coord.z =  (2.0f * windowCoord.z) -1;
  coord.w =   1.0f;

  //coord.x = ((2.0f *(windowCoord.x))/(float)vp.width) -1;
  //coord.y = ((2.0f *(windowCoord.y))/(float)vp.height) -1;
  //coord.z =  (2.0f * windowCoord.z) -1;
  //coord.w =   1.0f;

  patientCoord = coord * mvpinv;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeGetCoordinateProject(qfePoint patientCoord, qfePoint &windowCoord, qfeMatrix4f mv, qfeMatrix4f p, qfeViewport *vp)
{
  qfePoint    v;

  v = patientCoord*(mv*p);

  windowCoord.x = (float)vp->origin[0] + ((float)vp->width) *(v.x+1.0f)/2.0f;
  windowCoord.y = (float)vp->origin[1] + ((float)vp->height)*(v.y+1.0f)/2.0f;  
  windowCoord.z = (v.x+1.0f)/2.0f;
  windowCoord.w = 1.0f;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTransform::qfeTransformMeshToPatient(qfeVolume *volume, vtkPolyData *mesh, qfeMatrix4f &transform)
{
  qfeGrid     *grid;
  qfeFloat     extent[3];
  qfeFloat     origin[3];
  double       center[3];
  unsigned int dims[3];
  qfeFloat     axisX[3];
  qfeFloat     axisY[3];
  qfeFloat     axisZ[3];
  
  qfeMatrix4f O, R, T, Tinv, M;  

  // Get image properties
  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(dims[0],dims[1],dims[2]);
  grid->qfeGetGridAxes(axisX[0],axisX[1],axisX[2],axisY[0],axisY[1],axisY[2],axisZ[0],axisZ[1],axisZ[2]);
  grid->qfeGetGridOrigin(origin[0],origin[1],origin[2]);
  grid->qfeGetGridExtent(extent[0],extent[1],extent[2]);

  // Get mesh properties
  mesh->GetCenter(center);

  qfeFloat mO[16] = 
  { 1.0,  0.0,  0.0,   origin[0],
    0.0,  1.0,  0.0,   origin[1],
    0.0,  0.0,  1.0,   origin[2],
    0.0,  0.0,  0.0,   1.0 };

  qfeFloat mR[16] = 
  { axisX[0], axisX[1],  axisX[2],  0.0,
    axisY[0], axisY[1],  axisY[2],  0.0,
    axisZ[0], axisZ[1],  axisZ[2],  0.0,
    0.0,  0.0,  0.0,  1.0 };

  qfeFloat mT[16] = 
  {  1.0,  0.0,  0.0, (qfeFloat)center[0],
     0.0,  1.0,  0.0, (qfeFloat)center[1],
     0.0,  0.0,  1.0, (qfeFloat)center[2],
     0.0,  0.0,  0.0, 1.0 };  

  qfeMatrix4f::qfeSetMatrixElements(O,mO);
  qfeMatrix4f::qfeSetMatrixElements(R,mR);
  qfeMatrix4f::qfeSetMatrixElements(T,mT);

  qfeMatrix4f::qfeGetMatrixInverse(R,R);
  qfeMatrix4f::qfeGetMatrixInverse(T,Tinv);

  qfeMatrix4f::qfeSetMatrixIdentity(M);
  M = M*Tinv;
  M = R*Tinv;
  M = O*Tinv;

  transform = M;
  
/*
  vtkMatrix4x4 * O    = vtkMatrix4x4::New(); // Move to origin of slice / box in patient coordinates
  vtkMatrix4x4 * R    = vtkMatrix4x4::New(); // Rotate to patient coordinates    
  vtkMatrix4x4 * T    = vtkMatrix4x4::New(); // Move to volume center (should be equal to O^-1 + C, but that gives rounding errors?)
  vtkMatrix4x4 * Tinv = vtkMatrix4x4::New(); // Move to volume center (should be equal to O^-1 + C, but that gives rounding errors?)

  vtkMatrix4x4 * M    = vtkMatrix4x4::New(); 

  // Get image properties
  image->GetAxisX(axisX);
  image->GetAxisY(axisY);
  image->GetAxisZ(axisZ);
  image->GetOrigin(origin);
  image->GetDimensions(dims);
  image->GetSpacing(spacing);
  image->GetCenter(center);

  const double mO[] = { 1.0,  0.0,  0.0,   origin[0],
    0.0,  1.0,  0.0,   origin[1],
    0.0,  0.0,  1.0,   origin[2],
    0.0,  0.0,  0.0,   1.0 };

  const double mR[] = { axisX[0], axisX[1],  axisX[2],  0.0,
    axisY[0], axisY[1],  axisY[2],  0.0,
    axisZ[0], axisZ[1],  axisZ[2],  0.0,
    0.0,  0.0,  0.0,  1.0 };

  const double mTM[] = {1.0,  0.0,  0.0, center[0],
                        0.0,  1.0,  0.0, center[1],
                        0.0,  0.0,  1.0, center[2],
                        0.0,  0.0,  0.0, 1.0 };

  O->DeepCopy( mO );
  R->DeepCopy( mR ); 
  T->DeepCopy( mTM );

  R->Invert();

  vtkMatrix4x4::Invert(T, Tinv);

  // T S R P
  M->Identity();
  vtkMatrix4x4::Multiply4x4(Tinv,M,M); // Move the mesh center to the volume center - note: center != origin
  vtkMatrix4x4::Multiply4x4(R,M,M);
  vtkMatrix4x4::Multiply4x4(O,M,M);

  trans->SetMatrix(M);

  O->Delete();
  R->Delete();
  T->Delete();
  M->Delete();
  */

  return qfeSuccess;
}