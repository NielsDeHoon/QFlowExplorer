#include <QApplication>
#include <QCleanlooksStyle>
#include "gui/qtMainWindow.h"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  QApplication::setStyle(new QCleanlooksStyle());  
  qtMainWindow mainWindow;
  
  mainWindow.guiInitialize();

  return app.exec();
}
