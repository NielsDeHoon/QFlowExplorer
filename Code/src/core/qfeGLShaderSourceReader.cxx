#include "qfeGLShaderSourceReader.h"

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderSourceReader::qfeGetCwd(char **buffer)
{
  *buffer = (char *)calloc(_MAX_PATH, sizeof(char));
  _getcwd(*buffer, _MAX_PATH);
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderSourceReader::qfeLoadFromFile(char *path, char *file, const char **buffer)
{
  char *src;
  char *fullpath;

  fullpath = (char *)calloc((strlen(path)+strlen(file)+1), sizeof(char));
  strcpy(fullpath, path);
  strcat(fullpath, file);

	int count = 0;
	FILE *f = fopen(fullpath, "rt");
	if(f == NULL)
  {
    cout << "qfeGLShaderSourceReader::LoadFromFile - Could not open shader source" << file << endl;
    *buffer = NULL;
    return qfeError;
  }

	fseek(f, 0, SEEK_END);
	count = ftell(f);
	rewind(f);

	src = (char *) malloc((count+1)*sizeof(char));
	count = (int) fread(src, sizeof(char), count, f);
	src[count] = '\0';
	fclose(f);

  *buffer = src;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderSourceReader::qfeGetDefaultVertexShader(const char **buffer)
{
  char *src;
  char *tmp = "void main(void)\n{\ngl_Position = ftransform();\n}\n\0";

  src = (char *) malloc(54 * sizeof(char));
  memcpy(src, tmp, 54);

  *buffer = src;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderSourceReader::qfeGetDefaultGeometryShader(const char **buffer)
{
  char *src;
  char *tmp = "#version 120 \n #extension GL_EXT_geometry_shader4 : enable \n void main() { \n for(int i = 0; i < gl_VerticesIn; ++i) { \n gl_FrontColor = gl_FrontColorIn[i]; \n gl_Position = gl_PositionIn[i]; \n EmitVertex(); \n  } \n } ";
  
  src = (char*) malloc(223 * sizeof(char));
  memcpy(src, tmp, 223);

  *buffer = src;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderSourceReader::qfeGetDefaultFragmentShader(const char **buffer)
{
  char *src;
  char *tmp = "void main (void)\n{\ngl_FragColor = gl_Color; \n}\n\0";

  src = (char *) malloc(53*sizeof(char));  
  memcpy(src,tmp,53);

  *buffer = src;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderSourceReader::qfeGetDummyShader(const char **buffer)
{
  *buffer = "void dummy (void)\n{\n}\n\0";
  return qfeSuccess;
}