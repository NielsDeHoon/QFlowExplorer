#pragma once

#include "qflowexplorer.h"

#include "qfePoint.h"
#include "qfeVector.h"
#include "qfeTensor3f.h"
#include "qfeMatrix4f.h"
#include "qfeVolume.h"

#include <stdio.h>
#include <math.h>

#define QFE_ROTATE(a,i,j,k,l) g=a[i][j];h=a[k][l];a[i][j]=g-s*(h+g*tau);\
        a[k][l]=h+s*(g-h*tau)

/**
 * \file   qfeMath.h
 * \author Roy van Pelt
 * \class  qfeMath
 * \brief  Implements a 3x3 rank 2 tensor
 *
 * qfeMath provides a set of mathematical operations
 */


class qfeMath
{
public:
  qfeMath();
  ~qfeMath(){};

  static qfeReturnStatus qfeComputeGradientTensors(qfeVolume *volume, qfeTensor3f *gradients, bool useLinearRegression);
  static qfeReturnStatus qfeComputeLambda2s(qfeVolume *volume, float *lambda2s, float &rangeMin, float &rangeMax, bool discardNegLambda3);
  static qfeReturnStatus qfeComputeQCriterion(qfeVolume *volume, float *qcriterion, float &rangeMin, float &rangeMax);

  static qfeReturnStatus qfeComputeStructureTensor(qfeVector v, qfeTensor3f &t);
  static qfeReturnStatus qfeComputeAverageStructureTensor(qfeTensor3f neighborhood[8], qfeTensor3f &t);

  static qfeReturnStatus qfeComputeEigenSystem(qfeTensor3f t, qfeVector &lambda, qfeVector &v1, qfeVector &v2, qfeVector &v3);  
  static qfeReturnStatus qfeComputeEigenSystemVtk(qfeTensor3f t, qfeVector &lambda, qfeVector &v1, qfeVector &v2, qfeVector &v3);  
  static qfeReturnStatus qfeComputeEigenValues(qfeTensor3f t, qfeVector &lambda);

  static qfeReturnStatus qfeComputeCoherenceEDC(qfeTensor3f structuretensor, qfeFloat &coherence);

  static qfeReturnStatus qfeComputeDistancePointPlane(qfeVector n, qfePoint o, qfePoint p, qfeFloat &d);
  
  static qfeReturnStatus qfeComputeRank(qfeTensor3f &t, int &rank);
  // TODO: Jacobi functions should be in a template
  static qfeReturnStatus qfeComputeJacobi(double **a, double *w, double **v);
  static qfeReturnStatus qfeComputeJacobiN(double **a, int n, double *w, double **v);
  static qfeReturnStatus qfeComputeJacobiN(qfeFloat **a, int n, qfeFloat *w, qfeFloat **v);
  static qfeReturnStatus qfeSwapVectors(qfeVector& v1, qfeVector& v2);

  static qfeReturnStatus qfeMod(int x, int y, int &m);

protected:
 

};
