#pragma once

#include "qflowexplorer.h"

#include "qfeClusterHierarchy.h"
#include "qfeMeasurement.h"
#include "qfeModel.h"
#include "qfeProbe.h"
#include "qfeFrame.h"
#include "qfeVolume.h"
#include "qfeTransform.h"

#include "qfeImageData.h"
#include "qfeConvert.h"
#include "IO/qfeVTIImageDataWriter.h"

#include <vector>

#include <sstream>

//vtk includes
#include <vtkStructuredPoints.h>
#include <vtkContourFilter.h>
#include <vtkCleanPolyData.h>

//fluidsim includes (for generating a levelset from a mesh)
#include "fluidsim/array3.h"
#include "fluidsim/vec.h"
#include "fluidsim/makelevelset3.h"

//Qt UI includes
#include <QDialog.h>
#include <QDialogButtonBox.h>
#include <QCheckBox.h>
#include <qcombobox.h>
#include <QVBoxLayout>
#include <qmessagebox.h>

//plotting includes
#include "plot/qfeScatterPlot.h"
#include "plot/qfeTemporalGraphPlot.h"
#include "plot/qfeHistogramPlot.h"
#include "plot/qfeBlandAltmanPlot.h"
#include <qinputdialog.h>

//progress bar:
#include <QProgressDialog>

#define QFE_MAX_PHASES 400

typedef vector< qfeFrameSlice *>  qfePlaneSeries;
typedef vector< qfeModel  * >     qfeModelSeries;
typedef vector< qfeProbe3D >      qfeProbe3DSeries;
typedef vector< qfeProbe2D >      qfeProbe2DSeries;
typedef vector< qfePoint >        qfeVoxelArray;
typedef vector< qfeVoxelArray >   qfeVoxelSeries;
typedef vector< qfePoint >        qfeSeeds;

using namespace std;

/**
* \file   qfeStudy.h
* \author Roy van Pelt
* \class  qfeStudy
* \brief  Implements a study class
* \note   Confidential
*
* A study may contain a time series of anatomy, magnitude (PCA-M)
* and velocity (PCA-P) images;
*
*/
class qfeStudy
{
public:
  qfeStudy();
  ~qfeStudy();

  qfeVolumeSeries      ssfp3D;               // Additional anatomy scan
  qfeVolumeSeries      ssfp2D;               // Additional anatomy scan
  double               ssfp2D_phases;        // Number of cardiac phases

  qfeVolumeSeries      flow2D_ffe;           // Phase Contrast QFlow 2D slices  - Anatomy reconstruction   (FFE) 
  qfeVolumeSeries      flow2D_pcam;          // Phase Contrast QFlow 2D slices  - Magnitude reconstruction (PCA-M) = Complex Difference
  qfeVolumeSeries      flow2D_pcap;          // Phase Contrast QFlow 2D slices   - Phase reconstruction     (PCA-P) = Phase   Difference  
  int                  flow2D_phases;        // Number of cardiac phases  
  
  qfeVolumeSeries      ffe;                  // Phase Contrast Angiography - Anatomy reconstruction   (FFE) 
  qfeVolumeSeries      pcam;                 // Phase Contrast Angiography - Magnitude reconstruction (PCA-M) = Complex Difference
  qfeVolumeSeries      pcap;                 // Phase Contrast Angiography - Phase reconstruction     (PCA-P) = Phase   Difference
  qfeVolumeSeries      clusters;             // Clusters - time-dependent list of hierarchical clusters
  int                  phases;               // Number of cardiac phases
  qfeFloat             phaseDuration;        // Duration of cardiac phases in ms  
  
  qfeModelSeries       segmentation;         // Segmented structures
  
  qfeProbe2DSeries     probe2D;              // Set of 2D probes  
  qfeProbe3DSeries     probe3D;              // Set of 3D probes  
  qfeProbe3DSeries     probe3DCache;         // Set of 3D probes 

  qfeSeeds             seeds;                // Seeds loaded from file
  qfeSeeds             seedAttribs;          // Seed attribs (pattern z-value, maximum response, cluster ID)
  
  qfeClusterHierarchy *hierarchy;            // Full 4D flow hierarchy

  // TODO NDH: fill these structures
  qfeVolumeSeries      sim_pressures;        // Fluid simulation pressures
  qfeVolumeSeries      sim_velocities;       // Fluid simulation velocities
  qfeSeeds             sim_seeds;            // Fluid simulation particles per phase of the simulation
  int                  sim_phases;           // Fluid simulation number of cardiac phases
  qfeFloat             sim_phaseDuration;    // Fluid simulation duration of cardiac phases in ms  
  
  qfeVolume *          tmip;                 // Temporal Maximum Intensity Projection
  qfeVolume *          tmap;                 // Temporal Mean Angiography  Projection
  qfeVolume *          tmop;                 // Temporal Mean Orientation  Projection
  qfeVolumeSeries      ftle;                 // Finite Time Lyapunov Exponent fields
  qfeVolumeSeries      syntheticTestFlow;    // Synthetic test flow data set
  qfeVolumeSeries      curl;                 // Metric for rotation derived from PCA-P
  qfeVolumeSeries      flowSpeed;            // Vector lengths of PCA-P
  qfeVolumeSeries      lambda2;              // Lambda2 metric for vortex cores, derived from PCA-P
  qfeVolumeSeries      divergence;           // Outflow of a voxel, 0 (inflow = outflow) for incompressible fluids
  qfeVolumeSeries      qcriterion;           // Metric for vortex cores, derived from PCA-P

  qfeFloat *	       kernel;	             // Kernel for the first order Guassian derivative to compute the orientation of the probe

  void qfeGetStudyRangeSSFP(qfeFloat &vl, qfeFloat &vu) const;  
  void qfeGetStudyRangeFFE(qfeFloat &vl, qfeFloat &vu) const;
  void qfeGetStudyRangePCAM(qfeFloat &vl, qfeFloat &vu) const;
  void qfeGetStudyRangePCAP(qfeFloat &vl, qfeFloat &vu) const;  
  void qfeGetStudyRangeFTLE(qfeFloat &vl, qfeFloat &vu) const;

  //metrics computation:

  	enum dataID { dataID_diff, dataID_vol1, dataID_vol2 };

	typedef struct{
		std::string name;
		int metricType;
		bool diverging;
		bool needs_second_dataset;
		bool needs_solid;
		dataID dataID;
	} metric_descriptor;

	void qfeClearStoredData();

	void qfeGetScalarMetricList(std::vector<metric_descriptor> &metrics);	
	bool qfeScalarMetricExists(const string label);

	void qfeGetVectorMetricList(std::vector<metric_descriptor> &metrics);	
	bool qfeVectorMetricExists(const string label);

	void qfeGetExistingMetricList(std::vector<metric_descriptor> &metrics);
	void qfeGetExistingMetricList(std::vector<metric_descriptor> &metrics, bool scalar_only);

	metric_descriptor qfeGetScalarMetricProperties(std::string label);
	metric_descriptor qfeGetVectorMetricProperties(std::string label);

	//Returns if existing the metric associated with "label" for the currentphase
	void qfeGetScalarMetric(const string label, const float currentPhase, bool &existing_metric, Array3f &result, float &range_min, float &range_max);
	void qfeGetScalarMetric(const string label, const Array3f* solid, const float currentPhase, bool &existing_metric, Array3f &result, float &range_min, float &range_max);

	void qfeSetScalarMetric(const string label, const float currentPhase, Array3f &result);

	void qfeGetScalarVolume(const string label, const float currentPhase, bool &existing_metric, qfeVolume **result);
	void qfeGetScalarVolume(const string label, const Array3f* solid, const float currentPhase, bool &existing_metric, qfeVolume **result);

	void qfeGetVectorMetric(const string label, const float currentPhase, bool &existing_metric, Array3Vec3 &result);
	void qfeGetVectorMetric(const string label, const Array3f* solid, const float currentPhase, bool &existing_metric, Array3Vec3 &result);

	void qfeGetVectorVolume(const string label, const float currentPhase, bool &existing_metric, qfeVolume **result);
	void qfeGetVectorVolume(const string label, const Array3f* solid, const float currentPhase, bool &existing_metric, qfeVolume **result);

	void qfeGetScalarMetricStatistics(const string label, const Array3f* solid, const float currentPhase, bool &existing_metric, float &total, float &average, float &standard_deviation);
	void qfeGetMetricTotalInCells(const string label, const Array3f* solid, const Array3b* selected_cells, const float currentPhase, bool &existing_metric, float &total);
	void qfeGetMetricFluxInCells(const string label, const Array3f* solid, const Array3b* selected_cells, std::array<float,3> normal, const float currentPhase, bool &existing_metric, float &total);
	//returns the mean, standard deviation and time
	void qfeGetAverageOverTime(const string label, std::vector<std::array<float, 3>> &data);
	void qfeGetAverageOverTime(const string label, const Array3f* solid, std::vector<std::array<float, 3>> &data);

	void qfeGetSNR(const float inPhase, Array3f &result, float &sigma, float &range_min, float &range_max);

	//Returns a qfeModel of the isosurface in input for isoValue
	void qfeGetIsoSurface(qfeVolume *volume, Array3f *input, const float isoValue, qfeModel **model);
	void qfeGetIsoSurface(qfeVolume *volume, const Array3f* solid, Array3f *input, const float isoValue, qfeModel **model);
	void qfeGetIsoSurface(qfeVolume *volume, const float isoValue, qfeModel **model);

	void qfeRenderScalarMetric(const string label, const float currentPhase);
	void qfeRenderScalarMetric(const string label, const Array3f* solid, const float currentPhase);
	void qfeRenderVectorMetric(const string label, const float currentPhase);
	void qfeRenderVectorMetric(const string label, const Array3f* solid, const float currentPhase);	

	void qfeClearClosedPlots();
	void qfePlotData(const float currentPhase);

	void qfePlotScatterPlot(std::vector<string> labels, const Array3f* solid, const float currentPhase);
	void qfePlotTemporalGraph(std::vector<string> labels, const Array3f* solid);
	void qfePlotTemporalGraph(QVector<QString> labels, QList<QVector<QPair<double,double>>> dataSets);
	void qfePlotTemporalUncertainGraph(std::vector<string> labels, const Array3f* solid);
	void qfePlotHistogram(string label, const Array3f* solid, const float currentPhase);
	void qfePlotBlandAltman(const string label1, const string label2, const Array3f* solid, const float currentPhase);

	//progressbar:
	void qfeGetProgressBar(QString dialogTitle, int numberOfSteps);
	void qfeUpdateProgressBar(int currentStep);
	bool qfeWasCancelledProgressBar();
	void qfeClearProgressBar();

	qfeVolume qfeArray3fToQfeVolume(Array3f &array, float rangeMin, float rangeMax, const std::string &label);

	void qfeStoreAllMetricData(const float currentPhase, int fileNumber);
	void qfeStoreAllMetricData(const Array3f* solid, const float currentPhase, int fileNumber);

	void qfeStoreMetricData(const std::vector<std::string> labels,const float currentPhase, int fileNumber);
	void qfeStoreMetricData(const std::vector<std::string> labels, const Array3f* solid, const float currentPhase, int fileNumber);

	void qfeStoreUserSelectedMetrics();

	void qfeStoreCSVFile();

	//vector metrics:

	void qfeGetVelocityField(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3Vec3 &result);
	void qfeGetWallShearStress(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3Vec3 &result);
	void qfeGetCurl(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3Vec3 &result);
	void qfeGetQFlowVelocities(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3Vec3 &result);

	//scalar metrics:

	void qfeGetVelocityMagnitude(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetVelocityDivergence(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetAcceleration(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const float phaseDuration, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetWallShearStressMagnitude(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetCurlMagnitude(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetLambda2(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetQCriterion(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetSpatialIncoherence(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max);	
	void qfeGetSNR(const float inPhase, Array3f &result, float &range_min, float &range_max);

	void qfeSetSolidSDF(const float inPhase, const Array3f *solid);
	void qfeGetSolidSDF(const qfeVolumeSeries *volumes, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max);

	void qfeGetQFlowAnatomy(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max);

	void qfeGetDifferenceVelocityMagnitude(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetComparisonVelocityMagnitude(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetDifferenceVelocityAngles(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetDifferenceVelocitySquared(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetDifferenceDivergence(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetDifferenceAcceleration(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const float phaseDuration1, const float phaseDuration2, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetDifferenceWallShearStress(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max);
	void qfeGetDifferenceCurl(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max);

	void qfeGetCellVolume(const qfeVolumeSeries *volumes, const float inPhase, const Array3f *solid, const unsigned int subdivision, Array3f &fractions);
	
	void qfeGetCellVolume(const qfeVolumeSeries *volumes, const float inPhase, const Array3f *solid, std::vector<float> fraction_origin, unsigned int subdivision, Array3f& fractions);

	std::string qfeGetSeriesDataPath();

	void qfeSetSeriesDataFolder(std::string folderPath);
		
private:	
	enum scalarMetricType { VelocityMagnitude, Divergence, Acceleration, WSS, CurlMagnitude, Lambda2, QCriterion, VelocityMagnitudeDiff, VelocityMagnitudeComparison, VelocityAngleDiff, VelocitySquaredDiff, DivergenceDiff, AccelerationDiff, WSSDiff, CurlDiff, LevelSet, SpatialIncoherence, SNR, SolidSDF, QFlowAnatomy};
	enum vectorMetricType { Velocity, WallShearStress, Curl, QFlowVelocity };

	bool level_set_computed;
	Array3f mesh_level_set;
	
	typedef struct{
		scalarMetricType metricType;
		dataID dataID;
		float phase;
		Array3f data;
		float range_min;
		float range_max;
		qfeVolume volume;
	} scalar_metric_storage;
	
	typedef struct{
		vectorMetricType metricType;
		dataID dataID;
		float phase;
		Array3Vec3 data;
		float range_min;
		float range_max;
		qfeVolume volume;
	} vector_metric_storage;

	bool qfeScalarMetricPrecomputed(scalarMetricType metricType, dataID dataID, const float phase, Array3f &data, float &range_min, float &range_max);
	void qfeStoreComputedScalarMetric(scalarMetricType metricType, dataID dataID, const float phase, Array3f &data, float &range_min, float &range_max);

	bool qfeVectorMetricPrecomputed(vectorMetricType metricType, dataID dataID, const float phase, Array3Vec3 &data);
	void qfeStoreComputedVectorMetric(vectorMetricType metricType, dataID dataID, const float phase, Array3Vec3 &data);

	void qfeComputeLeveLSet();
	qfeReturnStatus loadMesh(vector<qfeModel*> modelSeries);
	qfeReturnStatus convertMeshToLevelSet(vtkPolyData *mesh);

	scalarMetricType qfeLabelToScalarMetricType(const std::string label);
	dataID qfeScalarLabelToDataID(const std::string label);
	std::string qfeScalarMetricTypeToLabel(scalarMetricType type);

	vectorMetricType qfeLabelToVectorMetricType(const std::string label);
	dataID qfeVectorLabelToDataID(const std::string label);
	std::string qfeVectorMetricTypeToLabel(vectorMetricType type);

	bool getValueTextureCoordinates(const std::array<float,3> textureCoords, const Array3b &input);
	float getValueTextureCoordinates(const std::array<float,3> textureCoords, const Array3f &input);
	std::array<float,3> getGradientTextureCoordinates(const std::array<float,3> textureCoords, const Array3f &input);

	std::array<float,3> getValueTextureCoordinates(const std::array<float,3> textureCoords, const Array3Vec3 &input);

	void extrapolateVectorField(Array3Vec3& grid, Array3b& valid);

	std::vector<scalar_metric_storage> stored_scalar_metrics;
	std::vector<vector_metric_storage> stored_vector_metrics;

	std::vector<std::string> UserSelectMetrics(bool initially_checked);
	std::vector<std::string> UserSelectMetrics(bool initially_checked, bool scalar_only);
	std::vector<std::string> UserSelectMetricsDropDown(unsigned int numberOfMetrics, bool scalar_only);

	//plots
	QVector<qfePlot*> plots;

	//progress bar
	QProgressDialog progressBar;

	std::string seriesDataPath;
};
