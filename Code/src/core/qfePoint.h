#pragma once
/**
 * \file   qfePoint.h
 * \author Roy van Pelt
 * \class  qfePoint
 * \brief  Implements a point.
 *
 * qfePoint provides a set of vector (x,y,z,w) operations
 */
#include "qflowexplorer.h"
#include "qfeVector.h"

using namespace std;

class qfePoint
{
  // Commutative operators
  friend const qfePoint operator  +  ( const qfePoint  & p1 , const qfePoint  & p2 );
  friend const qfeFloat operator  *  ( const qfePoint  & p1 , const qfePoint  & p2 );
 
public:
  qfeFloat x, y, z, w;

  qfePoint();
  qfePoint(qfeFloat x, qfeFloat y, qfeFloat z);
  virtual ~qfePoint();

  qfeReturnStatus qfeSetPointElements(qfeFloat v[4]);
  qfeReturnStatus qfeSetPointElements(qfeFloat x, qfeFloat y, qfeFloat z);
  qfeReturnStatus qfeSetPointElements(qfeFloat x, qfeFloat y, qfeFloat z, qfeFloat w);
  qfeReturnStatus qfeGetPointElements(qfeFloat v[4]);

  virtual qfePoint  operator  /  ( const qfePoint  & p ) const;
  virtual qfePoint  operator  +  ( const qfeVector & v ) const; 
  virtual qfeVector operator  -  ( const qfePoint  & p ) const;  
  virtual qfePoint  operator  -  ( const qfeVector & v ) const;  
  qfePoint operator / (const float scalar) const;
  qfePoint operator * (const float scalar) const;
  
  bool      operator  == ( const qfePoint & v ) const;
  bool      operator  != ( const qfePoint & v ) const;

  static qfeReturnStatus qfePointMultiply(qfeFloat f, qfePoint v, qfePoint &u);
  static qfeReturnStatus qfePointDivide(qfePoint v1, qfePoint v2, qfePoint &u);
  static qfeReturnStatus qfePointSubtract(qfePoint p1, qfePoint p2, qfeVector &u);
  static qfeReturnStatus qfePointAdd(qfePoint p1, qfePoint p2, qfePoint &u);
  static qfeReturnStatus qfePointDistance(qfePoint p1, qfePoint p2, float &distance);

  static qfeReturnStatus qfePointPrint(qfePoint p);
};
