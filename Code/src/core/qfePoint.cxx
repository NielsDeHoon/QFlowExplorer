#include "qfePoint.h"

//----------------------------------------------------------------------------
qfePoint::qfePoint()
{
  qfeSetPointElements(1.0,1.0,1.0);
}

//----------------------------------------------------------------------------
qfePoint::qfePoint(qfeFloat x, qfeFloat y, qfeFloat z)
{
  qfeSetPointElements(x,y,z);
}

//----------------------------------------------------------------------------
qfePoint::~qfePoint()
{
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePoint::qfeSetPointElements(qfeFloat v[4])
{
  this->x = v[0];
  this->y = v[1];
  this->z = v[2];
  this->w = v[3];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePoint::qfeSetPointElements(qfeFloat x, qfeFloat y, qfeFloat z)
{
  this->qfeSetPointElements(x, y, z, 1.0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePoint::qfeSetPointElements(qfeFloat x, qfeFloat y, qfeFloat z, qfeFloat w)
{
  this->x = x;
  this->y = y;
  this->z = z;
  this->w = w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePoint::qfeGetPointElements(qfeFloat v[4])
{
  v[0] = this->x;
  v[1] = this->y;
  v[2] = this->z;
  v[3] = this->w;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfePoint qfePoint::operator / ( const qfePoint &p) const
{
  qfePoint u;
  qfePointDivide((*this), p,  u);
  return u;
}

qfePoint qfePoint::operator*(const float scalar) const
{
  qfePoint p;
  p.x = x * scalar;
  p.y = y * scalar;
  p.z = z * scalar;
  return p;
}

qfePoint qfePoint::operator/(const float scalar) const
{
  qfePoint p;
  p.x = x / scalar;
  p.y = y / scalar;
  p.z = z / scalar;
  return p;
}

//----------------------------------------------------------------------------
const qfePoint operator  + ( const qfePoint &p1, const qfePoint &p2 )
{
  qfePoint u;
  qfePoint::qfePointAdd(p1, p2, u);
  return u;
}

//----------------------------------------------------------------------------
qfePoint qfePoint::operator + (const qfeVector &v ) const
{
  qfePoint u;
  u.x = (*this).x + v.x;
  u.y = (*this).y + v.y;
  u.z = (*this).z + v.z;
  u.w = (*this).w;
  return u;
}

//----------------------------------------------------------------------------
qfeVector qfePoint::operator - ( const qfePoint & p ) const
{
  qfeVector u;
  qfePointSubtract((*this), p, u);
  return u;
}

//----------------------------------------------------------------------------
qfePoint qfePoint::operator - ( const qfeVector & v ) const
{
  qfePoint u;
  u.x = (*this).x - v.x;
  u.y = (*this).y - v.y;
  u.z = (*this).z - v.z;
  u.w = (*this).w;
  return u;
}


//----------------------------------------------------------------------------
bool qfePoint::operator == ( const qfePoint & p ) const
{
  if((this->x == p.x) && (this->y == p.y) && (this->z == p.z)) return true;
  else return false;
}

//----------------------------------------------------------------------------
bool qfePoint::operator != ( const qfePoint & p ) const
{
  if((this->x == p.x) && (this->y == p.y) && (this->z == p.z)) return false;
  else return true;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePoint::qfePointMultiply(qfeFloat f, qfePoint p, qfePoint &u)
{
  u.x = f * p.x;
  u.y = f * p.y;
  u.z = f * p.z;
  u.w =     p.w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePoint::qfePointDivide(qfePoint p1, qfePoint p2, qfePoint &u)
{
  u.x = p1.x / p2.x;
  u.y = p1.y / p2.y;
  u.z = p1.z / p2.z;
  u.w = p1.w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePoint::qfePointSubtract(qfePoint p1, qfePoint p2, qfeVector &u)
{
  u.x = p1.x - p2.x;
  u.y = p1.y - p2.y;
  u.z = p1.z - p2.z;
  u.w = 0.0;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePoint::qfePointAdd(qfePoint p1, qfePoint p2, qfePoint &u)
{
  u.x = p1.x + p2.x;
  u.y = p1.y + p2.y;
  u.z = p1.z + p2.z;
  u.w = p1.w; 

  return qfeSuccess;
}

qfeReturnStatus qfePoint::qfePointDistance(qfePoint p1, qfePoint p2, float &distance)
{
	distance = sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y) + (p1.z-p2.z)*(p1.z-p2.z));

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePoint::qfePointPrint(qfePoint p)
{
  cout << "| " << p.x << ", " << p.y << ", " << p.z << ", " << p.w << " |";
  cout << endl;
  return qfeSuccess;
}