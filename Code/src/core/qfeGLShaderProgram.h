#pragma once

#include "qflowexplorer.h"

#include <gl/glew.h>

#include <stdio.h>
#include <stdlib.h>

#include <iostream>

#include "qfeGLShaderSourceReader.h"

#define QFE_VERTEX_SHADER   0
#define QFE_GEOMETRY_SHADER 1
#define QFE_FRAGMENT_SHADER 2
#define QFE_COMPUTE_SHADER 3

using namespace std;
/**
 * \file   qfeGLShaderProgram.h
 * \author Roy van Pelt
 * \class  qfeGLShaderProgram
 * \brief  Implements a GLSL shader program class
 *
 * qfeGLShaderProgram wraps common steps to create a
 * GLSL shader program
 */
class qfeGLShaderProgram
{
public:
  qfeGLShaderProgram();
  ~qfeGLShaderProgram();

  qfeReturnStatus qfeGetId(GLuint &id);

  qfeReturnStatus qfeAddShader(const char *shader, int shaderType);
  qfeReturnStatus qfeAddShaderFromFile(char *shaderFile, int shaderType);
  qfeReturnStatus qfeAddShaderFromFile(char *shaderPath, char *shaderFile, int shaderType); 

  qfeReturnStatus qfeGetShader(int index, int shaderType, GLuint &shaderId);
  qfeReturnStatus qfeGetShaderCount(int index, int shaderType, int &count);

  qfeReturnStatus qfeGetAttribLocation(const char* name, int &location);
  qfeReturnStatus qfeBindAttribLocation(const char* name, unsigned int location);

  qfeReturnStatus qfeBindFragDataLocation(const char*name, unsigned int location);

  qfeReturnStatus qfeLink();

  qfeReturnStatus qfeEnable();
  qfeReturnStatus qfeDisable();

  qfeReturnStatus qfeDispatchComputeShader(int nrGroupsX, int nrGroupsY, int nrGroupsZ);
  qfeReturnStatus qfeSyncComputeShader(GLbitfield barriers);

  qfeReturnStatus qfeSetUniform1i(const char *name, int u1);
  qfeReturnStatus qfeSetUniform2i(const char *name, int u1, int u2);
  qfeReturnStatus qfeSetUniform3i(const char *name, int u1, int u2, int u3);
  qfeReturnStatus qfeSetUniform1f(const char *name, float u1);
  qfeReturnStatus qfeSetUniform2f(const char *name, float u1, float u2);
  qfeReturnStatus qfeSetUniform3f(const char *name, float u1, float u2, float u3);
  qfeReturnStatus qfeSetUniform4f(const char *name, float u1, float u2, float u3, float u4);

  qfeReturnStatus qfeSetUniformMatrix4f(const char *name, int count, float *matrix);

  qfeReturnStatus qfeSetUniform1iv(const char *name, int count, int   *v);
  qfeReturnStatus qfeSetUniform3fv(const char *name, int count, float *v);

  qfeReturnStatus qfeSetGeometryShaderInOut(GLuint inType, GLuint outType, int verticesOut);
  qfeReturnStatus qfeSetTransformFeedbackVaryings(GLuint count, const char **varyings, GLenum bufferMode);

protected:

  GLuint     shaderProgram;
  GLuint    *vertexShaders;
  int        vertexShadersCount;
  GLuint    *geometryShaders;
  int        geometryShadersCount;
  GLuint    *fragmentShaders;
  int        fragmentShadersCount;
  GLuint    *computeShaders;
  int        computeShadersCount;

  void qfeCheckGLSL(unsigned int id, const char* name);

};
