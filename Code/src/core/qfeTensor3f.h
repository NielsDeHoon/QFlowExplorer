#pragma once
/**
 * \file   qfeTensor3f.h
 * \author Roy van Pelt
 * \class  qfeTensor3f
 * \brief  Implements a 3x3 rank 2 tensor
 *
 * qfeTensor3f provides a set of tensor operations
 * This tensor class works row-major
 *  xx xy xz
 *  yx yy yz
 *  zx zy zz
 */
#include <iostream>
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>

#include "qflowexplorer.h"

using namespace std;

class qfeTensor3f
{
public:
  qfeTensor3f();
  qfeTensor3f(qfeFloat *m);
  qfeTensor3f(double *m);
  ~qfeTensor3f(){};

  qfeFloat & operator  [] ( int index);
  qfeFloat &  operator () ( int row, int column );
  qfeFloat    operator () ( int row, int column ) const;
              operator float * ();
              operator const float * () const;
  qfeTensor3f operator * ( const qfeTensor3f & m ) const;
  qfeTensor3f operator * ( const qfeFloat    & s ) const;
  qfeTensor3f operator + ( const qfeTensor3f & m ) const;
  qfeTensor3f operator - ( const qfeTensor3f & m ) const;
  qfeTensor3f operator / ( const qfeFloat & s ) const;

  static qfeReturnStatus qfeSetTensorElements(qfeTensor3f &m, qfeFloat e[9]);
  static qfeReturnStatus qfeGetTensorElements(const qfeTensor3f &m, qfeFloat e[9]);

  static qfeReturnStatus qfeSetTensorIdentity(qfeTensor3f &m);
  static qfeReturnStatus qfeGetTensorDeterminant(const qfeTensor3f &m, qfeFloat &d);
  static qfeReturnStatus qfeGetTensorTranspose(const qfeTensor3f &m, qfeTensor3f &t);

  static qfeReturnStatus qfeTensorMultiply(const qfeTensor3f &m1, const qfeTensor3f &m2, qfeTensor3f &m);
  static qfeReturnStatus qfeTensorMultiply(const qfeTensor3f &m1, qfeFloat s, qfeTensor3f &m);
  static qfeReturnStatus qfeTensorDivide(const qfeTensor3f &m1, qfeFloat s, qfeTensor3f &m);
  static qfeReturnStatus qfeTensorAdd(const qfeTensor3f &m1, const qfeTensor3f &m2, qfeTensor3f &m);
  static qfeReturnStatus qfeTensorSubtract(const qfeTensor3f &m1, const qfeTensor3f &m2, qfeTensor3f &m);  

  static qfeReturnStatus qfeTensorPrint(const qfeTensor3f &m);

protected:
  qfeFloat tensor[9];

};
