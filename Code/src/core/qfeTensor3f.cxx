#include "qfeTensor3f.h"

//----------------------------------------------------------------------------
qfeTensor3f::qfeTensor3f()
{
  qfeSetTensorIdentity(*this);
}

//----------------------------------------------------------------------------
qfeTensor3f::qfeTensor3f(qfeFloat *m)
{
  qfeSetTensorElements(*this, m);
}

qfeTensor3f::qfeTensor3f(double *m)
{
  for (unsigned i = 0; i < 9; i++)
    tensor[i] = (float)m[i];
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTensor3f::qfeSetTensorElements(qfeTensor3f &m, qfeFloat e[9])
{
  for(int i=0; i<9; i++) m[i] = e[i];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTensor3f::qfeGetTensorElements(const qfeTensor3f &m, qfeFloat e[9])
{
  for(int i=0; i<9; i++) e[i] = m[i];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTensor3f::qfeSetTensorIdentity(qfeTensor3f &m)
{
  for(int i=0; i<9; i++) m[i] = 0.0;
  m[0] = m[4] = m[8] = 1.0;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTensor3f::qfeGetTensorDeterminant(const qfeTensor3f &m, qfeFloat &d)
{
  d = -m(0,2) * m(1,1) * m(2,0) + 
       m(0,1) * m(1,2) * m(2,0) + 
       m(0,2) * m(1,0) * m(2,1) - 
       m(0,0) * m(1,2) * m(2,1) - 
       m(0,1) * m(1,0) * m(2,2) +
       m(0,0) * m(1,1) * m(2,2);
    
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTensor3f::qfeGetTensorTranspose(const qfeTensor3f &m, qfeTensor3f &t)
{
  qfeFloat mat[9];
  mat[0] = m[0]; mat[1] = m[3]; mat[2] = m[6];
  mat[3] = m[1]; mat[4] = m[4]; mat[5] = m[7];
  mat[6] = m[2]; mat[7] = m[5]; mat[8] = m[8];  

  qfeSetTensorElements(t, &mat[0]);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeFloat & qfeTensor3f::operator [] ( int index )
{
  return this->tensor[index];
}

//----------------------------------------------------------------------------
qfeFloat & qfeTensor3f::operator () ( int row, int column )
{
  return this->tensor[3*row + column];
}

//----------------------------------------------------------------------------
qfeFloat qfeTensor3f::operator () ( int row, int column ) const
{
  return this->tensor[3*row + column];
}

//----------------------------------------------------------------------------
qfeTensor3f::operator float * ()
{
  return (qfeFloat *) &this->tensor[0];
}

//----------------------------------------------------------------------------
qfeTensor3f::operator const float * () const
{
  return (const qfeFloat *) &this->tensor[0];
}

//----------------------------------------------------------------------------
qfeTensor3f qfeTensor3f::operator * ( const qfeTensor3f & m ) const
{
  qfeTensor3f r;
  qfeTensor3f::qfeTensorMultiply(m, (*this),  r);
  return r;
}

//----------------------------------------------------------------------------
qfeTensor3f qfeTensor3f::operator * ( const qfeFloat & s ) const
{
  qfeTensor3f r;
  qfeTensor3f::qfeTensorMultiply((*this), s,  r);
  return r;
}

//----------------------------------------------------------------------------
qfeTensor3f qfeTensor3f::operator / ( const qfeFloat & s ) const
{
  qfeTensor3f r;
  qfeTensor3f::qfeTensorDivide((*this), s,  r);
  return r;
}

//----------------------------------------------------------------------------
qfeTensor3f qfeTensor3f::operator + ( const qfeTensor3f & m ) const
{
  qfeTensor3f r;
  qfeTensor3f::qfeTensorAdd(m, (*this),  r);
  return r;
}

//----------------------------------------------------------------------------
qfeTensor3f qfeTensor3f::operator - ( const qfeTensor3f & m ) const
{
  qfeTensor3f r;
  qfeTensor3f::qfeTensorSubtract(m, (*this),  r);
  return r;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTensor3f::qfeTensorMultiply(const qfeTensor3f &m1, const qfeTensor3f &m2, qfeTensor3f &m)
{
  qfeFloat mat[9];

  mat[0] = m1(0,0)*m2(0,0) + m1(0,1)*m2(1,0) + m1(0,2)*m2(2,0);
  mat[1] = m1(0,0)*m2(0,1) + m1(0,1)*m2(1,1) + m1(0,2)*m2(2,1);
  mat[2] = m1(0,0)*m2(0,2) + m1(0,1)*m2(1,2) + m1(0,2)*m2(2,2);

  mat[3] = m1(1,0)*m2(0,0) + m1(1,1)*m2(1,0) + m1(1,2)*m2(2,0);
  mat[4] = m1(1,0)*m2(0,1) + m1(1,1)*m2(1,1) + m1(1,2)*m2(2,1);
  mat[5] = m1(1,0)*m2(0,2) + m1(1,1)*m2(1,2) + m1(1,2)*m2(2,2);

  mat[6] = m1(2,0)*m2(0,0) + m1(2,1)*m2(1,0) + m1(2,2)*m2(2,0);
  mat[7] = m1(2,0)*m2(0,1) + m1(2,1)*m2(1,1) + m1(2,2)*m2(2,1);
  mat[8] = m1(2,0)*m2(0,2) + m1(2,1)*m2(1,2) + m1(2,2)*m2(2,2);

  qfeSetTensorElements(m, mat);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTensor3f::qfeTensorMultiply(const qfeTensor3f &m1, qfeFloat s, qfeTensor3f &m)
{
  qfeFloat mat[9];

  mat[0] = m1[0]*s;
  mat[1] = m1[1]*s;
  mat[2] = m1[2]*s;

  mat[3] = m1[3]*s;
  mat[4] = m1[4]*s;
  mat[5] = m1[5]*s;

  mat[6] = m1[6]*s;
  mat[7] = m1[7]*s;
  mat[8] = m1[8]*s;

  qfeSetTensorElements(m, mat);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTensor3f::qfeTensorDivide(const qfeTensor3f &m1, qfeFloat s, qfeTensor3f &m)
{
  qfeFloat mat[9];

  mat[0] = m1(0,0)/s;
  mat[1] = m1(0,1)/s;
  mat[2] = m1(0,2)/s;
  mat[3] = m1(1,0)/s;
  mat[4] = m1(1,1)/s;
  mat[5] = m1(1,2)/s;
  mat[6] = m1(2,0)/s;
  mat[7] = m1(2,1)/s;
  mat[8] = m1(2,2)/s;

  qfeSetTensorElements(m, mat);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTensor3f::qfeTensorAdd(const qfeTensor3f &m1, const qfeTensor3f &m2, qfeTensor3f &m)
{
  qfeFloat mat[9];

  mat[0] = m1(0,0)+m2(0,0);
  mat[1] = m1(0,1)+m2(0,1);
  mat[2] = m1(0,2)+m2(0,2);
  mat[3] = m1(1,0)+m2(1,0);
  mat[4] = m1(1,1)+m2(1,1);
  mat[5] = m1(1,2)+m2(1,2);
  mat[6] = m1(2,0)+m2(2,0);
  mat[7] = m1(2,1)+m2(2,1);
  mat[8] = m1(2,2)+m2(2,2);

  qfeSetTensorElements(m, mat);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTensor3f::qfeTensorSubtract(const qfeTensor3f &m1, const qfeTensor3f &m2, qfeTensor3f &m)
{
  qfeFloat mat[9];

  mat[0] = m1(0,0)-m2(0,0);
  mat[1] = m1(0,1)-m2(0,1);
  mat[2] = m1(0,2)-m2(0,2);
  mat[3] = m1(1,0)-m2(1,0);
  mat[4] = m1(1,1)-m2(1,1);
  mat[5] = m1(1,2)-m2(1,2);
  mat[6] = m1(2,0)-m2(2,0);
  mat[7] = m1(2,1)-m2(2,1);
  mat[8] = m1(2,2)-m2(2,2);

  qfeSetTensorElements(m, mat);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeTensor3f::qfeTensorPrint(const qfeTensor3f &m)
{
  cout << "| " << m[0]  << ", " << m[1]  << ", " << m[2]  << " |" << endl;
  cout << "| " << m[3]  << ", " << m[4]  << ", " << m[5]  << " |" << endl;
  cout << "| " << m[6]  << ", " << m[7]  << ", " << m[8]  << " |" << endl;

  cout << endl;
  return qfeSuccess;
}