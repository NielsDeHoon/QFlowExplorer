#include "qfeImageData.h"

#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"

vtkStandardNewMacro(qfeImageData);

//----------------------------------------------------------------------------
qfeImageData::qfeImageData()
{  
  this->PatientName      = NULL;
  this->PatientID        = "0";
  this->PhaseCount       = 1;
  this->PhaseDuration    = 40.0;
  this->PhaseTriggerTime = 0.0;
  this->ScanType         = qfeDataFlow4D;   
  this->ScanOrientation  = qfeSagittal;

  this->SetPatientName("Anonymous");

  this->SetAxisX(1,0,0);
  this->SetAxisY(0,1,0);
  this->SetAxisZ(0,0,1);

  this->SetVenc(0,0,0);
}

//----------------------------------------------------------------------------
qfeImageData::~qfeImageData()
{  
}

//----------------------------------------------------------------------------
void qfeImageData::DeepCopy(vtkDataObject *dataObject)
{
  qfeImageData *imageData = qfeImageData::SafeDownCast(dataObject);

  if ( imageData != NULL )
  {
    this->InternalImageDataCopy(imageData);
  }

  // Do superclass
  this->vtkImageData::DeepCopy(dataObject);
}

//----------------------------------------------------------------------------
// This copies all the local variables (but not objects).
void qfeImageData::InternalImageDataCopy(qfeImageData *src)
{
  //this->SetDataDescription(src->GetDataDescription());
  src->ShallowCopy(this);
  this->SetPatientName(src->GetPatientName());
  this->SetPatientID(src->GetPatientID());
  this->SetAxisX(src->GetAxisX());
  this->SetAxisY(src->GetAxisY());
  this->SetAxisZ(src->GetAxisZ());
  this->SetPhaseCount(src->GetPhaseCount());
  this->SetPhaseDuration(src->GetPhaseDuration());
  this->SetPhaseTriggerTime(src->GetPhaseTriggerTime());  
  this->SetVenc(src->GetVenc());
}

//----------------------------------------------------------------------------
qfeImageData* qfeImageData::GetData(vtkInformation* info)
{
  return info? static_cast<qfeImageData*>(info->Get(DATA_OBJECT())) : 0;
}

//----------------------------------------------------------------------------
qfeImageData* qfeImageData::GetData(vtkInformationVector* v, int i)
{
  return qfeImageData::GetData(v->GetInformationObject(i));
}



