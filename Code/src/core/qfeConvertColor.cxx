#include "qfeConvertColor.h"

const qfeColorXYZ qfeConvertColor::whiteD50 = { 0.9642f, 1.0f, 0.8249f };
const qfeColorXYZ qfeConvertColor::whiteD65 = { 0.9501548f, 1.0f, 1.0882598f };

//----------------------------------------------------------------------------
// rgb [0,1]
// h   [0,360] s [0,1] v [0,1]
qfeReturnStatus qfeConvertColor::qfeRgb2Hsv(qfeColorRGB in, qfeColorHSV &out)
{
  qfeFloat maxVal, minVal, delta;

  maxVal = max(max(in.r, in.g), in.b);
  minVal = min(min(in.r, in.g), in.b);

  out.v  = maxVal;

  delta = maxVal - minVal;

  if( maxVal != 0 )
    out.s = delta / maxVal;
  else {
    // r = g = b = 0		// s = 0, v is undefined
    out.s = 0;
    out.h = -1;
    return qfeError;
  }

  if( in.r == maxVal )
    out.h = 0 + ( in.g - in.b ) / delta;		// between yellow & magenta
  else
    if( in.g == maxVal )
      out.h = 2 + ( in.b - in.r ) / delta;	// between cyan & yellow
    else
      out.h = 4 + ( in.r - in.g ) / delta;	// between magenta & cyan

  out.h *= 60;				// degrees
  if( out.h < 0 )
    out.h += 360;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// h   [0,360] s [0,1] v [0,1]
// rgb [0,1]
qfeReturnStatus qfeConvertColor::qfeHsv2Rgb(qfeColorHSV in, qfeColorRGB &out)
{
  int i;
  float f, p, q, t;

  if( in.s == 0 ) {
    // achromatic (grey)
    out.r = out.g = out.b = in.v;
    return qfeSuccess;
  }

  in.h /= 60;			// sector 0 to 5
  i = (int)floor( in.h );
  f = in.h - i;			// factorial part of h
  p = in.v * ( 1 - in.s );
  q = in.v * ( 1 - in.s * f );
  t = in.v * ( 1 - in.s * ( 1 - f ) );

  switch( i ) {
    case 0: out.r = in.v; out.g = t; out.b = p;
      break;
    case 1: out.r = q; out.g = in.v; out.b = p;
      break;
    case 2: out.r = p; out.g = in.v; out.b = t;
      break;
    case 3: out.r = p; out.g = q; out.b = in.v;
      break;
    case 4: out.r = t; out.g = p; out.b = in.v;
      break;
    default:// case 5:
      out.r = in.v; out.g = p; out.b = q;
      break;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeConvertColor::qfeRgb2Xyz(qfeColorRGB in, qfeColorXYZ &out)
{
  qfeColorRGB in_linear;

  in_linear = linearize_sRGB(in);

  out.x = 0.4124564f * in_linear.r +
          0.3575761f * in_linear.g +
          0.1804375f * in_linear.b;

  out.y = 0.2126729f * in_linear.r +
          0.7151522f * in_linear.g +
          0.0721750f * in_linear.b;

  out.z = 0.0193339f * in_linear.r +
          0.1191920f * in_linear.g +
          0.9503041f * in_linear.b;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeConvertColor::qfeXyz2Rgb(qfeColorXYZ in, qfeColorRGB &out)
{
  qfeColorRGB out_linear;

  out_linear.r =  3.2404542f * in.x +
                 -1.5371385f * in.y +
                 -0.4985314f * in.z;

  out_linear.g = -0.9692660f * in.x +
                  1.8760108f * in.y +
                  0.0415560f * in.z;

  out_linear.b =  0.0556434f * in.x +
                 -0.2040259f * in.y +
                  1.0572252f * in.z;

  out = delinearize_sRGB(out_linear);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeConvertColor::qfeXyz2Lab(qfeColorXYZ in, qfeColorLab &out, const qfeColorXYZ white)
{
  out.L = 116.0f *  forwardLabTransform(in.y / white.y) - 16.0f;
  out.a = 500.0f * (forwardLabTransform(in.x / white.x) -
                    forwardLabTransform(in.y / white.y));
  out.b = 200.0f * (forwardLabTransform(in.y / white.y) -
                    forwardLabTransform(in.z / white.z));

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeConvertColor::qfeLab2Xyz(qfeColorLab in, qfeColorXYZ &out, const qfeColorXYZ white)
{
  const qfeFloat delta = 6.0f / 29.0f;
  qfeColorXYZ f;

  f.y = (in.L + 16.0f) / 116.0f;
  f.x = f.y + in.a / 500.0f;
  f.z = f.y - in.b / 200.0f;

  if (f.y > delta) {
    out.y = white.y * pow(f.y, 3.0f);
  } else {
    out.y = (f.y - 16.0f / 116.0f) *
      3.0f * pow(delta, 2.0f) * white.y;
  }

  if (f.x > delta) {
    out.x = white.x * pow(f.x, 3.0f);
  } else {
    out.x = (f.x - 16.0f / 116.0f) *
      3.0f * pow(delta, 2.0f) * white.x;
  }

  if (f.z > delta) {
    out.z = white.z * pow(f.z, 3.0f);
  } else {
    out.z = (f.z - 16.0f / 116.0f) *
      3.0f * pow(delta, 2.0f) * white.z;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeConvertColor::qfeXyz2Luv(qfeColorXYZ in, qfeColorLuv &out, const qfeColorXYZ white)
{
  const qfeFloat epsilon = 0.008856f;
  const qfeFloat kappa   = 903.3f;

  if (in.y > epsilon) {
    out.L = 116.0f * pow(in.y / white.y, 1.0f / 3.0f) - 16.0f;
  } else {
    out.L = kappa * in.y / white.y;
  }

  qfeFloat in_sum    = in.x + 15.0f * in.y + 3.0f * in.z;
  qfeFloat u_prime   = 4.0f * in.x / in_sum;
  qfeFloat v_prime   = 9.0f * in.y / in_sum;
  qfeFloat white_sum = white.x + 15.0f * white.y + 3.0f * white.z;
  qfeFloat u_prime_r = 4.0f * white.x / white_sum;
  qfeFloat v_prime_r = 9.0f * white.y / white_sum;

  if (in_sum == 0.0) {
    out.u = 0.0f;
    out.v = 0.0f;
  } else {
    out.u = 13.0f * out.L * (u_prime - u_prime_r);
    out.v = 13.0f * out.L * (v_prime - v_prime_r);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeConvertColor::qfeLuv2Xyz(qfeColorLuv in, qfeColorXYZ &out, const qfeColorXYZ white)
{
  const qfeFloat epsilon = 0.008856f;
  const qfeFloat kappa   = 903.3f;

  if (in.L > kappa * epsilon) {
    out.y = pow((in.L + 16.0f) / 116.0f, 3.0f);
  } else {
    out.y = in.L / kappa;
  }

  qfeFloat u_zero = 4.0f * white.x / (white.x + 15.0f * white.y + 3.0f * white.z);
  qfeFloat v_zero = 9.0f * white.y / (white.x + 15.0f * white.y + 3.0f * white.z);

  qfeFloat a = 1.0f / 3.0f * ((52.0f * in.L) / (in.u + 13.0f * in.L * u_zero) - 1.0f);
  qfeFloat b = -5.0f * out.y;
  qfeFloat c = -1.0f / 3.0f;
  qfeFloat d = out.y * ((39.0f * in.L) / (in.v + 13.0f * in.L * v_zero) - 5.0f);

  if(in.u == 0.0f && in.L == 0.0f) a = 0.0f;
  if(in.v == 0.0f && in.L == 0.0f) d = 0.0f;

  out.x = (d - b) / (a - c);
  out.z = out.x * a + b;

  if(a-c == 0.0f) out.x = 0.0f;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeColorRGB qfeConvertColor::lerp(qfeColorRGB value1, qfeColorRGB value2, qfeFloat amount)
{
  qfeColorRGB linear;

  linear.r = lerp(value1.r, value2.r, amount);
  linear.g = lerp(value1.g, value2.g, amount);
  linear.b = lerp(value1.b, value2.b, amount);

  return linear;
}

//----------------------------------------------------------------------------
qfeColorLab qfeConvertColor::lerp(qfeColorLab value1, qfeColorLab value2, qfeFloat amount)
{
  qfeColorLab linear;

  linear.L = lerp(value1.L, value2.L, amount);
  linear.a = lerp(value1.a, value2.a, amount);
  linear.b = lerp(value1.b, value2.b, amount);

  return linear;
}

//----------------------------------------------------------------------------
qfeColorLuv qfeConvertColor::lerp(qfeColorLuv value1, qfeColorLuv value2, qfeFloat amount)
{
  qfeColorLuv linear;

  linear.L = lerp(value1.L, value2.L, amount);
  linear.u = lerp(value1.u, value2.u, amount);
  linear.v = lerp(value1.v, value2.v, amount);

  return linear;
}

//----------------------------------------------------------------------------
qfeFloat qfeConvertColor::lerp(qfeFloat value1, qfeFloat value2, qfeFloat amount)
{
  return (value1 * (1.0f - amount) + value2  * amount);
}

//----------------------------------------------------------------------------
qfeColorRGB qfeConvertColor::clamp(qfeColorRGB in)
{
  qfeColorRGB out;
  out.r = clamp(in.r);
  out.g = clamp(in.g);
  out.b = clamp(in.b);
  return out;  
}

//----------------------------------------------------------------------------
qfeFloat qfeConvertColor::clamp(qfeFloat in)
{
  qfeFloat out;
  out = (in <0.0f)  ? 0.0f : in;
  out = (out>1.0f) ? 1.0f : out;
  return out;  
}

//----------------------------------------------------------------------------
qfeFloat qfeConvertColor::linearize_sRGB_component(qfeFloat in)
{
  const qfeFloat a = 0.055f;

  if (in <= 0.04045f) {
    return in / 12.92f;
  } else {
    return pow((in + a) / (1.0f + a), 2.4f);
  }
}

//----------------------------------------------------------------------------
qfeFloat qfeConvertColor::delinearize_sRGB_component(qfeFloat in)
{
  const qfeFloat a = 0.055f;

  if (in <= 0.0031308f) {
    return in * 12.92f;
  } else {
    return (1.0f + a) * pow(in, 1.0f/2.4f) - a;
  }
}

//----------------------------------------------------------------------------
qfeColorRGB qfeConvertColor::linearize_sRGB(const qfeColorRGB &in)
{
  qfeColorRGB out;

  out.r = linearize_sRGB_component(in.r);
  out.g = linearize_sRGB_component(in.g);
  out.b = linearize_sRGB_component(in.b);

  return out;
}

//----------------------------------------------------------------------------
qfeColorRGB qfeConvertColor::delinearize_sRGB(const qfeColorRGB &in)
{
  qfeColorRGB out;

  out.r = delinearize_sRGB_component(in.r);
  out.g = delinearize_sRGB_component(in.g);
  out.b = delinearize_sRGB_component(in.b);

  return out;
}

//----------------------------------------------------------------------------
qfeFloat qfeConvertColor::forwardLabTransform(qfeFloat in)
{
  if (in > pow(6.0f / 29.0f, 3.0f))
    return pow(in, 1.0f / 3.0f);
  else
    return 1.0f / 3.0f * pow(29.0f / 6.0f, 2.0f) * in + 4.0f / 29.0f;
}
