#pragma once
/**
 * \file   qfeMatrix4f.h
 * \author Roy van Pelt
 * \class  qfeMatrix4f
 * \brief  Implements a row-major matrix.
 *
 * qfeMatrix4f provides a set of matrix operations
 * This matrix class works row-major
 *  xx xy xz 0
 *  yx yy yz 0
 *  zx zy zz 0
 *  ox oy oz 1
 *
 * OpenGL works with a column-major matrix
 *  xx yx zx ox
 *  xy yy zy oy
 *  xz yz zz oz
 *   0  0  0  1
 * Note that memory layout is equal for DX and OpenGL
 * (I.e.: Translations on m[12], m[13] and m[14]
 */
#include <iostream>
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>

#include "qflowexplorer.h"

#include "qfePoint.h"
#include "qfeVector.h"

using namespace std;

class qfeMatrix4f
{

  friend const qfeVector operator  * ( const qfeVector   & v, const qfeMatrix4f & m );    
  friend const qfeVector operator  * ( const qfeMatrix4f & m, const qfeVector   & v ); 

  friend const qfePoint  operator  * ( const qfePoint    & p, const qfeMatrix4f & m ); 
  friend const qfePoint  operator  * ( const qfeMatrix4f & m, const qfePoint    & p ); 

public:
  qfeMatrix4f();
  qfeMatrix4f(qfeFloat *m);
  ~qfeMatrix4f(){};

  qfeFloat & operator  [] ( int index);
  qfeFloat &  operator () ( int row, int column );
  qfeFloat    operator () ( int row, int column ) const;
              operator float * ();
              operator const float * () const;
  qfeMatrix4f operator * ( const qfeMatrix4f & m ) const;

  static qfeReturnStatus qfeSetMatrixElements(qfeMatrix4f &m, qfeFloat e[16]);
  static qfeReturnStatus qfeGetMatrixElements(qfeMatrix4f  m, qfeFloat e[16]);

  static qfeReturnStatus qfeSetMatrixIdentity(qfeMatrix4f &m);
  static qfeReturnStatus qfeGetMatrixDeterminant(qfeMatrix4f m, qfeFloat &d);
  static qfeReturnStatus qfeGetMatrixInverse(qfeMatrix4f m, qfeMatrix4f &i);
  static qfeReturnStatus qfeGetMatrixTranspose(qfeMatrix4f m, qfeMatrix4f &t);

  static qfeReturnStatus qfeMatrixSubtract(qfeMatrix4f m1, qfeMatrix4f m2, qfeMatrix4f &m);

  static qfeReturnStatus qfeMatrixMultiply(qfeMatrix4f m1, qfeMatrix4f m2, qfeMatrix4f &m);
  
  static qfeReturnStatus qfeMatrixPreMultiply(qfePoint  p1, qfeMatrix4f m1, qfePoint    &p);
  static qfeReturnStatus qfeMatrixPostMultiply(qfeMatrix4f m1, qfePoint  p1, qfePoint    &p);

  static qfeReturnStatus qfeMatrixPreMultiply(qfeVector v1, qfeMatrix4f m1, qfeVector   &v);  
  static qfeReturnStatus qfeMatrixPostMultiply(qfeMatrix4f m1, qfeVector v1, qfeVector  &v);
  

  static qfeReturnStatus qfeMatrixPrint(qfeMatrix4f m);

protected:
  qfeFloat matrix[16];

};
