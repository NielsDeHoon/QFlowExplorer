#include "qfeGLShaderProgram.h"

//----------------------------------------------------------------------------
qfeGLShaderProgram::qfeGLShaderProgram()
{
  this->vertexShaders        = NULL;
  this->vertexShadersCount   = 0;
  this->geometryShaders      = NULL;
  this->geometryShadersCount = 0;
  this->fragmentShaders      = NULL;
  this->fragmentShadersCount = 0;
  computeShaders             = nullptr;
  computeShadersCount        = 0;

  GLenum err = glewInit();
  if (GLEW_OK != err)
  {
    cout << "qfeGLShaderProgram::qfeGLShaderProgram - " << glewGetErrorString(err) << endl;
  }

  this->shaderProgram        = glCreateProgram();
}

//----------------------------------------------------------------------------
qfeGLShaderProgram::~qfeGLShaderProgram()
{
  this->qfeDisable();

  for(int i=0; i<this->vertexShadersCount; i++)
  {
    glDetachShader(this->shaderProgram, this->vertexShaders[i]);
    glDeleteShader(this->vertexShaders[i]);
  }
  for(int i=0; i<this->geometryShadersCount; i++)
  {
    glDetachShader(this->shaderProgram, this->geometryShaders[i]);
    glDeleteShader(this->geometryShaders[i]);
  }
  for(int i=0; i<this->fragmentShadersCount; i++)
  {
    glDetachShader(this->shaderProgram, this->fragmentShaders[i]);
    glDeleteShader(this->fragmentShaders[i]);
  }
  for (int i = 0; i < computeShadersCount; i++) {
    glDetachShader(shaderProgram, computeShaders[i]);
    glDeleteShader(computeShaders[i]);
  }

  if (glIsProgram(this->shaderProgram))
    glDeleteProgram(this->shaderProgram);

  if(this->vertexShaders != NULL) 
  {
    free(this->vertexShaders);
    this->vertexShaders = NULL;
  }
  if(this->geometryShaders != NULL)
  {
    free(this->geometryShaders);
    this->geometryShaders = NULL;
  }
  if(this->fragmentShaders != NULL)
  {
    free(this->fragmentShaders);
    this->fragmentShaders = NULL;
  }
  if (computeShaders) {
    free(computeShaders);
    computeShaders = nullptr;
  }
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeGetId(GLuint &id)
{
  id = this->shaderProgram;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeAddShader(const char *shader, int shaderType)
{
  int id;

  switch(shaderType)
  {
  case QFE_VERTEX_SHADER :    // Compile
                              id = glCreateShader(GL_VERTEX_SHADER);
                              glShaderSource(id, 1, &shader, 0);
                              glCompileShader(id);

                              this->vertexShaders =
                                (GLuint*)realloc(this->vertexShaders,
                                (this->vertexShadersCount+1) * sizeof(GLuint));

                              this->vertexShaders[this->vertexShadersCount] = id;

                              this->vertexShadersCount++;

                              // Attach
                              glAttachShader(this->shaderProgram, id);

                              this->qfeCheckGLSL(id, "vertex shader");
                              break;
  case QFE_GEOMETRY_SHADER :  // Compile
                              id = glCreateShader(GL_GEOMETRY_SHADER_EXT);
                              glShaderSource(id, 1, &shader, 0);
                              glCompileShader(id);

                              this->geometryShaders =
                                (GLuint*)realloc(this->geometryShaders,
                                (this->geometryShadersCount+1) * sizeof(GLuint));

                              this->geometryShaders[this->geometryShadersCount] = id;

                              this->geometryShadersCount++;

                              // Attach
                              glAttachShader(this->shaderProgram, id);

                              this->qfeCheckGLSL(id, "geometry shader");                              
                              break;
  case QFE_FRAGMENT_SHADER :  // Compile
                              id = glCreateShader(GL_FRAGMENT_SHADER);
                              glShaderSource(id, 1, &shader, 0);
                              glCompileShader(id);

                              this->fragmentShaders =
                                (GLuint*)realloc(this->fragmentShaders,
                                (this->fragmentShadersCount+1) * sizeof(GLuint));

                              this->fragmentShaders[this->fragmentShadersCount] = id;

                              this->fragmentShadersCount++;

                              // Attach
                              glAttachShader(this->shaderProgram, id);

                              this->qfeCheckGLSL(id, "fragment shader");
                              break;
  case QFE_COMPUTE_SHADER :
    // Compile
    id = glCreateShader(GL_COMPUTE_SHADER);
    glShaderSource(id, 1, &shader, 0);
    glCompileShader(id);

    computeShaders = (GLuint*)realloc(computeShaders, (computeShadersCount + 1)*sizeof(GLuint));
    computeShaders[computeShadersCount] = id;
    computeShadersCount++;

    // Attach
    glAttachShader(shaderProgram, id);

    qfeCheckGLSL(id, "compute shader");
    break;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeAddShaderFromFile(char *shaderFile,  int shaderType)
{
  cout << shaderFile << endl;
  return this->qfeAddShaderFromFile(NULL, shaderFile, shaderType);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeAddShaderFromFile(char *shaderPath, char *shaderFile,  int shaderType)
{
  // TODO verify the path first
  const char *src;
  qfeReturnStatus  status;

  if(shaderPath == NULL)
  {
    char            *cwd;    

    qfeGLShaderSourceReader::qfeGetCwd(&cwd);
    status = qfeGLShaderSourceReader::qfeLoadFromFile(cwd, shaderFile, &src);

    if(status == qfeError) src = NULL;    
  }
  else
  {
    status = qfeGLShaderSourceReader::qfeLoadFromFile(shaderPath, shaderFile, &src);

    if(status == qfeError) src = NULL;
  }

  if(src == NULL)
  {
    switch(shaderType)
    {
    case QFE_VERTEX_SHADER   : qfeGLShaderSourceReader::qfeGetDefaultVertexShader(&src);
                               break;
    case QFE_GEOMETRY_SHADER : qfeGLShaderSourceReader::qfeGetDefaultGeometryShader(&src);
                               break;
    case QFE_FRAGMENT_SHADER : qfeGLShaderSourceReader::qfeGetDefaultFragmentShader(&src);
                               break;
    default                  : qfeGLShaderSourceReader::qfeGetDummyShader(&src);
    }
  }

  status = this->qfeAddShader(src, shaderType);
    
  if(src != NULL) free((void *)src);

  return status;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeGetShader(int index, int shaderType, GLuint &shaderId)
{
  switch(shaderType)
  {
  case QFE_VERTEX_SHADER :    if(index < this->vertexShadersCount)
                                shaderId = this->vertexShaders[index];
                              else return qfeError;
  case QFE_GEOMETRY_SHADER :  if(index < this->geometryShadersCount)
                                shaderId = this->geometryShaders[index];
                              else return qfeError;
  case QFE_FRAGMENT_SHADER :  if(index < this->fragmentShadersCount)
                                shaderId = this->fragmentShaders[index];
                              else return qfeError;
  case QFE_COMPUTE_SHADER:
    if (index < computeShadersCount)
      shaderId = computeShaders[index];
    else
      return qfeError;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeGetAttribLocation(const char* name, int &location)
{
  if(this->shaderProgram == NULL) return qfeError;
  
  location = glGetAttribLocation(this->shaderProgram, name);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeBindAttribLocation(const char* name, unsigned int location)
{
  if(this->shaderProgram == NULL) return qfeError;

  glBindAttribLocation(this->shaderProgram, location, name);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeBindFragDataLocation(const char* name, unsigned int location)
{
  if(this->shaderProgram == NULL) return qfeError;

  glBindFragDataLocation(this->shaderProgram, location, name);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeSetTransformFeedbackVaryings(GLuint count, const char **varyings, GLenum bufferMode)
{  
  glTransformFeedbackVaryings(this->shaderProgram, count, varyings, bufferMode); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeLink()
{
  glLinkProgram(this->shaderProgram);

  this->qfeCheckGLSL(this->shaderProgram, "shader program");

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeEnable()
{
  glUseProgram(this->shaderProgram);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeDisable()
{
  glUseProgram(0);

  return qfeSuccess;
}

qfeReturnStatus qfeGLShaderProgram::qfeDispatchComputeShader(int nrGroupsX, int nrGroupsY, int nrGroupsZ) {
  glDispatchCompute(nrGroupsX, nrGroupsY, nrGroupsZ);
  return qfeSuccess;
}

qfeReturnStatus qfeGLShaderProgram::qfeSyncComputeShader(GLbitfield barriers) {
  glMemoryBarrier(barriers);
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeSetUniform1i(const char *name, int u1)
{
  glUseProgram(this->shaderProgram);
  glUniform1i(glGetUniformLocation(this->shaderProgram, name), u1);
  glUseProgram(0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeSetUniform2i(const char *name, int u1, int u2)
{
  glUseProgram(this->shaderProgram);
  glUniform2i(glGetUniformLocation(this->shaderProgram, name), u1, u2);
  glUseProgram(0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeSetUniform3i(const char *name, int u1, int u2, int u3)
{
  glUseProgram(this->shaderProgram);
  glUniform3i(glGetUniformLocation(this->shaderProgram, name), u1, u2, u3);
  glUseProgram(0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeSetUniform1f(const char *name, float u1)
{
  glUseProgram(this->shaderProgram);
  glUniform1f(glGetUniformLocation(this->shaderProgram, name), u1);
  glUseProgram(0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeSetUniform2f(const char *name, float u1, float u2)
{
  glUseProgram(this->shaderProgram);
  glUniform2f(glGetUniformLocation(this->shaderProgram, name), u1, u2);
  glUseProgram(0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeSetUniform3f(const char *name, float u1, float u2, float u3)
{
  glUseProgram(this->shaderProgram);
  glUniform3f(glGetUniformLocation(this->shaderProgram, name), u1, u2, u3);
  glUseProgram(0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeSetUniform4f(const char *name, float u1, float u2, float u3, float u4)
{
  glUseProgram(this->shaderProgram);
  glUniform4f(glGetUniformLocation(this->shaderProgram, name), u1, u2, u3, u4);
  glUseProgram(0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeSetUniform1iv(const char *name, int count, int *v)
{
  glUseProgram(this->shaderProgram);
  glUniform1iv(glGetUniformLocation(this->shaderProgram, name), count, v);
  glUseProgram(0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeSetUniform3fv(const char *name, int count, float *v)
{
  glUseProgram(this->shaderProgram);
  glUniform3fv(glGetUniformLocation(this->shaderProgram, name), count, v);
  glUseProgram(0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLShaderProgram::qfeSetUniformMatrix4f(const char *name, int count, float *matrix)
{
  glUseProgram(this->shaderProgram);
  glUniformMatrix4fv(glGetUniformLocation(this->shaderProgram, name), count, false, matrix);
  glUseProgram(0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// IN:  GL_POINTS, GL_LINES_ADJECENCY, GL_TRIANGLES, GL_TRIANGLES_ADJECENTY
// OUT: GL_POINTS, GL_LINE_STRIP, GL_TRIANGLE_STRIP
qfeReturnStatus qfeGLShaderProgram::qfeSetGeometryShaderInOut(GLuint inType, GLuint outType, int verticesOut)
{
   glProgramParameteriEXT(this->shaderProgram,
    GL_GEOMETRY_INPUT_TYPE_EXT  , inType);
  glProgramParameteriEXT(this->shaderProgram,
    GL_GEOMETRY_OUTPUT_TYPE_EXT , outType);
  glProgramParameteriEXT(this->shaderProgram,
    GL_GEOMETRY_VERTICES_OUT_EXT, verticesOut);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeGLShaderProgram::qfeCheckGLSL(unsigned int id, const char* name)
{
  int len = 0;
  int written = 0;
  char *log;

  if(glIsProgram(id)){
    glGetProgramiv(id, GL_INFO_LOG_LENGTH, &len);
    if(len < 2)
      return;

    log = (char *) malloc(len*sizeof(char));
    glGetProgramInfoLog(id, len, &written, log);
  }
  else {
    glGetShaderiv(id, GL_INFO_LOG_LENGTH, &len);
    if(len < 2)
      return;

    log = (char *) malloc(len*sizeof(char));
    glGetShaderInfoLog(id, len, &written, log);
  }

  if(log){
    cout << name << ": " << endl << log << endl;
    //free(log);
  }
}