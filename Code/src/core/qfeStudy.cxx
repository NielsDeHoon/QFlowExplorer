#include "qfeStudy.h"

#include "qfeImageData.h"
#include "qfeConvert.h"
#include "io/qfeVTIImageDataWriter.h"

//----------------------------------------------------------------------------
qfeStudy::qfeStudy()
{ 
  this->ssfp2D.clear();
  this->ssfp3D.clear();
  
  this->flow2D_ffe.clear();  
  this->flow2D_pcam.clear();
  this->flow2D_pcap.clear();

  this->ffe.clear();
  this->pcam.clear();
  this->pcap.clear();
  this->clusters.clear();
  
  this->segmentation.clear();  

  this->sim_pressures.clear();
  this->sim_velocities.clear();
  this->sim_seeds.clear();

  this->seeds.clear();
  this->seedAttribs.clear();

  this->phases            = 0;
  this->phaseDuration     = 0.0;  
  this->flow2D_phases     = 0;
  this->ssfp2D_phases     = 0; 
  this->sim_phases        = 0;
  this->sim_phaseDuration = 0.0;

  this->tmip          = NULL;
  this->tmap          = NULL;
  this->tmop          = NULL;
  this->ftle.clear();

  this->kernel        = NULL;
  
  this->hierarchy     = new qfeClusterHierarchy();

  level_set_computed = false;

  stored_scalar_metrics.clear();
  stored_vector_metrics.clear();

  plots.clear();
}  

//----------------------------------------------------------------------------
qfeStudy::~qfeStudy()
{
  qfeVolumeSeries::iterator    i;  
  qfeModelSeries::iterator     j;
  
  if(this->tmip!=NULL && !pcam.empty() && this->tmip!=this->pcam.front() && !pcap.empty() && this->tmip!=this->pcap.front()) 
	delete this->tmip;

  if(this->tmap != NULL) delete this->tmap;

  if(this->tmop != NULL) delete this->tmop;

  if(this->kernel != NULL) delete this->kernel;

  if(this->hierarchy != NULL) delete this->hierarchy;

  for(i = this->ssfp2D.begin()          ; i < this->ssfp2D.end()          ; i++) {delete *i; *i = NULL;};
  for(i = this->ssfp3D.begin()          ; i < this->ssfp3D.end()          ; i++) {delete *i; *i = NULL;};
  for(i = this->flow2D_ffe.begin()      ; i < this->flow2D_ffe.end()      ; i++) {delete *i; *i = NULL;};
  for(i = this->flow2D_pcam.begin()     ; i < this->flow2D_pcam.end()     ; i++) {delete *i; *i = NULL;};
  for(i = this->flow2D_pcap.begin()     ; i < this->flow2D_pcap.end()     ; i++) {delete *i; *i = NULL;};
  for(i = this->ffe.begin()             ; i < this->ffe.end()             ; i++) {delete *i; *i = NULL;};
  for(i = this->pcam.begin()            ; i < this->pcam.end()            ; i++) {delete *i; *i = NULL;};
  for(i = this->pcap.begin()            ; i < this->pcap.end()            ; i++) {delete *i; *i = NULL;};  
  for(i = this->clusters.begin()        ; i < this->clusters.end()        ; i++) {delete *i; *i = NULL;};  
  for(i = this->sim_pressures.begin()   ; i < this->sim_pressures.end()   ; i++) {delete *i; *i = NULL;};  
  for(i = this->sim_velocities.begin()  ; i < this->sim_velocities.end()  ; i++) {delete *i; *i = NULL;};  
  for(j = this->segmentation.begin()    ; j < this->segmentation.end()    ; j++) {delete *j; *j = NULL;};
  for(i = this->ftle.begin()            ; i < this->ftle.end()            ; i++) {delete *i; *i = NULL;};  

  this->ffe.clear();
  this->flow2D_ffe.clear();
  this->flow2D_pcam.clear();
  this->flow2D_pcap.clear();
  this->pcam.clear();
  this->pcap.clear();
  this->sim_pressures.clear();
  this->sim_velocities.clear();  
  this->clusters.clear();
  this->segmentation.clear();
  this->sim_seeds.clear();
  this->seeds.clear();
  this->seedAttribs.clear();
  this->ftle.clear();

  plots.clear();

  stored_scalar_metrics.clear();
  stored_vector_metrics.clear();
}

void qfeStudy::qfeClearStoredData()
{
  level_set_computed = false;

  plots.clear();

  stored_scalar_metrics.clear();
  stored_vector_metrics.clear();
}

//----------------------------------------------------------------------------
void qfeStudy::qfeGetStudyRangeSSFP(qfeFloat &vl, qfeFloat &vu) const
{
  qfeFloat minL = 0.0;
  qfeFloat maxU = 0.0;

  for(int i=0; i<(int)this->ssfp3D.size(); i++)
  {
	qfeFloat l = 0.0;
	qfeFloat u = 0.0;

	this->ssfp3D[i]->qfeGetVolumeValueDomain(l, u);

	if(l < minL) minL = l;
	if(u > maxU) maxU = u;
  }

  for(int i=0; i<(int)this->ssfp2D.size(); i++)
  {
	qfeFloat l = 0.0;
	qfeFloat u = 0.0;

	this->ssfp2D[i]->qfeGetVolumeValueDomain(l, u);

	if(l < minL) minL = l;
	if(u > maxU) maxU = u;
  }

  vl = minL;
  vu = maxU;
}

//----------------------------------------------------------------------------
void qfeStudy::qfeGetStudyRangeFFE(qfeFloat &vl, qfeFloat &vu) const
{
  qfeFloat minL = 0.0;
  qfeFloat maxU = 0.0;

  for(int i=0; i<(int)this->ffe.size(); i++)
  {
	qfeFloat l = 0.0;
	qfeFloat u = 0.0;

	this->ffe[i]->qfeGetVolumeValueDomain(l, u);

	if(l < minL) minL = l;
	if(u > maxU) maxU = u;
  }

  for(int i=0; i<(int)this->flow2D_ffe.size(); i++)
  {
	qfeFloat l = 0.0;
	qfeFloat u = 0.0;

	this->flow2D_ffe[i]->qfeGetVolumeValueDomain(l, u);

	if(l < minL) minL = l;
	if(u > maxU) maxU = u;
  }
  
  if((minL == 0.0) && (maxU==0.0))
  {
	minL =    0.0;
	maxU = 4096.0;
  }

  vl = minL;
  vu = maxU;
}

//----------------------------------------------------------------------------
void qfeStudy::qfeGetStudyRangePCAM(qfeFloat &vl, qfeFloat &vu) const
{
  qfeFloat minL = 0.0;
  qfeFloat maxU = 0.0;

  for(int i=0; i<(int)this->pcam.size(); i++)
  {
	qfeFloat l = 0.0;
	qfeFloat u = 0.0;

	this->pcam[i]->qfeGetVolumeValueDomain(l, u);

	if(l < minL) minL = l;
	if(u > maxU) maxU = u;
  }

  for(int i=0; i<(int)this->flow2D_pcam.size(); i++)
  {
	qfeFloat l = 0.0;
	qfeFloat u = 0.0;

	this->flow2D_pcam[i]->qfeGetVolumeValueDomain(l, u);

	if(l < minL) minL = l;
	if(u > maxU) maxU = u;
  }

  /*if((minL == 0.0) && (maxU==0.0))
  {
	minL = 0.0;
	maxU = 1.0;
  }*/

  vl = minL;
  vu = maxU;
}

//----------------------------------------------------------------------------
void qfeStudy::qfeGetStudyRangePCAP(qfeFloat &vl, qfeFloat &vu) const
{
  qfeFloat minL = 0.0;
  qfeFloat maxU = 0.0;

  for(int i=0; i<(int)this->pcap.size(); i++)
  {
	qfeFloat l = 0.0;
	qfeFloat u = 0.0;

	this->pcap[i]->qfeGetVolumeValueDomain(l, u);

	if(l < minL) minL = l;
	if(u > maxU) maxU = u;
  }

  for(int i=0; i<(int)this->flow2D_pcap.size(); i++)
  {
	qfeFloat l = 0.0;
	qfeFloat u = 0.0;

	this->flow2D_pcap[i]->qfeGetVolumeValueDomain(l, u);

	if(l < minL) minL = l;
	if(u > maxU) maxU = u;
  }

  if((minL == 0.0) && (maxU==0.0))
  {
	minL = -200.0;
	maxU =  200.0;
  }

  vl = minL;
  vu = maxU;
}

void qfeStudy::qfeGetStudyRangeFTLE(qfeFloat &vl, qfeFloat &vu) const
{
  qfeFloat minL = 0.0;
  qfeFloat maxU = 0.0;

  for(int i=0; i<(int)this->ftle.size(); i++)
  {
	qfeFloat l = 0.0;
	qfeFloat u = 0.0;

	this->ftle[i]->qfeGetVolumeValueDomain(l, u);

	if(l < minL) minL = l;
	if(u > maxU) maxU = u;
  }

  if((minL == 0.0) && (maxU==0.0))
  {
	minL =  0;
	maxU =  200.0;
  }

  vl = minL;
  vu = maxU;
}

void qfeStudy::qfeRenderScalarMetric(const string label, const float currentPhase)
{
	unsigned w, h, d;
	if(pcap.size()<=0)
		return;
	pcap.front()->qfeGetVolumeSize(w, h, d);
	Array3f mock_solid(w, h, d);
	mock_solid.assign(w, h, d, 1.0f);

	this->qfeRenderScalarMetric(label, &mock_solid, currentPhase);
}

void qfeStudy::qfeRenderScalarMetric(const string label, const Array3f* solid, const float currentPhase)
{
	qfeMatrix4f   T2V, V2P;

	qfeVolume *volume = pcap[(int) currentPhase];

	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

	Array3f scalarField;

	bool existingMetric = false;

	float range_min = 0.0f;
	float range_max = 0.0f;

	this->qfeGetScalarMetric(label,solid,currentPhase,existingMetric,scalarField, range_min, range_max);

	if(existingMetric)
	{
		float scalar;

		for(unsigned int k = 0; k<scalarField.nk; k++)
		for(unsigned int j = 0; j<scalarField.nj; j++)
		for(unsigned int i = 0; i<scalarField.ni; i++)
		{
			scalar = scalarField.at(i,j,k);

			qfePoint texCoords((float)i/scalarField.ni,(float)j/scalarField.nj,(float)k/scalarField.nk);

			qfePoint voxCoords = texCoords * T2V;

			qfePoint patCoords = voxCoords * V2P;

			glPointSize(1.0f + 2.0f*fabs(scalar)/range_max);

			glBegin(GL_POINTS);
				glVertex3f(patCoords.x, patCoords.y, patCoords.z);
			glEnd();
		}
	}
}

void qfeStudy::qfeRenderVectorMetric(const string label, const float currentPhase)
{
	unsigned w, h, d;
	pcap.front()->qfeGetVolumeSize(w, h, d);
	Array3f mock_solid(w, h, d);
	mock_solid.assign(w, h, d, 1.0f);

	this->qfeRenderVectorMetric(label, &mock_solid, currentPhase);
}

void qfeStudy::qfeRenderVectorMetric(const string label, const Array3f* solid, const float currentPhase)
{
	qfeMatrix4f   T2V, V2P;
	
	qfeVolume *volume = pcap[(int) currentPhase];

	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

	Array3Vec3 vectorField;
	Array3f scalarField;

	bool existingMetric = false;

	this->qfeGetVectorMetric(label,solid,currentPhase,existingMetric,vectorField);

	float range_min, range_max;
	std::string scalar_label = label;
	scalar_label.insert(label.length()-2," magnitude"); //get the magnitude metric of the vector field
	this->qfeGetScalarMetric(scalar_label,solid,currentPhase,existingMetric,scalarField, range_min, range_max);

	if(existingMetric)
	{
		std::array<float,3> vector;

		for(unsigned int k = 0; k<vectorField.nk; k++)
		for(unsigned int j = 0; j<vectorField.nj; j++)
		for(unsigned int i = 0; i<vectorField.ni; i++)
		{
			if(i%4<3 || j%4<3 || k%4<3)
				continue;

			vector = vectorField.at(i,j,k);

			qfeVector vec;
			vec.x = vector[0]; vec.y = vector[1]; vec.z = vector[2];
			vec = vec * V2P;
			
			float magnitude = scalarField.at(i,j,k);

			glColor3f(magnitude/range_max,magnitude/range_max,magnitude/range_max);

			qfePoint texCoords((float)i/vectorField.ni,(float)j/vectorField.nj,(float)k/vectorField.nk);

			if(solid->at(texCoords.x*solid->ni,texCoords.y*solid->nj,texCoords.z*solid->nk) < 0)
				continue;			
			
			qfePoint patCoords = (texCoords * T2V) * V2P;

			glBegin(GL_LINES);
				glVertex3f(patCoords.x, patCoords.y, patCoords.z);
				glVertex3f(patCoords.x+vec.x/range_max*10.0f, patCoords.y+vec.y/range_max*10.0f, patCoords.z+vec.z/range_max*10.0f);
			glEnd();
		}
	}
}

void qfeStudy::qfeClearClosedPlots()
{
	int i = plots.size()-1;
	while(i>=0)
	{
		if(i>=0 && i<plots.size() && plots[i]->isClosed())
		{
			plots.remove(i);
		}
		i--;
	}
}

void qfeStudy::qfePlotData(const float currentPhase)
{
	qfeClearClosedPlots();

	QVector<QString> plotTypes;
	plotTypes.push_back("Scatter plot");
	plotTypes.push_back("Temporal graph");
	plotTypes.push_back("Temporal uncertain graph");
	plotTypes.push_back("Histogram");
	plotTypes.push_back("Bland Altman");

	QDialog dlg;
	QVBoxLayout *mainLayout = new QVBoxLayout;
	
	QComboBox *comboBox = new QComboBox();
	for(unsigned int i = 0; i<plotTypes.size(); i++)
	{
		comboBox->addItem(plotTypes.at(i));
	}
	mainLayout->addWidget(comboBox);

	QDialogButtonBox *buttonBoxControl = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	QObject::connect(buttonBoxControl, SIGNAL(accepted()), &dlg, SLOT(accept()));
	QObject::connect(buttonBoxControl, SIGNAL(rejected()), &dlg, SLOT(reject()));

	mainLayout->addWidget(buttonBoxControl);

	dlg.setLayout(mainLayout);

	int selected = 0;
	if(dlg.exec() == QDialog::Accepted)
	{
		selected = comboBox->currentIndex();
	}
	else
	{
		return;
	}

	int numberOfLabelsNeeded = -1;
	if(selected == 0) numberOfLabelsNeeded = 2;
	if(selected == 1) numberOfLabelsNeeded = -1;
	if(selected == 2) numberOfLabelsNeeded = -1;
	if(selected == 3) numberOfLabelsNeeded = 1;
	if(selected == 4) numberOfLabelsNeeded = 2;

	std::vector<std::string> labels;
	if(numberOfLabelsNeeded <= 0)
		labels = this->UserSelectMetrics(false, true);
	else
		labels = this->UserSelectMetricsDropDown(numberOfLabelsNeeded, true);

	bool mesh_exists = false;
	if(segmentation.size()>0)
		mesh_exists = true;

	Array3f *solid;	

	if(mesh_exists && level_set_computed == false)
	{
		this->qfeComputeLeveLSet();

		 if(mesh_level_set.ni > 1 && mesh_level_set.nj > 1 && mesh_level_set.nk > 1)
		 {
			level_set_computed = true;
			solid = &this->mesh_level_set;
		 }
	}
	if(true == level_set_computed)
	{
		solid = &this->mesh_level_set;
	}
	else
	{
		unsigned w, h, d;
		pcap.front()->qfeGetVolumeSize(w, h, d);
		solid = new Array3f(w, h, d);
		solid->assign(w, h, d, 1.0f);
	}

	if(selected == 0 && labels.size()>0)
	{
		this->qfePlotScatterPlot(labels, solid, currentPhase);
	}

	if(1 == selected && labels.size()>0)
	{
		this->qfePlotTemporalGraph(labels, solid);
	}

	if(2 == selected)
	{
		this->qfePlotTemporalUncertainGraph(labels, solid);
	}

	if(3 == selected && labels.size()>0)
	{
		this->qfePlotHistogram(labels.at(0), solid, currentPhase);
	}

	if(4 == selected && labels.size()>=2)
	{
		this->qfePlotBlandAltman(labels[0], labels[1], solid, currentPhase);
	}
}

void qfeStudy::qfePlotScatterPlot(std::vector<string> labels, const Array3f* solid, const float currentPhase)
{
	qfeClearClosedPlots();

	QList<QPair<QPair<QVector<double>,QVector<double>>,QPair<QString,QString>>> dataSets;

	bool existingMetric = false;

	float range_min = 0.0f;
	float range_max = 0.0f;

	Array3f scalarField1;
	Array3f scalarField2;

	bool existingMetric1 = false;
	bool existingMetric2 = false;
	
	qfeScatterPlot *plot = new qfeScatterPlot();

	if(labels.size()<2)
		return;
	
	this->qfeGetScalarMetric(labels.at(0),solid, currentPhase, existingMetric1, scalarField1, range_min, range_max);
	this->qfeGetScalarMetric(labels.at(1),solid, currentPhase, existingMetric2, scalarField2, range_min, range_max);
	
	if(existingMetric1 && existingMetric2)
	{		
		QVector<QPair<double,double>> points;
		
		int xmin = min(scalarField1.ni, scalarField2.ni);
		int ymin = min(scalarField1.nj, scalarField2.nj);
		int zmin = min(scalarField1.nk, scalarField2.nk);
		
		points.resize(xmin * ymin * zmin);
		
		unsigned int index = 0;
		for(unsigned int k = 0; k<zmin; k++)
		for(unsigned int j = 0; j<ymin; j++)
		for(unsigned int i = 0; i<xmin; i++)
		{
			points[index].first = scalarField1(i,j,k);
			points[index].second = scalarField2(i,j,k);
			index++;
		}

		QPair<QString, QString> names;
		names.first =  QString::fromUtf8(labels.at(0).c_str());
		names.second = QString::fromUtf8(labels.at(1).c_str()) + QString(" Phase (") + QString::number(currentPhase)+")";
		
		if(points.size()>0)
		{
			plot->setData(points,names);
			plot->showWindow();
			
			plots.push_back(plot);
		}
	}
}

void qfeStudy::qfePlotTemporalGraph(std::vector<string> labels, const Array3f* solid)
{
	qfeClearClosedPlots();

	QList<QVector<QPair<double,double>>> dataSets;
	QVector<QString> passLabels;

	bool existingMetric = false;

	float range_min = 0.0f;
	float range_max = 0.0f;
	
	qfeTemporalGraphPlot *plot = new qfeTemporalGraphPlot();

	for(unsigned int i = 0; i<labels.size(); i++)
	{
		std::vector<std::array<float, 3>> data;

		this->qfeGetAverageOverTime(labels.at(i), solid, data);

		if(data.size()>0)
		{
			QVector<QPair<double,double>> linePoints;

			for(unsigned int p = 0; p<data.size(); p++)
			{
				QPair<double, double> linePoint;
				linePoint.first = data.at(p)[2];
				linePoint.second = data.at(p)[0];

				linePoints.push_back(linePoint);
			}
			dataSets.push_back(linePoints);
			passLabels.push_back(labels.at(i).c_str());
		}
	}

	if(dataSets.size()>0)
	{
		plot->setData(dataSets, passLabels);
		plot->showWindow();

		plots.push_back(plot);
	}
}

void qfeStudy::qfePlotTemporalGraph(QVector<QString> labels, QList<QVector<QPair<double,double>>> dataSets)
{
	qfeClearClosedPlots();

	if(labels.size() != dataSets.size())
		return;

	if(dataSets.size()>0)
	{
		qfeTemporalGraphPlot *plot = new qfeTemporalGraphPlot();

		plot->setData(dataSets, labels);
		plot->showWindow();

		plots.push_back(plot);
	}
}

void qfeStudy::qfePlotTemporalUncertainGraph(std::vector<string> labels, const Array3f* solid)
{
	qfeClearClosedPlots();

	QList<QVector<TemporalGraphStatistic>> dataSets;
	QVector<QString> data_labels;

	bool existingMetric = false;

	float range_min = 0.0f;
	float range_max = 0.0f;
	
	qfeTemporalGraphPlot *plot = new qfeTemporalGraphPlot();

	for(unsigned int i = 0; i<labels.size(); i++)
	{
		std::vector<std::array<float, 3>> data;

		this->qfeGetAverageOverTime(labels.at(i), solid, data);

		if(data.size()>0)
		{
			QVector<TemporalGraphStatistic> linePoints;

			for(unsigned int p = 0; p<data.size(); p++)
			{
				TemporalGraphStatistic linePoint;
				linePoint.average = data.at(p)[0];
				linePoint.standard_deviation = data.at(p)[1];
				linePoint.time = data.at(p)[2];

				linePoints.push_back(linePoint);
			}
			QVector<TemporalGraphStatistic> graph;

			graph = linePoints;

			dataSets.push_back(graph);
			data_labels.push_back(QString::fromUtf8(labels.at(i).c_str()));
		}
	}

	if(dataSets.size()>0)
	{
		plot->setData(dataSets, data_labels);
		plot->showWindow();

		plots.push_back(plot);
	}
}

void qfeStudy::qfePlotHistogram(string label, const Array3f* solid, const float currentPhase)
{
	qfeClearClosedPlots();
	
	int number_of_classes = 0;

	QInputDialog dlg;
	dlg.setInputMode(QInputDialog::IntInput);
	dlg.setLabelText("Number of classes:");
	dlg.setIntValue(10);
	dlg.setIntMinimum(1);
	dlg.setIntMaximum(200);
	if(dlg.exec() == QDialog::Accepted)
	{
		number_of_classes = dlg.intValue();
	}
	else
	{
		return;
	}

	if(number_of_classes<=0)
		return;

	bool existingMetric = false;

	float range_min = 0.0f;
	float range_max = 0.0f;

	Array3f scalarField;
		
	this->qfeGetScalarMetric(label,solid, currentPhase, existingMetric, scalarField, range_min, range_max);

	if(existingMetric)
	{
		qfeHistogramPlot *plot = new qfeHistogramPlot();

		unsigned int valid_cells = 0;
				
		double mean = 0.0;
		double variance = 0.0;

		for(unsigned int k = 0; k<scalarField.nk; k++)
		for(unsigned int j = 0; j<scalarField.nj; j++)
		for(unsigned int i = 0; i<scalarField.ni; i++)
		{
			float si = (float)i/scalarField.ni * (float)solid->ni;
			float sj = (float)j/scalarField.nj * (float)solid->nj;
			float sk = (float)k/scalarField.nk * (float)solid->nk;
			
			if(solid->interpolate_value(si,sj,sk) > 0) //we are not in the solid, so this cell should be counted
			{
				valid_cells++;
				mean += scalarField(i,j,k);
			}
		}
		if(valid_cells > 0)
			mean /= valid_cells;
		else
		{
			std::cout<<"No valid cells to compute mean and variance from..."<<std::endl;
			mean = 0.0;
		}

		QVector<double> data(valid_cells);

		unsigned int index = 0;
		for(unsigned int k = 0; k<scalarField.nk; k++)
		for(unsigned int j = 0; j<scalarField.nj; j++)
		for(unsigned int i = 0; i<scalarField.ni; i++)
		{
			float si = (float)i/scalarField.ni * (float)solid->ni;
			float sj = (float)j/scalarField.nj * (float)solid->nj;
			float sk = (float)k/scalarField.nk * (float)solid->nk;
			if(solid->interpolate_value(si,sj,sk) > 0 && index<data.size()) //we are not in the solid, so this cell should be counted
			{
				data[index] = scalarField(i,j,k);
				index++;

				variance += pow(mean - scalarField(i,j,k), 2.0);
			}
		}

		variance /= valid_cells;

		std::cout<<"\n================"<<std::endl;
		std::cout<<"Mean:               "<<mean<<std::endl;
		std::cout<<"Sigma:              "<<sqrt(variance)<<std::endl;
		std::cout<<"Variance (sigma^2): "<<variance<<std::endl;
		std::cout<<"================"<<std::endl;

		QPair<double, double> range(range_min, range_max);

		plot->setData(data, number_of_classes, range, label.c_str() + QString(" (Phase '") + QString::number(currentPhase)+"')");
		plot->showWindow();

		plots.push_back(plot);
	}
}

void qfeStudy::qfePlotBlandAltman(const string label1, const string label2, const Array3f* solid, const float currentPhase)
{
	qfeClearClosedPlots();

	Array3f scalarField1;
	Array3f scalarField2;

	bool existingMetric1 = false;
	bool existingMetric2 = false;

	float range_min = 0.0f;
	float range_max = 0.0f;

	this->qfeGetScalarMetric(label1,solid,currentPhase,existingMetric1,scalarField1, range_min, range_max);
	this->qfeGetScalarMetric(label2,solid,currentPhase,existingMetric2,scalarField2, range_min, range_max);
	
	if(existingMetric1 && existingMetric2)
	{
		qfeBlandAltmanPlot *plot = new qfeBlandAltmanPlot();

		QVector<double> data1;
		QVector<double> data2;
		
		int xmin = min(scalarField1.ni, scalarField2.ni);
		int ymin = min(scalarField1.nj, scalarField2.nj);
		int zmin = min(scalarField1.nk, scalarField2.nk);

		unsigned int valid_cells = 0;

		for(unsigned int k = 0; k<zmin; k++)
		for(unsigned int j = 0; j<ymin; j++)
		for(unsigned int i = 0; i<xmin; i++)
		{
			float si = (float)i/xmin * (float)solid->ni;
			float sj = (float)j/ymin * (float)solid->nj;
			float sk = (float)k/zmin * (float)solid->nk;
			
			if(solid->interpolate_value(si,sj,sk) > 0) //we are not in the solid, so this cell should be counted
			{
				valid_cells++;
			}
		}

		data1.resize(valid_cells);
		data2.resize(valid_cells);

		unsigned int index = 0;
		for(unsigned int k = 0; k<zmin; k++)
		for(unsigned int j = 0; j<ymin; j++)
		for(unsigned int i = 0; i<xmin; i++)
		{
			float si = (float)i/xmin * (float)solid->ni;
			float sj = (float)j/ymin * (float)solid->nj;
			float sk = (float)k/zmin * (float)solid->nk;
			if(solid->interpolate_value(si,sj,sk) > 0 && index<data1.size()) //we are not in the solid, so this cell should be counted
			{
				data1[index] = scalarField1(i,j,k);
				data2[index] = scalarField2(i,j,k);
				index++;
			}
		}
		
		QPair<QString, QString> labels;
		labels.first = label1.c_str();
		labels.second = label2.c_str() + QString(" Phase (") + QString::number(currentPhase)+")";
		
		plot->setData(data1, data2, labels);
		plot->showWindow();

		plots.push_back(plot);
	}
}

void qfeStudy::qfeGetProgressBar(QString dialogTitle, int numberOfSteps)
{
	progressBar.setWindowTitle(dialogTitle);
	progressBar.setCancelButtonText("Cancel");
	progressBar.setMinimum(0);
	progressBar.setMaximum(numberOfSteps+1);
	progressBar.setWindowModality(Qt::WindowModal);
	progressBar.setVisible(true);
	progressBar.show();
	progressBar.setValue(0);
	progressBar.update();
}

void qfeStudy::qfeUpdateProgressBar(int currentStep)
{
	if(currentStep>=progressBar.maximum() || currentStep<progressBar.minimum())
		return;

	std::cout<<currentStep<<" / "<<progressBar.maximum()<<std::endl;

	progressBar.setValue(currentStep);
	progressBar.update();
}

bool qfeStudy::qfeWasCancelledProgressBar()
{
	return progressBar.wasCanceled();
}

void qfeStudy::qfeClearProgressBar()
{
	progressBar.hide();
	progressBar.setVisible(false);
}

bool qfeStudy::qfeScalarMetricPrecomputed(scalarMetricType metricType, dataID dataID, const float phase, Array3f &data, float &range_min, float &range_max)
{
	 for(unsigned int i = 0; i<stored_scalar_metrics.size(); i++)
	 {
		 if(stored_scalar_metrics.at(i).metricType == metricType && stored_scalar_metrics.at(i).dataID == dataID && stored_scalar_metrics.at(i).phase == phase)
		 {
			 data = stored_scalar_metrics.at(i).data;
			 if(data.ni == 0 || data.nj == 0 || data.nk == 0)
			 {
				 std::cout<<"Error: precomputed metric is not complete"<<std::endl;
				 continue;
			 }
			 range_min = stored_scalar_metrics.at(i).range_min;
			 range_max = stored_scalar_metrics.at(i).range_max;
			 return true;
		 }
	 }
	 
	 return false;
}

void qfeStudy::qfeStoreComputedScalarMetric(scalarMetricType metricType, dataID dataID, const float phase, Array3f &data, float &range_min, float &range_max)
{
	//check if it doesn't exist already
	for(unsigned int i = 0; i<stored_scalar_metrics.size(); i++)
	{
		if(stored_scalar_metrics.at(i).metricType == metricType && stored_scalar_metrics.at(i).dataID == dataID && stored_scalar_metrics.at(i).phase == phase)
		{
			return; //it already exists, so we don't have to store it
		}
	}

	//it doesn't exist yet, so we store it
	scalar_metric_storage p;

	p.metricType = metricType;
	p.dataID = dataID;
	p.phase = phase;
	p.data = data;
	p.range_min = range_min;
	p.range_max = range_max;

	//init volume:		
	qfeGrid *grid;
	this->pcap[0]->qfeGetVolumeGrid(&grid);
	p.volume.qfeSetVolumeComponentsPerVoxel(3);
	p.volume.qfeSetVolumeGrid(grid);

	float *vdata = new float[3 * data.ni * data.nj * data.nk];

	for(unsigned int k = 0; k<data.nk; k++)
	for(unsigned int j = 0; j<data.nj; j++)
	for(unsigned int i = 0; i<data.ni; i++)
	{
		unsigned int offset = 3*((int)i + (int)j*data.ni + (int)k*data.ni*data.nj);
			
		vdata[offset+0] = data.at(i,j,k);
		vdata[offset+1] = 0.0f;
		vdata[offset+2] = 0.0f;
	}

	p.volume.qfeSetVolumeData(qfeValueTypeFloat, data.ni, data.nj, data.nk, vdata);
	p.volume.qfeSetVolumeLabel("...");
	
	p.volume.qfeSetVolumeValueDomain(range_min, range_max);
	p.volume.qfeUpdateVolume();

	stored_scalar_metrics.push_back(p);
}

bool qfeStudy::qfeVectorMetricPrecomputed(vectorMetricType metricType, dataID dataID, const float phase, Array3Vec3 &data)
{
	 for(unsigned int i = 0; i<stored_vector_metrics.size(); i++)
	 {
		 if(stored_vector_metrics.at(i).metricType == metricType && stored_vector_metrics.at(i).dataID == dataID && stored_vector_metrics.at(i).phase == phase)
		 {
			 data = stored_vector_metrics.at(i).data;
			 return true;
		 }
	 }
	 
	 return false;
}

void qfeStudy::qfeStoreComputedVectorMetric(vectorMetricType metricType, dataID dataID, const float phase, Array3Vec3 &data)
{
	//check if it doesn't exist already
	for(unsigned int i = 0; i<stored_vector_metrics.size(); i++)
	{
		if(stored_vector_metrics.at(i).metricType == metricType && stored_vector_metrics.at(i).dataID == dataID && stored_vector_metrics.at(i).phase == phase)
		{
			return; //it already exists, so we don't have to store it
		}
	}

	vector_metric_storage p;

	p.metricType = metricType;
	p.dataID = dataID;
	p.phase = phase;
	p.data = data;

	//init volume:		
	qfeGrid *grid;
	this->pcap[0]->qfeGetVolumeGrid(&grid);
	p.volume.qfeSetVolumeComponentsPerVoxel(3);
	p.volume.qfeSetVolumeGrid(grid);

	float *vdata = new float[3 * data.ni * data.nj * data.nk];

	for(unsigned int k = 0; k<data.nk; k++)
	for(unsigned int j = 0; j<data.nj; j++)
	for(unsigned int i = 0; i<data.ni; i++)
	{
		unsigned int offset = 3*((int)i + (int)j*data.ni + (int)k*data.ni*data.nj);
			
		vdata[offset+0] = data.at(i,j,k)[0];
		vdata[offset+1] = data.at(i,j,k)[1];
		vdata[offset+2] = data.at(i,j,k)[2];
	}

	p.volume.qfeSetVolumeData(qfeValueTypeFloat, data.ni, data.nj, data.nk, vdata);
	p.volume.qfeSetVolumeLabel("...");
	
	p.volume.qfeUpdateVolume();

	stored_vector_metrics.push_back(p);
}

void qfeStudy::qfeGetScalarMetricList(std::vector<metric_descriptor> &metrics)
{
	metrics.clear();

	//fill the list (the items should match with qfeGetScalarMetric)

	metric_descriptor p;
	
	p.name = "Velocity magnitude 1"; p.metricType = VelocityMagnitude; p.diverging = false;	p.needs_second_dataset = false; p.needs_solid = false; p.dataID = dataID_vol1;
	metrics.push_back(p);

	p.name = "Velocity magnitude 2"; p.metricType = VelocityMagnitude; p.diverging = false;	p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_vol2;
	metrics.push_back(p);

	p.name = "Divergence 1"; p.metricType = Divergence; p.diverging = true; p.needs_second_dataset = false; p.needs_solid = false; p.dataID = dataID_vol1;
	metrics.push_back(p);

	p.name = "Divergence 2"; p.metricType = Divergence; p.diverging = true; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_vol2;
	metrics.push_back(p);

	p.name = "Acceleration 1"; p.metricType = Acceleration; p.diverging = true; p.needs_second_dataset = false; p.needs_solid = false; p.dataID = dataID_vol1;
	metrics.push_back(p);

	p.name = "Acceleration 2"; p.metricType = Acceleration; p.diverging = true; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_vol2;
	metrics.push_back(p);
		
	p.name = "WSS magnitude 1"; p.metricType = WSS; p.diverging = false; p.needs_second_dataset = false; p.needs_solid = true; p.dataID = dataID_vol1;
	metrics.push_back(p);
				
	p.name = "WSS magnitude 2"; p.metricType = WSS; p.diverging = false; p.needs_second_dataset = true; p.needs_solid = true; p.dataID = dataID_vol2;
	metrics.push_back(p);

	p.name = "Curl magnitude 1"; p.metricType = CurlMagnitude; p.diverging = false; p.needs_second_dataset = false; p.needs_solid = false; p.dataID = dataID_vol1;
	metrics.push_back(p);
				
	p.name = "Curl magnitude 2"; p.metricType = CurlMagnitude; p.diverging = false; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_vol2;
	metrics.push_back(p);

	p.name = "Lambda2 1"; p.metricType = Lambda2; p.diverging = true; p.needs_second_dataset = false; p.needs_solid = false; p.dataID = dataID_vol1;
	metrics.push_back(p);

	p.name = "Lambda2 2"; p.metricType = Lambda2; p.diverging = true; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_vol2;
	metrics.push_back(p);

	p.name = "QCriterion 1"; p.metricType = QCriterion; p.diverging = true; p.needs_second_dataset = false; p.needs_solid = false; p.dataID = dataID_vol1;
	metrics.push_back(p);

	p.name = "QCriterion 2"; p.metricType = QCriterion; p.diverging = true; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_vol2;
	metrics.push_back(p);

	p.name = "Spatial incoherence 1"; p.metricType = SpatialIncoherence; p.diverging = false; p.needs_second_dataset = false; p.needs_solid = false; p.dataID = dataID_vol1;
	metrics.push_back(p);
				
	p.name = "Spatial incoherence 2"; p.metricType = SpatialIncoherence; p.diverging = false; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_vol2;
	metrics.push_back(p);

	p.name = "SNR"; p.metricType = SNR; p.diverging = false; p.needs_second_dataset = false; p.needs_solid = false; p.dataID = dataID_vol1;
	metrics.push_back(p);
		
	p.name = "Solid SDF"; p.metricType = SolidSDF; p.diverging = true; p.needs_second_dataset = false; p.needs_solid = true; p.dataID = dataID_vol1;
	metrics.push_back(p);

	p.name = "qflow anatomy"; p.metricType = QFlowAnatomy; p.diverging = true; p.needs_second_dataset = false; p.needs_solid = false; p.dataID = dataID_vol1;
	metrics.push_back(p);
	
	p.name = "Vel magnitude diff"; p.metricType = VelocityMagnitudeDiff; p.diverging = true; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_diff;
	metrics.push_back(p);

	p.name = "Vel magnitude comparison"; p.metricType = VelocityMagnitudeComparison; p.diverging = true; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_diff;
	metrics.push_back(p);

	p.name = "Vel angle diff"; p.metricType = VelocityAngleDiff; p.diverging = false; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_diff;
	metrics.push_back(p);

	p.name = "Vel squared diff"; p.metricType = VelocitySquaredDiff; p.diverging = false; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_diff;
	metrics.push_back(p);

	p.name = "Divergence diff"; p.metricType = DivergenceDiff; p.diverging = true; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_diff;
	metrics.push_back(p);

	p.name = "Acceleration diff"; p.metricType = AccelerationDiff; p.diverging = true; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_diff;
	metrics.push_back(p);
					
	p.name = "WSS magnitude diff"; p.metricType = WSSDiff; p.diverging = true; p.needs_second_dataset = true; p.needs_solid = true; p.dataID = dataID_diff;
	metrics.push_back(p);

	p.name = "Curl magnitude diff"; p.metricType = CurlDiff; p.diverging = true; p.needs_second_dataset = true; p.needs_solid = false; p.dataID = dataID_diff;
	metrics.push_back(p);
}

qfeStudy::scalarMetricType qfeStudy::qfeLabelToScalarMetricType(const std::string label)
{
	scalarMetricType result = VelocityMagnitude;

	std::vector<metric_descriptor> metrics;

	this->qfeGetScalarMetricList(metrics);

	for(unsigned int i = 0; i<metrics.size(); i++)
	{
		if(label.compare(metrics.at(i).name) == 0)
		{
			result = (scalarMetricType) metrics.at(i).metricType;
			break;
		}		
	}	

	return result;
}

qfeStudy::dataID qfeStudy::qfeScalarLabelToDataID(const std::string label)
{
	dataID result = dataID_vol1;

	std::vector<metric_descriptor> metrics;

	this->qfeGetScalarMetricList(metrics);

	for(unsigned int i = 0; i<metrics.size(); i++)
	{
		if(label.compare(metrics.at(i).name) == 0)
		{
			result = (dataID) metrics.at(i).dataID;
			break;
		}		
	}	

	return result;
}

std::string qfeStudy::qfeScalarMetricTypeToLabel(const qfeStudy::scalarMetricType type)
{
	std::string result = "";

	std::vector<metric_descriptor> metrics;

	this->qfeGetScalarMetricList(metrics);

	for(unsigned int i = 0; i<metrics.size(); i++)
	{
		if((scalarMetricType) metrics.at(i).metricType == type)
		{
			result = metrics.at(i).name;
			break;
		}		
	}	

	return result;
}

void qfeStudy::qfeGetVectorMetricList(std::vector<metric_descriptor> &metrics)
{
	metrics.clear();

	//fill the list (the items should match with qfeGetVectorMetric)

	metric_descriptor p;
	
	p.name = "Velocity 1"; p.metricType = Velocity; p.diverging = false;	p.needs_second_dataset = false; p.needs_solid = false;
	metrics.push_back(p);

	p.name = "Velocity 2"; p.metricType = Velocity; p.diverging = false;	p.needs_second_dataset = true; p.needs_solid = false;
	metrics.push_back(p);
		
	p.name = "WSS 1"; p.metricType = WallShearStress; p.diverging = false;	p.needs_second_dataset = false; p.needs_solid = true;
	metrics.push_back(p);

	p.name = "WSS 2"; p.metricType = WallShearStress; p.diverging = false;	p.needs_second_dataset = true; p.needs_solid = true;
	metrics.push_back(p);
			
	p.name = "Curl 1"; p.metricType = Curl; p.diverging = false;	p.needs_second_dataset = false; p.needs_solid = false;
	metrics.push_back(p);

	p.name = "Curl 2"; p.metricType = Curl; p.diverging = false;	p.needs_second_dataset = true; p.needs_solid = false;
	metrics.push_back(p);

	p.name = "qflow velocities"; p.metricType = QFlowVelocity; p.diverging = false;	p.needs_second_dataset = false; p.needs_solid = false;
	metrics.push_back(p);
}

qfeStudy::vectorMetricType qfeStudy::qfeLabelToVectorMetricType(const std::string label)
{
	vectorMetricType result = Velocity;

	std::vector<metric_descriptor> metrics;

	this->qfeGetVectorMetricList(metrics);

	for(unsigned int i = 0; i<metrics.size(); i++)
	{
		if(label.compare(metrics.at(i).name) == 0)
		{
			result = (vectorMetricType) metrics.at(i).metricType;
			break;
		}		
	}	

	return result;
}

qfeStudy::dataID qfeStudy::qfeVectorLabelToDataID(const std::string label)
{
	dataID result = dataID_vol1;

	std::vector<metric_descriptor> metrics;

	this->qfeGetVectorMetricList(metrics);

	for(unsigned int i = 0; i<metrics.size(); i++)
	{
		if(label.compare(metrics.at(i).name) == 0)
		{
			result = (dataID) metrics.at(i).dataID;
			break;
		}		
	}	

	return result;
}

std::string qfeStudy::qfeVectorMetricTypeToLabel(const qfeStudy::vectorMetricType type)
{
	std::string result = "";

	std::vector<metric_descriptor> metrics;

	this->qfeGetVectorMetricList(metrics);

	for(unsigned int i = 0; i<metrics.size(); i++)
	{
		if((vectorMetricType) metrics.at(i).metricType == type)
		{
			result = metrics.at(i).name;
			break;
		}		
	}	

	return result;
}

void qfeStudy::qfeGetScalarMetricStatistics(const string label, const Array3f* solid, const float currentPhase, bool &existing_metric, float &total, float &average, float &standard_deviation)
{
	existing_metric = qfeScalarMetricExists(label);
	if(!existing_metric)
		return;

	Array3f result;

	float range_min = 0.0f;
	float range_max = 0.0f;

	this->qfeGetScalarMetric(label,solid,currentPhase,existing_metric,result,range_min,range_max);

	unsigned int valid_cells = 0;

	total = 0.0f;
	average = 0.0f;

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		float si = (float)i/result.ni * (float)solid->ni;
		float sj = (float)j/result.nj * (float)solid->nj;
		float sk = (float)k/result.nk * (float)solid->nk;
		
		if(solid->interpolate_value(si,sj,sk) > 0) //we are not in the solid, so this cell should be counted
		{
			valid_cells++;

			total += result.at(i,j,k);
		}
	}

	average = total/(float)valid_cells;

	float sdev = 0.0f;

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		float si = (float)i/result.ni * (float)solid->ni;
		float sj = (float)j/result.nj * (float)solid->nj;
		float sk = (float)k/result.nk * (float)solid->nk;
		
		if(solid->interpolate_value(si,sj,sk) > 0) //we are not in the solid, so this cell should be counted
		{
			sdev += pow(result.at(i,j,k)-average, 2.0);
		}
	}

	standard_deviation = sdev/(float)valid_cells;
}

void qfeStudy::qfeGetMetricTotalInCells(const string label, const Array3f* solid, const Array3b* selected_cells, const float currentPhase, bool &existing_metric, float &total)
{
	total = 0.0f;

	float range_min = 0.0f; float range_max = 0.0f;

	Array3f result;

	this->qfeGetScalarMetric(label,solid,currentPhase,existing_metric,result,range_min,range_max);

	if(!existing_metric)
		return;

	std::array<float,3> point;

	for(unsigned int k = 0; k<selected_cells->nk; k++)
	for(unsigned int j = 0; j<selected_cells->nj; j++)
	for(unsigned int i = 0; i<selected_cells->ni; i++)
	{
		point[0] = (float)i/selected_cells->ni; point[1] = (float)j/selected_cells->nj; point[2] = (float)k/selected_cells->nk;

		float phi = this->getValueTextureCoordinates(point, *solid);

		if(selected_cells->at(i,j,k) && phi>0.0f)
		{
			total += this->getValueTextureCoordinates(point, result);
		}
	}
}

//Instead of a surface the cell centers are used (which depending on the selected cells represent a surface)
void qfeStudy::qfeGetMetricFluxInCells(const string label, const Array3f* solid, const Array3b* selected_cells, std::array<float,3> normal, const float currentPhase, bool &existing_metric, float &total)
{	
	total = 0.0f;
	
	float range_min = 0.0f; float range_max = 0.0f;

	Array3f result;
		
	Array3Vec3 velocity;

	if(velocity.is_null_vector(&normal))
		return;
	
	this->qfeGetVectorMetric(label,solid,currentPhase,existing_metric,velocity);

	if(!existing_metric)
		return;

	velocity.vector_normalize(&normal);

	std::array<float,3> point;

	for(unsigned int k = 0; k<selected_cells->nk; k++)
	for(unsigned int j = 0; j<selected_cells->nj; j++)
	for(unsigned int i = 0; i<selected_cells->ni; i++)
	{
		point[0] = (float)i/selected_cells->ni; point[1] = (float)j/selected_cells->nj; point[2] = (float)k/selected_cells->nk;

		float phi = this->getValueTextureCoordinates(point, *solid);

		if(selected_cells->at(i,j,k) && phi>0.0f)
		{
			std::array<float,3> vel = this->getValueTextureCoordinates(point, velocity);
			
			if(velocity.is_null_vector(&vel))
				continue;

			velocity.vector_normalize(&vel);

			float dot = velocity.vector_dot_product(&normal,&vel);

			float sign = 1.0f;

			if(dot == 0.0f)
				sign = 0.0f;
			if(dot>0.0f)
				sign = 1.0f;
			if(dot<0.0f)
				sign = -1.0f;
			
			total += sign*velocity.vector_length(&vel);
		}
	}

	total = fabs(total);
}

//returns the mean, standard deviation and time
void qfeStudy::qfeGetAverageOverTime(const string label, std::vector<std::array<float, 3>> &data)
{
	unsigned w, h, d;
	pcap.front()->qfeGetVolumeSize(w, h, d);
	Array3f mock_solid(w, h, d);
	mock_solid.assign(w, h, d, 1.0f);

	bool mesh_exists = false;
	if(segmentation.size()>0)
		mesh_exists = true;

	if(mesh_exists && level_set_computed == false)
	{
		this->qfeComputeLeveLSet();

		if(mesh_level_set.ni > 1 && mesh_level_set.nj > 1 && mesh_level_set.nk > 1)
			level_set_computed = true;
	}
	if(level_set_computed)
	{
		mock_solid = this->mesh_level_set;
	}

	this->qfeGetAverageOverTime(label, &mock_solid, data);
}

//returns the mean, standard deviation and time
void qfeStudy::qfeGetAverageOverTime(const string label, const Array3f* solid, std::vector<std::array<float, 3>> &data)
{
	data.clear();

	if(!this->qfeScalarMetricExists(label))
		return;

	metric_descriptor metric = qfeGetScalarMetricProperties(label);

	size_t data_size = 0;
	float time_step = phaseDuration;
	bool sim_data = false;

	switch(metric.dataID)
	{
	case dataID_diff:
		data_size = pcap.size();
		time_step = phaseDuration;
		break;
	case dataID_vol1:
		data_size = pcap.size();
		time_step = phaseDuration;
		break;
	case dataID_vol2:
		data_size = sim_velocities.size();
		time_step = sim_phaseDuration;
		sim_data = true;
		break;
	default:
		data_size = pcap.size();
		break;
	}

	data.resize(data_size);

	bool existing_metric = true;

	for(unsigned int i = 0; i<data_size; i++)
	{		
		float total = 0.0f;
		float average = 0.0f;
		float sdev = 0.0f;

		float phase = i;
		if(sim_data)
		{
			phase = (i*sim_phaseDuration)/phaseDuration;
		}

		this->qfeGetScalarMetricStatistics(label, solid, phase, existing_metric, total, average, sdev);

		std::array<float,3> result;

		result[0] = average;
		result[1] = sdev;
		result[2] = i*time_step*0.001;
		
		data[i] = result;
	}
}

qfeStudy::metric_descriptor qfeStudy::qfeGetScalarMetricProperties(std::string label)
{
	metric_descriptor p;

	std::vector<metric_descriptor> metricList;
	this->qfeGetScalarMetricList(metricList);

	for(unsigned int i = 0; i<metricList.size(); i++)
	{
		if(label.compare(metricList.at(i).name)==0)
		{
			return metricList.at(i);
		}
	}

	return p;
}

qfeStudy::metric_descriptor qfeStudy::qfeGetVectorMetricProperties(std::string label)
{
	metric_descriptor p;

	std::vector<metric_descriptor> metricList;
	this->qfeGetVectorMetricList(metricList);

	for(unsigned int i = 0; i<metricList.size(); i++)
	{
		if(label.compare(metricList.at(i).name)==0)
		{
			return metricList.at(i);
		}
	}

	return p;
}

bool qfeStudy::qfeScalarMetricExists(const string label)
{
	std::vector<metric_descriptor> metrics;

	this->qfeGetScalarMetricList(metrics);

	bool second_data_set_available = sim_velocities.size() > 0;
	
	bool mesh_exists = false;
	if(segmentation.size()>0)
		mesh_exists = true;
	
	for(unsigned int i = 0; i<metrics.size(); i++)
	{
		if(label.compare(metrics.at(i).name)==0)
		{ 
			if(!metrics.at(i).needs_second_dataset || (metrics.at(i).needs_second_dataset && second_data_set_available))
			{
				if(!metrics.at(i).needs_solid || mesh_exists)
				{
					return true;
				}
			}
		}
	}

	return false;
}

bool qfeStudy::qfeVectorMetricExists(const string label)
{
	std::vector<metric_descriptor> metrics;

	this->qfeGetVectorMetricList(metrics);

	bool second_data_set_available = sim_velocities.size() != 0;

	bool mesh_exists = false;
	if(segmentation.size()>0)
		mesh_exists = true;
	
	for(unsigned int i = 0; i<metrics.size(); i++)
	{
		if(label.compare(metrics.at(i).name)==0)
		{ 
			if(!metrics.at(i).needs_second_dataset || (metrics.at(i).needs_second_dataset && second_data_set_available))
			{
				if(!metrics.at(i).needs_solid || mesh_exists)
				{
					return true;
				}
			}
		}
	}

	return false;
}

void qfeStudy::qfeGetExistingMetricList(std::vector<metric_descriptor> &metrics)
{
	qfeGetExistingMetricList(metrics, false);
}

void qfeStudy::qfeGetExistingMetricList(std::vector<metric_descriptor> &metrics, bool scalar_only)
{
	metrics.clear();

	std::vector<metric_descriptor> scalar_metrics;
	this->qfeGetScalarMetricList(scalar_metrics);

	for(unsigned int i = 0; i<scalar_metrics.size(); i++)
	{
		std::string label = scalar_metrics.at(i).name;

		if(this->qfeScalarMetricExists(label))
		{
			metrics.push_back(scalar_metrics.at(i));
		}
	}

	if(scalar_only)
		return;

	std::vector<metric_descriptor> vector_metrics;

	this->qfeGetVectorMetricList(vector_metrics);

	for(unsigned int i = 0; i<vector_metrics.size(); i++)
	{
		std::string label = vector_metrics.at(i).name;

		if(this->qfeVectorMetricExists(label))
		{
			metrics.push_back(vector_metrics.at(i));
		}
	}

	return;
}

void qfeStudy::qfeGetScalarMetric(const string label, const float currentPhase, bool &existing_metric, Array3f &result, float &range_min, float &range_max)
{
	std::vector<metric_descriptor> metrics;
	this->qfeGetScalarMetricList(metrics);

	existing_metric = false;

	unsigned w, h, d;
	pcap.front()->qfeGetVolumeSize(w, h, d);
	Array3f mock_solid(w, h, d);
	mock_solid.assign(w, h, d, 1.0f);

	bool mesh_exists = false;
	if(segmentation.size()>0)
		mesh_exists = true;

	if(mesh_exists && level_set_computed == false)
	{
		this->qfeComputeLeveLSet();

		if(mesh_level_set.ni > 1 && mesh_level_set.nj > 1 && mesh_level_set.nk > 1)
			level_set_computed = true;
	}
	if(level_set_computed)
	{
		mock_solid = this->mesh_level_set;
	}
	
	for(unsigned int i = 0; i<metrics.size(); i++)
	{
		if(label.compare(metrics.at(i).name) == 0)
		{
			if(metrics.at(i).needs_solid == false || level_set_computed)
			{
				this->qfeGetScalarMetric(label,&mock_solid,currentPhase,existing_metric,result, range_min, range_max);

				existing_metric = true;
			}
			return; //metric found and possible executed
		}
	}
}

void qfeStudy::qfeGetScalarMetric(const string label, const Array3f* solid, const float currentPhase, bool &existing_metric, Array3f &result, float &range_min, float &range_max)
{
	const qfeVolumeSeries *volser1 = &pcap;
	const qfeVolumeSeries *volser2 = &sim_velocities;

	float time = phaseDuration*currentPhase;
	const float currentPhase2 = time/sim_phaseDuration;

	existing_metric = this->qfeScalarMetricExists(label);

	result.resize(0,0,0);

	if(!existing_metric)
	{
		return;
	}

	bool solid_given = (solid->ni > 1 && solid->nj > 1 && solid->nk >1);

	bool mesh_exists = false;
	if(segmentation.size()>0)
		mesh_exists = true;

	if(!solid_given && mesh_exists && level_set_computed == false)
	{
		this->qfeComputeLeveLSet();

		if(mesh_level_set.ni > 1 && mesh_level_set.nj > 1 && mesh_level_set.nk > 1)
			level_set_computed = true;
	}

	if(level_set_computed)
	{
		solid = &this->mesh_level_set;
	}

	//std::cout<<"Dataset one index: "<<currentPhase<<std::endl<<"Dataset two index: "<<currentPhase2<<std::endl;

	if(label.compare("Velocity magnitude 1") == 0)
	{
		float min = 0.0f;
		this->qfeGetVelocityMagnitude(volser1, dataID_vol1, currentPhase, solid, result, range_min, range_max);
		qfeGetStudyRangePCAP(min, range_max);
		range_min = 0.0f;
	}

	if(label.compare("Velocity magnitude 2") == 0)
	{
		this->qfeGetVelocityMagnitude(volser2, dataID_vol2, currentPhase2, solid, result, range_min, range_max);
		range_min = 0.0f;
	}

	if(label.compare("Divergence 1") == 0)
	{
		this->qfeGetVelocityDivergence(volser1, dataID_vol1, currentPhase, solid, result, range_min, range_max);
	}

	if(label.compare("Divergence 2") == 0)
	{
		this->qfeGetVelocityDivergence(volser2, dataID_vol2, currentPhase2, solid, result, range_min, range_max);
	}
	
	if(label.compare("Acceleration 1") == 0)
	{
		this->qfeGetAcceleration(volser1, dataID_vol1, currentPhase, phaseDuration, solid, result, range_min, range_max);
	}

	if(label.compare("Acceleration 2") == 0)
	{
		this->qfeGetAcceleration(volser2, dataID_vol2, currentPhase2, sim_phaseDuration, solid, result, range_min, range_max);
	}

	if(label.compare("WSS magnitude 1") == 0)
	{
		this->qfeGetWallShearStressMagnitude(volser1, dataID_vol1, currentPhase, solid, result, range_min, range_max);
		range_min = 0.0f;
	}
	
	if(label.compare("WSS magnitude 2") == 0)
	{
		this->qfeGetWallShearStressMagnitude(volser2, dataID_vol2, currentPhase2, solid, result, range_min, range_max);
		range_min = 0.0f;
	}
	
	if(label.compare("Curl magnitude 1") == 0)
	{
		this->qfeGetCurlMagnitude(volser1, dataID_vol1, currentPhase, solid, result, range_min, range_max);
		range_min = 0.0f;
	}
	
	if(label.compare("Curl magnitude 2") == 0)
	{
		this->qfeGetCurlMagnitude(volser2, dataID_vol2, currentPhase2, solid, result, range_min, range_max);
		range_min = 0.0f;
	}

	if(label.compare("Lambda2 1") == 0)
	{
		this->qfeGetLambda2(volser1, dataID_vol1, currentPhase, solid, result, range_min, range_max);
	}
	
	if(label.compare("Lambda2 2") == 0)
	{
		this->qfeGetLambda2(volser2, dataID_vol2, currentPhase2, solid, result, range_min, range_max);
	}

	if(label.compare("QCriterion 1") == 0)
	{
		this->qfeGetQCriterion(volser1, dataID_vol1, currentPhase, solid, result, range_min, range_max);
	}
	
	if(label.compare("QCriterion 2") == 0)
	{
		this->qfeGetQCriterion(volser2, dataID_vol2, currentPhase2, solid, result, range_min, range_max);
	}

	if(label.compare("Spatial incoherence 1") == 0)
	{
		this->qfeGetSpatialIncoherence(volser1, dataID_vol1, currentPhase, solid, result, range_min, range_max);
	}
	
	if(label.compare("Spatial incoherence 2") == 0)
	{
		this->qfeGetSpatialIncoherence(volser2, dataID_vol2, currentPhase2, solid, result, range_min, range_max);
	}

	if(label.compare("SNR") == 0)
	{
		this->qfeGetSNR(currentPhase, result, range_min, range_max);
	}
	
	if(label.compare("Solid SDF") == 0)
	{
		this->qfeGetSolidSDF(volser1, currentPhase, solid, result, range_min, range_max);
	}

	if(label.compare("qflow anatomy") == 0)
	{
		this->qfeGetQFlowAnatomy(volser1, dataID_vol1, currentPhase, solid, result, range_min, range_max);
	}

	if(label.compare("Vel magnitude diff") == 0)
	{
		this->qfeGetDifferenceVelocityMagnitude(volser1, volser2, dataID_diff, currentPhase, currentPhase2, solid, result, range_min, range_max);
	}

	if(label.compare("Vel magnitude comparison") == 0)
	{
		this->qfeGetComparisonVelocityMagnitude(volser1, volser2, dataID_diff, currentPhase, currentPhase2, solid, result, range_min, range_max);
	}

	if(label.compare("Vel angle diff") == 0)
	{
		this->qfeGetDifferenceVelocityAngles(volser1, volser2, dataID_diff, currentPhase, currentPhase2, solid, result, range_min, range_max);
		range_min = 0.0f;
	}	

	if(label.compare("Vel squared diff") == 0)
	{
		this->qfeGetDifferenceVelocitySquared(volser1, volser2, dataID_diff, currentPhase, currentPhase2, solid, result, range_min, range_max);
		range_min = 0.0f;
	}

	if(label.compare("Divergence diff") == 0)
	{
		this->qfeGetDifferenceDivergence(volser1, volser2, dataID_diff, currentPhase, currentPhase2, solid, result, range_min, range_max);
	}

	if(label.compare("Acceleration diff") == 0)
	{
		this->qfeGetDifferenceAcceleration(volser1, volser2, dataID_diff, currentPhase, currentPhase2, phaseDuration, sim_phaseDuration, solid, result, range_min, range_max);
	}

	if(label.compare("WSS magnitude diff") == 0)
	{
		this->qfeGetDifferenceWallShearStress(volser1, volser2, dataID_diff, currentPhase, currentPhase2, solid, result, range_min, range_max);
	}

	if(label.compare("Curl magnitude diff") == 0)
	{
		this->qfeGetDifferenceCurl(volser1, volser2, dataID_diff, currentPhase, currentPhase2, solid, result, range_min, range_max);
	}

	if(result.ni == 0 && result.nj == 0 && result.nk == 0)
	{
		std::cout<<"Scalar metric '"<<label<<"' is not (yet) implemented!"<<std::endl;
	}

	return;
}

void qfeStudy::qfeSetScalarMetric(const string label, const float currentPhase, Array3f &result)
{
	if(label.size() == 0)
		return;

	scalarMetricType metricType = this->qfeLabelToScalarMetricType(label);
	dataID datID = qfeScalarLabelToDataID(label);

	float range_min = 1e6;
	float range_max = -1e6;

	if(this->qfeScalarMetricPrecomputed(metricType, datID, currentPhase, result, range_min, range_max))
		return;

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		range_max = std::max(range_max, result(i,j,k));
		range_min = std::min(range_min, result(i,j,k));
	}
		
	this->qfeStoreComputedScalarMetric(metricType, datID, currentPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetScalarVolume(const string label, const float currentPhase, bool &existing_metric, qfeVolume **result)
{
	Array3f array3D;
	float min = 0.0f; float max = 0.0f;

	this->qfeGetScalarMetric(label, currentPhase, existing_metric, array3D, min, max);

	if(!existing_metric)
		return;

	scalarMetricType metricType = this->qfeLabelToScalarMetricType(label);
	dataID dataType = this->qfeScalarLabelToDataID(label);
	
	for(unsigned int i = 0; i<stored_scalar_metrics.size(); i++)
	{
		if(stored_scalar_metrics.at(i).metricType == metricType && stored_scalar_metrics.at(i).dataID == dataType && stored_scalar_metrics.at(i).phase == currentPhase)
		{
			*result = &stored_scalar_metrics.at(i).volume;
			
			return;
		 }
	 }
}

void qfeStudy::qfeGetScalarVolume(const string label, const Array3f* solid, const float currentPhase, bool &existing_metric, qfeVolume **result)
{
	Array3f array3D;
	float min = 0.0f; float max = 0.0f;

	this->qfeGetScalarMetric(label, solid, currentPhase, existing_metric, array3D, min, max);

	scalarMetricType metricType = this->qfeLabelToScalarMetricType(label);
	dataID dataType = this->qfeScalarLabelToDataID(label);
	
	for(unsigned int i = 0; i<stored_scalar_metrics.size(); i++)
	{
		if(stored_scalar_metrics.at(i).metricType == metricType && stored_scalar_metrics.at(i).dataID == dataType && stored_scalar_metrics.at(i).phase == currentPhase)
		{
			*result = &stored_scalar_metrics.at(i).volume;

			 return;
		 }
	 }
}

void qfeStudy::qfeGetVectorMetric(const string label, const float currentPhase, bool &existing_metric, Array3Vec3 &result)
{
	std::vector<metric_descriptor> metrics;
	this->qfeGetVectorMetricList(metrics);

	existing_metric = false;

	unsigned w, h, d;
	pcap.front()->qfeGetVolumeSize(w, h, d);
	Array3f mock_solid(w, h, d);
	mock_solid.assign(w, h, d, 1.0f);

	bool mesh_exists = false;
	if(segmentation.size()>0)
		mesh_exists = true;

	if(mesh_exists && level_set_computed == false)
	{
		this->qfeComputeLeveLSet();

		if(mesh_level_set.ni > 1 && mesh_level_set.nj > 1 && mesh_level_set.nk > 1)
			level_set_computed = true;
	}
	if(level_set_computed)
	{
		mock_solid = this->mesh_level_set;
	}
	
	for(unsigned int i = 0; i<metrics.size(); i++)
	{
		if(label.compare(metrics.at(i).name) == 0)
		{
			if(metrics.at(i).needs_solid == false || level_set_computed)
			{
				this->qfeGetVectorMetric(label,&mock_solid,currentPhase,existing_metric,result);

				existing_metric = true;
			}
			return; //metric found and possible executed
		}
	}
}

void qfeStudy::qfeGetVectorMetric(const string label, const Array3f* solid, const float currentPhase, bool &existing_metric, Array3Vec3 &result)
{
	const qfeVolumeSeries *volser1 = &pcap;
	const qfeVolumeSeries *volser2 = &sim_velocities;

	float time = phaseDuration*currentPhase;
	const float currentPhase2 = time/sim_phaseDuration;

	existing_metric = this->qfeVectorMetricExists(label);

	if(!existing_metric)
	{
		return;
	}

	if(label.compare("Velocity 1") == 0)
	{
		this->qfeGetVelocityField(volser1, dataID_vol1, currentPhase, solid, result);
	}

	if(label.compare("Velocity 2") == 0)
	{
		this->qfeGetVelocityField(volser2, dataID_vol2, currentPhase2, solid, result);
	}

	if(label.compare("WSS 1") == 0)
	{
		this->qfeGetWallShearStress(volser1, dataID_vol1, currentPhase, solid, result);
	}

	if(label.compare("WSS 2") == 0)
	{
		this->qfeGetWallShearStress(volser2, dataID_vol2, currentPhase2, solid, result);
	}

	if(label.compare("Curl 1") == 0)
	{
		this->qfeGetCurl(volser1, dataID_vol1, currentPhase, solid, result);
	}

	if(label.compare("Curl 2") == 0)
	{
		this->qfeGetCurl(volser2, dataID_vol2, currentPhase2, solid, result);
	}

	if(label.compare("qflow velocities") == 0)
	{
		this->qfeGetQFlowVelocities(volser1, dataID_vol1, currentPhase, solid, result);
	}

	return;
}


void qfeStudy::qfeGetVectorVolume(const string label, const float currentPhase, bool &existing_metric, qfeVolume **result)
{
	Array3Vec3 array3D;

	this->qfeGetVectorMetric(label, currentPhase, existing_metric, array3D);

	if(!existing_metric)
		return;

	vectorMetricType metricType = this->qfeLabelToVectorMetricType(label);
	dataID dataType = this->qfeVectorLabelToDataID(label);
	
	for(unsigned int i = 0; i<stored_vector_metrics.size(); i++)
	{
		if(stored_vector_metrics.at(i).metricType == metricType && stored_vector_metrics.at(i).dataID == dataType && stored_vector_metrics.at(i).phase == currentPhase)
		{
			*result = &stored_vector_metrics.at(i).volume;
			
			return;
		 }
	 }
}

void qfeStudy::qfeGetVectorVolume(const string label, const Array3f* solid, const float currentPhase, bool &existing_metric, qfeVolume **result)
{
	Array3Vec3 array3D;

	this->qfeGetVectorMetric(label, solid, currentPhase, existing_metric, array3D);

	if(!existing_metric)
		return;

	vectorMetricType metricType = this->qfeLabelToVectorMetricType(label);
	dataID dataType = this->qfeVectorLabelToDataID(label);
	
	for(unsigned int i = 0; i<stored_vector_metrics.size(); i++)
	{
		if(stored_vector_metrics.at(i).metricType == metricType && stored_vector_metrics.at(i).dataID == dataType && stored_vector_metrics.at(i).phase == currentPhase)
		{
			*result = &stored_vector_metrics.at(i).volume;
			
			return;
		 }
	 }
}
	
void qfeStudy::qfeGetVelocityField(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3Vec3 &result)
{
	assert(dataID != dataID_diff);

	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}

	if(this->qfeVectorMetricPrecomputed(Velocity, dataID, inPhase, result))
		return;
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	float local_inPhase = inPhase;

	if(local_inPhase<0)
	{
		local_inPhase = volumes->size()-1 + local_inPhase;
	}
	
	if((unsigned int)local_inPhase >= volumes->size())
	{
		while((unsigned int)local_inPhase >= volumes->size())
		{
			local_inPhase = local_inPhase - volumes->size();
		}
	}

	qfeVector vi1, vi2;

	qfeMatrix4f P2V, V2P, T2V, V2T;
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volumes->at((int) local_inPhase));
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volumes->at((int) local_inPhase));	
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volumes->at((int) local_inPhase));
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volumes->at((int) local_inPhase));
	
	/*
	std::cout<<"P2V:"<<std::endl;
	std::cout<<P2V(0,0)<<" "<<P2V(1,0)<<" "<<P2V(2,0)<<" "<<P2V(3,0)<<std::endl;
	std::cout<<P2V(0,1)<<" "<<P2V(1,2)<<" "<<P2V(2,1)<<" "<<P2V(3,1)<<std::endl;
	std::cout<<P2V(0,2)<<" "<<P2V(1,3)<<" "<<P2V(2,2)<<" "<<P2V(3,2)<<std::endl;
	std::cout<<P2V(0,3)<<" "<<P2V(1,4)<<" "<<P2V(2,3)<<" "<<P2V(3,3)<<std::endl;
	*/

	bool needs_conversion = false;
	//check whether the patient presentation matches the voxel presentation
	//that is, the non-diagonal entries in the non-weight 3x3 matrix should be 0, otherwise convert
	if(
		fabs(P2V(0,1))<1e-12 && fabs(P2V(0,2))<1e-12 && 
		fabs(P2V(1,0))<1e-12 && fabs(P2V(1,2))<1e-12 && 
		fabs(P2V(2,0))<1e-12 && fabs(P2V(2,1))<1e-12
		)
	{
		needs_conversion = false;
	}
	else
	{
		needs_conversion = true;
		std::cout<<"Data is converted"<<std::endl;
	}

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		float x = (float) i / result.ni;
		float y = (float) j / result.nj;
		float z = (float) k / result.nk;

		qfePoint texCoords(x,y,z);
		qfePoint voxCoords = texCoords * T2V;

		// Seed in voxel coordinates, velocity in voxel coordinates
		unsigned int next = ((int)local_inPhase + 1) % volumes->size();
		volumes->at((int) local_inPhase)->qfeGetVolumeVectorAtPositionLinear(voxCoords, vi1);
		if(local_inPhase == (int)local_inPhase)
			vi2 = vi1;
		else
			volumes->at(next)->qfeGetVolumeVectorAtPositionLinear(voxCoords, vi2);
								
		if(vi1[0]!=vi1[0] || vi1[1]!=vi1[1] || vi1[2]!=vi1[2])
		{
			std::cout<<"NaN vector"<<std::endl;
			vi1[0]=0; vi1[1]=0; vi1[2]=0;
		}
		
		if(vi2[0]!=vi2[0] || vi2[1]!=vi2[1] || vi2[2]!=vi2[2])
		{
			std::cout<<"NaN vector"<<std::endl;
			vi2[0]=0; vi2[1]=0; vi2[2]=0;
		}
		
		float amount = local_inPhase-(int)local_inPhase;
		clamp(amount, 0.0f, 1.0f);
		qfeVector vi;
		vi[0] = (1-amount)*vi1[0]+(amount)*vi2[0];
		vi[1] = (1-amount)*vi1[1]+(amount)*vi2[1];
		vi[2] = (1-amount)*vi1[2]+(amount)*vi2[2];

		/*
		if(needs_conversion)
		{
			vi = vi*P2V;

			//scale back to real patient values:
			qfeVector     e;
			qfeGrid *grid;
			volumes->at(0)->qfeGetVolumeGrid(&grid);
			grid->qfeGetGridExtent(e.x,e.y,e.z);
			
			vi.x = vi.x*(e.x);
			vi.z = vi.y*(e.y);
			vi.y = vi.z*(e.z);
		}
		*/
		
		std::array<float,3> vector;
		vector[0] = vi[0]; vector[1] = vi[1]; vector[2] = vi[2];
		result.at(i,j,k) = vector;
	}

	this->qfeStoreComputedVectorMetric(Velocity, dataID, inPhase, result);
}

void qfeStudy::qfeGetWallShearStress(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3Vec3 &result)
{
	assert(dataID != dataID_diff);

	if(this->qfeVectorMetricPrecomputed(WallShearStress, dataID, inPhase, result))
		return;
	
	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	std::array<float,3> vector;
	std::array<float,3> normal;
	std::array<float,3> point;

	std::array<std::array<float,3>,3> velocityGradient;

	Array3Vec3 velocities;

	this->qfeGetVelocityField(volumes, dataID, inPhase, solid, velocities);

	float fx = 0.1f; float fy = 0.1f; float fz = 0.1f;

	Array3f fractions;
	this->qfeGetCellVolume(volumes, inPhase, solid, 2, fractions);

	for(unsigned int k = 1; k<result.nk-1; k++)
	for(unsigned int j = 1; j<result.nj-1; j++)
	for(unsigned int i = 1; i<result.ni-1; i++)
	{
		
		point[0] = (float)i/result.ni; point[1] = (float)j/result.nj; point[2] = (float)k/result.nk;	

		bool near_wall = fractions(i,j,k) < 1.0f || 
			fractions(i-1,j,k) < 1.0f ||
			fractions(i+1,j,k) < 1.0f ||
			fractions(i,j-1,k) < 1.0f ||
			fractions(i,j+1,k) < 1.0f ||
			fractions(i,j,k-1) < 1.0f ||
			fractions(i,j,k+1) < 1.0f;

		if(!near_wall) //we shouldn't consider cells that are not near a wall
		{			
			result.at(i,j,k)[0] = 0.0f;
			result.at(i,j,k)[1] = 0.0f;
			result.at(i,j,k)[2] = 0.0f;
			continue;
		}

		vector[0] = i; vector[1] = j; vector[2] = k;

		normal = this->getGradientTextureCoordinates(point, *solid);
		
		float multiplier = 1.0f; //could be viscosity if desired, but now it is normalized
		
		if(this->getValueTextureCoordinates(point, *solid)<0.0f)
			multiplier = 0.0f; //to make sure it is 0 when not applicable

		velocityGradient = velocities.interpolate_gradient(i,j,k);

		result.at(i,j,k)[0] = multiplier * 
			(velocityGradient[0][0] * normal[0] +
			 velocityGradient[1][0] * normal[1] +
			 velocityGradient[2][0] * normal[2]);
		result.at(i,j,k)[1] = multiplier *  
			(velocityGradient[0][1] * normal[0] +
			 velocityGradient[1][1] * normal[1] +
			 velocityGradient[2][1] * normal[2]);
		result.at(i,j,k)[2] =	multiplier * 
			(velocityGradient[0][2] * normal[0] +
			 velocityGradient[1][2] * normal[1] +
			 velocityGradient[2][2] * normal[2]);
	}
	
	this->qfeStoreComputedVectorMetric(WallShearStress, dataID, inPhase, result);
}

void qfeStudy::qfeGetCurl(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3Vec3 &result)
{
	assert(dataID != dataID_diff);

	if(this->qfeVectorMetricPrecomputed(Curl, dataID, inPhase, result))
		return;
	
	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	std::array<std::array<float,3>,3> velocityGradient;

	Array3Vec3 velocities;

	this->qfeGetVelocityField(volumes, dataID, inPhase, solid, velocities);

	float fx = 0.1f; float fy = 0.1f; float fz = 0.1f;

	for(unsigned int k = 0; k<result.nk-1; k++)
	for(unsigned int j = 0; j<result.nj-1; j++)
	for(unsigned int i = 0; i<result.ni-1; i++)
	{
		velocityGradient = velocities.interpolate_gradient(i,j,k);
		assert(velocityGradient.size() == 3 && velocityGradient[0].size() == 3 && velocityGradient[1].size() == 3 &&velocityGradient[2].size() == 3);
		
		result.at(i,j,k)[0] = velocityGradient[1][2] - velocityGradient[2][1]; //dy w - dz v
		result.at(i,j,k)[1] = velocityGradient[2][0] - velocityGradient[0][2]; //dz u - dx w
		result.at(i,j,k)[2] = velocityGradient[0][1] - velocityGradient[1][0]; //dx v - dy u
	}
	
	this->qfeStoreComputedVectorMetric(Curl, dataID, inPhase, result);
}

void qfeStudy::qfeGetQFlowVelocities(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3Vec3 &result)
{
	assert(dataID != dataID_vol1);

	if(this->qfeVectorMetricPrecomputed(QFlowVelocity, dataID, inPhase, result))
		return;

	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	this->qfeGetVelocityField(volumes, dataID, inPhase, solid, result);

	//get voxel spacing
	qfeGrid      *volume_grid;
	qfeVector     spacing;	
	volumes->at(0)->qfeGetVolumeGrid(&volume_grid);
	volume_grid->qfeGetGridExtent(spacing.x,spacing.y,spacing.z);

	float min_spacing = min(spacing.x, min(spacing.y, spacing.z));
	
	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		//normalize
		result.at(i,j,k)[0];// /= min_spacing;
		result.at(i,j,k)[1];// /= min_spacing;
		result.at(i,j,k)[2];// /= min_spacing;
	}

	this->qfeStoreComputedVectorMetric(QFlowVelocity, dataID, inPhase, result);
}

void qfeStudy::qfeGetVelocityMagnitude(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID != dataID_diff);

	if(this->qfeScalarMetricPrecomputed(VelocityMagnitude, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	qfeGetStudyRangePCAP(range_min, range_max);

	Array3Vec3 velocities;

	this->qfeGetVelocityField(volumes, dataID, inPhase, solid, velocities);

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		result.at(i,j,k) = velocities.vector_length(&velocities.at(i,j,k));
	}

	this->qfeStoreComputedScalarMetric(VelocityMagnitude, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetVelocityDivergence(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID != dataID_diff);

	if(this->qfeScalarMetricPrecomputed(Divergence, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);
	result.assign(ni,nj,nk,0.0f);

	range_min = (float) -50.0f;
	range_max = (float)  50.0f;

	Array3Vec3 velocities;

	this->qfeGetVelocityField(volumes, dataID, inPhase, solid, velocities);

	Array3f fractions;
	this->qfeGetCellVolume(volumes, inPhase, solid, 2, fractions);

	for(unsigned int k = 1; k<result.nk-1; k++)
	for(unsigned int j = 1; j<result.nj-1; j++)
	for(unsigned int i = 1; i<result.ni-1; i++)
	{
		if(velocities.inRange(i-1,j-1,k-1) && velocities.inRange(i+1,j+1,k+1))
			result(i,j,k) = 0.5*(
				fractions(i+1, j, k)*velocities.at(i+1, j, k)[0] - fractions(i-1, j, k)*velocities.at(i-1, j, k)[0] + 
				fractions(i, j+1, k)*velocities.at(i, j+1, k)[1] - fractions(i, j-1, k)*velocities.at(i, j-1, k)[1] + 
				fractions(i, j, k+1)*velocities.at(i, j, k+1)[2] - fractions(i, j, k-1)*velocities.at(i, j, k-1)[2]);
	}
		
	this->qfeStoreComputedScalarMetric(Divergence, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetAcceleration(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, float phaseDuration, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID != dataID_diff);

	if(this->qfeScalarMetricPrecomputed(Acceleration, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	Array3f velMag1;
	Array3f velMag2;

	float dt = 23;//ms

	float phase_offset = (dt/2.0f)/phaseDuration;

	//std::cout<<"Magnitude computation"<<std::endl;
	this->qfeGetVelocityMagnitude(volumes, dataID, inPhase-phase_offset, solid, velMag1, range_min, range_max);
	//std::cout<<"50%"<<std::endl;
	this->qfeGetVelocityMagnitude(volumes, dataID, inPhase+phase_offset, solid, velMag2, range_min, range_max);
	//std::cout<<"100%\nCompute difference"<<std::endl;

	this->qfeGetStudyRangePCAP(range_min, range_max);
	range_min *= (float) 10;
	range_max *= (float) 10;

	float maxi = 0.0f;

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		result.at(i,j,k) = (velMag2.at(i,j,k)-velMag1.at(i,j,k)) / ((dt)/1e3);

		maxi = max(fabs(result.at(i,j,k)),maxi);
	}

	//std::cout<<"Maxi: "<<maxi<<" range: "<<range_max<<std::endl;

	this->qfeStoreComputedScalarMetric(Acceleration, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetWallShearStressMagnitude(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID != dataID_diff);

	if(this->qfeScalarMetricPrecomputed(WSS, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	range_min = (float) 0;
	range_max = (float) 1.0f;

	Array3Vec3 wall_shear_stress;

	this->qfeGetWallShearStress(volumes, dataID, inPhase, solid, wall_shear_stress);

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		std::array<float,3> wss = wall_shear_stress.at(i,j,k);
		if(wss.size() == 3)
		{
			result.at(i,j,k) = wall_shear_stress.vector_length(&wss);
		}
		else
		{
			result.at(i,j,k) = 0.0f;
		}
	}

	this->qfeStoreComputedScalarMetric(WSS, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetCurlMagnitude(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID != dataID_diff);

	if(this->qfeScalarMetricPrecomputed(CurlMagnitude, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	range_min = (float) 0;
	range_max = (float) 15.0f;

	Array3Vec3 curl;

	this->qfeGetCurl(volumes, dataID, inPhase, solid, curl);

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		std::array<float,3> vec = curl.at(i,j,k);
		if(vec.size() == 3)
		{
			result.at(i,j,k) = curl.vector_length(&vec);
		}
		else
		{
			result.at(i,j,k) = 0.0f;
		}
	}

	this->qfeStoreComputedScalarMetric(CurlMagnitude, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetLambda2(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID != dataID_diff);

	if(this->qfeScalarMetricPrecomputed(Lambda2, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	range_min = (float) -20;
	range_max = (float) 20;
	
	unsigned size = ni*nj*nk;
	float *lambda2s = new float[size];

	qfeMath::qfeComputeLambda2s(volumes->at((int)inPhase), lambda2s, range_min, range_max, true);

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		unsigned int index = i+ni*(j+nj*k);

		result(i,j,k) = lambda2s[index];
	}

	this->qfeStoreComputedScalarMetric(Lambda2, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetQCriterion(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID != dataID_diff);

	if(this->qfeScalarMetricPrecomputed(QCriterion, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	range_min = (float) -20;
	range_max = (float) 20;
	
	unsigned size = ni*nj*nk;
	float *lambda2s = new float[size];

	qfeMath::qfeComputeQCriterion(volumes->at((int)inPhase), lambda2s, range_min, range_max);

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		unsigned int index = i+ni*(j+nj*k);

		result(i,j,k) = lambda2s[index];
	}

	this->qfeStoreComputedScalarMetric(QCriterion, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetSpatialIncoherence(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID != dataID_diff);

	//std::cout<<"Compute error"<<std::endl;

	if(this->qfeScalarMetricPrecomputed(SpatialIncoherence, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}

	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);
	result.assign(0.0f);

	Array3Vec3 vel;

	this->qfeGetVelocityField(volumes, dataID, inPhase, solid, vel);

	std::array<float,3> vec;

	float maxi = 0.0f;

	for(unsigned int k = 1; k<result.nk-1; k++)
	for(unsigned int j = 1; j<result.nj-1; j++)
	for(unsigned int i = 1; i<result.ni-1; i++)
	{
		vec[0] = vel(i,j,k)[0] - (vel(i-1,j,k)[0] + vel(i+1,j,k)[0] + vel(i,j-1,k)[0] + vel(i,j+1,k)[0] + vel(i,j,k-1)[0] + vel(i,j,k+1)[0])/6.0f;
		vec[1] = vel(i,j,k)[1] - (vel(i-1,j,k)[1] + vel(i+1,j,k)[1] + vel(i,j-1,k)[1] + vel(i,j+1,k)[1] + vel(i,j,k-1)[1] + vel(i,j,k+1)[1])/6.0f;
		vec[2] = vel(i,j,k)[2] - (vel(i-1,j,k)[2] + vel(i+1,j,k)[2] + vel(i,j-1,k)[2] + vel(i,j+1,k)[2] + vel(i,j,k-1)[2] + vel(i,j,k+1)[2])/6.0f;

		result.at(i,j,k) = vel.vector_length(&vec);

		maxi = max(result.at(i,j,k), maxi);
	}

	range_min = (float) 0;
	range_max = (float) maxi;

	this->qfeStoreComputedScalarMetric(SpatialIncoherence, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetSNR(const float inPhase, Array3f &result, float &range_min, float &range_max)
{
	float sigma = 0.0f;
	this->qfeGetSNR(inPhase, result, sigma, range_min, range_max);
}

void qfeStudy::qfeGetSNR(const float inPhase, Array3f &result, float &sigma, float &range_min, float &range_max)
{
	//Computed as done by O. Friman et al:
	//Probabilistic 4D blood flow tracking and uncertainty estimation
	//We, however, do not take the magnitude intensity account when it is not available

	if(this->qfeScalarMetricPrecomputed(SNR, dataID_vol1, inPhase, result, range_min, range_max))
	{
		sigma = range_min;
		range_min = 0.0f;
		return;
	}

	qfeFloat venc[3] = {0.0, 0.0, 0.0};
	pcap.at(0)->qfeGetVolumeVenc(venc[0], venc[1], venc[2]);

	bool magnitude_exists = ffe.size()>0;

	if(pcap.size() == 0)
	{
		result.resize(0,0,0);
		return;
	}

	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	pcap.at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);
	result.assign(0.0f);

	Array3Vec3 vel(ni,nj,nk);
	Array3f A(ni,nj,nk);
	Array3f anatomy(ni,nj,nk);

	unsigned int number_of_components = 3;
	if(magnitude_exists)
		number_of_components = 4;

	float weight = 1.0f/(float)number_of_components;

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		float magnitude = 0.0f;
		if(magnitude_exists)
		{
			ffe.at((int)inPhase)->qfeGetVolumeScalarAtPosition(i,j,k,magnitude);
			magnitude = fabs(magnitude); //range 0-1
		}
		anatomy(i,j,k) = magnitude;

		qfeVector veloc;
		pcap.at((int) inPhase)->qfeGetVolumeVectorAtPosition(i,j,k, veloc);
		veloc[0] = fabs(veloc[0]/venc[0]); veloc[1] = fabs(veloc[1]/venc[1]); veloc[2] = fabs(veloc[2]/venc[2]); //signal strength is absolute and in range 0 - 1

		A(i,j,k) = weight*(fabs(veloc.x)+fabs(veloc.y)+fabs(veloc.z)+anatomy(i,j,k));

		std::array<float,3> vector;
		vector[0] = veloc[0]; vector[1] = veloc[1]; vector[2] = veloc[2];
		vel(i,j,k) = vector;
	}

	float sigma_squared = 0.0f;

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		float local_sigma = 0.0f;
		for(unsigned int c = 0; c<3; c++)
		{
			local_sigma += (vel(i,j,k)[c] - A(i,j,k))*(vel(i,j,k)[c] - A(i,j,k));
		}
		if(magnitude_exists)
		{	
			local_sigma += (anatomy(i,j,k) - A(i,j,k))*(anatomy(i,j,k) - A(i,j,k));
		}
		sigma_squared += 1.0f/((float)number_of_components-1.0f)*local_sigma;
	}
		
	sigma_squared = sigma_squared/((float)ni*(float)nj*(float)nk-1.0f);
	sigma = sqrt(sigma_squared);

	//for user study, hardcoded SNR
	//float HARD_SNR = 5;
	//sigma = sigma = 0.01*venc*(1.0/HARD_SNR);

	//std::cout<<"Sigma is "<<sigma*venc<<std::endl;

	float maxi = 0.0f;

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		result(i,j,k) = A(i,j,k)/sigma;

		maxi = max(result.at(i,j,k), maxi);
	}

	range_min = (float) sigma;
	range_max = (float) maxi;

	this->qfeStoreComputedScalarMetric(SNR, dataID_vol1, inPhase, result, range_min, range_max);
	
	//Since we stored sigma in range_min, to get the correct result we ask for the just stored data 
	//this will give use the correct range_min and sigma
	this->qfeGetSNR(inPhase, result, sigma, range_min, range_max);
}

void qfeStudy::qfeSetSolidSDF(const float inPhase, const Array3f *solid)
{
	float range_min = 1e6;
	float range_max = -1e6;

	Array3f temp;

	if(this->qfeScalarMetricPrecomputed(SolidSDF, dataID_vol1, inPhase, temp, range_min, range_max))
		return;

	temp.resize(solid->ni, solid->nj, solid->nk);

	for(unsigned int k = 0; k<solid->nk; k++)
	{
		for(unsigned int j = 0; j<solid->nj; j++)
		{
			for(unsigned int i = 0; i<solid->ni; i++)
			{
				range_max = std::max(abs(solid->at(i,j,k)), range_max);

				temp(i,j,k) = solid->at(i,j,k);
			}
		}
	}

	range_min = -range_max;

	this->qfeStoreComputedScalarMetric(SolidSDF, dataID_vol1, inPhase, temp, range_min, range_max);
}

void qfeStudy::qfeGetSolidSDF(const qfeVolumeSeries *volumes, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	if(this->qfeScalarMetricPrecomputed(SolidSDF, dataID_vol1, inPhase, result, range_min, range_max))
		return;

	if(volumes->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	range_min = (float) 0.0f;
	range_max = (float) 0.0f;

	bool mesh_exists = false;
	if(segmentation.size()>0)
		mesh_exists = true;

	if(this->level_set_computed  && mesh_level_set.ni > 1 && mesh_level_set.nj > 1 && mesh_level_set.nk > 1)
	{
		result = this->mesh_level_set;
	}
	else if(mesh_exists)
	{
		this->qfeComputeLeveLSet();
		result = this->mesh_level_set;
	}
	else if(solid->ni > 1 && solid->nj > 1 && solid->nk > 1)
	{
		std::array<float,3> point;

		for(unsigned int k = 0; k<result.nk; k++)
		{
			point[2] = (float)k/result.nk;
			for(unsigned int j = 0; j<result.nj; j++)
			{
				point[1] = (float)j/result.nj;
				for(unsigned int i = 0; i<result.ni; i++)
				{
					point[0] = (float)i/result.ni;
				
					result.at(i,j,k) = getValueTextureCoordinates(point, *solid);

					range_max = max(abs(result.at(i,j,k)), range_max);
				}
			}
		}
	}

	for(unsigned int k = 0; k<result.nk; k++)
	{
		for(unsigned int j = 0; j<result.nj; j++)
		{
			for(unsigned int i = 0; i<result.ni; i++)
			{
				range_max = max(abs(result.at(i,j,k)), range_max);
			}
		}
	}

	range_min = -range_max;

	this->qfeStoreComputedScalarMetric(SolidSDF, dataID_vol1, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetQFlowAnatomy(const qfeVolumeSeries *volumes, dataID dataID, const float inPhase, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID == dataID_vol1);

	if(this->qfeScalarMetricPrecomputed(QFlowAnatomy, dataID, inPhase, result, range_min, range_max))
		return;

	if(ffe.size() == 0)
	{
		result.resize(0,0,0);
		return;
	}

	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	ffe.at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	for(unsigned int k = 0; k<result.nk; k++)
	for(unsigned int j = 0; j<result.nj; j++)
	for(unsigned int i = 0; i<result.ni; i++)
	{
		float anatomy = 0.0f;
		ffe.at((int)inPhase)->qfeGetVolumeScalarAtPosition(i,j,k,anatomy);

		result(i,j,k) = anatomy;

		range_min = min(range_min, anatomy);
		range_max = min(range_max, anatomy);
	}

	this->qfeStoreComputedScalarMetric(QFlowAnatomy, dataID_vol1, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetDifferenceDivergence(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID == dataID_diff);

	if(this->qfeScalarMetricPrecomputed(DivergenceDiff, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes1->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes1->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);
	
	Array3f divergence1;
	Array3f divergence2;

	this->qfeGetVelocityDivergence(volumes1, dataID_vol1, inPhase,  solid, divergence1, range_min, range_max);
	this->qfeGetVelocityDivergence(volumes2, dataID_vol2, inPhase2, solid, divergence2, range_min, range_max);

	range_min = (float) -2.0f*range_max;
	range_max = (float)  2.0f*range_max;

	std::array<float,3> point;

	for(unsigned int k = 0; k<result.nk; k++)
	{
		point[2] = (float)k/result.nk;
		for(unsigned int j = 0; j<result.nj; j++)
		{
			point[1] = (float)j/result.nj;
			for(unsigned int i = 0; i<result.ni; i++)
			{
				point[0] = (float)i/result.ni;
				
				result.at(i,j,k) =	getValueTextureCoordinates(point,divergence1) - 
									getValueTextureCoordinates(point,divergence2);
			}
		}
	}

	this->qfeStoreComputedScalarMetric(DivergenceDiff, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetDifferenceAcceleration(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const float phaseDuration1, const float phaseDuration2, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID == dataID_diff);

	if(this->qfeScalarMetricPrecomputed(AccelerationDiff, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes1->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes1->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);
	
	Array3f acceleration1;
	Array3f acceleration2;

	this->qfeGetAcceleration(volumes1, dataID_vol1, inPhase, phaseDuration1, solid, acceleration1, range_min, range_max);
	this->qfeGetAcceleration(volumes2, dataID_vol2, inPhase2, phaseDuration2, solid, acceleration2, range_min, range_max);

	range_min = (float) -2.0f*range_max;
	range_max = (float)  2.0f*range_max;

	std::array<float,3> point;

	for(unsigned int k = 0; k<result.nk; k++)
	{
		point[2] = (float)k/result.nk;
		for(unsigned int j = 0; j<result.nj; j++)
		{
			point[1] = (float)j/result.nj;
			for(unsigned int i = 0; i<result.ni; i++)
			{
				point[0] = (float)i/result.ni;
				
				result.at(i,j,k) =	getValueTextureCoordinates(point,acceleration1) - 
									getValueTextureCoordinates(point,acceleration2);
			}
		}
	}

	this->qfeStoreComputedScalarMetric(AccelerationDiff, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetDifferenceWallShearStress(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID == dataID_diff);

	if(this->qfeScalarMetricPrecomputed(WSSDiff, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes1->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes1->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);
	
	Array3f wss1;
	Array3f wss2;

	this->qfeGetWallShearStressMagnitude(volumes1, dataID_vol1, inPhase, solid, wss1, range_min, range_max);
	this->qfeGetWallShearStressMagnitude(volumes2, dataID_vol2, inPhase2, solid, wss2, range_min, range_max);

	range_min = (float) -2.0f*range_max;
	range_max = (float)  2.0f*range_max;

	std::array<float,3> point;

	for(unsigned int k = 0; k<result.nk; k++)
	{
		point[2] = (float)k/result.nk;
		for(unsigned int j = 0; j<result.nj; j++)
		{
			point[1] = (float)j/result.nj;
			for(unsigned int i = 0; i<result.ni; i++)
			{
				point[0] = (float)i/result.ni;
				
				result.at(i,j,k) =	getValueTextureCoordinates(point,wss1) - 
									getValueTextureCoordinates(point,wss2);
			}
		}
	}

	this->qfeStoreComputedScalarMetric(WSSDiff, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetDifferenceVelocityMagnitude(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID == dataID_diff);

	if(this->qfeScalarMetricPrecomputed(VelocityMagnitudeDiff, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes1->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes1->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	Array3f magnitudes1;
	Array3f magnitudes2;

	this->qfeGetVelocityMagnitude(volumes1, dataID_vol1, inPhase, solid,  magnitudes1, range_min, range_max);
	this->qfeGetVelocityMagnitude(volumes2, dataID_vol2, inPhase2, solid, magnitudes2, range_min, range_max);

	range_min = (float) -2.0f*range_max;
	range_max = (float)  2.0f*range_max;

	std::array<float,3> point;

	for(unsigned int k = 0; k<result.nk; k++)
	{
		point[2] = (float)k/result.nk;
		for(unsigned int j = 0; j<result.nj; j++)
		{
			point[1] = (float)j/result.nj;
			for(unsigned int i = 0; i<result.ni; i++)
			{
				point[0] = (float)i/result.ni;
				
				result.at(i,j,k) = getValueTextureCoordinates(point,magnitudes1)-getValueTextureCoordinates(point,magnitudes2);
			}
		}
	}

	this->qfeStoreComputedScalarMetric(VelocityMagnitudeDiff, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetComparisonVelocityMagnitude(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID == dataID_diff);

	if(this->qfeScalarMetricPrecomputed(VelocityMagnitudeComparison, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes1->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes1->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	Array3f magnitudes1;
	Array3f magnitudes2;

	this->qfeGetVelocityMagnitude(volumes1, dataID_vol1, inPhase, solid,  magnitudes1, range_min, range_max);
	this->qfeGetVelocityMagnitude(volumes2, dataID_vol2, inPhase2, solid, magnitudes2, range_min, range_max);

	range_min = (float) -1.0f;
	range_max = (float)  1.0f;

	std::array<float,3> point;

	for(unsigned int k = 0; k<result.nk; k++)
	{
		point[2] = (float)k/result.nk;
		for(unsigned int j = 0; j<result.nj; j++)
		{
			point[1] = (float)j/result.nj;
			for(unsigned int i = 0; i<result.ni; i++)
			{
				point[0] = (float)i/result.ni;
				
				float data1 = getValueTextureCoordinates(point,magnitudes1);
				float data2 = getValueTextureCoordinates(point,magnitudes2);

				result.at(i,j,k) = data1-data2;

				range_min = min(range_min, result.at(i,j,k));
				range_max = max(range_max, result.at(i,j,k));
			}
		}
	}

	this->qfeStoreComputedScalarMetric(VelocityMagnitudeComparison, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetDifferenceVelocityAngles(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID == dataID_diff);

	if(this->qfeScalarMetricPrecomputed(VelocityAngleDiff, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes1->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes1->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	Array3Vec3 velocities1;
	Array3Vec3 velocities2;

	this->qfeGetVelocityField(volumes1, dataID_vol1, inPhase, solid,  velocities1);
	this->qfeGetVelocityField(volumes2, dataID_vol2, inPhase2, solid, velocities2);

	range_min = (float) 0.0f;
	range_max = (float) 180.0f;

	std::array<float,3> point;

	for(unsigned int k = 0; k<result.nk; k++)
	{
		point[2] = (float)k/result.nk;
		for(unsigned int j = 0; j<result.nj; j++)
		{
			point[1] = (float)j/result.nj;
			for(unsigned int i = 0; i<result.ni; i++)
			{
				point[0] = (float)i/result.ni;
				
				std::array<float,3> vel1 = getValueTextureCoordinates(point,velocities1);
				std::array<float,3> vel2 = getValueTextureCoordinates(point,velocities2);
							
				float diff_angle = 0.0f;
				
				if(!velocities1.is_null_vector(&vel1) && !velocities2.is_null_vector(&vel2))
				{					
					std::array<float,3> norm_vel1 = velocities1.vector_normalize(&vel1);
					std::array<float,3> norm_vel2 = velocities2.vector_normalize(&vel2);
					
					diff_angle = acos(	norm_vel1[0]*norm_vel2[0] +
										norm_vel1[1]*norm_vel2[1] +
										norm_vel1[2]*norm_vel2[2]
									 ) //acos of dot product gives angle in radians
									 /3.14159265359f*180.0f; //convert radians to degrees
				}
				if(diff_angle == diff_angle)
					result.at(i,j,k) = diff_angle;
				else
					result.at(i,j,k) = 0.0f;
			}
		}
	}
	
	this->qfeStoreComputedScalarMetric(VelocityAngleDiff, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetDifferenceVelocitySquared(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID == dataID_diff);

	if(this->qfeScalarMetricPrecomputed(VelocitySquaredDiff, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes1->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes1->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);

	Array3Vec3 velocities1;
	Array3Vec3 velocities2;

	this->qfeGetVelocityField(volumes1, dataID_vol1, inPhase, solid,  velocities1);
	this->qfeGetVelocityField(volumes2, dataID_vol2, inPhase2, solid, velocities2);

	Array3f SNR_data;

	float r0 = 0.0f; float r1 = 0.0f;
	this->qfeGetSNR(inPhase, SNR_data, r0, r1);
	
	qfeGetStudyRangePCAP(r0, r1);
	float venc = max(fabs(r0),fabs(r1));

	range_min = (float) 0.0f;
	range_max = (float) venc*2.0;

	std::array<float,3> point;

	for(unsigned int k = 0; k<result.nk; k++)
	{
		point[2] = (float)k/result.nk;
		for(unsigned int j = 0; j<result.nj; j++)
		{
			point[1] = (float)j/result.nj;
			for(unsigned int i = 0; i<result.ni; i++)
			{
				point[0] = (float)i/result.ni;
				
				std::array<float,3> vel1 = getValueTextureCoordinates(point,velocities1);
				std::array<float,3> vel2 = getValueTextureCoordinates(point,velocities2);
							
				float squared_diff = 0.0f;

				double weight = SNR_data(i,j,k)/venc;
				if(weight>1.0)
					weight = 1.0;
				
				weight /= 3.0;
				
				if(!velocities1.is_null_vector(&vel1) && !velocities2.is_null_vector(&vel2))
				{					
					squared_diff += pow(vel2[0]-vel1[0],2.0f);
					squared_diff += pow(vel2[1]-vel1[1],2.0f);
					squared_diff += pow(vel2[2]-vel1[2],2.0f);
				}
				if(squared_diff == squared_diff)
					result.at(i,j,k) = squared_diff;
				else
					result.at(i,j,k) = 0.0f;
			}
		}
	}
	
	this->qfeStoreComputedScalarMetric(VelocitySquaredDiff, dataID, inPhase, result, range_min, range_max);
}

void qfeStudy::qfeGetDifferenceCurl(const qfeVolumeSeries *volumes1, const qfeVolumeSeries *volumes2, dataID dataID, const float inPhase, const float inPhase2, const Array3f *solid, Array3f &result, float &range_min, float &range_max)
{
	assert(dataID == dataID_diff);

	if(this->qfeScalarMetricPrecomputed(CurlDiff, dataID, inPhase, result, range_min, range_max))
		return;

	if(volumes1->size() == 0)
	{
		result.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes1->at(0)->qfeGetVolumeSize(ni,nj,nk);

	result.resize(ni,nj,nk);
	
	Array3f curl1;
	Array3f curl2;

	this->qfeGetCurlMagnitude(volumes1, dataID_vol1, inPhase, solid, curl1, range_min, range_max);
	this->qfeGetCurlMagnitude(volumes2, dataID_vol2, inPhase2, solid, curl2, range_min, range_max);

	range_min = (float) -2.0f*range_max;
	range_max = (float)  2.0f*range_max;

	std::array<float,3> point;

	for(unsigned int k = 0; k<result.nk; k++)
	{
		point[2] = (float)k/result.nk;
		for(unsigned int j = 0; j<result.nj; j++)
		{
			point[1] = (float)j/result.nj;
			for(unsigned int i = 0; i<result.ni; i++)
			{
				point[0] = (float)i/result.ni;
				
				result.at(i,j,k) =	getValueTextureCoordinates(point,curl1) - 
									getValueTextureCoordinates(point,curl2);
			}
		}
	}

	this->qfeStoreComputedScalarMetric(CurlDiff, dataID, inPhase, result, range_min, range_max);
}

//levelset is the input dataset of which we find out the volumes per cell
//fractions will contain the fraction of the cell that has a value > 0
//subdivision allows to control the precission of the fractions, the higher the more precise, but the slower it is
void qfeStudy::qfeGetCellVolume(const qfeVolumeSeries *volumes, const float inPhase, const Array3f *solid, unsigned int subdivision, Array3f& fractions)
{
	std::vector<float> offset(3);
	offset[0] = 0.0f; offset[1] = 0.0f; offset[2] = 0.0f;

	this->qfeGetCellVolume(volumes, inPhase, solid, offset, subdivision, fractions);
}

//levelset is the input dataset of which we find out the volumes per cell
//fractions will contain the fraction of the cell that has a value > 0
//fraction_origin can be used to get an offset
//subdivision allows to control the precission of the fractions, the higher the more precise, but the slower it is
void qfeStudy::qfeGetCellVolume(const qfeVolumeSeries *volumes, const float inPhase, const Array3f *solid, std::vector<float> fraction_origin, unsigned int subdivision, Array3f& fractions)
{   
	float range_min = 0.0f; float range_max = 0.0f;

	dataID dataID = dataID_vol1;

	if(this->qfeScalarMetricPrecomputed(LevelSet, dataID, inPhase, fractions, range_min, range_max))
		return;

	if(volumes->size() == 0)
	{
		fractions.resize(0,0,0);
		return;
	}
	
	unsigned int ni = 0; unsigned int nj = 0; unsigned int nk = 0;
	volumes->at(0)->qfeGetVolumeSize(ni,nj,nk);

	fractions.resize(ni+1,nj+1,nk+1);

	//upscale solid levelset
	Array3f levelset(fractions.ni,fractions.nj,fractions.nk);
	std::array<float,3> point;
	for(unsigned int k = 0; k<levelset.nk; k++)
	for(unsigned int j = 0; j<levelset.nj; j++)
	for(unsigned int i = 0; i<levelset.ni; i++)
	{
		if(solid->ni == 1 || solid->nj == 1 || solid->nk == 1) //no solid
		{
			levelset(i,j,k) = 1.0f;
		}
		else
		{
			point[0] = (float)i/levelset.ni; point[1] = (float)j/levelset.nj; point[2] = (float)k/levelset.nk; 
			
			levelset(i,j,k) = this->getValueTextureCoordinates(point, *solid);
		}
	}
	
	//Assumes levelset and fractions have the same dx (which should be implied by the equal size)
	float sub_dx = 1.0f / subdivision;
	int sample_max = subdivision*subdivision*subdivision;
	for(unsigned int k = 0; k < fractions.nk; ++k)
	{
		for(unsigned int j = 0; j < fractions.nj; ++j) 
		{
			for(unsigned int i = 0; i < fractions.ni; ++i) 
			{
				float start_x = fraction_origin[0] + (float)i;
				float start_y = fraction_origin[1] + (float)j;
				float start_z = fraction_origin[2] + (float)k;
				int incount = 0;
				
				for(unsigned int sub_k = 0; sub_k < subdivision; ++sub_k)
				{
					for(unsigned int sub_j = 0; sub_j < subdivision; ++sub_j) 
					{
						for(unsigned int sub_i = 0; sub_i < subdivision; ++sub_i) 
						{
							float x_pos = start_x + (sub_i+0.5f)*sub_dx;
							float y_pos = start_y + (sub_j+0.5f)*sub_dx;
							float z_pos = start_z + (sub_k+0.5f)*sub_dx;
							
							std::vector<float> point(3);
							point[0] = x_pos; point[1] = y_pos; point[2] = z_pos;
							float phi_val = levelset.interpolate_value(point);
							if(phi_val > 0) 
								++incount;
						}
					}
				}
				
				fractions(i,j,k) = (float)incount / (float)sample_max;
			}
		}
	}

	range_min = 0.0f; 
	range_max = 1.0f;
	this->qfeStoreComputedScalarMetric(LevelSet, dataID, inPhase, fractions, range_min, range_max);
}

std::string qfeStudy::qfeGetSeriesDataPath()
{
	return this->seriesDataPath;
}

void qfeStudy::qfeSetSeriesDataFolder(std::string folderPath)
{
	 this->seriesDataPath = folderPath;
}

bool qfeStudy::getValueTextureCoordinates(const std::array<float,3> textureCoords, const Array3b &input)
{
	std::array<float,3> point;
	point[0] = input.ni*textureCoords[0]; point[1] = input.nj*textureCoords[1]; point[2] = input.nk*textureCoords[2];

	bool output = false;
	if(input.inRange((unsigned int) point[0], (unsigned int) point[1], (unsigned int) point[2]))
		output =  input((unsigned int) point[0], (unsigned int) point[1], (unsigned int) point[2]);

	return output;
}

float qfeStudy::getValueTextureCoordinates(const std::array<float,3> textureCoords, const Array3f &input)
{
	std::array<float,3> point;
	point[0] = input.ni*textureCoords[0]; point[1] = input.nj*textureCoords[1]; point[2] = input.nk*textureCoords[2];
	return input.interpolate_value(point);
}

std::array<float,3> qfeStudy::getGradientTextureCoordinates(const std::array<float,3> textureCoords, const Array3f &input)
{
	std::array<float,3> point;
	point[0] = input.ni*textureCoords[0]; point[1] = input.nj*textureCoords[1]; point[2] = input.nk*textureCoords[2];
	return input.interpolate_gradient(point);
}

std::array<float,3> qfeStudy::getValueTextureCoordinates(const std::array<float,3> textureCoords, const Array3Vec3 &input)
{
	std::array<float,3> point;
	point[0] = input.ni*textureCoords[0]; point[1] = input.nj*textureCoords[1]; point[2] = input.nk*textureCoords[2];
	return input.interpolate_value(point);
}

void qfeStudy::qfeGetIsoSurface(qfeVolume *volume, Array3f *input, const float isoValue, qfeModel **model)
{
	qfeModel *local_model = new qfeModel();

	qfeColorRGB *color = new qfeColorRGB();
	color->r = 0.514f; color->g = 0.871f; color->b = 0.486f;
	local_model->qfeSetModelColor(*color);

	vtkSmartPointer<vtkStructuredPoints> grid = 
		vtkSmartPointer<vtkStructuredPoints>::New();
	grid->SetDimensions(input->ni,input->nj,input->nk);
	//get voxel spacing
	qfeGrid      *volume_grid;
	qfeVector     spacing;	
	volume->qfeGetVolumeGrid(&volume_grid);
	volume_grid->qfeGetGridExtent(spacing.x,spacing.y,spacing.z);
	grid->SetSpacing(1.0f,1.0f,1.0f);
	//grid->SetSpacing(spacing.x,spacing.y,spacing.z);
	grid->SetNumberOfScalarComponents(1);
	grid->SetScalarTypeToDouble();
	
	//fill every entry of the image data
	for (unsigned int z = 0; z < input->nk; z++)
	{
		for (unsigned int y = 0; y < input->nj; y++)
		{  
			for (unsigned int x = 0; x < input->ni; x++)
			{
				double* pixel = static_cast<double*>(grid->GetScalarPointer(x,y,z));
				float val = input->at(x,y,z);
				pixel[0] = val;
			}
		}
	}
	
	// Create the isosurface
	vtkSmartPointer<vtkContourFilter> contours = 
		vtkSmartPointer<vtkContourFilter>::New();
	contours->SetInput(grid);
	contours->GenerateValues(1, isoValue, isoValue);
	contours->ComputeNormalsOn();
	contours->Update();
	  
	vtkSmartPointer<vtkPolyData> polyData =
		vtkSmartPointer<vtkPolyData>::New();

	polyData = contours->GetOutput();

	local_model->qfeSetModelMesh(polyData);
	
	local_model->qfeSetModelVisible(true);

	(*model) = local_model;
}

void qfeStudy::qfeGetIsoSurface(qfeVolume *volume, const Array3f* solid, Array3f *input, const float isoValue, qfeModel **model)
{
	Array3f updated_input(input->ni, input->nj, input->nk);

	std::array<float,3> point;

	std::vector<std::vector<int>> N6(6); 
	for(unsigned int i = 0; i<N6.size(); i++)
		N6[i].resize(3);

	N6[0][0] =  0; N6[1][1] = -1; N6[2][2] =  0;
	N6[1][0] =  0; N6[1][1] =  1; N6[2][2] =  0;
	N6[2][0] =  1; N6[1][1] =  0; N6[2][2] =  0;
	N6[3][0] = -1; N6[1][1] =  0; N6[2][2] =  0;
	N6[4][0] =  0; N6[1][1] =  0; N6[2][2] =  1;
	N6[5][0] =  0; N6[1][1] =  0; N6[2][2] = -1;

	for (unsigned int z = 0; z < input->nk; z++)
	{
		for (unsigned int y = 0; y < input->nj; y++)
		{  
			for (unsigned int x = 0; x < input->ni; x++)
			{
				point[0] = (float)x/input->ni; point[1] = (float)y/input->nj; point[2] = (float)z/input->nk;

				float phi = this->getValueTextureCoordinates(point, *solid);

				updated_input(x,y,z) = input->at(x,y,z);

				float val = input->at(x,y,z);

				if(phi>0.0f && val >= isoValue)
				{
					//check neighbors and update their value if they are outside the mesh and have a high enough isoValue
					for(unsigned int i = 0; i<N6.size(); i++)
					{
						unsigned int ii = x+N6[i][0];
						unsigned int ij = y+N6[i][1];
						unsigned int ik = z+N6[i][2];

						float val2 = input->at(ii,ij,ik);

						if(input->inRange(ii,ij,ik) && val2 >= isoValue)
						{
							point[0] = (float)ii/input->ni; point[1] = (float)ij/input->nj; point[2] = (float)ik/input->nk;

							float phip = this->getValueTextureCoordinates(point, *solid);

							if(phip>=0.0f)
							{
								float x = -phi / (phip - phi);
								updated_input(ii,ij,ik) = val - (val-isoValue)*1.0f/x;
							}
						}
					}
				}
			}
		}
	}

	this->qfeGetIsoSurface(volume,&updated_input,isoValue,model);
}


void qfeStudy::qfeGetIsoSurface(qfeVolume *volume, const float isoValue, qfeModel **model)
{
	qfeModel *local_model = new qfeModel();

	qfeColorRGB *color = new qfeColorRGB();
	color->r = 0.514f; color->g = 0.871f; color->b = 0.486f;
	local_model->qfeSetModelColor(*color);

	vtkSmartPointer<vtkStructuredPoints> grid = 
		vtkSmartPointer<vtkStructuredPoints>::New();

	unsigned int ni, nj, nk;
	volume->qfeGetVolumeSize(ni, nj, nk);
	grid->SetDimensions(ni, nj, nk);
	//get voxel spacing
	qfeGrid      *volume_grid;
	qfeVector     spacing;	
	volume->qfeGetVolumeGrid(&volume_grid);
	volume_grid->qfeGetGridExtent(spacing.x,spacing.y,spacing.z);
	grid->SetSpacing(1.0f,1.0f,1.0f);
	//grid->SetSpacing(spacing.x,spacing.y,spacing.z);
	grid->SetNumberOfScalarComponents(1);
	grid->SetScalarTypeToDouble();
	
	float val = 0.0f;
	qfeVector vec;
	unsigned int components = 1;
	volume->qfeGetVolumeComponentsPerVoxel(components);
	//fill every entry of the image data
	for (unsigned int z = 0; z < nk; z++)
	{
		for (unsigned int y = 0; y < nj; y++)
		{  
			for (unsigned int x = 0; x < ni; x++)
			{
				double* pixel = static_cast<double*>(grid->GetScalarPointer(x,y,z));
				
				if(components == 3)
				{
					volume->qfeGetVolumeVectorAtPosition(x,y,z,vec);
					//we take the first component since it is most likely the most useful component
					val = vec.x;
				}

				if(components == 1)
					volume->qfeGetVolumeScalarAtPosition(x, y, z, val);

				pixel[0] = val;
				if(val > 0.0f)
				{
					pixel[0] = val;
				}
			}
		}
	}
	
	// Create the isosurface
	vtkSmartPointer<vtkContourFilter> contours = 
		vtkSmartPointer<vtkContourFilter>::New();
	contours->SetInput(grid);
	contours->GenerateValues(1, isoValue, isoValue);
	contours->ComputeNormalsOn();
	contours->Update();
	  
	vtkSmartPointer<vtkPolyData> polyData =
		vtkSmartPointer<vtkPolyData>::New();

	polyData = contours->GetOutput();

	local_model->qfeSetModelMesh(polyData);
	
	local_model->qfeSetModelVisible(true);

	(*model) = local_model;
}

//Apply several iterations of a very simple "Jacobi"-style propagation of valid velocity data in all directions
void qfeStudy::extrapolateVectorField(Array3Vec3& grid, Array3b& valid)
{
	Array3b old_valid(valid.ni,valid.nj,valid.nk);
	for(unsigned int layers = 0; layers < 3; ++layers)
	{
		old_valid = valid;
		for(unsigned int k = 1; k < grid.nk-1; ++k) for(unsigned int j = 1; j < grid.nj-1; ++j) for(unsigned int i = 1; i < grid.ni-1; ++i)
		{
			float sum_x = 0.0f;
			float sum_y = 0.0f;
			float sum_z = 0.0f;

			int count = 0;
			if(!old_valid(i,j,k))
			{
				if(old_valid(i+1,j,k))
				{
					sum_x += grid(i+1,j,k)[0];
					sum_y += grid(i+1,j,k)[1];
					sum_z += grid(i+1,j,k)[2];
					++count;
				}
				if(old_valid(i-1,j,k))
				{
					sum_x += grid(i-1,j,k)[0];
					sum_y += grid(i-1,j,k)[1];
					sum_z += grid(i-1,j,k)[2];
					++count;
				}
				if(old_valid(i,j+1,k))
				{
					sum_x += grid(i,j+1,k)[0];
					sum_y += grid(i,j+1,k)[1];
					sum_z += grid(i,j+1,k)[2];
					++count;
				}
				if(old_valid(i,j-1,k))
				{
					sum_x += grid(i,j-1,k)[0];
					sum_y += grid(i,j-1,k)[1];
					sum_z += grid(i,j-1,k)[2];
					++count;
				}
				if(old_valid(i,j,k+1))
				{
					sum_x += grid(i,j,k+1)[0];
					sum_y += grid(i,j,k+1)[1];
					sum_z += grid(i,j,k+1)[2];
					++count;
				}
				if(old_valid(i,j,k+1))
				{
					sum_x += grid(i,j,k-1)[0];
					sum_y += grid(i,j,k-1)[1];
					sum_z += grid(i,j,k-1)[2];
					++count;
				}

				//if any neighbour cell was valid,
				//we assign the cell their average value and make it valid
				if(count > 0)
				{
					grid(i,j,k)[0] = sum_x/(float) count;
					grid(i,j,k)[1] = sum_y/(float) count;
					grid(i,j,k)[2] = sum_z/(float) count;
					valid(i,j,k) = true;
				}
			}
		}
	}
}

qfeVolume qfeStudy::qfeArray3fToQfeVolume(Array3f &array, float rangeMin, float rangeMax, const std::string &label)
{
  qfeVolume volume;
  volume.qfeSetVolumeData(qfeValueTypeFloat, array.ni, array.nj, array.nk, 1, &(array.a[0]));
  volume.qfeSetVolumeGrid(new qfeGrid());
  volume.qfeSetVolumeValueDomain(rangeMin, rangeMax);
  volume.qfeSetVolumeLabel(label);
  return volume;
}

void qfeStudy::qfeStoreAllMetricData(const float currentPhase, int fileNumber)
{
	std::vector<string> labels;

	std::vector<metric_descriptor> scalar_metrics;
	std::vector<metric_descriptor> vector_metrics;

	this->qfeGetScalarMetricList(scalar_metrics);
	this->qfeGetVectorMetricList(vector_metrics);

	for(unsigned int i = 0; i<scalar_metrics.size(); i++)
	{
		labels.push_back(scalar_metrics.at(i).name);
	}

	for(unsigned int i = 0; i<vector_metrics.size(); i++)
	{
		labels.push_back(vector_metrics.at(i).name);
	}

	this->qfeStoreMetricData(labels, currentPhase, fileNumber);
}

void qfeStudy::qfeStoreAllMetricData(const Array3f* solid, const float currentPhase, int fileNumber)
{
	std::vector<string> labels;

	std::vector<metric_descriptor> scalar_metrics;
	std::vector<metric_descriptor> vector_metrics;

	this->qfeGetScalarMetricList(scalar_metrics);
	this->qfeGetVectorMetricList(vector_metrics);

	for(unsigned int i = 0; i<scalar_metrics.size(); i++)
	{
		labels.push_back(scalar_metrics.at(i).name);
	}

	for(unsigned int i = 0; i<vector_metrics.size(); i++)
	{
		labels.push_back(vector_metrics.at(i).name);
	}

	this->qfeStoreMetricData(labels, solid, currentPhase, fileNumber);
}

void qfeStudy::qfeStoreMetricData(const std::vector<std::string> labels, const float currentPhase, int fileNumber)
{
	Array3f mock_solid(1,1,1);
	mock_solid(0,0,0) = 1.0f;
	
	bool mesh_exists = false;
	if(segmentation.size()>0)
		mesh_exists = true;
	
	if(mesh_exists && level_set_computed == false)
	{
		this->qfeComputeLeveLSet();
		
		if(mesh_level_set.ni > 1 && mesh_level_set.nj > 1 && mesh_level_set.nk > 1)
			level_set_computed = true;
	}
	if(level_set_computed)
	{
		mock_solid = this->mesh_level_set;
	}

	//filter label list:
	
	std::vector<metric_descriptor> scalar_metrics;
	std::vector<metric_descriptor> vector_metrics;

	this->qfeGetScalarMetricList(scalar_metrics);
	this->qfeGetVectorMetricList(vector_metrics);
	
	bool second_data_set_available = sim_velocities.size() != 0;

	std::vector<std::string> valid_labels;

	for(unsigned int i = 0; i<labels.size(); i++)
	{
		string label = labels.at(i);

		bool label_found = false;
		
		for(unsigned int i = 0; i<scalar_metrics.size(); i++)
		{
			if(
					label.compare(scalar_metrics.at(i).name)==0 && 
					(scalar_metrics.at(i).needs_solid == false || this->level_set_computed) && 
					(
						!scalar_metrics.at(i).needs_second_dataset || 
						(
							scalar_metrics.at(i).needs_second_dataset && second_data_set_available
						)
					)
			  )
			{
				valid_labels.push_back(label);
				label_found = true;
				break;
			}
		}

		if(!label_found)
		{
			for(unsigned int i = 0; i<vector_metrics.size(); i++)
			{
				if(
						label.compare(vector_metrics.at(i).name)==0 && 
						(vector_metrics.at(i).needs_solid == false || this->level_set_computed) && 
						(
							!vector_metrics.at(i).needs_second_dataset || 
							(
								vector_metrics.at(i).needs_second_dataset && second_data_set_available
							)
						)
				  )
				{
					valid_labels.push_back(label);
					break;
				}
			}
		}
	}

	this->qfeStoreMetricData(valid_labels, &mock_solid, currentPhase, fileNumber);
}

void qfeStudy::qfeStoreMetricData(const std::vector<std::string> labels, const Array3f* solid, const float currentPhase, int fileNumber)
{
	//filter label list:	
	std::vector<metric_descriptor> scalar_metrics;
	std::vector<metric_descriptor> vector_metrics;

	this->qfeGetScalarMetricList(scalar_metrics);
	this->qfeGetVectorMetricList(vector_metrics);
	
	bool second_data_set_available = sim_velocities.size() != 0;

	std::vector<std::string> valid_labels;

	for(unsigned int i = 0; i<labels.size(); i++)
	{
		string label = labels.at(i);

		bool label_found = false;
		
		for(unsigned int i = 0; i<scalar_metrics.size(); i++)
		{
			if(
					label.compare(scalar_metrics.at(i).name)==0 && 
					(
						!scalar_metrics.at(i).needs_second_dataset || 
						(
							scalar_metrics.at(i).needs_second_dataset && second_data_set_available
						)
					)
			  )
			{
				valid_labels.push_back(label);
				label_found = true;
				break;
			}
		}

		if(!label_found)
		{
			for(unsigned int i = 0; i<vector_metrics.size(); i++)
			{
				if(
						label.compare(vector_metrics.at(i).name)==0 && 
						(
							!vector_metrics.at(i).needs_second_dataset || 
							(
								vector_metrics.at(i).needs_second_dataset && second_data_set_available
							)
						)
				  )
				{
					valid_labels.push_back(label);
					break;
				}
			}
		}
	}

	qfeGrid *grid = new qfeGrid();
	if(pcap.size()>0)
		pcap.at(0)->qfeGetVolumeGrid(&grid);
	else
	{
		std::cout<<"Load data first"<<std::endl;
		return;
	}

	//dimensions
	unsigned int dimX, dimY, dimZ;
	pcap.at(0)->qfeGetVolumeSize(dimX, dimY, dimZ);

	std::ostringstream strs;
	if(fileNumber<10)
		strs << '0' << fileNumber;
	else
		strs << fileNumber;
	std::string folderName = this->qfeGetSeriesDataPath() + "metrics/";

	//create the folder:
	_mkdir(folderName.c_str());

	std::string filename = folderName + "metrics_"+strs.str()+".vti";

	std::cout<<"  Processing file '"<<filename<<"'"<<std::endl;
	 
	qfeImageData *imageData = qfeImageData::New();
	imageData->SetDimensions(dimX,dimY,dimZ);

	//time variables
	float        triggerTime;
	float        phaseDuration;

	// Grid properties
	qfeFloat origin[3];
	qfeFloat axis[3][3];
	qfeFloat extent[3];


	//get meta data
	pcap.at(0)->qfeGetVolumeTriggerTime(triggerTime);
    pcap.at(0)->qfeGetVolumePhaseDuration(phaseDuration);  
	
	qfeFloat venc[3] = {0.0, 0.0, 0.0};
	pcap.at(0)->qfeGetVolumeVenc(venc[0], venc[1], venc[2]);
	
	grid->qfeGetGridOrigin(origin[0], origin[1], origin[2]);
	grid->qfeGetGridAxes(axis[0][0], axis[0][1], axis[0][2],
						 axis[1][0], axis[1][1], axis[1][2],
						 axis[2][0], axis[2][1], axis[2][2]);  
	grid->qfeGetGridExtent(extent[0], extent[1], extent[2]);

	imageData->SetOrigin((double)origin[0],(double)origin[1],(double)origin[2]);
	imageData->SetAxisX((double)axis[0][0],(double)axis[0][1],(double)axis[0][2]);
	imageData->SetAxisY((double)axis[1][0],(double)axis[1][1],(double)axis[1][2]);
	imageData->SetAxisZ((double)axis[2][0],(double)axis[2][1],(double)axis[2][2]);
	imageData->SetSpacing((double)extent[0],(double)extent[1],(double)extent[2]);  
	imageData->SetPhaseDuration(phaseDuration);
	imageData->SetPhaseTriggerTime(triggerTime);
	imageData->SetVenc(venc[0], venc[1], venc[2]);

	std::vector<vtkSmartPointer<vtkFloatArray>> scalar_list;
	std::vector<vtkSmartPointer<vtkFloatArray>> vector_list;

	Array3f scalar_data(0,0,0);
	Array3Vec3 vector_data(0,0,0);
	bool existing_metric = true;

	float range_min, range_max;
	
	for(unsigned int metric = 0; metric<valid_labels.size(); metric++)
	{
		string label = valid_labels.at(metric);
		string output_label = label;//.substr(0, label.find_last_of(" 1")); //remove trailing " 1" if applicable

		std::cout<<"	Processing "<<output_label<<", metric "<<metric+1<<" out of "<<valid_labels.size()<<std::endl;

		bool scalar = false;
		bool vector = false;

		unsigned int index = 0;

		for(unsigned int i = 0; i<scalar_metrics.size(); i++)
		{
			if(label.compare(scalar_metrics.at(i).name)==0)
			{
				scalar = true;
				break;
			}
		}
		if(!scalar)
		{
			for(unsigned int i = 0; i<vector_metrics.size(); i++)
			{
				if(label.compare(vector_metrics.at(i).name)==0)
				{
					vector = true;
					break;
				}
			}
		}

		if(scalar || vector)
		{
			 if(scalar)
			 {
				 this->qfeGetScalarMetric(label,solid,currentPhase,existing_metric,scalar_data,range_min,range_max);
				 
				 scalar_list.push_back(vtkSmartPointer<vtkFloatArray>::New());
				 index = scalar_list.size()-1;

				 scalar_list.at(index)->SetNumberOfComponents(1);
				 scalar_list.at(index)->SetNumberOfTuples(dimX * dimY * dimZ);
			 }
			 if(vector)
			 {
				 this->qfeGetVectorMetric(label,solid,currentPhase,existing_metric,vector_data);

				 vector_list.push_back(vtkSmartPointer<vtkFloatArray>::New());
				 index = vector_list.size()-1;
				 
				 vector_list.at(index)->SetNumberOfComponents(3);
				 vector_list.at(index)->SetNumberOfTuples(dimX * dimY * dimZ);
			 }

			for (unsigned int k = 0; k < dimZ; ++k)
			{
				for (unsigned int j = 0; j < dimY; ++j)
				{
					for (unsigned int i = 0; i < dimX; ++i)
					{		
						int id = i+dimX*(j+dimY*k);

						if(scalar)
						{
							if(scalar_data.inRange(i,j,k))
								scalar_list.at(index)->SetValue(id, (float) scalar_data(i,j,k));
							else
								scalar_list.at(index)->SetValue(id, 0.0f);
						}
					
						if(vector)
						{
							if(vector_data.inRange(i,j,k))
							{
								std::array<float,3> vector = vector_data(i,j,k);
								vector_list.at(index)->SetTuple3(id, vector[0], vector[1], vector[2]);
							}
							else
								vector_list.at(index)->SetTuple3(id, 0.0f, 0.0f, 0.0f);
						}
					}
				}
			}

			if(scalar)
				scalar_list.at(index)->SetName(output_label.c_str());

			if(vector)
				vector_list.at(index)->SetName(output_label.c_str());
		}
	}

	for(unsigned int i = 0; i<scalar_list.size(); i++)
	{
		imageData->GetPointData()->AddArray(scalar_list.at(i));
		//qflow_scalar->SetName(labels.at(metric).c_str());
		imageData->Update();
	}
	
	for(unsigned int i = 0; i<vector_list.size(); i++)
	{
		imageData->GetPointData()->AddArray(vector_list.at(i));
		//qflow_vector->SetName(labels.at(metric).c_str());
		imageData->Update();
	}

	// Write the temporal MIP
    qfeVTIImageDataWriter *writer = qfeVTIImageDataWriter::New();
    writer->SetInput(imageData);
    writer->SetFileName(filename.c_str());
    writer->SetDataModeToBinary();
    writer->Write(); 
    writer->Delete();

	//Delete is taken care of for vtkSmartPointers automatically when they get out of scope
}

void qfeStudy::qfeStoreUserSelectedMetrics()
{	
	unsigned phases = pcap.size();

	std::vector<std::string> labels = this->UserSelectMetrics(true);

	std::cout<<"Starting export:"<<std::endl;

	for(unsigned int i = 0; i<labels.size(); i++)
	{
		std::cout<<" -"<<labels.at(i)<<std::endl;
	}

	if(labels.size() == 0)
	{		
		QMessageBox *msgBox = new QMessageBox(0); //not setting a parent is not so smart, but there is no QWidget parent accessable
		msgBox->setText("No labels were selected, dump data to CSV?");
		//msgBox->setWindowModality(Qt::NonModal);
		//msgBox->setInformativeText("Please set a source and sink first.");
		msgBox->setStandardButtons(QMessageBox::Yes|QMessageBox::No);
		msgBox->setDefaultButton(QMessageBox::Yes);

		int ret = msgBox->exec();

		std::cout<<ret<<std::endl;
		if(ret == QMessageBox::Yes)
		{
			this->qfeStoreCSVFile();

			return;
		}

	}

	if(labels.size() == 0)
	{
		return;
	}

	this->qfeGetProgressBar("Exporting data...", phases);

	for (unsigned i = 0; i < phases; i++) 
	{
		this->qfeUpdateProgressBar(i);
		if(this->qfeWasCancelledProgressBar())
			break;

		cout << " Progress (" << i+1 << "/" << phases << ")" << endl;
	
		qfeStoreMetricData(labels, (float)i, i);
	}

	this->qfeClearProgressBar();

	std::cout<<"Finished"<<std::endl;
}

void qfeStudy::qfeStoreCSVFile()
{
	  std::fstream filestrTime;
	  filestrTime.open ("csv_dump.CSV", fstream::in | ios::trunc);
	  filestrTime.clear();  
	  filestrTime.close();
	  filestrTime.open ("csv_dump.CSV", std::fstream::app);  

	  Array3Vec3 vectorField;

	  //format [x,y,z, <velocity vector>, t]
	  unsigned phases = pcap.size();
	  for (unsigned t = 0; t < phases; t++) 
	  {
		  cout << "Progress (" << t+1 << "/" << phases << ")" << endl;

		  bool existingMetric;

		  this->qfeGetVectorMetric("Velocity 1", t , existingMetric, vectorField);		  

		  for(unsigned int z = 0; z<vectorField.nk; z++)
		  {
			  for(unsigned int y = 0; y<vectorField.nj; y++)
			  {
				  for(unsigned int x = 0; x<vectorField.ni; x++)
				  {
					  std::array<float,3> v = vectorField.at(x,y,z);
					  filestrTime<<
						  x<<","<<
						  y<<","<<
						  z<<","<<
						  v[0]<<","<<
						  v[1]<<","<<
						  v[2]<<","<<
						  t<<"\n";
				  }
			  }
		  }
	  }

	  filestrTime.close();

	  cout << "Data written to 'csv_dump.CSV'" << endl;
}

void qfeStudy::qfeComputeLeveLSet()
{
	this->mesh_level_set.resize(0,0,0);

	if(segmentation.size()>0)
		this->loadMesh(segmentation);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeStudy::loadMesh(vector<qfeModel*> modelSeries)
{
	if (modelSeries.size()<1) 
	{
		return qfeSuccess;
	}
		
	if (modelSeries.size() >= 1)
	{
		cout<<"Preprocessing mesh..."<<endl;

		vtkPolyData *mesh;

		qfeModel *model;
		model = modelSeries.at(0);
		model->qfeGetModelMesh(&mesh);
		mesh->BuildCells();
		mesh->BuildLinks(0);
		mesh->Update();

		mesh->BuildCells();
		mesh->BuildLinks(0);
		mesh->Update();

		//clean data, to remove duplicate points and cells:
		vtkSmartPointer<vtkCleanPolyData> pointCleaner =
			vtkSmartPointer<vtkCleanPolyData>::New();
		pointCleaner->SetInputConnection (mesh->GetProducerPort());
		pointCleaner->ConvertLinesToPointsOn();
		pointCleaner->ConvertPolysToLinesOn();
		pointCleaner->ConvertStripsToPolysOn();
		pointCleaner->PointMergingOn();
		pointCleaner->Update();

		vtkPolyData* resultClean = pointCleaner->GetOutput();

		mesh->DeepCopy(resultClean);

		//triangulate to be sure:
		vtkSmartPointer<vtkTriangleFilter> triangleFilter =
			vtkSmartPointer<vtkTriangleFilter>::New();
		triangleFilter->SetInputConnection(mesh->GetProducerPort());
		triangleFilter->Update();

		vtkPolyData* result = triangleFilter->GetOutput();
	
		mesh->DeepCopy(result);
		//optimize random access:
		mesh->BuildCells();
		mesh->BuildLinks(result->GetNumberOfPoints());
		
		mesh->Update();

		convertMeshToLevelSet(mesh);
	}

	return qfeSuccess;
}

qfeReturnStatus qfeStudy::convertMeshToLevelSet(vtkPolyData *mesh)
{
	vector<Vec3f> vertices;
	QList<Vec3f> normals; //needs a lot of replacements and is faster than std vector or list in this case
	vector<Vec3ui> triangles;

	//load in all points/vertices from the mesh
	for(vtkIdType id = 0; id < mesh->GetNumberOfPoints(); id++)
	{
		double p[3];
		mesh->GetPoint(id,p);

		Vec3f vertex(p[0],p[1],p[2]);

		vertices.push_back(vertex);
	}

	//cout<<"Checking for duplicate triangles:"<<endl;
	//std::string bar; //progress bar
	vtkIdType nc = mesh->GetNumberOfCells();

	//we only load the first half of the triangles due to duplicates...
	for(vtkIdType index = 0; index<nc/2; index++)
	{
		vtkCell *cell = mesh->GetCell(index);
		if(cell->GetNumberOfPoints()!=3)
		{
			cout<<"Cell "<<index<<" is not a triangle..."<<endl;
		}
		
		Vec3ui Triangle(cell->GetPointId(0),cell->GetPointId(1),cell->GetPointId(2));
		triangles.push_back(Triangle);
	}

	//add null vectors to fill the list:
	for(int n = 0; n<(int)vertices.size(); n++)
	{
		Vec3f Empty(0,0,0);
		normals.push_back(Empty);
	}
	//we traverse all triangles and add the normal to every vertex on the way
	for(int t = 0; t<(int)triangles.size(); t++)
	{
		Vec3ui triangle = triangles.at(t);
		Vec3f p1 = vertices.at(triangle[0]);
		Vec3f p2 = vertices.at(triangle[1]);
		Vec3f p3 = vertices.at(triangle[2]);

		Vec3f U = p2 - p1;
		Vec3f V = p3 - p1;

		Vec3f N(0,0,0);

		N[0] = U[1]*V[2] - U[2]*V[1];
		N[1] = U[2]*V[0] - U[0]*V[2];
		N[2] = U[0]*V[1] - U[1]*V[0];

		Vec3f old1 = normals.at(triangle[0]);
		Vec3f old2 = normals.at(triangle[1]);
		Vec3f old3 = normals.at(triangle[2]);

		normals.replace(triangle[0],old1+N);
		normals.replace(triangle[1],old2+N);
		normals.replace(triangle[2],old3+N);
	}
	//normalize the normals:
	for(int n = 0; n<normals.size(); n++)
	{
		Vec3f N = normals.at(n);
		float length = sqrt(sqr(N[0])+sqr(N[1])+sqr(N[2]));
		normals.replace(n,N/length);
	}

	cout<<"Number of vertices is "<<vertices.size()<<endl;
	cout<<"Number of normals is "<<normals.size()<<endl;
	cout<<"Number of triangles is "<<triangles.size()<<endl;	

	//cout<<endl<<"Removed "<<100-(float)Triangles.size()/mesh->GetNumberOfCells()*200<<"% of the original triangles"<<endl<<endl;
	
	unsigned int w, h, d;
	pcap.front()->qfeGetVolumeSize(w, h, d);

	cout<<"Level set size: "<<w<<"x"<<h<<"x"<<d<<std::endl;

	cout<<"Computing levelset from mesh..."<<std::endl;

	//get voxel spacing
	qfeGrid      *volume_grid;
	qfeVector     spacing;	
	pcap.front()->qfeGetVolumeGrid(&volume_grid);
	volume_grid->qfeGetGridExtent(spacing.x,spacing.y,spacing.z);

	float dx = min(spacing.x, min(spacing.y, spacing.z));

	mesh_level_set.resize(w, h, d);
	mesh_level_set.assign(0);
	Vec3f origin(0.0,0.0,0.0); //sets the origin to (0,0,0)
	make_level_set3(triangles, vertices,
					 origin, 1.0f, w, h, d,
					 mesh_level_set,5);

	//negative value found (thus a solid is initialized):
	bool negativeValue = false;
	bool positiveValue = false;

	for(unsigned int k = 0; k<mesh_level_set.nk; k++)
	{
		for(unsigned int j = 0; j<mesh_level_set.nj; j++)
		{
			for(unsigned int i = 0; i<mesh_level_set.ni; i++)
			{
				if (mesh_level_set(i,j,k)<0.0) 
				{
					negativeValue = true;
				}							
				if (mesh_level_set(i,j,k)>0.0) 
				{
					positiveValue = true;
				}
				//flip sign such that the inside of the mesh becomes "hollow"
				mesh_level_set(i,j,k) = -1.0f*mesh_level_set(i,j,k)*dx;
			}
		}
	}

	return qfeSuccess;
}

std::vector<std::string> qfeStudy::UserSelectMetrics(bool initially_checked)
{
	return this->UserSelectMetrics(initially_checked, false);
}

std::vector<std::string> qfeStudy::UserSelectMetrics(bool initially_checked, bool scalar_only)
{
	std::vector<qfeStudy::metric_descriptor> metricList;
	qfeGetExistingMetricList(metricList, scalar_only);

	std::vector<QString> metricLabelList;
	for(unsigned int i = 0; i<metricList.size(); i++)
	{
		QString label = QString::fromStdString(metricList.at(i).name);

		metricLabelList.push_back(label);
	}

	QDialog dlg;
	QVBoxLayout *mainLayout = new QVBoxLayout;
	
	std::vector<QCheckBox*> checkBoxes;
	
	int group_size = ceil(sqrt(metricLabelList.size())); //we want the buttons to be a square of options

	unsigned int current_button_index = 0;
	for(unsigned int row = 0; row<group_size; row++)
	{
		QDialogButtonBox *buttonBox = new QDialogButtonBox();
		buttonBox->setOrientation(Qt::Horizontal);
		buttonBox->setCenterButtons(true);

		//generate checkboxes
		for(unsigned int i = 0; i<group_size; i++)
		{
			if(current_button_index>=metricLabelList.size())
				continue;

			QCheckBox *item = new QCheckBox();
			item->setText(metricLabelList.at(current_button_index));
			//item->blockSignals(true);
			if(initially_checked)
				item->setCheckState(Qt::Checked);
			else
				item->setCheckState(Qt::Unchecked);
		
			checkBoxes.push_back(item);
			buttonBox->addButton(checkBoxes[current_button_index], QDialogButtonBox::ActionRole);

			current_button_index++;
		}

		mainLayout->addWidget(buttonBox);
	}

	QDialogButtonBox *buttonBoxControl = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	QObject::connect(buttonBoxControl, SIGNAL(accepted()), &dlg, SLOT(accept()));
	QObject::connect(buttonBoxControl, SIGNAL(rejected()), &dlg, SLOT(reject()));

	mainLayout->addWidget(buttonBoxControl);

	dlg.setLayout(mainLayout);

	//get all selected labels:
	std::vector<std::string> labels;

	if(dlg.exec() == QDialog::Accepted)
	{

		for(unsigned int i = 0; i<metricLabelList.size(); i++)
		{
			if(checkBoxes[i]->checkState() == Qt::Checked) //this metric should be exported
			{
				labels.push_back(metricLabelList.at(i).toUtf8().constData());
			}
		}
	}
		
	dlg.close();

	return labels;
}

std::vector<std::string> qfeStudy::UserSelectMetricsDropDown(unsigned int numberOfMetrics, bool scalar_only)
{
	std::vector<qfeStudy::metric_descriptor> metricList;
	qfeGetExistingMetricList(metricList, scalar_only);

	std::vector<QString> metricLabelList;
	for(unsigned int i = 0; i<metricList.size(); i++)
	{
		QString label = QString::fromStdString(metricList.at(i).name);

		metricLabelList.push_back(label);
	}

	QDialog dlg;
	QVBoxLayout *mainLayout = new QVBoxLayout;

	std::vector<QComboBox*> comboBoxes;

	//generate comboboxes
	for(unsigned int i = 0; i<numberOfMetrics; i++)
	{
		QComboBox *item = new QComboBox();
				
		for(unsigned int j = 0; j < metricLabelList.size(); j++)
		{
			item->addItem(metricLabelList.at(j));
		}
		
		comboBoxes.push_back(item);

		mainLayout->addWidget(item);
	}

	QDialogButtonBox *buttonBoxControl = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	QObject::connect(buttonBoxControl, SIGNAL(accepted()), &dlg, SLOT(accept()));
	QObject::connect(buttonBoxControl, SIGNAL(rejected()), &dlg, SLOT(reject()));

	mainLayout->addWidget(buttonBoxControl);

	dlg.setLayout(mainLayout);

	std::vector<std::string> result;
	if(dlg.exec() == QDialog::Accepted)
	{
		//generate comboboxes
		for(unsigned int i = 0; i<numberOfMetrics; i++)
		{
			std::string label = comboBoxes.at(i)->currentText().toUtf8().constData();
			result.push_back(label);
		}
	}

	dlg.close();

	return result;
}
