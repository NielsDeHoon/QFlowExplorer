#pragma once
/**
 * \file   qfeGLShaderSourceReader.h
 * \author Roy van Pelt
 * \class  qfeGLShaderSourceReader
 * \brief  Implements a shader source reader, which read the shader text file and returns it as character array.
 *
 * qfeGLShaderSourceReader reads shader source text from file and returns it as a
 * vtkCharArray object.
 */
#include <iostream>
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>

#include "qflowexplorer.h"

using namespace std;

class qfeGLShaderSourceReader
{
public:
  static qfeReturnStatus qfeGetCwd(char **buffer);
	static qfeReturnStatus qfeLoadFromFile(char *path, char *file, const char **buffer);

  static qfeReturnStatus qfeGetDefaultVertexShader(const char **buffer);
  static qfeReturnStatus qfeGetDefaultGeometryShader(const char **buffer);
  static qfeReturnStatus qfeGetDefaultFragmentShader(const char **buffer);
  static qfeReturnStatus qfeGetDummyShader(const char **buffer);

protected:
  qfeGLShaderSourceReader(){};
  ~qfeGLShaderSourceReader(){};
};
