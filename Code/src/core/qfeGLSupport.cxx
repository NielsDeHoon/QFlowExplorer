#include "qfeGLSupport.h"

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLSupport::qfeCheckFrameBuffer()
{
  GLenum status = (GLenum) glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);

  switch(status) {
        case GL_FRAMEBUFFER_COMPLETE_EXT:
          return qfeSuccess;
        case GL_FRAMEBUFFER_UNSUPPORTED_EXT:
          printf("Unsupported framebuffer format\n");
          return qfeError;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT_EXT:
          printf("Framebuffer incomplete, missing attachment\n");
          return qfeError;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT_EXT:
          printf("Framebuffer incomplete, duplicate attachment\n");
          return qfeError;
        case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
          printf("Framebuffer incomplete, attached images must have same dimensions\n");
          return qfeError;
        case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT:
          printf("Framebuffer incomplete, attached images must have same format\n");
          return qfeError;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER_EXT:
          printf("Framebuffer incomplete, missing draw buffer\n");
          return qfeError;
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER_EXT:
          printf("Framebuffer incomplete, missing read buffer\n");
          return qfeError;
        default:
          return qfeError;
  } 
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLSupport::qfeCheckOpenGL()
{
  GLenum status =(GLenum) glGetError();

  switch(status)
  {
  case GL_NO_ERROR:
    return qfeSuccess;
  case GL_INVALID_ENUM:
    cout << "qfeDriver::qfeCheckOpenGL - An unacceptable value is specified for an enumerated argument" << endl;
    return qfeError;
  case GL_INVALID_VALUE:
    cout << "qfeDriver::qfeCheckOpenGL - A numeric argument is out of range" << endl;
    return qfeError;
  case GL_INVALID_OPERATION:
    cout << "qfeDriver::qfeCheckOpenGL - The specified operation is not allowed in the current state" << endl;
    return qfeError;
  case GL_STACK_OVERFLOW:
    cout << "qfeDriver::qfeCheckOpenGL - This command would cause a stack overflow" << endl;
    return qfeError;
  case GL_STACK_UNDERFLOW:
    cout << "qfeDriver::qfeCheckOpenGL - This command would cause a stack underflow" << endl;
    return qfeError;
  case GL_OUT_OF_MEMORY:
    cout << "qfeDriver::qfeCheckOpenGL - There is not enough memory left to execute the command" << endl;
    return qfeError;
  case GL_TABLE_TOO_LARGE:
    cout << "qfeDriver::qfeCheckOpenGL - The specified table exceeds the implementation's maximum supported table size" << endl;
    return qfeError;
  }

  return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeGLSupport::qfeCheckGLSL(unsigned int id, const char* name)
{
  int len = 0;
  int written = 0;
  char *log;

  if(glIsProgram(id)){
    glGetProgramiv(id, GL_INFO_LOG_LENGTH, &len);
    if(len < 2)
      return qfeError;

    log = (char *) malloc(len*sizeof(char));
    glGetProgramInfoLog(id, len, &written, log);
  }
  else {
    glGetShaderiv(id, GL_INFO_LOG_LENGTH, &len);
    if(len < 2) 
      return qfeError;

    log = (char *) malloc(len*sizeof(char));
    glGetShaderInfoLog(id, len, &written, log);
  }

  if(log){
    cout << name << ": " << endl << log << endl;
    free(log);
  }

  return qfeSuccess;
}
