#pragma once

#include "qflowexplorer.h"

#include "vtkCamera.h"
#include "vtkMatrix4x4.h"
#include "vtkPolyData.h"

#include "qfeFrame.h"
#include "qfeGrid.h"
#include "qfeMatrix4f.h"
#include "qfeVector.h"
#include "qfeViewport.h"
#include "qfeVolume.h"

#define LARGEST_DISTANCE	32768.0
//#define LARGEST_DISTANCE	16384.0

/**
* \file   qfeTransform.h
* \author Roy van Pelt
* \class  qfeTransform
* \brief  Implements conversions between different coordinate systems
* \note   Confidential
*
* Convert coordinates between texture - voxel - patient - world space
*/
class qfeTransform
{
public:

  qfeTransform();
  ~qfeTransform();

  // World <-> Viewport (=Projection Matrix)
  static qfeReturnStatus qfeGetMatrixWorldToViewportOrthogonal(qfeMatrix4f &W2VP, qfeFrame *geo, qfeViewport *vp);
  static qfeReturnStatus qfeGetMatrixViewportToWorldOrthogonal(qfeMatrix4f &VP2W, qfeFrame *geo, qfeViewport *vp);

  static qfeReturnStatus qfeGetMatrixWorldToViewportPerspective(qfeMatrix4f &W2VP, qfeFrame *geo, double aspect);
  static qfeReturnStatus qfeGetMatrixViewportToWorldPerspective(qfeMatrix4f &VP2W, qfeFrame *geo, double aspect);

  // Patient <-> World   (=Model-View Matrix) (OpenGL: World-space = Camera-space)
  static qfeReturnStatus qfeGetMatrixPatientToWorld(qfeMatrix4f &P2W);
  static qfeReturnStatus qfeGetMatrixWorldToPatient(qfeMatrix4f &W2P);  

  // Voxel <-> Patient (Depends on patient position)
  static qfeReturnStatus qfeGetMatrixVoxelToPatient(qfeMatrix4f &V2P, qfeVolume *volume);
  static qfeReturnStatus qfeGetMatrixPatientToVoxel(qfeMatrix4f &P2V, qfeVolume *volume); 

  // Texture <-> Voxel  (Depends on voxel size)
  static qfeReturnStatus qfeGetMatrixTextureToVoxel(qfeMatrix4f &T2V, qfeVolume *volume);
  static qfeReturnStatus qfeGetMatrixVoxelToTexture(qfeMatrix4f &V2T, qfeVolume *volume);

  // Unproject a window coordinate  
  static qfeReturnStatus qfeGetCoordinateUnproject(qfePoint windowCoord, qfePoint &patientCoord, qfeMatrix4f mv, qfeMatrix4f p, qfeViewport *vp);
  static qfeReturnStatus qfeGetCoordinateProject(qfePoint patientCoord, qfePoint &windowCoord, qfeMatrix4f mv, qfeMatrix4f p, qfeViewport *vp); 

  // Transformation mesh -> patient coordiantes
  static qfeReturnStatus qfeTransformMeshToPatient(qfeVolume *volume, vtkPolyData *mesh, qfeMatrix4f &transform);

};
