#pragma once

#include "qflowexplorer.h"

#include "qfeStudy.h"

/**
* \file   qfePatient.h
* \author Roy van Pelt
* \class  qfePatient
* \brief  Implements a patient class
* \note   Confidential
*
* Storage of patient specific information.
* Currently only one study is supported per patient.
*/
class qfePatient
{
public:
  qfePatient();
  ~qfePatient();

  char       *name;
  int         id;
  qfeStudy   *study;  
};
