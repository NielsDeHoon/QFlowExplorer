#pragma once

#include "qflowexplorer.h"

#include <vtkCharArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkShortArray.h>
#include <vtkUnsignedShortArray.h>
#include <vtkFloatArray.h>
#include <vtkSmartPointer.h>

#include <vtkDataArray.h>
#include <vtkImageData.h>
#include <vtkImageImport.h>
#include <vtkMatrix4x4.h>
#include <vtkPointData.h>
#include <vtkVolume.h>

#include <QPair.h>
#include <QVector.h>

#include <vector>

#include "qfeImageData.h"
#include "qfeGrid.h"
#include "qfeVolume.h"
#include "qfeColorMap.h"
#include "fluidsim\array3.h"

/**
* \file   qfeConvert.h
* \author Roy van Pelt
* \class  qfeConvert
* \brief  Implements conversions between VTK / QT and QFE
* \note   Confidential
*
* Convert objects between VTK / QT and QFE
*/

using namespace std;

class qfeConvert
{
public:

  //! Takes a qfeImageData and returns a qfeVolume and its qfeGrid geometry.
  //!
  //! In VTK terms a vtkVolume actor describes the geometry of the volume by means of its bounding box,
  //! while the actual scalars are stored in a vtkImageData object. 
  //! The image data consists of the raw scalars and vtkPointData, where the points determine the voxel spacing.
  //!
  //! For QFE the qfeGrid describes the geometry, including the voxel extent (= spacing in VTK terminology).
  //! The qfeVolume contains the actual data and describes the voxel type.
  static qfeReturnStatus vtkImageDataToQfeVolume(qfeImageData *vtkData, qfeVolume *volume);

  //! Takes a qfeVolume and its qfeGrid geometry and returns a qfeImageData.  
  static qfeReturnStatus qfeVolumeToVtkImageData(qfeVolume *volume, qfeImageData *vtkData);

  //! Takes a QVector based TF and returns a qfeColorMap structure
  static qfeReturnStatus qtTransferFunctionToqfeColorMap
                                              (QVector< QPair<double, double*> > color, 
                                               QVector< QPair<double, double>  > opacity,
                                               QVector< QPair<double, double>  > gradient,
                                               int interpolation, int quantization, int space, 
                                               bool constlight, bool paduni,
                                               qfeColorMap &colorMap);

  static qfeVolume qfeArray3fToQfeVolume(Array3f &array, float rangeMin, float rangeMax, const std::string &label, qfeGrid *grid);

  static qfeVolume qfeFloattoQfeVolume(float *array, unsigned width, unsigned height, unsigned depth, unsigned nrComp,
    float rangeMin, float rangeMax, const std::string &label, qfeGrid *grid);
};
