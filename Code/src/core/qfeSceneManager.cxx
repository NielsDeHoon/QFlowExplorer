#include "qfeSceneManager.h"

//----------------------------------------------------------------------------
qfeSceneManager::qfeSceneManager()
{
  this->state         = sceneStateStartup;  

  this->scene         = new qfeScene();
  this->driver        = NULL;

  /*
  this->colorDefault1.r = this->colorDefault1.g = this->colorDefault1.b  = 1.0f;
  this->colorDefault2.r =  88.0f / 255.0f;
  this->colorDefault2.g = 110.0f / 255.0f;
  this->colorDefault2.b = 135.0f / 255.0f;
  */

  this->colorDefault1.r = this->colorDefault1.g = this->colorDefault1.b  = 0.94;
  this->colorDefault2.r = this->colorDefault2.g = this->colorDefault2.b  = 0.08;    

  this->renderWindow         = NULL;
  this->currentCardiacPhase  = 1;

  this->renderEvent          = 0;
  this->renderCommand        = NULL;

}

//----------------------------------------------------------------------------
qfeSceneManager::~qfeSceneManager()
{
  // Stop the scene manager
  this->qfeSceneManagerStop();

  // Remove scenes
  delete this->scene;
  this->scene = NULL;  
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSetSceneManagerRenderWindow(vtkRenderWindow *rw)
{
  this->renderWindow = rw;

  if(this->renderCommand != NULL)
  {
    this->renderWindow->AddObserver(this->renderEvent, this->renderCommand);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSetSceneManagerViewGeometry(int direction)
{
  qfeGeometry        *geo;
  qfeFrameProjection *frame;
  qfeMatrix4f         R, O, TAX, TCO;
  qfeMatrix4f         TAP, TPA, TLR, TRL, THF, TFH, TVA;
  qfeVector           x, y, z;
  qfeGridOrientation  orientation;

  if(this->scene == NULL) return qfeError;

  this->scene->qfeGetSceneGeometry(&geo);
  frame = (qfeFrameProjection*)geo;

  qfeMatrix4f::qfeSetMatrixIdentity(R);
  if(patient != NULL)
  {
    qfeGrid *grid = NULL;

    if((int)patient->study->pcap.size() > 0)
      patient->study->pcap[0]->qfeGetVolumeGrid(&grid);        

    if(grid != NULL)
    {
      grid->qfeGetGridAxes(R(0,0), R(0,1), R(0,2), R(1,0), R(1,1), R(1,2), R(2,0), R(2,1), R(2,2));
      grid->qfeGetGridOrientation(orientation);
    }

    x.qfeSetVectorElements(1.0f, 0.0f, 0.0f);
    y.qfeSetVectorElements(0.0f, 1.0f, 0.0f);
    z.qfeSetVectorElements(0.0f, 0.0f, 1.0f);

    // To axial (default sagittal)   
    qfeMatrix4f::qfeSetMatrixIdentity(TAX);
    TAX(0,0) =  0.0f; TAX(0,1) =  1.0f; TAX(0,2) =  0.0f;
    TAX(1,0) =  0.0f; TAX(1,1) =  0.0f; TAX(1,2) = -1.0f;
    TAX(2,0) = -1.0f; TAX(2,1) =  0.0f; TAX(2,2) =  0.0f;

    // To coronal (default sagittal)    
    qfeMatrix4f::qfeSetMatrixIdentity(TCO);
    TCO(0,0) =  0.0f; TCO(0,1) =  0.0f; TCO(0,2) =  1.0f;
    TCO(1,0) =  0.0f; TCO(1,1) =  1.0f; TCO(1,2) =  0.0f;
    TCO(2,0) = -1.0f; TCO(2,1) =  0.0f; TCO(2,2) =  0.0f;

    // Correct for scan orientation
    //qfeMatrix4f::qfeSetMatrixIdentity(O);

    //if(orientation == qfeGridAxial)
    //  O = TAX;
    //if(orientation == qfeGridCoronal)
    //  O = TCO; 

    //R = R * O;

    // Patient orthogonal view tranformations
    qfeMatrix4f::qfeSetMatrixIdentity(TAP);
    TAP(0,0) =  0.0f; TAP(0,1) =  0.0f; TAP(0,2) = -1.0f;
    TAP(1,0) =  0.0f; TAP(1,1) = -1.0f; TAP(1,2) =  0.0f;
    TAP(2,0) = -1.0f; TAP(2,1) =  0.0f; TAP(2,2) =  0.0f;

    qfeMatrix4f::qfeSetMatrixIdentity(TPA);
    TPA(0,0) =  0.0f; TPA(0,1) =  0.0f; TPA(0,2) =  1.0f;
    TPA(1,0) =  0.0f; TPA(1,1) = -1.0f; TPA(1,2) =  0.0f;
    TPA(2,0) =  1.0f; TPA(2,1) =  0.0f; TPA(2,2) =  0.0f;

    qfeMatrix4f::qfeSetMatrixIdentity(TLR);
    TLR(0,0) = -1.0f; TLR(0,1) =  0.0f; TLR(0,2) =  0.0f;
    TLR(1,0) =  0.0f; TLR(1,1) = -1.0f; TLR(1,2) =  0.0f;
    TLR(2,0) =  0.0f; TLR(2,1) =  0.0f; TLR(2,2) = -1.0f;

    qfeMatrix4f::qfeSetMatrixIdentity(TRL);
    TRL(0,0) =  1.0f; TRL(0,1) =  0.0f; TRL(0,2) =  0.0f;
    TRL(1,0) =  0.0f; TRL(1,1) = -1.0f; TRL(1,2) =  0.0f;
    TRL(2,0) =  0.0f; TRL(2,1) =  0.0f; TRL(2,2) =  1.0f;

    qfeMatrix4f::qfeSetMatrixIdentity(THF);
    THF(0,0) =  0.0f; THF(0,1) =  0.0f; THF(0,2) =  1.0f;
    THF(1,0) = -1.0f; THF(1,1) =  0.0f; THF(1,2) =  0.0f;
    THF(2,0) =  0.0f; THF(2,1) = -1.0f; THF(2,2) =  0.0f;  

    qfeMatrix4f::qfeSetMatrixIdentity(TFH);
    TFH(0,0) =  0.0f; TFH(0,1) =  0.0f; TFH(0,2) = -1.0f;
    TFH(1,0) = -1.0f; TFH(1,1) =  0.0f; TFH(1,2) =  0.0f;
    TFH(2,0) =  0.0f; TFH(2,1) =  1.0f; TFH(2,2) =  0.0f;  

    qfeMatrix4f::qfeSetMatrixIdentity(TVA);
    TVA(0,0) = -0.707107f; TVA(0,1) =  0.0f;      TVA(0,2) =  0.707107f;
    TVA(1,0) =  0.0f;      TVA(1,1) = -1.0f;      TVA(1,2) =  0.0f;
    TVA(2,0) = -0.707107f; TVA(2,1) = -0.353553f; TVA(2,2) = -0.707107f;

    switch(direction)
    {
    case AP: x = x*TAP*R; y = y*TAP*R; z = z*TAP*R;       
       break;                                 
    case PA: x = x*TPA*R; y = y*TPA*R; z = z*TPA*R;       
      break;                                  
    case LR: x = x*TLR*R; y = y*TLR*R; z = z*TLR*R;       
      break;                                  
    case RL: x = x*TRL*R; y = y*TRL*R; z = z*TRL*R; 
      break;                                  
    case HF: x = x*THF*R; y = y*THF*R; z = z*THF*R; 
      break;                                  
    case FH: x = x*TFH*R; y = y*TFH*R; z = z*TFH*R; 
      break;                                  
    case VA: x = x*TVA*R; y = y*TVA*R; z = z*TVA*R; 
      break;
    }
    frame->qfeSetFrameAxes(x.x,x.y,x.z,  y.x,y.y,y.z,  z.x,z.y,z.z);
  }
  else
  {
    switch(direction)
    {
    case AP: frame->qfeSetFrameAxes(+1.0,0.0,0.0,  0.0,0.0,-1.0,  0.0,+1.0,0.0);
      break;
    case PA: frame->qfeSetFrameAxes(+1.0,0.0,0.0,  0.0,0.0,-1.0,  0.0,-1.0,0.0);
      break;
    case LR: frame->qfeSetFrameAxes(0.0,-1.0,0.0,  0.0,0.0,-1.0,  -1.0,0.0,0.0);
      break;
    case RL: frame->qfeSetFrameAxes(0.0,-1.0,0.0,  0.0,0.0,-1.0,  +1.0,0.0,0.0);
      break;
    case HF: frame->qfeSetFrameAxes(-1.0,0.0,0.0,  0.0,+1.0,0.0,  0.0,0.0,-1.0);
      break;
    case FH: frame->qfeSetFrameAxes(+1.0,0.0,0.0,  0.0,+1.0,0.0,  0.0,0.0,+1.0);
      break;
    }
  }

  geo->qfeSetGeometryChanged(true);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSetSceneManagerTemporalPhase(float phase)
{
  vector<qfeVisual *> visuals;

  this->scene->qfeGetSceneVisuals(visuals);

  for(int i=0; i<(int)visuals.size(); i++)
    visuals[i]->qfeSetVisualActiveVolume(phase);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSetSceneManagerColorMap(int visualID, qfeColorMap map)
{
  if(this->scene == NULL) return qfeError;

  vector<qfeVisual*> visuals;
  this->scene->qfeGetSceneVisuals(visuals);

  if((visualID < 0) || (visualID >= (int)visuals.size()))
    return qfeError;
  
  visuals[visualID]->qfeSetVisualColorMap(map);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSetSceneManagerLight(qfeLightSource light)
{
  if(this->scene == NULL) return qfeError;
  
  this->scene->qfeSetSceneLightSource(new qfeLightSource(light));

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSetSceneManagerDisplay(qfeDisplay display)
{
  qfeDisplay  *d;
  qfeColorRGB  c1, c2;
  int          type;

  if(this->scene == NULL) return qfeError;
  
  display.qfeGetDisplayBackgroundType(type); 
  display.qfeGetDisplayBackgroundColorRGB(c1.r, c1.g, c1.b); 
  display.qfeGetDisplayBackgroundColorRGBSecondary(c2.r, c2.g, c2.b); 
  
  this->scene->qfeGetSceneDisplay(&d);  
  d->qfeSetDisplayBackgroundType(type);
  d->qfeSetDisplayBackgroundColorRGB(c1.r, c1.g, c1.b);
  d->qfeSetDisplayBackgroundColorRGBSecondary(c2.r, c2.g, c2.b);

  return qfeSuccess;
}

qfeReturnStatus qfeSceneManager::qfeSetSceneManagerFeatureTF(int id, QVector<QPair<double, double>> &tf, int dataType)
{
  scene->qfeSetSceneFeatureTF(id, tf, dataType);
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeGetSceneManagerScene(qfeScene **scene)
{
  *scene = this->scene;
  return qfeSuccess;  
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::AddObserver(unsigned long sceneEvent, vtkCommand *command)
{
  this->renderEvent   = sceneEvent;
  this->renderCommand = command;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerInit(qfePatient *patient)
{
  if(!(this->state==sceneStateStartup || this->state==sceneStateStopped))  
  {
    cout << "qfeSceneManager::qfeSceneManagerInit: Invalid state" << endl;
    return qfeError;
  }

  this->patient = patient;

  this->state = sceneStateStopped;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerStart(int sceneType)
{
  if(!(this->state==sceneStateStopped || this->state==sceneStateStartup))
  {
    cout << "qfeSceneManager::qfeSceneManagerStart: Invalid state" << endl;
    return qfeError;
  }

  if(this->renderWindow == NULL)
  {
    cout  << "qfeSceneManager::qfeSceneManagerStart: Initialize render window" << endl;
    return qfeError;
  }

  switch(sceneType)
  {
  case sceneDefault  : this->qfeSceneManagerCreateDefault();  break;
  case sceneMPR      : this->qfeSceneManagerCreateMPR();      break;
  case sceneMIP      : this->qfeSceneManagerCreateMIP();      break;
  case sceneDVR      : this->qfeSceneManagerCreateDVR();      break;
  case sceneIFR      : this->qfeSceneManagerCreateIFR();      break;
  case sceneVPR      : this->qfeSceneManagerCreateVPR();      break;
  case sceneUSR      : this->qfeSceneManagerCreateUSR();      break;
  case sceneHCR      : this->qfeSceneManagerCreateHCR();      break;
  case scenePMR      : this->qfeSceneManagerCreatePMR();      break;
  case sceneFS		 : this->qfeSceneManagerCreateFS();       break;
  case sceneIV		 : this->qfeSceneManagerCreateIV();       break;
  case sceneFilter   : qfeSceneManagerCreateFilter();         break;
  case sceneCEV      : qfeSceneManagerCreateCEV();            break;
  case sceneDA      :  this->qfeSceneManagerCreateDA();       break;
  default            : this->qfeSceneManagerCreateDefault();  
  }
  
  //post processing does not work for all drivers (yet)
  if(sceneType != sceneDefault && sceneType != sceneMIP && sceneType != sceneDVR && sceneType != sceneIFR && sceneType != sceneVPR && sceneType != sceneUSR && sceneType != scenePMR)
  {
	qfeVisualPostProcess *visualPostProcess= new qfeVisualPostProcess();
	visualPostProcess->qfeSetVisualStudy(patient->study);
	this->scene->qfeAddSceneVisual(visualPostProcess);
	delete visualPostProcess;
  }

  this->state = sceneStateStarted;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerRender(float *time)
{
  if(this->state != sceneStateStarted)
  {
    cout << "qfeSceneManager::qfeSceneManagerRender: Invalid state" << endl;
    return qfeError;
  }

  // Select the right driver for the 3D scene 
  this->qfeSceneManagerSelectDriver(this->scene, &driver);

  // Start the render process
  if(this->driver != NULL)
  {
    this->driver->qfeRenderCheck();
    this->driver->qfeRenderStart();
  }
  else
  {
    return qfeError;
  }  

  this->state = sceneStateRendering;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerStop()
{
  if(this->state==sceneStateStartup)
  {
    this->state = sceneStateStopped;
    return qfeSuccess;
  }
  
  // Remove driver  
  delete this->driver;
  this->driver = NULL;  

  this->state = sceneStateStopped;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerUpdate()
{
  vector<qfeVisual *> visuals;
  this->scene->qfeGetSceneVisuals(visuals);

  for(int i=0; i<(int)visuals.size(); i++)
  {
    int type = visualNone;
    visuals[i]->qfeGetVisualType(type);

    switch(type)
    {
    case(visualAnatomyIllustrative) : reinterpret_cast<qfeVisualAnatomyIllustrative *>(visuals[i])->qfeUpdateVisual();
                                      break;
    }     
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerRotate(double angle, bool autoupdate)
{
  if((this->driver != NULL) && (this->state == sceneStateRendering))
  {
    this->driver->qfeRenderRotate(angle, autoupdate);
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerRotate360(double angleStep, bool autoupdate, bool screenshot)
{
  if((this->driver != NULL) && (this->state == sceneStateRendering))
  {
    this->driver->qfeRenderRotate360(angleStep, autoupdate, screenshot);
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateDefault()
{
  // Clean scene object
  scene->qfeRemoveSceneVisuals();
  
  // Fill the scene graph
  qfeDisplay         *display  = new qfeDisplay();
  qfeFrameProjection *geometry = new qfeFrameProjection();
  qfeVisualDefault   *visual   = new qfeVisualDefault();

  unsigned int windowSize[2];
  float        windowScale;

  windowScale = 0.9;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);

  display->qfeSetDisplayBackgroundColorRGB(colorDefault1.r,colorDefault1.g,colorDefault1.b);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);

  geometry->qfeSetFrameOrigin(0.0, 0.0, 0.0);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0, 1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
                              windowSize[1] * windowScale);

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry);
  this->scene->qfeAddSceneVisual(visual);

  delete display;
  delete geometry;
  delete visual;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateMPR()
{
  if(this->patient == NULL)
  {
    cout << "qfeSceneManager::qfeSceneManagerCreateMPR - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay                *display         = new qfeDisplay();
  qfeFrameProjection        *geometry        = new qfeFrameProjection();
  qfeLightSource            *light           = new qfeLightSource();
  qfeVisualReformatOrtho    *visualReformat  = new qfeVisualReformatOrtho();
  qfeVisualReformatFixed    *visualQFlow     = new qfeVisualReformatFixed();
  qfeVisualPlanesArrowHeads *visualArrows    = new qfeVisualPlanesArrowHeads();
  qfeVisualAides            *visualAides     = new qfeVisualAides();

  qfeGrid            *grid            = NULL;
  qfePoint            origin;

  unsigned int        windowSize[2];
  float               windowScale;
  
  windowScale = 0.9;

  if(patient->study->pcap.size() > 0)
    patient->study->pcap.front()->qfeGetVolumeGrid(&grid);

  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);

  display->qfeSetDisplayBackgroundColorRGB(colorDefault1.r,colorDefault1.g,colorDefault1.b);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0, 1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
                              windowSize[1] * windowScale);

  visualReformat->qfeSetVisualStudy(patient->study);
  visualQFlow->qfeSetVisualStudy(patient->study);
  visualArrows->qfeSetVisualStudy(patient->study);
  visualAides->qfeSetVisualStudy(patient->study);

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry);
  this->scene->qfeSetSceneLightSource(light);
  this->scene->qfeAddSceneVisual(visualReformat);
  this->scene->qfeAddSceneVisual(visualQFlow);
  this->scene->qfeAddSceneVisual(visualArrows);
  this->scene->qfeAddSceneVisual(visualAides);

  delete display;
  delete geometry;
  delete visualReformat;
  delete visualQFlow;
  delete visualArrows;
  delete visualAides;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateMIP()
{
  if(this->patient == NULL)
  {
    cout << "qfeSceneManager::qfeSceneManagerCreateMIP - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay         *display  = new qfeDisplay();
  qfeFrameProjection *geometry = new qfeFrameProjection();
  qfeVisualMaximum   *visual   = new qfeVisualMaximum();

  qfeGrid            *grid     = NULL;
  qfePoint            origin;

  unsigned int        windowSize[2];
  float               windowScale;

  if(patient->study->pcap.size() > 0)
    patient->study->pcap.front()->qfeGetVolumeGrid(&grid);

  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  windowScale = 0.9;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(1);

  display->qfeSetDisplayBackgroundColorRGB(colorDefault1.r,colorDefault1.g,colorDefault1.b);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0, 1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
                              windowSize[1] * windowScale);

  visual->qfeSetVisualStudy(patient->study);

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry); 
  this->scene->qfeAddSceneVisual(visual);

  delete display;
  delete geometry;
  delete visual;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateFS()
{
  if(this->patient == NULL)
  {
    cout << "qfeSceneManager::qfeSceneManagerCreateFS - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay                     *display          = new qfeDisplay();
  qfeFrameProjection             *geometry         = new qfeFrameProjection();
  qfeLightSource                 *light            = new qfeLightSource();
  qfeVisualAnatomyIllustrative   *visualAnatomy    = new qfeVisualAnatomyIllustrative();
  qfeVisualFluid                 *visualFluid      = new qfeVisualFluid();
  qfeVisualSimParticleTrace      *visualParticles  = new qfeVisualSimParticleTrace();
  qfeVisualSimLinesComparison    *visualLines      = new qfeVisualSimLinesComparison();
  qfeVisualSimBoundaryComparison *visualBoundary   = new qfeVisualSimBoundaryComparison();
  qfeVisualAides                 *visualAides      = new qfeVisualAides();
  qfeVisualMetrics				 *visualMetrics	   = new qfeVisualMetrics();

  qfeGrid            *grid     = NULL;
  qfePoint            origin;

  unsigned int        windowSize[2];
  float               windowScale;

  if(patient->study->pcap.size() > 0)
    patient->study->pcap.front()->qfeGetVolumeGrid(&grid);

  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  windowScale = 0.9;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(1);

 /*
  display->qfeSetDisplayBackgroundColorRGB(colorDefault1.r,colorDefault1.g,colorDefault1.b);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);
  */
  display->qfeSetDisplayBackgroundColorRGB(1.0f,1.0f,1.0f);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundUniform);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0, 1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
                              windowSize[1] * windowScale);


  visualAnatomy->qfeSetVisualStudy(patient->study);
  visualFluid->qfeSetVisualStudy(patient->study);
  visualParticles->qfeSetVisualStudy(patient->study);
  visualLines->qfeSetVisualStudy(patient->study);
  visualBoundary->qfeSetVisualStudy(patient->study);
  visualAides->qfeSetVisualStudy(patient->study);

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry); 
  this->scene->qfeSetSceneLightSource(light);
  this->scene->qfeAddSceneVisual(visualFluid);    
  this->scene->qfeAddSceneVisual(visualLines);
  this->scene->qfeAddSceneVisual(visualBoundary);
  this->scene->qfeAddSceneVisual(visualMetrics);
  this->scene->qfeAddSceneVisual(visualParticles);
  this->scene->qfeAddSceneVisual(visualAnatomy);  
  this->scene->qfeAddSceneVisual(visualAides);

  delete display;
  delete geometry;
  delete visualAnatomy;
  delete visualFluid;
  delete visualParticles;
  delete visualLines;
  delete visualBoundary;
  delete visualAides;
  delete visualMetrics;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateDA()
{
  if(this->patient == NULL)
  {
    cout << "qfeSceneManager::qfeSceneManagerCreateDA - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay				*display			= new qfeDisplay();
  qfeFrameProjection		*geometry			= new qfeFrameProjection();
  qfeVisualSeedVolumes		*visualSeedVolumes	= new qfeVisualSeedVolumes();
  qfeVisualReformatOrtho    *visualOrtho		= new qfeVisualReformatOrtho();
  qfeVisualAnatomyIllustrative *visualAnatomy	= new qfeVisualAnatomyIllustrative();  
  qfeVisualDataAssimilation		*visualDA		= new qfeVisualDataAssimilation();
  qfeVisualInkVis			*visualInk1			= new qfeVisualInkVis();
  qfeVisualInkVisSecondary	*visualInk2			= new qfeVisualInkVisSecondary();
  qfeVisualAides			*visualAides		= new qfeVisualAides();

  qfeGrid            *grid     = NULL;
  qfePoint            origin;

  unsigned int        windowSize[2];
  float               windowScale;

  if(patient->study->pcap.size() > 0)
    patient->study->pcap.front()->qfeGetVolumeGrid(&grid);

  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  windowScale = 0.9;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(1);

  //display->qfeSetDisplayBackgroundColorRGB(0.1f,0.1f,0.1f);
  //display->qfeSetDisplayBackgroundColorRGBSecondary(0.1f,0.1f,0.1f);
  //display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundUniform);

  display->qfeSetDisplayBackgroundColorRGB(colorDefault1.r,colorDefault1.g,colorDefault1.b);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0, 1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
                              windowSize[1] * windowScale);
  
  visualOrtho->qfeSetVisualStudy(patient->study);
  visualSeedVolumes->qfeSetVisualStudy(patient->study);
  visualAnatomy->qfeSetVisualStudy(patient->study);
  visualDA->qfeSetVisualStudy(patient->study);
  visualInk1->qfeSetVisualStudy(patient->study);
  visualInk2->qfeSetVisualStudy(patient->study);
  visualAides->qfeSetVisualStudy(patient->study);

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry);  
  this->scene->qfeAddSceneVisual(visualDA);
  this->scene->qfeAddSceneVisual(visualOrtho);  
  this->scene->qfeAddSceneVisual(visualSeedVolumes);
  this->scene->qfeAddSceneVisual(visualInk1);
  this->scene->qfeAddSceneVisual(visualInk2);
  this->scene->qfeAddSceneVisual(visualAnatomy);
  this->scene->qfeAddSceneVisual(visualAides);

  delete display;
  delete geometry;
  delete visualSeedVolumes;
  delete visualOrtho;
  delete visualDA;
  delete visualInk1;
  delete visualInk2;
  delete visualAnatomy;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateIV()
{
  if(this->patient == NULL)
  {
    cout << "qfeSceneManager::qfeSceneManagerCreateIV - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay				*display			= new qfeDisplay();
  qfeFrameProjection		*geometry			= new qfeFrameProjection();
  qfeVisualSeedVolumes		*visualSeedVolumes	= new qfeVisualSeedVolumes();
  qfeVisualInkVis			*visualInk			= new qfeVisualInkVis();
  qfeVisualReformatOrtho    *visualOrtho		= new qfeVisualReformatOrtho();
  qfeVisualAnatomyIllustrative *visualAnatomy	= new qfeVisualAnatomyIllustrative();
  qfeVisualPlaneOrthoView   *visualPOV			= new qfeVisualPlaneOrthoView();
  qfeVisualMaximum			*visualMIP			= new qfeVisualMaximum();
  qfeVisualAides			*visualAides		= new qfeVisualAides();

  qfeGrid            *grid     = NULL;
  qfePoint            origin;

  unsigned int        windowSize[2];
  float               windowScale;

  if(patient->study->pcap.size() > 0)
    patient->study->pcap.front()->qfeGetVolumeGrid(&grid);

  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  windowScale = 0.9;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(1);

  //display->qfeSetDisplayBackgroundColorRGB(0.1f,0.1f,0.1f);
  //display->qfeSetDisplayBackgroundColorRGBSecondary(0.1f,0.1f,0.1f);
  //display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundUniform);
  display->qfeSetDisplayBackgroundColorRGB(1.0f,1.0f,1.0f);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundUniform);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0, 1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
                              windowSize[1] * windowScale);
  
  visualInk->qfeSetVisualStudy(patient->study);
  visualOrtho->qfeSetVisualStudy(patient->study);
  visualPOV->qfeSetVisualStudy(patient->study);
  visualSeedVolumes->qfeSetVisualStudy(patient->study);
  visualAnatomy->qfeSetVisualStudy(patient->study);
  visualMIP->qfeSetVisualStudy(patient->study);
  visualAides->qfeSetVisualStudy(patient->study);

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry); 
  this->scene->qfeAddSceneVisual(visualInk);
  this->scene->qfeAddSceneVisual(visualOrtho);  
  this->scene->qfeAddSceneVisual(visualPOV);
  this->scene->qfeAddSceneVisual(visualSeedVolumes);
  this->scene->qfeAddSceneVisual(visualAnatomy);
  this->scene->qfeAddSceneVisual(visualMIP);
  this->scene->qfeAddSceneVisual(visualAides);

  delete display;
  delete geometry;
  delete visualSeedVolumes;
  delete visualInk;
  delete visualOrtho;
  delete visualPOV;
  delete visualAnatomy;
  delete visualMIP;

  return qfeSuccess;
}

qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateFilter()
{
  if(!patient) {
    cout << "qfeSceneManager::qfeSceneManagerCreateFilter - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay         *display  = new qfeDisplay();
  qfeFrameProjection *geometry = new qfeFrameProjection();
  qfeVisualFilter    *visual   = new qfeVisualFilter();

  qfeGrid            *grid     = NULL;
  qfePoint            origin;

  unsigned int        windowSize[2];
  float               windowScale;

  if(patient->study->pcap.size() > 0)
    patient->study->pcap.front()->qfeGetVolumeGrid(&grid);

  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  windowScale = 0.9;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(1);

  display->qfeSetDisplayBackgroundColorRGB(colorDefault1.r,colorDefault1.g,colorDefault1.b);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
    windowSize[1] * windowScale);

  visual->qfeSetVisualStudy(patient->study);

  scene->qfeSetSceneDisplay(display); 
  scene->qfeSetSceneGeometry(geometry); 
  scene->qfeAddSceneVisual(visual);

  delete display;
  delete geometry;
  delete visual;

  return qfeSuccess;
}

qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateCEV()
{
  if (!patient) {
    cout << "qfeSceneManager::qfeSceneManagerCreateCEV - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay                  *display      = new qfeDisplay();
  qfeFrameProjection          *geometry     = new qfeFrameProjection();
  qfeLightSource              *light        = new qfeLightSource();
  qfeVisualCEV                *visual       = new qfeVisualCEV();
  qfeVisualFlowLineTrace      *visLine      = new qfeVisualFlowLineTrace();
  qfeVisualFlowParticleTrace  *visParticle  = new qfeVisualFlowParticleTrace();
  qfeVisualAides              *visAides     = new qfeVisualAides();
  qfeVisualReformatOrtho      *visOrtho     = new qfeVisualReformatOrtho();
  qfeVisualSeedEllipsoid      *visSE        = new qfeVisualSeedEllipsoid();
  qfeVisualPlaneOrthoView     *visPOV       = new qfeVisualPlaneOrthoView();
  qfeVisualFeatureSeeding     *visFS        = new qfeVisualFeatureSeeding();
  qfeVisualVolumeOfInterest   *visVOI       = new qfeVisualVolumeOfInterest();
  qfeVisualProbabilityDVR     *visPDVR      = new qfeVisualProbabilityDVR();

  qfeGrid            *grid     = nullptr;
  qfePoint            origin;

  unsigned int        windowSize[2];
  float               windowScale;

  if (patient->study->pcap.size() > 0)
    patient->study->pcap.front()->qfeGetVolumeGrid(&grid);

  if (grid)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  windowScale = 0.9;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(1);

  qfeColorRGB white; white.r = 1.0; white.g = 1.0; white.b = 1.0;
  display->qfeSetDisplayBackgroundColorRGB(white.r, white.g, white.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundUniform);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes(
    1.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, windowSize[1] * windowScale);

  visual->qfeSetVisualStudy(patient->study);
  visLine->qfeSetVisualStudy(patient->study);
  visParticle->qfeSetVisualStudy(patient->study);
  visAides->qfeSetVisualStudy(patient->study);
  visOrtho->qfeSetVisualStudy(patient->study);
  visSE->qfeSetVisualStudy(patient->study);
  visPOV->qfeSetVisualStudy(patient->study);
  visFS->qfeSetVisualStudy(patient->study);
  visVOI->qfeSetVisualStudy(patient->study);
  visPDVR->qfeSetVisualStudy(patient->study);

  scene->qfeSetSceneDisplay(display); 
  scene->qfeSetSceneGeometry(geometry); 
  scene->qfeSetSceneLightSource(light);
  scene->qfeAddSceneVisual(visual);
  scene->qfeAddSceneVisual(visOrtho);
  scene->qfeAddSceneVisual(visPOV);
  scene->qfeAddSceneVisual(visLine);
  scene->qfeAddSceneVisual(visParticle);
  scene->qfeAddSceneVisual(visAides);
  scene->qfeAddSceneVisual(visSE);
  scene->qfeAddSceneVisual(visFS);
  scene->qfeAddSceneVisual(visVOI);
  scene->qfeAddSceneVisual(visPDVR);

  delete display;
  delete geometry;
  delete visual;
  delete visLine;
  delete visParticle;
  delete visAides;
  delete visOrtho;
  delete visSE;
  delete visPOV;
  delete visFS;
  delete visVOI;
  delete visPDVR;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateDVR()
{
  if(this->patient == NULL)
  {
    cout << "qfeSceneManager::qfeSceneManagerCreateDVR - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay         *display  = new qfeDisplay();
  qfeFrameProjection *geometry = new qfeFrameProjection();
  qfeLightSource     *light    = new qfeLightSource();
  qfeVisualRaycast   *visual   = new qfeVisualRaycast();

  qfeGrid            *grid     = NULL;
  qfePoint            origin;

  unsigned int        windowSize[2];
  float               windowScale;

  if(patient->study->pcap.size() > 0)
    patient->study->pcap.front()->qfeGetVolumeGrid(&grid);

  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  windowScale = 0.9;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(1);

  display->qfeSetDisplayBackgroundColorRGB(colorDefault1.r,colorDefault1.g,colorDefault1.b);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0, 1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
                              windowSize[1] * windowScale);

  visual->qfeSetVisualStudy(patient->study);

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry); 
  this->scene->qfeSetSceneLightSource(light);
  this->scene->qfeAddSceneVisual(visual);

  delete display;
  delete geometry;
  delete visual;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateIFR()
{
  if(this->patient == NULL)
  {
    cout << "qfeSceneManager::qfeSceneManagerCreateIFR - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay                   *display          = new qfeDisplay();
  qfeFrameProjection           *geometry         = new qfeFrameProjection();
  qfeLightSource               *light            = new qfeLightSource();
  qfeVisualAnatomyIllustrative *visualAnatomy    = new qfeVisualAnatomyIllustrative();
  qfeVisualFlowProbe2D         *visualProbe      = new qfeVisualFlowProbe2D();
  qfeVisualFlowLines           *visualFlowLines  = new qfeVisualFlowLines(); 
  qfeVisualFlowPlanes          *visualFlowPlanes = new qfeVisualFlowPlanes(); 
  //qfeVisualFlowArrowTrails     *visualFlowTrails = new qfeVisualFlowArrowTrails(); 
  qfeVisualMaximum             *visualMIP        = new qfeVisualMaximum();

  qfeVisualAides               *visualAides      = new qfeVisualAides();

  qfeGrid                      *grid;
  qfePoint                      origin;

  unsigned int                  windowSize[2];
  float                         windowScale;

  windowScale = 0.9;

  patient->study->pcap.front()->qfeGetVolumeGrid(&grid);
  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(2);

  display->qfeSetDisplayBackgroundColorRGB(1.0,1.0,1.0);
  display->qfeSetDisplayBackgroundColorRGBSecondary(0.7843137,0.862745,0.964705);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0, 1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
                              windowSize[1] * windowScale, 1.0);

  visualAnatomy->qfeSetVisualStudy(patient->study);
  visualProbe->qfeSetVisualStudy(patient->study);
  visualFlowLines->qfeSetVisualStudy(patient->study);
  visualFlowPlanes->qfeSetVisualStudy(patient->study);
  //visualFlowTrails->qfeSetVisualStudy(patient->study);
  visualMIP->qfeSetVisualStudy(patient->study);
  visualAides->qfeSetVisualStudy(patient->study);

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry); 
  this->scene->qfeSetSceneLightSource(light);
  this->scene->qfeAddSceneVisual(visualAnatomy);
  this->scene->qfeAddSceneVisual(visualProbe);
  this->scene->qfeAddSceneVisual(visualFlowLines);
  //this->scene->qfeAddSceneVisual(visualFlowTrails);
  this->scene->qfeAddSceneVisual(visualFlowPlanes);
  this->scene->qfeAddSceneVisual(visualMIP);
  this->scene->qfeAddSceneVisual(visualAides);

  delete display;
  delete geometry;
  delete visualAnatomy;
  delete visualFlowLines;
  delete visualFlowPlanes;
  //delete visualFlowTrails;
  delete visualMIP;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateVPR()
{
  if(this->patient == NULL)
  {
    cout << "qfeSceneManager::qfeSceneManagerCreateVPR - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay                     *display          = new qfeDisplay();
  qfeFrameProjection             *geometry         = new qfeFrameProjection();
  qfeLightSource                 *light            = new qfeLightSource();
  qfeVisualFlowProbe3D           *visualProbe      = new qfeVisualFlowProbe3D();  
  qfeVisualReformatOrtho         *visualOrtho      = new qfeVisualReformatOrtho();  
  qfeVisualReformatOblique       *visualMPR        = new qfeVisualReformatOblique();  
  qfeVisualReformatObliqueProbe  *visualMPRP       = new qfeVisualReformatObliqueProbe();  
  qfeVisualFlowParticleTrace     *visualParticles  = new qfeVisualFlowParticleTrace();
  qfeVisualFlowLineTrace         *visualLines      = new qfeVisualFlowLineTrace();
  qfeVisualFlowSurfaceTrace      *visualSurfaces   = new qfeVisualFlowSurfaceTrace();
  qfeVisualRaycast               *visualDVR        = new qfeVisualRaycast();
  qfeVisualMaximum               *visualMIP        = new qfeVisualMaximum();
  qfeVisualAides                 *visualAides      = new qfeVisualAides();

  qfeGrid                      *grid;
  qfePoint                      origin;

  unsigned int                  windowSize[2];
  float                         windowScale;

  windowScale = 0.9;

  patient->study->pcap.front()->qfeGetVolumeGrid(&grid);
  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(2);

  display->qfeSetDisplayBackgroundColorRGB(colorDefault1.r,colorDefault1.g,colorDefault1.b);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0,  1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
    windowSize[1] * windowScale);
  
  visualProbe->qfeSetVisualStudy(patient->study);
  visualOrtho->qfeSetVisualStudy(patient->study);
  visualMPR->qfeSetVisualStudy(patient->study);  
  visualMPRP->qfeSetVisualStudy(patient->study);  
  visualParticles->qfeSetVisualStudy(patient->study);    
  visualLines->qfeSetVisualStudy(patient->study);  
  visualSurfaces->qfeSetVisualStudy(patient->study);
  visualDVR->qfeSetVisualStudy(patient->study);
  visualMIP->qfeSetVisualStudy(patient->study);  
  visualAides->qfeSetVisualStudy(patient->study);  

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry); 
  this->scene->qfeSetSceneLightSource(light);
  this->scene->qfeAddSceneVisual(visualProbe);  
  this->scene->qfeAddSceneVisual(visualOrtho);
  this->scene->qfeAddSceneVisual(visualMPR);  
  this->scene->qfeAddSceneVisual(visualMPRP);  
  this->scene->qfeAddSceneVisual(visualParticles);
  this->scene->qfeAddSceneVisual(visualLines);
  this->scene->qfeAddSceneVisual(visualSurfaces);
  this->scene->qfeAddSceneVisual(visualDVR);  
  this->scene->qfeAddSceneVisual(visualAides);

  delete display;
  delete geometry;
  delete visualProbe;  
  delete visualMPR;
  delete visualMPRP;
  delete visualParticles;
  delete visualLines;
  delete visualSurfaces;
  delete visualDVR;
  //delete visualAnatomy;
  delete visualMIP;
  delete visualAides; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateUSR()
{
  if(this->patient == NULL)
  {
    cout << "qfeSceneManager::qfeSceneManagerCreateUSR - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay                     *display          = new qfeDisplay();
  qfeFrameProjection             *geometry         = new qfeFrameProjection();
  qfeLightSource                 *light            = new qfeLightSource();
  qfeVisualFlowProbe3D           *visualProbe      = new qfeVisualFlowProbe3D();  
  qfeVisualReformatOrtho         *visualOrtho      = new qfeVisualReformatOrtho();  
  qfeVisualReformatOblique       *visualMPR        = new qfeVisualReformatOblique();
  qfeVisualReformatObliqueProbe  *visualMPRP       = new qfeVisualReformatObliqueProbe();  
  qfeVisualReformatFixed         *visualPlane      = new qfeVisualReformatFixed();
  qfeVisualReformatUltrasound    *visualMPRUS      = new qfeVisualReformatUltrasound();
  qfeVisualPlanesArrowHeads      *visualArrows     = new qfeVisualPlanesArrowHeads();
  qfeVisualFlowParticleTrace     *visualParticles  = new qfeVisualFlowParticleTrace();  
  qfeVisualFlowLineTrace         *visualLines      = new qfeVisualFlowLineTrace();
  qfeVisualMaximum               *visualMIP        = new qfeVisualMaximum();
  qfeVisualRaycast               *visualDVR        = new qfeVisualRaycast();  
  qfeVisualRaycastVector         *visualVFR        = new qfeVisualRaycastVector();  
  qfeVisualAides                 *visualAides      = new qfeVisualAides();

  qfeGrid                      *grid;
  qfePoint                      origin;

  unsigned int                  windowSize[2];
  float                         windowScale;

  windowScale = 0.9;

  patient->study->pcap.front()->qfeGetVolumeGrid(&grid);
  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(2);
  display->qfeSetDisplayLayout(qfeDisplayLayoutLeft, true, false);

  display->qfeSetDisplayBackgroundColorRGB(colorDefault1.r,colorDefault1.g,colorDefault1.b);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0, 1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
    windowSize[1] * windowScale);
  
  visualProbe->qfeSetVisualStudy(patient->study);
  visualOrtho->qfeSetVisualStudy(patient->study);  
  visualMPR->qfeSetVisualStudy(patient->study);  
  visualMPRP->qfeSetVisualStudy(patient->study); 
  visualMPRUS->qfeSetVisualStudy(patient->study);
  visualPlane->qfeSetVisualStudy(patient->study);
  visualArrows->qfeSetVisualStudy(patient->study);
  visualLines->qfeSetVisualStudy(patient->study);
  visualParticles->qfeSetVisualStudy(patient->study);    
  visualMIP->qfeSetVisualStudy(patient->study);
  visualDVR->qfeSetVisualStudy(patient->study);
  visualVFR->qfeSetVisualStudy(patient->study);
  visualAides->qfeSetVisualStudy(patient->study);  

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry); 
  this->scene->qfeSetSceneLightSource(light);
  this->scene->qfeAddSceneVisual(visualProbe);  
  this->scene->qfeAddSceneVisual(visualOrtho);  
  this->scene->qfeAddSceneVisual(visualMPR);  
  this->scene->qfeAddSceneVisual(visualMPRP);  
  this->scene->qfeAddSceneVisual(visualMPRUS); 
  this->scene->qfeAddSceneVisual(visualPlane);
  this->scene->qfeAddSceneVisual(visualArrows); 
  this->scene->qfeAddSceneVisual(visualLines);
  this->scene->qfeAddSceneVisual(visualParticles);  
  this->scene->qfeAddSceneVisual(visualDVR);  
  this->scene->qfeAddSceneVisual(visualVFR);  
  this->scene->qfeAddSceneVisual(visualMIP);   
  this->scene->qfeAddSceneVisual(visualAides);

  delete display;
  delete geometry;
  delete visualProbe;  
  delete visualMPR;
  delete visualMPRP;
  delete visualMPRUS;
  delete visualPlane;
  delete visualParticles;
  delete visualDVR;
  delete visualVFR;
  delete visualAides; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreateHCR()
{
  if(this->patient == NULL)
  {
    cout << "qfeSceneManager::qfeSceneManagerCreateHCR - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay                     *display             = new qfeDisplay();
  qfeFrameProjection             *geometry            = new qfeFrameProjection();
  qfeLightSource                 *light               = new qfeLightSource(); 
  qfeVisualClustering            *visualCluster       = new qfeVisualClustering();
  qfeVisualClusterArrowTrace     *visualClusterArrows = new qfeVisualClusterArrowTrace();
  qfeVisualReformatOrtho         *visualMPR           = new qfeVisualReformatOrtho();  
  qfeVisualAnatomyIllustrative   *visualAnatomy       = new qfeVisualAnatomyIllustrative();
  qfeVisualRaycast               *visualDVR           = new qfeVisualRaycast();
  qfeVisualAides                 *visualAides         = new qfeVisualAides();

  qfeGrid                        *grid;
  qfePoint                        origin;

  unsigned int                    windowSize[2];
  float                           windowScale;

  windowScale = 0.9;

  patient->study->pcap.front()->qfeGetVolumeGrid(&grid);
  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(2);

  display->qfeSetDisplayBackgroundColorRGB(colorDefault1.r,colorDefault1.g,colorDefault1.b);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0,  1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
                              windowSize[1] * windowScale);
    
  visualCluster->qfeSetVisualStudy(patient->study);
  visualClusterArrows->qfeSetVisualStudy(patient->study);
  visualMPR->qfeSetVisualStudy(patient->study);  
  visualAnatomy->qfeSetVisualStudy(patient->study);
  visualDVR->qfeSetVisualStudy(patient->study);
  visualAides->qfeSetVisualStudy(patient->study);    

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry); 
  this->scene->qfeSetSceneLightSource(light);
  this->scene->qfeAddSceneVisual(visualCluster); 
  this->scene->qfeAddSceneVisual(visualClusterArrows);
  this->scene->qfeAddSceneVisual(visualMPR);  
  this->scene->qfeAddSceneVisual(visualAnatomy);
  this->scene->qfeAddSceneVisual(visualDVR);
  this->scene->qfeAddSceneVisual(visualAides);

  delete display;
  delete geometry; 
  delete visualCluster;
  delete visualClusterArrows;
  delete visualMPR;
  delete visualAnatomy;
  delete visualDVR;
  delete visualAides; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerCreatePMR()
{
  if(this->patient == NULL)
  {
    cout << "qfeSceneManager::qfeSceneManagerCreatePMR - Load data first" << endl;
    return qfeError;
  }

  // Clean scene object
  scene->qfeRemoveSceneVisuals();

  // Fill the scene graph
  qfeDisplay                     *display             = new qfeDisplay();
  qfeFrameProjection             *geometry            = new qfeFrameProjection();
  qfeLightSource                 *light               = new qfeLightSource(); 
  qfeVisualPatternMatching       *visualPattern       = new qfeVisualPatternMatching(); 
  qfeVisualRaycast               *visualDVR           = new qfeVisualRaycast();
  qfeVisualAnatomyIllustrative   *visualAnatomy       = new qfeVisualAnatomyIllustrative();
  qfeVisualAides                 *visualAides         = new qfeVisualAides();

  qfeGrid                        *grid;
  qfePoint                        origin;

  unsigned int                    windowSize[2];
  float                           windowScale;

  windowScale = 0.9;

  patient->study->pcap.front()->qfeGetVolumeGrid(&grid);
  if(grid != NULL)
    grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  else
    origin.x = origin.y = origin.z = 0.0;

  display->qfeSetDisplayWindow(this->renderWindow);   
  display->qfeGetDisplaySize(windowSize[0], windowSize[1]);
  display->qfeSetDisplaySuperSampling(2);

  display->qfeSetDisplayBackgroundColorRGB(colorDefault1.r,colorDefault1.g,colorDefault1.b);
  display->qfeSetDisplayBackgroundColorRGBSecondary(colorDefault2.r,colorDefault2.g,colorDefault2.b);
  display->qfeSetDisplayBackgroundType(qfeDisplayBackgroundGradient);

  geometry->qfeSetFrameOrigin(origin.x, origin.y, origin.z);
  geometry->qfeSetFrameAxes  (1.0, 0.0, 0.0,
                              0.0, 1.0, 0.0,
                              0.0, 0.0,  1.0);
  geometry->qfeSetFrameExtent(windowSize[0] * windowScale, 
                              windowSize[1] * windowScale);
    
  visualPattern->qfeSetVisualStudy(patient->study);
  visualDVR->qfeSetVisualStudy(patient->study);
  visualAnatomy->qfeSetVisualStudy(patient->study);
  visualAides->qfeSetVisualStudy(patient->study);  

  this->scene->qfeSetSceneDisplay(display); 
  this->scene->qfeSetSceneGeometry(geometry); 
  this->scene->qfeSetSceneLightSource(light);
  this->scene->qfeAddSceneVisual(visualPattern); 
  this->scene->qfeAddSceneVisual(visualDVR);
  this->scene->qfeAddSceneVisual(visualAnatomy);
  this->scene->qfeAddSceneVisual(visualAides);

  delete display;
  delete geometry; 
  delete visualPattern;
  delete visualDVR;
  delete visualAnatomy;
  delete visualAides; 

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeSceneManager::qfeSceneManagerSelectDriver(qfeScene *scene, qfeDriver **driver)
{
  // \todo use input variable instead of global
  vector< qfeVisual*> visuals;
  vector< int >       visualTypes;
  
  if(scene == NULL) return qfeError;

  scene->qfeGetSceneVisuals(visuals);

  // Get a list of the visual types
  vector< qfeVisual* >::iterator it;
  for (it = visuals.begin(); it!=visuals.end(); ++it) {
    int type;
    (*it)->qfeGetVisualType(type);
    visualTypes.push_back(type);
  }  
  
  // Check for each driver if it can handle the requested visuals
    
  /* qfeDriverSV
  */
  if(
	  count(visualTypes.begin(), visualTypes.end(), visualInkVis) > 0 &&
	  count(visualTypes.begin(), visualTypes.end(), visualReformatOrtho) > 0 &&
	  count(visualTypes.begin(), visualTypes.end(), visualPlaneOrthoViewType) > 0 &&
	  count(visualTypes.begin(), visualTypes.end(), visualMaximum) > 0 &&
	  count(visualTypes.begin(), visualTypes.end(), visualSeedVolumesType) > 0 &&
	  count(visualTypes.begin(), visualTypes.end(), visualAnatomyIllustrative) > 0
	)
  {
	this->qfeSetSceneManagerViewGeometry(VA);

    this->driver = new qfeDriverIV(scene);

    return qfeSuccess;
  }

    /* qfeDriverDA: requires 
	- visualReformat
	- visualSeedVolumes
	- visualAnatomyIllustrative
	- visualDataAssimilation
	- 2 visualInkVisMinimal
	- visualFeatureSeedingType
  */
  if(
	  count(visualTypes.begin(), visualTypes.end(), visualReformatOrtho) > 0 &&
	  count(visualTypes.begin(), visualTypes.end(), visualSeedVolumesType) > 0 &&
	  count(visualTypes.begin(), visualTypes.end(), visualAnatomyIllustrative) > 0 &&
	  count(visualTypes.begin(), visualTypes.end(), visualDataAssimilation) > 0 &&
	  count(visualTypes.begin(), visualTypes.end(), visualInkVis) > 0 &&
	  count(visualTypes.begin(), visualTypes.end(), visualInkVisSecondary) > 0
	)
  {
	this->qfeSetSceneManagerViewGeometry(VA);

    this->driver = new qfeDriverDA(scene);

    return qfeSuccess;
  }

  // qfeDriverHCR: requires a visual for the hierarchical clustering
  if(count(visualTypes.begin(), visualTypes.end(), visualFlowClustering) > 0)
  {
    this->qfeSetSceneManagerViewGeometry(VA);

    this->driver = new qfeDriverHCR(scene);

    return qfeSuccess;
  }

  // qfeDriverPMR: requires a visual for the hierarchical clustering
  if(count(visualTypes.begin(), visualTypes.end(), visualFlowPatternMatching) > 0)
  {
    this->qfeSetSceneManagerViewGeometry(VA);

    this->driver = new qfeDriverPMR(scene);

    return qfeSuccess;
  }

  
  // qfeDriverUSR: requires a visual for the 3d probe and and US plane
  if(count(visualTypes.begin(), visualTypes.end(), visualFlowProbe3D) > 0 &&
     count(visualTypes.begin(), visualTypes.end(), visualReformatUltrasound) > 0)
  {
    this->qfeSetSceneManagerViewGeometry(VA);

    this->driver = new qfeDriverUSR(scene);

    return qfeSuccess;
  }
  
  // qfeDriverVPR: requires a visual for the 3d probe
  if(count(visualTypes.begin(), visualTypes.end(), visualFlowProbe3D) > 0)
  {
    this->qfeSetSceneManagerViewGeometry(LR);

    this->driver = new qfeDriverVPR(scene);

    return qfeSuccess;
  }

  // qfeDriverIFR: requires a visual for anatomy and flow primitives
  if(count(visualTypes.begin(), visualTypes.end(), visualAnatomyIllustrative) > 0 &&
       (count(visualTypes.begin(), visualTypes.end(), visualFlowLines)  > 0 ||
        count(visualTypes.begin(), visualTypes.end(), visualFlowPlanes) > 0 ||
        count(visualTypes.begin(), visualTypes.end(), visualFlowTrails) > 0))
  {
     this->qfeSetSceneManagerViewGeometry(VA);

     this->driver = new qfeDriverIFR(scene);

     return qfeSuccess;
  }

  // qfeDriverDVR: requires a visualRaycast
  if(count(visualTypes.begin(), visualTypes.end(), visualRaycast) > 0)
  {
     this->qfeSetSceneManagerViewGeometry(VA);

     this->driver = new qfeDriverDVR(scene);

     return qfeSuccess;
  }
  
  // qfeDriverMIP: requires a visualMaximum
  if(count(visualTypes.begin(), visualTypes.end(), visualMaximum) > 0)
  {
    this->qfeSetSceneManagerViewGeometry(VA);

    this->driver = new qfeDriverMIP(scene);

    return qfeSuccess;
  }

  // qfeDriverFS: requires a visualFluid
  if(count(visualTypes.begin(), visualTypes.end(), visualFluid) > 0)
  {
    this->qfeSetSceneManagerViewGeometry(VA);

    this->driver = new qfeDriverFS(scene);

    return qfeSuccess;
  }

  // qfeDriverFilter
  if (count(visualTypes.begin(), visualTypes.end(), visualFilterType) > 0) {
    qfeSetSceneManagerViewGeometry(VA);
    this->driver = new qfeDriverFilter(scene);
    return qfeSuccess;
  }

  // qfeDriverCEV
  if (count(visualTypes.begin(), visualTypes.end(), visualCEVType) > 0 &&
    count(visualTypes.begin(), visualTypes.end(), visualFlowLines) > 0 &&
    count(visualTypes.begin(), visualTypes.end(), visualFlowParticles) > 0 &&
    count(visualTypes.begin(), visualTypes.end(), visualReformatOrtho) > 0 &&
    count(visualTypes.begin(), visualTypes.end(), visualSeedEllipsoidType) > 0 &&
    count(visualTypes.begin(), visualTypes.end(), visualPlaneOrthoViewType) > 0 &&
    count(visualTypes.begin(), visualTypes.end(), visualFeatureSeedingType) > 0 &&
    count(visualTypes.begin(), visualTypes.end(), visualVolumeOfInterest) > 0 &&
    count(visualTypes.begin(), visualTypes.end(), visualProbabilityDVR) > 0) {
      qfeSetSceneManagerViewGeometry(VA);
      this->driver = new qfeDriverCEV(scene);
      return qfeSuccess;
  }

  // qfeDriverMPR: requires a visualReformat
  if(count(visualTypes.begin(), visualTypes.end(), visualReformat)         > 0 ||
     count(visualTypes.begin(), visualTypes.end(), visualReformatOrtho)      > 0 ||
     count(visualTypes.begin(), visualTypes.end(), visualReformatFixed)      > 0 ||
     count(visualTypes.begin(), visualTypes.end(), visualPlanesArrowHeads) > 0)
  {
      this->qfeSetSceneManagerViewGeometry(VA);

      this->driver = new qfeDriverMPR(scene);

      return qfeSuccess;
  }

  // qfeDriverDefault: requires a visualDefault
  if(count(visualTypes.begin(), visualTypes.end(), visualDefault) > 0)
  {
        this->driver = new qfeDriverDefault(scene);

        return qfeSuccess;
  }

  cout << "qfeSceneManager: no driver can be selected for the set of visuals" << endl;

  this->driver = new qfeDriverDefault(scene);
  return qfeError;  
}




