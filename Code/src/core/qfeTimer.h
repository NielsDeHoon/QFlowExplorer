#pragma once

#if defined(_MSC_VER) || defined(_BORLANDC_)
#define u64 __int64
#define WINDOWS
#elif defined(__LINUX__) || defined(__MACOSX__)
#define u64 unsigned long long
#define LINUX
#elif defined(__MINGW32__)
#define u64 unsigned long long
#define WINDOWS
#endif

class qfeTimer {
public:
	qfeTimer();
	qfeTimer(unsigned int type);
	qfeTimer(qfeTimer& oldTimer);

	~qfeTimer() { }

	void SetTimerType(unsigned int newType);
	unsigned int GetTimerType();

	void SetUpdateInterval(float newInterval);
	float GetUpdateInterval();

	void Reset();

	float GetFPS();
	float GetTime();
private:
	void InitTimer();

	float timeAtStart, lastUpdate, updateInterval, fps;
	u64  ticksPerSecond;
	unsigned int timerType, frames;
};

enum Timers {
	Z_TIMER_SDL,	//Uses SDL_GetTicks() for time
	Z_TIMER_QPC,	//Uses QueryPerformanceCounter() for time (Windows Only)
};

