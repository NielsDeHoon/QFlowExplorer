#include "qfeTimer.h"
#ifdef LINUX
#include <sys/time.h>
#endif
#ifdef WINDOWS
#include <windows.h>
#endif

qfeTimer::qfeTimer() : timerType(Z_TIMER_SDL) {
	//We Default To SDL Timers, Then Reset Timers
	Reset();
}

qfeTimer::qfeTimer(unsigned int type) {
#if defined (LINUX)
	timerType = Z_TIMER_SDL;
#elif defined (WINDOWS)
	timerType = type;
#endif
	Reset();
}

qfeTimer::qfeTimer(qfeTimer& oldTimer) {
	//Copy Old Data Into This Class
	timerType   = oldTimer.timerType;
	timeAtStart  = oldTimer.timeAtStart;
	ticksPerSecond = oldTimer.ticksPerSecond;

	//Do Timer Initialization
	InitTimer();
	timeAtStart = GetTime();
}

void qfeTimer::SetTimerType(unsigned int newType) {
#if defined (WINDOWS)
	if(newType != Z_TIMER_SDL || newType != Z_TIMER_QPC)
 newType = Z_TIMER_SDL;
	timerType = newType;
#elif defined (LINUX)
	timerType = Z_TIMER_SDL;
#endif

	//Initialize The Timer
	InitTimer();
}

unsigned int qfeTimer::GetTimerType() {
	return (timerType);
}

void qfeTimer::SetUpdateInterval(float newInterval) {
	updateInterval = newInterval;
}

float qfeTimer::GetUpdateInterval() {
	return updateInterval;
}

float qfeTimer::GetFPS() {
	frames++;
	float currentUpdate = GetTime();

	if(currentUpdate - lastUpdate > updateInterval) {
 fps = frames / (currentUpdate - lastUpdate);
 lastUpdate = currentUpdate;
 frames = 0;
	}

	return (fps);
}

void qfeTimer::Reset() {
	timeAtStart  = 0;
	ticksPerSecond = 0;
	frames     = 0;
	lastUpdate   = 0;
	fps      = 0;
	updateInterval = 0.5;

	InitTimer();
	timeAtStart = GetTime();
}

void qfeTimer::InitTimer() {
#if defined (WINDOWS)
	if(timerType == Z_TIMER_QPC) {
 //We Only Need To Do Initialization For the QPC Timer
 //We Need To Know How Often The Clock Is Updated
 if( !QueryPerformanceFrequency((LARGE_INTEGER*)&ticksPerSecond))
 	ticksPerSecond = 1000;
	}
#endif
}

#if defined (LINUX)
float qfeTimer::GetTime() {
	if(timerType == Z_TIMER_SDL) {
 return ((float)gettimeofday()/1000.0f);
	}
#if defined (WINDOWS)
	else if(timerType == Z_TIMER_QPC) {
 u64  ticks;
 float time;

 //This is the number of clock ticks since the start
 if( !QueryPerformanceCounter((LARGE_INTEGER*)&ticks))
 	ticks = (u64)timeGetTime();

 //Divide by frequency to get time in seconds
 time = (float)(u64)ticks / (float)(u64)ticksPerSecond;

 //Calculate Actual Time
 time -= timeAtStart;

 return (time);
	}
#endif
	return (0.0f);
}
