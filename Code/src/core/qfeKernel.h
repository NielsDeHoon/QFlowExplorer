/**
* \file   qfeKernel.h
* \author Roy van Pelt
* \class  qfeKernel
* \brief  Implements a kernel layer, to interface between QT and VTK classes
*
* \todo Implement screenshot functionality
* \todo Brightness and Contrast interactor
* \todo Implement slice views (with Axis direction on pick position)
* \todo Implement timer class
*
* \bug Decent error if shaders are not found
*/
#pragma once

#include "qflowexplorer.h"

#include <iostream>

#include <QVector.h>
#include <QColor.h>

#include "vtkCallbackProgressBar.h"
#include "qfeImageData.h"

#include "qfeConvert.h"
#include "qfeImageDataManager.h"
#include "qfeSceneManager.h"

#include "qfeDisplay.h"
#include "qfeDriver.h"
#include "qfeFrame.h"
#include "qfeScene.h"
#include "qfeVisual.h"

class qfeKernel
{
public:  
  // Singleton handle and destructor
  static qfeKernel* qfeGetInstance();
  static void       qfeDeleteInstance();

  qfeImageDataManager *DataManager;
  qfeSceneManager     *SceneManager;

  // Load the data initialize the scene manager
  qfeReturnStatus qfeKernelLoadData(char ** files, int count);  

  // Load additional fluid simulation data
  qfeReturnStatus qfeKernel::qfeKernelLoadSim(char **files, int count);

  // Release the data after stopping the scene manager
  qfeReturnStatus qfeKernelReleaseData();

  // Start rendering of the scene
  qfeReturnStatus qfeKernelRenderScene(int scenePreset);

  // Convert and update the color map
  qfeReturnStatus qfeKernelUpdateColorMap(int visualType, QVector< QPair<double, double*> > color, QVector< QPair<double, double>  > opacity, QVector< QPair<double, double>  > gradient, int interpolation, int quantization, int space, bool constlight, bool paduni);

  // Update the light 
  qfeReturnStatus qfeKernelUpdateLight(qfeVector lightDirection, qfeFloat ambientContrib, qfeFloat diffuseContrib, qfeFloat specularContrib, qfeFloat specularPower);

  // Update the display 
  qfeReturnStatus qfeKernelUpdateDisplay(int type, QColor color1, QColor color2);

  qfeReturnStatus qfeKernelUpdateFeatureTF(int id, QVector<QPair<double, double>> &tf, int dataType);

  // Connect GUI to the progress bar callback
  qfeReturnStatus qfeConnectProgressBar(qfeCallBackProgressBar *callback);  

  // Screen shot
  void CreateScreenShot();
  
  //Plot
  void RenderPlot(float phase);

  // Calculate scalar field for flow speed
  // Derived from vector field PCA-P
  void qfeCalculateSpeedVolumes();

protected:

  vtkCallbackProgressBar *progressCallback;

private:
  static qfeKernel *singleton;
  static bool       singletonFlag;

  qfeKernel();                                 // Private constructor
  qfeKernel(const qfeKernel&);                 // Prevent copy-construction
  qfeKernel& operator=(const qfeKernel&);      // Prevent assignment
  ~qfeKernel();                                // Private destructor
};
