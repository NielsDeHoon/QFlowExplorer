#include "vtkCallbackTimerOneShot.h"

//----------------------------------------------------------------------------
vtkCallbackTimerOneShot* vtkCallbackTimerOneShot::New()
{
  vtkCallbackTimerOneShot *cb = new vtkCallbackTimerOneShot;    
  cb->OneShotTimerId = 0;
  cb->flag           = false;
  cb->on             = false;
  return cb;
}

//----------------------------------------------------------------------------
void vtkCallbackTimerOneShot::Execute(vtkObject *caller, unsigned long eventId, void *callData)
{
 if (vtkCommand::TimerEvent == eventId)
 {
   int tid = * static_cast<int *>(callData);

   if (tid == this->OneShotTimerId)
   {     
     this->flag = true;
   }
 }
}

//----------------------------------------------------------------------------
void vtkCallbackTimerOneShot::SetOneShotTimerId(int tid)
{
  this->OneShotTimerId = tid;
  this->flag           = false;
}  

//----------------------------------------------------------------------------
void vtkCallbackTimerOneShot::SetOneShotTimerOn()
{
  this->on = true;
}

//----------------------------------------------------------------------------
void vtkCallbackTimerOneShot::SetOneShotTimerOff()
{
  this->on   = false;
}

//----------------------------------------------------------------------------
bool vtkCallbackTimerOneShot::IsOn()
{
  return this->on;
}  

//----------------------------------------------------------------------------
bool vtkCallbackTimerOneShot::IsShot()
{
  return this->flag;
}  

