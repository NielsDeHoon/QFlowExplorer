#include "qfeConvert.h"

//----------------------------------------------------------------------------
qfeReturnStatus qfeConvert::vtkImageDataToQfeVolume(qfeImageData *vtkData, qfeVolume *volume)
{
  // Volume data properties
  qfeValueType valueType;
  double       valueDomain[2];
  int          numberVoxels[3]; 
  int          numberComponents;
  int          dimensions;
  float        triggerTime;
  float        phaseDuration;
  double       venc[3] = {0.0, 0.0, 0.0};
  void        *voxels;

  // Grid properties
  qfeGrid           *grid;
  double             origin[3];
  double             axis[3][3];
  double             extent[3];
  qfeDataOrientation orientation;

  // Check if data is valid
  if(vtkData == NULL) return qfeError;
  
  // Obtain volume properties
  vtkData->GetDimensions(&numberVoxels[0]);

  numberComponents = vtkData->GetPointData()->GetNumberOfComponents();
  triggerTime      = vtkData->GetPhaseTriggerTime();
  phaseDuration    = vtkData->GetPhaseDuration();
  dimensions       = numberVoxels[0]*numberVoxels[1]*numberVoxels[2];  

  switch(numberComponents)
  {
  case 1 : voxels = vtkData->GetPointData()->GetScalars()->GetVoidPointer(0);
           vtkData->GetScalarRange(&valueDomain[0]);
           break;
  case 3:
  case 4:  voxels = vtkData->GetPointData()->GetVectors()->GetVoidPointer(0);
           vtkData->GetPointData()->GetVectors()->GetRange(&valueDomain[0]);     
           break;
  case 9:  voxels = vtkData->GetPointData()->GetTensors()->GetVoidPointer(0);
           vtkData->GetPointData()->GetTensors()->GetRange(&valueDomain[0]);  
           break;
  default: return qfeError;//vtkData->GetPointData()->GetArray();
  } 
 
  // Obtain grid properties
  vtkData->GetOrigin(origin);
  vtkData->GetAxisX(axis[0]);
  vtkData->GetAxisY(axis[1]);
  vtkData->GetAxisZ(axis[2]);
  vtkData->GetSpacing(extent);
  vtkData->GetVenc(venc);
  orientation = vtkData->GetScanOrientation();
  
  // Assign volume properties
  switch(vtkData->GetScalarType())
  {
    case VTK_CHAR           : valueType = qfeValueTypeInt8; 
                              break;
    case VTK_SIGNED_CHAR    : valueType = qfeValueTypeInt8;
                              break;
    case VTK_UNSIGNED_CHAR  : valueType = qfeValueTypeUnsignedInt8;  
                              break;
    case VTK_SHORT          : valueType = qfeValueTypeInt16;         
                              break;
    case VTK_UNSIGNED_SHORT : valueType = qfeValueTypeUnsignedInt16; 
                              break;
    case VTK_FLOAT          : valueType = qfeValueTypeFloat;         
                              break;
    default                 : cout << "qfeConvert::vtkImageData2qfeVolume unknown type" << endl;
                              return qfeError;
  }

  volume->qfeSetVolumeData(valueType, numberVoxels[0], numberVoxels[1], numberVoxels[2], numberComponents, voxels); 
  volume->qfeSetVolumeValueDomain(valueDomain[0], valueDomain[1]);
  volume->qfeSetVolumeComponentsPerVoxel(numberComponents);
  volume->qfeSetVolumeTriggerTime(triggerTime);
  volume->qfeSetVolumePhaseDuration(phaseDuration);
  volume->qfeSetVolumeLabel(vtkData->GetPointData()->GetArrayName(0));
  volume->qfeSetVolumeVenc(venc[0], venc[1], venc[2]);

  // Assign grid properties
  grid = new qfeGrid();
  grid->qfeSetGridOrigin(origin[0], origin[1], origin[2]);
  grid->qfeSetGridAxes(axis[0][0], axis[0][1], axis[0][2],
                       axis[1][0], axis[1][1], axis[1][2],
                       axis[2][0], axis[2][1], axis[2][2]);
  grid->qfeSetGridExtent(extent[0], extent[1], extent[2]);
  grid->qfeSetGridOrientation((qfeGridOrientation)orientation);

  volume->qfeUploadVolume();


  // Connect grid to the volume
  volume->qfeSetVolumeGrid(grid);

  
  delete grid;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeConvert::qfeVolumeToVtkImageData(qfeVolume *volume, qfeImageData *vtkData)
{
  // Volume data properties
  qfeValueType valueType;
  int          type;
  qfeFloat     valueDomain[2];
  unsigned int numberVoxels[3]; 
  unsigned int numberComponents;
  int          dimensions;
  float        triggerTime;
  float        phaseDuration;
  void        *voxels;
  std::string voxelLabel;

  // Grid properties
  qfeGrid *grid;
  qfeFloat origin[3];
  qfeFloat axis[3][3];
  qfeFloat extent[3];

  // Check if data is valid
  if(volume == NULL) return qfeError;

  volume->qfeGetVolumeGrid(&grid); 

  if(grid == NULL) return qfeError;
  
  // Obtain volume properties
  volume->qfeGetVolumeData(valueType, numberVoxels[0], numberVoxels[1], numberVoxels[2], &voxels);
  volume->qfeGetVolumeComponentsPerVoxel(numberComponents);
  volume->qfeGetVolumeValueDomain(valueDomain[0], valueDomain[1]);
  volume->qfeGetVolumeTriggerTime(triggerTime);
  volume->qfeGetVolumePhaseDuration(phaseDuration);  

  grid->qfeGetGridOrigin(origin[0], origin[1], origin[2]);
  grid->qfeGetGridAxes(axis[0][0], axis[0][1], axis[0][2],
                       axis[1][0], axis[1][1], axis[1][2],
                       axis[2][0], axis[2][1], axis[2][2]);  
  grid->qfeGetGridExtent(extent[0], extent[1], extent[2]);

  dimensions       = numberVoxels[0]*numberVoxels[1]*numberVoxels[2];  

  // Assign the voxel data
  vtkSmartPointer<vtkDataArray> data = NULL;

  vtkSmartPointer<vtkImageImport> importer = vtkSmartPointer<vtkImageImport>::New();
  importer->SetWholeExtent(1,numberVoxels[0],1,numberVoxels[1],1,numberVoxels[2]);
  importer->SetDataExtentToWholeExtent();
  importer->SetNumberOfScalarComponents(numberComponents);

  switch(valueType)
  {
    case qfeValueTypeInt8          : type = VTK_CHAR; 
                                     data = vtkSmartPointer<vtkCharArray>::New();                                     
                                     importer->SetDataScalarTypeToUnsignedChar();
                                     break;
    case qfeValueTypeUnsignedInt8  : type = VTK_UNSIGNED_CHAR;  
                                     data = vtkSmartPointer<vtkUnsignedCharArray>::New();
                                     importer->SetDataScalarTypeToUnsignedChar();  
                                     break;
    case qfeValueTypeInt16         : type = VTK_SHORT;         
                                     data = vtkSmartPointer<vtkShortArray>::New();
                                     importer->SetDataScalarTypeToUnsignedShort();  
                                     break;
    case qfeValueTypeUnsignedInt16 : type = VTK_UNSIGNED_SHORT; 
                                     data = vtkSmartPointer<vtkUnsignedShortArray>::New();
                                     importer->SetDataScalarTypeToUnsignedShort();  
                                     break;
    case qfeValueTypeFloat         : type = VTK_FLOAT; 
                                     data = vtkSmartPointer<vtkFloatArray>::New();
                                     importer->SetDataScalarTypeToFloat();  
                                     break;
    default                        : cout << "qfeConvert::qfeVolumeToVtkImageData unknown type" << endl;
                                     return qfeError;
  }

  volume->qfeGetVolumeLabel(voxelLabel);
  importer->SetImportVoidPointer(voxels);
  importer->SetScalarArrayName(voxelLabel.c_str());
  importer->Update();

  //data->SetNumberOfComponents(numberComponents);
  //data->SetVoidArray(voxels, dimensions, 1);   

  // Assign volume properties
  vtkData->DeepCopy(importer->GetOutput());
  vtkData->SetDimensions(numberVoxels[0], numberVoxels[1], numberVoxels[2]);
  //vtkData->SetOrigin((double)origin[0]+0.5*numberVoxels[0],(double)origin[1]+0.5*numberVoxels[1],(double)origin[2]+0.5*numberVoxels[2]);
  vtkData->SetOrigin((double)origin[0],(double)origin[1],(double)origin[2]);
  vtkData->SetAxisX((double)axis[0][0],(double)axis[0][1],(double)axis[0][2]);
  vtkData->SetAxisY((double)axis[1][0],(double)axis[1][1],(double)axis[1][2]);
  vtkData->SetAxisZ((double)axis[2][0],(double)axis[2][1],(double)axis[2][2]);
  vtkData->SetSpacing((double)extent[0],(double)extent[1],(double)extent[2]);  
  //vtkData->SetNumberOfScalarComponents(numberComponents);
  // Trigger time / phase duration? 
  vtkData->SetPhaseDuration(phaseDuration);

  switch(numberComponents)
  {
  case 1 : //vtkData->GetPointData()->SetScalars(data);
           vtkData->GetPointData()->SetActiveScalars(voxelLabel.c_str());
           break;
  case 3:
  case 4:  //vtkData->GetPointData()->SetVectors(data);           
           vtkData->GetPointData()->SetActiveVectors(voxelLabel.c_str());
           break;
  case 9:  //vtkData->GetPointData()->SetTensors(data);
          vtkData->GetPointData()->SetActiveTensors(voxelLabel.c_str());
           break;
  default: break;//vtkData->GetPointData()->AddArray(data);
  }  
  vtkData->GetPointData()->Update();  

  double range[2];
  vtkData->GetPointData()->GetArray(voxelLabel.c_str())->GetRange(range);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeConvert::qtTransferFunctionToqfeColorMap
                                        (QVector< QPair<double, double*> > color, 
                                         QVector< QPair<double, double>  > opacity,
                                         QVector< QPair<double, double>  > gradient,
                                         int interpolation, int quantization, int space, 
                                         bool constlight, bool paduni,
                                         qfeColorMap &colorMap)
{
  vector<qfeRGBMapping>     rgb;
  vector<qfeOpacityMapping> a;
  vector<qfeOpacityMapping> g;

  unsigned int nrgb = color.size();
  unsigned int na   = opacity.size();
  unsigned int ng   = gradient.size();

  QVector< QPair<double, double*> >::iterator cIter = color.begin();
  for ( int i = 0; i < color.size(); i++ )
  {
    qfeRGBMapping current;
    current.value    = cIter->first;
    current.color.r  = cIter->second[0];
    current.color.g  = cIter->second[1];
    current.color.b  = cIter->second[2];

    rgb.push_back(current);
    cIter++;
  }

  QVector< QPair<double, double> >::iterator oIter = opacity.begin();
  for ( int i = 0; i < opacity.size(); i++ )
  {
    qfeOpacityMapping current;
    current.value   = oIter->first;
    current.opacity = oIter->second;

    a.push_back(current);
    oIter++;
  }

  QVector< QPair<double, double> >::iterator gIter = gradient.begin();
  for ( int i = 0; i < gradient.size(); i++ )
  {
    qfeOpacityMapping current;
    current.value   = gIter->first;
    current.opacity = gIter->second;

    g.push_back(current);
    gIter++;
  }

  colorMap.qfeSetColorMapInterpolation(interpolation);
  colorMap.qfeSetColorMapQuantization(quantization);
  colorMap.qfeSetColorMapSpace(space);
  colorMap.qfeSetColorMapConstantLightness(constlight);
  colorMap.qfeSetColorMapPaddingUniform(paduni);
  colorMap.qfeSetColorMapRGB(nrgb, rgb);
  colorMap.qfeSetColorMapA(na, a);
  colorMap.qfeSetColorMapG(ng, g);

  return qfeSuccess;
}

qfeVolume qfeConvert::qfeArray3fToQfeVolume(Array3f &array, float rangeMin, float rangeMax, const std::string &label, qfeGrid *grid)
{
  qfeVolume volume;
  volume.qfeSetVolumeData(qfeValueTypeFloat, array.ni, array.nj, array.nk, 1, &(array.a[0]));
  volume.qfeSetVolumeGrid(grid);
  volume.qfeSetVolumeValueDomain(rangeMin, rangeMax);
  volume.qfeSetVolumeLabel(label);
  return volume;
}

qfeVolume qfeConvert::qfeFloattoQfeVolume(float *array, unsigned width, unsigned height, unsigned depth, unsigned nrComp, float rangeMin, float rangeMax, const std::string &label, qfeGrid *grid)
{
  qfeVolume volume;
  volume.qfeSetVolumeData(qfeValueTypeFloat, width, height, depth, 1, array);
  volume.qfeSetVolumeComponentsPerVoxel(nrComp);
  volume.qfeSetVolumeGrid(grid);
  volume.qfeSetVolumeValueDomain(rangeMin, rangeMax);
  volume.qfeSetVolumeLabel(label);
  return volume;
}
