#include "qfeMatrix4f.h"

//----------------------------------------------------------------------------
qfeMatrix4f::qfeMatrix4f()
{
  qfeSetMatrixIdentity(*this);
}

//----------------------------------------------------------------------------
qfeMatrix4f::qfeMatrix4f(qfeFloat *m)
{
  qfeSetMatrixElements(*this, m);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeSetMatrixElements(qfeMatrix4f &m, qfeFloat e[16])
{
  for(int i=0; i<16; i++) m[i] = e[i];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeGetMatrixElements(qfeMatrix4f m, qfeFloat e[16])
{
  for(int i=0; i<16; i++) e[i] = m[i]; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeSetMatrixIdentity(qfeMatrix4f &m)
{
  for(int i=0; i<16; i++) m[i] = 0.0;
  m[0] = m[5] = m[10] = m[15] = 1.0;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeGetMatrixDeterminant(qfeMatrix4f m, qfeFloat &d)
{
  d = 
    m(0,3) * m(1,2) * m(2,1) * m(3,0) - m(0,2) * m(1,3) * m(2,1) * m(3,0) - 
    m(0,3) * m(1,1) * m(2,2) * m(3,0) + m(0,1) * m(1,3) * m(2,2) * m(3,0) + 
    m(0,2) * m(1,1) * m(2,3) * m(3,0) - m(0,1) * m(1,2) * m(2,3) * m(3,0) - 
    m(0,3) * m(1,2) * m(2,0) * m(3,1) + m(0,2) * m(1,3) * m(2,0) * m(3,1) + 
    m(0,3) * m(1,0) * m(2,2) * m(3,1) - m(0,0) * m(1,3) * m(2,2) * m(3,1) - 
    m(0,2) * m(1,0) * m(2,3) * m(3,1) + m(0,0) * m(1,2) * m(2,3) * m(3,1) + 
    m(0,3) * m(1,1) * m(2,0) * m(3,2) - m(0,1) * m(1,3) * m(2,0) * m(3,2) - 
    m(0,3) * m(1,0) * m(2,1) * m(3,2) + m(0,0) * m(1,3) * m(2,1) * m(3,2) + 
    m(0,1) * m(1,0) * m(2,3) * m(3,2) - m(0,0) * m(1,1) * m(2,3) * m(3,2) - 
    m(0,2) * m(1,1) * m(2,0) * m(3,3) + m(0,1) * m(1,2) * m(2,0) * m(3,3) + 
    m(0,2) * m(1,0) * m(2,1) * m(3,3) - m(0,0) * m(1,2) * m(2,1) * m(3,3) - 
    m(0,1) * m(1,0) * m(2,2) * m(3,3) + m(0,0) * m(1,1) * m(2,2) * m(3,3);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeGetMatrixInverse(qfeMatrix4f m, qfeMatrix4f &i)
{
  qfeFloat mat[16];
  float x;
  qfeGetMatrixDeterminant(m, x);

  /*
  if (x < 0.0) 
  {
    qfeSetMatrixIdentity(i);
    return qfeError;
  }
  */

  mat[0]  = (-m(1,3) * m(2,2) * m(3,1) + m(1,2) * m(2,3) * m(3,1) + m(1,3) * m(2,1) * m(3,2) - m(1,1) * m(2,3) * m(3,2) - m(1,2) * m(2,1) * m(3,3) + m(1,1) * m(2,2) * m(3,3))/x;
  mat[1]  = ( m(0,3) * m(2,2) * m(3,1) - m(0,2) * m(2,3) * m(3,1) - m(0,3) * m(2,1) * m(3,2) + m(0,1) * m(2,3) * m(3,2) + m(0,2) * m(2,1) * m(3,3) - m(0,1) * m(2,2) * m(3,3))/x; 
  mat[2]  = (-m(0,3) * m(1,2) * m(3,1) + m(0,2) * m(1,3) * m(3,1) + m(0,3) * m(1,1) * m(3,2) - m(0,1) * m(1,3) * m(3,2) - m(0,2) * m(1,1) * m(3,3) + m(0,1) * m(1,2) * m(3,3))/x; 
  mat[3]  = ( m(0,3) * m(1,2) * m(2,1) - m(0,2) * m(1,3) * m(2,1) - m(0,3) * m(1,1) * m(2,2) + m(0,1) * m(1,3) * m(2,2) + m(0,2) * m(1,1) * m(2,3) - m(0,1) * m(1,2) * m(2,3))/x; 
  mat[4]  = ( m(1,3) * m(2,2) * m(3,0) - m(1,2) * m(2,3) * m(3,0) - m(1,3) * m(2,0) * m(3,2) + m(1,0) * m(2,3) * m(3,2) + m(1,2) * m(2,0) * m(3,3) - m(1,0) * m(2,2) * m(3,3))/x; 
  mat[5]  = (-m(0,3) * m(2,2) * m(3,0) + m(0,2) * m(2,3) * m(3,0) + m(0,3) * m(2,0) * m(3,2) - m(0,0) * m(2,3) * m(3,2) - m(0,2) * m(2,0) * m(3,3) + m(0,0) * m(2,2) * m(3,3))/x; 
  mat[6]  = ( m(0,3) * m(1,2) * m(3,0) - m(0,2) * m(1,3) * m(3,0) - m(0,3) * m(1,0) * m(3,2) + m(0,0) * m(1,3) * m(3,2) + m(0,2) * m(1,0) * m(3,3) - m(0,0) * m(1,2) * m(3,3))/x; 
  mat[7]  = (-m(0,3) * m(1,2) * m(2,0) + m(0,2) * m(1,3) * m(2,0) + m(0,3) * m(1,0) * m(2,2) - m(0,0) * m(1,3) * m(2,2) - m(0,2) * m(1,0) * m(2,3) + m(0,0) * m(1,2) * m(2,3))/x; 
  mat[8]  = (-m(1,3) * m(2,1) * m(3,0) + m(1,1) * m(2,3) * m(3,0) + m(1,3) * m(2,0) * m(3,1) - m(1,0) * m(2,3) * m(3,1) - m(1,1) * m(2,0) * m(3,3) + m(1,0) * m(2,1) * m(3,3))/x; 
  mat[9]  = ( m(0,3) * m(2,1) * m(3,0) - m(0,1) * m(2,3) * m(3,0) - m(0,3) * m(2,0) * m(3,1) + m(0,0) * m(2,3) * m(3,1) + m(0,1) * m(2,0) * m(3,3) - m(0,0) * m(2,1) * m(3,3))/x; 
  mat[10] = (-m(0,3) * m(1,1) * m(3,0) + m(0,1) * m(1,3) * m(3,0) + m(0,3) * m(1,0) * m(3,1) - m(0,0) * m(1,3) * m(3,1) - m(0,1) * m(1,0) * m(3,3) + m(0,0) * m(1,1) * m(3,3))/x; 
  mat[11] = ( m(0,3) * m(1,1) * m(2,0) - m(0,1) * m(1,3) * m(2,0) - m(0,3) * m(1,0) * m(2,1) + m(0,0) * m(1,3) * m(2,1) + m(0,1) * m(1,0) * m(2,3) - m(0,0) * m(1,1) * m(2,3))/x; 
  mat[12] = ( m(1,2) * m(2,1) * m(3,0) - m(1,1) * m(2,2) * m(3,0) - m(1,2) * m(2,0) * m(3,1) + m(1,0) * m(2,2) * m(3,1) + m(1,1) * m(2,0) * m(3,2) - m(1,0) * m(2,1) * m(3,2))/x; 
  mat[13] = (-m(0,2) * m(2,1) * m(3,0) + m(0,1) * m(2,2) * m(3,0) + m(0,2) * m(2,0) * m(3,1) - m(0,0) * m(2,2) * m(3,1) - m(0,1) * m(2,0) * m(3,2) + m(0,0) * m(2,1) * m(3,2))/x; 
  mat[14] = ( m(0,2) * m(1,1) * m(3,0) - m(0,1) * m(1,2) * m(3,0) - m(0,2) * m(1,0) * m(3,1) + m(0,0) * m(1,2) * m(3,1) + m(0,1) * m(1,0) * m(3,2) - m(0,0) * m(1,1) * m(3,2))/x; 
  mat[15] = (-m(0,2) * m(1,1) * m(2,0) + m(0,1) * m(1,2) * m(2,0) + m(0,2) * m(1,0) * m(2,1) - m(0,0) * m(1,2) * m(2,1) - m(0,1) * m(1,0) * m(2,2) + m(0,0) * m(1,1) * m(2,2))/x;
 
  qfeSetMatrixElements(i, mat);

  return qfeSuccess;
} 

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeGetMatrixTranspose(qfeMatrix4f m, qfeMatrix4f &t)
{
  qfeFloat mat[16];
  mat[ 0] = m[ 0]; mat[ 1] = m[ 4]; mat[ 2] = m[ 8]; mat[ 3] = m[12];
  mat[ 4] = m[ 1]; mat[ 5] = m[ 5]; mat[ 6] = m[ 9]; mat[ 7] = m[13];
  mat[ 8] = m[ 2]; mat[ 9] = m[ 6]; mat[10] = m[10]; mat[11] = m[14];
  mat[12] = m[ 3]; mat[13] = m[ 7]; mat[14] = m[11]; mat[15] = m[15];

  qfeSetMatrixElements(t, &mat[0]);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeFloat & qfeMatrix4f::operator [] ( int index ) 
{
  return this->matrix[index];
}

//----------------------------------------------------------------------------
qfeFloat & qfeMatrix4f::operator () ( int row, int column ) 
{
  return this->matrix[4*row + column];
}

//----------------------------------------------------------------------------
const qfeVector operator  * ( const qfeVector  & v, const qfeMatrix4f & m )
{
  qfeVector r;
  qfeMatrix4f::qfeMatrixPreMultiply(v, m, r);
  return r;
}

//----------------------------------------------------------------------------
const qfeVector operator  * ( const qfeMatrix4f & m, const qfeVector  & v )
{
  qfeVector r;
  qfeMatrix4f::qfeMatrixPostMultiply(m, v, r);
  return r;
}

//----------------------------------------------------------------------------
const qfePoint operator  * ( const qfePoint  & p, const qfeMatrix4f & m )
{
  qfePoint r;
  qfeMatrix4f::qfeMatrixPreMultiply(p, m, r);
  return r;
}

//----------------------------------------------------------------------------
const qfePoint operator  * ( const qfeMatrix4f & m, const qfePoint  & p )
{
  qfePoint r;
  qfeMatrix4f::qfeMatrixPostMultiply(m, p, r);
  return r;
}

//----------------------------------------------------------------------------
qfeFloat qfeMatrix4f::operator () ( int row, int column ) const 
{
  return this->matrix[4*row + column];
}

//----------------------------------------------------------------------------
qfeMatrix4f::operator float * () 
{
  return (qfeFloat *) &this->matrix[0];
}

//----------------------------------------------------------------------------
qfeMatrix4f::operator const float * () const 
{
  return (const qfeFloat *) &this->matrix[0];
}

//----------------------------------------------------------------------------
qfeMatrix4f qfeMatrix4f::operator * ( const qfeMatrix4f & m ) const 
{
  qfeMatrix4f r;
  qfeMatrix4f::qfeMatrixMultiply((*this), m, r);
  return r;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeMatrixMultiply(qfeMatrix4f m1, qfeMatrix4f m2, qfeMatrix4f &m)
{
  qfeFloat mat[16];

  mat[ 0] = m1(0,0)*m2(0,0) + m1(0,1)*m2(1,0) + m1(0,2)*m2(2,0) + m1(0,3)*m2(3,0);
  mat[ 1] = m1(0,0)*m2(0,1) + m1(0,1)*m2(1,1) + m1(0,2)*m2(2,1) + m1(0,3)*m2(3,1);
  mat[ 2] = m1(0,0)*m2(0,2) + m1(0,1)*m2(1,2) + m1(0,2)*m2(2,2) + m1(0,3)*m2(3,2);
  mat[ 3] = m1(0,0)*m2(0,3) + m1(0,1)*m2(1,3) + m1(0,2)*m2(2,3) + m1(0,3)*m2(3,3);

  mat[ 4] = m1(1,0)*m2(0,0) + m1(1,1)*m2(1,0) + m1(1,2)*m2(2,0) + m1(1,3)*m2(3,0);
  mat[ 5] = m1(1,0)*m2(0,1) + m1(1,1)*m2(1,1) + m1(1,2)*m2(2,1) + m1(1,3)*m2(3,1);
  mat[ 6] = m1(1,0)*m2(0,2) + m1(1,1)*m2(1,2) + m1(1,2)*m2(2,2) + m1(1,3)*m2(3,2);
  mat[ 7] = m1(1,0)*m2(0,3) + m1(1,1)*m2(1,3) + m1(1,2)*m2(2,3) + m1(1,3)*m2(3,3);

  mat[ 8] = m1(2,0)*m2(0,0) + m1(2,1)*m2(1,0) + m1(2,2)*m2(2,0) + m1(2,3)*m2(3,0);
  mat[ 9] = m1(2,0)*m2(0,1) + m1(2,1)*m2(1,1) + m1(2,2)*m2(2,1) + m1(2,3)*m2(3,1);
  mat[10] = m1(2,0)*m2(0,2) + m1(2,1)*m2(1,2) + m1(2,2)*m2(2,2) + m1(2,3)*m2(3,2);
  mat[11] = m1(2,0)*m2(0,3) + m1(2,1)*m2(1,3) + m1(2,2)*m2(2,3) + m1(2,3)*m2(3,3);

  mat[12] = m1(3,0)*m2(0,0) + m1(3,1)*m2(1,0) + m1(3,2)*m2(2,0) + m1(3,3)*m2(3,0);
  mat[13] = m1(3,0)*m2(0,1) + m1(3,1)*m2(1,1) + m1(3,2)*m2(2,1) + m1(3,3)*m2(3,1);
  mat[14] = m1(3,0)*m2(0,2) + m1(3,1)*m2(1,2) + m1(3,2)*m2(2,2) + m1(3,3)*m2(3,2);
  mat[15] = m1(3,0)*m2(0,3) + m1(3,1)*m2(1,3) + m1(3,2)*m2(2,3) + m1(3,3)*m2(3,3);


  qfeSetMatrixElements(m, mat);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeMatrixPreMultiply(qfePoint p1, qfeMatrix4f m1, qfePoint &p)
{
  p.w = p1.x*m1(0,3) + p1.y*m1(1,3) + p1.z*m1(2,3) + m1(3,3);

  p.x = (p1.x*m1(0,0) + p1.y*m1(1,0) + p1.z*m1(2,0) + m1(3,0)) / p.w; 
  p.y = (p1.x*m1(0,1) + p1.y*m1(1,1) + p1.z*m1(2,1) + m1(3,1)) / p.w;
  p.z = (p1.x*m1(0,2) + p1.y*m1(1,2) + p1.z*m1(2,2) + m1(3,2)) / p.w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeMatrixPostMultiply(qfeMatrix4f m1, qfePoint p1, qfePoint &p)
{
  p.w =  p1.x*m1(0,3) + p1.y*m1(1,3) + p1.z*m1(2,3) + m1(3,3);

  p.x = (p1.x*m1(0,0) + p1.y*m1(0,1) + p1.z*m1(0,2)) / p.w; 
  p.y = (p1.x*m1(1,0) + p1.y*m1(1,1) + p1.z*m1(1,2)) / p.w;
  p.z = (p1.x*m1(2,0) + p1.y*m1(2,1) + p1.z*m1(2,2)) / p.w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeMatrixPreMultiply(qfeVector v1, qfeMatrix4f m1, qfeVector &v)
{
  v.w = v1.x*m1(0,3) + v1.y*m1(1,3) + v1.z*m1(2,3) + m1(3,3);

  /*v.x = (v1.x*m1(0,0) + v1.y*m1(1,0) + v1.z*m1(2,0) + m1(3,0)) / v.w; 
  v.y = (v1.x*m1(0,1) + v1.y*m1(1,1) + v1.z*m1(2,1) + m1(3,1)) / v.w;
  v.z = (v1.x*m1(0,2) + v1.y*m1(1,2) + v1.z*m1(2,2) + m1(3,2)) / v.w;*/
  v.x = (v1.x*m1(0,0) + v1.y*m1(1,0) + v1.z*m1(2,0)) / v.w; 
  v.y = (v1.x*m1(0,1) + v1.y*m1(1,1) + v1.z*m1(2,1)) / v.w;
  v.z = (v1.x*m1(0,2) + v1.y*m1(1,2) + v1.z*m1(2,2)) / v.w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeMatrixPostMultiply(qfeMatrix4f m1, qfeVector v1, qfeVector   &v)
{
  v.w = v1.x*m1(0,3) + v1.y*m1(1,3) + v1.z*m1(2,3) + m1(3,3);

  v.x = (v1.x*m1(0,0) + v1.y*m1(0,1) + v1.z*m1(0,2)) / v.w; 
  v.y = (v1.x*m1(1,0) + v1.y*m1(1,1) + v1.z*m1(1,2)) / v.w;
  v.z = (v1.x*m1(2,0) + v1.y*m1(2,1) + v1.z*m1(2,2)) / v.w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeMatrixSubtract(qfeMatrix4f m1, qfeMatrix4f m2, qfeMatrix4f &m)
{
  qfeFloat mat[16];

  mat[ 0] = m1(0,0)-m2(0,0);
  mat[ 1] = m1(0,1)-m2(0,1);
  mat[ 2] = m1(0,2)-m2(0,2);
  mat[ 3] = m1(0,3)-m2(0,3);
  mat[ 4] = m1(1,0)-m2(1,0);
  mat[ 5] = m1(1,1)-m2(1,1);
  mat[ 6] = m1(1,2)-m2(1,2);
  mat[ 7] = m1(1,3)-m2(1,3);
  mat[ 8] = m1(2,0)-m2(2,0);
  mat[ 9] = m1(2,1)-m2(2,1);
  mat[10] = m1(2,2)-m2(2,2);
  mat[11] = m1(2,3)-m2(2,3);
  mat[12] = m1(3,0)-m2(3,0);
  mat[13] = m1(3,1)-m2(3,1);
  mat[14] = m1(3,2)-m2(3,2);
  mat[15] = m1(3,3)-m2(3,3);

  qfeSetMatrixElements(m, mat);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMatrix4f::qfeMatrixPrint(qfeMatrix4f m)
{
  cout << "| " << m[0]  << ", " << m[1]  << ", " << m[2]  << ", " << m[3]  << " |" << endl;
  cout << "| " << m[4]  << ", " << m[5]  << ", " << m[6]  << ", " << m[7]  << " |" << endl;
  cout << "| " << m[8]  << ", " << m[9]  << ", " << m[10] << ", " << m[10] << " |" << endl;
  cout << "| " << m[11] << ", " << m[12] << ", " << m[13] << ", " << m[14] << " |" << endl;
  cout << endl;
  return qfeSuccess;
}