/**
* \file   qfeImageData.h
* \author Roy van Pelt
* \class  qfeImageData
* \brief  Implements a data structure for qflow data
*
* qfeImageData derives from vtkImageData, and add domain
* specific parameters, such as the amount of phases of the cardiac cycle,
* and the image geometry (transformation from image space to patient space).
*
* The XML structured file (extension *.vti), contains
* the morphologic data in the scalar structure.
* The flow velocities are contained in the in the vector structure.
*/
#ifndef __qfeImageData_h
#define __qfeImageData_h

#include "vtkImageData.h"

enum qfeDataOrientation
{
  qfeAxial,
  qfeCoronal,
  qfeSagittal
};

enum qfeDataType
{
  qfeDataFlow4D,
  qfeDataFlow2D,
  qfeDataSSFP3D,
  qfeDataSSFP2D
};

class qfeImageData : public vtkImageData
{
public:
  static qfeImageData *New();

  vtkTypeMacro(qfeImageData,vtkImageData);

  vtkSetStringMacro(PatientName);
  vtkGetStringMacro(PatientName);

  vtkSetStringMacro(PatientID);
  vtkGetStringMacro(PatientID);

  vtkSetVector3Macro(AxisX,double);
  vtkGetVector3Macro(AxisX,double);

  vtkSetVector3Macro(AxisY,double);
  vtkGetVector3Macro(AxisY,double);

  vtkSetVector3Macro(AxisZ,double);
  vtkGetVector3Macro(AxisZ,double);

  vtkSetMacro(Phase, int);
  vtkGetMacro(Phase, int);

  vtkSetMacro(PhaseCount, int);
  vtkGetMacro(PhaseCount, int);

  vtkSetMacro(PhaseDuration, double);
  vtkGetMacro(PhaseDuration, double);

  vtkSetMacro(PhaseTriggerTime, double);
  vtkGetMacro(PhaseTriggerTime, double);

  vtkSetVector3Macro(Venc, double);
  vtkGetVector3Macro(Venc, double);

  vtkSetMacro(ScanType, qfeDataType);
  vtkGetMacro(ScanType, qfeDataType);

  vtkSetMacro(ScanOrientation, qfeDataOrientation);
  vtkGetMacro(ScanOrientation, qfeDataOrientation);

  virtual void DeepCopy(vtkDataObject *);
  virtual void InternalImageDataCopy(qfeImageData *);

  //BTX
  // Description:
  // Retrieve an instance of this class from an information object.
  static qfeImageData* GetData(vtkInformation* info);
  static qfeImageData* GetData(vtkInformationVector* v, int i=0);
  //ETX

protected:
  qfeImageData();
  ~qfeImageData();
private:
  char *              PatientName;
  char *              PatientID;
  double              AxisX[3];
  double              AxisY[3];
  double              AxisZ[3];
  int                 Phase;
  int                 PhaseCount;
  double              PhaseDuration;
  double              PhaseTriggerTime;
  double              Venc[3];  
  qfeDataType         ScanType;
  qfeDataOrientation  ScanOrientation;

  qfeImageData(const qfeImageData&);  // Not implemented.
  void operator=(const qfeImageData&);  // Not implemented.
};

#endif

