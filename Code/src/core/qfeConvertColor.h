#pragma once

#include "qflowexplorer.h"

#include <vector>
#include <cmath>

/**
* \file   qfeConvertColor.h
* \author Roy van Pelt
* \class  qfeConvertColor
* \brief  Implements conversions between color spaces
* \note   Confidential
*
* Convert between color spaces
*/

using namespace std;

class qfeConvertColor
{
public:
  static const qfeColorXYZ whiteD50;
  static const qfeColorXYZ whiteD65;

  static qfeReturnStatus qfeRgb2Hsv(qfeColorRGB in, qfeColorHSV &out);
  static qfeReturnStatus qfeHsv2Rgb(qfeColorHSV in, qfeColorRGB &out);

  static qfeReturnStatus qfeRgb2Xyz(qfeColorRGB in, qfeColorXYZ &out);
  static qfeReturnStatus qfeXyz2Rgb(qfeColorXYZ in, qfeColorRGB &out);
  static qfeReturnStatus qfeXyz2Lab(qfeColorXYZ in, qfeColorLab &out, const qfeColorXYZ white);
  static qfeReturnStatus qfeLab2Xyz(qfeColorLab in, qfeColorXYZ &out, const qfeColorXYZ white);
  static qfeReturnStatus qfeXyz2Luv(qfeColorXYZ in, qfeColorLuv &out, const qfeColorXYZ white);
  static qfeReturnStatus qfeLuv2Xyz(qfeColorLuv in, qfeColorXYZ &out, const qfeColorXYZ white);

  static qfeColorRGB  lerp(qfeColorRGB value1, qfeColorRGB value2, qfeFloat amount);
  static qfeColorLab  lerp(qfeColorLab value1, qfeColorLab value2, qfeFloat amount);
  static qfeColorLuv  lerp(qfeColorLuv value1, qfeColorLuv value2, qfeFloat amount);
  static qfeFloat     lerp(qfeFloat value1, qfeFloat value2, qfeFloat amount);

  static qfeColorRGB  clamp(qfeColorRGB in);
  static qfeFloat     clamp(qfeFloat in);

protected:
  static qfeFloat     linearize_sRGB_component(qfeFloat in);
  static qfeFloat     delinearize_sRGB_component(qfeFloat in);
  static qfeColorRGB  linearize_sRGB(const qfeColorRGB &in);
  static qfeColorRGB  delinearize_sRGB(const qfeColorRGB &in);

  static qfeFloat     forwardLabTransform(qfeFloat in);
};
