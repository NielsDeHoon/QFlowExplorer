#include "qfeImageDataManager.h"

#include <math.h>

//----------------------------------------------------------------------------
qfeImageDataManager::qfeImageDataManager()
{
  this->patient            = NULL;

  this->dataFormat         = QFE_FORMAT_VTK;

  this->glInit   = false;
  this->dataInit = false;
  this->simInit  = false;
}

//----------------------------------------------------------------------------
qfeImageDataManager::~qfeImageDataManager()
{
  this->qfeReleaseData();
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeLoadVolume(char *path)
{
  char *paths[1];
  paths[0] = path;
  return this->qfeLoadSeries(paths, 1);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeLoadSeries(char **paths, int phases)
{  
  if(phases > QFE_MAX_PHASES)
  {
    cout << "qfeImageDataManager::LoadSeries - Too many phases loaded." << endl;
    return qfeError;
  }

  if(phases == 0)
  {
    cout << "qfeImageDataManager::LoadSeries - No data loaded." << endl;
    return qfeError;
  }

  if(this->patient == NULL)
  {
    this->patient =  new qfePatient();
  }

  // Instantiate the data structures
  this->patient->id                = 0;  

  // Iterate over the phases and fill the data structures
  qfeCallData cd;
  cd.message  = "Loading volume data...";
  cd.stepSize = 100;

  if(phases > 0) cd.stepSize = (int)100.0/phases;
  
  for(int i=0; i < phases; i++)
  {
    switch(this->dataFormat)
    {
      case QFE_FORMAT_VTK : this->qfeReadVTK(paths[i], this->patient->study);
                            break;
      default             : cout << "qfeImageDataManager::qfeLoadSeries - Unsupported file format" << endl;
                            return qfeError;
    }  
    this->InvokeEvent(vtkCommand::UpdateEvent, &cd);

    cout << "Loaded: " << paths[i] << endl;
  }  
  
  this->patient->study->phases        = (int)this->patient->study->pcap.size();
  if(this->patient->study->phases > 0)
    this->patient->study->pcap.front()->qfeGetVolumePhaseDuration(this->patient->study->phaseDuration);

  // Fix round errors here
  //cd.stepSize = 100 - (phases * cd.stepSize);
  //if(cd.stepSize > 0) this->InvokeEvent(vtkCommand::UpdateEvent, &cd);

  // Finalize the progress bar
  cd.stepSize = -1;
  this->InvokeEvent(vtkCommand::UpdateEvent, &cd);
  this->InvokeEvent(vtkCommand::UpdateInformationEvent, &cd);

  // Get data folder name
  char   folder[500];
  int    index;

  index = (strrchr(paths[0], '/') - paths[0] + 1);
  strncpy(folder, paths[0], index);
  folder[index] = '\0';

  this->dataPath = string(folder);
  this->qfePostProcess(paths[0], this->patient->study);

  this->patient->study->qfeSetSeriesDataFolder(this->dataPath);
  
  // Data is now loaded
  this->dataInit = true;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeLoadFluidSimulation(char **paths, int phases)
{
  if(phases > QFE_MAX_PHASES)
  {
    cout << "qfeImageDataManager::LoadFluidSimulation - Too many phases loaded." << endl;
    return qfeError;
  }

  if(phases == 0)
  {
    cout << "qfeImageDataManager::LoadFluidSimulation - No data loaded." << endl;
    return qfeError;
  }

  if(this->patient == NULL)
  {
    cout << "qfeImageDataManager::LoadFluidSimulation - Load flow data first" << endl;
    return qfeError;    
  }

  // TODO NDH Load fluid simulation data to study
  cout << "TODO NDH: qfeImageDataManager::qfeWriteFluidSimulation" << endl;  

  // Iterate over the phases and fill the data structures
  qfeCallData cd;
  cd.message  = "Loading fluid simulation data...";
  cd.stepSize = 100;

  if(phases > 0) cd.stepSize = (int)100.0/phases;
  
  for(int i=0; i < phases; i++)
  {

    switch(this->dataFormat)
    {
      case QFE_FORMAT_VTK : this->qfeReadVTKFlowSim(paths[i], this->patient->study);
                            break;
      default             : cout << "qfeImageDataManager::qfeLoadSeries - Unsupported file format" << endl;
                            return qfeError;
    }  
    this->InvokeEvent(vtkCommand::UpdateEvent, &cd);

    cout << "Loaded: " << paths[i] << endl;
  }  
  
  this->patient->study->sim_phases        = (int)this->patient->study->sim_velocities.size();
  if(this->patient->study->sim_phases > 0)
    this->patient->study->sim_velocities.front()->qfeGetVolumePhaseDuration(this->patient->study->sim_phaseDuration);

  // Fix round errors here
  //cd.stepSize = 100 - (phases * cd.stepSize);
  //if(cd.stepSize > 0) this->InvokeEvent(vtkCommand::UpdateEvent, &cd);

  // Finalize the progress bar
  cd.stepSize = -1;
  this->InvokeEvent(vtkCommand::UpdateEvent, &cd);
  this->InvokeEvent(vtkCommand::UpdateInformationEvent, &cd);

  // Get data folder name
  char   folder[500];
  int    index;

  index = (strrchr(paths[0], '/') - paths[0] + 1);
  strncpy(folder, paths[0], index);
  folder[index] = '\0';

  this->dataPath = string(folder);
  
  // Fluid simulation is now loaded
  this->simInit = true;

  return qfeSuccess;  
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeLoadSegmentation(const char *path)
{
  vtkSmartPointer<vtkPolyData> model; 
  vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New();
  reader->SetFileName(path);
  reader->Update();

  string      p;
  qfeColorRGB c;
  string      l;

  p = string(path);

  // Read the model
  model = reader->GetOutput();

  // Fix missing information
  vtkCellArray *strips   = model->GetStrips();
  vtkDataArray *normals  = model->GetPointData()->GetNormals();
  vtkDataArray *curv_max = model->GetPointData()->GetArray("Maximum_Curvature_Vector");
  vtkDataArray *curv_min = model->GetPointData()->GetArray("Minimum_Curvature_Vector");
  vtkDataArray *curv_val = model->GetPointData()->GetArray("Principal_Curvatures");

  /*vtkSmartPointer<vtkQuadricDecimation> decimate = vtkSmartPointer<vtkQuadricDecimation>::New();
  decimate->SetInput(model);
  decimate->SetTargetReduction(0.9);
  decimate->AttributeErrorMetricOn();
  decimate->Update();  
  model->DeepCopy(decimate->GetOutput());*/

  if(strips == NULL || strips->GetNumberOfCells() == 0)
    this->qfeComputeTriangleStrips(model);
  if(normals == NULL) 
    this->qfeComputeNormals(model); 
  if((curv_max == NULL) || (curv_min == NULL) || (curv_val == NULL)) 
    this->qfeComputeCurvature(model); 

  //vtkDataArray *wss = model->GetPointData()->GetArray("wall_shear");
  //for(int i=0; i<100;i++)
  //   cout << wss->GetTuple1(i) << endl;

/*
  vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
  writer->SetInput(model);
  writer->SetFileName("c:\\test.vtk");
  writer->Write();
*/
  // Default color
  c.r = 0.9; c.g = 0.9;  c.b = 0.9;
  l = "";

  // New style
  // Presets for color and label, based on their file name
  if(p.find("_ao") != string::npos)
  {    
    c.r = 255.0/255.0; 
    c.g = 127.0/255.0;  
    c.b = 126.0/255.0;
    l = "aorta";
  }
  else
  if(p.find("_pa") != string::npos)
  {   
    c.r = 228.0/255.0; 
    c.g = 180.0/255.0;  
    c.b = 255.0/255.0;
    l = "pulmonary arteries";
  }
  else
  if(p.find("_vc") != string::npos)
  {
    c.r = 154.0/255.0; 
    c.g = 153.0/255.0;  
    c.b = 255.0/255.0;
    l = "vena cava";
  }
  else
  if(p.find("_rv") != string::npos)
  {
    c.r = 128.0/255.0; 
    c.g = 127.0/255.0;  
    c.b = 229.0/255.0;
    l = "right ventricle";
  }
  else
  if(p.find("_lv") != string::npos)
  {
    c.r = 128.0/255.0; 
    c.g = 127.0/255.0;  
    c.b = 229.0/255.0;
    l = "left ventricle";
  }
  else
  if(p.find("_ra") != string::npos)
  {
    c.r = 180.0/255.0; 
    c.g = 179.0/255.0;  
    c.b = 255.0/255.0;
    l = "right atrium";
  }
  else
  if(p.find("_la") != string::npos)
  {
    c.r = 180.0/255.0; 
    c.g = 179.0/255.0;  
    c.b = 255.0/255.0;
    l = "left atrium";
  }
  else
  if(p.find("_pv") != string::npos)
  {
    c.r = 255.0/255.0; 
    c.g = 203.0/255.0;  
    c.b = 153.0/255.0;
    l = "pulmonary veins";
  }

  // Presets for color and label, based on their file name
  if(p.find("aorta") != string::npos)
  {
    //R = 223 B = 125 G = 132
    c.r = 0.8745; c.g = 0.4901;  c.b = 0.5176;
    l = "aorta";
  }
  else
  if(p.find("pulmonaries") != string::npos)
  {
    //R = 158 B = 179 G = 198
    c.r = 0.6196; c.g = 0.702;  c.b = 0.7765;
    l = "pulmonary arteries";
  }
  else
  if(p.find("venacava") != string::npos)
  {
    //R = 244 B = 236 G = 223
    c.r = 0.9569; c.g = 0.9255;  c.b = 0.8745;
    l = "vena cava";
  }  
    
  this->patient->study->segmentation.push_back(new qfeModel());
  this->patient->study->segmentation.back()->qfeSetModelMesh(model);
  this->patient->study->segmentation.back()->qfeSetModelColor(c);
  this->patient->study->segmentation.back()->qfeSetModelLabel(l);
  this->patient->study->segmentation.back()->qfeSetModelVisible(true);

  cout << "Loaded: " << path << endl;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeLoadSegmentation(qfeVolume *volume, qfeFloat isoValue)
{
  vtkSmartPointer<qfeImageData> vtkVolume;
  qfeGrid      *grid;
  vtkPolyData  *model; 
  qfeColorRGB   c;
  string        l;

  vtkPoints    *points;
  double       *pointBounds;
  int           pointCount;
  qfeFloat      o[3];
  double        r[3],t[3];
  float         s[3];
  unsigned int  d[3];
    
  vtkVolume = vtkSmartPointer<qfeImageData>::New();
  qfeConvert::qfeVolumeToVtkImageData(volume, vtkVolume);

  // Extract the iso surface
  vtkSmartPointer<vtkContourFilter> isosurface = vtkSmartPointer<vtkContourFilter>::New();  
  isosurface->SetInput(vtkVolume);
  isosurface->SetValue(0, isoValue);

  // Only show largest component
  vtkSmartPointer<vtkPolyDataConnectivityFilter> conn = vtkSmartPointer<vtkPolyDataConnectivityFilter>::New();
  conn->SetInput(isosurface->GetOutput());
  conn->SetExtractionModeToLargestRegion();

  // Smoothen
  vtkSmartPointer<vtkWindowedSincPolyDataFilter> smoother = vtkSmartPointer<vtkWindowedSincPolyDataFilter>::New();
  smoother->SetInput(conn->GetOutput());
  smoother->SetNumberOfIterations(10);
  smoother->BoundarySmoothingOn();
  smoother->SetFeatureAngle(120);
  smoother->SetEdgeAngle(90);
  smoother->SetPassBand(0.1);   

  vtkSmartPointer<vtkCleanPolyData> cleaner = vtkSmartPointer<vtkCleanPolyData>::New();
  cleaner->SetInput(smoother->GetOutput());
  cleaner->Update();

  // Get the model
  model = cleaner->GetOutput();   

  // Fix missing information
  vtkCellArray *strips   = model->GetStrips();
  vtkDataArray *normals  = model->GetPointData()->GetNormals();
  vtkDataArray *curv_max = model->GetPointData()->GetArray("Maximum_Curvature_Vector");
  vtkDataArray *curv_min = model->GetPointData()->GetArray("Minimum_Curvature_Vector");
  vtkDataArray *curv_val = model->GetPointData()->GetArray("Principal_Curvatures");
  
  if(normals == NULL) 
    this->qfeComputeNormals(model); 
  if((curv_max == NULL) || (curv_min == NULL) || (curv_val == NULL)) 
    this->qfeComputeCurvature(model);
  if(strips == NULL || strips->GetNumberOfCells() == 0)
    this->qfeComputeTriangleStrips(model);

  //model->Print(cerr);

  // Fix the model
  model->GetCenter(r);

  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(d[0], d[1], d[2]);

  grid->qfeGetGridExtent(s[0],s[1],s[2]);
  grid->qfeGetGridOrigin(o[0],o[1],o[2]);

  r[0] = r[0] - 0.5*d[0]*s[0];
  r[1] = r[1] - 0.5*d[1]*s[1];
  r[2] = r[2] - 0.5*d[2]*s[2];

  t[0] = o[0]-r[0];
  t[1] = o[1]-r[1];
  t[2] = o[2]-r[2];
  
  points      = model->GetPoints();
  if(points != NULL)
  {
    pointCount  = points->GetNumberOfPoints();
    pointBounds = points->GetBounds();
  }
  else
    pointCount  = 0;
  
  for(int i=0; i<pointCount; i++)
  {
    double *p = points->GetPoint(i);    
    double  v[3];
    qfePoint current;

    v[0] = (p[0]-r[0]-t[0])/s[0];
    v[1] = (p[1]-r[1]-t[1])/s[1];
    v[2] = (p[2]-r[2]-t[2])/s[2];
    points->SetPoint(i, v[0],v[1],v[2]);
  }

  model->Update(); 

  // Default for segmentation
  c.r = 229.0/255.0;
  c.g = 202.0/255.0;
  c.b = 197.0/255.0;

  // Set the model  
  l = "isosurface";
  this->patient->study->segmentation.push_back(new qfeModel());
  this->patient->study->segmentation.back()->qfeSetModelMesh(model);
  this->patient->study->segmentation.back()->qfeSetModelColor(c);
  this->patient->study->segmentation.back()->qfeSetModelLabel(l);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeLoadSeeds(const char *path)
{
  ifstream    seedsFile;
  std::string line, word[8];
  qfePoint    point;
  qfePoint    attrib;
  int         count = 0;

  if(this->patient == NULL)
  {
    cout << "qfeImageDataMapper::qfeLoadSeeds - Load data first" << endl;
    return qfeError;
  }

  this->patient->study->seeds.clear();
  this->patient->study->seedAttribs.clear();

  seedsFile.open(path);

  if (seedsFile.is_open())
  {
    while(getline (seedsFile, line))
    {   
      std::string currentWord;
      std::istringstream iss(line, std::istringstream::in);
      for(int i=0; i<8; i++)
        if(iss >> currentWord)                   
          word[i] = currentWord;          
        else
          word[i] = "0.0";
      
      point.x = atof(word[0].c_str());
      point.y = atof(word[1].c_str());
      point.z = atof(word[2].c_str());
      point.w = atof(word[3].c_str());

      attrib.x = atof(word[4].c_str());
      attrib.y = atof(word[5].c_str());
      attrib.z = atof(word[6].c_str());
      attrib.w = atof(word[7].c_str());

      this->patient->study->seeds.push_back(point);
      this->patient->study->seedAttribs.push_back(attrib);

      count++; 
    }
  }

  seedsFile.close();

  cout << "Loaded: " << count << " seed positions" << endl;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeLoadClusterHierarchy(const char *path)
{
  if(this->patient == NULL) return qfeError;
  qfeStudy *study = this->patient->study;
  if(study == NULL) return qfeError;

  if(study->hierarchy != NULL) delete study->hierarchy;
  study->hierarchy = new qfeClusterHierarchy();

  if(qfeFileExists(path) == false) return qfeError;

  // Import cluster hierarchy
  study->hierarchy->qfeImportClusterData(path);
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeReleaseData()
{
  if(this->dataInit)
  {
    delete this->patient;
    this->patient = NULL;

    this->dataInit = false;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeLoadProbe(string path)
{
  qfeProbe3D  p;
  ifstream    probeFile;
  std::string line, word;

  if(this->patient == NULL) return qfeError;
  qfeStudy *study = this->patient->study;
  if(study == NULL) return qfeError;

  probeFile.open(path.c_str());    

  if (probeFile.is_open())
  {
    getline (probeFile, line);
    //header      = header.erase(0, 6);
    //colorSize   = atoi(header.c_str());

    if(line.compare("PROBE 3D") != 0) return qfeError;

    // probe origin
    getline (probeFile, line);
    //line  = line.erase(0, 7);
    std::istringstream iss(line, std::istringstream::in);
    iss >> word; iss >> word;          
    cout << word << ", ";
    p.origin.x = atof(word.c_str());
    iss >> word;   
    cout << word << ", ";
    p.origin.y = atof(word.c_str());
    iss >> word;   
    cout << word << endl;
    p.origin.z = atof(word.c_str()); 

    // probe axis X
    getline(probeFile, line);
    iss.clear();
    iss.str(line);
    iss >> word; iss >> word;
    cout << word << ", ";
    p.axes[0].x = atof(word.c_str());
    iss >> word;   
    cout << word << ", ";
    p.axes[0].y = atof(word.c_str());
    iss >> word;   
    cout << word << endl;
    p.axes[0].z = atof(word.c_str()); 

    // probe axis Y
    getline(probeFile, line);
    iss.clear();
    iss.str(line);
    iss >> word; iss >> word;
    cout << word << ", ";
    p.axes[1].x = atof(word.c_str());
    iss >> word;   
    cout << word << ", ";
    p.axes[1].y = atof(word.c_str());
    iss >> word;   
    cout << word << endl;
    p.axes[1].z = atof(word.c_str()); 

    // probe axis Z
    getline(probeFile, line);
    iss.clear();
    iss.str(line);
    iss >> word; iss >> word;
    cout << word << ", ";
    p.axes[2].x = atof(word.c_str());
    iss >> word;   
    cout << word << ", ";
    p.axes[2].y = atof(word.c_str());
    iss >> word;   
    cout << word << endl;
    p.axes[2].z = atof(word.c_str()); 

    // probe length
    getline(probeFile, line);
    iss.clear();
    iss.str(line);
    iss >> word; iss >> word;
    p.length = atof(word.c_str());

    // probe radius top
    getline(probeFile, line);
    iss.clear();
    iss.str(line);
    iss >> word; iss >> word;
    cout << word << endl;
    p.topRadius = atof(word.c_str());

    // probe radius base
    getline(probeFile, line);
    iss.clear();
    iss.str(line);
    iss >> word; iss >> word;
    cout << word << endl;
    p.baseRadius = atof(word.c_str());

    // number of slices
    getline(probeFile, line);
    iss.clear();
    iss.str(line);
    iss >> word; iss >> word;
    cout << word << endl;
    p.numSlices = atoi(word.c_str());

    // number of stacks
    getline(probeFile, line);
    iss.clear();
    iss.str(line);
    iss >> word; iss >> word;
    cout << word << endl;
    p.numStacks = atoi(word.c_str());

    // probe axis Z
    getline(probeFile, line);
    iss.clear();
    iss.str(line);
    iss >> word; iss >> word;
    cout << word << ", ";
    p.color.r = atof(word.c_str());
    iss >> word;   
    cout << word << ", ";
    p.color.g = atof(word.c_str());
    iss >> word;   
    cout << word << endl;
    p.color.b = atof(word.c_str()); 

    // border width
    getline(probeFile, line);
    iss.clear();
    iss.str(line);
    iss >> word; iss >> word;
    cout << word << endl;
    p.borderWidth = atoi(word.c_str());
  
    probeFile.close();   

    // Add the probe to the study
    if(this->qfeExistsProbe(&p) != qfeSuccess)
    {
      study->probe3D.push_back(p);
      study->probe3DCache.push_back(p);

      // Set the current state
      study->probe3D.back().interactionState = probeInteractNone;
      study->probe3D.back().selected         = true;

      // Probe specific planes  
      study->probe3D.back().parallelPlane.qfeSetFrameExtent(2.0*max(p.baseRadius, p.topRadius), p.length);
      study->probe3D.back().parallelPlane.qfeSetFrameOrigin(p.origin.x, p.origin.y, p.origin.z);
      study->probe3D.back().parallelPlane.qfeSetFrameAxes(p.axes[1].x, p.axes[1].y, p.axes[1].z,
                                                          p.axes[2].x, p.axes[2].y, p.axes[2].z,
                                                          p.axes[0].x, p.axes[0].y, p.axes[0].z);   

      
      study->probe3D.back().orthoPlane.qfeSetFrameExtent(1,1);
      study->probe3D.back().orthoPlane.qfeSetFrameOrigin(0.0,0.0,0.0);
      study->probe3D.back().orthoPlane.qfeSetFrameAxes(0.0,0.0,-1.0, 0.0,1.0,0.0, 1.0,0.0,0.0);
      study->probe3D.back().orthoPlaneOffset = 0.5f;  
    }
  }
  else cout << "Error loading probe" << endl;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeSaveProbe(qfeProbe3D *probe, string path)
{
  // Write to file
  ofstream probeFile;
  probeFile.open(path.c_str());  
  probeFile << "PROBE 3D\n";
  probeFile << "ORIGIN " << probe->origin.x << " " << probe->origin.y << " " << probe->origin.z <<"\n";
  probeFile << "AXISX " << probe->axes[0].x << " " << probe->axes[0].y << " " << probe->axes[0].z <<"\n";
  probeFile << "AXISY " << probe->axes[1].x << " " << probe->axes[1].y << " " << probe->axes[1].z <<"\n";
  probeFile << "AXISZ " << probe->axes[2].x << " " << probe->axes[2].y << " " << probe->axes[2].z <<"\n";
  probeFile << "LENGTH " << probe->length <<"\n";
  probeFile << "RADIUSTOP " << probe->topRadius <<"\n";
  probeFile << "RADIUSBASE " << probe->baseRadius <<"\n";
  probeFile << "NUMSLICES " << probe->numSlices <<"\n";
  probeFile << "NUMSTACKS " << probe->numStacks <<"\n";
  probeFile << "COLOR " << probe->color.r << " " << probe->color.g << " " << probe->color.b <<"\n";
  probeFile << "BORDER " << probe->borderWidth <<"\n";
  probeFile.close();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeExistsProbe(qfeProbe3D *probe)
{
  if(this->patient == NULL) return qfeError;
  qfeStudy *study = this->patient->study;
  if(study == NULL) return qfeError;

  bool equal = false;

  for(int i=0; i<(int)study->probe3D.size(); i++)
  {
    if(study->probe3D[i].origin.x   == probe->origin.x    &&
       study->probe3D[i].origin.y   == probe->origin.y    &&
       study->probe3D[i].origin.z   == probe->origin.z    &&
       study->probe3D[i].axes[0].x  == probe->axes[0].x   &&
       study->probe3D[i].axes[0].y  == probe->axes[0].y   &&
       study->probe3D[i].axes[0].z  == probe->axes[0].z   &&
       study->probe3D[i].axes[1].x  == probe->axes[1].x   &&
       study->probe3D[i].axes[1].y  == probe->axes[1].y   &&
       study->probe3D[i].axes[1].z  == probe->axes[1].z   &&
       study->probe3D[i].axes[2].x  == probe->axes[2].x   &&
       study->probe3D[i].axes[2].y  == probe->axes[2].y   &&
       study->probe3D[i].axes[2].z  == probe->axes[2].z   &&
       study->probe3D[i].length     == probe->length      &&
       study->probe3D[i].topRadius  == probe->topRadius   &&
       study->probe3D[i].baseRadius == probe->baseRadius)
       equal = true;
  }

  if(equal) return qfeSuccess;
  else      return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeGetPatient(qfePatient **patient)
{
  if(this->dataInit)
  {
    *patient = this->patient;
  }
  else
  {    
    *patient = NULL;
    return qfeError;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeReadVTK(char *path, qfeStudy *study)
{
  // Read the data at the given path
  vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
  reader->SetFileName(path);
  reader->Update();

  int scanType = reader->GetOutput()->GetScanType();

  if(scanType < 0 || scanType > 3) scanType = 0;

  switch(scanType)
  {
  case(qfeDataFlow4D) : qfeReadVTKFlow4D(path, study);
                        break;
  case(qfeDataFlow2D) : qfeReadVTKFlow2D(path, study);
                        break;
  case(qfeDataSSFP3D) : qfeReadVTKSSFP3D(path, study);
                        break;
  case(qfeDataSSFP2D) : qfeReadVTKSSFP2D(path, study);
                        break;
  default             : cout << "qfeImageDataManager::qfeReadVTK - Unknown data type" << endl;
                        return qfeError;
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeReadVTKFlow4D(char *path, qfeStudy *study)
{
  // Read the data at the given path
  qfeVTIImageDataReader *reader = qfeVTIImageDataReader::New();
  reader->SetFileName(path);
  reader->Update();

  vtkSmartPointer<qfeImageData> a = vtkSmartPointer<qfeImageData>::New();
  vtkSmartPointer<qfeImageData> m = vtkSmartPointer<qfeImageData>::New();
  vtkSmartPointer<qfeImageData> v = vtkSmartPointer<qfeImageData>::New();

  vtkDataArray *arrayAnatomy    = reader->GetOutput()->GetPointData()->GetArray("qflow anatomy");
  vtkDataArray *arrayMagnitudes = reader->GetOutput()->GetPointData()->GetArray("qflow magnitudes");
  vtkDataArray *arrayVelocities = reader->GetOutput()->GetPointData()->GetArray("qflow velocities");

  // The data comes from one file, which we separate here into two structures.
  if(arrayAnatomy != NULL)
  {
    study->ffe.push_back(new qfeVolume());

    a->CopyStructure(reader->GetOutput());    
    a->SetAxisX(reader->GetOutput()->GetAxisX());
    a->SetAxisY(reader->GetOutput()->GetAxisY());
    a->SetAxisZ(reader->GetOutput()->GetAxisZ());
    a->SetPhaseCount(reader->GetOutput()->GetPhaseCount());
    a->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    a->SetPhaseTriggerTime(reader->GetOutput()->GetPhaseTriggerTime());
    a->SetScanType(reader->GetOutput()->GetScanType());
    a->SetScanOrientation(reader->GetOutput()->GetScanOrientation());

    a->GetPointData()->AddArray(arrayAnatomy);
    a->GetPointData()->SetActiveScalars("qflow anatomy");

    a->SetScalarType(arrayAnatomy->GetDataType());
    a->Update();
  }
  else cout << "qfeImageDataManager::LoadSeries - Could not load anatomy data" << endl;

  if(arrayMagnitudes != NULL)
  {
    study->pcam.push_back(new qfeVolume());

    m->CopyStructure(reader->GetOutput());
    m->SetAxisX(reader->GetOutput()->GetAxisX());
    m->SetAxisY(reader->GetOutput()->GetAxisY());
    m->SetAxisZ(reader->GetOutput()->GetAxisZ());
    m->SetVenc(reader->GetOutput()->GetVenc());
    m->SetPhaseCount(reader->GetOutput()->GetPhaseCount());
    m->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    m->SetPhaseTriggerTime(reader->GetOutput()->GetPhaseTriggerTime());
    m->SetScanType(reader->GetOutput()->GetScanType());
    m->SetScanOrientation(reader->GetOutput()->GetScanOrientation());

    m->GetPointData()->AddArray(arrayMagnitudes);
    if(m->GetPointData()->GetNumberOfComponents() == 1)
      m->GetPointData()->SetActiveScalars("qflow magnitudes");
    else
      m->GetPointData()->SetActiveVectors("qflow magnitudes");

    m->SetScalarType(arrayMagnitudes->GetDataType());
    m->Update();
  }
  else cout << "qfeImageDataManager::LoadSeries - Could not load qflow data" << endl;

  if(arrayVelocities != NULL)
  {
    study->pcap.push_back(new qfeVolume());

    v->CopyStructure(reader->GetOutput());
    v->SetAxisX(reader->GetOutput()->GetAxisX());
    v->SetAxisY(reader->GetOutput()->GetAxisY());
    v->SetAxisZ(reader->GetOutput()->GetAxisZ());
    v->SetVenc(reader->GetOutput()->GetVenc());
    v->SetPhaseCount(reader->GetOutput()->GetPhaseCount());
    v->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    v->SetPhaseTriggerTime(reader->GetOutput()->GetPhaseTriggerTime());
    v->SetScanType(reader->GetOutput()->GetScanType());
    v->SetScanOrientation(reader->GetOutput()->GetScanOrientation());

    v->GetPointData()->AddArray(arrayVelocities);
    v->GetPointData()->SetActiveVectors("qflow velocities");

    v->SetScalarType(arrayVelocities->GetDataType());
    v->Update();
  }
  else cout << "qfeImageDataManager::LoadSeries - Could not load qflow data" << endl;

  // Convert to QFE structures
  if(arrayAnatomy != NULL)    qfeConvert::vtkImageDataToQfeVolume(a, study->ffe.back());
  if(arrayMagnitudes != NULL) qfeConvert::vtkImageDataToQfeVolume(m, study->pcam.back());
  if(arrayVelocities != NULL) qfeConvert::vtkImageDataToQfeVolume(v, study->pcap.back());

  reader->Delete();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeReadVTKFlow2D(char *path, qfeStudy *study)
{
  vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
  reader->SetFileName(path);
  reader->Update();

  study->flow2D_ffe.push_back(new qfeVolume());
  study->flow2D_pcam.push_back(new qfeVolume());
  study->flow2D_pcap.push_back(new qfeVolume());
  study->flow2D_phases = reader->GetOutput()->GetPhaseCount();

  vtkSmartPointer<qfeImageData> a = vtkSmartPointer<qfeImageData>::New();
  vtkSmartPointer<qfeImageData> m = vtkSmartPointer<qfeImageData>::New();
  vtkSmartPointer<qfeImageData> v = vtkSmartPointer<qfeImageData>::New();

  vtkDataArray *arrayAnatomy    = reader->GetOutput()->GetPointData()->GetArray("qflow anatomy");
  vtkDataArray *arrayMagnitudes = reader->GetOutput()->GetPointData()->GetArray("qflow magnitudes");
  vtkDataArray *arrayVelocities = reader->GetOutput()->GetPointData()->GetArray("qflow velocities");

  // Note that we load a 3D volume, for which the z direction encodes the slices
  if(arrayAnatomy != NULL)
  {
    a->CopyStructure(reader->GetOutput());    
    a->SetAxisX(reader->GetOutput()->GetAxisX());
    a->SetAxisY(reader->GetOutput()->GetAxisY());
    a->SetAxisZ(reader->GetOutput()->GetAxisZ());
    a->SetPhaseCount(reader->GetOutput()->GetPhaseCount());
    a->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    a->SetPhaseTriggerTime(reader->GetOutput()->GetPhaseTriggerTime());
    a->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    a->SetScanType(reader->GetOutput()->GetScanType());
    a->SetScanOrientation(reader->GetOutput()->GetScanOrientation());

    a->GetPointData()->AddArray(arrayAnatomy);
    a->GetPointData()->SetActiveScalars("qflow anatomy");

    a->SetScalarTypeToUnsignedShort();
    a->Update();
  }
  else cout << "qfeImageDataManager::LoadSeries - Could not load qflow slice data" << endl;

  if(arrayMagnitudes != NULL)
  {
    m->CopyStructure(reader->GetOutput());    
    m->SetAxisX(reader->GetOutput()->GetAxisX());
    m->SetAxisY(reader->GetOutput()->GetAxisY());
    m->SetAxisZ(reader->GetOutput()->GetAxisZ());
    m->SetPhaseCount(reader->GetOutput()->GetPhaseCount());
    m->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    m->SetPhaseTriggerTime(reader->GetOutput()->GetPhaseTriggerTime());
    m->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    m->SetScanType(reader->GetOutput()->GetScanType());
    m->SetScanOrientation(reader->GetOutput()->GetScanOrientation());

    m->GetPointData()->AddArray(arrayMagnitudes);
    m->GetPointData()->SetActiveScalars("qflow magnitudes");

    m->SetScalarTypeToFloat();
    m->Update();
  }
  else cout << "qfeImageDataManager::LoadSeries - Could not load qflow slice data" << endl;

  if(arrayVelocities != NULL)
  {
    v->CopyStructure(reader->GetOutput());    
    v->SetAxisX(reader->GetOutput()->GetAxisX());
    v->SetAxisY(reader->GetOutput()->GetAxisY());
    v->SetAxisZ(reader->GetOutput()->GetAxisZ());
    v->SetPhaseCount(reader->GetOutput()->GetPhaseCount());
    v->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    v->SetPhaseTriggerTime(reader->GetOutput()->GetPhaseTriggerTime());
    v->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    v->SetScanType(reader->GetOutput()->GetScanType());
    v->SetScanOrientation(reader->GetOutput()->GetScanOrientation());

    v->GetPointData()->AddArray(arrayVelocities);
    v->GetPointData()->SetActiveScalars("qflow velocities");

    v->SetScalarTypeToFloat();
    v->Update();
  }
  else cout << "qfeImageDataManager::LoadSeries - Could not load qflow slice data" << endl;

  // Convert to QFE structures
  qfeConvert::vtkImageDataToQfeVolume(a, study->flow2D_ffe.back());
  qfeConvert::vtkImageDataToQfeVolume(m, study->flow2D_pcam.back());
  qfeConvert::vtkImageDataToQfeVolume(v, study->flow2D_pcap.back());

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeReadVTKFlowSim(char *path, qfeStudy *study)
{
  // Read the data at the given path
  qfeVTIImageDataReader *reader = qfeVTIImageDataReader::New();
  reader->SetFileName(path);
  reader->Update();

  //vtkSmartPointer<qfeImageData> m = vtkSmartPointer<qfeImageData>::New();
  vtkSmartPointer<qfeImageData> v = vtkSmartPointer<qfeImageData>::New();

  //vtkDataArray *arrayMagnitudes = reader->GetOutput()->GetPointData()->GetArray("qflow magnitudes");
  vtkDataArray *arrayVelocities = reader->GetOutput()->GetPointData()->GetArray("qflow velocities");

  /*
  if(arrayMagnitudes != NULL)
  {
    study->sim_pressures.push_back(new qfeVolume());

    m->CopyStructure(reader->GetOutput());
    m->SetAxisX(reader->GetOutput()->GetAxisX());
    m->SetAxisY(reader->GetOutput()->GetAxisY());
    m->SetAxisZ(reader->GetOutput()->GetAxisZ());
    m->SetVenc(reader->GetOutput()->GetVenc());
    m->SetPhaseCount(reader->GetOutput()->GetPhaseCount());
    m->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    m->SetPhaseTriggerTime(reader->GetOutput()->GetPhaseTriggerTime());
    m->SetScanType(reader->GetOutput()->GetScanType());
    m->SetScanOrientation(reader->GetOutput()->GetScanOrientation());

    m->GetPointData()->AddArray(arrayMagnitudes);
    m->GetPointData()->SetActiveVectors("qflow magnitudes");

    m->SetScalarTypeToFloat();
    m->Update();
  }
  else cout << "qfeImageDataManager::LoadSeries - Could not load qflow data" << endl;
  */

  if(arrayVelocities != NULL)
  {
    study->sim_velocities.push_back(new qfeVolume());

    v->CopyStructure(reader->GetOutput());
    v->SetAxisX(reader->GetOutput()->GetAxisX());
    v->SetAxisY(reader->GetOutput()->GetAxisY());
    v->SetAxisZ(reader->GetOutput()->GetAxisZ());
    v->SetVenc(reader->GetOutput()->GetVenc());
    v->SetPhaseCount(reader->GetOutput()->GetPhaseCount());
    v->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    v->SetPhaseTriggerTime(reader->GetOutput()->GetPhaseTriggerTime());
    v->SetScanType(reader->GetOutput()->GetScanType());
    v->SetScanOrientation(reader->GetOutput()->GetScanOrientation());

    v->GetPointData()->AddArray(arrayVelocities);
    v->GetPointData()->SetActiveVectors("qflow velocities");

    v->SetScalarTypeToFloat();
    v->Update();
  }
  else cout << "qfeImageDataManager::LoadSeries - Could not load qflow data" << endl;

  // Convert to QFE structures  
  //if(arrayMagnitudes != NULL) qfeConvert::vtkImageDataToQfeVolume(m, study->sim_pressures.back());
  if(arrayVelocities != NULL) qfeConvert::vtkImageDataToQfeVolume(v, study->sim_velocities.back());

  reader->Delete();

  return qfeSuccess;

}


//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeReadVTKSSFP3D(char *path, qfeStudy *study)
{
  char *arrayName = new char[20];

  // Read the data at the given path
  vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
  reader->SetFileName(path);
  reader->Update();

  study->ssfp3D.push_back(new qfeVolume());   
  
  vtkSmartPointer<qfeImageData> a = vtkSmartPointer<qfeImageData>::New();
  
  arrayName = "anatomy";

  vtkDataArray *arrayAnatomy;

  arrayAnatomy = reader->GetOutput()->GetPointData()->GetArray(arrayName);

  // Legacy naming convention
  if(arrayAnatomy == NULL)
  {
    arrayName    = "qflow anatomy";
    arrayAnatomy = reader->GetOutput()->GetPointData()->GetArray(arrayName);
  }
  
  // The data comes from one file, which we separate here into two structures.
  if(arrayAnatomy != NULL)
  {
    a->CopyStructure(reader->GetOutput());    
    a->SetAxisX(reader->GetOutput()->GetAxisX());
    a->SetAxisY(reader->GetOutput()->GetAxisY());
    a->SetAxisZ(reader->GetOutput()->GetAxisZ());
    a->SetPhaseCount(reader->GetOutput()->GetPhaseCount());
    a->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    a->SetPhaseTriggerTime(reader->GetOutput()->GetPhaseTriggerTime());
    a->SetScanType(reader->GetOutput()->GetScanType());
    a->SetScanOrientation(reader->GetOutput()->GetScanOrientation());

    a->GetPointData()->AddArray(arrayAnatomy);
    a->GetPointData()->SetActiveScalars(arrayName);

    a->SetScalarTypeToUnsignedShort();
    a->Update();
  }
  else cout << "qfeImageDataManager::LoadSeries - Could not load anatomy data" << endl;

  // Convert to QFE structures
  qfeConvert::vtkImageDataToQfeVolume(a, study->ssfp3D.back());

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeReadVTKSSFP2D(char *path, qfeStudy *study)
{
  // Read the data at the given path
  vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
  reader->SetFileName(path);
  reader->Update();

  study->ssfp2D.push_back(new qfeVolume());
  study->ssfp2D_phases = reader->GetOutput()->GetPhaseCount();  
  
  vtkSmartPointer<qfeImageData> a = vtkSmartPointer<qfeImageData>::New();
  
  vtkDataArray *arrayAnatomy    = reader->GetOutput()->GetPointData()->GetArray("anatomy");
  
  // The data comes from one file, which we separate here into two structures.
  if(arrayAnatomy != NULL)
  {
    a->CopyStructure(reader->GetOutput());    
    a->SetAxisX(reader->GetOutput()->GetAxisX());
    a->SetAxisY(reader->GetOutput()->GetAxisY());
    a->SetAxisZ(reader->GetOutput()->GetAxisZ());
    a->SetPhaseCount(reader->GetOutput()->GetPhaseCount());
    a->SetPhaseDuration(reader->GetOutput()->GetPhaseDuration());
    a->SetPhaseTriggerTime(reader->GetOutput()->GetPhaseTriggerTime());
    a->SetScanType(reader->GetOutput()->GetScanType());
    a->SetScanOrientation(reader->GetOutput()->GetScanOrientation());

    a->GetPointData()->AddArray(arrayAnatomy);
    a->GetPointData()->SetActiveScalars("anatomy");

    a->SetScalarTypeToUnsignedShort();
    a->Update();
  }
  else cout << "qfeImageDataManager::LoadSeries - Could not load anatomy data" << endl;

  // Convert to QFE structures
  qfeConvert::vtkImageDataToQfeVolume(a, study->ssfp2D.back());

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfePostProcess(char *path, qfeStudy *study)
{  
  int    index;
  char   folder[500];

  // Read the data at the given path
  vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
  reader->SetFileName(path);
  reader->Update();

  int scanType = reader->GetOutput()->GetScanType();

  // Determine folder path
  index = (strrchr(path, '/') - path + 1);
  strncpy(folder, path, index);
  folder[index] = '\0';
    
  switch(scanType)
  {
  case(qfeDataFlow4D) : qfePostProcessFlow4D(folder, study);
                        break;
  case(qfeDataFlow2D) : qfePostProcessFlow2D(study);
                        break;
  case(qfeDataSSFP3D) :
  case(qfeDataSSFP2D) : qfePostProcessAnatomy(study);
                        break;
  default             : cout << "qfeImageDataManager::qfePostProcess - Unknown data type" << endl;
                        return qfeError;
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfePostProcessFlow4D(char *path, qfeStudy *study)
{  
  char fileTMIP[500];
  char fileTMOP[500];
  char fileFTLE[500];
  char fileCurl[500];
  char fileLambda2[500];
  char fileDiv[500];
  char fileQCrit[500];

  qfeImageData *tmipVTK;
  qfeImageData *tmopVTK;
  qfeImageData *ftleVTK;
  qfeImageData *curlVTK;
  qfeImageData *lambda2VTK;
  qfeImageData *divVTK;
  qfeImageData *qCritVTK;

  // TMIP location
  strcpy(fileTMIP, path);
  strcat(fileTMIP, "tmip.vti");

  // TMIP location
  strcpy(fileTMOP, path);
  strcat(fileTMOP, "tmop.vti");

  // FTLE first location
  strcpy(fileFTLE, path);
  strcat(fileFTLE, "FTLE_00.vti");

  // Curl first location
  strcpy(fileCurl, path);
  strcat(fileCurl, "curl_00.vti");

  // Lambda2 first location
  strcpy(fileLambda2, path);
  strcat(fileLambda2, "lambda2_00.vti");

  // Divergence first location
  strcpy(fileLambda2, path);
  strcat(fileLambda2, "lambda2_00.vti");

  // Divergence first location
  strcpy(fileDiv, path);
  strcat(fileDiv, "divergence_00.vti");

  // Q-Criterion first location
  strcpy(fileQCrit, path);
  strcat(fileQCrit, "qcriterion_00.vti");

  // For the flow data, compute some additional data sets
  if(((int)study->pcam.size()>0) ||
     ((int)study->pcap.size()>0))
  {

    // TMIP
    if(!this->qfeFileExists(fileTMIP))
    {
      cout << "No T-MIP data found, please generate." << endl;
    }
    else
    {     
      // Read the tmip       
      vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
      reader->SetFileName(fileTMIP);
      reader->Update();
      
      tmipVTK = reader->GetOutput();
  
      // Convert to QFE format
      if(study->tmip == NULL) study->tmip = new qfeVolume();
      qfeConvert::vtkImageDataToQfeVolume(tmipVTK, study->tmip);
    }    

    // TMOP
    if(!this->qfeFileExists(fileTMOP))
    {
      cout << "No T-MOP data found, please generate." << endl;
    }
    else
    {
      // Read the tmop       
      vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
      reader->SetFileName(fileTMOP);
      reader->Update();
      
      tmopVTK = reader->GetOutput();
  
      // Convert to QFE format
      if(study->tmop == NULL) study->tmop = new qfeVolume();
      qfeConvert::vtkImageDataToQfeVolume(tmopVTK, study->tmop);
    }

    if(!this->qfeFileExists(fileFTLE))
    {
      cout << "No FTLE image data found, please generate." << endl;
    }
    else
    {
      for(int i = 0; i<(int)study->pcap.size(); i++){
        sprintf(fileFTLE,"%sFTLE_%02d.vti",path,i);

        // Read the ftle       
        vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
        reader->SetFileName(fileFTLE);
        reader->Update();

        ftleVTK = reader->GetOutput();

        // Convert to QFE format
        if((int)study->ftle.size() <= i) study->ftle.push_back(new qfeVolume());
        qfeConvert::vtkImageDataToQfeVolume(ftleVTK, study->ftle[i]);
      }
    }

    // Curl
    if(!this->qfeFileExists(fileCurl))
    {
      cout << "No Curl data found, please generate." << endl;
    }
    else
    {     
      for(int i = 0; i<(int)study->pcap.size(); i++){
        sprintf(fileCurl,"%scurl_%02d.vti",path,i);

        // Read the curl       
        vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
        reader->SetFileName(fileCurl);
        reader->Update();

        curlVTK = reader->GetOutput();

        // Convert to QFE format
        if((int)study->curl.size() <= i) study->curl.push_back(new qfeVolume());
        qfeConvert::vtkImageDataToQfeVolume(curlVTK, study->curl[i]);
      }
    }
    // Lambda2
    if(!this->qfeFileExists(fileLambda2))
    {
      cout << "No Lambda2 data found, please generate." << endl;
    }
    else
    {     
      for(int i = 0; i<(int)study->pcap.size(); i++){
        sprintf(fileLambda2,"%slambda2_%02d.vti",path,i);

        // Read the lambda2       
        vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
        reader->SetFileName(fileLambda2);
        reader->Update();

        lambda2VTK = reader->GetOutput();

        // Convert to QFE format
        if((int)study->lambda2.size() <= i) study->lambda2.push_back(new qfeVolume());
        qfeConvert::vtkImageDataToQfeVolume(lambda2VTK, study->lambda2[i]);
      }
    }
    // Divergence
    if(!this->qfeFileExists(fileDiv))
    {
      cout << "No divergence data found, please generate." << endl;
    }
    else
    {     
      for(int i = 0; i<(int)study->pcap.size(); i++){
        sprintf(fileDiv,"%sdivergence_%02d.vti",path,i);

        // Read the divergence       
        vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
        reader->SetFileName(fileDiv);
        reader->Update();

        divVTK = reader->GetOutput();

        // Convert to QFE format
        if((int)study->divergence.size() <= i) study->divergence.push_back(new qfeVolume());
        qfeConvert::vtkImageDataToQfeVolume(divVTK, study->divergence[i]);
      }
    }

    // Q-Criterion
    if(!this->qfeFileExists(fileQCrit))
    {
      cout << "No Q-Criterion data found, please generate." << endl;
    }
    else
    {     
      for(int i = 0; i<(int)study->pcap.size(); i++){
        sprintf(fileQCrit,"%sqcriterion_%02d.vti",path,i);

        // Read the divergence       
        vtkSmartPointer<qfeVTIImageDataReader> reader = vtkSmartPointer<qfeVTIImageDataReader>::New();
        reader->SetFileName(fileQCrit);
        reader->Update();

        qCritVTK = reader->GetOutput();

        // Convert to QFE format
        if((int)study->qcriterion.size() <= i) study->qcriterion.push_back(new qfeVolume());
        qfeConvert::vtkImageDataToQfeVolume(qCritVTK, study->qcriterion[i]);
      }
    }
  }

  // Upload the flow data 
  for(int i=0; i<(int)study->pcam.size(); i++) study->pcam[i]->qfeUploadVolume();
  for(int i=0; i<(int)study->pcap.size(); i++) study->pcap[i]->qfeUploadVolume();
  for(int i=0; i<(int)study->ffe.size(); i++)  study->ffe[i]->qfeUploadVolume();
  for(int i=0; i<(int)study->ftle.size(); i++)  study->ftle[i]->qfeUploadVolume();
  for(int i=0; i<(int)study->curl.size(); i++)  study->curl[i]->qfeUploadVolume();
  for(int i=0; i<(int)study->lambda2.size(); i++) study->lambda2[i]->qfeUploadVolume();
  for(int i=0; i<(int)study->qcriterion.size(); i++) study->qcriterion[i]->qfeUploadVolume();
  
  if(study->tmip != NULL)
    study->tmip->qfeUploadVolume();  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfePostProcessFlow2D(qfeStudy *study)
{
  for(int i=0; i<(int)study->flow2D_ffe.size(); i++)  study->flow2D_ffe[i]->qfeUploadVolume(); 
  for(int i=0; i<(int)study->flow2D_pcam.size(); i++) study->flow2D_pcam[i]->qfeUploadVolume(); 
  for(int i=0; i<(int)study->flow2D_pcap.size(); i++) study->flow2D_pcap[i]->qfeUploadVolume(); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfePostProcessAnatomy(qfeStudy *study)
{
  for(int i=0; i<(int)study->ssfp3D.size(); i++) study->ssfp3D[i]->qfeUploadVolume(); 
  for(int i=0; i<(int)study->ssfp2D.size(); i++) study->ssfp2D[i]->qfeUploadVolume(); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeWriteTemporalMIP(qfeStudy *study)
{
  char fileTMIP[500];

  if(study == NULL) return qfeError;

  // TMIP location
  strcpy(fileTMIP, this->dataPath.c_str());
  strcat(fileTMIP, "tmip.vti");

  qfeImageData *tmipVTK;

  // Compute a temporal MIP
  this->qfeComputeTemporalMIP(study);

  if(study->tmip != NULL)
  {
    // Convert to VTK format
    tmipVTK = qfeImageData::New();
    qfeConvert::qfeVolumeToVtkImageData(study->tmip, tmipVTK);

    // Write the temporal MIP
    qfeVTIImageDataWriter *writer = qfeVTIImageDataWriter::New();
    writer->SetInput(tmipVTK);
    writer->SetFileName(fileTMIP);
    writer->SetDataModeToBinary();
    writer->Write(); 
    writer->Delete();

    // Clean up
    tmipVTK->Delete();
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeWriteTemporalMAP(qfeStudy *study)
{
  char fileTMAP[500];

  if(study == NULL) return qfeError;

  // TMIP location
  strcpy(fileTMAP, this->dataPath.c_str());
  strcat(fileTMAP, "tmap.vti");

  qfeImageData *tmapVTK;

  // Compute a temporal MIP
  this->qfeComputeTemporalMAP(study);

  if(study->tmap != NULL)
  {
    // Convert to VTK format
    tmapVTK = qfeImageData::New();
    qfeConvert::qfeVolumeToVtkImageData(study->tmap, tmapVTK);

    // Write the temporal MIP
    qfeVTIImageDataWriter *writer = qfeVTIImageDataWriter::New();
    writer->SetInput(tmapVTK);
    writer->SetFileName(fileTMAP);
    writer->SetDataModeToBinary();
    writer->Write(); 
    writer->Delete();

    // Clean up
    tmapVTK->Delete();
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeWriteTemporalMOP(qfeStudy *study)
{
  char fileTMOP[500];

  if(study == NULL) return qfeError;

  // TMOP location  
  strcpy(fileTMOP, this->dataPath.c_str());
  strcat(fileTMOP, "tmop.vti");

  qfeImageData *tmopVTK;

  // Compute a temporal MOP
  this->qfeComputeTemporalMOP(study);

  if(study->tmop != NULL)
  {
    // Convert to VTK format
    tmopVTK = qfeImageData::New();
    qfeConvert::qfeVolumeToVtkImageData(study->tmop, tmopVTK);

    // Write the temporal MOP
    qfeVTIImageDataWriter *writer = qfeVTIImageDataWriter::New();
    writer->SetInput(tmopVTK);
    writer->SetFileName(fileTMOP);
    writer->SetDataModeToBinary();
    writer->Write(); 
    writer->Delete();

    // Clean up
    tmopVTK->Delete();
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeWriteGaussianNoise(qfeStudy *study, double stDev, unsigned int count)
{
  char fileNoise[500];

  if(study == NULL) return qfeError;


  qfeVolumeSeries *flowData = NULL;   
  qfeVolume       *flowNoise;

  //char             message[200];
 
  qfeImageData *noiseVTK;
  

  // Initialize empty volume
  flowNoise = new qfeVolume();
  flowData   = &study->pcap;

  // Add noise to individual volumes
  for(int i=0; i<(int)(*flowData).size(); i++)
  {
    for(unsigned int n=0; n<count; n++)
    {
      // Add noise
      this->qfeAddGaussianNoise((*flowData)[i], &flowNoise, stDev);

      // Noisy data location  
      sprintf(fileNoise,"%sqflow_%02d_gauss_%6f_%1d.vti",this->dataPath.c_str(),i,stDev,n);

      // Convert to VTK format
      noiseVTK = qfeImageData::New();
      qfeConvert::qfeVolumeToVtkImageData(flowNoise, noiseVTK);

      noiseVTK->GetPointData()->GetVectors("converted")->SetName("qflow velocities");
      noiseVTK->GetPointData()->SetActiveVectors("qflow velocities");
      noiseVTK->GetPointData()->SetActiveScalars("");
      noiseVTK->GetPointData()->Update();
      noiseVTK->SetScanType(qfeDataSSFP3D);
      noiseVTK->Update();

      // Write the filtered data
      qfeVTIImageDataWriter *writer = qfeVTIImageDataWriter::New();
      writer->SetInput(noiseVTK);
      writer->SetFileName(fileNoise);
      writer->SetDataModeToBinary();
      writer->Write(); 
      writer->Delete();

      // Clean up
      noiseVTK->Delete();
    }
  }
  this->InvokeEvent(vtkCommand::EndEvent, &this->callData);

  delete flowNoise;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeWriteMedianFilter(qfeStudy *study)
{
  char fileMedian[500];

  if(study == NULL) return qfeError;


  qfeVolumeSeries *flowData = NULL;   
  qfeVolume       *flowMedian;

  char             message[200];
 
  qfeImageData *medianVTK;
  

  // Initialize filtered volume
  flowMedian = new qfeVolume();
  flowData   = &study->pcap;  

  // Compute tensor data for subsequent phases and average
  for(int i=0; i<(int)(*flowData).size(); i++)
  {
    // Progress bar text
    sprintf(message, "Applying median filter (step %d of %d) ...", i+1, (*flowData).size()); 
    this->callData.message  = message;
    this->callData.stepSize = 2;

    this->qfeFilterMedian((*flowData)[i], &flowMedian);

    // Median filtered location  
    sprintf(fileMedian,"%smedian_%02d.vti",this->dataPath.c_str(),i);
    flowMedian->qfeSetVolumeLabel("qflow velocities");

    // Convert to VTK format
    medianVTK = qfeImageData::New();
    qfeConvert::qfeVolumeToVtkImageData(flowMedian, medianVTK);

    // Write the filtered data
    qfeVTIImageDataWriter *writer = qfeVTIImageDataWriter::New();
    writer->SetInput(medianVTK);
    writer->SetFileName(fileMedian);
    writer->SetDataModeToBinary();
    writer->Write(); 
    writer->Delete();

    // Clean up
    medianVTK->Delete();
  }
  this->InvokeEvent(vtkCommand::EndEvent, &this->callData);

  delete flowMedian;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeWriteFTLE(qfeStudy *study, qfeVector sampleDim, float intTime, float intStepSize)
{
  char fileFTLE[500];

  if(study == NULL) return qfeError;


  qfeVolumeSeries *flowData = NULL;   
  qfeVolume       *flowFTLE;

  char             message[200];

  qfeImageData *FTLEVTK;


  // Initialize FTLE volume
  flowFTLE = new qfeVolume();
  flowData   = &study->pcap;  

  // Compute FTLE for subsequent phases
  for(int i=0; i<(int)(*flowData).size(); i++)
  {
    // Progress bar text
    sprintf(message, "Calculating FTLE volume (step %d of %d) ...", i+1, (*flowData).size()); 
    this->callData.message  = message;
    this->callData.stepSize = 2;

    // FTLE image location  
    sprintf(fileFTLE,"%sFTLE_%02d.vti",this->dataPath.c_str(),i);

    this->qfeComputeFTLE(flowData, flowFTLE, i, sampleDim,intTime, intStepSize);

    // Convert to VTK format
    FTLEVTK = qfeImageData::New();
    qfeConvert::qfeVolumeToVtkImageData(flowFTLE, FTLEVTK);

    // Write the filtered data
    qfeVTIImageDataWriter *writer = qfeVTIImageDataWriter::New();
    writer->SetInput(FTLEVTK);
    writer->SetFileName(fileFTLE);
    writer->SetDataModeToBinary();
    writer->Write(); 
    writer->Delete();

    // Clean up
    FTLEVTK->Delete();
  }
  this->InvokeEvent(vtkCommand::EndEvent, &this->callData);

  delete flowFTLE;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeWriteSynthFlow(qfeStudy *study, qfeVector size)
{
  char fileSynth[500];

  if(study == NULL) return qfeError;
 
  qfeVolume       *flowSynth;

  char             message[200];

  qfeImageData *SYNTHVTK;

  // Initialize synthetic flow volume
  flowSynth = new qfeVolume();
   
  // Compute synthetic flow dataset for subsequent phases
  for(int i=0; i<size.w; i++)
  {
    // Progress bar text
    sprintf(message, "Calculating Synthetic flow volume (step %d of %d) ...", i+1, size.w); 
    this->callData.message  = message;
    this->callData.stepSize = 2;

    // FTLE image location  
    sprintf(fileSynth,"%sSYNTH_%02d.vti",this->dataPath.c_str(),i);

    this->qfeComputeSynthFlow(study, flowSynth, size);

    // Convert to VTK format
    SYNTHVTK = qfeImageData::New();
    qfeConvert::qfeVolumeToVtkImageData(flowSynth, SYNTHVTK);

    // Write the filtered data
    qfeVTIImageDataWriter *writer = qfeVTIImageDataWriter::New();
    writer->SetInput(SYNTHVTK);
    writer->SetFileName(fileSynth);
    writer->SetDataModeToBinary();
    writer->Write(); 
    writer->Delete();

    // Clean up
    SYNTHVTK->Delete();
  }
  this->InvokeEvent(vtkCommand::EndEvent, &this->callData);

  delete flowSynth;

  return qfeSuccess;
}

qfeReturnStatus qfeImageDataManager::qfeWriteVolumeSeries(qfeVolumeSeries *series, const std::string &name)
{
  char fileName[500];
  char message[200];
  qfeImageData *volVTK;

  for(int i=0; i<(int)series->size(); i++)
  {
    // Progress bar text
    sprintf(message, "Writing volume series (step %d of %d) ...", i+1, series->size()); 
    callData.message  = message;
    callData.stepSize = 2;

    // File path 
    sprintf(fileName, "%s%s_%02d.vti", dataPath.c_str(), name.c_str(), i);

    // Convert to VTK format
    volVTK = qfeImageData::New();
    qfeConvert::qfeVolumeToVtkImageData(series->at(i), volVTK);

    // Write the filtered data
    qfeVTIImageDataWriter *writer = qfeVTIImageDataWriter::New();
    writer->SetInput(volVTK);
    writer->SetFileName(fileName);
    writer->SetDataModeToBinary();
    writer->Write(); 
    writer->Delete();

    // Clean up
    volVTK->Delete();
  }
  this->InvokeEvent(vtkCommand::EndEvent, &callData);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeWriteClusterHierarchy(qfeStudy *study)
{
  if(study == NULL || study->hierarchy == NULL) return qfeError;

  // Cluster hierarchy file location
  char clusterFile[500];
  
  time_t secs=time(0);
  tm *t=localtime(&secs);
  sprintf(clusterFile,"%sclusters_%04d_%02d_%02d_%02d%02d%02d.dat",this->dataPath.c_str(),\
    t->tm_year+1900,t->tm_mon,t->tm_mday,t->tm_hour,t->tm_min,t->tm_sec);

  // Export cluster hierarchy
  study->hierarchy->qfeExportClusterData(clusterFile);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeWriteFluidSimulation(qfeStudy *study)
{
  if(study == NULL) return qfeError;

  // TODO NDH write preprocessed fluid sim to file
  cout << "TODO NDH: qfeImageDataManager::qfeWriteFluidSimulation" << endl;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeComputeTemporalMIP(qfeStudy *study)
{
  int pcam_size, pcap_size, phases;
  int u;

  pcam_size = study->pcam.size();
  pcap_size = study->pcap.size();

  phases    = study->phases;

  u = 0.0;

  qfeVolumeSeries *flowData = NULL;  
 
  if(phases > 1)
  {
    qfeCallData cd;
    qfeFloat range[2];
	  unsigned short *dataTMIP;
    unsigned int nx, ny, nz;
    qfeGrid *grid;  

    if((pcam_size==0) && (pcap_size==0)) return qfeError;

    study->qfeGetStudyRangePCAM(range[0], range[1]);

    // Check for the available data
    if(pcap_size > 0) 
    {
      flowData = &study->pcap;  
      study->qfeGetStudyRangePCAP(range[0], range[1]);
    }
    else {
      flowData = &study->pcam;    
    }

    // We have multiple phases in the data set
    // PCA-M is preferred, otherwise we 
    // take the norm of the PCA-P series    
    (*flowData)[0]->qfeGetVolumeSize(nx, ny, nz);
    (*flowData)[0]->qfeGetVolumeGrid(&grid);

    dataTMIP = (unsigned short*)calloc(nx*ny*nz,sizeof(unsigned short));    

    for(int dim=0; dim<(int)(nx*ny*nz); dim++)
    {
      float valueMax     = 0.0;
      float valueCurrent = 0.0;
      for(int t=0; t<(int)phases; t++)
      {
        float        valueX, valueY, valueZ;
        unsigned int dummyN;
        qfeValueType type;
        void        *data;

        (*flowData)[t]->qfeGetVolumeData(type, dummyN, dummyN, dummyN, &data);

        // \todo other data types supported here
        if(type != qfeValueTypeFloat)
        {
          cout << "qfeImageDataManager::qfeComputeTemporalMIP - Only float type supported" << endl;
          return qfeError;
        }

        valueX       = *((float*)data + 3*dim+0);
        valueY       = *((float*)data + 3*dim+1);
        valueZ       = *((float*)data + 3*dim+2);
        valueCurrent = sqrt(pow(valueX,2)+pow(valueY,2)+pow(valueZ,2.0f))/sqrt(3.0);
        
        if(valueCurrent > valueMax) valueMax = valueCurrent;
      }  
      *(dataTMIP + dim) = (unsigned short)((valueMax/(float)max(abs(range[0]), abs(range[1])))*pow(2.0f,16.0f));

      if(*(dataTMIP + dim) > u) u = *(dataTMIP + dim);

      // Update the progress bar
      if(dim % (int)(0.05*nx*ny*nz) == 0)
      {
        cd.stepSize = 5;
        cd.message  = "Computing temporal mip...";
        this->InvokeEvent(vtkCommand::UpdateEvent, &cd);        
      }
    }

    if(study->tmip == NULL) study->tmip = new qfeVolume();
    study->tmip->qfeSetVolumeData(qfeValueTypeInt16, nx, ny, nz, 1, dataTMIP);
    study->tmip->qfeSetVolumeGrid(new qfeGrid(*grid));   
    study->tmip->qfeSetVolumeValueDomain(0.0, u);
    study->tmip->qfeSetVolumeLabel("converted");
    // Finalize the progress bar
    cd.stepSize = -1;
    this->InvokeEvent(vtkCommand::UpdateEvent, &cd);
    this->InvokeEvent(vtkCommand::UpdateInformationEvent, &cd);

    free(dataTMIP);
  }
  else
  {
    this->patient->study->tmip = NULL;
    return qfeError;
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeComputeTemporalMAP(qfeStudy *study)
{
  int pcam_size, pcap_size, phases;
  int u;

  pcam_size = study->pcam.size();
  pcap_size = study->pcap.size();

  phases    = study->phases;

  u = 0.0;

  qfeVolumeSeries *pcamData = NULL;  
  qfeVolumeSeries *pcapData = NULL;  
 
  if(phases > 1)
  {
    qfeCallData cd;
    qfeFloat pcamRange[2], pcapRange[2];
	  unsigned short *dataTMAP;
    unsigned int nx, ny, nz;
    qfeGrid *grid;  

    // This projection requires both pca-m and pca-p to be available
    if((pcam_size==0) || (pcap_size==0)) return qfeError;

    // Check for the available data
    if(pcam_size > 0) 
    {
      pcamData = &study->pcam;    
      study->qfeGetStudyRangePCAM(pcamRange[0], pcamRange[1]);
    }
    if(pcap_size > 0) 
    {
      pcapData = &study->pcap;  
      study->qfeGetStudyRangePCAP(pcapRange[0], pcapRange[1]);
    }

    // Build the volume
    (*pcapData)[0]->qfeGetVolumeSize(nx, ny, nz);
    (*pcapData)[0]->qfeGetVolumeGrid(&grid);

    dataTMAP = (unsigned short*)calloc(nx*ny*nz,sizeof(unsigned short));    

    for(int dim=0; dim<(int)(nx*ny*nz); dim++)
    {
      float valueCurrent = 0.0;

      for(int t=0; t<(int)phases; t++)
      {        
        unsigned int dummyN;
        qfeValueType dataType;
        void        *pcamBuffer, *pcapBuffer;
        float        pcam, pcamValue[3], pcapValue[3];

        (*pcamData)[t]->qfeGetVolumeData(dataType, dummyN, dummyN, dummyN, &pcamBuffer);
        (*pcapData)[t]->qfeGetVolumeData(dataType, dummyN, dummyN, dummyN, &pcapBuffer);

        // \todo other data types supported here
        if(dataType != qfeValueTypeFloat)
        {
          cout << "qfeImageDataManager::qfeComputeTemporalMAP - Only float type supported" << endl;
          return qfeError;
        }

        pcamValue[0] = *((float*)pcamBuffer + 3*dim+0);
        pcamValue[1] = *((float*)pcamBuffer + 3*dim+1);
        pcamValue[2] = *((float*)pcamBuffer + 3*dim+2);        

        pcapValue[0] = *((float*)pcapBuffer + 3*dim+0);
        pcapValue[1] = *((float*)pcapBuffer + 3*dim+1);
        pcapValue[2] = *((float*)pcapBuffer + 3*dim+2);

        pcam = sqrt(pow(pcamValue[0],2)+pow(pcamValue[1],2)+pow(pcamValue[2],2)) / (pcamRange[1] - pcamRange[0]);

        valueCurrent += pow(pcam,2) * (pow(pcapValue[0],2)+pow(pcapValue[1],2)+pow(pcapValue[2],2.0f));        
      }  

      valueCurrent = sqrt(1.0/phases * valueCurrent) / (pcapRange[1] - pcapRange[0]);
      *(dataTMAP + dim) = (unsigned short)(valueCurrent * pow(2.0f,16.0f));

      // Determine the upper limit of the range u
      if(*(dataTMAP + dim) > u) u = *(dataTMAP + dim);

      // Update the progress bar
      if(dim % (int)(0.05*nx*ny*nz) == 0)
      {
        cd.stepSize = 5;
        cd.message  = "Computing temporal map...";
        this->InvokeEvent(vtkCommand::UpdateEvent, &cd);        
      }
    }

    if(study->tmap == NULL) study->tmap = new qfeVolume();
    study->tmap->qfeSetVolumeData(qfeValueTypeInt16, nx, ny, nz, 1, dataTMAP);
    study->tmap->qfeSetVolumeGrid(new qfeGrid(*grid));   
    study->tmap->qfeSetVolumeValueDomain(0.0, u);

    // Finalize the progress bar
    cd.stepSize = -1;
    this->InvokeEvent(vtkCommand::UpdateEvent, &cd);
    this->InvokeEvent(vtkCommand::UpdateInformationEvent, &cd);

    free(dataTMAP);
  }
  else
  {
    this->patient->study->tmap = NULL;
    return qfeError;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeComputeTemporalMOP(qfeStudy *study)
{
  qfeVolumeSeries *flowData = NULL;   
  qfeVolume       *flowMedian;
  qfeGrid         *grid;  
  float           *dataTMOP;
  void            *dataMedian;
  unsigned int     nx, ny, nz, nd;
  qfeValueType     t;
  int              pcap_size, phases, u;  
  char             message[200];

  const int        TENSOR_CC = 9;
  const int        VECTOR_CC = 3;
  
  pcap_size = study->pcap.size();
  phases    = study->phases;
   
  if((pcap_size==0) || (phases <= 1))
  {
    this->patient->study->tmop = NULL;
    return qfeError;
  }  

  // Initialize
  u          = 0.0;
  flowMedian = new qfeVolume();
  flowData   = &study->pcap;  
  (*flowData)[0]->qfeGetVolumeSize(nx, ny, nz);
  (*flowData)[0]->qfeGetVolumeGrid(&grid);

  // Allocate memory for tmop data
  dataTMOP = (float*)calloc(TENSOR_CC*nx*ny*nz,sizeof(float));   

  // Enable for DTI tool use
  //qfeMatrix4f P2V;
  //qfeTransform::qfeGetMatrixPatientToVoxel(P2V, (*flowData)[0]);

  // Compute tensor data from first phase
  this->qfeInitializeTensorBuffer((*flowData)[0], &dataTMOP);

  // Compute tensor data for subsequent phases and average
  for(int i=0; i<(int)(*flowData).size(); i++)
  {
    // Progress bar text
    sprintf(message, "Computing temporal mop (step %d of %d) ...", i+1, (*flowData).size()); 
    this->callData.message  = message;
    this->callData.stepSize = 1;

    this->qfeFilterMedian((*flowData)[i], &flowMedian);

    flowMedian->qfeGetVolumeData(t, nd, nd, nd, &dataMedian);
    //(*flowData)[i]->qfeGetVolumeData(t, nd, nd, nd, &dataMedian);

    int p = 0;
    for(int z=0; z<(int)nz; z++)
    {      
      for(int y=0; y<(int)ny; y++)
      {
        for(int x=0; x<(int)nx; x++)
        {
          qfeVector    v;   
          qfeTensor3f  t;    
          int          rwi;
          float        fraction;

          // Determine read and write index
          rwi = x  + (y * nx) + (z *nx*ny);
          
          // Get the velocity vector
          v.x = *((float*)dataMedian + VECTOR_CC*rwi+0);
          v.y = *((float*)dataMedian + VECTOR_CC*rwi+1);
          v.z = *((float*)dataMedian + VECTOR_CC*rwi+2);

          //v = P2V*v;
          
          // Compute structure tensor
          qfeVector::qfeVectorTensorProduct(v, v, t);

          // Add part of the average
          fraction = 1.0f/(float)(*flowData).size();
          *((float*)dataTMOP + TENSOR_CC*rwi + 0) += fraction * t(0,0);
          *((float*)dataTMOP + TENSOR_CC*rwi + 1) += fraction * t(0,1);
          *((float*)dataTMOP + TENSOR_CC*rwi + 2) += fraction * t(0,2);
          *((float*)dataTMOP + TENSOR_CC*rwi + 3) += fraction * t(1,0);
          *((float*)dataTMOP + TENSOR_CC*rwi + 4) += fraction * t(1,1);
          *((float*)dataTMOP + TENSOR_CC*rwi + 5) += fraction * t(1,2);
          *((float*)dataTMOP + TENSOR_CC*rwi + 6) += fraction * t(2,0);
          *((float*)dataTMOP + TENSOR_CC*rwi + 7) += fraction * t(2,1);
          *((float*)dataTMOP + TENSOR_CC*rwi + 8) += fraction * t(2,2);
          
          // Update the progress bar
          p++;
          if(p % (int)(0.02*nx*ny*nz) == 0)
          {        
            this->InvokeEvent(vtkCommand::UpdateEvent, &this->callData);
          }          
        }
      }
    }    
  }

  // Create tmop volume
  if(study->tmop == NULL) study->tmop = new qfeVolume();
  study->tmop->qfeSetVolumeData(qfeValueTypeFloat, nx, ny, nz, TENSOR_CC, dataTMOP);
  study->tmop->qfeSetVolumeGrid(new qfeGrid(*grid));   
  study->tmop->qfeSetVolumeValueDomain(0.0, u);

  // Finalize the progress bar
  this->callData.stepSize = -1;
  this->InvokeEvent(vtkCommand::UpdateEvent, &this->callData);
  this->InvokeEvent(vtkCommand::UpdateInformationEvent, &this->callData);
  this->callData.stepSize = 1;

  delete flowMedian;
  free(dataTMOP);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeComputeFTLE(qfeVolumeSeries *in, qfeVolume *FTLE, int t, qfeVector sampleDim, float intTime, float intStepSize)
{
  unsigned int dims[3];
  qfeValueType type;
  void        *voxels;  
  float       *dataFTLE;
  float       uBound = 0;
  qfeGrid     *grid;
  qfeMatrix4f FTLE2P, P2IN;
  qfeFloat    phaseDuration;
  qfeFlowLines flowLinesAlgorithm;
  qfeFloat    ex;
  qfeFloat    ey;
  qfeFloat    ez;

  in->front()->qfeGetVolumeData(type, dims[0], dims[1], dims[2], &voxels);
  in->front()->qfeGetVolumeGrid(&grid);
  in->front()->qfeGetVolumePhaseDuration(phaseDuration);

  // Allocate the memory
  dataFTLE = (float*)calloc(sampleDim.x*sampleDim.y*sampleDim.z, sizeof(qfeFloat));

  FTLE->qfeSetVolumeComponentsPerVoxel(1);
  FTLE->qfeSetVolumeData(type, sampleDim.x, sampleDim.y, sampleDim.z, dataFTLE);  
  grid->qfeGetGridExtent(ex,ey,ez);
  qfeGrid* ftleGrid = new qfeGrid(*grid);
  ftleGrid->qfeSetGridExtent((dims[0]/sampleDim[0])*ex,(dims[1]/sampleDim[1])*ey,(dims[2]/sampleDim[2])*ez);
  FTLE->qfeSetVolumeGrid(ftleGrid);  

  qfeTransform::qfeGetMatrixVoxelToPatient(FTLE2P, FTLE);
  qfeTransform::qfeGetMatrixPatientToVoxel(P2IN, in->front());

  //Flowmap calculation
  vtkSmartPointer<vtkImageData> flowMap = vtkSmartPointer<vtkImageData>::New();
  flowMap->SetExtent(0,sampleDim.x-1,0,sampleDim.y-1,0,sampleDim.z-1);
  flowMap->SetNumberOfScalarComponents(3);
  flowMap->SetScalarTypeToFloat();
  flowMap->AllocateScalars();

  qfePoint p;  
  cout << "Calculating flow map" << endl;
  for(p.z = 0; p.z<sampleDim.z; p.z++){
    for(p.y = 0; p.y<sampleDim.y; p.y++){
      for(p.x = 0; p.x<sampleDim.x; p.x++){
        float* flowMapPoint = static_cast<float*>(flowMap->GetScalarPointer(p.x,p.y,p.z));
        qfePoint velocityCoords = p * FTLE2P * P2IN;

        if(velocityCoords.x < 0 || velocityCoords.y < 0 || velocityCoords.z < 0){
          velocityCoords.x = 0;
          velocityCoords.y = 0;
          velocityCoords.z = 0;
        }

        if(velocityCoords.x > dims[0] || velocityCoords.y > dims[1] || velocityCoords.z > dims[2]){
          velocityCoords.x = dims[0];
          velocityCoords.y = dims[1];
          velocityCoords.z = dims[2];
        }

        vector<qfePoint> pathline;     
        if(flowLinesAlgorithm.qfeComputePathline(velocityCoords, t, *in, intStepSize/phaseDuration, intTime/intStepSize, phaseDuration, pathline)) {
        // Place the vector in our new dataset    
        flowMapPoint[0] = pathline.back().x;
        flowMapPoint[1] = pathline.back().y;
        flowMapPoint[2] = pathline.back().z;
        }
      }
    }
  }
  //Gradient calculation
  vtkSmartPointer<vtkGradientFilter> gradientFilter = vtkSmartPointer<vtkGradientFilter>::New();
  gradientFilter->SetInput(flowMap);
  gradientFilter->Update();
  vtkSmartPointer<vtkDataArray> gradientImg = gradientFilter->GetOutput()->GetPointData()->GetArray("Gradients");

  //Cauchy Green Tensor and its maximum eigenvalue calculation (Final FTLE calculation stage)
  cout << "Calculating CGT and FTLE" << endl;
  for(p.z = 0; p.z<sampleDim.z; p.z++){
    for(p.y = 0; p.y<sampleDim.y; p.y++){
      for(p.x = 0; p.x<sampleDim.x; p.x++){

        //Cauchy Green Tensor calculation
        double gradientPointer[9]; 
        gradientImg->GetTuple(p.x+sampleDim.x*p.y+sampleDim.x*sampleDim.y*p.z, gradientPointer);
        qfeTensor3f cauchyGreenTensor;
        qfeTensor3f gradient;
        qfeTensor3f gradientTranspose;
        gradient(0,0) = (float) gradientPointer[0]; gradient(0,1) = (float) gradientPointer[1]; gradient(0,2) = (float) gradientPointer[2];
        gradient(1,0) = (float) gradientPointer[3]; gradient(1,1) = (float) gradientPointer[4]; gradient(1,2) = (float) gradientPointer[5]; 
        gradient(2,0) = (float) gradientPointer[6]; gradient(2,1) = (float) gradientPointer[7]; gradient(2,2) = (float) gradientPointer[8]; 
        
        qfeTensor3f::qfeGetTensorTranspose(gradient, gradientTranspose);
        cauchyGreenTensor = gradientTranspose * gradient;

        //Final FTLE calculation (max eigen + normalization)
        qfeVector eigenVals;
        qfeMath::qfeComputeEigenValues(cauchyGreenTensor, eigenVals);

        unsigned int writeIndex;
        //needs to be in last calculation
        writeIndex = p.x + (p.y *sampleDim[0]) + (p.z *sampleDim[0]*sampleDim[1]);
        float* wrtPtr = ((float*)dataFTLE + writeIndex);
        *wrtPtr = (1/intTime)*(std::log(std::sqrt(eigenVals.x)));
        if(*wrtPtr > uBound) uBound = *wrtPtr;
      }
    }
  }

  FTLE->qfeSetVolumeData(type, sampleDim.x, sampleDim.y, sampleDim.z, dataFTLE);    
  FTLE->qfeSetVolumeValueDomain(0.0, uBound);
  FTLE->qfeSetVolumeLabel("qflow FTLE");
  // Clean up
  free(dataFTLE);

  return qfeSuccess;
}

qfeReturnStatus qfeImageDataManager::qfeComputeSynthFlow(qfeStudy *study, qfeVolume *synthFlow, qfeVector size)
{
  qfeValueType type = qfeValueTypeFloat; 
  float       *dataSynth;
  float       uBound = 0;
  qfeGrid     *grid = new qfeGrid();

  // Allocate the memory
  dataSynth = (float*)calloc(size.x*size.y*size.z, 3*sizeof(qfeFloat));

  qfePoint p;
  for(p.z = 0; p.z < size.z; p.z++){
    for(p.y = 0; p.y < size.y; p.y++){
      for(p.x = 0; p.x < size.x; p.x++){
        unsigned int writeIndex = p.x + (p.y *size.x) + (p.z *size.x*size.y);

        //-------Double Gyre
        float cx = (p.x/size.x)*2;
        float cy = (p.y/size.y)*1;
        float cz = (p.z/size.z)*2;
        *((float*)dataSynth + 3*writeIndex+0) = (-PI*sin(PI*cx)*cos(PI*cy))*size.x/2;
        *((float*)dataSynth + 3*writeIndex+1) = (PI*cos(PI*cx)*sin(PI*cy))*size.y/1;
        *((float*)dataSynth + 3*writeIndex+2) = 0;

        //-------fake linear bifurcation
        /*if(p.x < size.x/3 && p.y < size.y/3 && p.z < size.z/3){ 
          *((float*)dataSynth + 3*writeIndex+0) = 10;
          *((float*)dataSynth + 3*writeIndex+1) = 10;
          *((float*)dataSynth + 3*writeIndex+2) = 10;
        }
        else{
          *((float*)dataSynth + 3*writeIndex+0) = 10 + (p.x-(size.x/3))*0.25 - (p.y-(size.y/3))*0.25 - (p.z-(size.z/3))*0.25;
          *((float*)dataSynth + 3*writeIndex+1) = 10 - (p.x-(size.x/3))*0.25 + (p.y-(size.y/3))*0.25 - (p.z-(size.z/3))*0.25;
          *((float*)dataSynth + 3*writeIndex+2) = 10 - (p.x-(size.x/3))*0.25 - (p.y-(size.y/3))*0.25 + (p.z-(size.z/3))*0.25;
        }*/

        if(*((float*)dataSynth + 3*writeIndex+0) > uBound) uBound = *((float*)dataSynth + 3*writeIndex+0);
        if(*((float*)dataSynth + 3*writeIndex+1) > uBound) uBound = *((float*)dataSynth + 3*writeIndex+1);
        if(*((float*)dataSynth + 3*writeIndex+2) > uBound) uBound = *((float*)dataSynth + 3*writeIndex+2);
      }
    }    
  }

  grid->qfeSetGridExtent(1,1,1);
  synthFlow->qfeSetVolumeComponentsPerVoxel(3);
  synthFlow->qfeSetVolumeData(type, size.x, size.y, size.z, dataSynth);   
  synthFlow->qfeSetVolumeGrid(grid);  
  synthFlow->qfeSetVolumeValueDomain(0.0, uBound);
  synthFlow->qfeSetVolumeLabel("qflow velocities");
  synthFlow->qfeSetVolumePhaseDuration(1000);

  // Clean up
  free(dataSynth);

  return qfeSuccess;
}
//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeComputeTriangleStrips(vtkPolyData *mesh)
{  
  //vtkTriangleFilter *triangleFilter = vtkTriangleFilter::New();
  //triangleFilter->SetInput(mesh);
  //triangleFilter->Update();

  vtkStripper *stripper = vtkStripper::New();
  stripper->SetInput(mesh); 
  stripper->Update();

  mesh->SetStrips(stripper->GetOutput()->GetStrips());

  stripper->Delete();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeComputeNormals(vtkPolyData *mesh)
{  
  vtkPolyDataNormals *normals = vtkPolyDataNormals::New();
  normals->SetInput(mesh);
  normals->SetFeatureAngle(80.0);
  normals->ConsistencyOn();
  normals->AutoOrientNormalsOff();
  normals->ComputePointNormalsOn();
  normals->SplittingOff();
  normals->FlipNormalsOff();
  normals->Update();

  mesh->GetPointData()->SetNormals(normals->GetOutput()->GetPointData()->GetNormals());

  normals->Delete();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeComputeCurvature(vtkPolyData *mesh)
{ 
  //cout << mesh->GetNumberOfPoints() << endl;
  // Does not seem to work for triangle strips
  //vtkCurvatures *curvGauss = vtkCurvatures::New();
  //curvGauss->SetInput(mesh);
  //curvGauss->SetCurvatureTypeToGaussian();
  //curvGauss->Update();

  //vtkCurvatures *curvMax = vtkCurvatures::New();
  //curvMax->SetInput(curvGauss->GetOutput());
  //curvMax->SetCurvatureTypeToMaximum();
  //curvMax->Update();

  //vtkCurvatures *curvMin = vtkCurvatures::New();
  //curvMin->SetInput(curvMax->GetOutput());
  //curvMin->SetCurvatureTypeToMinimum();
  //curvMin->Update();

  //curvMin->GetOutput()->Print(cerr);
  
  double        TOL;
  int           numPts;
  vtkDataArray *normals;
  
  if (mesh->GetNumberOfPoints()==0)  
    return qfeError;  

  TOL     = 0.01f; // will depend on the quality of vtkMath::Jacobi;
  normals = mesh->GetPointData()->GetNormals();
  numPts  = mesh->GetNumberOfPoints();

  //vtkIdList *vertices, *vertices_n
  vtkIdList *neighbours;

  // allocate
  neighbours = vtkIdList::New();

  // vertices, tangents(edges) and normals
  double v_i[3], v_j[3], v_k[3], n_i[3], e_ij[3], en, T_ij[3];
  double T_1[3], T_2[3];
  double** M_i, *pc;  
  double   S_i[3][3];
  double** T_i; // the Taubin tensor
  M_i    = new double*[3];
    
  T_i    = new double*[3];  
  M_i[0] = new double[3]; M_i[1] = new double[3]; M_i[2] = new double[3];
  T_i[0] = new double[3]; T_i[1] = new double[3]; T_i[2] = new double[3];  
  pc     = new double[3];

  // Data arrays
  vtkDoubleArray* curvatureTensor = vtkDoubleArray::New();
  curvatureTensor->SetName("Curvature_Tensor");
  curvatureTensor->SetNumberOfComponents(9);
  curvatureTensor->SetNumberOfTuples(numPts);

  vtkDoubleArray* curvatureVector1 = vtkDoubleArray::New();
  curvatureVector1->SetName("Maximum_Curvature_Vector");
  curvatureVector1->SetNumberOfComponents(3);
  curvatureVector1->SetNumberOfTuples(numPts);

  vtkDoubleArray* curvatureVector2 = vtkDoubleArray::New();
  curvatureVector2->SetName("Minimum_Curvature_Vector");
  curvatureVector2->SetNumberOfComponents(3);
  curvatureVector2->SetNumberOfTuples(numPts);

  vtkDoubleArray* principalCurvatures = vtkDoubleArray::New();
  principalCurvatures->SetName("Principal_Curvatures");
  principalCurvatures->SetNumberOfComponents(2);
  principalCurvatures->SetNumberOfTuples(numPts);

  // loop over vertices v_i (or loop over faces, increment?)
  //      get neighbours v_j to v_i
  unsigned short ncells;
  vtkIdType* cells, *pts, npts;

  mesh->BuildLinks();

  // temporary storage.
  double w_ij = 0.0, A_i = 0.0, a_i = 0.0, l_ij = 0.0, kappa_ij = 0.0,\
    k_1 = 0.0, k_2 = 0.0, m_1 = 0.0, m_2 = 0.0;
  double d0[3], d1[3], d2[3], c0[3], c1[3], c2[3];
  for (int i = 0; i < numPts; i++)
  {
    // cout << "vertex " << i << endl;

    mesh->GetPointCells(i,ncells,cells);
    mesh->GetPoint(i,v_i);
    normals->GetTuple(i,n_i);

    // resetting stuff for next vertex
    neighbours->Reset(); A_i = 0.0;
    for (int n = 0; n < 3; n++) for (int m = 0; m < 3; m++) M_i[n][m] = 0.0;

    // explicit computation, on neighbours, etc
    for (int c = 0; c < ncells; c++)
    {
      mesh->GetCellPoints(cells[c],npts,pts);
      for (int j = 0; j < npts; j++)
      {
        if (pts[j] != i)
        {
          neighbours->InsertNextId(pts[j]);
          //cout << "\tadding " << pts[j] << " to neighbours" << endl;
        }
      } // loop over vertices of cell
    } // loop over cells: neighbours should have all been found

    for (int j = 0; j < neighbours -> GetNumberOfIds(); j++)
    {
      mesh->GetPoint(neighbours->GetId(j),  v_j);
      mesh->GetPoint(neighbours->GetId((j+1) % neighbours -> GetNumberOfIds() ),v_k);  // use mod? or not necessary
      w_ij = vtkTriangle::TriangleArea(v_i,v_j,v_k);
      A_i += w_ij;

      e_ij[0] = v_i[0] - v_j[0];
      e_ij[1] = v_i[1] - v_j[1];
      e_ij[2] = v_i[2] - v_j[2];

      //--------------------------------------------
      //T = (I - n_i*n_i^t)* e_ij / norm
      //= e_ij - (e_ij*n_i)*n_i =: e_ij - en*n_i;
      //--------------------------------------------

      en = e_ij[0]*n_i[0] + e_ij[1]*n_i[1] + e_ij[2]*n_i[2];
      T_ij[0] = (e_ij[0] - en*n_i[0]);
      T_ij[1] = (e_ij[1] - en*n_i[1]);
      T_ij[2] = (e_ij[2] - en*n_i[2]);
      vtkMath::Normalize(T_ij);

      // normal curvature in direction i to j: Eq.(6) in Taubin
      kappa_ij = -2.0*en / vtkMath::Norm(e_ij);

      // contribution from neighbour j: Eq.(2) in Taubin
      M_i[0][0] += w_ij*kappa_ij*T_ij[0]*T_ij[0];
      M_i[0][1] += w_ij*kappa_ij*T_ij[0]*T_ij[1];
      M_i[1][0] += w_ij*kappa_ij*T_ij[1]*T_ij[0];
      M_i[1][1] += w_ij*kappa_ij*T_ij[1]*T_ij[1];
      M_i[0][2] += w_ij*kappa_ij*T_ij[0]*T_ij[2];
      M_i[2][0] += w_ij*kappa_ij*T_ij[2]*T_ij[0];
      M_i[2][2] += w_ij*kappa_ij*T_ij[2]*T_ij[2];
      M_i[1][2] += w_ij*kappa_ij*T_ij[1]*T_ij[2];
      M_i[2][1] += w_ij*kappa_ij*T_ij[2]*T_ij[1];

    } // loop over neighbours of v

    // Normalize
    if (fabs(A_i) > 2*VTK_DOUBLE_MIN) 
    {
      a_i = 1.0 / A_i;
    }
    else
    {
      //cout << "total surface area of facets less than tol" << endl;
    }

    for (int n = 0; n < 3; n++)
      for (int m = 0; m < 3; m++)
      {
        M_i[n][m] *= a_i;
      }

      // Diagonalize
      //vtkMath::Jacobi(M_i,pc,T_i);
      qfeMath::qfeComputeJacobi(M_i,pc,T_i);

      d0[0] = T_i[0][0]; d0[1] = T_i[1][0]; d0[2] = T_i[2][0];
      d1[0] = T_i[0][1]; d1[1] = T_i[1][1]; d1[2] = T_i[2][1]; 
      d2[0] = T_i[0][2]; d2[1] = T_i[1][2]; d2[2] = T_i[2][2];
      vtkMath::Cross(d0,n_i,c0);
      vtkMath::Cross(d1,n_i,c1);
      vtkMath::Cross(d2,n_i,c2);
      if (vtkMath::Norm(c0) < TOL)
      {
        m_1 = pc[1]; m_2 = pc[2];
        T_1[0] = d1[0]; T_1[1] = d1[1]; T_1[2] = d1[2]; 
        T_2[0] = d2[0]; T_2[1] = d2[1]; T_2[2] = d2[2]; 
      }
      else if (vtkMath::Norm(c1) < TOL)
      {
        m_1 = pc[0]; m_2 = pc[2];
        T_1[0] = d0[0]; T_1[1] = d0[1]; T_1[2] = d0[2]; 
        T_2[0] = d2[0]; T_2[1] = d2[1]; T_2[2] = d2[2];
      }
      else if (vtkMath::Norm(c2) < TOL)
      {
        m_1 = pc[0]; m_2 = pc[1];
        T_1[0] = d0[0]; T_1[1] = d0[1]; T_1[2] = d0[2]; 
        T_2[0] = d1[0]; T_2[1] = d1[1]; T_2[2] = d1[2];
      }
      else
      {
        //cout << "No eigenvector parallel to normal, keeping previous vertex's" << endl;
      }

      // Equation (5) in Taubin:
      // NB: m_1 >= m_2 according to vtkMath::Jacobi ==> k_1 >= k_2
      k_1 = 3.0*m_1 - m_2;
      k_2 = 3.0*m_2 - m_1;

      // the curvature tensor is the symmetric tensor with
      // eigenvectors: {N   T_1  T_2}					\
      //                              ==> S = k_1*T_1*T_1^t + k_2*T_2*T_2^t + 0*N*N^t;
      // eigenvalues:  {0   k_1  k_2}/
      S_i[0][0] = k_1*T_1[0]*T_1[0] + k_2*T_2[0]*T_2[0];
      S_i[1][1] = k_1*T_1[1]*T_1[1] + k_2*T_2[1]*T_2[1];
      S_i[2][2] = k_1*T_1[2]*T_1[2] + k_2*T_2[2]*T_2[2];
      S_i[0][1] = k_1*T_1[0]*T_1[1] + k_2*T_2[0]*T_2[1]; S_i[1][0] = S_i[0][1];
      S_i[0][2] = k_1*T_1[0]*T_1[2] + k_2*T_2[0]*T_2[2]; S_i[2][0] = S_i[0][2];
      S_i[1][2] = k_1*T_1[1]*T_1[2] + k_2*T_2[1]*T_2[2]; S_i[2][1] = S_i[1][2];

      // update data
      curvatureTensor->SetTuple9(i,S_i[0][0],S_i[0][1],S_i[0][2],		\
        S_i[1][0],S_i[1][1],S_i[1][2],		\
        S_i[2][0],S_i[2][1],S_i[2][2]);
      curvatureVector1->SetTuple3(i,T_1[0],T_1[1],T_1[2]);
      curvatureVector2->SetTuple3(i,T_2[0],T_2[1],T_2[2]);
      principalCurvatures->SetTuple2(i,k_1,k_2);

  } // loop over vertices v_i


  mesh->GetPointData()->AddArray(curvatureVector1);
  mesh->GetPointData()->AddArray(curvatureVector2);  
  mesh->GetPointData()->AddArray(principalCurvatures);

  // cleaning:
  if (principalCurvatures) { principalCurvatures->Delete(); }
  if (curvatureVector1)    { curvatureVector1->Delete(); }
  if (curvatureVector2)    { curvatureVector2->Delete(); }
  if (curvatureTensor)     { curvatureTensor->Delete(); }
  if (neighbours)          { neighbours->Delete(); }

  if (pc) 
  {
    delete [] pc; pc = 0;
  }
  if (M_i)
  {
    for (int n = 0; n < 3; n++) { delete [] M_i[n]; M_i[n] = 0; }
    delete [] M_i; M_i = 0;
  }
  if (T_i)
  {
    for (int n = 0; n < 3; n++) { delete [] T_i[n]; T_i[n] = 0; }
    delete [] T_i; T_i = 0;
  }
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeInitializeTensorBuffer(qfeVolume *volume, qfeFloat **buffer)    
{
  void         *data;
  qfeValueType  type;     
  unsigned int  nx, ny, nz, cc;
  int           dim;

  if((volume == NULL) || (*buffer == NULL)) return qfeError;
  
  volume->qfeGetVolumeData(type, nx, ny, nz, &data);
  volume->qfeGetVolumeComponentsPerVoxel(cc);

  if(type != qfeValueTypeFloat)
  {
    cout << "qfeImageDataManager::qfeComputeStructureTensorBuffer - Only float type supported" << endl;
    return qfeError;
  }

  dim = (int)nx*ny*nz;

  for(int i=0; i<dim; i++)
  {
    for(int j=0; j<(int)cc; j++)
    {
      // Set initial tensor field
      *((float*)*buffer + cc*i + j) = 0.0;    
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeFilterMedian(qfeVolume *in, qfeVolume **median)
{
  unsigned int dims[3];
  unsigned int components;
  unsigned int readIndex[27];
  unsigned int writeIndex;
  qfeVector    vectors[27];
  qfeFloat     distances[27];
  qfeValueType type;
  void        *voxels;  
  float       *filtered;
  qfeGrid     *grid;

  in->qfeGetVolumeComponentsPerVoxel(components);  
  in->qfeGetVolumeData(type, dims[0], dims[1], dims[2], &voxels);
  in->qfeGetVolumeGrid(&grid);

  if(components != 3 || type != qfeValueTypeFloat) return qfeError;

  // Allocate the memory
  filtered = (float*)calloc(dims[0]*dims[1]*dims[2], 3*sizeof(qfeFloat));

  int p = 0; 
  for(int z=0; z<(int)dims[2]; z++)
  {
    for(int y=0; y<(int)dims[1]; y++)
    {
      for(int x=0; x<(int)dims[0]; x++)
      {
        unsigned int valx[3], valy[3], valz[3];

        // Make sure the boundaries conditions are ok
        valx[1] = x;
        valy[1] = y;
        valz[1] = z;
        (x-1 <  (int)0)       ? valx[0] = x : valx[0] = x-1;
        (x+1 >= (int)dims[0]) ? valx[2] = x : valx[2] = x+1;
        (y-1 <  (int)0)       ? valy[0] = y : valy[0] = y-1;
        (y+1 >= (int)dims[1]) ? valy[2] = y : valy[2] = y+1;
        (z-1 <  (int)0)       ? valz[0] = z : valz[0] = z-1;
        (z+1 >= (int)dims[2]) ? valz[2] = z : valz[2] = z+1;
        
        // Get the readIndex for all neighbors
        int kernelIndex = 0;
        for(int i=0; i<3; i++)
        {
          for(int j=0; j<3; j++)
          {
            for(int k=0; k<3; k++)
            {
              readIndex[kernelIndex] = valx[i] + (valy[j] *dims[0]) + (valz[k]*dims[0]*dims[1]);
              kernelIndex++;
            }
          }
        }

        // Get the vector in the local neighborhood
        for(int i=0; i<27; i++)
        {
           vectors[i].x = *((float*)voxels + 3*readIndex[i]+0);
           vectors[i].y = *((float*)voxels + 3*readIndex[i]+1);
           vectors[i].z = *((float*)voxels + 3*readIndex[i]+2);
        }        

        // Get the distances for each vector
        qfeVector diff;        
        qfeFloat  diffsum;
        for(int i=0; i<27; i++)
        {
           diffsum = 0.0;
           for(int j=0; j<27; j++)
           {             
             // Differences based on L2 norm             
             diff     = vectors[i] - vectors[j];
             diffsum += sqrt(diff.x*diff.x + diff.y*diff.y + diff.z*diff.z);
           }
           distances[i] = diffsum;           
        }   

        // Find the smallest sum of differences to neighbors
        qfeFloat     min = distances[0];
        unsigned int minIndex = 0;
        for(int i=0; i<27; i++)
        {
          if(distances[i] < min)
          {  
            min = distances[i];
            minIndex = i;
          }
        }  

        // Place the vector in our new dataset    
        writeIndex = x + (y *dims[0]) + (z *dims[0]*dims[1]);
        *((float*)filtered + 3*writeIndex+0) = vectors[minIndex].x;
        *((float*)filtered + 3*writeIndex+1) = vectors[minIndex].y;
        *((float*)filtered + 3*writeIndex+2) = vectors[minIndex].z;    
         
		    // Update the progress bar        
        p++;
        if(p % (int)(0.02*dims[0]*dims[1]*dims[2]) == 0)
	      {	         
          this->InvokeEvent(vtkCommand::UpdateEvent, &this->callData);
	      }        
      }
    }
  }

  (*median)->qfeSetVolumeComponentsPerVoxel(3);
  (*median)->qfeSetVolumeData(type, dims[0], dims[1], dims[2], filtered);  
  (*median)->qfeSetVolumeGrid(new qfeGrid(*grid));  

  // Clean up
  free(filtered);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeImageDataManager::qfeAddGaussianNoise(qfeVolume *in, qfeVolume **noise, double stDev)
{
  unsigned int dims[3];
  unsigned int components;

  qfeValueType type;
  void        *voxels;  
  float       *data;
  qfeGrid     *grid;

  in->qfeGetVolumeComponentsPerVoxel(components);  
  in->qfeGetVolumeData(type, dims[0], dims[1], dims[2], &voxels);
  in->qfeGetVolumeGrid(&grid);

  if(components != 3 || type != qfeValueTypeFloat) return qfeError;

  // Allocate the memory
  data = (float*)calloc(dims[0]*dims[1]*dims[2], 3*sizeof(qfeFloat));

  
  double r1, r2, rn;
  const double pi = 3.141592653589793;
  bool generateNewSet = true;
  //srand(time(NULL));
  

  for(unsigned int i=0; i<dims[0]*dims[1]*dims[2]*components; i++)
  {
    // Generate gaussian noise using Box-Muller transform
    if(generateNewSet)
    {
      r1 = (rand()+1)/((double)(RAND_MAX+1));
      r2 = (rand()+1)/((double)(RAND_MAX+1));
      rn = sqrt(-2*log(r1))*cos(2*pi*r2);
      generateNewSet = false;
    }
    else
    {
      rn = sqrt(-2*log(r1))*sin(2*pi*r2);
      generateNewSet = true;
    }
    data[i] = ((qfeFloat*)voxels)[i] + rn*stDev;
  }

  (*noise)->qfeSetVolumeComponentsPerVoxel(3);
  (*noise)->qfeSetVolumeData(type, dims[0], dims[1], dims[2], data);  
  (*noise)->qfeSetVolumeGrid(new qfeGrid(*grid));  

  // Clean up
  free(data);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
bool qfeImageDataManager::qfeFileExists(string filename)
{
  struct stat stFileInfo;
  bool   result;
  int intStat;

  // Attempt to get the file attributes
  intStat = stat(filename.c_str(),&stFileInfo);
  if(intStat == 0) {
  // We were able to get the file attributes
  // so the file obviously exists.
   result = true;
  } else {
  // We were not able to get the file attributes.
  // This may mean that we don't have permission to
  // access the folder which contains this file. If you
  // need to do that level of checking, lookup the
  // return values of stat which will give you
  // more details on why stat failed.
   result = false;
 }

 return result;
}

