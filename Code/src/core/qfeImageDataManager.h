#pragma once

#include <vtkCurvatures.h>
#include <vtkCellArray.h>
#include <vtkCleanPolyData.h>
#include <vtkCommand.h>
#include <vtkContourFilter.h>
#include <vtkPolyDataConnectivityFilter.h>
#include <vtkDoubleArray.h>
#include <vtkFloatArray.h>
#include <vtkIdList.h>
#include <vtkMath.h>
#include <vtkMarchingCubes.h>
#include <vtkMatrix4x4.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkPolyDataWriter.h>
#include <vtkPolyDataNormals.h>
#include <vtkQuadricDecimation.h>
#include <vtkWindowedSincPolyDataFilter.h>
#include <vtkStripper.h>  
#include <vtkTriangle.h>
#include <vtkGradientFilter.h>

#include "qfeImageData.h"
#include "qfeVTIImageDataReader.h"
#include "qfeVTIImageDataWriter.h"

#include "qfeCallbackProgressBar.h"
#include "qfeConvert.h"
#include "qfeGrid.h"
#include "qfeImageData.h"
#include "qfePatient.h"
#include "qfeTransform.h"
#include "qfeVolume.h"
#include "qfeFlowLines.h"

#include <sys/stat.h>
#include <sstream>

// File formats
#define QFE_FORMAT_VTK 0

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

/**
* \file   qfeImageDataManager.h
* \author Roy van Pelt
* \class  qfeImageDataManager
* \brief  Implements a data manager for qflow data
*
* This data manager processes a series of qfeImageData files,
* and returns the morphologic and flow images. Furthermore,
* it uploads the scalar and vector data to the GPU, returning a
* texture ID for each data set.
*
* Several functions are provided, that require computations
* on the whole data set (i.e., all phases).
*
*/
class qfeImageDataManager : public vtkObject
{
public:
  qfeImageDataManager();
  ~qfeImageDataManager();

  qfeReturnStatus qfeLoadVolume(char  *path);
  qfeReturnStatus qfeLoadSeries(char **paths, int phases);
  qfeReturnStatus qfeLoadFluidSimulation(char **paths, int phases);
  qfeReturnStatus qfeLoadSegmentation(const char *path);
  qfeReturnStatus qfeLoadSegmentation(qfeVolume *volume, qfeFloat isoValue);
  qfeReturnStatus qfeLoadSeeds(const char *path);
  qfeReturnStatus qfeLoadClusterHierarchy(const char *path);  
  qfeReturnStatus qfeReleaseData();

  qfeReturnStatus qfeLoadProbe(string path);
  qfeReturnStatus qfeSaveProbe(qfeProbe3D *probe, string path);
  qfeReturnStatus qfeExistsProbe(qfeProbe3D *probe);

  qfeReturnStatus qfeGetPatient(qfePatient **patient);

  qfeReturnStatus qfeWriteClusterHierarchy(qfeStudy *study);
  qfeReturnStatus qfeWriteFluidSimulation(qfeStudy *study);
  qfeReturnStatus qfeWriteGaussianNoise(qfeStudy *study, double stDev, unsigned int count);
  qfeReturnStatus qfeWriteMedianFilter(qfeStudy *study);
  qfeReturnStatus qfeWriteFTLE(qfeStudy *study, qfeVector sampleDim, float intTime, float intStepSize);
  qfeReturnStatus qfeWriteTemporalMIP(qfeStudy *study);
  qfeReturnStatus qfeWriteTemporalMAP(qfeStudy *study);
  qfeReturnStatus qfeWriteTemporalMOP(qfeStudy *study);
  qfeReturnStatus qfeWriteSynthFlow(qfeStudy *study, qfeVector size);
  qfeReturnStatus qfeWriteVolumeSeries(qfeVolumeSeries *series, const std::string &name);

protected:  
  // Compute a temporal mip
  qfeReturnStatus qfeComputeTemporalMIP(qfeStudy *study);

  // Compute a temporal mop
  qfeReturnStatus qfeComputeTemporalMOP(qfeStudy *study);

  // Compute a temporal mean angiographic projection (Hennemuth et al. 2011)
  qfeReturnStatus qfeComputeTemporalMAP(qfeStudy *study);

  // Compute a FTLE volume
  qfeReturnStatus qfeComputeFTLE(qfeVolumeSeries *in, qfeVolume *FTLE, int t, qfeVector sampleDim, float intTime, float intStepSize);

  // Compute a synthetic flow data set
  qfeReturnStatus qfeComputeSynthFlow(qfeStudy *study, qfeVolume *synthFlow, qfeVector size);

  // Compute model / surface properties
  qfeReturnStatus qfeComputeTriangleStrips(vtkPolyData *mesh);
  qfeReturnStatus qfeComputeNormals(vtkPolyData *mesh);
  qfeReturnStatus qfeComputeCurvature(vtkPolyData *mesh);


private:

  // Data structures
  qfePatient   *patient;

  // Data properties
  int           dataFormat;
  string        dataPath;

  // Flags
  bool          glInit;
  bool          dataInit;
  bool          simInit;

  // Call data
  qfeCallData   callData;  

  // Read data
  qfeReturnStatus qfeReadVTK(char *path, qfeStudy *study);
  qfeReturnStatus qfeReadVTKFlow4D(char *path, qfeStudy *study);
  qfeReturnStatus qfeReadVTKFlow2D(char *path, qfeStudy *study);
  qfeReturnStatus qfeReadVTKFlowSim(char *path, qfeStudy *study);
  qfeReturnStatus qfeReadVTKSSFP3D(char *path, qfeStudy *study);
  qfeReturnStatus qfeReadVTKSSFP2D(char *path, qfeStudy *study);  

  // Post process data
  qfeReturnStatus qfePostProcess(char *path, qfeStudy *study);
  qfeReturnStatus qfePostProcessFlow4D(char *path, qfeStudy *study);
  qfeReturnStatus qfePostProcessFlow2D(qfeStudy *study);
  qfeReturnStatus qfePostProcessAnatomy(qfeStudy *study); 

  // Compute a median filtered volume
  qfeReturnStatus qfeFilterMedian(qfeVolume *in, qfeVolume **median);

  // Generate Gaussian noise
  qfeReturnStatus qfeAddGaussianNoise(qfeVolume *in, qfeVolume **noise, double stDev);

  // Compute structure tensor and helper functions    
  qfeReturnStatus qfeInitializeTensorBuffer(qfeVolume *volume, qfeFloat **buffer);     

  // Check if a file exists
  bool qfeFileExists(string filename);
};

