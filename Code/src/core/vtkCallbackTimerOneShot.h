/**
* \file   vtkCallbackTimerOneShot.h
* \author Roy van Pelt
* \class  vtkCallbackTimerOneShot
* \brief  Implements a timer callback
*/

#ifndef _vtkCallbackTimerOneShot_h
#define _vtkCallbackTimerOneShot_h

#include <vtkCommand.h>

class vtkCallbackTimerOneShot : public vtkCommand
{
public:
  static vtkCallbackTimerOneShot* New();

  virtual void Execute(vtkObject *, unsigned long, void *);
  void SetOneShotTimerId(int tid);
  void SetOneShotTimerOn();
  void SetOneShotTimerOff();
  bool IsOn();
  bool IsShot();

private:
  int  OneShotTimerId; 
  bool flag;
  bool on;

};

#endif // _vtkCallbackTimerOneShot_h