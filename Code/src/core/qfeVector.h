#pragma once
/**
 * \file   qfeVector.h
 * \author Roy van Pelt
 * \class  qfeVector
 * \brief  Implements a vector.
 *
 * qfeVector provides a set of vector (x,y,z,w) operations
 */
#include <iostream>
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "qfeTensor3f.h"

#include "qflowexplorer.h"

using namespace std;

class qfeVector
{
  // Commutative operators
  friend const qfeVector operator  +  ( const qfeVector  & v1 , const qfeVector & v2 );
  friend const qfeFloat  operator  *  ( const qfeVector  & v1 , const qfeVector & v2 );  
  friend const qfeVector operator  *  ( const qfeFloat   & f  , const qfeVector & v );
  friend const qfeVector operator  *  ( const qfeVector  & v  , const qfeFloat  & f ); // Dot

public:
  qfeFloat x, y, z, w;

  qfeVector();
  qfeVector(qfeFloat *v);
  qfeVector(qfeFloat x, qfeFloat y, qfeFloat z);
  ~qfeVector(){};

  qfeReturnStatus qfeSetVectorElements(qfeFloat v[4]);
  qfeReturnStatus qfeSetVectorElements(qfeFloat x, qfeFloat y, qfeFloat z);
  qfeReturnStatus qfeSetVectorElements(qfeFloat x, qfeFloat y, qfeFloat z, qfeFloat w);
  qfeReturnStatus qfeGetVectorElements(qfeFloat v[4]);

  qfeFloat& operator  [] ( int element);
  qfeVector operator  ^  ( const qfeVector & v ) const;
  qfeVector operator  /  ( const qfeVector & v ) const;
  qfeVector operator  -  ( const qfeVector & v ) const;  
  bool      operator  == ( const qfeVector & v ) const;
  bool      operator  != ( const qfeVector & v ) const;
  bool      operator  <  ( const qfeVector & v ) const;
  bool      operator  >  ( const qfeVector & v ) const;
  qfeVector operator  -  () const;

  static qfeReturnStatus qfeVectorLength(qfeVector v1, qfeFloat &u);
  static qfeReturnStatus qfeVectorNormalize(qfeVector &u);
  static qfeReturnStatus qfeVectorTensorProduct(qfeVector v1, qfeVector v2, qfeTensor3f &t);
  static qfeReturnStatus qfeVectorCross(qfeVector v1, qfeVector v2, qfeVector &u);
  static qfeReturnStatus qfeVectorDot(qfeVector v1, qfeVector v2, qfeFloat &u);
  static qfeReturnStatus qfeVectorMultiply(qfeFloat f, qfeVector v, qfeVector &u);
  static qfeReturnStatus qfeVectorDivide(qfeVector v1, qfeVector v2, qfeVector &u);
  static qfeReturnStatus qfeVectorSubtract(qfeVector v1, qfeVector v2, qfeVector &u);
  static qfeReturnStatus qfeVectorAdd(qfeVector v1, qfeVector v2, qfeVector &u);
  static qfeReturnStatus qfeVectorClear(qfeVector &v);

  static qfeReturnStatus qfeVectorOrthonormalBasis1(qfeVector v, qfeVector u, qfeVector &w);
  static qfeReturnStatus qfeVectorOrthonormalBasis2(qfeVector v, qfeVector &u, qfeVector &w);
  static qfeReturnStatus qfeVectorInterpolateLinear(qfeVector v1, qfeVector v2, qfeFloat t, qfeVector &u);
  static qfeReturnStatus qfeVectorInterpolateLinearSeparate(qfeVector v1, qfeVector v2, qfeFloat t, qfeVector &u);
  static qfeReturnStatus qfeVectorInterpolateSphericalLinear(qfeVector v1, qfeVector v2, qfeFloat t, qfeVector &u);

  static qfeReturnStatus qfeVectorPrint(qfeVector v);

private :
    qfeFloat e;

};

