#include "qfeKernel.h"

qfeKernel* qfeKernel::singleton     = NULL;
bool       qfeKernel::singletonFlag = false;

//----------------------------------------------------------------------------
qfeKernel* qfeKernel::qfeGetInstance()
{
  if(!singletonFlag)
  {
    singleton     = new qfeKernel();
    singletonFlag = true;
    return singleton;
  }
  else
  {
    return singleton;
  }
}

//----------------------------------------------------------------------------
void qfeKernel::qfeDeleteInstance()
{
  singletonFlag  = false;
  delete singleton;
}

//----------------------------------------------------------------------------
qfeKernel::qfeKernel()
{
  this->DataManager          = new qfeImageDataManager();
  this->SceneManager         = new qfeSceneManager();

  this->progressCallback     = vtkCallbackProgressBar::New();

  // Add observers
  this->DataManager->AddObserver(vtkCommand::UpdateEvent, this->progressCallback);
  this->SceneManager->AddObserver(vtkCommand::UpdateEvent, this->progressCallback);
}

//----------------------------------------------------------------------------
qfeKernel::~qfeKernel()
{
  this->DataManager->RemoveObserver(this->progressCallback);

  this->DataManager->Delete();  

  this->progressCallback->Delete();

  delete this->SceneManager;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeKernel::qfeKernelLoadData(char **files, int count)
{
  this->DataManager->qfeLoadSeries(files, count);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeKernel::qfeKernelLoadSim(char **files, int count)
{
  this->DataManager->qfeLoadFluidSimulation(files, count);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeKernel::qfeKernelReleaseData()
{
  this->DataManager->qfeReleaseData();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeKernel::qfeKernelRenderScene(int scenePreset)
{
  qfePatient *patient;
  this->DataManager->qfeGetPatient(&patient);

  this->SceneManager->qfeSceneManagerStop();  
  //if(scenePreset == sceneIFR) return qfeError;
  this->SceneManager->qfeSceneManagerInit(patient);  
  this->SceneManager->qfeSceneManagerStart(scenePreset);
  this->SceneManager->qfeSceneManagerRender(NULL);
    
  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeKernel::qfeKernelUpdateColorMap(int visualID, QVector< QPair<double, double*> > color, QVector< QPair<double, double>  > opacity, QVector< QPair<double, double>  > gradient, int interpolation, int quantization, int space, bool constlight, bool paduni)
{
  qfeColorMap cm;
  qfeConvert::qtTransferFunctionToqfeColorMap(color, opacity, gradient, interpolation, quantization, space, constlight, paduni, cm);
  this->SceneManager->qfeSetSceneManagerColorMap(visualID, cm);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeKernel::qfeKernelUpdateLight(qfeVector lightDirection, qfeFloat ambientContrib, qfeFloat diffuseContrib, 
                                                qfeFloat specularContrib, qfeFloat specularPower)
{
  qfeLightSource light;

  light.qfeSetLightSourceDirection(lightDirection);
  light.qfeSetLightSourceAmbientContribution(ambientContrib);
  light.qfeSetLightSourceDiffuseContribution(diffuseContrib);
  light.qfeSetLightSourceSpecularContribution(specularContrib);
  light.qfeSetLightSourceSpecularPower(specularPower);
  this->SceneManager->qfeSetSceneManagerLight(light);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeKernel::qfeKernelUpdateDisplay(int type, QColor color1, QColor color2)
{
  qfeDisplay  display;
  qfeColorRGB c1, c2;

  c1.r = color1.red()/255.0; c1.g = color1.green()/255.0; c1.b = color1.blue()/255.0;
  c2.r = color2.red()/255.0; c2.g = color2.green()/255.0; c2.b = color2.blue()/255.0;  

  display.qfeSetDisplayBackgroundType(type);
  display.qfeSetDisplayBackgroundColorRGB(c1.r, c1.g, c1.b);
  display.qfeSetDisplayBackgroundColorRGBSecondary(c2.r, c2.g, c2.b);

  this->SceneManager->qfeSetSceneManagerDisplay(display);

  return qfeSuccess;
}

qfeReturnStatus qfeKernel::qfeKernelUpdateFeatureTF(int id, QVector<QPair<double, double>> &tf, int dataType)
{
  SceneManager->qfeSetSceneManagerFeatureTF(id, tf, dataType);
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeKernel::qfeConnectProgressBar(qfeCallBackProgressBar *callback)
{
  this->progressCallback->SetProgressClass(callback);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// SCREENSHOT
//----------------------------------------------------------------------------
void qfeKernel::CreateScreenShot()
{
	this->SceneManager->qfeGetDriver()->qfeGetEngine()->StoreScreenshot();
  //vtkWindowToImageFilter* imageFilter= vtkWindowToImageFilter::New();
  //imageFilter->SetInput(this->RenderEngine->GetRenderer()->GetRenderWindow());

  //vtkPNGWriter* imageWriter= vtkPNGWriter::New();
  //imageWriter->SetInput(imageFilter->GetOutput());
  //imageWriter->SetFileName(path);
  //imageWriter->Write();
}

//Plot
void qfeKernel::RenderPlot(float phase)
{
	qfePatient *patient;
	DataManager->qfeGetPatient(&patient);

	qfeStudy *study = patient->study;

	study->qfePlotData(phase);
}

void qfeKernel::qfeCalculateSpeedVolumes()
{
  qfePatient *patient;
  DataManager->qfeGetPatient(&patient);

  if (!patient)
    return;
  qfeStudy *study = patient->study;
  if (!study)
    return;

  if (study->pcap.size() > 0) {
    qfeValueType t;
    unsigned w, h, d;
    float *data;
    const unsigned nrComp = 3;

    study->pcap.front()->qfeGetVolumeSize(w, h, d);
    float *speedData = new float[w*h*d];

    for (unsigned i = 0; i < study->pcap.size(); i++) {
      float minSpeed = 1e30f;
      float maxSpeed = 0;

      // Calculate speeds
      study->pcap[i]->qfeGetVolumeData(t, w, h, d, (void**)&data);
      for (unsigned j = 0; j < w*h*d; j++) {
        float x = data[j*nrComp];
        float y = data[j*nrComp+1];
        float z = data[j*nrComp+2];
        speedData[j] = sqrtf(x*x + y*y + z*z);

        if (speedData[j] < minSpeed) minSpeed = speedData[j];
        if (speedData[j] > maxSpeed) maxSpeed = speedData[j];
      }

      // Add speed volume to series
      qfeVolume *volume = new qfeVolume(*study->pcap[i]);
      volume->qfeSetVolumeData(t, w, h, d, 1, speedData); // Only one component
      volume->qfeSetVolumeValueDomain(minSpeed, maxSpeed);
      volume->qfeSetHistogramToNull();
      volume->qfeUploadVolume();
      study->flowSpeed.push_back(volume);
    }

    delete speedData;
  }
}
