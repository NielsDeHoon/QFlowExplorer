#include "qfeMath.h"
#include <vtkGradientFilter.h>
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <vtkDataArray.h>
#include <vtkPointData.h>
#include "qfeConvert.h"
#include "qfeImageData.h"
#include "qfeGradient.h"

//----------------------------------------------------------------------------
qfeMath::qfeMath()
{

}

qfeReturnStatus qfeMath::qfeComputeGradientTensors(qfeVolume *volume, qfeTensor3f *gradients, bool useLinearRegression)
{
  unsigned w, h, d;
  volume->qfeGetVolumeSize(w, h, d);
  unsigned size = w*h*d;

  if (useLinearRegression) {
    qfeGradient gradientCalculator;
    gradientCalculator.qfeGetGradientTensors(volume, gradients);
  } else {
    // Convert flow volume to vtk form
    vtkSmartPointer<qfeImageData> flowMap = qfeImageData::New();
    qfeConvert::qfeVolumeToVtkImageData(volume, flowMap);

    //Gradient calculation
    vtkSmartPointer<vtkGradientFilter> gradientFilter = vtkSmartPointer<vtkGradientFilter>::New();
    gradientFilter->SetInput(flowMap);
    gradientFilter->Update();
    vtkSmartPointer<vtkDataArray> gradientImg = gradientFilter->GetOutput()->GetPointData()->GetArray("Gradients");

    double grad[9];
    for (unsigned i = 0; i < size; i++) {
      gradientImg->GetTuple(i, grad);
      gradients[i] = qfeTensor3f(grad);
    }
  }

  return qfeSuccess;
}

qfeReturnStatus qfeMath::qfeComputeLambda2s(qfeVolume *volume, float *lambda2s, float &rangeMin, float &rangeMax, bool discardNegLambda3)
{
  unsigned w, h, d;
  volume->qfeGetVolumeSize(w, h, d);
  unsigned size = w*h*d;

  qfeTensor3f *gradients = new qfeTensor3f[size];
  qfeComputeGradientTensors(volume, gradients, true);

  rangeMin = 1e30f;
  rangeMax = -1e30f;
  for (unsigned i = 0; i < size; i++) {
    qfeTensor3f transpose;
    qfeTensor3f::qfeGetTensorTranspose(gradients[i], transpose);

    // Rate of deformation tensor
    qfeTensor3f D = (gradients[i] + transpose) / 2.f;
    // Spin tensor
    qfeTensor3f S = (gradients[i] - transpose) / 2.f;
    
    // Compute eigenvalues of D^2+S^2
    qfeTensor3f tensor = D*D + S*S;
    qfeVector lambda;
    qfeMath::qfeComputeEigenValues(tensor, lambda);

    // Sort the eigenvalues
    swap(lambda.x, min(lambda.x, min(lambda.y, lambda.z)));
    swap(lambda.y, min(lambda.y, lambda.z));

    if (discardNegLambda3 && lambda.z < 0.f)
      lambda.y = 0.f;

    // Save second eigenvalue (lambda2)
    lambda2s[i] = lambda.y;
    if (lambda.y > rangeMax) rangeMax = lambda.y;
    if (lambda.y < rangeMin) rangeMin = lambda.y;
  }

  delete[] gradients;

  return qfeSuccess;
}

qfeReturnStatus qfeMath::qfeComputeQCriterion(qfeVolume *volume, float *qcriterion, float &rangeMin, float &rangeMax)
{
  // Convert flow volume to vtk form
  vtkSmartPointer<qfeImageData> flowMap = qfeImageData::New();
  qfeConvert::qfeVolumeToVtkImageData(volume, flowMap);

  // Q-criterion calculation
  vtkSmartPointer<vtkGradientFilter> gradientFilter = vtkSmartPointer<vtkGradientFilter>::New();
  gradientFilter->SetInput(flowMap);
  gradientFilter->ComputeQCriterionOn();
  gradientFilter->Update();
  vtkPointData *pointData = gradientFilter->GetOutput()->GetPointData();
  vtkSmartPointer<vtkDataArray> qCritImg = pointData->GetArray("Q-criterion");

  unsigned w, h, d;
  volume->qfeGetVolumeSize(w, h, d);
  unsigned size = w*h*d;

  rangeMin = 1e30f;
  rangeMax = -1e30f;
  for (unsigned i = 0; i < size; i++) {
    double value;
    qCritImg->GetTuple(i, &value);
    qcriterion[i] = (float)value;

    if (value > rangeMax) rangeMax = (float)value;
    if (value < rangeMin) rangeMin = (float)value;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMath::qfeComputeStructureTensor(qfeVector v, qfeTensor3f &t)
{
  t(0,0) = v.x * v.x;
  t(0,1) = v.x * v.y;
  t(0,2) = v.x * v.z;

  t(1,0) = v.x * v.y;
  t(1,1) = v.y * v.y;
  t(1,2) = v.y * v.z;

  t(2,0) = v.x * v.z;
  t(2,1) = v.y * v.z;
  t(2,2) = v.z * v.z;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMath::qfeComputeAverageStructureTensor(qfeTensor3f neighborhood[8], qfeTensor3f &t)
{
  qfeTensor3f sum;

  qfeTensor3f::qfeSetTensorIdentity(sum);
  sum(0,0) = sum(1,1) = sum(2,2) = 0.0f;

  for(int i=0; i<8; i++)
  { 
    // Sum the neighborhood of tensors
    sum = sum + neighborhood[i];      
  }

  t = sum / 8.0f;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Based on Eigensystems for 3 � 3 Symmetric Matrices (Revisited) - D. Eberly
qfeReturnStatus qfeMath::qfeComputeEigenSystem(qfeTensor3f t, qfeVector &lambda, qfeVector &v1, qfeVector &v2, qfeVector &v3)
{
  qfeTensor3f I, A;
  qfeVector   ev;
  double root[3];

  A = t;
  qfeTensor3f::qfeSetTensorIdentity(I);

  qfeComputeEigenValues(t,ev);
  root[0] = ev.x; 
  root[1] = ev.y;
  root[2] = ev.z;
  lambda  = ev;

  qfeTensor3f M0 = A - I*ev.x; // I is the identity matrix
  
  int rank0;
  qfeComputeRank(A, rank0);
  
  if (rank0 == 0)
  {
    // evalue[0] = evalue[1] = evalue[2]
    v1.qfeSetVectorElements(1,0,0);
    v2.qfeSetVectorElements(0,1,0);
    v3.qfeSetVectorElements(0,0,1);

    return qfeSuccess;
  }
  
  if (rank0 == 1)
  {
    // evalue[0] = evalue[1] < evalue[2]    
    qfeVector row0;
    row0.qfeSetVectorElements(M0(0,0),M0(0,1),M0(0,2));

    qfeVector::qfeVectorOrthonormalBasis2(row0, v1, v2);
    qfeVector::qfeVectorCross(v1,v2,v3);
    
    return qfeSuccess;
  }

  
  // rank0 == 2
  qfeTensor3f M1;
  qfeVector  row0, row1;
  row0.qfeSetVectorElements(M0(0,0),M0(0,1),M0(0,2));
  row1.qfeSetVectorElements(M0(1,0),M0(1,1),M0(1,2));

  qfeVector::qfeVectorOrthonormalBasis1(row0, row1, v1);
  
  M1 = A - I*ev.y;
 
  int rank1;
  qfeComputeRank(M1, rank1); // zero rank detected earlier, rank1 must be positive
  
  if (rank1 == 1)
  {
    // evalue[0] < evalue[1] = evalue[2]    
    qfeVector::qfeVectorOrthonormalBasis2(v1,v2,v3);
    return qfeSuccess;
  }
  
  // rank1 == 2
  row0.qfeSetVectorElements(M1(0,0),M1(0,1),M1(0,2));
  row1.qfeSetVectorElements(M1(1,0),M1(1,1),M1(1,2));
  qfeVector::qfeVectorOrthonormalBasis1(row0,row1,v2);

  // rank2 == 2 (eigenvalues must be distinct at this point, rank2 must be 2)
  qfeVector::qfeVectorCross(v1,v2,v3);  

  return qfeSuccess;

}


//----------------------------------------------------------------------------
// Based on vtkMath::Diagonalize3x3
qfeReturnStatus qfeMath::qfeComputeEigenSystemVtk(qfeTensor3f t, qfeVector &lambda, qfeVector &v1, qfeVector &v2, qfeVector &v3)
{
  int         i,j,k,maxI;
  qfeFloat    tmp, maxVal;
  qfeTensor3f ev;
  
  // Convert the tensor before Jacobi calculation
  qfeFloat    Matrix[3][3];
  qfeTensor3f EigenVectors;
  qfeFloat    EigenValues[3];
  qfeFloat   *MatrixTemp[3], *EigenVectorsTemp[3];

  for (i = 0; i < 3; i++)
  {
    Matrix[i][0]        = t(i,0);
    Matrix[i][1]        = t(i,1);
    Matrix[i][2]        = t(i,2);
    MatrixTemp[i]       = Matrix[i];
    EigenVectorsTemp[i] = &EigenVectors(i,0);
  }

  // Diagonalize using Jacobi
  qfeComputeJacobiN(MatrixTemp,3,EigenValues, EigenVectorsTemp);

  // If all the eigenvalues are the same, return orthonormal vectors
  if (EigenValues[0] == EigenValues[1] && EigenValues[0] == EigenValues[2])
  {    
    v1.qfeSetVectorElements(1.0,0.0,0.0);
    v2.qfeSetVectorElements(0.0,1.0,0.0);
    v3.qfeSetVectorElements(0.0,0.0,1.0);
    return qfeSuccess;
  }

  // Transpose temporarily, it makes it easier to sort the eigenvectors  
  qfeTensor3f::qfeGetTensorTranspose(EigenVectors, EigenVectors);

  // If two eigenvalues are the same, re-orthogonalize to optimally line
  // up the eigenvectors with the x, y, and z axes
  for (i = 0; i < 3; i++)
  {
    if (EigenValues[(i+1)%3] == EigenValues[(i+2)%3]) // two eigenvalues are the same
    {
      // Find maximum element of the independant eigenvector
      maxVal = fabs(EigenVectors(i,0));
      maxI = 0;
      for (j = 1; j < 3; j++)
      {
        if (maxVal < (tmp = fabs(EigenVectors(i,j))))
        {
          maxVal = tmp;
          maxI = j;
        }
      }
      // Swap the eigenvector into its proper position
      if (maxI != i)
      {
        qfeVector w1, w2;
        tmp               = EigenValues[maxI];
        EigenValues[maxI] = EigenValues[i];
        EigenValues[i]    = tmp;

        w1.qfeSetVectorElements(EigenVectors(i,0),EigenVectors(i,1),EigenVectors(i,2)); 
        w2.qfeSetVectorElements(EigenVectors(maxI,0),EigenVectors(maxI,1),EigenVectors(maxI,2)); 

        qfeSwapVectors(w1,w2);

        EigenVectors(i,0)    = w1.x; EigenVectors(i,1)    = w1.y; EigenVectors(i,2)    = w1.z;
        EigenVectors(maxI,0) = w2.x; EigenVectors(maxI,1) = w2.y; EigenVectors(maxI,2) = w2.z;
      }
      // Maximum element of eigenvector should be positive
      if (EigenVectors(maxI,maxI) < 0)
      {
        EigenVectors(maxI,0) = -EigenVectors(maxI,0);
        EigenVectors(maxI,1) = -EigenVectors(maxI,1);
        EigenVectors(maxI,2) = -EigenVectors(maxI,2);
      }
      
      // De-orthogonalize the other two eigenvectors
      j = (maxI+1)%3;
      k = (maxI+2)%3;

      EigenVectors(j,0) = 0.0; 
      EigenVectors(j,1) = 0.0; 
      EigenVectors(j,2) = 0.0;
      EigenVectors(j,j) = 1.0;

      qfeVector w1, w2, w3;
      w1.qfeSetVectorElements(EigenVectors(maxI,0),EigenVectors(maxI,1),EigenVectors(maxI,2));
      w2.qfeSetVectorElements(EigenVectors(j,0)   ,EigenVectors(j,1)   ,EigenVectors(j,2));

      qfeVector::qfeVectorCross(w1,w2,w3);
      qfeVector::qfeVectorNormalize(w3);      
      qfeVector::qfeVectorCross(w3,w1,w2);

      EigenVectors(maxI,0) = w1.x; EigenVectors(maxI,1) = w1.y; EigenVectors(maxI,2) = w1.z;
      EigenVectors(j,0)    = w2.x; EigenVectors(j,1)    = w2.y; EigenVectors(j,2)    = w2.z;
      EigenVectors(k,0)    = w3.x; EigenVectors(k,1)    = w3.y; EigenVectors(k,2)    = w3.z;      

      // Transpose vectors back to columns
      qfeTensor3f::qfeGetTensorTranspose(EigenVectors, EigenVectors);
      return qfeSuccess;
    }
  }

  // The three eigenvalues are different, just sort the eigenvectors
  // to align them with the x, y, and z axes

  // Find the vector with the largest x element, make that vector
  // the first vector
  maxVal = fabs(EigenVectors(0,0));
  maxI   = 0;
  for (i = 1; i < 3; i++)
  {
    if (maxVal < (tmp = fabs(EigenVectors(i,0))))
    {
      maxVal = tmp;
      maxI = i;
    }
  }

  // Swap eigenvalue and eigenvector
  if (maxI != 0)
  {
    qfeVector w1, w2;

    tmp               = EigenValues[maxI];
    EigenValues[maxI] = EigenValues[0];
    EigenValues[0]    = tmp;    
    
    w1.qfeSetVectorElements(EigenVectors(maxI,0),EigenVectors(maxI,1),EigenVectors(maxI,2)); 
    w2.qfeSetVectorElements(EigenVectors(0,0)   ,EigenVectors(0,1)   ,EigenVectors(0,2)); 

    qfeSwapVectors(w1,w2);

    EigenVectors(maxI,0) = w1.x; EigenVectors(maxI,1) = w1.y; EigenVectors(maxI,2) = w1.z;
    EigenVectors(0,0)    = w2.x; EigenVectors(0,1)    = w2.y; EigenVectors(0,2)    = w2.z;
  }

  
  // Do the same for the y element
  if (fabs(EigenVectors(1,1)) < fabs(EigenVectors(2,1)))
  {
    qfeVector w1, w2;

    tmp               = EigenValues[2];
    EigenValues[2]    = EigenValues[1];
    EigenValues[1]    = tmp;    
    
    w1.qfeSetVectorElements(EigenVectors(2,0), EigenVectors(2,1), EigenVectors(2,2)); 
    w2.qfeSetVectorElements(EigenVectors(1,0), EigenVectors(1,1), EigenVectors(1,2)); 

    qfeSwapVectors(w1,w2);

    EigenVectors(2,0) = w1.x; EigenVectors(2,1) = w1.y; EigenVectors(2,2) = w1.z;
    EigenVectors(2,0) = w2.x; EigenVectors(1,1) = w2.y; EigenVectors(1,2) = w2.z;
  }

  // Ensure that the sign of the eigenvectors is correct
  for (i = 0; i < 2; i++)
  {
    if (EigenVectors(i,i) < 0)
    {
      EigenVectors(i,0) = -EigenVectors(i,0);
      EigenVectors(i,1) = -EigenVectors(i,1);
      EigenVectors(i,2) = -EigenVectors(i,2);
    }
  }
    
  // Set sign of final eigenvector to ensure that determinant is positive
  qfeFloat det;
  qfeTensor3f::qfeGetTensorDeterminant(EigenVectors, det);
  if (det < 0)
  {
    EigenVectors(2,0) = -EigenVectors(2,0);
    EigenVectors(2,1) = -EigenVectors(2,1);
    EigenVectors(2,2) = -EigenVectors(2,2);
  }

  // Transpose the eigenvectors back again
  qfeTensor3f::qfeGetTensorTranspose(EigenVectors,EigenVectors);

  // Set the return variables
  lambda.x = EigenValues[0];
  lambda.y = EigenValues[1];
  lambda.z = EigenValues[2];

  v1.qfeSetVectorElements(EigenVectors(0,0),EigenVectors(0,1),EigenVectors(0,2));
  v2.qfeSetVectorElements(EigenVectors(1,0),EigenVectors(1,1),EigenVectors(1,2));
  v3.qfeSetVectorElements(EigenVectors(2,0),EigenVectors(2,1),EigenVectors(2,2));

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeMath::qfeComputeEigenValues(qfeTensor3f t, qfeVector &lambda)
{  
  double    inv3, root3;
  double    c2Div3, aDiv3, mbDiv2, q;
  double    magnitude, angle, cs, sn;
  double    c[3];
  float     save;
  qfeVector l;

  inv3  = 1.0f/3.0f;
  root3 = sqrt(3.0f);

  c[0]  = t(0,0)*t(1,1)*t(2,2)      +
          t(0,1)*t(0,2)*t(1,2)*2.0f - 
          t(0,0)*t(1,2)*t(1,2)      - 
          t(1,1)*t(0,2)*t(0,2)      - 
          t(2,2)*t(0,1)*t(0,1);
  c[1]  = t(0,0)*t(1,1) - 
          t(0,1)*t(0,1) + 
          t(0,0)*t(2,2) - 
          t(0,2)*t(0,2) + 
          t(1,1)*t(2,2) - 
          t(1,2)*t(1,2);
  c[2]  = t(0,0) + t(1,1) + t(2,2);

  c2Div3 = c[2]*inv3;
  aDiv3  = (c[1]-c[2]*c2Div3)*inv3;

  if(aDiv3 > 0.0f) aDiv3 = 0.0f;

  mbDiv2 = 0.5f*(c[0]+c2Div3*(2.0f*c2Div3*c2Div3-c[1]));
  q      = mbDiv2*mbDiv2 + aDiv3*aDiv3*aDiv3;

  if(q>0.0f) q = 0.0f;

  magnitude = sqrt(-aDiv3);
  angle     = atan2(sqrt(-q),mbDiv2)*inv3;
  cs        = cos(angle);
  sn        = sin(angle);

  l.x       = (qfeFloat)(c2Div3 + 2.0f*magnitude*cs);
  l.y       = (qfeFloat)(c2Div3 - magnitude*(cs+root3*sn));
  l.z       = (qfeFloat)(c2Div3 - magnitude*(cs-root3*sn));

  // Sort the eigenvalues
  save = min(l.x, l.y); l.x = l.x + l.y - save; l.y = save;
  save = min(l.y, l.z); l.y = l.y + l.z - save; l.z = save;

  lambda = l;  

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeMath::qfeComputeCoherenceEDC(qfeTensor3f structuretensor, qfeFloat &coherence)
{
  qfeFloat  c;
  qfeVector lambda;
  qfeFloat  l1, l2;

  qfeMath::qfeComputeEigenValues(structuretensor, lambda);

  // Make sure we are not computing with floating point numbers [0,1]
  // In order to get the fraction, we temporarily upscale and use integers  
  if((lambda.x < 1.0) && (lambda.y < 1.0))
  {
    l1 = lambda.x * 100.0f;
    l2 = lambda.y * 100.0f;
  }
  else
  {
    l1 = lambda.x;
    l2 = lambda.y;
  }
  
  //cout << "Eigenvalues" << endl;
  //qfeVector::qfeVectorPrint(lambda);

  c          = (l1 + l2);
  coherence  = 1.0f - ((4*l1*l2) / (c*c));

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMath::qfeComputeRank(qfeTensor3f &t, int &rank)
{
  // Compute the maximum magnitude matrix entry.
  qfeFloat absv, save, max = -1;
  int      row, col, maxRow = -1, maxCol = -1;
  for (row = 0; row < 3; row++)
  {
    for (col = row; col < 3; col++)
    {    
      absv = abs(t(row,col));
      if (absv > max)
      {
        max    = absv;
        maxRow = row;
        maxCol = col;
      }
    }
  }

  if (max == 0)
  {
    // The rank is 0. The eigenvalue has multiplicity 3.
    rank = 0;
    return qfeSuccess;
  }

  // The rank is at least 1. Swap the row containing the maximum-magnitude
  // entry with row 0.
  if (maxRow != 0)
  {
    for (col = 0; col < 3; col++)
    {
      save          = t(0,col);
      t(0,col)      = t(maxRow,col);
      t(maxRow,col) = save;
    }
  }
  // Row-reduce the matrix...
  // Scale the row containing the maximum to generate a 1-valued pivot.
  qfeFloat invMax = 1/t(maxRow,maxCol);
  t(0,0) *= invMax;
  t(0,1) *= invMax;
  t(0,2) *= invMax;
  
  // Eliminate the maxCol column entries in rows 1 and 2.
  if (maxCol == 0)
  {
    t(1,1) -= t(1,0)*t(0,1);
    t(1,2) -= t(1,0)*t(0,2);
    t(2,1) -= t(2,0)*t(0,1);
    t(2,2) -= t(2,0)*t(0,2);
    t(1,0) = 0;
    t(2,0) = 0;
  }
  else if (maxCol == 1)
  {
    t(1,0) -= t(1,1)*t(0,0);
    t(1,2) -= t(1,1)*t(0,2);
    t(2,0) -= t(2,1)*t(0,0);
    t(2,2) -= t(2,1)*t(0,2);
    t(1,1) = 0;  
    t(2,1) = 0;
  }
  else
  {
    t(1,0) -= t(1,2)*t(0,0);
    t(1,1) -= t(1,2)*t(0,1);
    t(2,0) -= t(2,2)*t(0,0);
    t(2,1) -= t(2,2)*t(0,1);
    t(1,2) = 0;
    t(2,2) = 0;
  }
 
  // Compute the maximum-magnitude entry of the last two rows of the
  // row-reduced matrix.
  max = -1;
  maxRow = -1;
  maxCol = -1;
  for (row = 1; row < 3; row++)
  {
    for (col = 0; col < 3; col++)
    {
      absv = abs(t(row,col));
      if (absv > max)
      {
        max    = absv;
        maxRow = row;
        maxCol = col;
      }
    }
  }
  if (max == 0)
  {
    // The rank is 1. The eigenvalue has multiplicity 2.
    rank = 1;
    return qfeSuccess;
  }
 
  // If row 2 has the maximum-magnitude entry, swap it with row 1.
  if (maxRow == 2)
  {
    for (col = 0; col < 3; col++)
    {
      save      = t(1,col);
      t(1,col)  = t(2,col);
      t(2,col)  = save;
    }
  }
  // Scale the row containing the maximum to generate a 1-valued pivot.
  invMax = 1/t(maxRow,maxCol);
  t(1,0) *= invMax;
  t(1,1) *= invMax;
  t(1,2) *= invMax;
  
  // The rank is 2. The eigenvalue has multiplicity 1.
  rank =2;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Based on vtkMath::Jacobi
qfeReturnStatus qfeMath::qfeComputeJacobi(double **a, double *w, double **v)
{
  return qfeComputeJacobiN(a, 3, w, v);  
}

//----------------------------------------------------------------------------
// Based on vtkMath::JacobiN 
qfeReturnStatus qfeMath::qfeComputeJacobiN(double **a, int n, double *w, double **v)
{
  int i, j, k, iq, ip, numPos;
  double tresh, theta, tau, t, sm, s, h, g, c, tmp;
  double bspace[4], zspace[4];
  double *b = bspace;
  double *z = zspace;

  // only allocate memory if the matrix is large
  if (n > 4)
  {
    b = new double[n];
    z = new double[n]; 
  }

  // initialize
  for (ip=0; ip<n; ip++) 
  {
    for (iq=0; iq<n; iq++)
    {
      v[ip][iq] = 0.0;
    }
    v[ip][ip] = 1.0;
  }
  for (ip=0; ip<n; ip++) 
  {
    b[ip] = w[ip] = a[ip][ip];
    z[ip] = 0.0;
  }

  // begin rotation sequence
  for (i=0; i<20; i++) 
  {
    sm = 0.0;
    for (ip=0; ip<n-1; ip++) 
    {
      for (iq=ip+1; iq<n; iq++)
      {
        sm += fabs(a[ip][iq]);
      }
    }
    if (sm == 0.0)
    {
      break;
    }

    if (i < 3) // first 3 sweeps
    {
      tresh = (double)0.2*sm/(n*n);
    }
    else
    {
      tresh = (double)0.0;
    }

    for (ip=0; ip<n-1; ip++) 
    {
      for (iq=ip+1; iq<n; iq++) 
      {
        g = (double)(100.0*fabs(a[ip][iq]));

        // after 4 sweeps
        if (i > 3 && (fabs(w[ip])+g) == fabs(w[ip])
        && (fabs(w[iq])+g) == fabs(w[iq]))
        {
          a[ip][iq] = 0.0;
        }
        else if (fabs(a[ip][iq]) > tresh) 
        {
          h = w[iq] - w[ip];
          if ( (fabs(h)+g) == fabs(h))
          {
            t = (a[ip][iq]) / h;
          }
          else 
          {
            theta = (double)(0.5*h / (a[ip][iq]));
            t     = (double)(1.0 / (fabs(theta)+sqrt(1.0+theta*theta)));
            if (theta < 0.0)
            {
              t = -t;
            }
          }
          c   = (double)(1.0 / sqrt(1+t*t));
          s   = (double)(t*c);
          tau = (double)(s/(1.0+c));
          h   = t*a[ip][iq];
          z[ip] -= h;
          z[iq] += h;
          w[ip] -= h;
          w[iq] += h;
          a[ip][iq]=0.0;

          // ip already shifted left by 1 unit
          for (j = 0;j <= ip-1;j++) 
          {
            QFE_ROTATE(a,j,ip,j,iq);
          }
          // ip and iq already shifted left by 1 unit
          for (j = ip+1;j <= iq-1;j++) 
          {
            QFE_ROTATE(a,ip,j,j,iq);
          }
          // iq already shifted left by 1 unit
          for (j=iq+1; j<n; j++) 
          {
            QFE_ROTATE(a,ip,j,iq,j);
          }
          for (j=0; j<n; j++) 
          {
            QFE_ROTATE(v,j,ip,j,iq);
          }
        }
      }
    }

    for (ip=0; ip<n; ip++) 
    {
      b[ip] += z[ip];
      w[ip] = b[ip];
      z[ip] = 0.0;
    }
  }

  // sort eigenfunctions                 these changes do not affect accuracy 
  for (j=0; j<n-1; j++)                  // boundary incorrect
  {
    k = j;
    tmp = w[k];
    for (i=j+1; i<n; i++)                // boundary incorrect, shifted already
    {
      if (w[i] >= tmp)                   // why exchage if same?
      {
        k = i;
        tmp = w[k];
      }
    }
    if (k != j) 
    {
      w[k] = w[j];
      w[j] = tmp;
      for (i=0; i<n; i++) 
      {
        tmp = v[i][j];
        v[i][j] = v[i][k];
        v[i][k] = tmp;
      }
    }
  }
  // Ensure eigenvector consistency (i.e., Jacobi can compute vectors that
  // are negative of one another (.707,.707,0) and (-.707,-.707,0). This can
  // reek havoc in hyperstreamline/other stuff. We will select the most
  // positive eigenvector.
  int ceil_half_n = (n >> 1) + (n & 1);
  for (j=0; j<n; j++)
  {
    for (numPos=0, i=0; i<n; i++)
    {
      if ( v[i][j] >= 0.0 )
      {
        numPos++;
      }
    }

    if ( numPos < ceil_half_n)
    {
      for(i=0; i<n; i++)
        {
        v[i][j] *= -1.0;
      }
    }
  }

  if (n > 4)
  {
    delete [] b;
    delete [] z;
  }
  
  return qfeSuccess;
}


//----------------------------------------------------------------------------
// Based on vtkMath::JacobiN
qfeReturnStatus qfeMath::qfeComputeJacobiN(qfeFloat **a, int n, qfeFloat *w, qfeFloat **v)
{
  int i, j, k, iq, ip, numPos;
  qfeFloat tresh, theta, tau, t, sm, s, h, g, c, tmp;
  qfeFloat bspace[4], zspace[4];
  qfeFloat *b = bspace;
  qfeFloat *z = zspace;

  // only allocate memory if the matrix is large
  if (n > 4)
  {
    b = new qfeFloat[n];
    z = new qfeFloat[n]; 
  }

  // initialize
  for (ip=0; ip<n; ip++) 
  {
    for (iq=0; iq<n; iq++)
    {
      v[ip][iq] = 0.0;
    }
    v[ip][ip] = 1.0;
  }
  for (ip=0; ip<n; ip++) 
  {
    b[ip] = w[ip] = a[ip][ip];
    z[ip] = 0.0;
  }

  // begin rotation sequence
  for (i=0; i<20; i++) 
  {
    sm = 0.0;
    for (ip=0; ip<n-1; ip++) 
    {
      for (iq=ip+1; iq<n; iq++)
      {
        sm += fabs(a[ip][iq]);
      }
    }
    if (sm == 0.0)
    {
      break;
    }

    if (i < 3) // first 3 sweeps
    {
      tresh = (qfeFloat)0.2*sm/(n*n);
    }
    else
    {
      tresh = (qfeFloat)0.0;
    }

    for (ip=0; ip<n-1; ip++) 
    {
      for (iq=ip+1; iq<n; iq++) 
      {
        g = (qfeFloat)(100.0*fabs(a[ip][iq]));

        // after 4 sweeps
        if (i > 3 && (fabs(w[ip])+g) == fabs(w[ip])
        && (fabs(w[iq])+g) == fabs(w[iq]))
        {
          a[ip][iq] = 0.0;
        }
        else if (fabs(a[ip][iq]) > tresh) 
        {
          h = w[iq] - w[ip];
          if ( (fabs(h)+g) == fabs(h))
          {
            t = (a[ip][iq]) / h;
          }
          else 
          {
            theta = (qfeFloat)(0.5*h / (a[ip][iq]));
            t     = (qfeFloat)(1.0 / (fabs(theta)+sqrt(1.0+theta*theta)));
            if (theta < 0.0)
            {
              t = -t;
            }
          }
          c   = (qfeFloat)(1.0 / sqrt(1+t*t));
          s   = (qfeFloat)(t*c);
          tau = (qfeFloat)(s/(1.0+c));
          h   = t*a[ip][iq];
          z[ip] -= h;
          z[iq] += h;
          w[ip] -= h;
          w[iq] += h;
          a[ip][iq]=0.0;

          // ip already shifted left by 1 unit
          for (j = 0;j <= ip-1;j++) 
          {
            QFE_ROTATE(a,j,ip,j,iq);
          }
          // ip and iq already shifted left by 1 unit
          for (j = ip+1;j <= iq-1;j++) 
          {
            QFE_ROTATE(a,ip,j,j,iq);
          }
          // iq already shifted left by 1 unit
          for (j=iq+1; j<n; j++) 
          {
            QFE_ROTATE(a,ip,j,iq,j);
          }
          for (j=0; j<n; j++) 
          {
            QFE_ROTATE(v,j,ip,j,iq);
          }
        }
      }
    }

    for (ip=0; ip<n; ip++) 
    {
      b[ip] += z[ip];
      w[ip] = b[ip];
      z[ip] = 0.0;
    }
  }

  // sort eigenfunctions                 these changes do not affect accuracy 
  for (j=0; j<n-1; j++)                  // boundary incorrect
  {
    k = j;
    tmp = w[k];
    for (i=j+1; i<n; i++)                // boundary incorrect, shifted already
    {
      if (w[i] >= tmp)                   // why exchage if same?
      {
        k = i;
        tmp = w[k];
      }
    }
    if (k != j) 
    {
      w[k] = w[j];
      w[j] = tmp;
      for (i=0; i<n; i++) 
      {
        tmp = v[i][j];
        v[i][j] = v[i][k];
        v[i][k] = tmp;
      }
    }
  }
  // Ensure eigenvector consistency (i.e., Jacobi can compute vectors that
  // are negative of one another (.707,.707,0) and (-.707,-.707,0). This can
  // reek havoc in hyperstreamline/other stuff. We will select the most
  // positive eigenvector.
  int ceil_half_n = (n >> 1) + (n & 1);
  for (j=0; j<n; j++)
  {
    for (numPos=0, i=0; i<n; i++)
    {
      if ( v[i][j] >= 0.0 )
      {
        numPos++;
      }
    }

    if ( numPos < ceil_half_n)
    {
      for(i=0; i<n; i++)
        {
        v[i][j] *= -1.0;
      }
    }
  }

  if (n > 4)
  {
    delete [] b;
    delete [] z;
  }
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMath::qfeSwapVectors(qfeVector& v1, qfeVector& v2)
{
  for (int i = 0; i < 3; i++)
  {
    qfeFloat tmp = v1[i];
    v1[i] = v2[i];
    v2[i] = tmp;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMath::qfeComputeDistancePointPlane(qfeVector n, qfePoint o, qfePoint p, qfeFloat &d)
{
  d = abs(n*(p-o));

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMath::qfeMod(int x, int y, int &m)  
{
  int r = x%y;
  if(r<0)
    m = r+y;
  else
    m = r;

  return qfeSuccess;
}

