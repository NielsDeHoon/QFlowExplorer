#pragma once

#include <vector>

using namespace std;

/**
* \file   qfeSceneManager.h
* \author Roy van Pelt
* \class  qfeSceneManager
* \brief  Implements a scene manager
*
*/
#include "qfePatient.h"
#include "qfeScene.h"

#include "qfeDriverDefault.h"
#include "qfeDriverMPR.h"
#include "qfeDriverMIP.h"
#include "qfeDriverDVR.h"
#include "qfeDriverDA.h"
#include "qfeDriverIFR.h"
#include "qfeDriverUSR.h"
#include "qfeDriverVPR.h"
#include "qfeDriverHCR.h"
#include "qfeDriverPMR.h"
#include "qfeDriverFS.h"
#include "qfeDriverIV.h"
#include "qfeDriverFilter.h"
#include "qfeDriverCEV.h"

#include <vtkCommand.h>
#include <vtkRenderWindow.h>

enum qfeRenderState
{
  sceneStateStartup,
  sceneStateStarted,
  sceneStateRendering,
  sceneStateStopped
};

enum qfeScenePreset
{
  sceneDefault,
  sceneMPR,
  sceneMIP,
  sceneDVR,
  sceneIFR,  
  sceneVPR,
  sceneUSR,
  sceneHCR,
  scenePMR,
  sceneFS,
  sceneIV,
  sceneFilter,
  sceneCEV,
  sceneDA
};

class qfeSceneManager
{
public:
  qfeSceneManager();
  ~qfeSceneManager();

  qfeReturnStatus qfeSceneManagerInit(qfePatient *patient); 
  qfeReturnStatus qfeSceneManagerStart(int sceneType);  
  qfeReturnStatus qfeSceneManagerRender(float *time);
  qfeReturnStatus qfeSceneManagerStop();
  qfeReturnStatus qfeSceneManagerUpdate();

  qfeReturnStatus qfeSceneManagerRotate(double angle, bool autoupdate);
  qfeReturnStatus qfeSceneManagerRotate360(double angleStep, bool autoupdate, bool screenshot);

  qfeReturnStatus qfeSetSceneManagerRenderWindow(vtkRenderWindow *rw);
  qfeReturnStatus qfeSetSceneManagerViewGeometry(int direction);
  qfeReturnStatus qfeSetSceneManagerTemporalPhase(float phase);
  qfeReturnStatus qfeSetSceneManagerColorMap(int visualType, qfeColorMap map);
  qfeReturnStatus qfeSetSceneManagerLight(qfeLightSource light);
  qfeReturnStatus qfeSetSceneManagerDisplay(qfeDisplay display);
  qfeReturnStatus qfeSetSceneManagerFeatureTF(int id, QVector<QPair<double, double>> &tf, int dataType);
  
  qfeReturnStatus qfeGetSceneManagerScene(qfeScene **scene);
  
  qfeReturnStatus AddObserver(unsigned long sceneEvent, vtkCommand *command);

  qfeDriver *qfeGetDriver()
  {
	  return driver;
  }

protected:
  qfeReturnStatus qfeSceneManagerCreateDefault();
  qfeReturnStatus qfeSceneManagerCreateMPR(); 
  qfeReturnStatus qfeSceneManagerCreateMIP();
  qfeReturnStatus qfeSceneManagerCreateDVR();
  qfeReturnStatus qfeSceneManagerCreateIFR(); 
  qfeReturnStatus qfeSceneManagerCreateVPR(); 
  qfeReturnStatus qfeSceneManagerCreateUSR(); 
  qfeReturnStatus qfeSceneManagerCreateHCR();
  qfeReturnStatus qfeSceneManagerCreatePMR();
  qfeReturnStatus qfeSceneManagerCreateFS();
  qfeReturnStatus qfeSceneManagerCreateIV();
  qfeReturnStatus qfeSceneManagerCreateFilter();
  qfeReturnStatus qfeSceneManagerCreateCEV();
  qfeReturnStatus qfeSceneManagerCreateDA();

  qfeReturnStatus qfeSceneManagerSelectDriver(qfeScene *scene, qfeDriver **driver);

private:
  // State
  qfeRenderState    state;  

  // Patient variables
  qfePatient       *patient;
  int               currentCardiacPhase;

  // Scene variables
  qfeScene         *scene;    
  qfeDriver        *driver;
  qfeColorRGB       colorDefault1, colorDefault2;

  // Render variables
  vtkRenderWindow  *renderWindow;

  unsigned long     renderEvent;
  vtkCommand       *renderCommand;
};

