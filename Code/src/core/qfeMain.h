#ifndef __qfMain_h
#define __qfMain_h

/*! \mainpage QFlow Explorer
 *
 * \section intro_sec Introduction
 *
 * Explore 4D MRI blood-flow data.
 *
 * Currently supported scene presets:
 * - MPR: Multi Planar Reformat
 * - DVR: Direct Volume Rendering
 * - MIP: Maximum Intensity Projection
 * - IFR: Illustrative Flow Rendering
 * - VPR: Virtual Flow Probing
 * - USR: Ultrasound Rendering
 * - HCR: Hierarchical Clustering Rendering
 *
 * \section install_sec Quick Start
 *
 * \subsection step1 Step 1: Copy the build dir 
 * Copy the build dir to a suitable location (i.e. c:\qflowexplorer\). 
 * Make sure to include the necessary dll's (ADVSRImage, ADCenterlineRing, OpenGL, Glew, QPropertyBrowser, Wii, GLFont). 
 * In addition, make sure to have the following directories: shaders and prop
 * \subsection step2 Step 2: Start the prototype
 * Start the prototype by double-clicking QFlowExplorer.exe
 * \subsection step3 Step 3: Load the data
 * Load a single volume or a time series to start exploring the data
 *
 * \author Roy van Pelt
 */

#endif
