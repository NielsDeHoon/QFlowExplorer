#ifndef _qfeCallBackProgressBar_h
#define _qfeCallBackProgressBar_h

/**
* \brief  Structure for progressbar call data
*/
struct qfeCallData
{
  int   stepSize;
  char *message;
};

/**
* \author Roy van Pelt
* \brief  Abstract class for a progress bar execution
*/
class qfeCallBackProgressBar
{
public:
  virtual void Execute(qfeCallData cd) = 0;
};

#endif
