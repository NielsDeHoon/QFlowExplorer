/**
* \file   qfeCubic.h
* \author Roy van Pelt
* \class  qfeColorMap
* \brief  Implements a mapping from scalar values to colors
* \note   Confidential
*
* A color map defines a mapping of scalar values to colors.
* A color map contains a number of color mappings. (RGB, HLS)
*/
class qfeCubic
{
  private qfeVector a, b, c, d; // a + b*s + c*s^2 +d*s^3

  public qfeCubic(qfeVector a, qfeVector b, qfeVector c, qfeVector d)
  {
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
  }

  //evaluate the point using a cubic equation
  public qfeCubic GetPointOnSpline(float s)
  {
    return (((d * s) + c) * s + b) * s + a;
  }

  public static qfeVector[] CalculateCubicSpline(int n, vector<qfeRGBMapping> v)
  {
    qfeVector[] gamma = new qfeVector[n + 1];
    qfeVector[] delta = new qfeVector[n + 1];
    qfeVector[] D     = new qfeVector[n + 1];
    int i;
    /* We need to solve the equation
    * taken from: http://mathworld.wolfram.com/CubicSpline.html
    [2 1       ] [D[0]]   [3(v[1] - v[0])  ]
    |1 4 1     | |D[1]|   |3(v[2] - v[0])  |
    |  1 4 1   | | .  | = |      .         |
    |    ..... | | .  |   |      .         |
    |     1 4 1| | .  |   |3(v[n] - v[n-2])|
    [       1 2] [D[n]]   [3(v[n] - v[n-1])]

    by converting the matrix to upper triangular.
    The D[i] are the derivatives at the control points.
    */

    //this builds the coefficients of the left matrix
    //gamma[0] = qfeVector.Zero;
    gamma[0].X = 1.0f / 2.0f;
    gamma[0].Y = 1.0f / 2.0f;
    gamma[0].Z = 1.0f / 2.0f;
    gamma[0].W = 1.0f / 2.0f;

    qfeVector one;
    one.x = one.y = one.z = one.w = 1.0;
    for (i = 1; i < n; i++)
    {
      gamma[i] = one / ((4 * one) - gamma[i - 1]);
    }
    gamma[n] = one / ((2 * one) - gamma[n - 1]);

    delta[0] = 3 * (v[1].color - v[0].color) * gamma[0];
    for (i = 1; i < n; i++)
    {
      delta[i] = (3 * (v[i + 1].color - v[i - 1].color) - delta[i - 1]) * gamma[i];
    }
    delta[n] = (3 * (v[n].color - v[n - 1].color) - delta[n - 1]) * gamma[n];

    D[n] = delta[n];
    for (i = n - 1; i >= 0; i--)
    {
      D[i] = delta[i] - gamma[i] * D[i + 1];
    }

    // now compute the coefficients of the cubics
    qfeCubic[] C = new qfeCubic[n];
    for (i = 0; i < n; i++)
    {
      C[i] = new qfeCubic(v[i].color, D[i], 3 * (v[i + 1].color - v[i].color) - 2 * D[i] - D[i + 1],
        2 * (v[i].color - v[i + 1].color) + D[i] + D[i + 1]);
    }
    return C;
  }
}