#pragma once
/**
 * \file   qfeGLSupport.h
 * \author Roy van Pelt
 * \class  qfeGLSupport
 * \brief  Implements a static class with OpenGL support functions
 *
 * Implements a static class with OpenGL support functions
 */
#include <iostream>
#include <direct.h>
#include <stdio.h>
#include <stdlib.h>

#include <gl/glew.h>

#include "qflowexplorer.h"

using namespace std;

class qfeGLSupport
{
public:
  static qfeReturnStatus qfeCheckFrameBuffer();
  static qfeReturnStatus qfeCheckOpenGL();
  static qfeReturnStatus qfeCheckGLSL(unsigned int id, const char* name);

protected:
  qfeGLSupport(){};
  ~qfeGLSupport(){};
};
