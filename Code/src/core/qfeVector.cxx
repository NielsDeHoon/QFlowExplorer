#include "qfeVector.h"

//----------------------------------------------------------------------------
qfeVector::qfeVector()
{
  qfeSetVectorElements(1.0,1.0,1.0,0.0);
}

//----------------------------------------------------------------------------
qfeVector::qfeVector(qfeFloat *v)
{
  this->qfeSetVectorElements(v);
}

//----------------------------------------------------------------------------
qfeVector::qfeVector(qfeFloat x, qfeFloat y, qfeFloat z)
{
  this->qfeSetVectorElements(x,y,z,0.0);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeSetVectorElements(qfeFloat v[4])
{
  this->qfeSetVectorElements(v[0], v[1], v[2], v[3]);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeSetVectorElements(qfeFloat x, qfeFloat y, qfeFloat z)
{
  this->qfeSetVectorElements(x, y, z, 0.0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeSetVectorElements(qfeFloat x, qfeFloat y, qfeFloat z, qfeFloat w)
{
  this->x = x;
  this->y = y;
  this->z = z;
  this->w = w;

  this->e = -1.0;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeGetVectorElements(qfeFloat v[4])
{
  v[0] = this->x;
  v[1] = this->y;
  v[2] = this->z;
  v[3] = this->w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeFloat& qfeVector::operator [] ( int element )
{
  switch(element)
  {
    case 0: return x;
  		    break;
    case 1: return y;
		    break;
    case 2: return z;
		    break;
    case 3: return w;
		    break;
  }
  return e;
}

//----------------------------------------------------------------------------
qfeVector qfeVector::operator ^ ( const qfeVector & v ) const
{
  qfeVector u;
  qfeVector::qfeVectorCross((*this), v,  u);
  return u;
}

//----------------------------------------------------------------------------
const qfeFloat operator  * ( const qfeVector  & v1, const qfeVector &v2 )
{
  qfeFloat u;
  qfeVector::qfeVectorDot(v1, v2, u);
  return u;
}

//----------------------------------------------------------------------------
const qfeVector operator * ( const qfeVector  & v, const qfeFloat &f )
{
  qfeVector u;
  qfeVector::qfeVectorMultiply(f, v, u);
  return u;
}

//----------------------------------------------------------------------------
const qfeVector operator * ( const qfeFloat  & f, const qfeVector &v )
{
  qfeVector u;
  qfeVector::qfeVectorMultiply(f, v, u);
  return u;
}

//----------------------------------------------------------------------------
qfeVector qfeVector::operator / ( const qfeVector &v) const
{
  qfeVector u;
  qfeVector::qfeVectorDivide((*this), v,  u);
  return u;
}

//----------------------------------------------------------------------------
qfeVector qfeVector::operator - ( const qfeVector & v ) const
{
  qfeVector u;
  qfeVector::qfeVectorSubtract((*this), v,  u);
  return u;
}

qfeVector qfeVector::operator-() const
{
  qfeVector v;
  v.x = -x;
  v.y = -y;
  v.z = -z;
  return v;
}

//----------------------------------------------------------------------------
const qfeVector operator  + ( const qfeVector  & v1, const qfeVector &v2 )
{
  qfeVector u;
  qfeVector::qfeVectorAdd(v1, v2, u);
  return u;
}

//----------------------------------------------------------------------------
bool qfeVector::operator == ( const qfeVector & v ) const
{
  if((this->x == v.x) && (this->y == v.y) && (this->z == v.z) && (this->w == v.w)) return true;
  else return false;
}

//----------------------------------------------------------------------------
bool qfeVector::operator != ( const qfeVector & v ) const
{
  if((this->x == v.x) && (this->y == v.y) && (this->z == v.z) && (this->w == v.w)) return false;
  else return true;
}

//----------------------------------------------------------------------------
bool qfeVector::operator < ( const qfeVector & v ) const
{
  qfeFloat l1, l2;
  qfeVector::qfeVectorLength((*this), l1);
  qfeVector::qfeVectorLength(v, l2);
  return  l1 < l2;
}

//----------------------------------------------------------------------------
bool qfeVector::operator > ( const qfeVector & v ) const
{
  qfeFloat l1, l2;
  qfeVector::qfeVectorLength((*this), l1);
  qfeVector::qfeVectorLength(v, l2);
  return  l1 > l2;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorLength(qfeVector v1, qfeFloat &u)
{
  u = sqrt(v1.x*v1.x + v1.y*v1.y + v1.z*v1.z);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorNormalize(qfeVector &u)
{
  qfeFloat length = sqrt(u.x*u.x + u.y*u.y + u.z*u.z);

  if(length != 0){
    u.x = u.x / length;
    u.y = u.y / length;
    u.z = u.z / length;
    u.w = u.w;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorTensorProduct(qfeVector v1, qfeVector v2, qfeTensor3f &t)
{
  t(0,0) = v1.x*v2.x;
  t(0,1) = v1.x*v2.y;
  t(0,2) = v1.x*v2.z;

  t(1,0) = v1.x*v2.y;
  t(1,1) = v1.y*v2.y;
  t(1,2) = v1.y*v2.z;

  t(2,0) = v1.x*v2.z;
  t(2,1) = v1.y*v2.z;
  t(2,2) = v1.z*v2.z;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorCross(qfeVector v1, qfeVector v2, qfeVector &u)
{
  u.x = v1.y*v2.z - v1.z*v2.y;
  u.y = v1.z*v2.x - v1.x*v2.z;
  u.z = v1.x*v2.y - v1.y*v2.x;
  u.w = v1.w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorDot(qfeVector v1, qfeVector v2, qfeFloat &u)
{
  u = (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorMultiply(qfeFloat f, qfeVector v, qfeVector &u)
{
  u.x = f * v.x;
  u.y = f * v.y;
  u.z = f * v.z;
  u.w = v.w; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorDivide(qfeVector v1, qfeVector v2, qfeVector &u)
{
  u.x = v1.x / v2.x;
  u.y = v1.y / v2.y;
  u.z = v1.z / v2.z;
  u.w = v1.w; 

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorSubtract(qfeVector v1, qfeVector v2, qfeVector &u)
{
  u.x = v1.x - v2.x;
  u.y = v1.y - v2.y;
  u.z = v1.z - v2.z;
  u.w = v1.w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorAdd(qfeVector v1, qfeVector v2, qfeVector &u)
{
  u.x = v1.x + v2.x;
  u.y = v1.y + v2.y;
  u.z = v1.z + v2.z;
  u.w = v1.w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorOrthonormalBasis1(qfeVector v, qfeVector u, qfeVector &w)
{
  qfeVector::qfeVectorCross(v,u,w);
  qfeVector::qfeVectorNormalize(w);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorOrthonormalBasis2(qfeVector v, qfeVector &u, qfeVector &w)
{
  qfeVector s, sabs;

  s    = v;
  qfeVector::qfeVectorNormalize(s);

  sabs.qfeSetVectorElements(abs(s.x), abs(s.y), abs(s.z));
  u    = s;

  if((sabs.x <= sabs.y) && (sabs.x <= sabs.z))  
    u.qfeSetVectorElements(0.0, -s.z, s.y); 
  else
    if((sabs.y <= sabs.x) && (sabs.y <= sabs.z))  
      u.qfeSetVectorElements(-s.z, 0.0, s.x);  
    else
      if((sabs.z <= sabs.x) && (sabs.z <= sabs.y))  
        u.qfeSetVectorElements(-s.y, s.x, 0.0);  

  qfeVector::qfeVectorNormalize(u);  
  qfeVector::qfeVectorCross(s,u,w);
  qfeVector::qfeVectorNormalize(w);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorInterpolateLinear(qfeVector v1, qfeVector v2, qfeFloat t, qfeVector &u)
{
  u = v1 * ((qfeFloat)1.0-t) + v2 * t;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorInterpolateLinearSeparate(qfeVector v1, qfeVector v2, qfeFloat t, qfeVector &u)
{
  qfeVector v1n, v2n, un;
  qfeFloat  s1n, s2n, s;

  v1n = v1;
  v2n = v2;

  qfeVectorNormalize(v1n);
  qfeVectorNormalize(v2n);
  qfeVectorLength(v1, s1n);
  qfeVectorLength(v2, s2n);

  un = v1n * ((qfeFloat)1.0-t) + v2n * t;
  s  = s1n * ((qfeFloat)1.0-t) + s2n * t;

  u  = un * s;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorInterpolateSphericalLinear(qfeVector v1, qfeVector v2, qfeFloat t, qfeVector &u)
{
  qfeVector v1n, v2n;
  qfeFloat  alpha;

  v1n = v1;
  v2n = v2;

  qfeVectorNormalize(v1n);
  qfeVectorNormalize(v2n);

  alpha = acos(v1n*v2n);

  u = v1 * (qfeFloat)(sin((1.0-t)*alpha)/sin(alpha)) +
      v2 * (qfeFloat)(sin(     t *alpha)/sin(alpha));

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorClear(qfeVector &v)
{
  v.x = 0.0;
  v.y = 0.0;
  v.z = 0.0;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeVector::qfeVectorPrint(qfeVector v)
{
  cout << "| " << v.x << ", " << v.y << ", " << v.z << ", " << v.w << " |";
  cout << endl;
  return qfeSuccess;
}
