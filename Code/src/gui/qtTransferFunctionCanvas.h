/**
* \file   qtTransferFunctionCanvas.h
* \author Roy van Pelt
* \class  qtTransferFunctionCanvas
* \brief  Implements a 1D transfer function canvas
*
*/

#ifndef __qtTransferFunctionCanvas_h
#define __qtTransferFunctionCanvas_h

#include <algorithm>
#include <QtGui>

#include <iostream>
#include "qfeHistogram.h"
using namespace std;

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

typedef  QVector< int >  QHistogram;

enum qtColorInterpolation {
  qtColorInterpolationNearest,
  qtColorInterpolationLinear,
};

enum qtColorSpace {
  qtColorSpaceRGB,
  qtColorSpaceLuv,
  qtColorSpaceLab, 
};

/**
* \brief Structure for RGB color
*/
struct qtColorRGB {
  double r, g, b;
};

/**
* \brief Structure for XYZ color
*/
struct qtColorXYZ {
  double X, Y, Z;
};

/**
* \brief Structure for Lab color
*/
struct qtColorLab {
  double L, a, b;
};

/**
* \brief Structure for Luv color
*/
struct qtColorLuv {
  double L, u, v;
};

//----------------------------------------------------------------------------
class qtTransferFunctionCanvas : public QWidget
{
	Q_OBJECT

public:
	qtTransferFunctionCanvas(QWidget *parent = 0);
	~qtTransferFunctionCanvas();
	QSize minimumSizeHint() { return QSize(256, 128); }
	QSize sizeHint() { return QSize(256, 128); }
	
  void  addHistogram(qfeHistogram *histogram);
  void  clearHistograms();
  void  setHistogramScale(int scale);
  void  setHistogramScaleType(int logarithmic);
  void  setHistogramNumber(int current);
  void  setHistogramVisibility(int current);
  void  getColorInterpolation(int &mode);
  void  setColorInterpolation(int mode);
  void  getColorQuantization(int &steps);
  void  setColorQuantization(int steps);  
  void  getColorSpace(int &space);
  void  setColorSpace(int space);  
  void  setColorLightnessConstant(bool constant);
  void  getColorLightnessConstant(bool &constant);
  void  setColorPadding(bool padding);
  void  getColorPadding(bool &padding);
  void  getTransferFunction(QVector< QPair<double, double*> > &color, QVector< QPair<double, double> > &opacity, QVector< QPair<double, double> > &gradient);
  void  setTransferFunction(QVector< QPair<double, double*> > color, QVector< QPair<double, double> > opacity, QVector< QPair<double, double> > &gradient);

  void  setAxisX(QString label, double min, double max);
  void  setAxisY(QString label, double min, double max);  

signals:
	void transferFunctionChanged();

protected:
  int    PointSize;
  int    oSelected, gSelected, cSelected, crSelected;

  int    oEditorWidth;     // QRect
  int    oEditorHeight;
  int    oEditorOrigin[2];
  int    oEditorMargin[4]; // left, right, top, bottom
  int    gEditorWidth;
  int    gEditorHeight;
  int    gEditorOrigin[2];
  int    gEditorMargin[4];
  int    cEditorWidth;
  int    cEditorHeight;
  int    cEditorOrigin[2];
  int    cEditorMargin[4]; 

  // qfeTranferFunctionCanvasParameters struct
  int          interpolation;
  int          colorspace;
  int          quantization;
  int          histogramscale;
  int          histogramscaletype;
  int          histogramvisible;
  bool         constantlightness;
  bool         paddinguniform;
  qtColorRGB   paddingcolor;

  int          currentHistogramIndex;

  QString labelX;
  QString labelY;
  QString labelCount;
  double  rangeX[2];
  double  rangeY[2];

  QColorDialog *ColorDialog;
  QWidget      *Canvas;

  std::vector<qfeHistogram*>        Histograms;    
  QVector< QPair<double, double>  > OpacityPoints;  
  QVector< QPair<double, double>  > GradientPoints;  
  QVector< double >                 ColorRangePoints;
  QVector< QPair<double, double*> > ColorPoints;
  QVector< qtColorRGB >             ColorGradient;
  QVector< qtColorRGB >             ColorQuantizedGradient;

  QVector< QPolygonF >              HistogramPoly;

  void initTransferFunction();  

  void resizeEvent(QResizeEvent *event);
	void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void mouseDoubleClickEvent(QMouseEvent *event);
	void mouseMoveEvent(QMouseEvent *event);
	void paintEvent(QPaintEvent *event);
  
  void paintCheckerboard(QPainter *p, int o[], int w, int h);
  void paintAxis(QPainter *p, int o[], int w, int h);
  void paintColorBar(QPainter *p, int o[]);
  void paintColorBarNearest(QPainter *p, int o[]);
  void paintColorBarLinear(QPainter *p,  int o[]);
  void paintColorBarLinearRgb(QPainter *p,  int o[]);
  void paintColorOpacityMap(QPainter *p);
  void paintHistogram(QPainter *p);
  void paintOpacityPoints(QPainter *p, int o[], int w, int h, QVector< QPair<double, double> > points);
  void paintColorPoints(QPainter *p);
  void paintColorRangePoints(QPainter *p);
  void paintLabelX(QPainter *p, int o[], int w, int h, QString label);
  void paintLabelY(QPainter *p, int o[], int w, int h, QString label);
  void paintLabelCount(QPainter *p);

  QPolygon getTriangle(const QRect &rect);

  bool insideOpacityEditor(QPointF pos, bool borderX, bool borderY);
  int  insideColorEditor(QPointF pos, bool borderX, bool borderY);  
  bool insideGradientEditor(QPointF pos, bool borderX, bool borderY);
  int  selectOpacityPoint(QPointF pos);
  int  selectGradientPoint(QPointF pos);
  int  selectColorPoint(QPointF pos);
  int  selectColorRangePoint(QPointF pos);

  void      updateHistogramPoly();
  QPolygonF computeHistogramPoly(qfeHistogram *histogram);

private:
  void computeLinearGradient(QVector< QPair<double, double*> > ColorPoints, QVector< qtColorRGB > &gradient, int size, int colorspace);
  void computeQuantizedGradient(QVector< qtColorRGB > gradient, QVector< qtColorRGB > &quantized, int size);

  void convertRGB2XYZ(qtColorRGB in, qtColorXYZ &out); 
  void convertXYZ2RGB(qtColorXYZ in, qtColorRGB &out); 

  void convertXYZ2Lab(qtColorXYZ in, qtColorLab &out, const qtColorXYZ white);
  void convertLab2XYZ(qtColorLab in, qtColorXYZ &out, const qtColorXYZ white);

  void convertXYZ2Luv(qtColorXYZ in, qtColorLuv &out, const qtColorXYZ white);
  void convertLuv2XYZ(qtColorLuv in, qtColorXYZ &out, const qtColorXYZ white);
  
  static double       linearize_sRGB_component(double in);
  static double       delinearize_sRGB_component(double in);
  static qtColorRGB   linearize_sRGB(const qtColorRGB &in);
  static qtColorRGB   delinearize_sRGB(const qtColorRGB &in);

  double       forwardLabTransform(double in);
  qtColorRGB   clamp(qtColorRGB in);
  double       clamp(double in);
  qtColorRGB   lerp(qtColorRGB value1, qtColorRGB value2, double amount);
  qtColorLab   lerp(qtColorLab value1, qtColorLab value2, double amount);
  qtColorLuv   lerp(qtColorLuv value1, qtColorLuv value2, double amount);
  double       lerp(double value1, double value2, double amount);

  void         updateColorRange(int selectedCR);
};


#endif
