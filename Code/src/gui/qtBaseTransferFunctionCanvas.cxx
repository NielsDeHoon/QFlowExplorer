#include "qtBaseTransferFunctionCanvas.h"

qtBaseTransferFunctionCanvas::qtBaseTransferFunctionCanvas(QWidget *parent) : QWidget(parent)
{
  PointSize            =  4;
  oSelected            = -1;
  gSelected            = -1;
  cSelected            = -1;
  crSelected           = -1;

  oEditorWidth         =  0;
  oEditorHeight        =  0;
  oEditorOrigin[0]     =  0;
  oEditorOrigin[1]     =  0;
  oEditorMargin[0]     = 30; // left
  oEditorMargin[1]     = 30; // right
  oEditorMargin[2]     =  5; // top
  oEditorMargin[3]     = 10; // bottom

  quantization         = 7;
  histogramscale       = 50;
  histogramscaletype   = 1;
  histogramvisible     = 1;
  constantlightness    = false;
  paddinguniform       = false;

  currentHistogramIndex     = 0;

  labelX               = "x - axis";
  labelY               = "y - axis";
  rangeX[0]            = rangeY[0] = 0.0;
  rangeX[1]            = rangeY[1] = 1.0;

  dataType             = 0;

  HistogramPoly.clear();
  Canvas      = new QWidget();
  for (unsigned i = 0; i < histograms.size(); i++)
    delete histograms[i];
  histograms.clear();
  setMouseTracking(true);
  initTransferFunction();
}

//----------------------------------------------------------------------------
qtBaseTransferFunctionCanvas::~qtBaseTransferFunctionCanvas()
{
  delete Canvas;
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::addHistogram(qfeHistogram *histogram)
{
  histograms.push_back(histogram);
  
  emit transferFunctionChanged();
  updateHistogramPoly();
  update();
}

void qtBaseTransferFunctionCanvas::addHistograms(const std::vector<qfeHistogram*> &histograms)
{
  for (unsigned i = 0; i < histograms.size(); i++) {
    this->histograms.push_back(histograms[i]);
  }

  emit transferFunctionChanged();
  updateHistogramPoly();
  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::clearHistograms()
{
  for (unsigned i = 0; i < histograms.size(); i++)
    delete histograms[i];
  histograms.clear();
  updateHistogramPoly();
  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::setHistogramScale(int scale)
{
  histogramscale = scale;
  
  //emit transferFunctionChanged();
  updateHistogramPoly();
  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::setHistogramScaleType(int logarithmic)
{
  histogramscaletype = logarithmic;
  
  //emit transferFunctionChanged();
  updateHistogramPoly();
  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::setHistogramNumber(int current)
{
  currentHistogramIndex = MIN(current, (int)this->histograms.size()-1);
  currentHistogramIndex = MAX(currentHistogramIndex, 0);

  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::setHistogramVisibility(int value)
{
  if(value > 0)
    histogramvisible = 1;
  else
    histogramvisible = 0;

  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::getTransferFunction(QVector< QPair<double, double> > &opacity, int &dataType)
{
  opacity  = this->OpacityPoints;
  dataType = this->dataType;
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::setTransferFunction(QVector< QPair<double, double> > &opacity, int dataType)
{
  OpacityPoints  = opacity;
  this->dataType = dataType;
  update();

  emit transferFunctionChanged();
}

void qtBaseTransferFunctionCanvas::addTransferFunctionPoint(float x, float y)
{
  if (histograms.size() <= 0)
    return;

  float range[2];
  histograms[0]->qfeGetRange(range[0], range[1]);
  float t = (x - range[0]) / (range[1] - range[0]);
  t = clamp(t, 0.f, 1.f);
  y = clamp(y, 0.f, 1.f);
  
  OpacityPoints.push_back(QPair<double, double>(t, y));
  qSort(OpacityPoints.begin(),  OpacityPoints.end());
  emit transferFunctionChanged();
}

void qtBaseTransferFunctionCanvas::setDataType(int dataType)
{
  this->dataType = dataType;
  emit transferFunctionChanged();
}

void qtBaseTransferFunctionCanvas::setNrBins(int nrBins)
{
  for (unsigned i = 0; i < histograms.size(); i++) {
    histograms[i]->qfeSetNrOfBins(nrBins);
    histograms[i]->qfeUpdate();
  }
  updateHistogramPoly();
  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::setAxisX(QString label, double min, double max)
{
  labelX    = label;
  rangeX[0] = min;
  rangeX[1] = max;
  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::setAxisY(QString label, double min, double max)
{
  labelY    = label;
  rangeY[0] = min;
  rangeY[1] = max;
  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::initTransferFunction()
{
  // store opacity and color points in internal lists. this will
  // allow easy adding/removing and sorting
  OpacityPoints.clear();

  // add opacity points
  OpacityPoints.push_back(QPair<double, double>(0.0, 1.0));
  OpacityPoints.push_back(QPair<double, double>(1.0, 1.0));

  resizeEvent(NULL);
  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::mousePressEvent(QMouseEvent *event)
{
  bool insideO = insideOpacityEditor(event->pos(), true, true);

  // return quietly if user clicked outside drawing area
  if (OpacityPoints.size() == 0 || !insideO)
    return;

  if (insideO)
    oSelected = selectOpacityPoint(event->pos());

  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::mouseReleaseEvent(QMouseEvent *event)
{
  qSort(OpacityPoints.begin(),  OpacityPoints.end());

  oSelected  = -1;

  // notify render window that TF has changed
  emit transferFunctionChanged();
  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::mouseDoubleClickEvent(QMouseEvent *event)
{
  bool insideO = insideOpacityEditor(event->pos(), true, true);
  
  // return quietly if user clicked outside drawing area
  if (OpacityPoints.size() == 0 || !insideO)
    return;

  // return quietly if user clicked with right mouse button
  if (event->button() == Qt::RightButton)
    return;
  
  // double click inside the opacity editor
  if(insideO) {
    int selectedO = selectOpacityPoint(event->pos());

    // Add new opacity point
    if(selectedO == -1) {
      QPointF p(MAX(0.0,       ((event->x() - oEditorOrigin[0]) / (double)oEditorWidth)), 
                MIN(1.0, 1.0 - ((event->y() - oEditorOrigin[1]) / (double) (oEditorHeight+1))));
     
      OpacityPoints.push_back(QPair<double, double>(p.x(), p.y()));      
    } else {
      // Do not allow to delete outermost points
      if ((selectedO != 0) && (selectedO != OpacityPoints.size()-1 )) {
        OpacityPoints.erase(OpacityPoints.begin() + selectedO);
      }
    }
    qSort(OpacityPoints.begin(), OpacityPoints.end());
  }
    
  if (insideO)
    emit transferFunctionChanged();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::mouseMoveEvent(QMouseEvent *event)
{
  bool insideO = insideOpacityEditor(event->pos(), false, false);

  // Output the current value
  if (insideO && (this->histograms.size() > 0)){
    double x = event->x();

    if(x < oEditorOrigin[0])                 x = oEditorOrigin[0];
    if(x > oEditorOrigin[0] + oEditorWidth)  x = oEditorOrigin[0] + oEditorWidth;

    qfeHistogram *currentHistogram = histograms[currentHistogramIndex];
    float rangeMin, rangeMax;
    currentHistogram->qfeGetRange(rangeMin, rangeMax);

    double t = (x - oEditorOrigin[0]) / oEditorWidth;
    double value = rangeMin + t * (rangeMax - rangeMin);

    unsigned binIndex = currentHistogram->qfeGetBin(value);
    unsigned count = currentHistogram->qfeGetCount(binIndex);

    // \todo refresh also when update() is called, not only on mouse move
    labelCount.sprintf("%4d - # %7d", (int)value, count);    
  }

  // Move opacity points
  if(oSelected != -1) {
    // make sure we keep within the editor range
    double currentX = event->x();
    double currentY = event->y();

    if(currentX < oEditorOrigin[0])                 currentX = oEditorOrigin[0];
    if(currentX > oEditorOrigin[0] + oEditorWidth)  currentX = oEditorOrigin[0] + oEditorWidth;
    if(currentY < oEditorOrigin[1])                 currentY = oEditorOrigin[1];
    if(currentY > oEditorOrigin[1] + oEditorHeight) currentY = oEditorOrigin[1] + oEditorHeight;

    // get value and opacity associated with user point
    double x =       ((currentX - oEditorOrigin[0]) / (double) oEditorWidth);
    double y = 1.0 - ((currentY - oEditorOrigin[1]) / (double) oEditorHeight);

    // update selected opacity
    QPair<double, double> oPoint = OpacityPoints.at(oSelected);

    // fix outermost points
    if ((oSelected != 0) && (oSelected != OpacityPoints.size()-1 ))
    {
      // do not allow overlapping points
      if(x < OpacityPoints.at(oSelected-1).first) x = OpacityPoints.at(oSelected-1).first;
      if(x > OpacityPoints.at(oSelected+1).first) x = OpacityPoints.at(oSelected+1).first;

      oPoint.first  = x;
    }    
    oPoint.second = y;
    
    OpacityPoints[oSelected] = oPoint;
  }

  update();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::resizeEvent(QResizeEvent *event)
{  
    // Assumption that cEditorHeight is set and fixed
    oEditorWidth     = (int)(width()  - oEditorMargin[0] - oEditorMargin[1]);
    oEditorHeight    = (int)(height() * 0.95f - oEditorMargin[2] - oEditorMargin[3]);
    oEditorHeight    = max(0, oEditorHeight);
    oEditorOrigin[0] = (int) oEditorMargin[0];
    oEditorOrigin[1] = (int) oEditorMargin[2];

    this->updateHistogramPoly();
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::paintEvent(QPaintEvent *event)
{
	QPainter p(this);
  p.setRenderHint(QPainter::Antialiasing, true);
  p.setRenderHint(QPainter::HighQualityAntialiasing, true);
  
  // draw border	
  p.setPen(QPen(Qt::black));
	p.fillRect(0, 0, width(), height(), QBrush(QColor(224, 223, 227, 0)));  

  // draw checkerboard pattern
  this->paintCheckerboard(&p, oEditorOrigin, oEditorWidth, oEditorHeight);

  // draw the axis
  this->paintAxis(&p, oEditorOrigin, oEditorWidth, oEditorHeight);

  if (rangeX[0] < 0.f)
    this->paintZeroLine(&p, oEditorOrigin, oEditorWidth, oEditorHeight);

  // draw the labels
  this->paintLabelX(&p, oEditorOrigin, oEditorWidth, oEditorHeight, this->labelX);
  this->paintLabelY(&p, oEditorOrigin, oEditorWidth, oEditorHeight, this->labelY);
  this->paintLabelCount(&p);
	
  // draw the Histogram
  if(this->histogramvisible == 1)
    this->paintHistogram(&p);

  // draw opacity transfer function
  this->paintOpacityPoints(&p, oEditorOrigin, oEditorWidth, oEditorHeight, OpacityPoints);
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::paintAxis(QPainter *p, int o[], int w, int h)
{
  p->setPen(Qt::black);

  // Vertical Axis
  // Main line
  p->drawLine(o[0] - 0, o[1], 
              o[0] - 0, o[1] + h - 1);

  // Three ticks
  for(int i = 0; i<3; i++)
  {
    p->drawLine(o[0] + 2 , o[1] + (int)((h-1) * (i/2.0)), 
                o[0] - 2 , o[1] + (int)((h-1) * (i/2.0)));     
  }

  QString maxY, halfY, minY;

  maxY.sprintf("%.1f" , this->rangeY[1]);
  halfY.sprintf("%.1f", this->rangeY[1]/2.0);
  minY.sprintf("%.1f" , this->rangeY[0]);

  // Text
  p->drawText(o[0] - 23, o[1] + 4, maxY);
  p->drawText(o[0] - 23, o[1] + 4 + (int)(h / 2.0), halfY);
  p->drawText(o[0] - 23, o[1] + 4 + (int) h       , minY);

  // Horizontal Axis
  // Main line
  p->drawLine(o[0]        , o[1] + h - 1, 
              o[0] + w - 1, o[1] + h - 1);

  // Ticks
  QString tickText;
  int  n = 2;
  for(int i = 0; i<n; i++)
  {
    p->drawLine(o[0] + (int)((w-1) * (i/(double)(n-1))), o[1] + h - 1 - 2, 
                o[0] + (int)((w-1) * (i/(double)(n-1))), o[1] + h - 1 + 2);     

    tickText.sprintf("%.1f", (i/(double)(n-1))*(this->rangeX[1]-this->rangeX[0]) + this->rangeX[0]);

    p->drawText(o[0] + (int)((w-1) * (i/(double)(n-1))) - 8, o[1] + h - 1 + 15, tickText);
  }
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::paintCheckerboard(QPainter *p, int o[], int w, int h)
{
  int size =18;
  QImage image(w, h, QImage::Format_RGB32);
  image.fill(qRgb(255, 255, 255));
  for (int jj=0 ; jj<h ; ++jj)
  {
    for (int ii=0 ; ii<w ; ++ii)
    {
      if ( (((jj % size) >= 0) && ((jj % size) < (int)size/2)))
      {
        if ( (((ii % size) >= (int)size/2) && ((ii % size) < size))) 
          image.setPixel(ii, jj, qRgb(224, 224, 224));
      }
      else
      {
        if ( (((ii % size) >=  0) && ((ii % size) < int(size/2))))   
          image.setPixel(ii, jj, qRgb(224, 224, 224));
      }
    }
  }
  QRect srcRect(o[0], o[1], w - 1, h -1);

  p->drawImage(srcRect,image);
}


//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::paintHistogram(QPainter *p)
{
  if ( this->HistogramPoly.size() <= 0 ) return;
  if ( this->HistogramPoly[currentHistogramIndex].size() < 0 ) return;

  p->setPen(QColor(0, 0, 0));
  p->setBrush(QColor(100, 100, 100));
  p->drawPolygon(this->HistogramPoly[currentHistogramIndex]);  
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::paintOpacityPoints(QPainter *p, int o[], int w, int h, QVector< QPair<double, double> > points)
{
  QBrush lineBrush(QColor(192, 192, 192));
  QBrush pointBrush(QColor(255, 0, 0));

  QVector< QPair<double, double> >::iterator oIter = points.begin();

  p->setPen(QPen(QColor(60,60,60), 1.5f, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

  QPointF p1;
  double  o1;
  for ( int i = 0; i < (int)points.size(); i++ )
  {
    QPointF p2(o[0] +     (oIter->first  * w), 
               o[1] + h - (oIter->second * h));
    double  o2 = oIter->second;

    if(i==0)
    {
      p1 = p2;
      o1 = o2;
    }
    else
    {       
      p->setBrush(lineBrush);
      p->drawLine(p1, p2);
    }

    //pointBrush.setColor(QColor(255,255,255));
    pointBrush.setColor(QColor((1.0-o1)*255,(1.0-o1)*255,(1.0-o1)*255));
    p->setBrush(pointBrush);
    p->drawRoundedRect(QRect(p1.x()-PointSize, p1.y()-PointSize, 2*PointSize+1, 2*PointSize+1), PointSize/2, PointSize/2);

    p1 = p2;
    o1 = o2;
    oIter++;
  }
  
  pointBrush.setColor(QColor((1.0-o1)*255,(1.0-o1)*255,(1.0-o1)*255));
  p->setBrush(pointBrush);
  p->drawRoundedRect(QRect(p1.x()-PointSize, p1.y()-PointSize, 2*PointSize+1, 2*PointSize+1), PointSize/2, PointSize/2);
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::paintLabelX(QPainter *p, int o[], int w, int h, QString label)
{
  p->drawText(o[0] + 0.5*w - 20, o[1] + h + 12,  label);
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::paintLabelY(QPainter *p, int o[], int w, int h, QString label)
{
  p->save();
  p->translate(o[0] + w + 10, o[1] + 0.5*h - 20);
  p->rotate(90);
  p->drawText(0, 0, label);
  p->restore();  
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::paintLabelCount(QPainter *p)
{
  if(this->histograms.size() > 0)
    p->drawText(oEditorOrigin[0] + oEditorWidth - 110, oEditorOrigin[1] + 15,  this->labelCount);
}

void qtBaseTransferFunctionCanvas::paintZeroLine(QPainter *p, int o[], int w, int h)
{
  QPen linePen(QColor(180, 180, 180));
  p->setPen(linePen);
  float t = -rangeX[0] / (rangeX[1] - rangeX[0]);
  int x = o[0] + t*w;
  p->drawLine(x, o[1], x, o[1] + h - 1);
  p->drawText(x, o[1]+h+13, "0");
  QPen defaultPen(QColor(0, 0, 0));
  p->setPen(defaultPen);
}

//----------------------------------------------------------------------------
QPolygon qtBaseTransferFunctionCanvas::getTriangle(const QRect &rect)
{
  QPolygon result(3);
  result.setPoint(0, rect.center().x(), rect.top());
  result.setPoint(1, rect.right(), rect.bottom());
  result.setPoint(2, rect.left(), rect.bottom());
  return result;
}

//----------------------------------------------------------------------------
int qtBaseTransferFunctionCanvas::selectOpacityPoint(QPointF pos)
{
  int point = -1;

  QVector< QPair<double, double> >::iterator oIter = OpacityPoints.begin();

  for ( int i = 0; i < (int)OpacityPoints.size(); i++ )
  {
    QPointF p1(oEditorOrigin[0] +                 (oIter->first  * oEditorWidth), 
               oEditorOrigin[1] + oEditorHeight - (oIter->second * oEditorHeight));

    QPointF p = pos - p1;
    float dist = (float) sqrt(p.x()*p.x() + p.y()*p.y());
    if ( dist <= PointSize )
    {
      point = i;
      break;
    }

    oIter++;
  }

  return point;
}

//----------------------------------------------------------------------------
bool qtBaseTransferFunctionCanvas::insideOpacityEditor(QPointF pos, bool borderX, bool borderY)
{
  bool result = false;
  int incX = (borderX==true) ? 1 : 0;
  int incY = (borderY==true) ? 1 : 0;  

  if(pos.x() >= oEditorOrigin[0]                 - incX*PointSize && 
     pos.x() <= oEditorOrigin[0] + oEditorWidth  + incX*PointSize   &&
     pos.y() >= oEditorOrigin[1]                 - incY*PointSize  && 
     pos.y() <= oEditorOrigin[1] + oEditorHeight + incY*PointSize   )
  {
    result = true;
  }  
  return result;
}

//----------------------------------------------------------------------------
void qtBaseTransferFunctionCanvas::updateHistogramPoly()
{
  HistogramPoly.clear();

  for (unsigned i = 0; i < histograms.size(); i++)
    HistogramPoly.push_back(computeHistogramPoly(histograms[i]));
}

//----------------------------------------------------------------------------
QPolygonF qtBaseTransferFunctionCanvas::computeHistogramPoly(qfeHistogram *histogram)
{
  QPolygonF path; 
  path.clear();
  int scale = max(1, histogramscale);

  if (histogram->qfeGetNrOfBins() <= 0) return path; 
  unsigned maxCount = histogram->qfeGetMaxCount();

  path << QPointF(oEditorOrigin[0], oEditorOrigin[1]+oEditorHeight);
  for ( int i = 0; i < histogram->qfeGetNrOfBins(); i++ )
  {
    float probability;

    if(histogramscaletype > 0)
      probability = log((float)histogram->qfeGetCount(i)) /log((float)maxCount);
    else
      probability = (float)histogram->qfeGetCount(i) / (float)maxCount;

    probability = probability * scale/100.0f;

    probability = MIN(probability, 1.0);
    probability = MAX(probability, 0.0);   

    float x = oEditorOrigin[0] + (i * (oEditorWidth / (float)(histogram->qfeGetNrOfBins()-1)));
    float y = oEditorOrigin[1] + oEditorHeight * (1.0-probability);

    path << QPointF(x, y);
  }
  path << QPointF(oEditorOrigin[0] + oEditorWidth - 1, oEditorOrigin[1] + oEditorHeight);

  return path;  
}

//----------------------------------------------------------------------------
double qtBaseTransferFunctionCanvas::clamp(double in)
{
  double out;
  out = (in<0.0)  ? 0.0 : in;
  out = (out>1.0) ? 1.0 : out;
  return out;  
}

float qtBaseTransferFunctionCanvas::clamp(float v, float min, float max)
{
  if (v < min) v = min;
  if (v > max) v = max;
  return v;
}

//----------------------------------------------------------------------------
double qtBaseTransferFunctionCanvas::lerp(double value1, double value2, double amount)
{
  return (value1 * (1.0 - amount) + value2  * amount);
}