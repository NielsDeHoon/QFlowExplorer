#include <vtkCanvasInteractorStyle.h>
#include <vtkCanvas.h>

#include <vtkObjectFactory.h>
#include <vtkRenderWindowInteractor.h>

vtkCxxRevisionMacro( vtkCanvasInteractorStyle, "$Revision: 1.0 $");
vtkStandardNewMacro( vtkCanvasInteractorStyle );

/////////////////////////////////////////////////////////////////
vtkCanvasInteractorStyle::vtkCanvasInteractorStyle()
{
	this->Canvas = 0;
}

/////////////////////////////////////////////////////////////////
vtkCanvasInteractorStyle::~vtkCanvasInteractorStyle()
{
	this->SetCanvas( 0 );
}

/////////////////////////////////////////////////////////////////
void vtkCanvasInteractorStyle::SetCanvas( vtkCanvas * canvas )
{
	if( this->Canvas == canvas )
	{
		if( this->Canvas == 0 )
			return;
		if( this->Canvas->GetInteractor() == this->GetInteractor() )
			return;
	}

	if( this->Canvas != 0 )
	{
		this->SetInteractor( this->Canvas->GetInteractor() );
		this->Canvas->UnRegister( this );
	}

	this->Canvas = canvas;

	if( this->Canvas != 0 )
	{
		this->Canvas->Register( this );
		this->SetInteractor( this->Canvas->GetInteractor() );
	}
	else
	{
		this->SetInteractor( 0 );
	}
}

/////////////////////////////////////////////////////////////////
vtkCanvas * vtkCanvasInteractorStyle::GetCanvas()
{
	return this->Canvas;
}

/////////////////////////////////////////////////////////////////
void vtkCanvasInteractorStyle::SetInteractor( vtkRenderWindowInteractor * interactor )
{
	this->vtkInteractorStyle::SetInteractor( interactor );
}
