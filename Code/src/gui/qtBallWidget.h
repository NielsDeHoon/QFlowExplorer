/**
* \file   qtBallWidget.h
* \author Roy van Pelt
* \class  qtBallWidget
* \brief  Implements a ball rotation widget
*
*/

#ifndef __qtBallWidget_h
#define __qtBallWidget_h

#include <algorithm>
#include <QtGui>

#include <QtGui/qvector3d.h>
#include <QtGui/qquaternion.h>

#define QFE_SQRT1_2 sqrtf(0.5)
#define QFE_SQRT2   sqrtf(2.0)

class qtBallWidget : public QWidget
{
	Q_OBJECT

public:
	qtBallWidget();
	~qtBallWidget();
	QSize minimumSizeHint() { return QSize(256, 128); }
	QSize sizeHint() { return QSize(256, 128); }

  void      setDirectionVector(QVector3D vector);
  QVector3D getDirectionVector();

signals:
  void ballWidgetChanged();

protected:
  QQuaternion rotationQuaternion;
  QVector4D   directionVector;  
  QVector4D   directionVectorScaled;

  double      ballRadius;
  double      ballDiameter;
  QVector2D   ballOrigin; 

  QVector2D   mouseLastPos;

  int widgetHeight;

  void resizeEvent(QResizeEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void mouseDoubleClickEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void paintEvent(QPaintEvent *event);
  void paintEventInit();

  void paintBall(QPainter *p);
  void paintFrontPoint(QPainter *p);
  void paintRearPoint(QPainter *p);
  void paintArrow(QPainter *p);

  QMatrix4x4 moveTrackball(QVector2D p1, QVector2D p2, float radius);

private :
  QVector2D translateMousePosition(QVector2D position);
  float     projectToSphere(QVector2D point, float radius);

};

#endif
