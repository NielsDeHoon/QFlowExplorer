#ifndef __vtkMedicalCanvas_h
#define __vtkMedicalCanvas_h

#include <vtkCanvas.h>

class vtkRenderer;
class vtkSubCanvas;

class vtkMedicalCanvas : public vtkCanvas
{
public:

	static vtkMedicalCanvas * New();
	vtkTypeRevisionMacro( vtkMedicalCanvas, vtkCanvas );

	virtual vtkRenderer  * GetRenderer3D();

	// Get 3D and 3D canvasses

	virtual vtkSubCanvas * GetSubCanvas3D();
	virtual vtkSubCanvas * GetSubCanvas2D( int index );
	virtual int GetSubCanvasIndex( vtkSubCanvas * canvas );

	// Sets layout of canvas

	virtual void SetLayoutToOrientedLeft();
  virtual void SetLayoutToOrientedLeft2();
	virtual void SetLayoutToOrientedRight();
  virtual void SetLayoutToOrientedRight2();
	virtual void SetLayoutToOrientedTop();
	virtual void SetLayoutToOrientedBottom();
	virtual void SetLayoutToTiled();

	virtual void SetLayout( int layout );
	virtual int GetLayout();

	// Sets given canvas to full screen size

	virtual void SetToFullScreen( vtkSubCanvas * canvas );
	virtual bool IsFullScreen();

	// Constants for specifying the layout

	static const int LAYOUT_ORIENTED_LEFT   = 0;
	static const int LAYOUT_ORIENTED_RIGHT  = 1;
	static const int LAYOUT_ORIENTED_TOP    = 2;
	static const int LAYOUT_ORIENTED_BOTTOM = 3;
	static const int LAYOUT_TILED           = 4;
  static const int LAYOUT_ORIENTED_LEFT2  = 5;
  static const int LAYOUT_ORIENTED_RIGHT2 = 6;

protected:

	vtkMedicalCanvas();
	virtual ~vtkMedicalCanvas();

	void InitLayouts();

private:

	typedef struct ViewportLayouts
	{
		double Layouts3D[7][4];
		double Layouts2D[3][7][4];
	} ViewportLayouts;

	vtkSubCanvas * Canvas3D;
	vtkSubCanvas * Canvas2D[3];

	ViewportLayouts Layouts;

	int Layout;
	bool FullScreen;
};

#endif