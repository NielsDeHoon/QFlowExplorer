#pragma once

#include "ui_qtMainWindow.h"
#include "qtAboutDialog.h"

#include <QFileDialog>
#include <QMainWindow>
#include <QMap>
#include <QProgressBar>
#include <QString.h>
#include <QTimer>
#include <QVTKWidget.h>

#include <stdlib.h>
#include <string.h>

#include "qCustomPlot.h"
#include "qtBallWidget.h"
#include "qtTransferFunctionWidget.h"
#include "qtBaseTransferFunctionCanvas.h"
#include "qfeKernel.h"

#include "qtpropertymanager.h"
#include "qtvariantproperty.h"
#include "qttreepropertybrowser.h"

#include "vtkEventQtSlotConnect.h"
//#include "vtkTDxInteractorStyleCamera.h"
//#include "vtkTDxInteractorStyleSettings.h"

#include "qfeHistogram.h"

#define MAX_PHASES 100

class qtProgressBarCallBack;

enum guiState
{
  startup,
  initpre,
  initdata,
  initpost,
  running
};

//using namespace ui;

/**
* \file   qtMainWindow.h
* \author Roy van Pelt
* \class  qtMainWindow
* \brief  Implements the main window of the QFlow Explorer
*/
class qtMainWindow : public QMainWindow
{
	Q_OBJECT
  friend class qtTransferFunctionDialog;
  public :
	  qtMainWindow();
	  ~qtMainWindow();

	  void guiInitialize();
    void guiUninitialize();
  
  public slots:
    void on_action_Load_Phases_triggered() {this->guiShowDialogBrowseAll();};
    void on_action_Load_Phase_triggered()  {this->guiShowDialogBrowseSingle();};
	void on_action_Screenshot_triggered()  {this->kernel->CreateScreenShot();};
	void on_action_Plot_triggered()		   {this->guiPlotData();};
    void on_action_Load_Mesh_triggered()   {this->guiShowDialogBrowseMesh();};
    void on_action_Load_Seeds_triggered()  {this->guiShowDialogBrowseSeeds();};
    void on_action_Load_Cluster_Hierarchy_triggered()   {this->guiShowDialogBrowseClusterHierarchy();};
    void on_action_Save_Cluster_Hierarchy_triggered()   {this->guiSaveClusterHierarchy();};
    void on_action_Load_Fluid_Simulation_triggered()    {this->guiShowDialogBrowseFluidSimulation();};
    void on_action_Save_Fluid_Simulation_triggered()    {this->guiSaveFluidSimulation();};
    void on_action_Release_triggered()     {this->guiReleaseData();};
    void on_action_About_triggered()       {this->guiShowDialogAbout();};

    void on_toolButtonAP_released() {this->guiUpdateViewGeometry();};
    void on_toolButtonPA_released() {this->guiUpdateViewGeometry();};
    void on_toolButtonLR_released() {this->guiUpdateViewGeometry();};
    void on_toolButtonRL_released() {this->guiUpdateViewGeometry();};
    void on_toolButtonHF_released() {this->guiUpdateViewGeometry();};
    void on_toolButtonFH_released() {this->guiUpdateViewGeometry();};

    void on_comboBoxScenePreset_currentIndexChanged(int t)          {this->guiUpdateScenePreset(t);};

    void on_toolButtonPlay_clicked()                                {this->guiAnimationStartStop();};    
    void on_comboBoxAnimationSpeed_currentIndexChanged(int t)       {this->guiAnimationSpeed(t);};
    void on_horizontalSliderTime_valueChanged(int t)                {this->guiAnimationSlider(t);};

    void on_doubleSpinBoxLightAmbient_valueChanged(double)          {this->guiUpdateBackendLightSource();};
    void on_doubleSpinBoxLightDiffuse_valueChanged(double)          {this->guiUpdateBackendLightSource();};
    void on_doubleSpinBoxLightSpecular_valueChanged(double)         {this->guiUpdateBackendLightSource();};
    void on_doubleSpinBoxLightSpecularPower_valueChanged(double)    {this->guiUpdateBackendLightSource();};
    
    void on_radioButtonBackgroundUniform_toggled(bool)              {this->guiUpdateDisplay();};
    void on_radioButtonBackgroundGradient_toggled(bool)             {this->guiUpdateDisplay();};        
    void on_lineEditBackgroundColorPrimary_selectionChanged()       {this->guiUpdateDisplay();};
    void on_lineEditBackgroundColorSecondary_selectionChanged()     {this->guiUpdateDisplay();};

    void on_pushButtonFrameRate_clicked()                           {this->guiMeasureFrameRate();};

    void on_pushButtonTransferFunctionSave_clicked()                {this->guiSaveTransferFunction();};
    void on_pushButtonTransferFunctionLoad_clicked()                {this->guiLoadTransferFunction();};

    void on_comboBoxProbeQuantification_currentIndexChanged()       {this->guiUpdateFrontendProbeQuantification();};
    void on_treeWidgetProbes_itemClicked(QTreeWidgetItem *i, int c) {this->guiUpdateFrontendProbeBrowser(i, c);
                                                                     this->guiUpdateFrontendProbeQuantification(i,c);};

    void on_pushButtonRotate_clicked()                              {this->guiRotateCamera();};
    void on_pushButtonRotateLeft_clicked()                          {this->guiRotateCamera(abs((int)this->UI->spinBoxCameraRotationDegrees->value()));};
    void on_pushButtonRotateRight_clicked()                         {this->guiRotateCamera(-1*abs((int)this->UI->spinBoxCameraRotationDegrees->value()));};
    void on_pushButtonResize512_clicked()                           {this->guiUpdateRenderWidgetSize(512);};
    void on_pushButtonResize768_clicked()                           {this->guiUpdateRenderWidgetSize(768);};

    void on_pushButtonProbeUpdate_clicked()                         {this->guiUpdateFrontendProbeBrowser();};
    void on_pushButtonProbeLoad_clicked()                           {this->guiLoadProbe();};
    void on_pushButtonProbeSave_clicked()                           {this->guiSaveProbe();};

    void on_radioButtonParamBasic_toggled(bool)                     {this->guiUpdateFrontendPropertyBrowser();};
    void on_radioButtonParamAdvanced_toggled(bool)                  {this->guiUpdateFrontendPropertyBrowser();};

    void on_pushButtonComputeTMIP_clicked()                         {this->guiComputeTMIP();};
    void on_pushButtonComputeTMAP_clicked()                         {this->guiComputeTMAP();};
    void on_pushButtonComputeTMOP_clicked()                         {this->guiComputeTMOP();};
    void on_pushButtonComputeCurl_clicked()                         {this->guiComputeCurl();}
    void on_pushButtonComputeFTLE_clicked()                         {this->guiComputeFTLE();}
    void on_pushButtonComputeLambda2_clicked()                      {this->guiComputeLambda2();}
    void on_pushButtonComputeQCriterion_clicked()                   {this->guiComputeQCriterion();}
    void on_pushButtonComputeDivergence_clicked()                   {this->guiComputeDivergence();}
    void on_pushButtonMedianFilter_clicked()                        {this->guiApplyMedianFilter();}
	void on_pushButtonExportMetrics_clicked()						{this->guiExportMetrics();}

    void on_pushButtonSetNrBins_clicked()                           {this->guiSetNrOfBins();}

    void on_pushButtonFeatureAddPoint1_clicked()                    {this->guiAddFeaturePoint0();}
    void on_pushButtonFeatureAddPoint2_clicked()                    {this->guiAddFeaturePoint1();}

    void on_comboBoxFeatures1_currentIndexChanged(int t)            {this->guiUpdateFeatures1(t);}
    void on_comboBoxFeatures2_currentIndexChanged(int t)            {this->guiUpdateFeatures2(t);}

    void guiHandleAction();
    void guiUpdateScenePreset(int t);
    void guiUpdateDisplay();
    void guiAnimationTimer();      

    void guiUpdateFeatures1(int t);
    void guiUpdateFeatures2(int t);

	void guiPlotData();

  protected slots :     
    // Update backend (initiated by frontend)    
    void guiUpdateBackendColor();    
    void guiUpdateBackendLightSource();
    void guiUpdateBackendPropertyBrowserValue(QtProperty *property, const QVariant &value);
    void guiUpdateBackendModelBrowserValue(QtProperty *property, const QVariant &value);
    void guiUpdateBackendProbeBrowserValue(QtProperty *property, const QVariant &value);
    void guiUpdateBackendFeatures();
    
    // Update frontend (initiated by backend)    
    void guiUpdateFrontend();
    void guiUpdateFrontendBrowsers();
    void guiUpdateFrontendColor();
    void guiUpdateFrontendLightSource();
    void guiUpdateFrontendActions();
    void guiUpdateFrontendPropertyBrowser();       
    void guiUpdateFrontendModelBrowser();       
    void guiUpdateFrontendVolumeBrowser();
    void guiUpdateFrontendDisplayColor();
    void guiUpdateFrontendProbeTree();
    void guiUpdateFrontendProbeBrowser();   
    void guiUpdateFrontendProbeBrowser(QTreeWidgetItem *item, int column);   
    void guiUpdateFrontendProbeQuantification();
    void guiUpdateFrontendProbeQuantification(QTreeWidgetItem *item, int column);

    // Probe functions
    void guiLoadProbe();
    void guiSaveProbe();

    // Update the cursor
    void guiUpdateCursor();

    // Data preprocessing
    void guiComputeTMIP();
    void guiComputeTMAP();
    void guiComputeTMOP();
    void guiComputeFTLE();
    void guiComputeCurl();
    void guiComputeLambda2();
    void guiComputeQCriterion();
    void guiComputeDivergence();
    void guiApplyMedianFilter();
	void guiExportMetrics();

    void guiSetNrOfBins();

    void guiAddFeaturePoint0();
    void guiAddFeaturePoint1();

    // Transfer function relates
    void guiSaveTransferFunction();
    void guiLoadTransferFunction();

    // Frame rate measurement
    void guiMeasureFrameRate();

    // Display functions
    void guiRotateCamera();
    void guiRotateCamera(int angle);    
    void guiUpdateRenderWidgetSize(int size);
   
  protected:
    Ui_MainWindow         *UI;    
    qfeKernel             *kernel;    
    guiState               state;
    vtkEventQtSlotConnect* vtkevents;
    qtProgressBarCallBack *qfpcb;     

    // QT components
    QtTreePropertyBrowser      *propertyBrowser;   
    QtVariantPropertyManager   *propertyBrowserVariantManager;
    QtVariantEditorFactory     *propertyBrowserVariantFactory;
    QMap<QtProperty *, QString> propertyBrowserPropertyToId;
    bool                        propertyBrowserInitState;

    QtTreePropertyBrowser      *modelBrowser;    
    QtVariantPropertyManager   *modelBrowserVariantManager;
    QtVariantEditorFactory     *modelBrowserVariantFactory;
    QMap<QtProperty *, QString> modelBrowserPropertyToId;
    bool                        modelBrowserInitState;

    QtTreePropertyBrowser      *probeBrowser;    
    QtVariantPropertyManager   *probeBrowserVariantManager;
    QtVariantEditorFactory     *probeBrowserVariantFactory;
    QMap<QtProperty *, QString> probeBrowserPropertyToId;
    bool                        probeBrowserInitState;

    QtTreePropertyBrowser      *volumeBrowser;
    QtVariantPropertyManager   *volumeBrowserVariantManager;
    QtVariantEditorFactory     *volumeBrowserVariantFactory;
    bool                        volumeBrowserInitState;
    
    QProgressBar               *progressBar;
    qtTransferFunctionWidget   *transferFunction;
    qtBaseTransferFunctionCanvas *featureTF1;
    qtBaseTransferFunctionCanvas *featureTF2;
    qtBallWidget               *ballLight;
    QCustomPlot                *fluxPlot;

    //QMap<QString, QtVariantProperty *> idToProperty;
    //QMap<QString, bool> idToExpanded;

    // Animation variables 
    QTimer                    *timer;    
    int                        timeCounter;    	
    double					   animationFactor;
    bool                       animationEnabled;
    double                     phaseDuration;  

    // Transfer function variables
    std::vector<qfeHistogram *> histograms;

    QVector< QToolButton *>     actions;
    QVector< int >              actionsType;

    // Probe browser variable
    int  currentProbe2D; // -1 = none
    int  currentProbe3D;

    // General functions 
    void guiInitializePreData();
    void guiInitializePostData();

    void guiInitializeComponents();
    void guiInitializeLight(qfeScene *scene);

    void guiUninitializePreData();
    void guiUninitializePostData();

    // Action buttons
    void guiInitializeActions(qfeScene *scene, QBoxLayout *layout);
    void guiUninitializeActions(QBoxLayout *layout);

    // Property browser functions
    void guiInitializePropertyBrowser(qfeScene *scene, QtTreePropertyBrowser* pb, QtVariantPropertyManager *vm, QtVariantEditorFactory *vf, bool basic);
    void guiUninitializePropertyBrowser();

    void guiInitializeModelBrowser(qfePatient *patient, QtTreePropertyBrowser* pb, QtVariantPropertyManager *vm, QtVariantEditorFactory *vf);
    void guiUninitializeModelBrowser();
    
    void guiInitializeVolumeBrowser(qfePatient *patient, QtTreePropertyBrowser* pb, QtVariantPropertyManager *vm, QtVariantEditorFactory *vf);
    void guiUninitializeVolumeBrowser();

    void guiInitializeProbeTree(qfePatient *patient, QTreeWidget *tree);
    void guiUninitializeProbeTree(); 

    void guiInitializeProbeBrowser(qfeProbe2D *probe, QtTreePropertyBrowser* pb, QtVariantPropertyManager *vm, QtVariantEditorFactory *vf);
    void guiInitializeProbeBrowser(qfeProbe3D *probe, QtTreePropertyBrowser* pb, QtVariantPropertyManager *vm, QtVariantEditorFactory *vf);
    void guiUninitializeProbeBrowser();    

    void guiInitializeProbeQuantification(qfeProbe2D *probe, int index);
    
    // Dialogs
    void guiShowDialogBrowseSingle();
    void guiShowDialogBrowseAll();
    void guiShowDialogBrowseMesh();
    void guiShowDialogBrowseSeeds();
    void guiShowDialogBrowseClusterHierarchy();
    void guiShowDialogBrowseFluidSimulation();
    void guiShowDialogAbout();

    // Data
    void guiLoadData(QStringList);
    void guiReleaseData();

    // Mesh
    void guiLoadMesh(string path);

    // Seeds
    void guiLoadSeeds(string path);

    // Clusters
    void guiLoadClusterHierarchy(string path);
    void guiSaveClusterHierarchy();

    // Fluid Simulation
    void guiLoadFluidSimulation(QStringList);
    void guiSaveFluidSimulation();

    // Scene
    void guiSelectScene(int index);

    // Transfer Function
    void guiInitializeTransferFunctions();
    void guiUninitializeTransferFunctions();
    void guiPrecomputeHistograms(qfeVolumeSeries &volumes);

    // Updates   
    //void guiUpdateTabVisuals();
    //void guiUpdateTabAppearance();
    void guiUpdateUsername(qfePatient *p);    
    void guiUpdateMenu(); 

    // Updates Render Window
    void guiUpdateViewGeometry();
    void guiUpdateRenderWindow();

    // Animation
    void guiAnimationStartStop();    
    void guiAnimationSpeed(int t);
    void guiAnimationSlider(int t);
    void guiAnimationUpdateSlider(int phases, double animationFactor);

    // Event filtering
    bool eventFilter(QObject* watched, QEvent* event);

    // Visuals
    string guiGetVisualLabel(qfeVisual *visual);

    qfeStudy *getStudy();
    std::vector<qfeHistogram*> getHistograms(const qfeVolumeSeries &volumes);
};

/**
* \file   qtMainWindow.h
* \author Roy van Pelt
* \class  qtProgressBarCallBack
* \brief  Implements a callback function to update a progress bar
*/

//----------------------------------------------------------------------------
class qtProgressBarCallBack : public qfeCallBackProgressBar 
{
public:
  QProgressBar *progressBar;
  QStatusBar   *statusBar;
  void SetProgressBar(QProgressBar *pb)
  {
    this->progressBar = pb;
    this->progressBar->setValue(0);
  }

  void SetStatusBar(QStatusBar *bar)
  {
    this->statusBar = bar;
  }

  void Execute(qfeCallData cd) {
    int newValue = this->progressBar->value()+cd.stepSize;
    this->progressBar->setVisible(true);
    this->progressBar->setValue(newValue);
    this->statusBar->showMessage(QString(cd.message));
	  if((newValue >= 100) || (cd.stepSize == -1))
    {
      this->progressBar->setValue(100);
      this->progressBar->setVisible(false);
      this->progressBar->setValue(0);
      if(this->statusBar != NULL) this->statusBar->showMessage(QString(""));
    }
    if(cd.stepSize == -2) //just a message
    {
      this->statusBar->showMessage(QString(cd.message));
      this->progressBar->setVisible(false);
      this->progressBar->setValue(0);
    }
  }
};
