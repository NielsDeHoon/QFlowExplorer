#include <vtkCanvasInteractorStyleForward.h>
#include <vtkCanvas.h>

#include <vtkObjectFactory.h>

vtkCxxRevisionMacro( vtkCanvasInteractorStyleForward, "$Revision: 1.0 $");
vtkStandardNewMacro( vtkCanvasInteractorStyleForward );

/////////////////////////////////////////////////////////////////
vtkCanvasInteractorStyleForward::vtkCanvasInteractorStyleForward() : vtkCanvasInteractorStyle()
{
}

/////////////////////////////////////////////////////////////////
vtkCanvasInteractorStyleForward::~vtkCanvasInteractorStyleForward()
{
}

/////////////////////////////////////////////////////////////////
void vtkCanvasInteractorStyleForward::OnLeftButtonDown()
{
	this->OnButtonDown();
}

/////////////////////////////////////////////////////////////////
void vtkCanvasInteractorStyleForward::OnRightButtonDown()
{
	this->OnButtonDown();
}

/////////////////////////////////////////////////////////////////
void vtkCanvasInteractorStyleForward::OnMiddleButtonDown()
{
	this->OnButtonDown();
}

/////////////////////////////////////////////////////////////////
void vtkCanvasInteractorStyleForward::OnButtonDown()
{
	if( this->GetCanvas() == 0 )
		return;

	this->GetCanvas()->SelectSubCanvasFromLastEvent();
}

/////////////////////////////////////////////////////////////////
void vtkCanvasInteractorStyleForward::SetCanvas( vtkCanvas * canvas )
{
	if( this->GetCanvas() != 0 )
		this->GetCanvas()->SetInteractionOnSelectEnabled( false );

	this->vtkCanvasInteractorStyle::SetCanvas( canvas );

	if( this->GetCanvas() != 0 )
		this->GetCanvas()->SetInteractionOnSelectEnabled( true );
}