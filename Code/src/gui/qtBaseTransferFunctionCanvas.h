/**
* \file   qtTransferFunctionCanvas.h
* \author Roy van Pelt
* \class  qtTransferFunctionCanvas
* \brief  Implements a 1D transfer function canvas
*
*/

#ifndef __qtBaseTransferFunctionCanvas_h
#define __qtBaseTransferFunctionCanvas_h

#include <algorithm>
#include <QtGui>

#include <iostream>
using namespace std;

#include "qfeHistogram.h"

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

typedef QVector< int >  QHistogram;
typedef QVector<QHistogram> QHistogramVector;

//----------------------------------------------------------------------------
class qtBaseTransferFunctionCanvas : public QWidget
{
	Q_OBJECT

public:
  qtBaseTransferFunctionCanvas(QWidget *parent = 0);
  ~qtBaseTransferFunctionCanvas();
  QSize minimumSizeHint() { return QSize(256, 128); }
  QSize sizeHint() { return QSize(256, 128); }
	
  void  addHistogram(qfeHistogram *histogram);
  void  addHistograms(const std::vector<qfeHistogram*> &histograms);
  void  clearHistograms();
  void  setHistogramScale(int scale);
  void  setHistogramScaleType(int logarithmic);
  void  setHistogramNumber(int current);
  void  setHistogramVisibility(int current);
  void  getTransferFunction(QVector< QPair<double, double> > &opacity, int &dataType);
  void  setTransferFunction(QVector< QPair<double, double> > &opacity, int dataType);
  void  addTransferFunctionPoint(float x, float y);
  void  setDataType(int dataType);
  void  setNrBins(int nrBins);

  void  setAxisX(QString label, double min, double max);
  void  setAxisY(QString label, double min, double max);  

signals:
	void transferFunctionChanged();

protected:
  int    dataType;
  int    PointSize;
  int    oSelected, gSelected, cSelected, crSelected;

  int    oEditorWidth;     // QRect
  int    oEditorHeight;
  int    oEditorOrigin[2];
  int    oEditorMargin[4]; // left, right, top, bottom 

  // qfeTranferFunctionCanvasParameters struct
  int          quantization;
  int          histogramscale;
  int          histogramscaletype;
  int          histogramvisible;
  bool         constantlightness;
  bool         paddinguniform;

  int          currentHistogramIndex;

  QString labelX;
  QString labelY;
  QString labelCount;
  double  rangeX[2];
  double  rangeY[2];

  QWidget *Canvas;

  std::vector<qfeHistogram*>        histograms;    
  QVector< QPair<double, double>  > OpacityPoints;  

  QVector< QPolygonF >              HistogramPoly;

  void initTransferFunction();  

  void resizeEvent(QResizeEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void mouseReleaseEvent(QMouseEvent *event);
  void mouseDoubleClickEvent(QMouseEvent *event);
  void mouseMoveEvent(QMouseEvent *event);
  void paintEvent(QPaintEvent *event);
  
  void paintCheckerboard(QPainter *p, int o[], int w, int h);
  void paintAxis(QPainter *p, int o[], int w, int h);
  void paintHistogram(QPainter *p);
  void paintOpacityPoints(QPainter *p, int o[], int w, int h, QVector< QPair<double, double> > points);
  void paintLabelX(QPainter *p, int o[], int w, int h, QString label);
  void paintLabelY(QPainter *p, int o[], int w, int h, QString label);
  void paintLabelCount(QPainter *p);
  void paintZeroLine(QPainter *p, int o[], int w, int h);

  float clamp(float v, float min, float max);

  QPolygon getTriangle(const QRect &rect);

  bool insideOpacityEditor(QPointF pos, bool borderX, bool borderY);
  int  selectOpacityPoint(QPointF pos);

  void      updateHistogramPoly();
  QPolygonF computeHistogramPoly(qfeHistogram *histogram);

private:
  double       clamp(double in);
  double       lerp(double value1, double value2, double amount);
};

#endif
