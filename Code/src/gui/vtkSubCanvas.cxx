#include <vtkSubCanvas.h>

#include <vtkObjectFactory.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkInteractorStyle.h>
#include <vtkRenderWindowInteractor.h>

#include <assert.h>

vtkCxxRevisionMacro( vtkSubCanvas, "$Revision: 1.0 $");
vtkStandardNewMacro( vtkSubCanvas );

/////////////////////////////////////////////////////////////////
vtkSubCanvas::vtkSubCanvas()
{
	this->Style = 0;
	this->Window = 0;
	this->Interactor = 0;
	this->InteractionEnabled = false;

	this->Renderer = vtkRenderer::New();
	this->Renderer->SetBackground( 0.5, 0.5, 0.5 );

	this->SetInteractorStyle( 0 );
}

/////////////////////////////////////////////////////////////////
vtkSubCanvas::~vtkSubCanvas()
{
	this->SetInteractionEnabled( false );
	this->SetInteractorStyle( 0 );

	if( this->Renderer != 0 )
		this->Renderer->Delete();
	this->Renderer = 0;
}

/////////////////////////////////////////////////////////////////
void vtkSubCanvas::SetRenderWindow( vtkRenderWindow * window )
{
	if( this->Window == window )
		return;

	if( this->Window != 0 )
	{
		this->Window->RemoveRenderer( this->Renderer );
		this->Window->UnRegister( this );
	}

	this->Window = window;

	if( this->Window != 0 )
	{
		this->Window->Register( this );
		this->Window->AddRenderer( this->Renderer );
	}
}

/////////////////////////////////////////////////////////////////
vtkRenderWindow * vtkSubCanvas::GetRenderWindow()
{
	return this->Window;
}

/////////////////////////////////////////////////////////////////
void vtkSubCanvas::SetInteractor( vtkRenderWindowInteractor * interactor )
{
	if( this->Interactor == interactor )
		return;

	bool enabled = this->InteractionEnabled;
	this->SetInteractionEnabled( false );

	if( this->Interactor != 0 )
		this->Interactor->UnRegister( this );
	this->Interactor = interactor;
	if( this->Interactor != 0 )
		this->Interactor->Register( this );

	this->SetInteractionEnabled( enabled );
}

/////////////////////////////////////////////////////////////////
vtkRenderWindowInteractor * vtkSubCanvas::GetInteractor()
{
	return this->Interactor;
}

/////////////////////////////////////////////////////////////////
void vtkSubCanvas::SetInteractorStyle( vtkInteractorStyle * style )
{
	if( this->Style == style )
		return;

	bool enabled = this->InteractionEnabled;
	this->SetInteractionEnabled( false );

	if( this->Style != 0 )
		this->Style->UnRegister( this );
	this->Style = style;
	if( this->Style != 0 )
		this->Style->Register( this );

	this->SetInteractionEnabled( enabled );
}

/////////////////////////////////////////////////////////////////
vtkInteractorStyle * vtkSubCanvas::GetInteractorStyle()
{
	return this->Style;
}

/////////////////////////////////////////////////////////////////
void vtkSubCanvas::SetInteractionEnabled( bool enabled )
{
	if( this->InteractionEnabled == enabled )
		return;

	this->InteractionEnabled = enabled;

	if( this->Style == 0 || this->Interactor == 0 )
		return;

	assert( this->Renderer );

	if( this->InteractionEnabled )
	{
		this->Style->SetInteractor( this->Interactor );
		this->Style->SetDefaultRenderer( this->Renderer );
		this->Style->SetCurrentRenderer( this->Renderer );
	}
	else
	{
		this->Style->SetInteractor( 0 );
		this->Style->SetDefaultRenderer( 0 );
		this->Style->SetCurrentRenderer( 0 );
	}
}

/////////////////////////////////////////////////////////////////
bool vtkSubCanvas::IsInteractionEnabled()
{
	return this->InteractionEnabled;
}

/////////////////////////////////////////////////////////////////
void vtkSubCanvas::SetViewport( double x1, double x2, double y1, double y2 )
{
	assert( this->Renderer != 0 );
	this->Renderer->SetViewport( x1, x2, y1, y2 );
}

/////////////////////////////////////////////////////////////////
double * vtkSubCanvas::GetViewport()
{
	assert( this->Renderer != 0 );
	return this->Renderer->GetViewport();
}

/////////////////////////////////////////////////////////////////
bool vtkSubCanvas::InsideViewport( int x, int y )
{
	if( this->Window != 0 )
	{
		int * size = this->Window->GetSize();
		double * viewport = this->GetViewport();

		if( viewport[0] * size[0] <= x &&
			viewport[2] * size[0] >= x &&
			viewport[1] * size[1] <= y && viewport[3] * size[1] >= y )
		{
			return true;
		}
	}

	return false;
}

/////////////////////////////////////////////////////////////////
double * vtkSubCanvas::WindowToViewport( int x, int y )
{
	assert( this->Window != 0 );

	int * size = this->Window->GetSize();
	double * viewport = this->GetViewport();

	double * result = new double[2];
	result[0] = x - viewport[0] * size[0];
	result[1] = y - viewport[1] * size[1];

	return result;
}

/////////////////////////////////////////////////////////////////
void vtkSubCanvas::Render()
{
	assert( this->Renderer != 0 );

	this->Renderer->Render();
	if( this->Window )
		this->Window->Render();
}

/////////////////////////////////////////////////////////////////
void vtkSubCanvas::ResetCamera()
{
	assert( this->Renderer != 0 );
	this->Renderer->ResetCamera();
}

/////////////////////////////////////////////////////////////////
void vtkSubCanvas::SetBackgroundColor( double r, double g, double b )
{
	assert( this->Renderer != 0 );
	this->Renderer->SetBackground( r, g, b );
}

/////////////////////////////////////////////////////////////////
double * vtkSubCanvas::GetBackgroundColor()
{
	assert( this->Renderer != 0 );
	return this->Renderer->GetBackground();
}

/////////////////////////////////////////////////////////////////
vtkRenderer * vtkSubCanvas::GetRenderer()
{
	return this->Renderer;
}