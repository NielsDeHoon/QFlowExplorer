#include <vtkSubCanvasCollection.h>
#include <vtkSubCanvas.h>

#include <vtkObjectFactory.h>

vtkCxxRevisionMacro( vtkSubCanvasCollection, "$Revision: 1.0 $");
vtkStandardNewMacro( vtkSubCanvasCollection );

/////////////////////////////////////////////////////////////////
vtkSubCanvasCollection::vtkSubCanvasCollection()
{
}

/////////////////////////////////////////////////////////////////
vtkSubCanvasCollection::~vtkSubCanvasCollection()
{
}

/////////////////////////////////////////////////////////////////
void vtkSubCanvasCollection::AddItem( vtkSubCanvas * canvas )
{
	this->vtkCollection::AddItem( (vtkObject *) canvas );
}

/////////////////////////////////////////////////////////////////
void vtkSubCanvasCollection::RemoveItem( vtkSubCanvas * canvas )
{
	this->vtkCollection::RemoveItem( (vtkObject *) canvas );
}

/////////////////////////////////////////////////////////////////
vtkSubCanvas * vtkSubCanvasCollection::GetNextItem()
{
	return static_cast< vtkSubCanvas * >( this->GetNextItemAsObject() );
}

/////////////////////////////////////////////////////////////////
vtkSubCanvas * vtkSubCanvasCollection::GetItem( int index )
{
	return static_cast< vtkSubCanvas * >( this->GetItemAsObject( index ) );
}