#ifndef __vtkCanvas_h
#define __vtkCanvas_h

#include <vtkObject.h>

class vtkRenderer;
class vtkRenderWindow;
class vtkRenderWindowInteractor;

class vtkSubCanvas;
class vtkSubCanvasCollection;
class vtkCanvasInteractorStyle;

class vtkCanvas : public vtkObject
{
public:

	static vtkCanvas * New();
	vtkTypeRevisionMacro( vtkCanvas, vtkObject );

	virtual void AddSubCanvas( vtkSubCanvas * canvas );
	virtual void RemoveSubCanvas( vtkSubCanvas * canvas );

	virtual void SelectSubCanvas( vtkSubCanvas * canvas );
	virtual void SelectSubCanvasAt( int x, int y );
	virtual void SelectSubCanvasFromLastEvent();

	virtual vtkSubCanvas * GetSubCanvasAt( int x, int y );
  virtual int            GetSubCanvasIndexAt( int x, int y );

	virtual void SetRenderWindow( vtkRenderWindow * window );
	virtual vtkRenderWindow * GetRenderWindow();

	virtual void SetInteractor( vtkRenderWindowInteractor * interactor );
	virtual vtkRenderWindowInteractor * GetInteractor();

	virtual void SetInteractorStyle( vtkCanvasInteractorStyle * style );
	virtual class vtkCanvasInteractorStyle * GetInteractorStyle();

	virtual void SetSize( int width, int height );
	virtual int * GetSize();

	virtual void Start();

	virtual void ResetAllCameras();

	virtual void SetInteractionOnSelectEnabled( bool enabled );
	virtual bool IsInteractionOnSelectEnabled();

protected:

	vtkCanvas();
	virtual ~vtkCanvas();

private:

	vtkSubCanvasCollection * SubCanvasses;
	vtkSubCanvas * SelectedCanvas;

	vtkRenderer * BackgroundRenderer;
	vtkRenderWindow * Window;
	vtkCanvasInteractorStyle * Style;
	vtkRenderWindowInteractor * Interactor;

	bool InteractionOnSelectEnabled;
};

#endif