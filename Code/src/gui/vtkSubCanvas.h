#ifndef __vtkSubCanvas_h
#define __vtkSubCanvas_h

#include <vtkObject.h>

class vtkRenderer;
class vtkRenderWindow;
class vtkInteractorStyle;
class vtkRenderWindowInteractor;

class vtkSubCanvas : public vtkObject
{
public:

	static vtkSubCanvas * New();
	vtkTypeRevisionMacro( vtkSubCanvas, vtkObject );

	virtual void SetRenderWindow( vtkRenderWindow * window );
	virtual vtkRenderWindow * GetRenderWindow();

	virtual void SetInteractor( vtkRenderWindowInteractor * interactor );
	virtual vtkRenderWindowInteractor * GetInteractor();

	virtual void SetInteractorStyle( vtkInteractorStyle * style );
	virtual vtkInteractorStyle * GetInteractorStyle();

	virtual void SetViewport( double x1, double x2, double y1, double y2 );
	virtual double * GetViewport();

	virtual bool InsideViewport( int x, int y );
	virtual double * WindowToViewport( int x, int y );

	virtual void SetInteractionEnabled( bool enabled );
	virtual bool IsInteractionEnabled();

	virtual void ResetCamera();

	virtual void Render();

	virtual void SetBackgroundColor( double r, double g, double b );
	virtual double * GetBackgroundColor();

	virtual vtkRenderer * GetRenderer();

protected:

	vtkSubCanvas();
	virtual ~vtkSubCanvas();

private:

	vtkRenderer * Renderer;
	vtkRenderWindow * Window;
	vtkInteractorStyle * Style;
	vtkRenderWindowInteractor * Interactor;

	float Viewport[4];
	float BackgroundColor[3];
	
	bool InteractionEnabled;
};

#endif