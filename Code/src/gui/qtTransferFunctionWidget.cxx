#include "qtTransferFunctionWidget.h"
#include "vtkColorTransferFunction.h"
#include "vtkPiecewiseFunction.h"

const qtColorXYZ whiteD50 = { 0.9642, 1.0, 0.8249 };
const qtColorXYZ whiteD65 = { 0.9501548, 1.0, 1.0882598 };

//----------------------------------------------------------------------------
qtTransferFunctionWidget::qtTransferFunctionWidget(QWidget *parent) : QWidget(parent)
{
  this->initComponents();  

  this->initConnections();

  this->initCanvas();
}

//----------------------------------------------------------------------------
qtTransferFunctionWidget::~qtTransferFunctionWidget()
{  
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::initComponents()
{
  // Create the layouts
  QGridLayout *gridCentral = new QGridLayout;

  QGridLayout *gridButtons = new QGridLayout;
  QHBoxLayout *gridCanvas  = new QHBoxLayout;
  QGridLayout *gridSlider  = new QGridLayout;  

  gridCentral->setContentsMargins(QMargins(0,0,0,0));

  // Create the transfer function canvas
  this->canvas = new qtTransferFunctionCanvas();
  gridCanvas->addWidget(canvas);

  // Create the additional components
  QLabel *labelTFNr = new QLabel(QString("Visual"));
  QLabel *labelColorPreset        = new QLabel(QString("Map preset"));
  QLabel *labelColorInterpolation = new QLabel(QString("Interpolation"));
  QLabel *labelColorSpace         = new QLabel(QString("Space"));
  QLabel *labelHistogramScale     = new QLabel(QString("Hist. scale"));  

  this->comboBoxTransferFunctionNr = new QComboBox();

  this->comboBoxColorPreset = new QComboBox();
  std::vector<std::string> colorMapList = colorMaps.qfeGetAvailableColorMaps();
  for(unsigned int i = 0; i<colorMapList.size(); i++)
  {
	this->comboBoxColorPreset->addItem(QString::fromStdString(colorMapList.at(i)));
  }
  /*
  this->comboBoxColorPreset->addItem(QString("black body"));
  this->comboBoxColorPreset->addItem(QString("rainbow"));
  this->comboBoxColorPreset->addItem(QString("doppler 1"));  
  this->comboBoxColorPreset->addItem(QString("doppler 2"));  
  this->comboBoxColorPreset->addItem(QString("diverging 1"));
  this->comboBoxColorPreset->addItem(QString("diverging 2"));
  this->comboBoxColorPreset->addItem(QString("diverging 3"));
  this->comboBoxColorPreset->addItem(QString("cool / warm"));
  this->comboBoxColorPreset->addItem(QString("heat"));  
  this->comboBoxColorPreset->addItem(QString("gray")); 
  this->comboBoxColorPreset->addItem(QString("Pink to gray"));
  */

  this->comboBoxOpacityPreset = new QComboBox();  
  this->comboBoxOpacityPreset->addItem(QString("full"));
  this->comboBoxOpacityPreset->addItem(QString("step"));
  this->comboBoxOpacityPreset->addItem(QString("pcap"));
  this->comboBoxOpacityPreset->addItem(QString("pcam"));
  this->comboBoxOpacityPreset->addItem(QString("ssfp"));  
  this->comboBoxOpacityPreset->addItem(QString("tmip"));
  this->comboBoxOpacityPreset->addItem(QString("ivr"));
  this->comboBoxOpacityPreset->addItem(QString("ivr2"));
  this->comboBoxOpacityPreset->addItem(QString("ivr3"));
  this->comboBoxOpacityPreset->addItem(QString("ivr4"));
  this->comboBoxOpacityPreset->addItem(QString("increasing"));
  this->comboBoxOpacityPreset->setCurrentIndex(10);
  
  QHBoxLayout *layoutInterpolation = new QHBoxLayout();
  QHBoxLayout *layoutSpace         = new QHBoxLayout();
  QHBoxLayout *layoutCheckBoxes    = new QHBoxLayout();
  QHBoxLayout *layoutPresets       = new QHBoxLayout();

  this->comboBoxColorInterpolation = new QComboBox();
  this->comboBoxColorInterpolation->addItem(QString("nearest neighbor"));
  this->comboBoxColorInterpolation->addItem(QString("linear"));
  this->comboBoxColorInterpolation->setCurrentIndex(1);

  this->spinBoxColorQuantization = new QSpinBox();
  this->spinBoxColorQuantization->setMinimum(1);
  this->spinBoxColorQuantization->setMaximum(50);
  this->spinBoxColorQuantization->setValue(7);

  layoutInterpolation->setContentsMargins(QMargins(0,0,0,0));
  layoutInterpolation->addWidget(comboBoxColorInterpolation);
  layoutInterpolation->addWidget(spinBoxColorQuantization);

  this->comboBoxColorSpace = new QComboBox();
  this->comboBoxColorSpace->addItem(QString("RGB"));
  this->comboBoxColorSpace->addItem(QString("CIE Luv"));  
  this->comboBoxColorSpace->addItem(QString("CIE Lab"));  
  this->comboBoxColorSpace->setCurrentIndex(1);
  
  this->checkBoxLightnessConstant = new QCheckBox();
  this->checkBoxLightnessConstant->setText(QString("Lightness constant"));
  this->checkBoxLightnessConstant->setLayoutDirection(Qt::LeftToRight);

  this->checkBoxPadding = new QCheckBox();
  this->checkBoxPadding->setText(QString("Padding white"));
  this->checkBoxPadding->setLayoutDirection(Qt::LeftToRight);

  //this->sliderHistogramScale = new QSlider();
  //this->sliderHistogramScale->setOrientation(Qt::Horizontal);

  this->checkBoxHistogramVisible = new QCheckBox();
  this->checkBoxHistogramVisible->setText("Hist. visible");
  this->checkBoxHistogramVisible->setLayoutDirection(Qt::LeftToRight);
  this->checkBoxHistogramVisible->setChecked(true);

  this->checkBoxHistogramUpdate = new QCheckBox();
  this->checkBoxHistogramUpdate->setText("Hist. updates");
  this->checkBoxHistogramUpdate->setLayoutDirection(Qt::LeftToRight);
  this->checkBoxHistogramUpdate->setChecked(false);

  this->checkBoxHistogramLogarithmic = new QCheckBox();
  this->checkBoxHistogramLogarithmic->setText("Hist. log. scale");
  this->checkBoxHistogramLogarithmic->setLayoutDirection(Qt::LeftToRight);  
  this->checkBoxHistogramLogarithmic->setChecked(true);

  layoutPresets->addWidget(comboBoxColorPreset);
  layoutPresets->addWidget(comboBoxOpacityPreset);

  layoutSpace->addWidget(comboBoxColorSpace);
  layoutSpace->addWidget(this->checkBoxLightnessConstant);

  layoutCheckBoxes->addWidget(checkBoxHistogramVisible);
  layoutCheckBoxes->addWidget(checkBoxHistogramUpdate);
  layoutCheckBoxes->addWidget(checkBoxHistogramLogarithmic);

  gridButtons->addWidget(labelTFNr                  , 0, 0);
  gridButtons->addWidget(comboBoxTransferFunctionNr , 0, 1);
  gridButtons->addWidget(labelColorPreset           , 1, 0);    
  gridButtons->addLayout(layoutPresets              , 1, 1);    
  gridButtons->addWidget(labelColorInterpolation    , 2, 0);  
  gridButtons->addLayout(layoutInterpolation        , 2, 1);   
  gridButtons->addWidget(labelColorSpace            , 3, 0);  
  gridButtons->addLayout(layoutSpace                , 3, 1);     
  gridButtons->setColumnStretch(0,1);
  gridButtons->setColumnStretch(1,4);  

  //gridSlider->addWidget(labelHistogramScale          , 0, 0);
  //gridSlider->addWidget(sliderHistogramScale         , 0, 1);    
  gridSlider->addLayout(layoutCheckBoxes              , 0, 0);  
    
  // Fill the main layout and set to the current widget  
  gridCentral->addLayout(gridButtons, 0, 0);
  gridCentral->addLayout(gridCanvas,  1, 0);  
  gridCentral->addLayout(gridSlider,  2, 0);

  // Update the slider
  //this->sliderHistogramScale->setMinimum(100);
  //this->sliderHistogramScale->setMaximum(1000);
  //this->sliderHistogramScale->setSingleStep(10);
  //this->sliderHistogramScale->setPageStep(10);  

  this->setLayout(gridCentral);
  updateOpacityPreset(10);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::initConnections()
{
  // Connect to slots
  connect(this->canvas,                       SIGNAL(transferFunctionChanged()) , this, SLOT(emitUpdate()));
  connect(this->comboBoxTransferFunctionNr,   SIGNAL(currentIndexChanged(int))  , this, SLOT(updateTransferFunction(int)));
  connect(this->comboBoxColorPreset,          SIGNAL(currentIndexChanged(int))  , this, SLOT(updateColorPreset(int)));
  connect(this->comboBoxOpacityPreset,        SIGNAL(currentIndexChanged(int))  , this, SLOT(updateOpacityPreset(int)));
  connect(this->comboBoxColorInterpolation,   SIGNAL(currentIndexChanged(int))  , this, SLOT(updateColorInterpolation(int)));
  connect(this->comboBoxColorSpace,           SIGNAL(currentIndexChanged(int))  , this, SLOT(updateColorSpace(int)));
  connect(this->spinBoxColorQuantization,     SIGNAL(valueChanged(int))         , this, SLOT(updateColorQuantization(int)));
  connect(this->checkBoxLightnessConstant,    SIGNAL(stateChanged(int))         , this, SLOT(updateColorLightnessConstant(int)));
  connect(this->checkBoxPadding,              SIGNAL(stateChanged(int))         , this, SLOT(updateColorPadding(int)));
  //connect(this->sliderHistogramScale,         SIGNAL(valueChanged(int))         , this, SLOT(updateHistogramScale(int)));
  connect(this->checkBoxHistogramLogarithmic, SIGNAL(stateChanged(int))         , this, SLOT(updateHistogramScaleType(int)));
  connect(this->checkBoxHistogramVisible,     SIGNAL(stateChanged(int))         , this, SLOT(updateHistogramVisibility(int)));
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::initCanvas()
{
  this->updateColorPreset(this->comboBoxColorPreset->currentIndex());
  this->updateOpacityPreset(this->comboBoxOpacityPreset->currentIndex());
  this->updateColorInterpolation(this->comboBoxColorInterpolation->currentIndex());
  this->updateColorSpace(this->comboBoxColorSpace->currentIndex());
  this->updateColorQuantization(this->spinBoxColorQuantization->value());
  this->updateColorLightnessConstant(this->checkBoxLightnessConstant->isChecked());
  this->updateColorPadding(this->checkBoxPadding->isChecked());
  //this->updateHistogramScale(this->sliderHistogramScale->value());  
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::addTransferFunction(QString label)
{
  QTransferFunction         tf, tf2;
  QTransferFunctionGradient g;

  tf.first.clear();
  tf2.second.clear();
  g.clear();

  switch(comboBoxOpacityPreset->currentIndex())
  {
  case 0 :  // full             
    createPresetFull(tf2, g);
    break;
  case 1 :  // step                 
    createPresetStep(tf2, g);
    break;
  case 2 :  // pca-p
    createPresetPcap(tf2, g);
    break;
  case 3 :  // pca-m
    createPresetPcam(tf2, g);
    break;
  case 4 :  // ssfp         
    createPresetSsfp(tf2, g);
    break;
  case 5 :  // tmip
    createPresetTmip(tf2, g);            
    break;   
  case 6 : // IVR
    createPresetIVR(tf2, g);
    break;
  case 7 : // IVR2
    createPresetIVR2(tf2, g);
    break;
  case 8 : // IVR3
    createPresetIVR3(tf2, g);
    break;
  case 9 : // IVR4
    createPresetIVR4(tf2, g);
    break;
  case 10 : //increasing
	createPresetIncreasing(tf2, g);
  } 
  //this->createPresetBlackBody(tf, g);
  this->createColorPreset(0, tf, g);
  this->canvas->setTransferFunction(tf.first, tf2.second, g);

  this->comboBoxTransferFunctionNr->addItem(label);  
  this->TransferFunction.push_back(tf);
  this->TransferFunctionGradient.push_back(g);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::removeTransferFunction(QString label)
{
  QVector< int > index;

  index.clear();

  for(int i=0; i<(int)this->comboBoxTransferFunctionNr->count(); i++)
  {
    if(label.compare(this->comboBoxTransferFunctionNr->itemText(i)) == 0)
    {
      index.push_back(i);
    }
  }

  for(int i=index.size()-1; i>=0; i--)
  {
    this->comboBoxTransferFunctionNr->removeItem(index[i]);
    this->TransferFunction.remove(index[i]);
    this->TransferFunctionGradient.remove(index[i]);
  }
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::clearTransferFunction()
{
  QTransferFunction tf;
  QTransferFunctionGradient g;

  //this->createPresetBlackBody(tf, g);
  this->createColorPreset(0, tf, g);
  this->canvas->setTransferFunction(tf.first, tf.second, g);

  this->comboBoxTransferFunctionNr->clear();
  this->TransferFunction.clear();
  this->TransferFunctionGradient.clear();
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::getTransferFunction(QString label, QVector< QPair<double, double*> > &color, QVector< QPair<double, double> > &opacity,QVector< QPair<double, double> > &gradient)
{
  bool hit = false;
  for(int i=0; i<(int)this->comboBoxTransferFunctionNr->count(); i++)
  {
    if(this->comboBoxTransferFunctionNr->itemText(i).compare(label) == 0)
    {
      if(i < (int)this->TransferFunction.size()) 
      {
        color    = this->TransferFunction[i].first;
        opacity  = this->TransferFunction[i].second;
        gradient = this->TransferFunctionGradient[i];

        hit = true;
      }    
    }
  }

  if(!hit)
    this->getTransferFunction(color, opacity, gradient);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::getTransferFunction(int index, QVector< QPair<double, double*> > &color, QVector< QPair<double, double> > &opacity, QVector< QPair<double, double> > &gradient)
{
  bool hit = false;

  if((index >=0) && (index < (int)this->TransferFunction.size())) 
  {
    color    = this->TransferFunction[index].first;
    opacity  = this->TransferFunction[index].second;
    gradient = this->TransferFunctionGradient[index];

    hit = true;
  }    

  if(!hit)
    this->getTransferFunction(color, opacity, gradient);
}

//----------------------------------------------------------------------------
void  qtTransferFunctionWidget::saveTransferFunction(QString path)
{
  ofstream                  myfile;
  QTransferFunction         tf;
  QTransferFunctionGradient g;
  int colorSize, opacitySize, gradientSize;

  this->canvas->getTransferFunction(tf.first, tf.second, g);

  colorSize    = (int)tf.first.size();
  opacitySize  = (int)tf.second.size();
  gradientSize = (int)g.size();

  myfile.open(path.toStdString().c_str());    

  if (myfile.is_open())
  {
    // Write header
    myfile << "COLOR "    << colorSize << "\n";
    myfile << "OPACITY "  << opacitySize << "\n";
    myfile << "GRADIENT " << gradientSize << "\n";

    // Write colors
    QTransferFunctionColor c = tf.first;
    for(int i=0; i<(int)colorSize; i++)
    {
      myfile << c[i].first << " " << c[i].second[0] << " " << c[i].second[1] << " " << c[i].second[2] << "\n";
    }

    // Write opacity
    QTransferFunctionOpacity o = tf.second;
    for(int i=0; i<(int)opacitySize; i++)
    {
      myfile << o[i].first << " "  << o[i].second << "\n";
    }

    // Write gradient
    for(int i=0; i<(int)gradientSize; i++)
    {
      myfile << g[i].first << " "  << g[i].second << "\n";
    }

    myfile.close();
  }
  else cout << "Error saving transfer function" << endl;
}

//----------------------------------------------------------------------------
void  qtTransferFunctionWidget::loadTransferFunction(QString path)
{
  int                       index, colorSize, opacitySize, gradientSize, count;
  ifstream                  myfile;
  QTransferFunction         tf;
  QTransferFunctionGradient g;
  std::string               header;
  std::string               line, word;

  index = this->comboBoxTransferFunctionNr->currentIndex();

  myfile.open(path.toStdString().c_str());    

  if (myfile.is_open())
  {
    getline (myfile, header);
    header      = header.erase(0, 6);
    colorSize   = atoi(header.c_str());

    getline (myfile, header);
    header      = header.erase(0, 7);
    opacitySize = atoi(header.c_str());

    getline (myfile, header);
    header       = header.erase(0, 8);
    gradientSize = atoi(header.c_str());

    count = 0;
    while ( myfile.good() && count<(colorSize+opacitySize+gradientSize))
    {
      getline(myfile, line);

      if(count < colorSize)
      {        
        QPair<double, double*> item;
        double *c = new double[3];
        std::istringstream iss(line, std::istringstream::in);

        iss >> word;       
        item.first = atof(word.c_str()); 

        iss >> word;                    
        c[0] = atof(word.c_str()); 
        iss >> word;            
        c[1] = atof(word.c_str()); 
        iss >> word;            
        c[2] = atof(word.c_str()); 
        item.second = c;

        tf.first.push_back(item);  
      }
      else
      if(count < (colorSize + opacitySize))
      {
        QPair<double, double> item;        
        std::istringstream iss(line, std::istringstream::in);

        iss >> word;            
        item.first = atof(word.c_str()); 

        iss >> word;                    
        item.second = atof(word.c_str()); 
        
        tf.second.push_back(item);  
      }
      else
      {
        QPair<double, double> item;        
        std::istringstream iss(line, std::istringstream::in);

        iss >> word;            
        item.first = atof(word.c_str()); 

        iss >> word;                    
        item.second = atof(word.c_str()); 
        
        g.push_back(item);  
      }
      count++;
    }  
    myfile.close();  

    this->canvas->setTransferFunction(tf.first, tf.second, g);

    if((index >=0 ) && (index<(int)this->TransferFunction.size()))
    {
      this->TransferFunction[index]         = tf;
      this->TransferFunctionGradient[index] = g;
    }
  }
  else cout << "Error loading transfer function" << endl;
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::addHistogram(qfeHistogram *histogram)
{  
  this->canvas->addHistogram(histogram);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::setHistogram(int current)
{  
  if(this->checkBoxHistogramUpdate->isChecked())
    this->canvas->setHistogramNumber(current);  
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::clearHistograms()
{  
  this->canvas->clearHistograms();
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::updateTransferFunction(int index)
{
  if((index >= 0) && (index < this->TransferFunction.size()))
  {
    this->canvas->setTransferFunction(this->TransferFunction[index].first, this->TransferFunction[index].second, this->TransferFunctionGradient[index]);

	this->updateGlobalTransferFunction(index);
  }
}

void qtTransferFunctionWidget::updateGlobalTransferFunction(int index)
{
	qfeColorMap colorMap;
	QTransferFunctionColor tfc;
	QTransferFunctionOpacity tfo;
	QTransferFunctionGradient tfg;
	
	this->canvas->getTransferFunction(tfc, tfo, tfg);
	
	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 
	vector<qfeOpacityMapping> g;
	
	rgb.resize(tfc.size());
	for(unsigned int i = 0; i<tfc.size(); i++)
	{
		rgb[i].value = tfc.at(i).first;
		
		rgb[i].color.r = tfc.at(i).second[0];
		rgb[i].color.g = tfc.at(i).second[1];
		rgb[i].color.b = tfc.at(i).second[2];
	}
	
	o.resize(tfo.size());
	for(unsigned int i = 0; i<tfo.size(); i++)
	{
		o[i].value = tfo.at(i).first;
		o[i].opacity = tfo.at(i).second;
	}

	g.resize(tfg.size());
	for(unsigned int i = 0; i<tfg.size(); i++)
	{
		g[i].value = tfg.at(i).first;
		g[i].opacity = tfg.at(i).second;
	}

	colorMap.qfeSetColorMapConstantLightness(this->checkBoxLightnessConstant->isChecked());
	colorMap.qfeSetColorMapQuantization(this->spinBoxColorQuantization->value());

	colorMap.qfeSetColorMapRGB(rgb.size(), rgb);
	colorMap.qfeSetColorMapA(o.size(), o);
	colorMap.qfeSetColorMapG(g.size(), g);
	colorMap.qfeSetColorMapInterpolation(comboBoxColorInterpolation->currentIndex());
	colorMap.qfeSetColorMapSpace(comboBoxColorSpace->currentIndex());

	colorMaps.qfeUpdateTransferFunctionColorMap(colorMap);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::updateColorPreset(int index)
{
  // Set the color presets
  QVector< QPair<double, double*> > color;
  QVector< QPair<double, double>  > opacity;
  QVector< QPair<double, double>  > gradient;

  double *c[7];
  for(int i=0; i<7; i++)
  {
    c[i] = new double[3];
  }
  
  QTransferFunction tf, tf2;
  QTransferFunctionGradient g2;

  this->canvas->getTransferFunction(tf2.first, tf2.second, g2);

  /*
  switch(index)
  {
  case 0 :  // black body 
            this->createPresetBlackBody(tf, gradient);
            break;
  case 1 :  // rainbow                 
            this->createPresetRainbow(tf, gradient);
            break;
  case 2 :  // doppler 1
            this->createPresetDoppler1(tf, gradient);
            break;
  case 3 :  // doppler 2
            this->createPresetDoppler2(tf, gradient);
            break;
  case 4 :  // diverging 1              
            this->createPresetDiverging1(tf, gradient);
            break;
  case 5 :  // diverging 2
            this->createPresetDiverging2(tf, gradient);
            break;
  case 6 :  // diverging 3
            this->createPresetDiverging3(tf, gradient);
            break;
  case 7 :  // cool warm
            this->createPresetCoolWarm(tf, gradient);
            break;
  case 8 :  // heat
            this->createPresetHeat(tf, gradient);
            break;
  case 9 :  // gray
            this->createPresetGrey(tf, gradient);            
            break;   
  case 10:  // pink to gray
            this->createPresetPinkGray(tf, gradient);
            break;
  } 
  */
  this->createColorPreset(index, tf, gradient);

  this->canvas->setTransferFunction(tf.first, tf2.second, g2);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::updateOpacityPreset(int index)
{
  // Set the color presets
  QVector< QPair<double, double*> > color;
  QVector< QPair<double, double>  > opacity;
  QVector< QPair<double, double>  > gradient;

  double *c[7];
  for(int i=0; i<7; i++)
  {
    c[i] = new double[3];
  }
  
  QTransferFunction tf, tf2;
  QTransferFunctionGradient g2;

  this->canvas->getTransferFunction(tf2.first, tf2.second, g2);

  switch(index)
  {
  case 0 :  // full             
            this->createPresetFull(tf, gradient);
            break;
  case 1 :  // step                 
            this->createPresetStep(tf, gradient);
            break;
  case 2 :  // pca-p
            this->createPresetPcap(tf, gradient);
            break;
  case 3 :  // pca-m
            this->createPresetPcam(tf, gradient);
            break;
  case 4 :  // ssfp         
            this->createPresetSsfp(tf, gradient);
            break;
  case 5 :  // tmip
            this->createPresetTmip(tf, gradient);            
            break;   
  case 6 : // IVR
    createPresetIVR(tf, gradient);
    break;
  case 7 : // IVR2
    createPresetIVR2(tf, gradient);
    break;
  case 8 : // IVR 3
    createPresetIVR3(tf, gradient);
    break;
  case 9 : // IVR 4
    createPresetIVR4(tf, gradient);
    break;
  case 10: // increasing
    createPresetIncreasing(tf, gradient);
    break;
  } 

  this->canvas->setTransferFunction(tf2.first, tf.second, gradient);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::emitUpdate()
{
  int index;

  index = this->comboBoxTransferFunctionNr->currentIndex();

  if((index >= 0) && (index < this->TransferFunction.size()))
  {
    this->canvas->getTransferFunction(this->TransferFunction[index].first, this->TransferFunction[index].second, this->TransferFunctionGradient[index]); 

	this->updateGlobalTransferFunction(index);
  }

  emit transferFunctionChanged();
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::updateColorInterpolation(int index)
{
  this->canvas->setColorInterpolation(index);
  this->updateGlobalTransferFunction(index);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::updateColorSpace(int index)
{
  this->canvas->setColorSpace(index);
  this->updateGlobalTransferFunction(index);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::updateColorQuantization(int value)
{
  this->canvas->setColorQuantization(value);
  this->updateGlobalTransferFunction(value);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::updateColorLightnessConstant(int value)
{
  this->canvas->setColorLightnessConstant(value);
  this->updateGlobalTransferFunction(value);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::updateColorPadding(int value)
{
  this->canvas->setColorPadding(value);
  this->updateGlobalTransferFunction(value);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::updateHistogramScale(int value)
{
  this->canvas->setHistogramScale(value);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::updateHistogramScaleType(int value)
{
  this->canvas->setHistogramScaleType(value);
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::updateHistogramVisibility(int value)
{
  this->canvas->setHistogramVisibility(value);
}

void qtTransferFunctionWidget::createColorPreset(unsigned int id, QTransferFunction &tf, QTransferFunctionGradient &g)
{
	qfeColorMap cm;
	std::string name;

	colorMaps.qfeGetColorMap(id, name, cm);

	unsigned int n = 0;
	vector<qfeRGBMapping> listRGB;

	cm.qfeGetColorMapRGB(n, listRGB);

	for(unsigned int i = 0; i<listRGB.size(); i++)
	{
		double *color = new double[3];
		color[0] = listRGB.at(i).color.r;
		color[1] = listRGB.at(i).color.g;
		color[2] = listRGB.at(i).color.b;
		
		tf.first.push_back(QPair<double, double*>(listRGB.at(i).value, color)); 
	}
	
	tf.second.push_back(QPair<double, double>(0.0,1.0));
	tf.second.push_back(QPair<double, double>(1.0,1.0)); 
	
	g.push_back(QPair<double, double>(0.0,1.0));
	g.push_back(QPair<double, double>(1.0,1.0)); 
}

/*
//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetBlackBody(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double *c[4];
  for(int i=0; i<4; i++)
  {
    c[i] = new double[3];
  }

  c[0][0] = 0.000; c[0][1] = 0.000; c[0][2] = 0.000;
  c[1][0] = 0.902; c[1][1] = 0.000; c[1][2] = 0.000;
  c[2][0] = 0.902; c[2][1] = 0.902; c[2][2] = 0.000;
  c[3][0] = 1.000; c[3][1] = 1.000; c[3][2] = 1.000;

  tf.first.push_back(QPair<double, double*>(0.00, c[0]));           
  tf.first.push_back(QPair<double, double*>(0.40, c[1]));
  tf.first.push_back(QPair<double, double*>(0.80, c[2]));
  tf.first.push_back(QPair<double, double*>(1.00, c[3]));

  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0)); 

  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetRainbow(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double *c[5];
  for(int i=0; i<5; i++)
  {
    c[i] = new double[3];
  }

  c[0][0] = 0.0; c[0][1] = 0.0; c[0][2] = 1.0;
  c[1][0] = 0.0; c[1][1] = 1.0; c[1][2] = 1.0;
  c[2][0] = 0.0; c[2][1] = 1.0; c[2][2] = 0.0;
  c[3][0] = 1.0; c[3][1] = 1.0; c[3][2] = 0.0;
  c[4][0] = 1.0; c[4][1] = 0.0; c[4][2] = 0.0;

  tf.first.push_back(QPair<double, double*>(0.00, c[0]));
  tf.first.push_back(QPair<double, double*>(0.25, c[1]));
  tf.first.push_back(QPair<double, double*>(0.50, c[2]));
  tf.first.push_back(QPair<double, double*>(0.75, c[3]));
  tf.first.push_back(QPair<double, double*>(1.00, c[4]));

  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0));   

  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetDoppler1(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double *c[5];
  for(int i=0; i<5; i++)
  {
    c[i] = new double[3];
  }

  c[0][0] = 0.84;  c[0][1] = 0.879; c[0][2] = 0.355;
  c[1][0] = 0.594; c[1][1] = 0.137; c[1][2] = 0.0;
  c[2][0] = 0.001; c[2][1] = 0.001; c[2][2] = 0.001;
  c[3][0] = 0.047; c[3][1] = 0.551; c[3][2] = 1.0;
  c[4][0] = 0.484; c[4][1] = 0.965; c[4][2] = 0.99;

  tf.first.push_back(QPair<double, double*>(0.00, c[0]));
  tf.first.push_back(QPair<double, double*>(0.25, c[1]));
  tf.first.push_back(QPair<double, double*>(0.50, c[2]));
  tf.first.push_back(QPair<double, double*>(0.75, c[3]));
  tf.first.push_back(QPair<double, double*>(1.00, c[4]));

  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0));   

  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetDoppler2(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double *c[5];
  for(int i=0; i<5; i++)
  {
    c[i] = new double[3];
  }

  c[0][0] = 0.84;  c[0][1] = 0.879; c[0][2] = 0.355;  
  c[1][0] = 0.594; c[1][1] = 0.137; c[1][2] = 0.0;
  c[2][0] = 0.001; c[2][1] = 0.001; c[2][2] = 0.001;
  c[3][0] = 0.047; c[3][1] = 0.551; c[3][2] = 1.0;  
  c[4][0] = 0.0;   c[4][1] = 1.0;   c[4][2] = 0.351;

  tf.first.push_back(QPair<double, double*>(0.00, c[0]));
  tf.first.push_back(QPair<double, double*>(0.25, c[1]));
  tf.first.push_back(QPair<double, double*>(0.50, c[2]));
  tf.first.push_back(QPair<double, double*>(0.75, c[3]));
  tf.first.push_back(QPair<double, double*>(1.00, c[4]));

  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0));   

  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetDiverging1(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double *c[3];
  for(int i=0; i<3; i++)
  {
    c[i] = new double[3];
  }

  c[0][0] = 0.0; c[0][1] = 0.0; c[0][2] = 1.0;
  c[1][0] = 1.0; c[1][1] = 1.0; c[1][2] = 1.0;
  c[2][0] = 1.0; c[2][1] = 0.0; c[2][2] = 0.0;

  tf.first.push_back(QPair<double, double*>(0.0, c[0]));           
  tf.first.push_back(QPair<double, double*>(0.5, c[1]));
  tf.first.push_back(QPair<double, double*>(1.0, c[2]));
  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0));   
  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetDiverging2(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double *c[5];
  for(int i=0; i<5; i++)
  {
    c[i] = new double[3];
  }

  c[0][0] = 0.1020; c[0][1] = 0.5882; c[0][2] = 0.2550;
  c[1][0] = 0.6510; c[1][1] = 0.8510; c[1][2] = 0.4157;
  c[2][0] = 1.0000; c[2][1] = 1.0000; c[2][2] = 0.7490;
  c[3][0] = 0.9921; c[3][1] = 0.6823; c[3][2] = 0.3804;
  c[4][0] = 0.8431; c[4][1] = 0.0980; c[4][2] = 0.1098;

  tf.first.push_back(QPair<double, double*>(0.00, c[0]));           
  tf.first.push_back(QPair<double, double*>(0.25, c[1]));
  tf.first.push_back(QPair<double, double*>(0.50, c[2]));
  tf.first.push_back(QPair<double, double*>(0.75, c[3]));
  tf.first.push_back(QPair<double, double*>(1.00, c[4]));
  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0));        
  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetDiverging3(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double *c[5];
  for(int i=0; i<5; i++)
  {
    c[i] = new double[3];
  }

  c[0][0] = 140.0/255.0; c[0][1] = 140.0/255.0; c[0][2] = 140.0/255.0;    
  c[1][0] = 215.0/255.0; c[1][1] =  25.0/255.0; c[1][2] =  28.0/255.0;  
  c[2][0] = 255.0/255.0; c[2][1] = 255.0/255.0; c[2][2] = 191.0/255.0;
  c[3][0] =  26.0/255.0; c[3][1] = 150.0/255.0; c[3][2] =  65.0/255.0;
  c[4][0] = 140.0/255.0; c[4][1] = 140.0/255.0; c[4][2] = 140.0/255.0;    

  tf.first.push_back(QPair<double, double*>(0.00, c[0]));           
  tf.first.push_back(QPair<double, double*>(0.25, c[1]));
  tf.first.push_back(QPair<double, double*>(0.50, c[2]));
  tf.first.push_back(QPair<double, double*>(0.75, c[3]));
  tf.first.push_back(QPair<double, double*>(1.00, c[4]));
  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0));        
  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetCoolWarm(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double *c[2];
  for(int i=0; i<2; i++)
  {
    c[i] = new double[3];
  }

  c[0][0] = 0.0; c[0][1] = 0.0; c[0][2] = 0.8;
  c[1][0] = 0.8; c[1][1] = 0.8; c[1][2] = 0.0;

  tf.first.push_back(QPair<double, double*>(0.0, c[0]));           
  tf.first.push_back(QPair<double, double*>(1.0, c[1]));
  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0)); 
  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetHeat(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double *c[2];
  for(int i=0; i<2; i++)
  {
    c[i] = new double[3];
  }

  c[0][0] = 1.0; c[0][1] = 1.0; c[0][2] = 0.0;
  c[1][0] = 1.0; c[1][1] = 0.0; c[1][2] = 0.0;

  tf.first.push_back(QPair<double, double*>(0.0, c[0]));           
  tf.first.push_back(QPair<double, double*>(1.0, c[1]));
  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0));
  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetGrey(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double *c[2];
  for(int i=0; i<2; i++)
  {
    c[i] = new double[3];
  }

  c[0][0] = c[0][1] = c[0][2] = 0.0;
  c[1][0] = c[1][1] = c[1][2] = 1.0;

  tf.first.push_back(QPair<double, double*>(0.0, c[0]));           
  tf.first.push_back(QPair<double, double*>(1.0, c[1]));

  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0));

  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

void qtTransferFunctionWidget::createPresetPinkGray(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double *c[2];
  for(int i=0; i<2; i++)
  {
    c[i] = new double[3];
  }

  c[0][0] = 1.0; c[0][1] = 0.4; c[0][2] = 0.4;
  c[1][0] = 0.5; c[1][1] = 0.5; c[1][2] = 0.7;

  tf.first.push_back(QPair<double, double*>(0.0, c[0]));           
  tf.first.push_back(QPair<double, double*>(1.0, c[1]));
  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0));
  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}
*/

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetFull(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double c[3];

  c[0] = c[1] = c[2] = 1.0;
 
  tf.first.push_back(QPair<double, double*>(0.0, c));  
  tf.first.push_back(QPair<double, double*>(1.0, c));

  tf.second.push_back(QPair<double, double>(0.0,1.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0));
  
  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetStep(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double c[3];

  c[0] = c[1] = c[2] = 1.0;
 
  tf.first.push_back(QPair<double, double*>(0.0, c));  
  tf.first.push_back(QPair<double, double*>(1.0, c));

  tf.second.push_back(QPair<double, double>(0.0, 0.0));
  tf.second.push_back(QPair<double, double>(0.5, 0.0));
  tf.second.push_back(QPair<double, double>(0.5, 1.0));
  tf.second.push_back(QPair<double, double>(1.0, 1.0));
  
  g.push_back(QPair<double, double>(0.0,1.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetPcap(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double c[3];

  c[0] = c[1] = c[2] = 1.0;
 
  tf.first.push_back(QPair<double, double*>(0.0, c));  
  tf.first.push_back(QPair<double, double*>(1.0, c));

  tf.second.push_back(QPair<double, double>(0.0, 0.0));
  tf.second.push_back(QPair<double, double>(0.1, 0.0));
  tf.second.push_back(QPair<double, double>(0.14,0.35));
  tf.second.push_back(QPair<double, double>(1.0, 0.35));

  g.push_back(QPair<double, double>(0.0,  0.0 ));
  g.push_back(QPair<double, double>(0.05, 0.0 ));
  g.push_back(QPair<double, double>(0.06, 0.05));
  g.push_back(QPair<double, double>(0.25, 0.05));
  g.push_back(QPair<double, double>(0.30, 0.0));
  g.push_back(QPair<double, double>(1.0,  0.0));
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetPcam(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double c[3];

  c[0] = c[1] = c[2] = 1.0;
 
  tf.first.push_back(QPair<double, double*>(0.0, c));  
  tf.first.push_back(QPair<double, double*>(1.0, c));

  tf.second.push_back(QPair<double, double>(0.0,  0.0));
  tf.second.push_back(QPair<double, double>(0.14, 0.0));
  tf.second.push_back(QPair<double, double>(0.2,  0.4));
  tf.second.push_back(QPair<double, double>(1.0,  0.4));

  g.push_back(QPair<double, double>(0.0,  0.0 ));
  g.push_back(QPair<double, double>(0.05, 0.0 ));
  g.push_back(QPair<double, double>(0.06, 0.05));
  g.push_back(QPair<double, double>(0.25, 0.05));
  g.push_back(QPair<double, double>(0.30, 0.0));
  g.push_back(QPair<double, double>(1.0,  0.0));
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetSsfp(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double c[3];

  c[0] = c[1] = c[2] = 1.0;
 
  tf.first.push_back(QPair<double, double*>(0.0, c));  
  tf.first.push_back(QPair<double, double*>(1.0, c));

  tf.second.push_back(QPair<double, double>(0.0,  0.0));
  tf.second.push_back(QPair<double, double>(0.59, 0.0));
  tf.second.push_back(QPair<double, double>(0.67, 0.1));
  tf.second.push_back(QPair<double, double>(0.8,  0.1));
  tf.second.push_back(QPair<double, double>(0.92, 0));
  tf.second.push_back(QPair<double, double>(1.0,  0));

  g.push_back(QPair<double, double>(0.0,  0.0 ));
  g.push_back(QPair<double, double>(0.08, 0.0 ));
  g.push_back(QPair<double, double>(0.11, 0.08));
  g.push_back(QPair<double, double>(0.24, 0.08));
  g.push_back(QPair<double, double>(0.30, 0.0));
  g.push_back(QPair<double, double>(1.0,  0.0));
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetTmip(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double c[3];

  c[0] = c[1] = c[2] = 1.0;
 
  tf.first.push_back(QPair<double, double*>(0.0, c));  
  tf.first.push_back(QPair<double, double*>(1.0, c));

  tf.second.push_back(QPair<double, double>(0.0, 0.0));
  tf.second.push_back(QPair<double, double>(0.2, 0.0));
  tf.second.push_back(QPair<double, double>(0.4, 0.5));
  tf.second.push_back(QPair<double, double>(1.0, 0.5));

  g.push_back(QPair<double, double>(0.0,  0.0 ));
  g.push_back(QPair<double, double>(0.05, 0.0 ));
  g.push_back(QPair<double, double>(0.06, 0.05));
  g.push_back(QPair<double, double>(0.25, 0.05));
  g.push_back(QPair<double, double>(0.30, 0.0));
  g.push_back(QPair<double, double>(1.0,  0.0)); 
}

//----------------------------------------------------------------------------
void qtTransferFunctionWidget::createPresetIVR(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double c[3];

  c[0] = c[1] = c[2] = 1.0;

  tf.first.push_back(QPair<double, double*>(0.0, c));  
  tf.first.push_back(QPair<double, double*>(1.0, c));

  tf.second.push_back(QPair<double, double>(0.0, 0.0));
  tf.second.push_back(QPair<double, double>(0.29, 0.0));
  tf.second.push_back(QPair<double, double>(0.35, 0.9));
  tf.second.push_back(QPair<double, double>(0.45, 0.0));
  tf.second.push_back(QPair<double, double>(1.0, 0.0));

  g.push_back(QPair<double, double>(0.0,  0.0));
  g.push_back(QPair<double, double>(0.15,  0.0));
  g.push_back(QPair<double, double>(0.6,  0.08));
  g.push_back(QPair<double, double>(1.0,  0.0)); 
}

void qtTransferFunctionWidget::createPresetIVR2(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double c[3];

  c[0] = c[1] = c[2] = 1.0;

  tf.first.push_back(QPair<double, double*>(0.0, c));  
  tf.first.push_back(QPair<double, double*>(1.0, c));

  tf.second.push_back(QPair<double, double>(0.0, 0.0));
  tf.second.push_back(QPair<double, double>(0.28, 0.0));
  tf.second.push_back(QPair<double, double>(0.35, 0.07));
  tf.second.push_back(QPair<double, double>(0.42, 0.0));
  tf.second.push_back(QPair<double, double>(1.0, 0.0));

  g.push_back(QPair<double, double>(0.0,  0.0));
  g.push_back(QPair<double, double>(0.15,  0.0));
  g.push_back(QPair<double, double>(0.6,  0.08));
  g.push_back(QPair<double, double>(1.0,  0.0)); 
}

void qtTransferFunctionWidget::createPresetIVR3(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double c[3];

  c[0] = c[1] = c[2] = 1.0;

  tf.first.push_back(QPair<double, double*>(0.0, c));  
  tf.first.push_back(QPair<double, double*>(1.0, c));

  tf.second.push_back(QPair<double, double>(0.0, 0.0));
  tf.second.push_back(QPair<double, double>(0.15, 0.0));
  tf.second.push_back(QPair<double, double>(0.2, 0.07));
  tf.second.push_back(QPair<double, double>(0.3, 0.0));
  tf.second.push_back(QPair<double, double>(1.0, 0.0));

  g.push_back(QPair<double, double>(0.0,  0.0));
  g.push_back(QPair<double, double>(0.15,  0.0));
  g.push_back(QPair<double, double>(0.6,  0.08));
  g.push_back(QPair<double, double>(1.0,  0.0)); 
}

void qtTransferFunctionWidget::createPresetIVR4(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double c[3];

  c[0] = c[1] = c[2] = 1.0;

  tf.first.push_back(QPair<double, double*>(0.0, c));  
  tf.first.push_back(QPair<double, double*>(1.0, c));

  tf.second.push_back(QPair<double, double>(0.0, 0.0));
  tf.second.push_back(QPair<double, double>(0.26, 0.0));
  tf.second.push_back(QPair<double, double>(0.29, 0.24));
  tf.second.push_back(QPair<double, double>(0.33, 0.0));
  tf.second.push_back(QPair<double, double>(1.0, 0.0));

  g.push_back(QPair<double, double>(0.0,  0.0));
  g.push_back(QPair<double, double>(0.15,  0.0));
  g.push_back(QPair<double, double>(0.6,  0.08));
  g.push_back(QPair<double, double>(1.0,  0.0)); 
}


void qtTransferFunctionWidget::createPresetIncreasing(QTransferFunction &tf, QTransferFunctionGradient &g)
{
  double c[3];

  c[0] = c[1] = c[2] = 1.0;
 
  tf.first.push_back(QPair<double, double*>(0.0, c));  
  tf.first.push_back(QPair<double, double*>(1.0, c));

  tf.second.push_back(QPair<double, double>(0.0,0.0));
  tf.second.push_back(QPair<double, double>(1.0,1.0));
  
  g.push_back(QPair<double, double>(0.0,0.0));
  g.push_back(QPair<double, double>(1.0,1.0)); 
}

