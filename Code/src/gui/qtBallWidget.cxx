#include "qtBallWidget.h"
#include "vtkColorTransferFunction.h"
#include "vtkPiecewiseFunction.h"

//----------------------------------------------------------------------------
qtBallWidget::qtBallWidget()
{
  this->widgetHeight = 70;

  this->setFixedHeight(this->widgetHeight);

  this->directionVector = QVector4D(0.0,0.0,1.0,1.0);
}

//----------------------------------------------------------------------------
qtBallWidget::~qtBallWidget()
{
}

//----------------------------------------------------------------------------
QVector3D qtBallWidget::getDirectionVector()
{
  return this->directionVector.toVector3D();
}

//----------------------------------------------------------------------------
void qtBallWidget::setDirectionVector(QVector3D vector)
{
  this->directionVector = vector.normalized();
  this->directionVector.setW(1.0);
}


//----------------------------------------------------------------------------
void qtBallWidget::mousePressEvent(QMouseEvent *event)
{
  this->mouseLastPos = this->translateMousePosition(QVector2D(event->pos().x(), event->pos().y()));
}

//----------------------------------------------------------------------------
void qtBallWidget::mouseReleaseEvent(QMouseEvent *event)
{

}

//----------------------------------------------------------------------------
void qtBallWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

//----------------------------------------------------------------------------
void qtBallWidget::mouseMoveEvent(QMouseEvent *event)
{
  QVector2D  mouseCurrentPos;
  QMatrix4x4 rotationMatrix;
    
  mouseCurrentPos = this->translateMousePosition(QVector2D(event->pos().x(), event->pos().y()));
  
  rotationMatrix  = this->moveTrackball(this->mouseLastPos, mouseCurrentPos, this->ballRadius);

  this->directionVector = rotationMatrix * this->directionVector;
  this->directionVector.normalize();
  this->directionVector = this->directionVector / this->directionVector.w();

  this->mouseLastPos = mouseCurrentPos;

  update();

  emit ballWidgetChanged();
}

//----------------------------------------------------------------------------
void qtBallWidget::resizeEvent(QResizeEvent *event)
{

}

//----------------------------------------------------------------------------
void qtBallWidget::paintEvent(QPaintEvent *event)
{
  QPainter p(this);
  p.setRenderHint(QPainter::Antialiasing, true);
  p.setRenderHint(QPainter::HighQualityAntialiasing, true);

  // update ball variables
  this->ballDiameter  = 0.9 * height();
  this->ballRadius    = 0.5 * this->ballDiameter;
  this->ballOrigin    = QVector2D(width()/2.0, height()/2.0);  

  this->directionVectorScaled = this->directionVector * this->ballRadius;

  // draw border
  p.setPen(QPen(Qt::black));
  p.fillRect(0, 0, width(), height(), QBrush(QColor(224, 223, 227, 0)));

  // draw ball
  this->paintBall(&p);
  this->paintFrontPoint(&p);
  this->paintRearPoint(&p);
  this->paintArrow(&p);
 
}

//----------------------------------------------------------------------------
void qtBallWidget::paintBall(QPainter *p)
{
  float alpha    = 128.0;

  float offsetX  = 10;
  float offsetY  = 10;
 
  QRadialGradient grad(this->ballOrigin.x(), this->ballOrigin.y(), this->ballRadius, this->ballOrigin.x() - offsetX, this->ballOrigin.y() - offsetY) ;
  grad.setColorAt(0.0, QColor(255,255,255,alpha));
  grad.setColorAt(1.0, QColor(180,180,180, alpha));

  p->setPen(QPen(QColor(50, 50, 50, 255), 2));
  p->setBrush(QBrush(grad));

  p->drawEllipse(QRectF(this->ballOrigin.x() - this->ballRadius, this->ballOrigin.y() - this->ballRadius, this->ballDiameter, this->ballDiameter));
}

//----------------------------------------------------------------------------
void qtBallWidget::paintFrontPoint(QPainter *p)
{
  p->setPen(QPen(QColor(255, 255, 255, 255), 2));

  p->drawEllipse(QRectF(-1 * this->directionVectorScaled.x() + 0.5*width() -1, -1 * this->directionVectorScaled.y() + 0.5*height() -1, 2.0, 2.0));
}

//----------------------------------------------------------------------------
void qtBallWidget::paintRearPoint(QPainter *p)
{
  p->setPen(QPen(QColor(255, 0, 0, 255), 2));

  p->drawEllipse(QRectF(this->directionVectorScaled.x() + 0.5*width() -1, this->directionVectorScaled.y() + 0.5*height() -1, 2.0, 2.0));
}

//----------------------------------------------------------------------------
void qtBallWidget::paintArrow(QPainter *p)
{
  p->setPen(QPen(QColor(0, 0, 0, 255), 2));

  p->drawLine(QPointF(-1*this->directionVectorScaled.x() + 0.5*width(), -1*this->directionVectorScaled.y() + 0.5*height()),
              QPointF(   this->directionVectorScaled.x() + 0.5*width(),    this->directionVectorScaled.y() + 0.5*height()));
}

//----------------------------------------------------------------------------
QMatrix4x4 qtBallWidget::moveTrackball(QVector2D p1, QVector2D p2, float radius)
{
  QMatrix4x4  rotationMatrix;  
  QQuaternion rotationQuat;

  QVector3D p = QVector3D(p1.x(), p1.y(), 0.0);
  QVector3D q = QVector3D(p2.x(), p2.y(), 0.0);
  QVector3D axis;
  float     angle;

  // Project the points to the trackball sphere
  p.setZ(this->projectToSphere(p1, radius));
  q.setZ(this->projectToSphere(p2, radius));

  // Determine the axis of rotation (cross product of P1-P2 and O-P1)
  axis = QVector3D::crossProduct(p, q);

  // Determine the angle of rotation
  angle = 16.0 * asinf((p-q).length() / (2.0 * radius));

  // Axis to quaternion
  rotationQuat = QQuaternion::fromAxisAndAngle(axis, angle);

  // Quaternion to rotation matrix
  rotationMatrix.rotate(rotationQuat);

  return rotationMatrix;
}

//----------------------------------------------------------------------------
QVector2D qtBallWidget::translateMousePosition(QVector2D position)
{
  return QVector2D(position.x() - 0.5 * width(),
                   position.y() - 0.5 * height());
} 

//----------------------------------------------------------------------------
float qtBallWidget::projectToSphere(QVector2D point, float radius)
{
  float d = point.length();

  if(d < radius * QFE_SQRT1_2)
  {
    return sqrtf(radius * radius - d * d);
  }
  else
  {
    float t = radius / QFE_SQRT2;
    return t* t / d;
  }  
}


