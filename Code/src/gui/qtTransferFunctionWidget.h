/**
* \file   qtTransferFunctionWidget.h
* \author Roy van Pelt
* \class  qtTransferFunctionWidget
* \brief  Implements a 1D transfer function widget
*
*/

#ifndef __qtTransferFunctionWidget_h
#define __qtTransferFunctionWidget_h

#include <algorithm>
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <QtGui>

#include "qtTransferFunctionCanvas.h"
#include "qfeHistogram.h"

#include "qfeColorMaps.h"

typedef  QVector< QPair<double, double*> >  QTransferFunctionColor;  
typedef  QVector< QPair<double, double> >   QTransferFunctionOpacity;  
typedef  QVector< QPair<double, double> >   QTransferFunctionGradient;  
typedef  QPair< QTransferFunctionColor, QTransferFunctionOpacity >    QTransferFunction;  

class qtTransferFunctionWidget : public QWidget
{
	Q_OBJECT

public:
	qtTransferFunctionWidget(QWidget *parent = 0);
	~qtTransferFunctionWidget();	

  void  addTransferFunction(QString label);
  void  removeTransferFunction(QString label);
  void  clearTransferFunction();

  void  addHistogram(qfeHistogram *histogram);   
  void  setHistogram(int current);
  void  clearHistograms();  
  void  getColorInterpolation(int &mode)         
          {this->canvas->getColorInterpolation(mode);};  
  void  getColorQuantization(int &steps)         
          {this->canvas->getColorQuantization(steps);};  
  void  getColorSpace(int &space)                
          {this->canvas->getColorSpace(space);};    
  void  getColorLightnessConstant(bool &constant)
          {this->canvas->getColorLightnessConstant(constant);};  
  void  getColorPadding(bool &padding)           
          {this->canvas->getColorPadding(padding);};
  void  getTransferFunction(QVector< QPair<double, double*> > &color, QVector< QPair<double, double> > &opacity, QVector< QPair<double, double> > &gradient)
          {this->canvas->getTransferFunction(color, opacity, gradient);};
  void  getTransferFunction(QString label, QVector< QPair<double, double*> > &color, QVector< QPair<double, double> > &opacity, QVector< QPair<double, double> > &gradient);
  void  getTransferFunction(int index, QVector< QPair<double, double*> > &color, QVector< QPair<double, double> > &opacity, QVector< QPair<double, double> > &gradient);

  void  saveTransferFunction(QString path);
  void  loadTransferFunction(QString path);
  
  void  setAxisX(QString label, double min, double max){this->canvas->setAxisX(label, min, max);};
  void  setAxisY(QString label, double min, double max){this->canvas->setAxisY(label, min, max);};  

public slots : 
  void emitUpdate();

  void updateTransferFunction(int index);
  void updateGlobalTransferFunction(int index); //to make the tf globally available
  void updateColorPreset(int index);
  void updateOpacityPreset(int index);
  void updateColorInterpolation(int index);
  void updateColorSpace(int index);
  void updateColorQuantization(int value);
  void updateColorLightnessConstant(int value);
  void updateColorPadding(int value);
  void updateHistogramScale(int value);
  void updateHistogramScaleType(int value);
  void updateHistogramVisibility(int value);

signals:
  void transferFunctionChanged();

protected:  
 
  QVector< QTransferFunction >         TransferFunction;
  QVector< QTransferFunctionGradient > TransferFunctionGradient;

  qtTransferFunctionCanvas *canvas;
  QComboBox *comboBoxTransferFunctionNr;
  QComboBox *comboBoxColorPreset;
  QComboBox *comboBoxOpacityPreset;
  QComboBox *comboBoxColorInterpolation;
  QSpinBox  *spinBoxColorQuantization;
  QComboBox *comboBoxColorSpace;
  QCheckBox *checkBoxLightnessConstant;
  QCheckBox *checkBoxPadding;
  QSlider   *sliderHistogramScale;
  QCheckBox *checkBoxHistogramLogarithmic;
  QCheckBox *checkBoxHistogramUpdate;
  QCheckBox *checkBoxHistogramVisible;

private:
  void initComponents();
  void initConnections();
  void initCanvas();

  qfeColorMaps colorMaps;
  
  void createColorPreset(unsigned int id, QTransferFunction &tf, QTransferFunctionGradient &g);
  /*
  void createPresetBlackBody(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetRainbow(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetDoppler1(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetDoppler2(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetDiverging1(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetDiverging2(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetDiverging3(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetCoolWarm(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetHeat(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetGrey(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetPinkGray(QTransferFunction &tf, QTransferFunctionGradient &g);
  */

  void createPresetFull(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetStep(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetPcap(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetPcam(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetSsfp(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetTmip(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetIVR(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetIVR2(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetIVR3(QTransferFunction &tf, QTransferFunctionGradient &g);
  void createPresetIVR4(QTransferFunction &tf, QTransferFunctionGradient &g);  
  void createPresetIncreasing(QTransferFunction &tf, QTransferFunctionGradient &g);
};

#endif
