#include <vtkCanvas.h>
#include <vtkSubCanvas.h>
#include <vtkSubCanvasCollection.h>
#include <vtkCanvasInteractorStyle.h>

#include <vtkObjectFactory.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>

#include <assert.h>

vtkCxxRevisionMacro( vtkCanvas, "$Revision: 1.0 $");
vtkStandardNewMacro( vtkCanvas );

/////////////////////////////////////////////////////////////////
vtkCanvas::vtkCanvas()
{
	this->Interactor = 0;
	this->Window = 0;
	this->Style = 0;
	this->SelectedCanvas = 0;
	this->InteractionOnSelectEnabled = true;

	this->SubCanvasses = vtkSubCanvasCollection::New();
	assert( this->SubCanvasses->GetNumberOfItems() == 0 );

	this->BackgroundRenderer = vtkRenderer::New();
	this->BackgroundRenderer->SetViewport( 0, 0, 1, 1 );
	this->BackgroundRenderer->SetBackground( 0, 0, 0 );

	vtkRenderWindow * window = vtkRenderWindow::New();
	this->SetRenderWindow( window );
	window->Delete();
	window = 0;

	vtkRenderWindowInteractor * interactor = vtkRenderWindowInteractor::New();
	this->SetInteractor( interactor );
	interactor->Delete();
	interactor = 0;
}

/////////////////////////////////////////////////////////////////
vtkCanvas::~vtkCanvas()
{
	this->SetInteractor( 0 );
	this->SetRenderWindow( 0 );

	if( this->SubCanvasses )
	{
		this->SubCanvasses->RemoveAllItems();
		this->SubCanvasses->Delete();
		this->SubCanvasses = 0;
	}

	if( this->BackgroundRenderer )
		this->BackgroundRenderer->Delete();
	this->BackgroundRenderer = 0;
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::AddSubCanvas( vtkSubCanvas * canvas )
{
	assert( canvas != 0 );
	this->SubCanvasses->AddItem( canvas );

	canvas->SetRenderWindow( this->Window );
	canvas->SetInteractor( this->Interactor );
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::RemoveSubCanvas( vtkSubCanvas * canvas )
{
	assert( canvas != 0 );
	
	canvas->SetRenderWindow( 0 );
	canvas->SetInteractor( 0 );

	this->SubCanvasses->RemoveItem( canvas );
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::SelectSubCanvas( vtkSubCanvas * canvas )
{
	if( this->SelectedCanvas == canvas )
		return;

	if( this->IsInteractionOnSelectEnabled() )
	{
		if( this->SelectedCanvas != 0 )
			this->SelectedCanvas->SetInteractionEnabled( false );

		if( canvas != 0 )
			canvas->SetInteractionEnabled( true );
	}

	this->SelectedCanvas = canvas;
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::SelectSubCanvasAt( int x, int y )
{
	this->SelectSubCanvas( this->GetSubCanvasAt( x, y ) );
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::SelectSubCanvasFromLastEvent()
{
	if( this->Interactor == 0 )
	{
		this->SelectSubCanvas( 0 );
		return;
	}

	assert( this->Interactor != 0 );

	int x, y;
	this->Interactor->GetEventPosition( x, y );
	this->SelectSubCanvasAt( x, y );
}

/////////////////////////////////////////////////////////////////
vtkSubCanvas * vtkCanvas::GetSubCanvasAt( int x, int y )
{
	assert( this->SubCanvasses != 0 );
	for( int i = 0; i < this->SubCanvasses->GetNumberOfItems(); ++i )
	{
		vtkSubCanvas * canvas = this->SubCanvasses->GetItem( i );
		assert( canvas != 0 );

		if( canvas->InsideViewport( x, y ) )
		{
			return canvas;
		}
	}

	return 0;
}

/////////////////////////////////////////////////////////////////
int vtkCanvas::GetSubCanvasIndexAt( int x, int y )
{
	assert( this->SubCanvasses != 0 );

	for( int i = 0; i < this->SubCanvasses->GetNumberOfItems(); ++i )
	{
		vtkSubCanvas * canvas = this->SubCanvasses->GetItem( i );
		assert( canvas != 0 );

		if( canvas->InsideViewport( x, y ) )
		{
			return i;
		}
	}

	return 0;
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::SetRenderWindow( vtkRenderWindow * window )
{
	if( this->Window == window )
		return;

	if( this->Window != 0 )
		this->Window->UnRegister( this );
	this->Window = window;
	if( this->Window != 0 )
	{
		this->Window->Register( this );
		this->Window->AddRenderer( this->BackgroundRenderer );
	}

	assert( this->SubCanvasses != 0 );
	for( int i = 0; i < this->SubCanvasses->GetNumberOfItems(); ++i )
	{
		vtkSubCanvas * canvas = this->SubCanvasses->GetItem( i );
		assert( canvas );

		canvas->SetRenderWindow( this->Window );
	}
}

/////////////////////////////////////////////////////////////////
vtkRenderWindow * vtkCanvas::GetRenderWindow()
{
	return this->Window;
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::SetInteractor( vtkRenderWindowInteractor * interactor )
{
	if( this->Interactor == interactor )
		return;

	if( this->Interactor != 0 )
		this->Interactor->UnRegister( this );
	this->Interactor = interactor;
	if( this->Interactor != 0 )
		this->Interactor->Register( this );

	assert( this->Window != 0 );
	this->Window->SetInteractor( this->Interactor );

	assert( this->SubCanvasses != 0 );
	for( int i = 0; i < this->SubCanvasses->GetNumberOfItems(); ++i )
	{
		vtkSubCanvas * canvas = this->SubCanvasses->GetItem( i );
		assert( canvas != 0 );

		canvas->SetInteractor( this->Interactor );
	}

	if( this->Style )
	{
		if( this->Interactor )
			this->Interactor->SetInteractorStyle( this->Style );
		this->Style->SetCanvas( this );
	}
}

/////////////////////////////////////////////////////////////////
vtkRenderWindowInteractor * vtkCanvas::GetInteractor()
{
	return this->Interactor;
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::SetInteractorStyle( vtkCanvasInteractorStyle * style )
{
	if( this->Style == style )
		return;

	if( this->Style )
		this->Style->UnRegister( this );
	this->Style = style;
	if( this->Style )
	{
		this->Style->Register( this );
		this->Style->SetCanvas( this );
	}

	if( this->Interactor )
		this->Interactor->SetInteractorStyle( this->Style );
}

/////////////////////////////////////////////////////////////////
class vtkCanvasInteractorStyle * vtkCanvas::GetInteractorStyle()
{
	return this->Style;
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::SetSize( int width, int height )
{
}

/////////////////////////////////////////////////////////////////
int * vtkCanvas::GetSize()
{
	return 0;
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::Start()
{
	assert( this->Interactor != 0 );
	this->Interactor->Start();
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::ResetAllCameras()
{
	assert( this->SubCanvasses );
	for( int i = 0; i < this->SubCanvasses->GetNumberOfItems(); ++i )
	{
		vtkSubCanvas * canvas = this->SubCanvasses->GetItem( i );
		assert( canvas );

		canvas->ResetCamera();
	}
}

/////////////////////////////////////////////////////////////////
void vtkCanvas::SetInteractionOnSelectEnabled( bool enabled )
{
	if( this->InteractionOnSelectEnabled == enabled )
		return;

	this->InteractionOnSelectEnabled = enabled;
	if( this->SelectedCanvas != 0 )
	{
		assert( this->SubCanvasses != 0 );
		for( int i = 0; i < this->SubCanvasses->GetNumberOfItems(); ++i )
		{
			vtkSubCanvas * canvas = this->SubCanvasses->GetItem( i );
			assert( canvas != 0 );

			canvas->SetInteractionEnabled( false );
		}

		this->SelectedCanvas->SetInteractionEnabled( this->InteractionOnSelectEnabled );
	}
}

/////////////////////////////////////////////////////////////////
bool vtkCanvas::IsInteractionOnSelectEnabled()
{
	return this->InteractionOnSelectEnabled;
}