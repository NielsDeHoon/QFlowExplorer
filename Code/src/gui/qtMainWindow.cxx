#include "qtMainWindow.h"

//----------------------------------------------------------------------------
qtMainWindow::qtMainWindow() 
{ 
  this->timeCounter                   = 0;
  this->animationEnabled              = false;    
  
  this->kernel                        = qfeKernel::qfeGetInstance();
  this->propertyBrowser               = new QtTreePropertyBrowser();
  this->propertyBrowserVariantManager = new QtVariantPropertyManager();
  this->propertyBrowserVariantFactory = new QtVariantEditorFactory();
  this->modelBrowser                  = new QtTreePropertyBrowser();
  this->modelBrowserVariantManager    = new QtVariantPropertyManager();
  this->modelBrowserVariantFactory    = new QtVariantEditorFactory();  
  this->probeBrowser                  = new QtTreePropertyBrowser();
  this->probeBrowserVariantManager    = new QtVariantPropertyManager();
  this->probeBrowserVariantFactory    = new QtVariantEditorFactory();
  this->volumeBrowser                 = new QtTreePropertyBrowser();  
  this->volumeBrowserVariantManager   = new QtVariantPropertyManager();
  this->volumeBrowserVariantFactory   = new QtVariantEditorFactory();    

  this->propertyBrowserInitState      = false;
  this->modelBrowserInitState         = false;
  this->probeBrowserInitState         = false;
  this->volumeBrowserInitState        = false;

  this->progressBar                   = new QProgressBar;
  this->transferFunction              = new qtTransferFunctionWidget();
  this->featureTF1                     = new qtBaseTransferFunctionCanvas();
  this->featureTF2                     = new qtBaseTransferFunctionCanvas();
  this->ballLight                     = new qtBallWidget();
  this->fluxPlot                      = new QCustomPlot();
  this->timer                         = new QTimer(this);

  this->vtkevents                     = vtkEventQtSlotConnect::New();

  this->UI                            = new Ui_MainWindow();  
  this->qfpcb                         = new qtProgressBarCallBack();  
   
  this->currentProbe2D                = -1;    
  this->currentProbe3D                = -1;

  this->UI->setupUi(this);  

  this->UI->vtkWidget->installEventFilter(this);
  this->UI->vtkWidget->SetUseTDx(true);

  this->UI->horizontalSliderTime->installEventFilter(this);  
  
  this->state                         = startup;

  this->UI->checkBoxScreenshotRotate->setChecked(false);
}

//----------------------------------------------------------------------------
qtMainWindow::~qtMainWindow()
{
  this->kernel->qfeDeleteInstance();

  this->vtkevents->Delete();

  delete this->progressBar;
  delete this->transferFunction;
  delete this->featureTF1;
  delete this->featureTF2;
  delete this->ballLight;
  delete this->fluxPlot;
  delete this->propertyBrowserVariantFactory;
  delete this->propertyBrowserVariantManager;  
  delete this->modelBrowserVariantFactory;
  delete this->modelBrowserVariantManager;
  delete this->probeBrowserVariantFactory;
  delete this->probeBrowserVariantManager;
  delete this->volumeBrowserVariantFactory;
  delete this->volumeBrowserVariantManager;
    
  delete this->propertyBrowser;
  delete this->modelBrowser;
  delete this->probeBrowser;
  delete this->volumeBrowser;    
   
  for(int i=0; i < (int)this->histograms.size(); i++)
    delete histograms[i];
  histograms.clear();
  
  delete this->qfpcb;
  
  delete this->UI;
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitialize()
{
  switch(this->state)
  {
    case startup    : this->guiInitializePreData();
                      this->state = initpre;
                      break;
    case initpre    : break;
    case initdata   : this->guiInitializePostData();
                      this->state = initpost;
                      break;                
    case initpost   : // Render start?!
                      this->state = running;
                      break;
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUninitialize()
{
  switch(this->state)
  {
  case running    : // Render stop?!
                    this->state = initpre;
                    break;
  case initpost   : this->guiUninitializePostData();
                    this->state = initdata;
                    break;
  case initdata   : break;                   
  case initpre    : this->guiUninitializePreData();
                    this->state = startup;
                    break;
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializePreData()
{
  // Initialize UI (created with Qt Designer).
  this->setObjectName("qtMainWindow");
  this->setWindowTitle("QFlow Explorer");

  this->guiInitializeComponents();

  // Determine Initial Layout
  this->UI->action_Toolbar->setChecked(true);
  this->UI->action_Control_Panel->setChecked(true);
  this->UI->action_Slice_View->setChecked(true);
  this->UI->action_Transfer_Function->setChecked(false);

  // Connect the progress bar      
  this->qfpcb->SetProgressBar(this->progressBar);
  this->qfpcb->SetStatusBar(this->statusBar());

  this->kernel->qfeConnectProgressBar(this->qfpcb);  

  // Initialize temporal supersampling
  //this->VPR = 5;
  
  // Set up stereo rendering
  //this->widget->GetRenderWindow()->StereoCapableWindowOn();
  //this->widget->GetRenderWindow()->SetStereoTypeToRedBlue();
  //this->widget->GetRenderWindow()->SetStereoTypeToAnaglyph();
  //this->widget->GetRenderWindow()->StereoRenderOn();

   // Show GUI (creates GL context)
  this->show();
  
  // Connect the GUI to the kernel after the GL context is created      
  this->kernel->SceneManager->qfeSetSceneManagerRenderWindow(this->UI->vtkWidget->GetRenderWindow()); 
  //this->kernel->SceneManager->qfeSetSceneManagerMedicalCanvas(this->canvas);
  //this->kernel->SceneManager->qfeSetSceneManagerRenderWindow(this->viewWidget->canvas()->GetSubCanvas3D()->GetRenderWindow());   
  
  // Start the default rendering
  this->kernel->qfeKernelRenderScene(sceneDefault);  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializePostData()
{
  qfePatient *patient = NULL;
  qfeScene   *scene   = NULL;
  bool        paramState;

  paramState = this->UI->radioButtonParamBasic->isChecked();

  this->kernel->DataManager->qfeGetPatient(&patient);  
  this->kernel->SceneManager->qfeGetSceneManagerScene(&scene);

  // Clear
  this->guiUninitializeActions(this->UI->verticalLayoutTools);

  // Create action buttons
  this->guiInitializeActions(scene, this->UI->verticalLayoutTools);
  
  // Clear previous gui browser values
  this->guiUninitializePropertyBrowser();
  this->guiUninitializeModelBrowser();
  this->guiUninitializeProbeBrowser();
  this->guiUninitializeVolumeBrowser();

  // Create and fill gui browser values
  this->guiInitializePropertyBrowser(scene, this->propertyBrowser, this->propertyBrowserVariantManager, this->propertyBrowserVariantFactory, paramState);  
  this->guiInitializeModelBrowser(patient, this->modelBrowser, this->modelBrowserVariantManager, this->modelBrowserVariantFactory);
  this->guiInitializeVolumeBrowser(patient, this->volumeBrowser, this->volumeBrowserVariantManager, this->volumeBrowserVariantFactory);  
  this->guiInitializeProbeTree(patient, this->UI->treeWidgetProbes);  

  // Compute histograms
  this->guiPrecomputeHistograms(patient->study->pcap);

  patient->study->qfeClearStoredData();

  this->guiInitializeLight(scene);
  this->guiUpdateUsername(patient);
  this->guiUpdateBackendFeatures();
  this->guiUpdateBackendColor();  
  this->guiUpdateBackendLightSource();
  this->guiUpdateFrontendColor();
  this->guiUpdateMenu();  
  
  // Update Animation slider
  if(patient->study->phases <= 1) {
    this->UI->toolButtonPlay->setEnabled(false);
    this->UI->horizontalSliderTime->setEnabled(false);
  }
  else {
    this->UI->toolButtonPlay->setEnabled(true);
    this->UI->horizontalSliderTime->setEnabled(true);
    this->UI->horizontalSliderTime->setMinimum(0);

    this->guiAnimationUpdateSlider(patient->study->pcap.size(), this->animationFactor);
  }

  this->phaseDuration = patient->study->phaseDuration;
  if(this->phaseDuration <= 0) this->phaseDuration = 40.0;

  // Update Visualization selection 
  this->UI->comboBoxScenePreset->clear();
  this->UI->comboBoxScenePreset->addItem(QString("Choose scene..."));
  this->UI->comboBoxScenePreset->addItem(QString("Multi-planar reformat"));
  this->UI->comboBoxScenePreset->addItem(QString("Maximum intensity projection"));
  this->UI->comboBoxScenePreset->addItem(QString("Direct volume rendering"));
  this->UI->comboBoxScenePreset->addItem(QString("Illustrative flow rendering"));
  this->UI->comboBoxScenePreset->addItem(QString("Virtual probe rendering"));
  this->UI->comboBoxScenePreset->addItem(QString("Ultrasound rendering"));
  this->UI->comboBoxScenePreset->addItem(QString("Hierarchical clustering rendering"));
  this->UI->comboBoxScenePreset->addItem(QString("Pattern matching rendering"));
  this->UI->comboBoxScenePreset->addItem(QString("Fluid simulation"));
  this->UI->comboBoxScenePreset->addItem(QString("Ink visualization"));
  this->UI->comboBoxScenePreset->addItem(QString("Noise filters"));
  this->UI->comboBoxScenePreset->addItem(QString("Cardiac exploration"));
  this->UI->comboBoxScenePreset->addItem(QString("Data Assimilation"));

  // Connect to VTK event
  this->vtkevents->Connect(this->UI->vtkWidget->GetRenderWindow()->GetInteractor(), vtkCommand::RenderEvent, this, SLOT(guiUpdateCursor()));
  this->vtkevents->Connect(this->UI->vtkWidget->GetRenderWindow(), vtkCommand::UpdateInformationEvent, this, SLOT(guiUpdateFrontendBrowsers()));
  this->vtkevents->Connect(this->UI->vtkWidget->GetRenderWindow(), vtkCommand::UpdatePropertyEvent,   this, SLOT(guiUpdateFrontendProbeQuantification()));
  this->vtkevents->Connect(this->kernel->DataManager, vtkCommand::UpdateInformationEvent, this, SLOT(guiUpdateFrontendVolumeBrowser())); 
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUninitializePostData()
{
  qfePatient *patient;
  qfeScene   *scene;

  this->kernel->DataManager->qfeGetPatient(&patient);  
  this->kernel->SceneManager->qfeGetSceneManagerScene(&scene);

  // Clear
  this->guiUninitializeActions(this->UI->verticalLayoutTools);

  this->guiUninitializePropertyBrowser();  
  this->guiUninitializeModelBrowser();
  this->guiUninitializeProbeBrowser();
  this->guiUninitializeVolumeBrowser();

  for (unsigned i = 0; i < histograms.size(); i++)
    delete histograms[i];
  this->histograms.clear();

  this->guiUpdateUsername(patient);
  this->guiUpdateMenu();

  // Update Visualization selection 
  this->UI->comboBoxScenePreset->clear();
  this->UI->comboBoxScenePreset->addItem(QString("Choose scene..."));  

  // Update Animation slider
  this->UI->toolButtonPlay->setEnabled(false);
  this->UI->horizontalSliderTime->setEnabled(false);
  this->UI->horizontalSliderTime->setMinimum(0);
  this->UI->horizontalSliderTime->setMaximum(1);      
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUninitializePreData()
{
  // Nothing yet
}


//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateRenderWindow()
{  
  //this->viewWidget->canvas()->GetSubCanvas3D()->GetRenderWindow()->Render();
  this->UI->vtkWidget->GetRenderWindow()->Render();
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializeComponents()
{
  // Visualization Combo Box
  this->UI->comboBoxScenePreset->addItem(QString("Choose scene..."));
  
  // Progress Bar
  this->progressBar->setMaximumHeight(16);
  this->progressBar->setMaximumWidth(200);
  this->progressBar->setValue(1);
  this->progressBar->setMinimum(0);
  this->progressBar->setMaximum(100);
  this->progressBar->setTextVisible(true);
  this->progressBar->setVisible(false);
  this->statusBar()->addPermanentWidget(this->progressBar);

  // Time Slider and Animation Speed
  this->UI->horizontalSliderTime->setSliderPosition(0);
  this->UI->horizontalSliderTime->setMinimum(0);
  this->UI->horizontalSliderTime->setMaximum(0);
  this->UI->horizontalSliderTime->setEnabled(false);

  this->UI->comboBoxAnimationSpeed->addItem(QString("1.0x"));
  this->UI->comboBoxAnimationSpeed->addItem(QString("0.75x"));
  this->UI->comboBoxAnimationSpeed->addItem(QString("0.50x"));
  this->UI->comboBoxAnimationSpeed->addItem(QString("0.25x"));
  this->UI->comboBoxAnimationSpeed->addItem(QString("0.10x"));
  this->UI->comboBoxAnimationSpeed->addItem(QString("0.05x"));
  this->UI->comboBoxAnimationSpeed->addItem(QString("0.02x"));
  this->UI->comboBoxAnimationSpeed->addItem(QString("0.01x"));
  this->UI->comboBoxAnimationSpeed->setCurrentIndex(3);

  // Enable the preferred parameter tab
  this->UI->tabWidget->setCurrentIndex(0);

  // Data Buttons
  this->UI->action_Load_Phase->setEnabled(true);
  this->UI->action_Load_Phases->setEnabled(true);
  this->UI->action_Load_Mesh->setEnabled(false);
  this->UI->action_Load_Seeds->setEnabled(false);
  this->UI->action_Load_Cluster_Hierarchy->setEnabled(false);
  this->UI->action_Save_Cluster_Hierarchy->setEnabled(false);
  this->UI->action_Load_Fluid_Simulation->setEnabled(false);
  this->UI->action_Save_Fluid_Simulation->setEnabled(false);
  this->UI->action_Release->setEnabled(false);  
  this->UI->action_Plot->setEnabled(false);

  //plot button
  this->UI->action_Plot->setVisible(true);

  //screenshot button
  this->UI->action_Screenshot->setVisible(true);
  this->UI->action_Screenshot->setEnabled(true);

  // Animation Buttons
  this->UI->toolButtonPlay->setEnabled(false);

  // Splitter  
  QList<int> l;
  int size0 = (int)(200);
  int size1 = (int)(1);
  l.append(size0);
  l.append(size1);
  this->UI->splitter->setSizes(l);
  this->UI->splitter->refresh();

  // Add the Transfer Function Widget
  this->UI->gridLayoutTransferFunction->addWidget(this->transferFunction, 0, 0, 1, 1);
  this->transferFunction->setVisible(true);
  this->UI->featuresGrid1->addWidget(this->featureTF1);
  this->featureTF1->setVisible(true);
  this->UI->comboBoxFeatures1->addItem(QString("None"));
  this->UI->comboBoxFeatures1->addItem(QString("Flow speed"));
  this->UI->comboBoxFeatures1->addItem(QString("Magnitude (PCA-M)"));
  this->UI->comboBoxFeatures1->addItem(QString("Curl"));
  //this->UI->comboBoxFeatures1->addItem(QString("FTLE"));
  this->UI->comboBoxFeatures1->addItem(QString("Lambda2"));
  this->UI->comboBoxFeatures1->addItem(QString("Divergence"));
  this->UI->comboBoxFeatures1->addItem(QString("Q-Criterion"));
  this->UI->featuresGrid2->addWidget(this->featureTF2);
  this->featureTF2->setVisible(true);
  this->UI->comboBoxFeatures2->addItem(QString("None"));
  this->UI->comboBoxFeatures2->addItem(QString("Flow speed"));
  this->UI->comboBoxFeatures2->addItem(QString("Magnitude (PCA-M)"));
  this->UI->comboBoxFeatures2->addItem(QString("Curl"));
  //this->UI->comboBoxFeatures2->addItem(QString("FTLE"));
  this->UI->comboBoxFeatures2->addItem(QString("Lambda2"));
  this->UI->comboBoxFeatures2->addItem(QString("Divergence"));
  this->UI->comboBoxFeatures2->addItem(QString("Q-Criterion"));

   // Add the Ball widget
  //this->UI->horizontalLayoutBallWidget->addWidget(this->ballLight, 0, 0);
  //this->ballLight->setDirectionVector(QVector3D(-0.4,-0.6,0.7));

  // Create the flux graph
  QVector<double> x(101), y(101); // initialize with entries 0..100
  for (int i=0; i<101; ++i)
  {
    x[i] = i/50.0 - 1; // x goes from -1 to 1
    y[i] = x[i]*x[i]; // let's plot a quadratic function
  }
  // create graph and assign data to it:
  this->fluxPlot->addGraph();
  this->fluxPlot->graph(0)->setData(x, y);
  // give the axes some labels:
  this->fluxPlot->xAxis->setLabel("x");
  this->fluxPlot->yAxis->setLabel("y");
  // set axes ranges, so we see all data:
  this->fluxPlot->xAxis->setRange(-1, 1);
  this->fluxPlot->yAxis->setRange(0, 1);
  this->fluxPlot->replot();

  // Set the list of quantification parameters
  this->UI->comboBoxProbeQuantification->addItem("Flow rate (cm2/s)");
  this->UI->comboBoxProbeQuantification->addItem("Maximum speed (cm/s)");

  this->UI->doubleSpinBoxProbeQuantification->setMaximum(999.99);

  // Set the property browser (separately build qt solutions library)  
  this->UI->gridLayoutVisuals->addWidget(this->propertyBrowser);
  this->UI->gridLayoutModels->addWidget(this->modelBrowser);
  this->UI->gridLayoutProbeProperties->addWidget(this->probeBrowser);
  this->UI->gridLayoutProbeGraph->addWidget(this->fluxPlot);
  this->UI->gridLayoutVolumes->addWidget(this->volumeBrowser);

  this->UI->radioButtonParamAdvanced->setChecked(true);

  // Set the property tree
  this->guiUninitializeProbeTree();

  // Initialize color palletes
  QPalette palette;
  //palette.setColor(this->UI->lineEditBackgroundColorPrimary->backgroundRole(), QColor(255,255,255));
  //this->UI->lineEditBackgroundColorPrimary->setPalette(palette);
  //palette.setColor(this->UI->lineEditBackgroundColorSecondary->backgroundRole(), QColor(200,220,246));
  //this->UI->lineEditBackgroundColorSecondary->setPalette(palette);
  palette.setColor(this->UI->lineEditBackgroundColorPrimary->backgroundRole(), QColor(255,255,255));
  this->UI->lineEditBackgroundColorPrimary->setPalette(palette);
  palette.setColor(this->UI->lineEditBackgroundColorSecondary->backgroundRole(), QColor(88,110,135));
  this->UI->lineEditBackgroundColorSecondary->setPalette(palette);

  // Connect components to their slots
  connect(this->featureTF1, SIGNAL(transferFunctionChanged()), this, SLOT(guiUpdateBackendFeatures()));
  connect(this->featureTF2, SIGNAL(transferFunctionChanged()), this, SLOT(guiUpdateBackendFeatures()));
  connect(this->transferFunction, SIGNAL(transferFunctionChanged())                     , this, SLOT(guiUpdateBackendColor()));
  connect(this->ballLight       , SIGNAL(ballWidgetChanged())                           , this, SLOT(guiUpdateBackendLightSource()));
  connect(this->propertyBrowserVariantManager , SIGNAL(valueChanged(QtProperty *, const QVariant &))  , this, 
              SLOT(guiUpdateBackendPropertyBrowserValue(QtProperty *, const QVariant &)));
  connect(this->modelBrowserVariantManager    , SIGNAL(valueChanged(QtProperty *, const QVariant &))  , this, 
              SLOT(guiUpdateBackendModelBrowserValue(QtProperty *, const QVariant &)));
  connect(this->probeBrowserVariantManager    , SIGNAL(valueChanged(QtProperty *, const QVariant &))  , this, 
              SLOT(guiUpdateBackendProbeBrowserValue(QtProperty *, const QVariant &)));  
  connect(this->timer           , SIGNAL(timeout())                                     , this, SLOT(guiAnimationTimer()));  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateViewGeometry()
{
  //QAction *action = (QAction *)sender();
  QToolButton *button = (QToolButton *)sender();

  if(button == this->UI->toolButtonAP)  this->kernel->SceneManager->qfeSetSceneManagerViewGeometry(AP);
  if(button == this->UI->toolButtonPA)  this->kernel->SceneManager->qfeSetSceneManagerViewGeometry(PA);
  if(button == this->UI->toolButtonLR)  this->kernel->SceneManager->qfeSetSceneManagerViewGeometry(LR);
  if(button == this->UI->toolButtonRL)  this->kernel->SceneManager->qfeSetSceneManagerViewGeometry(RL);
  if(button == this->UI->toolButtonHF)  this->kernel->SceneManager->qfeSetSceneManagerViewGeometry(HF);
  if(button == this->UI->toolButtonFH)  this->kernel->SceneManager->qfeSetSceneManagerViewGeometry(FH);

  // Needs 2 render calls
  this->guiUpdateRenderWindow();
  this->guiUpdateRenderWindow();
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateUsername(qfePatient *p)
{
  this->UI->dockWidget->setWindowTitle("Patient: Anonymous");
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateMenu()
{
  bool dataLoaded;
  if(this->state >= initdata) dataLoaded = true;
  else                        dataLoaded = false;

  this->UI->action_Load_Mesh->setEnabled(dataLoaded);
  this->UI->action_Release->setEnabled(dataLoaded);  
  this->UI->action_Load_Seeds->setEnabled(dataLoaded);
  this->UI->action_Load_Cluster_Hierarchy->setEnabled(dataLoaded);
  this->UI->action_Save_Cluster_Hierarchy->setEnabled(dataLoaded);
  this->UI->action_Load_Fluid_Simulation->setEnabled(dataLoaded);
  this->UI->action_Save_Fluid_Simulation->setEnabled(dataLoaded);
  this->UI->action_Plot->setEnabled(dataLoaded);

}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontend()
{
  this->guiUpdateFrontendColor();
  this->guiUpdateFrontendLightSource();
  this->guiUpdateFrontendActions();
  this->guiUpdateFrontendDisplayColor();
  this->guiUpdateFrontendBrowsers();
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendBrowsers()
{
  this->guiUpdateFrontendPropertyBrowser();
  this->guiUpdateFrontendModelBrowser();  
  this->guiUpdateFrontendVolumeBrowser();    
  this->guiUpdateFrontendProbeTree();  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializeActions(qfeScene *scene, QBoxLayout *layout)
{
  if(scene == NULL) return;  

  QString     label; 

  vector<qfeVisual *>          visuals;
  int                          visualType;
  int                          visualActionsCount;

  scene->qfeGetSceneVisuals(visuals);  

  for(int i=(int)visuals.size()-1; i>=0; i--)
  {
    visuals[i]->qfeGetVisualType(visualType);
    visuals[i]->qfeGetVisualActionCount(visualActionsCount);

    // if there were action to place, add a dividing line
    if(visualActionsCount > 1)
    {
      QFrame * f = new QFrame(this);
      f->setFrameShape(QFrame::HLine);
      f->setFrameShadow(QFrame::Sunken);

      layout->insertWidget(0, f);        
    }

    // add the action buttons
    for(int j=visualActionsCount-1; j >= 0; j--)
    {
      qfeVisualAction *action;
      string           label;
      QString          icon;
      int              type;

      visuals[i]->qfeGetVisualAction(j, &action);

      action->qfeGetVisualActionLabel(label);
      action->qfeGetVisualActionType(type);

      icon.sprintf(":/ui/images/%s.png", label.c_str());

      if(!this->actionsType.contains(type))
      {
        this->actions.push_back(new QToolButton(this));      
        this->actionsType.push_back(type);
        
        this->actions.back()->setIcon(QIcon::QIcon(icon));
        this->actions.back()->setMinimumHeight(30);
        this->actions.back()->setMinimumWidth(30);
        this->actions.back()->setAccessibleDescription(QString(label.c_str()));
        this->actions.back()->setToolTip(QString(label.c_str()));
        
        layout->insertWidget(0, this->actions.back());         
        connect(this->actions.back(), SIGNAL(clicked()), this, SLOT(guiHandleAction()));     
      }
    }
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUninitializeActions(QBoxLayout *layout)
{ 
  QLayoutItem *child;
  
  while ((child = layout->takeAt(0)) != 0) {   
    if(child->widget() != NULL)
    {
      layout->removeWidget(child->widget());
      delete child->widget();
    }    
    delete child;
  }
  //for(int i = 0; i < this->actions.size(); i++) {delete this->actions[i]; this->actions[i] = NULL;};  

  this->actions.clear();
  this->actionsType.clear();
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializePropertyBrowser(qfeScene *scene, QtTreePropertyBrowser* pb, QtVariantPropertyManager *vm, QtVariantEditorFactory *vf, bool basic)
{  
  if(scene == NULL) return;  

  string      type;
  QString     label; 

  vector<qfeVisual *>          visuals;
  //int                          visualType;
  int                          visualParamsCount;

  this->propertyBrowserInitState = false;

  scene->qfeGetSceneVisuals(visuals);  

  QtProperty **topItem = new QtProperty*[visuals.size()];

  propertyBrowser->setFactoryForManager(propertyBrowserVariantManager, propertyBrowserVariantFactory);  
  propertyBrowser->setPropertiesWithoutValueMarked(true);
  propertyBrowser->setRootIsDecorated(false);

  for(int i=0; i<(int)visuals.size(); i++)
  {
    type = this->guiGetVisualLabel(visuals[i]);
    label.sprintf("Visual %d: %s", i, type.c_str());

    visuals[i]->qfeGetVisualParameterCount(visualParamsCount);       
    
    topItem[i] = vm->addProperty(QtVariantPropertyManager::groupTypeId(), label);

    // Add the properties for each visual
    for(int j=0; j < visualParamsCount; j++)
    {
      qfeVisualParameter     *param;
      string                  paramLabel;
      float                   paramRange[2];
      float                   paramStepSize;
      qfeParamType            paramType;
      qfeVisualParameterLevel paramLevel;
     
      QtVariantProperty  *subItem;
      QtVariantProperty  *subsubItem[6];
      QtVariantProperty  *subsubsubItem[3];  

      visuals[i]->qfeGetVisualParameter(j, &param);
      
      param->qfeGetVisualParameterLabel(&paramLabel);
      param->qfeGetVisualParameterRange(paramRange[0], paramRange[1]);
      param->qfeGetVisualParameterStepSize(paramStepSize);
      param->qfeGetVisualParameterType(paramType);
      param->qfeGetVisualParameterLevel(paramLevel);

      bool             paramBoolValue;
      double           paramDoubleValue;
      int              paramIntValue;
      qfeSelectionList paramStringListValue;
      qfeVector        paramVector;
      qfeColorRGBA     paramColor;
      qfeRingParam     paramRing;
      qfeRange         paramRangeMinMax;
  
      QStringList    list;   
	    QString        labelIn;

      // Check the parameter level for visibility of the parameter
      if(basic && (paramLevel == qfeVisualParameterAdvanced)) continue;

      switch(paramType)
      {
      case qfeParamTypeBool :
        param->qfeGetVisualParameterValue(paramBoolValue);

        subItem = vm->addProperty(QVariant::Bool, QString(paramLabel.c_str()));
        subItem->setValue(paramBoolValue);

        propertyBrowserPropertyToId[subItem] = QLatin1String((paramLabel).c_str());
        topItem[i]->addSubProperty(subItem);
        break;
      case qfeParamTypeDouble :
        param->qfeGetVisualParameterValue(paramDoubleValue);

        subItem = vm->addProperty(QVariant::Double, QString(paramLabel.c_str()));
        subItem->setAttribute(QLatin1String("minimum"), paramRange[0]);
        subItem->setAttribute(QLatin1String("maximum"), paramRange[1]);
        subItem->setAttribute(QLatin1String("singleStep"), paramStepSize);
        subItem->setValue(paramDoubleValue);

        propertyBrowserPropertyToId[subItem] = QLatin1String((paramLabel).c_str());
        topItem[i]->addSubProperty(subItem);
        break;
      case qfeParamTypeInt :
        param->qfeGetVisualParameterValue(paramIntValue);

        subItem = vm->addProperty(QVariant::Int, QString(paramLabel.c_str()));
        subItem->setAttribute(QLatin1String("minimum"), paramRange[0]);
        subItem->setAttribute(QLatin1String("maximum"), paramRange[1]);
        subItem->setAttribute(QLatin1String("singleStep"), paramStepSize);
        subItem->setValue(paramIntValue);

        propertyBrowserPropertyToId[subItem] = QLatin1String((paramLabel).c_str());
        topItem[i]->addSubProperty(subItem);
        break;
      case qfeParamTypeStringList :        
        param->qfeGetVisualParameterValue(paramStringListValue);	

        subItem = this->propertyBrowserVariantManager->addProperty(QtVariantPropertyManager::enumTypeId(), QString(paramLabel.c_str()));
        
        for(int k=0; k<(int)paramStringListValue.list.size(); k++) list << paramStringListValue.list.at(k).c_str();
        
        subItem->setAttribute(QLatin1String("enumNames"), list);
        subItem->setValue(paramStringListValue.active);

        propertyBrowserPropertyToId[subItem] = QLatin1String((paramLabel).c_str());
        topItem[i]->addSubProperty(subItem);
        break;
      case qfeParamTypeVector :        
        param->qfeGetVisualParameterValue(paramVector);

        subItem = vm->addProperty(QtVariantPropertyManager::flagTypeId(), QString(paramLabel.c_str()));
        subItem->setAttribute(QLatin1String("decimals"), 3);

        subsubItem[0] = vm->addProperty(QVariant::Double, QString("X"));
        subsubItem[0]->setValue(paramVector.x);
        subsubItem[1] = vm->addProperty(QVariant::Double, QString("Y"));
        subsubItem[1]->setValue(paramVector.y);
        subsubItem[2] = vm->addProperty(QVariant::Double, QString("Z"));
        subsubItem[2]->setValue(paramVector.z);

        subItem->addSubProperty(subsubItem[0]);
        subItem->addSubProperty(subsubItem[1]);
        subItem->addSubProperty(subsubItem[2]);

        // Assign same property for all sibblings
        // This is just necessary to detect which visual parameter is active
        propertyBrowserPropertyToId[subsubItem[0]] = QLatin1String(paramLabel.c_str());
        propertyBrowserPropertyToId[subsubItem[1]] = QLatin1String(paramLabel.c_str());
        propertyBrowserPropertyToId[subsubItem[2]] = QLatin1String(paramLabel.c_str());
        propertyBrowserPropertyToId[subItem]       = QLatin1String(paramLabel.c_str());
        topItem[i]->addSubProperty(subItem);
        break;      

      case qfeParamTypeColorRGB :   
        param->qfeGetVisualParameterValue(paramColor);

        subItem = vm->addProperty(QVariant::Color, QString(paramLabel.c_str()));     
        subItem->setValue(QColor((int)(paramColor.r*255.0), (int)(paramColor.g*255.0), (int)(paramColor.b*255.0), (int)(paramColor.a*255.0)));

        propertyBrowserPropertyToId[subItem] = QLatin1String((paramLabel).c_str());
        topItem[i]->addSubProperty(subItem);
        break; 

      case qfeParamTypeRing :
        param->qfeGetVisualParameterValue(paramRing);

        subItem = vm->addProperty(QVariant::String, QString(paramLabel.c_str()));

        subsubItem[0] = vm->addProperty(QVariant::Bool, QString("visible"));
        subsubItem[0]->setValue(paramRing.visible);

        subsubItem[1] = vm->addProperty(QtVariantPropertyManager::flagTypeId(), QString("seed point"));

        subsubItem[1]->setAttribute(QLatin1String("decimals"), 3);

        subsubsubItem[0] = vm->addProperty(QVariant::Double, QString("X"));
        subsubsubItem[0]->setValue(paramRing.seedPoint.x);
        subsubsubItem[1] = vm->addProperty(QVariant::Double, QString("Y"));
        subsubsubItem[1]->setValue(paramRing.seedPoint.y);
        subsubsubItem[2] = vm->addProperty(QVariant::Double, QString("Z"));
        subsubsubItem[2]->setValue(paramRing.seedPoint.z);

        subsubItem[2] = vm->addProperty(QVariant::Color, QString("color"));
        subsubItem[2]->setValue(QColor((int)(paramRing.color.r*255.0), (int)(paramRing.color.g*255.0), (int)(paramRing.color.b*255.0), (int)(paramRing.color.a*255.0)));

        for(int k=0; k<3; k++) subsubItem[1]->addSubProperty(subsubsubItem[k]);
        for(int k=0; k<3; k++) subItem->addSubProperty(subsubItem[k]);

        for(int k=0; k<3; k++) propertyBrowserPropertyToId[subsubsubItem[k]] = QLatin1String(paramLabel.c_str());
        for(int k=0; k<3; k++) propertyBrowserPropertyToId[subsubItem[k]]    = QLatin1String(paramLabel.c_str());
        propertyBrowserPropertyToId[subItem]       = QLatin1String(paramLabel.c_str());
        topItem[i]->addSubProperty(subItem);
        break;    
      case qfeParamTypeRange :        
        param->qfeGetVisualParameterValue(paramRangeMinMax);

        subItem = vm->addProperty(QtVariantPropertyManager::flagTypeId(), QString(paramLabel.c_str()));
        subItem->setAttribute(QLatin1String("decimals"), 3);

        subsubItem[0] = vm->addProperty(QVariant::Double, QString("min"));
        subsubItem[0]->setAttribute(QLatin1String("minimum"), paramRange[0]);
        subsubItem[0]->setAttribute(QLatin1String("maximum"), paramRange[1]);
        subsubItem[0]->setAttribute(QLatin1String("singleStep"), paramStepSize);        
        subsubItem[0]->setValue(paramRangeMinMax.min);
        subsubItem[1] = vm->addProperty(QVariant::Double, QString("max"));
        subsubItem[1]->setAttribute(QLatin1String("minimum"), paramRange[0]);
        subsubItem[1]->setAttribute(QLatin1String("maximum"), paramRange[1]);
        subsubItem[1]->setAttribute(QLatin1String("singleStep"), paramStepSize);        
        subsubItem[1]->setValue(paramRangeMinMax.max);
        
        subItem->addSubProperty(subsubItem[0]);
        subItem->addSubProperty(subsubItem[1]);        

        // Assign same property for all sibblings
        // This is just necessary to detect which visual parameter is active
        propertyBrowserPropertyToId[subsubItem[0]] = QLatin1String(paramLabel.c_str());
        propertyBrowserPropertyToId[subsubItem[1]] = QLatin1String(paramLabel.c_str());        
        propertyBrowserPropertyToId[subItem]       = QLatin1String(paramLabel.c_str());
        topItem[i]->addSubProperty(subItem);

        break;
      }      
    }
    
    pb->addProperty(topItem[i]);
  }  
  
  // Collapse all sub items
  QList<QtBrowserItem *> topList = pb->topLevelItems();
  QListIterator<QtBrowserItem *> topIt(topList);

  while (topIt.hasNext()) {
    QtBrowserItem *item = topIt.next();
  
    QList<QtBrowserItem *> subList = item->children();   
    QListIterator<QtBrowserItem *> subIt(subList);
    
    while (subIt.hasNext()) {
      QtBrowserItem *item2 = subIt.next();  
      pb->setExpanded(item2, false);

      QList<QtBrowserItem *> subsubList = item2->children();   
      QListIterator<QtBrowserItem *> subsubIt(subsubList);

      while (subsubIt.hasNext()) {
        QtBrowserItem *item3 = subsubIt.next();  
        pb->setExpanded(item3, false);
      }
    }
  }  

  this->propertyBrowserInitState = true;
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendActions()
{
  qfeScene *scene;
  this->kernel->SceneManager->qfeGetSceneManagerScene(&scene);

  if(scene == NULL) return;

  this->guiUninitializeActions(this->UI->verticalLayoutTools);

  this->guiInitializeActions(scene, this->UI->verticalLayoutTools);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendPropertyBrowser()
{
  qfeScene *scene;
  bool      paramState;
  this->kernel->SceneManager->qfeGetSceneManagerScene(&scene);

  if(scene == NULL) return;

  paramState = this->UI->radioButtonParamBasic->isChecked();

  this->guiUninitializePropertyBrowser();

  this->guiInitializePropertyBrowser(scene, this->propertyBrowser, this->propertyBrowserVariantManager, this->propertyBrowserVariantFactory, paramState);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendModelBrowser()
{
  qfePatient *patient;
  this->kernel->DataManager->qfeGetPatient(&patient);

  if(patient == NULL) return;

  this->guiUninitializeModelBrowser();

  this->guiInitializeModelBrowser(patient, this->modelBrowser, this->modelBrowserVariantManager, this->modelBrowserVariantFactory);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendProbeTree()
{
  qfePatient *patient;
  this->kernel->DataManager->qfeGetPatient(&patient);

  if(patient == NULL) return;

  this->guiUninitializeProbeTree();
  this->guiInitializeProbeTree(patient, this->UI->treeWidgetProbes);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendProbeBrowser()
{
  qfeScene     *scene;
  qfePatient   *patient;  

  this->kernel->SceneManager->qfeGetSceneManagerScene(&scene);
  this->kernel->DataManager->qfeGetPatient(&patient);

  // Update the probe browser
  if((this->currentProbe3D != -1) && (this->probeBrowser != NULL))
  {
    this->UI->treeWidgetProbes->topLevelItem(2)->setExpanded(true);
    for(int i=0; i<this->UI->treeWidgetProbes->topLevelItem(2)->childCount(); i++)
    {
      if(i == this->currentProbe3D)
        this->UI->treeWidgetProbes->topLevelItem(2)->child(i)->setSelected(true);
      else
        this->UI->treeWidgetProbes->topLevelItem(2)->child(i)->setSelected(false);
    }
    this->guiUninitializeProbeBrowser();
    this->guiInitializeProbeBrowser(&patient->study->probe3D[this->currentProbe3D], this->probeBrowser, this->probeBrowserVariantManager, this->probeBrowserVariantFactory);
  }
  else
  if((this->currentProbe2D != -1) && (this->probeBrowser != NULL))
  {
    this->UI->treeWidgetProbes->topLevelItem(1)->setExpanded(true);
    for(int i=0; i<this->UI->treeWidgetProbes->topLevelItem(1)->childCount(); i++)
    {
      if(i == this->currentProbe2D)
        this->UI->treeWidgetProbes->topLevelItem(1)->child(i)->setSelected(true);
      else
        this->UI->treeWidgetProbes->topLevelItem(1)->child(i)->setSelected(false);
    }
    this->guiUninitializeProbeBrowser();
    this->guiInitializeProbeBrowser(&patient->study->probe2D[this->currentProbe2D], this->probeBrowser, this->probeBrowserVariantManager, this->probeBrowserVariantFactory);
  }


}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendProbeBrowser(QTreeWidgetItem *item, int column)
{
  qfePatient *patient;
  int         index; 
  
  this->kernel->DataManager->qfeGetPatient(&patient);
  
  if(patient == NULL) return;

  index = item->data(0, Qt::UserRole).toInt();

  this->guiUninitializeProbeBrowser();

  this->currentProbe3D = -1;
  this->currentProbe2D = -1;
  if((item->childCount() == 0) && (item->parent() != NULL))
  {
    if(item->parent()->text(0) == "Probe 3D")
    {
        this->currentProbe3D = index;
        this->guiInitializeProbeBrowser(&patient->study->probe3D[index], this->probeBrowser, this->probeBrowserVariantManager, this->probeBrowserVariantFactory);

        for(int i=0; i<(int)patient->study->probe3D.size(); i++)
        {
          if(i == this->currentProbe3D)          
            patient->study->probe3D[i].selected = true;
          else
            patient->study->probe3D[i].selected = false;
        }
        this->guiUpdateRenderWindow();
    }
    if(item->parent()->text(0) == "Probe 2D" && this->currentProbe3D == -1)
    {
        this->currentProbe2D = index;
        this->guiInitializeProbeBrowser(&patient->study->probe2D[index], this->probeBrowser, this->probeBrowserVariantManager, this->probeBrowserVariantFactory);

        for(int i=0; i<(int)patient->study->probe2D.size(); i++)
        {
          if(i == this->currentProbe2D)          
            patient->study->probe2D[i].selected = true;
          else
            patient->study->probe2D[i].selected = false;
        }
        this->guiUpdateRenderWindow();
    }
  }  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendProbeQuantification()
{
  if(this->fluxPlot == NULL) return;

  qfePatient *patient;
  
  this->kernel->DataManager->qfeGetPatient(&patient);
  
  if(patient == NULL) return;

  for(int i=0; i<(int)patient->study->probe2D.size(); i++)
  {
    if(patient->study->probe2D[i].selected == true)
    {
      this->currentProbe2D = i;
      this->guiUpdateFrontendProbeBrowser();

      this->guiInitializeProbeQuantification(&patient->study->probe2D[i], this->UI->comboBoxProbeQuantification->currentIndex());
    }
  }
  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendProbeQuantification(QTreeWidgetItem *item, int column)
{
  if(this->fluxPlot == NULL) return;

    qfePatient *patient;
  int         index; 
  
  this->kernel->DataManager->qfeGetPatient(&patient);
  
  if(patient == NULL) return;

  index = item->data(0, Qt::UserRole).toInt();

  //this->currentProbe3D = -1;
  //this->currentProbe2D = -1;
  if((item->childCount() == 0) && (item->parent() != NULL))
  {
    //if(item->parent()->text(0) == "Probe 3D")
    //{
    //    this->currentProbe3D = index;
    //    this->guiInitializeProbeBrowser(&patient->study->probe3D[index], this->probeBrowser, this->probeBrowserVariantManager, this->probeBrowserVariantFactory);
    //}
    if(item->parent()->text(0) == "Probe 2D" && this->currentProbe3D == -1)
    {
        this->currentProbe2D = index;
        this->guiInitializeProbeQuantification(&patient->study->probe2D[index], 0);
    }
  }  

}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendVolumeBrowser()
{
  qfePatient *patient;
  this->kernel->DataManager->qfeGetPatient(&patient);

  if(patient == NULL) return;

  this->guiUninitializeVolumeBrowser();

  this->guiInitializeVolumeBrowser(patient, this->volumeBrowser, this->volumeBrowserVariantManager, this->volumeBrowserVariantFactory);  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendDisplayColor()
{
  qfeScene   *scene   = NULL;
  qfeDisplay *display = NULL;
  qfeFloat    r, g, b;
  QColor      c1, c2;

  this->kernel->SceneManager->qfeGetSceneManagerScene(&scene);
  if(scene == NULL) return;

  scene->qfeGetSceneDisplay(&display);
  if(display == NULL) return;

  display->qfeGetDisplayBackgroundColorRGB(r,g,b);
  c1.setRgb(r*255,g*255,b*255);

  display->qfeGetDisplayBackgroundColorRGBSecondary(r,g,b);
  c2.setRgb(r*255,g*255,b*255);

  QPalette palette;
  palette.setColor(this->UI->lineEditBackgroundColorPrimary->backgroundRole(),  c1);
  this->UI->lineEditBackgroundColorPrimary->setPalette(palette);

  palette.setColor(this->UI->lineEditBackgroundColorSecondary->backgroundRole(), c2);
  this->UI->lineEditBackgroundColorSecondary->setPalette(palette);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiLoadProbe()
{
  QStringList names;
  QStringList filters;
  filters << "QFlow probe (*.prb)" <<  "All Files (*.*)";

  QFileDialog dialog(this, tr("Load probe"), tr("c:"));
  dialog.setFilters(filters);
  dialog.setFileMode(QFileDialog::ExistingFile);

  if(dialog.exec())
  {
    names = dialog.selectedFiles();
    dialog.close();
  }
  
  if(names.isEmpty()) return;

  if(dialog.close()) {    
    this->guiUpdateRenderWindow();   
    this->kernel->DataManager->qfeLoadProbe(names.front().toStdString());
  }    
}

//----------------------------------------------------------------------------
void qtMainWindow::guiSaveProbe()
{
  qfePatient *patient;
  this->kernel->DataManager->qfeGetPatient(&patient);

  if(((int)patient->study->probe3D.size() > 0))
  {
    QString filename;

    QFileDialog dialog(this, tr("Save probe"), tr("c:"));  
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setNameFilter("QFlow probe (*.prb)");  
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setDefaultSuffix("prb");  

    if(dialog.exec() == QDialog::Rejected)
    {
      // user cancelled
      return; 
    }
    filename = dialog.selectedFiles().front();

    if(filename.isEmpty()) return;

    if(dialog.close()) {        
      this->guiUpdateRenderWindow();   
      if((this->currentProbe3D >= 0) && (this->currentProbe3D < (int)patient->study->probe3D.size()))         
        this->kernel->DataManager->qfeSaveProbe(&patient->study->probe3D[this->currentProbe3D], filename.toStdString());     
      else
        this->kernel->DataManager->qfeSaveProbe(&patient->study->probe3D.front(), filename.toStdString());     
    }    
  }
}


//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateCursor()
{
  qfeScene     *scene;  
  qfeCursorType type;

  this->kernel->SceneManager->qfeGetSceneManagerScene(&scene);  

  // Update the cursor
  scene->qfeGetSceneCursor(type);

  switch(type)
  {
    case cursorArrow : //this->viewWidget->setCursor(Qt::ArrowCursor);                       
                       this->UI->vtkWidget->setCursor(Qt::ArrowCursor);
                       break;
    case cursorCross : //this->viewWidget->setCursor(Qt::CrossCursor);
                       this->UI->vtkWidget->setCursor(Qt::CrossCursor);
                       break;
    default          : //this->viewWidget->setCursor(Qt::ArrowCursor);              
                       this->UI->vtkWidget->setCursor(Qt::ArrowCursor);              
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiComputeTMIP()
{
  qfePatient *patient;

  this->kernel->DataManager->qfeGetPatient(&patient);

  if(patient == NULL) return;
  this->kernel->DataManager->qfeWriteTemporalMIP(patient->study);

  unsigned int nx;
  unsigned int ny;
  unsigned int nz;
  patient->study->pcap[0]->qfeGetVolumeSize(nx,ny,nz);
  //qfeVector sampleDim((qfeFloat)nx*2,(qfeFloat)ny*2,(qfeFloat)nz*2);
  qfeVector sampleDim;
  sampleDim.qfeSetVectorElements(200,100,1,1);
  //this->kernel->DataManager->qfeWriteFTLE(patient->study,sampleDim, 1000, 1); //NEEDS TO HAVE ITS OWN BUTTON
  //this->kernel->DataManager->qfeWriteSynthFlow(patient->study,sampleDim);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiComputeTMAP()
{
  qfePatient *patient;

  this->kernel->DataManager->qfeGetPatient(&patient);

  if(patient == NULL) return;
  this->kernel->DataManager->qfeWriteTemporalMAP(patient->study);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiComputeTMOP()
{
  qfePatient *patient;
  QTime       time;
  int         milliseconds;   

  this->kernel->DataManager->qfeGetPatient(&patient);

  if(patient == NULL) return;

  time.start();

  this->kernel->DataManager->qfeWriteTemporalMOP(patient->study);

  milliseconds = time.elapsed();

  cout << "tMOP time: " << milliseconds << endl;
}

void qtMainWindow::guiComputeFTLE()
{
  qfePatient *patient;
  kernel->DataManager->qfeGetPatient(&patient);
  qfeStudy *study = patient->study;
  kernel->DataManager->qfeWriteFTLE(study, qfeVector(200.f, 100.f, 1.f), 1000.f, 1.f);
}

void qtMainWindow::guiComputeCurl()
{
  qfePatient *patient = nullptr;
  kernel->DataManager->qfeGetPatient(&patient);
  if (patient == nullptr)
    return;

  qfeStudy *study = patient->study;
  unsigned phases = study->pcap.size();
  qfeGrid *grid;
  study->pcap.front()->qfeGetVolumeGrid(&grid);

  study->curl.clear();
  for (unsigned i = 0; i < phases; i++) {
    bool metricExists;
    Array3f result;
    float rangeMin, rangeMax;

    cout << "Calculating curl (" << i+1 << "/" << phases << ")" << endl;
    study->qfeGetScalarMetric("Curl magnitude 1", nullptr, (float)i,
      metricExists, result, rangeMin, rangeMax);
    study->curl.push_back(new qfeVolume(qfeConvert::qfeArray3fToQfeVolume(result, rangeMin, rangeMax, "qflow curl", grid)));
  }
  kernel->DataManager->qfeWriteVolumeSeries(&study->curl, "curl");
}

void qtMainWindow::guiComputeLambda2()
{
  qfePatient *patient = nullptr;
  kernel->DataManager->qfeGetPatient(&patient);
  if (patient == nullptr)
    return;

  qfeStudy *study = patient->study;
  qfeGrid *grid;
  study->pcap.front()->qfeGetVolumeGrid(&grid);
  unsigned phases = study->pcap.size();

  unsigned w, h, d;
  study->pcap.front()->qfeGetVolumeSize(w, h, d);
  unsigned size = w*h*d;
  float *lambda2s = new float[size];

  study->lambda2.clear();
  const unsigned nrComp = 1; // Number of components per voxel
  for (unsigned i = 0; i < phases; i++) {
    cout << "Calculating lambda2 (" << i+1 << "/" << phases << ")" << endl;
    float rangeMin, rangeMax;
    qfeMath::qfeComputeLambda2s(study->pcap[i], lambda2s, rangeMin, rangeMax, true);
    qfeVolume *volume = new qfeVolume(qfeConvert::qfeFloattoQfeVolume(lambda2s, w, h, d, nrComp, rangeMin, rangeMax, "qflow lambda2", grid));
    study->lambda2.push_back(volume);
  }
  kernel->DataManager->qfeWriteVolumeSeries(&study->lambda2, "lambda2");
}

void qtMainWindow::guiComputeQCriterion()
{
  qfePatient *patient = nullptr;
  kernel->DataManager->qfeGetPatient(&patient);
  if (patient == nullptr)
    return;

  qfeStudy *study = patient->study;
  qfeGrid *grid;
  study->pcap.front()->qfeGetVolumeGrid(&grid);
  unsigned phases = study->pcap.size();

  unsigned w, h, d;
  study->pcap.front()->qfeGetVolumeSize(w, h, d);
  unsigned size = w*h*d;
  float *qcriterion = new float[size];

  study->qcriterion.clear();
  const unsigned nrComp = 1; // Number of components per voxel
  for (unsigned i = 0; i < phases; i++) {
    cout << "Calculating qcriterion (" << i+1 << "/" << phases << ")" << endl;
    float rangeMin, rangeMax;
    qfeMath::qfeComputeQCriterion(study->pcap[i], qcriterion, rangeMin, rangeMax);
    qfeVolume *volume = new qfeVolume(qfeConvert::qfeFloattoQfeVolume(qcriterion, w, h, d, nrComp, rangeMin, rangeMax, "qflow q-criterion", grid));
    study->qcriterion.push_back(volume);
  }
  kernel->DataManager->qfeWriteVolumeSeries(&study->qcriterion, "qcriterion");
}

void qtMainWindow::guiComputeDivergence()
{
  qfePatient *patient = nullptr;
  kernel->DataManager->qfeGetPatient(&patient);
  if (patient == nullptr)
    return;

  qfeStudy *study = patient->study;
  unsigned phases = study->pcap.size();
  qfeGrid *grid;
  study->pcap.front()->qfeGetVolumeGrid(&grid);

  study->divergence.clear();
  for (unsigned i = 0; i < phases; i++) {
    bool metricExists;
    Array3f result;
    float rangeMin, rangeMax;

    cout << "Calculating divergence (" << i+1 << "/" << phases << ")" << endl;
    study->qfeGetScalarMetric("Divergence 1", (float)i, metricExists, result, rangeMin, rangeMax);
    study->divergence.push_back(new qfeVolume(qfeConvert::qfeArray3fToQfeVolume(result, rangeMin, rangeMax, "qflow divergence", grid)));
  }
  kernel->DataManager->qfeWriteVolumeSeries(&study->divergence, "divergence");
}

//----------------------------------------------------------------------------
void qtMainWindow::guiApplyMedianFilter()
{
  qfePatient *patient;

  this->kernel->DataManager->qfeGetPatient(&patient);

  if(patient == NULL) return;
  this->kernel->DataManager->qfeWriteMedianFilter(patient->study);
}

void qtMainWindow::guiExportMetrics()
{
	qfePatient *patient = nullptr;
	kernel->DataManager->qfeGetPatient(&patient);
	if (patient == nullptr)
		return;

	qfeStudy *study = patient->study;

	study->qfeStoreUserSelectedMetrics();
}

void qtMainWindow::guiSetNrOfBins()
{
  unsigned nrBins = this->UI->textBoxNrBins->displayText().toUInt();
  featureTF1->setNrBins(nrBins);
  featureTF2->setNrBins(nrBins);
}

void qtMainWindow::guiAddFeaturePoint0()
{
  float value = UI->textBoxFeatureValue1->displayText().toFloat();
  float prob = UI->textBoxFeatureProb1->displayText().toFloat();
  featureTF1->addTransferFunctionPoint(value, prob);
}

void qtMainWindow::guiAddFeaturePoint1()
{
  float value = UI->textBoxFeatureValue2->displayText().toFloat();
  float prob = UI->textBoxFeatureProb2->displayText().toFloat();
  featureTF2->addTransferFunctionPoint(value, prob);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiSaveClusterHierarchy()
{
  qfePatient *patient;

  this->kernel->DataManager->qfeGetPatient(&patient);

  if(patient == NULL) return;
  this->kernel->DataManager->qfeWriteClusterHierarchy(patient->study);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiSaveFluidSimulation()
{
  qfePatient *patient;

  this->kernel->DataManager->qfeGetPatient(&patient);

  if(patient == NULL) return;
  this->kernel->DataManager->qfeWriteFluidSimulation(patient->study);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiSaveTransferFunction()
{
  QString filename;

  QFileDialog dialog(this, tr("Save transfer function"), tr("c:"));  
  dialog.setFileMode(QFileDialog::AnyFile);
  dialog.setNameFilter("QFlow transfer function (*.tf)");  
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setDefaultSuffix("tf");  

  if(dialog.exec() == QDialog::Rejected)
  {
    // user cancelled
    return; 
  }
  filename = dialog.selectedFiles().front();

  if(filename.isEmpty()) return;

  if(dialog.close()) {    
    this->guiUpdateRenderWindow();   
    this->transferFunction->saveTransferFunction(filename);    
  }    
}

//----------------------------------------------------------------------------
void qtMainWindow::guiLoadTransferFunction()
{
  QStringList names;
  QStringList filters;
  filters << "QFlow transfer function (*.tf)" <<  "All Files (*.*)";

  QFileDialog dialog(this, tr("Open transfer function"), tr("c:"));
  dialog.setFilters(filters);
  dialog.setFileMode(QFileDialog::ExistingFile);

  if(dialog.exec())
  {
    names = dialog.selectedFiles();
    dialog.close();
  }
  
  if(names.isEmpty()) return;

  if(dialog.close()) {    
    this->guiUpdateRenderWindow();   
    this->transferFunction->loadTransferFunction(names.front());    
  }  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiMeasureFrameRate()
{
  QTime  time;
  int    milliseconds;
  double fps; 
  int    frames = 100;

  time.start();

  for(int i=0; i<frames; i++)
  {
    this->UI->vtkWidget->GetRenderWindow()->Render();    
    //this->viewWidget->canvas()->GetSubCanvas3D()->GetRenderWindow()->Render();
  }

  milliseconds = time.elapsed();

  fps = frames / (milliseconds/1000.0);

  this->UI->doubleSpinBoxFrameRate->setValue(fps);  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateRenderWidgetSize(int size)
{
  // Difficulties with resizing the mainwindow
  // Typically, the mainwindows layout resize policy is changed.
  // In that case the window can no longer be resized
  // So here are some magic numbers!
  //resize(QSize(861,598));  
  //resize(QSize(1100,800));    
  resize(QSize(350+size,86+size));    
  this->update();  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiRotateCamera()
{
  double angle;
  bool   update;

  // if animation is running, don't update automatically
  update = !this->animationEnabled;
  angle  = (double)this->UI->spinBoxCameraRotationDegrees->value();

  bool screenshot = this->UI->checkBoxScreenshotRotate->checkState () == Qt::Checked;

  this->kernel->SceneManager->qfeSceneManagerRotate360(angle, update, screenshot);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiRotateCamera(int angle)
{
  this->kernel->SceneManager->qfeSceneManagerRotate(angle, true);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateBackendPropertyBrowserValue(QtProperty *property, const QVariant &value)
{
  qfeScene            *scene;
  vector<qfeVisual *>  visuals;
  int                  visualParamsCount;

  this->kernel->SceneManager->qfeGetSceneManagerScene(&scene);
  scene->qfeGetSceneVisuals(visuals);    

  if(this->propertyBrowserInitState == false) return;
  if(!propertyBrowserPropertyToId.contains(property)) return;

  // For all visuals
  for(int i=0; i<(int)visuals.size(); i++)
  {
    visuals[i]->qfeGetVisualParameterCount(visualParamsCount);

    // For all visual parameters
    for(int j=0; j<visualParamsCount; j++)
    {
      qfeVisualParameter *param;
      string              paramLabel;
      qfeParamType        paramType;

      visuals[i]->qfeGetVisualParameter(j, &param);
      
      param->qfeGetVisualParameterLabel(&paramLabel);
      param->qfeGetVisualParameterType(paramType);

      QString id = propertyBrowserPropertyToId[property];

      if(id == QString(paramLabel.c_str()))
      {
        qfeVector        v;
        qfeColorRGBA     c;
        qfeSelectionList l;
        qfeRingParam     r;
        qfeRange         a;

        QColor           color;

        switch(paramType)
        {
        case qfeParamTypeBool :
          param->qfeSetVisualParameterValue(value.toBool());
          break;
        case qfeParamTypeDouble :
          param->qfeSetVisualParameterValue(value.toDouble());
          break;
        case qfeParamTypeInt :
          param->qfeSetVisualParameterValue(value.toInt());
          break;
        case qfeParamTypeStringList :      
          param->qfeGetVisualParameterValue(l);
          l.active = value.toInt();
          param->qfeSetVisualParameterValue(l);
          break;
        case qfeParamTypeVector :            
          param->qfeGetVisualParameterValue(v);
          if(property->propertyName() == "X") v.x = value.toDouble();
          if(property->propertyName() == "Y") v.y = value.toDouble();
          if(property->propertyName() == "Z") v.z = value.toDouble();
          param->qfeSetVisualParameterValue(v);
          break;      
        case qfeParamTypeColorRGB :         
          color = qVariantValue<QColor>(value);

          int rgb[4];
          color.getRgb(&rgb[0], &rgb[1], &rgb[2], &rgb[3]);
          c.r = rgb[0]/255.0;
          c.g = rgb[1]/255.0;
          c.b = rgb[2]/255.0;
          c.a = rgb[3]/255.0;

          param->qfeSetVisualParameterValue(c);
          break;
        case qfeParamTypeRing :         
          param->qfeGetVisualParameterValue(r);
          if(property->propertyName() == "visible")    r.visible     = value.toBool();
          if(property->propertyName() == "X")          r.seedPoint.x = value.toDouble();
          if(property->propertyName() == "Y")          r.seedPoint.y = value.toDouble();
          if(property->propertyName() == "Z")          r.seedPoint.z = value.toDouble();
          //if(property->propertyName() == "dilation")   r.dilation    = value.toDouble();
          if(property->propertyName() == "color")
          {
            color = qVariantValue<QColor>(value);

            int rgb[4];
            color.getRgb(&rgb[0], &rgb[1], &rgb[2], &rgb[3]);
            r.color.r = rgb[0]/255.0;
            r.color.g = rgb[1]/255.0;
            r.color.b = rgb[2]/255.0;
            r.color.a = rgb[3]/255.0;
          }
          param->qfeSetVisualParameterValue(r);
          break;
        case qfeParamTypeRange :            
          param->qfeGetVisualParameterValue(a);
          if(property->propertyName() == "min") a.min = value.toDouble();
          if(property->propertyName() == "max") a.max = value.toDouble();          
          param->qfeSetVisualParameterValue(a);
          break;      
        }        
      }
    }
  }  

  this->guiUpdateRenderWindow();
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateBackendModelBrowserValue(QtProperty *property, const QVariant &value)
{
  qfePatient *patient;

  this->kernel->DataManager->qfeGetPatient(&patient);

  if(this->modelBrowserInitState == false) return;
  if(!modelBrowserPropertyToId.contains(property)) return;

  // For all models
  for(int i=0; i<(int)patient->study->segmentation.size(); i++)
  {
    qfeModel *model = patient->study->segmentation[i];

    char *paramLabel[3] = {"label", "visible", "color"};    

    QString id = modelBrowserPropertyToId[property];

    if(id == QString(paramLabel[0]))
    {    
      QString s = qVariantValue<QString>(value);
      model->qfeSetModelLabel(s.toStdString());        
    }

    if(id == QString(paramLabel[1]))
    {    
      bool b = qVariantValue<bool>(value);
      model->qfeSetModelVisible(b);        
    }

    if(id == QString(paramLabel[2]))
    {    
      int rgb[3];
      QColor qcol;
      qfeColorRGB qfecol;

      qcol = qVariantValue<QColor>(value);       
      qcol.getRgb(&rgb[0], &rgb[1], &rgb[2]);

      qfecol.r = rgb[0]/255.0;
      qfecol.g = rgb[1]/255.0;
      qfecol.b = rgb[2]/255.0;        

      model->qfeSetModelColor(qfecol);
    } 
  }  

  this->guiUpdateRenderWindow();
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateBackendProbeBrowserValue(QtProperty *property, const QVariant &value)
{
  qfePatient *patient;
  int         probeId2D = 0;
  int         probeId3D = 0;

  if(this->probeBrowserInitState == false)         return;
  if(!probeBrowserPropertyToId.contains(property)) return;

  this->kernel->DataManager->qfeGetPatient(&patient);

  // Update 3D probe
  if(patient->study->probe3D.size() > 0) 
  {
    probeId3D = max(this->currentProbe3D, 0);
    probeId3D = min(probeId3D, (int)patient->study->probe3D.size()-1); 
    
    qfeProbe3D *probe = &patient->study->probe3D[probeId3D];
     
    char *paramLabel[4] = {"length", "radius top", "radius base", "color"};  

    QString id = probeBrowserPropertyToId[property];

    if(id == QString(paramLabel[0]))
    {    
      probe->length= value.toDouble();        
    }

    if(id == QString(paramLabel[1]))
    {    
      probe->topRadius = value.toDouble();       
    }

    if(id == QString(paramLabel[2]))
    {    
      probe->baseRadius = value.toDouble();        
    }

    if(id == QString(paramLabel[3]))
    {    
      int rgb[3];
      QColor qcol;    

      qcol = qVariantValue<QColor>(value);       
      qcol.getRgb(&rgb[0], &rgb[1], &rgb[2]);

      probe->color.r = rgb[0]/255.0;
      probe->color.g = rgb[1]/255.0;
      probe->color.b = rgb[2]/255.0;            
    }  
  }

  // Update 3D probe
  if((patient->study->probe2D.size() > 0) && (this->currentProbe2D != -1)) 
  {
    if(this->currentProbe2D > (int)patient->study->probe2D.size()) 
      this->currentProbe2D = (patient->study->probe2D.size())-1;

    qfeProbe2D *probe = &patient->study->probe2D[this->currentProbe2D];
     
    char *paramLabel[2] = {"color"};  

    QString id = probeBrowserPropertyToId[property];

    if(id == QString(paramLabel[0]))
    {    
      int rgb[3];
      QColor qcol;    

      qcol = qVariantValue<QColor>(value);       
      qcol.getRgb(&rgb[0], &rgb[1], &rgb[2]);

      probe->color.r = rgb[0]/255.0;
      probe->color.g = rgb[1]/255.0;
      probe->color.b = rgb[2]/255.0;            
    }  
  }
 
  this->guiUpdateRenderWindow();
}


void qtMainWindow::guiUpdateBackendFeatures()
{
  QVector<QPair<double, double> > tf;
  int dataType;
  featureTF1->getTransferFunction(tf, dataType);
  kernel->qfeKernelUpdateFeatureTF(0, tf, dataType);

  featureTF2->getTransferFunction(tf, dataType);
  kernel->qfeKernelUpdateFeatureTF(1, tf, dataType);

  qfeVtkEngine::qfeGetInstance()->Refresh();
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUninitializePropertyBrowser()
{
  this->propertyBrowserInitState = false;
  this->propertyBrowser->clear();  
  this->propertyBrowserVariantManager->clear();    
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializeModelBrowser(qfePatient *patient, QtTreePropertyBrowser* pb, QtVariantPropertyManager *vm, QtVariantEditorFactory *vf)
{
  if(patient == NULL) return;  

  qfeColorRGB color;
  string      name;
  QString     label;     

  this->modelBrowserInitState = false;

  vector<qfeModel *> models = patient->study->segmentation;

  QtProperty **topItem = new QtProperty*[models.size()];

  pb->setFactoryForManager(vm, vf);  
  pb->setPropertiesWithoutValueMarked(true);
  pb->setRootIsDecorated(false);
  
  for(int i=0; i<(int)models.size(); i++)
  {
    QtVariantProperty  *subItem[3];

    label.sprintf("Model %d", i+1);
    
    models[i]->qfeGetModelColor(color);
    models[i]->qfeGetModelLabel(name);

    topItem[i] = vm->addProperty(QtVariantPropertyManager::groupTypeId(), label);  
 
    subItem[0] = vm->addProperty(QVariant::String, QString("label"));     
    subItem[0]->setValue(QString(name.c_str()));    

    subItem[1] = vm->addProperty(QVariant::Bool, QString("visible"));     
    subItem[1]->setValue(true); 

    subItem[2] = vm->addProperty(QVariant::Color, QString("color"));     
    subItem[2]->setValue(QColor((int)(color.r*255.0), (int)(color.g*255.0), (int)(color.b*255.0), 255));    

    modelBrowserPropertyToId[subItem[0]] = QLatin1String("label");
    modelBrowserPropertyToId[subItem[1]] = QLatin1String("visible");
    modelBrowserPropertyToId[subItem[2]] = QLatin1String("color");

    topItem[i]->addSubProperty(subItem[0]);
    topItem[i]->addSubProperty(subItem[1]);
    topItem[i]->addSubProperty(subItem[2]);

    pb->addProperty(topItem[i]);
  }

  // Collapse all subitems
  QList<QtBrowserItem *> topList = pb->topLevelItems();
  QListIterator<QtBrowserItem *> topIt(topList);

  while (topIt.hasNext()) {
    QtBrowserItem *item = topIt.next();
  
    QList<QtBrowserItem *> subList = item->children();   
    QListIterator<QtBrowserItem *> subIt(subList);
    
    while (subIt.hasNext()) {
      QtBrowserItem *item2 = subIt.next();  
      pb->setExpanded(item2, false);

      QList<QtBrowserItem *> subsubList = item2->children();   
      QListIterator<QtBrowserItem *> subsubIt(subsubList);

      while (subsubIt.hasNext()) {
        QtBrowserItem *item3 = subsubIt.next();  
        pb->setExpanded(item3, false);
      }
    }
  }

  this->modelBrowserInitState = true;
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializeProbeBrowser(qfeProbe2D *probe, QtTreePropertyBrowser* pb, QtVariantPropertyManager *vm, QtVariantEditorFactory *vf)
{
  if(probe == NULL) return;  

  QString     label;     

  this->probeBrowserInitState = false;

  pb->setFactoryForManager(vm, vf);  
  pb->setPropertiesWithoutValueMarked(true);
  
  QtVariantProperty  *subItem[7];
  QtVariantProperty  *subsubItem[3];
    
  subItem[0] = vm->addProperty(QVariant::Color, QString("color"));     
  subItem[0]->setValue(QColor((int)(probe->color.r*255.0), (int)(probe->color.g*255.0), (int)(probe->color.b*255.0), 255)); 

  subItem[1] = vm->addProperty(QtVariantPropertyManager::flagTypeId(), QString("origin (mm)"));
  subItem[1]->setEnabled(false);
  subItem[1]->setAttribute(QLatin1String("decimals"), 3);

  subsubItem[0] = vm->addProperty(QVariant::Double, QString("X"));
  subsubItem[0]->setValue(probe->origin.x);
  subsubItem[1] = vm->addProperty(QVariant::Double, QString("Y"));
  subsubItem[1]->setValue(probe->origin.y);
  subsubItem[2] = vm->addProperty(QVariant::Double, QString("Z"));
  subsubItem[2]->setValue(probe->origin.z);

  subItem[1]->addSubProperty(subsubItem[0]);
  subItem[1]->addSubProperty(subsubItem[1]);
  subItem[1]->addSubProperty(subsubItem[2]);

  subItem[2] = vm->addProperty(QVariant::Double, QString("radius (mm)"));
  subItem[2]->setEnabled(false);
  subItem[2]->setValue(probe->radius);

  subItem[3] = vm->addProperty(QVariant::Double, QString("area (mm2)"));
  subItem[3]->setEnabled(false);
  subItem[3]->setValue(probe->radius);

  subItem[4] = vm->addProperty(QtVariantPropertyManager::flagTypeId(), QString("axis X"));
  subItem[4]->setEnabled(false);
  subItem[4]->setAttribute(QLatin1String("decimals"), 3);

  subsubItem[0] = vm->addProperty(QVariant::Double, QString("X"));
  subsubItem[0]->setValue(probe->axisX.x);
  subsubItem[1] = vm->addProperty(QVariant::Double, QString("Y"));
  subsubItem[1]->setValue(probe->axisX.y);
  subsubItem[2] = vm->addProperty(QVariant::Double, QString("Z"));
  subsubItem[2]->setValue(probe->axisX.z);

  subItem[4]->addSubProperty(subsubItem[0]);
  subItem[4]->addSubProperty(subsubItem[1]);
  subItem[4]->addSubProperty(subsubItem[2]);

  subItem[5] = vm->addProperty(QtVariantPropertyManager::flagTypeId(), QString("axis Y"));
  subItem[5]->setEnabled(false);
  subItem[5]->setAttribute(QLatin1String("decimals"), 3);

  subsubItem[0] = vm->addProperty(QVariant::Double, QString("X"));
  subsubItem[0]->setValue(probe->axisY.x);
  subsubItem[1] = vm->addProperty(QVariant::Double, QString("Y"));
  subsubItem[1]->setValue(probe->axisY.y);
  subsubItem[2] = vm->addProperty(QVariant::Double, QString("Z"));
  subsubItem[2]->setValue(probe->axisY.z);

  subItem[5]->addSubProperty(subsubItem[0]);
  subItem[5]->addSubProperty(subsubItem[1]);
  subItem[5]->addSubProperty(subsubItem[2]);

  subItem[6] = vm->addProperty(QtVariantPropertyManager::flagTypeId(), QString("axis Z (normal)"));
  subItem[6]->setEnabled(false);
  subItem[6]->setAttribute(QLatin1String("decimals"), 3);

  subsubItem[0] = vm->addProperty(QVariant::Double, QString("X"));
  subsubItem[0]->setValue(probe->normal.x);
  subsubItem[1] = vm->addProperty(QVariant::Double, QString("Y"));
  subsubItem[1]->setValue(probe->normal.y);
  subsubItem[2] = vm->addProperty(QVariant::Double, QString("Z"));
  subsubItem[2]->setValue(probe->normal.z);

  subItem[6]->addSubProperty(subsubItem[0]);
  subItem[6]->addSubProperty(subsubItem[1]);
  subItem[6]->addSubProperty(subsubItem[2]);

  probeBrowserPropertyToId[subItem[0]] = QLatin1String("color");
  probeBrowserPropertyToId[subItem[1]] = QLatin1String("origin (mm)");
  probeBrowserPropertyToId[subItem[2]] = QLatin1String("radius (mm)");
  probeBrowserPropertyToId[subItem[3]] = QLatin1String("area (mm2)");
  probeBrowserPropertyToId[subItem[4]] = QLatin1String("axis X");
  probeBrowserPropertyToId[subItem[5]] = QLatin1String("axis Y");
  probeBrowserPropertyToId[subItem[6]] = QLatin1String("axis Z (normal)");

  for(int i=0; i<7; i++) pb->addProperty(subItem[i]);  

  // Collapse all sub items
  QList<QtBrowserItem *> topList = pb->topLevelItems();
  QListIterator<QtBrowserItem *> topIt(topList);
  
  while (topIt.hasNext()) {
    QtBrowserItem *item = topIt.next();
    pb->setExpanded(item, false);   
  }  

  this->probeBrowserInitState = true;
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializeProbeBrowser(qfeProbe3D *probe, QtTreePropertyBrowser* pb, QtVariantPropertyManager *vm, QtVariantEditorFactory *vf)
{
  if(probe == NULL) return;  

  QString     label;     

  this->probeBrowserInitState = false;

  pb->setFactoryForManager(vm, vf);  
  pb->setPropertiesWithoutValueMarked(true);
  //pb->setRootIsDecorated(false);
  
  QtVariantProperty  *subItem[8];
  QtVariantProperty  *subsubItem[3];

  subItem[0] = vm->addProperty(QVariant::Color, QString("color"));     
  subItem[0]->setValue(QColor((int)(probe->color.r*255.0), (int)(probe->color.g*255.0), (int)(probe->color.b*255.0), 255)); 
    
  subItem[1] = vm->addProperty(QVariant::Double, QString("length"));     
  subItem[1]->setAttribute(QLatin1String("minimum"), 0);
  subItem[1]->setValue(probe->length);  

  subItem[2] = vm->addProperty(QVariant::Double, QString("radius top"));    
  subItem[2]->setAttribute(QLatin1String("minimum"), 0);
  subItem[2]->setValue(probe->topRadius);    

  subItem[3] = vm->addProperty(QVariant::Double, QString("radius base"));     
  subItem[3]->setAttribute(QLatin1String("minimum"), 0);
  subItem[3]->setValue(probe->baseRadius);  
 
  subItem[4] = vm->addProperty(QtVariantPropertyManager::flagTypeId(), QString("origin (mm)"));
  subItem[4]->setAttribute(QLatin1String("decimals"), 3);

  subsubItem[0] = vm->addProperty(QVariant::Double, QString("X"));
  subsubItem[0]->setValue(probe->origin.x);
  subsubItem[1] = vm->addProperty(QVariant::Double, QString("Y"));
  subsubItem[1]->setValue(probe->origin.y);
  subsubItem[2] = vm->addProperty(QVariant::Double, QString("Z"));
  subsubItem[2]->setValue(probe->origin.z);

  subItem[4]->addSubProperty(subsubItem[0]);
  subItem[4]->addSubProperty(subsubItem[1]);
  subItem[4]->addSubProperty(subsubItem[2]);

  subItem[5] = vm->addProperty(QtVariantPropertyManager::flagTypeId(), QString("axis X (normal)"));
  subItem[5]->setAttribute(QLatin1String("decimals"), 3);

  subsubItem[0] = vm->addProperty(QVariant::Double, QString("X"));
  subsubItem[0]->setValue(probe->axes[0][0]);
  subsubItem[1] = vm->addProperty(QVariant::Double, QString("Y"));
  subsubItem[1]->setValue(probe->axes[0][1]);
  subsubItem[2] = vm->addProperty(QVariant::Double, QString("Z"));
  subsubItem[2]->setValue(probe->axes[0][2]);

  subItem[5]->addSubProperty(subsubItem[0]);
  subItem[5]->addSubProperty(subsubItem[1]);
  subItem[5]->addSubProperty(subsubItem[2]);

  subItem[6] = vm->addProperty(QtVariantPropertyManager::flagTypeId(), QString("axis Y (binormal)"));
  subItem[6]->setAttribute(QLatin1String("decimals"), 3);

  subsubItem[0] = vm->addProperty(QVariant::Double, QString("X"));
  subsubItem[0]->setValue(probe->axes[1][0]);
  subsubItem[1] = vm->addProperty(QVariant::Double, QString("Y"));
  subsubItem[1]->setValue(probe->axes[1][1]);
  subsubItem[2] = vm->addProperty(QVariant::Double, QString("Z"));
  subsubItem[2]->setValue(probe->axes[1][2]);

  subItem[6]->addSubProperty(subsubItem[0]);
  subItem[6]->addSubProperty(subsubItem[1]);
  subItem[6]->addSubProperty(subsubItem[2]);

  subItem[7] = vm->addProperty(QtVariantPropertyManager::flagTypeId(), QString("axis Z (long axis)"));
  subItem[7]->setAttribute(QLatin1String("decimals"), 3);

  subsubItem[0] = vm->addProperty(QVariant::Double, QString("X"));
  subsubItem[0]->setValue(probe->axes[2][0]);
  subsubItem[1] = vm->addProperty(QVariant::Double, QString("Y"));
  subsubItem[1]->setValue(probe->axes[2][1]);
  subsubItem[2] = vm->addProperty(QVariant::Double, QString("Z"));
  subsubItem[2]->setValue(probe->axes[2][2]);

  subItem[7]->addSubProperty(subsubItem[0]);
  subItem[7]->addSubProperty(subsubItem[1]);
  subItem[7]->addSubProperty(subsubItem[2]);

  probeBrowserPropertyToId[subItem[0]] = QLatin1String("color");
  probeBrowserPropertyToId[subItem[1]] = QLatin1String("length");
  probeBrowserPropertyToId[subItem[2]] = QLatin1String("radius top");
  probeBrowserPropertyToId[subItem[3]] = QLatin1String("radius base"); 
  probeBrowserPropertyToId[subItem[4]] = QLatin1String("origin (mm)");
  probeBrowserPropertyToId[subItem[5]] = QLatin1String("axis X (normal)");
  probeBrowserPropertyToId[subItem[6]] = QLatin1String("axis Y (binormal)");
  probeBrowserPropertyToId[subItem[7]] = QLatin1String("axis Z (long axis)");

  for(int i=0; i<8; i++) pb->addProperty(subItem[i]);  

  // Collapse all sub items
  QList<QtBrowserItem *> topList = pb->topLevelItems();
  QListIterator<QtBrowserItem *> topIt(topList);
  
  while (topIt.hasNext()) {
    QtBrowserItem *item = topIt.next();
    pb->setExpanded(item, false);   
  }  

  this->probeBrowserInitState = true;
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializeProbeQuantification(qfeProbe2D *probe, int index)
{
  QVector<double> x, y;
  int             xCount, yCount;

  if((int)probe->quantification.size() <= 0)     return;
  if((int)probe->quantification.size() <= index) index = 0;

  xCount = (int) ((abs(probe->quantification[index].rangeX[1]) - abs(probe->quantification[index].rangeX[0])) / probe->quantification[index].stepsX);
  yCount = (int) probe->quantification[index].values.size();

  x.clear(); 
  for (int i=0; i<xCount; ++i)
  {
    x.push_back(probe->quantification[index].rangeX[0] + i*probe->quantification[index].stepsX);
  }
  y.clear();
  for (int i=0; i<yCount; ++i)
  {
    y.push_back(probe->quantification[index].values[i]);
  }

  // create graph and assign data to it:
  this->fluxPlot->addGraph();
  this->fluxPlot->graph(0)->setData(x, y);
  // give the axes some labels:
  this->fluxPlot->xAxis->setLabel(probe->quantification[index].labelX.c_str());
  this->fluxPlot->yAxis->setLabel(probe->quantification[index].labelY.c_str());
  // set axes ranges, so we see all data:
  this->fluxPlot->xAxis->setRange(probe->quantification[index].rangeX[0], probe->quantification[index].rangeX[1]+10.0);
  this->fluxPlot->yAxis->setRange(probe->quantification[index].rangeY[0], probe->quantification[index].rangeY[1]+10.0);
  //this->fluxPlot->xAxis->setAutoTickStep(false);
  //this->fluxPlot->yAxis->setAutoTickStep(true);
  //this->fluxPlot->xAxis->setTickStep(probe->quantification[index].stepsX);
  this->fluxPlot->xAxis->setTickLength(probe->quantification[index].stepsX);
  this->fluxPlot->replot();

  int   currentPhase;
  currentPhase = floor(this->UI->horizontalSliderTime->value()*this->animationFactor);
  
  if(currentPhase < 0)       currentPhase = 0;
  if(currentPhase >= xCount) currentPhase = xCount-1;

  this->UI->doubleSpinBoxProbeQuantification->setValue(y[currentPhase]);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializeVolumeBrowser(qfePatient *patient, QtTreePropertyBrowser* pb, QtVariantPropertyManager *vm, QtVariantEditorFactory *vf)
{
  if(patient == NULL) return;  

  this->volumeBrowserInitState = false;

  qfeStudy *study = patient->study;

  QtProperty **topItem = new QtProperty*[1];

  pb->setFactoryForManager(vm, vf);  
  pb->setPropertiesWithoutValueMarked(true);
  pb->setRootIsDecorated(false);  

  QtVariantProperty  *subItem[9];

  topItem[0] = vm->addProperty(QtVariantPropertyManager::groupTypeId(), QString("Study volumes"));  

  subItem[0] = vm->addProperty(QVariant::Int,  QString("3D anatomy - ssfp"));     
  subItem[0]->setValue((int)study->ssfp3D.size());    
  subItem[0]->setEnabled(false);
  subItem[1] = vm->addProperty(QVariant::Int,  QString("2D anatomy - ssfp"));     
  subItem[1]->setValue((int)study->ssfp2D.size());    
  subItem[1]->setEnabled(false);
  subItem[2] = vm->addProperty(QVariant::Int,  QString("4D flow - ffe"));     
  subItem[2]->setValue((int)study->ffe.size());    
  subItem[2]->setEnabled(false); 
  subItem[3] = vm->addProperty(QVariant::Int,  QString("4D flow - pca-m"));     
  subItem[3]->setValue((int)study->pcam.size());    
  subItem[3]->setEnabled(false); 
  subItem[4] = vm->addProperty(QVariant::Int,  QString("4D flow - pca-p"));     
  subItem[4]->setValue((int)study->pcap.size());    
  subItem[4]->setEnabled(false); 
  subItem[5] = vm->addProperty(QVariant::Bool, QString("4D flow - t-mip"));     
  subItem[5]->setEnabled(false); 
  subItem[6] = vm->addProperty(QVariant::Bool, QString("4D flow - t-mop"));     
  subItem[6]->setEnabled(false); 
  subItem[7] = vm->addProperty(QVariant::Int, QString("2D flow"));     
  subItem[7]->setValue((int)study->flow2D_pcap.size());    
  subItem[7]->setEnabled(false); 
  subItem[8] = vm->addProperty(QVariant::Int,  QString("Fluid simulation"));     
  subItem[8]->setValue((int)study->sim_velocities.size());    
  subItem[8]->setEnabled(false);   

  if(study->tmip != NULL)
    subItem[5]->setValue(true);    
  else
    subItem[5]->setValue(false);  

  if(study->tmop != NULL)
    subItem[6]->setValue(true);    
  else
    subItem[6]->setValue(false);  
  
  topItem[0]->addSubProperty(subItem[0]);
  topItem[0]->addSubProperty(subItem[1]);
  topItem[0]->addSubProperty(subItem[2]);
  topItem[0]->addSubProperty(subItem[3]);
  topItem[0]->addSubProperty(subItem[4]);
  topItem[0]->addSubProperty(subItem[5]);
  topItem[0]->addSubProperty(subItem[6]);
  topItem[0]->addSubProperty(subItem[7]);
  topItem[0]->addSubProperty(subItem[8]);

  pb->addProperty(topItem[0]);  

  this->volumeBrowserInitState = true;
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializeProbeTree(qfePatient *patient, QTreeWidget *tree)
{  
  if(patient == NULL) return;  

  QString label;

  for(int i=0; i<(int)patient->study->probe2D.size(); i++)
  {
    label.sprintf("probe %d", i);

    QTreeWidgetItem *item = new QTreeWidgetItem(tree->topLevelItem(1));
    item->setText(0, label);
    item->setData(0, Qt::UserRole, i);
  }
  
  for(int i=0; i<(int)patient->study->probe3D.size(); i++)
  {
    label.sprintf("probe %d", i);

    QTreeWidgetItem *item = new QTreeWidgetItem(tree->topLevelItem(2));
    item->setText(0, label);
    item->setData(0, Qt::UserRole, i);
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUninitializeModelBrowser()
{
  this->modelBrowserInitState = false;
  this->modelBrowser->clear();  
  this->modelBrowserVariantManager->clear();  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUninitializeProbeBrowser()
{
  this->probeBrowserInitState = false;
  this->probeBrowser->clear();  
  this->probeBrowserVariantManager->clear();  
}


//----------------------------------------------------------------------------
void qtMainWindow::guiUninitializeVolumeBrowser()
{
  this->volumeBrowserInitState = false;
  this->volumeBrowser->clear();  
  this->volumeBrowserVariantManager->clear();  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUninitializeProbeTree()
{
  this->UI->treeWidgetProbes->clear();

  QTreeWidgetItem *pickPoint = new QTreeWidgetItem(this->UI->treeWidgetProbes);
  pickPoint->setText(0, tr("Pick"));
  QTreeWidgetItem *probe2D = new QTreeWidgetItem(this->UI->treeWidgetProbes);
  probe2D->setText(0, tr("Probe 2D"));
  QTreeWidgetItem *probe3D = new QTreeWidgetItem(this->UI->treeWidgetProbes);
  probe3D->setText(0, tr("Probe 3D"));

}


//----------------------------------------------------------------------------
void qtMainWindow::guiInitializeLight(qfeScene *scene)
{
  if(scene == NULL) return;

  qfeLightSource *light;
  scene->qfeGetSceneLightSource(&light);

  if(light == NULL) return;

  qfeFloat a, d, s, sp;

  light->qfeGetLightSourceAmbientContribution(a);
  light->qfeGetLightSourceDiffuseContribution(d);
  light->qfeGetLightSourceSpecularContribution(s);
  light->qfeGetLightSourceSpecularPower(sp);

  this->UI->doubleSpinBoxLightAmbient->setValue(a);
  this->UI->doubleSpinBoxLightDiffuse->setValue(d);
  this->UI->doubleSpinBoxLightSpecular->setValue(s);
  this->UI->doubleSpinBoxLightSpecularPower->setValue(sp);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendColor()
{  
  qfePatient           *patient;
  float                 maxVal;

  this->kernel->DataManager->qfeGetPatient(&patient);  

  if(patient == NULL) return;
  if(patient->study->pcap.size() == 0) return;

  int   currentPhase;
  currentPhase = floor(this->UI->horizontalSliderTime->value()*this->animationFactor);

  // Update the transfer functions for set of visuals
  this->guiUninitializeTransferFunctions();
  this->guiInitializeTransferFunctions();    

  transferFunction->clearHistograms();
 
  for(int i=0; i<(int)this->histograms.size(); i++)
  {
    this->transferFunction->addHistogram(this->histograms[i]);
  }

  maxVal = 0;
  for(int i=0; i<(int)patient->study->pcap.size(); i++)
  {
    float vu, vl;
    patient->study->pcap[i]->qfeGetVolumeValueDomain(vl, vu);

    if(maxVal < abs(vl)) maxVal = abs(vl);
    if(maxVal < abs(vu)) maxVal = abs(vu);   
  }
  maxVal = 10.0f*ceil(maxVal/10.0f);

  this->transferFunction->setAxisX("velocity (cm/s)", 0.0, maxVal);
  this->transferFunction->setAxisY("opacity", 0.0, 1.0);  
  this->featureTF1->setAxisX("x-axis", 0.0, 1.0);
  this->featureTF1->setAxisY("probability", 0.0, 1.0);
  this->featureTF2->setAxisX("x-axis", 0.0, 1.0);
  this->featureTF2->setAxisY("probability", 0.0, 1.0);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateBackendColor()
{
  qfeScene                         *scene;
  vector< qfeVisual * >             visuals;
  QVector< QPair<double, double*> > color;
  QVector< QPair<double, double>  > opacity;
  QVector< QPair<double, double>  > gradient;
  int                               interpolation;
  int                               quantization;
  int                               space;
  bool                              constlight;
  bool                              paduni;

  this->kernel->SceneManager->qfeGetSceneManagerScene(&scene);
  scene->qfeGetSceneVisuals(visuals);

  // Get the transfer function values   
  this->transferFunction->getColorInterpolation(interpolation);
  this->transferFunction->getColorLightnessConstant(constlight);
  this->transferFunction->getColorPadding(paduni);
  this->transferFunction->getColorQuantization(quantization);  
  this->transferFunction->getColorSpace(space);

  // Update the backend
  if(this->state >= initdata)
  {
    for(int i=0; i<(int)visuals.size(); i++)
    {
      this->transferFunction->getTransferFunction(i, color, opacity, gradient);
      this->kernel->qfeKernelUpdateColorMap(i, color, opacity, gradient, interpolation, quantization, space, constlight, paduni); 
    }
  }

  // Render the result
  this->guiUpdateRenderWindow();
}

//----------------------------------------------------------------------------
void qtMainWindow::guiShowDialogBrowseSingle()
{
  QStringList names;
  QStringList filters;
  filters << "QFlow VTK XML (*.vti)" <<  "All Files (*.*)";

  //QFileDialog dialog;
  //QString fileName = QFileDialog::getOpenFileName(this, "Test Dialog", "", "Zip-File (*.zip)");

  QFileDialog dialog(this, tr("Open QFlow data"), tr("c:"));
  dialog.setHistory(dialog.history());
  dialog.setFilters(filters);
  dialog.setFileMode(QFileDialog::ExistingFile);

  if(dialog.exec())
  {
    names = dialog.selectedFiles();
    dialog.close();
  }

  if(names.isEmpty()) return;

  if(dialog.close()) {    
    this->guiUpdateRenderWindow();   
    this->guiLoadData(names);
  }  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiShowDialogBrowseAll()
{
  QStringList names;
  QStringList filters;
  QStringList fileNames;
  filters << "QFlow VTK XML (*.vti)" <<  "All Files (*.*)";

  QFileDialog dialog(this, tr("Open QFlow series"), tr("c:"));
  dialog.setFilters(filters);
  dialog.setFileMode(QFileDialog::ExistingFile);

  //names = QFileDialog::getOpenFileNames(this, tr("Open QFlow series"), "c:/", tr("QFlow VTK XML (*.vti)"));
  //fileName = QFileDialog::getOpenFileName();
  
  if(dialog.exec()) names = dialog.selectedFiles();

  if(names.isEmpty()) return;

  if(names.size() == 1)
  {
    QString selectedFile = names.at(0);
    QString path, fileNameExt, fileName, fileNameNoNr, extension, currentFile;  

    // Get the file name
    if(selectedFile.contains('/'))  
      fileNameExt = selectedFile.split(tr("/")).last();
    else
      if(selectedFile.contains('\\'))  
        fileNameExt = selectedFile.split(tr("\\")).last();
      else 
        fileNameExt = selectedFile;

    // Get the path to the files
    path = selectedFile.remove(fileNameExt, Qt::CaseInsensitive);

    // Strip the extension
    if(fileNameExt.contains('.')){
      fileName.append(fileNameExt.split(tr(".")).first());
      extension = QString::fromLocal8Bit(".");
      extension.append(fileNameExt.split(tr(".")).last());
    }
    else{
      fileName.append(fileNameExt);
      extension = QString::fromLocal8Bit(".vti");
    }

    // Chop the series number
    fileNameNoNr.append(fileName);
    fileNameNoNr.chop(2);

    // Get the number of the current file
    QString number;
    /*
    number.sprintf("%.2d", 0);      
    
    currentFile.clear();
    currentFile += path;
    currentFile += fileNameNoNr;
    currentFile += number;
    currentFile += extension;*/

    for(int i=0; i< MAX_PHASES; i++)
    {     
      number.sprintf("%.2d", i);
      currentFile.clear();
      currentFile += path;
      currentFile += fileNameNoNr;        
      currentFile += number;
      currentFile += extension;

      if(QFile::exists(currentFile)){
        fileNames.append(currentFile);
      }
    }     
  }
  else
  {
    fileNames = QStringList::QStringList(names);
  }

  //this->state = initdata;

  // The browse dialog remain visible on the render window,
  // because the loading blocks the app execution.
  // Here we wait for the dialog to close, and redraw
  if(dialog.close()) {
    this->guiUpdateRenderWindow();
    this->guiLoadData(fileNames);    
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiShowDialogBrowseMesh()
{
  QStringList names;
  QStringList filters;
  filters << "VTK mesh (*.vtk)" <<  "All Files (*.*)";

  QFileDialog dialog(this, tr("Add QFlow mesh"), tr("c:"));
  dialog.setFilters(filters);
  dialog.setFileMode(QFileDialog::ExistingFile);

  if(dialog.exec())
  {
    names = dialog.selectedFiles();
    dialog.close();
  }

  if(names.isEmpty()) return;


  if(dialog.close()) {
    this->guiLoadMesh(string(names.back().toLocal8Bit().data()));
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiShowDialogBrowseSeeds()
{
  QStringList names;
  QStringList filters;
  filters << "QFE seeds (*.seeds)" <<  "All Files (*.*)";

  QFileDialog dialog(this, tr("Add QFlow seeds"), tr("c:"));
  dialog.setFilters(filters);
  dialog.setFileMode(QFileDialog::ExistingFile);

  if(dialog.exec())
  {
    names = dialog.selectedFiles();
    dialog.close();
  }

  if(names.isEmpty()) return;


  if(dialog.close()) {
    this->guiLoadSeeds(string(names.back().toLocal8Bit().data()));
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiShowDialogBrowseClusterHierarchy()
{
  QStringList names;
  QStringList filters;
  filters << "Cluster Hierarchy (*.dat)" <<  "All Files (*.*)";

  QFileDialog dialog(this, tr("Add Cluster Hierarchy"), tr("c:"));
  dialog.setFilters(filters);
  dialog.setFileMode(QFileDialog::ExistingFile);

  if(dialog.exec())
  {
    names = dialog.selectedFiles();
    dialog.close();
  }

  if(names.isEmpty()) return;


  if(dialog.close()) {
    this->guiLoadClusterHierarchy(string(names.back().toLocal8Bit().data()));
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiShowDialogBrowseFluidSimulation()
{
  QStringList names;
  QStringList filters;
  QStringList fileNames;
  filters << "QFlow VTK XML (*.vti)" <<  "All Files (*.*)";

  QFileDialog dialog(this, tr("Open QFlow series"), tr("c:"));
  dialog.setFilters(filters);
  dialog.setFileMode(QFileDialog::ExistingFile);

  //names = QFileDialog::getOpenFileNames(this, tr("Open QFlow series"), "c:/", tr("QFlow VTK XML (*.vti)"));
  //fileName = QFileDialog::getOpenFileName();
  
  if(dialog.exec()) names = dialog.selectedFiles();

  if(names.isEmpty()) return;

  if(names.size() == 1)
  {
    QString selectedFile = names.at(0);
    QString path, fileNameExt, fileName, fileNameNoNr, extension, currentFile;  

    // Get the file name
    if(selectedFile.contains('/'))  
      fileNameExt = selectedFile.split(tr("/")).last();
    else
      if(selectedFile.contains('\\'))  
        fileNameExt = selectedFile.split(tr("\\")).last();
      else 
        fileNameExt = selectedFile;

    // Get the path to the files
    path = selectedFile.remove(fileNameExt, Qt::CaseInsensitive);

    // Strip the extension
    if(fileNameExt.contains('.')){
      fileName.append(fileNameExt.split(tr(".")).first());
      extension = QString::fromLocal8Bit(".");
      extension.append(fileNameExt.split(tr(".")).last());
    }
    else{
      fileName.append(fileNameExt);
      extension = QString::fromLocal8Bit(".vti");
    }

    // Chop the series number
    fileNameNoNr.append(fileName);
    fileNameNoNr.chop(2);

    // Get the number of the current file
    QString number;
    /*
    number.sprintf("%.2d", 0);      
    
    currentFile.clear();
    currentFile += path;
    currentFile += fileNameNoNr;
    currentFile += number;
    currentFile += extension;*/

    for(int i=0; i< MAX_PHASES; i++)
    {     
      number.sprintf("%.2d", i);
      currentFile.clear();
      currentFile += path;
      currentFile += fileNameNoNr;        
      currentFile += number;
      currentFile += extension;

      if(QFile::exists(currentFile)){
        fileNames.append(currentFile);
      }
    }     
  }
  else
  {
    fileNames = QStringList::QStringList(names);
  }

  //this->state = initdata;

  // The browse dialog remain visible on the render window,
  // because the loading blocks the app execution.
  // Here we wait for the dialog to close, and redraw
  if(dialog.close()) {
    this->guiUpdateRenderWindow();
    this->guiLoadFluidSimulation(fileNames);    
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiShowDialogAbout()
{  
  qtAboutDialog *about = new qtAboutDialog();

  about->show();
  about->raise();  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiAnimationStartStop() {
  this->animationEnabled = !this->animationEnabled;

  if(this->animationEnabled){
    this->UI->toolButtonPlay->setIcon(QIcon::QIcon(":/ui/images/Pause.png"));

    // TODO keep animation soft real-time   
    this->timeCounter = this->UI->horizontalSliderTime->value();

    this->timer->setInterval((int)((1.0/this->animationFactor) * this->phaseDuration * this->animationFactor));
    this->timer->start();
  }
  else {
    this->UI->toolButtonPlay->setIcon(QIcon::QIcon(":/ui/images/Play.png"));

    this->timer->stop();
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiAnimationTimer()
{
  qfePatient *patient;
  float currentTime;    
  float phaseDuration;

  this->kernel->DataManager->qfeGetPatient(&patient);  

  // Releasing data during animation makes patient = NULL
  if (!patient) {
    guiAnimationStartStop();
    return;
  }

  if(patient->study->pcap.size() > 0)
    patient->study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);
  else 
    phaseDuration = 0;

  this->timeCounter++;
  this->timeCounter = this->timeCounter%(this->UI->horizontalSliderTime->maximum()+1);

  currentTime = timeCounter * this->animationFactor;

  this->UI->horizontalSliderTime->setSliderPosition(timeCounter);
  this->UI->spinBoxTime->setValue(currentTime * phaseDuration);
  this->kernel->SceneManager->qfeSetSceneManagerTemporalPhase(currentTime);

  this->guiUpdateRenderWindow();
}

void qtMainWindow::guiPlotData()
{
	float phase = (float)this->UI->horizontalSliderTime->sliderPosition() * this->animationFactor;//%(this->UI->horizontalSliderTime->maximum()+1);
	this->kernel->RenderPlot(phase);
}

void qtMainWindow::guiUpdateFeatures1(int t)
{
  featureTF1->clearHistograms();

  qfeStudy *study = getStudy();
  if (!study)
    return;

  std::vector<qfeHistogram*> histograms;
  std::string label;
  switch (t) {
  case 0: // None
    break;
  case 1: // Flow speed
    histograms = getHistograms(study->flowSpeed);
    label = "flow speed";
    break;
  case 2: // Magnitude (PCA-M)
    histograms = getHistograms(study->pcam);
    label = "magnitude";
    break;
  case 3: // Curl
    histograms = getHistograms(study->curl);
    label = "curl";
    break;
//   case 4: // FTLE
//     histograms = getHistograms(study->ftle); break;
  case 4: // Lambda2
    histograms = getHistograms(study->lambda2);
    label = "lambda2";
    break;
  case 5: // Divergence
    histograms = getHistograms(study->divergence);
    label = "divergence";
    break;
  case 6: // Q-Criterion
    histograms = getHistograms(study->qcriterion);
    label = "qcriterion";
    break;
  }

  float rangeMin, rangeMax;
  if (histograms.size() > 0) {
    histograms[0]->qfeGetRange(rangeMin, rangeMax);
    featureTF1->setAxisX(QString(label.c_str()), rangeMin, rangeMax);
  }
  featureTF1->addHistograms(histograms);
  featureTF1->setDataType(t);
}

void qtMainWindow::guiUpdateFeatures2(int t)
{
  featureTF2->clearHistograms();

  qfeStudy *study = getStudy();
  if (!study)
    return;

  std::vector<qfeHistogram*> histograms;
  std::string label;
  switch (t) {
  case 0: // None
    break;
  case 1: // Flow speed
    histograms = getHistograms(study->flowSpeed);
    label = "flow speed";
    break;
  case 2: // Magnitude (PCA-M)
    histograms = getHistograms(study->pcam);
    label = "magnitude";
    break;
  case 3: // Curl
    histograms = getHistograms(study->curl);
    label = "curl";
    break;
    //   case 4: // FTLE
    //     histograms = getHistograms(study->ftle); break;
  case 4: // Lambda2
    histograms = getHistograms(study->lambda2);
    label = "lambda2";
    break;
  case 5: // Divergence
    histograms = getHistograms(study->divergence);
    label = "divergence";
    break;
  case 6: // Q-Criterion
    histograms = getHistograms(study->qcriterion);
    label = "qcriterion";
    break;
  }

  float rangeMin, rangeMax;
  if (histograms.size() > 0) {
    histograms[0]->qfeGetRange(rangeMin, rangeMax);
    featureTF2->setAxisX(QString(label.c_str()), rangeMin, rangeMax);
  }
  featureTF2->addHistograms(histograms);
  featureTF2->setDataType(t);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiAnimationSpeed(int t)
{
  qfePatient *patient;

  this->kernel->DataManager->qfeGetPatient(&patient);  

  switch(t)
  {
  case(0) : this->animationFactor     = 1.0;            
            break;
  case(1) : this->animationFactor     = 0.75;            
            break;
  case(2) : this->animationFactor     = 0.5;            
            break;
  case(3) : this->animationFactor     = 0.25;            
            break;
  case(4) : this->animationFactor     = 0.1;            
            break;
  case(5) : this->animationFactor     = 0.05;            
            break;
  case(6) : this->animationFactor     = 0.02;            
            break;
  case(7) : this->animationFactor     = 0.01;            
            break;
  default : this->animationFactor     = 1.0;            
  }

  // Update the horizontal slider
  if(patient != NULL)
  {
    this->guiAnimationUpdateSlider(patient->study->pcap.size(), this->animationFactor);
  }

  // Restart the animation
  if(this->animationEnabled)
  {
    this->guiAnimationStartStop(); // stop
    this->guiAnimationStartStop(); // start
  }  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiAnimationSlider(int t)
{  
  qfePatient *patient;
  float currentTime;    
  float phaseDuration;

  this->kernel->DataManager->qfeGetPatient(&patient);  
  if(patient && patient->study->pcap.size() > 0)
    patient->study->pcap.front()->qfeGetVolumePhaseDuration(phaseDuration);
  else 
    phaseDuration = 0;
  
  currentTime = t * this->animationFactor;  

  this->transferFunction->setHistogram(floor(currentTime));
  this->featureTF1->setHistogramNumber(floor(currentTime));
  this->featureTF2->setHistogramNumber(floor(currentTime));
  this->UI->spinBoxTime->setValue(currentTime * phaseDuration);

  if(!this->animationEnabled)
  {
    this->kernel->SceneManager->qfeSetSceneManagerTemporalPhase(currentTime);
    this->guiUpdateRenderWindow();
    this->guiUpdateFrontendProbeQuantification();
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiAnimationUpdateSlider(int phases, double animationFactor)
{
  int maximum;
  int interval;
  if(phases > 0)
  {           
    interval = (int)1.0/this->animationFactor;
    maximum = phases + (interval - 1)*phases;
    this->UI->horizontalSliderTime->setMaximum(maximum);    
    this->UI->horizontalSliderTime->setTickInterval(interval);
  }
  else
    this->UI->horizontalSliderTime->setMaximum(0);      
}

//----------------------------------------------------------------------------
void qtMainWindow::guiHandleAction()
{  
  QToolButton *button = dynamic_cast<QToolButton *>(sender());

  for(int i=0; i<(int)this->actions.size(); i++)
  {
    if(button == this->actions[i])
    {      
      // Hack: use the VTK system indirectly to pass the action down the pipeline.
      // This should be processed through the kernel with separate QT and VTK event systems.
      this->UI->vtkWidget->GetRenderWindow()->InvokeEvent(vtkCommand::UserEvent, &this->actionsType[i]);      
      //this->viewWidget->canvas()->GetSubCanvas3D()->GetRenderWindow()->InvokeEvent(vtkCommand::UserEvent, &this->actionsType[i]);
    }    
  }     
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateScenePreset(int t)
{
  if(this->state >= initpost)
  {
    this->kernel->qfeKernelRenderScene(t);    

    this->guiUpdateBackendColor();
    this->guiUpdateFrontend();
    this->guiUpdateBackendLightSource();
    this->guiUpdateRenderWindow();
  }
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateBackendLightSource()
{
  QVector3D dir;
  qfeVector lightDir;
  qfeFloat  a, d, s, sp;

  // Get the light direction from the widget
  //dir = this->ballLight->getDirectionVector();

  // Medical images light direction (45 degrees around y-axis, 30 degrees around x axis)
  dir.setX(+0.707107);
  dir.setY(-0.353553);
  dir.setZ(-0.612372); 

  //dir.setX(0.0);
  //dir.setY(0.0);
  //dir.setZ(-1.0); 

  lightDir.qfeSetVectorElements(dir.x(), dir.y(), dir.z());
  lightDir.x = dir.x();
  lightDir.y = dir.y();
  lightDir.z = dir.z();

  a  = this->UI->doubleSpinBoxLightAmbient->value();
  d  = this->UI->doubleSpinBoxLightDiffuse->value();
  s  = this->UI->doubleSpinBoxLightSpecular->value();
  sp = this->UI->doubleSpinBoxLightSpecularPower->value();

  // Update the light direction
  this->kernel->qfeKernelUpdateLight(lightDir, a, d, s, sp);

  // Update the render windows
  this->guiUpdateRenderWindow();
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateFrontendLightSource()
{
  QVector3D       dir;
  qfeVector       lightDir;
  qfeFloat        a, d, s, sp;

  qfeScene       *scene;
  qfeLightSource *light;

  this->kernel->SceneManager->qfeGetSceneManagerScene(&scene);

  if(scene == NULL) return;

  scene->qfeGetSceneLightSource(&light); 

  if(light == NULL) return;

  light->qfeGetLightSourceAmbientContribution(a);
  light->qfeGetLightSourceDiffuseContribution(d);
  light->qfeGetLightSourceSpecularContribution(s);
  light->qfeGetLightSourceSpecularPower(sp);

  // Get the light direction from the widget
  //dir = this->ballLight->getDirectionVector();
  //lightDir.x = dir.x();
  //lightDir.y = dir.y();
  //lightDir.z = dir.z();

  this->UI->doubleSpinBoxLightAmbient->setValue(a);
  this->UI->doubleSpinBoxLightDiffuse->setValue(d);
  this->UI->doubleSpinBoxLightSpecular->setValue(s);
  this->UI->doubleSpinBoxLightSpecularPower->setValue(sp);  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUpdateDisplay()
{

  int         type;
  QLineEdit *button = (QLineEdit *)sender();

  if(button == this->UI->lineEditBackgroundColorPrimary) 
  {
    QColor currentColor = this->UI->lineEditBackgroundColorPrimary->palette().color(this->UI->lineEditBackgroundColorPrimary->backgroundRole());
    QColor color = QColorDialog::getColor(currentColor, this);

    QPalette palette;
    palette.setColor(this->UI->lineEditBackgroundColorPrimary->backgroundRole(), color);
    this->UI->lineEditBackgroundColorPrimary->setPalette(palette);
  }
  
  if(button == this->UI->lineEditBackgroundColorSecondary) 
  {
    QColor currentColor = this->UI->lineEditBackgroundColorSecondary->palette().color(this->UI->lineEditBackgroundColorSecondary->backgroundRole());
    QColor color = QColorDialog::getColor(currentColor, this);

    QPalette palette;
    palette.setColor(this->UI->lineEditBackgroundColorSecondary->backgroundRole(), color);
    this->UI->lineEditBackgroundColorSecondary->setPalette(palette);
  }

  QColor col1 = this->UI->lineEditBackgroundColorPrimary->palette().color(this->UI->lineEditBackgroundColorPrimary->backgroundRole());
  QColor col2 = this->UI->lineEditBackgroundColorSecondary->palette().color(this->UI->lineEditBackgroundColorSecondary->backgroundRole());

  if(this->UI->radioButtonBackgroundUniform->isChecked()) type = 0;
  else                                                    type = 1;

  this->kernel->qfeKernelUpdateDisplay(type, col1, col2);
}

//----------------------------------------------------------------------------
void qtMainWindow::guiLoadData(QStringList names)
{
  char **paths  = new char*[names.size()];
  int    phases = 0;

  // Convert pathnames from QString to char*
  for(int i=0; i<names.size(); i++)
  {    
    paths[i] = (char *)malloc((names.at(i).length()+1) * sizeof(char));
    //strcpy(paths[i], names.at(i).toLocal8Bit().data());
    strcpy_s(paths[i], names.at(i).length()+1, names.at(i).toLocal8Bit().data());
    phases++;
  } 

  // Load the data
  this->kernel->qfeKernelLoadData(paths,phases);
  this->kernel->qfeCalculateSpeedVolumes();
  this->kernel->qfeKernelRenderScene(sceneDefault);

  this->state = initdata;
  this->guiInitialize();  

  delete paths;
}

//----------------------------------------------------------------------------
void qtMainWindow::guiLoadMesh(string path)
{
  this->kernel->DataManager->qfeLoadSegmentation(path.c_str());  
  this->kernel->SceneManager->qfeSceneManagerUpdate();
  
  this->guiUpdateFrontendPropertyBrowser();
  this->guiUpdateFrontendModelBrowser();  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiLoadSeeds(string path)
{
  this->kernel->DataManager->qfeLoadSeeds(path.c_str()); 

  // TODO update data browser
}

//----------------------------------------------------------------------------
void qtMainWindow::guiLoadClusterHierarchy(string path)
{
  this->kernel->DataManager->qfeLoadClusterHierarchy(path.c_str());  
  this->kernel->SceneManager->qfeSceneManagerUpdate();
  
  this->guiUpdateFrontendPropertyBrowser();
  this->guiUpdateFrontendModelBrowser();  
}

//----------------------------------------------------------------------------
void qtMainWindow::guiLoadFluidSimulation(QStringList names)
{  
  this->guiUpdateFrontendPropertyBrowser();
  this->guiUpdateFrontendModelBrowser();  

  char **paths  = new char*[names.size()];
  int    phases = 0;

  // Convert pathnames from QString to char*
  for(int i=0; i<names.size(); i++)
  {    
    paths[i] = (char *)malloc((names.at(i).length()+1) * sizeof(char));
    strcpy_s(paths[i], names.at(i).length()+1, names.at(i).toLocal8Bit().data());
    phases++;
  } 

  // Load the data
  this->kernel->DataManager->qfeLoadFluidSimulation(paths,phases);  

  delete paths;
}

//----------------------------------------------------------------------------
void qtMainWindow::guiReleaseData()
{      
  this->state = initpre;
  this->guiUninitializePostData();

  this->kernel->qfeKernelRenderScene(sceneDefault); 
  this->guiUpdateRenderWindow();

  this->kernel->qfeKernelReleaseData();
}

//----------------------------------------------------------------------------
void qtMainWindow::guiSelectScene(int index)
{
  /*
  switch(index)
  {
  case 0  : this->kernel->SceneManager->qfeSceneManagerSelect(QFE_SCENE_DEFAULT); break;
  case 1  : this->kernel->SceneManager->qfeSceneManagerSelect(QFE_SCENE_MPR);     break;
  case 2  : this->kernel->SceneManager->qfeSceneManagerSelect(QFE_SCENE_MIP);     break;
  case 3  : this->kernel->SceneManager->qfeSceneManagerSelect(QFE_SCENE_IFR);     break;
  default : this->kernel->SceneManager->qfeSceneManagerSelect(QFE_SCENE_DEFAULT); break;
  }

  this->guiUpdatePropertyBrowser();

  this->kernel->SceneManager->qfeSceneManagerRender(NULL);
  */
}

//----------------------------------------------------------------------------
void qtMainWindow::guiInitializeTransferFunctions()
{
  qfeScene             *scene;
  vector< qfeVisual * > visuals;  

  this->kernel->SceneManager->qfeGetSceneManagerScene(&scene);

  if(scene != NULL)
  scene->qfeGetSceneVisuals(visuals);

  // Update the selection boxes
  for(int i=0; i<(int)visuals.size(); i++)
  {
    string label = this->guiGetVisualLabel(visuals[i]);
    this->transferFunction->addTransferFunction(QString(label.c_str()));    
  }   
}

//----------------------------------------------------------------------------
void qtMainWindow::guiUninitializeTransferFunctions()
{
  this->transferFunction->clearTransferFunction();
}

//----------------------------------------------------------------------------
void qtMainWindow::guiPrecomputeHistograms(qfeVolumeSeries &volumes)
{
  qfeCallData           cd;
  qtProgressBarCallBack cb;

  cb.SetProgressBar(progressBar);
  cb.SetStatusBar(statusBar());

  cd.message  = "Computing histograms..."; 
  cd.stepSize = 100;

  // Calculate range of values over all volumes
  float rangeMin = 1e30f;
  float rangeMax = -1e30f;
  for (unsigned i = 0; i < volumes.size(); i++) {
    float vl, vu;
    volumes[i]->qfeGetVolumeValueDomain(vl, vu);
    if (vl < rangeMin) rangeMin = vl;
    if (vu > rangeMax) rangeMax = vu;
  }

  // For 3 components, magnitude of 3d vector is taken,
  // but getVolumeValueDomain looks per component.
  // So below is some hacky stuff to set decent ranges
  unsigned nrComp;
  if (volumes.size() > 0)
    volumes[0]->qfeGetVolumeComponentsPerVoxel(nrComp);
  if (volumes.size() > 0 && nrComp == 3) {
    rangeMin = 0.f;
    rangeMax *= sqrtf(3.f);
  }

  for (unsigned i = 0; i < volumes.size(); i++) {
    qfeHistogram *histogram = new qfeHistogram;
    histogram->qfeSetVolume(volumes[i]);
    histogram->qfeSetNrOfBins(64);
    histogram->qfeSetRange(rangeMin, rangeMax);
    histogram->qfeUpdate();
    this->histograms.push_back(histogram);

    // Update progress bar
    cd.stepSize = MAX(0,(int)(100.0/(int)volumes.size()));
    cb.Execute(cd);
  }

  // Fix rounding errors here
  cd.stepSize = 100 - ((int)volumes.size()*cd.stepSize);    
  if (cd.stepSize > 0) cb.Execute(cd);
}

//----------------------------------------------------------------------------
// event filter implementation
bool qtMainWindow::eventFilter(QObject* watched, QEvent* event)
{
  int position;
  if (event->type() == QEvent::KeyPress )
  {
    QKeyEvent* keyEvent = (QKeyEvent*)event;
    if (keyEvent->key()==Qt::Key_Greater || keyEvent->key()==Qt::Key_Period)
    {      
      position = (this->UI->horizontalSliderTime->sliderPosition()+1) % (this->UI->horizontalSliderTime->maximum()+1) ;
     
      this->UI->horizontalSliderTime->setSliderPosition(position);
    }
    else if (keyEvent->key()==Qt::Key_Less || keyEvent->key()==Qt::Key_Comma)
    {      
      position = this->UI->horizontalSliderTime->sliderPosition()-1;
      if(position < 0) position = this->UI->horizontalSliderTime->maximum();

      this->UI->horizontalSliderTime->setSliderPosition(position);
    }
    if (keyEvent->key()==Qt::Key_Space)
    {
      this->guiAnimationStartStop();
    }

  }
  return QMainWindow::eventFilter(watched, event);
}

//----------------------------------------------------------------------------
string qtMainWindow::guiGetVisualLabel(qfeVisual *visual)
{
  string label;
  int    visualType;

  visual->qfeGetVisualType(visualType);  

  switch(visualType)
  {          
    case visualDefault              : label = "Default";
                                      break;
    case visualReformat             : label = "Multi Planar Reformat";
                                      break;      
    case visualMaximum              : label = "Maximum Intensity Projection";
                                      break;
    case visualRaycast              : label = "Direct Volume Rendering";
                                      break;
    case visualRaycastVector        : label = "Vector Field Raycasting";
                                      break;
    case visualReformatFixed        : label = "Multi Planar Reformat - Fixed slice";
                                      break;    
    case visualReformatOrtho        : label = "Multi Planar Reformat - Orthogonal";
                                      break;
    case visualReformatOblique      : label = "Multi Planar Reformat - Oblique";
                                      break;
    case visualReformatObliqueProbe : label = "Multi Planar Reformat - Probe";
                                      break;
    case visualReformatUltrasound   : label = "Multi Planar Reformat - Ultrasound";
                                      break;    
    case visualPlanesArrowHeads     : label = "Multi Planar Reformat - Arrow heads";
                                      break;
    case visualAnatomyIllustrative  : label = "Anatomical Context";
                                      break;
    case visualFlowLinesDepr        : label = "Flow Lines";
                                      break;
    case visualFlowLines            : label = "Flow Lines";
                                      break;
    case visualFlowParticles        : label = "Flow Particle Trace";
                                      break;
    case visualFlowSurfaces         : label = "Flow Surfaces";
                                      break;
    case visualFlowPlanes           : label = "Flow Planes";
                                      break;
    case visualFlowTrails           : label = "Flow Arrow Trails";
                                      break;
    case visualFlowProbe2D          : label = "Flow Probe 2D";
                                      break;
    case visualFlowProbe3D          : label = "Flow Probe 3D";
                                      break;
    case visualAides                : label = "Support";
                                      break;
    case visualFlowClustering       : label = "Flow Clustering";
                                      break;
    case visualFlowClusterArrows    : label = "Flow Cluster Arrows";
                                      break;
    case visualFlowPatternMatching  : label = "Pattern Matching Lines";
                                      break;   
   	case visualFluid			    : label = "Fluid Simulation";
                                      break;
	case visualInkVis			    : label = "Ink visualization";
                                      break;
	case visualInkVisSecondary	    : label = "Ink visualization 2";
                                      break;
    case visualSimParticles         : label = "Simulation Particles";
                                      break;
    case visualSimLinesComparison   : label = "Simulation Lines Comparison";
                                      break;
    case visualSimBoundaryComparison: label = "Simulation Boundary Comparison";
                                      break;
    case visualFilterType: label = "Noise Filter";
      break;
    case visualCEVType: label = "Cardiac Exploration";
      break;
    case visualSeedEllipsoidType: label = "Clipped Ellipsoid";
      break;
	case visualSeedVolumesType: label = "Seed Volumes";
      break;
    case visualPlaneOrthoViewType: label = "View-orthogonal Plane";
      break;
    case visualFeatureSeedingType: label = "Feature-based Seeding";
      break;
    case visualVolumeOfInterest: label = "Volume of Interest";
      break;
    case visualProbabilityDVR: label = "Probability DVR";
      break;
	case visualDataAssimilation	: label = "Data Assimilation";
                                      break;
	case visualPostProcess		    : label = "Post Processing";
                                      break;
    case visualNone: 
    default                         : label = "Unknown";
                                      break;
  }

  return label;
}

qfeStudy *qtMainWindow::getStudy()
{
  qfePatient *patient = nullptr;
  kernel->DataManager->qfeGetPatient(&patient);  
  qfeStudy *study = nullptr;
  if (patient)
    study = patient->study;
  return study;
}

std::vector<qfeHistogram*> qtMainWindow::getHistograms(const qfeVolumeSeries &volumes)
{
  qfeCallData           cd;
  qtProgressBarCallBack cb;

  cb.SetProgressBar(progressBar);
  cb.SetStatusBar(statusBar());

  cd.message  = "Computing histograms..."; 
  cd.stepSize = 100;

  // Calculate range of values over all volumes
  float rangeMin = 1e30f;
  float rangeMax = -1e30f;
  for (unsigned i = 0; i < volumes.size(); i++) {
    float vl, vu;
    volumes[i]->qfeGetVolumeValueDomain(vl, vu);
    if (vl < rangeMin) rangeMin = vl;
    if (vu > rangeMax) rangeMax = vu;
  }

  // Calculate histograms
  std::vector<qfeHistogram*> histograms;
  for (unsigned i = 0; i < volumes.size(); i++) {
    qfeHistogram *histogram = new qfeHistogram;
    histogram->qfeSetVolume(volumes[i]);
    histogram->qfeSetNrOfBins(64);
    histogram->qfeSetRange(rangeMin, rangeMax);
    histogram->qfeUpdate();
    histograms.push_back(histogram);

    // Update progress bar
    cd.stepSize = MAX(0,(int)(100.0/(int)volumes.size()));
    cb.Execute(cd);
  }

  // Fix rounding errors here
  cd.stepSize = 100 - ((int)volumes.size()*cd.stepSize);    
  if (cd.stepSize > 0) cb.Execute(cd);

  return histograms;
}