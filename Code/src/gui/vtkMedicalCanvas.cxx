#include <vtkMedicalCanvas.h>
#include <vtkSubCanvas.h>

#include <vtkObjectFactory.h>
#include <vtkRenderer.h>
#include <vtkInteractorStyleImage.h>
#include <vtkInteractorStyleTrackballCamera.h>

#include <assert.h>

vtkCxxRevisionMacro( vtkMedicalCanvas, "$Revision: 1.0 $");
vtkStandardNewMacro( vtkMedicalCanvas );

/////////////////////////////////////////////////////////////////
vtkMedicalCanvas::vtkMedicalCanvas() : vtkCanvas()
{
	this->Canvas3D = vtkSubCanvas::New();

	vtkInteractorStyleTrackballCamera * style = vtkInteractorStyleTrackballCamera::New();
	this->Canvas3D->SetInteractorStyle( style );  
	style->Delete();
	style = 0;

	//this->Canvas3D->GetRenderer()->SetBackground ( 0.8, 0.8, 0.8 );
  this->Canvas3D->GetRenderer()->SetBackground ( 0.0, 0.0, 0.0 );
	this->Canvas3D->GetRenderer()->GradientBackgroundOff();

	this->AddSubCanvas( this->Canvas3D );

	for( int i = 0; i < 3; ++i )
	{
		this->Canvas2D[i] = vtkSubCanvas::New();

		vtkInteractorStyleImage * style = vtkInteractorStyleImage::New();
		this->Canvas2D[i]->SetInteractorStyle( style );
		style->Delete();
		style = 0;

    this->Canvas2D[i]->GetRenderer()->SetBackground( 0.0, 0.0, 0.0 );
    /*
		this->Canvas2D[i]->GetRenderer()->SetBackground( 0.0, 0.0, 0.0 );
		if( i == 0 )
		this->Canvas2D[i]->GetRenderer()->SetBackground2( 1.0, 0.0, 0.0 );
		if( i == 1 )
		this->Canvas2D[i]->GetRenderer()->SetBackground2( 0.0, 1.0, 0.0 );
		if( i == 2 )
		this->Canvas2D[i]->GetRenderer()->SetBackground2( 0.0, 0.0, 1.0 );

    this->Canvas2D[i]->GetRenderer()->GradientBackgroundOn();
    */

    this->Canvas2D[i]->GetRenderer()->GradientBackgroundOff();
		this->AddSubCanvas( this->Canvas2D[i] );
	}

	this->InitLayouts();
	this->SetLayoutToTiled();

	this->FullScreen = false;
}

/////////////////////////////////////////////////////////////////
vtkMedicalCanvas::~vtkMedicalCanvas()
{
	if( this->Canvas3D ) 
		this->Canvas3D->Delete();
	this->Canvas3D = 0;

	for( int i = 0; i < 3; ++i )
	{
		if( this->Canvas2D[i] ) 
			this->Canvas2D[i]->Delete();
		this->Canvas2D[i] = 0;
	}
}

/////////////////////////////////////////////////////////////////
vtkRenderer * vtkMedicalCanvas::GetRenderer3D()
{
	assert( this->Canvas3D != 0 );
	return this->Canvas3D->GetRenderer();
}

/////////////////////////////////////////////////////////////////
vtkSubCanvas * vtkMedicalCanvas::GetSubCanvas3D()
{
	return this->Canvas3D;
}

/////////////////////////////////////////////////////////////////
vtkSubCanvas * vtkMedicalCanvas::GetSubCanvas2D( int index )
{
	assert( index >= 0 && index <= 2 );
	return this->Canvas2D[index];
}

/////////////////////////////////////////////////////////////////
int vtkMedicalCanvas::GetSubCanvasIndex( vtkSubCanvas * canvas )
{
	for( int i = 0; i < 3; ++i )
		if( canvas == this->GetSubCanvas2D( i ) )
			return i;
	return -1;
}

/////////////////////////////////////////////////////////////////
bool vtkMedicalCanvas::IsFullScreen()
{
	return this->FullScreen;
}

/////////////////////////////////////////////////////////////////
void vtkMedicalCanvas::SetLayout( int layout )
{
	assert( layout >= 0 && layout <= 6 );
	assert( this->Canvas3D != 0 );
	assert( this->Canvas2D[0] != 0 );
	assert( this->Canvas2D[1] != 0 );
	assert( this->Canvas2D[2] != 0 );

	this->Canvas3D->SetViewport(
		this->Layouts.Layouts3D[layout][0],
		this->Layouts.Layouts3D[layout][1],
		this->Layouts.Layouts3D[layout][2], this->Layouts.Layouts3D[layout][3] );

	for( int i = 0; i < 3; ++i )
	{
		this->Canvas2D[i]->SetViewport(
			this->Layouts.Layouts2D[i][layout][0],
			this->Layouts.Layouts2D[i][layout][1],
			this->Layouts.Layouts2D[i][layout][2], this->Layouts.Layouts2D[i][layout][3] );
	}

	this->Layout = layout;
	this->FullScreen = false;
}

/////////////////////////////////////////////////////////////////
int vtkMedicalCanvas::GetLayout()
{
	return this->Layout;
}

/////////////////////////////////////////////////////////////////
void vtkMedicalCanvas::SetLayoutToOrientedLeft()
{
	this->SetLayout( LAYOUT_ORIENTED_LEFT );
}

/////////////////////////////////////////////////////////////////
void vtkMedicalCanvas::SetLayoutToOrientedLeft2()
{
	this->SetLayout( LAYOUT_ORIENTED_LEFT2 );
}

/////////////////////////////////////////////////////////////////
void vtkMedicalCanvas::SetLayoutToOrientedRight()
{
	this->SetLayout( LAYOUT_ORIENTED_RIGHT );
}

/////////////////////////////////////////////////////////////////
void vtkMedicalCanvas::SetLayoutToOrientedRight2()
{
  this->SetLayout( LAYOUT_ORIENTED_RIGHT2 );
}

/////////////////////////////////////////////////////////////////
void vtkMedicalCanvas::SetLayoutToOrientedTop()
{
	this->SetLayout( LAYOUT_ORIENTED_TOP );
}

/////////////////////////////////////////////////////////////////
void vtkMedicalCanvas::SetLayoutToOrientedBottom()
{
	this->SetLayout( LAYOUT_ORIENTED_BOTTOM );
}

/////////////////////////////////////////////////////////////////
void vtkMedicalCanvas::SetLayoutToTiled()
{
	this->SetLayout( LAYOUT_TILED );
}

/////////////////////////////////////////////////////////////////
void vtkMedicalCanvas::SetToFullScreen( vtkSubCanvas * canvas )
{
	assert( canvas != 0 );
	assert( this->Canvas3D != 0 );
	assert( this->Canvas2D[0] != 0 );
	assert( this->Canvas2D[1] != 0 );
	assert( this->Canvas2D[2] != 0 );

	bool fullScreen = false;

	this->Canvas3D->SetViewport( 0.0, 0.0, 0.0, 0.0 );
	for( int i = 0; i < 3; ++i )
		this->Canvas2D[i]->SetViewport( 0.0, 0.0, 0.0, 0.0 );

	if( canvas == this->Canvas3D )
	{
		this->Canvas3D->SetViewport( 0.0, 0.0, 1.0, 1.0 );
		fullScreen = true;
	}
	else
	{
		for( int i = 0; i < 3; ++i )
		{
			if( canvas == this->Canvas2D[i] )
			{
				this->Canvas2D[i]->SetViewport( 0.0, 0.0, 1.0, 1.0 );
				fullScreen = true;
				break;
			}
		}
	}

	assert( fullScreen ); 
	this->FullScreen = fullScreen;
}

/////////////////////////////////////////////////////////////////
void vtkMedicalCanvas::InitLayouts()
{
	// 3D

	this->Layouts.Layouts3D[0][0] = 1.0 / 3.0;
	this->Layouts.Layouts3D[0][1] = 0.0;
	this->Layouts.Layouts3D[0][2] = 1.0;
	this->Layouts.Layouts3D[0][3] = 1.0;

	this->Layouts.Layouts3D[1][0] = 0.0;
	this->Layouts.Layouts3D[1][1] = 0.0;
	this->Layouts.Layouts3D[1][2] = 2.0 / 3.0;
	this->Layouts.Layouts3D[1][3] = 1.0;

	this->Layouts.Layouts3D[2][0] = 0.0;
	this->Layouts.Layouts3D[2][1] = 0.0;
	this->Layouts.Layouts3D[2][2] = 1.0;
	this->Layouts.Layouts3D[2][3] = 2.0 / 3.0;

	this->Layouts.Layouts3D[3][0] = 0.0;
	this->Layouts.Layouts3D[3][1] = 1.0 / 3.0;
	this->Layouts.Layouts3D[3][2] = 1.0;
	this->Layouts.Layouts3D[3][3] = 1.0;

  this->Layouts.Layouts3D[4][0] = 0.5;
	this->Layouts.Layouts3D[4][1] = 0.0;
	this->Layouts.Layouts3D[4][2] = 1.0;
	this->Layouts.Layouts3D[4][3] = 0.5;

  this->Layouts.Layouts3D[5][0] = 1.0 / 3.0;
	this->Layouts.Layouts3D[5][1] = 0.0;
	this->Layouts.Layouts3D[5][2] = 1.0;
	this->Layouts.Layouts3D[5][3] = 1.0;

  this->Layouts.Layouts3D[6][0] = 0.0;
	this->Layouts.Layouts3D[6][1] = 0.0;
	this->Layouts.Layouts3D[6][2] = 2.0 / 3.0;
	this->Layouts.Layouts3D[6][3] = 1.0;

	// SAGITTAL

	this->Layouts.Layouts2D[0][0][0] = 0.0;
	this->Layouts.Layouts2D[0][0][1] = 2.0 / 3.0;
	this->Layouts.Layouts2D[0][0][2] = 1.0 / 3.0;
	this->Layouts.Layouts2D[0][0][3] = 1.0;

	this->Layouts.Layouts2D[0][1][0] = 2.0 / 3.0;
	this->Layouts.Layouts2D[0][1][1] = 2.0 / 3.0;
	this->Layouts.Layouts2D[0][1][2] = 1.0;
	this->Layouts.Layouts2D[0][1][3] = 1.0;

	this->Layouts.Layouts2D[0][2][0] = 0.0;
	this->Layouts.Layouts2D[0][2][1] = 2.0 / 3.0;
	this->Layouts.Layouts2D[0][2][2] = 1.0 / 3.0;
	this->Layouts.Layouts2D[0][2][3] = 1.0;

	this->Layouts.Layouts2D[0][3][0] = 0.0;
	this->Layouts.Layouts2D[0][3][1] = 0.0;
	this->Layouts.Layouts2D[0][3][2] = 1.0 / 3.0;
	this->Layouts.Layouts2D[0][3][3] = 1.0 / 3.0;

	this->Layouts.Layouts2D[0][4][0] = 0.0;
	this->Layouts.Layouts2D[0][4][1] = 0.5;
	this->Layouts.Layouts2D[0][4][2] = 0.5;
	this->Layouts.Layouts2D[0][4][3] = 1.0;

  this->Layouts.Layouts2D[0][5][0] = 0.0;
	this->Layouts.Layouts2D[0][5][1] = 0.5;
	this->Layouts.Layouts2D[0][5][2] = 1.0 / 3.0;
	this->Layouts.Layouts2D[0][5][3] = 1.0;

  this->Layouts.Layouts2D[0][6][0] = 2.0 / 3.0;
	this->Layouts.Layouts2D[0][6][1] = 0.5;
	this->Layouts.Layouts2D[0][6][2] = 1.0;
	this->Layouts.Layouts2D[0][6][3] = 1.0;

	// CORONAL

	this->Layouts.Layouts2D[1][0][0] = 0.0;
	this->Layouts.Layouts2D[1][0][1] = 1.0 / 3.0;
	this->Layouts.Layouts2D[1][0][2] = 1.0 / 3.0;
	this->Layouts.Layouts2D[1][0][3] = 2.0 / 3.0;

	this->Layouts.Layouts2D[1][1][0] = 2.0 / 3.0;
	this->Layouts.Layouts2D[1][1][1] = 1.0 / 3.0;
	this->Layouts.Layouts2D[1][1][2] = 1.0;
	this->Layouts.Layouts2D[1][1][3] = 2.0 / 3.0;

	this->Layouts.Layouts2D[1][2][0] = 1.0 / 3.0;
	this->Layouts.Layouts2D[1][2][1] = 2.0 / 3.0;
	this->Layouts.Layouts2D[1][2][2] = 2.0 / 3.0;
	this->Layouts.Layouts2D[1][2][3] = 1.0;

	this->Layouts.Layouts2D[1][3][0] = 1.0 / 3.0;
	this->Layouts.Layouts2D[1][3][1] = 0.0;
	this->Layouts.Layouts2D[1][3][2] = 2.0 / 3.0;
	this->Layouts.Layouts2D[1][3][3] = 1.0 / 3.0;

	this->Layouts.Layouts2D[1][4][0] = 0.5;
	this->Layouts.Layouts2D[1][4][1] = 0.5;
	this->Layouts.Layouts2D[1][4][2] = 1.0;
	this->Layouts.Layouts2D[1][4][3] = 1.0;

  this->Layouts.Layouts2D[1][5][0] = 0.0;
	this->Layouts.Layouts2D[1][5][1] = 0.0;
	this->Layouts.Layouts2D[1][5][2] = 1.0 / 3.0;
	this->Layouts.Layouts2D[1][5][3] = 0.5;
  
  this->Layouts.Layouts2D[1][6][0] = 2.0 / 3.0;
	this->Layouts.Layouts2D[1][6][1] = 0.0;
	this->Layouts.Layouts2D[1][6][2] = 1.0;
	this->Layouts.Layouts2D[1][6][3] = 0.5;

	// AXIAL

	this->Layouts.Layouts2D[2][0][0] = 0.0;
	this->Layouts.Layouts2D[2][0][1] = 0.0;
	this->Layouts.Layouts2D[2][0][2] = 1.0 / 3.0;
	this->Layouts.Layouts2D[2][0][3] = 1.0 / 3.0;

	this->Layouts.Layouts2D[2][1][0] = 2.0 / 3.0;
	this->Layouts.Layouts2D[2][1][1] = 0.0;
	this->Layouts.Layouts2D[2][1][2] = 1.0;
	this->Layouts.Layouts2D[2][1][3] = 1.0 / 3.0;

	this->Layouts.Layouts2D[2][2][0] = 2.0 / 3.0;
	this->Layouts.Layouts2D[2][2][1] = 2.0 / 3.0;
	this->Layouts.Layouts2D[2][2][2] = 1.0;
	this->Layouts.Layouts2D[2][2][3] = 1.0;

	this->Layouts.Layouts2D[2][3][0] = 2.0 / 3.0;
	this->Layouts.Layouts2D[2][3][1] = 0.0;
	this->Layouts.Layouts2D[2][3][2] = 1.0;
	this->Layouts.Layouts2D[2][3][3] = 1.0 / 3.0;

	this->Layouts.Layouts2D[2][4][0] = 0.0;
	this->Layouts.Layouts2D[2][4][1] = 0.0;
	this->Layouts.Layouts2D[2][4][2] = 0.5;
	this->Layouts.Layouts2D[2][4][3] = 0.5;

  this->Layouts.Layouts2D[2][5][0] = 0.0;
	this->Layouts.Layouts2D[2][5][1] = 0.0;
	this->Layouts.Layouts2D[2][5][2] = 0.0;
	this->Layouts.Layouts2D[2][5][3] = 0.0;
  
  this->Layouts.Layouts2D[2][6][0] = 0.0;
	this->Layouts.Layouts2D[2][6][1] = 0.0;
	this->Layouts.Layouts2D[2][6][2] = 0.0;
	this->Layouts.Layouts2D[2][6][3] = 0.0;
}