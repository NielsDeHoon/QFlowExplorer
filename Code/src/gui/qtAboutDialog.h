#pragma once

#include "ui_qtAboutDialog.h"
using namespace Ui;

class qtAboutDialog : public QDialog
{
	Q_OBJECT
public:
	qtAboutDialog();
	
public slots:
  void on_pushButtonClose_clicked() {this->close();};

protected:
	Ui_DialogInfo *UI;
};


