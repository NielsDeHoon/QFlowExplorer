#ifndef __vtkCanvasInteractorStyle_h
#define __vtkCanvasInteractorStyle_h

#include <vtkInteractorStyle.h>

class vtkRenderWindowInteractor;
class vtkCanvas;

class vtkCanvasInteractorStyle : public vtkInteractorStyle
{
public:

	static vtkCanvasInteractorStyle * New();
	vtkTypeRevisionMacro( vtkCanvasInteractorStyle, vtkInteractorStyle );

	virtual void SetCanvas( vtkCanvas * canvas );
	virtual vtkCanvas * GetCanvas();

protected:

	vtkCanvasInteractorStyle();
	virtual ~vtkCanvasInteractorStyle();

private:

	// Hide this method from public interface so the interactor
	// can only be set through the vtkCanvas

	virtual void SetInteractor( vtkRenderWindowInteractor * interactor );

	vtkCanvas * Canvas;
};

#endif