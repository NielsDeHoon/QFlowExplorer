#ifndef __vtkSubCanvasCollection_h
#define __vtkSubCanvasCollection_h

#include <vtkCollection.h>

class vtkSubCanvas;

class vtkSubCanvasCollection : public vtkCollection
{
public:

	static vtkSubCanvasCollection * New();
	vtkTypeRevisionMacro( vtkSubCanvasCollection, vtkCollection );

	void AddItem( vtkSubCanvas * canvas );
	void RemoveItem( vtkSubCanvas * canvas );

	vtkSubCanvas * GetNextItem();
	vtkSubCanvas * GetItem( int index );

protected:

	vtkSubCanvasCollection();
	virtual ~vtkSubCanvasCollection();
};

#endif