#ifndef __vtkCanvasInteractorStyleForward_h
#define __vtkCanvasInteractorStyleForward_h

#include <vtkCanvasInteractorStyle.h>

class vtkCanvas;

class vtkCanvasInteractorStyleForward : public vtkCanvasInteractorStyle
{
public:

	static vtkCanvasInteractorStyleForward * New();
	vtkTypeRevisionMacro( vtkCanvasInteractorStyleForward, vtkCanvasInteractorStyle );

	virtual void OnLeftButtonDown();
	virtual void OnRightButtonDown();
	virtual void OnMiddleButtonDown();

	virtual void SetCanvas( vtkCanvas * canvas );

protected:

	vtkCanvasInteractorStyleForward();
	virtual ~vtkCanvasInteractorStyleForward();

	virtual void OnButtonDown();
};

#endif