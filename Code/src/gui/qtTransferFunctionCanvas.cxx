#include "qtTransferFunctionCanvas.h"

const qtColorXYZ whiteD50 = { 0.9642, 1.0, 0.8249 };
const qtColorXYZ whiteD65 = { 0.9501548, 1.0, 1.0882598 };

qtTransferFunctionCanvas::qtTransferFunctionCanvas(QWidget *parent) : QWidget(parent)
{
	PointSize            =  4;
	oSelected            = -1;
  gSelected            = -1;
  cSelected            = -1;
  crSelected           = -1;

  oEditorWidth         =  0;
  oEditorHeight        =  0;
  oEditorOrigin[0]     =  0;
  oEditorOrigin[1]     =  0;
  oEditorMargin[0]     = 30; // left
  oEditorMargin[1]     = 30; // right
  oEditorMargin[2]     =  5; // top
  oEditorMargin[3]     = 10; // bottom
  gEditorWidth         =  0;
  gEditorHeight        =  0;
  gEditorOrigin[0]     =  0;
  gEditorOrigin[1]     =  0;
  gEditorMargin[0]     = 30; // left
  gEditorMargin[1]     = 30; // right
  gEditorMargin[2]     =  5; // top
  gEditorMargin[3]     = 10; // bottom
  cEditorWidth         =  0;
  cEditorHeight        = 20;
  cEditorOrigin[0]     =  0;
  cEditorOrigin[1]     =  0;
  cEditorMargin[0]     = 30; // left
  cEditorMargin[1]     = 30; // right
  cEditorMargin[2]     = 30; // top
  cEditorMargin[3]     = 10; // bottom

  interpolation        = qtColorInterpolationNearest;
  colorspace           = qtColorSpaceRGB;
  quantization         = 7;
  histogramscale       = 50;
  histogramscaletype   = 1;
  histogramvisible     = 1;
  constantlightness    = false;
  paddinguniform       = false;

  currentHistogramIndex     = 0;

  labelX               = "x - axis";
  labelY               = "y - axis";
  rangeX[0]            = rangeY[0] = 0.0;
  rangeX[1]            = rangeY[1] = 1.0;

  this->HistogramPoly.clear();

  this->ColorDialog = new QColorDialog();
  this->Canvas      = new QWidget();

  for (unsigned i = 0; i < Histograms.size(); i++)
    delete Histograms[i];
  this->Histograms.clear();
  this->ColorGradient.clear();
  this->ColorQuantizedGradient.clear();

  this->ColorRangePoints.clear();
  this->ColorRangePoints.push_back(0.0);
  this->ColorRangePoints.push_back(1.0);

  this->setMouseTracking(true);

  this->initTransferFunction();
}

//----------------------------------------------------------------------------
qtTransferFunctionCanvas::~qtTransferFunctionCanvas()
{
  delete this->ColorDialog;
  delete this->Canvas;
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::addHistogram(qfeHistogram *histogram)
{
  this->Histograms.push_back(histogram);
  
  emit transferFunctionChanged();
  this->updateHistogramPoly();
  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::clearHistograms()
{  
  this->Histograms.clear();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setHistogramScale(int scale)
{
  this->histogramscale = scale;
  
  //emit transferFunctionChanged();
  this->updateHistogramPoly();
  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setHistogramScaleType(int logarithmic)
{
  this->histogramscaletype = logarithmic;
  
  //emit transferFunctionChanged();
  this->updateHistogramPoly();
  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setHistogramNumber(int current)
{
  this->currentHistogramIndex = MAX(current, 0);
  this->currentHistogramIndex = MIN(current, (int)this->Histograms.size()-1);

  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setHistogramVisibility(int value)
{
  if(value > 0)
    this->histogramvisible = 1;
  else
    this->histogramvisible = 0;

  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setColorInterpolation(int mode)
{
  this->interpolation = mode;
  emit transferFunctionChanged();
  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::getColorInterpolation(int &mode)
{
  mode = this->interpolation;  
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setColorQuantization(int steps)
{
  this->quantization = steps;
  emit transferFunctionChanged();
  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::getColorQuantization(int &steps)
{
  steps = this->quantization;
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setColorSpace(int space)
{
  this->colorspace = space;
  emit transferFunctionChanged();
  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::getColorSpace(int &space)
{
  space = this->colorspace;
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setColorLightnessConstant(bool constant)
{
  this->constantlightness = constant;
  emit transferFunctionChanged();
  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::getColorLightnessConstant(bool &constant)
{
  constant = this->constantlightness;
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setColorPadding(bool padding)
{
  this->paddinguniform = padding;
  emit transferFunctionChanged();
  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::getColorPadding(bool &padding)
{
  padding = this->paddinguniform;
  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::getTransferFunction(QVector< QPair<double, double*> > &color, QVector< QPair<double, double> > &opacity, QVector< QPair<double, double> > &gradient)
{
  color    = this->ColorPoints;
  opacity  = this->OpacityPoints;
  gradient = this->GradientPoints;
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setTransferFunction(QVector< QPair<double, double*> > color, QVector< QPair<double, double> > opacity, QVector< QPair<double, double> > &gradient)
{
  ColorPoints    = color;
  OpacityPoints  = opacity;
  GradientPoints = gradient;

  // Update range for both range points
  this->updateColorRange(0);  
  this->updateColorRange(1);  
  this->update();

  emit transferFunctionChanged();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setAxisX(QString label, double min, double max)
{
  this->labelX    = label;
  this->rangeX[0] = min;
  this->rangeX[1] = max;
  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::setAxisY(QString label, double min, double max)
{
  this->labelY    = label;
  this->rangeY[0] = min;
  this->rangeY[1] = max;
  this->update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::initTransferFunction()
{
  // store opacity and color points in internal lists. this will
  // allow easy adding/removing and sorting
  ColorPoints.clear();
  OpacityPoints.clear();
  GradientPoints.clear();

  // add opacity points
  OpacityPoints.push_back(QPair<double, double>(0.0, 1.0));
  OpacityPoints.push_back(QPair<double, double>(1.0, 1.0));

  // add opacity gradient points
  GradientPoints.push_back(QPair<double, double>(0.0, 1.0));
  GradientPoints.push_back(QPair<double, double>(1.0, 1.0));

  // add color points
  double *c1 = new double[3];
  c1[0] = 0.0;
  c1[1] = 0.0;
  c1[2] = 0.0;
  ColorPoints.push_back(QPair<double, double*>(0.0, c1));
  double *c2 = new double[3];
  c2[0] = 1.0;
  c2[1] = 1.0;
  c2[2] = 1.0;
  ColorPoints.push_back(QPair<double, double*>(1.0, c2));

  resizeEvent(NULL);
  update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::mousePressEvent(QMouseEvent *event)
{
  bool insideO = insideOpacityEditor(event->pos(), true, true);
  bool insideC = insideColorEditor(event->pos(), true, true);
  bool insideG = insideGradientEditor(event->pos(), true, true);

  // return quietly if user clicked outside drawing area
	if ( ColorPoints.size() == 0 || OpacityPoints.size() == 0 || GradientPoints.size() == 0 ||
       !(insideO || insideC || insideG) )
	{
		return;
	}

  if(insideO) oSelected = selectOpacityPoint(event->pos());

  if(insideG) gSelected = selectGradientPoint(event->pos());

  if(insideC)
  {
    cSelected  = selectColorPoint(event->pos());
    crSelected = selectColorRangePoint(event->pos());
  }

	update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::mouseReleaseEvent(QMouseEvent *event)
{
  // Change a color with the right mouse button
  if((event->button() == Qt::RightButton) && (cSelected != -1))
  {
    // Cannot erase outer most points
    if ((cSelected != 0) && (cSelected != ColorPoints.size()-1))
    {
      ColorPoints.erase(ColorPoints.begin() + cSelected);
    }   
  }

	qSort(ColorPoints.begin(),    ColorPoints.end());
  qSort(OpacityPoints.begin(),  OpacityPoints.end());
  qSort(GradientPoints.begin(), GradientPoints.end());

  oSelected  = -1;
  gSelected  = -1;
  cSelected  = -1;
  crSelected = -1;

	// notify render window that TF has changed
	emit transferFunctionChanged();
	update();

}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::mouseDoubleClickEvent(QMouseEvent *event)
{
  bool insideO = insideOpacityEditor(event->pos(), true, true);
  bool insideG = insideGradientEditor(event->pos(), true, true);
  int  insideC = insideColorEditor(event->pos(), true, true);
  
  // return quietly if user clicked outside drawing area
  if (ColorPoints.size() == 0 || OpacityPoints.size() == 0 || OpacityPoints.size() == 0 || !(insideO || insideC || insideG))
  {
    return;
  }

  // return quietly if user clicked with right mouse button
  if(event->button() == Qt::RightButton) return;
  
  // double click inside the opacity editor
  if(insideO)
  {
    int selectedO = selectOpacityPoint(event->pos());

    // Add new opacity point
    if(selectedO == -1)
    {
      QPointF p(MAX(0.0,       ((event->x() - oEditorOrigin[0]) / (double)oEditorWidth)), 
                MIN(1.0, 1.0 - ((event->y() - oEditorOrigin[1]) / (double) (oEditorHeight+1))));
     
      OpacityPoints.push_back(QPair<double, double>(p.x(), p.y()));      
    }
    else
    {
      // Do not allow to delete outermost points
      if ((selectedO != 0) && (selectedO != OpacityPoints.size()-1 ))
      {
        OpacityPoints.erase(OpacityPoints.begin() + selectedO);
      }
    }
    qSort(OpacityPoints.begin(), OpacityPoints.end());
  }

  // double click inside the opacity editor
  if(insideG)
  {
    int selectedG = selectGradientPoint(event->pos());

    // Add new opacity point
    if(selectedG == -1)
    {
      QPointF p(MAX(0.0,       ((event->x() - gEditorOrigin[0]) / (double) gEditorWidth)), 
                MIN(1.0, 1.0 - ((event->y() - gEditorOrigin[1]) / (double) (gEditorHeight+1))));
     
      GradientPoints.push_back(QPair<double, double>(p.x(), p.y()));      
    }
    else
    {
      // Do not allow to delete outermost points
      if ((selectedG != 0) && (selectedG != GradientPoints.size()-1 ))
      {
        GradientPoints.erase(GradientPoints.begin() + selectedG);
      }
    }
    qSort(GradientPoints.begin(), GradientPoints.end());
  }

  // Double click inside the color editor
  if(insideC)  
  {    
    this->cSelected  = selectColorPoint(event->pos());
    this->crSelected = selectColorRangePoint(event->pos());

    // Add new color point
    if((insideC == 1) && (cSelected == -1))
    {
      QColor color = ColorDialog->getColor(Qt::white, this);      

      if(color.isValid())
      {
        double *c = new double[3];
        color.getRgbF(&c[0],&c[1], &c[2]);
       
        double x = (event->x() - oEditorOrigin[0]) / (double)oEditorWidth;

        ColorPoints.push_back(QPair<double, double*>(x, c)); 
      }
      
    }
    // Update color point
    else
    if((insideC == 1) && (cSelected >= 0))
    {
      QColor color = ColorDialog->getColor(Qt::white, this);      

      if(color.isValid())
      {
        double *c = new double[3];
        color.getRgbF(&c[0],&c[1], &c[2]);

        ColorPoints[cSelected].second = c;
      }
    }

    qSort(ColorPoints.begin(), ColorPoints.end());
  }

  if(insideO || insideC || insideG)
    emit transferFunctionChanged();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::mouseMoveEvent(QMouseEvent *event)
{
  bool insideO = insideOpacityEditor(event->pos(), false, false);
  bool insideG = insideGradientEditor(event->pos(), false, false);
  bool insideC = insideColorEditor(event->pos(), false, true);

  // Output the current value
  if(insideO && (this->Histograms.size() > 0))
  {
    double x = event->x();

    if(x < oEditorOrigin[0])                 x = oEditorOrigin[0];
    if(x > oEditorOrigin[0] + oEditorWidth)  x = oEditorOrigin[0] + oEditorWidth;

    qfeHistogram *currentHistogram = Histograms[currentHistogramIndex];
    float rangeMin, rangeMax;
    currentHistogram->qfeGetRange(rangeMin, rangeMax);

    double t = (x - oEditorOrigin[0]) / oEditorWidth;
    double value = rangeMin + t * (rangeMax - rangeMin);

    unsigned binIndex = currentHistogram->qfeGetBin(value);
    unsigned count = currentHistogram->qfeGetCount(binIndex);

    // \todo refresh also when update() is called, not only on mouse move
    labelCount.sprintf("%4d - # %7d", (int)value, count);       
  }

  // Move opacity points
  if(oSelected != -1)
  {
    // make sure we keep within the editor range
    double currentX = event->x();
    double currentY = event->y();

    if(currentX < oEditorOrigin[0])                 currentX = oEditorOrigin[0];
    if(currentX > oEditorOrigin[0] + oEditorWidth)  currentX = oEditorOrigin[0] + oEditorWidth;
    if(currentY < oEditorOrigin[1])                 currentY = oEditorOrigin[1];
    if(currentY > oEditorOrigin[1] + oEditorHeight) currentY = oEditorOrigin[1] + oEditorHeight;

    // get value and opacity associated with user point
    double x =       ((currentX - oEditorOrigin[0]) / (double) oEditorWidth);
    double y = 1.0 - ((currentY - oEditorOrigin[1]) / (double) oEditorHeight);

    // update selected opacity
    QPair<double, double> oPoint = OpacityPoints.at(oSelected);

    // fix outermost points
    if ((oSelected != 0) && (oSelected != OpacityPoints.size()-1 ))
    {
      // do not allow overlapping points
      if(x < OpacityPoints.at(oSelected-1).first) x = OpacityPoints.at(oSelected-1).first;
      if(x > OpacityPoints.at(oSelected+1).first) x = OpacityPoints.at(oSelected+1).first;

      oPoint.first  = x;
    }    
    oPoint.second = y;
    
    OpacityPoints[oSelected] = oPoint;
  }

   // Move gradient points
  if(gSelected != -1)
  {
    // make sure we keep within the editor range
    double currentX = event->x();
    double currentY = event->y();

    if(currentX < gEditorOrigin[0])                 currentX = gEditorOrigin[0];
    if(currentX > gEditorOrigin[0] + gEditorWidth)  currentX = gEditorOrigin[0] + gEditorWidth;
    if(currentY < gEditorOrigin[1])                 currentY = gEditorOrigin[1];
    if(currentY > gEditorOrigin[1] + gEditorHeight) currentY = gEditorOrigin[1] + gEditorHeight;

    // get value and opacity associated with user point
    double x =       ((currentX - gEditorOrigin[0]) / (double) gEditorWidth);
    double y = 1.0 - ((currentY - gEditorOrigin[1]) / (double) gEditorHeight);

    // update selected opacity
    QPair<double, double> gPoint = GradientPoints.at(gSelected);

    // fix outermost points
    if ((gSelected != 0) && (gSelected != GradientPoints.size()-1 ))
    {
      // do not allow overlapping points
      if(x < GradientPoints.at(gSelected-1).first) x = GradientPoints.at(gSelected-1).first;
      if(x > GradientPoints.at(gSelected+1).first) x = GradientPoints.at(gSelected+1).first;

      gPoint.first  = x;
    }    
    gPoint.second = y;
    
    GradientPoints[gSelected] = gPoint;
  }

  // Move color points
  if(cSelected != -1)
  {
    // make sure we keep within the editor range
    double currentX = event->x();

    if(currentX < cEditorOrigin[0])                   currentX = cEditorOrigin[0];
    if(currentX > cEditorOrigin[0] + cEditorWidth -1) currentX = cEditorOrigin[0] + cEditorWidth - 1;

    // get value and opacity associated with user point
    double x = ((currentX - cEditorOrigin[0]) / (double)cEditorWidth);

    // update selected color point, don't allow outer most point to be moved    
    if ((cSelected != 0) && (cSelected != ColorPoints.size()-1 ))
    {
      ColorPoints[cSelected].first  = x;
    }    
  }

  // Move range points
  if(crSelected != -1)
  {
    // make sure we keep within the editor range
    double currentX = event->x();

    if(currentX < cEditorOrigin[0])                   currentX = cEditorOrigin[0];
    if(currentX > cEditorOrigin[0] + cEditorWidth -1) currentX = cEditorOrigin[0] + cEditorWidth - 1;

    // get value and opacity associated with user point
    double x = ((currentX - cEditorOrigin[0]) / (double)(cEditorWidth-1));

    // check if there is sufficient distance between the range points
    if(abs(x-ColorRangePoints[1-crSelected]) < (int)ColorPoints.size()/(double)cEditorWidth)
      x = ColorRangePoints[1-crSelected]+ (2*crSelected-1)*((int)ColorPoints.size()/(double)cEditorWidth);

    // update selected color range point 
    if(crSelected == 0 && x > ColorRangePoints[1] || crSelected == 1 && x < ColorRangePoints[0]) 
      x = ColorRangePoints[crSelected];

    ColorRangePoints[crSelected] = x;

    updateColorRange(crSelected);
  }

	update();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::resizeEvent(QResizeEvent *event)
{  
    // Assumption that cEditorHeight is set and fixed
    oEditorWidth     = (int)(width()  - oEditorMargin[0] - oEditorMargin[1]);
    oEditorHeight    = (int)(((height() - cEditorHeight - cEditorMargin[2] - cEditorMargin[3])/2.0f) - oEditorMargin[2] - oEditorMargin[3]);
    oEditorHeight    = max(0, oEditorHeight);
    oEditorOrigin[0] = (int) oEditorMargin[0];
    oEditorOrigin[1] = (int) oEditorMargin[2];

    cEditorWidth     = (int)(width()  - cEditorMargin[0] - cEditorMargin[1]);
    cEditorOrigin[0] = (int) cEditorMargin[0];
    //cEditorOrigin[1] = (int)(height() - gEditorHeight - 2.0 * gEditorMargin[1] - cEditorHeight - cEditorMargin[1]);
    cEditorOrigin[1] = (int)(oEditorHeight + oEditorMargin[2] +  oEditorMargin[3] + cEditorMargin[2]);

    gEditorWidth     = (int)(width()  - gEditorMargin[0] - gEditorMargin[1]);
    gEditorHeight    = (int)(((height() - cEditorHeight - cEditorMargin[2] - cEditorMargin[3])/2.0f) - gEditorMargin[2] - gEditorMargin[3]);
    gEditorHeight    = max(0, gEditorHeight);
    gEditorOrigin[0] = (int) gEditorMargin[0];
    //gEditorOrigin[1] = (int)(height() - gEditorHeight - gEditorMargin[1]);
    gEditorOrigin[1] = (int)(oEditorHeight + oEditorMargin[2] + oEditorMargin[3] + cEditorHeight + cEditorMargin[2] + cEditorMargin[3]);

    this->updateHistogramPoly();
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintEvent(QPaintEvent *event)
{
	QPainter p(this);
  p.setRenderHint(QPainter::Antialiasing, true);
  p.setRenderHint(QPainter::HighQualityAntialiasing, true);
  
  // draw border	
  p.setPen(QPen(Qt::black));
	p.fillRect(0, 0, width(), height(), QBrush(QColor(224, 223, 227, 0)));  
 
  // do we have a transfer function?
	if ( ColorPoints.size() == 0)
		return;

  // draw checkerboard pattern
  this->paintCheckerboard(&p, oEditorOrigin, oEditorWidth, oEditorHeight);
  this->paintCheckerboard(&p, gEditorOrigin, gEditorWidth, gEditorHeight);

  // draw the axis
  this->paintAxis(&p, oEditorOrigin, oEditorWidth, oEditorHeight);
  this->paintAxis(&p, gEditorOrigin, gEditorWidth, gEditorHeight);

  // draw the labels
  this->paintLabelX(&p, oEditorOrigin, oEditorWidth, oEditorHeight, this->labelX);
  this->paintLabelY(&p, oEditorOrigin, oEditorWidth, oEditorHeight, this->labelY);
  this->paintLabelCount(&p);

  this->paintLabelX(&p, gEditorOrigin, gEditorWidth, gEditorHeight, QString("gradient"));
  this->paintLabelY(&p, gEditorOrigin, gEditorWidth, gEditorHeight, QString("opacity"));

  // draw the color and opacity map
  //this->paintColorOpacityMap(&p);

  // draw fixed color bar
  this->paintColorBar(&p, cEditorOrigin);
	
  // draw the Histogram
  if(this->histogramvisible == 1)
    this->paintHistogram(&p);

  // draw opacity transfer function
  this->paintOpacityPoints(&p, oEditorOrigin, oEditorWidth, oEditorHeight, OpacityPoints);
  this->paintOpacityPoints(&p, gEditorOrigin, gEditorWidth, gEditorHeight, GradientPoints);

  // draw color transfer function
  this->paintColorPoints(&p);

  // draw the range points
  this->paintColorRangePoints(&p);
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintAxis(QPainter *p, int o[], int w, int h)
{
  p->setPen(Qt::black);

  // Vertical Axis
  // Main line
  p->drawLine(o[0] - 0, o[1], 
              o[0] - 0, o[1] + h - 1);

  // Three ticks
  for(int i = 0; i<3; i++)
  {
    p->drawLine(o[0] + 2 , o[1] + (int)((h-1) * (i/2.0)), 
                o[0] - 2 , o[1] + (int)((h-1) * (i/2.0)));     
  }

  QString maxY, halfY, minY;

  maxY.sprintf("%.1f" , this->rangeY[1]);
  halfY.sprintf("%.1f", this->rangeY[1]/2.0);
  minY.sprintf("%.1f" , this->rangeY[0]);

  // Text
  p->drawText(o[0] - 23, o[1] + 4, maxY);
  p->drawText(o[0] - 23, o[1] + 4 + (int)(h / 2.0), halfY);
  p->drawText(o[0] - 23, o[1] + 4 + (int) h       , minY);

  // Horizontal Axis
  // Main line
  p->drawLine(o[0]        , o[1] + h - 1, 
              o[0] + w - 1, o[1] + h - 1);

  // Ticks
  QString tickText;
  int  n = 2;
  for(int i = 0; i<n; i++)
  {
    p->drawLine(o[0] + (int)((w-1) * (i/(double)(n-1))), o[1] + h - 1 - 2, 
                o[0] + (int)((w-1) * (i/(double)(n-1))), o[1] + h - 1 + 2);     

    tickText.sprintf("%.1f", (i/(double)(n-1))*(this->rangeX[1]-this->rangeX[0]) + this->rangeX[0]);

    p->drawText(o[0] + (int)((w-1) * (i/(double)(n-1))) - 8, o[1] + h - 1 + 15, tickText);
  }
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintColorBar(QPainter *p, int o[])
{
  int upperOrigin[2], lowerOrigin[2];

  lowerOrigin[0] = o[0]; 
  lowerOrigin[1] = o[1];
  upperOrigin[0] = o[0];
  upperOrigin[1] = o[1]-(cEditorHeight/2.0);

  // Draw the lower part of the color bar
  this->paintColorBarLinear(p, lowerOrigin);

  // Draw the upper part of the bar
  if(this->interpolation == qtColorInterpolationNearest)
  {
    this->paintColorBarNearest(p, upperOrigin);

    // Draw a line between the parts
    p->setPen(Qt::black);
    p->drawLine(lowerOrigin[0], lowerOrigin[1], lowerOrigin[0] + cEditorWidth, lowerOrigin[1]);
    
  }
  else
    this->paintColorBarLinear(p, upperOrigin);

}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintColorBarNearest(QPainter *p, int o[])
{
  qtColorRGB c;
  float        width, height, border;

  width  = cEditorWidth/(float)this->quantization;
  height = cEditorHeight/2.0;
  border = 1.0;

  this->computeLinearGradient(this->ColorPoints, this->ColorGradient, cEditorWidth, this->colorspace);
  this->computeQuantizedGradient(this->ColorGradient, this->ColorQuantizedGradient, this->quantization);   
  
  p->setPen(Qt::NoPen); 
  
  for ( int i = 0; i < this->ColorQuantizedGradient.size(); i++)
  {
    c = this->ColorQuantizedGradient.at(i);
  
    p->setBrush(QColor(MIN(255*c.r,255), MIN(255*c.g,255), MIN(255*c.b,255)));
    p->drawRect(o[0]+(i*width)-border, o[1], width+border, height);
  }
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintColorBarLinear(QPainter *p, int o[])
{
  qtColorRGB c;
  QPen         pen;
  float        height;

  this->computeLinearGradient(this->ColorPoints, this->ColorGradient, cEditorWidth, this->colorspace);

  height = cEditorHeight/2;

  pen.setWidth(2);

  for ( int i = 0; i < this->ColorGradient.size(); i++ )
  {
    c = this->ColorGradient.at(i);
    
    pen.setColor(QColor(MIN(255*c.r,255), MIN(255*c.g,255), MIN(255*c.b,255)));

    p->setPen(pen);
    p->drawLine(o[0]+i, o[1], o[0]+i, o[1]+height);
  }
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintColorBarLinearRgb(QPainter *p, int o[])
{
  QLinearGradient grad(QPointF(cEditorOrigin[0], 0), QPointF(cEditorOrigin[0]+cEditorWidth-1, 0));

  QVector< QPair<double, double*> >::iterator cIter = ColorPoints.begin();
  
  // Compute the transfer function color map
  int x0 = 0;
  for ( int i = 0; i < (int)ColorPoints.size(); i++ )
  {
    double *color = cIter->second;

    grad.setColorAt(qMin(1.0f, (float)cIter->first), QColor((int) MIN(255*color[0],255), (int) MIN(255*color[1],255), (int) MIN(255*color[2],255)));
    
    cIter++;
  }

  p->setBrush(grad);
  p->setPen(Qt::NoPen);
  p->drawRect(o[0], o[1], cEditorWidth, (cEditorHeight - 1)/2);
  
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintColorOpacityMap(QPainter *p)
{
  QLinearGradient grad(QPointF(0, 0), QPointF(cEditorWidth-1, 0));
  
  // Compute the transfer function color map
  QVector< QPair<double, double*> >::iterator cIter = ColorPoints.begin();
  int x0 = 0;
  for ( int i = 0; i < (int)ColorPoints.size(); i++ )
  {
    double *color = cIter->second;

    int x1 = (int) (cIter->first * cEditorWidth);
    grad.setColorAt(qMin(1.0f, (float) x1 / cEditorWidth), QColor((int) 255*color[0], (int) 255*color[1], (int) 255*color[2]));

    cIter++;
  }

  // Create the alpha channel
  QImage          alphaChannel(oEditorWidth, oEditorHeight, QImage::Format_ARGB32_Premultiplied);
  QLinearGradient alphaGradient(QPointF(0, oEditorOrigin[1]), QPointF(0, oEditorOrigin[1] + oEditorHeight-1));
  alphaGradient.setColorAt(0.0, QColor(255,255,255,0));
  alphaGradient.setColorAt(1.0, QColor(0  ,0  ,0  ,255));
  QPainter p3(&alphaChannel);
  p3.fillRect(rect(), alphaGradient);  

  // Draw the gradient and apply the alpha channel
  QImage colorMap(oEditorWidth, oEditorHeight, QImage::Format_ARGB32_Premultiplied);
  QPainter p2(&colorMap);
  p2.fillRect(rect(), grad);
  colorMap.setAlphaChannel(alphaChannel);

  p->drawImage(oEditorOrigin[0],oEditorOrigin[1],colorMap);  
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintCheckerboard(QPainter *p, int o[], int w, int h)
{
  int size =18;
  QImage image(w, h, QImage::Format_RGB32);
  image.fill(qRgb(255, 255, 255));
  for (int jj=0 ; jj<h ; ++jj)
  {
    for (int ii=0 ; ii<w ; ++ii)
    {
      if ( (((jj % size) >= 0) && ((jj % size) < (int)size/2)))
      {
        if ( (((ii % size) >= (int)size/2) && ((ii % size) < size))) 
          image.setPixel(ii, jj, qRgb(224, 224, 224));
      }
      else
      {
        if ( (((ii % size) >=  0) && ((ii % size) < int(size/2))))   
          image.setPixel(ii, jj, qRgb(224, 224, 224));
      }
    }
  }
  QRect srcRect(o[0], o[1], w - 1, h -1);

  p->drawImage(srcRect,image);
}


//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintHistogram(QPainter *p)
{
  if ( this->HistogramPoly.size() <= 0 ) return;
  if ( this->HistogramPoly[currentHistogramIndex].size() < 0 ) return;

  p->setPen(QColor(0, 0, 0));
  p->setBrush(QColor(100, 100, 100));
  p->drawPolygon(this->HistogramPoly[currentHistogramIndex]);  
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintOpacityPoints(QPainter *p, int o[], int w, int h, QVector< QPair<double, double> > points)
{
  QBrush lineBrush(QColor(192, 192, 192));
  QBrush pointBrush(QColor(255, 0, 0));

  QVector< QPair<double, double> >::iterator oIter = points.begin();

  p->setPen(QPen(QColor(60,60,60), 1.5f, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

  QPointF p1;
  double  o1;
  for ( int i = 0; i < (int)points.size(); i++ )
  {
    QPointF p2(o[0] +     (oIter->first  * w), 
               o[1] + h - (oIter->second * h));
    double  o2 = oIter->second;

    if(i==0)
    {
      p1 = p2;
      o1 = o2;
    }
    else
    {       
      p->setBrush(lineBrush);
      p->drawLine(p1, p2);
    }

    //pointBrush.setColor(QColor(255,255,255));
    pointBrush.setColor(QColor((1.0-o1)*255,(1.0-o1)*255,(1.0-o1)*255));
    p->setBrush(pointBrush);
    p->drawRoundedRect(QRect(p1.x()-PointSize, p1.y()-PointSize, 2*PointSize+1, 2*PointSize+1), PointSize/2, PointSize/2);

    p1 = p2;
    o1 = o2;
    oIter++;
  }
  
  pointBrush.setColor(QColor((1.0-o1)*255,(1.0-o1)*255,(1.0-o1)*255));
  p->setBrush(pointBrush);
  p->drawRoundedRect(QRect(p1.x()-PointSize, p1.y()-PointSize, 2*PointSize+1, 2*PointSize+1), PointSize/2, PointSize/2);
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintColorPoints(QPainter *p)
{
  QBrush lineBrush(QColor(192, 192, 192));
  QBrush pointBrush(QColor(255, 0, 0));

  QVector< QPair<double, double*>  >::iterator cIter = ColorPoints.begin();

  p->setPen(QPen(Qt::black, 1.5f, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));  

  for ( int i = 0; i < (int)this->ColorPoints.size(); i++ )
  {
    QPointF p1(cEditorOrigin[0] + (cIter->first * (cEditorWidth-1)), 
               cEditorOrigin[1] + ((int)(0.6f*cEditorHeight)-1));

    pointBrush.setColor(QColor(cIter->second[0]*255,cIter->second[1]*255,cIter->second[2]*255));
    p->setBrush(pointBrush);
    p->drawPolygon(getTriangle(QRect(p1.x()-1.5*PointSize, p1.y()-1.5*PointSize, 1.5*2*PointSize+1, 1.5*2*PointSize+1)));

    cIter++;
  }
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintColorRangePoints(QPainter *p)
{
  QBrush lineBrush(QColor(192, 192, 192));
  QBrush pointBrush(QColor(255, 255, 255));

  double trianglesize = 1.5*PointSize;
  
  p->setPen(QPen(Qt::black, 1.5f, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));  
  p->setBrush(pointBrush);

  for(int i=0; i< (int)this->ColorRangePoints.size();i++)
  {
    QPointF point(cEditorOrigin[0] + (ColorRangePoints.at(i) * (cEditorWidth-1)),  
                  cEditorOrigin[1] + ((int)(0.4f*cEditorHeight)-1) - trianglesize);

    p->drawPolygon(getTriangle(QRect(point.x()-1.5*PointSize, point.y()-1.5*PointSize, 1.5*2*PointSize+1, -1.5*2*PointSize+1)));
  }
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintLabelX(QPainter *p, int o[], int w, int h, QString label)
{
  p->drawText(o[0] + 0.5*w - 20, o[1] + h + 12,  label);
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintLabelY(QPainter *p, int o[], int w, int h, QString label)
{
  p->save();
  p->translate(o[0] + w + 10, o[1] + 0.5*h - 20);
  p->rotate(90);
  p->drawText(0, 0, label);
  p->restore();  
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::paintLabelCount(QPainter *p)
{
  if(this->Histograms.size() > 0)
    p->drawText(oEditorOrigin[0] + oEditorWidth - 110, oEditorOrigin[1] + 15,  this->labelCount);
}

//----------------------------------------------------------------------------
QPolygon qtTransferFunctionCanvas::getTriangle(const QRect &rect)
{
  QPolygon result(3);
  result.setPoint(0, rect.center().x(), rect.top());
  result.setPoint(1, rect.right(), rect.bottom());
  result.setPoint(2, rect.left(), rect.bottom());
  return result;
}

//----------------------------------------------------------------------------
int qtTransferFunctionCanvas::selectOpacityPoint(QPointF pos)
{
  int point = -1;

  QVector< QPair<double, double> >::iterator oIter = OpacityPoints.begin();

  for ( int i = 0; i < (int)OpacityPoints.size(); i++ )
  {
    QPointF p1(oEditorOrigin[0] +                 (oIter->first  * oEditorWidth), 
               oEditorOrigin[1] + oEditorHeight - (oIter->second * oEditorHeight));

    QPointF p = pos - p1;
    float dist = (float) sqrt(p.x()*p.x() + p.y()*p.y());
    if ( dist <= PointSize )
    {
      point = i;
      break;
    }

    oIter++;
  }

  return point;
}

//----------------------------------------------------------------------------
int qtTransferFunctionCanvas::selectGradientPoint(QPointF pos)
{
  int point = -1;

  QVector< QPair<double, double> >::iterator oIter = GradientPoints.begin();

  for ( int i = 0; i < (int)GradientPoints.size(); i++ )
  {
    QPointF p1(gEditorOrigin[0] +                 (oIter->first  * gEditorWidth), 
               gEditorOrigin[1] + gEditorHeight - (oIter->second * gEditorHeight));

    QPointF p = pos - p1;
    float dist = (float) sqrt(p.x()*p.x() + p.y()*p.y());
    if ( dist <= PointSize )
    {
      point = i;
      break;
    }

    oIter++;
  }

  return point;
}

//----------------------------------------------------------------------------
int qtTransferFunctionCanvas::selectColorPoint(QPointF pos)
{
  int point = -1;

  QVector< QPair<double, double*> >::iterator cIterC = ColorPoints.begin();

  // Check if we hit a color point
  for ( int i = 0; i < (int)ColorPoints.size(); i++ )
  {
    QPointF p1(cEditorOrigin[0] + (cIterC->first   * cEditorWidth), pos.y());

    QPointF p = pos - p1;
    float dist = (float) sqrt(p.x()*p.x() + p.y()*p.y());
    if ( (dist <= PointSize)  && (pos.y() > cEditorOrigin[1]))
    {
      point = i;
      break;
    }

    cIterC++;
  }

  return point;
}

//----------------------------------------------------------------------------
int qtTransferFunctionCanvas::selectColorRangePoint(QPointF pos)
{
  int point = -1;

  for ( int i = 0; i < (int)ColorRangePoints.size(); i++ )
  {
    QPointF p1(cEditorOrigin[0] + (ColorRangePoints.at(i)  * cEditorWidth), pos.y());

    QPointF p = pos - p1;
    float dist = (float) sqrt(p.x()*p.x() + p.y()*p.y());
    if ( (dist <= PointSize) && (pos.y() < cEditorOrigin[1]))
    {
      point = i;
      break;
    }
  }

  return point;
}


//----------------------------------------------------------------------------
bool qtTransferFunctionCanvas::insideOpacityEditor(QPointF pos, bool borderX, bool borderY)
{
  bool result = false;
  int incX = (borderX==true) ? 1 : 0;
  int incY = (borderY==true) ? 1 : 0;  

  if(pos.x() >= oEditorOrigin[0]                 - incX*PointSize && 
     pos.x() <= oEditorOrigin[0] + oEditorWidth  + incX*PointSize   &&
     pos.y() >= oEditorOrigin[1]                 - incY*PointSize  && 
     pos.y() <= oEditorOrigin[1] + oEditorHeight + incY*PointSize   )
  {
    result = true;
  }  
  return result;
}

//----------------------------------------------------------------------------
bool qtTransferFunctionCanvas::insideGradientEditor(QPointF pos, bool borderX, bool borderY)
{
  bool result = false;
  int incX = (borderX==true) ? 1 : 0;
  int incY = (borderY==true) ? 1 : 0;  

  if(pos.x() >= gEditorOrigin[0]                 - incX*PointSize && 
     pos.x() <= gEditorOrigin[0] + gEditorWidth  + incX*PointSize   &&
     pos.y() >= gEditorOrigin[1]                 - incY*PointSize  && 
     pos.y() <= gEditorOrigin[1] + gEditorHeight + incY*PointSize   )
  {
    result = true;
  }  
  return result;
}

//----------------------------------------------------------------------------
// return value
//    0 = not in color editor
//    1 = color editor area
//    2 = color range area
int qtTransferFunctionCanvas::insideColorEditor(QPointF pos, bool borderX, bool borderY)
{
  int result = 0;
  int incX = (borderX==true) ? 1 : 0;
  int incY = (borderY==true) ? 1 : 0;  
  
  // Color editor area
  if(pos.x() >= cEditorOrigin[0]                           - incX*PointSize && 
     pos.x() <= cEditorOrigin[0] +       cEditorWidth  - 1 + incX*PointSize &&
     pos.y() >= cEditorOrigin[1] - 0.5 * cEditorHeight - 1 - incY*PointSize && 
     pos.y() <= cEditorOrigin[1] +       cEditorHeight - 1 + incY*PointSize  )
  {
    result = 1;
  }

  // Color range area
  if(pos.x() >= cEditorOrigin[0]                           - incX*PointSize && 
     pos.x() <= cEditorOrigin[0] +       cEditorWidth  - 1 + incX*PointSize &&
     pos.y() >= cEditorOrigin[1] -       cEditorHeight - 1 - incY*PointSize && 
     pos.y() <= cEditorOrigin[1] - 0.5 * cEditorHeight - 1 + incY*PointSize  )
  {
    result = 2;
  }
 
  return result;
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::updateHistogramPoly()
{
  this->HistogramPoly.clear();

  for(int i=0; i<(int)this->Histograms.size(); i++)
    this->HistogramPoly.push_back(this->computeHistogramPoly(this->Histograms[i]));
}

//----------------------------------------------------------------------------
QPolygonF qtTransferFunctionCanvas::computeHistogramPoly(qfeHistogram *histogram)
{
  QPolygonF path; 
  path.clear();
  int scale = max(1, histogramscale);

  if (histogram->qfeGetNrOfBins() <= 0) return path; 
  unsigned maxCount = histogram->qfeGetMaxCount();

  path << QPointF(oEditorOrigin[0], oEditorOrigin[1]+oEditorHeight);
  for ( int i = 0; i < histogram->qfeGetNrOfBins(); i++ )
  {
    float probability;

    if(histogramscaletype > 0)
      probability = log((float)histogram->qfeGetCount(i)) /log((float)maxCount);
    else
      probability = (float)histogram->qfeGetCount(i) / (float)maxCount;

    probability = probability * scale/100.0f;

    probability = MIN(probability, 1.0);
    probability = MAX(probability, 0.0);   

    float x = oEditorOrigin[0] + (i * (oEditorWidth / (float)(histogram->qfeGetNrOfBins()-1)));
    float y = oEditorOrigin[1] + oEditorHeight * (1.0-probability);

    path << QPointF(x, y);
  }
  path << QPointF(oEditorOrigin[0] + oEditorWidth - 1, oEditorOrigin[1] + oEditorHeight);

  return path;  
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::computeLinearGradient(QVector< QPair<double, double*> > ColorPoints, QVector< qtColorRGB > &gradient, int size, int colorspace)
{
  float        currentPos, nextPos;
  qtColorRGB  cRGB, nRGB;
  qtColorXYZ  cXYZ, nXYZ, XYZ;
  qtColorLuv  cLuv, nLuv, Luv;
  qtColorLab  cLab, nLab, Lab;

  QVector< QPair<double, double*> > points = ColorPoints;

  qSort(points.begin(), points.end());
  gradient.clear();  

  // Make sure colors points are between 0 and 1
  // make sure we have point at begin and end

  points.insert(points.begin(),QPair< double, double * >(0.0,points.front().second));
  points.push_back(QPair< double, double * >(1.0,points.back().second));

  // Uniform color padding
  double paddingColor[3];
  paddingColor[0] = paddingColor[1] = paddingColor[2] =  1.0;

  if(this->paddinguniform)
  {
    points.front().second = paddingColor;
    points.back().second  = paddingColor;
  }  

  // Linearly interpolate between the color points
  for(int i=0; i<(int)points.size()-1; i++)
  {    
    currentPos = points.at(i).first;
    nextPos    = points.at(i+1).first;

    cRGB.r = points.at(i).second[0];   
    cRGB.g = points.at(i).second[1];   
    cRGB.b = points.at(i).second[2];
    nRGB.r = points.at(i+1).second[0]; 
    nRGB.g = points.at(i+1).second[1]; 
    nRGB.b = points.at(i+1).second[2];

    int steps = (int)((nextPos - currentPos)*size);

    // Fill the color gradient
    for (int j = 0; j < steps; j++)
    {    
      qtColorRGB RGB;

      double amount = (double)j / (double)(steps);

      // For constant lightness, set the lightness to the
      // best possible value, remaining sufficient chromaticity
      switch(colorspace)
      {
      case qtColorSpaceRGB : RGB = this->lerp(cRGB, nRGB, amount);
        break;
      case qtColorSpaceLuv : this->convertRGB2XYZ(cRGB, cXYZ);
        this->convertXYZ2Luv(cXYZ, cLuv, whiteD65);                            
        this->convertRGB2XYZ(nRGB, nXYZ);
        this->convertXYZ2Luv(nXYZ, nLuv, whiteD65);                               

        Luv = this->lerp(cLuv, nLuv, amount);

        if(this->constantlightness) Luv.L = 72.1;

        this->convertLuv2XYZ(Luv, XYZ, whiteD65);
        this->convertXYZ2RGB(XYZ, RGB);

        RGB = this->clamp(RGB);
        break;
      case qtColorSpaceLab : this->convertRGB2XYZ(cRGB, cXYZ);
        this->convertXYZ2Lab(cXYZ, cLab, whiteD65);
        this->convertRGB2XYZ(nRGB, nXYZ);
        this->convertXYZ2Lab(nXYZ, nLab, whiteD65); 

        Lab = this->lerp(cLab, nLab, amount);

        if(this->constantlightness) Lab.L = 72.1;

        this->convertLab2XYZ(Lab, XYZ, whiteD65);
        this->convertXYZ2RGB(XYZ, RGB);

        RGB = this->clamp(RGB);
        break;
      default :  RGB = this->lerp(cRGB, nRGB, amount);
        break;
      }

      if(this->paddinguniform)
      {
        if(i==0)                      
          RGB = cRGB; // padding left  

        if(i==((int)points.size()-2)) 
          RGB = nRGB; // padding right
      }   
      
      gradient.push_back(RGB);    
    }  
  }

  // Check the table size, and fill if necessary
  int currentSize = gradient.size();
  if(currentSize < size)
  {
    for(int i=currentSize; i<size; i++)
    {
      gradient.push_back(gradient.at(currentSize-1));
    }
  }
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::computeQuantizedGradient(QVector< qtColorRGB > gradient, QVector< qtColorRGB > &quantized, int size)
{
  int index;

  quantized.clear();

  for ( int i = 0; i < size; i++ )
  {
    index = (int)(((i+0.5)/(float)size) * gradient.size());
   
    quantized.push_back(gradient.at(index));
  }
}


//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::convertRGB2XYZ(qtColorRGB in, qtColorXYZ &out)
{
  qtColorRGB in_linear;

  in_linear = linearize_sRGB(in);

  out.X = 0.4124564 * in_linear.r +
          0.3575761 * in_linear.g +
          0.1804375 * in_linear.b;

  out.Y = 0.2126729 * in_linear.r +
          0.7151522 * in_linear.g +
          0.0721750 * in_linear.b;

  out.Z = 0.0193339 * in_linear.r +
          0.1191920 * in_linear.g +
          0.9503041 * in_linear.b;
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::convertXYZ2RGB(qtColorXYZ in, qtColorRGB &out)
{
  qtColorRGB out_linear;

  out_linear.r =  3.2404542 * in.X +
                 -1.5371385 * in.Y +
                 -0.4985314 * in.Z;

  out_linear.g = -0.9692660 * in.X +
                  1.8760108 * in.Y +
                  0.0415560 * in.Z;

  out_linear.b =  0.0556434 * in.X +
                 -0.2040259 * in.Y +
                  1.0572252 * in.Z;

  out = delinearize_sRGB(out_linear);
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::convertXYZ2Lab(qtColorXYZ in, qtColorLab &out, const qtColorXYZ white)
{

  out.L = 116.0 *  forwardLabTransform(in.Y / white.Y) - 16.0;
  out.a = 500.0 * (forwardLabTransform(in.X / white.X) -
                   forwardLabTransform(in.Y / white.Y));
  out.b = 200.0 * (forwardLabTransform(in.Y / white.Y) -
                   forwardLabTransform(in.Z / white.Z));
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::convertLab2XYZ(qtColorLab in, qtColorXYZ &out, const qtColorXYZ white)
{
  const double delta = 6.0 / 29.0;
  qtColorXYZ f;

  f.Y = (in.L + 16.0) / 116.0;
  f.X = f.Y + in.a / 500.0;
  f.Z = f.Y - in.b / 200.0;

  if (f.Y > delta) {
    out.Y = white.Y * pow(f.Y, 3.0);
  } else {
    out.Y = (f.Y - 16.0 / 116.0) *
      3.0 * pow(delta, 2.0) * white.Y;
  }

  if (f.X > delta) {
    out.X = white.X * pow(f.X, 3.0);
  } else {
    out.X = (f.X - 16.0 / 116.0) *
      3.0 * pow(delta, 2.0) * white.X;
  }

  if (f.Z > delta) {
    out.Z = white.Z * pow(f.Z, 3.0);
  } else {
    out.Z = (f.Z - 16.0 / 116.0) *
      3.0 * pow(delta, 2.0) * white.Z;
  }
}

//----------------------------------------------------------------------------
double qtTransferFunctionCanvas::linearize_sRGB_component(double in)
{
  const double a = 0.055;

  if (in <= 0.04045) {
    return in / 12.92;
  } else {
    return pow((in + a) / (1.0 + a), 2.4);
  }
}

//----------------------------------------------------------------------------
double qtTransferFunctionCanvas::delinearize_sRGB_component(double in)
{
  const double a = 0.055;

  if (in <= 0.0031308) {
    return 12.92 * in;
  } else {
    return (1.0 + a) * pow(in, 1.0 / 2.4) - a;
  }
}

//----------------------------------------------------------------------------
qtColorRGB qtTransferFunctionCanvas::linearize_sRGB(const qtColorRGB &in)
{
  qtColorRGB out;

  out.r = linearize_sRGB_component(in.r);
  out.g = linearize_sRGB_component(in.g);
  out.b = linearize_sRGB_component(in.b);

  return out;
}

//----------------------------------------------------------------------------
qtColorRGB qtTransferFunctionCanvas::delinearize_sRGB(const qtColorRGB &in)
{
  qtColorRGB out;

  out.r = delinearize_sRGB_component(in.r);
  out.g = delinearize_sRGB_component(in.g);
  out.b = delinearize_sRGB_component(in.b);

  return out;
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::convertXYZ2Luv(qtColorXYZ in, qtColorLuv &out, const qtColorXYZ white)
{
  const double epsilon = 0.008856;
  const double kappa   = 903.3;

  if (in.Y > epsilon) {
    out.L = 116.0 * pow(in.Y / white.Y, 1.0 / 3.0) - 16.0;
  } else {
    out.L = kappa * in.Y / white.Y;
  }

  double in_sum    = in.X + 15.0 * in.Y + 3.0 * in.Z;
  double u_prime   = 4.0 * in.X / in_sum;
  double v_prime   = 9.0 * in.Y / in_sum;
  double white_sum = white.X + 15.0 * white.Y + 3.0 * white.Z;
  double u_prime_r = 4.0 * white.X / white_sum;
  double v_prime_r = 9.0 * white.Y / white_sum;

  if (in_sum == 0.0) {
    out.u = 0.0;
    out.v = 0.0;
  } else {
    out.u = 13.0 * out.L * (u_prime - u_prime_r);
    out.v = 13.0 * out.L * (v_prime - v_prime_r);
  }
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::convertLuv2XYZ(qtColorLuv in, qtColorXYZ &out, const qtColorXYZ white)
{
  const double epsilon = 0.008856;
  const double kappa   = 903.3;

  if (in.L > kappa * epsilon) {
    out.Y = pow((in.L + 16.0) / 116.0, 3.0);
  } else {
    out.Y = in.L / kappa;
  }

  double u_zero = 4.0 * white.X /
    (white.X + 15.0 * white.Y + 3.0 * white.Z);
  double v_zero = 9.0 * white.Y /
    (white.X + 15.0 * white.Y + 3.0 * white.Z);

  double a = 1.0 / 3.0 * (52.0 * in.L /
    (in.u + 13.0 * in.L * u_zero) - 1.0);
  double b = -5.0 * out.Y;
  double c = -1.0 / 3.0;
  double d = out.Y * (39.0 * in.L /
    (in.v + 13.0 * in.L * v_zero) - 5.0);

  out.X = (d - b) / (a - c);
  out.Z = out.X * a + b;
}

//----------------------------------------------------------------------------
double qtTransferFunctionCanvas::forwardLabTransform(double in)
{
  if (in > pow(6.0 / 29.0, 3)) {
    return pow(in, 1.0 / 3.0);
  } else {
    return 1.0 / 3.0 * pow(29.0 / 6.0, 2.0) * in + 4.0 / 29.0;
  }
}

//----------------------------------------------------------------------------
qtColorRGB qtTransferFunctionCanvas::clamp(qtColorRGB in)
{
  qtColorRGB out;
  out.r = clamp(in.r);
  out.g = clamp(in.g);
  out.b = clamp(in.b);
  return out;  
}

//----------------------------------------------------------------------------
double qtTransferFunctionCanvas::clamp(double in)
{
  double out;
  out = (in<0.0)  ? 0.0 : in;
  out = (out>1.0) ? 1.0 : out;
  return out;  
}

//----------------------------------------------------------------------------
qtColorRGB qtTransferFunctionCanvas::lerp(qtColorRGB value1, qtColorRGB value2, double amount)
{
  qtColorRGB linear;

  linear.r = lerp(value1.r, value2.r, amount);
  linear.g = lerp(value1.g, value2.g, amount);
  linear.b = lerp(value1.b, value2.b, amount);

  return linear;
}

//----------------------------------------------------------------------------
qtColorLab qtTransferFunctionCanvas::lerp(qtColorLab value1, qtColorLab value2, double amount)
{
  qtColorLab linear;

  linear.L = lerp(value1.L, value2.L, amount);
  linear.a = lerp(value1.a, value2.a, amount);
  linear.b = lerp(value1.b, value2.b, amount);

  return linear;
}

//----------------------------------------------------------------------------
qtColorLuv qtTransferFunctionCanvas::lerp(qtColorLuv value1, qtColorLuv value2, double amount)
{
  qtColorLuv linear;

  linear.L = lerp(value1.L, value2.L, amount);
  linear.u = lerp(value1.u, value2.u, amount);
  linear.v = lerp(value1.v, value2.v, amount);

  return linear;
}

//----------------------------------------------------------------------------
double qtTransferFunctionCanvas::lerp(double value1, double value2, double amount)
{
  return (value1 * (1.0 - amount) + value2  * amount);
}

//----------------------------------------------------------------------------
void qtTransferFunctionCanvas::updateColorRange(int selectedCR)
{
  if(selectedCR < 0 || selectedCR > ColorRangePoints.size()) return;

  double r, a, b, c, d, x;
  a = ColorPoints.front().first;
  b = ColorPoints.back().first;

  x = ColorRangePoints[selectedCR];

  for(int i=0; i<(int)ColorPoints.size();i++)
  {
    if(selectedCR == 0)    
    {
      c = x;
      d = b;
    }
    if(selectedCR == 1)
    {
      c = a;
      d = x;
    }
    
    r = (d-c)/(b-a);

    ColorPoints[i].first = (ColorPoints[i].first - a)*r+c;
  }    
}