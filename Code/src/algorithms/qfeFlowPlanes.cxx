#include "qfeFlowPlanes.h"

//----------------------------------------------------------------------------
qfeFlowPlanes::qfeFlowPlanes()
{
  this->shaderProgram = new qfeGLShaderProgram();
  this->shaderProgram->qfeAddShaderFromFile("/shaders/ifr/plane-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgram->qfeAddShaderFromFile("/shaders/ifr/plane-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgram->qfeLink();

  this->firstRender = true;

  this->volumeTexId        = 0;

  this->paramViewUp.x      = 0;
  this->paramViewUp.y      = 1;
  this->paramViewUp.z      = 0;
  this->paramFlowPlaneType = 0;
  this->paramFlowPlaneData = 0;
  this->paramFlowPlaneSize = 30.0;

  this->qfeCreateTextures();
};

//----------------------------------------------------------------------------
qfeFlowPlanes::qfeFlowPlanes(const qfeFlowPlanes &fp)
{
  this->firstRender        = fp.firstRender;
  this->shaderProgram      = fp.shaderProgram;
  
  this->paramViewUp.x      = fp.paramViewUp.x;
  this->paramViewUp.y      = fp.paramViewUp.y;
  this->paramViewUp.z      = fp.paramViewUp.z;
  this->paramFlowPlaneType = fp.paramFlowPlaneType;
  this->paramFlowPlaneSize = fp.paramFlowPlaneSize;
};

//----------------------------------------------------------------------------
qfeFlowPlanes::~qfeFlowPlanes()
{
  delete this->shaderProgram;

  this->qfeDeleteTextures();
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeRenderFlowPlanes(qfeProbe2D *probe, qfeVolume *volume)
{
  return this->qfeRenderFlowPlanesGPU(probe, volume);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeRenderFlowPlanesGPU(qfeProbe2D *probe, qfeVolume *volume)
{      
  if(this->firstRender)
  {     
    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  volume->qfeGetVolumeTextureId(this->volumeTexId);

  this->qfeBindTextures();
  
  this->qfeSetDynamicUniformLocations();      

  switch(this->paramFlowPlaneType)
  {
  case(0) : break;
  case(1) : this->qfeRenderFlowPlanesIntegratedGPU(probe, volume);
            break;
  case(2) : this->qfeRenderFlowPlanesExplodedGPU(probe, volume);
            break;

  }
  
  this->qfeUnbindTextures();
 

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeRenderFlowPlanesIntegratedGPU(qfeProbe2D *probe, qfeVolume *volume)
{
  qfeMatrix4f      P2V, V2T;
  double           scale;
  qfePoint         o, quad[4], tex[4];
  qfeVector        x, y, z, s;
  vector<qfePoint> p, t;

  scale = this->paramFlowPlaneSize / 2.0;

  // Get transformation matrices
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);

  o.qfeSetPointElements(probe->origin.x, probe->origin.y, probe->origin.z);
  x.qfeSetVectorElements(probe->axisX.x, probe->axisX.y,  probe->axisX.z);
  y.qfeSetVectorElements(probe->axisY.x, probe->axisY.y,  probe->axisY.z);
  z.qfeSetVectorElements(probe->normal.x, probe->normal.y,  probe->normal.z);

  this->paramPlaneVector = x;
  this->paramPlaneNormal = z;

  // Scale to the right size in millimeters
  quad[0] = o + (x*scale) + (y*scale);
  quad[1] = o - (x*scale) + (y*scale);
  quad[2] = o - (x*scale) - (y*scale);
  quad[3] = o + (x*scale) - (y*scale);
 
  // Set the points and texture coordinates for rendering
  for(int i=0; i<4; i++)
  {
    tex[i] = quad[i]*(P2V*V2T);

    p.push_back(qfePoint(quad[i].x, quad[i].y, quad[i].z));
    t.push_back(qfePoint(tex[i].x,  tex[i].y,  tex[i].z));
  }

  // Render the polygon
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in patient coordinates

  this->shaderProgram->qfeEnable();

  this->qfeDrawPlane3D(p, t);

  this->shaderProgram->qfeDisable();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeRenderFlowPlanesExplodedGPU(qfeProbe2D *probe, qfeVolume *volume)
{
  qfeMatrix4f      P2V, V2T, MVinv, Pinv;
  double           scale;
  qfePoint         o3d, o2d, quad3D[4], quad2D[4], tex[4], closest, farthest;
  qfeVector        e, u, v, w, x, y, z, s;
  vector<qfePoint> p, q, t;
  int              upperIndex, lowerIndex;

  double distance = 50.0;

  scale = this->paramFlowPlaneSize / 2.0;

  // Get transformation matrices
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);

  // Convert to qfe format
  o3d.qfeSetPointElements(probe->origin.x, probe->origin.y, probe->origin.z);
  x.qfeSetVectorElements(probe->axisX.x, probe->axisX.y,  probe->axisX.z);
  y.qfeSetVectorElements(probe->axisY.x, probe->axisY.y,  probe->axisY.z);
  z.qfeSetVectorElements(probe->normal.x, probe->normal.y,  probe->normal.z);

  // Get coordinate system at the view vector (e, u, v)
  GLfloat modelview[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f MV;
  qfeMatrix4f::qfeSetMatrixElements(MV, modelview);
  qfeMatrix4f::qfeGetMatrixInverse(MV, MVinv);  

  e.qfeSetVectorElements(0.0,0.0,-1.0,0.0);
  qfeMatrix4f::qfeMatrixPreMultiply(e, MVinv, e);
  qfeVector::qfeVectorNormalize(e);

  u = this->paramViewUp;
  qfeVector::qfeVectorNormalize(u);

  qfeVector::qfeVectorCross(e, u, v);
  qfeVector::qfeVectorNormalize(v);

  // Compute axis to move the the plane
  qfeVector::qfeVectorCross(e, u, w);

  // Move outwards
  double angle = w*(o3d-qfePoint(0.0,0.0,0.0));
  if(angle >= 0)
    o2d   = o3d + distance * w;
  else
    o2d   = o3d - distance * w;

  // Scale to the right size in millimeters
  quad3D[0] = o3d + (x*scale) + (y*scale);
  quad3D[1] = o3d - (x*scale) + (y*scale);
  quad3D[2] = o3d - (x*scale) - (y*scale);
  quad3D[3] = o3d + (x*scale) - (y*scale);

  quad2D[0] = o2d - (u*scale) - (v*scale);
  quad2D[1] = o2d + (u*scale) - (v*scale);
  quad2D[2] = o2d + (u*scale) + (v*scale);
  quad2D[3] = o2d - (u*scale) + (v*scale);

  // Set the points and texture coordinates for rendering
  for(int i=0; i<4; i++)
  {
    tex[i] = quad3D[i] * (P2V*V2T);

    p.push_back(qfePoint(quad2D[i].x, quad2D[i].y, quad2D[i].z));
    q.push_back(qfePoint(quad3D[i].x, quad3D[i].y, quad3D[i].z));
    t.push_back(qfePoint(tex[i].x,  tex[i].y,  tex[i].z));
  }

  // Render the polygon
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in patient coordinates

  // Render the view aligned plane
  this->shaderProgram->qfeEnable();
 
  this->qfeDrawPlane3D(p, t);

  this->shaderProgram->qfeDisable();

  // Render the indication lines
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  glColor3f(0.5,0.5,0.5);
  this->qfeDrawPlane3D(p, t);
  this->qfeDrawPlane3D(q, t);

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND);        

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  // Render dashed connection lines
  
  // Determine the right corner of the 2D quad
  if(angle >= 0)
  {
    upperIndex = 2;
    lowerIndex = 0;
  }
  else
  {
    upperIndex = 1;
    lowerIndex = 3;
  }

  // Draw the lines
  glEnable(GL_LINE_STIPPLE);
  glLineStipple(1.0, 0x00FF);

  glColor4f(0.5,0.5,0.5,0.5);
  glBegin(GL_LINES);
   glVertex3f(quad3D[upperIndex].x, quad3D[upperIndex].y, quad3D[upperIndex].z);
   glVertex3f(quad2D[upperIndex].x, quad2D[upperIndex].y, quad2D[upperIndex].z);

   glVertex3f(quad3D[lowerIndex].x, quad3D[lowerIndex].y, quad3D[lowerIndex].z);
   glVertex3f(quad2D[lowerIndex].x, quad2D[lowerIndex].y, quad2D[lowerIndex].z);
  glEnd();

  glDisable(GL_LINE_STIPPLE);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeDrawPlane3D(vector<qfePoint> points, vector<qfePoint> texCoords)
{
  if(points.size() != texCoords.size())
    return qfeError;

  glBegin(GL_QUADS);
  for(int i=0; i<(int)points.size(); i++)
  {
    glTexCoord3f(texCoords[i].x, texCoords[i].y, texCoords[i].z);
    glVertex3f(points[i].x, points[i].y, points[i].z);
  }
  glEnd();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeSetPlaneType(int type)
{
  this->paramFlowPlaneType = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeSetPlaneData(int data)
{
  this->paramFlowPlaneData = data;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeSetPlaneSize(double size)
{
  this->paramFlowPlaneSize = size;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeSetCameraViewUp(qfeVector viewup)
{
  this->paramViewUp = viewup;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeCreateTextures()
{
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeDeleteTextures()
{
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeBindTextures()
{
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_3D, this->volumeTexId);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeSetStaticUniformLocations()
{
  this->shaderProgram->qfeSetUniform1i("volumeData", 1);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowPlanes::qfeSetDynamicUniformLocations()
{
  this->shaderProgram->qfeSetUniform1i("planeData"  , this->paramFlowPlaneData);
  this->shaderProgram->qfeSetUniform3f("planeNormal", this->paramPlaneNormal.x, this->paramPlaneNormal.y, this->paramPlaneNormal.z);
  this->shaderProgram->qfeSetUniform3f("planeVector", this->paramPlaneVector.x, this->paramPlaneVector.y, this->paramPlaneVector.z);

  return qfeSuccess;
}
