#include "qfeStdDevFilter.h"

qfeStdDevFilter::qfeStdDevFilter() {
  below = true;

  size = 0;
  width = 0;
  height = 0;
  depth = 0;
  phaseSpan = 5;
  
  velExponent = 0.5f;
  magExponent = 0.5f;
  threshold = 0.7f;

  meansx = nullptr;
  meansy = nullptr;
  meansz = nullptr;
  sq_sumsx = nullptr;
  sq_sumsy = nullptr;
  sq_sumsz = nullptr;
  stddevsx = nullptr;
  stddevsy = nullptr;
  stddevsz = nullptr;
}

qfeStdDevFilter::~qfeStdDevFilter()
{
  // Release reserved memory
  /*for (unsigned vi = 0; vi < size; vi++) {
    delete[] stddevs[vi];
    delete[] sq_sums[vi];
    delete[] means[vi];
  }*/
  delete[] stddevsx;
  delete[] stddevsy;
  delete[] stddevsz;
  delete[] sq_sumsx;
  delete[] sq_sumsy;
  delete[] sq_sumsz;
  delete[] meansx;
  delete[] meansy;
  delete[] meansz;
}

void qfeStdDevFilter::qfeSetData(qfeVolumeSeries *magnitudeData) {
  (*magnitudeData)[0]->qfeGetVolumeSize(width, height, depth);
  size = width * height * depth;
  nrFrames = (unsigned)magnitudeData->size();

  // Reserve memory for means, squared sums, and standard deviations
  // Initialize them to 0
  if (meansx == nullptr) {
    meansx = new float*[size];
    for (unsigned vi = 0; vi < size; vi++)
      meansx[vi] = new float[nrFrames];
    meansy = new float*[size];
    for (unsigned vi = 0; vi < size; vi++)
      meansy[vi] = new float[nrFrames];
    meansz = new float*[size];
    for (unsigned vi = 0; vi < size; vi++)
      meansz[vi] = new float[nrFrames];
    sq_sumsx = new float*[size];
    for (unsigned vi = 0; vi < size; vi++)
      sq_sumsx[vi] = new float[nrFrames];
    sq_sumsy = new float*[size];
    for (unsigned vi = 0; vi < size; vi++)
      sq_sumsy[vi] = new float[nrFrames];
    sq_sumsz = new float*[size];
    for (unsigned vi = 0; vi < size; vi++)
      sq_sumsz[vi] = new float[nrFrames];
    stddevsx = new float*[size];
    for (unsigned vi = 0; vi < size; vi++)
      stddevsx[vi] = new float[nrFrames];
    stddevsy = new float*[size];
    for (unsigned vi = 0; vi < size; vi++)
      stddevsy[vi] = new float[nrFrames];
    stddevsz = new float*[size];
    for (unsigned vi = 0; vi < size; vi++)
      stddevsz[vi] = new float[nrFrames];
  }
}

void qfeStdDevFilter::qfeSetThreshold(float percentage) {
  threshold = percentage;
}

void qfeStdDevFilter::qfeSetVelocityFilterExponent(float exponent)
{
  velExponent = exponent;
}

void qfeStdDevFilter::qfeSetMagnitudeFilterExponent(float exponent)
{
  magExponent = exponent;
}

void qfeStdDevFilter::qfeSetPhaseSpan(unsigned phases)
{
  phaseSpan = phases;
}

void qfeStdDevFilter::qfeUpdate() {
}
 
void qfeStdDevFilter::qfeApplyFilterMagnitude(qfeVolumeSeries *magnitudeData, bool inverted) {
  // Sanity check
  if (phaseSpan > nrFrames)
    phaseSpan = nrFrames;

  // If there is no time to take standard deviation over
  if (phaseSpan <= 1)
    return;

  ResetToZero();

  // Calculate the standard deviation through time
  // For every voxel and every frame, look at the phaseSpan frames around the current frame
  const float proportion = 1.f / phaseSpan;
  for (unsigned vi = 0; vi < size; vi++) {
    int offset = -(int)phaseSpan/2;
    for (unsigned fi = 0; fi < nrFrames; fi++) {
      for (unsigned pi = 0; pi < phaseSpan; pi++) {
        int index = offset + pi;
        if (index < 0)
          index = index + nrFrames;

        qfeValueType t;
        unsigned short *magnitudes = nullptr;
        (*magnitudeData)[pi]->qfeGetVolumeData(t, width, height, depth, (void **)&magnitudes);

        const float mag = magnitudes[vi];
        meansx[vi][fi] += mag*proportion;
      }
    }
  }

  for (unsigned vi = 0; vi < size; vi++) {
    int offset = -(int)phaseSpan/2;
    for (unsigned fi = 0; fi < nrFrames; fi++) {
      for (unsigned pi = 0; pi < phaseSpan; pi++) {
        int index = offset + pi;
        if (index < 0)
          index = index + nrFrames;

        qfeValueType t;
        unsigned short *magnitudes = nullptr;
        (*magnitudeData)[pi]->qfeGetVolumeData(t, width, height, depth, (void **)&magnitudes);

        const float dif = magnitudes[vi] - meansx[vi][fi];
        sq_sumsx[vi][fi] += dif*dif*proportion;
      }
    }
  }

  // Use the calculated means and squared sums to calculate the standard deviation
  float minsd = 1e30f;
  float maxsd = -1.f;
  for (unsigned vi = 0; vi < size; vi++) {
    for (unsigned fi = 0; fi < nrFrames; fi++) {
      stddevsx[vi][fi] = sqrtf(sq_sumsx[vi][fi]);
      if (stddevsx[vi][fi] < minsd) minsd = stddevsx[vi][fi];
      if (stddevsx[vi][fi] > maxsd) maxsd = stddevsx[vi][fi];
    }
  }

  // Multiply the current magnitude according to the standard deviation,
  // in relation to the min and max standard deviation
  for (unsigned fi = 0; fi < nrFrames; fi++) {
    qfeValueType t;
    unsigned short *magnitudes = nullptr;
    (*magnitudeData)[fi]->qfeGetVolumeData(t, width, height, depth, (void **)&magnitudes);
    for (unsigned vi = 0; vi < size; vi++) {
      if (!inverted)
        magnitudes[vi] *= powf((stddevsx[vi][fi] - minsd) / (maxsd - minsd), magExponent);
      else
        magnitudes[vi] *= 1.f - powf((stddevsx[vi][fi] - minsd) / (maxsd - minsd), magExponent);
    }
  }
}

void qfeStdDevFilter::ResetToZero()
{
  for (unsigned vi = 0; vi < size; vi++) {
    for (unsigned fi = 0; fi < nrFrames; fi++) {
      meansx[vi][fi] = 0.f;
      meansy[vi][fi] = 0.f;
      meansz[vi][fi] = 0.f;
      sq_sumsx[vi][fi] = 0.f;
      sq_sumsy[vi][fi] = 0.f;
      sq_sumsz[vi][fi] = 0.f;
    }
  }
}

void qfeStdDevFilter::qfeApplyFilterVelocity(qfeVolumeSeries *velocityData, bool inverted) {
  // Sanity check
  if (phaseSpan > nrFrames)
    phaseSpan = nrFrames;

  // If there is no time to take standard deviation over
  if (phaseSpan <= 1)
    return;

  ResetToZero();

  // Calculate the standard deviation through time
  // For every voxel and every frame, look at the phaseSpan frames around the current frame
  const float proportion = 1.f / phaseSpan;
  for (unsigned vi = 0; vi < size; vi++) {
    int offset = -(int)phaseSpan/2;
    for (unsigned fi = 0; fi < nrFrames; fi++) {
      for (unsigned pi = 0; pi < phaseSpan; pi++) {
        int index = offset + pi;
        if (index < 0)
          index = index + nrFrames;

        qfeValueType t;
        float *velocities = nullptr;
        (*velocityData)[pi]->qfeGetVolumeData(t, width, height, depth, (void **)&velocities);

        const float v1 = velocities[vi*3];
        const float v2 = velocities[vi*3+1];
        const float v3 = velocities[vi*3+2];
        const float speed = sqrtf(v1*v1 + v2*v2 + v3*v3);
        meansx[vi][fi] += v1*proportion;
        meansy[vi][fi] += v2*proportion;
        meansz[vi][fi] += v3*proportion;
      }
    }
  }

  for (unsigned vi = 0; vi < size; vi++) {
    int offset = -(int)phaseSpan/2;
    for (unsigned fi = 0; fi < nrFrames; fi++) {
      for (unsigned pi = 0; pi < phaseSpan; pi++) {
        int index = offset + pi;
        if (index < 0)
          index = index + nrFrames;

        qfeValueType t;
        float *velocities = nullptr;
        (*velocityData)[pi]->qfeGetVolumeData(t, width, height, depth, (void **)&velocities);

        const float v1 = velocities[vi*3];
        const float v2 = velocities[vi*3+1];
        const float v3 = velocities[vi*3+2];
        const float speed = sqrtf(v1*v1 + v2*v2 + v3*v3);
        const float difx = v1 - meansx[vi][fi];
        const float dify = v2 - meansy[vi][fi];
        const float difz = v3 - meansz[vi][fi];
        sq_sumsx[vi][fi] += difx*difx*proportion;
        sq_sumsy[vi][fi] += dify*dify*proportion;
        sq_sumsz[vi][fi] += difz*difz*proportion;
      }
    }
  }

  // Use the calculated means and squared sums to calculate the standard deviation
  float minsdx = 1e30f;
  float minsdy = 1e30f;
  float minsdz = 1e30f;
  float maxsdx = -1e30f;
  float maxsdy = -1e30f;
  float maxsdz = -1e30f;
  for (unsigned vi = 0; vi < size; vi++) {
    for (unsigned fi = 0; fi < nrFrames; fi++) {
      stddevsx[vi][fi] = sqrtf(sq_sumsx[vi][fi]);
      stddevsy[vi][fi] = sqrtf(sq_sumsy[vi][fi]);
      stddevsz[vi][fi] = sqrtf(sq_sumsz[vi][fi]);
      if (stddevsx[vi][fi] < minsdx) minsdx = stddevsx[vi][fi];
      if (stddevsx[vi][fi] > maxsdx) maxsdx = stddevsx[vi][fi];
      if (stddevsy[vi][fi] < minsdy) minsdy = stddevsy[vi][fi];
      if (stddevsy[vi][fi] > maxsdy) maxsdy = stddevsy[vi][fi];
      if (stddevsz[vi][fi] < minsdz) minsdz = stddevsz[vi][fi];
      if (stddevsz[vi][fi] > maxsdz) maxsdz = stddevsz[vi][fi];
    }
  }

  // Zero out any high standard deviation voxels
  float absThresholdx = minsdx + threshold * (maxsdx - minsdx);
  float absThresholdy = minsdy + threshold * (maxsdy - minsdy);
  float absThresholdz = minsdz + threshold * (maxsdz - minsdz);
  for (unsigned fi = 0; fi < nrFrames; fi++) {
    qfeValueType t;
    float *velocities = nullptr;
    (*velocityData)[fi]->qfeGetVolumeData(t, width, height, depth, (void **)&velocities);
    for (unsigned vi = 0; vi < size; vi++) {
      if (!inverted && (stddevsx[vi][fi] > absThresholdx || stddevsy[vi][fi] > absThresholdy || stddevsz[vi][fi] > absThresholdz)) {
        velocities[vi*3] = 0.f;
        velocities[vi*3+1] = 0.f;
        velocities[vi*3+2] = 0.f;
      } else if (inverted && stddevsx[vi][fi] <= absThresholdx && stddevsy[vi][fi] <= absThresholdy && stddevsz[vi][fi] <= absThresholdz) {
        velocities[vi*3] = 0.f;
        velocities[vi*3+1] = 0.f;
        velocities[vi*3+2] = 0.f;
      }
    }
  }
}