#include "qfePostProcessing.h"

//----------------------------------------------------------------------------
qfePostProcessing::qfePostProcessing()
{
	textureToScreenShader.qfeAddShaderFromFile("/shaders/postprocessing/texture-to-screen-vert.glsl", QFE_VERTEX_SHADER);
	textureToScreenShader.qfeAddShaderFromFile("/shaders/postprocessing/texture-to-screen-frag.glsl", QFE_FRAGMENT_SHADER);
	textureToScreenShader.qfeLink();

	depthMapShader.qfeAddShaderFromFile("/shaders/postprocessing/depth-map-vert.glsl", QFE_VERTEX_SHADER);
	depthMapShader.qfeAddShaderFromFile("/shaders/postprocessing/depth-map-frag.glsl", QFE_FRAGMENT_SHADER);
	depthMapShader.qfeLink();

	passePartoutShader.qfeAddShaderFromFile("/shaders/postprocessing/passe-partout-vert.glsl", QFE_VERTEX_SHADER);
	passePartoutShader.qfeAddShaderFromFile("/shaders/postprocessing/passe-partout-frag.glsl", QFE_FRAGMENT_SHADER);
	passePartoutShader.qfeLink();

	estimateNormalMapShader.qfeAddShaderFromFile("/shaders/postprocessing/estimate-normal-map-vert.glsl", QFE_VERTEX_SHADER);
	estimateNormalMapShader.qfeAddShaderFromFile("/shaders/postprocessing/estimate-normal-map-frag.glsl", QFE_FRAGMENT_SHADER);
	estimateNormalMapShader.qfeLink();
	
	gammaCorrectionShader.qfeAddShaderFromFile("/shaders/postprocessing/gamma-correction-vert.glsl", QFE_VERTEX_SHADER);
	gammaCorrectionShader.qfeAddShaderFromFile("/shaders/postprocessing/gamma-correction-frag.glsl", QFE_FRAGMENT_SHADER);
	gammaCorrectionShader.qfeLink();

	toneMappingShader.qfeAddShaderFromFile("/shaders/postprocessing/tone-mapping-vert.glsl", QFE_VERTEX_SHADER);
	toneMappingShader.qfeAddShaderFromFile("/shaders/postprocessing/tone-mapping-frag.glsl", QFE_FRAGMENT_SHADER);
	toneMappingShader.qfeLink();

	contourShader.qfeAddShaderFromFile("/shaders/postprocessing/contours-vert.glsl", QFE_VERTEX_SHADER);
	contourShader.qfeAddShaderFromFile("/shaders/postprocessing/contours-frag.glsl", QFE_FRAGMENT_SHADER);
	contourShader.qfeLink();

	sobelShader.qfeAddShaderFromFile("/shaders/postprocessing/sobel-vert.glsl", QFE_VERTEX_SHADER);
	sobelShader.qfeAddShaderFromFile("/shaders/postprocessing/sobel-frag.glsl", QFE_FRAGMENT_SHADER);
	sobelShader.qfeLink();

	crosshatchingShader.qfeAddShaderFromFile("/shaders/postprocessing/crosshatching-vert.glsl", QFE_VERTEX_SHADER);
	crosshatchingShader.qfeAddShaderFromFile("/shaders/postprocessing/crosshatching-frag.glsl", QFE_FRAGMENT_SHADER);
	crosshatchingShader.qfeLink();

	fqaaShader.qfeAddShaderFromFile("/shaders/postprocessing/fqaa-vert.glsl", QFE_VERTEX_SHADER);
	fqaaShader.qfeAddShaderFromFile("/shaders/postprocessing/fqaa-frag.glsl", QFE_FRAGMENT_SHADER);
	fqaaShader.qfeLink();

	depthDarkeningShader.qfeAddShaderFromFile("/shaders/postprocessing/depth-darkening-vert.glsl", QFE_VERTEX_SHADER);
	depthDarkeningShader.qfeAddShaderFromFile("/shaders/postprocessing/depth-darkening-frag.glsl", QFE_FRAGMENT_SHADER);
	depthDarkeningShader.qfeLink();

	ssaoShader.qfeAddShaderFromFile("/shaders/postprocessing/ssao-vert.glsl", QFE_VERTEX_SHADER);
	ssaoShader.qfeAddShaderFromFile("/shaders/postprocessing/ssao-frag.glsl", QFE_FRAGMENT_SHADER);
	ssaoShader.qfeLink();

	sharpenShader.qfeAddShaderFromFile("/shaders/postprocessing/sharpen-vert.glsl", QFE_VERTEX_SHADER);
	sharpenShader.qfeAddShaderFromFile("/shaders/postprocessing/sharpen-frag.glsl", QFE_FRAGMENT_SHADER);
	sharpenShader.qfeLink();

	gaussianBlurVerticalShader.qfeAddShaderFromFile("/shaders/postprocessing/gaussian-blur-vertical-vert.glsl", QFE_VERTEX_SHADER);
	gaussianBlurVerticalShader.qfeAddShaderFromFile("/shaders/postprocessing/gaussian-blur-vertical-frag.glsl", QFE_FRAGMENT_SHADER);
	gaussianBlurVerticalShader.qfeLink();
	gaussianBlurHorizontalShader.qfeAddShaderFromFile("/shaders/postprocessing/gaussian-blur-horizontal-vert.glsl", QFE_VERTEX_SHADER);
	gaussianBlurHorizontalShader.qfeAddShaderFromFile("/shaders/postprocessing/gaussian-blur-horizontal-frag.glsl", QFE_FRAGMENT_SHADER);
	gaussianBlurHorizontalShader.qfeLink();

	bilateralFilterShader.qfeAddShaderFromFile("/shaders/postprocessing/bilateral-vert.glsl", QFE_VERTEX_SHADER);
	bilateralFilterShader.qfeAddShaderFromFile("/shaders/postprocessing/bilateral-frag.glsl", QFE_FRAGMENT_SHADER);
	bilateralFilterShader.qfeLink();
};

//----------------------------------------------------------------------------
qfePostProcessing::~qfePostProcessing()
{
	//DISABLE 3D rendering
	engine->canvas->GlobalWarningDisplayOff();
	engine->canvas->GetRenderWindow()->SetStereoCapableWindow(1);
	engine->canvas->GetRenderWindow()->StereoRenderOff();
	engine->canvas->GetRenderWindow()->StereoUpdate();
	engine->canvas->GlobalWarningDisplayOn();
};

void qfePostProcessing::qfeApplyPostProcessing(GLuint &_baseframeBuffer, GLuint *frameBufferProps, qfeVtkEngine *_engine, qfeVisualPostProcess *_visual)
{
	visual = _visual;
	engine = _engine;
	
	int paramCount = 0;
	visual->qfeGetVisualParameterCount(paramCount);
	if(visual == NULL || paramCount == 0)
		return;

	qfeViewport *vp3D;
	engine->GetViewport3D(&vp3D);

	vp_width = vp3D->width;
	vp_height = vp3D->height;

	//vtkCamera *vtkcamera = engine->GetRenderer()->GetActiveCamera();
	//vtkcamera->GetClippingRange(znear,zfar);
	znear = (double)LARGEST_DISTANCE;
	zfar = 3.0*(double)LARGEST_DISTANCE;
	
	baseframeBuffer = _baseframeBuffer;

	frameBufferTextureID = frameBufferProps[0];
	frameBufferDepthID = frameBufferProps[2];	
	frameBufferNormalID = this->EstimateNormalMap();

	glGetFloatv(GL_PROJECTION_MATRIX, this->matrixProjection);

	qfeVisualParameter *p = NULL;
	qfeSelectionList    pList;

	bool depth_map_enabled = false;

	visual->qfeGetVisualParameter("Depth map", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(depth_map_enabled);     

	if(depth_map_enabled)
	{				
		DepthMap();
	}

	bool normal_map_enabled = false;

	visual->qfeGetVisualParameter("Normal map", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(normal_map_enabled);     

	if(normal_map_enabled)
	{				
		NormalMap();
	}

	bool depth_darkening_enabled = false;

	visual->qfeGetVisualParameter("Depth darkening", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(depth_darkening_enabled);     

	if(depth_darkening_enabled)
	{		
		bool blend = true;
		double power = 0.0;
		int kernel_size = 1;

		visual->qfeGetVisualParameter("Depth darkening blend", &p);		
		p->qfeGetVisualParameterValue(blend); 

		visual->qfeGetVisualParameter("Depth darkening power", &p);		
		p->qfeGetVisualParameterValue(power); 
		
		visual->qfeGetVisualParameter("Depth darkening kernel size", &p);		
		p->qfeGetVisualParameterValue(kernel_size); 

		DepthDarkening(blend, power, kernel_size);
	}

	bool ssao_enabled = false;

	visual->qfeGetVisualParameter("SSAO", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(ssao_enabled);     

	if(ssao_enabled)
	{	
		bool blend = true;
		double power = 0.0;

		visual->qfeGetVisualParameter("SSAO blend", &p);		
		p->qfeGetVisualParameterValue(blend); 

		visual->qfeGetVisualParameter("SSAO power", &p);		
		p->qfeGetVisualParameterValue(power); 
			
		SSAO(blend, (float)power);
	}

	bool crosshatching_enabled = false;

	visual->qfeGetVisualParameter("Crosshatching", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(crosshatching_enabled);   

	if(crosshatching_enabled)
	{
		Crosshatching();
	}

	bool sobel = false;

	visual->qfeGetVisualParameter("Sobel", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(sobel);     

	if(sobel)
	{
		this->Sobel();
	}

	bool contours = false;

	visual->qfeGetVisualParameter("Contours", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(contours);     

	if(contours)
	{
		this->Contours();
	}

	bool sharpen = false;

	visual->qfeGetVisualParameter("Sharpen", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(sharpen);     

	if(sharpen)
	{
		this->Sharpen();
	}
	
	bool gaussianBlur = false;

	visual->qfeGetVisualParameter("Gaussian Blur", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(gaussianBlur);     

	if(gaussianBlur)
	{
		visual->qfeGetVisualParameter("Gaussian Blur kernel size", &p);
		if(p != NULL) 
		{
			int kernelSize = 0;
			p->qfeGetVisualParameterValue(kernelSize); 
			
			GaussianBlur(kernelSize);
		}
	}

	bool bilateral_filter_enabled = false;

	visual->qfeGetVisualParameter("Bilateral filter", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(bilateral_filter_enabled);   

	if(bilateral_filter_enabled)
	{
		BilateralFilter();
	}

	bool fqaa_enabled = false;

	visual->qfeGetVisualParameter("FQAA", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(fqaa_enabled);   

	if(fqaa_enabled)
	{
		FQAA();
	}

	bool tone_mapping_enabled;

	visual->qfeGetVisualParameter("Tone mapping", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(tone_mapping_enabled); 

	if(tone_mapping_enabled)
	{	
		double exposure = 2.0;

		visual->qfeGetVisualParameter("TM Exposure", &p);		
		if(p != NULL) p->qfeGetVisualParameterValue(exposure); 

		ToneMapping(exposure);
	}

	bool gamma_correction_enabled;

	visual->qfeGetVisualParameter("Gamma correction", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(gamma_correction_enabled); 

	if(gamma_correction_enabled)
	{	
		double gamma = 0.0;

		visual->qfeGetVisualParameter("Gamma", &p);		
		p->qfeGetVisualParameterValue(gamma); 

		GammaCorrection(gamma);
	}


	bool passe_partout_enabled;

	visual->qfeGetVisualParameter("Passe-partout", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(passe_partout_enabled); 

	if(passe_partout_enabled)
	{	
		PassePartout();
	}

	bool enable_3D_rendering = false;

	visual->qfeGetVisualParameter("3D rendering", &p);
	if(p != NULL) p->qfeGetVisualParameterValue(enable_3D_rendering);  
	if(enable_3D_rendering)
	{
		int type3D = 1;
		visual->qfeGetVisualParameter("3D Rendering Type", &p);
		if(p != NULL) p->qfeGetVisualParameterValue(pList);  
		type3D = pList.active+1;
		
		//ENABLE 3D rendering
		engine->canvas->GlobalWarningDisplayOff();
		engine->canvas->GetRenderWindow()->SetStereoCapableWindow(1);
		engine->canvas->GetRenderWindow()->StereoRenderOn();
		engine->canvas->GetRenderWindow()->SetStereoType(type3D);
		engine->canvas->GetRenderWindow()->StereoUpdate();
		engine->canvas->GlobalWarningDisplayOn();
	}
	else
	{
		//DISABLE 3D rendering
		engine->canvas->GlobalWarningDisplayOff();
		engine->canvas->GetRenderWindow()->SetStereoCapableWindow(1);
		engine->canvas->GetRenderWindow()->StereoRenderOff();
		engine->canvas->GetRenderWindow()->StereoUpdate();
		engine->canvas->GlobalWarningDisplayOn();
	}

	//this->qfeRenderTextureToScreen(frameBufferNormalID);

	frameBufferProps[0] = frameBufferTextureID;

	glDeleteTextures(1, &frameBufferNormalID);
}

//----------------------------------------------------------------------------
GLuint qfePostProcessing::EstimateNormalMap() //returns normal texture
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);

	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferDepthID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->estimateNormalMapShader.qfeSetUniform1i("depthTex", 0); //the depth texture
	this->estimateNormalMapShader.qfeSetUniform1f("znear", (float)znear);
	this->estimateNormalMapShader.qfeSetUniform1f("zfar", (float)zfar);
	this->estimateNormalMapShader.qfeEnable();

	this->qfeMockRender();
	
	this->estimateNormalMapShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeDeleteFBOKeepTexture(fbo_id, fbo_depth_id);

	return fbo_texture_id;
}

//----------------------------------------------------------------------------
void qfePostProcessing::DepthMap() 
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);

	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferDepthID);  
		
	glActiveTexture(GL_TEXTURE1);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->depthMapShader.qfeSetUniform1i("depthTex", 0); //the depth texture
	this->depthMapShader.qfeSetUniform1i("sceneTex", 1); //the scene texture
	this->depthMapShader.qfeSetUniform1f("znear", (float)znear);
	this->depthMapShader.qfeSetUniform1f("zfar", (float)zfar);
	this->depthMapShader.qfeEnable();

	this->qfeMockRender();
	
	this->depthMapShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}

//----------------------------------------------------------------------------
void qfePostProcessing::NormalMap() 
{
	this->qfeRenderTextureToScreen(this->frameBufferNormalID);
}


//----------------------------------------------------------------------------
void qfePostProcessing::Contours()
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);

	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferDepthID);  

	glActiveTexture(GL_TEXTURE1);   
	glBindTexture(GL_TEXTURE_2D, frameBufferNormalID);  
		
	glActiveTexture(GL_TEXTURE2);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->contourShader.qfeSetUniform1i("depthTex", 0); //the depth texture
	this->contourShader.qfeSetUniform1i("normalTex", 1); //the normal texture
	this->contourShader.qfeSetUniform1i("sceneTex", 2); //the scene texture
	this->contourShader.qfeSetUniform1f("znear", (float)znear);
	this->contourShader.qfeSetUniform1f("zfar", (float)zfar);
	this->contourShader.qfeEnable();

	this->qfeMockRender();
	
	this->contourShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}

//----------------------------------------------------------------------------
void qfePostProcessing::Sobel()
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);

	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferDepthID);  

	glActiveTexture(GL_TEXTURE1);   
	glBindTexture(GL_TEXTURE_2D, frameBufferNormalID);  
		
	glActiveTexture(GL_TEXTURE2);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->sobelShader.qfeSetUniform1i("depthTex", 0); //the depth texture
	this->sobelShader.qfeSetUniform1i("normalTex", 1); //the normal texture
	this->sobelShader.qfeSetUniform1i("sceneTex", 2); //the scene texture
	this->sobelShader.qfeSetUniform1f("znear", (float)znear);
	this->sobelShader.qfeSetUniform1f("zfar", (float)zfar);
	this->sobelShader.qfeEnable();

	this->qfeMockRender();
	
	this->sobelShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}

void qfePostProcessing::Crosshatching()
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);

	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);

	glActiveTexture(GL_TEXTURE1);   
	glBindTexture(GL_TEXTURE_2D, frameBufferNormalID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->crosshatchingShader.qfeSetUniform1i("sceneTex", 0); //the scene texture
	this->crosshatchingShader.qfeSetUniform1i("normalTex", 1); //the normals texture
	this->crosshatchingShader.qfeEnable();

	this->qfeMockRender();
	
	this->crosshatchingShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}


void qfePostProcessing::BilateralFilter()
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);

	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->bilateralFilterShader.qfeSetUniform1i("sceneTex", 0); //the scene texture
	this->bilateralFilterShader.qfeEnable();

	this->qfeMockRender();
	
	this->bilateralFilterShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}

void qfePostProcessing::FQAA()
{
	//FQAA (flipquad anti aliasing) is a cheap anti aliasing method which only requires 4 samples
	//code based on https://www.shadertoy.com/view/MtSGRG

	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);

	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->fqaaShader.qfeSetUniform1i("sceneTex", 0); //the scene texture
	this->fqaaShader.qfeEnable();

	this->qfeMockRender();
	
	this->fqaaShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}

//----------------------------------------------------------------------------
void qfePostProcessing::DepthDarkening(bool blend, float power, int kernel_size)
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);
 
	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  	
	glActiveTexture(GL_TEXTURE1);   
	glBindTexture(GL_TEXTURE_2D, frameBufferDepthID); 
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->depthDarkeningShader.qfeSetUniform1i("sceneTex", 0); //the color texture	
	this->depthDarkeningShader.qfeSetUniform1i("depthTex", 1); //the depth texture
	this->depthDarkeningShader.qfeSetUniform1f("znear", (float)znear);
	this->depthDarkeningShader.qfeSetUniform1f("zfar", (float)zfar);
	if(blend)
		this->depthDarkeningShader.qfeSetUniform1i("blend_with_scene", 1); //whether the depth darkening should be blended with the original image
	else
		this->depthDarkeningShader.qfeSetUniform1i("blend_with_scene", 0); //whether the depth darkening should be blended with the original image

	this->depthDarkeningShader.qfeSetUniform1f("depth_darkening_power", power); //shadow power
	this->depthDarkeningShader.qfeSetUniform1i("kernel_size", kernel_size); //shadow kernel size


	this->depthDarkeningShader.qfeEnable();

	this->qfeMockRender();
	
	this->depthDarkeningShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}

//----------------------------------------------------------------------------
void qfePostProcessing::SSAO(bool blend, float power)
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);

	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferDepthID);  
	glActiveTexture(GL_TEXTURE1);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->ssaoShader.qfeSetUniform1i("depthTex", 0); //the depth texture
	this->ssaoShader.qfeSetUniform1i("sceneTex", 1); //the color texture	
	this->ssaoShader.qfeSetUniform1f("znear", (float)znear);
	this->ssaoShader.qfeSetUniform1f("zfar", (float)zfar);
	if(blend)
		this->ssaoShader.qfeSetUniform1i("blend_with_scene", 1); //whether the ao should be blended with the original image
	else
		this->ssaoShader.qfeSetUniform1i("blend_with_scene", 0); //whether the ao should be blended with the original image

	this->ssaoShader.qfeSetUniform1f("ao_power", power/4.0); //occlusion power
		
	this->ssaoShader.qfeEnable();

	this->qfeMockRender();
	
	this->ssaoShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}

//----------------------------------------------------------------------------
void qfePostProcessing::GammaCorrection(float gamma)
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);
		
	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->gammaCorrectionShader.qfeSetUniform1i("sceneTex", 0); //the scene texture
	this->gammaCorrectionShader.qfeSetUniform1f("gamma", (float)gamma); //amount of gamma correction
	this->gammaCorrectionShader.qfeEnable();

	this->qfeMockRender();
	
	this->gammaCorrectionShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}

//----------------------------------------------------------------------------
void qfePostProcessing::ToneMapping(double exposure)
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);
		
	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->toneMappingShader.qfeSetUniform1i("sceneTex", 0); //the scene texture
	this->toneMappingShader.qfeSetUniform1f("exposure", (float)exposure); //amount of gamma correction
	this->toneMappingShader.qfeEnable();

	this->qfeMockRender();
	
	this->toneMappingShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}

//----------------------------------------------------------------------------
void qfePostProcessing::Sharpen()
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);
		
	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->sharpenShader.qfeSetUniform1i("sceneTex", 0); //the scene texture
	this->sharpenShader.qfeEnable();

	this->qfeMockRender();
	
	this->sharpenShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}

//----------------------------------------------------------------------------
void qfePostProcessing::GaussianBlur(int kernelSize)
{
	GLuint fbo_id1 = 0;
	GLuint fbo_texture_id1 = 0;
	GLuint fbo_depth_id1 = 0;

	this->qfeSetUpFBO(fbo_id1, fbo_texture_id1, fbo_depth_id1);
	this->qfeEnableFBO(fbo_id1);

	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->gaussianBlurVerticalShader.qfeSetUniform1i("kernel_size", kernelSize);
	this->gaussianBlurVerticalShader.qfeSetUniform1i("sceneTex", 0); //the texture to blur
	this->gaussianBlurVerticalShader.qfeEnable();

	this->qfeMockRender();
	
	this->gaussianBlurVerticalShader.qfeDisable();
	glUseProgram(0);

	this->qfeDisableFBO();

	GLuint fbo_id2 = 0;
	GLuint fbo_texture_id2 = 0;
	GLuint fbo_depth_id2 = 0;

	this->qfeSetUpFBO(fbo_id2, fbo_texture_id2, fbo_depth_id2);
	this->qfeEnableFBO(fbo_id2);

	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, fbo_texture_id1);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->gaussianBlurHorizontalShader.qfeSetUniform1i("kernel_size", kernelSize);
	this->gaussianBlurHorizontalShader.qfeSetUniform1i("sceneTex", 0); //the texture to blur
	this->gaussianBlurHorizontalShader.qfeEnable();

	this->qfeMockRender();
	
	this->gaussianBlurHorizontalShader.qfeDisable();
	glUseProgram(0);

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id2);

	this->qfeDeleteFBO(fbo_id1, fbo_texture_id1, fbo_depth_id1);
	this->qfeDeleteFBO(fbo_id2, fbo_texture_id2, fbo_depth_id2);
}

//----------------------------------------------------------------------------
void qfePostProcessing::PassePartout()
{
	GLuint fbo_id = 0;
	GLuint fbo_texture_id = 0;
	GLuint fbo_depth_id = 0;

	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);

	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, frameBufferDepthID);  
		
	glActiveTexture(GL_TEXTURE1);   
	glBindTexture(GL_TEXTURE_2D, frameBufferTextureID);  
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	this->passePartoutShader.qfeSetUniform1i("depthTex", 0); //the depth texture
	this->passePartoutShader.qfeSetUniform1i("sceneTex", 1); //the scene texture
	this->passePartoutShader.qfeSetUniform1f("znear", (float)znear);
	this->passePartoutShader.qfeSetUniform1f("zfar", (float)zfar);
	this->passePartoutShader.qfeEnable();

	this->qfeMockRender();
	
	this->passePartoutShader.qfeDisable();

	this->qfeDisableFBO();

	this->qfeRenderTextureToScreen(fbo_texture_id);

	this->qfeDeleteFBO(fbo_id, fbo_texture_id, fbo_depth_id);
}

qfeReturnStatus qfePostProcessing::qfeSetUpFBO(GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &fbo_depth_id)
{	
	glBindTexture(GL_TEXTURE_2D, 0);                                // unlink all textures

	//create fbo texture
	createTexture(&fbo_texture_id,GL_TEXTURE_MAG_FILTER, GL_TEXTURE_MIN_FILTER, GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_T,
		GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_RGBA16F, GL_BGRA, GL_UNSIGNED_BYTE,vp_width,vp_height,0);
 
	// create a depth texture
	createTexture(&fbo_depth_id, GL_TEXTURE_MAG_FILTER, GL_TEXTURE_MIN_FILTER, GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_T,
	GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE,GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8,vp_width,vp_height,0);

	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	fbo_id = 0;
	glGenFramebuffers(1, &fbo_id);	
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);
	glViewport(0,0,vp_width,vp_height);

	// attach color and depth
	 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo_texture_id, 0);
	 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, fbo_depth_id, 0);

	// Always check that our framebuffer is ok
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		if(GL_FRAMEBUFFER_UNDEFINED == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_UNDEFINED"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER"<<std::endl;

		if(GL_FRAMEBUFFER_UNSUPPORTED == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_UNSUPPORTED"<<std::endl;
		
		if(GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE"<<std::endl;
		
		if(GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE"<<std::endl;
		
		if(GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS"<<std::endl;

		return qfeError;
	}

	// unbind fbo
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return qfeSuccess;
}

qfeReturnStatus qfePostProcessing::qfeDisableFBO()
{
	// reset to base fbo
	glBindFramebuffer(GL_FRAMEBUFFER, baseframeBuffer);
	glViewport(0,0,vp_width,vp_height);

	return qfeSuccess;
}

qfeReturnStatus qfePostProcessing::qfeEnableFBO(GLuint &fbo_id)
{
	glViewport (0, 0, vp_width, vp_height);                         // set The Current Viewport to the fbo size
	
	// bind the framebuffer as the output framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);
		
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);            // Clear Screen And Depth Buffer on the fbo

	glEnable(GL_DEPTH_TEST);
	
	// define the index array for the outputs
	GLuint attachments[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1,  attachments);

	glViewport(0,0,vp_width,vp_height);

	return qfeSuccess;
}

qfeReturnStatus qfePostProcessing::qfeDeleteFBO(GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &fbo_depth_id)
{
	glDeleteFramebuffers(1, &fbo_id);

	// reset to base fbo
	glBindFramebuffer(GL_FRAMEBUFFER, baseframeBuffer);

	//delete textures:
	glDeleteTextures(1, &fbo_texture_id);
	glDeleteTextures(1, &fbo_depth_id);
	
	return qfeSuccess;
}

qfeReturnStatus qfePostProcessing::qfeDeleteFBOKeepTexture(GLuint &fbo_id, GLuint &fbo_depth_id)
{
	glDeleteFramebuffers(1, &fbo_id);

	// reset to base fbo
	glBindFramebuffer(GL_FRAMEBUFFER, baseframeBuffer);

	//delete textures:
	glDeleteTextures(1, &fbo_depth_id);
	
	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePostProcessing::qfeRenderTextureToScreen(GLuint texture)
{
	glActiveTexture(GL_TEXTURE0);   
	glBindTexture(GL_TEXTURE_2D, texture);  
	
	glDisable(GL_DEPTH_TEST);

	this->textureToScreenShader.qfeSetUniform1i("sceneTex", 0); //texture
	this->textureToScreenShader.qfeEnable();
	
	glEnable(GL_TEXTURE_2D);
		
	glBegin(GL_QUADS);
	glVertex2f(-1, -1);
	glVertex2f(-1,  1);
	glVertex2f( 1,  1);
	glVertex2f( 1, -1);
	glEnd();
	
	glDisable(GL_TEXTURE_2D);

	this->textureToScreenShader.qfeDisable();
	glUseProgram(0);

	glEnable(GL_DEPTH_TEST);

	glBindTexture(GL_TEXTURE_2D, 0);

  return qfeSuccess;
}


qfeReturnStatus qfePostProcessing::qfeRenderTextureToFBO(GLuint texture, GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &fbo_depth_id)
{
	this->qfeSetUpFBO(fbo_id, fbo_texture_id, fbo_depth_id);
	this->qfeEnableFBO(fbo_id);

	this->qfeRenderTextureToScreen(texture);

	this->qfeDisableFBO();

	return qfeSuccess;
}

qfeReturnStatus qfePostProcessing::qfeMockRender()
{
	glBegin(GL_QUADS);
	glVertex2f(-1, -1);
	glVertex2f(-1,  1);
	glVertex2f( 1,  1);
	glVertex2f( 1, -1);
	glEnd();

	return qfeSuccess;
}

