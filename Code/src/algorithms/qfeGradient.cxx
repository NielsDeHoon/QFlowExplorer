#include "qfeGradient.h"

qfeGradient::qfeGradient() {
  magnitudeData = nullptr;

  size = 0;
  width = 0;
  height = 0;
  depth = 0;

  maxGradLength = 0.f;
}

void qfeGradient::qfeSetData(qfeVolumeSeries *magnitudeData) {
  this->magnitudeData = magnitudeData;
  (*magnitudeData)[0]->qfeGetVolumeSize(width, height, depth);
  size = width * height * depth;
  nrFrames = (unsigned)magnitudeData->size();
  qfeGrid *grid;
  (*magnitudeData)[0]->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(sx, sy, sz);
  CalculateWk();
  GetWeights(wA, wB, wC, wD);
}

void qfeGradient::qfeGetGradientTensors(qfeVolume *volume, qfeTensor3f *gradients)
{
  if (!volume)
    return;

  unsigned nrComp;
  volume->qfeGetVolumeComponentsPerVoxel(nrComp);
  if (nrComp != 3)
    return;

  qfeValueType t;
  float *data;
  volume->qfeGetVolumeData(t, width, height, depth, (void**)&data);
  size = width*height*depth;
  if (t != qfeValueTypeFloat)
    return;

  qfeGrid *grid;
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(sx, sy, sz);
  CalculateWk();
  GetWeights(wA, wB, wC, wD);

  // Split vector data into components
  float *cx = new float[size];
  float *cy = new float[size];
  float *cz = new float[size];
  for (unsigned i = 0; i < size; i++) {
    cx[i] = data[i*3];
    cy[i] = data[i*3 + 1];
    cz[i] = data[i*3 + 2];
  }

  for (int z = 0; z < (int)depth; z++) {
    for (int y = 0; y < (int)height; y++) {
      for (int x = 0; x < (int)width; x++) {
        float A, B, C, D;
        unsigned index = GetIndex(x, y, z);
        GetGradient(cx, x, y, z, A, B, C, D);
        gradients[index][0] = A;
        gradients[index][1] = B;
        gradients[index][2] = C;

        GetGradient(cy, x, y, z, A, B, C, D);
        gradients[index][3] = A;
        gradients[index][4] = B;
        gradients[index][5] = C;

        GetGradient(cz, x, y, z, A, B, C, D);
        gradients[index][6] = A;
        gradients[index][7] = B;
        gradients[index][8] = C;
      }
    }
  }
}

// Calculate gradients (leaving out D)
float **qfeGradient::qfeCalculateGradients() {
  float **gradients = new float*[nrFrames];
  for (int fi = 0; fi < (int)nrFrames; fi++)
    gradients[fi] = new float[size*3];

  for (int fi = 0; fi < (int)nrFrames; fi++) {
    qfeValueType t;
    unsigned nrComp;
    (*magnitudeData)[fi]->qfeGetVolumeValueType(t);
    (*magnitudeData)[fi]->qfeGetVolumeComponentsPerVoxel(nrComp);

    if (t == qfeValueTypeUnsignedInt16 || t == qfeValueTypeInt16) {
      unsigned short *magnitudes;
      (*magnitudeData)[fi]->qfeGetVolumeData(t, width, height, depth, (void **)&magnitudes);

      for (int z = 0; z < (int)depth; z++) {
        for (int y = 0; y < (int)height; y++) {
          for (int x = 0; x < (int)width; x++) {
            float A, B, C, D;
            GetGradient(magnitudes, (int)nrComp, x, y, z, A, B, C, D);
            gradients[fi][GetIndex(x, y, z)*3] = A;
            gradients[fi][GetIndex(x, y, z)*3 + 1] = B;
            gradients[fi][GetIndex(x, y, z)*3 + 2] = C;
            float length = sqrt(A*A + B*B + C*C);
            if (length > maxGradLength)
              maxGradLength = length;
          }
        }
      }
    }

    else if (t == qfeValueTypeFloat) {
      float *magnitudes;
      (*magnitudeData)[fi]->qfeGetVolumeData(t, width, height, depth, (void **)&magnitudes);

      for (int z = 0; z < (int)depth; z++) {
        for (int y = 0; y < (int)height; y++) {
          for (int x = 0; x < (int)width; x++) {
            float A, B, C, D;
            GetGradient(magnitudes, (int)nrComp, x, y, z, A, B, C, D);
            gradients[fi][GetIndex(x, y, z)*3] = A;
            gradients[fi][GetIndex(x, y, z)*3 + 1] = B;
            gradients[fi][GetIndex(x, y, z)*3 + 2] = C;
            float length = sqrt(A*A + B*B + C*C);
            if (length > maxGradLength)
              maxGradLength = length;
          }
        }
      }
    }
  }

  return gradients;
}

float qfeGradient::qfeGetMaxGradientLength() {
  return maxGradLength;
}

void qfeGradient::CalculateWk() {
  for (int z = -1; z <= 1; z++) {
    for (int y = -1; y <= 1; y++) {
      for (int x = -1; x <= 1; x++) {
        if (x == 0 && y == 0 && z == 0) {
          wk[13] = 0.f;
          continue;
        }
        const float fx = x*sx;
        const float fy = y*sy;
        const float fz = z*sz;
        wk[z*9 + y*3 + x + 13] = 1.f / (fx*fx + fy*fy + fz*fz);
      }
    }
  }
}

float qfeGradient::GetWk(int x, int y, int z) {
  return wk[z*9 + y*3 + x + 13];
}

void qfeGradient::GetWeights(float &wA, float &wB, float &wC, float &wD) {
  wA = wB = wC = wD = 0.f;

  for (int zk = -1; zk <= 1; zk++) {
    for (int yk = -1; yk <= 1; yk++) {
      for (int xk = -1; xk <= 1; xk++) {
        const float wk = GetWk(xk, yk, zk);
        wA += wk*xk*xk;
        wB += wk*yk*yk;
        wC += wk*zk*zk;
        wD += wk;
      }
    }
  }

  wA = 1.f / wA;
  wB = 1.f / wB;
  wC = 1.f / wC;
  wD = 1.f / wD;
}

void qfeGradient::GetGradient(float *magnitudes, int nrComp, int x, int y, int z, float &A, float &B, float &C, float &D) {
  A = B = C = D = 0.f;

  for (int zk = -1; zk <= 1; zk++) {
    for (int yk = -1; yk <= 1; yk++) {
      for (int xk = -1; xk <= 1; xk++) {
        const float wk = GetWk(xk, yk, zk);
        float mag;
        if (nrComp == 1) {
          mag = magnitudes[GetIndex(x + xk, y + yk, z + zk)];
        } else if (nrComp == 3) {
          float a = magnitudes[GetIndex(x + xk, y + yk, z + zk)*nrComp];
          float b = magnitudes[GetIndex(x + xk, y + yk, z + zk)*nrComp+1];
          float c = magnitudes[GetIndex(x + xk, y + yk, z + zk)*nrComp+2];
          mag = sqrtf(a*a + b*b + c*c);
        }
        A += wk*mag*xk;
        B += wk*mag*yk;
        C += wk*mag*zk;
        D += wk*mag;
      }
    }
  }

  A *= wA;
  B *= wB;
  C *= wC;
  D *= wD;
}

void qfeGradient::GetGradient(float *values, int x, int y, int z, float &A, float &B, float &C, float &D) {
  A = B = C = D = 0.f;

  for (int zk = -1; zk <= 1; zk++) {
    for (int yk = -1; yk <= 1; yk++) {
      for (int xk = -1; xk <= 1; xk++) {
        const float wk = GetWk(xk, yk, zk);
        float value = values[GetIndex(x + xk, y + yk, z + zk)];
        A += wk*value*xk;
        B += wk*value*yk;
        C += wk*value*zk;
        D += wk*value;
      }
    }
  }

  A *= wA;
  B *= wB;
  C *= wC;
  D *= wD;
}

void qfeGradient::GetGradient(unsigned short *magnitudes, int nrComp, int x, int y, int z, float &A, float &B, float &C, float &D) {
  A = B = C = D = 0.f;

  for (int zk = -1; zk <= 1; zk++) {
    for (int yk = -1; yk <= 1; yk++) {
      for (int xk = -1; xk <= 1; xk++) {
        const float wk = GetWk(xk, yk, zk);
        float mag;
        if (nrComp == 1) {
          mag = magnitudes[GetIndex(x + xk, y + yk, z + zk)];
        } else if (nrComp == 3) {
          float a = (float)magnitudes[GetIndex(x + xk, y + yk, z + zk)*nrComp];
          float b = (float)magnitudes[GetIndex(x + xk, y + yk, z + zk)*nrComp+1];
          float c = (float)magnitudes[GetIndex(x + xk, y + yk, z + zk)*nrComp+2];
          mag = sqrtf(a*a + b*b + c*c);
        }
        A += wk*mag*xk;
        B += wk*mag*yk;
        C += wk*mag*zk;
        D += wk*mag;
      }
    }
  }

  A *= wA;
  B *= wB;
  C *= wC;
  D *= wD;
}

int qfeGradient::GetIndex(int x, int y, int z) {
  x = max(0, x); y = max(0, y); z = max(0, z);
  x = min(width-1, x); y = min(height-1, y); z = min(depth-1, z);
  return z * width * height + y * width + x;
}