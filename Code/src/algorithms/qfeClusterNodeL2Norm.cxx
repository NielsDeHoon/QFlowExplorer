#include "qfeClusterNodeL2Norm.h"

//----------------------------------------------------------------------------
//  Constructors
//----------------------------------------------------------------------------
qfeClusterNodeL2Norm::qfeClusterNodeL2Norm(qfeClusterPoint point)
{
  this->size = 1;

  this->meanFlow.qfeSetVectorElements(point.flow[0],point.flow[1],point.flow[2],0.0);
};

//----------------------------------------------------------------------------
//  Destructor
//----------------------------------------------------------------------------
qfeClusterNodeL2Norm::~qfeClusterNodeL2Norm()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeClusterNodeL2Norm::qfeMergeRepresentative(qfeClusterNode *c2)
{
  qfeClusterNodeL2Norm *other = (qfeClusterNodeL2Norm*)c2;

  // TODO: Investigate/minimize effect of limited float accuracy with very different cluster sizes
  qfeFloat thisWeight  = (qfeFloat)(1/(1+(double)other->size/this->size));
  qfeFloat otherWeight = (qfeFloat)(1/(1+(double)this->size/other->size));

  this->meanFlow = thisWeight*this->meanFlow + otherWeight*other->meanFlow;
  this->size    += other->size;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeFloat qfeClusterNodeL2Norm::qfeComputeDistanceTo(qfeClusterNode *c2)
{
  qfeClusterNodeL2Norm *other = (qfeClusterNodeL2Norm*)c2;

  qfeFloat l;
  qfeVector::qfeVectorLength(this->meanFlow-other->meanFlow,l);

  return l;
};