#pragma once

#include "qflowexplorer.h"

#include <vector>
#include <random>

#include "fluidsim\array2_utils.h"

#include "qfeAlgorithmFlow.h"
#include "qfeGLShaderProgram.h"
#include "qfeMatrix4f.h"
#include "qfeTransform.h"
#include "qfeVector.h"
#include "qfeVolume.h"
#include "qfeColorMaps.h"
#include "qfeSeedVolumeCollection.h"
#include "qfe2DBasicRendering.h"
#include "qfeConvert.h"

#include "fluidsim\vec.h"
#include <QtCore>

#include "qfeImageData.h"
#include "vtkFloatArray.h"
#include <vtkXMLImageDataWriter.h>
#include <vtkPolyDataWriter.h>

#include "plot\qfeTemporalGraphPlot.h"

#define MIN(a, b) ((a < b) ? a : b)
#define MAX(a, b) ((a > b) ? a : b) 

#include <ppl.h> //parallel algorithms

//mesh related includes:
#include <vtkCleanPolyData.h>
#include "fluidsim\makelevelset3.h"

#define M_PI 3.14159265358979323846
#define M_E  2.71828182845904523536

#define  P_saturat_r  .299
#define  P_saturat_g  .587
#define  P_saturat_b  .114


/**
* \file   qfeInkParticles.h
* \author Roy van Pelt
* \class  qfeFlowLines
* \brief  Implements rendering of velocity vectors
* \note   Confidential
*
* Rendering of streamlines
*
*/
using namespace std;

typedef std::array<qfePoint,2> InkLine;  
typedef std::array<qfePoint,3> InkVolume;  

typedef struct
{
	float x;
	float y;
	float z;
} gl_vec3;

typedef struct
{
	qfePoint position;
	float age;
	float seed_id1;
	float seed_id2;
	float depth;
	float seed_time;
	float deviation;

	Vec3i initial_voxel;
	int deletion_reason;

	float speed;
} particle;

typedef struct
{
	particle seedPoint;
	std::vector<particle> line;
} guideline;

typedef struct
{
	qfePoint seedPoint;
	float color_id;
}featureSeedPoint;

typedef struct
{
	gl_vec3 position; //basically 3 floats

	GLfloat age;
	GLfloat seedTime;

	GLfloat use_uncertainty;
	GLfloat deviation;
	
	GLfloat initial_voxel_x;
	GLfloat initial_voxel_y;
	GLfloat initial_voxel_z;

	GLfloat deletion_reason;

	GLfloat speed;	//velocity magnitude of the particle [0-1]
	GLfloat padding0;
	GLfloat padding1;
	GLfloat padding2;
	GLfloat padding3;
} gpu_particle;

typedef struct
{
	std::string label;
	float value;
} statistic;

enum advection_type {Stream, Path, Streak };

enum graph_data_type {UncertaintyGraph, SpeedGraph};

inline bool farther(const particle &a, const particle &b) //for sorting
{
	return a.depth > b.depth;

}

inline void normalize(std::array<double, 3> &vector)
{
	float length = sqrt(vector[0]*vector[0] + vector[1]*vector[1] + vector[2]*vector[2]);

	vector[0]/=length;
	vector[1]/=length;
	vector[2]/=length;

	return;
}

inline float dot_product(std::array<double, 3> &vector1, std::array<double, 3> &vector2)
{
	return vector1[0]*vector2[0] + vector1[1]*vector2[1] + vector1[2]*vector2[2];
}

inline std::array<double, 3> cross_product(std::array<double, 3> &vector1, std::array<double, 3> &vector2)
{
	std::array<double, 3> result;

	result[0] = vector1[1] * vector2[2] - vector1[2] * vector2[1];
	result[1] = vector1[2] * vector2[0] - vector1[0] * vector2[2];
	result[2] = vector1[0] * vector2[1] - vector1[1] * vector2[0];

	return result;
}

// This function extracts the hue, saturation, and luminance from "color" 
// and places these values in h, s, and l respectively.
inline qfeColorHLS convertRGBtoHLS(const qfeColorRGB& color)
{
	double r_percent = color.r;
	double g_percent = color.g;
	double b_percent = color.b;

	double max_color = 0;
	if((r_percent >= g_percent) && (r_percent >= b_percent))
		max_color = r_percent;
	if((g_percent >= r_percent) && (g_percent >= b_percent))
		max_color = g_percent;
	if((b_percent >= r_percent) && (b_percent >= g_percent))
		max_color = b_percent;

	double min_color = 0;
	if((r_percent <= g_percent) && (r_percent <= b_percent))
		min_color = r_percent;
	if((g_percent <= r_percent) && (g_percent <= b_percent))
		min_color = g_percent;
	if((b_percent <= r_percent) && (b_percent <= g_percent))
		min_color = b_percent;

	double L = 0;
	double S = 0;
	double H = 0;

	L = (max_color + min_color)/2;

	if(max_color == min_color)
	{
		S = 0;
		H = 0;
	}
	else
	{
		if(L < .50)
		{
			S = (max_color - min_color)/(max_color + min_color);
		}
		else
		{
			S = (max_color - min_color)/(2 - max_color - min_color);
		}
		if(max_color == r_percent)
		{
			H = (g_percent - b_percent)/(max_color - min_color);
		}
		if(max_color == g_percent)
		{
			H = 2 + (b_percent - r_percent)/(max_color - min_color);
		}
		if(max_color == b_percent)
		{
			H = 4 + (r_percent - g_percent)/(max_color - min_color);
		}
	}

	qfeColorHLS hls;
	hls.s = (uint)(S*100);
	hls.l = (uint)(L*100);
	H = H*60;
	if(H < 0)
		H += 360;
	hls.h = (uint)H;

	return hls;
}

// This is a subfunction of HSLtoRGB
inline void convertHSLtoRGB_Subfunction(uint& c, const double& temp1, const double& temp2, const double& temp3)
{
	if((temp3 * 6) < 1)
		c = (uint)((temp2 + (temp1 - temp2)*6*temp3)*100);
	else
	if((temp3 * 2) < 1)
		c = (uint)(temp1*100);
	else
	if((temp3 * 3) < 2)
		c = (uint)((temp2 + (temp1 - temp2)*(.66666 - temp3)*6)*100);
	else
		c = (uint)(temp2*100);
	return;
}

// This function converts the "color" object to the equivalent RGB values of
// the hue, saturation, and luminance passed as h, s, and l respectively
inline qfeColorRGB convertHLStoRGB(qfeColorHLS color)
{
	uint r = 0;
	uint g = 0;
	uint b = 0;

	double L = ((double)color.l)/100;
	double S = ((double)color.s)/100;
	double H = ((double)color.h)/360;

	if(color.s == 0)
	{
		r = color.l;
		g = color.l;
		b = color.l;
	}
	else
	{
		double temp1 = 0;
		if(L < .50)
		{
			temp1 = L*(1 + S);
		}
		else
		{
			temp1 = L + S - (L*S);
		}

		double temp2 = 2*L - temp1;

		double temp3 = 0;
		for(int i = 0 ; i < 3 ; i++)
		{
			switch(i)
			{
			case 0: // red
				{
					temp3 = H + .33333;
					if(temp3 > 1)
						temp3 -= 1;
					convertHSLtoRGB_Subfunction(r,temp1,temp2,temp3);
					break;
				}
			case 1: // green
				{
					temp3 = H;
					convertHSLtoRGB_Subfunction(g,temp1,temp2,temp3);
					break;
				}
			case 2: // blue
				{
					temp3 = H - .33333;
					if(temp3 < 0)
						temp3 += 1;
					convertHSLtoRGB_Subfunction(b,temp1,temp2,temp3);
					break;
				}
			default:
				{

				}
			}
		}
	}
	
	qfeColorRGB rgb;
	rgb.r = (float)r/100.0f;
	rgb.g = (float)g/100.0f;
	rgb.b = (float)b/100.0f;

	return rgb;
}

//RGB saturation, according to: http://alienryderflex.com/saturation.html
//  public-domain function by Darel Rex Finley
//
//  The passed-in RGB values can be on any desired scale, such as 0 to
//  to 1, or 0 to 255.  (But use the same scale for all three!)
//
//  The "ratio" parameter works like this:
//    0.0 creates a black-and-white image.
//    0.5 reduces the color saturation by half.
//    1.0 causes no change.
//    2.0 doubles the color saturation.
//  Note:  A "change" value greater than 1.0 may project your RGB values
//  beyond their normal range, in which case you probably should truncate
//  them to the desired range before trying to use them in an image.
inline void desaturate(qfeColorRGB &color, float ratio)
{
	float P=sqrt(
		(color.r) * (color.r) * P_saturat_r +
		(color.g) * (color.g) * P_saturat_g +
		(color.b) * (color.b) * P_saturat_b);
	
	color.r = P + ((color.r) - P) * ratio;
	color.g = P + ((color.g) - P) * ratio;
	color.b = P + ((color.b) - P) * ratio;
}

//desaturation, not changing the hue and the lightness
inline void desaturateIsoL(qfeColorRGB &color, float ratio)
{
	//convert to HSL:
	qfeColorHLS hls = convertRGBtoHLS(color);

	hls.s *= ratio;

	color = convertHLStoRGB(hls);
}

//brightness change, not changing the hue and the saturation
inline void brightnessIsoS(qfeColorRGB &color, float ratio)
{
	//convert to HSL:
	qfeColorHLS hls = convertRGBtoHLS(color);

	hls.l *= ratio;

	color = convertHLStoRGB(hls);
}

inline float uniform_random_value_0_1()
{
	return (static_cast<float>(rand()) / static_cast<float>(RAND_MAX)); //random value [0-1]
}

class qfeInkParticles : public qfeAlgorithmFlow
{
public:

  qfeInkParticles();
  qfeInkParticles(const qfeInkParticles &sp);
  ~qfeInkParticles();

  qfeSeedVolumeCollection *particleSinks;

  //Provide the code with the study information
  qfeReturnStatus qfeInitialize(qfeStudy *_study);
  //Provide the code with the study information and velocity data to be used
  qfeReturnStatus qfeInitialize(qfeStudy *_study, qfeVolumeSeries *_volumeData);

  unsigned int qfeNumberOfParticles();

  qfeReturnStatus qfeRenderSeedingLine(InkLine line);
  qfeReturnStatus qfeRenderSeedingVolume(InkVolume volume, vtkCamera *camera, int vp_width, int vp_height, bool plane);

  qfeReturnStatus qfeRenderColormapToScreen(int vp_width, int vp_height);
  qfeColorMap *qfeGetSeedVolumeColorMap();
  unsigned int qfeGetSeedVolumeCount();
  
  qfeReturnStatus qfeRenderParticles(std::array<double,3> camera, std::array<double,3> viewUp, std::array<double,3> viewDir, float znear, float zfar, int vp_width, int vp_height, bool mapToOpacityUncertainty, bool depthDarkening, float depthDarkeningAmount, bool depthDarkeningLight, bool chromaDepth, bool aerialPerspective, float particle_size, int particle_type, GLuint baseframeBuffer, unsigned int glyph_type, unsigned int glyph_frequency, bool clipSeedVolume);
  qfeReturnStatus qfeRenderParticlesGPUFriendly(float particle_size, std::array<double,3> viewDir, bool clipSeedVolume, int glyph_type, int glyph_frequency);

  graph_data_type selected_graph_data;
  qfeReturnStatus qfeRenderGraph(unsigned int x_size, unsigned int y_size, graph_data_type data_id, bool invert_graph);

  std::vector<statistic> qfeGetStatistics();

  qfeReturnStatus qfeSetSelectionRange(Vec2f _age_selection, Vec2f _filter_selection, bool invert_graph);
  
  qfeReturnStatus qfeRenderGuideLines(std::array<double,3> viewDir);
  
  qfeReturnStatus qfeRenderCube(qfePoint pos, float size, bool draw_outline, qfeColorRGB color);

  qfeReturnStatus qfeUseSeedingVolume(bool _use_seeding_volume);

  qfeReturnStatus qfeStoreInkDensity();

  qfeReturnStatus qfeSetSeedLine(InkLine _seedPosition);
  qfeReturnStatus qfeSetSeedVolume(InkVolume _seedPosition);
  qfeReturnStatus qfeSetFeatureBasedSeeds(std::vector<featureSeedPoint> _featureSeeds);
    
  qfeReturnStatus qfeGetSeedVolumeDetails(qfeVector &a1, qfeVector &a2, qfeVector &a3, qfePoint &c);

  qfeReturnStatus qfeSetGapSettings(int _spatialGapAmount1, int _spatialGapAmount2, int _temporalGapAmount, float _spatialGapWidth1, float _spatialGapWidth2, float _temporalGapWidth);

  //get number of smoke particles
  unsigned int qfeGetNumberOfInkParticles();

  //get SNR volume data:
  qfeReturnStatus qfeGetSNRVolumes(std::vector<qfeVolume> &volumes, std::array<float, 2> &range, unsigned int dimensions);
  //get Signal Strenght volume data:
  qfeReturnStatus qfeGetSignalStrengthVolumes(std::vector<qfeVolume> &volumes, std::array<float, 2> &range, unsigned int dimensions);
  //get particle density volume data:
  qfeReturnStatus qfeGetParticleDensityVolume(qfeVolume &volume, unsigned int dimensions, bool volumetricSeeding);
  //get mesh levelset volume data
  qfeReturnStatus qfeGetMeshLevelsetVolume(qfeVolume &volume, std::array<float, 2> &range, unsigned int dimensions);
  //get per voxel seeding origin volume data
  qfeReturnStatus qfeGetSeedingOriginVolume(qfeVolume &volume, unsigned int dimensions, unsigned int selected_particle_sink);
  //get per voxel highest probably seeding origin volume data
  qfeReturnStatus qfeGetAllSeedingOriginVolume(qfeVolume &volume, unsigned int dimensions);
  //get per voxel number of particles leaving the mesh
  qfeReturnStatus qfeGetParticlesLeavingMeshVolume(qfeVolume &volume, unsigned int dimensions);

  //obtain a list of the fixed seeding positions that are used
  std::vector<particle>* getFixedSeedPositions();
  //set the list of the fixed seeding positions
  void setFixedSeedPositions(std::vector<particle> *positions);

  //Clip all seeding positions that are not within the seeding region
  qfeReturnStatus qfeClipSeedOutsideMesh(bool _clip_seed_outside_mesh);

  //Clip all particles that leave the mesh with a margin of _clipping_margin
  qfeReturnStatus qfeClipOutsideMesh(bool _clip_outside_mesh, float _clipping_margin);

  //Use the inverse of the color map
  qfeReturnStatus qfeInverseColorMap(bool _inverseColorMap);

  //Select how to color the particles
  qfeReturnStatus qfeSetColorSetting(unsigned int id);

  //set number of guidelines
  qfeReturnStatus qfeSetNumberOfLines(unsigned int _number_of_lines);
  //Current phase, if right in between measurement 0 and 1, then input should be 0.5
  qfeReturnStatus qfeSetPhase(double current); 
  //Set particle life time in ms
  qfeReturnStatus qfeSetParticleLifeTime(double lifeTime);
  //Set time step time in ms
  qfeReturnStatus qfeSetTimeStepSize(double timeStep);
  //Get time step in ms
  double qfeGetTimeStepSize();
  //Stop seeding particles after stopSeeding, -1 is don't stop
  qfeReturnStatus qfeSetStopSeedingTime(double stopSeeding);

  //Whether uncertainty information should be used
  qfeReturnStatus qfeUseUncertainty(bool _uncertainty);

  //Set advection type
  qfeReturnStatus qfeSetAdvectionType(advection_type _advection);
  //qfeReturnStatus qfeSetNumberOfColorBands(unsigned int _colorBands);
  qfeReturnStatus qfeSetParticleMultiplier(float _multiplier);

  qfeReturnStatus qfeSetParticleOpacity(float _opacity);
	
  qfeReturnStatus qfeSeedParticles(InkLine line, bool seedFromVoxelCenter, bool seedFromFixedPositions, float seedFromFixedPositionsMultiplier);
  qfeReturnStatus qfeSeedParticles(InkVolume volume, bool seedFromVoxelCenter, bool seedFromFixedPositions, float seedFromFixedPositionsMultiplier, bool plane);
  qfeReturnStatus qfeSeedParticlesWholeMesh(bool seedFromVoxelCenter, bool seedFromFixedPositions, float seedFromFixedPositionsMultiplier);
  qfeReturnStatus qfeSeedParticles(std::vector<featureSeedPoint> seedPositions);

  //computes the new position of all particles
  qfeReturnStatus qfeComputeNextTimeStep(bool volumetric, bool seedFromVoxelCenter, bool seedFromFixedPositions, float seedFromFixedPositionsMultiplier, bool planar, bool seedWholeMesh);

  //clears all particles
  qfeReturnStatus qfeClearParticles();
  //clears all particles that are seeded in a feature based manner
  qfeReturnStatus qfeClearParticlesFeatureBased();

  qfeColorRGB qfeInterpolateColor(float index);
  qfeColorRGB qfeGetColor(particle *part);

  //computes the CFL time step size (a particle can move at most one cell per step)
  //scale can be used to use an even smaller time step
  qfeReturnStatus qfeComputeCFL(float scale);
  
  //set color map
  qfeReturnStatus qfeUpdateColorMap(qfeColorMap *colormap);
  
  void showTimeLinePlot();

  qfePoint volumeCenter;
	
  inline void depth_sort_particles(std::vector<particle> *particles, std::array<double,3> camera)
  {
	  //get nearest corner (equals nearest distance to volume)

	  //place camera on sphere to get consistent depth:
	  qfeVector vect(camera[0] - volumeCenter.x, camera[1] - volumeCenter.y, camera[2] - volumeCenter.z);
	  float length = sqrt(vect[0]*vect[0] + vect[1]*vect[1] + vect[2]*vect[2]);
	  qfeVector::qfeVectorNormalize(vect);

	  //scale:
	  vect[0] *= max_dist/2.0f;
	  vect[1] *= max_dist/2.0f;
	  vect[2] *= max_dist/2.0f;

	  qfePoint newCamera(volumeCenter.x+vect[0], volumeCenter.y+vect[1], volumeCenter.z+vect[2]);

	  //initialize data structure
	  for(unsigned int i = 0; i<particles->size(); i++)
	  {
		  qfePoint position = particles->at(i).position;
		  //use the projection matrix to compute the depth:
		  particles->at(i).depth = sqrt(
			  (position.x - newCamera.x) * (position.x - newCamera.x) + 
			  (position.y - newCamera.y) * (position.y - newCamera.y) + 
			  (position.z - newCamera.z) * (position.z - newCamera.z));
	  }
	  
	  concurrency::parallel_sort(particles->begin(), particles->end(), farther);
  }

protected:

  qfeReturnStatus qfeBindTextures(qfeVolumeSeries *series, qfeColorMap *colormap);
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

  qfeReturnStatus qfeComputeParticles();

private :
	unsigned int total_number_of_particles;
	unsigned int number_of_particles_removed_age;
	unsigned int number_of_particles_removed_domain;

	Array3ui particlesSeededFromVoxel;
	Array3ui particlesLeavingMeshFromVoxel;
	bool is_data_masked;
	Array3b masked_voxels;
	  
  std::vector<std::pair<int, Array3i>> reason_particles_removed; //stores the index of the seed volume and a voxel grid
  void clearReasonParticlesRemoved();
  void countReasonParticlesRemoved(int reason, Vec3i initial_voxel);

  float dx;

  double cfl;
  double particleLifeTime;
  double stopSeedingTime;
  double venc;
  double currentTime;
  double internalTime;
  double timeStepSize; 
  double phaseDuration;
  double max_dist;
  float particleMultiplier;
  float particleOpacity;
  bool use_uncertainty;
  bool use_seeding_volume;
  bool inverseColorMap;
  bool clip_seed_outside_mesh;
  bool clip_outside_mesh;
  float clipping_margin;
  GLuint colorMapTextureId;
  qfeSeedCylinder seedVolume;

  int spatialGapAmount1;
  int spatialGapAmount2;
  int temporalGapAmount;
  float spatialGapWidth1;
  float spatialGapWidth2;
  float temporalGapWidth;

  bool valid_selection_range;
  Vec2f age_selection;
  Vec2f filter_selection;

  Array3f mesh_level_set;
  unsigned int inside_mesh_voxel_count;

  std::array<GLfloat,256*3> colorMapping;
  qfeColorMap seedVolumeColorMap;

  unsigned int color_setting;

  advection_type advection;

  std::array<qfePoint,8> volumeCorners; //in patient coordinates
  std::array<qfeVector,6> volumeNormals; //in patient coordinates

  vector<qfeColorRGB> colors;
  unsigned int color_size;
  //unsigned int nr_color_bands;

  unsigned int number_of_lines;

  InkLine seedPosition;
  InkVolume seedVolumePoints;
  std::vector<featureSeedPoint> featureBasedSeeds;

  std::vector<guideline> guideLines;

  qfeVolume flowTextureVolume;
  qfeVolume SNRTextureVolume;  

  qfeVolume meshSDFTextureVolume;
  bool meshSDFTextureVolumeExists;

  std::array<float, 3> spacing; 
  std::array<unsigned int, 3> volumeSize;

  std::vector<Array3f>	SNR_data; 
  std::vector<float>	sigma_data;

  bool                firstRender;
  qfeGLShaderProgram *shaderComputeParticles;

  qfeGLShaderProgram renderShader;

  // Static uniform locations
  qfeMatrix4f P2V, T2V, V2P, V2T;
  qfeStudy    *study;

  qfeVolumeSeries *volumeData;

  std::vector<particle> particles; //stores the particles in voxel coordinates

  qfeReturnStatus qfeAdvectParticles(float dt);
  
  qfeReturnStatus qfeSetUpFBO(int vp_width, int vp_height, GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &depth_buffer_id);
  qfeReturnStatus qfeDisableFBO(int vp_width, int vp_height, GLuint base_fbo_id);
  qfeReturnStatus qfeEnableFBO(int vp_width, int vp_height, GLuint fbo_id);
  qfeReturnStatus qfeDeleteFBO(int vp_width, int vp_height, GLuint fbo_id, GLuint fbo_texture_id, GLuint depth_buffer_id, GLuint base_fbo_id);

  void createTexture(GLuint *textureName, GLenum pname1, GLenum pname2, GLenum pname3, GLenum pname4, GLfloat param1, GLfloat param2, GLfloat param3, GLfloat param4, GLint level, GLenum format, GLenum type, int screenSize_x, int screenSize_y,GLfloat *data)
  {
	  if (*textureName > 0)
	  {
		  glDeleteTextures(1, textureName);
	  }
	  glGenTextures(1, textureName);
	  glBindTexture(GL_TEXTURE_2D, *textureName);
	  glTexParameterf(GL_TEXTURE_2D, pname1, param1);
	  glTexParameterf(GL_TEXTURE_2D, pname2, param2);
	  glTexParameterf(GL_TEXTURE_2D, pname3, param3);
	  glTexParameterf(GL_TEXTURE_2D, pname4, param4);
	  glTexImage2D(GL_TEXTURE_2D, 0, level, screenSize_x, screenSize_y, 0, format, type, data);
	  glBindTexture(GL_TEXTURE_2D, 0);
  }

  void bindImage(GLuint texid, int texunit,GLenum format)
  {
	  glBindImageTexture(texunit, texid, 0, GL_FALSE, 0, GL_READ_WRITE, format);
  }

  void updateColorMap();

  qfeReturnStatus qfeRenderTextureToScreen(int vp_width, int vp_height, GLuint texture);

  float GaussianNoise();
  float GaussianNoise(float mu, float sigma);

  qfeVector interpolateVelocity(qfePoint position, float time);
  qfeVector interpolateVelocityUncertain(qfePoint position, float time, float time_step);

  //shader setup code:
  qfeReturnStatus qfeBindTextures();

  qfeReturnStatus qfeConvertGpuParticleToCpuParticle(gpu_particle gpu_part, particle &cpu_part);
  qfeReturnStatus qfeConvertCpuParticleToGpuParticle(particle cpu_part, gpu_particle &gpu_part, bool _use_uncertainty);

  qfeTemporalGraphPlot timeLinePlot;
  void updateTimeLinePlot();

  bool fixedPositionsComputed;
  float fixedPositionsMultiplier;
  std::vector<particle> fixedSeedPositions;

  bool IsInFrontOfClippingPlane(const qfePoint &position, qfeVector clipNormal, qfePoint planePoint);
};
