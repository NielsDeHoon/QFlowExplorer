#pragma once

#include <assert.h>

#include "qflowexplorer.h"
#include "qfeCenterline.h"
#include "qfeMatrix4f.h"
#include "qfeDisplay.h"

#include "fluidsim/array3.h"
#include "fluidsim/vec.h"

/**
* \file   qfeCenterline.h
* \author Niels de Hoon
* \class  qfeCenterline
* \brief  Implements a centerline computation
* \note   Confidential
*
* Computation of centerline for a given signed distance field
*
*/

class qfeCenterline
{
public:		
	qfeCenterline() //constructor
	{
	};

	~qfeCenterline()  //destructor
	{};

	void qfeInit(Array3f _solid, float _dx, qfeMatrix4f _V2P, qfeMatrix4f _V2T, qfeMatrix4f _T2V);

	void qfeRenderCenterline();

	void qfeGetNearestCenterlinePoint(Vec3ui inputCell, Vec3ui &outputCell);
	
private:	
	void computeCenterline();
	unsigned int computeNumberOfComponents();

	static const unsigned int N6_size = 6;
	static const Vec3i N6[];
	
	static const unsigned int N26_size = 26;
	static const Vec3i N26[];

	float dx;

	qfeMatrix4f V2P, V2T, T2V;
	Array3f solid;

	Array3b centerline;

	bool centerline_computed;
};