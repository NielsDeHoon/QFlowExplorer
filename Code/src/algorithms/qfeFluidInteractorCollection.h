#pragma once

#include <assert.h>

#include "qflowexplorer.h"
#include "qfeFlowProbe2D.h"
#include "qfeFluidInteractor.h"

#include "fluidsim/array3.h"

/**
* \file   qfeSimulation.h
* \author Niels de Hoon
* \class  qfeFluidInteractor
* \brief  Implements a fluid interactor collection
* \note   Confidential
*
* Computetion of fluid simulation for coupling with measured data
*
*/

class qfeFluidInteractorCollection
{
public:		
	enum CellValue { UNDEF, VALID, SOURCE, SINK, SOURCE_REACHABLE, SINK_REACHABLE, SOLID };
	typedef CellValue CellType;

	qfeFluidInteractorCollection()
	{
		qfeMatrix4f empty;
		init(Array3f(0,0,0), 0.0f, empty, empty, empty);
	};

	void init(Array3f _solid, float dx, qfeMatrix4f _V2P, qfeMatrix4f _V2T, qfeMatrix4f _T2V);

	~qfeFluidInteractorCollection()
	{};
	
	void setV2PMatrix(qfeMatrix4f _V2P);
	void setV2TMatrix(qfeMatrix4f _V2T);
	void setT2VMatrix(qfeMatrix4f _T2V);

	void setSelectedInteractorIndex(unsigned int _id);
	unsigned int getSelectedInteractorIndex();
	unsigned int getNumberOfInteractors();
	unsigned int size();
	qfeFluidInteractor at(unsigned int i);

	void disableCreator();
	void enableCreator(qfeFluidInteractor *interactor);
	bool creatorEnabled();

	qfeReturnStatus add(qfeFluidInteractor interactor);
	qfeReturnStatus remove(unsigned int index);
	qfeReturnStatus removeSelected();

	qfeReturnStatus selectLastCreated();
	
	qfeFluidInteractor *qfeGetFluidInteractor(unsigned int _id);
	qfeFluidInteractor *qfeGetFluidInteractorSelector();

	//rendering functionalities
	void renderAll();

	//interactor properties:
	Array3b qfeGetFluidInteractorCells(qfeFluidInteractor *interactor);
	bool qfeIsValidFluidInteractor(qfeFluidInteractor *interactor);

	qfeFluidInteractor* qfeGetFluidInteractorInCell(unsigned int i, unsigned int j, unsigned int k);

	int qfeCellContainsFluidInteractor(unsigned int i, unsigned int j, unsigned int k);
	
	Array3<CellType> qfeGetCellTypes();

	Array3b qfeGetValidFluidCells();

private:
	qfeFluidInteractor				*visFluidInteractorSelector;
	std::vector<qfeFluidInteractor>  visFluidInteractors;

	Array3f solid; // to compute the covering cells
	Array3ui covered_cells;

	float dx;

	unsigned int selectedID;

	qfeMatrix4f V2P, V2T, T2V;

	void computePolygon(qfeFluidInteractor *interactor);

	/**
	Computes an approximation of where the solid is crossed when traversing the line given by direction from point
	**/
	qfePoint computeSolidCrossing(qfePoint point, qfeVector direction);

	//inspect the given cell and return a new queue
	std::vector<Vec3ui> inspectNeighbors(Vec3ui cell, Array3<CellType> *cells, qfeFluidInteractor::InteractorType type);
};