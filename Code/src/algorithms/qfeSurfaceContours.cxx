#include "qfeSurfaceContours.h"

//----------------------------------------------------------------------------
qfeSurfaceContours::qfeSurfaceContours()
{
  //this->shaderProgram = new qfeGLShaderProgram();
  //this->shaderProgram->qfeAddShaderFromFile("/shaders/ifr/contour-vert.glsl", QFE_VERTEX_SHADER);
  //this->shaderProgram->qfeAddShaderFromFile("/shaders/ifr/contour-geom.glsl", QFE_GEOMETRY_SHADER);
  //this->shaderProgram->qfeAddShaderFromFile("/shaders/ifr/contour-frag.glsl", QFE_FRAGMENT_SHADER);
  ////this->shaderProgram->qfeSetGeometryShaderInOut(GL_TRIANGLES, GL_TRIANGLE_STRIP, 3);  
  //this->shaderProgram->qfeLink();
};

//----------------------------------------------------------------------------
qfeSurfaceContours::qfeSurfaceContours(const qfeSurfaceContours &contours)
{
  this->pointID  = contours.pointID;
	this->offset   = contours.offset;

  //this->shaderProgram = contours.shaderProgram;
};

//----------------------------------------------------------------------------
qfeSurfaceContours::~qfeSurfaceContours()
{
  //delete this->shaderProgram;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfeRenderContour(vtkPolyData *mesh, qfeVolume *volume, qfeColorRGBA color, double lineWidth)
{
  return this->qfeRenderContourCPU(mesh, volume, color, lineWidth);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfeRenderContourCPU(vtkPolyData *mesh, qfeVolume *volume, qfeColorRGBA color, double lineWidth)
{
  qfeMatrix4f   V2P;
  GLfloat       matrix[16];
  vector<float> ndotv;
  vector<float> kr;

  if(mesh == NULL || volume == NULL) return qfeError;

  //this->qfeComputePerView(mesh, volume, ndotv, kr);
  this->qfeUpdateSurfaceProperties(mesh, volume, ndotv, kr);

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);    
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]);  

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in voxel coordinates
  glMultMatrixf(&matrix[0]);

  glLineWidth(lineWidth);

  this->qfeEnableSmoothing();

  this->qfeDrawContour(mesh, ndotv, kr, color);

  this->qfeDisableSmoothing();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfeRenderContourGPU(vtkPolyData *mesh, qfeVolume *volume, qfeColorRGBA color, double lineWidth)
{
  /*
  qfeMatrix4f   V2P;
  GLfloat       matrix[16];
  vector<float> ndotv;
  vector<float> kr;

  if(mesh == NULL || volume == NULL) return qfeError;
  
  //this->qfeUpdateSurfaceProperties(mesh, volume, ndotv, kr);

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);    
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]);  

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in voxel coordinates
  glMultMatrixf(&matrix[0]);

  glLineWidth(lineWidth);

  //this->qfeEnableSmoothing();

  //this->qfeDrawContour(mesh, ndotv, kr, color);

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  this->shaderProgram->qfeEnable();

  this->qfeDrawMesh(mesh, color);

  this->shaderProgram->qfeDisable();

  glDisable(GL_CULL_FACE);

  //this->qfeDrawEdgeMesh(this->edgeMeshOutput);

  //this->qfeDisableSmoothing();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  */
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfeRenderContourHidden(vtkPolyData *mesh, qfeVolume *volume, qfeColorRGBA color, double lineWidth)
{
  glDisable(GL_DEPTH_TEST);

  this->qfeRenderContour(mesh, volume, color, lineWidth);

  glEnable(GL_DEPTH_TEST);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfeDrawContour(vtkPolyData *mesh, vector<qfeFloat> ndotv, vector<qfeFloat> kr, qfeColorRGBA color)
{
  vtkCellArray *strips   = mesh->GetStrips();

  if(strips == NULL )
  {
    cout << "qfeSurfaceContours::qfeDrawIsoLines - Could not read mesh information" << endl;
    return qfeError;
  }
  vtkIdType nPts = 0;
  vtkIdType *ptIds = strips->GetPointer();

  int index=0;
  for (strips->InitTraversal(); strips->GetNextCell(nPts,ptIds);)
  {
    for (int j = 0; j < nPts - 2; j++)
    {
      const float &v0 = ndotv.at(ptIds[j]);
      const float &v1 = ndotv.at(ptIds[j+1]);
      const float &v2 = ndotv.at(ptIds[j+2]);

      if ((v0 > 0.0f || v1 > 0.0f || v2 > 0.0f) && (v0 < 0.0f || v1 < 0.0f || v2 < 0.0f))
      {
        glBegin(GL_LINES);
        this->qfeDrawFaceIsoLine(mesh, ptIds[j], ptIds[j+1], ptIds[j+2], ndotv, kr, color);
        glEnd();
      }
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfeDrawMesh(vtkPolyData *mesh, qfeColorRGBA color)
{
  if(mesh == NULL) return qfeError;

  //mesh->GetCellEdgeNeighbors(

  vtkCellArray *strips     = mesh->GetStrips();
  vtkPoints    *points     = mesh->GetPoints();
  vtkDataArray *normals    = mesh->GetPointData()->GetNormals();
  vtkDataArray *curv_max   = mesh->GetPointData()->GetArray("Maximum_Curvature_Vector");
  vtkDataArray *curv_min   = mesh->GetPointData()->GetArray("Minimum_Curvature_Vector");
  vtkDataArray *curv_princ = mesh->GetPointData()->GetArray("Principal_Curvatures");

  if(strips == NULL || points == NULL || normals == NULL)
  {
    cout << "qfeDriverIFR::qfeDrawMesh - Could not read mesh information" << endl;
    return qfeError;
  }

  vtkIdType nPts = 0;
  vtkIdType *ptIds = strips->GetPointer();

  int index=0;
  for (strips->InitTraversal(); strips->GetNextCell(nPts,ptIds);)
  {
    glBegin(GL_TRIANGLE_STRIP);
    for (int j = 0; j < nPts; j++)
    {
      double v[3], n[3], c[2], min[3], max[3];

      points->GetPoint(ptIds[j], v);
      normals->GetTuple(ptIds[j], n);
      curv_max->GetTuple(ptIds[j], max);
      curv_min->GetTuple(ptIds[j], min);
      curv_princ->GetTuple(ptIds[j],c);

      glColor3f(color.r, color.g, color.b);
      glMultiTexCoord3f(GL_TEXTURE1, max[0], max[1], max[2]);
      glMultiTexCoord3f(GL_TEXTURE2, min[0], min[1], min[2]);
      glMultiTexCoord2f(GL_TEXTURE3, c[0], c[1]);
      glNormal3f(n[0], n[1], n[2]);
      glVertex3f (v[0], v[1], v[2]);
    }
    glEnd();
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfeDrawFaceIsoLine(vtkPolyData *mesh, int v0, int v1, int v2, const vector<float> &ndotv, const vector<float> &kr, qfeColorRGBA color)
{
  // Quick reject if derivs are negative
  if (-1.0*kr.at(v0) <= 0.0f && -1.0*kr.at(v1) <= 0.0f && -1.0*kr.at(v2) <= 0.0f)
    return qfeError;

  // Figure out which val has different sign, and draw
  if (ndotv.at(v0) < 0.0f && ndotv.at(v1) >= 0.0f && ndotv.at(v2) >= 0.0f ||
    ndotv.at(v0) > 0.0f && ndotv.at(v1) <= 0.0f && ndotv.at(v2) <= 0.0f)
    this->qfeDrawFaceIsoLine2(mesh, v0, v1, v2, ndotv, kr, color);

  else if (ndotv.at(v1) < 0.0f && ndotv.at(v2) >= 0.0f && ndotv.at(v0) >= 0.0f ||
    ndotv.at(v1) > 0.0f && ndotv.at(v2) <= 0.0f && ndotv.at(v0) <= 0.0f)
    this->qfeDrawFaceIsoLine2(mesh, v1, v2, v0, ndotv, kr, color);

  else if (ndotv.at(v2) < 0.0f && ndotv.at(v0) >= 0.0f && ndotv.at(v1) >= 0.0f ||
    ndotv.at(v2) > 0.0f && ndotv.at(v0) <= 0.0f && ndotv.at(v1) <= 0.0f)
    this->qfeDrawFaceIsoLine2(mesh, v2, v0, v1, ndotv, kr, color);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfeDrawFaceIsoLine2(vtkPolyData *mesh, int v0, int v1, int v2, const vector<float> &ndotv, const vector<float> &kr, qfeColorRGBA color)
{
  vtkPoints    *points   = mesh->GetPoints();
  double        fade     = 0.0;

  // How far along each edge?
  float w10 = ndotv.at(v0) / (ndotv.at(v0) - ndotv.at(v1));
  float w01 = 1.0f - w10;
  float w20 = ndotv.at(v0) / (ndotv.at(v0) - ndotv.at(v2));
  float w02 = 1.0f - w20;

  // Points along edges
  double vec0[3], vec1[3], vec2[3];
  points->GetPoint(v0, vec0);
  points->GetPoint(v1, vec1);
  points->GetPoint(v2, vec2);

  qfeVector vec_0, vec_1, vec_2;
  vec_0.qfeSetVectorElements(vec0[0], vec0[1], vec0[2], 1.0);
  vec_1.qfeSetVectorElements(vec1[0], vec1[1], vec1[2], 1.0);
  vec_2.qfeSetVectorElements(vec2[0], vec2[1], vec2[2], 1.0);

  qfeVector p1 = vec_0*w01 + vec_1*w10;
  qfeVector p2 = vec_0*w02 + vec_2*w20;

  float test_num1 = 1.0f, test_num2 = 1.0f;
  float test_den1 = 1.0f, test_den2 = 1.0f;
  float z1 = 0.0f, z2 = 0.0f;
  bool valid1 = true;

  // Interpolate to find value of test at p1, p2
  test_num1 = w01 * -1.0*kr.at(v0) + w10 * -1.0*kr.at(v1);
  test_num2 = w02 * -1.0*kr.at(v0) + w20 * -1.0*kr.at(v2);

  // First point is valid iff num1/den1 is positive,
  // i.e. the num and den have the same sign
  valid1 = ((test_num1 >= 0.0f) == (test_den1 >= 0.0f));

  // There are two possible zero crossings of the test,
  // corresponding to zeros of the num and den
  if ((test_num1 >= 0.0f) != (test_num2 >= 0.0f))
    z1 = test_num1 / (test_num1 - test_num2);
  if ((test_den1 >= 0.0f) != (test_den2 >= 0.0f))
    z2 = test_den1 / (test_den1 - test_den2);

  // Sort and order the zero crossings
  if (z1 == 0.0f)
    z1 = z2, z2 = 0.0f;
  else if (z2 < z1)
    swap(z1, z2);

  // If the beginning of the segment was not valid, and
  // no zero crossings, then whole segment invalid
  if (!valid1 && !z1 && !z2)
    return qfeError;

  // Draw the valid piece(s)
  int npts = 0;
  if (valid1) {
    glColor4f(color.r, color.g, color.b, test_num1 / (test_den1 * fade + test_num1));
    glVertex3f(p1.x, p1.y, p1.z);
    npts++;
  }
  if (z1) {
    float num = (1.0f - z1) * test_num1 + z1 * test_num2;
    float den = (1.0f - z1) * test_den1 + z1 * test_den2;
    glColor4f(color.r, color.g, color.b, num / (den * fade + num));
    glVertex3f((1.0f - z1) * p1.x + z1 * p2.x, (1.0f - z1) * p1.y + z1 * p2.y, (1.0f - z1) * p1.z + z1 * p2.z);
    npts++;
  }
  if (z2) {
    float num = (1.0f - z2) * test_num1 + z2 * test_num2;
    float den = (1.0f - z2) * test_den1 + z2 * test_den2;
    glColor4f(color.r, color.g, color.b, num / (den * fade + num));
    glVertex3f((1.0f - z2) * p1.x + z2 * p2.x, (1.0f - z2) * p1.y + z2 * p2.y, (1.0f - z2) * p1.z + z2 * p2.z);
    npts++;
  }
  if (npts != 2) {
    glColor4f(color.r, color.g, color.b, test_num2 / (test_den2 * fade + test_num2));
    glVertex3f(p2.x, p2.y, p2.z);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeSurfaceContours::qfeUpdateSurfaceProperties(vtkPolyData *mesh, qfeVolume *volume, vector<qfeFloat> &ndotv, vector<qfeFloat> &kr)
{
  GLfloat modelview[16];
  qfeMatrix4f MV, MVinv, P2V, M;
  qfeVector   view;

  view.qfeSetVectorElements(0.0,0.0,-1.0,0.0);

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f::qfeSetMatrixElements(MV, modelview);
  qfeMatrix4f::qfeGetMatrixInverse(MV, MVinv);

  M = MVinv*P2V;

  qfeMatrix4f::qfeMatrixPreMultiply(view, M, view);
  qfeVector::qfeVectorNormalize(view);

  //this->viewVector = view;

  vtkPoints    *points   = mesh->GetPoints();
  vtkDataArray *normals  = mesh->GetPointData()->GetNormals();
  vtkDataArray *curv_max = mesh->GetPointData()->GetArray("Maximum_Curvature_Vector");
  vtkDataArray *curv_min = mesh->GetPointData()->GetArray("Minimum_Curvature_Vector");
  vtkDataArray *curv_val = mesh->GetPointData()->GetArray("Principal_Curvatures");

  ndotv.clear();
  kr.clear();

  double n[3], k1[3], k2[3], val[2];
  qfeVector normal, dir1, dir2;
  qfeFloat  val1, val2;

  vtkIdType nPts = points->GetNumberOfPoints();

  int index=0;
  for (int i = 0; i < nPts; i++)
  {
    // Compute diffuse lighting component ndotv for current mesh
    normals->GetTuple(i, n);
    normal.qfeSetVectorElements(n[0], n[1], n[2], 0.0);
    qfeVector::qfeVectorNormalize(normal);

    ndotv.push_back(normal*view);

    // Compute radial curvature kr for current mesh
    curv_max->GetTuple(i, k1);
    curv_min->GetTuple(i, k2);
    curv_val->GetTuple(i, val);
    dir1.qfeSetVectorElements(k1[0], k1[1], k1[2],0.0); // \todo homogeneous?!
    dir2.qfeSetVectorElements(k2[0], k2[1], k2[2],0.0);
    qfeVector::qfeVectorNormalize(dir1);
    qfeVector::qfeVectorNormalize(dir2);
    val1 = val[0];
    val2 = val[1];

    qfeFloat u = dir1 * view;
    qfeFloat v = dir2 * view;

    qfeFloat u2 = u*u;
    qfeFloat v2 = v*v;

    // Note:  this is actually Kr * sin^2 theta
    kr.push_back(val1 * u2 + val2 * v2);
  }
}

//----------------------------------------------------------------------------
void qfeSurfaceContours::qfeEnableSmoothing()
{
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);
  glDepthMask(GL_FALSE);
}

//----------------------------------------------------------------------------
void qfeSurfaceContours::qfeDisableSmoothing()
{
  glDepthMask(GL_TRUE);
  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfePerformPreprocessing(vtkPolyData *mesh)
{  
  // Initializing of some local data structures
	int numInputPolyDataCells  = mesh->GetNumberOfCells();	
	int numInputPolyDataPoints = mesh->GetNumberOfPoints();
	
	vtkSmartPointer<vtkIdList> neighbors = vtkSmartPointer<vtkIdList>::New();
	neighbors->Allocate(VTK_CELL_SIZE);
	vtkSmartPointer<vtkIdList> cellPointIds = vtkSmartPointer<vtkIdList>::New();
  
	qfeInitDataStructures(mesh, numInputPolyDataPoints);
	
	vtkIdType v0;
	vtkIdType v1;
	vtkIdType v2;

	for (vtkIdType cellID = 0; cellID < numInputPolyDataCells; cellID++)
	{
  
		// Fetch vertices of the current cell
		mesh->GetCellPoints(cellID, cellPointIds);
 
		v0 = cellPointIds->GetId(0);
		v1 = cellPointIds->GetId(1);
		v2 = cellPointIds->GetId(2);

		// Test if cell has valid neighbors at given cell edge.
		// ToDo: Treat the case, if edge has only one neighbor face ==> border edge!!!
		mesh->GetCellEdgeNeighbors(cellID, v0, v1, neighbors);		
		qfeBuildEdgeMeshForFeatureLines(v0, v1, cellID, neighbors->GetId(0));
		neighbors->Reset();

		mesh->GetCellEdgeNeighbors(cellID, v1, v2, neighbors);
		qfeBuildEdgeMeshForFeatureLines(v1, v2, cellID, neighbors->GetId(0));
		neighbors->Reset();

		mesh->GetCellEdgeNeighbors(cellID, v0, v2, neighbors);
		qfeBuildEdgeMeshForFeatureLines(v0, v2, cellID, neighbors->GetId(0));		
	}

	this->qfeBuildFeatureLinesOutput();
	
	//_preprocessingFinished = true;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfeInitDataStructures(vtkPolyData *mesh, int numPoints)
{
  pointID = 0;

	// Global data structures for the edge mesh construction of the feature lines
	inputPolyDataPts          = mesh->GetPoints();
	inputPolyDataCellNormals  = mesh->GetCellData()->GetNormals();
	inputPolyDataPointNormals = mesh->GetPointData()->GetNormals();

	poPointLocator = vtkSmartPointer<vtkKdTree>::New();
	poPointLocator->SetDataSet(mesh);
	poPointLocator->BuildLocatorFromPoints(inputPolyDataPts);

	edgeMeshFeatureLinesTable = vtkSmartPointer<vtkEdgeTable>::New();
	edgeMeshFeatureLinesTable->InitEdgeInsertion(numPoints);

	edgeMeshFeatureLinesPts = vtkSmartPointer<vtkPoints>::New();

	edgeMeshFeatureLinesCells = vtkSmartPointer<vtkCellArray>::New();

	edgeMeshFeatureLinesCellNormals = vtkFloatArray::New();
	edgeMeshFeatureLinesCellNormals->SetNumberOfComponents(3);

	edgeMeshFeatureLinesTangents = vtkSmartPointer<vtkFloatArray>::New();
	edgeMeshFeatureLinesTangents->SetNumberOfComponents(3);
	edgeMeshFeatureLinesTangents->SetName("TangentVectors");

	edgeMeshFeatureLinesNeighbors = vtkSmartPointer<vtkFloatArray>::New();
	edgeMeshFeatureLinesNeighbors->SetNumberOfComponents(3);
	edgeMeshFeatureLinesNeighbors->SetName("NeighborVertices");

	edgeMeshFeatureLinesVertexNormals01 = vtkSmartPointer<vtkFloatArray>::New();
	edgeMeshFeatureLinesVertexNormals01->SetNumberOfComponents(3);
	edgeMeshFeatureLinesVertexNormals01->SetName("VertexNormals01");

	edgeMeshFeatureLinesVertexNormals02 = vtkSmartPointer<vtkFloatArray>::New();
	edgeMeshFeatureLinesVertexNormals02->SetNumberOfComponents(3);
	edgeMeshFeatureLinesVertexNormals02->SetName("VertexNormals02");

	edgeMeshFeatureLinesIDs = vtkSmartPointer<vtkFloatArray>::New();
	edgeMeshFeatureLinesIDs->SetNumberOfComponents(1);
	edgeMeshFeatureLinesIDs->SetName("NeighborIDs");

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfeBuildEdgeMeshForFeatureLines(vtkIdType v1, vtkIdType v2, int currentCellID, int neighboredCellID)
{
  // If the give edge is not already traversed, insert it and construct edge mesh for this edge.
	if (edgeMeshFeatureLinesTable->IsEdge(v1,v2) == -1)
	{
    
		edgeMeshFeatureLinesTable->InsertEdge(v1,v2);
		// Fetch the two normals of the adjacent faces.
		double *cellNormal01 = new double[3];
		inputPolyDataCellNormals->GetTuple(currentCellID, cellNormal01);
		double *cellNormal02 = new double[3];
		inputPolyDataCellNormals->GetTuple(neighboredCellID, cellNormal02);

		double *vertexNormal01 = new double[3];
		inputPolyDataPointNormals->GetTuple(v1, vertexNormal01);
		double *vertexNormal02 = new double[3];
		inputPolyDataPointNormals->GetTuple(v2, vertexNormal02);

		double *posV1 = new double[3];
		inputPolyDataPts->GetPoint(v1, posV1);
		double *posV2 = new double[3];
		inputPolyDataPts->GetPoint(v2, posV2);

		// Two lists with point ID's, which describes the topology of the two triangles (cells).
		vtkSmartPointer<vtkIdList> pointIDList01 = vtkSmartPointer<vtkIdList>::New();

		// Every edge is constructed from four vertices consisted of the two edge vertices
		// of the original edge. These four vertices define two degenerated cells (triangles).
		// The vertex positions are later modified (in the vertex program) so that the edge mesh 
		// triangles only appear where a special edge should be seen.

		edgeMeshFeatureLinesPts->InsertPoint(pointID, posV1);
		edgeMeshFeatureLinesNeighbors->InsertNextTuple(posV2);
		edgeMeshFeatureLinesCellNormals->InsertNextTuple(cellNormal01);
		edgeMeshFeatureLinesTangents->InsertNextTuple(cellNormal02);
		edgeMeshFeatureLinesVertexNormals01->InsertNextTuple(vertexNormal01);
		edgeMeshFeatureLinesVertexNormals02->InsertNextTuple(vertexNormal02);
		edgeMeshFeatureLinesIDs->InsertValue(pointID, 0);
		pointIDList01->InsertId(0, pointID);
		  pointID++;

		//cout << "inputMeshPts->GetPoint(v1) " << inputMeshPts->GetPoint(v1)[0] << ", " << inputMeshPts->GetPoint(v1)[1] << ", " << inputMeshPts->GetPoint(v1)[2] << endl;
		//cout << "inputMeshPts->GetPoint(v2) " << inputMeshPts->GetPoint(v2)[0] << ", " << inputMeshPts->GetPoint(v2)[1] << ", " << inputMeshPts->GetPoint(v2)[2] << endl;
		//cout << "posV1 " << posV1[0] << ", " << posV1[1] << ", " << posV1[2] << endl;
		//cout << "posV2 " << posV2[0] << ", " << posV2[1] << ", " << posV2[2] << endl;

		edgeMeshFeatureLinesPts->InsertPoint(pointID, posV1);
		edgeMeshFeatureLinesNeighbors->InsertNextTuple(posV2);
		edgeMeshFeatureLinesCellNormals->InsertNextTuple(cellNormal01);
		edgeMeshFeatureLinesTangents->InsertNextTuple(cellNormal02);
		edgeMeshFeatureLinesVertexNormals01->InsertNextTuple(vertexNormal01);
		edgeMeshFeatureLinesVertexNormals02->InsertNextTuple(vertexNormal02);
		edgeMeshFeatureLinesIDs->InsertValue(pointID, 1);
		pointIDList01->InsertId(1, pointID);
		  pointID++;

		//_edgeMeshPts->InsertPoint(_pointID, posV1[0] + _offset, posV1[1] + _offset, posV1[2] + _offset);
		edgeMeshFeatureLinesPts->InsertPoint(pointID, posV1);
		edgeMeshFeatureLinesNeighbors->InsertNextTuple(posV2);
		edgeMeshFeatureLinesCellNormals->InsertNextTuple(cellNormal01);
		edgeMeshFeatureLinesTangents->InsertNextTuple(cellNormal02);
		edgeMeshFeatureLinesVertexNormals01->InsertNextTuple(vertexNormal01);
		edgeMeshFeatureLinesVertexNormals02->InsertNextTuple(vertexNormal02);
		edgeMeshFeatureLinesIDs->InsertValue(pointID, 2);
		pointIDList01->InsertId(2, pointID);
		  pointID++;

		//_edgeMeshPts->InsertPoint(_pointID, posV1[0] + _offset, posV1[1] + _offset, posV1[2] + _offset);
		edgeMeshFeatureLinesPts->InsertPoint(pointID, posV1);
		edgeMeshFeatureLinesNeighbors->InsertNextTuple(posV2);
		edgeMeshFeatureLinesCellNormals->InsertNextTuple(cellNormal01);
		edgeMeshFeatureLinesTangents->InsertNextTuple(cellNormal02);
		edgeMeshFeatureLinesVertexNormals01->InsertNextTuple(vertexNormal01);
		edgeMeshFeatureLinesVertexNormals02->InsertNextTuple(vertexNormal02);
		edgeMeshFeatureLinesIDs->InsertValue(pointID, 3);
		pointIDList01->InsertId(3, pointID);
		  pointID++;

		// Now construct topology on the degenerated cells. 			
		edgeMeshFeatureLinesCells->InsertNextCell(pointIDList01);	

		delete[] cellNormal01;
		delete[] cellNormal02;
		delete[] vertexNormal01;
		delete[] vertexNormal02;
		delete[] posV1;
		delete[] posV2;    
	}
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceContours::qfeBuildFeatureLinesOutput()
{
  this->edgeMeshOutput = vtkSmartPointer<vtkPolyData>::New();

	this->edgeMeshOutput->SetPoints(edgeMeshFeatureLinesPts);
	this->edgeMeshOutput->SetPolys(edgeMeshFeatureLinesCells);
	//this->edgeMeshOutput->SetLines(edgeMeshCells);
  //this->edgeMeshOutput->GetPointData()->SetTCoords(edgeMeshTangents);
	this->edgeMeshOutput->GetPointData()->SetNormals(edgeMeshFeatureLinesCellNormals);
	this->edgeMeshOutput->GetPointData()->AddArray(edgeMeshFeatureLinesTangents);	
	this->edgeMeshOutput->GetPointData()->AddArray(edgeMeshFeatureLinesNeighbors);
	this->edgeMeshOutput->GetPointData()->AddArray(edgeMeshFeatureLinesVertexNormals01);
	this->edgeMeshOutput->GetPointData()->AddArray(edgeMeshFeatureLinesVertexNormals02);
	this->edgeMeshOutput->GetPointData()->AddArray(edgeMeshFeatureLinesIDs);
	this->edgeMeshOutput->Update();

  return qfeSuccess;
}
