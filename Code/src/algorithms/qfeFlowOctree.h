#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithmFlow.h"
#include "qfeTensor3f.h"
#include "qfeVolume.h"

#include "qfeMath.h"
#include "qfeMatrix4f.h"
#include "qfeTransform.h"

enum qfeFlowOctreeNodeType
{
  qfeFlowOctreeNodeInternal,
  qfeFlowOctreeNodeLeaf
};

struct qfeFlowOctreeNode
{
  qfeTensor3f           orientation;
  qfeFloat              coherence;  
  qfeFlowOctreeNodeType type;
};

struct qfeFlowOctreeLayer
{
  qfeFlowOctreeNode   *data;
  unsigned int         dimsActual[3];  
  unsigned int         dimsConceptual;
};

//typedef vector< vector< vector< qfeFlowOctreeNode > > >  qfeFlowOctreeLayer;

/**
* \file   qfeFlowOctree.h
* \author Roy van Pelt
* \class  qfeFlowOctree
* \brief  Implements computation and rendering of a flow octree
* \note   Confidential
*
* Computation and rendering of a flow octree.
* Builds the octree in a bottom-up, combining leaves based on
* the average structure tensor. Each node now represents an
* average direction, from which the local coherence can be deduced.
*
*/
class qfeFlowOctree : public qfeAlgorithmFlow
{
public:
  qfeFlowOctree();
  qfeFlowOctree(const qfeFlowOctree &ot);
  ~qfeFlowOctree();
  
  qfeReturnStatus qfeBuildFlowOctree(qfeVolume *volume);

  qfeReturnStatus qfeWriteTest();

  qfeReturnStatus qfeRenderOctree(qfeFloat coherence);
  qfeReturnStatus qfeRenderOctree(int level);
  qfeReturnStatus qfeRenderOctree(qfePoint sample, int level);

  qfeReturnStatus qfeGetPointCoherence(qfeVector v, qfePoint p, int layer, qfeFloat &coherence);
  qfeReturnStatus qfeGetLineCoherence(qfePoint p1, qfePoint p2, int layer, qfeFloat &coherence);
protected:

  qfeReturnStatus qfeBuildFirstLayer(qfeVolume *volume, qfeFlowOctreeLayer **layer);  
  qfeReturnStatus qfeBuildLayer(qfeFlowOctreeLayer *layer, qfeFlowOctreeLayer **newLayer);  

  qfeReturnStatus qfeBuildFlowOctreeGeometry(vector< qfeFlowOctreeLayer * > octree, int level, GLuint &octreeGeoVBO, GLint &vertexCount);
  qfeReturnStatus qfeBuildFlowOctreeGeometry(vector< qfeFlowOctreeLayer * > octree, qfePoint sample, int level, GLuint &octreeGeoVBO, GLint &vertexCount);
  qfeReturnStatus qfeBuildFlowOctreeGeometryCoherence(vector< qfeFlowOctreeLayer * > octree, qfeFloat coherence, GLuint &octreeGeoVBO, GLint &vertexCount);
  qfeReturnStatus qfeBuildFlowOctreeGeometryRecursive(vector< qfeFlowOctreeLayer * > octree, int layerIndex, qfePoint nodeCoord, qfeFloat coherence, vector<float> &octreeGeo);

  qfeReturnStatus qfeDrawBlock(qfeFloat xmin, qfeFloat xmax, qfeFloat ymin, qfeFloat ymax, qfeFloat zmin, qfeFloat zmax);
  qfeReturnStatus qfeDrawBlock(qfeFloat xmin, qfeFloat xmax, qfeFloat ymin, qfeFloat ymax, qfeFloat zmin, qfeFloat zmax, vector< float > &octree);

private :
  vector< qfeFlowOctreeLayer* > octree;
  GLuint                        octreeGeoVBO;
  int                           octreeGeoVBOSize; 
  GLuint                        octreeGeoVBOCoherence;
  int                           octreeGeoVBOCoherenceSize; 
  vector<float>                 octreeGeo;

  qfeFloat                      currentCoherence;
  int                           currentLevel;

  bool                          firstRender;
  qfeVolume                    *flowVolume;    
      
  qfeReturnStatus qfeTakeOctant(qfeVolume *volume, qfePoint p, qfeVector v[8]);
  qfeReturnStatus qfeTakeOctant(qfeFlowOctreeLayer *layer, qfePoint p, qfeFlowOctreeNode n[8]);
  qfeReturnStatus qfeTakeNode(qfeFlowOctreeLayer *layer, qfePoint p, qfeFlowOctreeNode **n);  

  qfeReturnStatus qfeGetVoxelToOctree(qfePoint pointVoxel, int octreeLevel, qfePoint &pointOctree);

  qfeReturnStatus qfeGetTensorCoherence(qfeTensor3f t1, qfeTensor3f t2, qfeFloat &c);

  qfeReturnStatus qfeVolumeToLayer(qfeVolume *volume, qfeFlowOctreeLayer **layer);
  qfeReturnStatus qfeGetNextPowerOfTwo(int i, int& n); 
};
