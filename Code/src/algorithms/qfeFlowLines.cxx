#include "qfeFlowLines.h"

//----------------------------------------------------------------------------
qfeFlowLines::qfeFlowLines()
{
  this->shaderProgramVectorLines = new qfeGLShaderProgram();
  this->shaderProgramVectorLines->qfeAddShaderFromFile("/shaders/ifr/line-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramVectorLines->qfeAddShaderFromFile("/shaders/ifr/line-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramVectorLines->qfeAddShaderFromFile("/shaders/ifr/line-vector-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramVectorLines->qfeBindAttribLocation("qfe_Vertex", ATTRIB_VERTEX);
  this->shaderProgramVectorLines->qfeBindAttribLocation("qfe_TexCoord", ATTRIB_TEXCOORD0);  
  this->shaderProgramVectorLines->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderProgramVectorLines->qfeLink();

  this->shaderProgramStreamLines = new qfeGLShaderProgram();
  this->shaderProgramStreamLines->qfeAddShaderFromFile("/shaders/ifr/line-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramStreamLines->qfeAddShaderFromFile("/shaders/ifr/line-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramStreamLines->qfeAddShaderFromFile("/shaders/ifr/line-stream-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramStreamLines->qfeBindAttribLocation("qfe_Vertex", ATTRIB_VERTEX);
  this->shaderProgramStreamLines->qfeBindAttribLocation("qfe_TexCoord", ATTRIB_TEXCOORD0);  
  this->shaderProgramStreamLines->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderProgramStreamLines->qfeLink();

  this->shaderProgramPathLines = new qfeGLShaderProgram();
  this->shaderProgramPathLines->qfeAddShaderFromFile("/shaders/ifr/line-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramPathLines->qfeAddShaderFromFile("/shaders/ifr/line-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramPathLines->qfeAddShaderFromFile("/shaders/ifr/line-path-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramPathLines->qfeBindAttribLocation("qfe_Vertex", ATTRIB_VERTEX);
  this->shaderProgramPathLines->qfeBindAttribLocation("qfe_TexCoord", ATTRIB_TEXCOORD0);  
  this->shaderProgramPathLines->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderProgramPathLines->qfeLink();

  this->firstRender = true;

  this->paramLineWidth       = 1.0;
  this->paramLineColorMode   = 0;
  this->paramLineShading     = 0;
  this->paramTraceDirection  = 1;
  this->paramTraceStep       = 0.1;
  this->paramTraceIterations = 20;
  this->paramTraceCounter    = 0;

};

//----------------------------------------------------------------------------
qfeFlowLines::qfeFlowLines(const qfeFlowLines &fv)
{
  this->firstRender               = fv.firstRender;
  this->shaderProgramVectorLines  = fv.shaderProgramVectorLines;
  this->shaderProgramStreamLines  = fv.shaderProgramStreamLines;
  this->shaderProgramPathLines    = fv.shaderProgramPathLines;
  this->light                     = fv.light;

  this->V2P                       = fv.V2P;
  this->P2V                       = fv.P2V;
  this->volumeSize[0]             = fv.volumeSize[0];
  this->volumeSize[1]             = fv.volumeSize[1];
  this->volumeSize[2]             = fv.volumeSize[2];
  this->phaseDuration             = fv.phaseDuration;

  // Dynamic uniform locations
  this->phaseCurrent              = fv.phaseCurrent;
  this->paramLineColorMode        = fv.paramLineColorMode;
  this->paramLineColor            = fv.paramLineColor;
  this->paramLineShading          = fv.paramLineShading;
  this->paramTraceCounter         = fv.paramTraceCounter;
  this->paramTraceDirection       = fv.paramTraceDirection;
  this->paramTraceIterations      = fv.paramTraceIterations;
  this->paramTraceStep            = fv.paramTraceStep;
  this->paramSeedTime             = fv.paramSeedTime;
};

//----------------------------------------------------------------------------
qfeFlowLines::~qfeFlowLines()
{
  delete this->shaderProgramVectorLines;
  delete this->shaderProgramStreamLines;
  delete this->shaderProgramPathLines;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeRenderVectorLines(qfeSeeds seeds, qfeVolume *volume, qfeColorMap *colormap)
{
  qfeVolumeSeries *series = new qfeVolumeSeries;
  series->push_back(volume);

  qfeReturnStatus result = this->qfeRenderLinesGPU(seeds, series, colormap, QFE_VECTOR);  

  delete series;

  return result;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeRenderStreamLines(qfeSeeds seeds, qfeVolume *volume, qfeColorMap *colormap)
{
  qfeVolumeSeries *series = new qfeVolumeSeries;
  series->push_back(volume);

  qfeReturnStatus result = this->qfeRenderLinesGPU(seeds, series, colormap, QFE_STREAMLINE);  

  delete series;

  return result;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeRenderPathLines(qfeSeeds seeds, qfeVolumeSeries *series, qfeColorMap *colormap)
{
  return this->qfeRenderLinesGPU(seeds, series, colormap, QFE_PATHLINE);
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeRenderStreamlinesCPU(qfeLineSet streamlines, qfeVolume *volume)
{
  qfeMatrix4f                V2P;
  GLfloat                    matrix[16];

  if(volume == NULL) return qfeError;

  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]); 

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(matrix);

  glLineWidth(this->paramLineWidth);  
  glColor3f(0.7,0.1,0.1);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  for(int i=0; i<(int)streamlines.size(); i++)
  {
    this->qfeDrawLine(streamlines[i]);
  }  

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND); 

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeRenderPathLinesCPU(qfeLineSet pathlines, qfeVolume *volume)
{
  qfeMatrix4f                V2P;
  GLfloat                    matrix[16];

  if(volume == NULL)
  {
    cout << "qfeDriverIFR::qfeRenderPathlines - Cannot render pathlines with a single volume" << endl;
    return qfeError;
  }

  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]); 

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(matrix);

  glLineWidth(this->paramLineWidth);  
  glColor3f(0.7,0.1,0.1);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  for(int i=0; i<(int)pathlines.size(); i++)
  {
    this->qfeDrawLine(pathlines[i]);
  }  

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND); 

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;

}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeRenderLinesGPU(qfeSeeds seeds, qfeVolumeSeries *series, qfeColorMap *colormap, qfeLineType type)
{
  qfeMatrix4f V2T;
  GLuint      vbo;
  GLint       sizeBuffer;
  qfeFloat   *vertices = new qfeFloat[3*seeds.size()];
  qfeFloat   *texcoord = new qfeFloat[3*seeds.size()];

  if(series == NULL) return qfeError;

  if(this->firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, series->front());
    qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, series->front());
    qfeTransform::qfeGetMatrixVoxelToTexture(V2T, series->front());

    series->front()->qfeGetVolumeSize(this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);

    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  // Fill the buffer
  for(int i=0; i<(int)seeds.size(); i++)
  {
    qfePoint tex    = seeds[i]*V2T;

    vertices[3*i+0] = seeds[i].x;
    vertices[3*i+1] = seeds[i].y;
    vertices[3*i+2] = seeds[i].z;

    texcoord[3*i+0] = tex.x;
    texcoord[3*i+1] = tex.y;
    texcoord[3*i+2] = tex.z;
  }

  // Create the vertex buffer
  sizeBuffer = seeds.size()*3*sizeof(GLfloat);

  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeBuffer, 0, GL_STATIC_DRAW);
  glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, 0,          sizeBuffer, vertices);
  glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, sizeBuffer, sizeBuffer, texcoord);

  this->qfeGetModelViewMatrix();

  // Render
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLineWidth(this->paramLineWidth);
  glColor3f(0.7,0.1,0.1);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  this->qfeBindTextures(series, colormap);
  this->qfeSetDynamicUniformLocations();

  if(type == QFE_VECTOR)
  {
    this->shaderProgramVectorLines->qfeEnable();

    glEnableVertexAttribArray(ATTRIB_VERTEX);
    glEnableVertexAttribArray(ATTRIB_TEXCOORD0);

    glVertexAttribPointer(ATTRIB_VERTEX,    3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glVertexAttribPointer(ATTRIB_TEXCOORD0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeBuffer));

    glDrawArrays(GL_POINTS, 0, seeds.size());

    glDisableVertexAttribArray(ATTRIB_VERTEX);
    glDisableVertexAttribArray(ATTRIB_TEXCOORD0);

    this->shaderProgramVectorLines->qfeDisable();
  }
  else
  if(type == QFE_PATHLINE)
  {
    this->shaderProgramPathLines->qfeEnable();

    glEnableVertexAttribArray(ATTRIB_VERTEX);
    glEnableVertexAttribArray(ATTRIB_TEXCOORD0);

    glVertexAttribPointer(ATTRIB_VERTEX,    3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glVertexAttribPointer(ATTRIB_TEXCOORD0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeBuffer));

    glDrawArrays(GL_POINTS, 0, seeds.size());

    glDisableVertexAttribArray(ATTRIB_VERTEX);
    glDisableVertexAttribArray(ATTRIB_TEXCOORD0);

    this->shaderProgramPathLines->qfeDisable();
  }
  else
  {
    this->shaderProgramStreamLines->qfeEnable();

    glEnableVertexAttribArray(ATTRIB_VERTEX);
    glEnableVertexAttribArray(ATTRIB_TEXCOORD0);

    glVertexAttribPointer(ATTRIB_VERTEX,    3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glVertexAttribPointer(ATTRIB_TEXCOORD0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeBuffer));

    glDrawArrays(GL_POINTS, 0, seeds.size());

    glDisableVertexAttribArray(ATTRIB_VERTEX);
    glDisableVertexAttribArray(ATTRIB_TEXCOORD0);

    this->shaderProgramStreamLines->qfeDisable();
  }
  this->qfeUnbindTextures();

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  delete vertices;
  delete texcoord;
  glDeleteBuffers(1, &vbo);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfePrecomputeStreamlines(qfeSeedsGroup seeds, qfeVolumeSeries series,  qfeFloat traceStep, int traceIterations, qfeLineSetGroupSeries &lines)
{
  if(series.size() <= 0 || seeds.size() <= 0) return qfeError;

  lines.clear();

  // For each volume in a series
  for(int i=0; i<(int)series.size(); i++)
  {
    qfeLineSetGroup lineSetGroup;
     
    this->qfePrecomputeStreamlines(seeds, series[i], traceStep, traceIterations, lineSetGroup);

    lines.push_back(lineSetGroup);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfePrecomputeStreamlines(qfeSeedsGroup seeds, qfeVolume *volume, qfeFloat traceStep, int traceIterations, qfeLineSetGroup &lines)
{
  if(seeds.size() <=0 || volume == NULL) return qfeError;

  lines.clear();

  // For each 2D probe in a volume
  for(int i=0; i<(int)seeds.size(); i++)
  { 
    qfeLineSet lineSet;

    this->qfePrecomputeStreamlines(seeds[i], volume, traceStep, traceIterations, lineSet);
   
    lines.push_back(lineSet);    
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfePrecomputeStreamlines(qfeSeeds seeds, qfeVolume *volume, qfeFloat traceStep, int traceIterations, qfeLineSet &lines)
{
  if(seeds.size() <= 0 || volume == NULL) return qfeError;

  lines.clear();

  // For each seed in a 2D probe
  for(int i=0; i<(int)seeds.size(); i++)
  {
    vector<qfePoint> line;
    this->qfeComputeStreamline(seeds[i], volume, traceStep, traceIterations, line);      
    lines.push_back(line);
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfePrecomputePathlines(qfeSeedsGroup seeds, qfeVolumeSeries series, qfeFloat traceStep, int traceIterations, qfeFloat phaseDuration, qfeLineSetGroupSeries &lines)
{
  if(series.size() <= 0 || seeds.size() <= 0) return qfeError;

  lines.clear();

  // For each time in a series
  for(int i=0; i<(int)series.size(); i++)
  {
    qfeLineSetGroup lineSetGroup;

    this->qfePrecomputePathlines(seeds, i, series, traceStep, traceIterations, phaseDuration, lineSetGroup);

    lines.push_back(lineSetGroup);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfePrecomputePathlines(qfeSeedsGroup seeds, qfeFloat seedTime, qfeVolumeSeries series, qfeFloat traceStep, int traceIterations, qfeFloat phaseDuration, qfeLineSetGroup &lines)
{
  if(seeds.size() <=0 || series.size() <= 0) return qfeError;

  lines.clear();

  // For each 2D probe in a volume
  for(int i=0; i<(int)seeds.size(); i++)
  { 
    qfeLineSet lineSet;

    this->qfePrecomputePathlines(seeds[i], seedTime, series, traceStep, traceIterations, phaseDuration, lineSet);

    lines.push_back(lineSet);    
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfePrecomputePathlines(qfeSeeds seeds, qfeFloat seedTime, qfeVolumeSeries series, qfeFloat traceStep, int traceIterations, qfeFloat phaseDuration, qfeLineSet &lines)
{
  if(seeds.size() <= 0 || series.size() <= 0) return qfeError;

  lines.clear();

  // For each seed in a 2D probe
  for(int i=0; i<(int)seeds.size(); i++)
  {
    vector<qfePoint> line;
    this->qfeComputePathline(seeds[i], seedTime, series, traceStep, traceIterations, phaseDuration, line);      
    lines.push_back(line);
  }  

  return qfeSuccess;
}

//---------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeComputeStreamline(qfePoint seed, qfeVolume *volume, qfeFloat step, int iterations, vector<qfePoint> &streamline)
{
  streamline.clear();
  streamline.push_back(seed);

  for(int i=0; i<iterations; i++)
  {
    qfePoint p;
    this->qfeComputeRungeKuttaSpatial(streamline.back(), step, volume, p);
    streamline.push_back(p);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeComputePathline(qfePoint seed, qfeFloat seedTime, qfeVolumeSeries series, qfeFloat step, int iterations, qfeFloat phaseDuration, vector<qfePoint> &pathline)
{
  pathline.clear();
  pathline.push_back(seed);

  if(series.size() <= 1) return qfeError;

  qfeFloat t;

  t = seedTime;

  for(int i=0; i<iterations; i++)
  {
    qfePoint p;
    
    if(this->qfeComputeRungeKuttaTemporal(pathline.back(), t, step, phaseDuration, series, p, t)) return qfeError;
    pathline.push_back(p);
  }  

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeDrawLine(qfeLine line)
{
  glBegin(GL_LINE_STRIP);
  for(int i=0; i<(int)line.size(); i++)
  {
    glVertex3f(line[i].x,line[i].y,line[i].z);
  }
  glEnd(); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
//! Seed in voxel coordinates
//! Step size in mm
//! Point in voxel coordinates
qfeReturnStatus qfeFlowLines::qfeComputeRungeKuttaSpatial(qfePoint seed, qfeFloat step, qfeVolume *volume, qfePoint &point)
{
  qfeVector vi, vi_1, vi_2, vi_3;
  qfePoint  s;
  qfeFloat  stepSize;

  // Phase image velocities are defined in cm/s
  // Step in spatial domain is defined in mm
  stepSize = step/10.0;

  qfeMatrix4f P2V, V2P;
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);

  s = seed*V2P;

  // Seed in patient coordinates, velocity in patient coordinates
  volume->qfeGetVolumeVectorAtPositionLinear(seed, vi);

  qfeVector k1     = vi   * stepSize;  
  qfePoint  p1     = s    + k1*0.5;  

  p1 = p1 * P2V;
  volume->qfeGetVolumeVectorAtPositionLinear(p1, vi_1);

  qfeVector k2     = vi_1 * stepSize;
  qfePoint  p2     = s    + k2*0.5;
  
  p2 = p2 * P2V;
  volume->qfeGetVolumeVectorAtPositionLinear(p2, vi_2);

  qfeVector k3     = vi_2 * stepSize;
  qfePoint  p3     = s    + k3;
  
  p3 = p3 * P2V;
  volume->qfeGetVolumeVectorAtPositionLinear(p3, vi_3);

  qfeVector k4     = vi_3 * stepSize;

  // New position in voxel coordinates
  s     = s + (k1 + k2*2.0 + k3*2.0 + k4)*(1.0/6.0);
  point = s * P2V;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
//! Seed in voxel coordinates
//! Seed time in phases
//! Step size in phases
//! Point in voxel coordinates
//! Time in phases
qfeReturnStatus qfeFlowLines::qfeComputeRungeKuttaTemporal(qfePoint seed, qfeFloat seedTime, qfeFloat step, qfeFloat phaseDuration, qfeVolumeSeries series, qfePoint &point, qfeFloat &time)
{
  qfeVector vi, vi_1, vi_2, vi_3;
  qfePoint  s;
  qfeFloat  t, stepSize;

  if(series.size() <= 0) return qfeError;

  // Phase image velocities is defined in cm/s
  // Step size in seconds
  stepSize = (step * phaseDuration)/1000.0; 

  qfeMatrix4f P2V, V2P;
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, series.front());
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, series.front());

  s = seed * V2P;
  t = (seedTime * phaseDuration)/1000.0;

  // Seed in patient coordinates, velocity in patient coordinates
  if(this->qfeVolumeSeriesGetVectorAtPositionLinear(series, seed, (t*1000)/phaseDuration, vi)) return qfeError;

  qfeVector k1     = vi   * stepSize;  
  qfePoint  p1     = s    + k1*0.5;  
  qfeFloat  t1     = t    + stepSize*0.5;

  p1 = p1 * P2V;
  if(this->qfeVolumeSeriesGetVectorAtPositionLinear(series, p1, (t1*1000)/phaseDuration, vi_1)) return qfeError;

  qfeVector k2     = vi_1 * stepSize;
  qfePoint  p2     = s    + k2*0.5;
  qfeFloat  t2     = t    + stepSize*0.5;

  p2 = p2 * P2V;
  if(this->qfeVolumeSeriesGetVectorAtPositionLinear(series, p2, (t2*1000)/phaseDuration, vi_2)) return qfeError;

  qfeVector k3     = vi_2 * stepSize;
  qfePoint  p3     = s    + k3;
  qfeFloat  t3     = t    + stepSize;

  p3 = p3 * P2V;
  if(this->qfeVolumeSeriesGetVectorAtPositionLinear(series, p3, (t3*1000)/phaseDuration, vi_3)) return qfeError;

  qfeVector k4     = vi_3 * stepSize;

  // New position in voxel coordinates
  s     = s + (k1 + k2*2.0 + k3*2.0 + k4)*(1.0/6.0);
  point = s * P2V;
  time  = ((t + stepSize)*1000.0)/phaseDuration;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
//! Point p in voxel coordinates
//! Time  t in phases (0,1,...)
qfeReturnStatus qfeFlowLines::qfeVolumeSeriesGetVectorAtPositionLinear(qfeVolumeSeries series, qfePoint p, qfeFloat t, qfeVector &v)
{
  if(t < 0.0 || t >= series.size())
  {
    v.x = p.x; v.y = p.y; v.z = p.z;
    return qfeError;
  }

  int       tl, tr;
  qfeFloat  ti;
  qfeVector yl, yr;

  // We make the assumption that the acquired data is one full heart beat
  // and that we can consider the dataset cyclic
  tl = (int)floor(t);
  tr = (tl+1)%(int)series.size();
  ti = t - tl;

  if(series.at(tl)->qfeGetVolumeVectorAtPositionLinear(p, yl)) return qfeError;
  if(series.at(tr)->qfeGetVolumeVectorAtPositionLinear(p, yr)) return qfeError;

  qfeVector::qfeVectorInterpolateLinearSeparate(yl, yr, ti, v);

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeBindTextures(qfeVolumeSeries *series, qfeColorMap *colormap)
{
  GLuint        cid;
  GLuint        tids[5];
  int           phase = 0;
  int           phasesCount;

  if(this->phaseCurrent < (float)series->size())
    phase       = floor(this->phaseCurrent);

  phasesCount = (int)series->size();

  for(int i=0; i<5; i++)
    series->at((phase+i)%phasesCount)->qfeGetVolumeTextureId(tids[i]);

  colormap->qfeGetColorMapTextureId(cid);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_3D, tids[0]);

  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_3D, tids[1]);

  glActiveTexture(GL_TEXTURE3);
  glBindTexture(GL_TEXTURE_3D, tids[2]);

  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_3D, tids[3]);

  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_3D, tids[4]);

  glActiveTexture(GL_TEXTURE7);
  glBindTexture(GL_TEXTURE_1D, cid);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetStaticUniformLocations()
{
  GLfloat matrix1[16], matrix2[16];

  qfeMatrix4f::qfeGetMatrixElements(this->V2P, &matrix1[0]);
  qfeMatrix4f::qfeGetMatrixElements(this->P2V, &matrix2[0]);

  this->shaderProgramVectorLines->qfeSetUniform1i("volumeData",  1);
  this->shaderProgramVectorLines->qfeSetUniform3i("volumeDimensions", this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);  
  this->shaderProgramVectorLines->qfeSetUniformMatrix4f("matrixVoxelPatient", 1, matrix1);
  this->shaderProgramVectorLines->qfeSetUniformMatrix4f("matrixPatientVoxel", 1, matrix2);

  this->shaderProgramStreamLines->qfeSetUniform1i("volumeData",  1);
  this->shaderProgramStreamLines->qfeSetUniform3i("volumeDimensions", this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);  
  this->shaderProgramStreamLines->qfeSetUniformMatrix4f("matrixVoxelPatient", 1, matrix1);
  this->shaderProgramStreamLines->qfeSetUniformMatrix4f("matrixPatientVoxel", 1, matrix2);

  this->shaderProgramPathLines->qfeSetUniform1i("volumeData",  1);
  this->shaderProgramPathLines->qfeSetUniform1i("volumeData1", 2);
  this->shaderProgramPathLines->qfeSetUniform1i("volumeData2", 3);
  this->shaderProgramPathLines->qfeSetUniform1i("volumeData3", 4);
  this->shaderProgramPathLines->qfeSetUniform1i("volumeData4", 5);
  this->shaderProgramPathLines->qfeSetUniform3i("volumeDimensions", this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);  
  this->shaderProgramPathLines->qfeSetUniformMatrix4f("matrixVoxelPatient", 1, matrix1);
  this->shaderProgramPathLines->qfeSetUniformMatrix4f("matrixPatientVoxel", 1, matrix2);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetDynamicUniformLocations()
{
  qfePoint  lightSpec;
  qfeVector lightDir;

  // Obtain actual lighting parameters
  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 10.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w);
    this->light->qfeGetLightSourceDirection(lightDir);
  }

  this->shaderProgramVectorLines->qfeSetUniform1i("transferFunction"    , 7);
  this->shaderProgramVectorLines->qfeSetUniform1f("phaseDuration"       , this->phaseDuration);
  this->shaderProgramVectorLines->qfeSetUniform3f("lineColor"           , this->paramLineColor.r, this->paramLineColor.g, this->paramLineColor.b);
  this->shaderProgramVectorLines->qfeSetUniform1i("lineColorMode"       , this->paramLineColorMode);
  this->shaderProgramVectorLines->qfeSetUniform1i("lineShadingMode"     , this->paramLineShading);
  this->shaderProgramVectorLines->qfeSetUniform4f("lightSpec"           , lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramVectorLines->qfeSetUniform4f("lightDirection"      , lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramVectorLines->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramVectorLines->qfeSetUniformMatrix4f("matrixModelViewInverse"   , 1, this->matrixModelViewInverse);
  this->shaderProgramVectorLines->qfeSetUniformMatrix4f("matrixModelView"          , 1, this->matrixModelView);

  this->shaderProgramStreamLines->qfeSetUniform1i("traceDirection"      , this->paramTraceDirection);
  this->shaderProgramStreamLines->qfeSetUniform1f("traceStep"           , this->paramTraceStep);  
  this->shaderProgramStreamLines->qfeSetUniform1i("traceIterations"     , this->paramTraceIterations);
  this->shaderProgramStreamLines->qfeSetUniform1i("traceCounter"        , this->paramTraceCounter);
  this->shaderProgramStreamLines->qfeSetUniform1i("transferFunction"    , 7);
  this->shaderProgramStreamLines->qfeSetUniform3f("lineColor"           , this->paramLineColor.r, this->paramLineColor.g, this->paramLineColor.b);
  this->shaderProgramStreamLines->qfeSetUniform1i("lineColorMode"       , this->paramLineColorMode);
  this->shaderProgramStreamLines->qfeSetUniform1i("lineShadingMode"     , this->paramLineShading);
  this->shaderProgramStreamLines->qfeSetUniform4f("lightSpec"           , lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramStreamLines->qfeSetUniform4f("lightDirection"      , lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramStreamLines->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramStreamLines->qfeSetUniformMatrix4f("matrixModelViewInverse"   , 1, this->matrixModelViewInverse);
  this->shaderProgramStreamLines->qfeSetUniformMatrix4f("matrixModelView"          , 1, this->matrixModelView);

  this->shaderProgramPathLines->qfeSetUniform1i("traceDirection"      , this->paramTraceDirection);
  this->shaderProgramPathLines->qfeSetUniform1f("traceStep"           , this->paramTraceStep);  
  this->shaderProgramPathLines->qfeSetUniform1i("traceIterations"     , this->paramTraceIterations);
  this->shaderProgramPathLines->qfeSetUniform1i("traceCounter"        , this->paramTraceCounter);
  this->shaderProgramPathLines->qfeSetUniform1f("traceSeedTime"       , this->paramSeedTime);
  this->shaderProgramPathLines->qfeSetUniform1f("phaseDuration"       , this->phaseDuration);
  this->shaderProgramPathLines->qfeSetUniform1i("transferFunction"    , 7);
  this->shaderProgramPathLines->qfeSetUniform3f("lineColor"           , this->paramLineColor.r, this->paramLineColor.g, this->paramLineColor.b);
  this->shaderProgramPathLines->qfeSetUniform1i("lineColorMode"       , this->paramLineColorMode);
  this->shaderProgramPathLines->qfeSetUniform1i("lineShadingMode"     , this->paramLineShading);
  this->shaderProgramPathLines->qfeSetUniform4f("lightSpec"           , lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramPathLines->qfeSetUniform4f("lightDirection"      , lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramPathLines->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramPathLines->qfeSetUniformMatrix4f("matrixModelViewInverse"   , 1, this->matrixModelViewInverse);
  this->shaderProgramPathLines->qfeSetUniformMatrix4f("matrixModelView"          , 1, this->matrixModelView);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetPhase(double current)
{ 
  this->phaseCurrent = current;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetPhaseDuration(double duration)
{
  this->phaseDuration = duration;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetLineTrace(int direction, double step, int iterations)
{
  this->paramTraceDirection  = direction;
  this->paramTraceStep       = step;
  this->paramTraceIterations = iterations;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetLineSeedTime(double phase)
{
  this->paramSeedTime = phase;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetLineHighlight(int counter)
{
  this->paramTraceCounter    = counter;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetLineColorMode(int mode)
{
  this->paramLineColorMode = mode;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetLineColor(qfeColorRGB color)
{
  this->paramLineColor = color;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetLineShadingMode(int mode)
{
  this->paramLineShading = mode;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetLineWidth(int width)
{
  this->paramLineWidth = width;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLines::qfeSetLightSource(qfeLightSource *light)
{
  this->light = light;

  return qfeSuccess;
}


