#include "qfeFluidInteractor.h"

static const float boundary_thickness = 0.1f;
static const float offset = 0.75f;
static const unsigned int pointsize = 10;

void qfeFluidInteractor::Initialize(qfePoint _origin, float _radius, qfeVector _normal, qfeVector _axis1, qfeVector _axis2, InteractorType _type, float _dx, qfeMatrix4f _V2P, qfeMatrix4f _V2T, qfeMatrix4f _T2V, Array3f *_solid)
{
	origin = _origin;
	radius = _radius;
	normal =_normal;
	axisX = _axis1;
	axisY = _axis2;
	type = _type;
	dx = _dx;
	V2P = _V2P;
	V2T = _V2T;
	T2V = _T2V;
	solid = _solid;
};


qfePoint qfeFluidInteractor::getOrigin()
{
	return origin;
}

void qfeFluidInteractor::setOrigin(qfePoint _origin)
{
	origin = _origin;
}

float qfeFluidInteractor::getRadius()
{
	return radius;
}

void qfeFluidInteractor::setRadius(float _radius)
{
	radius = _radius;
}

qfeVector qfeFluidInteractor::getNormal()
{
	return normal;
}

void qfeFluidInteractor::setNormal(qfeVector _normal)
{
	normal = _normal;
}

qfeVector qfeFluidInteractor::getAxis1()
{
	return axisX;
}

qfeVector qfeFluidInteractor::getAxis2()
{
	return axisY;
}

void qfeFluidInteractor::setAxis(qfeVector _axis1, qfeVector _axis2)
{
	axisX = _axis1;
	axisY = _axis2;
}

qfeFluidInteractor::InteractorType qfeFluidInteractor::getType()
{
	return type;
}

void qfeFluidInteractor::setType(InteractorType _type)
{
	type = _type;
}

float qfeFluidInteractor::getdx()
{
	return dx;
}

void qfeFluidInteractor::setdx(float _dx)
{
	dx = _dx;
}

unsigned int qfeFluidInteractor::getID()
{
	return id;
}

void qfeFluidInteractor::setID(unsigned int _id)
{
	id = _id;
}


void qfeFluidInteractor::setV2PMatrix(qfeMatrix4f _V2P)
{
	V2P = _V2P;
}

void qfeFluidInteractor::setV2TMatrix(qfeMatrix4f _V2T)
{
	V2T = _V2T;
}

void qfeFluidInteractor::setT2VMatrix(qfeMatrix4f _T2V)
{
	T2V = _T2V;
}

void qfeFluidInteractor::setSolid(Array3f *_solid)
{
	solid = _solid;
}

void qfeFluidInteractor::renderPolygon(bool isSelected)
{	
	//disk color:
	float r = 0.0f;
	float g = 0.0f;
	float b = 0.0f;
	float a = 0.0f;

	switch(type)
	{
	case SOURCE:
		r = source_color[0];
		g = source_color[1];
		b = source_color[2];
		a = source_color[3];
		break;
	case SOURCE_SELECTION:
		r = source_creation_color[0];
		g = source_creation_color[1];
		b = source_creation_color[2];
		a = source_creation_color[3];
		break;
	case SINK:
		r = sink_color[0];
		g = sink_color[1];
		b = sink_color[2];
		a = sink_color[3];
		break;
	case SINK_SELECTION:
		r = sink_creation_color[0];
		g = sink_creation_color[1];
		b = sink_creation_color[2];
		a = sink_creation_color[3];
		break;		
	case INCORRECT:
		r = incorrect_color[0];
		g = incorrect_color[1];
		b = incorrect_color[2];
		a = incorrect_color[3];
		break;
	case INSPECTOR:
		r = inspection_color[0];
		g = inspection_color[1];
		b = inspection_color[2];
		a = inspection_color[3];
		break;
	default:
		r = 0.0f; g = 0.0f; b = 0.0f; a = 0.0f;
		break;
	}

	if(isSelected)
	{
		renderQuad();

		r += 0.1f;
		g += 0.1f;
		b += 0.1f;
		a = 1.0f;
	}

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	
	// Draw the probe plane in patient coordinates
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);

	glColor4f(normal_color[0],normal_color[1],normal_color[2],normal_color[3]);
	
	if(isSelected)
		glColor4f(r/2.0f,g/2.0f,b/2.0f,a);

	qfePoint renderCenter = origin*V2P;

	qfePoint n1 = (origin+normal*5.0f)*V2P;
	qfePoint n2 = (origin-normal*5.0f)*V2P;

	/*/normal
	glBegin(GL_LINES);
		glVertex3f(n1.x,n1.y,n1.z);
		glVertex3f(n2.x,n2.y,n2.z);
	glEnd();
	//*/

	GLUquadric *quad = gluNewQuadric();
	
	glPushMatrix();	
		float rad = (radius+3.0f*dx)*40.0f; //convert radius to "patient coordinates"
		glTranslatef(renderCenter.x,renderCenter.y,renderCenter.z);
		gluSphere(quad,radius/2.0,16,16);
	glPopMatrix();

	int count = (int)polygon.size();
	
	glBegin(GL_TRIANGLE_FAN);
		/*Transparent center
		glColor4f(0.0f,0.0f,0.0f,0.2f);
		//*/
		glColor4f(r,g,b,a);
		if(isSelected)
			glColor4f(r/2.0f,g/2.0f,b/2.0f,a);
		glVertex3f(renderCenter.x, renderCenter.y, renderCenter.z);
		
	
		for(int i=0; i<=count; i++)
		{
			glColor4f(r,g,b,a);
			if(isSelected && (i < (count/4) || (i>(count/2) && i<(3*count/4))))
				glColor4f(r/2.0f,g/2.0f,b/2.0f,a);
			glVertex3f((polygon[i%count]*V2P).x, (polygon[i%count]*V2P).y, (polygon[i%count]*V2P).z);
		}
	glEnd();
	
	//render outline
	glLineWidth(2.0f);
	glBegin(GL_LINE_STRIP);

	glColor4f(outline_color[0],outline_color[1],outline_color[2],outline_color[3]);
	if(isSelected)
		glColor4f(incorrect_color[0],incorrect_color[1],incorrect_color[2],incorrect_color[3]);

	for(int i=0; i<=count; i++)
	{
	  glVertex3f((polygon[i%count]*V2P).x, (polygon[i%count]*V2P).y, (polygon[i%count]*V2P).z);
	}
	glEnd();

	glDisable(GL_LINE_SMOOTH);
	glDisable(GL_BLEND);   
	
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void qfeFluidInteractor::setPolygon(std::vector<qfePoint>  _polygon)
{
	this->polygon = _polygon;

	//recompute the radius:
	float distance = 0.0f;
	this->radius = 0.0f;
	for(unsigned int i = 0; i<_polygon.size(); i++)
	{
		qfePoint::qfePointDistance(_polygon.at(i), this->origin, distance);

		if(distance>radius)
		{
			this->radius = distance;
		}
	}

	this->radius *= this->dx;

	//recompute the area:
	this->area = 3.14159 * (radius*radius);
}

std::vector<qfePoint> qfeFluidInteractor::getPolygon()
{
	return this->polygon;
}

bool qfeFluidInteractor::textureExists(string label, float time, GLfloat* &image, unsigned int size, float &range_min, float &range_max, unsigned int colorMapId)
{
	if(!textures.empty())
	{
		if(textures.at(0).first.size != size)
			return false;

		for(unsigned int t = 0; t < textures.size(); t++)
		{
			if(label.compare(textures.at(t).first.label) == 0 && (textures.at(t).first.time == time) && (textures.at(t).first.colorMapId == colorMapId))
			{
				image = new GLfloat[size*size*4]();

				range_min = textures.at(t).first.range_min;
				range_max = textures.at(t).first.range_max;

				for(unsigned int p = 0; p<size*size*4; p++)
				{
					image[p] = textures.at(t).second[p];
				}
				return true;
			}
		}
	}

	return false;
}

void qfeFluidInteractor::mapTo2D(Array3f *source, string label, float time, GLfloat* &image, unsigned int size, float range_min, float range_max, qfeColorMap *colorMap,unsigned int colorMapId)
{	
	bool exists = this->textureExists(label, time, image, size, range_min, range_max, colorMapId);
	if(exists)
		return;
	
	if(!textures.empty())
	{
		if(textures.at(0).first.size != size)
		{
			textures.clear();
		}

		for(unsigned int t = 0; t < textures.size(); t++)
		{
			if(label.compare(textures.at(t).first.label) == 0 && (textures.at(t).first.time == time) && (textures.at(t).first.colorMapId != colorMapId))
			{
				textures.erase(textures.begin()+t);
			}
		}
	}
	
	assert(solid != NULL);
	if(range_max<=range_min)
	{
		std::cout<<"Incorrect ranges: ["<<range_min<<", "<<range_max<<"]"<<std::endl;
	}
	assert(range_max>range_min);
	
	qfePoint texCenter = this->origin*V2T;

	qfePoint edge1a = this->computeSolidCrossing(texCenter,
		this->axisX);
	qfePoint edge1b = this->computeSolidCrossing(texCenter,
		qfeVector(-1.0f*this->axisX.x,-1.0f*this->axisX.y,-1.0f*this->axisX.z));
	qfePoint edge2a = this->computeSolidCrossing(texCenter,
		this->axisY);
	qfePoint edge2b = this->computeSolidCrossing(texCenter,
		qfeVector(-1.0f*this->axisY.x,-1.0f*this->axisY.y,-1.0f*this->axisY.z));
	
	qfePoint diff;
	bool axis1_selected = true;
	float magnitude = 0.0f;

	float tx = (edge1a.x-texCenter.x);
	float ty = (edge1a.y-texCenter.y);
	float tz = (edge1a.z-texCenter.z);
	if(tx*tx+ty*ty+tz*tz > magnitude)
	{
		diff.x = tx; diff.y = ty; diff.z = tz;
		magnitude = tx*tx+ty*ty+tz*tz;
		axis1_selected = false;
	}
	tx = (edge1b.x-texCenter.x);
	ty = (edge1b.y-texCenter.y);
	tz = (edge1b.z-texCenter.z);
	if(tx*tx+ty*ty+tz*tz > magnitude)
	{
		diff.x = tx; diff.y = ty; diff.z = tz;
		magnitude = tx*tx+ty*ty+tz*tz;
		axis1_selected = false;
	}
	tx = (edge2a.x-texCenter.x);
	ty = (edge2a.y-texCenter.y);
	tz = (edge2a.z-texCenter.z);
	if(tx*tx+ty*ty+tz*tz > magnitude)
	{
		diff.x = tx; diff.y = ty; diff.z = tz;
		magnitude = tx*tx+ty*ty+tz*tz;
		axis1_selected = true;
	}
	tx = (edge2b.x-texCenter.x);
	ty = (edge2b.y-texCenter.y);
	tz = (edge2b.z-texCenter.z);
	if(tx*tx+ty*ty+tz*tz > magnitude)
	{
		diff.x = tx; diff.y = ty; diff.z = tz;
		magnitude = tx*tx+ty*ty+tz*tz;
		axis1_selected = true;
	}

	diff.x = offset * diff.x; diff.y = offset * diff.y; diff.z = offset * diff.z; 

	qfePoint texCoord1a;
	qfePoint texCoord1b;
	qfePoint texCoord2a;
	qfePoint texCoord2b;

	//axis1
	if(axis1_selected)
	{
		texCoord1a.x = edge1a.x + diff.x; texCoord1a.y = edge1a.y + diff.y; texCoord1a.z = edge1a.z + diff.z;	
		texCoord1b.x = edge1a.x - diff.x; texCoord1b.y = edge1a.y - diff.y; texCoord1b.z = edge1a.z - diff.z;	
		texCoord2a.x = edge1b.x + diff.x; texCoord2a.y = edge1b.y + diff.y; texCoord2a.z = edge1b.z + diff.z;	
		texCoord2b.x = edge1b.x - diff.x; texCoord2b.y = edge1b.y - diff.y; texCoord2b.z = edge1b.z - diff.z;		
	}
	else
	{
		texCoord1a.x = edge2a.x + diff.x; texCoord1a.y = edge2a.y + diff.y; texCoord1a.z = edge2a.z + diff.z;	
		texCoord1b.x = edge2a.x - diff.x; texCoord1b.y = edge2a.y - diff.y; texCoord1b.z = edge2a.z - diff.z;	
		texCoord2a.x = edge2b.x + diff.x; texCoord2a.y = edge2b.y + diff.y; texCoord2a.z = edge2b.z + diff.z;	
		texCoord2b.x = edge2b.x - diff.x; texCoord2b.y = edge2b.y - diff.y; texCoord2b.z = edge2b.z - diff.z;
	}

	//scale
	texCoord1a.x -= offset * (texCenter.x - texCoord1a.x); texCoord1a.y -= offset * (texCenter.y - texCoord1a.y); texCoord1a.z -= offset * (texCenter.z - texCoord1a.z);
	texCoord1b.x -= offset * (texCenter.x - texCoord1b.x); texCoord1b.y -= offset * (texCenter.y - texCoord1b.y); texCoord1b.z -= offset * (texCenter.z - texCoord1b.z);
	texCoord2a.x -= offset * (texCenter.x - texCoord2a.x); texCoord2a.y -= offset * (texCenter.y - texCoord2a.y); texCoord2a.z -= offset * (texCenter.z - texCoord2a.z);
	texCoord2b.x -= offset * (texCenter.x - texCoord2b.x); texCoord2b.y -= offset * (texCenter.y - texCoord2b.y); texCoord2b.z -= offset * (texCenter.z - texCoord2b.z);
	
	GLfloat *local_image = new GLfloat[size*size*4]();

	qfePoint centre = this->origin * V2T;
	qfePoint middle = qfePoint(centre.x*solid->ni,centre.y*solid->nj,centre.z*solid->nk);
	qfePoint simCoord1a(texCoord1a.x*solid->ni, texCoord1a.y*solid->nj, texCoord1a.z*solid->nk);
	qfePoint simCoord1b(texCoord1b.x*solid->ni, texCoord1b.y*solid->nj, texCoord1b.z*solid->nk);
	qfePoint simCoord2a(texCoord2a.x*solid->ni, texCoord2a.y*solid->nj, texCoord2a.z*solid->nk);
	qfePoint simCoord2b(texCoord2b.x*solid->ni, texCoord2b.y*solid->nj, texCoord2b.z*solid->nk);

	qfePoint corner = simCoord1a;	
	qfeVector dir1(simCoord1b.x - simCoord1a.x, simCoord1b.y - simCoord1a.y, simCoord1b.z - simCoord1a.z);
	qfeVector dir2(simCoord2a.x - simCoord1a.x, simCoord2a.y - simCoord1a.y, simCoord2a.z - simCoord1a.z);

	std::vector<std::vector<Vec4f>> texture;
	texture.resize(size);
	for(unsigned int p = 0; p<size; p++)
		texture.at(p).resize(size);
	
	float color_range = range_max-range_min;

	//colormap
	std::vector<qfeColorRGB> colors;
	colorMap->qfeGetAllColorsRGB(colors);
	unsigned int max_index_colors = colors.size()-1;

	Array3b inspected(source->ni, source->nj, source->nk);
	for(unsigned int k = 0; k<inspected.nk; k++)
	for(unsigned int j = 0; j<inspected.nj; j++)
	for(unsigned int i = 0; i<inspected.ni; i++)
	{
		inspected(i,j,k) = false;
	}
	
	std::vector<float> data;

	for(unsigned int i = 0; i<size; i++)//height
	{
		float s = (float)i/(size-1);

		for(unsigned int j = 0; j<size; j++)//width 
		{
			float t = (float)j/(size-1);

			qfePoint point;
			point.x = corner.x + s*dir1.x + t*dir2.x;
			point.y = corner.y + s*dir1.y + t*dir2.y;
			point.z = corner.z + s*dir1.z + t*dir2.z;

			unsigned int x = floor(point.x); float fx = point.x - x;
			unsigned int y = floor(point.y); float fy = point.y - y;
			unsigned int z = floor(point.z); float fz = point.z - z;
					
			GLfloat r,g,b,a;

			r = (GLfloat)1.0f;
			g = (GLfloat)1.0f;
			b = (GLfloat)1.0f;			
			a = (GLfloat)0.0f;
			if(i%2 ==  j%2) //checkerboard transparency
				a = (GLfloat)1.0f;
		
			float draw = -1.0f;
			if(solid->inRange(x,y,z) && solid->inRange(x+1,y+1,z+1))
			{
				draw = solid->gridtrilerp(x,y,z,fx,fy,fz);
			}

			if(draw>0.0f)
			{
				float value = 0.0f;
				float ii = point.x/solid->ni * (float)source->ni;
				float ij = point.y/solid->nj * (float)source->nj;
				float ik = point.z/solid->nk * (float)source->nk;

				value = source->at(ii,ij,ik);
				//more smooth, use: value = source->gridtrilerp(x,y,z,fx,fy,fz);
				
				float tex = 0.0f; 
				if(value<range_min)
				{
					tex = 0.0f;
				}
				else if(value>range_max)
				{
					tex = 1.0f;
				}
				else
				{
					tex = (value-range_min)/color_range;
				}
				unsigned int col_index = floor((float) max_index_colors*tex);
				r = colors[col_index].r;
				g = colors[col_index].g;
				b = colors[col_index].b;
				a = (GLfloat)1.0f;

				if(!inspected(ii,ij,ik))
				{
					inspected(ii,ij,ik) = true;

					data.push_back(value);
				}
			}
			else if(draw<=0.0f && draw >= -boundary_thickness)
			{
				r = (GLfloat)0.0f;
				g = (GLfloat)0.0f;
				b = (GLfloat)0.0f;
				a = (GLfloat)1.0f;
			}
			
			if(i<=pointsize+2 && j<=pointsize+2) //draw dots in the corner
			{
				r = corner_color0[0];
				g = corner_color0[1];
				b = corner_color0[2];
				if(i==pointsize+2 || j==pointsize+2)
				{
					r = 0.0f; g = 0.0f; b = 0.0f;
				}
				a = 1.0f;
			}
			if(i>=size-(pointsize+2) && j<=pointsize+2)
			{
				r = corner_color1[0];
				g = corner_color1[1];
				b = corner_color1[2];
				if(i==size-(pointsize+2) || j==pointsize+2)
				{
					r = 0.0f; g = 0.0f; b = 0.0f;
				}
				a = 1.0f;
			}
			if(i>=size-(pointsize+2) && j>=size-(pointsize+2))
			{
				r = corner_color2[0];
				g = corner_color2[1];
				b = corner_color2[2];
				if(i==size-(pointsize+2) || j==size-(pointsize+2))
				{
					r = 0.0f; g = 0.0f; b = 0.0f;
				}
				a = 1.0f;
			}
			if(i<=pointsize+2 && j>=size-(pointsize+2))
			{
				r = corner_color3[0];
				g = corner_color3[1];
				b = corner_color3[2];
				if(i==pointsize+2 || j==size-(pointsize+2))
				{
					r = 0.0f; g = 0.0f; b = 0.0f;
				}
				a = 1.0f;
			}

			//draw color scheme
			if(i>=2*pointsize && i<=size-2*pointsize && j<pointsize+2)
			{
				float value = (i - 2.0f*pointsize)/((float)size - 2.0f*2*pointsize);
				unsigned int col_index = floor((float) max_index_colors*value);
				r = colors[col_index].r;
				g = colors[col_index].g;
				b = colors[col_index].b;
				if(i==2*pointsize || i==size-2*pointsize || j==0 || j==pointsize+2-1) //borders
				{
					r = 0.0f; g = 0.0f; b = 0.0f;
				}
				a = 1.0f;
			}
			
			if(i == 0 || i == size-1 || j == 0 || j == size-1) //borders
			{
				r = 0.0f;
				g = 0.0f;
				b = 0.0f;
				a = 1.0f;
			}

			texture[i][j][0] = r; 
			texture[i][j][1] = g; 
			texture[i][j][2] = b; 
			texture[i][j][3] = a;
		}
	}

	if(data.size()>0)
	{
		sort(data.begin(), data.end());

		float Q0 = data[0];
		float Q4 = data[data.size()-1];

		int index = (float)data.size()/4.0f;
		float weight = (float)data.size()/4.0f - index;
		float Q1 = (1.0f-weight)*data[index] + weight*data[index+1];

		index = (float)data.size()/2.0f;
		weight = (float)data.size()/2.0f - index;
		float Q2 = (1.0f-weight)*data[index] + weight*data[index+1];

		index = 3.0f*(float)data.size()/4.0f;
		weight = 3.0f*(float)data.size()/4.0f - index;
		float Q3 = (1.0f-weight)*data[index] + weight*data[index+1];

		std::cout<<label<<" statistics: "<<std::endl;
		std::cout<<"--- 0%:   "<<Q0<<std::endl;
		std::cout<<" |  25%:  "<<Q1<<std::endl;
		std::cout<<" =  50%:  "<<Q2<<std::endl;
		std::cout<<" |  75%:  "<<Q3<<std::endl;
		std::cout<<"--- 100%: "<<Q4<<std::endl;
	}
	else
	{
		std::cout<<"The interactor does not contain data"<<std::endl;
	}
		
	for(unsigned int i = 0; i<size; i++)//height
		for(unsigned int j = 0; j<size; j++)//width 
		{
			local_image[0+4*(i+size*j)] = texture[i][j][0];
			local_image[1+4*(i+size*j)] = texture[i][j][1];
			local_image[2+4*(i+size*j)] = texture[i][j][2];
			local_image[3+4*(i+size*j)] = texture[i][j][3];
		}
		
	texture_descriptor descr = {};
	descr.label = label;
	descr.time = time;
	descr.size = size;
	descr.range_min = range_min;
	descr.range_max = range_max;
	descr.colorMapId = colorMapId;

	std::pair<texture_descriptor, GLfloat*> pair(descr,	new GLfloat[size*size*4]());
	for(unsigned int t = 0; t<size*size*4; t++)
	{
		pair.second[t] = local_image[t];
	}

	image = new GLfloat[size*size*4]();
	for(unsigned int p = 0; p<size*size*4; p++)
	{
		image[p] = local_image[p];
	}

	//store image
	unsigned int max_value = 255;

	ofstream file;
	stringstream filename (stringstream::in | stringstream::out);
	filename << "slice_results\\" << label << "_" << time << ".ppm";
	file.open (filename.str());

	//write header:
	file << "P3\n";
	file << size << " " << size << "\n";
	file << max_value << "\n";
	
	//write content:
	for(unsigned int i = 0; i<size; i++)
	{
		for(unsigned int j = 0; j<size; j++)
		{
			float r = texture[j][i][0];
			float g = texture[j][i][1];
			float b = texture[j][i][2];
			float a = texture[j][i][3];

			if(a == 0.0f)
			{
				r = 1.0f; g = 1.0f; b = 1.0f;
			}

			r *= max_value; g *= max_value; b *= max_value;

			//write pixel to file:
			file << (unsigned int) r << " " << (unsigned int) g << " " << (unsigned int) b << "\n";
		}

	}

	file.close();
		
	delete[size*size*4] local_image;

	textures.push_back(pair);
}

qfePoint qfeFluidInteractor::computeSolidCrossing(qfePoint point, qfeVector direction)
{
	assert(solid != NULL);
	if(solid->ni == 0 && solid->nj == 0 && solid->nk == 0)
	{
		std::cout<<"SOLID HAS NO CELLS"<<std::endl;
		return qfePoint(0.0f, 0.0f, 0.0f);
	}

	if(solid->ni > 1e6 && solid->nj > 1e6 && solid->nk > 1e6)
	{
		std::cout<<"SOLID HAS TO MANY CELLS"<<std::endl;
		return qfePoint(0.0f, 0.0f, 0.0f);
	}

	qfePoint simCoord(point.x*solid->ni, point.y*solid->nj, point.z*solid->nk);

	qfeVector::qfeVectorNormalize(direction);

	//the direction vector is in the right direction in voxel coordinates

	float step_size = 0.05f;
	unsigned int max_steps = ceil((solid->ni + solid->nj + solid->nk)/step_size);
	
	unsigned int step = 0;
	bool found = false;

	Vec3f p(simCoord.x, simCoord.y, simCoord.z);
	std::vector<float> vector(3);
	vector[0] = p[0]; vector[1] = p[1]; vector[2] = p[2];
	float value = solid->interpolate_value(vector);

	qfePoint currentPoint = simCoord;

	while (step < max_steps && !found)
	{
		qfePoint nextPoint = simCoord + step*step_size*direction;
		p[0] = nextPoint.x; p[1] = nextPoint.y; p[2] = nextPoint.z;

		if(!solid->inRange(ceil(p[0]), ceil(p[1]), ceil(p[2])) || !solid->inRange(floor(p[0]), floor(p[1]), floor(p[2])))
			break;

		std::vector<float> vector(3);
		vector[0] = p[0]; vector[1] = p[1]; vector[2] = p[2];
		float value2 = solid->interpolate_value(vector);

		if(value2 <= 0.0f)
		{
			vector[0] = currentPoint.x; vector[1] = currentPoint.y; vector[2] = currentPoint.z;
			float prev_value = solid->interpolate_value(vector);

			float x = -prev_value/(value2 - prev_value);
			
			currentPoint = currentPoint + x*step_size*direction;
			found = true;
			break;
		}

		value = value2;
		currentPoint = nextPoint;

		step++;
	}

	qfePoint texResult(currentPoint.x/solid->ni, currentPoint.y/solid->nj, currentPoint.z/solid->nk);

	return texResult;
}

void qfeFluidInteractor::renderQuad()
{
	qfePoint texCenter = this->origin*V2T;

	qfePoint edge1a = this->computeSolidCrossing(texCenter,
		this->axisX);
	qfePoint edge1b = this->computeSolidCrossing(texCenter,
		qfeVector(-1.0f*this->axisX.x,-1.0f*this->axisX.y,-1.0f*this->axisX.z));
	qfePoint edge2a = this->computeSolidCrossing(texCenter,
		this->axisY);
	qfePoint edge2b = this->computeSolidCrossing(texCenter,
		qfeVector(-1.0f*this->axisY.x,-1.0f*this->axisY.y,-1.0f*this->axisY.z));
	
	qfePoint diff;
	bool axisX_selected = true;
	float magnitude = 0.0f;

	float tx = (edge1a.x-texCenter.x);
	float ty = (edge1a.y-texCenter.y);
	float tz = (edge1a.z-texCenter.z);
	if(tx*tx+ty*ty+tz*tz > magnitude)
	{
		diff.x = tx; diff.y = ty; diff.z = tz;
		magnitude = tx*tx+ty*ty+tz*tz;
		axisX_selected = false;
	}
	tx = (edge1b.x-texCenter.x);
	ty = (edge1b.y-texCenter.y);
	tz = (edge1b.z-texCenter.z);
	if(tx*tx+ty*ty+tz*tz > magnitude)
	{
		diff.x = tx; diff.y = ty; diff.z = tz;
		magnitude = tx*tx+ty*ty+tz*tz;
		axisX_selected = false;
	}
	tx = (edge2a.x-texCenter.x);
	ty = (edge2a.y-texCenter.y);
	tz = (edge2a.z-texCenter.z);
	if(tx*tx+ty*ty+tz*tz > magnitude)
	{
		diff.x = tx; diff.y = ty; diff.z = tz;
		magnitude = tx*tx+ty*ty+tz*tz;
		axisX_selected = true;
	}
	tx = (edge2b.x-texCenter.x);
	ty = (edge2b.y-texCenter.y);
	tz = (edge2b.z-texCenter.z);
	if(tx*tx+ty*ty+tz*tz > magnitude)
	{
		diff.x = tx; diff.y = ty; diff.z = tz;
		magnitude = tx*tx+ty*ty+tz*tz;
		axisX_selected = true;
	}

	diff.x = offset * diff.x; diff.y = offset * diff.y; diff.z = offset * diff.z; 

	qfePoint texCoord1a;
	qfePoint texCoord1b;
	qfePoint texCoord2a;
	qfePoint texCoord2b;

	//axis1
	if(axisX_selected)
	{
		texCoord1a.x = edge1a.x + diff.x; texCoord1a.y = edge1a.y + diff.y; texCoord1a.z = edge1a.z + diff.z;	
		texCoord1b.x = edge1a.x - diff.x; texCoord1b.y = edge1a.y - diff.y; texCoord1b.z = edge1a.z - diff.z;	
		texCoord2a.x = edge1b.x + diff.x; texCoord2a.y = edge1b.y + diff.y; texCoord2a.z = edge1b.z + diff.z;	
		texCoord2b.x = edge1b.x - diff.x; texCoord2b.y = edge1b.y - diff.y; texCoord2b.z = edge1b.z - diff.z;		
	}
	else
	{
		texCoord1a.x = edge2a.x + diff.x; texCoord1a.y = edge2a.y + diff.y; texCoord1a.z = edge2a.z + diff.z;	
		texCoord1b.x = edge2a.x - diff.x; texCoord1b.y = edge2a.y - diff.y; texCoord1b.z = edge2a.z - diff.z;	
		texCoord2a.x = edge2b.x + diff.x; texCoord2a.y = edge2b.y + diff.y; texCoord2a.z = edge2b.z + diff.z;	
		texCoord2b.x = edge2b.x - diff.x; texCoord2b.y = edge2b.y - diff.y; texCoord2b.z = edge2b.z - diff.z;
	}

	//scale
	texCoord1a.x -= offset * (texCenter.x - texCoord1a.x); texCoord1a.y -= offset * (texCenter.y - texCoord1a.y); texCoord1a.z -= offset * (texCenter.z - texCoord1a.z);
	texCoord1b.x -= offset * (texCenter.x - texCoord1b.x); texCoord1b.y -= offset * (texCenter.y - texCoord1b.y); texCoord1b.z -= offset * (texCenter.z - texCoord1b.z);
	texCoord2a.x -= offset * (texCenter.x - texCoord2a.x); texCoord2a.y -= offset * (texCenter.y - texCoord2a.y); texCoord2a.z -= offset * (texCenter.z - texCoord2a.z);
	texCoord2b.x -= offset * (texCenter.x - texCoord2b.x); texCoord2b.y -= offset * (texCenter.y - texCoord2b.y); texCoord2b.z -= offset * (texCenter.z - texCoord2b.z);
		
	qfePoint gl1 = (texCoord1a * T2V) * V2P;
	qfePoint gl2 = (texCoord1b * T2V) * V2P;
	qfePoint gl3 = (texCoord2b * T2V) * V2P;		
	qfePoint gl4 = (texCoord2a * T2V) * V2P;
	
	glPointSize((float)pointsize+2.0f);	
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_POINTS); //black border
		glVertex3f(gl1.x,gl1.y,gl1.z);
		glVertex3f(gl2.x,gl2.y,gl2.z);
		glVertex3f(gl3.x,gl3.y,gl3.z);
		glVertex3f(gl4.x,gl4.y,gl4.z);
	glEnd();
	glPointSize((float)pointsize);	
	glBegin(GL_POINTS); //actual points				
		glColor3f(corner_color0[0], corner_color0[1], corner_color0[2]);	glVertex3f(gl1.x,gl1.y,gl1.z);
		glColor3f(corner_color1[0], corner_color1[1], corner_color1[2]);	glVertex3f(gl2.x,gl2.y,gl2.z);
		glColor3f(corner_color2[0], corner_color2[1], corner_color2[2]);	glVertex3f(gl3.x,gl3.y,gl3.z);
		glColor3f(corner_color3[0], corner_color3[1], corner_color3[2]);	glVertex3f(gl4.x,gl4.y,gl4.z);
	glEnd();

	glLineWidth(2.0f);	
	glColor3f(0.0f, 0.0f, 0.0f);	
	glBegin(GL_LINE_LOOP);
		glVertex3f(gl1.x,gl1.y,gl1.z);
		glVertex3f(gl2.x,gl2.y,gl2.z);
		glVertex3f(gl3.x,gl3.y,gl3.z);
		glVertex3f(gl4.x,gl4.y,gl4.z);
	glEnd();
}