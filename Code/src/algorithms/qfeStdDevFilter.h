#pragma once

#include "qfeAlgorithm.h"
#include "qfeVolume.h"

/**
* \file   qfeStdDevFilter.h
* \author Arjan Broos
* \class  qfeStdDevFilter
* \brief  Algorithm that generates a filter for the magnitude data based on the standard deviation through time
* \note   Confidential
*
*/
class qfeStdDevFilter : public qfeAlgorithm {
public:
  qfeStdDevFilter();
  ~qfeStdDevFilter();

  void qfeSetData(qfeVolumeSeries *data);
  void qfeSetThreshold(float percentage);
  void qfeSetVelocityFilterExponent(float exponent);
  void qfeSetMagnitudeFilterExponent(float exponent);
  void qfeSetPhaseSpan(unsigned phases);
  void qfeUpdate();
  void qfeApplyFilterVelocity(qfeVolumeSeries *velocityData, bool inverted);
  void qfeApplyFilterMagnitude(qfeVolumeSeries *magnitudeData, bool inverted);

private:
  bool below;

  unsigned size;
  unsigned width, height, depth;
  unsigned nrFrames;
  unsigned phaseSpan;

  float velExponent;
  float magExponent;
  float threshold; // Percentage based on rangeMin and rangeMax, not an absolute value

  float **meansx;
  float **meansy;
  float **meansz;
  float **sq_sumsx;
  float **sq_sumsy;
  float **sq_sumsz;
  float **stddevsx;
  float **stddevsy;
  float **stddevsz;

  void ResetToZero();
};