#pragma once

#include "qfeAlgorithm.h"
#include "qfeVolume.h"

/**
* \file   qfeTMIPFilter.h
* \author Arjan Broos
* \class  qfeTMIPFilter
* \brief  Algorithm that generates a filter for the magnitude data based on the tMIP
* \note   Confidential
*
* For every voxel that has a tMIP speed value below the threshold:
* set the magnitude data to 0.
*
*/
class qfeTMIPFilter : public qfeAlgorithm {
public:
  qfeTMIPFilter();

  void qfeSetData(qfeVolume *tMIP);
  void qfeSetThreshold(float percentage);
  void qfeUpdate();
  void qfeApplyFilter(qfeVolumeSeries *magnitudeData, bool inverted);

private:
  qfeVolume *tmip;

  unsigned size;
  unsigned width, height, depth;

  float rangeMin, rangeMax;
  float threshold; // Percentage based on rangeMin and rangeMax, not an absolute value
};