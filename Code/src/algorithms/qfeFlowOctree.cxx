#include "qfeFlowOctree.h"

//----------------------------------------------------------------------------
qfeFlowOctree::qfeFlowOctree()
{
  this->firstRender = true;

  this->octree.clear();
  this->octreeGeoVBO              = 0;
  this->octreeGeoVBOSize          = 0;
  this->octreeGeoVBOCoherence     = 0;
  this->octreeGeoVBOCoherenceSize = 0;
  this->octreeGeo.clear();

  this->currentCoherence = 0.0;
  this->currentLevel     = 0;

  this->flowVolume = NULL;
};

//----------------------------------------------------------------------------
qfeFlowOctree::qfeFlowOctree(const qfeFlowOctree &ot)
{
  this->octree                    = ot.octree;
  this->octreeGeoVBOCoherence     = ot.octreeGeoVBOCoherence;
  this->octreeGeoVBOCoherenceSize = ot.octreeGeoVBOCoherenceSize;
  this->octreeGeo                 = ot.octreeGeo;

  this->firstRender               = ot.firstRender;
  this->flowVolume                = ot.flowVolume;   

  this->currentCoherence          = ot.currentCoherence;
  this->currentLevel              = ot.currentLevel;

};

//----------------------------------------------------------------------------
qfeFlowOctree::~qfeFlowOctree()
{
  for(int i=0; i<(int)this->octree.size(); i++)
    free(this->octree[i]->data);

  this->octree.clear();
  this->octreeGeo.clear();

  if(glIsBuffer(this->octreeGeoVBOCoherence)) glDeleteBuffers(1, &this->octreeGeoVBOCoherence);

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeBuildFlowOctree(qfeVolume *volume)
{
  if(volume == NULL) return qfeError;

  this->flowVolume = volume;
  
  int current = -1;
  
  this->octree.push_back(new qfeFlowOctreeLayer);
  this->qfeVolumeToLayer(volume, &this->octree.back());  
  current++;

  while((this->octree[current]->dimsActual[0] > 1) || 
        (this->octree[current]->dimsActual[1] > 1) || 
        (this->octree[current]->dimsActual[2] > 1))
  {
    this->octree.push_back(new qfeFlowOctreeLayer);
    this->qfeBuildLayer(this->octree[current], &this->octree.back());    
    current++;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeWriteTest()
{  
  unsigned int dims[3];
  
  int level      = 6;
  int layerIndex = ((int)this->octree.size() -1) - level;

  qfeFlowOctreeLayer *layer = this->octree[layerIndex];
  dims[0] = this->octree.front()->dimsActual[0];
  dims[1] = this->octree.front()->dimsActual[1];
  dims[2] = floor(this->octree.front()->dimsActual[2]/4.0f);

  qfeFloat  coh;
  qfeVector v;
  qfePoint point;

  v.x = 0.0; v.y = 0.0; v.z = 1.0;
  
  // Write to file
  ofstream myfile;
  myfile.open ("octree_slice.txt");  
  myfile << "{";
  for(int y=0; y<(int)dims[1]; y++)
  {
    myfile << "{";
    for(int x=0; x<(int)dims[0]; x++)
    {      
      point.x = x;
      point.y = y;
      point.z = dims[2];

      //this->qfeTakeNode(layer, point, &n);
      this->qfeGetPointCoherence(v, point, level, coh);

      if(x == dims[0]-1)
        myfile << coh;
      else
        myfile << coh << ", ";
    }    
    if(y == dims[1]-1) 
      myfile << "}\n";
    else 
      myfile << "},\n";
  } myfile << "}";
  myfile.close();
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeBuildFlowOctreeGeometry(vector< qfeFlowOctreeLayer * > octree, int level, GLuint &octreeGeoVBO, GLint &vertexCount)
{
  int    layer;
  int    octreeSize;
  int    voxelActualDims[3];
  int    voxelConceptualDim;
  double textureConceptualDims[3];

  int    layerIndex;
  int    conceptualDivision;
  double blockSize[3];
  int    dataSize;

  vector< float > octreeVertices; 

  if(this->flowVolume == NULL) return qfeError;

  octreeVertices.clear();
  octreeSize = this->octree.size();

  if(level < 0 || level >= octreeSize) return qfeError;

  layer      = level;
  layerIndex = (octreeSize-1)-layer;

  voxelActualDims[0] = octree.front()->dimsActual[0];
  voxelActualDims[1] = octree.front()->dimsActual[1];
  voxelActualDims[2] = octree.front()->dimsActual[2];

  voxelConceptualDim = octree.front()->dimsConceptual;

  textureConceptualDims[0] = voxelConceptualDim / (double) voxelActualDims[0];
  textureConceptualDims[1] = voxelConceptualDim / (double) voxelActualDims[1];
  textureConceptualDims[2] = voxelConceptualDim / (double) voxelActualDims[2];

  conceptualDivision = (int)pow(2.0f,(float)layer);

  blockSize[0] =  textureConceptualDims[0] / (double) conceptualDivision;
  blockSize[1] =  textureConceptualDims[1] / (double) conceptualDivision;
  blockSize[2] =  textureConceptualDims[2] / (double) conceptualDivision;

  for(int z=0; z<conceptualDivision; z++)
  {
    for(int y=0; y<conceptualDivision; y++)
    {
      for(int x=0; x<conceptualDivision; x++)
      {
        double region[2][3];

        // Determine minimum corner
        region[0][0] = x*blockSize[0];
        region[0][1] = y*blockSize[1];
        region[0][2] = z*blockSize[2];

        if((region[0][0] < 1.0) && (region[0][1] < 1.0) && (region[0][2] < 1.0))
        {
          // Determine minimum corner
          region[1][0] = (x+1)*blockSize[0];
          region[1][1] = (y+1)*blockSize[1];
          region[1][2] = (z+1)*blockSize[2];

          // Clamp to 1
          if(region[0][0] > 1.0) region[0][0] = 1.0;
          if(region[0][1] > 1.0) region[0][1] = 1.0;
          if(region[0][2] > 1.0) region[0][2] = 1.0;

          if(region[1][0] > 1.0) region[1][0] = 1.0;
          if(region[1][1] > 1.0) region[1][1] = 1.0;
          if(region[1][2] > 1.0) region[1][2] = 1.0;

          this->qfeDrawBlock(region[0][0], region[1][0], region[0][1], region[1][1], region[0][2], region[1][2], octreeVertices);
        }        
      }
    }
  }

  // Fill the VBO
  if(glIsBuffer(octreeGeoVBO)) glDeleteBuffers(1, &octreeGeoVBO);

  vertexCount  = octreeVertices.size();
  dataSize     = vertexCount * sizeof(GLfloat);

  glGenBuffers(1, &octreeGeoVBO);    
  glBindBuffer(GL_ARRAY_BUFFER, octreeGeoVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &octreeVertices[0], GL_STATIC_DRAW);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// sample in patient coordinates
qfeReturnStatus qfeFlowOctree::qfeBuildFlowOctreeGeometry(vector< qfeFlowOctreeLayer * > octree, qfePoint sample, int level, GLuint &octreeGeoVBO, GLint &vertexCount)
{
  int         layer;
  int         octreeSize;
  int         voxelActualDims[3];  
  int         voxelConceptualDim;
  double      textureConceptualDims[3];

  qfeMatrix4f P2V;
  qfePoint    sampleVoxelCoords;
  int         sampleOctreeCoords[3];

  int    layerIndex;
  int    conceptualDivision;
  double blockSize[3];
  int    dataSize;

  vector< float > octreeVertices; 

  if(this->flowVolume == NULL) return qfeError;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, this->flowVolume);

  octreeVertices.clear();
  octreeSize = this->octree.size();

  if(level < 0 || level >= octreeSize) return qfeError;

  layer      = level;
  layerIndex = (octreeSize-1)-layer;

  voxelActualDims[0] = octree.front()->dimsActual[0];
  voxelActualDims[1] = octree.front()->dimsActual[1];
  voxelActualDims[2] = octree.front()->dimsActual[2];

  voxelConceptualDim = octree.front()->dimsConceptual;

  textureConceptualDims[0] = voxelConceptualDim / (double) voxelActualDims[0];
  textureConceptualDims[1] = voxelConceptualDim / (double) voxelActualDims[1];
  textureConceptualDims[2] = voxelConceptualDim / (double) voxelActualDims[2];

  conceptualDivision = (int)pow(2.0f,(float)layer);

  blockSize[0] =  textureConceptualDims[0] / (double) conceptualDivision;
  blockSize[1] =  textureConceptualDims[1] / (double) conceptualDivision;
  blockSize[2] =  textureConceptualDims[2] / (double) conceptualDivision;

  sampleVoxelCoords     =  sample * P2V;
  sampleOctreeCoords[0] = (sampleVoxelCoords.x / (double)voxelConceptualDim) * conceptualDivision;
  sampleOctreeCoords[1] = (sampleVoxelCoords.y / (double)voxelConceptualDim) * conceptualDivision;
  sampleOctreeCoords[2] = (sampleVoxelCoords.z / (double)voxelConceptualDim) * conceptualDivision;

  double region[2][3];

  // Determine minimum corner
  region[0][0] = sampleOctreeCoords[0]*blockSize[0];
  region[0][1] = sampleOctreeCoords[1]*blockSize[1];
  region[0][2] = sampleOctreeCoords[2]*blockSize[2];

  if((region[0][0] < 1.0) && (region[0][1] < 1.0) && (region[0][2] < 1.0))
  {
    // Determine minimum corner
    region[1][0] = (sampleOctreeCoords[0]+1)*blockSize[0];
    region[1][1] = (sampleOctreeCoords[1]+1)*blockSize[1];
    region[1][2] = (sampleOctreeCoords[2]+1)*blockSize[2];

    // Clamp to 1
    if(region[0][0] > 1.0) region[0][0] = 1.0;
    if(region[0][1] > 1.0) region[0][1] = 1.0;
    if(region[0][2] > 1.0) region[0][2] = 1.0;

    if(region[1][0] > 1.0) region[1][0] = 1.0;
    if(region[1][1] > 1.0) region[1][1] = 1.0;
    if(region[1][2] > 1.0) region[1][2] = 1.0;

    this->qfeDrawBlock(region[0][0], region[1][0], region[0][1], region[1][1], region[0][2], region[1][2], octreeVertices);
  }        

  // Fill the VBO
  if(glIsBuffer(octreeGeoVBO)) glDeleteBuffers(1, &octreeGeoVBO);

  vertexCount  = octreeVertices.size();
  dataSize     = vertexCount * sizeof(GLfloat);

  glGenBuffers(1, &octreeGeoVBO);    
  glBindBuffer(GL_ARRAY_BUFFER, octreeGeoVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &octreeVertices[0], GL_STATIC_DRAW);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeBuildFlowOctreeGeometryCoherence(vector< qfeFlowOctreeLayer * > octree, qfeFloat coherence, GLuint &octreeGeoVBO, GLint &vertexCount)
{
  vector< float > octreeVertices; 
  int             dataSize;

  if(this->flowVolume == NULL) return qfeError;

  octreeVertices.clear();

  // Build the octree geometry
  this->qfeBuildFlowOctreeGeometryRecursive(this->octree,this->octree.size()-1, qfePoint(0.0,0.0,0.0), coherence, octreeVertices);

  // Fill the VBO
  if(glIsBuffer(octreeGeoVBO)) glDeleteBuffers(1, &octreeGeoVBO);

  vertexCount  = octreeVertices.size();
  dataSize     = vertexCount * sizeof(GLfloat);

  glGenBuffers(1, &octreeGeoVBO);    
  glBindBuffer(GL_ARRAY_BUFFER, octreeGeoVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &octreeVertices[0], GL_STATIC_DRAW);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeBuildFlowOctreeGeometryRecursive(vector< qfeFlowOctreeLayer * > octree, int layerIndex, 
                                                                   qfePoint nodeCoord, qfeFloat coherence, vector<float> &octreeGeo)
{
  int    octantCount;
  int    voxelFullDims[3];
  int    voxelActualDims[3];
  int    voxelConceptualDim;
  double textureConceptualDims[3];

  qfeFlowOctreeNode n[8];

  octantCount        = (int)pow(2.0f,(float)((octree.size()-1)-layerIndex));

  voxelActualDims[0] = octree[layerIndex]->dimsActual[0];
  voxelActualDims[1] = octree[layerIndex]->dimsActual[1];
  voxelActualDims[2] = octree[layerIndex]->dimsActual[2];

  voxelFullDims[0] = octree.front()->dimsActual[0];
  voxelFullDims[1] = octree.front()->dimsActual[1];
  voxelFullDims[2] = octree.front()->dimsActual[2];
  
  voxelConceptualDim  = octree.front()->dimsConceptual;

  textureConceptualDims[0] = voxelConceptualDim / (double) voxelFullDims[0];
  textureConceptualDims[1] = voxelConceptualDim / (double) voxelFullDims[1];
  textureConceptualDims[2] = voxelConceptualDim / (double) voxelFullDims[2];  
  
  // Root node
  if(octantCount == 1)
  {
    // Traverse one layer down and get the nodes
    this->qfeTakeOctant(octree[layerIndex-1], qfePoint(0.0,0.0,0.0), n);
        
    for(int i=0; i<2; i++)
    {
      for(int j=0; j<2; j++)
      {
        for(int k=0; k<2; k++)
        {
          this->qfeBuildFlowOctreeGeometryRecursive(octree, layerIndex-1, qfePoint(i,j,k), coherence, octreeGeo);          
        }
      }
    }    
  }
  // Internal node
  else
  {    
    qfeFloat     parentCoherence, nodeCoherence;
    qfePoint     parentCoord;
    unsigned int parentDims[3];

    // Check if the current position is part of the actual octree
    if((nodeCoord.x < voxelActualDims[0]) && 
       (nodeCoord.y < voxelActualDims[1]) && 
       (nodeCoord.z < voxelActualDims[2]))
    {     
      parentCoord.x = floor(nodeCoord.x/2.0f);
      parentCoord.y = floor(nodeCoord.y/2.0f);
      parentCoord.z = floor(nodeCoord.z/2.0f);
      parentDims[0]  = octree[layerIndex+1]->dimsActual[0];
      parentDims[1]  = octree[layerIndex+1]->dimsActual[1];
      parentDims[2]  = octree[layerIndex+1]->dimsActual[2];
      
      int parentIndex = parentCoord.x + parentDims[0]*parentCoord.y + parentDims[0]*parentDims[1]*parentCoord.z;
      int nodeIndex   = nodeCoord.x + voxelActualDims[0]*nodeCoord.y + voxelActualDims[0]*voxelActualDims[1]*nodeCoord.z;

      parentCoherence = octree[layerIndex+1]->data[parentIndex].coherence; 
      nodeCoherence   = octree[layerIndex]->data[nodeIndex].coherence;

      // Check coherence condition
      // Build the block if node is coherent, also draw if we hit a leave node   
      if((nodeCoherence >= coherence) || (layerIndex <= 0)) 
      {        
        qfePoint blockPos;
        double   blockSize[3];
        double   region[2][3];           

        // Determine the block size at this layer
        blockSize[0] =  textureConceptualDims[0] / (double) octantCount;
        blockSize[1] =  textureConceptualDims[1] / (double) octantCount;
        blockSize[2] =  textureConceptualDims[2] / (double) octantCount;

        // Determine minimum corner
        region[0][0] = nodeCoord.x*blockSize[0];
        region[0][1] = nodeCoord.y*blockSize[1];
        region[0][2] = nodeCoord.z*blockSize[2];

        // Determine maximum corner
        region[1][0] = (nodeCoord.x+1)*blockSize[0];
        region[1][1] = (nodeCoord.y+1)*blockSize[1];
        region[1][2] = (nodeCoord.z+1)*blockSize[2];

        // Clamp to 1
        if(region[0][0] > 1.0) region[0][0] = 1.0;
        if(region[0][1] > 1.0) region[0][1] = 1.0;
        if(region[0][2] > 1.0) region[0][2] = 1.0;

        if(region[1][0] > 1.0) region[1][0] = 1.0;
        if(region[1][1] > 1.0) region[1][1] = 1.0;
        if(region[1][2] > 1.0) region[1][2] = 1.0;

        this->qfeDrawBlock(region[0][0], region[1][0], region[0][1], region[1][1], region[0][2], region[1][2], octreeGeo);
      }
      // Otherwise traverse down the tree
      else
      {        
        if(layerIndex > 0)
        {
          // Traverse one layer down and get the nodes
          this->qfeTakeOctant(octree[layerIndex-1], nodeCoord, n);
          
          qfePoint childPoint;                

          for(int i=0; i<2; i++)
          {
            for(int j=0; j<2; j++)
            {
              for(int k=0; k<2; k++)
              {
                childPoint.x = nodeCoord.x * 2.0f;
                childPoint.y = nodeCoord.y * 2.0f;
                childPoint.z = nodeCoord.z * 2.0f;   
                childPoint   = childPoint + qfePoint(i,j,k);

                this->qfeBuildFlowOctreeGeometryRecursive(octree, layerIndex-1, childPoint, coherence, octreeGeo);                
              }
            }
          }          
        }
      }    
    }
  } 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeRenderOctree(qfeFloat coherence)
{
  int         linesCount;
  qfeMatrix4f V2P, T2V;
  GLfloat     matrix[16];

  // Rebuild the octree geometry only when it is changed
  if(this->currentCoherence != coherence)
  {
    this->qfeBuildFlowOctreeGeometryCoherence(this->octree, coherence, this->octreeGeoVBOCoherence, this->octreeGeoVBOCoherenceSize);
    
    this->currentCoherence = coherence;
  }

  // Determine coordinate transformation texture >> patient
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, this->flowVolume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, this->flowVolume);  
  qfeMatrix4f::qfeGetMatrixElements(T2V*V2P, &matrix[0]);

  linesCount = (int)(this->octreeGeoVBOCoherenceSize / 3.0);

  // Draw the octree division, based on the given coherence value
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(&matrix[0]); 

  glLineWidth(1.0);  
  glColor3f(0.3,0.3,0.3);

  if(!glIsBuffer(this->octreeGeoVBOCoherence)) return qfeError;

  glBindBuffer(GL_ARRAY_BUFFER, this->octreeGeoVBOCoherence);
  glEnableClientState(GL_VERTEX_ARRAY);

  glVertexPointer(3, GL_FLOAT, 0, 0); 
  glDrawArrays(GL_LINES, 0, linesCount);

  glDisableClientState(GL_VERTEX_ARRAY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;

}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeRenderOctree(int level)
{
  int         linesCount;
  qfeMatrix4f V2P, T2V;
  GLfloat     matrix[16];

  // Rebuild the octree geometry only when it is changed
  if((this->currentLevel != level) && (level <= (int)(this->octree.size()-1)))
  {
    this->qfeBuildFlowOctreeGeometry(this->octree, level, this->octreeGeoVBO, this->octreeGeoVBOSize);

    this->currentLevel = level;
  }

  // Determine coordinate transformation texture >> patient
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, this->flowVolume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, this->flowVolume);  
  qfeMatrix4f::qfeGetMatrixElements(T2V*V2P, &matrix[0]);

  linesCount = (int)(this->octreeGeoVBOSize / 3.0);

  // Draw the octree division, based on the given coherence value
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(&matrix[0]); 

  glLineWidth(1.0);  
  glColor3f(0.3,0.3,0.3);

  if(!glIsBuffer(this->octreeGeoVBO)) return qfeError;

  glBindBuffer(GL_ARRAY_BUFFER, this->octreeGeoVBO);
  glEnableClientState(GL_VERTEX_ARRAY);

  glVertexPointer(3, GL_FLOAT, 0, 0); 
  glDrawArrays(GL_LINES, 0, linesCount);

  glDisableClientState(GL_VERTEX_ARRAY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeRenderOctree(qfePoint sample, int level)
{
  int         linesCount;
  qfeMatrix4f V2P, T2V;
  GLfloat     matrix[16];

  // Rebuild the octree geometry only when it is changed
  this->qfeBuildFlowOctreeGeometry(this->octree, sample, level, this->octreeGeoVBO, this->octreeGeoVBOSize);

  // Determine coordinate transformation texture >> patient
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, this->flowVolume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, this->flowVolume);  
  qfeMatrix4f::qfeGetMatrixElements(T2V*V2P, &matrix[0]);

  linesCount = (int)(this->octreeGeoVBOSize / 3.0);

  // Draw the octree division, based on the given coherence value
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(&matrix[0]); 

  glLineWidth(1.0);  
  glColor3f(0.3,0.3,0.3);

  if(!glIsBuffer(this->octreeGeoVBO)) return qfeError;

  glBindBuffer(GL_ARRAY_BUFFER, this->octreeGeoVBO);
  glEnableClientState(GL_VERTEX_ARRAY);

  glVertexPointer(3, GL_FLOAT, 0, 0); 
  glDrawArrays(GL_LINES, 0, linesCount);

  glDisableClientState(GL_VERTEX_ARRAY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;  
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeBuildLayer(qfeFlowOctreeLayer *layer, qfeFlowOctreeLayer **newLayer)
{
  unsigned int dims[3], newDimsActual[3], newDimsConceptual;

  if(layer == NULL) return qfeError;

  dims[0] = layer->dimsActual[0];
  dims[1] = layer->dimsActual[1];
  dims[2] = layer->dimsActual[2];

  newDimsActual[0]     = (unsigned int)ceil((float)dims[0]/2);
  newDimsActual[1]     = (unsigned int)ceil((float)dims[1]/2);
  newDimsActual[2]     = (unsigned int)ceil((float)dims[2]/2);

  newDimsConceptual    = (unsigned int)((float)layer->dimsConceptual/2.0);
  
  (*newLayer)->data = (qfeFlowOctreeNode*)calloc(newDimsActual[0] * newDimsActual[1] * newDimsActual[2], sizeof(qfeFlowOctreeNode));
  (*newLayer)->dimsActual[0]     = newDimsActual[0];
  (*newLayer)->dimsActual[1]     = newDimsActual[1];
  (*newLayer)->dimsActual[2]     = newDimsActual[2];  
  (*newLayer)->dimsConceptual    = newDimsConceptual;
  
  for(int z=0; z<(int)dims[2]; z+=2)
  {
    for(int y=0; y<(int)dims[1]; y+=2)
    {
      for(int x=0; x<(int)dims[0]; x+=2)
      {   
        int               writeIndex;   
        qfeFloat          c;
        qfePoint          p;
        qfeFlowOctreeNode n[8];
        qfeTensor3f       t[8];
        qfeTensor3f       o;
        qfeFlowOctreeNode node;

        p.x = x; p.y = y; p.z = z;

        // Take the vector neighborhood from the data 
        this->qfeTakeOctant(layer, p, n);         

        // Compute the average tensor
        for(int i=0; i<8; i++)
        {           
          t[i] = n[i].orientation; 
        } 

        qfeMath::qfeComputeAverageStructureTensor(t, o);
        
        // Compute the local coherence        
        qfeMath::qfeComputeCoherenceEDC(o, c);

        // Set the node values
        node.orientation = o;
        node.coherence   = c;
        node.type        = qfeFlowOctreeNodeInternal;

        //cout << c << endl;

        // Set the tensor in the dataset
        writeIndex = int(x/2)  + (int(y/2) * newDimsActual[0]) + (int(z/2) * (newDimsActual[0]*newDimsActual[1]));

        (*newLayer)->data[writeIndex] = node;
      }
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeDrawBlock(qfeFloat xmin, qfeFloat xmax, qfeFloat ymin, qfeFloat ymax, qfeFloat zmin, qfeFloat zmax)
{
  glBegin(GL_LINE_STRIP);
  glVertex3f(xmin,ymin,zmin);
  glVertex3f(xmax,ymin,zmin);
  glVertex3f(xmax,ymax,zmin);
  glVertex3f(xmin,ymax,zmin);
  glVertex3f(xmin,ymin,zmin);
  glVertex3f(xmin,ymin,zmax);
  glVertex3f(xmin,ymax,zmax);
  glVertex3f(xmax,ymax,zmax);
  glVertex3f(xmax,ymin,zmax);
  glVertex3f(xmax,ymin,zmin);
  glEnd();

  glBegin(GL_LINES);
  glVertex3f(xmin,ymax,zmin);
  glVertex3f(xmin,ymax,zmax);

  glVertex3f(xmax,ymax,zmin);
  glVertex3f(xmax,ymax,zmax);

  glVertex3f(xmin,ymin,zmax);
  glVertex3f(xmax,ymin,zmax);
  glEnd();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeDrawBlock(qfeFloat xmin, qfeFloat xmax, qfeFloat ymin, qfeFloat ymax, qfeFloat zmin, qfeFloat zmax, vector< float > &octree)
{
  // bottom
  octree.push_back(xmin); octree.push_back(ymin); octree.push_back(zmin);
  octree.push_back(xmax); octree.push_back(ymin); octree.push_back(zmin);
  
  octree.push_back(xmax); octree.push_back(ymin); octree.push_back(zmin);
  octree.push_back(xmax); octree.push_back(ymax); octree.push_back(zmin);

  octree.push_back(xmax); octree.push_back(ymax); octree.push_back(zmin);
  octree.push_back(xmin); octree.push_back(ymax); octree.push_back(zmin);

  octree.push_back(xmin); octree.push_back(ymax); octree.push_back(zmin);
  octree.push_back(xmin); octree.push_back(ymin); octree.push_back(zmin);

  // top
  octree.push_back(xmin); octree.push_back(ymin); octree.push_back(zmax);
  octree.push_back(xmax); octree.push_back(ymin); octree.push_back(zmax);

  octree.push_back(xmax); octree.push_back(ymin); octree.push_back(zmax);
  octree.push_back(xmax); octree.push_back(ymax); octree.push_back(zmax);

  octree.push_back(xmax); octree.push_back(ymax); octree.push_back(zmax);
  octree.push_back(xmin); octree.push_back(ymax); octree.push_back(zmax);

  octree.push_back(xmin); octree.push_back(ymax); octree.push_back(zmax);
  octree.push_back(xmin); octree.push_back(ymin); octree.push_back(zmax);

  // connect
  octree.push_back(xmin); octree.push_back(ymin); octree.push_back(zmin);
  octree.push_back(xmin); octree.push_back(ymin); octree.push_back(zmax);

  octree.push_back(xmax); octree.push_back(ymin); octree.push_back(zmin);
  octree.push_back(xmax); octree.push_back(ymin); octree.push_back(zmax);

  octree.push_back(xmax); octree.push_back(ymax); octree.push_back(zmin);
  octree.push_back(xmax); octree.push_back(ymax); octree.push_back(zmax);

  octree.push_back(xmin); octree.push_back(ymax); octree.push_back(zmin);
  octree.push_back(xmin); octree.push_back(ymax); octree.push_back(zmax);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// v         - vector to compare
// p         - point in patient coordinates
// layer     - index of the octree layer 
// coherence - coherence between v and the node of the octree based on p
qfeReturnStatus qfeFlowOctree::qfeGetPointCoherence(qfeVector v, qfePoint p, int layer, qfeFloat &coherence)
{
  qfeMatrix4f         P2V;
  int                 treeLayerIndex;
  qfeFlowOctreeLayer *treeLayer;
  qfeFlowOctreeNode  *treeNode;
  qfePoint            octreePoint;
  qfeTensor3f         dirTensor;  
  int                 division;

  if((int)this->octree.size() < 0 || this->flowVolume == NULL) return qfeError;

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, this->flowVolume);

  // Get the right layer
  treeLayerIndex         = (this->octree.size()-1)-layer;
  treeLayer              = this->octree[treeLayerIndex];

  // Convert from patient coordinates to octree coordinates
  division               = (int)pow(2.0f,(float)treeLayerIndex);

  octreePoint            =  p * P2V;                              // voxel coordinates   
  octreePoint.x          = floor(octreePoint.x /float(division)); // octree coordinates
  octreePoint.y          = floor(octreePoint.y /float(division)); 
  octreePoint.z          = floor(octreePoint.z /float(division));

  // Get the node
  this->qfeTakeNode(treeLayer, octreePoint, &treeNode);

  // Compute structure tensor for the given direction
  qfeMath::qfeComputeStructureTensor(v, dirTensor);   

  // Return the coherence  
  qfeGetTensorCoherence(treeNode->orientation, dirTensor, coherence);    

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeGetLineCoherence(qfePoint p1, qfePoint p2, int layer, qfeFloat &coherence)
{
  qfeMatrix4f  P2V;
  qfeVector    dir;
  qfeVector    dirNormal;
  qfeTensor3f  dirTensor;
  qfeFloat     dirLength;  
  qfeTensor3f  currentTensor;
  qfePoint    *samples;
  qfePoint     currentPoint;
  qfeFloat     tempCoherence, totalCoherence;  
  int          conceptualDivision;
  int          voxelActualDims[3];
  int          voxelConceptualDim;  
  double       blockSize;
  int          blockCount;
  
  if((int)this->octree.size() < 0 || this->flowVolume == NULL) return qfeError;

  // Get transformation matrix
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, this->flowVolume);

  // Get the direction of the centerline and compute the structure tensor
  dir = p2 - p1;
  qfeVector::qfeVectorLength(dir * P2V, dirLength);
  
  dirNormal = dir;
  qfeVector::qfeVectorNormalize(dirNormal);  
  qfeMath::qfeComputeStructureTensor(dirNormal, dirTensor);       

  // Determine how many points we need to check at this layer
  conceptualDivision = (int)pow(2.0f,(float)layer);

  voxelActualDims[0] = octree.front()->dimsActual[0];
  voxelActualDims[1] = octree.front()->dimsActual[1];
  voxelActualDims[2] = octree.front()->dimsActual[2];  
  voxelConceptualDim = octree.front()->dimsConceptual;

  blockSize          = voxelConceptualDim / (double) conceptualDivision;
  blockCount         = dirLength / (float)blockSize;

  if(blockCount < 2) blockCount = 2;
  
  // Accumulate the coherence
  totalCoherence  = 0.0f;
  samples         = new qfePoint[blockCount];  

  for(int i=0; i<blockCount; i++)
  {
    qfeFloat  blockFraction, avgFraction;

    avgFraction   = 1.0f/(float)blockCount;    
    blockFraction = 1.0f/(float)(blockCount-1);    
    currentPoint  = p1 + (i*blockFraction)*dir;

    this->qfeGetPointCoherence(dirNormal, currentPoint, layer, tempCoherence); 

    tempCoherence < 0.0f ? tempCoherence = 0.0f : tempCoherence = tempCoherence;
    tempCoherence > 1.0f ? tempCoherence = 1.0f : tempCoherence = tempCoherence;
    
    totalCoherence += avgFraction*tempCoherence;
  }

  coherence = totalCoherence;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeGetTensorCoherence(qfeTensor3f t1, qfeTensor3f t2, qfeFloat &c)
{
  qfeVector lambda;
  qfeTensor3f avg;
  
  avg = (t1 + t2)*0.5f;

  qfeMath::qfeComputeEigenValues(avg, lambda);

  c = pow((lambda.x - lambda.y)/(lambda.x + lambda.y), 2.0f);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeTakeOctant(qfeVolume *volume, qfePoint p, qfeVector v[8])
{
  unsigned int  dims[3];
  qfeValueType  t;
  void         *data;
  qfePoint      p1;
  int           readIndex[8];
  
  if(volume == NULL) return qfeError;

  volume->qfeGetVolumeData(t, dims[0], dims[1], dims[2], &data);
    
  // Make sure the boundaries conditions are ok
  (p.x+1 < (int)dims[0]) ? p1.x = p.x+1 : p1.x = p.x;        
  (p.y+1 < (int)dims[1]) ? p1.y = p.y+1 : p1.y = p.y;
  (p.z+1 < (int)dims[2]) ? p1.z = p.z+1 : p1.z = p.z;

  // Take the vectors from the data and compute structure tensors
  readIndex[0] = p.x  + (p.y *dims[0]) + (p.z *dims[0]*dims[1]);
  readIndex[1] = p1.x + (p.y *dims[0]) + (p.z *dims[0]*dims[1]);
  readIndex[2] = p.x  + (p1.y*dims[0]) + (p.z *dims[0]*dims[1]);
  readIndex[3] = p1.x + (p1.y*dims[0]) + (p.z *dims[0]*dims[1]);
  readIndex[4] = p.x  + (p.y *dims[0]) + (p1.z*dims[0]*dims[1]);
  readIndex[5] = p1.x + (p.y *dims[0]) + (p1.z*dims[0]*dims[1]);
  readIndex[6] = p.x  + (p1.y*dims[0]) + (p1.z*dims[0]*dims[1]);
  readIndex[7] = p1.x + (p1.y*dims[0]) + (p1.z*dims[0]*dims[1]);

  for(int i=0; i<8; i++)
  {
    v[i].x = *((float*)data + 3*readIndex[i]+0);
    v[i].y = *((float*)data + 3*readIndex[i]+1);
    v[i].z = *((float*)data + 3*readIndex[i]+2);    
  }   
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeTakeOctant(qfeFlowOctreeLayer *layer, qfePoint p, qfeFlowOctreeNode n[8])
{
  unsigned int  dims[3];
  qfePoint      p1;
  int           readIndex[8];

  if(layer == NULL) return qfeError;

  dims[0] = layer->dimsActual[0];
  dims[1] = layer->dimsActual[1];
  dims[2] = layer->dimsActual[2];

  // Make sure the boundaries conditions are ok
  (p.x+1 < (int)dims[0]) ? p1.x = p.x+1 : p1.x = p.x;        
  (p.y+1 < (int)dims[1]) ? p1.y = p.y+1 : p1.y = p.y;
  (p.z+1 < (int)dims[2]) ? p1.z = p.z+1 : p1.z = p.z;

  // Take the nodes from the data 
  readIndex[0] = p.x  + (p.y *dims[0]) + (p.z *dims[0]*dims[1]);
  readIndex[1] = p1.x + (p.y *dims[0]) + (p.z *dims[0]*dims[1]);
  readIndex[2] = p.x  + (p1.y*dims[0]) + (p.z *dims[0]*dims[1]);
  readIndex[3] = p1.x + (p1.y*dims[0]) + (p.z *dims[0]*dims[1]);
  readIndex[4] = p.x  + (p.y *dims[0]) + (p1.z*dims[0]*dims[1]);
  readIndex[5] = p1.x + (p.y *dims[0]) + (p1.z*dims[0]*dims[1]);
  readIndex[6] = p.x  + (p1.y*dims[0]) + (p1.z*dims[0]*dims[1]);
  readIndex[7] = p1.x + (p1.y*dims[0]) + (p1.z*dims[0]*dims[1]);

  if(layer->data == NULL) return qfeError;

  for(int i=0; i<8; i++)
  {
    n[i] = *((qfeFlowOctreeNode*)layer->data + readIndex[i]);         
  }   

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// layer - current layer of the octree
// p     - point in the octree in octree positions
qfeReturnStatus qfeFlowOctree::qfeTakeNode(qfeFlowOctreeLayer *layer, qfePoint p, qfeFlowOctreeNode **n)
{
  unsigned int  dims[3];  
  int           readIndex;

  if(layer == NULL) return qfeError;

  dims[0] = layer->dimsActual[0];
  dims[1] = layer->dimsActual[1];
  dims[2] = layer->dimsActual[2];

  // Make sure the boundaries conditions are ok
  (p.x < 0)             ? p.x = 0         : p.x = p.x;        
  (p.y < 0)             ? p.y = 0         : p.y = p.y;
  (p.z < 0)             ? p.z = 0         : p.z = p.z;
  (p.x >= (int)dims[0]) ? p.x = dims[0]-1 : p.x = p.x;        
  (p.y >= (int)dims[1]) ? p.y = dims[1]-1 : p.y = p.y;
  (p.z >= (int)dims[2]) ? p.z = dims[2]-1 : p.z = p.z;

  // Take the node from the data 
  readIndex = p.x  + (p.y *dims[0]) + (p.z *dims[0]*dims[1]);
  
  if(readIndex < 0 || readIndex >= (int)(dims[0]*dims[1]*dims[2]))
  {
    *n = NULL;
    return qfeError;
  }

  *n = ((qfeFlowOctreeNode*)layer->data + readIndex);           
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeGetVoxelToOctree(qfePoint pointVoxel, int octreeLevel, qfePoint &pointOctree)
{
  int division;

  if(this->octree.size() < 0) return qfeError;

  division = (int)pow(2.0f,(float)((this->octree.size()-1)-octreeLevel));

  pointOctree            = pointVoxel;
  pointOctree.x          = floor(pointOctree.x /float(division)); 
  pointOctree.y          = floor(pointOctree.y /float(division)); 
  pointOctree.z          = floor(pointOctree.z /float(division));

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeVolumeToLayer(qfeVolume *volume, qfeFlowOctreeLayer **layer)
{
  unsigned int dims[3];
  qfeValueType t;
  unsigned int nc;
  void        *data;
  unsigned int dimMax;  
  int          dimConceptual;

  if(volume == NULL) return qfeError;

  volume->qfeGetVolumeData(t, dims[0], dims[1], dims[2], &data);
  volume->qfeGetVolumeComponentsPerVoxel(nc);

  if(nc!=3 && nc!=9) return qfeError;

  dimMax = max(max(dims[0], dims[1]), dims[2]);
  this->qfeGetNextPowerOfTwo(dimMax, dimConceptual);

  (*layer)->data = (qfeFlowOctreeNode*)calloc(dims[0] * dims[1] * dims[2], sizeof(qfeFlowOctreeNode));
  (*layer)->dimsActual[0]     = dims[0];
  (*layer)->dimsActual[1]     = dims[1];
  (*layer)->dimsActual[2]     = dims[2]; 
  (*layer)->dimsConceptual    = dimConceptual;
  
  for(int z=0; z<(int)dims[2]; z++)
  {
    for(int y=0; y<(int)dims[1]; y++)
    {
      for(int x=0; x<(int)dims[0]; x++)
      { 
        int          readIndex;
        int          writeIndex;         
        qfeVector    v;
        qfeTensor3f  t;
        qfeTensor3f  o;
        qfeVector    l;
        qfeFlowOctreeNode node;

        // Get the vector
        readIndex = x  + (y *dims[0]) + (z *dims[0]*dims[1]);

        if(nc == 3)
        {
          v.x = *((float*)data + nc*readIndex+0);
          v.y = *((float*)data + nc*readIndex+1);
          v.z = *((float*)data + nc*readIndex+2);

          // Compute the structure tensor   
          qfeMath::qfeComputeStructureTensor(v, t);   
        }
        else
        {
          t(0,0) = *((float*)data + nc*readIndex+0);
          t(0,1) = *((float*)data + nc*readIndex+1);
          t(0,2) = *((float*)data + nc*readIndex+2);
          t(1,0) = *((float*)data + nc*readIndex+3);
          t(1,1) = *((float*)data + nc*readIndex+4);
          t(1,2) = *((float*)data + nc*readIndex+5);
          t(2,0) = *((float*)data + nc*readIndex+6);
          t(2,1) = *((float*)data + nc*readIndex+7);
          t(2,2) = *((float*)data + nc*readIndex+8);
        }

        // Set the node values
        node.orientation = t;
        node.coherence   = 1.0;
        node.type        = qfeFlowOctreeNodeLeaf;

        // Add node to the tree
        writeIndex = x  + (y * dims[0]) + (z * (dims[0]*dims[1]));

        if((*layer)->data == NULL) return qfeError;

        (*layer)->data[writeIndex] = node;  
      }
    }
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowOctree::qfeGetNextPowerOfTwo(int i, int &n)
{
	n = 1;
	while(n < i)
		n *= 2;

	return qfeSuccess;
}


