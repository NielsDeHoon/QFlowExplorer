#pragma once

#include "qfeAlgorithmFlow.h"
#include "qfeVolume.h"
#include "qfeStudy.h" // TODO remove
#include "qfeClusterHierarchy.h"

#include "qfeAlgorithm.h"
#include "qfeClusterNodeElliptic.h"
#include "qfeClusterNodeElliptic4D.h"
#include "qfeClusterNodeL2Norm.h"
#include "qfeClusterNodeLinearModel.h"
#include "qfeClusterNodeScalar.h"
#include "qfeTransform.h"

#include <list>
#include <set>
#include <vector>
#include <algorithm>

/**
* \file   qfeHierarchicalClustering.h
* \author Sander Jacobs, Roy van Pelt
* \class  qfeHierarchicalClustering
* \brief  
* \note   Confidential
*
*/
class qfeHierarchicalClustering : public qfeAlgorithm
{
public:
  enum qfeClusterCentersType
  {
    qfeCenterOfGravity4D,
    qfeCenterStochastic3D,
    qfeCenterStochastic4D
  };

  qfeHierarchicalClustering();
  ~qfeHierarchicalClustering();

  // clustering functions
  qfeReturnStatus qfeSelectLeavesByVolume(    qfeClusterHierarchy *hierarchy, qfeVolume *volume);
  qfeReturnStatus qfeSelectLeavesByThreshold( qfeClusterHierarchy *hierarchy, qfeVolume *volume, double threshold);
  qfeReturnStatus qfeSelectLeavesByPercentage(qfeClusterHierarchy *hierarchy, qfeVolume *volume, double percentage);
  qfeReturnStatus qfeSelectLeavesByZSlice(    qfeClusterHierarchy *hierarchy, qfeVolume *volume, unsigned int zSliceIndex);

  qfeReturnStatus qfeInitializeLeafNodes(qfeClusterHierarchy *hierarchy, qfeVolumeSeries volumes);

  qfeReturnStatus qfeAddClusterDistance(qfeClusterNode *c1, qfeClusterNode *c2, qfeDistanceListNode* dPtr);
  qfeReturnStatus qfePrintDistanceList();

  qfeReturnStatus qfeProcessSmallestDistance(    qfeClusterHierarchy *hierarchy);
  qfeReturnStatus qfeProcessDistancesByThreshold(qfeClusterHierarchy *hierarchy, qfeFloat distanceThreshold);

  qfeReturnStatus qfeBuildTree(qfeClusterHierarchy *hierarchy);

  qfeReturnStatus qfeGetInitialClusterLabels( qfeClusterHierarchy *hierarchy, qfeVolumeSeries *clusters);
  qfeReturnStatus qfeGetClusterLabels(        qfeClusterHierarchy *hierarchy, qfeVolumeSeries *clusters, int level);
  
  // rendering functions  
  qfeReturnStatus qfeGetClusterVoxelPositions(qfeClusterHierarchy *hierarchy, qfeVolume *boundingVolume, int phaseCount, int level, qfeVoxelSeries &clusters);
  qfeReturnStatus qfeGetClusterCenterPositions(qfeVoxelSeries *clusters, qfeClusterCentersType type, int samples, qfeVoxelArray &centers);
  
  qfeReturnStatus qfeGetCenterOfGravity(qfeVoxelArray *voxels, qfePoint &center, unsigned int &phase);
  qfeReturnStatus qfeGetCenterStochastic3D(qfeVoxelArray *voxels, unsigned int phase, int sampleCount, qfePoint &center);
  qfeReturnStatus qfeGetCenterStochastic4D(qfeVoxelArray *voxels, int sampleCount, qfePoint &center, unsigned int &phase);

  //qfeReturnStatus qfeGeneratePathlines(

  qfeReturnStatus qfeRenderClusterCenters(qfeVolume *boundingVolume, qfeVoxelArray *centers, int phase);  

protected:
  qfeReturnStatus qfeGetVoxelCoord(qfeVolume *boundingVolume, unsigned int voxelIndex, qfePoint &voxelCoord);
  
private:

  // Distance list variables
  list<int>            *clusterDefinitions;
  int                   clusterCount;
  qfeDistanceListNode  *clusterDistances;
  qfeDistanceListNode  *firstDistance;
  qfeDistanceListNode  *lastDistance;
  qfeFloat              smallestDistance;

  // Other variables
  
  time_t                displayTime;

  unsigned int          totalDistanceCount;
  unsigned int          computedDistanceCount;
  unsigned int          processedDistanceCount;  
};