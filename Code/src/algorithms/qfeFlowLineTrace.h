#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithmFlow.h"
#include "qfeColorMap.h"
#include "qfeDisplay.h"
#include "qfeGLShaderProgram.h"
#include "qfeGLSupport.h"
#include "qfeLightSource.h"
#include "qfeMath.h"
#include "qfeMatrix4f.h"
#include "qfePoint.h"
#include "qfeSeeding.h"
#include "qfeTransform.h"
#include "qfeVolume.h"

#include <vector>

#define MAX_SEEDS 8192
#define BUFFER_OFFSET(i) ((char*)NULL + (i))

typedef vector< qfePoint >  qfeParticles;

enum qfeFlowLinesStyle
{
  qfeFlowLinesTuboid,
  qfeFlowLinesLine,  
  qfeFlowLinesArrow
};

enum qfeFlowSeedingBehavior
{
  qfeFlowLinesPathDynamic,
  qfeFlowLinesPathStatic,
  qfeFlowLinesStream
};

enum qfeFlowLinesShading
{
	qfeFlowLinesCel,
	qfeFlowLinesPhongAdditive,
  qfeFlowLinesPhongMultiplicative,
  qfeFlowLinesNone
};

enum qfeFlowLinesIntegrationScheme
{
  qfeFlowLinesRK1,
  qfeFlowLinesRK2,
  qfeFlowLinesRK4
};

/**
* \file   qfeFlowLineTrace.h
* \author Roy van Pelt
* \class  qfeFlowLineTrace
* \brief  Implements rendering of a line trace
* \note   Confidential
*
* Rendering of a line trace
*/

class qfeFlowLineTrace : public qfeAlgorithmFlow
{
public:
  qfeFlowLineTrace();
  qfeFlowLineTrace(const qfeFlowLineTrace &fv);
  ~qfeFlowLineTrace();
  
  qfeReturnStatus qfeAddSeedPositions(qfeSeeds seeds);
  qfeReturnStatus qfeAddSeedPositions(qfeSeeds seeds, float phase);
  qfeReturnStatus qfeUpdateSeedPositions(qfeSeeds seeds, float phase);
  qfeReturnStatus qfeClearSeedPositions();
  
  qfeReturnStatus qfeGeneratePathlines(qfeVolumeSeries series, qfeFloat phase, qfeFlowSeedingBehavior type = qfeFlowLinesPathStatic);    

  qfeReturnStatus qfeRenderPathlines(qfeColorMap *colormap, qfeFlowSeedingBehavior type); 
  qfeReturnStatus qfeRenderPathlinesStatic(qfeColorMap *colormap);
  qfeReturnStatus qfeRenderPathlinesDynamic(qfeColorMap *colormap);

  qfeReturnStatus qfeRenderStyleLine(int currentBuffer, int attribBuffer, int segmentCount, int segmentSize);
  qfeReturnStatus qfeRenderStyleTuboid(int currentBuffer, int attribBuffer, int segmentCount, int segmentSize);

  qfeReturnStatus qfeSetStyle(qfeFlowLinesStyle style);
  qfeReturnStatus qfeSetTraceSteps(int steps);
  qfeReturnStatus qfeSetTraceDirection(int dir);
  qfeReturnStatus qfeSetTraceScheme(qfeFlowLinesIntegrationScheme scheme);
  qfeReturnStatus qfeSetTraceSpeed(int percentage);
  qfeReturnStatus qfeSetTracePhasesStatic(int phases);
  qfeReturnStatus qfeSetTracePhasesDynamic(int phases);
  qfeReturnStatus qfeSetTraceMaximumAngle(int degrees);  
  qfeReturnStatus qfeSetPhase(float phase);
  qfeReturnStatus qfeSetPhaseDuration(qfeFloat duration);
  qfeReturnStatus qfeSetLineShading(qfeFlowLinesShading shading);
  qfeReturnStatus qfeSetLineWidth(float width);
  qfeReturnStatus qfeSetLineContour(bool contour);
  qfeReturnStatus qfeSetLineHalo(bool halo);
  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);
  qfeReturnStatus qfeSetColorScale(int colorscale);
  qfeReturnStatus qfeSetSuperSampling(int factor); 

protected:
  int                   MAX_OUTPUT_VERTICES;
  int                   MAX_OUTPUT_SAMPLES_PER_ITERATION;
  int                   MAX_OUTPUT_SAMPLES_PER_TRACE;

  bool                  firstSeeds;  
  bool                  firstGenerate;
  bool                  firstRender;  
  qfeGLShaderProgram   *shaderProgramCopy;
  qfeGLShaderProgram   *shaderProgramGenerate;
  qfeGLShaderProgram   *shaderProgramRenderLine;  
  qfeGLShaderProgram   *shaderProgramRenderImposter;

  qfeVolumeSeries       volumeSeries;
  qfeLightSource       *light;
  GLuint                colormap;

  GLuint                seedPositionsVBO;  
  vector< qfeFloat >    seedPositions; 
  GLuint                linePositionsStatVBO;
  GLuint                lineAttributesStatVBO;
  GLuint                linePositionsDynVBO;
  GLuint                lineAttributesDynVBO;   
  unsigned int          seedsCount;

  GLint                 locationPosition;
  GLint                 locationAttribute;
  
  // Static uniform locations
  qfeMatrix4f          V2P, P2V;
  unsigned int         volumeSize[3];
  qfeFloat             phaseDuration;

  // Dynamic uniform locations
  float                phaseCurrent;     // Current phase to be updated during rendering
  float                phaseCurrentWrap; // Current phase that keeps increasing until the #cycles are done
  float                phaseGenerate;    // Current phase for generating the line
  int                  phaseCycle;       // Current cycle for traces longer than one cardiac cycle
  int                  phaseCycleCount;  // Number of cycles we are tracing  
  float                phaseStart;       // Phase of seeding
  int                  phasesPerIteration;
  qfeFlowLinesStyle    traceStyle;
  int                  traceIterations;
  int                  tracePhasesStatic;
  int                  tracePhasesDynamic;  
  int                  traceSamples;  
  int                  traceSteps;
  int                  traceStepsPhase;
  float                traceStepSize;
  float                traceStepDuration;
  int                  traceScheme;
  int                  traceSpeed;
  int                  traceDirection;
  int                  traceVertexId;
  int                  traceMaxAngle;
  int                  traceSeedingBehavior;
  qfeFlowLinesShading  lineShading;
  float                lineWidth;
  bool                 lineContour;
  bool                 lineHalo;  
  int                  colorScale;  
  int                  supersampling;

  qfeReturnStatus qfeDrawSeeds(GLuint seeds, GLint attribLocation, int count);
  qfeReturnStatus qfeDrawEndPoints(GLuint lines, GLint attributes, int currentPhase, int count);
  qfeReturnStatus qfeDrawLines(GLuint lines, GLuint attribs, int segmentCount, int segmentSize);    

  qfeReturnStatus qfeInitTraceStatic();
  qfeReturnStatus qfeInitTraceDynamic();

  qfeReturnStatus qfeInitSeedBuffer();
  qfeReturnStatus qfeClearSeedBuffer();

  qfeReturnStatus qfeInitLineBufferStatic();
  qfeReturnStatus qfeClearLineBufferStatic();

  qfeReturnStatus qfeInitLineBufferDynamic();
  qfeReturnStatus qfeClearLineBufferDynamic();
    
  qfeReturnStatus qfeBindTexturesSliding(qfeVolumeSeries series, int phase);
  qfeReturnStatus qfeBindTexturesRender();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

private :
  static const char    *feedbackVarsCopy[];
  static const char    *feedbackVarsGenerate[];
 
};
