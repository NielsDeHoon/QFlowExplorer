#include "qfeFlowLineTrace.h"

const char *qfeFlowLineTrace::feedbackVarsCopy[]     = {"qfe_LinePoints"};
const char *qfeFlowLineTrace::feedbackVarsGenerate[] = {"linePoints", "lineAttribs"};

//----------------------------------------------------------------------------
qfeFlowLineTrace::qfeFlowLineTrace()
{
  qfePoint initPos;

  initPos.qfeSetPointElements(0.0,0.0,0.0);

  this->phasesPerIteration = 2;

  // Obtain the maximum amount of output vertices for the geometry shader
  glGetIntegerv(GL_MAX_GEOMETRY_OUTPUT_VERTICES, &this->MAX_OUTPUT_VERTICES);

  //this->MAX_OUTPUT_SAMPLES_PER_ITERATION = (this->MAX_OUTPUT_VERTICES/(float)sizeof(GLfloat)) / (float)this->phasesPerIteration; 
  this->MAX_OUTPUT_SAMPLES_PER_ITERATION = 32; 
  this->MAX_OUTPUT_SAMPLES_PER_TRACE     = 4096;

  this->shaderProgramCopy = new qfeGLShaderProgram();
  this->shaderProgramCopy->qfeAddShaderFromFile("/shaders/ltr/ltr-copy-vert.glsl", QFE_VERTEX_SHADER);  
  this->shaderProgramCopy->qfeSetTransformFeedbackVaryings(1, feedbackVarsCopy, GL_SEPARATE_ATTRIBS);
  this->shaderProgramCopy->qfeLink();

  this->shaderProgramGenerate = new qfeGLShaderProgram();
  this->shaderProgramGenerate->qfeAddShaderFromFile("/shaders/library/transform.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramGenerate->qfeAddShaderFromFile("/shaders/library/fetch.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramGenerate->qfeAddShaderFromFile("/shaders/library/integrate.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramGenerate->qfeAddShaderFromFile("/shaders/ltr/ltr-generate-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramGenerate->qfeAddShaderFromFile("/shaders/ltr/ltr-generate-geom.glsl", QFE_GEOMETRY_SHADER);  
  this->shaderProgramGenerate->qfeSetTransformFeedbackVaryings(2, feedbackVarsGenerate, GL_SEPARATE_ATTRIBS);
  this->shaderProgramGenerate->qfeLink();

  this->shaderProgramRenderLine = new qfeGLShaderProgram();
  this->shaderProgramRenderLine->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderLine->qfeAddShaderFromFile("/shaders/ltr/ltr-render-vert.glsl", QFE_VERTEX_SHADER);  
  this->shaderProgramRenderLine->qfeAddShaderFromFile("/shaders/ltr/ltr-render-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderLine->qfeAddShaderFromFile("/shaders/ltr/ltr-render-frag.glsl", QFE_FRAGMENT_SHADER);  
  this->shaderProgramRenderLine->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderProgramRenderLine->qfeLink();

  this->shaderProgramRenderImposter = new qfeGLShaderProgram();  
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/ltr/ltr-render-vert.glsl", QFE_VERTEX_SHADER);  
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/ltr/ltr-render-imposter-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/ltr/ltr-render-imposter-frag.glsl", QFE_FRAGMENT_SHADER);  
  this->shaderProgramRenderImposter->qfeLink();
  
  this->firstSeeds            = true;  
  this->firstGenerate         = true;
  this->firstRender           = true;

  this->volumeSeries.clear();

  this->seedPositions.clear();
  this->seedsCount            = 0;

  this->phaseCurrent          = 0.0;  
  this->phaseCurrentWrap      = 0.0;
  this->phaseGenerate         = 0.0;
  this->phaseCycle            = 0;
  this->phaseCycleCount       = 0;
  this->phaseDuration         = 0.0;
  this->phaseStart            = 0;
  this->phasesPerIteration    = 2;

  this->traceStyle            = qfeFlowLinesTuboid;
  this->traceIterations       = 10;
  this->tracePhasesStatic     = 10;
  this->tracePhasesDynamic    = 2;
  this->traceSamples          = 10;
  this->traceSteps            = 50;
  this->traceStepsPhase       = 25;
  this->traceStepSize         = 0.1;
  this->traceStepDuration     = 10.0;
  this->traceDirection        = 1;
  this->traceScheme           = qfeFlowLinesRK4;
  this->traceSpeed            = 100;
  this->traceVertexId         = 0;
  this->traceMaxAngle         = 180;
  this->traceSeedingBehavior  = (int)qfeFlowLinesPathStatic;

  this->lineShading            = qfeFlowLinesCel;
  this->lineWidth              = 1.0;
  this->lineContour            = true;
  this->lineWidth              = true;

  this->colorScale             = 100;
  this->supersampling          = 1;

  this->locationPosition       = 0;
  this->locationAttribute      = 0;

  this->light                  = NULL;
  this->colormap               = 0;  
};

//----------------------------------------------------------------------------
qfeFlowLineTrace::qfeFlowLineTrace(const qfeFlowLineTrace &fv)
{
  this->MAX_OUTPUT_VERTICES              = fv.MAX_OUTPUT_VERTICES;
  this->MAX_OUTPUT_SAMPLES_PER_ITERATION = fv.MAX_OUTPUT_SAMPLES_PER_ITERATION;
  this->MAX_OUTPUT_SAMPLES_PER_TRACE     = fv.MAX_OUTPUT_SAMPLES_PER_TRACE;

  this->firstSeeds                       = fv.firstSeeds;  
  this->firstGenerate                    = fv.firstGenerate;
  this->firstRender                      = fv.firstRender;
  
  this->shaderProgramCopy                = fv.shaderProgramCopy;
  this->shaderProgramGenerate            = fv.shaderProgramGenerate;
  this->shaderProgramRenderLine          = fv.shaderProgramRenderLine;
  this->shaderProgramRenderImposter      = fv.shaderProgramRenderImposter;

  this->volumeSeries                     = fv.volumeSeries;
  this->light                            = fv.light;
  this->colormap                         = fv.colormap;

  this->V2P                              = fv.V2P;
  this->P2V                              = fv.P2V;
  this->volumeSize[0]                    = fv.volumeSize[0];
  this->volumeSize[1]                    = fv.volumeSize[1];
  this->volumeSize[2]                    = fv.volumeSize[2];

  this->phaseCurrent                     = fv.phaseCurrent;
  this->phaseCurrentWrap                 = fv.phaseCurrentWrap;
  this->phaseGenerate                    = fv.phaseGenerate;
  this->phaseCycle                       = fv.phaseCycle;
  this->phaseCycleCount                  = fv.phaseCycleCount;
  this->phaseDuration                    = fv.phaseDuration;
  this->phaseStart                       = fv.phaseStart;
  this->phasesPerIteration               = fv.phasesPerIteration;

  this->traceStyle                       = fv.traceStyle;
  this->traceIterations                  = fv.traceIterations;
  this->tracePhasesStatic                = fv.tracePhasesStatic;
  this->tracePhasesDynamic               = fv.tracePhasesDynamic;
  this->traceSamples                     = fv.traceSamples;
  this->traceSteps                       = fv.traceSteps;
  this->traceStepsPhase                  = fv.traceStepsPhase;
  this->traceStepSize                    = fv.traceStepSize;
  this->traceStepDuration                = fv.traceStepDuration;
  this->traceScheme                      = fv.traceScheme;
  this->traceSpeed                       = fv.traceSpeed;
  this->traceDirection                   = fv.traceDirection;
  this->traceVertexId                    = fv.traceVertexId;
  this->traceMaxAngle                    = fv.traceMaxAngle;
  this->traceSeedingBehavior             = fv.traceSeedingBehavior;

  this->lineShading                      = fv.lineShading;
  this->lineWidth                        = fv.lineWidth;
  this->lineContour                      = fv.lineContour;
  this->lineWidth                        = fv.lineWidth;

  this->colorScale                       = fv.colorScale;
  this->supersampling                    = fv.supersampling;

  this->locationPosition                 = fv.locationPosition;
  this->locationAttribute                = fv.locationAttribute;

  this->seedPositionsVBO                 = fv.seedPositionsVBO;  
  this->linePositionsStatVBO             = fv.linePositionsStatVBO;
  this->lineAttributesStatVBO            = fv.lineAttributesStatVBO;
  this->linePositionsDynVBO              = fv.linePositionsDynVBO;
  this->lineAttributesDynVBO             = fv.lineAttributesDynVBO;
  this->seedPositions                    = fv.seedPositions;
  this->seedsCount                       = fv.seedsCount;
};

//----------------------------------------------------------------------------
qfeFlowLineTrace::~qfeFlowLineTrace()
{  
  delete this->shaderProgramCopy;
  delete this->shaderProgramGenerate;
  delete this->shaderProgramRenderLine;
  delete this->shaderProgramRenderImposter;

  this->qfeClearSeedBuffer();
  this->qfeClearLineBufferStatic();
  this->qfeClearLineBufferDynamic();
  this->seedPositions.clear();
};

//----------------------------------------------------------------------------
// seeds - patient coordinates + time
// phase - current time in phases
qfeReturnStatus qfeFlowLineTrace::qfeAddSeedPositions(qfeSeeds seeds)
{
  if(int(seeds.size()) <= 0) return qfeError;

  for(int i=0; i<(int)seeds.size(); i++)
  {
    this->seedPositions.push_back(seeds[i].x);
    this->seedPositions.push_back(seeds[i].y);
    this->seedPositions.push_back(seeds[i].z);
    this->seedPositions.push_back(seeds[i].w);               
  }

  this->seedsCount += seeds.size();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// seeds - patient coordinates
// phase - current time in phases
qfeReturnStatus qfeFlowLineTrace::qfeAddSeedPositions(qfeSeeds seeds, float phase)
{
  if(int(seeds.size()) <= 0) return qfeError;

  for(int i=0; i<(int)seeds.size(); i++)
  {
    this->seedPositions.push_back(seeds[i].x);
    this->seedPositions.push_back(seeds[i].y);
    this->seedPositions.push_back(seeds[i].z);
    this->seedPositions.push_back(phase);               
  }

  this->seedsCount += seeds.size();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// seeds - patient coordinates
// phase - current time in phases
qfeReturnStatus qfeFlowLineTrace::qfeUpdateSeedPositions(qfeSeeds seeds, float phase)
{
  if(int(seeds.size()) <= 0) return qfeError;

  this->qfeClearSeedPositions();

  for(int i=0; i<(int)seeds.size(); i++)
  {
    this->seedPositions.push_back(seeds[i].x);
    this->seedPositions.push_back(seeds[i].y);
    this->seedPositions.push_back(seeds[i].z);
    this->seedPositions.push_back(this->phaseStart);               
  }

  this->seedsCount += seeds.size();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeClearSeedPositions()
{
  // Erase all information
  this->qfeClearSeedBuffer();
  this->seedPositions.clear();

  this->seedsCount = 0;
  this->firstGenerate = true;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// phase         - current phase in phases
// traceDuration - total time to trace in ms
// This function will trace one cardiac cycle, starting the trace at phase
qfeReturnStatus qfeFlowLineTrace::qfeGeneratePathlines(qfeVolumeSeries series, qfeFloat phase, qfeFlowSeedingBehavior type)
{
  GLuint  count = 0;
  //GLuint  query;
  GLuint  activePosVBO, activeAttrVBO;
  int     dataSize, offset;  
  int     locPosition;

  if(((int)series.size() <= 0)) return qfeError;

  // First time of generation, prepare some global variables
  if(this->firstGenerate)
  {
    this->volumeSeries  = series;

    this->volumeSeries.front()->qfeGetVolumeSize(this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);

    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, this->volumeSeries.front());
    qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, this->volumeSeries.front());

    this->qfeSetStaticUniformLocations();

    this->phaseStart = this->phaseCurrent;

    this->firstGenerate = false;
  }
    
  // Update line trace
  this->traceSeedingBehavior = (int) type;
  switch(type)
  {
  case qfeFlowLinesPathDynamic : this->phaseStart = this->phaseCurrent;
                                 this->qfeInitTraceDynamic();                                 
                                 break;
  case qfeFlowLinesPathStatic  :
  default                      : this->qfeInitTraceStatic();
  }

  // Set the initial particles in the buffer
  this->qfeInitSeedBuffer();

  // Create the output buffer & check if we have the necessary buffers
  if(!glIsBuffer(this->seedPositionsVBO))       return qfeError;  
  switch(type)
  {
  case qfeFlowLinesPathDynamic : this->qfeInitLineBufferDynamic();
                                 if(!glIsBuffer(this->linePositionsDynVBO))   return qfeError;
                                 if(!glIsBuffer(this->lineAttributesDynVBO))  return qfeError;
                                 activePosVBO  = this->linePositionsDynVBO;
                                 activeAttrVBO = this->lineAttributesDynVBO;
                                 break;
  case qfeFlowLinesPathStatic  :
  default                      : this->qfeInitLineBufferStatic();
                                 if(!glIsBuffer(this->linePositionsStatVBO))   return qfeError;
                                 if(!glIsBuffer(this->lineAttributesStatVBO))  return qfeError;
                                 activePosVBO  = this->linePositionsStatVBO;
                                 activeAttrVBO = this->lineAttributesStatVBO;
  }  

  // No need for rasterization
  glEnable(GL_RASTERIZER_DISCARD);

  // Iterate to fill the entire buffer
  for(int i=0; i<this->traceIterations;i++)  
  {
    this->phaseGenerate =  this->phaseStart + phasesPerIteration*i;    

    // Enable the velocity volumes
    this->qfeBindTexturesSliding(this->volumeSeries, (int)floor(this->phaseGenerate));

    // Initialize the current vertex ID
    this->traceVertexId = i * this->traceSamples;
    
    // -- 1 Render seeds to get a subsection of the line --
   
    // Get input location
    this->shaderProgramGenerate->qfeGetAttribLocation("qfe_SeedPoints", locPosition);  

    GLsizei vertexSize =   4*sizeof(GLfloat);
    GLsizei vertexCount =  this->traceSamples;

    // Bind the output buffer at the right offset
    dataSize        = this->seedsCount * vertexSize * vertexCount;    
    offset          = i * dataSize;   
    glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, activePosVBO, offset, dataSize);    
    glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 1, activeAttrVBO, offset, dataSize);    

    // Set the dynamic uniform inputs
    this->qfeSetDynamicUniformLocations();

    // Enable the shader program
    this->shaderProgramGenerate->qfeEnable();

    // Generate the lines using a transform feedback
    //glGenQueries(1, &query);
    //glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
    glBeginTransformFeedback(GL_POINTS);

    this->qfeDrawSeeds(this->seedPositionsVBO, locPosition, (int)this->seedsCount); 

    glEndTransformFeedback();
    //glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

    // Check if we have output in the buffer
    //glGetQueryObjectuiv(query, GL_QUERY_RESULT, &count);
    
    // Disable the shader program
    this->shaderProgramGenerate->qfeDisable();

    // Clean-up
    glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, 0);

    glFinish(); 

    // -- 2 Update seed positions --    

    // Get input location
    this->shaderProgramCopy->qfeGetAttribLocation("qfe_SeedPoints", locPosition);  

    // Bind the output buffer
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, this->seedPositionsVBO);

    // Enable the shader program
    this->shaderProgramCopy->qfeEnable();

    // Copy the intermediate positions
    glBeginTransformFeedback(GL_POINTS);

    this->qfeDrawEndPoints(activePosVBO, locPosition, i, (int)this->seedsCount); 
    
    glEndTransformFeedback();

    // Disable the shader program
    this->shaderProgramCopy->qfeDisable();

    // Clean-up
    glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, 0);

    // Unbind the volumes
    this->qfeUnbindTextures();
  }  

  // Clean up
  glDisable(GL_RASTERIZER_DISCARD);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeRenderPathlines(qfeColorMap *colormap, qfeFlowSeedingBehavior type)
{ 
  switch(type)
  {
  case qfeFlowLinesPathStatic  : this->qfeRenderPathlinesStatic(colormap);
                                 break;
  case qfeFlowLinesPathDynamic : this->qfeRenderPathlinesDynamic(colormap);
                                 break;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeRenderPathlinesStatic(qfeColorMap *colormap)
{
  // First time of rendering, prepare some global variables
  if(this->firstRender)
  {
    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }
  
  if(!glIsBuffer(this->linePositionsStatVBO))  return qfeError;  

  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->colormap); 

  // Compute the amount of points to render  
  GLsizei segmentCount = this->seedsCount * this->traceIterations;
  GLsizei segmentSize  = this->traceSamples;
  
  // Obtain the modelview information
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Set point parameters, in case of normal point rendering
  glLineWidth(this->supersampling * this->lineWidth * 2);
  glEnable(GL_LINE_SMOOTH);
  glPointSize(4.0);
  glEnable(GL_POINT_SMOOTH);
  glColor3f(0.0,1.0,0.0);

  // Bind textures
  this->qfeBindTexturesRender();

  // Allow blending for transparency
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocations();

  switch(this->traceStyle)
  {
  case qfeFlowLinesTuboid : 
    this->qfeRenderStyleTuboid(this->linePositionsStatVBO, this->lineAttributesStatVBO, segmentCount, segmentSize);
    break;
  case qfeFlowLinesLine   :   
    this->qfeRenderStyleLine(this->linePositionsStatVBO, this->lineAttributesStatVBO, segmentCount, segmentSize);  
    break;
  default :
    this->qfeRenderStyleLine(this->linePositionsStatVBO, this->lineAttributesStatVBO, segmentCount, segmentSize);  
    break;
  }

  glDisable(GL_BLEND);

  // Unbind textures
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeRenderPathlinesDynamic(qfeColorMap *colormap)
{
  // First time of rendering, prepare some global variables
  if(this->firstRender)
  {
    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->colormap); 

  // Dynamic trace generates lines for each phase
  this->qfeGeneratePathlines(this->volumeSeries, this->phaseCurrent, qfeFlowLinesPathDynamic);

  // Check if the buffer is available
  if(!glIsBuffer(this->linePositionsDynVBO))  return qfeError;  

  // Compute the amount of points to render  
  GLsizei segmentCount = this->seedsCount * this->traceIterations;
  GLsizei segmentSize  = this->traceSamples;
  
  // Obtain the modelview information
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Set point parameters, in case of normal point rendering
  glLineWidth(this->supersampling * this->lineWidth * 2);
  glEnable(GL_LINE_SMOOTH);
  glPointSize(4.0);
  glEnable(GL_POINT_SMOOTH);
  glColor3f(0.0,1.0,0.0);

  // Bind textures
  this->qfeBindTexturesRender();

  // Allow blending for transparency
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocations();

  switch(this->traceStyle)
  {
  case qfeFlowLinesTuboid : 
    this->qfeRenderStyleTuboid(this->linePositionsDynVBO, this->lineAttributesDynVBO, segmentCount, segmentSize);
    break;
  case qfeFlowLinesLine   :   
    this->qfeRenderStyleLine(this->linePositionsDynVBO, this->lineAttributesDynVBO, segmentCount, segmentSize);  
    break;
  default :
    this->qfeRenderStyleLine(this->linePositionsDynVBO, this->lineAttributesDynVBO, segmentCount, segmentSize);  
    break;
  }

  glDisable(GL_BLEND);

  // Unbind textures
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeRenderStyleLine(int currentBuffer, int attribBuffer, int segmentCount, int segmentSize)
{
  this->shaderProgramRenderLine->qfeGetAttribLocation("qfe_Position",  this->locationPosition);
  this->shaderProgramRenderLine->qfeGetAttribLocation("qfe_Attribute", this->locationAttribute);

  this->shaderProgramRenderLine->qfeEnable();  
  
  this->qfeDrawLines(currentBuffer, attribBuffer, segmentCount, segmentSize);  
  
  this->shaderProgramRenderLine->qfeDisable();  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeRenderStyleTuboid(int currentBuffer, int attribBuffer, int segmentCount, int segmentSize)
{
  this->shaderProgramRenderImposter->qfeGetAttribLocation("qfe_Position",  this->locationPosition);
  this->shaderProgramRenderImposter->qfeGetAttribLocation("qfe_Attribute", this->locationAttribute);

  this->shaderProgramRenderImposter->qfeEnable();  
  
  this->qfeDrawLines(currentBuffer, attribBuffer, segmentCount, segmentSize);  
  
  this->shaderProgramRenderImposter->qfeDisable();  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetStyle(qfeFlowLinesStyle style)
{
  this->traceStyle = style;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// step - amount of integration steps per phase
qfeReturnStatus qfeFlowLineTrace::qfeSetTraceSteps(int steps)
{
  this->traceStepsPhase    = steps;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// direction - 1 = forward, -1 = backward
qfeReturnStatus qfeFlowLineTrace::qfeSetTraceDirection(int dir)
{
  this->traceDirection = dir;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// scheme - 1st, 2nd or 4th order Runge Kutta integration
qfeReturnStatus qfeFlowLineTrace::qfeSetTraceScheme(qfeFlowLinesIntegrationScheme scheme)
{
  this->traceScheme = scheme;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// percentage - speed modulation in percentage
qfeReturnStatus qfeFlowLineTrace::qfeSetTraceSpeed(int percentage)
{
  if(percentage <= 0)
    this->traceSpeed = 1;
  else
    this->traceSpeed = percentage;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetTracePhasesStatic(int phases)
{
  if(this->firstGenerate == true)
  {
    this->tracePhasesStatic = phases; 
  }

  if((phases != this->tracePhasesStatic) && (this->seedsCount > 0) && ((int)this->volumeSeries.size() > 0))
  {
    this->tracePhasesStatic = phases; 
    this->qfeGeneratePathlines(this->volumeSeries, this->phaseStart);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetTracePhasesDynamic(int phases)
{
  this->tracePhasesDynamic = phases;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetTraceMaximumAngle(int degrees)
{
  if((degrees != this->traceMaxAngle) && (this->seedsCount > 0) && ((int)this->volumeSeries.size() > 0))
  {
    this->traceMaxAngle = degrees;    
    this->qfeGeneratePathlines(this->volumeSeries, this->phaseStart);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// phase - current phase
qfeReturnStatus qfeFlowLineTrace::qfeSetPhase(float phase)
{
  // determine the cycle wrap-around point
  if((int)this->volumeSeries.size() > 0)
  {
    // forward wrap-around
    if(((int)floor(phase) == 0.0) &&
       ((int)ceil(this->phaseCurrent) == (int)this->volumeSeries.size()))    
      this->phaseCycle++;    
    // backward wrap-around
    if(((int)floor(this->phaseCurrent) == 0.0) &&
       ((int)ceil(phase) == (int)this->volumeSeries.size()))    
      this->phaseCycle--;

    this->phaseCycle = max(0, phaseCycle);    

    // determine the current phase (keep counting until #cycles have passed)
    this->phaseCurrentWrap = (float)((this->phaseCycle*this->volumeSeries.size())+phase);
    
    if(this->phaseCurrentWrap > (this->phaseStart + this->tracePhasesStatic))
    {
      this->phaseCurrentWrap = this->phaseStart + this->tracePhasesStatic;  
    }
    
    // reset the cycle counter
    if(this->phaseCycle > this->phaseCycleCount)
    {
      this->phaseCurrentWrap = this->phaseStart;
      this->phaseCycle       = 0;
    }
  }

  this->phaseCurrent = phase;

  /*
  cout << "phase current:     " << this->phaseCurrentWrap  << endl;
  cout << "phase start:       " << this->phaseStart  << endl;
  cout << "phase cycle:       " << this->phaseCycle  << endl;
  cout << "phase cycle count: " << this->phaseCycleCount  << endl << endl;
  */
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// duration - total time for one phase in ms
qfeReturnStatus qfeFlowLineTrace::qfeSetPhaseDuration(qfeFloat duration)
{
  this->phaseDuration = duration;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetLineShading(qfeFlowLinesShading shading)
{
  this->lineShading = shading; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetLineWidth(float width)
{
  this->lineWidth = width;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetLineContour(bool contour)
{
  this->lineContour = contour;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetLineHalo(bool halo)
{
  this->lineHalo = halo;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetLightSource(qfeLightSource *light)
{
  this->light = light;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetSuperSampling(int factor)
{
  this->supersampling = factor;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetColorScale(int colorscale)
{
  this->colorScale = colorscale;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// If you assign a data pointer to glVertexAttribPointer or glVertexPointer,
// do NOT bind a VBO. If you bind a VBO, pointer assignment should be 0.
qfeReturnStatus qfeFlowLineTrace::qfeDrawSeeds(GLuint seeds, GLint attribLocation, int count)
{
  if(!glIsBuffer(seeds) || (attribLocation == -1)) return qfeError;

  glBindBuffer(GL_ARRAY_BUFFER, seeds);

  glEnableVertexAttribArray(attribLocation);
  glVertexAttribPointer(attribLocation,  4, GL_FLOAT, GL_FALSE, 0, 0);
  
  glDrawArrays(GL_POINTS, 0, count);

  glDisableVertexAttribArray(attribLocation); 

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeDrawEndPoints(GLuint lines, GLint attribLocation, int currentPhase, int count)
{
  if(!glIsBuffer(lines)) return qfeError;

  glBindBuffer(GL_ARRAY_BUFFER, lines);

  GLsizei sizeVertex       = 4*sizeof(GLfloat);
  GLsizei countVertex      = this->traceSamples;
  GLsizei lineSegmentSize  = countVertex * sizeVertex;
  GLsizei lineSegmentCount = this->seedsCount*currentPhase;

  // We get the two but last point of each last segment (for adjacency reasons)
  GLsizei stride = lineSegmentSize;
  GLvoid *offset = BUFFER_OFFSET((lineSegmentCount+1)*lineSegmentSize - 3*sizeVertex);

  glEnableVertexAttribArray(attribLocation);
  glVertexAttribPointer(attribLocation,  4, GL_FLOAT, GL_FALSE, stride, offset);    
  
  glDrawArrays(GL_POINTS, 0, count);

  glDisableVertexAttribArray(attribLocation); 

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeDrawLines(GLuint lines, GLuint attribs, int segmentCount, int segmentSize)
{
  if(!glIsBuffer(lines)) return qfeError;
  
  glBindBuffer(GL_ARRAY_BUFFER, lines);
  glEnableVertexAttribArray(this->locationPosition);
  glVertexAttribPointer(this->locationPosition,  4, GL_FLOAT, GL_FALSE, 0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, attribs);
  glEnableVertexAttribArray(this->locationAttribute);
  glVertexAttribPointer(this->locationAttribute,  4, GL_FLOAT, GL_FALSE, 0, 0);

  // Note that here we still draw each line-strip (one per seed per segment)
  // sequentially. Otherwise, OpenGL cannot distinguish when the primitive ends.
  for(int i=0; i<segmentCount; i++)
  {
    glDrawArrays(GL_LINE_STRIP_ADJACENCY_ARB, i*segmentSize, segmentSize);
  }

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDisableVertexAttribArray(this->locationPosition);
  glDisableVertexAttribArray(this->locationAttribute);  

  return qfeSuccess;
}
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeInitTraceStatic()
{
  float   traceDuration;  
  float   samplesPerPhase, samplesPerPhase1, samplesPerPhase2;

  traceDuration            = this->tracePhasesStatic*this->phaseDuration;    

  samplesPerPhase1         = this->MAX_OUTPUT_SAMPLES_PER_TRACE     / (float)this->tracePhasesStatic;
  samplesPerPhase2         = this->MAX_OUTPUT_SAMPLES_PER_ITERATION / (float)phasesPerIteration;
  samplesPerPhase          = min(samplesPerPhase1, samplesPerPhase2);

  this->traceSamples       = phasesPerIteration * samplesPerPhase;
  //this->traceSteps         = (this->traceStepsPhase * phasesPerIteration);
  //this->traceSteps         = floor(this->traceSteps / (float)(this->traceSamples-3)) * (this->traceSamples-3);
  this->traceStepDuration  = (this->phaseDuration * phasesPerIteration) / this->traceSteps;
  this->traceStepSize      = phasesPerIteration / (float)this->traceSteps;

  this->traceIterations    = (int)ceil(this->tracePhasesStatic / (float)phasesPerIteration);
  this->phaseCycleCount    = (int)((this->phaseStart + this->tracePhasesStatic - 1) / (float)this->volumeSeries.size());  

  /*
  cout << "phases     : "<< this->tracePhasesStatic << endl;
  cout << "samples    : "<< this->traceSamples << endl;
  cout << "steps      : "<< this->traceSteps << endl;
  cout << "iterations : "<< this->traceIterations << endl;
  cout << "cycle count: "<< this->phaseCycleCount << endl;
  */

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeInitTraceDynamic()
{
  float   traceDuration;  
  float   samplesPerPhase, samplesPerPhase1, samplesPerPhase2;

  traceDuration            = this->tracePhasesDynamic*this->phaseDuration;    

  samplesPerPhase1         = this->MAX_OUTPUT_SAMPLES_PER_TRACE     / (float)this->tracePhasesDynamic;
  samplesPerPhase2         = this->MAX_OUTPUT_SAMPLES_PER_ITERATION / (float)this->phasesPerIteration;
  samplesPerPhase          = min(samplesPerPhase1, samplesPerPhase2);

  this->traceSamples       = this->phasesPerIteration * samplesPerPhase;
  this->traceSteps         = (this->traceStepsPhase * this->phasesPerIteration);
  this->traceStepDuration  = (this->phaseDuration * phasesPerIteration) / this->traceSteps;
  this->traceStepSize      = this->phasesPerIteration / (float)this->traceSteps;

  this->traceIterations    = (int)ceil(this->tracePhasesDynamic / (float)this->phasesPerIteration);
  this->phaseCycleCount    = (int)((this->phaseStart + this->tracePhasesDynamic - 1) / (float)this->volumeSeries.size());  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeInitSeedBuffer()
{
  int             dataSize;
  vector< float > seedsAdjacency;
  float           timeShift;

  if(this->seedPositions.size() <= 0) return qfeError;

  this->qfeClearSeedBuffer();

  timeShift = (this->phasesPerIteration)/(float)(this->traceSamples-2);
  dataSize  = (int)this->seedsCount * 4 * sizeof(GLfloat);

  seedsAdjacency.clear();
  // Adjust the time components (every fourth element)
  for(int i=0; i<(int)this->seedsCount*4; i++)
  {
    if((i+1)%(4) == 0)
      seedsAdjacency.push_back(this->seedPositions[i]-timeShift);
    else
      seedsAdjacency.push_back(this->seedPositions[i]);
  }

  glGenBuffers(1, &this->seedPositionsVBO);    

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->seedPositionsVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &seedsAdjacency[0], GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  
  //cout << "seed position 0: " << this->seedPositionsVBO[0] << endl;
  //cout << "seed position 1: " << this->seedPositionsVBO[1] << endl;  
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeClearSeedBuffer()
{
  if(glIsBuffer(this->seedPositionsVBO))
    glDeleteBuffers(1, &this->seedPositionsVBO);    

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeInitLineBufferStatic()
{
  int sizeVertex;
  int countVertex;
  int dataSize;

  this->qfeClearLineBufferStatic();

  sizeVertex  = 4*sizeof(GLfloat);  
  countVertex = this->traceSamples;

  dataSize = this->seedsCount * this->traceIterations  * countVertex * sizeVertex;

  glGenBuffers(1, &this->linePositionsStatVBO);  
  glGenBuffers(1, &this->lineAttributesStatVBO);  

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->linePositionsStatVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, 0, GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindBuffer(GL_ARRAY_BUFFER, this->lineAttributesStatVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, 0, GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  //cout << "line position  : "  << this->linePositionsStatVBO << endl;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeClearLineBufferStatic()
{
  if(glIsBuffer(this->linePositionsStatVBO))
    glDeleteBuffers(1, &this->linePositionsStatVBO);    

  if(glIsBuffer(this->lineAttributesStatVBO))
    glDeleteBuffers(1, &this->lineAttributesStatVBO);  
    
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeInitLineBufferDynamic()
{
  int sizeVertex;
  int countVertex;
  int dataSize;

  this->qfeClearLineBufferDynamic();

  sizeVertex  = 4*sizeof(GLfloat);  
  countVertex = this->traceSamples;

  dataSize = this->seedsCount * this->traceIterations  * countVertex * sizeVertex;

  glGenBuffers(1, &this->linePositionsDynVBO);  
  glGenBuffers(1, &this->lineAttributesDynVBO);  

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->linePositionsDynVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, 0, GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindBuffer(GL_ARRAY_BUFFER, this->lineAttributesDynVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, 0, GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeClearLineBufferDynamic()
{
  if(glIsBuffer(this->linePositionsDynVBO))
    glDeleteBuffers(1, &this->linePositionsDynVBO);    

  if(glIsBuffer(this->lineAttributesDynVBO))
    glDeleteBuffers(1, &this->lineAttributesDynVBO);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeBindTexturesSliding(qfeVolumeSeries series, int phase)
{
  GLuint        tids[5];
  int           phasesCount;

  phasesCount = (int)series.size();

  if(phasesCount > 0)
  {
    // We take a range [phase-2, phase+2] datasets, surrounding the current phase
    int index;
    for(int i=-2; i<=2; i++)
    {
      if(phasesCount == 1)
        index = 0;
      else
      {
        index = phase+i;
        qfeMath::qfeMod(index, phasesCount, index);
      }

      //cout << index << " - ";
      series[index]->qfeGetVolumeTextureId(tids[i+2]);
    }
    //cout << endl;

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_3D, tids[0]);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_3D, tids[1]);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_3D, tids[2]);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_3D, tids[3]);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_3D, tids[4]);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeBindTexturesRender()
{  
  glActiveTexture(GL_TEXTURE7);
  glBindTexture(GL_TEXTURE_1D, this->colormap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetStaticUniformLocations()
{
  //int series[5] = {1,2,3,4,5};
  this->shaderProgramGenerate->qfeSetUniform1i("volumeData0", 1);
  this->shaderProgramGenerate->qfeSetUniform1i("volumeData1", 2);
  this->shaderProgramGenerate->qfeSetUniform1i("volumeData2", 3);
  this->shaderProgramGenerate->qfeSetUniform1i("volumeData3", 4);
  this->shaderProgramGenerate->qfeSetUniform1i("volumeData4", 5);  
  this->shaderProgramGenerate->qfeSetUniform3i("volumeDimensions", this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);
  this->shaderProgramGenerate->qfeSetUniformMatrix4f("voxelPatientMatrix", 1, this->V2P);
  this->shaderProgramGenerate->qfeSetUniformMatrix4f("patientVoxelMatrix", 1, this->P2V);

  this->shaderProgramRenderLine->qfeSetUniform1i("lineTransferFunction", 7);
  this->shaderProgramRenderImposter->qfeSetUniform1i("lineTransferFunction", 7); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTrace::qfeSetDynamicUniformLocations()
{
  qfePoint  lightSpec;
  qfeVector lightDir;
  qfeFloat  vl, vu;
  GLfloat   patientVoxel[16];
  float     angle;
  int       vertexCount;
  int       totalCount;

  // Obtain actual lighting parameters
  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 10.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w); 
    this->light->qfeGetLightSourceDirection(lightDir);
  }

  // Obtain the dataset venc
  if((int)this->volumeSeries.size() > 0)     
  {
    this->volumeSeries.front()->qfeGetVolumeValueDomain(vl, vu);
  }
  else
  {
    vu = -200.0;
    vl = 200.0;
  }

  // Obtain the current P2V matrix
  qfeMatrix4f::qfeGetMatrixElements(this->P2V, patientVoxel);

  // Compute the current phase, including wrap-around cycles
  angle       = cos((float)this->traceMaxAngle * (float)(PI/180.0));
  vertexCount = (int)this->traceSamples;
  totalCount  = (int)(this->traceIterations * (this->traceSamples-2));

  this->shaderProgramGenerate->qfeSetUniform1f("phaseStart",                    this->phaseStart);
  this->shaderProgramGenerate->qfeSetUniform1f("phaseCurrent",                  this->phaseGenerate);  
  this->shaderProgramGenerate->qfeSetUniform1i("traceSamples",                  this->traceSamples);
  this->shaderProgramGenerate->qfeSetUniform1i("traceSteps",                    this->traceSteps);
  this->shaderProgramGenerate->qfeSetUniform1f("traceStepSize",                 this->traceStepSize);
  this->shaderProgramGenerate->qfeSetUniform1f("traceStepDuration",             this->traceStepDuration);
  this->shaderProgramGenerate->qfeSetUniform1i("traceScheme",                   this->traceScheme);
  this->shaderProgramGenerate->qfeSetUniform1i("traceSpeed",                    this->traceSpeed);
  this->shaderProgramGenerate->qfeSetUniform1i("traceDirection",                this->traceDirection);
  this->shaderProgramGenerate->qfeSetUniform1i("traceVertexId",                 this->traceVertexId);
  this->shaderProgramGenerate->qfeSetUniform1f("maxAngle",                      angle);

  this->shaderProgramRenderLine->qfeSetUniform1i("colorScale",                  this->colorScale);  
  this->shaderProgramRenderLine->qfeSetUniform1f("phaseCurrent",                this->phaseCurrentWrap);  
  this->shaderProgramRenderLine->qfeSetUniform1f("phaseStart",                  this->phaseStart);  
  this->shaderProgramRenderLine->qfeSetUniform1f("phaseEnd",                    this->phaseStart + this->tracePhasesStatic);   
  this->shaderProgramRenderLine->qfeSetUniform1i("seedingType",                 this->traceSeedingBehavior);
  this->shaderProgramRenderLine->qfeSetUniform1i("vertexCount",                 vertexCount);   
  this->shaderProgramRenderLine->qfeSetUniform4f("light",                       lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderLine->qfeSetUniform4f("lightDirection",              lightDir.x, lightDir.y, lightDir.z, lightDir.w);  
  this->shaderProgramRenderLine->qfeSetUniform2f("dataRange",                   vu, vl);      
  this->shaderProgramRenderLine->qfeSetUniform3f("volumeDimensions",            (GLfloat)this->volumeSize[0], (GLfloat)this->volumeSize[1], (GLfloat)this->volumeSize[2]);  
  this->shaderProgramRenderLine->qfeSetUniformMatrix4f("patientVoxelMatrix",        1, patientVoxel);
  this->shaderProgramRenderLine->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramRenderLine->qfeSetUniformMatrix4f("matrixModelView",           1, this->matrixModelView);
  this->shaderProgramRenderLine->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);

  this->shaderProgramRenderImposter->qfeSetUniform1i("colorScale",              this->colorScale);  
  this->shaderProgramRenderImposter->qfeSetUniform1i("lineShading",             (int)this->lineShading); 
  this->shaderProgramRenderImposter->qfeSetUniform1f("lineWidth",               this->lineWidth);    
  this->shaderProgramRenderImposter->qfeSetUniform1i("lineContour",             (int)this->lineContour);  
  this->shaderProgramRenderImposter->qfeSetUniform1i("lineHalo",                (int)this->lineHalo);  
  this->shaderProgramRenderImposter->qfeSetUniform1f("phaseCurrent",            this->phaseCurrentWrap);  
  this->shaderProgramRenderImposter->qfeSetUniform1f("phaseStart",              this->phaseStart);  
  this->shaderProgramRenderImposter->qfeSetUniform1f("phaseEnd",                this->phaseStart + this->tracePhasesStatic);
  this->shaderProgramRenderImposter->qfeSetUniform1i("seedingType",             this->traceSeedingBehavior);
  this->shaderProgramRenderImposter->qfeSetUniform1i("vertexCount",             vertexCount);   
  this->shaderProgramRenderImposter->qfeSetUniform1i("totalCount",              totalCount);   
  this->shaderProgramRenderImposter->qfeSetUniform4f("light",                   lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderImposter->qfeSetUniform4f("lightDirection",          lightDir.x, lightDir.y, lightDir.z, lightDir.w);  
  this->shaderProgramRenderImposter->qfeSetUniform2f("dataRange",               vu, vl);      
  this->shaderProgramRenderImposter->qfeSetUniform3f("volumeDimensions",        (GLfloat)this->volumeSize[0], (GLfloat)this->volumeSize[1], (GLfloat)this->volumeSize[2]);  
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("patientVoxelMatrix" ,       1, patientVoxel);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixModelView",           1, this->matrixModelView);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixProjection",          1, this->matrixProjection);
  
  return qfeSuccess;
}
