#pragma once

#include "qflowexplorer.h"

#include <vector>

#include "qfeAlgorithm.h"

typedef vector< qfePoint >         qfeSeeds;              // Per ring
typedef vector< qfeSeeds >         qfeSeedsGroup;         // Per volume

/**
* \file   qfeAlgorithmFlow.h
* \author Roy van Pelt
* \class  qfeAlgorithmFlow
* \brief  Abstract class for a driver algorithm
* \note   Confidential
*
* Abstract class for driver algorithm.
* A driver processor outsources a part of the rendering work in the driver.
* The rendering work may be GPU based using GLSL
*
*/

class qfeAlgorithmFlow : public qfeAlgorithm
{
public:
  qfeAlgorithmFlow();
  qfeAlgorithmFlow(const qfeAlgorithmFlow &af);
  ~qfeAlgorithmFlow();

protected:

};
