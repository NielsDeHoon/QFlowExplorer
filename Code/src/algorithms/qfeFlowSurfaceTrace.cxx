#include "qfeFlowSurfaceTrace.h"

const char *qfeFlowSurfaceTrace::feedbackVars1[] = {"qfe_SurfacePoints", "qfe_SurfaceAttribs"};
const char *qfeFlowSurfaceTrace::feedbackVars2[] = {"qfe_SurfaceNormals"};

//----------------------------------------------------------------------------
qfeFlowSurfaceTrace::qfeFlowSurfaceTrace()
{
  this->firstGenerate       = true;
  this->firstRender         = true;

  this->volumeSeries.clear();

  this->traceSamples        = 32; // fixed sufficient number  

  this->shaderProgramGeneratePathlines = new qfeGLShaderProgram();
  this->shaderProgramGeneratePathlines->qfeAddShaderFromFile("/shaders/library/transform.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramGeneratePathlines->qfeAddShaderFromFile("/shaders/library/fetch.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramGeneratePathlines->qfeAddShaderFromFile("/shaders/library/integrate.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramGeneratePathlines->qfeAddShaderFromFile("/shaders/str/str-generate-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramGeneratePathlines->qfeAddShaderFromFile("/shaders/str/str-generate-geom.glsl", QFE_GEOMETRY_SHADER);        
  this->shaderProgramGeneratePathlines->qfeSetTransformFeedbackVaryings(2, feedbackVars1, GL_SEPARATE_ATTRIBS);
  this->shaderProgramGeneratePathlines->qfeLink();

  this->shaderProgramGenerateNormals = new qfeGLShaderProgram();
  this->shaderProgramGenerateNormals->qfeAddShaderFromFile("/shaders/str/str-normals-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramGenerateNormals->qfeAddShaderFromFile("/shaders/str/str-normals-geom.glsl", QFE_GEOMETRY_SHADER);  
  this->shaderProgramGenerateNormals->qfeSetTransformFeedbackVaryings(1, feedbackVars2, GL_SEPARATE_ATTRIBS);
  this->shaderProgramGenerateNormals->qfeLink();

  this->shaderProgramRenderTube = new qfeGLShaderProgram();
  this->shaderProgramRenderTube->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderTube->qfeAddShaderFromFile("/shaders/str/str-render-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramRenderTube->qfeAddShaderFromFile("/shaders/str/str-render-tube-geom.glsl", QFE_GEOMETRY_SHADER);    
  this->shaderProgramRenderTube->qfeAddShaderFromFile("/shaders/str/str-render-tube-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderTube->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderProgramRenderTube->qfeLink();  

  this->shaderProgramRenderStrip = new qfeGLShaderProgram();
  this->shaderProgramRenderStrip->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderStrip->qfeAddShaderFromFile("/shaders/str/str-render-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramRenderStrip->qfeAddShaderFromFile("/shaders/str/str-render-strip-geom.glsl", QFE_GEOMETRY_SHADER);    
  this->shaderProgramRenderStrip->qfeAddShaderFromFile("/shaders/str/str-render-strip-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderStrip->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderProgramRenderStrip->qfeLink();  

  this->light             = NULL;
  this->colormap          = 0;
  this->texCoordT         = 0.0;

  this->seedPositions.clear();  
  this->seedsCount        = 0;  
  this->itemsCount        = 0;
  
  this->tracePhases       = 1.0;
  this->traceSteps        = 10;
  this->traceStepsPhase   = 50;
  this->traceStepSize     = 0.1;
  this->traceStepDuration = 10.0;
  this->traceDirection    = 1;
  this->traceScheme       = qfeFlowSurfaceRK4;
  this->traceSpeed        = 100; 

  this->surfaceStyle      = qfeFlowSurfaceTube;
  this->surfaceShading    = qfeFlowSurfaceCel;
};

//----------------------------------------------------------------------------
qfeFlowSurfaceTrace::qfeFlowSurfaceTrace(const qfeFlowSurfaceTrace &fv)
{
  this->firstGenerate                   = fv.firstGenerate;
  this->firstRender                     = fv.firstRender;

  this->shaderProgramGeneratePathlines  = fv.shaderProgramGeneratePathlines;
  this->shaderProgramGenerateNormals    = fv.shaderProgramGenerateNormals;
  this->shaderProgramRenderTube         = fv.shaderProgramRenderTube;
  this->shaderProgramRenderStrip        = fv.shaderProgramRenderStrip;

  this->volumeSeries                    = fv.volumeSeries;
  this->light                           = fv.light;
  this->colormap                        = fv.colormap;
  this->texCoordT                       = fv.texCoordT;

  this->volumeSize[0]                   = fv.volumeSize[0];
  this->volumeSize[1]                   = fv.volumeSize[1];
  this->volumeSize[2]                   = fv.volumeSize[2];

  this->surfaceGeometryVBO              = fv.surfaceGeometryVBO;
  this->surfaceNormalsVBO               = fv.surfaceNormalsVBO;
  this->surfaceAttribsVBO               = fv.surfaceAttribsVBO;
  this->seedPositionsVBO                = fv.seedPositionsVBO;  
  this->seedPositions                   = fv.seedPositions;  
  this->seedsCount                      = fv.seedsCount;
  this->seeds                           = fv.seeds;
  this->itemsCount                      = fv.itemsCount;

  this->phaseCurrent                    = fv.phaseCurrent;  
  this->phaseDuration                   = fv.phaseDuration;  
  
  this->tracePhases                     = fv.tracePhases;
  this->traceSamples                    = fv.traceSamples;
  this->traceSteps                      = fv.traceSteps;
  this->traceStepsPhase                 = fv.traceStepsPhase;
  this->traceStepSize                   = fv.traceStepSize;
  this->traceStepDuration               = fv.traceStepDuration;
  this->traceDuration                   = fv.traceDuration;
  this->traceScheme                     = fv.traceScheme;
  this->traceSpeed                      = fv.traceSpeed;
  this->traceDirection                  = fv.traceDirection; 

  this->surfaceStyle                    = fv.surfaceStyle;
  this->surfaceShading                  = fv.surfaceShading;

  for(int i=0; i<16; i++)
  { 
    this->matrixModelViewInverse[i]           = fv.matrixModelViewInverse[i];
    this->matrixModelViewProjection[i]        = fv.matrixModelViewProjection[i];
    this->matrixModelViewProjectionInverse[i] = fv.matrixModelViewProjectionInverse[i];
  }
};

//----------------------------------------------------------------------------
qfeFlowSurfaceTrace::~qfeFlowSurfaceTrace()
{
  delete this->shaderProgramGeneratePathlines;
  delete this->shaderProgramGenerateNormals;
  delete this->shaderProgramRenderTube;
  delete this->shaderProgramRenderStrip;

  this->qfeClearSeedBuffer();  
  this->qfeClearGeometryBuffer();
  this->qfeClearAttribsBuffer();
  this->qfeClearNormalsBuffer();
  this->seedPositions.clear();
};

//----------------------------------------------------------------------------
// seeds - patient coordinates
// phase - current time in phases
qfeReturnStatus qfeFlowSurfaceTrace::qfeAddSeedPositions(qfeSeeds seeds, int items)
{  
  qfeSeeds s;

  if(int(seeds.size()) <= 0) return qfeError; 

  // we don't need a time component here
  // the w coordinate is used to store the line id
  s.clear();
  for(int i=0; i<(int)seeds.size(); i++)
  {
    qfePoint p;
    p.qfeSetPointElements(seeds[i].x, seeds[i].y, seeds[i].z, i);

    s.push_back(p);
  }

  // TODO: multiple sets
  this->seeds      = s;
  this->seedsCount = (int)seeds.size();
  this->itemsCount = items;

  this->qfeUpdateSeedPositionBuffer();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeClearSeedPositions()
{
  // Erase all information
  this->qfeClearSeedBuffer();
  this->seedPositions.clear();

  this->seedsCount = 0;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeSetSurfaceStyle(qfeFlowSurfaceStyle style)
{
  if(((style != this->surfaceStyle) || firstGenerate) && (this->seedsCount > 0))
  {
    this->qfeUpdateSeedPositionBuffer();
  }
  this->surfaceStyle = style;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeSetSurfaceShading(qfeFlowSurfaceShading shading)
{
  this->surfaceShading = shading;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeSetSurfaceTracePhases(qfeFloat phases)
{
  this->tracePhases = phases;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeSetVolumeSeries(qfeVolumeSeries series)
{
  this->volumeSeries = series;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// phase - current phase
qfeReturnStatus qfeFlowSurfaceTrace::qfeSetPhase(float phase)
{
  this->phaseCurrent = phase;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// phase         - current phase in phases
// traceDuration - total time to trace in ms
// This function will trace one cardiac cycle, starting the trace at phase
qfeReturnStatus qfeFlowSurfaceTrace::qfeGeneratePathlines(qfeVolumeSeries series, qfeFloat phase)
{
  GLuint  seedsPerItem = 0;  

  if(((int)series.size() <= 0)) return qfeError;

  // First time of generation, prepare some global variables
  if(this->firstGenerate)
  {
    this->volumeSeries  = series;

    this->volumeSeries.front()->qfeGetVolumeSize(this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);
    this->volumeSeries.front()->qfeGetVolumePhaseDuration(this->phaseDuration);

    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, this->volumeSeries.front());
    qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, this->volumeSeries.front());

    this->qfeSetStaticUniformLocations();

    this->firstGenerate = false;
  }
  
  // Set the initial particles in the buffer
  this->qfeInitSeedBuffer();
  
  this->traceDuration     = this->tracePhases*this->phaseDuration;    
  
  this->traceSteps        = (this->traceStepsPhase * this->tracePhases);
  this->traceSteps        = floor(this->traceSteps / (float)(this->traceSamples-3)) * (this->traceSamples-3);
  //this->traceSteps        = max(this->traceSteps, 2*this->traceSamples);
  this->traceStepDuration = this->traceDuration / (float)this->traceSteps;
  this->traceStepSize     = this->tracePhases   / (float)this->traceSteps;

  // Create the output buffer
  this->qfeInitGeometryBuffer();
  this->qfeInitAttribsBuffer();

  // Check if we have the necessary buffers
  if(!glIsBuffer(this->seedPositionsVBO))     return qfeError;  
  if(!glIsBuffer(this->surfaceGeometryVBO))   return qfeError;
  if(!glIsBuffer(this->surfaceAttribsVBO)) return qfeError;
  
  // No need for rasterization
  glEnable(GL_RASTERIZER_DISCARD);
 
  // Enable the velocity volumes
  this->qfeBindTexturesSliding();

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocations();

  seedsPerItem = (int)(seedsCount / (float)itemsCount);

  this->shaderProgramGeneratePathlines->qfeEnable();

    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, this->surfaceGeometryVBO);    
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, this->surfaceAttribsVBO);    

      glBeginTransformFeedback(GL_POINTS);
        
        this->qfeRenderStyleSeeds(this->seedPositionsVBO, seedsPerItem, this->itemsCount);
  
      glEndTransformFeedback();

    glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, 0);
  
  this->shaderProgramGeneratePathlines->qfeDisable();  

  // Unbind the volumes
  this->qfeUnbindTextures(); 

  // Clean up
  glDisable(GL_RASTERIZER_DISCARD);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeGenerateNormals(GLuint lines)
{
  GLint seedsPerItem;
  int   lineSize, dataSize, offset, vertexSize;
  int   adjacencyCount;
  
  adjacencyCount = 3;
  seedsPerItem   = (int)(this->seedsCount/(float)this->itemsCount)-adjacencyCount;

//  cout << seedsPerItem << endl;

  vertexSize  = 4*sizeof(GLfloat);
  offset      = this->traceSamples * vertexSize;     
  lineSize    = (seedsPerItem)   * offset;
  dataSize    = (seedsPerItem+3) * offset;

  // Create the output buffer
  this->qfeInitNormalsBuffer();

   // Check if we have the necessary buffers  
  if(!glIsBuffer(this->surfaceGeometryVBO)) return qfeError;
  if(!glIsBuffer(this->surfaceNormalsVBO))  return qfeError;
  
  // No need for rasterization
  glEnable(GL_RASTERIZER_DISCARD);
 
  // Enable the velocity volumes
  this->qfeBindTexturesSliding();
  
  this->qfeSetDynamicUniformLocations();

  for(int i=0; i<(int)this->itemsCount;i++)
  {  
    this->shaderProgramGenerateNormals->qfeEnable();
    
      glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, this->surfaceNormalsVBO, i*dataSize + offset, lineSize);    
      
        glBeginTransformFeedback(GL_POINTS);

          this->qfeRenderStyleNormals(this->surfaceGeometryVBO, seedsPerItem, this->traceSamples, this->itemsCount);
     
        glEndTransformFeedback();
      
      glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, 0);

    this->shaderProgramGenerateNormals->qfeDisable();
  }

  // Unbind the volumes
  this->qfeUnbindTextures(); 

  // Clean up
  glDisable(GL_RASTERIZER_DISCARD);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfePostProcessSeeds(qfeSeeds seedsIn, int itemCount, vector< float > &seedsOut)
{
  qfeSeeds seeds;
  int      count;

  // Initialize
  seeds.clear();
  count    = (int)(seedsIn.size()/(float)itemCount); 

  // Modify buffer for adjacency information
  for(int i=0; i<itemCount; i++)
  {
    int offset = i*count;

    seeds.push_back(seedsIn[offset+1]);
    
    for(int i=0; i<count; i++)  
      seeds.push_back(seedsIn[offset+i]);    

    seeds.push_back(seedsIn[offset+count-2]);
  }

  // Build the output structure
  for(int i=0; i<(int)seeds.size(); i++)
  {
    seedsOut.push_back(seeds[i].x);
    seedsOut.push_back(seeds[i].y);
    seedsOut.push_back(seeds[i].z);  
    seedsOut.push_back(seeds[i].w);  
  } 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfePostProcessSeedsLoop(qfeSeeds seedsIn, int itemCount,  vector< float > &seedsOut)
{
  qfeSeeds seeds;
  int      count;

  // Initialize
  seeds.clear();
  count    = (int)(seedsIn.size()/(float)itemCount); 

  // Modify buffer for adjacency information    
  for(int i=0; i<itemCount; i++)
  {
    int offset = i*count;

    seeds.push_back(seedsIn[offset+count-1]);
    
    for(int i=0; i<count; i++)  
      seeds.push_back(seedsIn[offset+i]);    

    seeds.push_back(seedsIn[offset+0]);
    seeds.push_back(seedsIn[offset+1]);
  }

  // Build the output structure
  for(int i=0; i<(int)seeds.size(); i++)
  {
    seedsOut.push_back(seeds[i].x);
    seedsOut.push_back(seeds[i].y);
    seedsOut.push_back(seeds[i].z);  
    seedsOut.push_back(seeds[i].w);  
  } 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeRenderSurface(qfeColorMap *colormap)
{
  // First time of rendering, prepare some global variables
  if(this->firstRender)
  {
    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->colormap);
  
  if((int)this->volumeSeries.size() <= 0 || this->seedsCount <= 0) return qfeError;

  // Obtain the modelview information
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Set point parameters, in case of normal point rendering
  glLineWidth(4.0);
  glPointSize(10.0);
  glEnable(GL_LINE_SMOOTH);
  glEnable(GL_POINT_SMOOTH);
  glColor3f(1.0,0.0,0.0);

  // Allow blending for transparency
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Bind the texture
  this->qfeBindTexturesRender();

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocations();

  this->qfeGeneratePathlines(this->volumeSeries, this->phaseCurrent);  
  this->qfeGenerateNormals(this->surfaceNormalsVBO);

  switch(this->surfaceStyle)
  {
  case qfeFlowSurfaceTube :       
    this->qfeRenderStyleTube(this->surfaceGeometryVBO, this->surfaceNormalsVBO, this->surfaceAttribsVBO);
    break;
  case qfeFlowSurfaceStrip :       
    this->qfeRenderStyleStrip(this->surfaceGeometryVBO, this->surfaceNormalsVBO, this->surfaceAttribsVBO);
    break;
  case qfeFlowSurfaceCross :       
    this->qfeRenderStyleStrip(this->surfaceGeometryVBO, this->surfaceNormalsVBO, this->surfaceAttribsVBO);
    break;
  default :          
    this->qfeRenderStyleStrip(this->surfaceGeometryVBO, this->surfaceNormalsVBO, this->surfaceAttribsVBO);    
  }

  this->qfeUnbindTextures();

  glDisable(GL_BLEND);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeRenderStyleSeeds(GLuint seeds, int seedsCount, int itemCount)
{
  GLint location;

  this->shaderProgramGeneratePathlines->qfeGetAttribLocation("qfe_SeedPoints", location);  

  if(!glIsBuffer(seeds) || location == -1)
    return qfeError;  

  glBindBuffer(GL_ARRAY_BUFFER, seeds);

  glEnableVertexAttribArray(location);
  glVertexAttribPointer(location,  4, GL_FLOAT, GL_FALSE, 0, 0);

  this->qfeDrawPoints(seeds, seedsCount, itemCount);   

  glDisableVertexAttribArray(location); 

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeRenderStyleNormals(GLuint surfaceLines, int lineCount, int lineSize, int itemCount)
{
  GLint   location[3];
  GLsizei vertexSize;
  GLvoid *offset1, *offset2;

  vertexSize  = 4*sizeof(GLfloat);
  offset1     = BUFFER_OFFSET(  lineSize*vertexSize);
  offset2     = BUFFER_OFFSET(2*lineSize*vertexSize);

  this->shaderProgramGenerateNormals->qfeGetAttribLocation("qfe_QuadLine",      location[0]);  
  this->shaderProgramGenerateNormals->qfeGetAttribLocation("qfe_QuadLineLeft",  location[1]);  
  this->shaderProgramGenerateNormals->qfeGetAttribLocation("qfe_QuadLineRight", location[2]);  

  if(!glIsBuffer(surfaceLines) || location[0] == -1 || location[1] == -1 || location[2] == -1)
    return qfeError;

  glBindBuffer(GL_ARRAY_BUFFER, surfaceLines);

  glEnableVertexAttribArray(location[0]);
  glVertexAttribPointer(location[0],  4, GL_FLOAT, GL_FALSE, 0, offset1);
  
  glEnableVertexAttribArray(location[1]);
  glVertexAttribPointer(location[1],  4, GL_FLOAT, GL_FALSE, 0, 0);
  
  glEnableVertexAttribArray(location[2]);
  glVertexAttribPointer(location[2],  4, GL_FLOAT, GL_FALSE, 0, offset2);  

  this->qfeDrawLineStripsAdjacent(surfaceLines, lineCount, lineSize, itemCount); 
      
  glDisableVertexAttribArray(location[0]); 
  glDisableVertexAttribArray(location[1]);   
  glDisableVertexAttribArray(location[2]);   

  glBindBuffer(GL_ARRAY_BUFFER, 0);
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeRenderStyleTube(GLuint surfaceLines, GLuint surfaceNormals, GLuint surfaceAttribs)
{
  GLint location[6]; 
  GLsizei lineCount, lineSize;  
  GLsizei vertexSize;
  GLvoid *offset1, *offset2;
  GLint   seedsPerItem;

  // We render until seedcount-1 and provide a shifted attribute buffer to form the quads
  this->shaderProgramRenderTube->qfeGetAttribLocation("qfe_QuadLine1",    location[0]);  
  this->shaderProgramRenderTube->qfeGetAttribLocation("qfe_QuadLine2",    location[1]);  
  this->shaderProgramRenderTube->qfeGetAttribLocation("qfe_QuadNormals1", location[2]);  
  this->shaderProgramRenderTube->qfeGetAttribLocation("qfe_QuadNormals2", location[3]);  
  this->shaderProgramRenderTube->qfeGetAttribLocation("qfe_QuadAttribs1", location[4]);  
  this->shaderProgramRenderTube->qfeGetAttribLocation("qfe_QuadAttribs2", location[5]);  

  this->shaderProgramRenderTube->qfeEnable();

  seedsPerItem = (this->seedsCount/(float)this->itemsCount);
  lineSize     = this->traceSamples;
  lineCount    = seedsPerItem-3;
  vertexSize   = 4*sizeof(GLfloat);
  offset1      = BUFFER_OFFSET(  lineSize*vertexSize);
  offset2      = BUFFER_OFFSET(2*lineSize*vertexSize);

  glBindBuffer(GL_ARRAY_BUFFER, surfaceLines);

  // surface positions
  glEnableVertexAttribArray(location[0]);
  glVertexAttribPointer(location[0],  4, GL_FLOAT, GL_FALSE, 0, offset1);
  glEnableVertexAttribArray(location[1]);
  glVertexAttribPointer(location[1],  4, GL_FLOAT, GL_FALSE, 0, offset2);

  // surface normals
  // caution: the normalbuffer is shifted by one linesegment
  //          the normals are not computed for the first line
  glBindBuffer(GL_ARRAY_BUFFER, surfaceNormals);

  glEnableVertexAttribArray(location[2]);
  glVertexAttribPointer(location[2],  4, GL_FLOAT, GL_FALSE, 0, offset1);
  glEnableVertexAttribArray(location[3]);
  glVertexAttribPointer(location[3],  4, GL_FLOAT, GL_FALSE, 0, offset2);

  // surface attributes
  glBindBuffer(GL_ARRAY_BUFFER, surfaceAttribs);
  
  glEnableVertexAttribArray(location[4]);
  glVertexAttribPointer(location[4],  4, GL_FLOAT, GL_FALSE, 0, offset1);
  glEnableVertexAttribArray(location[5]);
  glVertexAttribPointer(location[5],  4, GL_FLOAT, GL_FALSE, 0, offset2);
  
  this->qfeDrawQuads(this->surfaceGeometryVBO, this->surfaceNormalsVBO, this->surfaceAttribsVBO, lineCount, lineSize, this->itemsCount);

  glDisableVertexAttribArray(location[0]); 
  glDisableVertexAttribArray(location[1]); 
  glDisableVertexAttribArray(location[2]);
  glDisableVertexAttribArray(location[3]);
  glDisableVertexAttribArray(location[4]);
  glDisableVertexAttribArray(location[5]);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  this->shaderProgramRenderTube->qfeDisable(); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeRenderStyleStrip(GLuint surfaceLines, GLuint surfaceNormals, GLuint surfaceAttribs)
{
  GLint location[6]; 
  GLsizei lineCount, lineSize;  
  GLsizei vertexSize;
  GLvoid *offset1, *offset2;
  GLint   seedsPerItem;

  // We render until seedcount-1 and provide a shifted attribute buffer to form the quads
  this->shaderProgramRenderStrip->qfeGetAttribLocation("qfe_QuadLine1",    location[0]);  
  this->shaderProgramRenderStrip->qfeGetAttribLocation("qfe_QuadLine2",    location[1]);  
  this->shaderProgramRenderStrip->qfeGetAttribLocation("qfe_QuadNormals1", location[2]);  
  this->shaderProgramRenderStrip->qfeGetAttribLocation("qfe_QuadNormals2", location[3]);  
  this->shaderProgramRenderStrip->qfeGetAttribLocation("qfe_QuadAttribs1", location[4]);  
  this->shaderProgramRenderStrip->qfeGetAttribLocation("qfe_QuadAttribs2", location[5]);  

  this->shaderProgramRenderStrip->qfeEnable();

  seedsPerItem = (this->seedsCount/(float)this->itemsCount);
  lineSize     = this->traceSamples;
  lineCount    = seedsPerItem-2;
  vertexSize   = 4*sizeof(GLfloat);
  offset1      = BUFFER_OFFSET(  lineSize*vertexSize);
  offset2      = BUFFER_OFFSET(2*lineSize*vertexSize);

  glBindBuffer(GL_ARRAY_BUFFER, surfaceLines);

  glEnableVertexAttribArray(location[0]);
  glVertexAttribPointer(location[0],  4, GL_FLOAT, GL_FALSE, 0, offset1);
  glEnableVertexAttribArray(location[1]);
  glVertexAttribPointer(location[1],  4, GL_FLOAT, GL_FALSE, 0, offset2);

  // caution: the normalbuffer is shifted by one linesegment
  //          the normals are not computed for the first line
  glBindBuffer(GL_ARRAY_BUFFER, surfaceNormals);

  glEnableVertexAttribArray(location[2]);
  glVertexAttribPointer(location[2],  4, GL_FLOAT, GL_FALSE, 0, offset1);
  glEnableVertexAttribArray(location[3]);
  glVertexAttribPointer(location[3],  4, GL_FLOAT, GL_FALSE, 0, offset2);

  // surface attributes
  glBindBuffer(GL_ARRAY_BUFFER, surfaceAttribs);
  
  glEnableVertexAttribArray(location[4]);
  glVertexAttribPointer(location[4],  4, GL_FLOAT, GL_FALSE, 0, offset1);
  glEnableVertexAttribArray(location[5]);
  glVertexAttribPointer(location[5],  4, GL_FLOAT, GL_FALSE, 0, offset2);
  
  this->qfeDrawQuads(this->surfaceGeometryVBO, this->surfaceNormalsVBO, this->surfaceAttribsVBO, lineCount, lineSize, this->itemsCount);

  glDisableVertexAttribArray(location[0]); 
  glDisableVertexAttribArray(location[1]); 
  glDisableVertexAttribArray(location[2]);
  glDisableVertexAttribArray(location[3]);
  glDisableVertexAttribArray(location[4]);
  glDisableVertexAttribArray(location[5]);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  this->shaderProgramRenderStrip->qfeDisable(); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeUpdateSeedPositionBuffer()
{
  if((int)this->seeds.size() <= 0) return qfeError;

  // Generate the right set of seeds with adjacency
  this->seedPositions.clear();
  switch(this->surfaceStyle)
  {
  case qfeFlowSurfaceTube : 
    this->qfePostProcessSeedsLoop(this->seeds, this->itemsCount, this->seedPositions);
    break;
  case qfeFlowSurfaceStrip :
  case qfeFlowSurfaceCross :
    this->qfePostProcessSeeds(this->seeds, this->itemsCount, this->seedPositions);
    break;  
  }
  
  this->seedsCount = (int)(this->seedPositions.size()/4.0);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeSetLightSource(qfeLightSource *light)
{
  this->light = light;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeDrawPoints(GLuint points, int pointCount, int itemCount)
{
  if(!glIsBuffer(points)) return qfeError;

  int chunkSize = (int)(this->seedsCount/(float)itemCount);

  for(int i=0; i<itemCount; i++)
  {
    glDrawArrays(GL_POINTS, (i*chunkSize), pointCount);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeDrawLineStripsAdjacent(GLuint lines, int lineCount, int lineSize, int itemCount)
{
  if(!glIsBuffer(lines)) return qfeError;    

  int chunkSize = (int)(this->seedsCount/(float)itemCount)*lineSize;
  
  for(int i=0; i<itemCount; i++)
  {
    for(int j=0; j<lineCount; j++)
    {
      glDrawArrays(GL_LINE_STRIP_ADJACENCY_ARB, (i*chunkSize) + j*lineSize, lineSize);
    }  
  }
    
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeDrawQuads(GLuint surfaceLines, GLuint surfaceNormals, GLuint surfaceAttribs, int lineCount, int lineSize, int itemCount)
{
  if(!glIsBuffer(surfaceLines) || !glIsBuffer(surfaceNormals)) return qfeError;

  int chunkSize = (int)(this->seedsCount/(float)itemCount)*lineSize;
 
  for(int i=0; i<itemCount; i++)
  {
    for(int j=0; j<lineCount; j++)
    {
      glDrawArrays(GL_LINE_STRIP_ADJACENCY_ARB, (i*chunkSize) + j*lineSize, lineSize);
    }
  }

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeInitSeedBuffer()
{
  float           timeShift;
  int             dataSize;

  if(this->seedPositions.size() <= 0) return qfeError;

  this->qfeClearSeedBuffer();

  timeShift = this->tracePhases/(float)(this->traceSamples-3);
  dataSize  = (int)this->seedsCount * 4 * sizeof(GLfloat);

  glGenBuffers(1, &this->seedPositionsVBO);    

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->seedPositionsVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &this->seedPositions[0], GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeClearSeedBuffer()
{
  if(glIsBuffer(this->seedPositionsVBO))
    glDeleteBuffers(1, &this->seedPositionsVBO);      
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeInitGeometryBuffer()
{
  GLsizei dataSize;
  GLsizei vertexSize;
  GLsizei vertexCount;

  this->qfeClearGeometryBuffer();

  vertexSize  = 4*sizeof(GLfloat);
  vertexCount = this->traceSamples;
  dataSize    = (int)this->seedsCount * vertexCount * vertexSize;

  glGenBuffers(1, &this->surfaceGeometryVBO);    

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->surfaceGeometryVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, 0, GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeClearGeometryBuffer()
{
  if(glIsBuffer(this->surfaceGeometryVBO))
    glDeleteBuffers(1, &this->surfaceGeometryVBO);      
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeInitNormalsBuffer()
{
  GLsizei dataSize;
  GLsizei scalarCount;
  GLsizei vertexSize;
  GLsizei vertexCount;
  vector< float > empty;

  this->qfeClearNormalsBuffer();

  vertexSize  = 4*sizeof(GLfloat);
  vertexCount = this->traceSamples;
  dataSize    = (int)this->seedsCount * vertexCount * vertexSize;
  scalarCount = (int)this->seedsCount * vertexCount * 4;

  // Make sure we initialize the buffer with zeros
  empty.clear();
  for(int i=0; i<scalarCount; i++)  empty.push_back(0.0);  

  glGenBuffers(1, &this->surfaceNormalsVBO);    

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->surfaceNormalsVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &empty[0], GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeClearNormalsBuffer()
{
  if(glIsBuffer(this->surfaceNormalsVBO))
    glDeleteBuffers(1, &this->surfaceNormalsVBO);      
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeInitAttribsBuffer()
{
  GLsizei dataSize;
  GLsizei scalarCount;
  GLsizei vertexSize;
  GLsizei vertexCount;
  vector< float > empty;

  this->qfeClearAttribsBuffer();

  vertexSize  = 4*sizeof(GLfloat);
  vertexCount = this->traceSamples;
  dataSize    = (int)this->seedsCount * vertexCount * vertexSize;
  scalarCount = (int)this->seedsCount * vertexCount * 4;

  // Make sure we initialize the buffer with zeros
  empty.clear();
  for(int i=0; i<scalarCount; i++)  empty.push_back(0.0);  

  glGenBuffers(1, &this->surfaceAttribsVBO);    

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->surfaceAttribsVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &empty[0], GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeClearAttribsBuffer()
{
  if(glIsBuffer(this->surfaceAttribsVBO))
    glDeleteBuffers(1, &this->surfaceAttribsVBO);      
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeGetModelViewMatrix()
{
  qfeMatrix4f mv, p, mvp, mvinv, mvpinv;

  GLfloat modelview[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f::qfeSetMatrixElements(mv, modelview);

  GLfloat projection[16];
  glGetFloatv(GL_PROJECTION_MATRIX, projection);
  qfeMatrix4f::qfeSetMatrixElements(p, projection);

  mvp    = mv*p;
  qfeMatrix4f::qfeGetMatrixInverse(mvp, mvpinv);
  qfeMatrix4f::qfeGetMatrixInverse(mv, mvinv);

  qfeMatrix4f::qfeGetMatrixElements(mvinv,  this->matrixModelViewInverse);
  qfeMatrix4f::qfeGetMatrixElements(mvp,    this->matrixModelViewProjection);
  qfeMatrix4f::qfeGetMatrixElements(mvpinv, this->matrixModelViewProjectionInverse);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeBindTexturesSliding()
{
  GLuint        tids[5];
  int           phasesCount;

  phasesCount = (int)this->volumeSeries.size();

  if(phasesCount > 0)
  {
    //cout << phaseGenerate << ": ";
    // We take a range [phase-2, phase+2] datasets, surrounding the current phase
    int index;
    for(int i=-2; i<=2; i++)
    {
      if(phasesCount == 1)
        index = 0;
      else
      {
        index = ((int)floor(this->phaseCurrent))+i;
        qfeMath::qfeMod(index, phasesCount, index);
      }

      //cout << index << " - ";

      this->volumeSeries[index]->qfeGetVolumeTextureId(tids[i+2]);
    }
    //cout << endl;

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_3D, tids[0]);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_3D, tids[1]);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_3D, tids[2]);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_3D, tids[3]);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_3D, tids[4]);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeBindTexturesRender()
{  
  glActiveTexture(GL_TEXTURE7);
  glBindTexture(GL_TEXTURE_1D, this->colormap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeSetStaticUniformLocations()
{
  this->shaderProgramGeneratePathlines->qfeSetUniform1i("volumeData0", 1);
  this->shaderProgramGeneratePathlines->qfeSetUniform1i("volumeData1", 2);
  this->shaderProgramGeneratePathlines->qfeSetUniform1i("volumeData2", 3);
  this->shaderProgramGeneratePathlines->qfeSetUniform1i("volumeData3", 4);
  this->shaderProgramGeneratePathlines->qfeSetUniform1i("volumeData4", 5);
  this->shaderProgramGeneratePathlines->qfeSetUniform3i("volumeDimensions", this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);
  this->shaderProgramGeneratePathlines->qfeSetUniformMatrix4f("voxelPatientMatrix", 1, this->V2P);
  this->shaderProgramGeneratePathlines->qfeSetUniformMatrix4f("patientVoxelMatrix", 1, this->P2V);

  this->shaderProgramRenderTube->qfeSetUniform1i("transferFunction", 7);     
  this->shaderProgramRenderStrip->qfeSetUniform1i("transferFunction", 7);     

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowSurfaceTrace::qfeSetDynamicUniformLocations()
{
  qfePoint  lightSpec;
  qfeVector lightDir;
  qfeFloat  vu, vl;
  qfeVector texRange;

  // Obtain actual lighting parameters
  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 10.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w);
    this->light->qfeGetLightSourceDirection(lightDir);
  }

  // Obtain the dataset venc
  if((int)this->volumeSeries.size() > 0)     
  {
    this->volumeSeries.front()->qfeGetVolumeValueDomain(vl, vu);
  }
  else
  {
    vu = -200.0;
    vl = 200.0;
  }

  // Set the tex range
  texRange.x = this->traceSamples-3;
  texRange.y = (this->seedsCount/(float)this->itemsCount)-1;  

//  cout << this->traceDuration << ", " << this->traceSteps << " - " << this->traceStepDuration << " : ";
//  cout << int(max(traceSamples-3, traceSteps)/float(traceSamples-3)) << endl;

  this->shaderProgramGeneratePathlines->qfeSetUniform1f("phaseCurrent",         this->phaseCurrent);
  this->shaderProgramGeneratePathlines->qfeSetUniform1i("traceSamples",         this->traceSamples);
  this->shaderProgramGeneratePathlines->qfeSetUniform1i("traceSteps",           this->traceSteps);
  this->shaderProgramGeneratePathlines->qfeSetUniform1f("traceStepSize",        this->traceStepSize);
  this->shaderProgramGeneratePathlines->qfeSetUniform1f("traceStepDuration",    this->traceStepDuration);
  this->shaderProgramGeneratePathlines->qfeSetUniform1i("traceScheme",          this->traceScheme);
  this->shaderProgramGeneratePathlines->qfeSetUniform1i("traceSpeed",           this->traceSpeed);
  this->shaderProgramGeneratePathlines->qfeSetUniform1i("traceDirection",       this->traceDirection);
  this->shaderProgramGeneratePathlines->qfeSetUniform2f("texRange",             texRange.x, texRange.y);

  this->shaderProgramGenerateNormals->qfeSetUniform1f("traceSamples",           this->traceSamples);

  this->shaderProgramRenderTube->qfeSetUniform4f("light",                           lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderTube->qfeSetUniform4f("lightDirection",                  lightDir.x, lightDir.y, lightDir.z, lightDir.w);     
  this->shaderProgramRenderTube->qfeSetUniform1i("surfaceShading",                  this->surfaceShading);       
  this->shaderProgramRenderTube->qfeSetUniform2f("dataRange",                       vl, vu);       
  this->shaderProgramRenderTube->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);
  this->shaderProgramRenderTube->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);

  this->shaderProgramRenderStrip->qfeSetUniform4f("light",                           lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderStrip->qfeSetUniform4f("lightDirection",                  lightDir.x, lightDir.y, lightDir.z, lightDir.w);     
  this->shaderProgramRenderStrip->qfeSetUniform1i("surfaceShading",                  this->surfaceShading);       
  this->shaderProgramRenderStrip->qfeSetUniform2f("dataRange",                       vl, vu);       
  this->shaderProgramRenderStrip->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);
  this->shaderProgramRenderStrip->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);

  return qfeSuccess;
}
