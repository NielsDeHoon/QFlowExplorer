#include "qfeMagnitudeFilter.h"

qfeMagnitudeFilter::qfeMagnitudeFilter() {
  magnitudeData = nullptr;
  
  size = 0;
  width = 0;
  height = 0;
  depth = 0;
  nrFrames = 0;

  rangeMin = 0;
  rangeMax = 0;

  threshold = 0.f;
}

void qfeMagnitudeFilter::qfeSetData(qfeVolumeSeries *magnitudeData) {
  this->magnitudeData = magnitudeData;

  (*magnitudeData)[0]->qfeGetVolumeSize(width, height, depth);
  size = width * height * depth;
  nrFrames = (unsigned)magnitudeData->size();

  (*magnitudeData)[0]->qfeGetVolumeValueDomain(rangeMin, rangeMax);
}

void qfeMagnitudeFilter::qfeSetThreshold(float percentage) {
  this->threshold = percentage;
}

void qfeMagnitudeFilter::qfeUpdate() {
}

void qfeMagnitudeFilter::qfeApplyFilter(qfeVolumeSeries *velocityData, bool inverted) {
  int size = width * height * depth;
  unsigned short absThreshold = (unsigned short)(rangeMin + threshold * (rangeMax - rangeMin));
  for (unsigned fi = 0; fi < nrFrames; fi++) {
    qfeValueType t;
    short *magnitude = nullptr;
    float *velocity = nullptr;
    (*magnitudeData)[fi]->qfeGetVolumeData(t, width, height, depth, (void **)&magnitude);
    (*velocityData)[fi]->qfeGetVolumeData(t, width, height, depth, (void **)&velocity);
    for (unsigned vi = 0; vi < size; vi++) {
      if (!inverted && magnitude[vi] < absThreshold) {
        velocity[vi*3] *= 0.f;
        velocity[vi*3+1] *= 0.f;
        velocity[vi*3+2] *= 0.f;
      } else if (inverted && magnitude[vi] > absThreshold) {
        velocity[vi*3] *= 0.f;
        velocity[vi*3+1] *= 0.f;
        velocity[vi*3+2] *= 0.f;
      }
    }
  }
}