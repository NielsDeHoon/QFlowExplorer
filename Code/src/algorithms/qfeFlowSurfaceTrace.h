#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithmFlow.h"
#include "qfeColorMap.h"
#include "qfeDisplay.h"
#include "qfeGLShaderProgram.h"
#include "qfeGLSupport.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeMath.h"
#include "qfePoint.h"
#include "qfeSeeding.h"
#include "qfeTransform.h"
#include "qfeVolume.h"

#include <vector>

#define BUFFER_OFFSET(i) ((char*)NULL + (i))

typedef vector< qfePoint >  qfeParticles;

enum qfeFlowSurfaceStyle
{
  qfeFlowSurfaceTube,
  qfeFlowSurfaceStrip,
  qfeFlowSurfaceCross
};

enum qfeFlowSurfaceShading
{
	qfeFlowSurfaceCel,
  qfeFlowSurfacePhong,
  qfeFlowSurfaceStripes,	
  qfeFlowSurfaceNone
};

enum qfeFlowSurfaceIntegrationScheme
{
  qfeFlowSurfaceRK1,
  qfeFlowSurfaceRK2,
  qfeFlowSurfaceRK4
};

/**
* \file   qfeFlowSurfaceTrace.h
* \author Roy van Pelt
* \class  qfeFlowSurfaceTrace
* \brief  Implements rendering of a line trace
* \note   Confidential
*
* Rendering of a surface trace
*/

class qfeFlowSurfaceTrace : public qfeAlgorithmFlow
{
public:
  qfeFlowSurfaceTrace();
  qfeFlowSurfaceTrace(const qfeFlowSurfaceTrace &fv);
  ~qfeFlowSurfaceTrace();

  qfeReturnStatus qfeClearSeedPositions();
  qfeReturnStatus qfeAddSeedPositions(qfeSeeds seeds, int items);  
   
  qfeReturnStatus qfeRenderSurface(qfeColorMap *colormap);

  qfeReturnStatus qfeSetSurfaceStyle(qfeFlowSurfaceStyle type);
  qfeReturnStatus qfeSetSurfaceShading(qfeFlowSurfaceShading shading);
  qfeReturnStatus qfeSetSurfaceTracePhases(qfeFloat phases);
  qfeReturnStatus qfeSetVolumeSeries(qfeVolumeSeries series);
  qfeReturnStatus qfeSetPhase(float phase);
  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);

protected:
  qfeReturnStatus qfeGeneratePathlines(qfeVolumeSeries series, qfeFloat phase);
  qfeReturnStatus qfeGenerateNormals(GLuint lines);

  qfeReturnStatus qfePostProcessSeeds(qfeSeeds seedsIn, int itemCount, vector< float > &seedsOut);
  qfeReturnStatus qfePostProcessSeedsLoop(qfeSeeds seedsIn, int itemCount, vector< float > &seedsOut);
  
  qfeReturnStatus qfeInitSeedBuffer();
  qfeReturnStatus qfeClearSeedBuffer();
  qfeReturnStatus qfeInitGeometryBuffer();
  qfeReturnStatus qfeClearGeometryBuffer();
  qfeReturnStatus qfeInitNormalsBuffer();
  qfeReturnStatus qfeClearNormalsBuffer();
  qfeReturnStatus qfeInitAttribsBuffer();
  qfeReturnStatus qfeClearAttribsBuffer();

  qfeReturnStatus qfeGetModelViewMatrix();
  
  qfeReturnStatus qfeRenderStyleSeeds(GLuint seeds, int seedsCount, int itemCount);
  qfeReturnStatus qfeRenderStyleNormals(GLuint surfaceLines, int lineCount, int lineSize, int itemCount);  
  qfeReturnStatus qfeRenderStyleTube(GLuint surfaceLines, GLuint surfaceNormals, GLuint surfaceAttribs);
  qfeReturnStatus qfeRenderStyleStrip(GLuint surfaceLines, GLuint surfaceNormals, GLuint surfaceAttribs);
  
  qfeReturnStatus qfeUpdateSeedPositionBuffer();
  
  qfeReturnStatus qfeDrawPoints(GLuint points, int pointCount, int itemCount);  
  qfeReturnStatus qfeDrawLineStripsAdjacent(GLuint surfaceLines, int lineCount, int lineSize, int itemCount);
  qfeReturnStatus qfeDrawQuads(GLuint surfaceLines, GLuint surfaceNormals, GLuint surfaceAttribs, int lineCount, int lineSize, int itemCount);  
  
  qfeReturnStatus qfeBindTexturesSliding();
  qfeReturnStatus qfeBindTexturesRender();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

private :
  bool                  firstGenerate;
  bool                  firstRender;
  qfeGLShaderProgram   *shaderProgramGeneratePathlines;
  qfeGLShaderProgram   *shaderProgramGenerateNormals;
  qfeGLShaderProgram   *shaderProgramRenderTube;
  qfeGLShaderProgram   *shaderProgramRenderStrip;

  qfeVolumeSeries       volumeSeries;  
  qfeLightSource       *light;
  GLuint                colormap;  

  GLuint                surfaceGeometryVBO;  
  GLuint                surfaceNormalsVBO;  
  GLuint                surfaceAttribsVBO;  
  GLuint                seedPositionsVBO;  
  vector< qfeFloat >    seedPositions;   
  unsigned int          itemsCount;
  qfeSeeds              seeds;
  unsigned int          seedsCount;  

  qfeFloat              texCoordT;

  static const char    *feedbackVars1[];
  static const char    *feedbackVars2[];
  
  // Static uniform locations
  qfeMatrix4f           V2P, P2V;
  unsigned int          volumeSize[3];
  qfeFloat              phaseDuration;

  // Dynamic uniform locations
  float                 phaseCurrent;  
  float                 tracePhases;
  int                   traceSamples;  
  int                   traceSteps;
  int                   traceStepsPhase;
  float                 traceStepSize;
  float                 traceStepDuration;
  float                 traceDuration;
  int                   traceScheme;
  int                   traceSpeed;
  int                   traceDirection;
  qfeFlowSurfaceStyle   surfaceStyle;
  qfeFlowSurfaceShading surfaceShading;
  GLfloat               matrixModelViewInverse[16];
  GLfloat               matrixModelViewProjection[16];
  GLfloat               matrixModelViewProjectionInverse[16];
};
