#include "qfeClusterNodeLinearModel.h"

//----------------------------------------------------------------------------
//  Constructors
//----------------------------------------------------------------------------
qfeClusterNodeLinearModel::qfeClusterNodeLinearModel(qfeClusterPoint point)
{
  this->modelError = -1;
  this->modelSize = 1;

  this->clusterPoints.clear();
  this->clusterPoints.push_back(point);

  for(int i=0; i<4; i++)
    this->X[i] = point.position[i];

  for(int i=0; i<3; i++)
    this->V[i] = point.flow[i];


  int XXindex = 0;
  for(int i=0; i<4; i++)
  {
    for(int j=i; j<4; j++)
      this->XX[XXindex++] = this->X[i] * this->X[j];
  }

  int XVindex = 0;
  for(int i=0; i<4; i++)
  {
    for(int j=0; j<3; j++)
      this->XV[XVindex++] = this->X[i] * this->V[j];
  }
};

//----------------------------------------------------------------------------
//  Destructor
//----------------------------------------------------------------------------
qfeClusterNodeLinearModel::~qfeClusterNodeLinearModel()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeClusterNodeLinearModel::qfeMergeRepresentative(qfeClusterNode *c2)
{
  qfeClusterNodeLinearModel *other = (qfeClusterNodeLinearModel*)c2;

  this->modelSize += other->modelSize;

  for(int i=0; i<4; i++)
    this->X[i] += other->X[i];

  for(int i=0; i<3; i++)
    this->V[i] += other->V[i];

  for(int i=0; i<10; i++)
    this->XX[i] += other->XX[i];

  for(int i=0; i<12; i++)
    this->XV[i] += other->XV[i];

  this->clusterPoints.reserve(this->clusterPoints.size() + other->clusterPoints.size());
  this->clusterPoints.insert(this->clusterPoints.end(),other->clusterPoints.begin(),other->clusterPoints.end());

  this->qfeCalculateClusterModelError();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeClusterNodeLinearModel::qfeAddModelPoint(qfeClusterPoint point)
{
  //cout << "Adding:\tv(" << posArray[0] << "," << posArray[1] << "," << posArray[2] << "," << posArray[3] << ") = ";
  //cout << "[" << flowArray[0] << "," << flowArray[1] << "," << flowArray[2] << "]\n";
  for(int i=0; i<4; i++)
    this->X[i] += point.position[i];

  for(int i=0; i<3; i++)
    this->V[i] += point.flow[i];

  int XXindex = 0;
  for(int i=0; i<4; i++)
  {
    for(int j=i; j<4; j++)
      this->XX[XXindex++] += point.position[i] * point.position[j];
  }

  int XVindex = 0;
  for(int i=0; i<4; i++)
  {
    for(int j=0; j<3; j++)
      this->XV[XVindex++] += point.position[i] * point.flow[j];
  }

  this->modelSize++;

  //cout << "\npos  " << modelSize << " = \n";
  //for(int i=0; i<4; i++) cout << "\t" << posArray[i];
  //cout << "\nflow " << modelSize << " = \n";
  //for(int i=0; i<3; i++) cout << "\t" << flowArray[i];

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeClusterNodeLinearModel::qfeCalculateClusterModelError()
{
  qfeFloat A[12];
  qfeFloat v0[3];
  bool singular;

  qfeClusterNodeLinearModel::qfeEstimateLinearModel(this->X, this->V, this->XX, this->XV, this->modelSize, A, v0, singular);

  if(singular)
    qfeClusterNodeLinearModel::qfeCalculateModelError(v0, &this->clusterPoints, this->modelError);
  else
    qfeClusterNodeLinearModel::qfeCalculateModelError(A, v0, &this->clusterPoints, this->modelError);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeClusterNodeLinearModel::qfeEstimateLinearModel(qfeFloat X[4], qfeFloat V[3], qfeFloat XX[10], qfeFloat XV[12], \
                                                                  unsigned int modelSize, qfeFloat A[12], qfeFloat v0[3], bool &singular)
{

  qfeFloat vArray[16];
  qfeFloat wTranspose[12];

  bool singularDimensions[4];

  int XXindex = 0;
  for(int i=0; i<4; i++)
  {
    for(int j=i; j<4; j++)
    {
      vArray[i*4 + j] = XX[XXindex] - X[i] * X[j] / modelSize;
      vArray[j*4 + i] = vArray[i*4 + j];
      XXindex++;
    }

    // Part 1 of hack to deal with singular dimensions
    singularDimensions[i] = (vArray[i*5]==0);
    if(singularDimensions[i]) vArray[i*5]=1.0f;
  }

  for(int i=0; i<4; i++)
  {
    for(int j=0; j<3; j++)
    {
      wTranspose[j*4 + i] = XV[i*3 + j] - X[i] * V[j] / modelSize;
    }
  }

  qfeMatrix4f vMatrix = qfeMatrix4f(vArray);
  qfeMatrix4f vInverse;
  qfeMatrix4f::qfeGetMatrixInverse(vMatrix,vInverse);

  // Part 2 of hack to deal with singular dimensions
  for(int i=0; i<4; i++) if(singularDimensions[i]) vInverse[i*5]=0;

  //  Wtr  *  Vin  =  A 
  // [i,k] * [k,j] = [i,j]
  singular = false;
  for(int i=0; i<3; i++)
  {
    qfeFloat dotAXX = 0;
    for(int j=0; j<4; j++)
    {
      qfeFloat coefA = 0;
      for(int k=0; k<4; k++)
      {
        coefA += wTranspose[i*4 + k] * vInverse[k*4 + j];
      }
      singular |= !(coefA<0 || coefA>=0);

      A[i*4 + j] = coefA;
      dotAXX += coefA * X[j];
    }
    v0[i] = (dotAXX - V[i]) / modelSize;
  }

  if(singular)
  {
    for(int i=0; i<3; i++)
    {
      v0[i] = V[i]/modelSize;
    }
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeClusterNodeLinearModel::qfeCalculateModelError(qfeFloat A[12], qfeFloat v0[3], vector<qfeClusterPoint>* clusterPoints, float &modelError)
{
  qfeFloat modeledFlow[3];
  qfeFloat flowDifference[3];

  modelError = 0;

  // For each cluster point n:
  //  - Calculate modeled flow:     vModel(n) = A*x - v0
  //  - Calculate model error:      error(n)  = || vModel(n) - vTrue(n) ||^2

  //cout << "\nA  = [\t";
  //cout << A[0] << ",\t" << A[1] << ",\t" << A[2] << ",\t" << A[3] << ",\n\t";
  //cout << A[4] << ",\t" << A[5] << ",\t" << A[6] << ",\t" << A[7] << ",\n\t";
  //cout << A[8] << ",\t" << A[9] << ",\t" << A[10] << ",\t" << A[11] << "]\n";
  //cout << "\nv0 = [\t" << v0[0] << ",\t" << v0[1] << ",\t" << v0[2] << "]\n\n";

  for(int n=0; n<(int)clusterPoints->size(); n++)
  {
    
    for(int i=0; i<3; i++)
    {
      modeledFlow[i]= -v0[i];
      for(int j=0; j<4; j++)
        modeledFlow[i] += clusterPoints->at(n).position[j] * A[i*4 + j];
    }

    for(int i=0; i<3; i++)
      flowDifference[i] = modeledFlow[i] - clusterPoints->at(n).flow[i];

    //cout << "v_true(" << clusterPoints->at(n).position.x << "," << clusterPoints->at(n).position.y << "," <<\
    // clusterPoints->at(n).position.z << "," << clusterPoints->at(n).position.t << ")  = [\t";
    //cout << clusterPoints->at(n).flow.x << ",\t" << clusterPoints->at(n).flow.y << ",\t" << clusterPoints->at(n).flow.z << "]\n";

    //cout << "v_model(" << clusterPoints->at(n).position.x << "," << clusterPoints->at(n).position.y << "," <<\
    // clusterPoints->at(n).position.z << "," << clusterPoints->at(n).position.t << ") = [\t";
    //cout << modeledFlow[0] << ",\t" << modeledFlow[1] << ",\t" << modeledFlow[2] << "]\n";

    //cout << "v_error(" << clusterPoints->at(n).position.x << "," << clusterPoints->at(n).position.y << "," <<\
    // clusterPoints->at(n).position.z << "," << clusterPoints->at(n).position.t << ") = [\t";
    //cout << flowDifference[0] << ",\t" << flowDifference[1] << ",\t" << flowDifference[2] << "]\n";

    //cout << "eucl^2 = " << (flowDifference[0]*flowDifference[0] + flowDifference[1]*flowDifference[1] + flowDifference[2]*flowDifference[2]) << "\n";

    modelError += (flowDifference[0]*flowDifference[0] + flowDifference[1]*flowDifference[1] + flowDifference[2]*flowDifference[2]);
  }
  //cout << modelError << endl;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeClusterNodeLinearModel::qfeCalculateModelError(qfeFloat v0[3], vector<qfeClusterPoint>* clusterPoints, float &modelError)
{
  qfeFloat flowDifference[3];

  modelError = 0;
  for(int n=0; n<(int)clusterPoints->size(); n++)
  {
    for(int i=0; i<3; i++)
      flowDifference[i] = v0[i] - clusterPoints->at(n).flow[i];

    modelError += (flowDifference[0]*flowDifference[0] + flowDifference[1]*flowDifference[1] + flowDifference[2]*flowDifference[2]);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeFloat qfeClusterNodeLinearModel::qfeComputeDistanceTo(qfeClusterNode *c2)
{
  qfeClusterNodeLinearModel *other = (qfeClusterNodeLinearModel*)c2;

  qfeFloat mergedX[4];
  qfeFloat mergedV[3];
  qfeFloat mergedXX[10];
  qfeFloat mergedXV[12];

  unsigned int mergedModelSize = this->modelSize + other->modelSize;

  for(int i=0; i<4; i++)  mergedX[i]  = this->X[i]  + other->X[i];
  for(int i=0; i<3; i++)  mergedV[i]  = this->V[i]  + other->V[i];
  for(int i=0; i<10; i++) mergedXX[i] = this->XX[i] + other->XX[i];
  for(int i=0; i<12; i++) mergedXV[i] = this->XV[i] + other->XV[i];

  qfeFloat A[12];
  qfeFloat v0[3];
  qfeFloat thisMergedModelError, otherMergedModelError;
  bool singular;

  qfeClusterNodeLinearModel::qfeEstimateLinearModel(mergedX, mergedV, mergedXX, mergedXV, mergedModelSize, A, v0, singular);

  if(singular)
  {
    qfeClusterNodeLinearModel::qfeCalculateModelError(v0, &this->clusterPoints, thisMergedModelError);
    qfeClusterNodeLinearModel::qfeCalculateModelError(v0, &other->clusterPoints, otherMergedModelError);
  }
  else
  {
    qfeClusterNodeLinearModel::qfeCalculateModelError(A, v0, &this->clusterPoints, thisMergedModelError);
    qfeClusterNodeLinearModel::qfeCalculateModelError(A, v0, &other->clusterPoints, otherMergedModelError);
  }

  qfeFloat clusterDistance = ((thisMergedModelError+otherMergedModelError)-(this->modelError+other->modelError));
  //  /(this->clusterPoints.size()+other->clusterPoints.size());

  // Make sure the cluster distance is positive; 
  // Negative cluster distance can interfere with the implicit cluster distance flags (-1 = unevaluated distance, -2 = invalid/merged cluster)
  if(clusterDistance<0) return 0;
  else                  return clusterDistance;
};