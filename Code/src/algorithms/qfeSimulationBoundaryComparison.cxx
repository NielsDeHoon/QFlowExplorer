#include "qfeSimulationBoundaryComparison.h"

//----------------------------------------------------------------------------
qfeSimulationBoundaryComparison::qfeSimulationBoundaryComparison()
{
  this->particlePositions.clear();  
  this->particleVelocitiesFirst.clear();
  this->particleVelocitiesSecond.clear();
  this->particleNormals;

  this->arrowsVisible              = true;
  this->angleFilter               = 90;
  this->colorType                 = qfeComparisonColorVelocity;

  this->currentTime               = 0;
  this->currentRenderFirst        = false;

  this->particlesCount            = 0;
  this->particleTransferFunction  = 0;

  this->light                     = NULL;

  this->arrowWidth                = 1.0;
  this->seedsCount                = 0;

  this->seriesFirst.clear();
  this->seriesSecond.clear();;

  this->paramParticleSize         = 2.0;
  this->paramCurrentPhase         = 0.0;

  this->firstRender               = true;

  this->shaderProgramRenderArrowSphere = new qfeGLShaderProgram();
  this->shaderProgramRenderArrowSphere->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderArrowSphere->qfeAddShaderFromFile("/shaders/fsr/fsr-render-arrow-sphere-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramRenderArrowSphere->qfeAddShaderFromFile("/shaders/fsr/fsr-render-arrow-sphere-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderArrowSphere->qfeAddShaderFromFile("/shaders/fsr/fsr-render-arrow-sphere-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderArrowSphere->qfeLink();

  this->shaderProgramRenderArrowBody = new qfeGLShaderProgram();
  this->shaderProgramRenderArrowBody->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderArrowBody->qfeAddShaderFromFile("/shaders/library/fetch.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderArrowBody->qfeAddShaderFromFile("/shaders/fsr/fsr-render-arrow-body-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramRenderArrowBody->qfeAddShaderFromFile("/shaders/fsr/fsr-render-arrow-body-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderArrowBody->qfeAddShaderFromFile("/shaders/fsr/fsr-render-arrow-body-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderArrowBody->qfeLink();
};

//----------------------------------------------------------------------------
qfeSimulationBoundaryComparison::qfeSimulationBoundaryComparison(const qfeSimulationBoundaryComparison &fv) : qfeAlgorithmFlow(fv)
{
  this->firstRender                      = fv.firstRender;
  this->arrowsVisible                    = fv.arrowsVisible;
  this->angleFilter                      = fv.angleFilter;
  this->colorType                        = fv.colorType;

  this->currentTime                      = fv.currentTime;
  this->currentRenderFirst               = fv.currentRenderFirst;

  this->light                            = fv.light;
  this->particlePositions                = fv.particlePositions;  
  this->particleVelocitiesFirst          = fv.particleVelocitiesFirst;  
  this->particleVelocitiesSecond         = fv.particleVelocitiesSecond;  
  this->particleNormals                  = fv.particleNormals;
  this->particlePositionsVBO             = fv.particlePositionsVBO;  
  this->particleVelocitiesFirstVBO       = fv.particleVelocitiesFirstVBO;
  this->particleVelocitiesSecondVBO      = fv.particleVelocitiesSecondVBO;
  this->particleNormalsVBO               = fv.particleNormalsVBO;

  this->locationPosition                 = fv.locationPosition; 
  this->locationVelocitiesFirst          = fv.locationVelocitiesFirst;
  this->locationVelocitiesSecond         = fv.locationVelocitiesSecond;
  this->locationNormals                  = fv.locationNormals;

  this->particlesCount                   = fv.particlesCount;
  this->particleTransferFunction         = fv.particleTransferFunction;

  this->seriesFirst                      = fv.seriesFirst;
  this->seriesSecond                     = fv.seriesSecond;

  this->paramParticleSize                = fv.paramParticleSize;
  this->paramCurrentPhase                = fv.paramCurrentPhase;
  this->paramFirstHighlight              = fv.paramFirstHighlight;
 
  this->shaderProgramRenderArrowSphere   = fv.shaderProgramRenderArrowSphere;
  this->shaderProgramRenderArrowBody     = fv.shaderProgramRenderArrowBody;
};

//----------------------------------------------------------------------------
qfeSimulationBoundaryComparison::~qfeSimulationBoundaryComparison()
{  
  delete this->shaderProgramRenderArrowSphere;
  delete this->shaderProgramRenderArrowBody;

  if(glIsTexture(this->particleTransferFunction))
    glDeleteTextures(1, &this->particleTransferFunction);
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeSetLightSource(qfeLightSource *light)
{
  this->light = light;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeSetVolumeSeries(qfeVolumeSeries first, qfeVolumeSeries second)
{
  this->seriesFirst  = first;
  this->seriesSecond = second;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeSetArrowsVisible(bool arrows)
{
  this->arrowsVisible = arrows;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeSetAngleFilter(int angle)
{
  this->angleFilter = angle;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeSetColorType(qfeComparisonColorType color)
{
  this->colorType = color;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeSetArrowWidth(qfeFloat width)
{
  this->arrowWidth = width;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeSetArrowFirstHighlight(bool highlight)
{
  this->paramFirstHighlight = highlight;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeAddSeedsFromModel(qfeModel *model, qfeFloat offset, qfeVolume *volume)
{
  qfeMatrix4f      V2P;
  qfeMeshVertices *vertices = NULL;
  qfeMeshVertices *normals  = NULL;
  vector< float >         seed;
  vector< vector<float> > seeds;
  vector< float >         velocities;
  qfeFloat                vl, vu, vmax;

  if(volume == NULL) return qfeError;

  // get the voxel to patient coordinate transformation
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);   
  

  // obtain the mesh vertices and normals
  model->qfeGetModelMesh(&vertices, &normals);

  // get the normals
  this->particleNormals.clear();
  for(int i=0; i<(int)vertices->size(); i++)
  {
    this->particleNormals.push_back(normals->at(i).x);   
    this->particleNormals.push_back(normals->at(i).y);   
    this->particleNormals.push_back(normals->at(i).z);   
  }

  seeds.clear();  
  //for(int i=0; i<(int)vertices->size(); i++)
  for(int i = 0; i < this->seedsCount; i++)
  {
    qfePoint p;
    qfeVector n;

    // get the point in voxel coordinates
    p.x = vertices->at(i).x;
    p.y = vertices->at(i).y;
    p.z = vertices->at(i).z;   
    n.x = normals->at(i).x;
    n.y = normals->at(i).y;
    n.z = normals->at(i).z;
    p   = p - offset*n;

    // store the point in patient coordinates
    p   = p * V2P;  
  
    seed.clear();
    seed.push_back(p.x);
    seed.push_back(p.y);
    seed.push_back(p.z);    

    seeds.push_back(seed);
  }

  this->qfeAddParticlePositions(seeds, volume);

  this->particlesCount = seeds.size();

  // get the first velocity vectors  
  seriesFirst.front()->qfeGetVolumeValueDomain(vl, vu);
  vmax = max(abs(vl),abs(vu));

  this->particleVelocitiesFirst.clear();
  for(int t=0; t<(int)seriesFirst.size(); t++)
  {
    velocities.clear();
    for(int i=0; i<(int)vertices->size(); i++)
    {
      qfePoint p;
      qfeVector n;
      qfeVector v;

      // get the point in voxel coordinates
      p.x = vertices->at(i).x;
      p.y = vertices->at(i).y;
      p.z = vertices->at(i).z;   
      n.x = normals->at(i).x;
      n.y = normals->at(i).y;
      n.z = normals->at(i).z;
      p   = p - offset*n;

      this->qfeFetchVelocityLinear(seriesFirst[t], p, v);

      velocities.push_back(v.x / vmax);
      velocities.push_back(v.y / vmax);
      velocities.push_back(v.z / vmax);
    }
    this->particleVelocitiesFirst.push_back(velocities);
  }

  // get the second velocity vectors  
  this->particleVelocitiesSecond.clear();
  for(int t=0; t<(int)seriesSecond.size(); t++)
  {
    velocities.clear();
    for(int i=0; i<(int)vertices->size(); i++)
    {
      qfePoint p;
      qfeVector n;
      qfeVector v;

      // get the point in voxel coordinates
      p.x = vertices->at(i).x;
      p.y = vertices->at(i).y;
      p.z = vertices->at(i).z;   
      n.x = normals->at(i).x;
      n.y = normals->at(i).y;
      n.z = normals->at(i).z;
      p   = p - offset*n;

      this->qfeFetchVelocityLinear(seriesSecond[t], p, v);

      velocities.push_back(v.x / vmax);
      velocities.push_back(v.y / vmax);
      velocities.push_back(v.z / vmax);
    }
    this->particleVelocitiesSecond.push_back(velocities);
  }  

  this->qfeInitBufferStatic(&this->particlePositions,       this->particlePositionsVBO);  
  
  if(this->particleVelocitiesFirst.size() > 0) 
    this->qfeInitBufferStatic(&this->particleVelocitiesFirst.front(), this->particleVelocitiesFirstVBO);  

  if(this->particleVelocitiesSecond.size() > 0) 
    this->qfeInitBufferStatic(&this->particleVelocitiesSecond.front(), this->particleVelocitiesSecondVBO);  
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeClearSeeds()
{
  this->particlePositions.clear();
  this->particlesCount = 0;

  this->qfeClearBufferStatic(this->particlePositionsVBO);  
  this->qfeClearBufferStatic(this->particleVelocitiesFirstVBO);
  this->qfeClearBufferStatic(this->particleVelocitiesSecondVBO);

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeRenderArrowsComparison(qfeColorMap *colormap, qfeFloat currentPhase)
{
  // First time of rendering, prepare some global variables
  if(this->firstRender)
  {
    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  // Update the current phase
  this->paramCurrentPhase = currentPhase;

  // Update the particle and line buffers
  if(this->currentTime != currentPhase)
  { 
    int currentTimeFirst  = 0;
    int currentTimeSecond = 0;

    float phaseDurationFirst;
    float phaseDurationSecond;
    int   phaseMultiplier = 1;

    if((this->seriesFirst.size()>0) && (this->seriesSecond.size()>0))
    {
      this->seriesFirst.front()->qfeGetVolumePhaseDuration(phaseDurationFirst);
      this->seriesSecond.front()->qfeGetVolumePhaseDuration(phaseDurationSecond);

      phaseMultiplier = (int)(phaseDurationFirst / phaseDurationSecond);
    }   

    this->currentTime = currentPhase;
    
    currentTimeFirst  = (int)floor(currentPhase);
    currentTimeSecond = (int)(currentPhase * phaseMultiplier);

    if(currentTimeSecond >= this->seriesSecond.size()) currentTimeSecond = this->seriesSecond.size() - 1;

    if((currentTimeFirst > 0) && (currentTimeFirst < (int)this->particleVelocitiesFirst.size()))        
      this->qfeInitBufferStatic(&this->particleVelocitiesFirst[currentTimeFirst], this->particleVelocitiesFirstVBO);        

    if((currentTimeSecond > 0) && (currentTimeSecond < (int)this->particleVelocitiesSecond.size()))        
      this->qfeInitBufferStatic(&this->particleVelocitiesSecond[currentTimeSecond], this->particleVelocitiesSecondVBO);          
  }

  // Get the color transferfunction
  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->particleTransferFunction);

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  // Obtain the modelview information
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
 	glPushMatrix();

  // Bind the textures
  this->qfeBindTextures();

  glPointSize( 5.0 );
  glColor3f(1.0f, 0.0f, 0.0f);

  this->qfeSetDynamicUniformLocations();

  //this->currentRenderFirst = true;
  this->qfeSetDynamicUniformLocations();

  if(this->arrowsVisible)
  {
    //this->qfeRenderSeeds(this->particlePositionsVBO, this->particleVelocitiesFirstVBO, this->particleVelocitiesSecondVBO, this->particlesCount);
    this->qfeRenderBody(this->particlePositionsVBO, this->particleVelocitiesFirstVBO, this->particleVelocitiesSecondVBO, this->particlesCount);
  }

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeRenderSeeds(GLuint particles, GLuint velocities1, GLuint velocities2, int count)
{  
  // sphere
  this->shaderProgramRenderArrowSphere->qfeGetAttribLocation("qfe_ParticlePosition", this->locationPosition);  
  this->shaderProgramRenderArrowSphere->qfeGetAttribLocation("qfe_ParticleVelocity1", this->locationVelocitiesFirst);  
  this->shaderProgramRenderArrowSphere->qfeGetAttribLocation("qfe_ParticleVelocity2", this->locationVelocitiesSecond);  

  this->shaderProgramRenderArrowSphere->qfeEnable();

  this->qfeDrawParticles(particles, count);

  this->shaderProgramRenderArrowSphere->qfeDisable();
  
  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeRenderBody(GLuint particles, GLuint velocities1, GLuint velocities2, int count)
{   
  // body
  this->shaderProgramRenderArrowBody->qfeGetAttribLocation("qfe_ParticlePosition", this->locationPosition);  
  this->shaderProgramRenderArrowBody->qfeGetAttribLocation("qfe_ParticleVelocity1", this->locationVelocitiesFirst);  
  this->shaderProgramRenderArrowBody->qfeGetAttribLocation("qfe_ParticleVelocity2", this->locationVelocitiesSecond);  

  this->shaderProgramRenderArrowBody->qfeEnable();

  this->qfeDrawParticles(particles, velocities1, velocities2, count);

  this->shaderProgramRenderArrowBody->qfeDisable();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeDrawParticles(GLuint particles, int count)
{
  if(!glIsBuffer(particles)) return qfeError;
  if(this->locationPosition == -1) return qfeError;

  glBindBuffer(GL_ARRAY_BUFFER, particles);
  glEnableVertexAttribArray(this->locationPosition);
  glVertexAttribPointer(this->locationPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glDrawArrays(GL_POINTS, 0, count);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDisableVertexAttribArray(this->locationPosition);  


  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeDrawParticles(GLuint particles, GLuint velocities1, GLuint velocities2, int count)
{
  if(!glIsBuffer(particles)) return qfeError;
  if(this->locationPosition == -1) return qfeError;

  glBindBuffer(GL_ARRAY_BUFFER, particles);
  glEnableVertexAttribArray(this->locationPosition);
  glVertexAttribPointer(this->locationPosition, 3, GL_FLOAT, GL_FALSE, 0, 0);

  if(glIsBuffer(velocities1))
  {
    glBindBuffer(GL_ARRAY_BUFFER, velocities1);
    glEnableVertexAttribArray(this->locationVelocitiesFirst);
    glVertexAttribPointer(this->locationVelocitiesFirst, 3, GL_FLOAT, GL_FALSE, 0, 0);
  }

  if(glIsBuffer(velocities2))
  {
    glBindBuffer(GL_ARRAY_BUFFER, velocities2);
    glEnableVertexAttribArray(this->locationVelocitiesSecond);
    glVertexAttribPointer(this->locationVelocitiesSecond, 3, GL_FLOAT, GL_FALSE, 0, 0);
  }

  glDrawArrays(GL_POINTS, 0, count);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDisableVertexAttribArray(this->locationPosition);  

  if(glIsBuffer(velocities1))
  {
    glDisableVertexAttribArray(this->locationVelocitiesFirst);  
  }

  if(glIsBuffer(velocities2))
  {
    glDisableVertexAttribArray(this->locationVelocitiesSecond);  
  }


  return qfeSuccess;
}

//----------------------------------------------------------------------------
// seeds should be provided in texture coordinates,
// and will be stored in patient coordinates
qfeReturnStatus qfeSimulationBoundaryComparison::qfeAddParticlePositions(vector< vector<float> > seeds, qfeVolume *volume)
{
  if((volume == NULL) || (int(seeds.size()) <= 0))
    return qfeError;

  int              currentTime;
  vector< float >  currentParticles;  
  qfeGrid         *grid;
  qfePoint         spacing;
  qfePoint         currentPosition;

  // we fix the particle visualization size based on the voxel size
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(spacing.x, spacing.y, spacing.z);

  this->paramParticleSize = 0.3 * min(min(spacing.x, spacing.y), spacing.z);

  // obtain and transform all particles
  this->particlePositions.clear();  
  currentParticles.clear();  
  currentTime = -1.0;

  for(int i=0; i<(int)seeds.size(); i++)
  {
    // we assume x, y, z in texture coordinates and time in phases
    if((int)seeds[i].size() != 3) continue;

    this->particlePositions.push_back(seeds[i][0]);
    this->particlePositions.push_back(seeds[i][1]);
    this->particlePositions.push_back(seeds[i][2]);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeFetchVelocityLinear(qfeVolume *vol, qfePoint p, qfeVector &v)
{
  unsigned int  dims[3];
  qfeValueType  type;
  void         *data;
  qfeFloat      xd, yd, zd;
  qfePoint      p1;
  qfeVector     i1, i2, i1l, i1r, i2l, i2r;
  qfeVector     j1, j2, j1l, j1r, j2l, j2r;
  qfeVector     w1, w2;
  int           index[2];

  // Fetch the volume data
  vol->qfeGetVolumeData(type, dims[0], dims[1], dims[2], &data);

  // Make sure the boundaries conditions are ok
  (p.x     > 0.0)            ? p1.x = p.x   : p1.x = 1;
  (p.y     > 0.0)            ? p1.y = p.y   : p1.y = 1;
  (p.z     > 0.0)            ? p1.z = p.z   : p1.z = 1;
  (p.x+1.0 < (float)dims[0]) ? p1.x = p1.x  : p1.x = (int)dims[0]-2;
  (p.y+1.0 < (float)dims[1]) ? p1.y = p1.y  : p1.y = (int)dims[1]-2;
  (p.z+1.0 < (float)dims[2]) ? p1.z = p1.z  : p1.z = (int)dims[2]-2;

  // Get the lattice point
  xd = p1.x - floor(p1.x);
  yd = p1.y - floor(p1.y);
  zd = p1.z - floor(p1.z);

  // Compute along z
  index[0] = floor(p1.x)  + (floor(p1.y) *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = floor(p1.x)  + (floor(p1.y) *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  i1l.x = *((float*)data + 3*index[0]+0);
  i1l.y = *((float*)data + 3*index[0]+1);
  i1l.z = *((float*)data + 3*index[0]+2);
  i1r.x = *((float*)data + 3*index[1]+0);
  i1r.y = *((float*)data + 3*index[1]+1);
  i1r.z = *((float*)data + 3*index[1]+2);
  i1.x = i1l.x*(1.0-zd)  + i1r.x*zd;
  i1.y = i1l.y*(1.0-zd)  + i1r.y*zd;
  i1.z = i1l.z*(1.0-zd)  + i1r.z*zd;

  index[0] = floor(p1.x)  + (ceil(p1.y)  *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = floor(p1.x)  + (ceil(p1.y)  *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  i2l.x = *((float*)data + 3*index[0]+0);
  i2l.y = *((float*)data + 3*index[0]+1);
  i2l.z = *((float*)data + 3*index[0]+2);
  i2r.x = *((float*)data + 3*index[1]+0);
  i2r.y = *((float*)data + 3*index[1]+1);
  i2r.z = *((float*)data + 3*index[1]+2);
  i2.x = i2l.x*(1.0-zd)  + i2r.x*zd;
  i2.y = i2l.y*(1.0-zd)  + i2r.y*zd;
  i2.z = i2l.z*(1.0-zd)  + i2r.z*zd;

  index[0] = ceil(p1.x)   + (floor(p1.y) *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = ceil(p1.x)   + (floor(p1.y) *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  j1l.x = *((float*)data + 3*index[0]+0);
  j1l.y = *((float*)data + 3*index[0]+1);
  j1l.z = *((float*)data + 3*index[0]+2);
  j1r.x = *((float*)data + 3*index[1]+0);
  j1r.y = *((float*)data + 3*index[1]+1);
  j1r.z = *((float*)data + 3*index[1]+2);
  j1.x = j1l.x*(1.0-zd)  + j1r.x*zd;
  j1.y = j1l.y*(1.0-zd)  + j1r.y*zd;
  j1.z = j1l.z*(1.0-zd)  + j1r.z*zd;

  index[0] = ceil(p1.x)   + (ceil(p1.y)  *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = ceil(p1.x)   + (ceil(p1.y)  *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  j2l.x = *((float*)data + 3*index[0]+0);
  j2l.y = *((float*)data + 3*index[0]+1);
  j2l.z = *((float*)data + 3*index[0]+2);
  j2r.x = *((float*)data + 3*index[1]+0);
  j2r.y = *((float*)data + 3*index[1]+1);
  j2r.z = *((float*)data + 3*index[1]+2);
  j2.x = j2l.x*(1.0-zd)  + j2r.x*zd;
  j2.y = j2l.y*(1.0-zd)  + j2r.y*zd;
  j2.z = j2l.z*(1.0-zd)  + j2r.z*zd;

  // compute along y
  w1.x = i1.x*(1.0-yd) + i2.x*yd;
  w1.y = i1.y*(1.0-yd) + i2.y*yd;
  w1.z = i1.z*(1.0-yd) + i2.z*yd;

  w2.x = j1.x*(1.0-yd) + j2.x*yd;
  w2.y = j1.y*(1.0-yd) + j2.y*yd;
  w2.z = j1.z*(1.0-yd) + j2.z*yd;

  // compute along z
  v.x = w1.x*(1.0-xd) + w2.x*xd;
  v.y = w1.y*(1.0-xd) + w2.y*xd;
  v.z = w1.z*(1.0-xd) + w2.z*xd;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// p in voxel coordinates
qfeReturnStatus qfeSimulationBoundaryComparison::qfeFetchVelocityLinearTemporalLinear(qfeVolumeSeries series, qfePoint p, qfeFloat t, qfeVector &v)
{
  float     ti, time;
  int       tl, tr;
  qfeVector vl, vr;

  tl = floor(t);
  tr = tl + 1;
  ti = t - float(tl);

  // Check the boundary conditions and make cyclic
  if(tl < 0) tl = (int)series.size() - abs(tl);
  tl = tl % series.size();
  if(tr < 0) tr = (int)series.size() - abs(tr);
  tr = tr % series.size();

  // Fetch the velocity vectors
  qfeFetchVelocityLinear(series[tl], p, vl);
  qfeFetchVelocityLinear(series[tr], p, vr);

  // Correct the current time
  time = tl + ti;

  // Compute the linear interpolation
  v.x = vl.x*(1.0-ti) + vr.x*ti;
  v.y = vl.y*(1.0-ti) + vr.y*ti;
  v.z = vl.z*(1.0-ti) + vr.z*ti;
  v.w = time;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeInitBufferStatic(vector<float> *vertices, GLuint &VBO)
{
  int dataSize;

  this->qfeClearBufferStatic(VBO);

  dataSize = vertices->size() * sizeof(GLfloat);

  glGenBuffers(1, &VBO);

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &((*vertices)[0]), GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeClearBufferStatic(GLuint &VBO)
{
  if(glIsBuffer(VBO))
  {
    glDeleteBuffers(1, &VBO);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeBindTextures()
{
  glActiveTexture(GL_TEXTURE8);
  glBindTexture(GL_TEXTURE_1D, this->particleTransferFunction);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeSetStaticUniformLocations()
{
  this->shaderProgramRenderArrowSphere->qfeSetUniform1i("particleTransferFunction", 8);  
  this->shaderProgramRenderArrowBody->qfeSetUniform1i("particleTransferFunction", 8);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationBoundaryComparison::qfeSetDynamicUniformLocations()
{
  qfePoint  lightSpec;
  qfeVector lightDir;
  qfeFloat  phaseCount;
  int       highlight;

  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 10.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w);
    this->light->qfeGetLightSourceDirection(lightDir);
  }

  if((int)this->seriesFirst.size() > 0)
  {
    phaseCount = (qfeFloat)this->seriesFirst.size();
  }
  else
  {
    phaseCount  = this->paramCurrentPhase;
  }

  highlight = false;
  if(this->currentRenderFirst)
    highlight =  this->paramFirstHighlight ? 1 : 0;

  this->shaderProgramRenderArrowSphere->qfeSetUniform4f("light",                                     lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderArrowSphere->qfeSetUniform4f("lightDirection",                            lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramRenderArrowSphere->qfeSetUniform1f("particleSize",                              this->paramParticleSize);
  this->shaderProgramRenderArrowSphere->qfeSetUniform1i("angleFilter",                               this->angleFilter);  
  this->shaderProgramRenderArrowSphere->qfeSetUniformMatrix4f("matrixModelView",                     1, this->matrixModelView);
  this->shaderProgramRenderArrowSphere->qfeSetUniformMatrix4f("matrixModelViewProjection",           1, this->matrixModelViewProjection);
  this->shaderProgramRenderArrowSphere->qfeSetUniformMatrix4f("matrixModelViewProjectionInverse",    1, this->matrixModelViewProjectionInverse);
  this->shaderProgramRenderArrowSphere->qfeSetUniformMatrix4f("matrixProjection",                    1, this->matrixProjection);

  this->shaderProgramRenderArrowBody->qfeSetUniform4f("light",                                       lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderArrowBody->qfeSetUniform4f("lightDirection",                              lightDir.x, lightDir.y, lightDir.z, lightDir.w);  
  this->shaderProgramRenderArrowBody->qfeSetUniform1i("angleFilter",                                 this->angleFilter);  
  this->shaderProgramRenderArrowBody->qfeSetUniform1f("lineWidth",                                   this->paramParticleSize);  
  this->shaderProgramRenderArrowBody->qfeSetUniform1f("arrowScale",                                  5.0);  
  this->shaderProgramRenderArrowBody->qfeSetUniformMatrix4f("matrixModelView",                       1, this->matrixModelView);
  this->shaderProgramRenderArrowBody->qfeSetUniformMatrix4f("matrixModelViewProjection",             1, this->matrixModelViewProjection);
  this->shaderProgramRenderArrowBody->qfeSetUniformMatrix4f("matrixModelViewProjectionInverse",      1, this->matrixModelViewProjectionInverse);
  this->shaderProgramRenderArrowBody->qfeSetUniformMatrix4f("matrixModelViewInverse",                1, this->matrixModelViewInverse);
  this->shaderProgramRenderArrowBody->qfeSetUniformMatrix4f("matrixProjection",                      1, this->matrixProjection);

  return qfeSuccess;
}

