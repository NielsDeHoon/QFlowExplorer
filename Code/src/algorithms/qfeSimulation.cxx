#include "qfeSimulation.h"

//----------------------------------------------------------------------------
qfeSimulation::qfeSimulation()
{
  this->firstRender       = true;

  this->mesh              = NULL;
  this->fluidsim          = NULL;

  dx = 0.1;

  time = 0;

  removeNearBoundary = true;
  /*
  std::fstream filestrTime;
  filestrTime.open ("resultTime.txt", fstream::in | ios::trunc);
  filestrTime.clear();  
  filestrTime.close();
  filestrTime.open ("resultTime.txt", std::fstream::app);  
  filestrTime<<"Time"<<std::endl;
  filestrTime<<"0,"<<std::endl;

  std::fstream filestrAngle;
  filestrAngle.open ("resultAngle.txt", fstream::in | ios::trunc);
  filestrAngle.clear();  
  filestrAngle.close();
  filestrAngle.open ("resultAngle.txt", std::fstream::app);  
  filestrAngle<<"Angle"<<std::endl;
  filestrAngle<<"0,"<<std::endl;

  std::fstream filestrAngleSigma;
  filestrAngleSigma.open ("resultAngleSigma.txt", fstream::in | ios::trunc);
  filestrAngleSigma.clear();  
  filestrAngleSigma.close();
  filestrAngleSigma.open ("resultAngleSigma.txt", std::fstream::app);  
  filestrAngleSigma<<"AngleSigma"<<std::endl;
  filestrAngleSigma<<"0,"<<std::endl;

  std::fstream filestrMagnitude;
  filestrMagnitude.open ("resultMagnitude.txt", fstream::in | ios::trunc);
  filestrMagnitude.clear();  
  filestrMagnitude.close();
  filestrMagnitude.open ("resultMagnitude.txt", std::fstream::app);  
  filestrMagnitude<<"Magnitude"<<std::endl;
  filestrMagnitude<<"0,"<<std::endl;

  std::fstream filestrMagnitudeSigma;
  filestrMagnitudeSigma.open ("resultMagnitudeSigma.txt", fstream::in | ios::trunc);
  filestrMagnitudeSigma.clear();  
  filestrMagnitudeSigma.close();
  filestrMagnitudeSigma.open ("resultMagnitudeSigma.txt", std::fstream::app);  
  filestrMagnitudeSigma<<"MagnitudeSigma"<<std::endl;
  filestrMagnitudeSigma<<"0,"<<std::endl;
  */
  measurementsWritten = 0;
  writingErrors = false;
  allErrorsWritten = false;

  negative_time_step = false;

  simulation_model_computed = false;
}

//----------------------------------------------------------------------------
qfeSimulation::qfeSimulation(const qfeSimulation &pr)
{
  this->firstRender                  = pr.firstRender;

  this->viewportSize[0]              = pr.viewportSize[0];
  this->viewportSize[1]              = pr.viewportSize[1];
};

//----------------------------------------------------------------------------
qfeSimulation::~qfeSimulation()
{
};


//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeSetViewportSize(unsigned int x, unsigned int y, int supersampling)
{
  this->viewportSize[0] = x;
  this->viewportSize[1] = y;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeGetParticlePositions(std::vector<std::vector<float>> &positions)
{
  if(this->fluidsim == NULL) 
	return qfeError;

  std::vector<Vec3f> simPos = this->fluidsim->getParticleLocations();

  float                            simTime = this->time;

  positions.clear();

  for(int i=0; i<(int)simPos.size(); i++)
  {
	std::vector<float> pos(4);
	pos[0] = simPos[i][0] / this->scaleSimX;    
	pos[1] = simPos[i][1] / this->scaleSimY;    
	pos[2] = simPos[i][2] / this->scaleSimZ;    
	pos[3] = simTime;    

	positions.push_back(pos);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeGetParticleVelocities(std::vector<std::vector<float>> &velocities)
{
  if(this->fluidsim == NULL) 
	return qfeError;

  velocities.clear();

  std::vector<Vec3f> velocs = this->fluidsim->getParticleVelocities();
  velocities.resize(velocs.size());
  
  for(unsigned int i = 0; i<velocs.size(); i++)
  {
	  Vec3f vel = velocs.at(i);

	  std::vector<float> velocity(3);
	  velocity[0] = vel[0];
	  velocity[1] = vel[1];
	  velocity[2] = vel[2];

	  velocities.at(i) = velocity;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeRenderBoundingBox(qfeVolume *volume)
{
  qfeMatrix4f   P2W, V2P, T2V, M;
  GLfloat       matrix[16];

  qfeTransform::qfeGetMatrixPatientToWorld(P2W);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  M = T2V*V2P*P2W;
  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();
  glMultMatrixf(&matrix[0]);  // Texture to World   Coordinates

  glBegin(GL_QUADS);

  // Front
  glNormal3f(0.0f, 0.0f, -1.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);

  // Back
  glNormal3f(0.0f, 0.0f, 1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f, 1.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f, 1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f,  1.0f, 1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f, 1.0f);

  // Left
  glNormal3f(-1.0f, 0.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f,  1.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);

  // Right
  glNormal3f(1.0f, 0.0f, 0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f(1.0f,  0.0f,  1.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f(1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f(1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f(1.0f,  1.0f,  1.0f);

  // Top
  glNormal3f(0.0f, -1.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f,  1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);

  // Bottom
  glNormal3f(0.0f, 1.0f, 0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f, 1.0f,  1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f, 1.0f,  1.0f);

  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
};

//----------------------------------------------------------------------------
void qfeSimulation::qfeUpdateSerie(unsigned int index)
{
	if(mesh == NULL)
	{
		cout<<"Load in a mesh and try loading this scene again"<<endl;
		return;
	}

	this->serie = index;
	
	//for synthetic data, disable this
	applyDifferenceMethod();

	return;
	//till here

	std::cout<<"REPLACING THE VELOCITY: FOR TESTING ONLY"<<std::endl;

	for(unsigned int p = 0; p<fluidsim->particles.numberOfParticles; p++)
	{
		Vec3f location = fluidsim->particles.x.at(p);
		Vec3f velocity = fluidsim->particles.u.at(p);
		
		float x=location[0];
		float y=location[1];
		float z=location[2];
		
		Vec3f measurement = interpolateMeasurement(x,y,z,this->serie);
		
		if(fluidsim->using_negative_time_step == true)
		{
			measurement *= -1.0f;
		}

		fluidsim->particles.u.at(p) = measurement;
	}

	/*
	//just use the distance
	double averageAngleError = 0.0;
	double averageSpeedError = 0.0;
	double maxVel = -1.0;

	int countAngle = 0;

	for(int p = 0; p<fluidsim->particles.numberOfParticles; p++)
	{
		Vec3f velM = this->interpolateMeasurement(fluidsim->particles.x.at(p)[0],
			fluidsim->particles.x.at(p)[1],
			fluidsim->particles.x.at(p)[2], 
			time);
		Vec3f velS(fluidsim->particles.u.at(p)[0],
			fluidsim->particles.u.at(p)[1],
			fluidsim->particles.u.at(p)[2]);

		float divX = velS[0] - velM[0];
		float divY = velS[1] - velM[1];
		float divZ = velS[2] - velM[2];


		//normalize:
		float lengthVelM = sqrt(sqr(velM[0])+sqr(velM[1])+sqr(velM[2]));
		float lengthVelS = sqrt(sqr(velS[0])+sqr(velS[1])+sqr(velS[2]));

		//add up the error in speed:
		averageSpeedError += fabs(lengthVelM - lengthVelS);

		Vec3f velMN = velM/lengthVelM;
		Vec3f velSN = velS/lengthVelS;

		if (maxVel < lengthVelM) maxVel = lengthVelM;

		//dot product to calculate the error in angle:
		double dot = velMN[0]*velSN[0] + velMN[1]*velSN[1] + velMN[2]*velSN[2];
		if(lengthVelM != 0 && lengthVelS != 0) 
		{
			averageAngleError -= 90*(dot-1.0); //negative value
			countAngle++;
		}

		fluidsim->particles.u.at(p)[0] = velM[0];
		fluidsim->particles.u.at(p)[1] = velM[1];
		fluidsim->particles.u.at(p)[2] = velM[2];
	}

	//take the average:
	averageAngleError /= countAngle;
	averageSpeedError /= fluidsim->particles.numberOfParticles;

	std::cout<<"Average angle error: "<<averageAngleError<<" of "<<(double)countAngle/fluidsim->particles.numberOfParticles<<std::endl;
	std::cout<<"Average speed error: "<<averageSpeedError<<"  of "<<maxVel<<std::endl;
	*/
}

//----------------------------------------------------------------------------
void qfeSimulation::qfeUpdateTime(double t)
{
	this->time = t;
	return;
}

//----------------------------------------------------------------------------
double qfeSimulation::qfeGetSimulationTime()
{
	return this->time;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeSetLightSource(qfeLightSource *light)
{
  this->light = light; 
  return qfeSuccess;
}

qfeReturnStatus qfeSimulation::qfeSetVelocityField(qfeVolumeSeries mVelocityField)
{
	velocityField = mVelocityField;
	return qfeSuccess;
}

//----------------------------------------------------------------------------
//returns the velocity at the coordinates (which should be in simulation coordinates)
Vec3f qfeSimulation::interpolateMeasurement(float x, float y, float z, double inPhase)
{
	if(mesh == NULL) 
	{
		cout<<"No mesh was defined"<<endl;
		Vec3f failed(0,0,0);
		return failed;
	}

	//if(ENRIGHT_EXPERIMENT)
	//	return EnrightData(x,y,z,inPhase);

	//! Seed in voxel coordinates
	//! Step size in mm
	//! Point in voxel coordinates

	//

	qfeVector vi1, vi2;
	qfePoint  point;

	qfeMatrix4f P2V, V2P, T2V, V2T;
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volumeMeasurement);
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volumeMeasurement);	
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volumeMeasurement);
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volumeMeasurement);

	//ensure that "input" is in texture coordinates so we can transform it to voxel coordinates
	Vec3f in(x,y,z);
	Vec3f textureC = simulationToTexture(in);
	qfePoint input(textureC[0],textureC[1],textureC[2]);

	//ensure that the requested point is in voxel format
	point = T2V * input;

	if(point.x!=point.x || point.y!=point.y || point.z!=point.z)
	{
		std::cout<<"NaN point"<<std::endl;
	}

	// Seed in voxel coordinates, velocity in voxel coordinates
	int dir = 1;
	if(this->negative_time_step)
		dir = -1;

	unsigned int next = ((int)inPhase + 1) % velocityField.size();

	if(this->negative_time_step)
	{
		if((int)inPhase<0)
		{
			inPhase = velocityField.size()-1+(inPhase - 1);
		}
	}
	while(inPhase >= velocityField.size())
		inPhase = inPhase - velocityField.size();

	velocityField.at((int) inPhase)->qfeGetVolumeVectorAtPositionLinear(point, vi1);

	if(inPhase == (int)inPhase)
		vi2 = vi1;
	else
		velocityField.at(next)->qfeGetVolumeVectorAtPositionLinear(point, vi2);

	if(vi1[0]!=vi1[0] || vi1[1]!=vi1[1] || vi1[2]!=vi1[2])
	{
		//std::cout<<"NaN measurement current phase"<<std::endl;
		vi1[0]=0; vi1[1]=0; vi1[2]=0;
	}

	if(vi2[0]!=vi2[0] || vi2[1]!=vi2[1] || vi2[2]!=vi2[2])
	{
		//std::cout<<"NaN measurement"<<std::endl;
		vi2[0]=0; vi2[1]=0; vi2[2]=0;
	}

	double amount = inPhase-(int)inPhase;
	qfeVector vi;
	vi[0] = (1-amount)*vi1[0]+(amount)*vi2[0];
	vi[1] = (1-amount)*vi1[1]+(amount)*vi2[1];
	vi[2] = (1-amount)*vi1[2]+(amount)*vi2[2];

	//if(fabs(vi[0])<1e-6) vi[0] = 0;
	//if(fabs(vi[1])<1e-6) vi[1] = 0;
	//if(fabs(vi[2])<1e-6) vi[2] = 0;

	//return Vec3f(vi[0],vi[1],vi[2]);

	//if(fabs(vi[0])>1e-3 && writingErrors)
	//	std::cout<<vi[0]<<" "<<amount<<" "<<vi1[0]<<" "<<vi2[0]<<std::endl;
	if(inPhase == (int)inPhase)
	{
		vi[0] = vi1[0];vi[1] = vi1[1];vi[2] = vi1[2];
	}
	//transform it to patient coordinates to get the right units
	vi = V2P * vi;

	//std::cout<<"Patient coords "<<vi.x<<" "<<vi.y<<" "<<vi.z<<std::endl;

	//vi is in patient coordinates (so in cm/s)
	float measurementWidth = nx * actualCellWidth; //number of cells times the cell width
	//measurement width ~ simulation width = measurementWidth ~ 1
	//so 1 cm in the measurements is equal to 1/measurementWidth  
	//scale the velocity back from patient coordinates to simulation coordinates:
	Vec3f result(vi[0]/measurementWidth, vi[1]/measurementWidth, vi[2]/measurementWidth);

	return result;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeSetVolume(qfeVolume *volume)
{
	volumeMeasurement = volume;
	return qfeSuccess;
}

//----------------------------------------------------------------------------
Vec3f qfeSimulation::interpolateSimulation(float x, float y, float z)
{
	float u,v,w;
	unsigned int i,j,k;
	float fx,fy,fz;

	this->fluidsim->grid.bary_x_centre(x,i,fx);
	this->fluidsim->grid.bary_y_centre(y,j,fy);
	this->fluidsim->grid.bary_z_centre(z,k,fz);

	u = this->fluidsim->grid.u.gridtrilerp(i,j,k,fx,fy,fz);
	v = this->fluidsim->grid.v.gridtrilerp(i,j,k,fx,fy,fz);
	w = this->fluidsim->grid.w.gridtrilerp(i,j,k,fx,fy,fz);

	if(this->fluidsim->grid.nodal_solid_phi.gridtrilerp(i,j,k,fx,fy,fz)<0)
		return Vec3f(0,0,0); //inside the mesh

	Vec3f result(u,v,w);
	return result;
}

//----------------------------------------------------------------------------
float qfeSimulation::interpolatePressure(float x, float y, float z)
{
	float u,v,w;
	unsigned int i,j,k;
	float fx,fy,fz;

	this->fluidsim->grid.bary_x_centre(x,i,fx);
	this->fluidsim->grid.bary_y_centre(y,j,fy);
	this->fluidsim->grid.bary_z_centre(z,k,fz);

	u = this->fluidsim->grid.u.gridtrilerp(i,j,k,fx,fy,fz);
	v = this->fluidsim->grid.v.gridtrilerp(i,j,k,fx,fy,fz);
	w = this->fluidsim->grid.w.gridtrilerp(i,j,k,fx,fy,fz);

	return this->fluidsim->grid.vel_pressure.gridtrilerp(i,j,k,fx,fy,fz);
}

//----------------------------------------------------------------------------
float qfeSimulation::interpolateSolidPhi(float x, float y, float z)
{
	unsigned int i,j,k;
	float fx,fy,fz;

	this->fluidsim->grid.bary_x_centre(x,i,fx);
	this->fluidsim->grid.bary_y_centre(y,j,fy);
	this->fluidsim->grid.bary_z_centre(z,k,fz);

	return this->fluidsim->grid.nodal_solid_phi.gridtrilerp(i,j,k,fx,fy,fz);
}

//----------------------------------------------------------------------------
//uses texture coordinates
Vec3f qfeSimulation::trilerpMeasurement(float x, float y, float z, unsigned int serie)
{


	unsigned int op,oq,or;
	unsigned int p,q,r;
	velocityField.at(serie)->qfeGetVolumeSize(op,oq,or);

	// get the right coordinates using our texture coordinates (x,y,z)
	p = floor(x*(float)op);
	q = floor(y*(float)oq);
	r = floor(z*(float)or);

	float fx = x*(float)op-p;
	float fy = y*(float)oq-q;
	float fz = z*(float)or-r;

	//trilerp
	/**
	   C011_______C111
		 /|		  / |
		/ |		 /  |
	C001--+---C101  |
	  |   |_____|___|C110
	  |  /C010  |  /
	  | /       | /
	  |/________|/
	C000      C100

	**/
	//get the vectors of the corners:
	qfeVector vi000, vi001, vi010, vi011;
	qfeVector vi100, vi101, vi110, vi111;
	velocityField.at(serie)->qfeGetVolumeVectorAtPosition(p,q,r,vi000);
	velocityField.at(serie)->qfeGetVolumeVectorAtPosition(p,q,r+1,vi001);
	velocityField.at(serie)->qfeGetVolumeVectorAtPosition(p,q+1,r,vi010);
	velocityField.at(serie)->qfeGetVolumeVectorAtPosition(p,q+1,r+1,vi011);
	velocityField.at(serie)->qfeGetVolumeVectorAtPosition(p+1,q,r,vi100);
	velocityField.at(serie)->qfeGetVolumeVectorAtPosition(p+1,q,r+1,vi101);
	velocityField.at(serie)->qfeGetVolumeVectorAtPosition(p+1,q+1,r,vi110);
	velocityField.at(serie)->qfeGetVolumeVectorAtPosition(p+1,q+1,r+1,vi111);

	Vec3f v000(vi000.x,vi000.y,vi000.z);
	Vec3f v001(vi001.x,vi001.y,vi001.z);
	Vec3f v010(vi010.x,vi010.y,vi010.z);
	Vec3f v011(vi011.x,vi011.y,vi011.z);
	Vec3f v100(vi100.x,vi100.y,vi100.z);
	Vec3f v101(vi101.x,vi101.y,vi101.z);
	Vec3f v110(vi110.x,vi110.y,vi110.z);
	Vec3f v111(vi111.x,vi111.y,vi111.z);

	//find intermediate  values of the corners of a square around the value we want the find
	Vec3f d00 = v000*(1-fx) + v010*fx;//(i,j,k) and (i,j+1,k)
	Vec3f d10 = v010*(1-fx) + v110*fx;//(i,j+1,k) and (i+1,j+1,k)
	Vec3f d01 = v001*(1-fx) + v101*fx;//(i,j,k+1) and (i+1,j,k+1)
	Vec3f d11 = v011*(1-fx) + v111*fx;//(i,j+1,k+1) and (i+1,j+1,k+1)
	//now we do not need the x dimension anymore, do bilerp on y using the corners above to get a line:
	Vec3f e0 = d00*(1-fy)+d10*fy;
	Vec3f e1 = d01*(1-fy)+d11*fy;
	//now we only need liniear interpolation on the line defined above:
	Vec3f result = e0*(1-fz)+e1*fz;
	//scale it to texture coordinates:
	result[0]/=(float)op;
	result[1]/=(float)oq;
	result[2]/=(float)or;

	return result;
}


//----------------------------------------------------------------------------
//PARAMETER SETTINGS
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeSetNearBoundary(bool remove)
{
  this->removeNearBoundary = remove;

  //std::cout<<"qfeSetNearBoundary shouldn't be required anymore"<<std::endl;
  return qfeSuccess;  
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeSetFractionOfParticles(double fraction)
{
	this->fractionOfParticlesToRender = fraction;
	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeSetTailLength( unsigned int tailLength)
{
	this->tailLength = tailLength;
	return qfeSuccess;
}

//----------------------------------------------------------------------------
//FLUID SIMULATION INTERFACE
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
void qfeSimulation::qfeAdvanceOneFrame(double frameTime)
{
	//Note that frameTime can be negative if we simulate backwards

	//set fluid source velocity fields
	Array3Vec3 sourceVeloc1(fluidsim->grid.ni,fluidsim->grid.nj,fluidsim->grid.nk);
	Array3Vec3 sourceVeloc2(fluidsim->grid.ni,fluidsim->grid.nj,fluidsim->grid.nk);

	//std::cout<<"Set the source velocities"<<std::endl;
	for(unsigned int k = 0; k<fluidsim->grid.nk; k++) for(unsigned int j = 0; j<fluidsim->grid.nj; j++)	for(unsigned int i = 0; i<fluidsim->grid.ni; i++)
	{
		Vec3f textureCoords((float)i/nx, (float)j/ny, (float)k/nz);

		Vec3f simulationCoords = this->textureToSimulation(textureCoords);

		Vec3f curFrame = this->interpolateMeasurement(simulationCoords[0], simulationCoords[0], simulationCoords[0],time);
		Vec3f nextFrame = this->interpolateMeasurement(simulationCoords[0], simulationCoords[0], simulationCoords[0],time+frameTime);

		if(negative_time_step)
		{
			curFrame[0]  *= -1; curFrame[1]  *= -1; curFrame[2]  *= -1;
			nextFrame[0] *= -1; nextFrame[1] *= -1; nextFrame[2] *= -1;
		}

		sourceVeloc1(i,j,k) = sourceVeloc1.init_vector(curFrame[0],curFrame[1],curFrame[2]);
		sourceVeloc2(i,j,k) = sourceVeloc1.init_vector(nextFrame[0],nextFrame[1],nextFrame[2]);
	}

	fluidsim->setSourceVelocityFieldCurrentFrame(sourceVeloc1);
	fluidsim->setSourceVelocityFieldNextFrame(sourceVeloc2);

	//phaseDuration is the time between measurements in ms, here we need it in seconds to match the measurement velocity
	//std::cout<<frameTime*(phaseDuration/1000.0)<<" seconds"<<std::endl;
	this->fluidsim->advance_one_frame(frameTime*(phaseDuration/1000.0));
	if(noise>0) 
		this->fluidsimNoise->advance_one_frame(frameTime*(phaseDuration/1000.0));
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeInitFluidSimulation(unsigned int _nx, qfeVolume *volume, vector< qfeModel  * > modelSeries, qfeStudy *_study)
{
	if(_study == NULL)
		return qfeError;

	if((int)modelSeries.size() <= 0 || volume == NULL)
		return qfeError;

	this->study = _study;

	volume->qfeGetVolumeSize(volumeDimX, volumeDimY, volumeDimZ);
	//set phase duration (needed to get the actual time):
	volume->qfeGetVolumePhaseDuration(phaseDuration); 

	preprocessMesh(modelSeries,volume);

	//get voxel spacing
	qfeGrid      *grid;
	qfeVector     spacing;
	
	volume->qfeGetVolumeGrid(&grid);

	grid->qfeGetGridExtent(spacing.x,spacing.y,spacing.z);
	std::cout<<"volume size (voxels): "<<volumeDimX<<" x "<<volumeDimY<<" x "<<volumeDimZ<<endl;

	//spacing is in mm we need cm
	//measurement width
	double measWidth = volumeDimX*spacing.x/10.0;//total width of the measurements in cm
	double measHeight = volumeDimY*spacing.y/10.0;//total height of the measurements in cm
	double measDepth = volumeDimZ*spacing.z/10.0;//total depth of the measurements in cm
	std::cout<<"Size of the measurements in cm: "<<measWidth<<"x"<<measHeight<<"x"<<measDepth<<std::endl;
	actualCellWidth = measWidth/(float)_nx;
		
	lx = measWidth;

	dx = lx/(float)(_nx);
		
	nx = _nx;
	ny = ceil(measHeight/dx);
	nz = ceil(measDepth/dx);

	serie = 0;

	std::cout<<nx<<"  "<<ny<<" "<<nz<<"  "<<lx<<std::endl;
	this->fluidsim = new FLIP_simulation(nx,ny,nz,lx,BLOOD_DENSITY);
	this->fluidsim->setFillHolesAutomatically(true);
	this->fluidsim->setViscosity(0.0f);//BLOOD_VISCOSITY);
		
	this->qfeSetNearBoundary(true);

	if(noise>0)
	{
		this->fluidsimNoise = new FLIP_simulation(nx,ny,nz,lx,BLOOD_DENSITY);
		this->fluidsimNoise->setFillHolesAutomatically(true);
		this->fluidsimNoise->setViscosity(BLOOD_VISCOSITY);
	}

	Vec3f force;
	force[0] = 0.0;
	force[1] = 0.0;
	force[2] = 0.0;
	fluidsim->setBodyForce(force);

	if(noise>0)
		fluidsimNoise->setBodyForce(force);
	
	fluidsim->firstRun = true;

	if(noise>0)
		fluidsimNoise->firstRun = true;

	cout<<"Grid size is set to: "<<nx<<" x "<<ny<<" x "<<nz<<" with cell dimensions: "<<actualCellWidth<<"cm"<<endl;
	numberOfParticlesInitialized.resize(nx,ny,nz);

  if(this->mesh != NULL)
	  buildMesh(volume);
  else
  {
	cout << "qfeSimulation::qfeInitFluidSimulation - Please load the bounding geometry" << endl;
	return qfeError;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeNoiseErrors()
{
	if(time>7) 
		return qfeSuccess;

	float scale = 2;
		
	int count = 0;
	float angleTot = 0;
	float magnTot = 0;
	float magnTotDev = 0;
	float angleTotDev = 0;

	for(int k = 0; k<nz*scale; k++) for(int j = 0; j<ny*scale; j++) for(int i = 0; i<nx*scale; i++)
	{
		float x = (float)i/(nx*scale);
		float y = (float)j/(ny*scale);
		float z = (float)k/(nz*scale);
		//should be sim coordinates
					
		if(interpolateSolidPhi(x,y,z)>0)
		{
			count ++;

			float noiseX, noiseY, noiseZ;
			fluidsimNoise->grid.trilerp_uvw(x,y,z, noiseX,noiseY,noiseZ);
						
			float origX, origY, origZ;
			fluidsim->grid.trilerp_uvw(x,y,z, origX,origY,origZ);

			float sim = sqrt(sqr(origX)+sqr(origY)+sqr(origZ));
			float simNoise = sqrt(sqr(noiseX)+sqr(noiseY)+sqr(noiseZ));
			if(sim==0)
			{
				count --;
				continue;
			}
			magnTot += 1-(simNoise/sim);

			float dot = origX/sim * noiseX/simNoise + origY/sim * noiseY/simNoise + origZ/sim * noiseZ/simNoise;							
			if(dot>1) dot = 1;
			if(dot<0) dot = 0;
				angleTot += acos(dot)*180.0/M_PI;
		}
	}

	std::cout<<time<<" -> "<<magnTot/(float)count<<" "<<angleTot/(float)count<<std::endl;
	float averageAbsoluteMagnitude = magnTot/(float)count;
	float averageAngle = angleTot/(float)count;
		
	for(int k = 0; k<nz*scale; k++) for(int j = 0; j<ny*scale; j++) for(int i = 0; i<nx*scale; i++)
	{
		float x = (float)i/(nx*scale);
		float y = (float)j/(ny*scale);
		float z = (float)k/(nz*scale);
		//should be sim coordinates
					
		if(interpolateSolidPhi(x,y,z)>0)
		{
			float noiseX, noiseY, noiseZ;
			fluidsimNoise->grid.trilerp_uvw(x,y,z, noiseX,noiseY,noiseZ);
						
			float origX, origY, origZ;
			fluidsim->grid.trilerp_uvw(x,y,z, origX,origY,origZ);

			float sim = sqrt(sqr(origX)+sqr(origY)+sqr(origZ));
			float simNoise = sqrt(sqr(noiseX)+sqr(noiseY)+sqr(noiseZ));

			if(sim==0)
			{
				continue;
			}

			magnTotDev += sqr(1-(simNoise/sim)-averageAbsoluteMagnitude);

			float dot = origX/sim * noiseX/simNoise + origY/sim * noiseY/simNoise + origZ/sim * noiseZ/simNoise;							
			if (dot>1) dot = 1;
			if (dot<0) dot = 0;
			angleTotDev += sqr(acos(dot)*180.0/M_PI-averageAngle);
		}
	}

	float absoluteMagnitudeDeviation = sqrt(magnTotDev/(float)count);
	float angleDeviation = sqrt(angleTotDev/(float)count);

	std::fstream filestrTime;
	filestrTime.open ("resultTime.txt", std::fstream::app);
	filestrTime<<time<<",";
	filestrTime.close();
		
	std::fstream filestrAngle;
	filestrAngle.open ("resultAngle.txt", std::fstream::app);
	filestrAngle<<averageAngle<<",";
	filestrAngle.close();

	std::fstream filestrAngleSigma;
	filestrAngleSigma.open ("resultAngleSigma.txt", std::fstream::app);
	filestrAngleSigma<<angleDeviation<<",";
	filestrAngleSigma.close();

	std::fstream filestrMagnitude;
	filestrMagnitude.open ("resultMagnitude.txt", std::fstream::app);	
	filestrMagnitude<<averageAbsoluteMagnitude<<",";
	filestrMagnitude.close();

	std::fstream filestrMagnitudeSigma;
	filestrMagnitudeSigma.open ("resultMagnitudeSigma.txt", std::fstream::app);
	filestrMagnitudeSigma<<absoluteMagnitudeDeviation<<",";
	filestrMagnitudeSigma.close();

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeInterpolationErrors()
{
	if(time>7) 
		return qfeSuccess;

	if(time==(int)time && time != 0)
	{
		MACGrid t0 = MacGridsInnerSteps.at(0);
		MACGrid t1 = fluidsim->grid;

		int n = (int)MacGridsInnerSteps.size();
		float scale = 2;

		for(int ind = 0; ind<n; ind++)
		{
			MACGrid tv = MacGridsInnerSteps.at(ind);

			int count = 0;
			float angleTot = 0;
			float magnTot = 0;
			float magnTotDev = 0;
			float angleTotDev = 0;

			float weight = (float)ind/n;
			if(ind!=0)
			{
				for(int k = 0; k<nz*scale; k++) for(int j = 0; j<ny*scale; j++) for(int i = 0; i<nx*scale; i++)
				{
					float x = (float)i/(nx*scale);
					float y = (float)j/(ny*scale);
					float z = (float)k/(nz*scale);
					//should be sim coordinates
					
					if(interpolateSolidPhi(x,y,z)>0)
					{
						count ++;

						float t0x, t0y, t0z;
						t0.trilerp_uvw(x,y,z, t0x,t0y,t0z);
						
						float t1x, t1y, t1z;
						t1.trilerp_uvw(x,y,z, t1x,t1y,t1z);

						float tvx, tvy, tvz;
						tv.trilerp_uvw(x,y,z, tvx,tvy,tvz);

						//interpolated:
						float tix = (1-weight)*t0x + weight*t1x; 
						float tiy = (1-weight)*t0y + weight*t1y; 
						float tiz = (1-weight)*t0z + weight*t1z;

						float sim = sqrt(sqr(tvx)+sqr(tvy)+sqr(tvz));
						float interp = sqrt(sqr(tix)+sqr(tiy)+sqr(tiz));
						magnTot += 1-(interp/sim);

						float dot = tvx/sim * tix/interp + tvy/sim * tiy/interp + tvz/sim * tiz/interp;							
						if(dot>1) dot = 1;
						if(dot<0) dot = 0;
						angleTot += acos(dot)*180.0/M_PI;
					}
				}
			}
			else
			{
				count = 1;
			}

			std::cout<<time-1+(float)ind/n<<" -> "<<magnTot/(float)count<<std::endl;
			float averageAbsoluteMagnitude = magnTot/(float)count;
			float averageAngle = angleTot/(float)count;

			if(ind!=0)
			{
				for(int k = 0; k<nz*scale; k++) for(int j = 0; j<ny*scale; j++) for(int i = 0; i<nx*scale; i++)
				{
					float x = (float)i/(nx*scale);
					float y = (float)j/(ny*scale);
					float z = (float)k/(nz*scale);
					//should be sim coordinates
					
					if(interpolateSolidPhi(x,y,z)>0)
					{
						float t0x, t0y, t0z;
						t0.trilerp_uvw(x,y,z, t0x,t0y,t0z);
						
						float t1x, t1y, t1z;
						t1.trilerp_uvw(x,y,z, t1x,t1y,t1z);

						float tvx, tvy, tvz;
						tv.trilerp_uvw(x,y,z, tvx,tvy,tvz);

						//interpolated:
						float tix = (1-weight)*t0x + weight*t1x; 
						float tiy = (1-weight)*t0y + weight*t1y; 
						float tiz = (1-weight)*t0z + weight*t1z;

						float sim = sqrt(sqr(tvx)+sqr(tvy)+sqr(tvz));
						float interp = sqrt(sqr(tix)+sqr(tiy)+sqr(tiz));
						magnTotDev += sqr(1-(interp/sim)-averageAbsoluteMagnitude);

						float dot = tvx/sim * tix/interp + tvy/sim * tiy/interp + tvz/sim * tiz/interp;	
						if (dot>1) dot = 1;
						if (dot<0) dot = 0;
						angleTotDev += sqr(acos(dot)*180.0/M_PI-averageAngle);
					}
				}
			}
			else
			{
				count = 1;
			}

			float absoluteMagnitudeDeviation = sqrt(magnTotDev/(float)count);
			float angleDeviation = sqrt(angleTotDev/(float)count);

			std::fstream filestrTime;
			filestrTime.open ("resultTime.txt", std::fstream::app);
			filestrTime<<time-1+(float)ind/n<<",";
			filestrTime.close();
			
			std::fstream filestrAngle;
			filestrAngle.open ("resultAngle.txt", std::fstream::app);
			filestrAngle<<averageAngle<<",";
			filestrAngle.close();

			std::fstream filestrAngleSigma;
			filestrAngleSigma.open ("resultAngleSigma.txt", std::fstream::app);
			filestrAngleSigma<<angleDeviation<<",";
			filestrAngleSigma.close();

			std::fstream filestrMagnitude;
			filestrMagnitude.open ("resultMagnitude.txt", std::fstream::app);
			filestrMagnitude<<averageAbsoluteMagnitude<<",";
			filestrMagnitude.close();

			std::fstream filestrMagnitudeSigma;
			filestrMagnitudeSigma.open ("resultMagnitudeSigma.txt", std::fstream::app);
			filestrMagnitudeSigma<<absoluteMagnitudeDeviation<<",";
			filestrMagnitudeSigma.close();
		}


		MacGridsInnerSteps.clear();
	}
	MacGridsInnerSteps.push_back(fluidsim->grid);
	//std::cout<<"GRIDS: "<<MacGridsInnerSteps.size()<<std::endl;

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeWriteErrors()
{
	if(allErrorsWritten)
		return qfeSuccess;
	writingErrors = true;
	std::ostringstream strs;
	strs << time/1000.0;
	std::string filenameDot = "differenceAngle-"+strs.str()+".vti";
	std::string filenameMagn = "differenceMagn-"+strs.str()+".vti";

	int scale = 1;
	 
	vtkSmartPointer<vtkImageData> imageDataDot =
		vtkSmartPointer<vtkImageData>::New();
	imageDataDot->SetDimensions(nz*scale,ny*scale,nz*scale);
	#if VTK_MAJOR_VERSION <= 5
	  imageDataDot->SetNumberOfScalarComponents(1);	  
	  imageDataDot->SetScalarTypeToDouble();
	#else
	  imageDataDot->AllocateScalars(VTK_DOUBLE, 1);
	#endif

	vtkSmartPointer<vtkImageData> imageDataMagn =
		vtkSmartPointer<vtkImageData>::New();
	imageDataMagn->SetDimensions(nz*scale,ny*scale,nz*scale);
	#if VTK_MAJOR_VERSION <= 5
	  imageDataMagn->SetNumberOfScalarComponents(1);	  
	  imageDataMagn->SetScalarTypeToDouble();
	#else
	  imageDataMagn->AllocateScalars(VTK_DOUBLE, 1);
	#endif
		 
	int* dims = imageDataDot->GetDimensions();	

	float totalAngle = 0;
	float totalAbsoluteMagnitude = 0;

	float averageAngle = 0;
	float averageAbsoluteMagnitude = 0;

	float totalAngleDeviation = 0;
	float totalAbsoluteMagnitudeDeviation = 0;

	float angleDeviation = 0;
	float absoluteMagnitudeDeviation = 0;

	int countAngle = 0;
	int countMagnitude = 0;

	float maxMVel = 0.0;

	std::cout<<"Time: "<<time<<std::endl;
	if (time > velocityField.size()-1)
	{
		writingErrors = false;
		allErrorsWritten = true;
		return qfeSuccess;
	}

	/*
	for(int k = 0; k<dims[2]; k++)
	{
		for(int j = 0; j<dims[1]; j++)
		{			
			for(int i = 0; i<dims[0]; i++)
			{
				float x = (float)i/dims[0];
				float y = (float)j/dims[1];
				float z = (float)k/dims[2];

				float magnError = 0;
				float angleError = 0;

				Vec3f texC(x,y,z);
				Vec3f simC = textureToSimulation(texC);

				Vec3f mVel = interpolateMeasurement(simC[0],simC[1],simC[2],time);
				Vec3f sVel = interpolateSimulation(simC[0],simC[1],simC[2]);

				float lengthM = sqrt(mVel[0]*mVel[0]+mVel[1]*mVel[1]+mVel[2]*mVel[2]);
				float lengthS = sqrt(sVel[0]*sVel[0]+sVel[1]*sVel[1]+sVel[2]*sVel[2]);

				if (lengthM<lengthS) magnError = 1.0-lengthM/lengthS;
				if (lengthM>=lengthS) magnError = 1.0-lengthS/lengthM;

				float dot = 0;
				if (lengthM == 0 || lengthS == 0)
					dot = 1;
				else
					dot = sVel[0]/lengthS * mVel[0]/lengthM + sVel[1]/lengthS * mVel[1]/lengthM + sVel[2]/lengthS * mVel[2]/lengthM;			
				
				if(dot>1) dot = 1;
				if(dot<0) dot = 0;
				angleError = acos(dot)*180.0/M_PI;

				//if something went wrong or isn't defined we set the error value to 0
				if (angleError<=0) angleError = 0;
				if (magnError<=0) magnError = 0;

				if(interpolateSolidPhi(simC[0],simC[1],simC[2])<=0)
				{
					angleError = -180;
					magnError = -1;
				}
				else
				{
					countAngle ++;
					countMagnitude ++;
					totalAngle += angleError;
					//std::cout<<fabs(lengthM)<<" "<<fabs(lengthS)<<" "<<fabs(lengthM-lengthS)<<std::endl;
					if(lengthM>1e-6 && lengthS>1e-6) //measurement should be zero, however due to normalization somewhere in the code it cannot be stored as 0...
					{
						totalAbsoluteMagnitude += 1.0-lengthS/lengthM;
					}
				}

				//to get correct scaling:
				if ( i==0 && j==0 && k==0 )
				{
					angleError = 180;
					magnError = 1;
				}

				double* pixelDot = static_cast<double*>(imageDataDot->GetScalarPointer(i,j,k));
				pixelDot[0] = angleError;
				
				double* pixelMagn = static_cast<double*>(imageDataMagn->GetScalarPointer(i,j,k));
				pixelMagn[0] = magnError;
			}
		}
	}
	//std::cout<<"Max velocity: "<<maxMVel<<std::endl;

	averageAngle = totalAngle/countAngle;
	averageAbsoluteMagnitude = totalAbsoluteMagnitude/countMagnitude;
	std::cout<<"Avg velocity error: "<<averageAbsoluteMagnitude<<std::endl;

	for(int k = 0; k<dims[2]; k++)
	{
		for(int j = 0; j<dims[1]; j++)
		{			
			for(int i = 0; i<dims[0]; i++)
			{
				float x = (float)i/dims[0];
				float y = (float)j/dims[1];
				float z = (float)k/dims[2];

				float magnError = 0;
				float angleError = 0;

				Vec3f texC(x,y,z);
				Vec3f simC = textureToSimulation(texC);

				Vec3f mVel = interpolateMeasurement(simC[0],simC[1],simC[2],time);
				Vec3f sVel = interpolateSimulation(simC[0],simC[1],simC[2]);

				float lengthM = sqrt(mVel[0]*mVel[0]+mVel[1]*mVel[1]+mVel[2]*mVel[2]);
				float lengthS = sqrt(sVel[0]*sVel[0]+sVel[1]*sVel[1]+sVel[2]*sVel[2]);

				if (lengthM<lengthS) magnError = 1.0-lengthM/lengthS;
				if (lengthM>lengthS) magnError = 1.0-lengthS/lengthM;

				float dot = 0;
				if (lengthM == 0 || lengthS == 0)
					dot = 1;
				else
					dot = sVel[0]/lengthS * mVel[0]/lengthM + sVel[1]/lengthS * mVel[1]/lengthM + sVel[2]/lengthS * mVel[2]/lengthM;			
				
				if(dot>1) dot = 1;
				if(dot<0) dot = 0;
				angleError = acos(dot)*180.0/M_PI;

				//if something went wrong or isn't defined we set the error value to 0
				if (angleError<=0) angleError = 0;
				if (magnError<=0) magnError = 0;

				if(interpolateSolidPhi(simC[0],simC[1],simC[2])<=0)
				{
					angleError = -180;
					magnError = -1;
				}
				else
				{
					if(i!=0 && j!=0 && k!=0)
					{					
						totalAngleDeviation += sqr(angleError - averageAngle);
						totalAbsoluteMagnitudeDeviation += sqr(1.0-lengthS/lengthM-averageAbsoluteMagnitude);
					}
				}
			}
		}
	}
	*/

	for(unsigned int p = 0; p<fluidsim->particles.numberOfParticles; p++)
	{
		Vec3f sLocat = fluidsim->particles.x.at(p);
		Vec3f sVeloc = fluidsim->particles.u.at(p);
		
		float x = sLocat[0];
		float y = sLocat[1];
		float z = sLocat[2];
		
		float magnError = 0;
		float angleError = 0;

		Vec3f simC(x,y,z);
		Vec3f sVel(sVeloc[0],sVeloc[1],sVeloc[2]);
		
		Vec3f mVel = interpolateMeasurement(simC[0],simC[1],simC[2],time);

		float lengthM = sqrt(mVel[0]*mVel[0]+mVel[1]*mVel[1]+mVel[2]*mVel[2]);
		float lengthS = sqrt(sVel[0]*sVel[0]+sVel[1]*sVel[1]+sVel[2]*sVel[2]);

		if (lengthM<lengthS) magnError = 1.0-lengthM/lengthS;
		if (lengthM>=lengthS) magnError = 1.0-lengthS/lengthM;

		float dot = 0;
		if (lengthM == 0 || lengthS == 0)
			dot = 1;
		else
			dot = sVel[0]/lengthS * mVel[0]/lengthM + sVel[1]/lengthS * mVel[1]/lengthM + sVel[2]/lengthS * mVel[2]/lengthM;
		
		if(dot>1) dot = 1;
		if(dot<0) dot = 0;
		angleError = acos(dot)*180.0/M_PI;
		
		//if something went wrong or isn't defined we set the error value to 0
		if (angleError<=0) angleError = 0;
		if (magnError<=0) magnError = 0;
		
		if(interpolateSolidPhi(simC[0],simC[1],simC[2])<=0)
		{
			angleError = -180;
			magnError = -1;
		}
		else
		{
			countAngle ++;
			countMagnitude ++;
			totalAngle += angleError;
			
			//std::cout<<fabs(lengthM)<<" "<<fabs(lengthS)<<" "<<fabs(lengthM-lengthS)<<std::endl;
			if(lengthM>1e-6)
				totalAbsoluteMagnitude += 1.0-lengthS/lengthM;
		}
	}
	
	averageAngle = totalAngle/countAngle;
	averageAbsoluteMagnitude = totalAbsoluteMagnitude/countMagnitude;
	std::cout<<"Avg velocity error: "<<averageAbsoluteMagnitude*100<<"%"<<std::endl;

	for(unsigned int p = 0; p<fluidsim->particles.numberOfParticles; p++)
	{
		Vec3f sLocat = fluidsim->particles.x.at(p);
		Vec3f sVeloc = fluidsim->particles.u.at(p);
		
		float x = sLocat[0];
		float y = sLocat[1];
		float z = sLocat[2];
		
		float magnError = 0;
		float angleError = 0;

		Vec3f simC(x,y,z);
		Vec3f sVel(sVeloc[0],sVeloc[1],sVeloc[2]);
		
		Vec3f mVel = interpolateMeasurement(simC[0],simC[1],simC[2],time);

		float lengthM = sqrt(mVel[0]*mVel[0]+mVel[1]*mVel[1]+mVel[2]*mVel[2]);
		float lengthS = sqrt(sVel[0]*sVel[0]+sVel[1]*sVel[1]+sVel[2]*sVel[2]);

		if (lengthM<lengthS) magnError = 1.0-lengthM/lengthS;
		if (lengthM>=lengthS) magnError = 1.0-lengthS/lengthM;

		float dot = 0;
		if (lengthM == 0 || lengthS == 0)
			dot = 1;
		else
			dot = sVel[0]/lengthS * mVel[0]/lengthM + sVel[1]/lengthS * mVel[1]/lengthM + sVel[2]/lengthS * mVel[2]/lengthM;
		
		if(dot>1) dot = 1;
		if(dot<0) dot = 0;
		angleError = acos(dot)*180.0/M_PI;
		
		//if something went wrong or isn't defined we set the error value to 0
		if (angleError<=0) angleError = 0;
		if (magnError<=0) magnError = 0;
		
		if(interpolateSolidPhi(simC[0],simC[1],simC[2])<=0)
		{
			angleError = -180;
			magnError = -1;
		}
		else
		{
			totalAngleDeviation += sqr(angleError - averageAngle);
			if(lengthM>1e-6)
				totalAbsoluteMagnitudeDeviation += sqr(1.0-lengthS/lengthM-averageAbsoluteMagnitude);
		}
	}

	angleDeviation = sqrt(totalAngleDeviation/countAngle);
	absoluteMagnitudeDeviation = sqrt(totalAbsoluteMagnitudeDeviation/countMagnitude);

	/*
	if(time==0)
	{
		time = -1;
		averageAngle = -1;
		angleDeviation = -1;
		averageAbsoluteMagnitude = -1;
		absoluteMagnitudeDeviation = -1;
	}
	*/

	std::fstream filestrTime;
	filestrTime.open ("resultTime.txt", std::fstream::app);
	filestrTime<<time<<",";
	filestrTime.close();
	
	std::fstream filestrAngle;
	filestrAngle.open ("resultAngle.txt", std::fstream::app);
	filestrAngle<<averageAngle<<",";
	filestrAngle.close();

	std::fstream filestrAngleSigma;
	filestrAngleSigma.open ("resultAngleSigma.txt", std::fstream::app);
	filestrAngleSigma<<angleDeviation<<",";
	filestrAngleSigma.close();

	std::fstream filestrMagnitude;
	filestrMagnitude.open ("resultMagnitude.txt", std::fstream::app);
	filestrMagnitude<<averageAbsoluteMagnitude<<",";
	filestrMagnitude.close();

	std::fstream filestrMagnitudeSigma;
	filestrMagnitudeSigma.open ("resultMagnitudeSigma.txt", std::fstream::app);
	filestrMagnitudeSigma<<absoluteMagnitudeDeviation<<",";
	filestrMagnitudeSigma.close();

	//no differences needed:
	/*
	vtkSmartPointer<vtkXMLImageDataWriter> writerDot =
		vtkSmartPointer<vtkXMLImageDataWriter>::New();
	writerDot->SetFileName(filenameDot.c_str());
	#if VTK_MAJOR_VERSION <= 5
	  writerDot->SetInputConnection(imageDataDot->GetProducerPort());
	#else
	  writerDot->SetInputData(imageDataDot);
	#endif
	writerDot->Write();

	vtkSmartPointer<vtkXMLImageDataWriter> writerMagn =
		vtkSmartPointer<vtkXMLImageDataWriter>::New();
	writerMagn->SetFileName(filenameMagn.c_str());
	#if VTK_MAJOR_VERSION <= 5
	  writerMagn->SetInputConnection(imageDataMagn->GetProducerPort());
	#else
	  writerMagn->SetInputData(imageDataMagn);
	#endif
	writerMagn->Write();
	*/

	writingErrors = false;
	return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeRenderParticles(qfeVolume *volume)
{
	if(volume == NULL) return qfeError;

	if(this->fractionOfParticlesToRender==0.0) return qfeSuccess; //we do not have to render any particles

	qfeMatrix4f   P2W, V2P, T2V, M;
	GLfloat       matrixSim[16];

	qfeTransform::qfeGetMatrixPatientToWorld(P2W);
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

	// OpenGL works with column-major matrices
	// OpenGL works with left-multiplication
	M = T2V*V2P*P2W;
	qfeMatrix4f::qfeGetMatrixElements(M, &matrixSim[0]);

	//set up some variables to ensure we can draw just a fraction of the particles:
	std::vector<Vec3f> locations = this->fluidsim->getParticleLocations();
	std::vector<Vec3f> velocities = this->fluidsim->getParticleVelocities();
	int total = (int) locations.size();
	int stepsize = (float)total/((float)total*this->fractionOfParticlesToRender);
	int i;

	glPushMatrix();
		glLoadIdentity();
		glMultMatrixf(&matrixSim[0]);  // Texture to World   Coordinates

		glEnable(GL_LINE_SMOOTH);
		glLineWidth(2.0f);

		glPointSize( 5.0 );
		glColor3f(1.0f, 1.0f, 1.0f);
		
		if(locations.size()==0)
		{
			if(fluidsim->firstRun) 
			{
				cout<<"No fluid defined"<<endl;
				fluidsim->firstRun = false;
				if(noise>0)
					fluidsimNoise->firstRun = false;
			}
		}
		i = 0;
		
		/*
		int scale = 2;
		for(int k = 0; k<(fluidsim->grid.phi.nk-1)*scale; k++)
		{
			for(int j = 0; j<(fluidsim->grid.phi.nj-1)*scale; j++)
			{
				for(int i = 0; i<(fluidsim->grid.phi.ni-1)*scale; i++)
				{
					float x = (float)i/((fluidsim->grid.phi.ni-1)*scale)*scaleSimX;
					float y = (float)j/((fluidsim->grid.phi.nj-1)*scale)*scaleSimY;
					float z = (float)k/((fluidsim->grid.phi.nk-1)*scale)*scaleSimZ;
					int ix = i/scale;
					int jx = j/scale;
					int kx = k/scale;

					float fx = x-ix;
					float fy = y-jx;
					float fz = z-kx;

					float value = fluidsim->grid.phi.trilerp(ix,jx,kx,fx,fy,fz);
					if(value<=0) continue;

					if(value<0) glColor3f(0.0f, 1.0f, 1.0f);
					if(value>0) glColor3f(0.0f, 0.0f, 1.0f);
					glPointSize(fabs(value));
					glBegin(GL_POINTS);
						glVertex3f(x/scaleSimX,y/scaleSimY,z/scaleSimZ);
					glEnd();
					glPointSize(5.0);
				}
			}
		}
		*/
		/*
		float maxMagn = 0;
		while (i<total)
		{
			double x = locations.at(i)[0];
			double y = locations.at(i)[1];
			double z = locations.at(i)[2];

			//calculate the error
			float dotError = 0;
			float magnError = 0;

			Vec3f sVel(velocities.at(i)[0],velocities.at(i)[1],velocities.at(i)[2]);
			Vec3f mVel = interpolateMeasurement(x,y,z,time);
				
			float lengthM = sqrt(mVel[0]*mVel[0]+mVel[1]*mVel[1]+mVel[2]*mVel[2]);
			float lengthS = sqrt(sVel[0]*sVel[0]+sVel[1]*sVel[1]+sVel[2]*sVel[2]);

			if (lengthM!=0 && lengthS !=0) 
			{
				magnError = 1.0-fabs(lengthM/lengthS);
				float dot = sVel[0]/lengthS * mVel[0]/lengthM + sVel[1]/lengthS * mVel[1]/lengthM + sVel[2]/lengthS * mVel[2]/lengthM;
				
				if(dot>1) dot = 1;			
				if(dot<0) dot = 0;
				dotError = acos(dot)*180.0/M_PI;
				if(maxMagn<fabs(magnError))
					maxMagn = fabs(magnError);
			}
			i = i+stepsize;
		}
		*/

		i = 0;
		while (i<total)
		{
			double x = locations.at(i)[0];
			double y = locations.at(i)[1];
			double z = locations.at(i)[2];

			/*
			//calculate the error
			float dotError = 0;
			float magnError = 0;

			Vec3f sVel(velocities.at(i)[0],velocities.at(i)[1],velocities.at(i)[2]);
			Vec3f mVel = qfeInterpolateMeasurements(x,y,z,time);

			float lengthM = sqrt(mVel[0]*mVel[0]+mVel[1]*mVel[1]+mVel[2]*mVel[2]);
			float lengthS = sqrt(sVel[0]*sVel[0]+sVel[1]*sVel[1]+sVel[2]*sVel[2]);

			float color = 1.0;
			if (lengthM!=0 && lengthS !=0) 
			{
				magnError = 1.0-fabs(lengthM/lengthS);
				float dot = sVel[0]/lengthS * mVel[0]/lengthM + sVel[1]/lengthS * mVel[1]/lengthM + sVel[2]/lengthS * mVel[2]/lengthM;
				
				if(dot>1) dot = 1;			
				if(dot<0) dot = 0;
				dotError = acos(dot)*180.0/M_PI;

				color = fabs(magnError)/maxMagn;
			}

			glColor4f(1.0,color,0,0.5+color/2);
			*/
			//glColor3f(magnError, 0.0f, dotError);
			//glColor3f(1.0,0.4,0.4);
			glColor3f(0.5,0.8,0.9);

			/*
			glBegin(GL_POINTS);
				glVertex3f(x/scaleSimX,y/scaleSimY,z/scaleSimZ);
			glEnd();
			*/
			
			float velocityScale = 1.0/25.0;
			
			/*
			glLineWidth(1.0f);
			//render particle velocity
			double vx = velocities.at(i)[0]*velocityScale;
			double vy = velocities.at(i)[1]*velocityScale;
			double vz = velocities.at(i)[2]*velocityScale;
			
			glBegin(GL_LINES);
				glColor3f(0.5,0.8,0.9);
				glVertex3f(x/scaleSimX,y/scaleSimY,z/scaleSimZ);
				glColor3f(0.0f, 1.0f, 0.0f);
				glVertex3f(x/scaleSimX-vx/scaleSimX,y/scaleSimY-vy/scaleSimY,z/scaleSimZ-vz/scaleSimZ);
			glEnd();
			*/
			
			/*
			//measurement particle velocity
			Vec3f meas = interpolateMeasurement(locations.at(i)[0],locations.at(i)[1],locations.at(i)[2],time);

			vx = meas[0]*velocityScale/10.0;
			vy = meas[1]*velocityScale/10.0;
			vz = meas[2]*velocityScale/10.0;

			glColor3f(1.0f, 0.0f, 1.0f);
			glBegin(GL_LINES);
				glVertex3f(x,y,z);
				glVertex3f(x+vx/scaleSimX,y+vy/scaleSimY,z+vz/scaleSimZ);
			glEnd();
			glLineWidth(1.0f);
			*/
			i = i+stepsize;
		}
		
		glPointSize(5.0);

		//draw traces:
		/*
		i = 0;
		while (i<traces.size())
		{
			glLineWidth(1.5);
			QList<Vec3f> lines = this->traces.at(i);
			glBegin(GL_LINE_STRIP);
				for(int line = 0; line<lines.size(); line++)
				{
					if(line>tailLength) break;
					Vec3f point = lines.at(line);
					float col = (float)(line+1)/lines.size();
					glColor4f(col, col, col,col);
					glVertex3f(point[0]/scaleSimX,point[1]/scaleSimY,point[2]/scaleSimZ);
				}				
			glEnd();
			i = i+stepsize;
		}
		*/
		


		/*
		bool negative = false;
		bool positive = false;

		for(int k = 0; k<nz; k++)
		{
			for(int j = 0; j<ny; j++)
			{
				for(int i = 0; i<nx; i++)
				{
					glPointSize(fabs(10.0*fluidsim->grid.nodal_solid_phi(i,j,k)));
					//if (fluidsim->grid.nodal_solid_phi(i,j,k)<0) continue;

					float phi = fluidsim->grid.nodal_solid_phi(i,j,k);

					if (phi>0) 
					{
						glColor3f(0,0,1);
						positive = true;
						continue;
					}
					if (phi<0) 
					{
						glColor3f(0,1,0);
						negative = true;
						continue;
					}
					if (phi == 0)
					{
						glColor3f(1,0,0);
						continue;
					}

					glBegin(GL_POINTS);
						float x = (float) i/nx;
						float y = (float) j/ny;
						float z = (float) k/nz;
						glVertex3f(x,y,z);
					glEnd();
				}
			}
		}
		*/

		
		//draw interpolation of the simulation:
		
		i = 0;
		while (i<total)
		{

			double x = locations.at(i)[0];
			double y = locations.at(i)[1];
			double z = locations.at(i)[2];

			Vec3f simC(x,y,z);

			glBegin(GL_LINE_STRIP);
				int steps = 10;
				for(int line = 0; line<steps; line++)
				{				
					float t = (float)line/steps;
					glColor3f(0,0,t);
					glVertex3f(x/scaleSimX,y/scaleSimY,z/scaleSimZ);

					float dtTime = phaseDuration/(1000.0*steps);

					Vec3f sVel = interpolateSimulation(x,y,z);
					float xmid = x + 0.5*dtTime*sVel[0];
					float ymid = y + 0.5*dtTime*sVel[1];
					float zmid = z + 0.5*dtTime*sVel[2];

					sVel = interpolateSimulation(xmid,ymid,zmid);

					x = x + dtTime*sVel[0];
					y = y + dtTime*sVel[1];
					z = z + dtTime*sVel[2];

					//glVertex(x,y,z);
				}				
			glEnd();
			i = i+stepsize;
		}		
		/*

		//draw interpolation of the measurements:	
		
		i = 0;
		while (i<total)
		{
			double x = locations.at(i)[0];
			double y = locations.at(i)[1];
			double z = locations.at(i)[2];

			Vec3f simC(x,y,z);

			glBegin(GL_LINE_STRIP);
				int steps = 10;
				for(int line = 0; line<steps; line++)
				{				
					float t = (float)line/steps;
					glColor3f(t,0,0);
					glVertex3f(x/scaleSimX,y/scaleSimY,z/scaleSimZ);

					float dtTime = phaseDuration/(1000.0*steps);

					Vec3f sVel = interpolateMeasurement(x*scaleSimX,y*scaleSimY,z*scaleSimZ,time);
					float xmid = x + 0.5*dtTime*sVel[0];
					float ymid = y + 0.5*dtTime*sVel[1];
					float zmid = z + 0.5*dtTime*sVel[2];

					sVel = interpolateMeasurement(xmid,ymid,zmid,time);

					x = x + dtTime*sVel[0];
					y = y + dtTime*sVel[1];
					z = z + dtTime*sVel[2];

					//glVertex(x,y,z);
				}				
			glEnd();
			i = i+stepsize;
		}*/
			

		/*
		//render the error
		
		double accumError = 0;
		int errorCount = 0;

		i = 0;
		while (i<total)
		{		
			//if the particle is not newly generated we draw the error
			if(traces.at(i).size()<=1) {i = i+stepsize; continue;}

			vector<float> locat = locations.at(i);
			vector<float> veloc = velocities.at(i);

			Vec3f vec1(veloc[0],veloc[1],veloc[2]);
			Vec3f vec2 = this->interpolateMeasurement(locat[0],locat[1],locat[2],this->time);

			//normalize both:
			float length1 = sqrt(sqr(vec1[0])+sqr(vec1[1])+sqr(vec1[2]));
			float length2 = sqrt(sqr(vec2[0])+sqr(vec2[1])+sqr(vec2[2]));

			if(length1<1e-5) {i = i+stepsize; continue;}
			if(length2<1e-5) {i = i+stepsize; continue;}

			vec1 /= length1;
			vec2 /= length2;

			double dot = vec1[0]*vec2[0]+vec1[1]*vec2[1]+vec1[2]*vec2[2];

			float errorDotSize = 12.0;
			glPointSize(errorDotSize);
					
			glBegin(GL_POINTS);
				glColor3f(1.0,1.0,1.0);
				glVertex3f(locat[0],locat[1],locat[2]);
			glEnd();

			//clamp dot to get the right input for acos due to rounding errors
			if(dot>1.0) dot = 1.0;
			if(dot<-1.0) dot = -1.0;

			//if the error is to small we do not render it
			//this is due to the fact that a point size of 0 effectively is a point size of 1
			float errorSize = acos(dot)/M_PI;
			if(errorSize>1e-3)
			{
				glPointSize(errorSize*errorDotSize);

				accumError+=abs(acos(dot)/M_PI);
				errorCount++;
						
				glBegin(GL_POINTS);
					glColor3f(0.0,1.0,0.0);
					glVertex3f(locat[0],locat[1],locat[2]);
				glEnd();
			}

			glPointSize(5.0);

			i = i+stepsize;
		}

		if(errorCount>0) 
		{
			//cout<<"Accumulated error: "<<accumError/(float)errorCount *100.0<<"%"<<endl;
		}
		*/

	glPopMatrix();
	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeRenderEnergy(qfeVolume *volume)
{
	if(volume == NULL) return qfeError;

	qfeMatrix4f   P2W, V2P, T2V, M;
	GLfloat       matrixSim[16];

	qfeTransform::qfeGetMatrixPatientToWorld(P2W);
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

	// OpenGL works with column-major matrices
	// OpenGL works with left-multiplication
	M = T2V*V2P*P2W;
	qfeMatrix4f::qfeGetMatrixElements(M, &matrixSim[0]);

	glPushMatrix();
		glLoadIdentity();
		glMultMatrixf(&matrixSim[0]);  // Texture to World   Coordinates
		//energy = 1/2*m*v^2

		int details = 4;

		float maxP = 0.0;

		for(int k = 1; k<(int)(details*nz-details); k++)
		{
			for(int j = 1; j<(int)(details*ny-details); j++)
			{
				for(int i = 1; i<(int)(details*nx-details); i++)
				{
					float x = (float)i/((float)details*nx);
					float y = (float)j/((float)details*ny);
					float z = (float)k/((float)details*nz);

					unsigned int oi, oj, ok;
					float fx, fy, fz;

					oi = x*(float)nx;
					oj = y*(float)ny;
					ok = z*(float)nz;

					fx = x-x*(float)nx;
					fy = y-y*(float)ny;
					fz = z-z*(float)nz;

					float p = fluidsim->grid.vel_pressure.gridtrilerp(oi,oj,ok,fx,fy,fz);		
	
					unsigned int ip,jp,kp;
					float fxp,fyp,fzp;

					fluidsim->grid.bary_x_centre(x*scaleSimX,ip,fxp);
					fluidsim->grid.bary_y_centre(y*scaleSimY,jp,fyp);
					fluidsim->grid.bary_z_centre(z*scaleSimZ,kp,fzp);
					
					if(fluidsim->grid.nodal_solid_phi.gridtrilerp(ip,jp,kp,fxp,fyp,fzp)<0)
						continue;

					if(fabs(p)>maxP)
						maxP = fabs(p);
				}
			}
		}

		//cut away below:
		float thresshold = 0.1;

		for(int k = 1; k<(int)(details*nz-details); k++)
		{
			for(int j = 1; j<(int)(details*ny-details); j++)
			{
				for(int i = 1; i<(int)(details*nx-details); i++)
				{
					float x = (float)i/((float)details*nx);
					float y = (float)j/((float)details*ny);
					float z = (float)k/((float)details*nz);

					int oi, oj, ok;
					float fx, fy, fz;

					oi = x*(float)nx;
					oj = y*(float)ny;
					ok = z*(float)nz;

					fx = x-floor(x*(float)nx);
					fy = y-floor(y*(float)ny);
					fz = z-floor(z*(float)nz);

					float pressure = fluidsim->grid.vel_pressure.gridtrilerp(oi,oj,ok,fx,fy,fz);
					
					unsigned int ip,jp,kp;
					float fxp,fyp,fzp;

					fluidsim->grid.bary_x_centre(x*scaleSimX,ip,fxp);
					fluidsim->grid.bary_y_centre(y*scaleSimY,jp,fyp);
					fluidsim->grid.bary_z_centre(z*scaleSimZ,kp,fzp);
					
					if(fluidsim->grid.nodal_solid_phi.gridtrilerp(ip,jp,kp,fxp,fyp,fzp)<0)
						continue;

					if(fabs(pressure)>0 && fabs(pressure)/maxP>thresshold)
					{
						glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
						float amount = fabs(pressure)/maxP;
						glColor4f(1,(1+thresshold)*amount-thresshold,0,1);
						glBegin(GL_POINTS);
							glVertex3f(x,y,z);	
						glEnd();
					}

					/*
					float u = fluidsim->grid.u.trilerp(oi,oj,ok,fx,fy,fz);
					float v = fluidsim->grid.v.trilerp(oi,oj,ok,fx,fy,fz);
					float w = fluidsim->grid.w.trilerp(oi,oj,ok,fx,fy,fz);

					//use the measurement
					//Vec3f veloc = this->interpolateMeasurement(x,y,z);
					//float m = sqrt(sqr(veloc[0])+sqr(veloc[1])+sqr(veloc[2]));
					//m*=10.0;

					//calculate the magnitude
					float m = sqrt(sqr(u)+sqr(v)+sqr(w));

					m /= 200.0; //scale it down a bit, since the maximum speed is 200cm/s we scale it with a factor 200

					float max = std::max(abs(u),std::max(abs(v),abs(w)));
					//render a dot with the magnitude as size:
					if (m*fluidsim->grid.particleCells(oi,oj,ok) <1e-2) continue;
					glPointSize( 2.0*m*fluidsim->grid.particleCells(oi,oj,ok) );
					glColor3f(abs(u)/max, abs(v)/max, abs(w)/max);
					glBegin(GL_POINTS);
						glVertex3f(x+0.5/(float)nx,y+0.5/(float)ny,z+0.5/(float)nz);
					glEnd();
					glPointSize( 5.0 );
					*/
				}
			}
		}
		//rendering
	glPopMatrix();
	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeRenderGrid(qfeVolume *volume)
{
	if(volume == NULL) return qfeError;

	qfeMatrix4f   P2W, V2P, T2V, M;
	GLfloat       matrixSim[16];

	qfeTransform::qfeGetMatrixPatientToWorld(P2W);
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

	// OpenGL works with column-major matrices
	// OpenGL works with left-multiplication
	M = T2V*V2P*P2W;
	qfeMatrix4f::qfeGetMatrixElements(M, &matrixSim[0]);

	glPushMatrix();

		glLoadIdentity();
		glMultMatrixf(&matrixSim[0]);  // Texture to World   Coordinates
		glColor4f(0.0f, 1.0f, 0.0f,0.05f);
		glBegin(GL_LINES);			
			for(int i = 0; i<(int)nx+1; i++)
			{
				for(int j = 0; j<(int)ny+1; j++)
				{
					float ix = (float)i/nx;
					float jx = (float)j/ny;
					glVertex3f(ix,jx,0.0);
					glVertex3f(ix,jx,1.0);
				}
			}
			for(int i = 0; i<(int)nx+1; i++)
			{
				for(int k = 0; k<(int)nz+1; k++)
				{
					float ix = (float)i/nx;
					float kx = (float)k/nz;
					glVertex3f(ix,0.0,kx);
					glVertex3f(ix,1.0,kx);
				}
			}
			for(int j = 0; j<(int)ny+1; j++)
			{
				for(int k = 0; k<(int)nz+1; k++)
				{
					float jx = (float)j/ny;
					float kx = (float)k/nz;
					glVertex3f(0.0,jx,kx);
					glVertex3f(1.0,jx,kx);
				}
			}
		glEnd();

		glColor4f(1.0f, 1.0f, 0.0f,0.5f);


	glPopMatrix();

	return qfeSuccess;
}

//----------------------------------------------------------------------------
void qfeSimulation::qfeSeedParticles()
{
	if(mesh == NULL)
	{
		cout<<"Load in a mesh and try loading this scene again"<<endl;
		return;
	}
	//first run check:

	bool firstRun = fluidsim->particles.numberOfParticles==0;
	
	if(firstRun)
		AverageVelocity();

	for(int k = 1; k<(int)nz-1; k++)
	{
		for (int j = 1; j<(int)ny-1; j++)
		{
			for(int i = 1; i<(int)nx-1; i++)
			{
				//determine the number of particles we have to add to the current cell
				int amountToAdd = fluidsim->getInitialParticlesPerCell();
				if (!firstRun)
				{
					if(numberOfParticlesInitialized(i,j,k)<fluidsim->grid.particleCells(i,j,k)) 
						continue;

					amountToAdd = numberOfParticlesInitialized(i,j,k)-fluidsim->grid.particleCells(i,j,k);
				}

				if ( amountToAdd <= 0 ) continue;

				for(int n = 0; n<amountToAdd; n++)//generate n particles per cell
				{
					float x=(float)i+(float)rand()/RAND_MAX;
					float y=(float)j+(float)rand()/RAND_MAX;
					float z=(float)k+(float)rand()/RAND_MAX;

					if(fluidsim->grid.nodal_solid_phi.interpolate_value(x,y,z)<=0.01) //inside solid, so we should skip this particle
					{
						continue;
					}

					//indices to texture coordinates
					x/=(float)nx;
					y/=(float)ny;
					z/=(float)nz;

					Vec3f texCoord(x,y,z);
					Vec3f simCoord = textureToSimulation(texCoord);

					Vec3f location(3);
					location[0] = simCoord[0];
					location[1] = simCoord[1];
					location[2] = simCoord[2];

					Vec3f velocity(3);
					
					velocity[0] = 0;
					velocity[1] = 0;
					velocity[2] = 0;

					if(firstRun) //initialize it with the measurements velocity
					{
						Vec3f veloc = interpolateMeasurement(simCoord[0],simCoord[1],simCoord[2],time);

						velocity[0] = veloc[0];
						velocity[1] = veloc[1];
						velocity[2] = veloc[2];
					}

					fluidsim->setParticle(location,velocity);

					if(noise>0)
					{
						Vec3f velocNoise(3);
						velocNoise[0] = velocity[0]+GaussianNoise(noise);
						velocNoise[1] = velocity[1]+GaussianNoise(noise);
						velocNoise[2] = velocity[2]+GaussianNoise(noise);

						fluidsimNoise->setParticle(location,velocNoise);
					}
				}
			}
		}
	}
	//std::cout<<"Particles initialized"<<std::endl;

	//store the initial number of particles per cell:
	fluidsim->particles.transfer_to_grid();
	if(noise>0)
		fluidsimNoise->particles.transfer_to_grid();
	//std::cout<<"Transfered to grid"<<std::endl;

	if(firstRun)
	{
		for(int i = 1; i<(int)nx-1; i++)
		{
			for (int j = 1; j<(int)ny-1; j++)
			{
				for(int k = 1; k<(int)nz-1; k++)
				{

					numberOfParticlesInitialized(i,j,k) = fluidsim->grid.particleCells(i,j,k);
				}
			}
		}
		//this->qfeMakeIncompressible(1);
	}		

	std::cout<<"Number of particles: "<<fluidsim->particles.numberOfParticles<<std::endl;
	if(noise>0)
		std::cout<<"Number of noise particles: "<<fluidsimNoise->particles.numberOfParticles<<std::endl;

	if(ENRIGHT_EXPERIMENT)
	{
		std::cout<<std::endl<<"ENRIGHT EXPERIMENT ENABLED"<<std::endl<<"-------------------------------"<<std::endl<<std::endl;
	}
}

void qfeSimulation::setMeasurementVelocityField(double measurementTime)
{
	//std::cout<<"Set measurement velocity field"<<std::endl;
	time = measurementTime;

	//std::cout<<"  Set the particle velocities"<<std::endl;
	for(unsigned int p = 0; p<fluidsim->particles.numberOfParticles; p++)
	{
		Vec3f x = fluidsim->particles.x.at(p);
		Vec3f u = interpolateMeasurement(x[0],x[1],x[2],time);

		fluidsim->particles.u.at(p) = u;		
	}

	int dir = 1;
	if(negative_time_step)
		dir = -1;

	//std::cout<<"  Make incompressible"<<std::endl;
	this->qfeMakeIncompressible(dir);
	//std::cout<<"Set measurement velocity field done"<<std::endl;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::preprocessMesh(vector< qfeModel  * > modelSeries, qfeVolume *volume)
{
	if (modelSeries.size()<1) 
	{
		cout<<"Number of models found: "<<modelSeries.size()<<endl;
		cout<<"Please load in a mesh"<<endl;
		return qfeError; //no model => no boundary conditions
	}
	
	if (modelSeries.size() == 1)
	{
		qfeModel *model;
		model = modelSeries.at(0);
		model->qfeGetModelMesh(&this->mesh);
		mesh->BuildCells();
		mesh->BuildLinks(0);
		mesh->Update();

		this->mesh->BuildCells();
		this->mesh->BuildLinks(0);
		this->mesh->Update();
		//std::cout << "Input mesh has " << mesh->GetNumberOfPoints() << " vertices." << std::endl;
		//std::cout << "Input mesh has " << mesh->GetNumberOfCells() << " polygons." << std::endl;

		//clean data, to remove duplicate points and cells:
		vtkSmartPointer<vtkCleanPolyData> pointCleaner =
			vtkSmartPointer<vtkCleanPolyData>::New();
		pointCleaner->SetInputConnection (this->mesh->GetProducerPort());
		pointCleaner->ConvertLinesToPointsOn();
		pointCleaner->ConvertPolysToLinesOn();
		pointCleaner->ConvertStripsToPolysOn();
		pointCleaner->PointMergingOn();
		pointCleaner->Update();

		vtkPolyData* resultClean = pointCleaner->GetOutput();

		mesh->DeepCopy(resultClean);

		//triangulate to be sure:
		vtkSmartPointer<vtkTriangleFilter> triangleFilter =
			vtkSmartPointer<vtkTriangleFilter>::New();
		triangleFilter->SetInputConnection(this->mesh->GetProducerPort());
		triangleFilter->Update();

		vtkPolyData* result = triangleFilter->GetOutput();
	
		this->mesh->DeepCopy(result);
		//optimize random access:
		this->mesh->BuildCells();
		this->mesh->BuildLinks(result->GetNumberOfPoints());
		
		this->mesh->Update();

		//std::cout << "Cleaned mesh has " << mesh->GetNumberOfPoints() << " vertices." << std::endl;
		//std::cout << "Cleaned mesh has " << mesh->GetNumberOfCells() << " polygons." << std::endl;
		return qfeSuccess;
	}
	else
	{
		return qfeError;
	}
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::buildMesh(qfeVolume *volume)
{
	unsigned int ui,uj,uk;
	volume->qfeGetVolumeSize(ui,uj,uk);
	std::cout<<ui<<" "<<uj<<" "<<uk<<std::endl;

	std::vector<Vec3f> VerticesSim; //to store the vertices for the signed distance function generator
	//the triangles have the same vertex index and thus we can use the same list

	scaleSimX = nx*dx;
	scaleSimY = ny*dx;
	scaleSimZ = nz*dx;

	std::cout<<scaleSimX<<" "<<scaleSimY<<" "<<scaleSimZ<<std::endl;

  // TODO NDH - crashes when no mesh is loaded.
  // Instead have a warning and make sure the tool proceeds

	//load in all points/vertices from the mesh
	for(vtkIdType id = 0; id < mesh->GetNumberOfPoints(); id++)
	{
		double p[3];
		mesh->GetPoint(id,p);
			qfePoint point(p[0],p[1],p[2]);
		qfePoint transformed(p[0],p[1],p[2]);

		//ensure the point is in the right space:
		unsigned int i,j,k;
		volume->qfeGetVolumeSize(i,j,k);
		
		transformed.x = p[0]/(double)i;
		transformed.y = p[1]/(double)j;
		transformed.z = p[2]/(double)k;

		Vec3f Vertex(transformed.x,transformed.y,transformed.z);

		Vec3f VertexSim(transformed.x * scaleSimX,
			transformed.y * scaleSimY,
			transformed.z * scaleSimZ);

		Vertices.push_back(Vertex);

		VerticesSim.push_back(VertexSim);
	}

	//cout<<"Checking for duplicate triangles:"<<endl;
	//std::string bar; //progress bar
	vtkIdType nc = mesh->GetNumberOfCells();

	//we only load the first half of the triangles due to duplicates...
	for(vtkIdType index = 0; index<nc/2; index++)
	{
		vtkCell *cell = mesh->GetCell(index);
		if(cell->GetNumberOfPoints()!=3)
		{
			cout<<"Cell "<<index<<" is not a triangle..."<<endl;
		}
		
		//int index2 = index+1;
		//int percent = (float)index2/nc/2 * 100;
		////update the progressbaar
		//for(int i = 0; i < 50; i++)
		//{
		//	if( i < (percent/2))
		//	{				
		//		bar.replace(i,1,"=");
		//	}
		//	else if( i == (percent/2))
		//	{
		//		bar.replace(i,1,">");
		//	}
		//	else
		//	{
		//		bar.replace(i,1," ");
		//	}
		//}

		//std::cout<< "\r" "[" << bar << "] ";
		//std::cout.width( 3 );
		//std::cout<< percent << "% " <<index+1<<" of "<<nc<< std::flush;

		////check for duplicates before we add the triangle:
		//bool duplicate = false;

		//int id0 = cell->GetPointId(0); int id1 = cell->GetPointId(1); int id2 = cell->GetPointId(2);

		//for(int i = 0; i<Triangles.size(); i++)
		//{
		//	int n0 = Triangles.at(i)[0]; int n1 = Triangles.at(i)[1]; int n2 = Triangles.at(i)[2];
		//	//to speed it up we ensure that the sum of the indices is equal, which reduces the number of checks:
		//	if(id0+id1+id2!=n0+n1+n2) continue;

		//	if(id0 == n0 && id1 == n1 && id2 == n2) duplicate = true;
		//	if(id0 == n0 && id1 == n2 && id2 == n1) duplicate = true;

		//	if(id0 == n1 && id1 == n0 && id2 == n2) duplicate = true;
		//	if(id0 == n1 && id1 == n2 && id2 == n0) duplicate = true;

		//	if(id0 == n2 && id1 == n0 && id2 == n1) duplicate = true;
		//	if(id0 == n2 && id1 == n1 && id2 == n0) duplicate = true;
		//	if(duplicate) break;
		//}

		//if(!duplicate)
		//{
		
		Vec3ui Triangle(cell->GetPointId(0),cell->GetPointId(1),cell->GetPointId(2));
		Triangles.push_back(Triangle);
		//}
	}
	//calculate normals:	
	//for a triangle p1, p2, p3, if the vector U = p2 - p1 and the vector V = p3 - p1 then the normal N = U X V and can be calculated by:
	//Nx = UyVz - UzVy
	//Ny = UzVx - UxVz
	//Nz = UxVy - UyVx	

	//add null vectors to fill the list:
	for(int n = 0; n<(int)Vertices.size(); n++)
	{
		Vec3f Empty(0,0,0);
		Normals.push_back(Empty);
	}
	//we traverse all triangles and add the normal to every vertex on the way
	for(int t = 0; t<(int)Triangles.size(); t++)
	{
		Vec3ui triangle = Triangles.at(t);
		Vec3f p1 = Vertices.at(triangle[0]);
		Vec3f p2 = Vertices.at(triangle[1]);
		Vec3f p3 = Vertices.at(triangle[2]);

		Vec3f U = p2 - p1;
		Vec3f V = p3 - p1;

		Vec3f N(0,0,0);

		N[0] = U[1]*V[2] - U[2]*V[1];
		N[1] = U[2]*V[0] - U[0]*V[2];
		N[2] = U[0]*V[1] - U[1]*V[0];

		Vec3f old1 = Normals.at(triangle[0]);
		Vec3f old2 = Normals.at(triangle[1]);
		Vec3f old3 = Normals.at(triangle[2]);

		Normals.replace(triangle[0],old1+N);
		Normals.replace(triangle[1],old2+N);
		Normals.replace(triangle[2],old3+N);
	}
	//normalize the normals:
	for(int n = 0; n<Normals.size(); n++)
	{
		Vec3f N = Normals.at(n);
		float length = sqrt(sqr(N[0])+sqr(N[1])+sqr(N[2]));
		Normals.replace(n,N/length);
	}

	cout<<"Number of vertices is "<<Vertices.size()<<endl;
	cout<<"Number of normals is "<<Normals.size()<<endl;
	cout<<"Number of triangles is "<<Triangles.size()<<endl;	

	//cout<<endl<<"Removed "<<100-(float)Triangles.size()/mesh->GetNumberOfCells()*200<<"% of the original triangles"<<endl<<endl;
	cout<<"Compute levelset from mesh"<<std::endl;
	Array3f meshGrid = fluidsim->meshToLevelSet(Triangles,VerticesSim);
	fluidsim->setSolids(meshGrid);

	if(noise>0)
	{
		fluidsimNoise->setSolids(meshGrid);
	}

	//negative value found (thus a solid is initialized):
	bool negativeValue = false;
	bool positiveValue = false;

	for(unsigned int k = 0; k<fluidsim->phi_solid.nk; k++)
	{
		for(unsigned int j = 0; j<fluidsim->phi_solid.nj; j++)
		{
			for(unsigned int i = 0; i<fluidsim->phi_solid.ni; i++)
			{
				if (fluidsim->phi_solid(i,j,k)<0.0) 
				{
					negativeValue = true;
				}							
				if (fluidsim->phi_solid(i,j,k)>0.0) 
				{
					positiveValue = true;
				}
				//flip sign such that the inside of the mesh becomes "hollow"
				fluidsim->phi_solid(i,j,k) = -1.0f*fluidsim->phi_solid(i,j,k);


				if(noise>0)
					fluidsimNoise->phi_solid(i,j,k) = -1.0f*fluidsimNoise->phi_solid(i,j,k);
			}
		}
	}
	fluidsim->setSolids(fluidsim->phi_solid);

	if(noise>0)
		fluidsimNoise->setSolids(fluidsimNoise->phi_solid);

	
	//implicit sphere:
	//float mx = nx/2;
	//float my = ny/2;
	//float mz = nz/2;
	//float radius = 4;
	//for(int k = 0; k<nz; k++)
	//{
	//	for(int j = 0; j<ny; j++)
	//	{
	//		for(int i = 0; i<nx; i++)
	//		{
	//			float x = i - mx;
	//			float y = j - my;
	//			float z = k - mz;
	//			fluidsim->phi_solid(i,j,k) = -(x*x+y*y+z*z-radius*radius);
	//		}
	//	}
	//}
	//fluidsim->setImplicitSolids(fluidsim->phi_solid);
	

	if(!negativeValue)
	{
		cout<<"The given mesh was not closed..."<<endl;
		return qfeError;
	}

	if(!positiveValue)
	{
		cout<<"The given mesh was not hollow..."<<endl;
		return qfeError;
	}
	
	return qfeSuccess;
}

qfeReturnStatus qfeSimulation::qfeGetSimulationModel(qfeModel &model)
{
	if(mesh == NULL)
		return qfeError;

	if(simulation_model_computed)
	{
		model = simulation_model;
		return qfeSuccess;
	}
			
	Array3f solid = this->fluidsim->getSolidSurface();

	qfeColorRGB *color = new qfeColorRGB();
	color->r = 0.871f; color->g = 0.486f; color->b = 0.514f; 
	simulation_model.qfeSetModelColor(*color);

	vtkSmartPointer<vtkStructuredPoints> grid = 
		vtkSmartPointer<vtkStructuredPoints>::New();
	grid->SetDimensions(solid.ni,solid.nj,solid.nk);

	//get voxel spacing
	qfeGrid      *volume_grid;

	this->volumeMeasurement->qfeGetVolumeGrid(&volume_grid);

	float spacing_x = (float)volumeDimX/solid.ni;
	float spacing_y = (float)volumeDimY/solid.nj;
	float spacing_z = (float)volumeDimZ/solid.nk;

	grid->SetSpacing(spacing_x,spacing_y,spacing_z);

	grid->SetNumberOfScalarComponents(1);
	grid->SetScalarTypeToDouble();
	
	//fill every entry of the image data
	for(unsigned int z = 0; z < solid.nk; z++)
	{
		for(unsigned int y = 0; y < solid.nj; y++)
		{  
			for(unsigned int x = 0; x < solid.ni; x++)
			{
				double* pixel = static_cast<double*>(grid->GetScalarPointer(x,y,z));
				float val = solid(x,y,z);
				pixel[0] = val;
			}
		}
	}
	
	// Create the 0 isosurface
	vtkSmartPointer<vtkContourFilter> contours = 
		vtkSmartPointer<vtkContourFilter>::New();
	contours->SetInput(grid);
	contours->GenerateValues(1, 0, 0);
	contours->ComputeNormalsOn();
	contours->Update();
	  
	vtkSmartPointer<vtkPolyData> polyData =
		vtkSmartPointer<vtkPolyData>::New();

	polyData = contours->GetOutput();

	simulation_model.qfeSetModelMesh(polyData);
	
	simulation_model.qfeSetModelVisible(true);

	model = simulation_model;

	simulation_model_computed = true;

	return qfeSuccess;
}

//----------------------------------------------------------------------------
Vec3f qfeSimulation::simulationToTexture(Vec3f in)
{
	Vec3f result(in[0]/scaleSimX, in[1]/scaleSimY, in[2]/scaleSimZ);
	return result;
}

//----------------------------------------------------------------------------
Vec3f qfeSimulation::textureToSimulation(Vec3f in)
{
	Vec3f result(in[0]*scaleSimX, in[1]*scaleSimY, in[2]*scaleSimZ);
	return result;
}

void qfeSimulation::applyDifferenceMethod()
{
	//initialize coupling simulation:
	FLIP_simulation	*differencesim = new FLIP_simulation(nx,ny,nz,lx,BLOOD_DENSITY);
	if(!this->fluidsim->grid.use_viscosity)
	{
		differencesim->setViscosity(0.0f);
	}
	else
	{
		differencesim->setViscosity(BLOOD_VISCOSITY);
	}

	differencesim->setSolids(fluidsim->grid.nodal_solid_phi);

	differencesim->firstRun = false; //this means we have to take care of all initializations, but have full control
	
	//we don't need this since we don't set the sources and sinks and use small timestep
	differencesim->setFillHolesAutomatically(false);	 
	
	Vec3f force;
	force[0] = 0.0;
	force[1] = 0.0;
	force[2] = 0.0;
	differencesim->setBodyForce(force);
	differencesim->using_negative_time_step = fluidsim->using_negative_time_step;

	//apply difference velocity into differencesim:
	for(unsigned int p = 0; p<fluidsim->particles.numberOfParticles; p++)
	{
		Vec3f location = fluidsim->particles.x.at(p);
		
		Vec3f measurement = interpolateMeasurement(location[0],location[1],location[2],this->serie);
		Vec3f simulated = fluidsim->particles.u.at(p);
		
		if(fluidsim->using_negative_time_step)
		{
			measurement *= -1.0f;
		}

		Vec3f velocity = simulated - measurement;	

		differencesim->setParticle(location, velocity);
	}
	differencesim->particles.transfer_to_grid();


	//simulate both fluidsim and difference sim for a small timestep
	float timestep = 1e-6; //<-- must be small, we did not copy the sources and sinks
	if(fluidsim->using_negative_time_step)
		timestep = -timestep;

	//advance one frame (advance one step messes up the simulations internal bookkeeping)
	fluidsim->advance_one_frame(timestep);
	differencesim->advance_one_frame(timestep);
		
	//make sure the velocity field is stored on the grid
	fluidsim->particles.transfer_to_grid();
	differencesim->particles.transfer_to_grid();

	//apply difference to fluidsim (using the grid, particles may have different positions)
	for(unsigned int k = 0; k<fluidsim->grid.w.nk; k++)
	{
		for(unsigned int j = 0; j<fluidsim->grid.v.nj; j++)
		{
			for(unsigned int i = 0; i<fluidsim->grid.u.ni; i++)
			{
				//subtract the difference
				if(fluidsim->grid.u.inRange(i,j,k))
				{
					fluidsim->grid.u.at(i,j,k) -= differencesim->grid.u.at(i,j,k);
				}

				if(fluidsim->grid.v.inRange(i,j,k))
				{
					fluidsim->grid.v.at(i,j,k) -= differencesim->grid.v.at(i,j,k);
				}

				if(fluidsim->grid.w.inRange(i,j,k))
				{
					fluidsim->grid.w.at(i,j,k) -= differencesim->grid.w.at(i,j,k);
				}
			}
		}
	}
	
	//We should not do a (partial) FLIP update (by updating the particle velocity),
	//instead we have to enforce a PIC update (by replacing the particles velocity):
	//Therefore, we cannot use: fluidsim->particles.update_from_grid(); but instead we use
	fluidsim->particles.transfer_velocity_from_grid();
}

//----------------------------------------------------------------------------
//applys the nabla operator on the specified dimension (x, y or z)
void qfeSimulation::computeNabla(Array3f &result, Array3f &input, bool x, bool y, bool z)
{
	int xv = 0; if(x) xv = 1;
	int yv = 0; if(y) yv = 1;
	int zv = 0; if(z) zv = 1;

	float dx = 1.0;
	for(unsigned int k = 0; k<input.nk; k++)
	{
		for(unsigned int j = 0; j<input.nj; j++)
		{
			for(unsigned int i = 0; i<input.ni; i++)
			{
				//if we are at the borders we calculate a one sided difference:
				if(i==0 && x) 
				{
					result(i,j,k) = (input(i+xv,j,k) - input(i,j,k))/dx; continue;
				}
				if(j==0 && y) 
				{
					result(i,j,k) = (input(i,j+yv,k) - input(i,j,k))/dx; continue;
				}
				if(k==0 && z) 
				{
					result(i,j,k) = (input(i,j,k+zv) - input(i,j,k))/dx; continue;
				}

				if(i==input.ni-1 && x) 
				{
					result(i,j,k) = (input(i,j,k) - input(i-xv,j,k))/dx; continue;
				}
				if(j==input.nj-1 && y) 
				{
					result(i,j,k) = (input(i,j,k) - input(i,j-yv,k))/dx; continue;
				}
				if(k==input.nk-1 && z) 
				{
					result(i,j,k) = (input(i,j,k) - input(i,j,k-zv))/dx; continue;
				}

				result(i,j,k) = (input(i+xv,j+yv,k+zv) - 2 * input(i,j,k) + input(i-xv,j-yv,k-zv))/(dx*dx);
			}
		}
	}
}

Vec3f qfeSimulation::VoxelVelocityToSimVelocity(Vec3f vel)
{
	qfeVector vi;
	qfePoint  point;

	qfeMatrix4f P2V, V2P, T2V, V2T;
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volumeMeasurement);
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volumeMeasurement);	
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volumeMeasurement);
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volumeMeasurement);

	// velocity in voxel coordinates
	vi.x = vel[0];
	vi.y = vel[1];
	vi.z = vel[2];
	
	//vi is in patient coordinates (so in cm/s)
	float measurementWidth = nx * actualCellWidth; //number of cells times the cell width
	//measurement width ~ simulation width = measurementWidth ~ 1
	//so 1 cm in the measurements is equal to 1/measurementWidth  
	//scale the velocity back from patient coordinates to simulation coordinates:
	Vec3f result(vi[0]/measurementWidth, vi[1]/measurementWidth, vi[2]/measurementWidth);

	return result;
}

Vec3f qfeSimulation::SimVelocityToVoxelVelocity(Vec3f vel)
{
	if(mesh == NULL) 
	{
		cout<<"No mesh was defined"<<endl;
		Vec3f failed(0,0,0);
		return failed;
	}

	//uncomment the next line to output the data in simulation coordinates that is inspectable in paraview
	//return vel;
	
	qfeMatrix4f P2V, V2P, T2V, V2T;
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volumeMeasurement);
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volumeMeasurement);	
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volumeMeasurement);
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volumeMeasurement);
	
	float measurementWidth = nx * actualCellWidth;

	qfeVector vi(vel[0],vel[1],vel[2]);

	vi[0] *= measurementWidth;
	vi[1] *= measurementWidth;
	vi[2] *= measurementWidth;
		
	/*
	qfeVector     spacing;	
	qfeGrid *grid = new qfeGrid();
	volumeMeasurement->qfeGetVolumeGrid(&grid);
	grid->qfeGetGridExtent(spacing.x,spacing.y,spacing.z);

	vi[0] *= spacing.x;
	vi[1] *= spacing.y;
	vi[2] *= spacing.z;
	*/
	
	vi = P2V*vi;

	//vi = T2V*vi;	

	Vec3f result(vi[0], vi[1], vi[2]);

	return result;
}

//----------------------------------------------------------------------------
Vec3f qfeSimulation::interpolateSyntheticData(float x, float y, float z, float phase)
{
	//cout<<" SYNTHETIC "<<endl;
	//the synthetic data is inside a cube so max x = max y = max z = 1
	float timeFactor = 250*fabs(phase);

	float r = sqrt(sqr(2.0f*(x-0.5))+sqr(2.0f*(y-0.5))+sqr(2.0f*(z-0.5)));
	float weight = 0.7-r;

	if(weight<=0.0f)
		return VoxelVelocityToSimVelocity(Vec3f(0.0f,0.0f,0.0f));

	float vx = (y-0.5);
	float vy = (-(x-0.5));

	float length = sqrt(sqr(vx)+sqr(vy));

	if(length!=0.0f)
	{
		vx /= length;
		vy /= length;
	}

	vx *= weight*timeFactor;
	vy *= weight*timeFactor;

	return VoxelVelocityToSimVelocity(Vec3f(vx,vy,0.0f));
}

//----------------------------------------------------------------------------
Vec3f qfeSimulation::EnrightData(float x, float y, float z, float phase)
{
	//return interpolateSyntheticData(x,y,z,phase);

	float x1 = x*2 - 0.5;
	float y1 = y*2 - 0.5;
	float z1 = z*2 - 0.5;

	if(x1<=0 || x1>=1 || y1<=0 || y1>=1 || z1<=0 || z1>=1)
		return Vec3f(0,0,0);

	float period = 8;//3 * scaleSimX;

	float x2 = (float)M_PI * 2.0*x1;
	float y2 = (float)M_PI * 2.0*y1;
	float z2 = (float)M_PI * 2.0*z1;

	float vx = 2  * sqr(sin(M_PI*x1)) * sin(y2) * sin(z2);
	float vy = -1 * sin(x2) * sqr(sin(M_PI*y1)) * sin(z2);
	float vz = -1 * sin(x2) * sin(y2) * sqr(sin(M_PI*z1));

	vx*=cos(M_PI*phase/period);
	vy*=cos(M_PI*phase/period);
	vz*=cos(M_PI*phase/period);

	float measurementWidth = nx * actualCellWidth * 100; //number of cells times the cell width
	//measurement width ~ simulation width = measurementWidth ~ 1
	//so 1 cm in the measurements is equal to 1/measurementWidth  
	//scale the velocity back from patient coordinates to simulation coordinates:

	return Vec3f(vx,vy,vz);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeGenerateEnrightData()
{
	//ranges:
	// -32 <= x <= 32
	// -32 <= y <= 32
	// -32 <= z <= 32
	//to texture: +32 / 64

	//dimensions
	int dimX = 128;
	int dimY = 128;
	int dimZ = 128;

	qfeMatrix4f V2P;

	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volumeMeasurement);

	std::cout<<"Start generating Enright data"<<std::endl;
	for(int time = 0; time<8; time++)
	{
		std::cout<<"  "<<(float)time/8<<std::endl;
		std::ostringstream strs;
		if(time<10)
			strs << '0' << time;
		else
			strs << time;
		std::string filename = "synthetic_data_Enright-"+strs.str()+".vti";
		 
		vtkSmartPointer<vtkImageData> imageData =
			vtkSmartPointer<vtkImageData>::New();
		imageData->SetDimensions(dimX,dimY,dimZ);
		//imageData->SetOrigin(0,0,0);

		//imageData->SetNumberOfScalarComponents(3);	  
		//imageData->SetScalarTypeToDouble();
		//imageData->SetSpacing(1,1,1);
		//imageData->SetWholeExtent(0,dimX,0,dimY,0,dimZ);
	
		vtkSmartPointer<vtkIntArray> qflow_anatomy =
			vtkSmartPointer<vtkIntArray>::New();		
		qflow_anatomy->SetNumberOfComponents(1);
		qflow_anatomy->SetNumberOfTuples(dimX * dimY * dimZ);

		vtkSmartPointer<vtkFloatArray> qflow_magnitudes =
			vtkSmartPointer<vtkFloatArray>::New();		
		qflow_magnitudes->SetNumberOfComponents(3);
		qflow_magnitudes->SetNumberOfTuples(dimX * dimY * dimZ);

		vtkSmartPointer<vtkFloatArray> qflow_velocities =
			vtkSmartPointer<vtkFloatArray>::New();
		qflow_velocities->SetNumberOfComponents(3);
		qflow_velocities->SetNumberOfTuples(dimX * dimY * dimZ);
		
		for (int k = 0; k < dimZ; ++k)
		{
			for (int j = 0; j < dimY; ++j)
			{
				for (int i = 0; i < dimX; ++i)
				{					
					int id = i+dimX*(j+dimY*k);
					if(id>=qflow_velocities->GetNumberOfTuples())
					{
						std::cout<<"error"<<std::endl;
						return qfeError;
					}
					float xc = (float)i/(float)(dimX-1);
					float yc = (float)j/(float)(dimY-1);
					float zc = (float)k/(float)(dimZ-1);
									
					Vec3f synth = EnrightData(xc,yc,zc,time);

					float m = sqrt(synth[0]*synth[0]+synth[1]*synth[1]+synth[2]*synth[2]);

					qflow_anatomy->SetValue(id, (int) m);

					qfeVector synthV(synth[0], synth[1], synth[2]);
					qfeVector voxVel = V2P*synthV;
					qflow_velocities->SetTuple3(id, voxVel.x, voxVel.y, voxVel.z);

					qflow_magnitudes->SetTuple3(id, synth[0], synth[1], synth[2]);
					if((int) m != qflow_anatomy->GetValue(id))
					{
						std::cout<<"error "<<(int) m<<qflow_anatomy->GetValue(id)<<std::endl;
						return qfeError;
					}
				}
			}
		}

		/*
		for(int i = 0; i<qflow_velocities->GetNumberOfTuples(); i++)
		{
			double* tuple = qflow_velocities->GetTuple3(i);
			std::cout<<"tuple: "<<tuple[0]<<" "<<tuple[1]<<" "<<tuple[2]<<std::endl;
		}
		*/

		imageData->GetPointData()->AddArray(qflow_anatomy);
		qflow_anatomy->SetName("qflow anatomy");
		imageData->Update();

		imageData->GetPointData()->AddArray(qflow_magnitudes);
		qflow_magnitudes->SetName("qflow magnitudes");
		imageData->Update();

		imageData->GetPointData()->AddArray(qflow_velocities);
		qflow_velocities->SetName("qflow velocities");
		imageData->Update();

		vtkIndent indent;
		imageData->PrintSelf(std::cout,indent);

		vtkSmartPointer<vtkXMLImageDataWriter> writer =
			vtkSmartPointer<vtkXMLImageDataWriter>::New();
		//writer->SetDataModeToAscii();
		writer->SetDataModeToBinary();
		writer->SetFileName(filename.c_str());
		writer->SetInputConnection(imageData->GetProducerPort());
		writer->Write();
	}
	std::cout<<"Done"<<std::endl;

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeGenerateSyntheticData()
{
	//ranges:
	// -32 <= x <= 32
	// -32 <= y <= 32
	// -32 <= z <= 32
	//to texture: +32 / 64

	//dimensions
	int dimX = 128;
	int dimY = 128;
	int dimZ = 128;

	std::cout<<"Start generating synthetic data"<<std::endl;
	for(int time = 0; time<8; time++)
	{
		std::cout<<"  "<<(float)time/8<<std::endl;
		std::ostringstream strs;
		if(time<10)
			strs << '0' << time;
		else
			strs << time;
		std::string filename = "synthetic_data-"+strs.str()+".vti";
		 
		vtkSmartPointer<vtkImageData> imageData =
			vtkSmartPointer<vtkImageData>::New();
		imageData->SetDimensions(dimX,dimY,dimZ);
		//imageData->SetOrigin(0,0,0);

		//imageData->SetNumberOfScalarComponents(3);	  
		//imageData->SetScalarTypeToDouble();
		//imageData->SetSpacing(1,1,1);
		//imageData->SetWholeExtent(0,dimX,0,dimY,0,dimZ);
	
		vtkSmartPointer<vtkIntArray> qflow_anatomy =
			vtkSmartPointer<vtkIntArray>::New();		
		qflow_anatomy->SetNumberOfComponents(1);
		qflow_anatomy->SetNumberOfTuples(dimX * dimY * dimZ);

		vtkSmartPointer<vtkFloatArray> qflow_magnitudes =
			vtkSmartPointer<vtkFloatArray>::New();		
		qflow_magnitudes->SetNumberOfComponents(3);
		qflow_magnitudes->SetNumberOfTuples(dimX * dimY * dimZ);

		vtkSmartPointer<vtkFloatArray> qflow_velocities =
			vtkSmartPointer<vtkFloatArray>::New();
		qflow_velocities->SetNumberOfComponents(3);
		qflow_velocities->SetNumberOfTuples(dimX * dimY * dimZ);
		
		for (int k = 0; k < dimZ; ++k)
		{
			for (int j = 0; j < dimY; ++j)
			{
				for (int i = 0; i < dimX; ++i)
				{					
					int id = i+dimX*(j+dimY*k);
					if(id>=qflow_velocities->GetNumberOfTuples())
					{
						std::cout<<"error"<<std::endl;
						return qfeError;
					}
					float xc = (float)i/(float)(dimX-1);
					float yc = (float)j/(float)(dimY-1);
					float zc = (float)k/(float)(dimZ-1);
					
					Vec3f synth = interpolateSyntheticData(xc,yc,zc,time);

					float m = sqrt(synth[0]*synth[0]+synth[1]*synth[1]+synth[2]*synth[2]);

					qflow_anatomy->SetValue(id, (int) m);
					qflow_magnitudes->SetTuple3(id, m,m,m);

					qflow_velocities->SetTuple3(id, synth[0], synth[1], synth[2]);
				}
			}
		}

		/*
		for(int i = 0; i<qflow_velocities->GetNumberOfTuples(); i++)
		{
			double* tuple = qflow_velocities->GetTuple3(i);
			std::cout<<"tuple: "<<tuple[0]<<" "<<tuple[1]<<" "<<tuple[2]<<std::endl;
		}
		*/

		imageData->GetPointData()->AddArray(qflow_anatomy);
		qflow_anatomy->SetName("qflow anatomy");
		imageData->Update();

		imageData->GetPointData()->AddArray(qflow_magnitudes);
		qflow_magnitudes->SetName("qflow magnitudes"); //not needed and some error in generation
		imageData->Update();

		imageData->GetPointData()->AddArray(qflow_velocities);
		qflow_velocities->SetName("qflow velocities");
		imageData->Update();

		vtkIndent indent;
		imageData->PrintSelf(std::cout,indent);

		vtkSmartPointer<vtkXMLImageDataWriter> writer =
			vtkSmartPointer<vtkXMLImageDataWriter>::New();
		//writer->SetDataModeToAscii();
		writer->SetDataModeToBinary();
		writer->SetFileName(filename.c_str());
		writer->SetInputConnection(imageData->GetProducerPort());
		writer->Write();
	}
	std::cout<<"Done"<<std::endl;

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulation::qfeStoreSimulationData(qfeVolume *volume,int measurementNumber)
{
	qfeGrid *grid = new qfeGrid();
	volume->qfeGetVolumeGrid(&grid);

	//dimensions
	unsigned int dimX, dimY, dimZ;
	volume->qfeGetVolumeSize(dimX, dimY, dimZ);


	std::cout<<"	measurement:  "<<measurementNumber<<std::endl;
	std::ostringstream strs;
	if(measurementsWritten<10)
		strs << '0' << measurementsWritten;
	else
		strs << measurementsWritten;
	std::string filename = "simulation_data-"+strs.str()+".vti";
	measurementsWritten ++;
	 
	vtkSmartPointer<vtkImageData> imageData =
		vtkSmartPointer<vtkImageData>::New();
	imageData->SetDimensions(dimX,dimY,dimZ);

	//set origin
	float ox, oy, oz;
	grid->qfeGetGridOrigin(ox,oy,oz);
	imageData->SetOrigin(ox,oy,oz);

	//set spacing
	qfeVector     spacing;	
	grid->qfeGetGridExtent(spacing.x,spacing.y,spacing.z);
	imageData->SetSpacing(spacing.x,spacing.y,spacing.z);
	
	vtkSmartPointer<vtkIntArray> qflow_anatomy =
		vtkSmartPointer<vtkIntArray>::New();		
	qflow_anatomy->SetNumberOfComponents(1);
	qflow_anatomy->SetNumberOfTuples(dimX * dimY * dimZ);

	vtkSmartPointer<vtkFloatArray> qflow_magnitudes =
		vtkSmartPointer<vtkFloatArray>::New();		
	qflow_magnitudes->SetNumberOfComponents(1);
	qflow_magnitudes->SetNumberOfTuples(dimX * dimY * dimZ);

	vtkSmartPointer<vtkFloatArray> qflow_velocities =
		vtkSmartPointer<vtkFloatArray>::New();
	qflow_velocities->SetNumberOfComponents(3);
	qflow_velocities->SetNumberOfTuples(dimX * dimY * dimZ);
	
	for (int k = 0; k < (int)dimZ; ++k)
	{
		for (int j = 0; j < (int)dimY; ++j)
		{
			for (int i = 0; i < (int)dimX; ++i)
			{					
				int id = i+dimX*(j+dimY*k);
				if(id>=qflow_velocities->GetNumberOfTuples())
				{
					std::cout<<"error"<<std::endl;
					return qfeError;
				}
				float x = (float)i/(dimX-1);
				float y = (float)j/(dimY-1);
				float z = (float)k/(dimZ-1);

				Vec3f texC(x,y,z);
				Vec3f simC = textureToSimulation(texC);
				
				Vec3f sim = interpolateSimulation(simC[0],simC[1],simC[2]);

				//do scaling and add noise scale with nx*actualcellwidth to scale it back to patient coordinates
				sim[0] = sim[0];//*nx*actualCellWidth;
				sim[1] = sim[1];//*nx*actualCellWidth;
				sim[2] = sim[2];//*nx*actualCellWidth;

				if(sim[0]*sim[0]+sim[1]*sim[1]+sim[2]*sim[2] == 0)
				{
					//if a velocity (0,0,0) is stored the normalization goes wrong when loading the file later
					sim[0] = 1e-6;
					sim[1] = 1e-6;
					sim[2] = 1e-6;
				}

				float m = sqrt(sim[0]*sim[0]+sim[1]*sim[1]+sim[2]*sim[2]);
				float solidPhi = this->interpolateSolidPhi(simC[0],simC[1],simC[2]);
				if(solidPhi<0)
					solidPhi = 1.0;
				else
					solidPhi = 0.0;
				qflow_anatomy->SetValue(id, (int) solidPhi);
				qflow_magnitudes->SetValue(id, m);

				qflow_velocities->SetTuple3(id,sim[0],sim[1],sim[2]);
			}
		}
	}

	imageData->GetPointData()->AddArray(qflow_anatomy);
	qflow_anatomy->SetName("qflow anatomy");
	imageData->Update();

	imageData->GetPointData()->AddArray(qflow_magnitudes);
	qflow_magnitudes->SetName("qflow magnitudes-error"); //not needed and some error in generation
	imageData->Update();

	imageData->GetPointData()->AddArray(qflow_velocities);
	qflow_velocities->SetName("qflow velocities");
	imageData->Update();

	vtkIndent indent;
	imageData->PrintSelf(std::cout,indent);

	vtkSmartPointer<vtkXMLImageDataWriter> writer =
		vtkSmartPointer<vtkXMLImageDataWriter>::New();
	//writer->SetDataModeToAscii();
	writer->SetDataModeToBinary();
	writer->SetFileName(filename.c_str());
	writer->SetInputConnection(imageData->GetProducerPort());
	writer->Write();

	return qfeSuccess;
}

float qfeSimulation::GaussianNoise()
{
	/*
	Steven W. Smith:
	The Scientist and Engineer's Guide to Digital Signal Processing	
	http://www.dspguide.comCode
	exact chapter:
	http://www.dspguide.com/ch2/6.htm
	*/
	
	//float R1 = (float)rand()/RAND_MAX;
	//float R2 = (float)rand()/RAND_MAX;
	
	//return (float) sqrt( -2.0 * log( R1 )) * cos( 2.0 * M_PI * R2 );

	//Box-Muller:
	float x1, x2, w;
	do {
		x1 = 2.0 * (float)rand()/RAND_MAX - 1.0;
		x2 = 2.0 * (float)rand()/RAND_MAX - 1.0;
		w = x1 * x1 + x2 * x2;
	} while ( w >= 1.0 );
	
	w = sqrt( (-2.0 * log( w ) ) / w );

	return x1 * w;
	return x2 * w; //if another number is needed this can also be used
}

void qfeSimulation::AverageVelocity()
{
	avgVeloc = 0;
	int count = 0;
	
	int scale = 2;
	for(int k = 0; k < (int)nz*scale; k++) 
	for(int j = 0; j < (int)ny*scale; j++) 
	for(int i = 0; i < (int)nx*scale; i++)
	{
		float x = (float)i/(nx*scale);
		float y = (float)j/(ny*scale);
		float z = (float)k/(nz*scale);

		if(interpolateSolidPhi(x,y,z))
		{
			Vec3f veloc = interpolateMeasurement(x,y,z,time);
			float magnitude = sqrt(sqr(veloc[0])+sqr(veloc[1])+sqr(veloc[2]));
			count++;
			avgVeloc += magnitude;
		}
	}
	avgVeloc /= (float)count;
	//std::cout<<"average velocity: "<<avgVeloc<<std::endl;
}

float qfeSimulation::GaussianNoise(float SNR)
{
	return avgVeloc/SNR * GaussianNoise();
}

Vec3f qfeSimulation::qfeInterpolateMeasurements(float x, float y, float z, float inPhase)
{
	if(ENRIGHT_EXPERIMENT)
		return EnrightData(x,y,z,inPhase);
	//! Seed in voxel coordinates
	//! Step size in mm
	//! Point in voxel coordinates

	qfeVector vi1, vi2;
	qfePoint  point;

	qfeMatrix4f P2V, V2P, T2V, V2T;
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volumeMeasurement);
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volumeMeasurement);	
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volumeMeasurement);
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volumeMeasurement);

	//ensure that "input" is in texture coordinates so we can transform it to voxel coordinates
	Vec3f in(x,y,z);
	Vec3f textureC = simulationToTexture(in);
	qfePoint input(textureC[0],textureC[1],textureC[2]);

	//ensure that the requested point is in voxel format
	point = T2V * input;

	if(point.x!=point.x || point.y!=point.y || point.z!=point.z)
	{
		std::cout<<"NaN point"<<std::endl;
	}

	// Seed in voxel coordinates, velocity in voxel coordinates
	unsigned int next = ((int)inPhase + 1) % velocityField.size();
	velocityField.at((int) inPhase)->qfeGetVolumeVectorAtPositionLinear(point, vi1);
	if(inPhase == (int)inPhase)
		vi2 = vi1;
	else
		velocityField.at(next)->qfeGetVolumeVectorAtPositionLinear(point, vi2);

	if(vi1[0]!=vi1[0] || vi1[1]!=vi1[1] || vi1[2]!=vi1[2])
	{
		//std::cout<<"NaN measurement current phase"<<std::endl;
		vi1[0]=0; vi1[1]=0; vi1[2]=0;
	}

	if(vi2[0]!=vi2[0] || vi2[1]!=vi2[1] || vi2[2]!=vi2[2])
	{
		//std::cout<<"NaN measurement"<<std::endl;
		vi2[0]=0; vi2[1]=0; vi2[2]=0;
	}

	double amount = inPhase-(int)inPhase;
	qfeVector vi;
	vi[0] = (1-amount)*vi1[0]+(amount)*vi2[0];
	vi[1] = (1-amount)*vi1[1]+(amount)*vi2[1];
	vi[2] = (1-amount)*vi1[2]+(amount)*vi2[2];

	if(vi[0]<1e-6) vi[0] = 0;
	if(vi[1]<1e-6) vi[1] = 0;
	if(vi[2]<1e-6) vi[2] = 0;

	//return Vec3f(vi[0],vi[1],vi[2]);

	//if(fabs(vi[0])>1e-3 && writingErrors)
	//	std::cout<<vi[0]<<" "<<amount<<" "<<vi1[0]<<" "<<vi2[0]<<std::endl;

	
	//vi is in patient coordinates (so in cm/s)
	float measurementWidth = nx * actualCellWidth; //number of cells times the cell width
	//measurement width ~ simulation width = measurementWidth ~ 1
	//so 1 cm in the measurements is equal to 1/measurementWidth  
	//scale the velocity back from patient coordinates to simulation coordinates:
	Vec3f result(vi[0]/measurementWidth, vi[1]/measurementWidth, vi[2]/measurementWidth);

	//transform it to patient coordinates to get the right units
	vi = V2P * vi;

	//std::cout<<"Patient coords "<<vi.x<<" "<<vi.y<<" "<<vi.z<<std::endl;


	return result;
}

void qfeSimulation::qfeCompareSimInterpol(qfeVolume *volume)
{
	qfeGrid *grid = new qfeGrid();
	volume->qfeGetVolumeGrid(&grid);

	//dimensions
	unsigned int dimX, dimY, dimZ;
	volume->qfeGetVolumeSize(dimX, dimY, dimZ);

	std::cout<<"comparison "<<std::endl;
	std::string filename = "interpolationVSsimulation.vtp";
	 
	vtkSmartPointer<vtkPoints> points =
		vtkSmartPointer<vtkPoints>::New();

	vtkSmartPointer<vtkDoubleArray> angles =
		vtkSmartPointer<vtkDoubleArray>::New();

	vtkSmartPointer<vtkDoubleArray> magnitudes =
		vtkSmartPointer<vtkDoubleArray>::New();
	
	angles->SetNumberOfValues(2);
	magnitudes->SetNumberOfValues(2);

	//loop to set all points
	std::vector<Vec3f> locations  = fluidsim->getParticleLocations();
	std::vector<Vec3f> velocities  = fluidsim->getParticleVelocities();
	points->InsertNextPoint(1,2,3);
	angles->SetValue(0,2.0);

	for(int p = 0; locations.size(); p++)
	{
		break;
		if(p>=(int)velocities.size())
			break;

		float x = locations.at(p)[0];
		float y = locations.at(p)[1];
		float z = locations.at(p)[2];

		float vx = velocities.at(p)[0];
		float vy = velocities.at(p)[1];
		float vz = velocities.at(p)[2];

		Vec3f iv = qfeInterpolateMeasurements(x,y,z, time);

		float sim = sqrt(sqr(vx)+sqr(vy)+sqr(vx));
		float interp = sqrt(sqr(iv[0])+sqr(iv[1])+sqr(iv[2]));
		if(sim==0 || interp == 0)
		{		
			continue;
		}
		
		float magnitudeError = 1-abs(interp/sim);

		float dot = vx/sim * iv[0]/interp + vy/sim * iv[1]/interp + vz/sim * iv[2]/interp;		
		if(dot>1) dot = 1;
		if(dot<0) dot = 0;
		float angleError = acos(dot)*180.0/M_PI;

		//set vtk data:
		points->InsertPoint((vtkIdType) p,x,y,z);
		angles->SetValue((vtkIdType) p,angleError);
		magnitudes->SetValue((vtkIdType) p,magnitudeError);
	}

	vtkSmartPointer<vtkPolyData> data =
		vtkSmartPointer<vtkPolyData>::New();

	data->SetPoints(points);

	angles->SetName("Angle errors");
	magnitudes->SetName("Magnitude errors");
	data->GetPointData()->SetScalars(angles);
	data->GetPointData()->SetScalars(magnitudes);

	vtkSmartPointer<vtkPolyDataWriter> writer =
		vtkSmartPointer<vtkPolyDataWriter>::New();
	writer->SetFileName(filename.c_str());
	writer->SetInput(data);
	writer->Write();

	std::cout<<"done writing see "<<filename<<" for the outcome"<<std::endl;
}

//----------------------------------------------------------------------------
void qfeSimulation::qfeWriteInterpolationErrors()
{
	float scale = 0.05;
	for(float i = 0; i<=7; i=i+scale)
	{
		float countAngle = 0;
		float countMagnitude = 0;
		float totalAngle = 0;
		float totalAbsoluteMagnitude = 0;	
		float averageAngle = 0;
		float averageAbsoluteMagnitude = 0;
		float averageMagnitudeDeviation = 0;
		float totalAngleDeviation = 0;
		float totalAbsoluteMagnitudeDeviation = 0;
		float angleDeviation = 0;
		float absoluteMagnitudeDeviation = 0;

		if(floor(i) == i)
		{
			std::cout<<"Time = "<<i<<std::endl;
		}

		for(unsigned int p = 0; p<fluidsim->particles.numberOfParticles; p++)
		{
			Vec3f sLocat = fluidsim->particles.x.at(p);
			Vec3f sVeloc = fluidsim->particles.u.at(p);
			
			float x = sLocat[0];
			float y = sLocat[1];
			float z = sLocat[2];
			
			float magnError = 0;
			float angleError = 0;

			Vec3f simC(x,y,z);
			Vec3f sVel = interpolateMeasurement(simC[0],simC[1],simC[2],i);
			
			Vec3f mVel = interpolateMeasurement(simC[0],simC[1],simC[2],i);
			if(floor(i) != i)
			{
				float weight = i-(int)i; //0.75 - 0
				Vec3f vel1 = interpolateMeasurement(simC[0],simC[1],simC[2],(int)i);
				Vec3f vel2 = interpolateMeasurement(simC[0],simC[1],simC[2],(int)i+1);

				sVel[0] = (1.0-weight) * vel1[0] + weight * vel2[0];
				sVel[1] = (1.0-weight) * vel1[1] + weight * vel2[1];
				sVel[2] = (1.0-weight) * vel1[2] + weight * vel2[2];
			}

			float lengthM = sqrt(mVel[0]*mVel[0]+mVel[1]*mVel[1]+mVel[2]*mVel[2]);
			float lengthS = sqrt(sVel[0]*sVel[0]+sVel[1]*sVel[1]+sVel[2]*sVel[2]);

			if (lengthM<lengthS) magnError = 1.0-lengthM/lengthS;
			if (lengthM>=lengthS) magnError = 1.0-lengthS/lengthM;

			float dot = 0;
			if (lengthM == 0 || lengthS == 0)
				dot = 1;
			else
				dot = sVel[0]/lengthS * mVel[0]/lengthM + sVel[1]/lengthS * mVel[1]/lengthM + sVel[2]/lengthS * mVel[2]/lengthM;
			
			if(dot>1) dot = 1;
			if(dot<0) dot = 0;
			angleError = acos(dot)*180.0/M_PI;
			
			//if something went wrong or isn't defined we set the error value to 0
			if (angleError<=0) angleError = 0;
			if (magnError<=0) magnError = 0;
			
			if(interpolateSolidPhi(simC[0],simC[1],simC[2])<=0)
			{
				angleError = -180;
				magnError = -1;
			}
			else
			{
				countAngle ++;
				countMagnitude ++;
				totalAngle += angleError;
				
				//std::cout<<fabs(lengthM)<<" "<<fabs(lengthS)<<" "<<fabs(lengthM-lengthS)<<std::endl;
				if(lengthM>1e-6)
					totalAbsoluteMagnitude += 1.0-lengthS/lengthM;
			}
		}
		
		averageAngle = totalAngle/countAngle;
		averageAbsoluteMagnitude = totalAbsoluteMagnitude/countMagnitude;
		//std::cout<<"Avg velocity error: "<<averageAbsoluteMagnitude<<std::endl;

		for(unsigned int p = 0; p<fluidsim->particles.numberOfParticles; p++)
		{
			Vec3f sLocat = fluidsim->particles.x.at(p);
			Vec3f sVeloc = fluidsim->particles.u.at(p);
			
			float x = sLocat[0];
			float y = sLocat[1];
			float z = sLocat[2];
			
			float magnError = 0;
			float angleError = 0;

			Vec3f simC(x,y,z);
			Vec3f sVel(sVeloc[0],sVeloc[1],sVeloc[2]);
			
			Vec3f mVel = interpolateMeasurement(simC[0],simC[1],simC[2],time);

			float lengthM = sqrt(mVel[0]*mVel[0]+mVel[1]*mVel[1]+mVel[2]*mVel[2]);
			float lengthS = sqrt(sVel[0]*sVel[0]+sVel[1]*sVel[1]+sVel[2]*sVel[2]);

			if (lengthM<lengthS) magnError = 1.0-lengthM/lengthS;
			if (lengthM>=lengthS) magnError = 1.0-lengthS/lengthM;

			float dot = 0;
			if (lengthM == 0 || lengthS == 0)
				dot = 1;
			else
				dot = sVel[0]/lengthS * mVel[0]/lengthM + sVel[1]/lengthS * mVel[1]/lengthM + sVel[2]/lengthS * mVel[2]/lengthM;
			
			if(dot>1) dot = 1;
			if(dot<0) dot = 0;
			angleError = acos(dot)*180.0/M_PI;
			
			//if something went wrong or isn't defined we set the error value to 0
			if (angleError<=0) angleError = 0;
			if (magnError<=0) magnError = 0;
			
			if(interpolateSolidPhi(simC[0],simC[1],simC[2])<=0)
			{
				angleError = -180;
				magnError = -1;
			}
			else
			{
				totalAngleDeviation += sqr(angleError - averageAngle);
				if(lengthM>1e-6)
					totalAbsoluteMagnitudeDeviation += sqr(1.0-lengthS/lengthM-averageAbsoluteMagnitude);
			}
		}

		angleDeviation = sqrt(totalAngleDeviation/countAngle);
		absoluteMagnitudeDeviation = sqrt(totalAbsoluteMagnitudeDeviation/countMagnitude);

		std::fstream filestrTime;
		filestrTime.open ("resultTime.txt", std::fstream::app);
		filestrTime<<time<<",";
		filestrTime.close();
		
		std::fstream filestrAngle;
		filestrAngle.open ("resultAngle.txt", std::fstream::app);
		filestrAngle<<averageAngle<<",";
		filestrAngle.close();

		std::fstream filestrAngleSigma;
		filestrAngleSigma.open ("resultAngleSigma.txt", std::fstream::app);
		filestrAngleSigma<<angleDeviation<<",";
		filestrAngleSigma.close();

		std::fstream filestrMagnitude;
		filestrMagnitude.open ("resultMagnitude.txt", std::fstream::app);
		filestrMagnitude<<averageAbsoluteMagnitude<<",";
		filestrMagnitude.close();

		std::fstream filestrMagnitudeSigma;
		filestrMagnitudeSigma.open ("resultMagnitudeSigma.txt", std::fstream::app);
		filestrMagnitudeSigma<<absoluteMagnitudeDeviation<<",";
		filestrMagnitudeSigma.close();

		writingErrors = false;
	}
	std::cout<<"Done"<<std::endl;
}

//----------------------------------------------------------------------------
qfeVolume *qfeSimulation::WriteCurrentVelocityFieldToVolume(int id, int phaseCount, float phaseDuration, float phaseTriggerTime)
{
	qfeGrid *grid = new qfeGrid();
	volumeMeasurement->qfeGetVolumeGrid(&grid);

	//cout<<" Times: "<<phaseDuration<<"  "<<phaseTriggerTime<<endl;

	//dimensions
	unsigned int dimX, dimY, dimZ;
	volumeMeasurement->qfeGetVolumeSize(dimX, dimY, dimZ);
	dimX--;
	dimY--;
	dimZ--;

	std::ostringstream strs;
	if(measurementsWritten<10)
		strs << '0' << measurementsWritten;
	else
		strs << measurementsWritten;
	std::string filename = "qflow_sim_"+strs.str()+".vti";
	measurementsWritten ++;
	 
  vtkSmartPointer<qfeImageData> imageData = 
	vtkSmartPointer<qfeImageData>::New();
  imageData->SetDimensions(dimX,dimY,dimZ);
  
  qfePoint      origin;
  qfeVector     spacing;	
  qfeVector     axisX, axisY, axisZ;

  // get the data characteristics
  grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  grid->qfeGetGridExtent(spacing.x,spacing.y,spacing.z);
  grid->qfeGetGridAxes(axisX.x, axisX.y, axisX.z, axisY.x, axisY.y, axisY.z, axisZ.x, axisZ.y, axisZ.z);

  // set the data characteristics	
  imageData->SetOrigin(origin.x,origin.y,origin.z);
  imageData->SetSpacing(spacing.x,spacing.y,spacing.z);  
  imageData->SetAxisX(axisX.x, axisX.y, axisX.z);
  imageData->SetAxisY(axisY.x, axisY.y, axisY.z);
  imageData->SetAxisZ(axisZ.x, axisZ.y, axisZ.z);  
  imageData->SetPhaseCount(phaseCount);
  imageData->SetPhaseDuration(phaseDuration);
  imageData->SetPhaseTriggerTime(phaseTriggerTime);
  double venc[3] = {200.0, 200.0, 200.0}; 
  imageData->SetVenc(venc); //additional check todo ndh
  imageData->SetScanType(qfeDataFlow4D);
  imageData->SetScanOrientation(qfeSagittal);
	
  // store the data arrays
	vtkSmartPointer<vtkFloatArray> qflow_anatomy =
		vtkSmartPointer<vtkFloatArray>::New();		
	qflow_anatomy->SetNumberOfComponents(1);
	qflow_anatomy->SetNumberOfTuples(dimX * dimY * dimZ);

	vtkSmartPointer<vtkFloatArray> qflow_magnitudes =
		vtkSmartPointer<vtkFloatArray>::New();		
	qflow_magnitudes->SetNumberOfComponents(3);
	qflow_magnitudes->SetNumberOfTuples(dimX * dimY * dimZ);

	vtkSmartPointer<vtkFloatArray> qflow_velocities =
		vtkSmartPointer<vtkFloatArray>::New();
	qflow_velocities->SetNumberOfComponents(3);
	qflow_velocities->SetNumberOfTuples(dimX * dimY * dimZ);
	
	for (int k = 0; k < (int)dimZ; k++)
	{
		for (int j = 0; j < (int)dimY; j++)
		{
			for (int i = 0; i < (int)dimX; i++)
			{					
				int id = i+dimX*(j+dimY*k);
				
				if(id>=qflow_velocities->GetNumberOfTuples())
				{
					std::cout<<"error"<<std::endl;
					qfeVolume *emptyVolume = NULL;
					return emptyVolume;
				}
				float x = (float)i/(dimX-1);
				float y = (float)j/(dimY-1);
				float z = (float)k/(dimZ-1);

				Vec3f texC(x,y,z);
				Vec3f simC = textureToSimulation(texC);
				
				Vec3f sim = SimVelocityToVoxelVelocity(interpolateSimulation(simC[0],simC[1],simC[2]));

				qflow_anatomy->SetValue(id, interpolatePressure(simC[0],simC[1],simC[2]));
				
				float errorAngle = 0;
				float errorMagn = 0;

				if(interpolateSolidPhi(simC[0],simC[1],simC[2])>=0.0f && i<(int)(dimX-2) && j<(int)(dimY-2) && k<(int)(dimZ-2)) // && velocityField.size() -1 < phaseTriggerTime
				{
					Vec3f simVel = interpolateSimulation(simC[0],simC[1],simC[2]);
					Vec3f measVel = interpolateMeasurement(simC[0],simC[1],simC[2],phaseTriggerTime);

					if(simVel[0] != simVel[0] && simVel[1] != simVel[1] && simVel[2] != simVel[2])
						continue;
					if(measVel[0] != measVel[0] && measVel[1] != measVel[1] && measVel[2] != measVel[2])
						continue;
	
					float simLength = sqrt(sqr(simVel[0])+sqr(simVel[1])+sqr(simVel[2]));
					float measLength = sqrt(sqr(measVel[0])+sqr(measVel[1])+sqr(measVel[2]));
					
		
					float dot = 0;

					if(simLength != 0 && measLength!=0)
					{
						dot = simVel[0]/simLength * measVel[1]/measLength + 
							simVel[1]/simLength * measVel[1]/measLength + 
							simVel[2]/simLength * measVel[2]/measLength;
						
						if(simLength<measLength)
							errorMagn = fabs(1-(simLength/measLength));
						else
							errorMagn = fabs(1-(measLength/simLength));
					}
				
					if(dot>1)  dot = 1;
					if(dot<-1) dot = -1;
					
					errorAngle = acos(dot)*180.0/M_PI;
				}

				qflow_magnitudes->SetTuple3(id, errorAngle, errorMagn, 0);

				qflow_velocities->SetTuple3(id,sim[0],sim[1],sim[2]);
			}
		}
	}

	qflow_anatomy->SetName("qflow anatomy");
  imageData->GetPointData()->AddArray(qflow_anatomy);
  imageData->GetPointData()->SetActiveScalars("qflow anatomy");	
	imageData->Update();

	qflow_magnitudes->SetName("qflow magnitudes"); //not needed and some error in generation
	imageData->GetPointData()->SetActiveVectors("qflow magnitudes");
  imageData->GetPointData()->AddArray(qflow_magnitudes);	
	imageData->Update();

	qflow_velocities->SetName("qflow velocities");
  imageData->GetPointData()->AddArray(qflow_velocities);
  imageData->GetPointData()->SetActiveVectors("qflow velocities");	
	imageData->Update();

	qfeVolume *simVolume = new qfeVolume();
	qfeConvert::vtkImageDataToQfeVolume(imageData, simVolume);

	vtkIndent indent;
	//imageData->PrintSelf(std::cout,indent);

	vtkSmartPointer<qfeVTIImageDataWriter> writer =
		vtkSmartPointer<qfeVTIImageDataWriter>::New();
	
	writer->SetDataModeToBinary();
	writer->SetFileName(filename.c_str());
	writer->SetInput(imageData);
	writer->Write();

	return simVolume;
}

//----------------------------------------------------------------------------
qfeVolume *qfeSimulation::WriteSimulationVelocityFieldToVolume(int id, int phaseCount, string fileName, float phaseDuration, float phaseTriggerTime, Array3Vec3 velocityField)
{
	qfeVolume *simVolume = new qfeVolume();

	qfeGrid *grid = new qfeGrid();
	volumeMeasurement->qfeGetVolumeGrid(&grid);
	
	//dimensions
	unsigned int dimX, dimY, dimZ;
	volumeMeasurement->qfeGetVolumeSize(dimX, dimY, dimZ);
	//dimX--;
	//dimY--;
	//dimZ--;

	std::ostringstream strs;
	if(measurementsWritten<10)
		strs << '0' << measurementsWritten;
	else
		strs << measurementsWritten;
	std::string filename = fileName+"_"+strs.str()+".vti";
	measurementsWritten ++;

	//std::cout<<"Writing "<<filename;
	 
  vtkSmartPointer<qfeImageData> imageData = 
	vtkSmartPointer<qfeImageData>::New();
  imageData->SetDimensions(dimX,dimY,dimZ);
  
  qfePoint      origin;
  qfeVector     spacing;	
  qfeVector     axisX, axisY, axisZ;

  // get the data characteristics
  grid->qfeGetGridOrigin(origin.x, origin.y, origin.z);
  grid->qfeGetGridExtent(spacing.x,spacing.y,spacing.z);
  grid->qfeGetGridAxes(axisX.x, axisX.y, axisX.z, axisY.x, axisY.y, axisY.z, axisZ.x, axisZ.y, axisZ.z);

  // set the data characteristics	
  imageData->SetOrigin(origin.x,origin.y,origin.z);
  imageData->SetSpacing(spacing.x,spacing.y,spacing.z);  
  imageData->SetAxisX(axisX.x, axisX.y, axisX.z);
  imageData->SetAxisY(axisY.x, axisY.y, axisY.z);
  imageData->SetAxisZ(axisZ.x, axisZ.y, axisZ.z);  
  imageData->SetPhaseCount(phaseCount);
  imageData->SetPhaseDuration(phaseDuration);
  imageData->SetPhaseTriggerTime(phaseTriggerTime);
  double venc[3] = {200.0, 200.0, 200.0}; 
  imageData->SetVenc(venc); //additional check todo ndh
  imageData->SetScanType(qfeDataFlow4D);
  imageData->SetScanOrientation(qfeSagittal);
	
  // store the data arrays
	vtkSmartPointer<vtkFloatArray> qflow_anatomy =
		vtkSmartPointer<vtkFloatArray>::New();		
	qflow_anatomy->SetNumberOfComponents(1);
	qflow_anatomy->SetNumberOfTuples(dimX * dimY * dimZ);

	vtkSmartPointer<vtkFloatArray> qflow_magnitudes =
		vtkSmartPointer<vtkFloatArray>::New();		
	qflow_magnitudes->SetNumberOfComponents(3);
	qflow_magnitudes->SetNumberOfTuples(dimX * dimY * dimZ);

	vtkSmartPointer<vtkFloatArray> qflow_velocities =
		vtkSmartPointer<vtkFloatArray>::New();
	qflow_velocities->SetNumberOfComponents(3);
	qflow_velocities->SetNumberOfTuples(dimX * dimY * dimZ);

	/*
	vtkSmartPointer<vtkFloatArray> qflow_sources =
		vtkSmartPointer<vtkFloatArray>::New();		
	qflow_sources->SetNumberOfComponents(1);
	qflow_sources->SetNumberOfTuples(dimX * dimY * dimZ);

	vtkSmartPointer<vtkFloatArray> qflow_sinks =
		vtkSmartPointer<vtkFloatArray>::New();		
	qflow_sinks->SetNumberOfComponents(1);
	qflow_sinks->SetNumberOfTuples(dimX * dimY * dimZ);

	vtkSmartPointer<vtkFloatArray> qflow_angle_differences =
		vtkSmartPointer<vtkFloatArray>::New();		
	qflow_angle_differences->SetNumberOfComponents(1);
	qflow_angle_differences->SetNumberOfTuples(dimX * dimY * dimZ);

	vtkSmartPointer<vtkFloatArray> qflow_magnitude_differences =
		vtkSmartPointer<vtkFloatArray>::New();		
	qflow_magnitude_differences->SetNumberOfComponents(1);
	qflow_magnitude_differences->SetNumberOfTuples(dimX * dimY * dimZ);

	vtkSmartPointer<vtkFloatArray> qflow_wall_shear_stress =
		vtkSmartPointer<vtkFloatArray>::New();		
	qflow_wall_shear_stress->SetNumberOfComponents(3);
	qflow_wall_shear_stress->SetNumberOfTuples(dimX * dimY * dimZ);
	*/
	
	for (int k = 0; k < (int)dimZ; k++)
	{
		for (int j = 0; j < (int)dimY; j++)
		{
			for (int i = 0; i < (int)dimX; i++)
			{					
				int id = i+dimX*(j+dimY*k);
				
				if(id>=qflow_velocities->GetNumberOfTuples())
				{
					std::cout<<"error"<<std::endl;
					return simVolume;
				}
				float x = (float)i/(dimX);
				float y = (float)j/(dimY);
				float z = (float)k/(dimZ);
								
				Vec3f velocity(0.0f, 0.0f, 0.0f);

				unsigned int ci,cj,ck;
				float fx,fy,fz;
				ci = floor(x*fluidsim->grid.ni);
				cj = floor(y*fluidsim->grid.nj);
				ck = floor(z*fluidsim->grid.nk);
				fx = x*fluidsim->grid.ni - ci;
				fy = y*fluidsim->grid.nj - cj;
				fz = z*fluidsim->grid.nk - ck;

				float anatomy = -1.0f;

				if(velocityField.inRange(ci+1,cj+1,ck+1))
				{

					for(unsigned int it = 0; it<=2; it++)
					{
						float d00 = velocityField(ci,cj,ck)[it]         *(1-fx)+
									velocityField(ci+1,cj,ck)[it]       *fx;//(i,j,k) and (i,j+1,k)
						float d10 = velocityField(ci,cj+1,ck)[it]       *(1-fx)+
									velocityField(ci+1,cj+1,ck)[it]     *fx;//(i,j+1,k) and (i+1,j+1,k)
						float d01 = velocityField(ci,cj,ck+1)[it]       *(1-fx)+
									velocityField(ci+1,cj,ck+1)[it]     *fx;//(i,j,k+1) and (i+1,j,k+1)
						float d11 = velocityField(ci,cj+1,ck+1)[it]     *(1-fx)+
									velocityField(ci+1,cj+1,ck+1)[it]   *fx;//(i,j+1,k+1) and (i+1,j+1,k+1)

						//now we do not need the x dimension anymore, do bilerp on y using the corners above to get a line:
						float e0 = d00*(1-fy)+d10*fy;
						float e1 = d01*(1-fy)+d11*fy;

						//now we only need liniear interpolation on the line defined above:
						velocity[it] = e0*(1-fz)+e1*fz;
					}

					anatomy = fluidsim->grid.nodal_solid_phi.gridtrilerp(ci,cj,ck,fx,fy,fz);
				}
				if(anatomy<0.0f)//outside vessel
				{
					velocity[0] = 0.0f; velocity[1] = 0.0f; velocity[2] = 0.0f; 
				}
											
				Vec3f sim = SimVelocityToVoxelVelocity(velocity);

				qflow_anatomy->SetValue(id, anatomy);

				
				
				int simulatedCell = -1;
				int wall = -1;
				if(velocityField.inRange(ci,cj,ck))
				{
					if(fluidsim->fillHolesCells(ci,cj,ck))
					{
						simulatedCell = 1;
					}
					if(fluidsim->grid.solid_phi(ci,cj,ck) < 0.0f)
						wall = 1;
				}
				qflow_magnitudes->SetTuple3(id, velocity[0], velocity[1], velocity[2]);

				qflow_velocities->SetTuple3(id,sim[0],sim[1],sim[2]);

				/*

				int source = 0;
				if(fluidsim->source_cells.inRange(ci,cj,ck) && fluidsim->source_cells(ci,cj,ck))
				{
					source = 1;
				}

				int sink = 0;
				if(fluidsim->sink_cells.inRange(ci,cj,ck) && fluidsim->sink_cells(ci,cj,ck))
				{
					sink = 1;
				}

				qflow_sources->SetValue(id, source);
				qflow_sinks->SetValue(id, sink);

				Vec3f measurement = Vec3f(0.0f,0.0f,0.0f);
				float tx = (float) (ci+fx)/fluidsim->sink_cells.ni; 
				float ty = (float) (cj+fy)/fluidsim->sink_cells.nj;
				float tz = (float) (ck+fz)/fluidsim->sink_cells.nk;

				Vec3f simulationCoords = this->textureToSimulation(Vec3f(tx,ty,tz));

				if(fluidsim->sink_cells.inRange(ci,cj,ck) && fluidsim->sink_cells.inRange(ci-1,cj-1,ck-1))
				{
					measurement = interpolateMeasurement(simulationCoords[0], simulationCoords[1], simulationCoords[2], phaseTriggerTime);
				}		

				float sim_magnitude = sqrt(velocity[0] * velocity[0] + velocity[1] * velocity[1] + velocity[2] * velocity[2]);
				float meas_magnitude = sqrt(measurement[0] * measurement[0] + measurement[1] * measurement[1] + measurement[2] * measurement[2]);

				float diff_magn = 0.0f;
				float diff_angle = 0.0f;
				
				std::vector<float> vector(3);
				vector[0] = ci+fx; vector[1] = cj+fy; vector[2] = ck+fz;

				if(fluidsim->grid.nodal_solid_phi.inRange(ci,cj,ck) && fluidsim->grid.nodal_solid_phi.inRange(ci-1,cj-1,ck-1) &&
					fluidsim->grid.nodal_solid_phi.interpolate_value(vector)>=0.0f &&
					fluidsim->fillHolesCells(ci,cj,ck))
				{
					if (meas_magnitude <  sim_magnitude) 
						diff_magn = 1.0f   - meas_magnitude / sim_magnitude;
					if (meas_magnitude >= sim_magnitude) 
						diff_magn = -(1.0f - sim_magnitude  / meas_magnitude);

					if(sim_magnitude != 0.0f && meas_magnitude != 0.0f)
					{				
						Vec3f normalized_sim = Vec3f(velocity[0]/sim_magnitude, velocity[1]/sim_magnitude, velocity[2]/sim_magnitude);
						Vec3f normalized_meas = Vec3f(measurement[0]/meas_magnitude, measurement[1]/meas_magnitude, measurement[2]/meas_magnitude);
						
						diff_angle = acos(	normalized_sim[0]*normalized_meas[0] +
											normalized_sim[1]*normalized_meas[1] +
											normalized_sim[2]*normalized_meas[2]) //acos of dot product gives angle
											/M_PI*180; //convert radians to degrees
					}
				}

				qflow_magnitude_differences->SetValue(id,diff_magn);
				qflow_angle_differences->SetValue(id,diff_angle);

				//tua = mu * (Gradient of u dot outward normal)
				Vec3f wall_shear_stress = 0.0f;
				float height_above_boundary = anatomy;
				float velocityGradient[3][3];
				Vec3f normal = Vec3f(1.0f, 0.0f, 0.0f);

				for(unsigned int mi = 0; mi<2; mi++) for(unsigned int mj = 0; mj<2; mj++)
					velocityGradient[mi][mj] = 0.0f;

				Vec3f center;
				Vec3f x_off;
				Vec3f y_off;
				Vec3f z_off;
				if(velocityField.inRange(ci,cj,ck) && velocityField.inRange(ci+1,cj+1,ck+1))
				{
					for(unsigned int ind = 0; ind<4; ind++)
					{
						Vec3f result(0.0f, 0.0f, 0.0f);

						float _fx = fx; float _fy = fy; float _fz = fz;

						if(ind == 1) _fx += 0.05;
						if(ind == 2) _fy += 0.05;
						if(ind == 3) _fz += 0.05;

						for(unsigned int it = 0; it<=2; it++)
						{
							float d00 = velocityField(ci,cj,ck)[it]         *(1-_fx)+
										velocityField(ci+1,cj,ck)[it]       *_fx;//(i,j,k) and (i,j+1,k)
							float d10 = velocityField(ci,cj+1,ck)[it]       *(1-_fx)+
										velocityField(ci+1,cj+1,ck)[it]     *_fx;//(i,j+1,k) and (i+1,j+1,k)
							float d01 = velocityField(ci,cj,ck+1)[it]       *(1-_fx)+
										velocityField(ci+1,cj,ck+1)[it]     *_fx;//(i,j,k+1) and (i+1,j,k+1)
							float d11 = velocityField(ci,cj+1,ck+1)[it]     *(1-_fx)+
										velocityField(ci+1,cj+1,ck+1)[it]   *_fx;//(i,j+1,k+1) and (i+1,j+1,k+1)

							//now we do not need the x dimension anymore, do bilerp on y using the corners above to get a line:
							float e0 = d00*(1-_fy)+d10*_fy;
							float e1 = d01*(1-_fy)+d11*_fy;

							//now we only need liniear interpolation on the line defined above:
							result[it] = e0*(1-_fz)+e1*_fz;
						}

						if(ind == 0) center = result;
						if(ind == 1) x_off  = result;
						if(ind == 2) y_off  = result;
						if(ind == 3) z_off  = result;
					}
					
					//compute gradient tensor
					velocityGradient[0][0] = x_off[0] - center[0];
					velocityGradient[1][0] = x_off[1] - center[1];
					velocityGradient[2][0] = x_off[2] - center[2];
					velocityGradient[0][1] = y_off[0] - center[0];
					velocityGradient[1][1] = y_off[1] - center[1];
					velocityGradient[2][1] = y_off[2] - center[2];
					velocityGradient[0][2] = z_off[0] - center[0];
					velocityGradient[1][2] = z_off[1] - center[1];
					velocityGradient[2][2] = z_off[2] - center[2];
				}
				
				vector[0] = ci+fx; vector[1] = cj+fy; vector[2] = ck+fz;

				if(fluidsim->phi_solid.inRange(ci,cj,ck) && fluidsim->phi_solid.inRange(ci+1,cj+1,ck+1))
					fluidsim->phi_solid.interpolate_gradient(vector);

				float viscosity = 1.0f;
				if(this->fluidsim->grid.use_viscosity)
					viscosity = BLOOD_VISCOSITY;

				if(!simulatedCell)
					viscosity = 0.0f; //to make sure it is 0 when not applicable

				wall_shear_stress[0] = viscosity * 
									   (velocityGradient[0][0] * normal[0] +
										velocityGradient[1][0] * normal[1] +
										velocityGradient[2][0] * normal[2]);
				wall_shear_stress[1] = viscosity *  
									   (velocityGradient[0][1] * normal[0] +
										velocityGradient[1][1] * normal[1] +
										velocityGradient[2][1] * normal[2]);
				wall_shear_stress[2] =	viscosity * 
									   (velocityGradient[0][2] * normal[0] +
										velocityGradient[1][2] * normal[1] +
										velocityGradient[2][2] * normal[2]);

				Vec3f stress = SimVelocityToVoxelVelocity(wall_shear_stress);
				
				qflow_wall_shear_stress->SetTuple3(id,stress[0],stress[1],stress[2]);
				*/
			}
		}
	}

	qflow_anatomy->SetName("qflow anatomy");
	imageData->GetPointData()->AddArray(qflow_anatomy);
	imageData->GetPointData()->SetActiveScalars("qflow anatomy");
	imageData->Update();
	
	qflow_magnitudes->SetName("qflow magnitudes");
	imageData->GetPointData()->SetActiveVectors("qflow magnitudes");
	imageData->GetPointData()->AddArray(qflow_magnitudes);	
	imageData->Update();
	
	qflow_velocities->SetName("qflow velocities");
	imageData->GetPointData()->AddArray(qflow_velocities);
	imageData->GetPointData()->SetActiveVectors("qflow velocities");
	imageData->Update();

	qfeConvert::vtkImageDataToQfeVolume(imageData, simVolume);
	/*

	qflow_sources->SetName("qflow fluid sources");
	imageData->GetPointData()->AddArray(qflow_sources);
	imageData->GetPointData()->SetActiveScalars("qflow fluid sources");
	imageData->Update();

	qflow_sinks->SetName("qflow fluid sinks");
	imageData->GetPointData()->AddArray(qflow_sinks);
	imageData->GetPointData()->SetActiveScalars("qflow fluid sinks");
	imageData->Update();

	qflow_magnitude_differences->SetName("qflow magnitude differences");
	imageData->GetPointData()->AddArray(qflow_magnitude_differences);
	imageData->GetPointData()->SetActiveScalars("qflow magnitude differences");
	imageData->Update();

	qflow_angle_differences->SetName("qflow angle differences");
	imageData->GetPointData()->AddArray(qflow_angle_differences);
	imageData->GetPointData()->SetActiveScalars("qflow angle differences");
	imageData->Update();

	qflow_wall_shear_stress->SetName("qflow wall shear stress");
	imageData->GetPointData()->AddArray(qflow_wall_shear_stress);
	imageData->GetPointData()->SetActiveScalars("qflow wall shear stress");
	imageData->Update();
	*/

	vtkIndent indent;
	//imageData->PrintSelf(std::cout,indent);

	vtkSmartPointer<qfeVTIImageDataWriter> writer =
		vtkSmartPointer<qfeVTIImageDataWriter>::New();
	
	writer->SetDataModeToBinary();
	writer->SetFileName(filename.c_str());
	writer->SetInput(imageData);
	writer->Write();

	//std::cout<<"\r"<<"Written "<<filename<<"       "<<std::endl;

	return simVolume;
}

qfeReturnStatus qfeSimulation::qfeComputeFluidInteractor(qfePoint simCoord, qfeFluidInteractor::InteractorType type, qfeFluidInteractor& interactor)
{
	float epsilon = 1e-3f;//fluidsim->grid.dx*1.0f;

	qfeMatrix4f P2V, V2P, T2V, V2T;
	qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volumeMeasurement);
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volumeMeasurement);	
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volumeMeasurement);
	qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volumeMeasurement);

	//for two points inside the mesh close to eachother we compute the normal of the sdf
	//these normals span a surface that is approximately perpendicular to the "tube" shape of the mesh

	unsigned int i1 = floor(simCoord.x);
	unsigned int j1 = floor(simCoord.y);
	unsigned int k1 = floor(simCoord.z);

	float fx1 = simCoord.x - i1;
	float fy1 = simCoord.y - j1;
	float fz1 = simCoord.z - k1;

	Array3f solidSurface = fluidsim->getSolidSurface();
	float phi1 = solidSurface.gridtrilerp(i1,j1,k1,fx1,fy1,fz1);

	if(phi1<0.0f)
		return qfeError;

	unsigned int i2 = floor(simCoord.x+epsilon);
	unsigned int j2 = floor(simCoord.y+epsilon);
	unsigned int k2 = floor(simCoord.z+epsilon);

	float fx2 = simCoord.x+epsilon - i2;
	float fy2 = simCoord.y+epsilon - j2;
	float fz2 = simCoord.z+epsilon - k2;
	
	float phi2 = solidSurface.gridtrilerp(i2,j2,k2,fx2,fy2,fz2);
	if(phi2<0.0f) //the point is outside the mesh
	{
		epsilon = -epsilon;
		 i2 = floor(simCoord.x+epsilon);
		 j2 = floor(simCoord.y+epsilon);
		 k2 = floor(simCoord.z+epsilon);
		 
		 fx2 = simCoord.x+epsilon - i2;
		 fy2 = simCoord.y+epsilon - j2;
		 fz2 = simCoord.z+epsilon - k2;
		 
		 phi2 = solidSurface.gridtrilerp(i2,j2,k2,fx2,fy2,fz2);
	}
	if(phi2<0.0f) //the point is still outside the mesh, abort (the original point is too close to the boundary)
		return qfeError;

	//compute the gradient for both points, this gradient should span up the surface we want
	std::vector<float> vector1(3), vector2(3);
	vector1[0] = i1+fx1; vector1[1] = j1+fy1; vector1[2] = k1+fz1;
	vector2[0] = i2+fx2, vector2[1] = j2+fy2; vector2[2] = k2+fz2;

	std::vector<float> grad1 = solidSurface.interpolate_gradient(vector1);
	std::vector<float> grad2 = solidSurface.interpolate_gradient(vector2);

	float length1 = grad1[0] + grad1[1] + grad1[2];
	float length2 = grad2[0] + grad2[1] + grad2[2];
	grad1[0] /= length1; grad1[1] /= length1; grad1[2] /= length1; 
	grad2[0] /= length2; grad2[1] /= length2; grad2[2] /= length2;

	//compute the normal of the plane using the cross-product
	Vec3f plane_normal;
	plane_normal[0] = grad1[1] * grad2[2] - grad1[2] * grad2[1]; // a_y * b_z - a_z * b_y
	plane_normal[1] = grad1[2] * grad2[0] - grad1[0] * grad2[2]; // a_z * b_x - a_x * b_z
	plane_normal[2] = grad1[0] * grad2[1] - grad1[1] * grad2[0]; // a_x * b_y - a_y * b_x
	plane_normal.normalize();

	if(plane_normal[0] == 0 && plane_normal[1] == 0 && plane_normal[2] == 0)
	{
		//the gradients were parallel to each other
		return qfeError;
	}

	//now we take the cross product of the gradient of the first point with the normal to get the perpendicular vector
	Vec3f v1(grad1[0],grad1[1],grad1[2]);
	Vec3f v2(0.0f, 0.0f, 0.0f);

	v2[0] = v1[1] * plane_normal[2] - v1[2] * plane_normal[1]; // a_y * b_z - a_z * b_y
	v2[1] = v1[2] * plane_normal[0] - v1[0] * plane_normal[2]; // a_z * b_x - a_x * b_z
	v2[2] = v1[0] * plane_normal[1] - v1[1] * plane_normal[0]; // a_x * b_y - a_y * b_x

	//find center
	qfePoint center;

	std::vector<float> vector(3);
	vector[0] = i1+fx1; vector[1] = j1+fy1; vector[2] = k1+fz1;
	std::vector<float> normal = solidSurface.interpolate_gradient(vector);
	float step_size = fluidsim->grid.dx*0.5f;
	int max_steps = ceil((float)max(max(fluidsim->grid.ni,fluidsim->grid.nj),fluidsim->grid.nk)/step_size);
	float maximum = -1;

	for(unsigned int signs = 0; signs<2; signs++)
	{
		int sign = signs == 0 ? -1 : 1;

		for(int step = 0; step<max_steps; step++)
		{
			float offset = sign*step*step_size;

			int cx = ceil(i1+fx1+offset*normal[0]);
			int cy = ceil(j1+fy1+offset*normal[1]);
			int cz = ceil(k1+fz1+offset*normal[2]);

			if(!solidSurface.inRange(cx,cy,cz))
				break; //outside the grid, so we can stop in this direction

			std::vector<float> vec(3);
			vector[0] = i1+fx1+offset*normal[0]; vector[1] = j1+fy1+offset*normal[1]; vector[2] = k1+fz1+offset*normal[2];
			float search_phi = solidSurface.interpolate_value(vec);
			
			if(search_phi<=0.0f && step < 0)//boundary reached, search in the other direction
			{
				step = 0;
			}
			if(search_phi<=0.0f && step >0)//again boundary reached => done
			{
				break;
			}


			if(search_phi>maximum)
			{
				center.x = i1+fx1+offset*normal[0];
				center.y = j1+fy1+offset*normal[1];
				center.z = k1+fz1+offset*normal[2];

				maximum = search_phi;
			}
		}
	}

	//convert to voxel coordinates for convenience
	qfePoint texCoord;
	texCoord.x = center.x/fluidsim->grid.ni;
	texCoord.y = center.y/fluidsim->grid.nj;
	texCoord.z = center.z/fluidsim->grid.nk;

	qfePoint voxCoord = (texCoord * T2V);

	interactor.setOrigin(voxCoord);
	interactor.setRadius(maximum);
	interactor.setNormal(qfeVector(plane_normal[0],plane_normal[1],plane_normal[2]));
	interactor.setAxis(qfeVector(v1[0],v1[1],v1[2]),qfeVector(v2[0],v2[1],v2[2]));
	interactor.setType(type);
	interactor.setdx(this->fluidsim->grid.dx);
	
	return qfeSuccess;
}

qfeReturnStatus qfeSimulation::qfeUseViscosity(bool use_viscosity)
{
	if(!use_viscosity)
	{
		this->fluidsim->setViscosity(0.0f);
	}
	else
	{
		this->fluidsim->setViscosity(BLOOD_VISCOSITY);
	}

	this->fluidsim->grid.use_viscosity = use_viscosity;

	return qfeSuccess;
}

void qfeSimulation::qfeMakeIncompressible(int timeDirection)
{
	assert(timeDirection == -1 || timeDirection == 1);
	this->qfeAdvanceOneFrame(timeDirection * 1e-6);
}