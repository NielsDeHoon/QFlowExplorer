#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithmFlow.h"
#include "qfeColorMap.h"
#include "qfeGLShaderProgram.h"
#include "qfeGLSupport.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeMath.h"
#include "qfePoint.h"
#include "qfeSeeding.h"
#include "qfeTransform.h"
#include "qfeVolume.h"

#include <vector>

#define MAX_PARTICLES 65536

typedef vector< qfePoint >  qfeParticles;

/**
* \file   qfeFlowParticleTrace.h
* \author Roy van Pelt
* \class  qfeFlowParticleTrace
* \brief  Implements rendering of a particle trace
* \note   Confidential
*
* Rendering of a particle trace
*/

class qfeFlowParticleTrace : public qfeAlgorithmFlow
{
public:
  enum qfeFlowParticlesStyle
  {
	  qfeFlowParticlesEllipsoid,
	  qfeFlowParticlesSphere  
  };

  enum qfeFlowParticlesColor
  {
	  qfeFlowParticlesColorSpeed,
	  qfeFlowParticlesColorSeedTime
  };

  enum qfeFlowParticlesShading
  {
	  qfeFlowParticlesCel,
	  qfeFlowParticlesPhongAdditive,
    qfeFlowParticlesPhongMultiplicative,
    qfeFlowParticlesNone
  };

  enum qfeFlowParticlesTrailType
  {
	  qfeFlowParticlesTrailHalo,
	  qfeFlowParticlesTrail,
    qfeFlowParticlesTrailNone
  };

  enum qfeFlowParticlesIntegrationScheme
  {
	  qfeFlowParticlesRK1,
	  qfeFlowParticlesRK2,
    qfeFlowParticlesRK4
  };

  qfeFlowParticleTrace();
  qfeFlowParticleTrace(const qfeFlowParticleTrace &fv);
  ~qfeFlowParticleTrace();

  qfeReturnStatus qfeClearSeedPositions();  
  qfeReturnStatus qfeAddSeedPositions(qfeSeeds seeds);
  qfeReturnStatus qfeAddSeedPositions(qfeSeeds seeds, float phase);
  qfeReturnStatus qfeAddSeedPositions(qfeSeeds seeds, float phase, qfeColorRGBA color);
  
  qfeReturnStatus qfeAdvectParticles(qfeVolumeSeries series, qfeFloat phase, qfeFloat traceDuration);
  qfeReturnStatus qfeEvolveParticles();
  qfeReturnStatus qfeRenderParticles(qfeColorMap *colormap);

  qfeReturnStatus qfeSetTraceSteps(int steps);  
  qfeReturnStatus qfeSetTraceDirection(int dir);
  qfeReturnStatus qfeSetTraceScheme(qfeFlowParticlesIntegrationScheme scheme);
  qfeReturnStatus qfeSetTraceSpeed(int percentage);
  qfeReturnStatus qfeSetTraceLifeTime(int lifetime);
  qfeReturnStatus qfeSetTrailStyle(qfeFlowParticlesTrailType style);  
  qfeReturnStatus qfeSetTrailLifeTime(float lifeTime);
  qfeReturnStatus qfeSetPhaseDuration(qfeFloat duration);  
  qfeReturnStatus qfeSetParticleStyle(qfeFlowParticlesStyle style);
  qfeReturnStatus qfeSetParticleColor(qfeFlowParticlesColor color);
  qfeReturnStatus qfeSetParticleShading(qfeFlowParticlesShading shading);
  qfeReturnStatus qfeSetParticleContour(bool contour);  
  qfeReturnStatus qfeSetParticleSize(int size);
  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);
  qfeReturnStatus qfeSetColorScale(int scale);  
  

protected:
  // x, y, z    - position (in patient coordinates)
  // t          - particle time (in phases)
  // age        - particle age (in phases, -1 = death)
  // speed      - particle velocity
  // residence  - particle residence time
  // birth      - age of birth
  struct qfeParticle
  {
    qfeFloat x, y, z, t;
    qfeFloat age, speed, color, birth;
  };

  qfeReturnStatus qfeDrawParticles(GLuint particles, GLuint attribs, int count);

  qfeReturnStatus qfeInitParticleData();
  qfeReturnStatus qfeClearParticleData();
  qfeReturnStatus qfeInitParticleBuffers();
  qfeReturnStatus qfeClearParticleBuffers();  

  qfeReturnStatus qfeInitNewBornData();
  qfeReturnStatus qfeClearNewBornData();  
  qfeReturnStatus qfeInitNewBornBuffers();
  qfeReturnStatus qfeClearNewBornBuffers();  

  qfeReturnStatus qfeCreateParticleColorMap(GLuint &colormap);
  qfeReturnStatus qfeCreateParticleNormalMap(GLuint &normalmap);

  qfeReturnStatus qfeGetModelViewMatrix();

  qfeReturnStatus qfeAddParticlePositions(qfeSeeds seeds);
  qfeReturnStatus qfeAddParticlePositionsCurrent(qfeSeeds seeds, float phase);
  qfeReturnStatus qfeUpdateParticlePositionsNewBorn(float phase);

  qfeReturnStatus qfeRenderStyleSphere(int currentBuffer);
  qfeReturnStatus qfeRenderStyleEllipsoid(int currentBuffer);
  qfeReturnStatus qfeRenderStyleTrail(int currentBuffer);
  
  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeBindTextureSliding();
  qfeReturnStatus qfeBindTextureRender();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocationsEvolve();
  qfeReturnStatus qfeSetStaticUniformLocationsAdvect();
  qfeReturnStatus qfeSetStaticUniformLocationsRender();
  qfeReturnStatus qfeSetDynamicUniformLocationsEvolve();
  qfeReturnStatus qfeSetDynamicUniformLocationsAdvect();
  qfeReturnStatus qfeSetDynamicUniformLocationsRender();

private :  
  bool                   firstEvolve;
  bool                   firstAdvect;
  bool                   firstRender;  
  qfeGLShaderProgram    *shaderProgramEvolution;
  qfeGLShaderProgram    *shaderProgramAdvection;
  qfeGLShaderProgram    *shaderProgramRenderSphere;
  qfeGLShaderProgram    *shaderProgramRenderEllipsoid;  
  qfeGLShaderProgram    *shaderProgramRenderTrail;

  qfeVolumeSeries        volumeSeries;
  qfeLightSource        *light;
  GLuint                 colormap;

  GLuint                 particlePositionsVBO[2];      
  GLuint                 particlePositionsNewBornVBO;     
  GLuint                 particleAttributesVBO[2];        
  vector< qfeFloat >     particlePositionsCurrent[2];
  vector< qfeFloat >     particlePositionsNewBorn;
  vector< qfeFloat >     particleAttributes[2];  
  unsigned int           particlesCount; 
  unsigned int           setCount;
  vector< qfeColorRGBA > setColor;

  GLuint                 particleColorMap;
  GLuint                 particleNormalMap;

  GLint                  locationPosition;
  GLint                  locationAttribute;
  GLint                  locationPositionBirth;

  bool                   ping;

  static const char     *feedbackVars[];

  // Static uniform locations
  qfeMatrix4f            V2P, P2V;  
  unsigned int           volumeSize[3];    
  qfeFloat               phaseDuration;

  // Dynamic uniform locations
  float                     phaseCurrent;  
  int                       traceSteps;  
  int                       traceStepsPhase; 
  float                     traceStepSize;
  float                     traceStepDuration;
  int                       traceScheme;  
  int                       traceSpeed;
  int                       traceDirection;
  int                       traceLifeTime;  
  qfeFlowParticlesTrailType trailStyle;
  float                     trailLifeTime;
  qfeFlowParticlesStyle     particleStyle;
  qfeFlowParticlesColor     particleColor;
  qfeFlowParticlesShading   particleShading;
  bool                      particleContour;  
  int                       particleSize;  
  int                       colorScale;
  GLfloat                   matrixProjection[16];
  GLfloat                   matrixModelView[16];
  GLfloat                   matrixModelViewInverse[16];
  GLfloat                   matrixModelViewProjection[16];
  GLfloat                   matrixModelViewProjectionInverse[16];
};
