#include "qfeTMIPFilter.h"

qfeTMIPFilter::qfeTMIPFilter() {
  tmip = nullptr;

  size = 0;
  width = 0;
  height = 0;
  depth = 0;

  rangeMin = 0.f;
  rangeMax = 0.f;
  threshold = 0.f;
}

void qfeTMIPFilter::qfeSetData(qfeVolume *tMIP) {
  tmip = tMIP;
  tmip->qfeGetVolumeSize(width, height, depth);
  size = width * height * depth;
  tmip->qfeGetVolumeValueDomain(rangeMin, rangeMax);
}

void qfeTMIPFilter::qfeSetThreshold(float percentage) {
  threshold = percentage;
}

void qfeTMIPFilter::qfeUpdate() {
}

void qfeTMIPFilter::qfeApplyFilter(qfeVolumeSeries *magnitudeData, bool inverted) {
  if (!tmip) return;

  qfeValueType t;
  unsigned short *tmipArray = nullptr;
  tmip->qfeGetVolumeData(t, width, height, depth, (void **)&tmipArray);

  unsigned short absThreshold = (unsigned short)(rangeMin + threshold * (rangeMax - rangeMin));

  for (unsigned fi = 0; fi < (unsigned)magnitudeData->size(); fi++) {
    unsigned short *magnitudes;
    (*magnitudeData)[fi]->qfeGetVolumeData(t, width, height, depth, (void **)&magnitudes);
    for (unsigned vi = 0; vi < size; vi++) {
      if (!inverted && tmipArray[vi] < absThreshold)
        magnitudes[vi] = 0;
      else if (inverted && tmipArray[vi] >= absThreshold)
        magnitudes[vi] = 0;
    }
  }
}