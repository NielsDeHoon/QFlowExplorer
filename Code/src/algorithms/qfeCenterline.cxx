#include "qfeCenterline.h"
//based on: http://www.insight-journal.org/browse/publication/181

const Vec3i qfeCenterline::N6[6] = 
	{
		Vec3i( 0,-1, 0),
		Vec3i( 0, 1, 0),
		Vec3i( 1, 0, 0),
		Vec3i(-1, 0, 0),
		Vec3i( 0, 0, 1),
		Vec3i( 0, 0,-1)
	};

const Vec3i qfeCenterline::N26[26] = 
	{
		//N26
		Vec3i( 1,-1, 1),
		Vec3i( 0,-1, 1),
		Vec3i(-1,-1, 1),
		Vec3i( 1, 0, 1),
		Vec3i( 0, 0, 1),
		Vec3i(-1, 0, 1),
		Vec3i( 1, 1, 1),
		Vec3i( 0, 1, 1),
		Vec3i(-1, 1, 1),
		Vec3i( 1,-1, 0),
		Vec3i( 0,-1, 0),
		Vec3i(-1,-1, 0),
		Vec3i( 1, 0, 0),
		//we do not add the center (since it is not a neighbor)
		Vec3i(-1, 0, 0),
		Vec3i( 1, 1, 0),
		Vec3i( 0, 1, 0),
		Vec3i(-1, 1, 0),
		Vec3i( 1,-1,-1),
		Vec3i( 0,-1,-1),
		Vec3i(-1,-1,-1),
		Vec3i( 1, 0,-1),
		Vec3i( 0, 0,-1),
		Vec3i(-1, 0,-1),
		Vec3i( 1, 1,-1),
		Vec3i( 0, 1,-1),
		Vec3i(-1, 1,-1)
		//as given by: http://ac.els-cdn.com/S104996528471042X/1-s2.0-S104996528471042X-main.pdf?_tid=c6474908-f3d5-11e4-a8d0-00000aab0f01&acdnat=1430906200_2889346ec6e34d4c26b8d6578ebc87e3
	};

void qfeCenterline::qfeInit(Array3f _solid, float _dx, qfeMatrix4f _V2P, qfeMatrix4f _V2T, qfeMatrix4f _T2V)
{
	solid = _solid;
	dx  = _dx;
	V2P = _V2P;
	V2T = _V2T;
	T2V = _T2V;

	centerline_computed = FALSE;
}

void qfeCenterline::qfeRenderCenterline()
{
	if(!centerline_computed)
		this->computeCenterline();

	if(centerline.ni == 0 || centerline.nj == 0 || centerline.nk == 0)
		return;
	
	for(unsigned int k = 1; k<this->centerline.nk-1; k++) for(unsigned int j = 1; j<this->centerline.nj-1; j++) for(unsigned int i = 1; i<this->centerline.ni-1; i++) 
	{
		if(centerline(i,j,k))
		{
			/*
			qfePoint center((float)i/centerline.ni,(float)j/centerline.nj,(float)k/centerline.nk);
			qfePoint point = (center * T2V) * V2P;
			
			glPointSize(3.0f);
			glLineWidth(1.0f);

			
			glBegin(GL_POINTS);
				glColor3f(solid(i,j,k)/0.12f, 0.0f, 0.0f);
				glVertex3f(point.x, point.y, point.z);
			glEnd();
			*/

			//glBegin(GL_LINES);
			//for(unsigned int n = 0; n<N26_size; n++)
			//{
			//	int ni = i+N26[n][0];
			//	int nj = j+N26[n][1];
			//	int nk = k+N26[n][2]; 

			//	if(!centerline(ni,nj,nk))
			//		return;

			//	qfePoint center_n((float)ni/centerline.ni,(float)nj/centerline.nj,(float)nk/centerline.nk);
			//	qfePoint point_n = (center_n * T2V) * V2P;
			//	
			//	glColor3f(0.0f, 0.0f, 0.2f);
			//	glVertex3f(point.x, point.y, point.z);
			//	glVertex3f(point_n.x, point_n.y, point_n.z);
			//}
			//glEnd();

		}
	}
	
}

void qfeCenterline::computeCenterline()
{	
	if(solid.ni == 0 || solid.nj == 0 || solid.nk == 0)
		return;
	
	centerline.resize(solid.ni, solid.nj, solid.nk);

	for(unsigned int k = 0; k<solid.nk; k++) for(unsigned int j = 0; j<solid.nj; j++) for(unsigned int i = 0; i<solid.ni; i++)
	{
		centerline(i,j,k) = FALSE;

		if(solid(i,j,k) >= 0.0f)
		{
			centerline(i,j,k) = TRUE;
		}
	}

	for(unsigned int k = 1; k<solid.nk-1; k++) for(unsigned int j = 1; j<solid.nj-1; j++) for(unsigned int i = 1; i<solid.ni-1; i++)
	{
		centerline(i,j,k) = false;

		int count = 0;

		float phi = solid(i,j,k);
		
		for(unsigned int n = 0; n<N26_size; n++)
		{
			int ni = i+N26[n][0];
			int nj = j+N26[n][1];
			int nk = k+N26[n][2];
			if(solid(ni,nj,nk)>=phi)
				count++;			
		}
		if(count == 0)
			centerline(i,j,k) = true;
		/*else if(count<=1)
		{
			centerline(i,j,k) = true;
		}*/
		else
			centerline(i,j,k) = false;
	}

	bool changes = true;
	while(changes)
	{
		changes = false;
		for(unsigned int k = 1; k<solid.nk-1; k++) for(unsigned int j = 1; j<solid.nj-1; j++) for(unsigned int i = 1; i<solid.ni-1; i++)
		{
			if(!centerline(i,j,k))
				continue;

			float phi = solid(i,j,k);

			Vec3ui n1(0,0,0);

			float max = -1.0f;

			for(unsigned int n = 0; n<N26_size; n++)
			{
				int ni = i+N26[n][0];
				int nj = j+N26[n][1];
				int nk = k+N26[n][2];

				if(solid(ni,nj,nk)>=max && solid(ni,nj,nk)>0.0f)
				{
					max = solid(ni,nj,nk);
					n1[0] = ni; n1[1] = nj; n1[2] = nk;
				}		
			}
			if(!centerline(n1[0], n1[1], n1[2]))
			{
				centerline(n1[0], n1[1], n1[2]) = TRUE;
				changes = true;
			}
		}
	}
	
	//compute components
	std::vector<std::vector<Vec3ui>> components;	
	std::vector<Vec3ui> queue;

	Array3b clustered(solid.ni,solid.nj,solid.nk);
	clustered.set_zero();
	for(unsigned int k = 1; k<solid.nk-1; k++) for(unsigned int j = 1; j<solid.nj-1; j++) for(unsigned int i = 1; i<solid.ni-1; i++)
	{
		if(!centerline(i,j,k))
			continue;

		if(clustered(i,j,k))
			continue;

		std::vector<Vec3ui> component;
		component.push_back(Vec3ui(i,j,k));
		clustered(i,j,k) = true;
		queue.push_back(Vec3ui(i,j,k));

		while(queue.size() != 0)
		{
			Vec3ui point = queue[queue.size()-1];
			for(unsigned int n = 0; n<N26_size; n++)
			{
				int ni = point[0]+N26[n][0];
				int nj = point[1]+N26[n][1];
				int nk = point[2]+N26[n][2];

				if(!centerline.inRange(ni,nj,nk))
					continue;

				if(centerline(ni,nj,nk) && !clustered(ni,nj,nk))
				{
					clustered(ni,nj,nk) = true;
					component.push_back(Vec3ui(ni,nj,nk));
					queue.insert(queue.begin(),Vec3ui(ni,nj,nk));
				}
			}
			queue.pop_back();
		}

		components.push_back(component);
	}
	
	std::cout<<"Nr of components: "<<components.size()<<std::endl;

	//connect components
	std::vector<std::pair<int,Vec3ui>> search_queue;
	while(components.size() > 1)
	{
		std::vector<Vec3ui> component = components[components.size()-1];

		Vec3ui own_point;
		Vec3ui other_point;
		unsigned int other_component_id = 0;
		float min_dist = 1e16;

		bool match_found = false;

		for(unsigned int comp_id = 0; comp_id<components.size()-1; comp_id++)
		{
			std::vector<Vec3ui> other_component = components.at(comp_id);

			for(unsigned int own_i = 0; own_i<component.size(); own_i++)
			{
				Vec3ui p1 = component.at(own_i);

				for(unsigned int oth_i = 0; oth_i<other_component.size(); oth_i++)
				{
					Vec3ui p2 = other_component.at(oth_i);

					float dist = sqrt(
						sqr((float)p2[0]-(float)p1[0]) + 
						sqr((float)p2[1]-(float)p1[1]) + 
						sqr((float)p2[2]-(float)p1[2]));

					//check for solid between points:
					Vec3f direction((float)p2[0]-(float)p1[0],(float)p2[1]-(float)p1[1],(float)p2[2]-(float)p1[2]);
					float step_size = 0.1;
					Vec3ui current = p1;	
					float i = current[0]; float j = current[1]; float k = current[2];
					
					bool not_found = true;
					while(!(current[0] == p2[0] && current[1] == p2[1] && current[2] == p2[2]) //continue if we haven't reached the end point yet
						&& centerline.inRange(current[0],current[1],current[2]) && not_found)
					{	
						int step = 1;

						direction.normalize();
						
						while(floor(i) == current[0] && floor(j) == current[1] && floor(k) == current[2])							
						{
							i+=step*step_size*direction[0];
							j+=step*step_size*direction[1];
							k+=step*step_size*direction[2];
							step++;
						}
						
						current[0] = floor(i); current[1] = floor(j); current[2] = floor(k);
						if(solid.inRange(current[0], current[1], current[2]) && solid(current[0], current[1], current[2])<=0.0f)
						{
							not_found = false;
							dist = min_dist+1.0f; //increase the distance to make sure we don't select this point
							break;
						}
					}

					//if the distance is smaller than the current smallest, update the current smallest

					if(dist<min_dist)
					{
						own_point = p1;
						other_point = p2;
						other_component_id = comp_id;
						min_dist = dist;
						match_found = true;
					}
				}
			}
		}

		if(match_found)
		{
			//connect found closet component
			for(unsigned int n = 0; n<component.size(); n++)
			{
				components.at(other_component_id).push_back(component.at(n));
			}
			//points in between to add:
			Vec3f direction((float)other_point[0]-(float)own_point[0],
							(float)other_point[1]-(float)own_point[1],
							(float)other_point[2]-(float)own_point[2]);
			float length = direction.length();
			//normalize
			direction[0] = direction[0]/length; 
			direction[1] = direction[1]/length; 
			direction[2] = direction[2]/length;

			float step_size = 0.1;
			Vec3ui current = own_point;	
			float i = current[0]; float j = current[1]; float k = current[2];

			bool not_found = true;

			while(!(current[0] == other_point[0] && current[1] == other_point[1] && current[2] == other_point[2]) //continue if we haven't reached the end point yet
				&& centerline.inRange(current[0],current[1],current[2]) && not_found)
			{			
				int step = 1;
			
				while(floor(i) == current[0] && floor(j) == current[1] && floor(k) == current[2])
				{
					i+=step*step_size*direction[0];
					j+=step*step_size*direction[1];
					k+=step*step_size*direction[2];
					step++;
				}

				current[0] = floor(i); current[1] = floor(j); current[2] = floor(k);

				if(centerline.inRange(current[0],current[1],current[2]) && !centerline(current[0],current[1],current[2]))
				{
					centerline(current[0],current[1],current[2]) = true;
					components.at(other_component_id).push_back(current);
				}
				for(unsigned int n = 0; n<N26_size; n++)
				{
					int ni = current[0]+N26[n][0];
					int nj = current[1]+N26[n][1];
					int nk = current[2]+N26[n][2];

					if(other_point[0] == ni && other_point[1] == nj && other_point[2] == nk)
					{
						not_found = false;
						break;
					}
				}
			}
		}
		components.pop_back();
	}

	//add better points (with higher phi)
	for(unsigned int k = 1; k<solid.nk-1; k++) for(unsigned int j = 1; j<solid.nj-1; j++) for(unsigned int i = 1; i<solid.ni-1; i++)
	{
		if(!centerline(i,j,k))
			continue;

		float phi = solid(i,j,k);

		for(unsigned int n = 0; n<N26_size; n++)
		{
			int ni = i+N26[n][0];
			int nj = j+N26[n][1];
			int nk = k+N26[n][2];

			if(solid(ni,nj,nk)>phi)
			{
				centerline(ni,nj,nk) = true;
			}
		}
	}

	//filter out unnecessary points
	unsigned int numberOfComponents = computeNumberOfComponents();
	for(unsigned int k = 1; k<solid.nk-1; k++) for(unsigned int j = 1; j<solid.nj-1; j++) for(unsigned int i = 1; i<solid.ni-1; i++)
	{
		if(!centerline(i,j,k))
			continue;

		unsigned int count = 0;
		float phi = solid(i,j,k);

		for(unsigned int n = 0; n<N26_size; n++)
		{
			int ni = i+N26[n][0];
			int nj = j+N26[n][1];
			int nk = k+N26[n][2];

			if(centerline(ni,nj,nk) && solid(ni,nj,nk)<phi )
			{
				centerline(ni,nj,nk) = false;
				if(numberOfComponents != computeNumberOfComponents())
					centerline(ni,nj,nk) = true; //couldn't be deleted without breaking it
			}
		}	
	}
	
	clustered.set_zero();
	for(unsigned int k = 1; k<solid.nk-1; k++) for(unsigned int j = 1; j<solid.nj-1; j++) for(unsigned int i = 1; i<solid.ni-1; i++)
	{
		if(!centerline(i,j,k))
			continue;

		if(clustered(i,j,k))
			continue;

		std::vector<Vec3ui> component;
		component.push_back(Vec3ui(i,j,k));
		clustered(i,j,k) = true;
		queue.push_back(Vec3ui(i,j,k));

		while(queue.size() != 0)
		{
			Vec3ui point = queue[queue.size()-1];
			for(unsigned int n = 0; n<N26_size; n++)
			{
				int ni = point[0]+N26[n][0];
				int nj = point[1]+N26[n][1];
				int nk = point[2]+N26[n][2];

				if(!centerline.inRange(ni,nj,nk))
					continue;

				if(centerline(ni,nj,nk) && !clustered(ni,nj,nk))
				{
					clustered(ni,nj,nk) = true;
					component.push_back(Vec3ui(ni,nj,nk));
					queue.insert(queue.begin(),Vec3ui(ni,nj,nk));
				}
			}
			queue.pop_back();
		}

		components.push_back(component);
	}
	
	std::cout<<"Nr of components: "<<components.size()<<std::endl;

	centerline_computed = TRUE;
}

unsigned int qfeCenterline::computeNumberOfComponents()
{
	//compute components
	std::vector<std::vector<Vec3ui>> components;	
	std::vector<Vec3ui> queue;

	Array3b clustered(solid.ni,solid.nj,solid.nk);
	clustered.set_zero();
	for(unsigned int k = 1; k<solid.nk-1; k++) for(unsigned int j = 1; j<solid.nj-1; j++) for(unsigned int i = 1; i<solid.ni-1; i++)
	{
		if(!centerline(i,j,k))
			continue;

		if(clustered(i,j,k))
			continue;

		std::vector<Vec3ui> component;
		component.push_back(Vec3ui(i,j,k));
		clustered(i,j,k) = true;
		queue.push_back(Vec3ui(i,j,k));

		while(queue.size() != 0)
		{
			Vec3ui point = queue[queue.size()-1];
			for(unsigned int n = 0; n<N26_size; n++)
			{
				int ni = point[0]+N26[n][0];
				int nj = point[1]+N26[n][1];
				int nk = point[2]+N26[n][2];

				if(!centerline.inRange(ni,nj,nk))
					continue;

				if(centerline(ni,nj,nk) && !clustered(ni,nj,nk))
				{
					clustered(ni,nj,nk) = true;
					component.push_back(Vec3ui(ni,nj,nk));
					queue.insert(queue.begin(),Vec3ui(ni,nj,nk));
				}
			}
			queue.pop_back();
		}

		components.push_back(component);
	}
	
	return (unsigned int) components.size();
}

void qfeCenterline::qfeGetNearestCenterlinePoint(Vec3ui inputCell, Vec3ui &outputCell)
{
	float minDist = 1e16;
	for(unsigned int k = 1; k<centerline.nk-1; k++) for(unsigned int j = 1; j<centerline.nj-1; j++) for(unsigned int i = 1; i<centerline.ni-1; i++)
	{
		if(centerline(i,j,k))
		{
			float dist = sqrt(
				sqr((float)i-(float)inputCell[0]) + 
				sqr((float)j-(float)inputCell[1]) + 
				sqr((float)k-(float)inputCell[2]));
			if(minDist>dist)
			{
				outputCell[0] = i; outputCell[1] = j; outputCell[2] = k;

				minDist = dist;
			}
		}
	}
}