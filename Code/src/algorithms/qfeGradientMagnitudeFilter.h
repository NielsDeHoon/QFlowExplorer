#pragma once

#include "qfeAlgorithm.h"
#include "qfeVolume.h"
#include "qfeGradient.h"

/**
* \file   qfeGradientMagnitudeFilter.h
* \author Arjan Broos
* \class  qfeGradientMagnitudeFilter
* \brief  Algorithm that generates the gradients for a given volume series
* \note   Confidential
*
*/
class qfeGradientMagnitudeFilter : public qfeAlgorithm {
public:
  qfeGradientMagnitudeFilter();
  ~qfeGradientMagnitudeFilter();
  void qfeSetData(qfeVolumeSeries *magnitudeData);
  void qfeApplyFilter(qfeVolumeSeries *magnitudeData, bool inverted);

private:
  float **gradients;
  unsigned nrFrames;
  qfeGradient gradient;
};