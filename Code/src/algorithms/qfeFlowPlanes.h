#pragma once

#include "qflowexplorer.h"

#include <vector>

#include "qfeAlgorithmFlow.h"
#include "qfeGLShaderProgram.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeProbe.h"
#include "qfeTransform.h"
#include "qfeVector.h"
#include "qfeVolume.h"

/**
* \file   qfeFlowPlanes.h
* \author Roy van Pelt
* \class  qfeFlowPlanes
* \brief  Implements rendering of flow multiplanar reformat
* \note   Confidential
*
* Rendering of flow multiplanar reformat
*
*/
using namespace std;

class qfeFlowPlanes : public qfeAlgorithmFlow
{
public:
  qfeFlowPlanes();
  qfeFlowPlanes(const qfeFlowPlanes &vv);
  ~qfeFlowPlanes();

  qfeReturnStatus qfeRenderFlowPlanes(qfeProbe2D *probe, qfeVolume *volume);
  qfeReturnStatus qfeRenderFlowPlanesGPU(qfeProbe2D *probe, qfeVolume *volume);
  qfeReturnStatus qfeRenderFlowPlanesCPU(qfeProbe2D *probe, qfeVolume *volume);

  qfeReturnStatus qfeRenderFlowPlanesIntegratedGPU(qfeProbe2D *probe, qfeVolume *volume);
  qfeReturnStatus qfeRenderFlowPlanesExplodedGPU(qfeProbe2D *probe, qfeVolume *volume);

  qfeReturnStatus qfeDrawPlane3D(vector<qfePoint> points, vector<qfePoint> texCoords); 

  qfeReturnStatus qfeSetPlaneType(int type);
  qfeReturnStatus qfeSetPlaneData(int data);
  qfeReturnStatus qfeSetPlaneSize(double size);  
  qfeReturnStatus qfeSetCameraViewUp(qfeVector viewup);

protected:

  qfeReturnStatus qfeCreateTextures();
  qfeReturnStatus qfeDeleteTextures();
  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

private :

  bool                firstRender;
  qfeGLShaderProgram *shaderProgram;
  GLuint              volumeTexId;
  
  qfeVector           paramPlaneNormal;
  qfeVector           paramPlaneVector;

  qfeVector           paramViewUp;
  int                 paramFlowPlaneType;
  int                 paramFlowPlaneData;
  double              paramFlowPlaneSize;
};
