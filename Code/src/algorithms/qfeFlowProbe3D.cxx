#include "qfeFlowProbe3D.h"

//----------------------------------------------------------------------------
qfeFlowProbe3D::qfeFlowProbe3D()
{
  this->appearance    = qfeFlowProbePhong;
  this->light         = NULL;
  this->superSampling = 1;
  this->contour       = true;
  this->probeFit      = false;

  this->shaderRenderProbe = new qfeGLShaderProgram();
  this->shaderRenderProbe->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderRenderProbe->qfeAddShaderFromFile("/shaders/probe/probe-vert.glsl", QFE_VERTEX_SHADER);  
  this->shaderRenderProbe->qfeAddShaderFromFile("/shaders/probe/probe-frag.glsl", QFE_FRAGMENT_SHADER);  
  this->shaderRenderProbe->qfeBindAttribLocation("qfe_Vertex", ATTRIB_VERTEX);
  this->shaderRenderProbe->qfeBindAttribLocation("qfe_Normal", ATTRIB_NORMAL);  
  this->shaderRenderProbe->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderRenderProbe->qfeLink();
};

//----------------------------------------------------------------------------
qfeFlowProbe3D::qfeFlowProbe3D(const qfeFlowProbe3D &fp)
{  
  this->shaderRenderProbe   = fp.shaderRenderProbe;  

  this->appearance    = fp.appearance;
  this->contour       = fp.contour;
  this->superSampling = fp.superSampling;
  this->light         = fp.light;
  this->point1        = fp.point1;
  this->point2        = fp.point2; 
};

//----------------------------------------------------------------------------
qfeFlowProbe3D::~qfeFlowProbe3D()
{
  delete this->shaderRenderProbe;  
};

//----------------------------------------------------------------------------
// Probe axes in patient coordinates
// Probe initial alignment along z-axis
qfeReturnStatus qfeFlowProbe3D::qfeRenderProbe(qfeProbe3D *probe)
{
  GLfloat      transform[16];
  qfeMatrix4f  MV, MVinv, T, R;
  qfeVector    axisU, axisV, axisE, axisT;
  qfeVector    axisX, axisY, axisZ;

  qfeProbe3D inner;
  qfeProbe3D outer;

  // Obtain the current axes
  axisX = probe->axes[0];
  axisZ = probe->axes[2];
  qfeVector::qfeVectorNormalize(axisX);
  qfeVector::qfeVectorNormalize(axisZ);
  qfeVector::qfeVectorOrthonormalBasis1(axisZ, axisX, axisY);
  
  // Set the properties for both layers
  inner = *probe;
  outer = *probe;
  outer.baseRadius += outer.borderWidth;
  outer.topRadius  += outer.borderWidth; 

  // Compute transformation matrices
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  qfeMatrix4f::qfeSetMatrixIdentity(R);

  // Move to the probe origin
  T(3,0) = probe->origin.x;
  T(3,1) = probe->origin.y;
  T(3,2) = probe->origin.z;

  // Transform to the current probe axes
  R(0,0) = axisX.x; 
  R(0,1) = axisX.y;
  R(0,2) = axisX.z;
  R(1,0) = axisY.x; 
  R(1,1) = axisY.y;
  R(1,2) = axisY.z;
  R(2,0) = axisZ.x; 
  R(2,1) = axisZ.y;
  R(2,2) = axisZ.z;

  // Store the probe color
  this->color[0] = probe->color.r;
  this->color[1] = probe->color.g;
  this->color[2] = probe->color.b;
  this->color[3] = probe->color.a;

  qfeMatrix4f::qfeGetMatrixElements(R*T, transform);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glMultMatrixf(transform);   
  
  // Render the contour (first because of blending)
  if(this->contour)
    this->qfeRenderProbeContour(probe, 2.0);
  
  // Render the probe geometry
  // Don't use a shader for flat rendering to allow external shaders
  if(this->appearance != qfeFlowProbeFlat)
  {
    this->qfeSetDynamicUniformLocationsRender();
    this->shaderRenderProbe->qfeEnable();
  }

  glEnable(GL_POLYGON_OFFSET_FILL);

  glPolygonOffset(1.0, 2);

  this->qfeRenderCylinder(&inner, qfeProbeFront);
  this->qfeRenderCylinder(&outer, qfeProbeBack);  
  this->qfeRenderCutPlanes(probe);
  this->qfeRenderCutCaps(probe);

  glDisable(GL_POLYGON_OFFSET_FILL);

  if(this->appearance != qfeFlowProbeFlat)
    this->shaderRenderProbe->qfeDisable();    
 
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  glDisable(GL_BLEND);

  // Render the rotation handle  
  qfeColorRGB col;
  if(probe->selected == true)
  {
    col.r = 0.25f;
    col.g = 0.70f;
    col.b = 0.30f;
  }
  else
  {
    col.r = 0.90f;
    col.g = 0.67f;
    col.b = 0.27f;
  }  
  
  this->qfeRenderProbeHandleRotation(probe, col);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeRenderProbeHandleRotation(qfeProbe3D *probe, qfeColorRGB color)
{
  GLfloat     matrix[16];
  qfeFloat   *sphere;
  int         size;
  GLuint      vbo;
  GLint       sizeBuffer;  
  qfeFloat    radius;
  qfePoint    handleOrigin;
  qfeMatrix4f T;

  if(probe == NULL) return qfeError;

  // Get the sphere
  this->qfeCreateSphere(probe->borderWidth-1, 20, 20, &sphere, &size);

  // Build the translation matrix
  radius = (probe->topRadius + probe->baseRadius) / 2.0f;
  handleOrigin = probe->origin + probe->axes[1] * (radius + 0.5f*probe->borderWidth);

  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) =  handleOrigin.x;
  T(3,1) =  handleOrigin.y;
  T(3,2) =  handleOrigin.z;
  qfeMatrix4f::qfeGetMatrixElements(T, matrix);

  sizeBuffer = size*3*sizeof(GLfloat);

  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeBuffer, sphere, GL_STATIC_DRAW);  

  if(!glIsBuffer(vbo)) return qfeError;
  
  // Start the rendering
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(matrix);
    
  glColor4f(color.r, color.g, color.b, 1.0f);

  // Draw the sphere
  glBindBuffer(GL_ARRAY_BUFFER_ARB, vbo);  
    
  glEnableClientState(GL_NORMAL_ARRAY);
  glEnableClientState(GL_VERTEX_ARRAY);

  glNormalPointer(GL_FLOAT, 0, 0);  
  glVertexPointer(3, GL_FLOAT, 0, 0);  
 
  glDrawArrays(GL_TRIANGLE_STRIP, 0, size);

  glDisableClientState(GL_VERTEX_ARRAY); 
  glDisableClientState(GL_NORMAL_ARRAY);  

  glBindBuffer(GL_ARRAY_BUFFER, 0);  
    
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // Remove the vertex buffer
  glDeleteBuffers(1, &vbo);

  delete [] sphere;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeRenderProbeClipPlane(qfeProbe3D *probe)
{
  qfePoint quad[4];

  quad[0] = probe->origin - 0.5f*probe->length*probe->axes[2] - (probe->baseRadius+probe->borderWidth)*probe->axes[1];
  quad[1] = probe->origin - 0.5f*probe->length*probe->axes[2] + (probe->baseRadius+probe->borderWidth)*probe->axes[1];
  quad[2] = probe->origin + 0.5f*probe->length*probe->axes[2] + (probe->topRadius+probe->borderWidth)*probe->axes[1];
  quad[3] = probe->origin + 0.5f*probe->length*probe->axes[2] - (probe->topRadius+probe->borderWidth)*probe->axes[1];

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  
  glColor3f(1.0,0.0,0.0);  

  glBegin(GL_QUADS);
    glVertex3f(quad[0].x, quad[0].y, quad[0].z);
    glVertex3f(quad[1].x, quad[1].y, quad[1].z);
    glVertex3f(quad[2].x, quad[2].y, quad[2].z);
    glVertex3f(quad[3].x, quad[3].y, quad[3].z);
  glEnd();
  
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeRenderProbeClipPlane(qfeProbe3D *probe, qfeFrameSlice *slice)
{
  const int   quality = 40;
  qfeFloat    angleStep, angleDelta, delta, scale, distt, distb;
  qfeFloat    rt, rb;
  qfePoint    ot, ob, op, o;
  qfePoint    pt[quality], pb[quality], is[quality];
  qfeVector   np, dp, x, y, z, u, v;
  qfeMatrix4f R, T;

  // collect the required plane properties
  slice->qfeGetFrameOrigin(op.x,op.y,op.z);
  slice->qfeGetFrameAxes(dp.x,dp.y,dp.z,dp.x,dp.y,dp.z,np.x,np.y,np.z);

  // collect the required probe properties
  rt = probe->topRadius;
  rb = probe->baseRadius;
  ot = probe->origin + 0.5f*probe->length*probe->axes[2];
  ob = probe->origin - 0.5f*probe->length*probe->axes[2];

  // Obtain the current origin and axes, and make invariant to roll operation
  o = probe->origin;
  z = probe->axes[2];  
  qfeVector::qfeVectorNormalize(z);
  qfeVector::qfeVectorCross(z, np, y);
  qfeVector::qfeVectorNormalize(y);
  qfeVector::qfeVectorOrthonormalBasis1(z, y, x);
  
  // Compute transformation matrices  
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  qfeMatrix4f::qfeSetMatrixIdentity(R);
  
  T(3,0) = o.x; T(3,1) = o.y; T(3,2) = o.z;

  R(0,0) = x.x; R(1,0) = y.x; R(2,0) = z.x;
  R(0,1) = x.y; R(1,1) = y.y; R(2,1) = z.y;
  R(0,2) = x.z; R(1,2) = y.z; R(2,2) = z.z; 

  angleStep = float(2*PI) / float(quality-1);
  for(int i=0; i<quality; i++)
  {
    pt[i].x = cos(i*angleStep+0.5f*float(PI)) * rt;
    pt[i].y = sin(i*angleStep+0.5f*float(PI)) * rt;
    pt[i].z = 0.5f*probe->length;

    pb[i].x = cos(i*angleStep+0.5f*float(PI)) * rb;
    pb[i].y = sin(i*angleStep+0.5f*float(PI)) * rb;
    pb[i].z = -0.5f*probe->length;

    pt[i]   = pt[i]*(R*T);
    pb[i]   = pb[i]*(R*T);
  }

  // compute the probe-plane intersection
  for(int i=0; i<quality; i++)
  {
    delta    = (np*(op-pt[i])) / (np*(pb[i]-pt[i])); 
    distt    =  np*(pt[i]-op);
    distb    =  np*(pb[i]-op);

    is[i] = pt[i] + delta*(pb[i]-pt[i]);
    
    if((distt < 0 && distb < 0) || (distt > 0 && distb > 0))
    {
      qfeFloat l2t, l2b;
      qfeVector::qfeVectorLength(is[i]-pt[i], l2t);
      qfeVector::qfeVectorLength(is[i]-pb[i], l2b);

      if(abs(distt) < abs(distb))
        is[i] = pt[i] - distt*np;
      else
        is[i] = pb[i] - distb*np;
    }    
  }

  // ensure visibility when center points are close to the plane
  angleDelta = 0.08f;
  if((1.0-abs(x*np)) < angleDelta)
  {
    qfeFloat dist[4];
    dist[0] =  np*(pt[0]-op);
    dist[1] =  np*(pt[quality/2-1]-op);
    dist[2] =  np*(pb[0]-op);
    dist[3] =  np*(pb[quality/2-1]-op);

    is[0] = pt[0]           - dist[0]*np;
    is[1] = pt[quality/2-1] - dist[1]*np;    
    is[2] = pb[quality/2-1] - dist[3]*np;
    is[3] = pb[0]           - dist[2]*np;

    for(int i=4; i<quality; i++)
    {
      is[i] = is[3];     
    }      
  }

  // scale the vertices (assumes probe origin is in the plane)
  qfeVector::qfeVectorCross(y, np, u);
  v = y;     
  scale = 2;
  for(int i=0; i<quality; i++)
  {
    qfeVector dir;
    dir = is[i]-o;
    qfeVector::qfeVectorNormalize(dir);

    if(dir*u > 0)
      is[i] = is[i] + scale*u;
    else
      is[i] = is[i] - scale*u;

    if(dir*v > 0)
      is[i] = is[i] + (probe->borderWidth+scale)*v;
    else
      is[i] = is[i] - (probe->borderWidth+scale)*v;
  }

  glBegin(GL_POLYGON);    
    glColor3f(0.0,1.0,0.0);    
    for(int i=0; i<quality; i++)
      glVertex3f(is[i].x, is[i].y, is[i].z);
  glEnd();


  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Probe axes in patient coordinates
// Probe initial alignment along z-axis
qfeReturnStatus qfeFlowProbe3D::qfeRenderProbeClipVolume(qfeProbe3D *probe)
{
  GLfloat      transform[16];
  qfeMatrix4f  MV, MVinv, T, R;
  qfeVector    axisU, axisV, axisE, axisT;
  qfeVector    axisX, axisY, axisZ;

  qfeProbe3D   clip;  

  // Obtain the current axes
  axisX = probe->axes[0];
  axisZ = probe->axes[2];
  qfeVector::qfeVectorNormalize(axisX);
  qfeVector::qfeVectorNormalize(axisZ);
  qfeVector::qfeVectorOrthonormalBasis1(axisZ, axisX, axisY);
  
  // Set the properties for both layers
  clip = *probe;
  clip.baseRadius += clip.borderWidth+5.0f;
  clip.topRadius  += clip.borderWidth+5.0f; 
  clip.length     += 5.0f;

  // Compute transformation matrices
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  qfeMatrix4f::qfeSetMatrixIdentity(R);

  // Move to the probe origin
  T(3,0) = probe->origin.x;
  T(3,1) = probe->origin.y;
  T(3,2) = probe->origin.z;

  // Transform to the current probe axes
  R(0,0) = axisX.x; 
  R(0,1) = axisX.y;
  R(0,2) = axisX.z;
  R(1,0) = axisY.x; 
  R(1,1) = axisY.y;
  R(1,2) = axisY.z;
  R(2,0) = axisZ.x; 
  R(2,1) = axisZ.y;
  R(2,2) = axisZ.z;

  qfeMatrix4f::qfeGetMatrixElements(R*T, transform);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  //glEnable(GL_BLEND);
  //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glMultMatrixf(transform);   

  // Render the probe geometry  
  //glEnable(GL_POLYGON_OFFSET_FILL);

  //glPolygonOffset(1.0, 2);
  glColor3f(1.0,1.0,1.0);

  this->qfeRenderCylinder(&clip, qfeProbeFront);  
  this->qfeRenderClipCaps(&clip);

  glDisable(GL_POLYGON_OFFSET_FILL);
 
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  //glDisable(GL_BLEND);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeRenderProbeCenterline(qfeProbe3D *probe, int lineWidth)
{
  qfePoint *centerLine = new qfePoint[2];

  this->qfeGetProbeCenterline(probe, 2, &centerLine);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glColor3f(1.0,0.0,0.0);
  glLineWidth(this->superSampling*lineWidth);

  glBegin(GL_LINES);
    glVertex3f(centerLine[0].x, centerLine[0].y, centerLine[0].z);
    glVertex3f(centerLine[1].x, centerLine[1].y, centerLine[1].z);
  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeSetProbeSuperSamplingFactor(qfeFloat factor)
{
  this->superSampling = factor;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeSetProbeAppearance(qfeFlowProbe3DAppearance mode)
{
  this->appearance = mode;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeSetProbeContour(bool contour)
{
  this->contour = contour;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeSetProbeLightSource(qfeLightSource *light)
{
  this->light = light;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeRenderProbeAxes(qfeProbe3D *probe, int lineWidth)
{
  if(probe == NULL) return qfeError;

  const int axisLength = 20;

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  
  glLineWidth(this->superSampling*lineWidth);

  glBegin(GL_LINES);
    glColor3f(1.0,0.0,0.0);
    glVertex3f(probe->origin.x, probe->origin.y, probe->origin.z);
    glVertex3f(probe->origin.x + axisLength*probe->axes[0].x, 
               probe->origin.y + axisLength*probe->axes[0].y, 
               probe->origin.z + axisLength*probe->axes[0].z);
    glColor3f(0.0,1.0,0.0);
    glVertex3f(probe->origin.x, probe->origin.y, probe->origin.z);
    glVertex3f(probe->origin.x + axisLength*probe->axes[1].x, 
               probe->origin.y + axisLength*probe->axes[1].y, 
               probe->origin.z + axisLength*probe->axes[1].z);
    glColor3f(0.0,0.0,1.0);
    glVertex3f(probe->origin.x, probe->origin.y, probe->origin.z);
    glVertex3f(probe->origin.x + axisLength*probe->axes[2].x, 
               probe->origin.y + axisLength*probe->axes[2].y, 
               probe->origin.z + axisLength*probe->axes[2].z);
  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();


  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeGetProbeCenterline(qfeProbe3D *probe, int samples, qfePoint **centerline)
{
  if(probe == NULL) return qfeError;

  qfePoint *p = new qfePoint[samples];

  qfePoint  base = probe->origin - 0.5f*probe->length*probe->axes[2];

  for(int i=0; i<samples; i++)
  {
     p[i] = base + (i/float(samples-1))*probe->length*probe->axes[2];
  }

  *centerline = p;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeGetProbeViewAligned(qfeProbe3D &probe, qfeVector eye)
{
  qfeMatrix4f  MV, MVinv, T, R;
  qfeVector    axisU, axisV, axisE, axisT;
  qfeVector    axisX, axisZ;

  // Initialize vectors
  axisE = -1.0f*eye;  
  axisZ.qfeSetVectorElements(probe.axes[2].x, probe.axes[2].y, probe.axes[2].z);
  qfeVector::qfeVectorNormalize(axisE);
  qfeVector::qfeVectorNormalize(axisZ);

  // Projection of the eye vector in the probe x/y plane
  // This is used to rotate the probe to the viewer
  axisU = (axisE - (axisE*axisZ)*axisZ);

  // Check the extreme case where the projection becomes zero
  if(axisU.x == 0.0 && axisU.y == 0.0 && axisU.z == 0.0)
    axisU = axisX;

  qfeVector::qfeVectorCross(axisZ, axisU, axisV);  
  qfeVector::qfeVectorNormalize(axisU);  
  qfeVector::qfeVectorNormalize(axisV); 

  // Update properties
  probe.axes[0] = axisU;
  probe.axes[1] = axisV;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// angle - in radians [0,2pi]
qfeReturnStatus qfeFlowProbe3D::qfeGetProbeRotation(qfeProbe3D &probe, qfeFloat angle)
{
  qfeMatrix4f R;

  qfeVector u;
  qfeFloat  phi, c, t, s;

  u    = probe.axes[0];
  phi  = angle;
  c    = cos(phi);
  t    = 1.0f - cos(phi);
  s    = sin(phi);

  qfeMatrix4f::qfeSetMatrixIdentity(R);  
  R(0,0) = u.x*u.x*t + c;
  R(0,1) = u.x*u.y*t + u.z*s;
  R(0,2) = u.x*u.z*t - u.y*s;
  R(1,0) = u.x*u.y*t - u.z*s;
  R(1,1) = u.y*u.y*t + c;
  R(1,2) = u.y*u.z*t + u.x*s;
  R(2,0) = u.x*u.z*t + u.y*s;
  R(2,1) = u.y*u.z*t - u.x*s;
  R(2,2) = u.z*u.z*t + c;

  // Update the z-axis
  probe.axes[2] = probe.axes[2] * R;

  // Create new orthogonal basis
  qfeVector::qfeVectorCross(probe.axes[0], -1.0f*probe.axes[2], probe.axes[1]);

  // Normalize new vectors
  qfeVector::qfeVectorNormalize(probe.axes[1]);
  qfeVector::qfeVectorNormalize(probe.axes[2]);

  qfeVector::qfeVectorNormalize(probe.axes[0]);
  qfeVector::qfeVectorNormalize(probe.axes[1]);
  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeGetProbeFitLocal(qfeProbe3D &probe, qfeFlowOctree *octree, qfeMatrix4f mv)
{
  qfePoint    p1, p2, q1, q2, r1, r2, c1, c2;
  qfeFloat    stepSize;
  int         steps;
  qfeFloat    depth, offset, coherenceMax;
  qfeFloat  **costMatrix;
  qfeVector   eyeVec;
  qfeMatrix4f mvInv;
    
  if(octree == NULL) return qfeError;

  this->probeCache = probe;

  // Initialize variables
  p1 = probe.origin + 0.5f*probe.length*probe.axes[2];
  p2 = probe.origin - 0.5f*probe.length*probe.axes[2];

  // Get the eye vector in patient coordinates
  qfeMatrix4f::qfeGetMatrixInverse(mv, mvInv);
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv; 
  qfeVector::qfeVectorNormalize(eyeVec);

  // Get pointsin eye / camera coordinates to determine closest point
  c1 = p1*mv;
  c2 = p2*mv;
  
  // Initialize parameters for optimization  
  offset       = max(probe.baseRadius, probe.topRadius);
  depth        = abs((p2-p1)*eyeVec);
  stepSize     = 1.0f;
  steps        = (int)abs((depth + 2.0f*offset) / stepSize);    
  coherenceMax = 0.0;
  
  if(c1.z > c2.z)
  {
    // p1 is closest, move p2
    p1 = p1 - eyeVec*offset;     
    p2 = p2 - eyeVec*(depth + offset);  
  }
  else
  {
    // p2 is closest, move p1
    p1 = p1 - eyeVec*(depth + offset);  
    p2 = p2 - eyeVec*offset; 
  }

  // Allocate the cost matrix
  costMatrix = (qfeFloat**)malloc(steps*sizeof(qfeFloat));  
  for(int i = 0; i < steps; i++)
   costMatrix[i] = (qfeFloat*) malloc(steps*sizeof(qfeFloat));  

  for(int i=0; i<steps; i++)
  {
    for(int j=0; j<steps; j++)
    {
      qfeFloat coherence;

      q1 = p1 + i*stepSize*eyeVec;
      q2 = p2 + j*stepSize*eyeVec;

      octree->qfeGetLineCoherence(q1, q2, 7, coherence);

      costMatrix[i][j] = coherence;

      if(coherence > coherenceMax) 
      {
        r1            = q1;
        r2            = q2;
        coherenceMax  = coherence;

        this->point1 = q1;
        this->point2 = q2;

        //cout << coherenceMax << endl;
        
        qfeVector newZ;
        qfePoint  newO;
        newZ = r1 - r2;
        newO = r2 + 0.5f*newZ;
        qfeVector::qfeVectorNormalize(newZ);

        probe.axes[2] = newZ;
        probe.origin  = newO;
        this->qfeGetProbeViewAligned(probe, eyeVec);        
      }      
    }    
  }

  this->probeFit = true;
/*
  // Write to file
  ofstream myfile;
  myfile.open ("optimization_space.txt");  
  myfile << "{";
  for(int i=0; i<steps; i++)
  {
    myfile << "{";
    for(int j=0; j<steps; j++)
    {
      if(j == steps-1)
        myfile << costMatrix[i][j];
      else
        myfile << costMatrix[i][j] << ", ";
    }    
    if(i == steps-1) 
      myfile << "}\n";
    else 
      myfile << "},\n";
  }myfile << "}";
  myfile.close();
*/
  // Clean up
  for(int i = 0; i < steps; i++){
   free(costMatrix[i]);
  }
  free(costMatrix);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeGetProbeFitGlobal(qfeProbe3D &probe, qfeVolume *volume, qfeFlowOctree *octree, qfeMatrix4f mv)
{
  qfePoint       top, bottom, p1f, p1b, p2f, p2b, swap;
  qfeFloat       stepSize;
  qfeFloat       depth1, depth2, coherenceMax;
  int            steps1, steps2;
  qfePoint       q1, q2, r1, r2;
  qfeVector      eyeVec;
  qfeFloat     **costMatrix;
  qfeMatrix4f    mvInv;
  qfePoint      *intersectTop, *intersectBottom;
  int            countTop, countBottom;
  
  if(volume == NULL || octree == NULL) return qfeError;

  this->probeCache = probe;
    
  // Initialize variables
  top    = probe.origin + 0.5f*probe.length*probe.axes[2];
  bottom = probe.origin - 0.5f*probe.length*probe.axes[2];

  // Get the eye vector in patient coordinates
  qfeMatrix4f::qfeGetMatrixInverse(mv, mvInv);
  eyeVec.qfeSetVectorElements(0.0f,0.0f,-1.0f);
  eyeVec = eyeVec*mvInv; 
  qfeVector::qfeVectorNormalize(eyeVec);

  // Compute the intersections with the bounding box
  this->qfeGetIntersectionsBoxLine(volume, top,    top+eyeVec,    &intersectTop,    countTop);
  this->qfeGetIntersectionsBoxLine(volume, bottom, bottom+eyeVec, &intersectBottom, countBottom);

  if((countTop < 2) || (countBottom < 2)) return qfeError;

  // Assign the intersection point and sort them according to their depth
  p1f = intersectTop[0];
  p1b = intersectTop[1];
  p2f = intersectBottom[0];
  p2b = intersectBottom[1];

  if((p1f*mv).z < (p1b*mv).z)
  {
    swap = p1f;
    p1f  = p1b;
    p1b  = swap;
  }

  if((p2f*mv).z < (p2b*mv).z)
  {
    swap = p2f;
    p2f  = p2b;
    p2b  = swap;
  }

  // Initialize parameters
  qfeVector::qfeVectorLength(p1f-p1b, depth1);
  qfeVector::qfeVectorLength(p2f-p2b, depth2);
  stepSize      = 1.0f;  
  steps1        = (int)(abs(depth1) / stepSize);  
  steps2        = (int)(abs(depth2) / stepSize);  
  coherenceMax  = 0.0;

  // Allocate the cost matrix
  costMatrix = (qfeFloat**)malloc(steps1*sizeof(qfeFloat));  
  for(int i = 0; i < steps1; i++)
   costMatrix[i] = (qfeFloat*) malloc(steps2*sizeof(qfeFloat));  

  for(int i=0; i<steps1; i++)
  {
    for(int j=0; j<steps2; j++)
    {
      qfeFloat coherence;

      q1 = p1f + i*stepSize*eyeVec;
      q2 = p2f + j*stepSize*eyeVec;

      octree->qfeGetLineCoherence(q1, q2, 5, coherence);

      costMatrix[i][j] = coherence;

      if(coherence > coherenceMax) 
      {
        r1           = q1;
        r2           = q2;
        coherenceMax = coherence;

        //this->point1 = q1;
        //this->point2 = q2;

        //cout << coherence << endl;
        
        qfeVector newZ;
        qfePoint  newO;
        newZ = r1 - r2;
        newO = r2 + 0.5f*newZ;
        qfeVector::qfeVectorNormalize(newZ);

        probe.axes[2] = newZ;
        probe.origin  = newO;
        this->qfeGetProbeViewAligned(probe, eyeVec);
        
      }      
    }    
  }

  this->probeFit = true;
/*
   // Write to file
  ofstream myfile;
  myfile.open ("optimization_space.txt");  
  myfile << "{";
  for(int i=0; i<steps1; i++)
  {
    myfile << "{";
    for(int j=0; j<steps2; j++)
    {
      if(j == steps2-1)
        myfile << costMatrix[i][j];
      else
        myfile << costMatrix[i][j] << ", ";
    }    
    if(i == steps1-1) 
      myfile << "}\n";
    else 
      myfile << "},\n";
  }myfile << "}";
  myfile.close();
*/
  // Clean up
  for(int i = 0; i < steps1; i++){
   free(costMatrix[i]);
  }
  free(costMatrix);

  delete [] intersectTop;
  delete [] intersectBottom;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeGetProbeFitUndo(qfeProbe3D &probe)
{
  if(this->probeFit)
  {
    probe = this->probeCache;
    return qfeSuccess;
  }

  return qfeError;  
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeRenderCylinder(qfeProbe3D *probe, qfeFlowProbeSide side)
{
  GLuint     vbo;
  GLfloat   *vertices;
  GLfloat   *normals;
  GLubyte   *topology;
  GLint      size, tsize; 
  GLint      sizeBuffer;  
  
  qfeMatrix4f M;
  GLfloat     matrix[16];

  qfeFloat    theta;

  // Get the cylinder geometry and topology
  qfeCreateCylinderGeometry(probe, side, &vertices, &normals, &size); 
  qfeCreateCylinderTopology(probe, &topology, &tsize);

  // Create transformation matrix 
  // Rotate to required system and translate to the origin
  theta = -1.0f*(float(PI) / 2.0f);
  qfeMatrix4f::qfeSetMatrixIdentity(M);
  M(0,0) =  cos(theta); 
  M(0,1) =  sin(theta)*-1.0f;
  M(1,0) =  sin(theta); 
  M(1,1) =  cos(theta);
  M(3,2) = -0.5f*probe->length;
  
  qfeMatrix4f::qfeGetMatrixElements(M, matrix);

  // Create the vertex buffer
  sizeBuffer = size*3*sizeof(GLfloat);

  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, 2*sizeBuffer, 0, GL_STATIC_DRAW);  
  glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, 0, sizeBuffer, vertices);            
  glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, sizeBuffer, sizeBuffer, normals);  

  // Start the rendering
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(matrix);

  // Obtain the modelview matrix
  this->qfeGetModelViewMatrix();
  
  // Draw the cylinder
  glBindBuffer(GL_ARRAY_BUFFER_ARB, vbo);  
    
  glEnableVertexAttribArray(ATTRIB_VERTEX);
  glEnableVertexAttribArray(ATTRIB_NORMAL);

  glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  glVertexAttribPointer(ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeBuffer));
  
  glDrawElements(GL_TRIANGLE_STRIP, tsize, GL_UNSIGNED_BYTE, topology);  
  
  glDisableVertexAttribArray(ATTRIB_NORMAL); 
  glDisableVertexAttribArray(ATTRIB_VERTEX);  

  glBindBuffer(GL_ARRAY_BUFFER, 0);  
    
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // Remove the vertex buffer
  glDeleteBuffers(1, &vbo);

  delete [] vertices;
  delete [] normals;
  delete [] topology;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeRenderCutPlanes(qfeProbe3D *probe)
{
  GLfloat *p1,*p2;
  GLfloat *n1,*t1;
  GLint    size;
  qfeFloat theta;

  this->qfeCreateCutPlanes(probe, &p1, &p2, &n1, &t1, size);

  // By default, the cylinder is rendered along the z-axis
  // Create transformation matrix towards the view vector
  GLfloat transform[16];
  qfeMatrix4f  T;

  theta = -1.0f*(float(PI) / 2.0f);
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(0,0) =  cos(theta); 
  T(0,1) =  sin(theta)*-1.0f;
  T(1,0) =  sin(theta); 
  T(1,1) =  cos(theta);
  T(3,2) = -0.5f*probe->length;

  qfeMatrix4f::qfeGetMatrixElements(T, transform);
  
  // Start the rendering
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(transform);

  // Draw the first plane
  glColor4f(probe->color.r, probe->color.g, probe->color.b, probe->color.a);

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  glEnableClientState( GL_TEXTURE_COORD_ARRAY );
      
  // Plane
  glVertexPointer(3, GL_FLOAT, 0, p1); 
  glNormalPointer(GL_FLOAT, 0, n1);
  glTexCoordPointer(2, GL_FLOAT, 0, t1);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);  

  glVertexPointer(3, GL_FLOAT, 0, p2);  
  glNormalPointer(GL_FLOAT, 0, n1);
  glTexCoordPointer(2, GL_FLOAT, 0, t1);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  
  glDisableClientState( GL_TEXTURE_COORD_ARRAY );
  glDisableClientState( GL_NORMAL_ARRAY );
  glDisableClientState(GL_VERTEX_ARRAY);
  
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // Clear the buffers
  delete p1;
  delete p2;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeRenderCutCaps(qfeProbe3D *probe)
{
  GLfloat *c1, *c2;
  GLfloat *n1, *n2;
  GLint    size;
  qfeFloat theta;

  this->qfeCreateCutCaps(probe, &c1, &c2, &n1, &n2, size);

  // By default, the cylinder is rendered along the z-axis
  // Create transformation matrix towards the view vector
  GLfloat transform[16];
  qfeMatrix4f  T;

  theta = -1.0f*(float(PI) / 2.0f);
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(0,0) =  cos(theta); 
  T(0,1) =  sin(theta)*-1.0f;
  T(1,0) =  sin(theta); 
  T(1,1) =  cos(theta);
  T(3,2) = -0.5f*probe->length;

  qfeMatrix4f::qfeGetMatrixElements(T, transform);
  
  // Start the rendering
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(transform);

  // Draw the first plane
  glColor4f(probe->color.r, probe->color.g, probe->color.b, probe->color.a);

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  
  // Plane
  glVertexPointer(3, GL_FLOAT, 0, c1);    
  glNormalPointer(GL_FLOAT, 0, n1);    
  glDrawArrays(GL_TRIANGLE_STRIP, 0, ((probe->numSlices/2)+1)*2);  

  glVertexPointer(3, GL_FLOAT, 0, c2);  
  glNormalPointer(GL_FLOAT, 0, n2);    
  glDrawArrays(GL_TRIANGLE_STRIP, 0, ((probe->numSlices/2)+1)*2);  

  glDisableClientState(GL_NORMAL_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);
  
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // Clear the buffers
  delete c1;
  delete c2;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeRenderClipCaps(qfeProbe3D *probe)
{
  qfePoint  p[4], t[22], b[22], o;
  qfeVector x, y, z;
  qfeFloat  theta;
  qfeFloat  quality, angle, alpha, beta, radius;
  qfePoint  to, bo;

  o.qfeSetPointElements(0.0,0.0,0.5*probe->length);
  x.qfeSetVectorElements(1.0,0.0,0.0);
  y.qfeSetVectorElements(0.0,1.0,0.0);
  z.qfeSetVectorElements(0.0,0.0,1.0);

  // Compute the large front cap
  p[0] = o - 0.5f*probe->length*z - probe->baseRadius*x;
  p[1] = o + 0.5f*probe->length*z - probe->topRadius *x; 
  p[2] = o + 0.5f*probe->length*z + probe->topRadius *x;
  p[3] = o - 0.5f*probe->length*z + probe->baseRadius*x;

  // Compute the top cap
  quality = 20.0;
  angle   = 180.0f;
  alpha   = (angle/2.0f)*(float(PI)/180.0f);
  beta    = (float(2*PI) - 2.0*alpha)/float(quality);
  radius  = probe->topRadius; 
  to.qfeSetPointElements(0.0,0.0,probe->length);

  t[0]    = to;
  t[1]    = to - y*cos(alpha)*radius + x*sin(alpha)*radius;
  for(int i=1; i<=quality; i++)
    t[i+1] = to - y*cos(alpha+i*beta)*radius + x*sin(alpha+i*beta)*radius;

  // Compute the base cap 
  radius  = probe->baseRadius; 
  to.qfeSetPointElements(0.0,0.0,0.0);

  b[0]    = bo;
  b[1]    = bo - y*cos(alpha)*radius + x*sin(alpha)*radius;
  for(int i=1; i<=quality; i++)
    b[i+1] = bo - y*cos(alpha+i*beta)*radius + x*sin(alpha+i*beta)*radius;

  // By default, the cylinder is rendered along the z-axis
  // Create transformation matrix towards the view vector
  GLfloat transform[16];
  qfeMatrix4f  T;

  theta = -1.0f*(float(PI) / 2.0f);
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(0,0) =  cos(theta); 
  T(0,1) =  sin(theta)*-1.0f;
  T(1,0) =  sin(theta); 
  T(1,1) =  cos(theta);
  T(3,2) = -0.5f*probe->length;

  qfeMatrix4f::qfeGetMatrixElements(T, transform);
  
  // Start the rendering
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(transform);

  glColor4f(probe->color.r, probe->color.g, probe->color.b, probe->color.a);

  // Draw the front plane
  glBegin(GL_QUADS);
    glVertex3f(p[0].x, p[0].y, p[0].z);
    glVertex3f(p[1].x, p[1].y, p[1].z);
    glVertex3f(p[2].x, p[2].y, p[2].z);
    glVertex3f(p[3].x, p[3].y, p[3].z);
  glEnd();

  // Draw the top cap
  glBegin(GL_POLYGON);
  for(int i=0; i<quality+2; i++)
    glVertex3f(t[i].x, t[i].y, t[i].z);    
  glEnd();

  // Draw the base cap
  glBegin(GL_POLYGON);
  for(int i=0; i<quality+2; i++)
    glVertex3f(b[i].x, b[i].y, b[i].z);    
  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeRenderProbeContour(qfeProbe3D *probe, qfeFloat lineWidth)
{
  GLfloat *c1, *c2;
  GLfloat *p1, *p2;
  GLint    size1, size2;
  qfeFloat theta;

  this->qfeCreateCutCapsContours(probe, &c1, &c2, size1);
  this->qfeCreateCutPlanesContours(probe, &p1, &p2, size2);

  // By default, the cylinder is rendered along the z-axis
  // Create transformation matrix towards the view vector
  GLfloat transform[16];
  qfeMatrix4f  T;

  theta = -1.0f*(float(PI) / 2.0f);
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(0,0) =  cos(theta); 
  T(0,1) =  sin(theta)*-1.0f;
  T(1,0) =  sin(theta); 
  T(1,1) =  cos(theta);
  T(3,2) = -0.5f*probe->length;

  qfeMatrix4f::qfeGetMatrixElements(T, transform);

  // Start the rendering
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(transform);

  glColor4f(0.0,0.0,0.0,1.0);
  
  glEnable(GL_LINE_SMOOTH);  
  glLineWidth(this->superSampling*lineWidth);  
  glEnableClientState(GL_VERTEX_ARRAY);

  // Cut caps
  glVertexPointer(3, GL_FLOAT, 0, c1);    
  glDrawArrays(GL_LINE_STRIP, 0, size1);  

  glVertexPointer(3, GL_FLOAT, 0, c2);  
  glDrawArrays(GL_LINE_STRIP, 0, size1);  

  // Cut planes
  glVertexPointer(3, GL_FLOAT, 0, p1);    
  glDrawArrays(GL_LINE_STRIP, 0, size2);  

  glVertexPointer(3, GL_FLOAT, 0, p2);    
  glDrawArrays(GL_LINE_STRIP, 0, size2);  

  glDisableClientState(GL_VERTEX_ARRAY);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  // Clear the buffers
  delete c1;
  delete c2;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeCreateCylinderGeometry(qfeProbe3D *probe, qfeFlowProbeSide side, GLfloat** vertices, GLfloat **normals, GLint *size)
{       
  GLfloat   theta, radiusStep;  
  int       slices;

  qfePoint  currentVertex;
  qfePoint  neighborVertex[2];
  qfeVector normal;  

  GLfloat  *v;
  GLfloat  *n;
    
  slices        = (int)(probe->numSlices / 2.0f);
  theta         = float(PI) / float(slices);  
  radiusStep    = (probe->topRadius - probe->baseRadius) / float(probe->numStacks-1);        

  *size         = (slices+1) * (probe->numStacks);  
  v             = new GLfloat[*size*3];
  n             = new GLfloat[*size*3];  
  
  for (int i = 0; i < probe->numStacks; i++) 
  {         
    float currentRadius = probe->baseRadius + (radiusStep * float(i));
    float nextRadius    = probe->baseRadius + (radiusStep * float(i+1));    

    float currentZ      = float(i)   * (probe->length / float(probe->numStacks-1)); 
    float nextZ         = float(i+1) * (probe->length / float(probe->numStacks-1));

    for (int j = 0; j <= slices; j++) 
    { 
      qfeVector u, w;  
      float     currentAngle = j * theta;
      float     nextAngle    = (j+1) * theta;
      int       currentIndex = (i*(slices+1)) + j;

      // Compute the vertices
      currentVertex.x = cos(currentAngle) * currentRadius;   
      currentVertex.y = sin(currentAngle) * currentRadius;   
      currentVertex.z = currentZ;  

      // Compute the normal (per vertex)
      neighborVertex[0].x = cos(nextAngle) * currentRadius;  
      neighborVertex[0].y = sin(nextAngle) * currentRadius;   
      neighborVertex[0].z = currentZ;

      neighborVertex[1].x = cos(currentAngle) * nextRadius;  
      neighborVertex[1].y = sin(currentAngle) * nextRadius;   
      neighborVertex[1].z = nextZ;

      u = neighborVertex[0] - currentVertex;
      w = neighborVertex[1] - currentVertex;

      qfeVector::qfeVectorCross(u, w, normal);
      qfeVector::qfeVectorNormalize(normal);      

      // Store the vertex and normal
      v[3*currentIndex + 0 ] = currentVertex.x; 
      v[3*currentIndex + 1 ] = currentVertex.y; 
      v[3*currentIndex + 2 ] = currentVertex.z;       

      if(side == qfeProbeFront)
      {
        n[3*currentIndex + 0 ] = -normal.x; 
        n[3*currentIndex + 1 ] = -normal.y; 
        n[3*currentIndex + 2 ] = -normal.z;   
      }
      else
      {
        n[3*currentIndex + 0 ] = normal.x; 
        n[3*currentIndex + 1 ] = normal.y; 
        n[3*currentIndex + 2 ] = normal.z;   
      }
    }    
  }   

  (*vertices) = v;
  (*normals)  = n; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeCreateCylinderTopology(qfeProbe3D *probe, GLubyte **topology, GLint *tsize)
{       
  GLfloat   theta, radiusStep;  
  int       slices;
  int       currentIndex;
  int       tcount = 0;  

  qfePoint  currentVertex;
  qfePoint  neighborVertex[2];

  GLubyte  *t;
    
  slices        = (int)(probe->numSlices / 2.0f);
  theta         = float(PI) / float(slices);
  radiusStep    = (probe->topRadius - probe->baseRadius) / float(probe->numStacks-1);        
    
  // GL_TRIANGLE_STRIP
     
  *tsize        = (slices+1) * (probe->numStacks-1) * 2;
   t             = new GLubyte[*tsize];

  for (int i = 0; i < probe->numStacks-1; i++) 
  {  
    for (int j = 0; j <= slices; j++) 
    {             
      // Take care of correct winding here
      // Reverse order of strip for each stack
      if(i%2 == 0)
      {
        currentIndex = (i*(slices+1))+j;

        t[2*tcount + 0 ] = currentIndex + slices + 1;
        t[2*tcount + 1 ] = currentIndex;  
      }
      else
      {
        currentIndex = (i*(slices+1))+(slices-j);

        t[2*tcount + 0 ] = currentIndex;
        t[2*tcount + 1 ] = currentIndex + slices + 1;        
      }
    
      tcount ++;        
    }
  } 

  (*topology) = t;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeCreateCutPlanes(qfeProbe3D *probe, GLfloat **plane1, GLfloat**plane2, GLfloat **normals, GLfloat **planeTex, GLint &size)
{
  GLfloat *p1, *p2, *t1, *n1;
  p1  = new GLfloat[4*3];
  p2  = new GLfloat[4*3];
  n1  = new GLfloat[4*3];
  t1  = new GLfloat[4*2];
  //qfeVector u,v,n;

  p1[0*3+0] = -probe->baseRadius-probe->borderWidth; 
  p1[0*3+1] = 0.0f; 
  p1[0*3+2] = 0.0f;
  
  p1[1*3+0] = -probe->topRadius -probe->borderWidth;
  p1[1*3+1] = 0.0f;  
  p1[1*3+2] = probe->length;

  p1[2*3+0] = -probe->baseRadius;
  p1[2*3+1] = 0.0f; 
  p1[2*3+2] = 0.0f;

  p1[3*3+0] = -probe->topRadius;
  p1[3*3+1] = 0.0f;  
  p1[3*3+2] = probe->length;

  p2[0*3+0] = +probe->baseRadius; 
  p2[0*3+1] = 0.0f; 
  p2[0*3+2] = 0.0f;

  p2[1*3+0] = +probe->topRadius;
  p2[1*3+1] = 0.0f; 
  p2[1*3+2] = probe->length;

  p2[2*3+0] = +probe->baseRadius+probe->borderWidth;
  p2[2*3+1] = 0.0f;  
  p2[2*3+2] = 0.0f;

  p2[3*3+0] = +probe->topRadius+probe->borderWidth;
  p2[3*3+1] = 0.0f;  
  p2[3*3+2] = probe->length;

  t1[0*2+0] = 0.0f;
  t1[0*2+1] = 0.0f;  

  t1[1*2+0] = 0.0f;
  t1[1*2+1] = 1.0f;  

  t1[2*2+0] = 1.0f;
  t1[2*2+1] = 0.0f;  

  t1[3*2+0] = 1.0f;
  t1[3*2+1] = 1.0f;  

  for(int i=0; i<4; i++)
  {
    n1[i*3+ 0] =  0.0;
    n1[i*3+ 1] = -1.0;
    n1[i*3+ 2] =  0.0;
  }

  (*plane1)   = p1;
  (*plane2)   = p2;
  (*normals)  = n1;
  (*planeTex) = t1;
  size        = 4;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeCreateCutPlanesContours(qfeProbe3D *probe, GLfloat **plane1, GLfloat**plane2, GLint &size)
{
  GLfloat *p1, *p2;
  p1  = new GLfloat[5*3];
  p2  = new GLfloat[5*3];

  p1[0*3+0] = -probe->baseRadius-probe->borderWidth; 
  p1[0*3+1] = 0.0f; 
  p1[0*3+2] = 0.0f;

  p1[1*3+0] = -probe->topRadius -probe->borderWidth;
  p1[1*3+1] = 0.0f;  
  p1[1*3+2] = probe->length;

  p1[2*3+0] = -probe->topRadius;
  p1[2*3+1] = 0.0f; 
  p1[2*3+2] = probe->length;

  p1[3*3+0] = -probe->baseRadius;
  p1[3*3+1] = 0.0f;  
  p1[3*3+2] = 0.0;

  p1[4*3+0] = -probe->baseRadius-probe->borderWidth; 
  p1[4*3+1] = 0.0f; 
  p1[4*3+2] = 0.0f;

  p2[0*3+0] = +probe->baseRadius; 
  p2[0*3+1] = 0.0f; 
  p2[0*3+2] = 0.0f;

  p2[1*3+0] = +probe->topRadius;
  p2[1*3+1] = 0.0f; 
  p2[1*3+2] = probe->length;

  p2[2*3+0] = +probe->topRadius+probe->borderWidth;
  p2[2*3+1] = 0.0f;  
  p2[2*3+2] = probe->length;

  p2[3*3+0] = +probe->baseRadius+probe->borderWidth;
  p2[3*3+1] = 0.0f;  
  p2[3*3+2] = 0.0f;

  p2[4*3+0] = +probe->baseRadius; 
  p2[4*3+1] = 0.0f; 
  p2[4*3+2] = 0.0f;

  (*plane1)   = p1;
  (*plane2)   = p2;
  size        = 5;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeCreateCutCaps(qfeProbe3D *probe, GLfloat **baseCap, GLfloat**topCap, GLfloat **baseNormals, GLfloat **topNormals, GLint &size)
{
  int      slices;
  GLfloat *base, *baseNorm;
  GLfloat *top, *topNorm;

  GLfloat   theta;  

  qfePoint  innerVertex, outerVertex;
  qfePoint  neighborVertex[2];
  qfeVector normal;  
   
  slices   = (int)(probe->numSlices / 2.0f);
  theta    = float(PI) / float(slices);

  base     = new GLfloat[(slices+1) * 2 * 3];  
  top      = new GLfloat[(slices+1) * 2 * 3];  
  baseNorm = new GLfloat[(slices+1) * 2 * 3];
  topNorm  = new GLfloat[(slices+1) * 2 * 3];
         
  for (int j = 0; j <= slices; j++) 
  { 
    qfeVector u, w;  
    float     currentAngle = j * theta;

    // Compute the base strip
    innerVertex.x = cos(currentAngle) * probe->baseRadius;   
    innerVertex.y = sin(currentAngle) * probe->baseRadius;   
    innerVertex.z = 0.0f;  

    outerVertex.x = cos(currentAngle) * (probe->baseRadius+probe->borderWidth);   
    outerVertex.y = sin(currentAngle) * (probe->baseRadius+probe->borderWidth);   
    outerVertex.z = 0.0f;  

    // Store the vertices
    base[6*j + 0 ] = outerVertex.x; 
    base[6*j + 1 ] = outerVertex.y; 
    base[6*j + 2 ] = outerVertex.z;     

    base[6*j + 3 ] = innerVertex.x; 
    base[6*j + 4 ] = innerVertex.y; 
    base[6*j + 5 ] = innerVertex.z;   

    baseNorm[6*j + 0] = baseNorm[6*j + 3 ] =  0.0;
    baseNorm[6*j + 1] = baseNorm[6*j + 4 ] =  0.0;
    baseNorm[6*j + 2] = baseNorm[6*j + 5 ] = -1.0;
    
    // Compute the top strip
    innerVertex.x = cos(currentAngle) * probe->topRadius;   
    innerVertex.y = sin(currentAngle) * probe->topRadius;   
    innerVertex.z = probe->length;  

    outerVertex.x = cos(currentAngle) * (probe->topRadius+probe->borderWidth);   
    outerVertex.y = sin(currentAngle) * (probe->topRadius+probe->borderWidth);   
    outerVertex.z = probe->length;  

    // Store the vertices
    top[6*j + 0 ] = outerVertex.x; 
    top[6*j + 1 ] = outerVertex.y; 
    top[6*j + 2 ] = outerVertex.z;     

    top[6*j + 3 ] = innerVertex.x; 
    top[6*j + 4 ] = innerVertex.y; 
    top[6*j + 5 ] = innerVertex.z;     

    topNorm[6*j + 0] = topNorm[6*j + 3 ] = 0.0;
    topNorm[6*j + 1] = topNorm[6*j + 4 ] = 0.0;
    topNorm[6*j + 2] = topNorm[6*j + 5 ] = 1.0;
  }    

  (*baseCap)     = base;
  (*topCap)      = top;
  (*baseNormals) = baseNorm;
  (*topNormals)  = topNorm;
  size           = (GLint)((slices/2)+1)*4;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeCreateCutCapsContours(qfeProbe3D *probe, GLfloat **baseCap, GLfloat**topCap, GLint &size)
{
  int      slices;
  GLfloat *base;
  GLfloat *top;

  GLfloat   theta;  

  qfePoint  innerVertex, outerVertex;
  qfePoint  neighborVertex[2];
  qfeVector normal;  

  slices  = (int)(probe->numSlices / 2.0f);
  theta   = float(PI) / float(slices);

  base     = new GLfloat[(2*slices+3) * 3];  
  top      = new GLfloat[(2*slices+3) * 3];

  for (int j = 0; j <= slices; j++) 
  { 
    qfeVector u, w;  
    float     currentAngle = j * theta;

    // Compute the base strip
    innerVertex.x = cos(currentAngle) * probe->baseRadius;   
    innerVertex.y = sin(currentAngle) * probe->baseRadius;   
    innerVertex.z = 0.0f;  

    outerVertex.x = cos(currentAngle) * (probe->baseRadius+probe->borderWidth);   
    outerVertex.y = sin(currentAngle) * (probe->baseRadius+probe->borderWidth);   
    outerVertex.z = 0.0f;  

    // Store the vertices
    base[3*j + 0 ] = outerVertex.x; 
    base[3*j + 1 ] = outerVertex.y; 
    base[3*j + 2 ] = outerVertex.z;     

    base[3*(slices+1) + 3*(slices-j) + 0 ] = innerVertex.x; 
    base[3*(slices+1) + 3*(slices-j) + 1 ] = innerVertex.y; 
    base[3*(slices+1) + 3*(slices-j) + 2 ] = innerVertex.z;   

    // Compute the top strip
    innerVertex.x = cos(currentAngle) * probe->topRadius;   
    innerVertex.y = sin(currentAngle) * probe->topRadius;   
    innerVertex.z = probe->length;  

    outerVertex.x = cos(currentAngle) * (probe->topRadius+probe->borderWidth);   
    outerVertex.y = sin(currentAngle) * (probe->topRadius+probe->borderWidth);   
    outerVertex.z = probe->length;  

    // Store the vertices
    top[3*j + 0 ] = outerVertex.x; 
    top[3*j + 1 ] = outerVertex.y; 
    top[3*j + 2 ] = outerVertex.z;     

    top[3*(slices+1) + 3*(slices-j) + 0 ] = innerVertex.x; 
    top[3*(slices+1) + 3*(slices-j) + 1 ] = innerVertex.y; 
    top[3*(slices+1) + 3*(slices-j) + 2 ] = innerVertex.z;       
  }  

  // closing point
  base[3*(2*slices+2) + 0 ] = base[0 + 0]; 
  base[3*(2*slices+2) + 1 ] = base[0 + 1]; 
  base[3*(2*slices+2) + 2 ] = base[0 + 2];   

  top[3*(2*slices+2) + 0 ] = top[0 + 0]; 
  top[3*(2*slices+2) + 1 ] = top[0 + 1]; 
  top[3*(2*slices+2) + 2 ] = top[0 + 2];   

  (*baseCap) = base;
  (*topCap)  = top;
  size = (GLint)2*slices+3;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeCreateSphere(qfeFloat radius, qfeFloat slices, qfeFloat stacks, GLfloat**sphere, GLint *size)
{     
  GLfloat  *s;
  int       count;

  s     = new GLfloat[stacks*(slices+1)*2.0f*3.0f];
  count = 0;

  for(int lat=0; lat<stacks; lat++) 
  {
	  for(int lon=0; lon<=slices; lon++) 
    {
      double la1, la2, lo;

      la1 = 2.0f*float(PI)*lat/stacks;
      la2 = 2.0f*float(PI)*(lat+1)/stacks;
      lo  = float(PI)*lon/slices;
       
      s[3*count + 0] = radius*cos(lo)*sin(la1);
      s[3*count + 1] = radius*sin(lo)*sin(la1);
      s[3*count + 2] = radius*cos(la1);

      count++;

      s[3*count + 0] = radius*cos(lo)*sin(la2);
      s[3*count + 1] = radius*sin(lo)*sin(la2);
      s[3*count + 2] = radius*cos(la2);

      count++;
    }  	
  }  

  *size       = (GLint)(stacks*(slices+1))*2;
  *sphere     = s;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// p - patient coordinates
qfeReturnStatus qfeFlowProbe3D::qfeGetInsideBox(qfeVolume *volume, qfePoint p, bool &inside)
{
  qfeMatrix4f      P2V;
  qfeGrid         *grid;
  qfePoint         pVoxel;
  unsigned int     size[3];

  if(volume == NULL) return qfeError; 

  // Get transformation patient to voxel
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  // Get volume properties
  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(size[0],size[1],size[2]);

  // Transform the coordinate
  pVoxel = p * P2V;

  // Check if the point is in the bounding box
  inside = false;

  if((ceil(pVoxel.x) >=0) && (floor(pVoxel.x) <= size[0]) &&
     (ceil(pVoxel.y) >=0) && (floor(pVoxel.y) <= size[1]) &&
     (ceil(pVoxel.z) >=0) && (floor(pVoxel.z) <= size[2]))
  {    
    inside = true;
  }
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// p1, p2 - points on a line in patient coordinates
qfeReturnStatus qfeFlowProbe3D::qfeGetIntersectionsBoxLine(qfeVolume *volume, qfePoint p1, qfePoint p2, qfePoint **intersections, int &count)
{
  const int        boxSides = 6;

  qfeVector        normals[boxSides];
  qfePoint         origins[boxSides];
  vector<qfePoint> intersect;

  qfeGrid         *grid;
  unsigned int     size[3];
  qfePoint         o, p;
  qfeVector        e, axes[3];
  qfeFloat         nom, den, u;
  bool             inside;

  if(volume == NULL) return qfeError;

  qfeMatrix4f P2V;
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  // Get volume properties
  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(size[0],size[1],size[2]);
  grid->qfeGetGridOrigin(o.x, o.y, o.z);
  grid->qfeGetGridExtent(e.x, e.y, e.z);
  grid->qfeGetGridAxes(axes[0].x, axes[0].y, axes[0].z, axes[1].x, axes[1].y, axes[1].z, axes[2].x, axes[2].y, axes[2].z);

  // Compute the normals of the bounding box sides
  qfeVector::qfeVectorCross(axes[0], axes[1], normals[0]);
  qfeVector::qfeVectorCross(axes[0], axes[2], normals[2]);
  qfeVector::qfeVectorCross(axes[1], axes[2], normals[4]);
  normals[1] = -1.0f * normals[0];
  normals[3] = -1.0f * normals[2];
  normals[5] = -1.0f * normals[4];

  for(int i=0; i<boxSides; i++)
    qfeVector::qfeVectorNormalize(normals[i]);

  // Compute the origins of the bounding box sides
  origins[0] = o + e.z*0.5f*size[2]*axes[2];
  origins[1] = o - e.z*0.5f*size[2]*axes[2];
  origins[2] = o - e.y*0.5f*size[1]*axes[1];
  origins[3] = o + e.y*0.5f*size[1]*axes[1];
  origins[4] = o + e.x*0.5f*size[0]*axes[0];
  origins[5] = o - e.x*0.5f*size[0]*axes[0];

  // Compute the intersections between the line p1-p2 and the planes
  for(int i=0; i<boxSides; i++)
  {
    den = normals[i] * (p2-p1);
    // Check if the line is parallel to the plane
    if(den != 0)
    {
      nom = normals[i] * (origins[i] - p1);
      u   = nom / den;
      p   = p1 + u*(p2-p1);

      this->qfeGetInsideBox(volume, p, inside);

      if(inside) 
        intersect.push_back(p);
    }
  } 

  // Output the intersection
  count = (int)intersect.size();
  
  (*intersections) = new qfePoint[count];
  for(int i=0; i<count; i++)
    (*intersections)[i] = intersect[i];

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeRenderBoundingBox(qfeVolume *volume)
{
  qfeMatrix4f   P2W, V2P, T2V, M;
  GLfloat       matrix[16];

  qfeTransform::qfeGetMatrixPatientToWorld(P2W);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  M = T2V*V2P*P2W;
  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();
  glMultMatrixf(&matrix[0]);  // Texture to World   Coordinates

  glBegin(GL_QUADS);

  // Front
  glNormal3f(0.0f, 0.0f, -1.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);

  // Back
  glNormal3f(0.0f, 0.0f, 1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f, 1.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f, 1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f,  1.0f, 1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f, 1.0f);

  // Left
  glNormal3f(-1.0f, 0.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f,  1.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);

  // Right
  glNormal3f(1.0f, 0.0f, 0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f(1.0f,  0.0f,  1.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f(1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f(1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f(1.0f,  1.0f,  1.0f);

  // Top
  glNormal3f(0.0f, -1.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f,  1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);

  // Bottom
  glNormal3f(0.0f, 1.0f, 0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f, 1.0f,  1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f, 1.0f,  1.0f);

  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeCreateTextureProbe(qfeFlowProbe3DAppearance appearance)
{
  if (glIsTexture(this->texProbe)) glDeleteTextures(1, (GLuint *)&this->texProbe);

  GLubyte data[32][32][4];
  
  for (int i = 0; i < 32; i++) { 
    for (int j = 0; j < 32; j++) {    
      if(i<2 || i>30 || j<2 || j>30)
      {
       data[i][j][0] = (GLubyte) 0;
       data[i][j][1] = (GLubyte) 0;
       data[i][j][2] = (GLubyte) 0;       
       data[i][j][3] = (GLubyte) 255;
      }
      else
      {
        data[i][j][0] = (GLubyte) 255;
        data[i][j][1] = (GLubyte) 255;
        data[i][j][2] = (GLubyte) 255;       
        data[i][j][3] = (GLubyte) 255;
      }
    }
  } 

  glGenTextures(1, &this->texProbe);
  glBindTexture(GL_TEXTURE_2D, this->texProbe);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, TEX_PROBE_SIZE, TEX_PROBE_SIZE, 0, GL_BGRA, GL_UNSIGNED_BYTE, data);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeDeleteTextureProbe()
{
  if (glIsTexture(this->texProbe)) glDeleteTextures(1, (GLuint *)&this->texProbe);
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe3D::qfeSetDynamicUniformLocationsRender()
{   
  qfePoint  lightSpec;
  qfeVector lightDir;

  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 10.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w); 
    this->light->qfeGetLightSourceDirection(lightDir);
  }
  
  this->shaderRenderProbe->qfeSetUniform4f("light",          lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderRenderProbe->qfeSetUniform4f("lightDirection", lightDir.x, lightDir.y, lightDir.z, lightDir.w);  
  this->shaderRenderProbe->qfeSetUniform4f("color",          this->color[0],this->color[1],this->color[2],this->color[3]);  
  this->shaderRenderProbe->qfeSetUniform1i("appearance",     this->appearance);      
  this->shaderRenderProbe->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection); 
  this->shaderRenderProbe->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse); 


  return qfeSuccess;
}

