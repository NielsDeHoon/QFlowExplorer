#pragma once

#include "qflowexplorer.h"

#include "qfeColorMap.h"
#include "qfeFlowLineTrace.h"
#include "qfeGLShaderProgram.h"
#include "qfeGLSupport.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeMath.h"
#include "qfePoint.h"
#include "qfeSeeding.h"
#include "qfeTransform.h"
#include "qfeVolume.h"

#include <vector>
#include <limits>

#define MAX_PARTICLES 8192

/**
* \file   qfeSimulationLinesComparison.h
* \author Roy van Pelt
* \class  qfeSimulationLinesComparison
* \brief  Implements rendering of a line comparison
* \note   Confidential
*
* Rendering of a line comparison
*/

class qfeSimulationLinesComparison : public qfeFlowLineTrace
{
public:
  enum qfeComparisonColorType
  {
    qfeComparisonColorVelocity,
    qfeComparisonColorDistanceLocal,
    qfeComparisonColorDistanceMean,
    qfeComparisonColorDistanceHausdorff    
  };
  qfeSimulationLinesComparison();
  qfeSimulationLinesComparison(const qfeSimulationLinesComparison &fv);
  ~qfeSimulationLinesComparison();

  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);

  qfeReturnStatus qfeSetVolumeSeries(qfeVolumeSeries first, qfeVolumeSeries second);
  qfeReturnStatus qfeSetSeedsVisible(bool seeds);
  qfeReturnStatus qfeSetLinesFirstVisible(bool lines);  
  qfeReturnStatus qfeSetLinesSecondVisible(bool lines);  
  qfeReturnStatus qfeSetSurfaceVisible(bool surface);
  qfeReturnStatus qfeSetLinesFirstHighlight(bool highlight);
  qfeReturnStatus qfeSetDistanceFilter(int percentage);
  qfeReturnStatus qfeSetColorType(qfeComparisonColorType color);
  qfeReturnStatus qfeSetLinesWidth(qfeFloat width);
  qfeReturnStatus qfeSetLinesLength(qfeFloat _phases);
  
  qfeReturnStatus qfeAddSeedPositions(vector< vector<float> >  seeds, qfeVolume *volume);  
  qfeReturnStatus qfeClearSeedPositions();  

  qfeReturnStatus qfeGeneratePathlines();
  qfeReturnStatus qfeClearPathlines();  
  
  qfeReturnStatus qfeRenderPathlineComparison(qfeColorMap *colormap, qfeFloat currentPhase);

protected:
  qfeReturnStatus qfeInitTraceStatic();

  qfeReturnStatus qfeAddParticlePositions(vector< vector<float> > seeds, qfeVolume *volume);    

  qfeReturnStatus qfeGeneratePathlines(std::vector<float> seeds, qfeVolumeSeries series, int traceSteps, float traceStepSize, float traceStepDuration, qfeMatrix4f P2V, vector<float> &lines);  
  qfeReturnStatus qfeGeneratePathlineAttributes(std::vector<float> linesFirst, std::vector<float> linesSecond, qfeVolumeSeries seriesFirst, qfeVolumeSeries seriesSecond, qfeMatrix4f P2V, vector<float> &attribsFirst, vector<float> &attribsSecond);  

  qfeReturnStatus qfeRenderSeeds(GLuint particles, GLuint attribs, int count);
  qfeReturnStatus qfeRenderPathlines(GLuint lines, GLuint attribs, int count, int size);
  qfeReturnStatus qfeRenderPathSurfaces(GLuint linesFirst, GLuint linesSecond, GLuint attribs, int count, int size);

  qfeReturnStatus qfeDrawParticles(GLuint particles, GLuint attribs, int count);
  qfeReturnStatus qfeDrawLines(GLuint lines, GLuint attribs, int segmentCount, int segmentSize);
  qfeReturnStatus qfeDrawLines(GLuint linesFirst, GLuint linesSecond, GLuint attribs, int segmentCount, int segmentSize);

  qfeReturnStatus qfeComputeHausdorff(vector< qfePoint > line1, vector< qfePoint > line2, float &distance);

  qfeReturnStatus qfeFetchVelocityLinear(qfeVolume *vol, qfePoint p, qfeVector &v);
  qfeReturnStatus qfeFetchVelocityLinearTemporalLinear(qfeVolumeSeries series, qfePoint p, qfeFloat t, qfeVector &v);
  qfeReturnStatus qfeIntegrateRungeKutta4(qfeVolumeSeries series, qfePoint seed, float stepSize, float stepDuration, int stepDirection, int stepModulation, qfeMatrix4f p2v, qfePoint &pos);

  qfeReturnStatus qfeInitBufferStatic(vector<float> *vertices, GLuint &VBO);
  qfeReturnStatus qfeClearBufferStatic(GLuint &VBO);  

  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

private :
  bool                           firstRender;
  bool                           seedsVisible;
  bool                           linesFirstVisible;
  bool                           linesSecondVisible;
  bool                           surfaceVisible;
  int                            distanceFilter;
  qfeComparisonColorType         colorType;

  qfeLightSource                *light;

  qfeVolumeSeries                seriesFirst;
  qfeVolumeSeries                seriesSecond;

  int                            currentTime;
  bool                           currentRenderFirst;

  vector< vector< qfeFloat > >   particlePositions;
  vector< vector< qfeFloat > >   particleAttribs;
  GLuint                         particlePositionsVBO;  
  GLuint                         particleAttribsVBO;

  vector< vector< qfeFloat > >   linePositionsFirst;
  vector< vector< qfeFloat > >   lineAttribsFirst;
  vector< vector< qfeFloat > >   linePositionsSecond;
  vector< vector< qfeFloat > >   lineAttribsSecond;
  GLuint                         linePositionsFirstVBO;
  GLuint                         lineAttribsFirstVBO;
  GLuint                         linePositionsSecondVBO;
  GLuint                         lineAttribsSecondVBO;

  GLuint                         particlesCount;  
  GLuint                         particleTransferFunction; 

  GLint                          locationPosition;
  GLint                          locationPositionSecond;
  GLint                          locationAttribs;  

  qfeFloat                       paramParticleSize;
  qfeFloat                       paramCurrentPhase;
  bool                           paramFirstHighlight;

  qfeFloat						 tracePhases;  
  qfeFloat                       traceStepDurationFirst;
  qfeFloat                       traceStepDurationSecond;
  qfeFloat                       traceStepSizeFirst;
  qfeFloat                       traceStepSizeSecond;

  qfeGLShaderProgram            *shaderProgramRenderSphere;
  qfeGLShaderProgram            *shaderProgramRenderTuboid;  
  qfeGLShaderProgram            *shaderProgramRenderArrows;
  qfeGLShaderProgram            *shaderProgramRenderSurface;
};
