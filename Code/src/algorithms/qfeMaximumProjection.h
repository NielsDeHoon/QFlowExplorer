#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithm.h"
#include "qfeColorMap.h"
#include "qfeDriver.h"
#include "qfeGLShaderProgram.h"
#include "qfeMatrix4f.h"
#include "qfeTransform.h"
#include "qfeVolume.h"

/**
* \file   qfeMaximumProjection.h
* \author Roy van Pelt
* \class  qfeMaximumProjection
* \brief  Implements a planar reformat
* \note   Confidential
*
* Rendering of a maximum intensity projection
*
*/

class qfeMaximumProjection : public qfeAlgorithm
{
public:
  qfeMaximumProjection();
  qfeMaximumProjection(const qfeMaximumProjection &pr);
  ~qfeMaximumProjection();

  qfeReturnStatus qfeRenderMaximumIntensityProjection(GLuint colortex, qfeVolume *volume, qfeColorMap *colormap);

  qfeReturnStatus qfeSetViewportSize(unsigned int x, unsigned int y, int supersampling);
  qfeReturnStatus qfeSetGeometryBuffers(GLuint color, GLuint depth);
  qfeReturnStatus qfeSetDataComponent(int component);
  qfeReturnStatus qfeSetDataRepresentation(int type);
  qfeReturnStatus qfeSetDataRange(qfeFloat vl, qfeFloat vu);
  qfeReturnStatus qfeSetGradient(bool gradient);
  qfeReturnStatus qfeSetTransparency(bool transparency);
  qfeReturnStatus qfeSetContrast(qfeFloat contrast);
  qfeReturnStatus qfeSetBrightness(qfeFloat brightness);

protected:

  qfeReturnStatus qfeCreateTextures();
  qfeReturnStatus qfeDeleteTextures();
  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

private :
  bool                firstRender; 
  qfeGLShaderProgram *shaderMIP;

  unsigned int        viewportSize[2];
  int                 superSampling;

  GLuint              texRayStart;
  GLuint              texRayEnd;  
  GLuint              texVolume;
  GLuint              texColorMap;
  GLuint              texGeoColor;
  GLuint              texGeoDepth;

  int                 paramData;
  int                 paramComponent;
  int                 paramRepresentation;
  qfeFloat            paramDataRange[2];
  float               paramScaling[3];           
  float               paramStepSize;
  int                 paramMode;
  bool                paramGradient;
  bool                paramTransparency;
  qfeFloat            paramContrast;
  qfeFloat            paramBrightness;

  qfeReturnStatus qfeRenderBoundingBox(qfeVolume *volume);

};
