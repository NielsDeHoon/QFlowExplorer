#pragma once

#include "qfeClusterNode.h"

/**
* \file   qfeClusterNodeL2Norm.h
* \author Sander Jacobs
* \class  qfeClusterNodeL2Norm
* \brief  Implementation of a cluster node using the L2-Norm metric.
* \note   Confidential
*/


class qfeClusterNodeL2Norm : public qfeClusterNode
{
public:
  qfeClusterNodeL2Norm(qfeClusterPoint point);
  ~qfeClusterNodeL2Norm();

  qfeReturnStatus qfeMergeRepresentative(qfeClusterNode *c2);
  qfeFloat        qfeComputeDistanceTo(qfeClusterNode *c2);

private:
  qfeVector     meanFlow;
  unsigned int  size;
};





