#include "qfeClusterNodeElliptic4D.h"

//----------------------------------------------------------------------------
//  Constructors
//----------------------------------------------------------------------------
qfeClusterNodeElliptic4D::qfeClusterNodeElliptic4D(qfeClusterPoint point)
{
  this->size = 1;

  for(int i=0; i<4; i++)
    this->meanPosition[i] = point.position[i];

  for(int i=0; i<3; i++)
    this->meanFlow[i] = point.flow[i];

  this->meanFlow[3] = 1;
};

//----------------------------------------------------------------------------
//  Destructor
//----------------------------------------------------------------------------
qfeClusterNodeElliptic4D::~qfeClusterNodeElliptic4D()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeClusterNodeElliptic4D::qfeMergeRepresentative(qfeClusterNode *c2)
{
  qfeClusterNodeElliptic4D *other = (qfeClusterNodeElliptic4D*)c2;

  // TODO: Investigate/minimize effect of limited float accuracy with very different cluster sizes
  qfeFloat thisWeight  = (qfeFloat)(1/(1+(double)other->size/this->size));
  qfeFloat otherWeight  = (qfeFloat)(1/(1+(double)this->size/other->size));

  for(int i=0; i<4; i++)
    this->meanPosition[i] = thisWeight*this->meanPosition[i] + otherWeight*other->meanPosition[i];

  for(int i=0; i<4; i++)
    this->meanFlow[i] = thisWeight*this->meanFlow[i] + otherWeight*other->meanFlow[i];

  this->size += other->size;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeFloat qfeClusterNodeElliptic4D::qfeComputeDistanceTo(qfeClusterNode *c2)
{
  double d = qfeClusterNodeElliptic::B;
  double e = 1-d;

  qfeClusterNodeElliptic4D *other = (qfeClusterNodeElliptic4D*)c2;

  qfeFloat u[4] = {this->meanFlow[0],this->meanFlow[1],this->meanFlow[2],this->meanFlow[3]};
  qfeFloat v[4] = {other->meanFlow[0],other->meanFlow[1],other->meanFlow[2],other->meanFlow[3]};

  qfeFloat b1[4];
  qfeFloat b2[4];
  qfeFloat b3[4];
  qfeFloat b4[4];

  qfeFloat x,y,z,t,lu,lv,lb1,lb2,lb3,lb4;

  qfeFloat p[4];
  for(int i=0; i<4; i++)
    p[i] = this->meanPosition[i] - other->meanPosition[i];

  double dDir,dPos;

  lu = sqrt(u[0]*u[0] + u[1]*u[1] + u[2]*u[2] + u[3]*u[3]);
  lv = sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2] + v[3]*v[3]);

  if(lu>0)
  {
    // Use "this" cluster meanFlow as main basis vector
                             b1[0] = u[0]; b1[1] =  u[1]; b1[2] = u[2]; b1[3] =  u[3];
    if(b1[0]==0 && b1[1]==0){b2[0] = 1;    b2[1] = -1;    b2[2] = 0;    b2[3] =  0;   }
    else                    {b2[0] = u[1]; b2[1] = -u[0]; b2[2] = 0;    b2[3] =  0;   }

    if(b1[2]==0 && b1[3]==0){b3[0] = 0;    b3[1] =  0;    b3[2] = 1;    b3[3] = -1;   }
    else                    {b3[0] = 0;    b3[1] =  0;    b3[2] = u[3]; b3[3] = -u[2];}

    // Fourth basis vector is defined by the cross product
    b4[0] = b1[3]*(b2[2]*b3[1] - b2[1]*b3[2]) + b1[2]*(-(b2[3]*b3[1]) + b2[1]*b3[3]) + b1[1]*(b2[3]*b3[2] - b2[2]*b3[3]);
    b4[1] = b1[3]*(-(b2[2]*b3[0]) + b2[0]*b3[2]) + b1[2]*(b2[3]*b3[0] - b2[0]*b3[3]) + b1[0]*(-(b2[3]*b3[2]) + b2[2]*b3[3]);
    b4[2] = b1[3]*(b2[1]*b3[0] - b2[0]*b3[1]) + b1[1]*(-(b2[3]*b3[0]) + b2[0]*b3[3]) + b1[0]*(b2[3]*b3[1] - b2[1]*b3[3]);
    b4[3] = b1[2]*(-(b2[1]*b3[0]) + b2[0]*b3[1]) + b1[1]*(b2[2]*b3[0] - b2[0]*b3[2]) + b1[0]*(-(b2[2]*b3[1]) + b2[1]*b3[2]);

    lb1 = sqrt(b1[0]*b1[0] + b1[1]*b1[1] + b1[2]*b1[2] + b1[3]*b1[3]);
    lb2 = sqrt(b2[0]*b2[0] + b2[1]*b2[1] + b2[2]*b2[2] + b2[3]*b2[3]);
    lb3 = sqrt(b3[0]*b3[0] + b3[1]*b3[1] + b3[2]*b3[2] + b3[3]*b3[3]);
    lb4 = sqrt(b4[0]*b4[0] + b4[1]*b4[1] + b4[2]*b4[2] + b4[3]*b4[3]);

    // Calculate directional components
    x = (b1[0]*v[0]+b1[1]*v[1]+b1[2]*v[2]+b1[3]*v[3])/lb1;
    y = (b2[0]*v[0]+b2[1]*v[1]+b2[2]*v[2]+b2[3]*v[3])/lb2;
    z = (b3[0]*v[0]+b3[1]*v[1]+b3[2]*v[2]+b3[3]*v[3])/lb3;
    t = (b4[0]*v[0]+b4[1]*v[1]+b4[2]*v[2]+b4[3]*v[3])/lb4;

    dDir = ((lu-x) + 2*sqrt( (lu-x)*(lu-x) + 3*(y*y+z*z+t*t))) / (3*lu);
    if(dDir > qfeClusterNodeElliptic::maxDirectionalCost)
      dDir = qfeClusterNodeElliptic::maxDirectionalCost;


    // Calculate positional component
    x = (b1[0]*p[0]+b1[1]*p[1]+b1[2]*p[2]+b1[3]*p[3])/lb1;
    y = (b2[0]*p[0]+b2[1]*p[1]+b2[2]*p[2]+b2[3]*p[3])/lb2;
    z = (b3[0]*p[0]+b3[1]*p[1]+b3[2]*p[2]+b3[3]*p[3])/lb3;
    t = (b4[0]*p[0]+b4[1]*p[1]+b4[2]*p[2]+b4[3]*p[3])/lb4;
    dPos = sqrt((x*x)/(d*d) + (y*y + z*z + t*t)/(e*e));
    
  }
  else  // lu == 0 -> singularity
  {
    dDir = qfeClusterNodeElliptic::maxDirectionalCost;
    dPos = sqrt(p[0]*p[0]/0.25f + p[1]*p[1]/0.25f + p[2]*p[2]/0.25f + p[3]*p[3]/0.25f);
  }


  if(lv>0)
  {
    // Use "this" cluster meanFlow as main basis vector
                             b1[0] = v[0]; b1[1] =  v[1]; b1[2] = v[2]; b1[3] =  v[3];

    if(b1[0]==0 && b1[1]==0){b2[0] = 1;    b2[1] = -1;    b2[2] = 0;    b2[3] =  0;   }
    else                    {b2[0] = v[1]; b2[1] = -v[0]; b2[2] = 0;    b2[3] =  0;   }

    if(b1[2]==0 && b1[3]==0){b3[0] = 0;    b3[1] =  0;    b3[2] = 1;    b3[3] = -1;   }
    else                    {b3[0] = 0;    b3[1] =  0;    b3[2] = v[3]; b3[3] = -v[2];}

    // Fourth basis vector is defined by the cross product
    b4[0] = b1[3]*(b2[2]*b3[1] - b2[1]*b3[2]) + b1[2]*(-(b2[3]*b3[1]) + b2[1]*b3[3]) + b1[1]*(b2[3]*b3[2] - b2[2]*b3[3]);
    b4[1] = b1[3]*(-(b2[2]*b3[0]) + b2[0]*b3[2]) + b1[2]*(b2[3]*b3[0] - b2[0]*b3[3]) + b1[0]*(-(b2[3]*b3[2]) + b2[2]*b3[3]);
    b4[2] = b1[3]*(b2[1]*b3[0] - b2[0]*b3[1]) + b1[1]*(-(b2[3]*b3[0]) + b2[0]*b3[3]) + b1[0]*(b2[3]*b3[1] - b2[1]*b3[3]);
    b4[3] = b1[2]*(-(b2[1]*b3[0]) + b2[0]*b3[1]) + b1[1]*(b2[2]*b3[0] - b2[0]*b3[2]) + b1[0]*(-(b2[2]*b3[1]) + b2[1]*b3[2]);

    lb1 = sqrt(b1[0]*b1[0] + b1[1]*b1[1] + b1[2]*b1[2] + b1[3]*b1[3]);
    lb2 = sqrt(b2[0]*b2[0] + b2[1]*b2[1] + b2[2]*b2[2] + b2[3]*b2[3]);
    lb3 = sqrt(b3[0]*b3[0] + b3[1]*b3[1] + b3[2]*b3[2] + b3[3]*b3[3]);
    lb4 = sqrt(b4[0]*b4[0] + b4[1]*b4[1] + b4[2]*b4[2] + b4[3]*b4[3]);

    // Calculate directional components
    x = (b1[0]*u[0]+b1[1]*u[1]+b1[2]*u[2]+b1[3]*u[3])/lb1;
    y = (b2[0]*u[0]+b2[1]*u[1]+b2[2]*u[2]+b2[3]*u[3])/lb2;
    z = (b3[0]*u[0]+b3[1]*u[1]+b3[2]*u[2]+b3[3]*u[3])/lb3;
    t = (b4[0]*u[0]+b4[1]*u[1]+b4[2]*u[2]+b4[3]*u[3])/lb4;

    qfeFloat dDir2 = ((lv-x) + 2*sqrt( (lv-x)*(lv-x) + 3*(y*y+z*z+t*t))) / (3*lv);
    dDir += (dDir2 > qfeClusterNodeElliptic::maxDirectionalCost) ? qfeClusterNodeElliptic::maxDirectionalCost : dDir2;

    // Calculate positional component
    x = (b1[0]*p[0]+b1[1]*p[1]+b1[2]*p[2]+b1[3]*p[3])/lb1;
    y = (b2[0]*p[0]+b2[1]*p[1]+b2[2]*p[2]+b2[3]*p[3])/lb2;
    z = (b3[0]*p[0]+b3[1]*p[1]+b3[2]*p[2]+b3[3]*p[3])/lb3;
    t = (b4[0]*p[0]+b4[1]*p[1]+b4[2]*p[2]+b4[3]*p[3])/lb4;
    dPos += sqrt((x*x)/(d*d) + (y*y + z*z + t*t)/(e*e));
    
  }
  else  // lv == 0 -> singularity
  {
    dDir += qfeClusterNodeElliptic::maxDirectionalCost;
    dPos += sqrt(p[0]*p[0]/0.25f + p[1]*p[1]/0.25f + p[2]*p[2]/0.25f + p[3]*p[3]/0.25f);
  }


  // Check for unrealistic distances (mostly infinity)
  if((dDir<100000)==false)
  {
    cout << "qfeClusterNodeElliptic::qfeComputeDistanceTo - Encountered unexpected directional distance ("\
      << dDir << ")" << endl;
  }

  return (qfeFloat) (qfeClusterNodeElliptic::A * (dPos/qfeClusterNodeElliptic::maxSpatialDistance)\
    + (1-qfeClusterNodeElliptic::A) * dDir);
};