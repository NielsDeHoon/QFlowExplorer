#include "qfeVectorMedianFilter.h"

qfeVectorMedianFilter::qfeVectorMedianFilter()
{
}

void qfeVectorMedianFilter::qfeSetData(qfeVolume *volume)
{
}

void qfeVectorMedianFilter::qfeUpdate() {}

void qfeVectorMedianFilter::qfeApplyFilter(qfeVolumeSeries *velocityData, bool inverted)
{
  for (unsigned fi = 0; fi < velocityData->size(); fi++) {
    FilterMedian((*velocityData)[fi]);
  }
}

void qfeVectorMedianFilter::FilterMedian(qfeVolume *volume)
{
  unsigned int dims[3];
  unsigned int components;
  unsigned int readIndex[27];
  unsigned int writeIndex;
  qfeVector    vectors[27];
  qfeFloat     distances[27];
  qfeValueType type;
  void        *voxels;  
  float       *filtered;
  qfeGrid     *grid;

  volume->qfeGetVolumeComponentsPerVoxel(components);  
  volume->qfeGetVolumeData(type, dims[0], dims[1], dims[2], &voxels);
  volume->qfeGetVolumeGrid(&grid);

  if (components != 3 || type != qfeValueTypeFloat)
    return;

  // Allocate the memory
  filtered = (float*)calloc(dims[0]*dims[1]*dims[2], 3*sizeof(float));

  int p = 0; 
  for (int z = 0; z < (int)dims[2]; z++) {
    for (int y = 0; y < (int)dims[1]; y++) {
      for (int x = 0; x < (int)dims[0]; x++) {
        unsigned int valx[3], valy[3], valz[3];

        // Make sure the boundaries conditions are ok
        valx[1] = x;
        valy[1] = y;
        valz[1] = z;
        (x-1 <  (int)0)       ? valx[0] = x : valx[0] = x-1;
        (x+1 >= (int)dims[0]) ? valx[2] = x : valx[2] = x+1;
        (y-1 <  (int)0)       ? valy[0] = y : valy[0] = y-1;
        (y+1 >= (int)dims[1]) ? valy[2] = y : valy[2] = y+1;
        (z-1 <  (int)0)       ? valz[0] = z : valz[0] = z-1;
        (z+1 >= (int)dims[2]) ? valz[2] = z : valz[2] = z+1;

        // Get the readIndex for all neighbors
        int kernelIndex = 0;
        for (int i = 0; i < 3; i++) {
          for (int j = 0; j < 3; j++) {
            for (int k = 0; k < 3; k++) {
              readIndex[kernelIndex] = valx[i] + (valy[j] *dims[0]) + (valz[k]*dims[0]*dims[1]);
              kernelIndex++;
            }
          }
        }

        // Get the vector in the local neighborhood
        for (int i = 0; i < 27; i++) {
          vectors[i].x = *((float*)voxels + 3*readIndex[i]+0);
          vectors[i].y = *((float*)voxels + 3*readIndex[i]+1);
          vectors[i].z = *((float*)voxels + 3*readIndex[i]+2);
        }        

        // Get the distances for each vector
        qfeVector diff;        
        qfeFloat  diffsum;
        for (int i = 0; i < 27; i++) {
          diffsum = 0.0;
          for (int j = 0; j < 27; j++) {             
            // Differences based on L2 norm             
            diff     = vectors[i] - vectors[j];
            diffsum += diff.x*diff.x + diff.y*diff.y + diff.z*diff.z;
          }
          distances[i] = diffsum;           
        }   

        // Find the smallest sum of differences to neighbors
        qfeFloat     min = distances[0];
        unsigned int minIndex = 0;
        for (int i = 0; i < 27; i++) {
          if(distances[i] < min) {  
            min = distances[i];
            minIndex = i;
          }
        }  

        // Place the vector in our new dataset    
        writeIndex = x + (y *dims[0]) + (z *dims[0]*dims[1]);
        *((float*)filtered + 3*writeIndex+0) = vectors[minIndex].x;
        *((float*)filtered + 3*writeIndex+1) = vectors[minIndex].y;
        *((float*)filtered + 3*writeIndex+2) = vectors[minIndex].z;          
      }
    }
  }

  volume->qfeSetVolumeData(type, dims[0], dims[1], dims[2], filtered);  

  // Clean up
  free(filtered);
}