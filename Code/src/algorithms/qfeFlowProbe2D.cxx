#include "qfeFlowProbe2D.h"

//----------------------------------------------------------------------------
qfeFlowProbe2D::qfeFlowProbe2D()
{
  this->currentVolume   = NULL;

  this->color.r         = 0.3;
  this->color.g         = 0.3;
  this->color.b         = 0.3;
  this->color.a         = 1.0;
  this->lineWidth       = 3;

  this->cutSize         = 20; 
  this->sigma           = 0.5;
  this->kernelSize      = 5;
  this->rmax            = 10;
  this->radiusmax       = 1.5 * this->rmax;
};

//----------------------------------------------------------------------------
qfeFlowProbe2D::qfeFlowProbe2D(const qfeFlowProbe2D &fp)
{
  this->cutSize       = fp.cutSize; 
  this->sigma         = fp.sigma;
  this->kernelSize    = fp.kernelSize;
  this->rmax          = fp.rmax;
  this->radiusmax     = fp.radiusmax;

  this->color         = fp.color;
  this->lineWidth     = fp.lineWidth;

  this->probeCache    = fp.probeCache;

  this->currentVolume = fp.currentVolume;
};

//----------------------------------------------------------------------------
qfeFlowProbe2D::~qfeFlowProbe2D()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe2D::qfeRenderProbe(qfeProbe2D probe)
{
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Draw the probe in patient coordinates
  glLineWidth(this->lineWidth);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  this->qfeDrawProbe(probe, this->color);

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND);   

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe2D::qfeRenderProbeAxes(qfeProbe2D probe)
{
  qfeVector x, y, z;
  qfePoint end;

  x = probe.axisX;  qfeVector::qfeVectorNormalize(x);
  y = probe.axisY;  qfeVector::qfeVectorNormalize(y);
  z = probe.normal; qfeVector::qfeVectorNormalize(z);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Draw the probe in patient coordinates
  glLineWidth(this->lineWidth);
  glPointSize(8);

  glEnable(GL_LINE_SMOOTH);

  glColor3f(1.0,0.0,0.0);
  end = probe.origin + 10.0*x;
  glBegin(GL_LINES);
    glVertex3f(probe.origin.x, probe.origin.y, probe.origin.z);
    glVertex3f(end.x, end.y, end.z);
  glEnd();

  glColor3f(0.0,1.0,0.0);
  end = probe.origin + 10.0*y;
  glBegin(GL_LINES);
    glVertex3f(probe.origin.x, probe.origin.y, probe.origin.z);
    glVertex3f(end.x, end.y, end.z);
  glEnd();

  glColor3f(0.0,0.0,1.0);
  end = probe.origin + 10.0*z;
  glBegin(GL_LINES);
    glVertex3f(probe.origin.x, probe.origin.y, probe.origin.z);
    glVertex3f(end.x, end.y, end.z);
  glEnd();

  glDisable(GL_LINE_SMOOTH);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe2D::qfeRenderProbePlane(qfeProbe2D probe)
{
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Draw the probe plane in patient coordinates
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  this->qfeDrawProbePlane(probe, color);

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND);   

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// input s: seed position in voxel coordinates
qfeReturnStatus qfeFlowProbe2D::qfeLocateProbe(qfePoint s, qfeStudy *study, qfeProbe2D &probe)
{ 
  // Get the probe orientation
  qfeGetProbeOrientation(cutSize, sigma, kernelSize, s, study, &probe.axisX, &probe.axisY, &probe.normal); 

  // Get centre and radius and points of probe
  qfePoint r[rays];
  qfeGetProbePoints(s, study->tmip, &probe.axisX, &probe.axisY, &probe.normal, rays, rmax, &probe.origin, &probe.radius, &r[0]);

  // Load V2P to transform voxels to patient data
  qfeMatrix4f V2P;
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, study->tmip);

  // Set the probe properties
  probe.radius   = probe.radius*2;
  probe.area     = PI*probe.radius*probe.radius;
  probe.origin   = probe.origin*V2P;
  probe.normal   = probe.normal*V2P;
  probe.axisX    = probe.axisX *V2P;

  probe.visible  = true;
  probe.selected = true;

  qfeVector::qfeVectorNormalize(probe.normal);
  qfeVector::qfeVectorNormalize(probe.axisX);
  qfeVector::qfeVectorCross(probe.axisX, probe.normal, probe.axisY);

  // Get the points of the probe
  probe.points.clear();
  for(int i=0; i<rays; i++)
  {
    qfePoint p;
    p.x = r[i].x;
    p.y = r[i].y;
    p.z = r[i].z;

    p = p*V2P;

    probe.points.push_back(p);
  }

  probe.quantification.clear();

  /*
  if(this->qfeCheckConvexPolygon(&probe) == qfeSuccess)
    cout << "convex" << endl;
  else
    cout << "ouch" << endl;
  */

  if(this->qfeCheckRadii(&probe, this->radiusmax) == qfeSuccess)
    this->probeCache = probe;
  else
    if((int)this->probeCache.points.size() > 0)
      probe = this->probeCache;
    else
      return qfeError;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe2D::qfeGetProbeOrientation(int cutSize, qfeFloat sigma, int kernelSize, qfePoint s, qfeStudy *study, qfeVector *v1, qfeVector *v2, qfeVector *v3)
{ 
  // Initialize
  unsigned int dimX, dimY, dimZ;
  void *tmipData;
  qfeValueType type;
  study->tmip->qfeGetVolumeData(type,dimX,dimY,dimZ,&tmipData);
  qfeTensor3f tAVG;
  qfeFloat e[9] = {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f};
  qfeTensor3f::qfeSetTensorElements(tAVG, e);  
  int radiusKernel = (kernelSize-1)/2;

  // Check if the gaussian kernel already is computed
  if(study->kernel == NULL)
  { 
	// Compute the kernel
	float  *kernel;
	kernel = (float*)calloc(kernelSize*kernelSize*kernelSize,sizeof(float));
	for (int z = -radiusKernel; z <= radiusKernel; ++z)
	{
	  for (int y = -radiusKernel; y <= radiusKernel; ++y)
	  {
	    for (int x = -radiusKernel; x <= radiusKernel; ++x)
		{
		  int wi = (x+radiusKernel) + (y+radiusKernel)*kernelSize + (z+radiusKernel)*(2*radiusKernel+1)*kernelSize; // write index
		  *(kernel+wi) = -x*pow(sigma,-6.0f)*pow(2*PI,-3.0/2.0f)*exp(-(x*x+y*y+z*z)/(2*sigma*sigma));
		}
	  }
	}
	study->kernel = kernel;
  }

  float  *kernel=study->kernel;

  // Compute the gradients of the extracted subset around the seedpunt with size cutSize*cutSize*cutSize
  for (int z = (int)(s.z-cutSize/2); z < (int)(s.z+cutSize/2); ++z)
  {
    for (int y = (int)(s.y-cutSize/2); y < (int)(s.y+cutSize/2); ++y)
    {
      for (int x = (int)(s.x-cutSize/2); x < (int)(s.x+cutSize/2); ++x)
      {
		// Initialize
        qfeVector gradient;
		gradient.x = gradient.y = gradient.z = 0; 
		qfeTensor3f t;

		// Compute gradients
		if(x>=radiusKernel && y>=radiusKernel && z>=radiusKernel && x<(int)(dimX-radiusKernel) && y<(int)(dimY-radiusKernel) && z<(int)(dimZ-radiusKernel) )
		{
		  for (int zi = -radiusKernel; zi <= radiusKernel; ++zi)
		  {
		    for (int yi = -radiusKernel; yi <= radiusKernel; ++yi)
			{
			  for (int xi = -radiusKernel; xi <= radiusKernel; ++xi)
			  {
			    int rikx = (xi+radiusKernel) + (yi+radiusKernel)*kernelSize + (zi+radiusKernel)*kernelSize*kernelSize; // read index kernel xgradient
				int riky = (yi+radiusKernel) + (xi+radiusKernel)*kernelSize + (zi+radiusKernel)*kernelSize*kernelSize; // read index kernel ygradient
				int rikz = (zi+radiusKernel) + (yi+radiusKernel)*kernelSize + (xi+radiusKernel)*kernelSize*kernelSize; // read index kernel zgradient
				int ri = (x+xi)  + ((y+yi) * dimX) + ((z+zi) *dimX*dimY); // read index tmip data
				int current = *((unsigned short *)tmipData + ri); // value tmip data current pixel

			    gradient.x = gradient.x + *(kernel + rikx) * current; // value kernel x times value tmip
				gradient.y = gradient.y + *(kernel + riky) * current; // value kernel y times value tmip
				gradient.z = gradient.z + *(kernel + rikz) * current; // value kernel z times value tmip
			  }
			}
		  }
		}

		// Compute structure tensor
		qfeMath::qfeComputeStructureTensor(gradient, t);

		// Compute avarage structure tensor
		qfeTensor3f::qfeTensorDivide(t,(float)(cutSize*cutSize*cutSize),t);
		qfeTensor3f::qfeTensorAdd(t,tAVG,tAVG);		
	  }
	}
  }

  // Probe normal is the eigenvector corresponding to the minimum eigenvalue
  qfeVector lambda;
  qfeMath::qfeComputeEigenValues(tAVG,lambda);
  v3->z = ((lambda.z-tAVG(0,0))*(lambda.z-tAVG(1,1))-tAVG(0,1)*tAVG(1,0))/(tAVG(0,2)*tAVG(1,0)+(lambda.z-tAVG(0,0))*tAVG(1,2));
  v3->y = 1;
  if(tAVG(1,0)!=0)
	  v3->x = ((lambda.z-tAVG(1,1))*v3->y-tAVG(1,2)*v3->z)/tAVG(1,0);
  else if(tAVG(2,0)!=0)
	  v3->x = ((lambda.z-tAVG(2,2))*v3->z-tAVG(2,1)*v3->y)/tAVG(2,0);
  else if (lambda.z-tAVG(0,0)!=0)
	  v3->x = (tAVG(0,1)*v3->y+tAVG(0,2)*v3->z)/(lambda.z-tAVG(0,0));
  else
	  return qfeError;
  qfeVector::qfeVectorNormalize(*v3);
  qfeVector::qfeVectorOrthonormalBasis2(*v3,*v2,*v1);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe2D::qfeCheckRadii(qfeProbe2D *probe, float rmax)
{
  float l;

  for(int i=0; i<(int)probe->points.size();i++)
  {
    qfeVector::qfeVectorLength(probe->points[i]-probe->origin, l);

    if(l > rmax)
      return qfeError;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe2D::qfeCheckConvexPolygon(qfeProbe2D *probe)
{
  float     x, y;
  std::vector<qfePoint> points2D; 

  if ((int)probe->points.size() < 3)
    return qfeError;

  // project the points in the x,y plane
  for(int i=0; i<(int)probe->points.size(); i++)
  {
    qfeVector current = probe->points[i] - probe->origin;

    qfeVector::qfeVectorDot(current, probe->axisX, x);
    qfeVector::qfeVectorDot(current, probe->axisY, y);

    points2D.push_back(qfePoint(x,y,0.0));
  }

  // check if the probe is convex
  int   j,k;
  int   flag = 0;
  float z;
  for (int i=0;i<(int)points2D.size();i++) {
    j = (i + 1) % (int)points2D.size();
    k = (i + 2) % (int)points2D.size();
    z  = (points2D[j].x - points2D[i].x) * (points2D[k].y - points2D[j].y);
    z -= (points2D[j].y - points2D[i].y) * (points2D[k].x - points2D[j].x);
    if (z < 0)
       flag |= 1;
    else if (z > 0)
       flag |= 2;
    if (flag == 3)
       return qfeError;
  }
  if (flag != 0)
    return qfeSuccess;
  else
    return qfeError;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe2D::qfeGetProbePoints(qfePoint s, qfeVolume *volume, qfeVector *v1, qfeVector *v2, qfeVector *v3, int rays, int rmax, qfePoint *cAVG, qfeFloat *rAVG, qfePoint *points)
{
  // Initialize
  unsigned int dimX, dimY, dimZ;
  void *data;
  qfeValueType type;
  volume->qfeGetVolumeData(type,dimX, dimY, dimZ,&data); 
  int max = 0;
  int min = 65535;

  // Compute the FWHM=(max+min)/2 criterium which will be used as threshold.
  for (int r=-rmax; r<=rmax; r++)
  {
	  // Check if the current pixel is inside the tmip data
	  if ((s.x + r*(v1->x + v2->x))<0 || (s.x + r*(v1->x + v2->x))>=dimX || (s.y+ r*(v1->y + v2->y))<0 || (s.y+ r*(v1->y + v2->y))>=dimY || (s.z+ r*(v1->z + v2->z))<0 || (s.z+ r*(v1->z + v2->z))>=dimZ)
	  {
		  if (r<0)
		  {
			  r++;
		  }
		  else
		  {
			  break;
		  }
	  }
	  else
	  {
		  int rwi = (int)(s.x + r*(v1->x + v2->x))  + (int)(s.y+ r*(v1->y + v2->y))*dimX + (int)(s.z+ r*(v1->z + v2->z))*dimX*dimY; // read index
		  unsigned short current = *((unsigned short *)data + rwi);
		  if (current>max)
		  {
			  max = current;
		  }

		  if (current<min)
		  {
			  min = current;
		  }
	  }
  }
  float FWHM = (max+min)/2;

  // Estimate the centroid
  // Initialize
  *rAVG = 0;
  int r=1;
  int angle=0;
  qfePoint *centre;
  centre = new qfePoint[0.5*rays+1];
  centre[0] = s;
  qfePoint p1;

  while (angle<(int)(0.5*rays))
  {
	  qfePoint p;
	  p.x = r * v2->x * cos(4*PI/rays*angle) + r * v1->x * sin(4*PI/rays*angle) + centre[angle].x;
	  p.y = r * v2->y * cos(4*PI/rays*angle) + r * v1->y * sin(4*PI/rays*angle) + centre[angle].y;
	  p.z = r * v2->z * cos(4*PI/rays*angle) + r * v1->z * sin(4*PI/rays*angle) + centre[angle].z;
  
	  // Check if the pixel is inside the data volume and r is smaller than rmax.
	  if (p.x>=0 && p.y>=0 && p.z>=0 && p.x<dimX && p.y<dimY && p.z<dimZ && r<rmax && r>-rmax)
	  {
		  // Determine the value of current pixel
		  unsigned short current = *((unsigned short *)data + (int)(p.x)  + (int)(p.y)  * dimX + (int)(p.z)  * dimX*dimY);

		  // Check if the value is smaller than the FWHM, than the edge is detected and r has not longer to be increased. 
		  if (current<FWHM)
		  {
			  // Check if the negative or positive direction is analyzed. First the positive direction
			  // will be analyzed the possition of the point and the radius will be saved, the the negative
			  // direction will be analyzed and the centroid is the avaerage of the point of the negative 
			  // direction and the point of the positive direction.
			  if (r>0)
			  {
				  // Save the point, because this point is neccessary to compute the centroid if the negative direction is analyzed.
				  p1 = p;
				  
				  // Compute the average radius
				  *rAVG = (angle* *rAVG + r)/(angle+1);
				  
				  // Set r to -1 to analyze the negative direction
				  r = -1;
			  }
			  else
			  {
				  // Compute the centroid
				  qfePoint::qfePointMultiply(0.5,(p1+p),centre[angle+1]);
				  
				  // Compute the average radius
				  *rAVG = (angle* *rAVG - r)/(angle+1);
				  
				  // Start with a new angle
				  r = 1;
				  angle++;
			  }
		  }
		  // If the edge is not reached, r has to be increased (or decreased if r<0).
		  else if (r>0)
		  {
			  r++;
		  }
		  else
		  {
			  r--;
		  }
	  }
	  // r is larger than rmax or the pixel is outside the data volume, therefore the segmentation is probably wrong.
	  // If r is greater than the average radius, than the radius will be set to the average radius.
	  else if(r>*rAVG)
	  {
		  // Determine the position of the pixel with the average radius and current angle
		  p.x = *rAVG * v2->x * cos(4*PI/rays*angle) + *rAVG * v1->x * sin(4*PI/rays*angle) + centre[angle].x;
		  p.y = *rAVG * v2->y * cos(4*PI/rays*angle) + *rAVG * v1->y * sin(4*PI/rays*angle) + centre[angle].y;
		  p.z = *rAVG * v2->z * cos(4*PI/rays*angle) + *rAVG * v1->z * sin(4*PI/rays*angle) + centre[angle].z;

		  // Save the point, because this point is neccessary to compute the centroid if the negative direction is analyzed.
		  p1 = p;

		  // Set r to -1 to analyze the negative direction
		  r = -1;
	  }
	  else if(r<-*rAVG)
	  {
		  // Determine the position of the pixel with the average radius and current angle
		  p.x = - *rAVG * v2->x * cos(4*PI/rays*angle) - *rAVG * v1->x * sin(4*PI/rays*angle) + centre[angle].x;
		  p.y = - *rAVG * v2->y * cos(4*PI/rays*angle) - *rAVG * v1->y * sin(4*PI/rays*angle) + centre[angle].y;
		  p.z = - *rAVG * v2->z * cos(4*PI/rays*angle) - *rAVG * v1->z * sin(4*PI/rays*angle) + centre[angle].z;

		  // Compute the centroid
		  qfePoint::qfePointMultiply(0.5,(p1+p),centre[angle+1]);

		  // Start with a new angle
		  r = 1;
		  angle++;
	  }
	  // If r is not larger than the average radius, than the point is outside the volume, but smaller than rAVG. Therefore the average radius
	  // is a bad estimation for the radius and it is better to set the edge of the volume as point.
	  else
	  {
		  if (p.x<0) {p.x=0;}
		  else if (p.x>=dimX) {p.x=dimX-1;}
		  if (p.y<0) {p.y=0;}
		  else if (p.y>=dimY) {p.y=dimY-1;}
		  if (p.z<0) {p.z=0;}
		  else if (p.z<dimZ) {p.z=dimZ-1;}
		  
		  if(r>0)
		  {
			  // Save the point, because this point is neccessary to compute the centroid
			  p1 = p;

			  // Compute the average radius
			  r = sqrt((p.x-cAVG->x)*(p.x-cAVG->x) + (p.y-cAVG->y)*(p.y-cAVG->y) + (p.z-cAVG->z)*(p.z-cAVG->z));
			  *rAVG = (angle* *rAVG - r)/(angle+1);

			  // Set r to -1 to analyze the negative direction
			  r = -1;
		  }
		  else
		  {
			  // Compute the centroid
			  qfePoint::qfePointMultiply(0.5,(p1+p),centre[angle+1]);
	
			  // Compute the average radius
			  r = sqrt((p.x-cAVG->x)*(p.x-cAVG->x) + (p.y-cAVG->y)*(p.y-cAVG->y) + (p.z-cAVG->z)*(p.z-cAVG->z));
			  *rAVG = (angle* *rAVG - r)/(angle+1);
		  
			  // Start with a new angle
			  r = 1;
			  angle++;
		  }
	  }
  }

  // Compute the average of the centerpoints
  cAVG->x=0;
  cAVG->y=0;
  cAVG->z=0;
  for(int i=0;i<(int)(0.5*rays);i++)
  {
	  qfePoint::qfePointMultiply(1/(float)(0.5*rays),centre[i+1],centre[i+1]);
	  qfePoint::qfePointAdd(*cAVG,centre[i+1],*cAVG);
  }

  delete[] centre;

  // The centroid is estimated, now the real segmentation can start
  // Initialize
  *rAVG = 0;
  r=1;
  angle=0;
  unsigned short previous=FWHM;

  while (angle<(int)(0.5*rays))
  {
	  qfePoint p;
	  p.x = r * v2->x * cos(2*PI/rays*angle) + r * v1->x * sin(2*PI/rays*angle) + cAVG->x;
	  p.y = r * v2->y * cos(2*PI/rays*angle) + r * v1->y * sin(2*PI/rays*angle) + cAVG->y;
	  p.z = r * v2->z * cos(2*PI/rays*angle) + r * v1->z * sin(2*PI/rays*angle) + cAVG->z;
  
	  // Check if the pixel is inside the data volume and r is smaller than rmax
	  if (p.x>=0 && p.y>=0 && p.z>=0 && p.x<dimX && p.y<dimY && p.z<dimZ && r<rmax && r>-rmax)
	  {
		  // Interpolation

		  // Get the values of the pixels arround the current pixel
		  unsigned short v000 = *((unsigned short *)data + (int)(p.x)  + (int)(p.y)  * dimX + (int)(p.z)  * dimX*dimY);
		  unsigned short v100 = *((unsigned short *)data + (int)(p.x+1)+ (int)(p.y)  * dimX + (int)(p.z)  * dimX*dimY);
		  unsigned short v010 = *((unsigned short *)data + (int)(p.x)  + (int)(p.y+1)* dimX + (int)(p.z)  * dimX*dimY);
		  unsigned short v001 = *((unsigned short *)data + (int)(p.x)  + (int)(p.y)  * dimX + (int)(p.z+1)* dimX*dimY);
		  unsigned short v101 = *((unsigned short *)data + (int)(p.x+1)+ (int)(p.y)  * dimX + (int)(p.z+1)* dimX*dimY);
		  unsigned short v011 = *((unsigned short *)data + (int)(p.x)  + (int)(p.y+1)* dimX + (int)(p.z+1)* dimX*dimY);
		  unsigned short v110 = *((unsigned short *)data + (int)(p.x+1)+ (int)(p.y+1)* dimX + (int)(p.z)  * dimX*dimY);
		  unsigned short v111 = *((unsigned short *)data + (int)(p.x+1)+ (int)(p.y+1)* dimX + (int)(p.z+1)* dimX*dimY);

		  // Get the local cordinates
		  qfePoint localp;
		  localp.x=p.x-(int)(p.x);
		  localp.y=p.y-(int)(p.y);
		  localp.z=p.z-(int)(p.z);

		  // Get the interpolated value of the current pixel
		  unsigned short current = v000*(1 - localp.x)*(1 - localp.y)*(1 - localp.z) + 
			  v100*localp.x*(1 - localp.y)*(1 - localp.z) + 
			  v010*(1 - localp.x)*localp.y*(1 - localp.z) + 
			  v001*(1 - localp.x)*(1 - localp.y)*localp.z + 
			  v101*localp.x*(1 - localp.y)*localp.z + 
			  v011*(1 - localp.x)*localp.y*localp.z + 
			  v110*localp.x*localp.y*(1 - localp.z) + 
			  v111*localp.x*localp.y*localp.z;

		  // Check if the value is smaller than the FWHM, than the edge is detected and r has not longer to be increased. 
		  if (current<FWHM)
		  {
			  // Check if the negative or positive direction is analyzed. First the positive direction
			  // will be analyzed the possition of the point and the radius will be saved, the the negative
			  // direction will be analyzed and the centroid is the avaerage of the point of the negative 
			  // direction and the point of the positive direction.
			  if (r>0)
			  {
				  // Compute which radius the interpolated pixel value is equaal to the FWHM
				  float rr = r + (FWHM - previous)/(current - previous) - 1;

				  // Get the coordinates of the point with that radius				  
				  p.x = rr * v2->x * cos(2*PI/rays*angle) + rr * v1->x * sin(2*PI/rays*angle) + cAVG->x;
				  p.y = rr * v2->y * cos(2*PI/rays*angle) + rr * v1->y * sin(2*PI/rays*angle) + cAVG->y;
				  p.z = rr * v2->z * cos(2*PI/rays*angle) + rr * v1->z * sin(2*PI/rays*angle) + cAVG->z;

				  // Write the point
				  *(points+angle) = p;

				  // Compute the average radius
				  *rAVG = (angle* *rAVG + rr)/(angle+1);
				  //cout << "rAVG: " << *rAVG << endl;

				  // Set the radius to -1 to analyze the negative direction
				  r = -1;
			  }
			  else
			  {
				  // Compute which interpolated pixel value is equaal to the FWHM
				  float rr = r - (FWHM - previous)/(current - previous) + 1;

				  // Get the coordinates of the point with that radius and current angle			  
				  p.x = rr * v2->x * cos(2*PI/rays*angle) + rr * v1->x * sin(2*PI/rays*angle) + cAVG->x;
				  p.y = rr * v2->y * cos(2*PI/rays*angle) + rr * v1->y * sin(2*PI/rays*angle) + cAVG->y;
				  p.z = rr * v2->z * cos(2*PI/rays*angle) + rr * v1->z * sin(2*PI/rays*angle) + cAVG->z;

				  // Write the point
				  *(points+angle+(int)(rays/2+0.5)) = p;

				  // Calculate the average centre (cAVG = cAVGOld*(angle+1) + currentCentre)/(angle+2)
				  // qfePoint::qfePointMultiply(0.5,*(points+angle) + *(points+angle+(int)(rays/2+0.5))),*cAVG); //currentCentre
				  qfePoint::qfePointMultiply(2*angle+2,*cAVG,*cAVG); // cAVG = 2*cAVGOld*(angle+1)
				  qfePoint::qfePointMultiply(0.5/(angle+2),*cAVG + *(points+angle) + *(points+angle+(int)(rays/2+0.5)),*cAVG); // (cAVG = cAVGOld*(angle+1) + currentCentre)/(angle+2)
				  
				  // Compute the average radius
				  *rAVG = (angle* *rAVG - rr)/(angle+1);

				  // Set the radius to 1 to compute the next angle
				  r = 1;
				  angle++;
			  }
		  }
		  // If the edge is not reached, r has to be increased (or decreased if r<0).
		  else if (r>0)
		  {
			  r++;
		  }
		  else
		  {
			  r--;
		  }
		  previous = current;
	  }
	  // r is larger than rmax or the pixel is outside the data volume, therefore the segmentation is probably wrong.
	  // If r is greater than the average radius, than the radius will be set to the average radius.
	  else if (r>*rAVG)
	  {
		  // Determine the position of the pixel with the average radius and current angle
		  p.x = *rAVG * v2->x * cos(4*PI/rays*angle) + *rAVG * v1->x * sin(4*PI/rays*angle) + cAVG->x;
		  p.y = *rAVG * v2->y * cos(4*PI/rays*angle) + *rAVG * v1->y * sin(4*PI/rays*angle) + cAVG->y;
		  p.z = *rAVG * v2->z * cos(4*PI/rays*angle) + *rAVG * v1->z * sin(4*PI/rays*angle) + cAVG->z;

		  // Write the point
		  *(points+angle) = p;

		  // Set r to -1 to analyze the negative direction
		  r = -1;
	  }
	  else if(r<-*rAVG)
	  {
		  // Determine the position of the pixel with the average radius and current angle
		  p.x = *rAVG * v2->x * cos(4*PI/rays*angle) + *rAVG * v1->x * sin(4*PI/rays*angle) + cAVG->x;
		  p.y = *rAVG * v2->y * cos(4*PI/rays*angle) + *rAVG * v1->y * sin(4*PI/rays*angle) + cAVG->y;
		  p.z = *rAVG * v2->z * cos(4*PI/rays*angle) + *rAVG * v1->z * sin(4*PI/rays*angle) + cAVG->z;

		  // Write the point
		  *(points+angle+(int)(rays/2+0.5)) = p;
		  
		  // Calculate the average centre (cAVG = cAVGOld*(angle+1) + currentCentre)/(angle+2)
		  // qfePoint::qfePointMultiply(0.5,*(points+angle) + *(points+angle+(int)(rays/2+0.5))),*cAVG); //currentCentre
		  qfePoint::qfePointMultiply(2*angle+2,*cAVG,*cAVG); // cAVG = 2*cAVGOld*(angle+1)
		  qfePoint::qfePointMultiply(0.5/(angle+2),*cAVG + *(points+angle) + *(points+angle+(int)(rays/2+0.5)),*cAVG); // (cAVG = cAVGOld*(angle+1) + currentCentre)/(angle+2)

		  // Set the radius to 1 to compute the next angle
		  r = 1;
		  angle++;
	  }
	  // If r is not larger than the average radius, than the point is outside the volume, but smaller than rAVG. Therefore the average radius
	  // is a bad estimation for the radius and it is better to set the edge of the volume as point.
	  else
	  {
		  if (p.x<0) {p.x=0;}
		  else if (p.x>=dimX) {p.x=dimX-1;}
		  if (p.y<0) {p.y=0;}
		  else if (p.y>=dimY) {p.y=dimY-1;}
		  if (p.z<0) {p.z=0;}
		  else if (p.z<dimZ) {p.z=dimZ-1;}
		  
		  if(r>0)
		  {
			  // Write the point
			  *(points+angle) = p;

			  // Compute the average radius
			  r = sqrt((p.x-cAVG->x)*(p.x-cAVG->x) + (p.y-cAVG->y)*(p.y-cAVG->y) + (p.z-cAVG->z)*(p.z-cAVG->z));
			  *rAVG = (angle* *rAVG - r)/(angle+1);

			  // Set r to -1 to analyze the negative direction
			  r = -1;
		  }
		  else
		  {
			  // Calculate the average centre (cAVG = cAVGOld*(angle+1) + currentCentre)/(angle+2)
			  // qfePoint::qfePointMultiply(0.5,*(points+angle) + *(points+angle+(int)(rays/2+0.5))),*cAVG); //currentCentre
			  qfePoint::qfePointMultiply(2*angle+2,*cAVG,*cAVG); // cAVG = 2*cAVGOld*(angle+1)
			  qfePoint::qfePointMultiply(0.5/(angle+2),*cAVG + *(points+angle) + *(points+angle+(int)(rays/2+0.5)),*cAVG); // (cAVG = cAVGOld*(angle+1) + currentCentre)/(angle+2)
			
			  // Compute the average radius
			  r = sqrt((p.x-cAVG->x)*(p.x-cAVG->x) + (p.y-cAVG->y)*(p.y-cAVG->y) + (p.z-cAVG->z)*(p.z-cAVG->z));
			  *rAVG = (angle* *rAVG - r)/(angle+1);

			  // Set the radius to 1 to compute the next angle
			  r = 1;
			  angle++;
		  }
	  }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe2D::qfeQuantifyProbe(qfeVolumeSeries *series, qfeProbe2D &probe)
{
  qfeQuantification::qfeGetFlowRate(series, probe);
  qfeQuantification::qfeGetMaximumSpeed(series, probe, 5.0);
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe2D::qfeSetProbeColor(qfeColorRGBA color)
{
  this->color = color;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe2D::qfeDrawProbe(qfeProbe2D probe, qfeColorRGBA color)
{
  int count = (int)probe.points.size();

  glBegin(GL_LINE_STRIP);
    glColor3f(color.r, color.g, color.b);
    for(int i=0; i<=count; i++)
    {
      glVertex3f(probe.points[i%count].x, probe.points[i%count].y, probe.points[i%count].z);
    }
  glEnd();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowProbe2D::qfeDrawProbePlane(qfeProbe2D probe, qfeColorRGBA color)
{
  int count = (int)probe.points.size();

  glBegin(GL_POLYGON);
    glColor3f(color.r, color.g, color.b);
    for(int i=0; i<=count; i++)
    {
      glVertex3f(probe.points[i%count].x, probe.points[i%count].y, probe.points[i%count].z);
    }
  glEnd();

  return qfeSuccess;
}

