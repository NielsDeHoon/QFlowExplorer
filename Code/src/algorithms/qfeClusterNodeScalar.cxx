#include "qfeClusterNodeScalar.h"
#include "qfeClusterNodeElliptic4D.h"

double qfeClusterNodeScalar::maxScalarVal = -100;

//----------------------------------------------------------------------------
//  Constructors
//----------------------------------------------------------------------------
qfeClusterNodeScalar::qfeClusterNodeScalar(qfeClusterPoint point)
{
  this->size = 1;

  for(int i=0; i<4; i++)
    this->meanPosition[i] = point.position[i];

  this->meanScalar = point.scalar;

};

//----------------------------------------------------------------------------
//  Destructor
//----------------------------------------------------------------------------
qfeClusterNodeScalar::~qfeClusterNodeScalar()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeClusterNodeScalar::qfeMergeRepresentative(qfeClusterNode *c2)
{
  qfeClusterNodeScalar *other = (qfeClusterNodeScalar*)c2;

  // TODO: Investigate/minimize effect of limited float accuracy with very different cluster sizes
  qfeFloat thisWeight  = (qfeFloat)(1/(1+(double)other->size/this->size));
  qfeFloat otherWeight  = (qfeFloat)(1/(1+(double)this->size/other->size));

  for(int i=0; i<4; i++)
    this->meanPosition[i] = thisWeight*this->meanPosition[i] + otherWeight*other->meanPosition[i];

  this->meanScalar = thisWeight*this->meanScalar + otherWeight*other->meanScalar;

  this->size += other->size;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeFloat qfeClusterNodeScalar::qfeComputeDistanceTo(qfeClusterNode *c2)
{
  qfeClusterNodeScalar *other = (qfeClusterNodeScalar*)c2;

  qfeFloat x,y,z,t,dScalar;
  double dPos;

  qfeFloat p[4];
  for(int i=0; i<4; i++){
    p[i] = this->meanPosition[i] - other->meanPosition[i];
  }

  dPos = sqrt(p[0]*p[0]/0.25f + p[1]*p[1]/0.25f + p[2]*p[2]/0.25f + p[3]*p[3]/0.25f);

  //dScalar = 0.80*(abs(this->meanScalar - other->meanScalar)/qfeClusterNodeScalar::maxScalarVal) + 0.20*(this->meanScalar/qfeClusterNodeScalar::maxScalarVal + other->meanScalar/qfeClusterNodeScalar::maxScalarVal);
  dScalar = (abs(this->meanScalar - other->meanScalar)/qfeClusterNodeScalar::maxScalarVal);
  return (qfeFloat) (qfeClusterNodeElliptic::A * (dPos/qfeClusterNodeElliptic::maxSpatialDistance)\
    + (1-qfeClusterNodeElliptic::A) * dScalar);
};