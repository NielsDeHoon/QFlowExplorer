#pragma once

#include "qflowexplorer.h"

#include "qfeFlowLineTrace.h"

#include <vector>

/**
* \file   qfeFlowLineTraceMatching.h
* \author Roy van Pelt
* \class  qfeFlowLineTraceMatching
* \brief  Implements rendering of a line trace for pattern matching results
* \note   Confidential
*
* Rendering of a line trace for pattern matching results using a file of seeds
*/

class qfeFlowLineTraceMatching : public qfeFlowLineTrace
{
public:
  enum qfeFlowLinesColorType
  {
    qfeFlowLinesColorUniform,
    qfeFlowLinesColorPatternType,
    qfeFlowLinesColorPatternResponse,
    qfeFlowLinesColorPatternClusterId,
    qfeFlowLinesColorPatternSpeed,
  };

  qfeFlowLineTraceMatching();
  qfeFlowLineTraceMatching(const qfeFlowLineTraceMatching &fv);
  ~qfeFlowLineTraceMatching();

  qfeReturnStatus qfeSetPhase(float phase);
  qfeReturnStatus qfeSetPhaseSpan(float span);
  qfeReturnStatus qfeSetPhaseVisible(int visible);
  qfeReturnStatus qfeSetArrowScale(int percentage);
  qfeReturnStatus qfeSetColorType(qfeFlowLinesColorType type);
  qfeReturnStatus qfeSetColor(qfeColorRGBA color);

  qfeReturnStatus qfeGenerateSeeds(qfeVolumeSeries series, qfeSeeds *seeds);

  qfeReturnStatus qfeGeneratePathLinesStatic(qfeVolumeSeries series, qfeSeeds *seeds, qfeSeeds *attribs);

  qfeReturnStatus qfeRenderMatchingSeeds();

  qfeReturnStatus qfeRenderMatchingPathlinesStatic(qfeColorMap *colormap);
  qfeReturnStatus qfeRenderMatchingPathlineArrows(qfeColorMap *colormap);

protected:
  qfeVolumeSeries clusterLabelSeries;;

  qfeReturnStatus qfeInitTraceStatic();

  qfeReturnStatus qfeInitSeedBuffer(qfeSeeds *seeds);
  qfeReturnStatus qfeClearSeedBuffer();

  qfeReturnStatus qfeRenderStyleTube(int currentBuffer, int attribBuffer, int lineCount, int lineSize);
  qfeReturnStatus qfeRenderStyleArrow(int currentBuffer, int attribBuffer, int lineCount, int lineSize);
  
  qfeReturnStatus qfeFetchVelocityLinear(qfeVolume *vol, qfePoint p, qfeVector &v);
  qfeReturnStatus qfeFetchVelocityLinearTemporalLinear(qfeVolumeSeries series, qfePoint p, qfeFloat t, qfeVector &v);
  qfeReturnStatus qfeIntegrateRungeKutta4(qfeVolumeSeries series, qfePoint seed, float stepSize, float stepDuration, int stepDirection, int stepModulation, qfeMatrix4f p2v, qfePoint &pos);

  qfeReturnStatus qfeInitLineBufferStatic(vector<float> *vertices, vector<float> *attribs);

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

private :
  qfeFlowLinesColorType   paramColorType;
  qfeColorRGBA            paramColor;
  qfeFloat                paramPhaseSpan;
  int                     paramPhaseVisible;
  int                     paramArrowScale;

  qfeGLShaderProgram     *shaderProgramEndCaps;
  qfeGLShaderProgram     *shaderProgramArrows;

};
