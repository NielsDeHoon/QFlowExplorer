#pragma once

#include "qflowexplorer.h"

#include "qfeFlowLineTrace.h"

#include <vector>

/**
* \file   qfeFlowLineTraceCluster.h
* \author Roy van Pelt
* \class  qfeFlowLineTraceCluster
* \brief  Implements rendering of a line trace for cluster data
* \note   Confidential
*
* Rendering of a line trace within a cluster data set
*/

class qfeFlowLineTraceCluster : public qfeFlowLineTrace
{
public:
  enum qfeFlowLinesColorType
  {
    qfeFlowLinesColorUniform,
    qfeFlowLinesColorSpeed,  
    qfeFlowLinesColorTime,
    qfeFlowLinesColorStatDyn1,
    qfeFlowLinesColorStatDyn2
  };

  qfeFlowLineTraceCluster();
  qfeFlowLineTraceCluster(const qfeFlowLineTraceCluster &fv);
  ~qfeFlowLineTraceCluster();

  qfeReturnStatus qfeSetPhase(float phase);
  qfeReturnStatus qfeSetClusterMask(bool masking);
  qfeReturnStatus qfeSetLengthMask(float length);
  qfeReturnStatus qfeSetColorType(qfeFlowLinesColorType type);
  qfeReturnStatus qfeSetColor(qfeColorRGBA color);
  
  qfeReturnStatus qfeGenerateSeeds(qfeVolumeSeries series, qfeSeeds *seeds);

  qfeReturnStatus qfeGeneratePathLinesStatic(qfeVolumeSeries series, qfeSeeds *seeds);

  qfeReturnStatus qfeGeneratePathLinesDynamic(qfeVolumeSeries series, qfeVolumeSeries clusterLabels, qfeSeeds *seeds, qfeFloat phase);  
  qfeReturnStatus qfeGeneratePathLinesStatic(qfeVolumeSeries series, qfeVolumeSeries clusterLabels, qfeSeeds *seeds);
  qfeReturnStatus qfeGeneratePathLinesStatic2(qfeVolumeSeries series, qfeVolumeSeries clusterLabels, qfeSeeds *seeds);

  qfeReturnStatus qfeRenderClusterSeeds();

  qfeReturnStatus qfeRenderClusterPathlinesStatic(qfeColorMap *colormap);
  qfeReturnStatus qfeRenderClusterPathlinesDynamic(qfeColorMap *colormap);

protected:  
  qfeGLShaderProgram   *shaderProgramRenderArrowEnds;   

  qfeVolumeSeries clusterLabelSeries;;

  qfeReturnStatus qfeInitTraceStatic();
  qfeReturnStatus qfeInitTraceDynamic();

  qfeReturnStatus qfeInitSeedBuffer(qfeSeeds *seeds);
  qfeReturnStatus qfeInitSeedBuffer(qfeSeeds *seeds, float phase);
  qfeReturnStatus qfeClearSeedBuffer();

  qfeReturnStatus qfeBindTexturesCluster(qfeVolumeSeries clusterLabels, int phase);
  qfeReturnStatus qfeBindTexturesClusterSliding(qfeVolumeSeries clusterLabels, int phase);

  qfeReturnStatus qfeRenderStyleLine(int currentBuffer, int attribBuffer, int lineCount, int lineSize);
  qfeReturnStatus qfeRenderStyleTube(int currentBuffer, int attribBuffer, int lineCount, int lineSize);
  qfeReturnStatus qfeRenderStyleArrow(int currentBuffer, int attribBuffer, int lineCount, int lineSize);

  //qfeReturnStatus qfeDrawLines(GLuint lines, GLuint attribs, int lineCount, int lineSize);

  qfeReturnStatus qfeFetchVelocityLinear(qfeVolume *vol, qfePoint p, qfeVector &v);
  qfeReturnStatus qfeFetchVelocityLinearTemporalLinear(qfeVolumeSeries series, qfePoint p, qfeFloat t, qfeVector &v);
  qfeReturnStatus qfeFetchClusterLabelTemporalNearest(qfeVolumeSeries clusterLabels, qfePoint p, qfeFloat t, qfeFloat &label);
  qfeReturnStatus qfeIntegrateRungeKutta4(qfeVolumeSeries series, qfePoint seed, float stepSize, float stepDuration, int stepDirection, int stepModulation, qfeMatrix4f p2v, qfePoint &pos);

  qfeReturnStatus qfeInitLineBufferStatic(vector<float> *vertices, vector<float> *attribs);

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();
  
private :  
  static const char    *feedbackVarsCopy[];
  static const char    *feedbackVarsGenerate[];

  bool                    paramClusterMasking;
  float                   paramLengthMasking;
  qfeFlowLinesColorType   paramColorType;
  qfeColorRGBA            paramColor;

};
