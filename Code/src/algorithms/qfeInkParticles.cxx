﻿#include "qfeInkParticles.h"

#include <ctime>
const std::string currentDateTime() {
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	// Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
	// for more information about date/time format
	strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

	return buf;
}

//----------------------------------------------------------------------------
qfeInkParticles::qfeInkParticles()
{
	this->shaderComputeParticles = new qfeGLShaderProgram();
	this->shaderComputeParticles->qfeAddShaderFromFile("/shaders/ink/ink-compute-particles-comp.glsl", QFE_COMPUTE_SHADER);
	this->shaderComputeParticles->qfeLink();
	
	renderShader.qfeAddShaderFromFile("/shaders/ink/ink-render-geom.glsl", QFE_GEOMETRY_SHADER);
	renderShader.qfeAddShaderFromFile("/shaders/ink/ink-render-vert.glsl", QFE_VERTEX_SHADER);
	renderShader.qfeAddShaderFromFile("/shaders/ink/ink-render-frag.glsl", QFE_FRAGMENT_SHADER);
	renderShader.qfeLink();

	/*
	this->shaderProgramVectorLines->qfeAddShaderFromFile("/shaders/ifr/line-frag.glsl", QFE_FRAGMENT_SHADER);
	this->shaderProgramVectorLines->qfeAddShaderFromFile("/shaders/ifr/line-vector-geom.glsl", QFE_GEOMETRY_SHADER);
	this->shaderProgramVectorLines->qfeBindAttribLocation("qfe_Vertex", ATTRIB_VERTEX);
	this->shaderProgramVectorLines->qfeBindAttribLocation("qfe_TexCoord", ATTRIB_TEXCOORD0);  
	this->shaderProgramVectorLines->qfeBindFragDataLocation("qfe_FragColor", 0);
	this->shaderProgramVectorLines->qfeLink();
	*/

	this->firstRender = true;

	this->colorMapTextureId = 0;
	inverseColorMap = false;

	clip_outside_mesh = false;
	clipping_margin = 0.0f;
	
	//seed the random number generator
	srand(static_cast <unsigned> (time(0)));
	
	spatialGapAmount1 = 0;
	spatialGapAmount2 = 0;
	temporalGapAmount = 0;
	spatialGapWidth1 = 0.0;
	spatialGapWidth2 = 0.0;
	temporalGapWidth = 0.0;

	total_number_of_particles = 0;
	number_of_particles_removed_age = 0;
	number_of_particles_removed_domain = 0;

	valid_selection_range = false;

	meshSDFTextureVolumeExists = false;

	fixedPositionsComputed = false;
	fixedPositionsMultiplier = 1.0f;
};

//----------------------------------------------------------------------------
qfeInkParticles::qfeInkParticles(const qfeInkParticles &sp)
{
	std::cout<<"THIS TYPE OF INITIALIZATION OF InkParticles IS NOT YET FULLY IMPLEMENTED"<<std::endl;
};

//----------------------------------------------------------------------------
qfeInkParticles::~qfeInkParticles()
{
	this->timeLinePlot.closeWindow();

	delete this->shaderComputeParticles;

	delete this->particleSinks;
};

qfeReturnStatus qfeInkParticles::qfeInitialize(qfeStudy *_study)
{
	if(_study != NULL)
		return this->qfeInitialize(_study, &_study->pcap);
	
	return qfeError;
}

qfeReturnStatus qfeInkParticles::qfeInitialize(qfeStudy *_study, qfeVolumeSeries *_volumeData)
{
	char *cwd = (char *)calloc(_MAX_PATH, sizeof(char));
	_getcwd(cwd, _MAX_PATH);
	std::cout<<"Executable location: "<<cwd<<std::endl;

	bool init_failed = false;

	if(_study == NULL)
	{
		init_failed = true;
	}

	if(_volumeData == NULL)
	{
		std::cout<<"No volume data provided"<<std::endl;
		init_failed = true;
	}
	
	if(!init_failed && _volumeData->size()==0)
	{
		std::cout<<"Empty volume data provided"<<std::endl;
		init_failed = true;
	}
		
	inside_mesh_voxel_count = 0;

	if(!init_failed)
	{
		study = _study;
		volumeData = _volumeData;
		
		particleSinks = new qfeSeedVolumeCollection(_study);	
		
		qfeGrid *grid = new qfeGrid();
		volumeData->at(0)->qfeGetVolumeGrid(&grid);
		grid->qfeGetGridExtent(spacing[0],spacing[1],spacing[2]);

		particleMultiplier = 1.0f;
		particleOpacity = 0.1f;
		use_uncertainty = false;

		color_setting = 0;

		qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volumeData->at(0));
		qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volumeData->at(0));
		qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volumeData->at(0));
		qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volumeData->at(0));

		volumeData->at(0)->qfeGetVolumeSize(volumeSize[0], volumeSize[1], volumeSize[2]);

		float range_min = 0.0f;
		float range_max = 0.0f;		

		this->phaseDuration = study->phaseDuration;

		this->qfeComputeCFL(1.0f);

		float sigma = 0.0f;

		Array3f result;

		for(unsigned int t = 0; t<volumeData->size(); t++)
		{
			study->qfeGetSNR((float)t, result, sigma, range_min, range_max);

			qfeVolume volume;
			qfeGrid *grid;
			volumeData->at(0)->qfeGetVolumeGrid(&grid);

			SNR_data.push_back(result);
			sigma_data.push_back(sigma);
		}

		study->qfeGetStudyRangePCAP(range_min, range_max);

		this->venc = max(fabs(range_min),fabs(range_max))/100.0f; //convert from cm/s to m/s

		this->number_of_lines = 1;

		this->advection = Path;

		//set a default color scheme:
		std::string name = "";
		qfeColorMap colorMap;
		qfeColorMaps colorMaps;
		colorMap.qfeUploadColorMap();
		colorMaps.qfeGetColorMap(0, name, colorMap); 
		colorMap.qfeGetAllColorsRGB(colors);
		color_size = colors.size()-1;
		//nr_color_bands = 0;

		colorMaps.qfeUpdateTransferFunctionColorMap(colorMap);

		qfeMatrix4f M = T2V*V2P; 

		volumeCorners[0] = qfePoint( 0.0f, 0.0f, 0.0f) * M;
		volumeCorners[1] = qfePoint( 1.0f, 0.0f, 0.0f) * M;
		volumeCorners[2] = qfePoint( 1.0f, 1.0f, 0.0f) * M;
		volumeCorners[3] = qfePoint( 0.0f, 1.0f, 0.0f) * M;
		volumeCorners[4] = qfePoint( 0.0f, 0.0f, 1.0f) * M;
		volumeCorners[5] = qfePoint( 1.0f, 0.0f, 1.0f) * M;
		volumeCorners[6] = qfePoint( 1.0f, 1.0f, 1.0f) * M;
		volumeCorners[7] = qfePoint( 0.0f, 1.0f, 1.0f) * M;
	
		// Normals in patient space
		volumeNormals[0] = qfeVector(-1.0f,  0.0f,  0.0f) * M;  // left   face
		volumeNormals[1] = qfeVector( 0.0f,  0.0f,  1.0f) * M;  // front  face
		volumeNormals[2] = qfeVector( 1.0f,  0.0f,  0.0f) * M;  // right  face
		volumeNormals[3] = qfeVector( 0.0f,  0.0f, -1.0f) * M;  // back   face
		volumeNormals[4] = qfeVector( 0.0f,  1.0f,  0.0f) * M;  // top    face
		volumeNormals[5] = qfeVector( 0.0f, -1.0f,  0.0f) * M;  // bottom face

		for(unsigned int i = 0; i<volumeNormals.size(); i++)
		{
			qfeVector::qfeVectorNormalize(volumeNormals[i]);
		}

		max_dist = sqrt(
			(volumeCorners.at(0).x - volumeCorners.at(6).x) * (volumeCorners.at(0).x - volumeCorners.at(6).x) + 
			(volumeCorners.at(0).y - volumeCorners.at(6).y) * (volumeCorners.at(0).y - volumeCorners.at(6).y) + 
			(volumeCorners.at(0).z - volumeCorners.at(6).z) * (volumeCorners.at(0).z - volumeCorners.at(6).z)
			);
		
		volumeCenter = qfePoint(
			(volumeCorners[0].x + volumeCorners[6].x)/2.0f,
			(volumeCorners[0].y + volumeCorners[6].y)/2.0f,
			(volumeCorners[0].z + volumeCorners[6].z)/2.0f);

		this->particlesSeededFromVoxel.resize(volumeSize[0], volumeSize[1], volumeSize[2]);
		this->particlesSeededFromVoxel.set_zero();

		this->particlesLeavingMeshFromVoxel.resize(volumeSize[0], volumeSize[1], volumeSize[2]);
		this->particlesLeavingMeshFromVoxel.set_zero();

		this->mesh_level_set.resize(0,0,0);

		featureBasedSeeds.resize(0);
		
		masked_voxels.resize(volumeSize[0], volumeSize[1], volumeSize[2]);
		Array3f maskLevelSet(volumeSize[0], volumeSize[1], volumeSize[2]);
		is_data_masked = false;

		unsigned int total_voxels = volumeSize[0] * volumeSize[1] * volumeSize[2];
		unsigned int zero_voxels = 0;

		if(study->ffe.size() > 0 && study->segmentation.size()==0)
		{
			//check if the anatomy scalar field is actualy a mask:
			for(int k = 0; k<volumeSize[2]; k++)
			{
				for(int j = 0; j<volumeSize[1]; j++)
				{
					for(int i = 0; i<volumeSize[0]; i++)
					{	
						masked_voxels.at(i,j,k) = false;
						maskLevelSet(i,j,k) = 1.0f;
						float value = 0.0f;
							
						study->ffe[0]->qfeGetVolumeScalarAtPosition(i,j,k,value);

						if(fabs(value)<1e-12f)
						{
							zero_voxels++;
							masked_voxels.at(i,j,k) = true;
							maskLevelSet(i,j,k) = -1.0f;
						}
					}
				}
			}
		}

		if(zero_voxels > total_voxels/2 && study->segmentation.size()==0) //if enough voxels are zero we assume a mask is given
		{
			is_data_masked = true;
			std::cout<<"Data has a mask"<<std::endl;
		}

		zero_voxels = 0;

		if(!is_data_masked && study->segmentation.size()==0) //check if the velocity field is masked
		{
			qfeVector velocityVector;
			float length = 0.0f;
			for(int k = 0; k<volumeSize[2]; k++)
			{
				for(int j = 0; j<volumeSize[1]; j++)
				{
					for(int i = 0; i<volumeSize[0]; i++)
					{					
						masked_voxels.at(i,j,k) = false;
						maskLevelSet(i,j,k) = 1.0f;

						volumeData->at(0)->qfeGetVolumeVectorAtPositionLinear(qfePoint(i,j,k), velocityVector);
						qfeVector::qfeVectorLength(velocityVector, length);
						if(length<1e-12f)
						{
							zero_voxels++;
							masked_voxels.at(i,j,k) = true;
							maskLevelSet(i,j,k) = -1.0f;
						}
					}
				}
			}
		}

		if(zero_voxels > total_voxels/2 && study->segmentation.size()==0) //if enough voxels are zero we assume a mask is given
		{
			is_data_masked = true;
			std::cout<<"Data has a mask"<<std::endl;
		}

		if(is_data_masked && study->segmentation.size()==0) //use mask as segmentation
		{
			// Get the model  
			qfeModel *model;
			study->qfeGetIsoSurface(study->pcap[0],&maskLevelSet,0.0f,&model);
			
			model->qfeSetModelColor(qfeColorRGB(0.5f,0.5f,0.5f));
			model->qfeSetModelLabel("Mask");
			model->qfeSetModelVisible(true);
			
			study->segmentation.push_back(model);
			study->qfeSetSolidSDF(0.0f, &maskLevelSet);
		}
		else
		{
			masked_voxels.resize(0,0,0);
		}
		
		if(study->segmentation.size()>0) //load the mesh
		{
			if(is_data_masked)
			{
				this->mesh_level_set.resize(maskLevelSet.ni, maskLevelSet.nj, maskLevelSet.nk);
				for(unsigned int i = 0; i<maskLevelSet.size(); i++)
				{
					this->mesh_level_set.a[i] = maskLevelSet.a[i];
				}
			}
			else
			{
				bool exists;
				float range_min, range_max;
				study->qfeGetScalarMetric("Solid SDF", 0.0f, exists, this->mesh_level_set, range_min, range_max);
			}
			
			volumeData->at(0)->qfeGetVolumeGrid(&grid);
			std::array<float, 2> range;
			this->qfeGetMeshLevelsetVolume(this->meshSDFTextureVolume, range, 1);

			meshSDFTextureVolumeExists = true;

			std::pair<int, Array3i> mesh_pair;
			mesh_pair.first = 0;
			mesh_pair.second = Array3i(volumeSize[0], volumeSize[1], volumeSize[2]);
			mesh_pair.second.set_zero();
			this->reason_particles_removed.push_back(mesh_pair);
		}

		//count number of voxels inside the mesh/mask
		inside_mesh_voxel_count = 0;
		for(int k = 0; k<this->mesh_level_set.nk; k++)
		{
			for(int j = 0; j<this->mesh_level_set.nj; j++)
			{
				for(int i = 0; i<this->mesh_level_set.ni; i++)
				{	
					if(mesh_level_set(i,j,k)>=0.0f)
						inside_mesh_voxel_count++;
				}
			}
		}
	}
	else
	{
		std::cout<<"qfeInkParticles initialization failed"<<std::endl;
		return qfeError;
	}

	QVector<QPair<double,double>> dataSet;

	std::vector<std::array<float, 3>> data;

	study->qfeGetAverageOverTime("Velocity magnitude 1", data);

	for(int i = 0; i<data.size(); i++)
	{
		QPair<double, double> linePoint;
		linePoint.first = data.at(i)[2]; //time
		linePoint.second = data.at(i)[0];//average vel

		dataSet.push_back(linePoint);
	}

	this->timeLinePlot.setData(dataSet, "Velocity");
	
	return qfeSuccess;
}

unsigned int qfeInkParticles::qfeNumberOfParticles()
{
	return particles.size();
}

qfeReturnStatus qfeInkParticles::qfeRenderSeedingLine(InkLine line)
{
	//draw the seeding points
	qfePoint p1 = line[0] * V2P;
	qfePoint p2 = line[1] * V2P;
	glEnable(GL_POINT_SMOOTH);
	
	glPointSize(5.0f);

	particle part;
	part.age = 0.0f;
	part.depth = 0.5f;
	part.deviation = 0.0f;
	
	qfeColorRGB col;
		
	part.seed_id1 = 0.0f;
	part.seed_id2 = 0.0f;
	col = this->qfeGetColor(&part);
	
	qfeRenderCube(p1, dx*4.0, true, col);

	part.seed_id1 = 1.0f;
	part.seed_id2 = 1.0f;
	col = this->qfeGetColor(&part);

	qfeRenderCube(p2, dx*4.0, true, col);
	

	glBegin(GL_LINE_STRIP);
		for(unsigned int i = 0; i<5; i++)
		{
			float frac = (float)i/(5.0f-1.0f);

			qfePoint p = p1*(1.0f-frac)+p2*frac;

			part.seed_id1 = frac;
			part.seed_id2 = frac;

			col = this->qfeGetColor(&part);
			glColor3f(col.r, col.g, col.b);
			glVertex3f(p.x,p.y,p.z);
		}
	glEnd();

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeRenderSeedingVolume(InkVolume volume, vtkCamera *camera, int vp_width, int vp_height, bool plane)
{
	seedVolume.qfeSetGuidePoints(volume[0]*V2P, volume[1]*V2P, volume[2]*V2P);

	seedVolume.qfeSetVtkCamera(camera, vp_width, vp_height);

	//const qfeColorRGB &color, bool surface, bool selected, bool useClipPlane
	qfeColorRGB col(0.5f, 0.6f, 0.9f);
	seedVolume.qfeRenderVolume(col, false, false, true, plane);

	qfePoint p;
	glEnable(GL_POINT_SMOOTH);
	
	glPointSize(5.0f);

	p = volume[0] * V2P;
	qfeRenderCube(p, dx*4.0, true, qfeColorRGB(1.0f,0.0f,0.0f));
	
	p = volume[1] * V2P;
	qfeRenderCube(p, dx*4.0, true, qfeColorRGB(0.0f,1.0f,0.0f));
	glVertex3f(p.x,p.y,p.z);	

	p = volume[2] * V2P;
	qfeRenderCube(p, dx*4.0, true, qfeColorRGB(0.0f,0.0f,1.0f));
	
	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeInkParticles::qfeRenderParticles(std::array<double,3> camera, std::array<double,3> viewUp, std::array<double,3> viewDir, float znear, float zfar, int vp_width, int vp_height, bool mapToOpacityUncertainty, bool depthDarkening, float depthDarkeningAmount, bool depthDarkeningLight, bool chromaDepth, bool aerialPerspective, float particle_size, int particle_type, GLuint baseframeBuffer, unsigned int glyph_type, unsigned int glyph_frequency, bool clipSeedVolume)
{
	//qfeUpdateColorMap();
	if(particles.size()==0)
		return qfeSuccess;

	qfePoint point1 = this->seedPosition.at(0)*V2P;
	qfePoint point2 = this->seedPosition.at(1)*V2P;
	
	qfeColorRGB col(0.0f,0.0f,0.0f);
	//draw the seeding line:

	qfePoint point(0.0f,0.0f,0.0f);

	glGetFloatv(GL_PROJECTION_MATRIX, this->matrixProjection);
	glGetFloatv(GL_MODELVIEW_MATRIX, this->matrixModelView);
	
	if(particleOpacity > 0.0f)
		depth_sort_particles(&this->particles, camera);

	glClearColor(1.0,1.0,1.0,1.0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	if(particleOpacity < 1.0f || depthDarkening)
		glDepthMask(GL_FALSE);

	glPointSize(5.0f);

	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_ALWAYS,0.0);
	
	normalize(viewUp);
	normalize(viewDir);

	this->renderShader.qfeSetUniformMatrix4f("projectionMatrix", 1, this->matrixProjection);
	this->renderShader.qfeSetUniformMatrix4f("modelViewMatrix", 1, this->matrixModelView);
					
	if(particleOpacity >= 0.0f)
	{		
		//create triangle list:
		float farthest = particles.at(0).depth;//farthest;
		float nearest = particles.at(particles.size()-1).depth;//nearest;

		std::vector<std::array<float,4>> triangle_list(particles.size()*3);
		for(unsigned int i = 0; i<particles.size(); i++)
		{
			point = particles.at(i).position;

			float age = particles.at(i).age/(this->particleLifeTime);

			if(age<0.0f)
				continue;

			if(spatialGapAmount1 > 0)
			{
				float seedPos1 = particles.at(i).seed_id1;
				float gapPos1 = seedPos1*(spatialGapAmount1-1);
				if(fabs(gapPos1-(int)(gapPos1+0.5))>spatialGapWidth1)
					continue;
			}

			if(spatialGapAmount2 > 0)
			{
				float seedPos2 = particles.at(i).seed_id2;
				float gapPos2 = seedPos2*(spatialGapAmount2-1);
				if(fabs(gapPos2-(int)(gapPos2+0.5))>spatialGapWidth2)
					continue;
			}

			if(temporalGapAmount > 0)
			{
				float agePos = (fabs((particles.at(i).age-internalTime))/(this->particleLifeTime));
				float ageGapPos = (fabs((particles.at(i).age-internalTime))/(this->particleLifeTime))*(temporalGapAmount-1);
				if(fabs(ageGapPos-(int)(ageGapPos+0.5))>temporalGapWidth)
					continue;
			}

			if(clipSeedVolume && this->IsInFrontOfClippingPlane(point, qfeVector(-viewDir[0], -viewDir[1], -viewDir[2]), seedVolume.qfeGetCenter()))
			{
				continue;
			}
				
			//actual particle:
			qfeColorRGB color = qfeGetColor(&particles.at(i));
			if(particles.at(i).seed_id2 < -0.5f && color_setting == 0) //feature based, color id stored in seed_id1
			{
				int color_index = (int)(particles.at(i).seed_id1 * (this->particleSinks->size()-1));
				color = this->particleSinks->qfeGetSeedVolumeColor(color_index);
			}

			if(chromaDepth)
			{
				float current = 2.0f*(particles.at(i).depth-nearest)/(farthest-nearest)-1.0f;
				color.r *= (1.0f-0.25f*current);
				color.b *= (1.0f+0.25f*current);
			}

			if(aerialPerspective)
			{
				float current = particles.at(i).depth;
				//color_index = part->depth/(this->max_dist/2.0f);
				float desaturation_ratio = 1.0f-0.75*(current-nearest)/(farthest-nearest);
				//desaturateIsoL(color, desaturation_ratio);
				desaturate(color, desaturation_ratio);
			}
			
			float opacity = particleOpacity-age/(1.0f/particleOpacity);
			
			if(mapToOpacityUncertainty)
			{
				if(particles.at(i).deviation > venc || venc == 0.0f)
				{
					opacity = 0.0f;
				}
				else
				{
					opacity = particleOpacity - (particles.at(i).deviation/venc)/(1.0f/particleOpacity);
				}
			}

			if(opacity<0.0f)
				opacity = 0.0f;
			if(particleOpacity == 1.0f)
				opacity = 1.0f;

			//if the particles are within the selection, render them green and without opacity
			if(valid_selection_range)
			{
				if(age_selection[0] < particles.at(i).age && particles.at(i).age < age_selection[1])
				{
					bool selected = false;
					
					if(selected_graph_data == UncertaintyGraph && filter_selection[0] < particles.at(i).deviation && particles.at(i).deviation < filter_selection[1])
					{
						selected = true;
					}

					if(selected_graph_data == SpeedGraph && filter_selection[0] < particles.at(i).speed*2.0 && particles.at(i).speed*2.0 < filter_selection[1])
					{
						selected = true;
					}

					if(selected)
					{
						if(opacity >= 0.9)
							opacity = 1.0f;
						else
							opacity = 0.99;
					}
				}
			}

			//glColor4f(color.r, color.g, color.b, particleOpacity-age/(1.0f/particleOpacity));
			
			std::array<float,4> vertex0 = {point.x,point.y,point.z,-1.0f}; //vertex 0 contains the position

			std::array<float,4> vertex1 = {color.r, color.g, color.b, 0.0f};//vertex 1 contains the color

			float relative_age = (glyph_frequency*fabs((particles.at(i).age-internalTime))/(this->particleLifeTime));
			
			std::array<float,4> vertex2 = {opacity, particles.at(i).depth/(max_dist), relative_age, 0.0f};//vertex 2 contains alpha, depth and relative age

			triangle_list.at(i*3+0) = vertex0;
			triangle_list.at(i*3+1) = vertex1;
			triangle_list.at(i*3+2) = vertex2;
		}
		
		//std::vector<GLfloat> image_data(vp_width*vp_height);
		GLfloat *image_data = new GLfloat[4*vp_width*vp_height];
		for(unsigned int i = 0; i<4*vp_width*vp_height; i++)
		{
			image_data[i] = 0.0f;
		}

		GLuint depth_image_id;		
		glGenTextures(1, &depth_image_id);
		glBindTexture(GL_TEXTURE_2D, depth_image_id);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, vp_width, vp_height, 0, GL_RGBA, GL_FLOAT, &image_data[0]);		
		//glBindImageTexture(0, depth_image_id, 0, GL_FALSE, 0, GL_READ_WRITE, GL_R32F);

		bindImage(depth_image_id, 0, GL_RGBA32F);

		this->renderShader.qfeSetUniform1f("particle_size", particle_size);
		this->renderShader.qfeSetUniform1i("particle_type", particle_type);
		this->renderShader.qfeSetUniform1i("glyph_type", glyph_type);
		this->renderShader.qfeSetUniform1i("depth_buffer", 0);
		
		this->renderShader.qfeSetUniform1f("znear", znear);
		this->renderShader.qfeSetUniform1f("zfar", zfar);
		
		if(depthDarkening)
			this->renderShader.qfeSetUniform1i("depth_darkening", 1);
		else
			this->renderShader.qfeSetUniform1i("depth_darkening", 0);

		this->renderShader.qfeSetUniform1f("depth_darkening_amount", depthDarkeningAmount);

		if(depthDarkeningLight)
			this->renderShader.qfeSetUniform1i("depth_darkening_light", 1);
		else
			this->renderShader.qfeSetUniform1i("depth_darkening_light", 0);
		
		renderShader.qfeEnable();
		
		glBegin(GL_TRIANGLES);
		
		for(unsigned int i = 0; i<particles.size(); i++)
		{	
			std::array<float,4> vertex0 = triangle_list.at(i*3+0);
			std::array<float,4> vertex1 = triangle_list.at(i*3+1);
			std::array<float,4> vertex2 = triangle_list.at(i*3+2);
						
			glVertex4f(vertex0[0],vertex0[1],vertex0[2],vertex0[3]);//contains the position

			glVertex4f(vertex1[0],vertex1[1],vertex1[2],vertex1[3]);//contains the color

			glVertex4f(vertex2[0],vertex2[1],vertex2[2],vertex2[3]);//contains alpha, depth and relative age
		}
		
		glEnd();

		renderShader.qfeDisable();

		glBindTexture(GL_TEXTURE_2D, 0);
		
		glDeleteTextures(1, &depth_image_id);

		delete[] image_data;
	}


	glDepthMask(GL_TRUE);
	glDisable(GL_POINT_SMOOTH);
	glDisable(GL_BLEND);
	glDisable(GL_ALPHA_TEST);

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeInkParticles::qfeRenderParticlesGPUFriendly(float particle_size, std::array<double,3> viewDir, bool clipSeedVolume, int glyph_type, int glyph_frequency)
{	
	//qfeUpdateColorMap();
	if(particles.size()==0)
		return qfeSuccess;

	qfePoint point1 = this->seedPosition.at(0)*V2P;
	qfePoint point2 = this->seedPosition.at(1)*V2P;
	
	qfeColorRGB col(0.0f,0.0f,0.0f);
	//draw the seeding line:

	qfePoint point(0.0f,0.0f,0.0f);
	
	//glPointSize(particle_size);

	//glEnable(GL_POINT_SMOOTH);

	//glBegin(GL_POINTS);

	glGetFloatv(GL_PROJECTION_MATRIX, this->matrixProjection);
	glGetFloatv(GL_MODELVIEW_MATRIX, this->matrixModelView);
	
	glClearColor(1.0,1.0,1.0,1.0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glPointSize(5.0f);

	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_ALWAYS,0.0);

	this->renderShader.qfeSetUniformMatrix4f("projectionMatrix", 1, this->matrixProjection);
	this->renderShader.qfeSetUniformMatrix4f("modelViewMatrix", 1, this->matrixModelView);

	this->renderShader.qfeSetUniform1f("particle_size", particle_size);
	this->renderShader.qfeSetUniform1i("particle_type", 1);
	this->renderShader.qfeSetUniform1i("glyph_type", glyph_type);
	this->renderShader.qfeSetUniform1i("depth_buffer", 0);
	
	this->renderShader.qfeSetUniform1f("znear", 0.0f);
	this->renderShader.qfeSetUniform1f("zfar", 1e6f);
	
	this->renderShader.qfeSetUniform1i("depth_darkening", 0);
	
	this->renderShader.qfeSetUniform1f("depth_darkening_amount", 0.0f);
	
	this->renderShader.qfeSetUniform1i("depth_darkening_light", 0);
	
	renderShader.qfeEnable();
				
	for(unsigned int i = 0; i<particles.size(); i++)
	{
		point = particles.at(i).position;
		
		float age = particles.at(i).age/(this->particleLifeTime);
		
		if(age<0.0f)
			continue;
		
		if(spatialGapAmount1 > 0)
		{
			float seedPos1 = particles.at(i).seed_id1;
			float gapPos1 = seedPos1*(spatialGapAmount1-1);
			if(fabs(gapPos1-(int)(gapPos1+0.5))>spatialGapWidth1)
				continue;
		}
		
		if(spatialGapAmount2 > 0)
		{
			float seedPos2 = particles.at(i).seed_id2;
			float gapPos2 = seedPos2*(spatialGapAmount2-1);
			if(fabs(gapPos2-(int)(gapPos2+0.5))>spatialGapWidth2)
				continue;
		}
		
		if(temporalGapAmount > 0)
		{
			float agePos = (fabs((particles.at(i).age-internalTime))/(this->particleLifeTime));
			float ageGapPos = (fabs((particles.at(i).age-internalTime))/(this->particleLifeTime))*(temporalGapAmount-1);
			if(fabs(ageGapPos-(int)(ageGapPos+0.5))>temporalGapWidth)
				continue;
		}

		if(clipSeedVolume && this->IsInFrontOfClippingPlane(point, qfeVector(-viewDir[0], -viewDir[1], -viewDir[2]), seedVolume.qfeGetCenter()))
		{
			continue;
		}
		
		//actual particle:
		qfeColorRGB color = qfeGetColor(&particles.at(i));
		if(particles.at(i).seed_id2 < -0.5f && color_setting == 0) //feature based, color id stored in seed_id1
		{
			int color_index = (int)(particles.at(i).seed_id1 * (this->particleSinks->size()-1));
			color = this->particleSinks->qfeGetSeedVolumeColor(color_index);
		}
		
		float opacity = particleOpacity-age/(1.0f/particleOpacity);
		if(opacity<0.0f)
			opacity = 0.0f;
		if(particleOpacity == 1.0f)
			opacity = 1.0f;
		
		//if the particles are within the selection, render them green and without opacity
		if(valid_selection_range)
		{
			if(age_selection[0] < particles.at(i).age && particles.at(i).age < age_selection[1])
			{
				bool selected = false;

				if(selected_graph_data == UncertaintyGraph && filter_selection[0] < particles.at(i).deviation && particles.at(i).deviation < filter_selection[1])
				{
					selected = true;
				}

				if(selected_graph_data == SpeedGraph && filter_selection[0] < particles.at(i).speed*2.0 && particles.at(i).speed*2.0 < filter_selection[1])
				{
					selected = true;
				}

				if(selected)
				{
					color.r = 0.0f; color.g = 1.0f; color.b = 0.0f;
					
					opacity = 1.0f;
				}
			}
		}

		//glColor3f(color.r, color.g, color.b);
			
		//glVertex3f(point.x,point.y,point.z);

		//float relative_age = (fabs((particles.at(i).age-internalTime))/(this->particleLifeTime));
		float relative_age = (glyph_frequency*fabs((particles.at(i).age-internalTime))/(this->particleLifeTime));
				
		glBegin(GL_TRIANGLES);
		glVertex4f(point.x, point.y, point.z, -1.0f);//contains the position
		
		glVertex4f(color.r, color.g, color.b, 0.0f);//contains the color
		
		glVertex4f(1.0f, particles.at(i).depth/(max_dist), relative_age, 0.0f);//contains alpha, depth and relative age
		glEnd();
	}

	//glEnd();

	//glDisable(GL_POINT_SMOOTH);
		
	renderShader.qfeDisable();

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeInkParticles::qfeRenderGuideLines(std::array<double,3> viewDir)
{
	//qfeUpdateColorMap();

	glLineWidth(2.0); 

	normalize(viewDir);

	for(unsigned int gl = 0; gl<this->guideLines.size(); gl++)
	{
		if(guideLines.at(gl).line.size()>0)
		{			
			glBegin(GL_LINE_STRIP);
				for(unsigned int i = 1; i<guideLines.at(gl).line.size(); i++) //first point has no predecessor
				{					
					std::array<double,3> dir;
					dir[0] = guideLines.at(gl).line.at(i-1).position.x - guideLines.at(gl).line.at(i).position.x;
					dir[1] = guideLines.at(gl).line.at(i-1).position.y - guideLines.at(gl).line.at(i).position.y;
					dir[2] = guideLines.at(gl).line.at(i-1).position.z - guideLines.at(gl).line.at(i).position.z;

					normalize(dir);

					float highlight = -(fabs(dot_product(viewDir,dir)))/2.0f;

					qfeColorRGB color = qfeGetColor(&guideLines.at(gl).line.at(i));
					glColor3f(color.r+highlight, color.g+highlight, color.b+highlight);

					qfePoint p = guideLines.at(gl).line.at(i).position;
					glVertex3f(p.x, p.y, p.z);
				}				
				qfePoint sp = guideLines.at(gl).seedPoint.position;
				glVertex3f(sp.x, sp.y, sp.z);
			glEnd();
			
			glBegin(GL_POINTS);
				for(unsigned int i = 0; i<guideLines.at(gl).line.size(); i++) //first point has no predecessor
				{
					qfePoint p = guideLines.at(gl).line.at(i).position;
					glVertex3f(p.x, p.y, p.z);
				}
			glEnd();
		}
	}

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeRenderGraph(unsigned int x_size, unsigned int y_size, graph_data_type data_id, bool invert_graph)
{
	unsigned int map_x_size = x_size/2;
	unsigned int map_y_size = y_size/2;

	Array2f map_values(map_x_size, map_y_size);
	map_values.fill(map_x_size, map_y_size+1, 0.0f);

	this->selected_graph_data = data_id;

	for(unsigned int p = 0; p<particles.size(); p++)
	{
		float age = particles.at(p).age;
		if(age<0)
			continue;
		
		unsigned int x_coord = (unsigned int) map_x_size*(age/this->particleLifeTime);

		if(x_coord>=map_x_size)
			continue;

		float value = 0.0f;
		
		if(data_id == UncertaintyGraph)
			value = particles.at(p).deviation;

		if(data_id == SpeedGraph)
			value = particles.at(p).speed*2.0;

		if(value>venc)
			value = venc;

		if(invert_graph)
			value = venc-value;

		unsigned int y_coord = (unsigned int) map_y_size*(value/venc);

		map_values(x_coord, y_coord)++;
		map_values(x_coord, map_y_size)++;
	}

	GLfloat *image = new GLfloat[x_size*y_size*4]();

	for(unsigned int i = 0; i<x_size; i++)//width
	{
		float s = (float)i/(x_size);

		for(unsigned int j = 0; j<y_size; j++)//height
		{
			float t = (float)j/(y_size);

			Vec2f point(s*(float)map_x_size,t*(float)map_y_size);
			Vec2f column_count(s*(float)map_x_size,map_y_size);
			float val = interpolate_value(point,map_values);
			float count = interpolate_value(column_count,map_values);

			float value = 0.0f;
			if(val>0.0f && count>0.0f)
			{
				value = sqrt(val/count);
			}

			//qfeColorRGB color = qfeInterpolateColor(value);
			int border_width = 2;
			if(j<=border_width || i<=border_width || (j>=y_size-(border_width+1)) || (i>=x_size-(border_width+1))) //border
			{
				value = 1.0f;
			}

			image[4*(i+x_size*j)+0] = 1.0-value; //R
			image[4*(i+x_size*j)+1] = 1.0-value; //G
			image[4*(i+x_size*j)+2] = 1.0-value; //B
			image[4*(i+x_size*j)+3] = 1.0f; //A
		}
	}
	
	glDrawPixels(x_size,y_size,GL_RGBA,GL_FLOAT,image);

	delete[] image;

	return qfeSuccess;
}

std::vector<statistic> qfeInkParticles::qfeGetStatistics()
{
	std::vector<statistic> output;

	if(this->particleSinks->size() == 0)
		return output;

	statistic new_statistic;
	new_statistic.label = "Total number of particles";
	new_statistic.value = (float)this->total_number_of_particles;
	output.push_back(new_statistic);

	new_statistic.label = "Removed due age (%)";
	new_statistic.value = 100.0f*(float)this->number_of_particles_removed_age/(float)this->total_number_of_particles;
	output.push_back(new_statistic);

	new_statistic.label = "Removed due leaving domain (%)";
	new_statistic.value =  100.0f*(float)this->number_of_particles_removed_domain/(float)this->total_number_of_particles;
	output.push_back(new_statistic);

	for(unsigned int i = 0; i<this->particleSinks->size(); i++)
	{
		std::ostringstream strs;
		strs << "Removed due particle sink "<<i<<" (%)";

		new_statistic.label = strs.str();
		new_statistic.value =  100.0f*(float)particleSinks->at(i)->counter/(float)this->total_number_of_particles;
		output.push_back(new_statistic);
	}

	return output;
}

qfeReturnStatus qfeInkParticles::qfeSetSelectionRange(Vec2f _age_selection, Vec2f _filter_selection, bool invert_graph)
{
	if(_age_selection[0] < 0.0f || _age_selection[1] < 0.0f || _filter_selection[0] < 0.0f || _filter_selection[1] < 0.0f)
	{
		valid_selection_range = false;

		return qfeSuccess;
	}

	valid_selection_range = true;

	age_selection = _age_selection;

	filter_selection = _filter_selection;


	if(invert_graph)
	{
		filter_selection[0] = 1.0f-_filter_selection[1];
		filter_selection[1] = 1.0f-_filter_selection[0];
	}

	filter_selection[0] *= venc;
	filter_selection[1] *= venc;

	if(filter_selection[1] > venc - 1.0)
		filter_selection[1] = 100.0*venc;

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeStoreInkDensity()
{	
	//dimensions
	unsigned int dimX, dimY, dimZ;

	dimX = this->volumeSize.at(0)*2;
	dimY = this->volumeSize.at(1)*2;
	dimZ = this->volumeSize.at(2)*2;

	std::string filename = "ink_volume.vti";

	std::cout<<"Writing "<<filename;
			
	// store the data arrays
	vtkSmartPointer<qfeImageData> imageData = 
		vtkSmartPointer<qfeImageData>::New();
	imageData->SetDimensions(dimX,dimY,dimZ);

	vtkSmartPointer<vtkFloatArray> ink_volume =
		vtkSmartPointer<vtkFloatArray>::New();		
	ink_volume->SetNumberOfComponents(1);
	ink_volume->SetNumberOfTuples(dimX * dimY * dimZ);

	for (int k = 0; k < (int)dimZ; k++)
	{
		for (int j = 0; j < (int)dimY; j++)
		{
			for (int i = 0; i < (int)dimX; i++)
			{					
				int id = i+dimX*(j+dimY*k);
				ink_volume->SetValue(id, 0.0);
			}
		}
	}

	for(unsigned int p = 0; p<particles.size(); p++)
	{
		particle part = particles.at(p);
		qfePoint point = (part.position * P2V) * V2T; //position in patient coords to texture coords

		int i = point.x * dimX;
		int j = point.y * dimY;
		int k = point.z * dimZ;

		int id = i+dimX*(j+dimY*k);

		float value = ink_volume->GetValue(id);
		ink_volume->SetValue(id, value+1.0);
	}

	ink_volume->SetName("ink density");
	imageData->GetPointData()->AddArray(ink_volume);
	imageData->GetPointData()->SetActiveScalars("qflow anatomy");
	imageData->Update();
		
	vtkSmartPointer<vtkXMLImageDataWriter> writer =
		vtkSmartPointer<vtkXMLImageDataWriter>::New();
	
	writer->SetDataModeToBinary();
	writer->SetFileName(filename.c_str());
	writer->SetInput(imageData);
	writer->Write();

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSetSeedLine(InkLine _seedPosition)
{
	//check if equal:
	bool equal = seedPosition.size() == _seedPosition.size();

	equal = number_of_lines == guideLines.size();

	if(equal)
	{
		for(unsigned int i = 0; i<seedPosition.size(); i++)
		{
			if(equal)
				equal = seedPosition.at(i) == _seedPosition.at(i);
		}
	}

	if(equal)
		return qfeSuccess;

	this->seedPosition = _seedPosition;

	std::cout<<"P1: ("<<_seedPosition[0].x<<","<<_seedPosition[0].y<<","<<_seedPosition[0].z<<")"<<std::endl;
	std::cout<<"P2: ("<<_seedPosition[1].x<<","<<_seedPosition[1].y<<","<<_seedPosition[1].z<<")"<<std::endl;

	this->qfeClearParticles();

	this->fixedPositionsComputed = false;
	this->fixedSeedPositions.clear();
	
	for(unsigned int i = 0; i<number_of_lines; i++)
	{				
		float fraction = 0.5f;
		if(number_of_lines > 1)
			fraction = i/(float)(number_of_lines-1);

		particle seedPoint;
		seedPoint.age = 0.0f;
		seedPoint.position = (seedPosition[0]*(1.0f-fraction) + seedPosition[1]*(fraction)) * V2P;
		seedPoint.seed_time = this->currentTime;
		seedPoint.seed_id1 = fraction;
		seedPoint.seed_id2 = 1.0f-fraction;
		seedPoint.deviation = 0.0f;

		guideline gl;
		gl.seedPoint = seedPoint;
		
		guideLines.push_back(gl);
	}
	

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSetSeedVolume(InkVolume _seedVolume)
{
	//check if equal:
	bool equal = seedVolumePoints.size() == _seedVolume.size();

	if(equal)
		equal = this->number_of_lines == guideLines.size();

	if(equal)
	{
		for(unsigned int i = 0; i<seedVolumePoints.size(); i++)
		{
			if(equal)
				equal = seedVolumePoints.at(i) == _seedVolume.at(i);
		}
	}
	if(equal)
		return qfeSuccess;

	this->seedVolumePoints = _seedVolume;

	std::cout<<"P1: ("<<_seedVolume[0].x<<","<<_seedVolume[0].y<<","<<_seedVolume[0].z<<")"<<std::endl;
	std::cout<<"P2: ("<<_seedVolume[1].x<<","<<_seedVolume[1].y<<","<<_seedVolume[1].z<<")"<<std::endl;
	std::cout<<"P3: ("<<_seedVolume[2].x<<","<<_seedVolume[2].y<<","<<_seedVolume[2].z<<")"<<std::endl;

	this->fixedPositionsComputed = false;
	this->fixedSeedPositions.clear();

	this->qfeClearParticles();
	
	seedVolume.qfeSetGuidePoints(_seedVolume[0]*V2P, _seedVolume[1]*V2P, _seedVolume[2]*V2P);

	seedVolume.GenerateSeedPointsOnClipPlaneSurface(this->number_of_lines);

	std::vector<qfePoint> surfacePoints = seedVolume.qfeGetSurfacePoints();

	for(unsigned int i = 0; i<surfacePoints.size(); i++)
	{		
		particle seedPoint;
		seedPoint.age = 0.0f;
		seedPoint.position = surfacePoints.at(i);
		seedPoint.seed_time = this->currentTime;
		seedPoint.seed_id1 = 0.0f;
		seedPoint.seed_id2 = 0.0f;
		seedPoint.deviation = 0.0f;
		
		guideline gl;
		gl.seedPoint = seedPoint;
		
		guideLines.push_back(gl);
	}
	

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSetFeatureBasedSeeds(std::vector<featureSeedPoint> _featureSeeds)
{
	this->featureBasedSeeds = _featureSeeds;

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeGetSeedVolumeDetails(qfeVector &a1, qfeVector &a2, qfeVector &a3, qfePoint &c)
{
	a1 = seedVolume.qfeGetAxis1();
	a2 = seedVolume.qfeGetAxis2();
	a3 = seedVolume.qfeGetAxis3();
	c = seedVolume.qfeGetCenter();

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSetGapSettings(int _spatialGapAmount1, int _spatialGapAmount2, int _temporalGapAmount, float _spatialGapWidth1, float _spatialGapWidth2, float _temporalGapWidth)
{
	spatialGapAmount1 = _spatialGapAmount1;
	spatialGapAmount2 = _spatialGapAmount2;
	temporalGapAmount = _temporalGapAmount;
	spatialGapWidth1 = _spatialGapWidth1;
	spatialGapWidth2 = _spatialGapWidth2;
	temporalGapWidth = _temporalGapWidth;

	return qfeSuccess;
}

//get number of ink particles
unsigned int qfeInkParticles::qfeGetNumberOfInkParticles()
{
	return particles.size();
}

//get SNR volume data:
qfeReturnStatus qfeInkParticles::qfeGetSNRVolumes(std::vector<qfeVolume> &volumes, std::array<float, 2> &range, unsigned int dimensions)
{	
	volumes.clear();

	assert(dimensions>=1 && dimensions <=3);

	float max = 0.0f;

	for(unsigned int id = 0; id<SNR_data.size(); id++)
	{
		float *data = new float[dimensions*this->volumeSize[0]*volumeSize[1]*volumeSize[2]];

		qfeVolume SNRVolume;

		//init volume:		
		qfeGrid *grid;
		this->volumeData->at(0)->qfeGetVolumeGrid(&grid);
		SNRVolume.qfeSetVolumeComponentsPerVoxel(dimensions);
		SNRVolume.qfeSetVolumeGrid(grid);
		
		for(unsigned int k = 0; k<volumeSize[2]; k++)
		for(unsigned int j = 0; j<volumeSize[1]; j++)
		for(unsigned int i = 0; i<volumeSize[0]; i++)
		{
			unsigned int offset = dimensions*((int)i + (int)j*volumeSize[0] + (int)k*volumeSize[0]*volumeSize[1]);
						
			if(SNR_data[id].at(i,j,k)>max)
				max = SNR_data[id].at(i,j,k);

			data[offset+0] = SNR_data[id].at(i,j,k);
			if(dimensions>1)
				data[offset+1] = 0.0f;
			if(dimensions>2)
				data[offset+2] = 0.0f;
		}

		SNRVolume.qfeSetVolumeData(qfeValueTypeFloat, volumeSize[0], volumeSize[1], volumeSize[2], data);
		SNRVolume.qfeSetVolumeLabel("SNR");
		
		SNRVolume.qfeUpdateVolume();

		volumes.push_back(SNRVolume);
		
		delete[] data;
	}

	range[0] = 0.0f;
	range[1] = max;

	return qfeSuccess;
}

//get signal strength volume data:
qfeReturnStatus qfeInkParticles::qfeGetSignalStrengthVolumes(std::vector<qfeVolume> &volumes, std::array<float, 2> &range, unsigned int dimensions)
{
	volumes.clear();

	assert(dimensions>=1 && dimensions <=3);

	float max = 0.0f;
	
	for(unsigned int id = 0; id<SNR_data.size(); id++)
	{
		float *data = new float[dimensions*this->volumeSize[0]*volumeSize[1]*volumeSize[2]];

		qfeVolume SignalVolume;

		//init volume:		
		qfeGrid *grid;
		this->volumeData->at(0)->qfeGetVolumeGrid(&grid);
		SignalVolume.qfeSetVolumeComponentsPerVoxel(dimensions);
		SignalVolume.qfeSetVolumeGrid(grid);
		
		for(unsigned int k = 0; k<volumeSize[2]; k++)
		for(unsigned int j = 0; j<volumeSize[1]; j++)
		for(unsigned int i = 0; i<volumeSize[0]; i++)
		{
			unsigned int offset = dimensions*((int)i + (int)j*volumeSize[0] + (int)k*volumeSize[0]*volumeSize[1]);
						
			if(SNR_data[id].at(i,j,k) * this->sigma_data[id]>max)
				max = SNR_data[id].at(i,j,k) * this->sigma_data[id];

			data[offset+0] = SNR_data[id].at(i,j,k) * this->sigma_data[id]; //convert to signal strength
			if(dimensions>1)
				data[offset+1] = 0.0f;
			if(dimensions>2)
				data[offset+2] = 0.0f;
		}

		SignalVolume.qfeSetVolumeData(qfeValueTypeFloat, volumeSize[0], volumeSize[1], volumeSize[2], data);
		SignalVolume.qfeSetVolumeLabel("Signal strength");
		
		SignalVolume.qfeUpdateVolume();

		volumes.push_back(SignalVolume);
		
		delete[] data;
	}

	range[0] = 0.0f;
	range[1] = max;

	return qfeSuccess;
}

//get particle density volume data:
qfeReturnStatus qfeInkParticles::qfeGetParticleDensityVolume(qfeVolume &volume, unsigned int dimensions, bool volumetricSeeding)
{
	assert(dimensions>=1 && dimensions <=3);

	float max = 0.0f;	

	//subsampling -> more voxels per actually measured voxels
	unsigned int subsample = 2;

	float *data = new float[dimensions*(this->volumeSize[0]*subsample*volumeSize[1]*subsample*volumeSize[2]*subsample)];
	
	//init volume:		
	qfeGrid *grid;
	this->volumeData->at(0)->qfeGetVolumeGrid(&grid);

	qfeGrid *new_grid = new qfeGrid(*grid);

	float extent_x, extent_y, extent_z = 0.0f;
	grid->qfeGetGridExtent(extent_x, extent_y, extent_z);

	new_grid->qfeSetGridExtent(extent_x/(float)subsample, extent_y/(float)subsample, extent_z/(float)subsample);

	volume.qfeSetVolumeComponentsPerVoxel(dimensions);
	volume.qfeSetVolumeGrid(new_grid); 
	
	for(unsigned int k = 0; k<volumeSize[2]*subsample; k++)
	for(unsigned int j = 0; j<volumeSize[1]*subsample; j++)
	for(unsigned int i = 0; i<volumeSize[0]*subsample; i++)
	{
		unsigned int offset = dimensions*((int)i + (int)j*volumeSize[0] + (int)k*volumeSize[0]*volumeSize[1]);
		
		data[offset+0] = 0.0f;
		if(dimensions>1)
			data[offset+1] = 0.0f;
		if(dimensions>2)
			data[offset+2] = 0.0f;
	}

	for(unsigned int p = 0; p<particles.size(); p++)
	{
		qfePoint voxel = particles.at(p).position * P2V;
		voxel.x *= subsample; voxel.y *= subsample; voxel.z *= subsample;

		unsigned int offset = dimensions*((int)voxel.x + (int)voxel.y*volumeSize[0]*subsample + (int)voxel.z*volumeSize[0]*subsample*volumeSize[1]*subsample);
		
		if(data[offset+0]<0.5f) //create a decent offset (otherwise the MIP will be vague)
			data[offset+0] = 0.5f;

		if(volumetricSeeding) //seeding is less dense so density should be compensated for
			data[offset+0] += 0.05f/this->particleMultiplier;
		if(!volumetricSeeding) //seeding is denser so density should be compensated for
			data[offset+0] += 0.05f/this->particleMultiplier;
		if(data[offset+0]>1.0f)
			data[offset+0] = 1.0f;
	}	

	volume.qfeSetVolumeData(qfeValueTypeFloat, volumeSize[0]*subsample, volumeSize[1]*subsample, volumeSize[2]*subsample, data);
	volume.qfeSetVolumeLabel("Particle density");

	volume.qfeSetHistogramToNull();
		
	volume.qfeUpdateVolume();
		
	delete[] data;

	return qfeSuccess;
}

//get mesh levelset volume:
qfeReturnStatus qfeInkParticles::qfeGetMeshLevelsetVolume(qfeVolume &volume, std::array<float, 2> &range, unsigned int dimensions)
{
	assert(dimensions>=1 && dimensions <=3);
		
	float *data = new float[dimensions*this->volumeSize[0]*volumeSize[1]*volumeSize[2]];
	
	//init volume:
	qfeGrid *grid;
	this->volumeData->at(0)->qfeGetVolumeGrid(&grid);
	volume.qfeSetVolumeComponentsPerVoxel(dimensions);
	volume.qfeSetVolumeGrid(grid);

	float max = 0.0f;
	
	for(unsigned int k = 0; k<volumeSize[2]; k++)
	for(unsigned int j = 0; j<volumeSize[1]; j++)
	for(unsigned int i = 0; i<volumeSize[0]; i++)
	{
		unsigned int offset = dimensions*((int)i + (int)j*volumeSize[0] + (int)k*volumeSize[0]*volumeSize[1]);
						
		float value = this->mesh_level_set.at(i,j,k);

		data[offset+0] = value;
		if(dimensions>1)
			data[offset+1] = 0.0f;
		if(dimensions>2)
			data[offset+2] = 0.0f;

		if(abs(value) > max)
			max = abs(value);
	}
	
	range[0] = -max;
	range[1] = max;
	
	std::cout<<"Volume range: "<<range[0]<<" "<<range[1]<<std::endl;

	volume.qfeSetVolumeData(qfeValueTypeFloat, volumeSize[0], volumeSize[1], volumeSize[2], data);
	volume.qfeSetVolumeLabel("Mesh levelset");
	volume.qfeSetVolumeValueDomain(range[0], range[1]);
		
	volume.qfeSetHistogramToNull();

	volume.qfeUpdateVolume();
		
	delete[] data;

	return qfeSuccess;
}

//get per voxel seeding origin volume data:
qfeReturnStatus qfeInkParticles::qfeGetSeedingOriginVolume(qfeVolume &volume, unsigned int dimensions, unsigned int selected_particle_sink)
{
	assert(dimensions>=1 && dimensions <=3);

	float *data = new float[dimensions*(this->volumeSize[0]*volumeSize[1]*volumeSize[2])];
	
	//init volume:		
	qfeGrid *grid;
	this->volumeData->at(0)->qfeGetVolumeGrid(&grid);

	qfeGrid *new_grid = new qfeGrid(*grid);

	float extent_x, extent_y, extent_z = 0.0f;
	grid->qfeGetGridExtent(extent_x, extent_y, extent_z);

	new_grid->qfeSetGridExtent(extent_x, extent_y, extent_z);

	volume.qfeSetVolumeComponentsPerVoxel(dimensions);
	volume.qfeSetVolumeGrid(new_grid); 

	int per_voxel_count = -1;
	for(unsigned int i = 0; i<this->reason_particles_removed.size(); i++)
	{
		if(this->reason_particles_removed.at(i).first == selected_particle_sink)
		{
			per_voxel_count = i;

			continue;
		}
	}
	
	for(unsigned int k = 0; k<volumeSize[2]; k++)
	for(unsigned int j = 0; j<volumeSize[1]; j++)
	for(unsigned int i = 0; i<volumeSize[0]; i++)
	{
		unsigned int offset = dimensions*((int)i + (int)j*volumeSize[0] + (int)k*volumeSize[0]*volumeSize[1]);
		
		float value = 0.0f;

		if(per_voxel_count >= 0)
		{
			float total_removed_due_to_sink = (float)this->reason_particles_removed.at(per_voxel_count).second.at(i,j,k);
			float total_released_from_voxel = (float)this->particlesSeededFromVoxel(i,j,k);

			if(total_released_from_voxel > 0)
				value = total_removed_due_to_sink / total_released_from_voxel;
		}
		
		data[offset+0] = value;
		if(dimensions>1)
			data[offset+1] = 0.0f;
		if(dimensions>2)
			data[offset+2] = 0.0f;
	}
	
	volume.qfeSetVolumeData(qfeValueTypeFloat, volumeSize[0], volumeSize[1], volumeSize[2], data);
	volume.qfeSetVolumeLabel("Seeding Origin");

	volume.qfeSetHistogramToNull();
		
	volume.qfeUpdateVolume();
		
	delete[] data;

	return qfeSuccess;
}

//get per voxel seeding origin volume data:
qfeReturnStatus qfeInkParticles::qfeGetAllSeedingOriginVolume(qfeVolume &volume, unsigned int dimensions)
{
	assert(dimensions>=1 && dimensions <=3);

	float *data = new float[dimensions*(this->volumeSize[0]*volumeSize[1]*volumeSize[2])];
	
	//init volume:		
	qfeGrid *grid;
	this->volumeData->at(0)->qfeGetVolumeGrid(&grid);

	qfeGrid *new_grid = new qfeGrid(*grid);

	float extent_x, extent_y, extent_z = 0.0f;
	grid->qfeGetGridExtent(extent_x, extent_y, extent_z);

	new_grid->qfeSetGridExtent(extent_x, extent_y, extent_z);

	volume.qfeSetVolumeComponentsPerVoxel(dimensions);
	volume.qfeSetVolumeGrid(new_grid); 
	
	bool mesh_clipped = false;

	for(unsigned int i = 0; i<reason_particles_removed.size(); i++)
	{
		if(reason_particles_removed.at(i).first == 0) //particles were removed due to the mesh
		{
			mesh_clipped = true;
			continue;
		}
	}
	
	for(unsigned int k = 0; k<volumeSize[2]; k++)
	for(unsigned int j = 0; j<volumeSize[1]; j++)
	for(unsigned int i = 0; i<volumeSize[0]; i++)
	{
		unsigned int offset = dimensions*((int)i + (int)j*volumeSize[0] + (int)k*volumeSize[0]*volumeSize[1]);
		
		float value = 0.0f;

		int max_reason_id = -1;
		int max_removed = -1;

		for(unsigned int index = 0; index<this->reason_particles_removed.size(); index++)
		{
			if(this->reason_particles_removed.at(index).second.at(i,j,k) > max_removed)
			{
				max_removed = this->reason_particles_removed.at(index).second.at(i,j,k);

				max_reason_id = this->reason_particles_removed.at(index).first; //store unique particle sink id
			}
		}

		float total_removed_due_to_sink = 0.0f;
		if(max_reason_id>=0)
			total_removed_due_to_sink = (float)max_removed;

		unsigned sink_index = 0;
		for(unsigned int c = 0; c<particleSinks->size(); c++)
		{
			if(max_reason_id == particleSinks->at(c)->unique_id)
			{
				if(mesh_clipped)
					c += 1;
				sink_index = c;
				break;
			}
		}

		float total_released_from_voxel = (float)this->particlesSeededFromVoxel(i,j,k);
		
		if(total_released_from_voxel > 0)
			value = (sink_index) + total_removed_due_to_sink / total_released_from_voxel;

		data[offset+0] = value;
		if(dimensions>1)
			data[offset+1] = 0.0f;
		if(dimensions>2)
			data[offset+2] = 0.0f;
	}
	
	volume.qfeSetVolumeData(qfeValueTypeFloat, volumeSize[0], volumeSize[1], volumeSize[2], data);
	volume.qfeSetVolumeLabel("Seeding Origin");

	volume.qfeSetHistogramToNull();
		
	volume.qfeUpdateVolume();
		
	delete[] data;

	return qfeSuccess;
}

//get per voxel number of particles leaving the mesh
qfeReturnStatus qfeInkParticles::qfeGetParticlesLeavingMeshVolume(qfeVolume &volume, unsigned int dimensions)
{
	assert(dimensions>=1 && dimensions <=3);

	float *data = new float[dimensions*(this->volumeSize[0]*volumeSize[1]*volumeSize[2])];
	
	//init volume:		
	qfeGrid *grid;
	this->volumeData->at(0)->qfeGetVolumeGrid(&grid);

	qfeGrid *new_grid = new qfeGrid(*grid);

	float extent_x, extent_y, extent_z = 0.0f;
	grid->qfeGetGridExtent(extent_x, extent_y, extent_z);

	new_grid->qfeSetGridExtent(extent_x, extent_y, extent_z);

	volume.qfeSetVolumeComponentsPerVoxel(dimensions);
	volume.qfeSetVolumeGrid(new_grid); 

	unsigned int max_count = 0;

	for(unsigned int k = 0; k<particlesLeavingMeshFromVoxel.nk; k++)
	for(unsigned int j = 0; j<particlesLeavingMeshFromVoxel.nj; j++)
	for(unsigned int i = 0; i<particlesLeavingMeshFromVoxel.ni; i++)
	{
		if(this->particlesLeavingMeshFromVoxel.at(i,j,k) > max_count)
		{
			max_count = this->particlesLeavingMeshFromVoxel.at(i,j,k);
		}
	}

	for(unsigned int k = 0; k<volumeSize[2]; k++)
	for(unsigned int j = 0; j<volumeSize[1]; j++)
	for(unsigned int i = 0; i<volumeSize[0]; i++)
	{
		unsigned int offset = dimensions*((int)i + (int)j*volumeSize[0] + (int)k*volumeSize[0]*volumeSize[1]);
		
		float value = sqrt((float)this->particlesLeavingMeshFromVoxel.at(i,j,k) / (float)max_count);
		
		data[offset+0] = value;
		if(dimensions>1)
			data[offset+1] = 0.0f;
		if(dimensions>2)
			data[offset+2] = 0.0f;
	}
	
	volume.qfeSetVolumeData(qfeValueTypeFloat, volumeSize[0], volumeSize[1], volumeSize[2], data);
	volume.qfeSetVolumeLabel("Seeding Origin");

	volume.qfeSetHistogramToNull();
		
	volume.qfeUpdateVolume();
		
	delete[] data;

	return qfeSuccess;
}

std::vector<particle>* qfeInkParticles::getFixedSeedPositions()
{
	return &this->fixedSeedPositions;
}

void qfeInkParticles::setFixedSeedPositions(std::vector<particle> *positions)
{
	this->fixedSeedPositions.clear();
	this->fixedSeedPositions = *positions;
}

unsigned int qfeInkParticles::qfeGetSeedVolumeCount()
{
	unsigned int count = particleSinks->size();

	for(unsigned int i = 0; i<reason_particles_removed.size(); i++)
	{
		if(reason_particles_removed.at(i).first == 0) //particles were removed due to the mesh
		{
			count++;
			continue;
		}
	}

	return count;
}

qfeColorMap *qfeInkParticles::qfeGetSeedVolumeColorMap()
{
	vector<qfeRGBMapping>     rgb;
	vector<qfeOpacityMapping> o; 

	int extra_colors = 0;
	for(unsigned int i = 0; i<reason_particles_removed.size(); i++)
	{
		if(reason_particles_removed.at(i).first == 0) //particles were removed due to the mesh
		{
			extra_colors = 2;
			continue;
		}
	}
	
	unsigned int particleSinkSize = particleSinks->size();

	unsigned int size = 2*particleSinkSize+extra_colors;
	rgb.resize(size);

	float step = 1.0/(size-1);
	//black to color
	//first is gray for particles that left the domain
	if(extra_colors == 2)
	{
		rgb[0].color.r = 0.0; rgb[0].color.g = 0.0; rgb[0].color.b = 0.0; rgb[0].value = 0.0f;
		rgb[1].color.r = 0.5; rgb[1].color.g = 0.5; rgb[1].color.b = 0.5; rgb[1].value = step;
	}
	for(unsigned int i = 0; i<particleSinkSize; i++)
	{
		int index = 2*i+extra_colors;

		rgb[index].color.r = 0.0; rgb[index].color.g = 0.0; rgb[index].color.b = 0.0; rgb[index].value = step*index;

		qfeColorRGB col = particleSinks->qfeGetSeedVolumeColor(i);
		rgb[index+1].color.r = col.r; rgb[index+1].color.g = col.g; rgb[index+1].color.b = col.b; rgb[index+1].value = step*(index+1);
	}
	
	o.resize(2);
	o[0].opacity = 1.0; o[0].value = 0.0;
	o[1].opacity = 1.0; o[1].value = 1.0;
	
	seedVolumeColorMap.qfeSetColorMapRGB(size, rgb);
	seedVolumeColorMap.qfeSetColorMapA(2, o);
	seedVolumeColorMap.qfeSetColorMapG(2, o);
	seedVolumeColorMap.qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
	seedVolumeColorMap.qfeSetColorMapSpace(qfeColorSpaceLab);

	seedVolumeColorMap.qfeUploadColorMap();

	return &seedVolumeColorMap;
}

//set color map
qfeReturnStatus qfeInkParticles::qfeUpdateColorMap(qfeColorMap *colorMap)
{
	unsigned int size = 0;
	colorMap->qfeGetColorMapRGBSize(size); //no color map yet initialized
	if(size == 0)
	{
		return qfeSuccess;
	}

	colorMap->qfeGetAllColorsRGB(this->colors);
	this->color_size = this->colors.size() - 1;

	this->updateColorMap();

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeInverseColorMap(bool _inverseColorMap)
{
	if(inverseColorMap == _inverseColorMap)
	{
		return qfeSuccess;
	}

	inverseColorMap = _inverseColorMap;

	this->updateColorMap();

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeClipSeedOutsideMesh(bool _clip_seed_outside_mesh)
{
	if(clip_seed_outside_mesh == _clip_seed_outside_mesh)
	{
		return qfeSuccess;
	}

	clip_seed_outside_mesh = _clip_seed_outside_mesh;

	this->qfeClearParticles();

	return qfeSuccess;
}


qfeReturnStatus qfeInkParticles::qfeClipOutsideMesh(bool _clip_outside_mesh, float _clipping_margin)
{
	clipping_margin = _clipping_margin;

	if(_clip_outside_mesh)
	{
		clip_seed_outside_mesh = true;
	}

	if(clip_outside_mesh == _clip_outside_mesh)
	{
		return qfeSuccess;
	}

	clip_outside_mesh = _clip_outside_mesh;

	this->qfeClearParticles();

	return qfeSuccess;
}

//set color setting
qfeReturnStatus qfeInkParticles::qfeSetColorSetting(unsigned int id)
{
	this->color_setting = id;

	return qfeSuccess;
}

//set number of guidelines
qfeReturnStatus qfeInkParticles::qfeSetNumberOfLines(unsigned int _number_of_lines)
{
	this->number_of_lines = _number_of_lines;

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSetPhase(double current)
{
	if(current != this->currentTime)
	{
		this->currentTime = current;
		this->internalTime = current;
		
		updateTimeLinePlot();
	}
	
	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSetParticleLifeTime(double lifeTime)
{
	this->particleLifeTime = lifeTime; //convert to seconds

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSetTimeStepSize(double timeStep)
{
	this->timeStepSize = timeStep;

	return qfeSuccess;
}

double qfeInkParticles::qfeGetTimeStepSize()
{
	return this->timeStepSize;
}

qfeReturnStatus qfeInkParticles::qfeSetStopSeedingTime(double stopSeeding)
{
	if(stopSeeding<=0.0)
		stopSeedingTime = 1e12;
	else
		stopSeedingTime = stopSeeding;

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeUseUncertainty(bool _uncertainty)
{
	if(this->use_uncertainty != _uncertainty)
	{
		this->use_uncertainty = _uncertainty;

		this->qfeClearParticles();
	}

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSetAdvectionType(advection_type _advection)
{
	if(this->advection != _advection)
	{
		this->advection = _advection;

		this->qfeClearParticles();
	}

	return qfeSuccess;
}

/*
qfeReturnStatus qfeInkParticles::qfeSetNumberOfColorBands(unsigned int _colorBands)
{
	if(nr_color_bands != _colorBands)
	{
		this->nr_color_bands = _colorBands;

		this->updateColorMap();
	}

	return qfeSuccess;
}
*/

//to control the amount of particles that should be seeded every step
qfeReturnStatus qfeInkParticles::qfeSetParticleMultiplier(float _multiplier)
{
	if(this->particleMultiplier != _multiplier)
	{
		this->particleMultiplier = _multiplier;

		this->fixedPositionsComputed = false;
		fixedSeedPositions.clear();
	}

	return qfeSuccess;
}

//to control the opacity of particles
qfeReturnStatus qfeInkParticles::qfeSetParticleOpacity(float _opacity)
{
	this->particleOpacity = _opacity;

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSeedParticles(InkLine line, bool seedFromVoxelCenter, bool seedFromFixedPositions, float seedFromFixedPositionsMultiplier)
{
	if(internalTime-currentTime > stopSeedingTime)
		return qfeSuccess;

	if(!seedFromFixedPositions || fabs(fixedPositionsMultiplier - seedFromFixedPositionsMultiplier) > 1e-6f)
	{
		this->fixedPositionsComputed = false;
		fixedPositionsMultiplier = seedFromFixedPositionsMultiplier;
		fixedSeedPositions.clear();
	}

	unsigned int count_before_seed = particles.size();

	unsigned int internal_steps = ceil(4.0*timeStepSize/cfl); //steps executed by the advection algorithm

	for(unsigned int gl = 0; gl < this->guideLines.size(); gl++)
	{
		particle point = guideLines.at(gl).seedPoint;

		for(int i = 0; i<internal_steps; i++)
		{
			float t = timeStepSize*((float)i/internal_steps);
			
			switch(advection)
			{
			case Stream: 
				{
					point.age = -t;
					point.seed_time = this->currentTime;
					
					guideLines.at(gl).line.push_back(point);
				}
				break;
			case Path: 
				{
					point.age = -t;
					point.seed_time = this->currentTime;
					
					guideLines.at(gl).line.push_back(point);
				}
				break;
			case Streak: 
				{
					point.age = -t;
					point.seed_time = this->internalTime+t;
					
					guideLines.at(gl).line.push_back(point);
				}
				break;
			default:
				{
					point.age = -t;
					point.seed_time = this->currentTime;
					
					guideLines.at(gl).line.push_back(point);
				}
				break;
			}
		}
	}	

	float dist = sqrt(
		(line[0].x-line[1].x)*(line[0].x-line[1].x) + 
		(line[0].y-line[1].y)*(line[0].y-line[1].y) + 
		(line[0].z-line[1].z)*(line[0].z-line[1].z)
		);

	particle p;

	unsigned int steps_per_voxel = 10;
	if(seedFromVoxelCenter)
	{
		steps_per_voxel = 1;
	}

	unsigned int total_steps = (unsigned int)((dist*(float)steps_per_voxel) * (float)internal_steps * particleMultiplier);

	qfePoint position;

	std::random_device rd;
	std::mt19937 generator(rd());
	std::uniform_real_distribution<> distribution(0, 1);
	
	if(seedFromFixedPositions && !this->fixedPositionsComputed)//generate seed positions
	{
		fixedSeedPositions.clear();
		for(unsigned int i = 0; i<=(unsigned int)((dist*(float)steps_per_voxel*fixedPositionsMultiplier)); i++)
		{
			float scale = distribution(generator);
			position.x = (1.0f-scale)*line[0].x + scale * line[1].x;
			position.y = (1.0f-scale)*line[0].y + scale * line[1].y;
			position.z = (1.0f-scale)*line[0].z + scale * line[1].z;

			p.position = qfePoint(position.x, position.y, position.z) * V2P;
			p.seed_id1 = scale;
			p.seed_id2 = 1.0f-scale;
			p.deviation = 0.0f;

			p.initial_voxel = Vec3i((int)position.x, (int)position.y, (int)position.z);

			p.deletion_reason = -1;

			fixedSeedPositions.push_back(p);
		}

		this->fixedPositionsComputed = true;
	}

	for(unsigned int i = 0; i<=total_steps; i++)
	{
		
		float scale = distribution(generator);

		if(seedFromFixedPositions)
		{
			//select a random fixed seed particle
			unsigned int random_index = (unsigned int)(scale*(fixedSeedPositions.size()-1));

			p = fixedSeedPositions.at(random_index);
		}
		else
		{		
			position.x = (1.0f-scale)*line[0].x + scale * line[1].x;
			position.y = (1.0f-scale)*line[0].y + scale * line[1].y;
			position.z = (1.0f-scale)*line[0].z + scale * line[1].z;

			if(    position.x < 0.0f || position.x > particlesSeededFromVoxel.ni-1
				|| position.y < 0.0f || position.y > particlesSeededFromVoxel.nj-1
				|| position.z < 0.0f || position.z > particlesSeededFromVoxel.ni-1)
				continue;

			if(clip_seed_outside_mesh && mesh_level_set.ni != 0 && mesh_level_set.nj != 0 && mesh_level_set.nk != 0 
				&& mesh_level_set.interpolate_value(position.x, position.y, position.z) < 0.0f)
			{
				continue;
			}

			if(seedFromVoxelCenter)
			{
				position.x = floor(position.x);
				position.y = floor(position.y);
				position.z = floor(position.z);
			}
					
			p.position = qfePoint(position.x, position.y, position.z) * V2P;
			p.seed_id1 = scale;
			p.seed_id2 = 1.0f-scale;
			p.deviation = 0.0f;

			p.initial_voxel = Vec3i((int)position.x, (int)position.y, (int)position.z);

			p.deletion_reason = -1;
		}

		float t = timeStepSize*distribution(generator);

		this->particlesSeededFromVoxel(p.initial_voxel[0], p.initial_voxel[1], p.initial_voxel[2])++;
		
		switch(advection)
		{
		case Stream: 
			{
				p.age = -t;
				p.seed_time = this->currentTime;
			}
			break;
		case Path: 
			{
				p.age = -t;
				p.seed_time = this->currentTime;
			}
			break;
		case Streak: 
			{
				p.age = -t;
				p.seed_time = this->internalTime+t;
			}
			break;
		default:
			{
				p.age = -t;
				p.seed_time = this->currentTime;
			}
			break;
		}
	
		particles.push_back(p);
	}

	//remove the particles that are outside the mesh (if this option was selected)
	if(clip_outside_mesh && mesh_level_set.ni != 0 && mesh_level_set.nj != 0 && mesh_level_set.nk != 0)
	{
		Array3f *level_set = &mesh_level_set;
		qfeMatrix4f pat2vox = P2V;

		float margin = 0.0f;//-this->clipping_margin;

		particles.erase(std::remove_if(particles.begin(), 
								 particles.end(),
								 [&pat2vox, level_set, &margin](particle p)
				{
					qfePoint voxel = p.position*pat2vox;
					return(level_set->interpolate_value(voxel.x, voxel.y, voxel.z) < margin);
				}),
				particles.end()
			);
	}
	
	total_number_of_particles += particles.size() - count_before_seed;

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSeedParticles(InkVolume volume, bool seedFromVoxelCenter, bool seedFromFixedPositions, float seedFromFixedPositionsMultiplier, bool plane)
{
	if(internalTime-currentTime > stopSeedingTime)
		return qfeSuccess;

	if(!seedFromFixedPositions || fabs(fixedPositionsMultiplier - seedFromFixedPositionsMultiplier) > 1e-6f)
	{
		this->fixedPositionsComputed = false;
		fixedPositionsMultiplier = seedFromFixedPositionsMultiplier;
		fixedSeedPositions.clear();
	}

	unsigned int count_before_seed = particles.size();

	unsigned int internal_steps = ceil(4.0*timeStepSize/cfl); //steps executed by the advection algorithm

	for(unsigned int gl = 0; gl < this->guideLines.size(); gl++)
	{
		particle point = guideLines.at(gl).seedPoint;
		for(int i = 0; i<internal_steps; i++)
		{
			float t = timeStepSize*((float)i/internal_steps);
			
			switch(advection)
			{
			case Stream: 
				{
					point.age = -t;
					point.seed_time = this->currentTime;
					
					guideLines.at(gl).line.push_back(point);
				}
				break;
			case Path: 
				{
					point.age = -t;
					point.seed_time = this->currentTime;
					
					guideLines.at(gl).line.push_back(point);
				}
				break;
			case Streak: 
				{
					point.age = -t;
					point.seed_time = this->internalTime+t;
					
					guideLines.at(gl).line.push_back(point);
				}
				break;
			default:
				{
					point.age = -t;
					point.seed_time = this->currentTime;
					
					guideLines.at(gl).line.push_back(point);
				}
				break;
			}
		}
	}	

	seedVolume.qfeSetGuidePoints(volume[0]*V2P, volume[1]*V2P, volume[2]*V2P);
	float volume_size = (seedVolume.qfeGetVolume()/2.0f);

	// compute constant particles per voxel based on voxel size
	float scale_voxels_per_cubed_cm = (1.0f/6.25f)*(1.0f/(spacing[0]*spacing[1]*spacing[2]));

	//generate seed points:
	qfePoint center = seedVolume.qfeGetCenter();
	qfeVector axis1 = seedVolume.qfeGetAxis1();
	qfeVector axis2 = seedVolume.qfeGetAxis2();
	qfeVector axis3 = seedVolume.qfeGetAxis3();
	float lambda1 = seedVolume.qfeGetLambda1();
	float lambda2 = seedVolume.qfeGetLambda2();
	float lambda3 = seedVolume.qfeGetLambda3();

	//seedEllipsoid.qfeGenerateSeedPoints(internal_steps*particleMultiplier*(int)(volume_ellipsoid*scale_voxels_per_cubed_cm));

	particle p;

	qfePoint position;

	std::random_device rd;
	std::mt19937 generator(rd());
	std::uniform_real_distribution<> distribution(0, 1);

	std::random_device rd_position;
	std::mt19937 position_generator(rd_position());
	std::uniform_real_distribution<> position_distribution(-1, 1);
	
	std::array<double,3> base_axis;
	base_axis[0] = axis1.x; base_axis[1] = axis1.y; base_axis[2] = axis1.z;

	std::array<double,3> plane_normal;
	plane_normal[0] = axis2.x; plane_normal[1] = axis2.y; plane_normal[2] = axis2.z;

	unsigned int nr_particles_to_seed = (unsigned int)(fixedPositionsMultiplier*particleMultiplier*volume_size*scale_voxels_per_cubed_cm);
		
	if(seedFromFixedPositions && !this->fixedPositionsComputed)//generate seed positions
	{
		fixedSeedPositions.clear();
		for(unsigned int i = 0; i<nr_particles_to_seed; i++)
		{
			float u1 = position_distribution(position_generator);
			float u2 = position_distribution(position_generator);
			float u3 = position_distribution(position_generator);

			if(plane)
				u2 = 0.0f;

			qfePoint position = center + u1*lambda1*axis1 + u2*lambda2*axis2 + u3*lambda3*axis3;
						
			if(!seedVolume.IsInVolume(position))
				continue;
			
			qfePoint vp = position*P2V;

			if(    vp.x < 0.0f || vp.x > particlesSeededFromVoxel.ni-1
				|| vp.y < 0.0f || vp.y > particlesSeededFromVoxel.nj-1
				|| vp.z < 0.0f || vp.z > particlesSeededFromVoxel.nk-1)
			{
				continue;
			}

			if(clip_seed_outside_mesh && mesh_level_set.ni != 0 && mesh_level_set.nj != 0 && mesh_level_set.nk != 0 
				&& mesh_level_set.interpolate_value(vp.x, vp.y, vp.z) < 0.0f)
			{
				continue;
			}
			
			p.position = qfePoint(position.x, position.y, position.z);// * V2P;
			p.seed_id1 = sqrt(u1*u1 + u3*u3); //radial distance
			p.seed_id2 = (atan2(u3,u1)+M_PI)/(2.0f*M_PI);
			p.deviation = 0.0f;

			p.initial_voxel = Vec3i((int)position.x, (int)position.y, (int)position.z);

			p.deletion_reason = -1;

			fixedSeedPositions.push_back(p);
		}

		this->fixedPositionsComputed = true;
	}
			
	unsigned int seedCount = 0;
	
	unsigned int total_nr_particles = internal_steps*particleMultiplier*(int)(volume_size*scale_voxels_per_cubed_cm);

	while(seedCount < total_nr_particles)
	{	
		float t = timeStepSize*distribution(generator);
				
		if(seedFromFixedPositions)
		{
			seedCount++;

			float scale = distribution(generator);
			//select a random fixed seed particle
			unsigned int random_index = (unsigned int)(scale*(fixedSeedPositions.size()-1));

			p = fixedSeedPositions.at(random_index);
		}
		else
		{
			float u1 = position_distribution(position_generator);
			float u2 = position_distribution(position_generator);
			float u3 = position_distribution(position_generator);

			if(plane)
				u2 = 0.0f;

			qfePoint position = center + u1*lambda1*axis1 + u2*lambda2*axis2 + u3*lambda3*axis3;

			qfePoint vp = position*P2V;

			if(    vp.x < 0.0f || vp.x > particlesSeededFromVoxel.ni-1
				|| vp.y < 0.0f || vp.y > particlesSeededFromVoxel.nj-1
				|| vp.z < 0.0f || vp.z > particlesSeededFromVoxel.nk-1)
				continue;

			if(clip_seed_outside_mesh && mesh_level_set.ni != 0 && mesh_level_set.nj != 0 && mesh_level_set.nk != 0 
				&& mesh_level_set.interpolate_value(vp.x, vp.y, vp.z) < 0.0f)
			{
				continue;
			}

			if(seedFromVoxelCenter)
			{
				position = position*P2V;

				position.x = floor(position.x);
				position.y = floor(position.y);
				position.z = floor(position.z);

				position = position*V2P;
			}

			//if (!seedVolume.IsInFrontOfClippingPlane(position) && seedVolume.IsInVolume(position))
			if(seedVolume.IsInVolume(position))
				seedCount++;
			else
				continue; //generate new point
			
			p.position = position;
			p.seed_id1 = sqrt(u1*u1 + u3*u3); //radial distance
			p.seed_id2 = (atan2(u3,u1)+M_PI)/(2.0f*M_PI);
			p.deviation = 0.0f;

			qfePoint seedPos = position * P2V;
			p.initial_voxel = Vec3i((int)seedPos.x, (int)seedPos.y, (int)seedPos.z);
			this->particlesSeededFromVoxel(p.initial_voxel[0], p.initial_voxel[1], p.initial_voxel[2])++;

			p.deletion_reason = -1;
		}

		switch(advection)
		{
		case Stream: 
			{
				p.age = -t;
				p.seed_time = this->currentTime;
			}
			break;
		case Path: 
			{
				p.age = -t;
				p.seed_time = this->currentTime;
			}
			break;
		case Streak: 
			{
				p.age = -t;
				p.seed_time = this->internalTime+t;
			}
			break;
		default:
			{
				p.age = -t;
				p.seed_time = this->currentTime;
			}
			break;
		}
	
		particles.push_back(p);
	}

	//remove the particles that are outside the mesh (if this option was selected)
	if(clip_outside_mesh && mesh_level_set.ni > 1 && mesh_level_set.nj > 1 && mesh_level_set.nk > 1)
	{
		Array3f *level_set = &mesh_level_set;
		qfeMatrix4f pat2vox = P2V;

		float margin = 0.0f;//-this->clipping_margin;

		particles.erase(std::remove_if(particles.begin(), 
								 particles.end(),
								 [&pat2vox, level_set, &margin](particle p)
				{
					qfePoint voxel = p.position*pat2vox;
					return(level_set->interpolate_value(voxel.x, voxel.y, voxel.z) < margin);
				}),
			particles.end());
	}
	
	total_number_of_particles += particles.size() - count_before_seed;

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSeedParticlesWholeMesh(bool seedFromVoxelCenter, bool seedFromFixedPositions, float seedFromFixedPositionsMultiplier)
{
	if(internalTime-currentTime > stopSeedingTime)
		return qfeSuccess;

	if(mesh_level_set.ni <= 1 && mesh_level_set.nj <= 1 && mesh_level_set.nk <= 1)
		return qfeSuccess;

	if(!seedFromFixedPositions || fabs(fixedPositionsMultiplier - seedFromFixedPositionsMultiplier) > 1e-6f)
	{
		this->fixedPositionsComputed = false;
		fixedPositionsMultiplier = seedFromFixedPositionsMultiplier;
		fixedSeedPositions.clear();
	}

	unsigned int count_before_seed = particles.size();

	unsigned int internal_steps = ceil(4.0*timeStepSize/cfl); //steps executed by the advection algorithm
		
	float volume_size = 0.0f;
	float cell_size = spacing[0]*spacing[1]*spacing[2];

	volume_size = inside_mesh_voxel_count*cell_size;

	// compute constant particles per voxel based on voxel size
	float scale_voxels_per_cubed_cm = (1.0f/6.25f)*(1.0f/(spacing[0]*spacing[1]*spacing[2]));

	particle p;

	qfePoint position;

	std::random_device rd;
	std::mt19937 generator(rd());
	std::uniform_real_distribution<> distribution(0, 1);

	std::random_device rd_position;
	std::mt19937 position_generator(rd_position());
	std::uniform_real_distribution<> position_distribution(-1, 1);
	
	unsigned int nr_particles_to_seed = 10*(unsigned int)(fixedPositionsMultiplier*particleMultiplier*volume_size*scale_voxels_per_cubed_cm);
		
	if(seedFromFixedPositions && !this->fixedPositionsComputed)//generate seed positions
	{
		fixedSeedPositions.clear();
		for(unsigned int i = 0; i<nr_particles_to_seed; i++)
		{
			float u1 = position_distribution(position_generator);
			float u2 = position_distribution(position_generator);
			float u3 = position_distribution(position_generator);
								
			qfePoint vp = qfePoint(u1*mesh_level_set.ni, u2*mesh_level_set.nj, u3*mesh_level_set.nk);

			if(mesh_level_set.interpolate_value(vp.x, vp.y, vp.z) <= 0.0f)
				continue;
						
			qfePoint position = vp*V2P;

			if(    vp.x < 0.0f || vp.x > particlesSeededFromVoxel.ni-1
				|| vp.y < 0.0f || vp.y > particlesSeededFromVoxel.nj-1
				|| vp.z < 0.0f || vp.z > particlesSeededFromVoxel.nk-1)
			{
				continue;
			}
			
			p.position = qfePoint(position.x, position.y, position.z);// * V2P;
			p.seed_id1 = 0.0f; //radial distance
			p.seed_id2 = 1.0f;
			p.deviation = 0.0f;

			p.initial_voxel = Vec3i((int)position.x, (int)position.y, (int)position.z);

			p.deletion_reason = -1;

			fixedSeedPositions.push_back(p);
		}

		this->fixedPositionsComputed = true;
	}
			
	unsigned int seedCount = 0;
	
	unsigned int total_nr_particles = internal_steps*particleMultiplier*(int)(volume_size*scale_voxels_per_cubed_cm);

	while(seedCount < total_nr_particles)
	{	
		float t = timeStepSize*distribution(generator);
				
		if(seedFromFixedPositions)
		{
			seedCount++;

			float scale = distribution(generator);
			//select a random fixed seed particle
			unsigned int random_index = (unsigned int)(scale*(fixedSeedPositions.size()-1));

			p = fixedSeedPositions.at(random_index);
		}
		else
		{
			float u1 = position_distribution(position_generator);
			float u2 = position_distribution(position_generator);
			float u3 = position_distribution(position_generator);
			
			qfePoint vp = qfePoint(u1*mesh_level_set.ni, u2*mesh_level_set.nj, u3*mesh_level_set.nk);

			if(mesh_level_set.interpolate_value(vp.x, vp.y, vp.z) <= 0.0f)
				continue;
						
			qfePoint position = vp*V2P;

			if(    vp.x < 0.0f || vp.x > particlesSeededFromVoxel.ni-1
				|| vp.y < 0.0f || vp.y > particlesSeededFromVoxel.nj-1
				|| vp.z < 0.0f || vp.z > particlesSeededFromVoxel.nk-1)
				continue;

			if(clip_seed_outside_mesh && mesh_level_set.ni != 0 && mesh_level_set.nj != 0 && mesh_level_set.nk != 0 
				&& mesh_level_set.interpolate_value(vp.x, vp.y, vp.z) < 0.0f)
			{
				continue;
			}

			if(seedFromVoxelCenter)
			{
				position = position*P2V;

				position.x = floor(position.x);
				position.y = floor(position.y);
				position.z = floor(position.z);

				position = position*V2P;
			}

			//if (!seedVolume.IsInFrontOfClippingPlane(position) && seedVolume.IsInVolume(position))
			if(seedVolume.IsInVolume(position))
				seedCount++;
			else
				continue; //generate new point
			
			p.position = position;
			p.seed_id1 = sqrt(u1*u1 + u3*u3); //radial distance
			p.seed_id2 = (atan2(u3,u1)+M_PI)/(2.0f*M_PI);
			p.deviation = 0.0f;

			qfePoint seedPos = position * P2V;
			p.initial_voxel = Vec3i((int)seedPos.x, (int)seedPos.y, (int)seedPos.z);
			this->particlesSeededFromVoxel(p.initial_voxel[0], p.initial_voxel[1], p.initial_voxel[2])++;

			p.deletion_reason = -1;
		}

		switch(advection)
		{
		case Stream: 
			{
				p.age = -t;
				p.seed_time = this->currentTime;
			}
			break;
		case Path: 
			{
				p.age = -t;
				p.seed_time = this->currentTime;
			}
			break;
		case Streak: 
			{
				p.age = -t;
				p.seed_time = this->internalTime+t;
			}
			break;
		default:
			{
				p.age = -t;
				p.seed_time = this->currentTime;
			}
			break;
		}
	
		particles.push_back(p);
	}

	//remove the particles that are outside the mesh (if this option was selected)
	if(clip_outside_mesh && mesh_level_set.ni > 1 && mesh_level_set.nj > 1 && mesh_level_set.nk > 1)
	{
		Array3f *level_set = &mesh_level_set;
		qfeMatrix4f pat2vox = P2V;

		float margin = 0.0f;//-this->clipping_margin;

		particles.erase(std::remove_if(particles.begin(), 
								 particles.end(),
								 [&pat2vox, level_set, &margin](particle p)
				{
					qfePoint voxel = p.position*pat2vox;
					return(level_set->interpolate_value(voxel.x, voxel.y, voxel.z) < margin);
				}),
			particles.end());
	}
	
	total_number_of_particles += particles.size() - count_before_seed;

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeSeedParticles(std::vector<featureSeedPoint> seedPositions)
{
	unsigned int count_before_seed = particles.size();

	unsigned int internal_steps = ceil(4.0*timeStepSize/cfl); //steps executed by the advection algorithm

	particle p;

	unsigned int steps_per_voxel = 1;

	unsigned int total_steps = (unsigned int)(((float)steps_per_voxel) * (float)internal_steps * particleMultiplier);

	qfePoint position;

	std::random_device rd;
	std::mt19937 generator(rd());
	std::uniform_real_distribution<> distribution(0, 1);
	
	for(int line = 0; line<seedPositions.size(); line++)
	{
		position.x = seedPositions.at(line).seedPoint.x;
		position.y = seedPositions.at(line).seedPoint.y;
		position.z = seedPositions.at(line).seedPoint.z;

		float color = seedPositions.at(line).color_id;

		for(unsigned int i = 0; i<=total_steps; i++)
		{		
			float t = timeStepSize*distribution(generator);
			p.position = qfePoint(position.x, position.y, position.z) * V2P;
			p.seed_id1 = color;
			p.seed_id2 = -1.0f; //Mark particle as "feature based seeded" particle
			p.deviation = 0.0f;

			p.initial_voxel = Vec3i((int)position.x, (int)position.y, (int)position.z);
			this->particlesSeededFromVoxel(p.initial_voxel[0], p.initial_voxel[1], p.initial_voxel[2])++;

			p.deletion_reason = -1;

			switch(advection)
			{
			case Stream: 
				{
					p.age = -t;
					p.seed_time = this->currentTime;
				}
				break;
			case Path: 
				{
					p.age = -t;
					p.seed_time = this->currentTime;
				}
				break;
			case Streak: 
				{
					p.age = -t;
					p.seed_time = this->internalTime+t;
				}
				break;
			default:
				{
					p.age = -t;
					p.seed_time = this->currentTime;
				}
				break;
			}
	
			particles.push_back(p);
		}
	}

	//remove the particles that are outside the mesh (if this option was selected)
	if(clip_outside_mesh && mesh_level_set.ni != 0 && mesh_level_set.nj != 0 && mesh_level_set.nk != 0)
	{
		Array3f *level_set = &mesh_level_set;
		qfeMatrix4f pat2vox = P2V;

		float margin = 0.0f;//-this->clipping_margin;

		particles.erase(std::remove_if(particles.begin(), 
								 particles.end(),
								 [&pat2vox, level_set, &margin](particle p)
				{
					qfePoint voxel = p.position*pat2vox;
					return(level_set->interpolate_value(voxel.x, voxel.y, voxel.z) < margin);
				}),
			particles.end());
	}

	total_number_of_particles += particles.size() - count_before_seed;

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeComputeNextTimeStep(bool volumetric, bool seedFromVoxelCenter, bool seedFromFixedPositions, float seedFromFixedPositionsMultiplier, bool plane, bool seedWholeMesh)
{
	if(timeStepSize == 0.0f)
		return qfeSuccess;

	if(featureBasedSeeds.size() == 0)
	{
		if(!seedWholeMesh)
		{
			if(!volumetric)
				qfeSeedParticles(seedPosition, seedFromVoxelCenter, seedFromFixedPositions, seedFromFixedPositionsMultiplier);
			else
				qfeSeedParticles(seedVolumePoints, seedFromVoxelCenter, seedFromFixedPositions, seedFromFixedPositionsMultiplier, plane);
		}
		else
		{
			qfeSeedParticlesWholeMesh(seedFromVoxelCenter, seedFromFixedPositions, seedFromFixedPositionsMultiplier);
		}
	}

	if(featureBasedSeeds.size()>0)
	{
		qfeSeedParticles(featureBasedSeeds);
	}
	
	qfeAdvectParticles(timeStepSize);
	/*
	if(particleSinks->size() == 0) //we don't have to bother about particles not being captured by a sink
		qfeAdvectParticles(timeStepSize); 
	else //TODO MOVE THIS CHECK TO THE GPU
	{
		//we must reduce the timeStepSize to ensure no particles slip through a particle sink...
		float step = timeStepSize;
		int nr_steps = 1;
		while(step>=this->cfl)
		{
			nr_steps++;
			step = timeStepSize/((float) nr_steps);
		}

		for(int i = 0; i<nr_steps; i++)
		{
			qfeAdvectParticles(step); 
		}
	}
	*/

	this->internalTime += timeStepSize;
	
	updateTimeLinePlot();

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeClearParticles()
{	
	guideLines.clear();
	particles.clear();

	featureBasedSeeds.clear();

	total_number_of_particles = 0;
	number_of_particles_removed_age = 0;
	number_of_particles_removed_domain = 0;

	particleSinks->qfeResetCounters();

	this->internalTime = currentTime;

	this->clearReasonParticlesRemoved();

	particlesSeededFromVoxel.set_zero();

	particlesLeavingMeshFromVoxel.set_zero();
	
	updateTimeLinePlot();

	return qfeSuccess;
}

/*
* Does not clear the particle counting and feature based seeds
*/
qfeReturnStatus qfeInkParticles::qfeClearParticlesFeatureBased()
{
	guideLines.clear();
	particles.clear();

	total_number_of_particles = 0;

	this->internalTime = currentTime;

	return qfeSuccess;
}

qfeColorRGB qfeInkParticles::qfeInterpolateColor(float index)
{
	if(inverseColorMap)
		index = 1.0f-index;

	return colors.at((unsigned int) floor((float)this->color_size * index + 0.5f));
	/*
	if(nr_color_bands == 0)
	{
		return colors.at((unsigned int) floor((float)this->color_size * index + 0.5f));
	}

	float fraction = 0.5f;
	if(nr_color_bands > 1)
	{	float id = index;
		if(index >= 1.0f)
			id = 0.99f;//to ensure the banding works for this "extreme" case

		fraction = floor(id*(float)(nr_color_bands));
		fraction /= (float)(nr_color_bands);
		fraction += 0.5/((float)(nr_color_bands));

		if(fraction >= 1.0f)
			fraction = 0.999999999999f;
	}
	
	return colors.at((unsigned int) floor((float)this->color_size * fraction + 0.5f));
	*/
}

qfeColorRGB qfeInkParticles::qfeGetColor(particle *part)
{
	float color_index = 0.0f;

	switch(color_setting)
	{
		case 0: // seeding
		{	
			color_index = part->seed_id1;
		}
		break;
		case 1: // angular seeding
		{	
			color_index = part->seed_id2;
		}
		break;
		case 2: //age
		{
			color_index = part->age/(float)this->particleLifeTime;
		}
		break;
		case 3: //deviation
		{
			color_index = part->deviation/(venc);
		}
		break;
		case 4: //depth
		{
			if(particles.size()>0)
			{
				float farthest = particles.at(0).depth;//farthest;
				float nearest = particles.at(particles.size()-1).depth;//nearest;
				float current = part->depth;
				//color_index = part->depth/(this->max_dist/2.0f);
				color_index = (current-nearest)/(farthest-nearest);
			}
		}
		break;
		case 5: //seeding and deviation
		{
			color_index = part->seed_id1;

			qfeColorRGB col = qfeInterpolateColor(color_index);

			//brightnessIsoS(col,1.0f+part->deviation/venc);

			col.r += part->deviation/venc;
			col.g += part->deviation/venc;
			col.b += part->deviation/venc;

			return col;
		}
		break;
		case 6: //angular seeding and deviation
		{
			color_index = part->seed_id2;

			qfeColorRGB col = qfeInterpolateColor(color_index);

			//brightnessIsoS(col,1.0f+part->deviation/venc);

			col.r += part->deviation/venc;
			col.g += part->deviation/venc;
			col.b += part->deviation/venc;

			return col;
		}
		break;
		case 7: //seeding and age
		{
			color_index = part->seed_id1;

			qfeColorRGB col = qfeInterpolateColor(color_index);

			//brightnessIsoS(col,1.0f+part->deviation/venc);

			col.r += part->age/(float)this->particleLifeTime;
			col.g += part->age/(float)this->particleLifeTime;
			col.b += part->age/(float)this->particleLifeTime;

			return col;
		}
		break;
		case 8: //angular seeding and age
		{
			color_index = part->seed_id2;

			qfeColorRGB col = qfeInterpolateColor(color_index);

			//brightnessIsoS(col,1.0f+part->deviation/venc);

			col.r += part->age/(float)this->particleLifeTime;
			col.g += part->age/(float)this->particleLifeTime;
			col.b += part->age/(float)this->particleLifeTime;

			return col;
		}
		break;
		case 9: //speed
		{
			color_index = part->speed;
		}
		break;
		default:
		{
			color_index = 0.0f;
		}
		break;
	}

	if(color_index>1.0f)
		color_index = 1.0f;
	if(color_index<0.0f)
		color_index = 0.0f;

	return qfeInterpolateColor(color_index);
}

qfeReturnStatus qfeInkParticles::qfeComputeCFL(float scale)
{
	float range_min = 0.0f;
	float range_max = 0.0f;		

	study->qfeGetStudyRangePCAP(range_min, range_max);
	
	float max_vel = max(fabs(range_min),fabs(range_max));
	
	dx = (min(min(spacing[0],spacing[1]),spacing[2]))/10.0f; //mm to cm
	this->cfl = ((dx)/max_vel); //cm / (cm/s) = seconds for a safe time step
	this->cfl /= (study->phaseDuration/1000.0f); //convert to phases

	if(scale > 0.0f && scale<1.0f)
		this->cfl *= scale;

	return qfeSuccess;
}

//advance the particle "dt" phases
qfeReturnStatus qfeInkParticles::qfeAdvectParticles(float dt)
{
	if(particles.size() == 0)
		return qfeSuccess;

	unsigned int number_of_particles = particles.size();

	//prepare the data
	unsigned int line_particles = 0;
	for(unsigned int i = 0; i<this->guideLines.size(); i++)
	{
		line_particles += guideLines.at(i).line.size();
	}

	GLuint shader_id = 0;

	shaderComputeParticles->qfeGetShader(0, QFE_COMPUTE_SHADER, shader_id);

	this->qfeBindTextures();

	this->shaderComputeParticles->qfeSetUniform1i("phaseCount",this->study->pcap.size());
	this->shaderComputeParticles->qfeSetUniform3i("volumeDimensions", this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);
	this->shaderComputeParticles->qfeSetUniform1i("particleCount", line_particles + this->particles.size());
	this->shaderComputeParticles->qfeSetUniform1f("stepSize", dt); //step size in phases
	this->shaderComputeParticles->qfeSetUniform1f("phaseDuration", phaseDuration); //in seconds
	this->shaderComputeParticles->qfeSetUniformMatrix4f("patientVoxelMatrix", 1, P2V);
	this->shaderComputeParticles->qfeSetUniform1f("CFL", cfl); //cfl condition in phases
	this->shaderComputeParticles->qfeSetUniform1i("advectionType", advection);
	this->shaderComputeParticles->qfeSetUniform1f("venc", venc);
	this->shaderComputeParticles->qfeSetUniform1i("deleteOutsideMesh", 0);
	if(clip_outside_mesh && this->meshSDFTextureVolumeExists)
		this->shaderComputeParticles->qfeSetUniform1i("deleteOutsideMesh", 1);
	this->shaderComputeParticles->qfeSetUniform1f("clippingMargin", clipping_margin);

	std::vector<gpu_particle> parts(line_particles+number_of_particles); //for storing the position and current phase of the particle

	unsigned int ind = 0;
	for(unsigned int gl = 0; gl<this->guideLines.size(); gl++)
	{
		for(unsigned int l = 0; l <guideLines.at(gl).line.size(); l++)
		{
			particle part = guideLines.at(gl).line.at(l);
			
			//convert to gpu_particle
			gpu_particle gpu_part;
			this->qfeConvertCpuParticleToGpuParticle(part, gpu_part, false);

			parts[ind] = gpu_part;	
			ind++;
		}
	}

	for (int i = 0; i < number_of_particles; i++)
	{		
		particle part = particles.at(i);
		
		gpu_particle gpu_part;
		this->qfeConvertCpuParticleToGpuParticle(part, gpu_part, use_uncertainty);

		//if(part.seed_id2 < -0.5f) //The user can choose whether he wants uncertainty to be included
		//	gpu_part.use_uncertainty = false;
		
		parts[line_particles + i] = gpu_part;
	}

	//glUseProgram:
	this->shaderComputeParticles->qfeEnable(); //only enable if all variables are set!

	//Create the buffer
	GLuint particleBufferID;
	//generate 1 buffer id, stored in the "array" particleOutBufferID
	glGenBuffers(1, &particleBufferID); 	
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, particleBufferID);
	//Set the data of the buffer
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(gpu_particle) * parts.size(), &parts[0], GL_DYNAMIC_DRAW);	
	//bind the buffer base to the shader (using the binding=x)
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, particleBufferID);

	gpu_particle *ptr_store;
	ptr_store = (gpu_particle *) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_WRITE_ONLY);

	//move the data to the GPU:
	for(unsigned int p = 0; p<parts.size(); p++)
	{
		(gpu_particle)ptr_store[p] = parts[p];
	}		
	//unmap the data
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

	//glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	//execute the shader:
	glDispatchCompute((parts.size())/32+1,1,1);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	//retrieve the data:
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, particleBufferID);

	gpu_particle *ptr;
	ptr = (gpu_particle *) glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);

	//update the particle positions:
	ind = 0;
	for(unsigned int gl = 0; gl<this->guideLines.size(); gl++)
	{
		for(unsigned int l = 0; l <guideLines.at(gl).line.size(); l++)
		{
			gpu_particle part = (gpu_particle)ptr[ind];
			this->qfeConvertGpuParticleToCpuParticle(part, guideLines.at(gl).line.at(l));
			ind++;
		}
	}

	for(unsigned int p = 0; p<number_of_particles; p++)
	{
		gpu_particle part = (gpu_particle)ptr[line_particles + p];
		this->qfeConvertGpuParticleToCpuParticle(part, particles.at(p));
	}

	//unmap the data
	glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

	//Clean up:
	this->shaderComputeParticles->qfeDisable(); //glUseProgram(0)
	glDeleteBuffers(1, &particleBufferID);	
	
	this->shaderComputeParticles->qfeDisable();

	for(unsigned int gl = 0; gl<this->guideLines.size(); gl++)
	{
		for(unsigned int l = (unsigned int) guideLines.at(gl).line.size()-1; l>0; l--)
		{
			//guideLines.at(gl).line.at(l).age += dt;
			if(guideLines.at(gl).line.at(l).age >= this->particleLifeTime)
			{
				guideLines.at(gl).line.erase(guideLines.at(gl).line.begin()+l);
				continue;
			}
		}
	}

	float lifeTime = particleLifeTime;

	//remove all particles that are too old in an more efficient way compared to removeing them one by one
	unsigned int before_removal_age = particles.size();
	particles.erase(std::remove_if(particles.begin(), 
							 particles.end(),
							  [&lifeTime](particle p){return p.age >= lifeTime;}),
							  particles.end());
	number_of_particles_removed_age += before_removal_age - particles.size();

	qfeMatrix4f pat2vox = P2V;
	unsigned int vol_size_x = volumeSize[0];
	unsigned int vol_size_y = volumeSize[1];
	unsigned int vol_size_z = volumeSize[2];
	//remove all particles that left the domain
	unsigned int before_removal_domain = particles.size();
	particles.erase(std::remove_if(particles.begin(), 
							 particles.end(),
							  [&pat2vox, &vol_size_x, &vol_size_y, &vol_size_z](particle p)
							  {
								  qfePoint voxel = p.position*pat2vox;
								  return (voxel.x < 0 || voxel.x >= vol_size_x || voxel.y < 0 || voxel.y >= vol_size_y || voxel.z < 0 || voxel.z >= vol_size_z);
							  }),
							  particles.end());

	//remove all particles that are outside the mask
							  /*
	Array3f *level_set = &mesh_level_set;

	if(clip_outside_mesh && mesh_level_set.ni != 0 && mesh_level_set.nj != 0 && mesh_level_set.nk != 0)
	{
		particles.erase(std::remove_if(particles.begin(), 
								 particles.end(),
								 [&pat2vox, level_set](particle p)
				{
					qfePoint voxel = p.position*pat2vox;
					return(level_set->interpolate_value(voxel.x, voxel.y, voxel.z)<0.0f);
				}),
			particles.end());
	}
	

	number_of_particles_removed_domain += before_removal_domain - particles.size();

	//TODO removal should be done on the gpu, a particle attribute could indicate by which sink it was removed
	for(unsigned int i = 0; i < particleSinks->size(); i++)
	{
		unsigned int remove_count = particles.size();
		
		qfeSeedVolume *volume = particleSinks->at(i)->volume;
		
		particles.erase(std::remove_if(particles.begin(), 
								 particles.end(),
								 [volume](particle p)
				{
					return(volume->IsInVolume(p.position));
				}),
			particles.end());
		
		this->particleSinks->at(i)->counter += remove_count - particles.size();
	}
	*/

	for(unsigned int p = 0; p<particles.size(); p++)
	{
		if(this->particles.at(p).seed_id2 < -0.5f) //do not count the feature based seeded particles
			continue;

		this->countReasonParticlesRemoved(this->particles.at(p).deletion_reason, this->particles.at(p).initial_voxel);

		if(this->particles.at(p).deletion_reason == 0)//particle removed for leaving the domain/mesh
		{
			qfePoint p_pos = this->particles.at(p).position*P2V;

			particlesLeavingMeshFromVoxel.at((int)p_pos.x, (int)p_pos.y, (int)p_pos.z) ++;
		}
	}

	 //delete particle because it left the mesh (deletion reason = 0) or if it is not a feature-based seeded particle delete for being counted
	particles.erase(std::remove_if(particles.begin(), 
								 particles.end(),
								 [](particle p)
				{
					return((p.deletion_reason>=0 && p.seed_id2 > -0.5f) || (p.deletion_reason == 0));
				}),
			particles.end());

	std::cout<<particles.size()<<std::endl;

	return qfeSuccess;
}


qfeReturnStatus qfeInkParticles::qfeSetUpFBO(int vp_width, int vp_height, GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &depth_buffer_id)
{	
	glBindTexture(GL_TEXTURE_2D, 0);                                // unlink all textures

	//create fbo texture
	/*
	glGenTextures(1, &fbo_texture_id);
	glBindTexture(GL_TEXTURE_2D, fbo_texture_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 
					vp_width, vp_height, 
					0, GL_RGBA, GL_UNSIGNED_BYTE,
					NULL); 

	glBindTexture(GL_TEXTURE_2D, 0);*/
	createTexture(&fbo_texture_id,GL_TEXTURE_MAG_FILTER, GL_TEXTURE_MIN_FILTER, GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_T,
		GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE,vp_width,vp_height,0);
 
	// create a depth texture
	/*
	glGenTextures(1, &depth_buffer_id);
	glBindTexture(GL_TEXTURE_2D, depth_buffer_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 
					vp_width, vp_height, 
					0, GL_DEPTH_COMPONENT, GL_FLOAT,
					NULL); 

	glBindTexture(GL_TEXTURE_2D, 0);
	*/
	createTexture(&depth_buffer_id, GL_TEXTURE_MAG_FILTER, GL_TEXTURE_MIN_FILTER, GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_T,
	GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE,GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT, GL_FLOAT,vp_width,vp_height,0);

	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	fbo_id = 0;
	glGenFramebuffers(1, &fbo_id);	
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);
	glViewport(0,0,vp_width,vp_height);

	// attach color and depth
	//glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, fbo_texture_id, 0);
	//glFramebufferTexture(GL_FRAMEBUFFER,  GL_DEPTH_ATTACHMENT, depth_buffer_id, 0);
	 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fbo_texture_id, 0);
	 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_buffer_id, 0);

	// Always check that our framebuffer is ok
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		if(GL_FRAMEBUFFER_UNDEFINED == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_UNDEFINED"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER"<<std::endl;

		if(GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER"<<std::endl;

		if(GL_FRAMEBUFFER_UNSUPPORTED == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_UNSUPPORTED"<<std::endl;
		
		if(GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE"<<std::endl;
		
		if(GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE"<<std::endl;
		
		if(GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS == glCheckFramebufferStatus(GL_FRAMEBUFFER))
			std::cout<<"GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS"<<std::endl;

		return qfeError;
	}

	// unbind fbo
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeDisableFBO(int vp_width, int vp_height, GLuint base_fbo_id)
{
	// unbind fbo
	glBindFramebuffer(GL_FRAMEBUFFER, base_fbo_id);
	glViewport(0,0,vp_width,vp_height);

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeEnableFBO(int vp_width, int vp_height, GLuint fbo_id)
{
	glViewport (0, 0, vp_width, vp_height);                         // set The Current Viewport to the fbo size
	
	// bind the framebuffer as the output framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);
		
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);            // Clear Screen And Depth Buffer on the fbo

	glEnable(GL_DEPTH_TEST);
	
	// define the index array for the outputs
	GLuint attachments[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1,  attachments);

	glViewport(0,0,vp_width,vp_height);

	return qfeSuccess;
}

qfeReturnStatus qfeInkParticles::qfeDeleteFBO(int vp_width, int vp_height, GLuint fbo_id, GLuint fbo_texture_id, GLuint depth_buffer_id, GLuint base_fbo_id)
{
	glDeleteFramebuffers(1, &fbo_id);

	// unbind fbo
	glBindFramebuffer(GL_FRAMEBUFFER, base_fbo_id);

	//delete textures:
	glDeleteTextures(1, &fbo_texture_id);
	glDeleteTextures(1, &depth_buffer_id);
	
	return qfeSuccess;
}

void qfeInkParticles::updateColorMap()
{
	unsigned int size = (colorMapping.size()/3)-1;
	for(unsigned int i = 0; i<=size; i++)
	{
		qfeColorRGB color = this->qfeInterpolateColor((float)i/size);

		colorMapping.at(i*3+0) = color.r;
		colorMapping.at(i*3+1) = color.g;
		colorMapping.at(i*3+2) = color.b;
	}

	this->createTexture(&this->colorMapTextureId,GL_TEXTURE_MAG_FILTER, GL_TEXTURE_MIN_FILTER, GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_T,
		GL_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_RGB, GL_RGB, GL_FLOAT,size+1,1,&colorMapping[0]);
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeInkParticles::qfeRenderColormapToScreen(int vp_width, int vp_height)
{
	if(this->colorMapTextureId == 0)
		return qfeSuccess;

  glViewport(0,0,vp_width,vp_height);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(-1, 1, -1, 1);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glActiveTexture(GL_TEXTURE0);   
  glBindTexture(GL_TEXTURE_2D, this->colorMapTextureId);  
 
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glEnable(GL_TEXTURE_2D);
  glDisable(GL_DEPTH_TEST);
  
  glBegin(GL_QUADS);
  glTexCoord2f(0, 0); glVertex2f(-0.25, -1);
  glTexCoord2f(0, 1); glVertex2f(-0.25, -0.9);
  glTexCoord2f(1, 1); glVertex2f( 0.25, -0.9);
  glTexCoord2f(1, 0); glVertex2f( 0.25, -1);
  glEnd();

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_TEXTURE_2D); 

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeInkParticles::qfeRenderTextureToScreen(int vp_width, int vp_height, GLuint texture)
{
  glViewport(0,0,vp_width,vp_height);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(-1, 1, -1, 1);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();

  glActiveTexture(GL_TEXTURE0);   
  glBindTexture(GL_TEXTURE_2D, texture);  
 
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glEnable(GL_TEXTURE_2D);
  //glDisable(GL_DEPTH_TEST);
  
  glBegin(GL_QUADS);
  glTexCoord2f(0, 0); glVertex2f(-1, -1);
  glTexCoord2f(0, 1); glVertex2f(-1,  1);
  glTexCoord2f(1, 1); glVertex2f( 1,  1);
  glTexCoord2f(1, 0); glVertex2f( 1, -1);
  glEnd();

  //glEnable(GL_DEPTH_TEST);
  glDisable(GL_TEXTURE_2D); 

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

float qfeInkParticles::GaussianNoise()
{
	return this->GaussianNoise(0,1);
}

float qfeInkParticles::GaussianNoise(float mu, float sigma)
{
	const double epsilon = DBL_MIN;;
	const double two_pi = 2.0*M_PI;

	static double z0, z1;
	static bool generate;
	generate = !generate;

	if (!generate)
		return z1 * sigma + mu;

	double u1, u2;
	do
	{
		u1 = rand() * (1.0 / RAND_MAX);
		u2 = rand() * (1.0 / RAND_MAX);
	}
	while ( u1 <= epsilon );

	z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
	z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);
	return z0 * sigma + mu;
}

qfeVector qfeInkParticles::interpolateVelocity(qfePoint position, float time)
{
	qfeVector vi1, vi2;

	// Seed in voxel coordinates, velocity in patient coordinates
	unsigned int next = ((int)time + 1) % volumeData->size();

	volumeData->at((int) time)->qfeGetVolumeVectorAtPositionLinear(position, vi1);
	if(time == (int)time)
		vi2 = vi1;
	else
		volumeData->at(next)->qfeGetVolumeVectorAtPositionLinear(position, vi2);

	double amount = time-(int)time;
	qfeVector vi;
	vi[0] = ((1-amount)*vi1[0]+(amount)*vi2[0]);
	vi[1] = ((1-amount)*vi1[1]+(amount)*vi2[1]);
	vi[2] = ((1-amount)*vi1[2]+(amount)*vi2[2]);

	vi = (vi*10)*P2V;//to mm and voxel coordinates

	return vi;
}

qfeVector qfeInkParticles::interpolateVelocityUncertain(qfePoint position, float time, float time_step)
{
	//compute uncertainty parameters:
	unsigned int next = ((int)time + 1) % volumeData->size();	

	double amount = time-(int)time;

	float sigma1 = sigma_data[(int)time];
	float sigma2 = sigma_data[next];
	float sigma = (1-amount)*sigma1+(amount)*sigma2;

	float A1 = this->SNR_data.at((int)time).interpolate_value(position.x, position.y, position.z)*sigma1;
	float A2 = this->SNR_data.at(next).interpolate_value(position.x, position.y, position.z)*sigma2;

	float A = (1-amount)*A1+(amount)*A2;

	/*Covariance:
	venc^2/pi^2 * sigma^2/A^2 * matrix

	with matrix:
	2 1 1
	1 2 1
	1 1 2
	*/

	float variance = 100.0f;
	if(A!=0.0f)
		variance = (2*(sigma*sigma))/(A*A)*((venc*venc)/(M_PI*M_PI));

	qfeVector vi1, vi2;

	// Seed in voxel coordinates, velocity in patient coordinates
	volumeData->at((int) time)->qfeGetVolumeVectorAtPositionLinear(position, vi1);
	if(time == (int)time)
		vi2 = vi1;
	else
		volumeData->at(next)->qfeGetVolumeVectorAtPositionLinear(position, vi2);

	qfeVector vi;

	// Compute measured velocity
	vi[0] = ((1-amount)*vi1[0]+(amount)*vi2[0]);
	vi[1] = ((1-amount)*vi1[1]+(amount)*vi2[1]);
	vi[2] = ((1-amount)*vi1[2]+(amount)*vi2[2]);

	// Apply uncertainty
	vi[0] = GaussianNoise(vi[0], variance);
	vi[1] = GaussianNoise(vi[1], variance);
	vi[2] = GaussianNoise(vi[2], variance);

	// Convert to mm and voxel coordinates
	vi = (vi*10)*P2V;

	return vi;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeInkParticles::qfeBindTextures()
{
	int           phasesCount;

	phasesCount = (int)this->volumeData->size();

	static bool volume_computed = false;

	std::string label1 = "";
	std::string label2 = "";
	flowTextureVolume.qfeGetVolumeLabel(label1);
	SNRTextureVolume.qfeGetVolumeLabel(label2);
	if(label1.empty() || label2.empty())
		volume_computed = false;

	//we create a single texture that contains all velocity fields, aka a "texture atlas"

	if(phasesCount > 0 && !volume_computed) 
	{
		//init volume:		
		qfeGrid *grid;
		this->volumeData->at(0)->qfeGetVolumeGrid(&grid);

		flowTextureVolume.qfeSetVolumeComponentsPerVoxel(3);
		flowTextureVolume.qfeSetVolumeGrid(grid);

		SNRTextureVolume.qfeSetVolumeComponentsPerVoxel(3);
		SNRTextureVolume.qfeSetVolumeGrid(grid);
		
		float *data1 = new float[phasesCount*3*this->volumeSize[0]*volumeSize[1]*volumeSize[2]];

		float *data2 = new float[phasesCount*3*this->volumeSize[0]*volumeSize[1]*volumeSize[2]];

		qfeVector vector(0.0f, 0.0f, 0.0f);

		unsigned int offset = 0;

		for(unsigned int phase = 0; phase<phasesCount; phase++)
		{
			for(unsigned int k = 0; k<volumeSize[2]; k++)
				for(unsigned int j = 0; j<volumeSize[1]; j++)
					for(unsigned int i = 0; i<volumeSize[0]; i++)
					{
						this->volumeData->at(phase)->qfeGetVolumeVectorAtPosition(i,j,k,vector);

						offset = 3*((int)i + (int)j*volumeSize[0] + (int)k*volumeSize[0]*volumeSize[1]);

						offset += phase*3*volumeSize[0]*volumeSize[1]*volumeSize[2];
												
						data1[offset+0] = vector.x;
						data1[offset+1] = vector.y;
						data1[offset+2] = vector.z;

						data2[offset+0] = SNR_data[phase].at(i,j,k) * this->sigma_data[phase]; //convert to signal strength
						data2[offset+1] = this->sigma_data[phase];
						data2[offset+2] = 0.0f;
					}
		}

		flowTextureVolume.qfeSetVolumeData(qfeValueTypeFloat, volumeSize[0], volumeSize[1], volumeSize[2]*phasesCount, data1);
		flowTextureVolume.qfeSetVolumeLabel("FlowTexture");

		SNRTextureVolume.qfeSetVolumeData(qfeValueTypeFloat, volumeSize[0], volumeSize[1], volumeSize[2]*phasesCount, data2);
		SNRTextureVolume.qfeSetVolumeLabel("SNRTexture");

		volume_computed = true;
		
		this->flowTextureVolume.qfeUpdateVolume();
		this->SNRTextureVolume.qfeUpdateVolume();
		
		delete[] data1;
		delete[] data2;
	}

	GLuint tid0 = 0;
	GLuint tid1 = 1;

	this->flowTextureVolume.qfeGetVolumeTextureId(tid0);
	this->SNRTextureVolume.qfeGetVolumeTextureId(tid1);

	this->shaderComputeParticles->qfeSetUniform1i("flowData", 0);
	this->shaderComputeParticles->qfeSetUniform1i("snrData", 1);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D, tid0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_3D, tid1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_3D, 0);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_3D, 0);

	this->shaderComputeParticles->qfeSetUniform1i("meshData", 0);
	this->shaderComputeParticles->qfeSetUniform1i("particleSinkData", 0);

	if(meshSDFTextureVolumeExists && mesh_level_set.ni > 1 && mesh_level_set.nj > 1 && mesh_level_set.nk > 1)
	{
		GLuint tid2 = 2;

		this->meshSDFTextureVolume.qfeGetVolumeTextureId(tid2);

		this->shaderComputeParticles->qfeSetUniform1i("meshData", 2);
		
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_3D, tid2);
	}
	//if(this->particleSinks->size()>0) //we always want to link the texture to make sure it is defined correctly in the shader
	{
		GLuint tid3 = 3;

		this->particleSinks->GetVolumeIndexTextureID(tid3);

		this->shaderComputeParticles->qfeSetUniform1i("particleSinkData", 3);
		
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_3D, tid3);
		
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	}

	return qfeSuccess;
}

  qfeReturnStatus qfeInkParticles::qfeConvertGpuParticleToCpuParticle(gpu_particle gpu_part, particle &cpu_part)
  {
	  //x:
	  cpu_part.position.x = gpu_part.position.x;	//position in patient coords
	  //y:
	  cpu_part.position.y = gpu_part.position.y;	//position in patient coords
	  //z:
	  cpu_part.position.z = gpu_part.position.z;	//position in patient coords
	  //age:
	  cpu_part.age = gpu_part.age;
	  //deviation:
	  cpu_part.deviation = gpu_part.deviation;

	  cpu_part.initial_voxel = Vec3i(gpu_part.initial_voxel_x, gpu_part.initial_voxel_y, gpu_part.initial_voxel_z);

	  cpu_part.deletion_reason = gpu_part.deletion_reason;

	  cpu_part.speed = gpu_part.speed;

	  return qfeSuccess;
  }

  qfeReturnStatus qfeInkParticles::qfeConvertCpuParticleToGpuParticle(particle cpu_part, gpu_particle &gpu_part, bool _use_uncertainty)
  {
	  gpu_part.position.x = cpu_part.position.x;
	  gpu_part.position.y = cpu_part.position.y;
	  gpu_part.position.z = cpu_part.position.z;
	  
	  gpu_part.seedTime = cpu_part.seed_time;	
	  gpu_part.age = cpu_part.age;

	  gpu_part.use_uncertainty = _use_uncertainty;
	  gpu_part.deviation = cpu_part.deviation;
		  
	  gpu_part.initial_voxel_x = cpu_part.initial_voxel[0];
	  gpu_part.initial_voxel_y = cpu_part.initial_voxel[1];
	  gpu_part.initial_voxel_z = cpu_part.initial_voxel[2];

	  gpu_part.deletion_reason = cpu_part.deletion_reason;

	  gpu_part.speed = cpu_part.speed;

	  return qfeSuccess;
  }

qfeReturnStatus qfeInkParticles::qfeRenderCube(qfePoint pos, float size, bool draw_outline, qfeColorRGB color)
{
  glPushMatrix();

  glTranslatef(pos.x, pos.y, pos.z);
  glScalef(size, size, size);

  glBegin(GL_QUADS);
	glColor3f(color.r, color.g, color.b);    
	glVertex3f( 1.0f, 1.0f,-1.0f);
	glVertex3f(-1.0f, 1.0f,-1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f( 1.0f, 1.0f, 1.0f);
	glVertex3f( 1.0f,-1.0f, 1.0f);
	glVertex3f(-1.0f,-1.0f, 1.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);
	glVertex3f( 1.0f,-1.0f,-1.0f); 
	glVertex3f( 1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f,-1.0f, 1.0f);
	glVertex3f( 1.0f,-1.0f, 1.0f);  
	glVertex3f( 1.0f,-1.0f,-1.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);
	glVertex3f(-1.0f, 1.0f,-1.0f);
	glVertex3f( 1.0f, 1.0f,-1.0f); 
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f,-1.0f);
	glVertex3f(-1.0f,-1.0f,-1.0f);
	glVertex3f(-1.0f,-1.0f, 1.0f);   
	glVertex3f( 1.0f, 1.0f,-1.0f);
	glVertex3f( 1.0f, 1.0f, 1.0f);
	glVertex3f( 1.0f,-1.0f, 1.0f);
	glVertex3f( 1.0f,-1.0f,-1.0f);
  glEnd();  

  if(draw_outline)
  {
	  glLineWidth(2.0); 

	  glColor3f(0.0f, 0.0f, 0.0f); 

	  glBegin(GL_LINE_LOOP);	  
		glVertex3f( -1.0f, -1.0f, 1.0f);
		glVertex3f( 1.0f, -1.0f, 1.0f);
		glVertex3f( 1.0f, 1.0f, 1.0f);
		glVertex3f( -1.0f, 1.0f, 1.0f);
	  glEnd();

	  glBegin(GL_LINE_LOOP);	  
		glVertex3f( -1.0f, -1.0f, -1.0f);
		glVertex3f( 1.0f, -1.0f, -1.0f);
		glVertex3f( 1.0f, 1.0f, -1.0f);
		glVertex3f( -1.0f, 1.0f, -1.0f);
	  glEnd();

	  glBegin(GL_LINES);
		glVertex3f( -1.0f, -1.0f, 1.0f);
		glVertex3f( -1.0f, -1.0f, -1.0f);

		glVertex3f( 1.0f, -1.0f, 1.0f);
		glVertex3f( 1.0f, -1.0f, -1.0f);

		glVertex3f( 1.0f, 1.0f, 1.0f);
		glVertex3f( 1.0f, 1.0f, -1.0f);

		glVertex3f( -1.0f, 1.0f, 1.0f);
		glVertex3f( -1.0f, 1.0f, -1.0f);
	  glEnd();
  }

  glPopMatrix();

  return qfeSuccess;
}

void qfeInkParticles::clearReasonParticlesRemoved()
{
	this->reason_particles_removed.clear();
}

void qfeInkParticles::countReasonParticlesRemoved(int reason, Vec3i initial_voxel)
{
	if(reason<0) //this particle is not to be removed
		return;

	for(unsigned int i = 0; i<this->reason_particles_removed.size(); i++)
	{
		if(this->reason_particles_removed.at(i).first == reason)
		{
			//count the reason of removal
			if(this->reason_particles_removed.at(i).second.inRange(initial_voxel[0], initial_voxel[1], initial_voxel[2]))
			{
				this->reason_particles_removed.at(i).second.at(initial_voxel[0], initial_voxel[1], initial_voxel[2])++;
			}

			if(reason == 0)//removed because outside mesh
			{
				this->number_of_particles_removed_domain++;
			}
			else
			{
				for(unsigned int p = 0; p<particleSinks->size(); p++)
				{
					if(particleSinks->at(p)->unique_id == reason)
					{
						particleSinks->at(p)->counter++;

						continue;
					}
				}
			}
			return;
		}
	}
	//the reason was not found so we add it:
	
	std::pair<int, Array3i> reason_pair;
	reason_pair.first = reason;
	reason_pair.second = Array3i(volumeSize[0], volumeSize[1], volumeSize[2]);
	reason_pair.second.set_zero();
	if(reason_pair.second.inRange(initial_voxel[0], initial_voxel[1], initial_voxel[2]))
		reason_pair.second.at(initial_voxel[0], initial_voxel[1], initial_voxel[2])++;
	this->reason_particles_removed.push_back(reason_pair);
}

void qfeInkParticles::updateTimeLinePlot()
{
	float seedTime = this->currentTime;
	float seedTime2 = this->currentTime;

	QString label1 = "Current Time";
	QString label2 = "Start seeding";

	bool second_needed = false;

	switch(advection)
	{
	case Stream: 
		{
			seedTime = this->currentTime;
			second_needed = false;
		}
		break;
	case Path: 
		{					
			seedTime = this->internalTime;
			seedTime2 = this->currentTime;
			second_needed = true;
		}
		break;
	case Streak: 
		{			
			seedTime = this->internalTime;

			seedTime2 = this->internalTime-this->particleLifeTime;
			if(seedTime2<this->currentTime)
				seedTime2 = currentTime;

			second_needed = true;
		}
		break;
	default:
		seedTime = this->currentTime;
		second_needed = false;
		break;
	}

	while(seedTime>(int)this->volumeData->size()-1)
	{
		seedTime -= (int)this->volumeData->size()-1;
	}

	this->timeLinePlot.drawTime(seedTime*this->phaseDuration*0.001, label1);

	if(second_needed)
	{
		while(seedTime2>(int)this->volumeData->size()-1)
		{
			seedTime2 -= (int)this->volumeData->size()-1;
		}
		this->timeLinePlot.drawTime2(seedTime2*this->phaseDuration*0.001, label2);
	}
}

void qfeInkParticles::showTimeLinePlot()
{
	this->timeLinePlot.showWindow(400, 400);
}

bool qfeInkParticles::IsInFrontOfClippingPlane(const qfePoint &position, qfeVector clipNormal, qfePoint planePoint)
{
  float dot;
  qfeVector::qfeVectorDot(position - planePoint, clipNormal, dot);
  if (dot > 0.f)
	return true;
  return false;
}