#pragma once

#include "qflowexplorer.h"

#include <iostream>
#include <vector>

#include <vtkCellArray.h>
#include <vtkCellData.h>
#include <vtkIdList.h>
#include <vtkEdgeTable.h>
#include <vtkFloatArray.h>
#include <vtkKdTree.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

#include "qfeGLShaderProgram.h"
#include "qfeMatrix4f.h"
#include "qfeTransform.h"
#include "qfeVector.h"
#include "qfeVolume.h"

/**
* \file   qfeSurfaceContours.h
* \author Roy van Pelt
* \class  qfeSurfaceContours
* \brief  Implements object space contours for meshes
* \note   Confidential
*
* Rendering of occluding contours for meshes
*
* \todo Why is the radial curvature negative in drawfaceisoline?
*/
using namespace std;

class qfeSurfaceContours
{
public:
  qfeSurfaceContours();
  qfeSurfaceContours(const qfeSurfaceContours &contours);
  ~qfeSurfaceContours();

  qfeReturnStatus qfeRenderContour(vtkPolyData *mesh, qfeVolume *volume, qfeColorRGBA color, double lineWidth);  
  qfeReturnStatus qfeRenderContourCPU(vtkPolyData *mesh, qfeVolume *volume, qfeColorRGBA color, double lineWidth);  
  qfeReturnStatus qfeRenderContourGPU(vtkPolyData *mesh, qfeVolume *volume, qfeColorRGBA color, double lineWidth);  
  qfeReturnStatus qfeRenderContourHidden(vtkPolyData *mesh, qfeVolume *volume, qfeColorRGBA color, double lineWidth);

  qfeReturnStatus qfeDrawContour(vtkPolyData *mesh, vector<qfeFloat> ndotv, vector<qfeFloat> kr, qfeColorRGBA color);
  qfeReturnStatus qfeDrawMesh(vtkPolyData *mesh, qfeColorRGBA color);

protected:
  qfeReturnStatus qfeDrawFaceIsoLine(vtkPolyData *mesh, int v0, int v1, int v2, const vector<float> &ndotv, const vector<float> &kr, qfeColorRGBA color);
  qfeReturnStatus qfeDrawFaceIsoLine2(vtkPolyData *mesh, int v0, int v1, int v2, const vector<float> &ndotv, const vector<float> &kr, qfeColorRGBA color);

  qfeReturnStatus qfePerformPreprocessing(vtkPolyData *mesh);
  qfeReturnStatus qfeInitDataStructures(vtkPolyData *mesh, int numPoints);
  qfeReturnStatus qfeBuildEdgeMeshForFeatureLines(vtkIdType v1, vtkIdType v2, int currentCellID, int neighboredCellID);
  qfeReturnStatus qfeBuildFeatureLinesOutput();

private :
  void qfeUpdateSurfaceProperties(vtkPolyData *mesh, qfeVolume *volume, vector<qfeFloat> &ndotv, vector<qfeFloat> &kr);
  void qfeEnableSmoothing();
  void qfeDisableSmoothing();

  vtkSmartPointer<vtkKdTree> poPointLocator;

  // Data structures for edge mesh of the feature lines
	vtkPoints    *inputPolyDataPts;
	vtkDataArray *inputPolyDataCellNormals;
	vtkDataArray *inputPolyDataPointNormals;
	vtkSmartPointer<vtkPoints>     edgeMeshFeatureLinesPts;
	vtkSmartPointer<vtkCellArray>  edgeMeshFeatureLinesCells;
	vtkSmartPointer<vtkEdgeTable>  edgeMeshFeatureLinesTable;
	vtkSmartPointer<vtkFloatArray> edgeMeshFeatureLinesCellNormals;
	vtkSmartPointer<vtkFloatArray> edgeMeshFeatureLinesVertexNormals01;
	vtkSmartPointer<vtkFloatArray> edgeMeshFeatureLinesVertexNormals02;
	vtkSmartPointer<vtkFloatArray> edgeMeshFeatureLinesTangents;
	vtkSmartPointer<vtkFloatArray> edgeMeshFeatureLinesNeighbors;
	vtkSmartPointer<vtkFloatArray> edgeMeshFeatureLinesIDs;
  vtkSmartPointer<vtkPolyData>   edgeMeshOutput;
		
	// Common data structures and fields
	int   pointID;
	float offset;

  qfeGLShaderProgram *shaderProgram;
};
