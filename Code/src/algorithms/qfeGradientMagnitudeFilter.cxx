#include "qfeGradientMagnitudeFilter.h"

qfeGradientMagnitudeFilter::qfeGradientMagnitudeFilter()
{
  gradients = nullptr;
}

qfeGradientMagnitudeFilter::~qfeGradientMagnitudeFilter()
{
  if (gradients) {
    for (int i = 0; i < nrFrames; i++)
      delete[] gradients[i];
    delete[] gradients;
  }
}

void qfeGradientMagnitudeFilter::qfeSetData(qfeVolumeSeries *magnitudeData) {
  gradient.qfeSetData(magnitudeData);
  gradients = gradient.qfeCalculateGradients();
  nrFrames = magnitudeData->size();
}

void qfeGradientMagnitudeFilter::qfeApplyFilter(qfeVolumeSeries *magnitudeData, bool inverted) {
  qfeValueType t;
  unsigned w, h, d;
  unsigned short *magnitudes;
  for (int fi = 0; fi < (int)magnitudeData->size(); fi++) {
    (*magnitudeData)[fi]->qfeGetVolumeData(t, w, h, d, (void **)&magnitudes);
    for (int i = 0; i < (int)(w*h*d); i++) {
      float A = gradients[fi][i*3];
      float B = gradients[fi][i*3+1];
      float C = gradients[fi][i*3+2];
      magnitudes[i] = (unsigned short)(sqrtf(A*A + B*B + C*C));
    }
  }
}

