#include "qfeClusterNodeElliptic.h"

double qfeClusterNodeElliptic::A = 0.5;
double qfeClusterNodeElliptic::B = 0.8;
double qfeClusterNodeElliptic::maxDirectionalCost = 2;
double qfeClusterNodeElliptic::maxSpatialDistance = 100;

//----------------------------------------------------------------------------
//  Constructors
//----------------------------------------------------------------------------
qfeClusterNodeElliptic::qfeClusterNodeElliptic(qfeClusterPoint point)
{
  this->size = 1;
  this->meanPosition.qfeSetVectorElements(point.position);
  this->meanFlow.qfeSetVectorElements(point.flow[0],point.flow[1],point.flow[2]);
};

//----------------------------------------------------------------------------
//  Destructor
//----------------------------------------------------------------------------
qfeClusterNodeElliptic::~qfeClusterNodeElliptic()
{
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeClusterNodeElliptic::qfeMergeRepresentative(qfeClusterNode *c2)
{
  qfeClusterNodeElliptic *other = (qfeClusterNodeElliptic*)c2;

  // TODO: Investigate/minimize effect of limited float accuracy with very different cluster sizes
  qfeFloat thisWeight  = (qfeFloat)(1/(1+(double)other->size/this->size));
  qfeFloat otherWeight  = (qfeFloat)(1/(1+(double)this->size/other->size));

  this->meanPosition = thisWeight*this->meanPosition + otherWeight*other->meanPosition;
  this->meanFlow     = thisWeight*this->meanFlow     + otherWeight*other->meanFlow;

  this->size += other->size;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeFloat qfeClusterNodeElliptic::qfeComputeDistanceTo(qfeClusterNode *c2)
{
  double d = qfeClusterNodeElliptic::B;
  double e = 1-d;

  qfeClusterNodeElliptic *other = (qfeClusterNodeElliptic*)c2;

  qfeVector u = qfeVector(this->meanFlow[0],this->meanFlow[1],this->meanFlow[2]);
  qfeVector v = qfeVector(other->meanFlow[0],other->meanFlow[1],other->meanFlow[2]);
  qfeVector b2,b3;

  qfeFloat x,y,z,lu,lv;
  
  qfeVector diff = qfeVector(this->meanPosition[0] - other->meanPosition[0],\
                             this->meanPosition[1] - other->meanPosition[1],\
                             this->meanPosition[2] - other->meanPosition[2]);
  double dDir,dPos = 0;

  qfeVector::qfeVectorLength(u,lu);
  if(lu>0)
  {
    // Use "this" cluster meanFlow as basis
    qfeVector::qfeVectorOrthonormalBasis2(u,b2,b3);

    // Calculate directional component 
    x = (u*v)/lu;
    y = (b2*v);
    z = (b3*v);
    dDir = ((lu-x) + 2*sqrt( (lu-x)*(lu-x) + 3*(y*y+z*z))) / (3*lu);
    if(dDir > qfeClusterNodeElliptic::maxDirectionalCost)
      dDir = qfeClusterNodeElliptic::maxDirectionalCost;

    // Calculate positional component
    x = (u*diff)/lu;
    y = (b2*diff);
    z = (b3*diff);
    dPos = sqrt((x*x)/(d*d) + (y*y + z*z)/(e*e));
  }
  else  // lu == 0 -> singularity
  {
    dDir = qfeClusterNodeElliptic::maxDirectionalCost;
    dPos = sqrt(diff.x*diff.x/0.25f + diff.y*diff.y/0.25f + diff.z*diff.z/0.25f);
  }

  qfeVector::qfeVectorLength(v,lv);
  if(lv>0)
  {
    // Calculate directional component using "other" cluster meanFlow as basis
    qfeVector::qfeVectorOrthonormalBasis2(v,b2,b3);
    x = (v*u)/lv;
    y = (b2*u);
    z = (b3*u);
    qfeFloat dDir2 = ((lv-x) + 2*sqrt( (lv-x)*(lv-x) + 3*(y*y+z*z))) / (3*lv);
    dDir += (dDir2 > qfeClusterNodeElliptic::maxDirectionalCost) ? qfeClusterNodeElliptic::maxDirectionalCost : dDir2;

    // Calculate positional component
    x = (v*diff)/lv;
    y = (b2*diff);
    z = (b3*diff);
    dPos += sqrt((x*x)/(d*d) + (y*y + z*z)/(e*e));
  }
  else // lv == 0 -> singularity
  {
    dDir += qfeClusterNodeElliptic::maxDirectionalCost;
    dPos += sqrt(diff.x*diff.x/0.25f + diff.y*diff.y/0.25f + diff.z*diff.z/0.25f);
  }

  // Check for unrealistic distances (mostly infinity)
  if((dDir<100000)==false)
  {
    cout << "qfeClusterNodeElliptic::qfeComputeDistanceTo - Encountered unexpected directional distance ("\
      << dDir << ")" << endl;
  }

  return (qfeFloat) (qfeClusterNodeElliptic::A * (dPos/qfeClusterNodeElliptic::maxSpatialDistance)\
    + (1-qfeClusterNodeElliptic::A) * dDir);
};