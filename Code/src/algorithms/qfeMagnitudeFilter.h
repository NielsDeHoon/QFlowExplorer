#pragma once

#include "qfeAlgorithm.h"
#include "qfeVolume.h"

/**
* \file   qfeMagnitudeFilter.h
* \author Arjan Broos
* \class  qfeMagnitudeFilter
* \brief  Algorithm that generates a filter for the velocity data based on the magnitude data
* \note   Confidential
*
* For every voxel that has a magnitude value below the threshold:
* set the velocity data for that voxel to 0.
* For now, it assumes magnitude data to be of type unsigned short and
* velocity data to be triples of type float
*
*/
class qfeMagnitudeFilter : public qfeAlgorithm {
public:
  qfeMagnitudeFilter();

  void qfeSetData(qfeVolumeSeries *magnitudeData);
  void qfeSetThreshold(float percentage);
  void qfeUpdate();
  void qfeApplyFilter(qfeVolumeSeries *velocityData, bool inverted);

private:
  qfeVolumeSeries *magnitudeData;

  unsigned size;
  unsigned width, height, depth;
  unsigned nrFrames;

  float rangeMin, rangeMax;
  float threshold; // Percentage based on rangeMin and rangeMax, not an absolute value
};