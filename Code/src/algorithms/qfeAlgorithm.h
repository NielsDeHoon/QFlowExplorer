#pragma once

#include "qflowexplorer.h"

#include <gl/glew.h>

#include <iostream>

#include "qfeMatrix4f.h"
#include "qfePoint.h"

#define BUFFER_OFFSET(i) ((void*)(i))

/**
* \file   qfeAlgorithm.h
* \author Roy van Pelt
* \class  qfeAlgorithm
* \brief  Abstract class for a driver algorithm
* \note   Confidential
*
* Abstract class for driver algorithm.
* A driver processor outsources a part of the rendering work in the driver.
* The rendering work may be GPU based using GLSL
*
*/

using namespace std;

enum qfeAlgorithmVertexAttribs
{
  ATTRIB_VERTEX,
  ATTRIB_NORMAL,
  ATTRIB_TEXCOORD0,
  ATTRIB_TEXCOORD1
};

class qfeAlgorithm
{
public:
  qfeAlgorithm();
  qfeAlgorithm(const qfeAlgorithm &af);
  ~qfeAlgorithm();

  qfeReturnStatus  qfeGetModelViewMatrix();

  GLfloat          matrixModelView[16];
  GLfloat          matrixProjection[16];
  GLfloat          matrixModelViewInverse[16];
  GLfloat          matrixModelViewProjection[16]; 
  GLfloat          matrixModelViewProjectionInverse[16]; 

};
