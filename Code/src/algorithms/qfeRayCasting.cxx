#include "qfeRayCasting.h"

//----------------------------------------------------------------------------
qfeRayCasting::qfeRayCasting()
{
  this->shaderDVR = new qfeGLShaderProgram();
  this->shaderDVR->qfeAddShaderFromFile("/shaders/library/transform.glsl",QFE_FRAGMENT_SHADER);
  this->shaderDVR->qfeAddShaderFromFile("/shaders/library/fetch.glsl",    QFE_FRAGMENT_SHADER);
  this->shaderDVR->qfeAddShaderFromFile("/shaders/library/normalize.glsl",QFE_FRAGMENT_SHADER);
  this->shaderDVR->qfeAddShaderFromFile("/shaders/library/composite.glsl",QFE_FRAGMENT_SHADER);
  this->shaderDVR->qfeAddShaderFromFile("/shaders/library/shading.glsl",  QFE_FRAGMENT_SHADER);
  this->shaderDVR->qfeAddShaderFromFile("/shaders/dvr/dvr-vert.glsl",     QFE_VERTEX_SHADER);  
  this->shaderDVR->qfeAddShaderFromFile("/shaders/dvr/dvr-frag.glsl",     QFE_FRAGMENT_SHADER);
  this->shaderDVR->qfeLink();

  this->firstRender           = true;
  
  this->texRayEnd             = 0;
  this->texRayEndDepth        = 0;
  this->texVolume             = 0;
  this->texColorMap           = 0;
  this->texGradientMap        = 0;
  
  this->texIntersectColor     = 0;
  this->texIntersectDepth     = 0;  
  this->texClippingFrontDepth = 0;
  this->texClippingBackDepth  = 0;

  this->paramScaling[0]       = 1.0f;
  this->paramScaling[1]       = 1.0f;
  this->paramScaling[2]       = 1.0f;
  this->paramStepSize         = 1.0f/250.0f;
  this->paramGradient         = true;
  this->paramShading          = false;
  this->paramQuality          = 4.0;
  this->paramStyle            = 0;
  this->paramMIDAgamma        = -1.0;

  this->paramClipping[0] = this->paramClipping[2] = this->paramClipping[4] = 0.0;
  this->paramClipping[1] = this->paramClipping[3] = this->paramClipping[5] = 1.0;  

  this->light = NULL;
};

//----------------------------------------------------------------------------
qfeRayCasting::qfeRayCasting(const qfeRayCasting &pr)
{
  this->firstRender                  = pr.firstRender;
  this->shaderDVR                    = pr.shaderDVR;

  this->viewportSize[0]              = pr.viewportSize[0];
  this->viewportSize[1]              = pr.viewportSize[1];
  this->superSampling                = pr.superSampling;
  
  this->texRayEnd                    = pr.texRayEnd;
  this->texRayEndDepth               = pr.texRayEndDepth;
  this->texVolume                    = pr.texVolume;
  this->texColorMap                  = pr.texColorMap;
  this->texGradientMap               = pr.texGradientMap;

  this->texIntersectColor            = pr.texIntersectColor;
  this->texIntersectDepth            = pr.texIntersectDepth;  
  this->texClippingFrontDepth        = pr.texClippingFrontDepth;
  this->texClippingBackDepth         = pr.texClippingBackDepth;
  this->planeClippingOblique         = pr.planeClippingOblique;

  this->paramData                    = pr.paramData;
  this->paramScaling[0]              = pr.paramScaling[0];
  this->paramScaling[1]              = pr.paramScaling[1];
  this->paramScaling[2]              = pr.paramScaling[2];
  this->paramStepSize                = pr.paramStepSize;
  this->paramComponent               = pr.paramComponent;
  this->paramRepresentation          = pr.paramRepresentation;
  this->paramDataRange[0]            = pr.paramDataRange[0];
  this->paramDataRange[1]            = pr.paramDataRange[1];
  this->paramMode                    = pr.paramMode;

  this->paramGradient                = pr.paramGradient;
  this->paramShading                 = pr.paramShading;
  this->paramQuality                 = pr.paramQuality;
  this->paramStyle                   = pr.paramStyle;
  this->paramMIDAgamma               = pr.paramMIDAgamma;

  for(int i=0; i<6; i++) 
    this->paramClipping[i] = pr.paramClipping[i];

  this->light                        = pr.light;
  this->V2P                          = pr.V2P;
};

//----------------------------------------------------------------------------
qfeRayCasting::~qfeRayCasting()
{
  delete this->shaderDVR;

  this->qfeDeleteTextures();
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *volume, qfeColorMap *colormap)
{
  if(volume == NULL) return qfeError;

  return qfeRenderRaycasting(colortex, depthtex, volume, volume, colormap);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeColorMap *colormap)
{
  if(boundingVolume == NULL || textureVolume == NULL) return qfeError;

  qfeStudy      *study    = NULL;

  if(firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, boundingVolume);
    
    this->qfeSetStaticUniformLocations();   

    firstRender = false;
  }

  // Obtain color texture id
  textureVolume->qfeGetVolumeTextureId(this->texVolume);

  colormap->qfeGetColorMapTextureId(this->texColorMap);  
  colormap->qfeGetGradientMapTextureId(this->texGradientMap);
  textureVolume->qfeGetVolumeType(this->paramMode);

  // Compute the stepsize and scaling factor
  qfeGrid     *grid;
  unsigned int dims[3];
  qfePoint     extent;
  textureVolume->qfeGetVolumeSize(dims[0], dims[1], dims[2]);
  textureVolume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(extent.x, extent.y, extent.z);

  this->paramStepSize   = min(extent.x, min(extent.y, extent.z));  
  this->paramScaling[0] = dims[0]*extent.x; 
  this->paramScaling[1] = dims[1]*extent.y; 
  this->paramScaling[2] = dims[2]*extent.z; 

  glDisable(GL_LIGHTING);
  glEnable(GL_DEPTH_TEST);

  // Start rendering
  // 1. Generate texture with ray end position
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, this->texRayEnd, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT,   GL_TEXTURE_2D, this->texRayEndDepth, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, this->texRayEndDepth, 0);
  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // First render the bounding box stop positions
  glEnable(GL_CULL_FACE);
  glCullFace(GL_FRONT);

  this->qfeRenderBoundingBox(boundingVolume, textureVolume);

  glDisable(GL_CULL_FACE);

  qfeGLSupport::qfeCheckFrameBuffer();

  // 2. Execute the raycasting (cull the boundingbox to overcome double rendering)
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, colortex, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT,   GL_TEXTURE_2D, depthtex, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depthtex, 0);

  this->qfeBindTextures();
  this->qfeSetDynamicUniformLocations();

  this->shaderDVR->qfeEnable();

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  this->qfeRenderBoundingBox(boundingVolume, textureVolume);

  glDisable(GL_CULL_FACE);

  this->shaderDVR->qfeDisable();
  this->qfeUnbindTextures();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetViewportSize(unsigned int x, unsigned int y, int supersampling)
{
  if(viewportSize[0] != x || viewportSize[1] != y)
  {
    this->viewportSize[0] = x;
    this->viewportSize[1] = y;
  
    this->qfeUnbindTextures();
    this->qfeDeleteTextures();
    this->qfeCreateTextures();
  }

  this->superSampling   = supersampling;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetIntersectionBuffers(GLuint color, GLuint depth)
{
  this->texIntersectColor = color;
  this->texIntersectDepth = depth;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetConvexClippingBuffers(GLuint depthFront, GLuint depthBack)
{  
  this->texClippingFrontDepth = depthFront;
  this->texClippingBackDepth  = depthBack;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetOrthoClippingRanges(qfeRange x, qfeRange y, qfeRange z)
{
  this->paramClipping[0] = x.min;
  this->paramClipping[1] = x.max;
  this->paramClipping[2] = y.min;
  this->paramClipping[3] = y.max;
  this->paramClipping[4] = z.min;
  this->paramClipping[5] = z.max;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetObliqueClippingPlane(qfeFrameSlice *plane)
{
  qfeVector axes[3];
  qfePoint  origin;
  qfeVector planeEquation;
  qfeFloat  d;

  plane->qfeGetFrameAxes(axes[0].x, axes[0].y, axes[0].z, axes[1].x, axes[1].y, axes[1].z, axes[2].x, axes[2].y, axes[2].z);
  plane->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  d = -1.0f*axes[2].x*origin.x - axes[2].y*origin.y - axes[2].z*origin.z;

  planeEquation.qfeSetVectorElements(axes[2].x, axes[2].y, axes[2].z);
  planeEquation.w = d;

  this->planeClippingOblique = planeEquation;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetDataComponent(int component)
{
  this->paramComponent = component;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetDataRepresentation(int type)
{
  this->paramRepresentation = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetDataRange(qfeFloat vl, qfeFloat vu)
{
  this->paramDataRange[0] = vl;
  this->paramDataRange[1] = vu;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetGradientRendering(bool gradient)
{
  this->paramGradient = gradient;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetShading(bool shading)
{
  this->paramShading = shading;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetLightSource(qfeLightSource *light)
{
  this->light = light;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetQuality(float factor)
{
  this->paramQuality = factor;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetStyle(int style)
{
  this->paramStyle = style;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetMIDAgamma(float gamma)
{
  this->paramMIDAgamma = gamma;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeCreateTextures()
{
  if(this->superSampling < 0) this->superSampling = 1;

  glGenTextures(1, &this->texRayEnd);
  glBindTexture(GL_TEXTURE_2D, this->texRayEnd);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA16F,
    this->viewportSize[0]*this->superSampling, this->viewportSize[1]*this->superSampling, 0, GL_BGRA, GL_UNSIGNED_BYTE, NULL);

  glGenTextures(1, &this->texRayEndDepth);
  glBindTexture(GL_TEXTURE_2D, this->texRayEndDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  //glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, 
  //  this->viewportSize[0]*this->superSampling, this->viewportSize[1]*this->superSampling, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);  
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8_EXT, 
    this->viewportSize[0]*this->superSampling, this->viewportSize[1]*this->superSampling, 0, GL_DEPTH_STENCIL_EXT, GL_UNSIGNED_INT_24_8_EXT, 0);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeDeleteTextures()
{  
  if (glIsTexture(this->texRayEnd))       glDeleteTextures(1, (GLuint *)&this->texRayEnd);
  if (glIsTexture(this->texRayEndDepth))  glDeleteTextures(1, (GLuint *)&this->texRayEndDepth);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeBindTextures()
{
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_3D, this->texVolume);

  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_1D, this->texColorMap);

  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_1D, this->texGradientMap);

  glActiveTexture(GL_TEXTURE6);
  glBindTexture(GL_TEXTURE_2D, this->texRayEnd);

  glActiveTexture(GL_TEXTURE7);
  glBindTexture(GL_TEXTURE_2D, this->texRayEndDepth);

  if(glIsTexture(this->texIntersectColor) && glIsTexture(this->texIntersectDepth))
  {
    glActiveTexture(GL_TEXTURE8);
    glBindTexture(GL_TEXTURE_2D, this->texIntersectColor);

    glActiveTexture(GL_TEXTURE9);
    glBindTexture(GL_TEXTURE_2D, this->texIntersectDepth);
  }

  if(glIsTexture(this->texClippingFrontDepth))
  {
    glActiveTexture(GL_TEXTURE10);
    glBindTexture(GL_TEXTURE_2D, this->texClippingFrontDepth);
  }

  if(glIsTexture(this->texClippingBackDepth))
  {
    glActiveTexture(GL_TEXTURE11);
    glBindTexture(GL_TEXTURE_2D, this->texClippingBackDepth);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeRenderBoundingBox(qfeVolume *volume)
{
  qfeMatrix4f   P2W, V2P, T2V, M;
  GLfloat       matrix[16];

  qfeTransform::qfeGetMatrixPatientToWorld(P2W);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

  // OpenGL works with column-major matrices
  // OpenGL works with right-multiplication
  M = T2V*V2P*P2W;
  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();
  glMultMatrixf(&matrix[0]);  // Texture to World   Coordinates
  
  this->qfeGetModelViewMatrix();

  glBegin(GL_QUADS);

  // Front
  glNormal3f(0.0f, 0.0f, -1.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);

  // Back
  glNormal3f(0.0f, 0.0f, 1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f, 1.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f, 1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f,  1.0f, 1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f, 1.0f);

  // Left
  glNormal3f(-1.0f, 0.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f,  1.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);

  // Right
  glNormal3f(1.0f, 0.0f, 0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f(1.0f,  0.0f,  1.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f(1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f(1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f(1.0f,  1.0f,  1.0f);

  // Top
  glNormal3f(0.0f, -1.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f,  1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);

  // Bottom
  glNormal3f(0.0f, 1.0f, 0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f, 1.0f,  1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f, 1.0f,  1.0f);

  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeRenderBoundingBox(qfeVolume *boundingVolume, qfeVolume* textureVolume)
{
  qfePoint      c[4], v[4], min, max;
  qfeMatrix4f   P2W, V2P, T2V, M, T2P, P2T, P2V, V2T, TextureVolumeCoords, BoundingVolumeCoords;
  GLfloat       matrix[16];

  if(boundingVolume == NULL || textureVolume == NULL) return qfeError;

  // apply the modelview matrix
  qfeTransform::qfeGetMatrixPatientToWorld(P2W);
  
  // bounding volume transformations
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, boundingVolume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, boundingVolume);
  T2P = T2V*V2P;

  // texture volume transformations
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, textureVolume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, textureVolume);
  P2T = P2V*V2T;  

  TextureVolumeCoords = T2P*P2T;
  qfeMatrix4f::qfeGetMatrixInverse(TextureVolumeCoords, BoundingVolumeCoords);

  // clip the bounding box with ortho planes
  max.x = std::min(1.0f, this->paramClipping[1]);
  max.y = std::min(1.0f, this->paramClipping[3]);
  max.z = std::min(1.0f, this->paramClipping[5]);

  min.x = std::max(0.0f, this->paramClipping[0]);
  min.y = std::max(0.0f, this->paramClipping[2]);
  min.z = std::max(0.0f, this->paramClipping[4]);

  min.x = std::min(min.x, max.x);
  min.y = std::min(min.y, max.y);
  min.z = std::min(min.z, max.z);

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  M = T2V*V2P*P2W;
  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();
  glMultMatrixf(&matrix[0]);  // Texture to World Coordinates

  this->qfeGetModelViewMatrix();

  glBegin(GL_QUADS);

  // Front
  glNormal3f(0.0f, 0.0f, -1.0f);  
  v[0].qfeSetPointElements(min.x,min.y,min.z);
  v[1].qfeSetPointElements(min.x,max.y,min.z);
  v[2].qfeSetPointElements(max.x,max.y,min.z);
  v[3].qfeSetPointElements(max.x,min.y,min.z);
  for(int i=0; i<4; i++)
  {
    c[i] = v[i] * TextureVolumeCoords;

    this->qfeClampTextureRange(c[i], c[i]);

    v[i] = c[i] * BoundingVolumeCoords;

    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);    
  }
  
  // Back
  glNormal3f(0.0f, 0.0f, 1.0f);
  v[0].qfeSetPointElements(min.x,min.y,max.z);
  v[1].qfeSetPointElements(max.x,min.y,max.z);
  v[2].qfeSetPointElements(max.x,max.y,max.z);
  v[3].qfeSetPointElements(min.x,max.y,max.z);
  for(int i=0; i<4; i++)
  {
    c[i] = v[i] * TextureVolumeCoords;

    this->qfeClampTextureRange(c[i], c[i]);

    v[i] = c[i] * BoundingVolumeCoords;

    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);    
  }  

  // Left
  glNormal3f(-1.0f, 0.0f, 0.0f);
  v[0].qfeSetPointElements(min.x,min.y,min.z);
  v[1].qfeSetPointElements(min.x,min.y,max.z);
  v[2].qfeSetPointElements(min.x,max.y,max.z);
  v[3].qfeSetPointElements(min.x,max.y,min.z);
  for(int i=0; i<4; i++)
  {
    c[i] = v[i] * TextureVolumeCoords;

    this->qfeClampTextureRange(c[i], c[i]);

    v[i] = c[i] * BoundingVolumeCoords;

    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }
  
  // Right
  glNormal3f(1.0f, 0.0f, 0.0f);
  v[0].qfeSetPointElements(max.x,min.y,max.z);
  v[1].qfeSetPointElements(max.x,min.y,min.z);
  v[2].qfeSetPointElements(max.x,max.y,min.z);
  v[3].qfeSetPointElements(max.x,max.y,max.z);
  for(int i=0; i<4; i++)
  {
    c[i] = v[i] * TextureVolumeCoords;

    this->qfeClampTextureRange(c[i], c[i]);

    v[i] = c[i] * BoundingVolumeCoords;

    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }
  
  // Top
  glNormal3f(0.0f, -1.0f, 0.0f);
  v[0].qfeSetPointElements(min.x,min.y,min.z);
  v[1].qfeSetPointElements(max.x,min.y,min.z);
  v[2].qfeSetPointElements(max.x,min.y,max.z);
  v[3].qfeSetPointElements(min.x,min.y,max.z);
  for(int i=0; i<4; i++)
  {
    c[i] = v[i] * TextureVolumeCoords;

    this->qfeClampTextureRange(c[i], c[i]);

    v[i] = c[i] * BoundingVolumeCoords;

    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }
  
  // Bottom
  glNormal3f(0.0f, 1.0f, 0.0f);
  v[0].qfeSetPointElements(max.x,max.y,min.z);
  v[1].qfeSetPointElements(min.x,max.y,min.z);
  v[2].qfeSetPointElements(min.x,max.y,max.z);
  v[3].qfeSetPointElements(max.x,max.y,max.z);
  for(int i=0; i<4; i++)
  {
    c[i] = v[i] * TextureVolumeCoords;

    this->qfeClampTextureRange(c[i], c[i]);

    v[i] = c[i] * BoundingVolumeCoords;

    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }

  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeClampTextureRange(qfePoint pos, qfePoint &newpos)
{
  if(pos.x < 0.5) newpos.x = std::max(pos.x, 0.0f);
  else            newpos.x = std::min(pos.x, 1.0f);
  if(pos.y < 0.5) newpos.y = std::max(pos.y, 0.0f);
  else            newpos.y = std::min(pos.y, 1.0f);
  if(pos.z < 0.5) newpos.z = std::max(pos.z, 0.0f);
  else            newpos.z = std::min(pos.z, 1.0f);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetStaticUniformLocations()
{
  this->shaderDVR->qfeSetUniform1i("transferFunction"  , 4);  
  this->shaderDVR->qfeSetUniform1i("transferGradient"  , 5);  
  this->shaderDVR->qfeSetUniform1i("rayEnd"            , 6);
  this->shaderDVR->qfeSetUniform1i("rayEndDepth"       , 7);
  this->shaderDVR->qfeSetUniform1i("intersectColor"    , 8);
  this->shaderDVR->qfeSetUniform1i("intersectDepth"    , 9);
  this->shaderDVR->qfeSetUniform1i("clippingFrontDepth",10);
  this->shaderDVR->qfeSetUniform1i("clippingBackDepth" ,11);
  this->shaderDVR->qfeSetUniformMatrix4f("voxelPatientMatrix", 1, this->V2P);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeSetDynamicUniformLocations()
{
  int       intersectAvailable = 0;
  int       clippingAvailable = 0;
  qfePoint  lightSpec;
  qfeVector lightDir;

  if(glIsTexture(this->texIntersectColor) && glIsTexture(this->texIntersectDepth))
    intersectAvailable = 1;

  if(glIsTexture(this->texClippingFrontDepth) && glIsTexture(this->texClippingBackDepth))
    clippingAvailable = 1;
    
  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 100.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w); 
    this->light->qfeGetLightSourceDirection(lightDir);
  }

  // Provide parameters to the shader
  this->shaderDVR->qfeSetUniform1i("dvrDataSet1"           , 1);
  this->shaderDVR->qfeSetUniform1i("dvrDataComponent"      , this->paramComponent);
  this->shaderDVR->qfeSetUniform1i("dvrDataRepresentation" , this->paramRepresentation);
  this->shaderDVR->qfeSetUniform2f("dvrDataRange"          , this->paramDataRange[0], this->paramDataRange[1]);
  this->shaderDVR->qfeSetUniform3f("scaling"               , this->paramScaling[0], this->paramScaling[1], this->paramScaling[2]);
  this->shaderDVR->qfeSetUniform1f("stepSize"              , this->paramStepSize);
  this->shaderDVR->qfeSetUniform1f("quality"               , this->paramQuality);  
  this->shaderDVR->qfeSetUniform1i("mode"                  , this->paramMode);
  this->shaderDVR->qfeSetUniform1i("gradient"              , (int)this->paramGradient);
  this->shaderDVR->qfeSetUniform1i("shading"               , (int)this->paramShading);
  this->shaderDVR->qfeSetUniform1f("gamma"                 , (float)this->paramMIDAgamma);
  this->shaderDVR->qfeSetUniform1i("intersectAvailable"    , intersectAvailable);
  this->shaderDVR->qfeSetUniform1i("clippingAvailable"     , clippingAvailable);
  this->shaderDVR->qfeSetUniform4f("light"                 , lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderDVR->qfeSetUniform4f("lightDirection"        , lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderDVR->qfeSetUniform1i("style"                 , this->paramStyle);  
  this->shaderDVR->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCasting::qfeGetModelViewMatrix()
{
  qfeMatrix4f mv, p, mvp, mvinv, mvpinv;

  GLfloat modelview[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f::qfeSetMatrixElements(mv, modelview);

  GLfloat projection[16];
  glGetFloatv(GL_PROJECTION_MATRIX, projection);
  qfeMatrix4f::qfeSetMatrixElements(p, projection);

  mvp    = mv*p;
  qfeMatrix4f::qfeGetMatrixInverse(mvp, mvpinv);
  qfeMatrix4f::qfeGetMatrixInverse(mv, mvinv);

  qfeMatrix4f::qfeGetMatrixElements(mvinv,  this->matrixModelViewInverse);
  qfeMatrix4f::qfeGetMatrixElements(mvp,    this->matrixModelViewProjection);
  qfeMatrix4f::qfeGetMatrixElements(mvpinv, this->matrixModelViewProjectionInverse);

  return qfeSuccess;
}



