#pragma once

#include "qflowexplorer.h"

#include <gl/glew.h>

#include <iostream>
#include <array>
#include <random>

#include "qfeGLShaderProgram.h"
#include "qfeAlgorithm.h"

#include "qfeVisual.h"
#include "qfeVtkEngine.h"

#include "qfeTransform.h"

/**
* \file   qfePostProcessing.h
* \author Niels de Hoon
* \class  qfePostProcessing
* \brief  Class for post processing the final image
*
* Class for post processing the final image
* Filtering such as Gaussian blur can be applied
* The rendering work may be GPU based using GLSL
*
*/

class qfePostProcessing : public qfeAlgorithm
{
public:
  qfePostProcessing();
  ~qfePostProcessing();

  //----------------------------------------------------------------------------
  void qfeApplyPostProcessing(GLuint &_baseframeBuffer, GLuint *frameBufferProps, qfeVtkEngine *engine, qfeVisualPostProcess *_visual);

  GLuint EstimateNormalMap(); //returns normal texture

  void DepthMap();
  void NormalMap();

  void GammaCorrection(float gamma);

  void ToneMapping(double exposure);

  void Contours();

  void Sobel();

  void Crosshatching();

  void FQAA();

  void DepthDarkening(bool blend, float power, int kernel_size);

  void SSAO(bool blend, float power);

  void Sharpen();

  void GaussianBlur(int kernelSize);

  void BilateralFilter();

  void PassePartout();

private:
  qfeGLShaderProgram textureToScreenShader;
  qfeGLShaderProgram depthMapShader;
  qfeGLShaderProgram passePartoutShader;
  qfeGLShaderProgram estimateNormalMapShader;
  qfeGLShaderProgram gammaCorrectionShader;
  qfeGLShaderProgram toneMappingShader;
  qfeGLShaderProgram contourShader;
  qfeGLShaderProgram sobelShader;
  qfeGLShaderProgram fqaaShader;
  qfeGLShaderProgram crosshatchingShader;
  qfeGLShaderProgram depthDarkeningShader;
  qfeGLShaderProgram ssaoShader;
  qfeGLShaderProgram sharpenShader;
  qfeGLShaderProgram gaussianBlurVerticalShader;
  qfeGLShaderProgram gaussianBlurHorizontalShader;
  qfeGLShaderProgram bilateralFilterShader;
  
  GLuint baseframeBuffer;
  GLuint frameBufferTextureID;
  GLuint frameBufferDepthID;
  GLuint frameBufferNormalID;
	
  int vp_width;
  int vp_height;
  double znear, zfar;

  qfeVtkEngine *engine;

  qfeVisualPostProcess *visual;

  qfeReturnStatus qfeSetUpFBO(GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &fbo_depth_id);
  qfeReturnStatus qfeDisableFBO();
  qfeReturnStatus qfeEnableFBO(GLuint &fbo_id);
  qfeReturnStatus qfeDeleteFBO(GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &fbo_depth_);
  qfeReturnStatus qfeDeleteFBOKeepTexture(GLuint &fbo_id, GLuint &fbo_depth_);

  void createTexture(GLuint *textureName, GLenum pname1, GLenum pname2, GLenum pname3, GLenum pname4, GLfloat param1, GLfloat param2, GLfloat param3, GLfloat param4, GLint level, GLenum format, GLenum type, int screenSize_x, int screenSize_y,GLfloat *data)
  {
	  if (*textureName > 0)
	  {
		  glDeleteTextures(1, textureName);
	  }
	  glGenTextures(1, textureName);
	  glBindTexture(GL_TEXTURE_2D, *textureName);
	  glTexParameterf(GL_TEXTURE_2D, pname1, param1);
	  glTexParameterf(GL_TEXTURE_2D, pname2, param2);
	  glTexParameterf(GL_TEXTURE_2D, pname3, param3);
	  glTexParameterf(GL_TEXTURE_2D, pname4, param4);
	  glTexImage2D(GL_TEXTURE_2D, 0, level, screenSize_x, screenSize_y, 0, format, type, data);
	  glBindTexture(GL_TEXTURE_2D, 0);
  }

  
	//----------------------------------------------------------------------------
	void qfeCheckGLSL(unsigned int id, const char* name)
	{
	  int len = 0;
	  int written = 0;
	  char *log;

	  if(glIsProgram(id))
	  {
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &len);
		if(len < 2)
		  return;

		log = (char *) malloc(len*sizeof(char));
		glGetProgramInfoLog(id, len, &written, log);
	  }
	  else {
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &len);
		if(len < 2)
		  return;

		log = (char *) malloc(len*sizeof(char));
		glGetShaderInfoLog(id, len, &written, log);
	  }

	  if(log){
		cout << name << ": " << endl << log << endl;
		//free(log);
	  }
	}
	
	GLfloat lerp(GLfloat a, GLfloat b, GLfloat f)
	{
		return a + f * (b - a);
	}

  qfeReturnStatus qfeRenderTextureToScreen(GLuint texture);
  qfeReturnStatus qfeRenderTextureToFBO(GLuint texture, GLuint &fbo_id, GLuint &fbo_texture_id, GLuint &fbo_depth_id);

  qfeReturnStatus qfeMockRender();
};