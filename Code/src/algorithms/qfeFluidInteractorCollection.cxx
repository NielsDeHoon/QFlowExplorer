#include "qfeFluidInteractorCollection.h"

void qfeFluidInteractorCollection::init(Array3f _solid, float dx, qfeMatrix4f _V2P, qfeMatrix4f _V2T , qfeMatrix4f _T2V)
{
	solid = _solid;
	dx = dx;

	V2P = _V2P;
	V2T = _V2T;
	T2V = _T2V;

	//init other data
	covered_cells.resize(_solid.ni,_solid.nj,_solid.nk);
	covered_cells.set_zero();

	this->selectedID = 0;
}

void qfeFluidInteractorCollection::setV2PMatrix(qfeMatrix4f _V2P)
{
	V2P = _V2P;
}

void qfeFluidInteractorCollection::setV2TMatrix(qfeMatrix4f _V2T)
{
	V2T = _V2T;
}

void qfeFluidInteractorCollection::setT2VMatrix(qfeMatrix4f _T2V)
{
	T2V = _T2V;
}

void qfeFluidInteractorCollection::setSelectedInteractorIndex(unsigned int _id)
{
	this->selectedID = _id;
	this->visFluidInteractorSelector = this->qfeGetFluidInteractor(selectedID);
}

unsigned int qfeFluidInteractorCollection::getSelectedInteractorIndex()
{
	return this->selectedID;
}

unsigned int qfeFluidInteractorCollection::getNumberOfInteractors()
{
	return this->visFluidInteractors.size();
}

unsigned int qfeFluidInteractorCollection::size()
{
	return this->visFluidInteractors.size();
}

qfeFluidInteractor qfeFluidInteractorCollection::at(unsigned int i)
{
	return this->visFluidInteractors.at(i);
}

void qfeFluidInteractorCollection::disableCreator()
{
	this->visFluidInteractorSelector = NULL;
}

void qfeFluidInteractorCollection::enableCreator(qfeFluidInteractor *interactor)
{
	this->visFluidInteractorSelector = interactor;
}

bool qfeFluidInteractorCollection::creatorEnabled()
{
	if(this->visFluidInteractorSelector->getID() == 0)
		return true;
	else
		return false;
}

qfeReturnStatus qfeFluidInteractorCollection::add(qfeFluidInteractor interactor)
{	
	static unsigned int id = 1;//id = 0 is used to denote empty cells

	if(!this->qfeIsValidFluidInteractor(&interactor))
		return qfeError;

	Array3b currentCells = this->qfeGetFluidInteractorCells(&interactor);
	
	interactor.setID(id);	
	this->computePolygon(&interactor);

	for(unsigned int k = 0; k<currentCells.nk; k++) for(unsigned int j = 0; j<currentCells.nj; j++) for(unsigned int i = 0; i<currentCells.ni; i++) 
	{
		if(currentCells(i,j,k))
		{
			covered_cells(i,j,k) = id;
		}
	}

	id++;
	interactor.setV2PMatrix(V2P);
	interactor.setV2TMatrix(V2T);
	interactor.setT2VMatrix(T2V);
	interactor.setSolid(&solid);
	
	this->visFluidInteractors.push_back(interactor);
	
	return qfeSuccess;
}

qfeReturnStatus qfeFluidInteractorCollection::remove(unsigned int index)
{
	for(unsigned int k = 0; k<covered_cells.nk; k++) for(unsigned int j = 0; j<covered_cells.nj; j++) for(unsigned int i = 0; i<covered_cells.ni; i++)
	{
		if(covered_cells(i,j,k) == index)
			covered_cells(i,j,k) = 0;
	}

	for(unsigned int p = 0; p<visFluidInteractors.size(); p++)
	{
		if(visFluidInteractors.at(p).getID() == index)
		{
			visFluidInteractors.erase(visFluidInteractors.begin() + p);
			break;
		}
	}
	return qfeSuccess;
}

qfeReturnStatus qfeFluidInteractorCollection::removeSelected()
{	
	return this->remove(selectedID);
}

qfeReturnStatus qfeFluidInteractorCollection::selectLastCreated()
{
	if(!this->visFluidInteractors.empty())
	{
		this->selectedID = this->visFluidInteractors.back().getID();
		this->visFluidInteractorSelector = qfeGetFluidInteractor(selectedID);
	}
	return qfeSuccess;
}

qfeFluidInteractor *qfeFluidInteractorCollection::qfeGetFluidInteractor(unsigned int _id)
{
	for(unsigned int p = 0; p<visFluidInteractors.size(); p++)
	{
		if(this->visFluidInteractors.at(p).getID() == _id)
		{
			return &this->visFluidInteractors.at(p);
		}
	}

	return NULL;
}

qfeFluidInteractor *qfeFluidInteractorCollection::qfeGetFluidInteractorSelector()
{
	if(this->visFluidInteractorSelector == NULL && selectedID != 0)
	{
		this->visFluidInteractorSelector = this->qfeGetFluidInteractor(selectedID);
	}
	return this->visFluidInteractorSelector;
}

void qfeFluidInteractorCollection::renderAll()
{
  //render temporary fluid interactor if available
  if(this->visFluidInteractorSelector != NULL)
  { 
	this->computePolygon(this->visFluidInteractorSelector);

	  if(!qfeIsValidFluidInteractor(this->visFluidInteractorSelector))
	  {
		  this->visFluidInteractorSelector->setType(qfeFluidInteractor::INCORRECT);
	  }
	  this->visFluidInteractorSelector->setV2PMatrix(V2P);
	  this->visFluidInteractorSelector->setV2TMatrix(V2T);
	  this->visFluidInteractorSelector->setT2VMatrix(T2V);
	  this->visFluidInteractorSelector->setSolid(&this->solid);
	  this->visFluidInteractorSelector->renderPolygon(false);
  }
  
  //render temporary fluid interactor if available
  bool isSelected = false;
  for(unsigned int i = 0; i<this->visFluidInteractors.size(); i++)
  {	  
	  isSelected = this->visFluidInteractors[i].getID() == selectedID;
	  this->visFluidInteractors[i].setSolid(&this->solid);
	  this->visFluidInteractors[i].renderPolygon(isSelected);
  }
}

Array3b qfeFluidInteractorCollection::qfeGetFluidInteractorCells(qfeFluidInteractor *interactor)
{
	Array3b result(covered_cells.ni, covered_cells.nj, covered_cells.nk);

	for(unsigned int k = 0; k<result.nk; k++) for(unsigned int j = 0; j<result.nj; j++) for(unsigned int i = 0; i<result.ni; i++) 
	{
		result(i,j,k) = false;
	}
	
	float rad = (interactor->getRadius()+3.0f*1.0f);//*40.0f; //convert radius to "patient coordinates"

	unsigned int steps = 64;
	float angle = 0.0f;
	qfePoint point;

	for(unsigned int step = 0; step < steps; step++)
	{
		angle = 2.0f*PI*(float)step/steps;
		qfeVector direction = interactor->getAxis1()*cos(angle)+interactor->getAxis2()*sin(angle);
		
		qfePoint texCoord = interactor->getOrigin() * V2T;

		qfePoint simCoord(texCoord.x*covered_cells.ni, texCoord.y*covered_cells.nj, texCoord.z*covered_cells.nk);

		//the direction vector is in the right direction in voxel coordinates

		float step_size = 0.01f;
		unsigned int max_steps = ceil((covered_cells.ni + covered_cells.nj + covered_cells.nk)/step_size);
	
		unsigned int dx = 0;
		bool found = false;

		Vec3f p(simCoord.x, simCoord.y, simCoord.z);
		std::vector<float> vector(3);
		vector[0] = p[0]; vector[1] = p[1]; vector[2] = p[2];
		float value = solid.interpolate_value(vector);

		qfePoint currentPoint = simCoord;

		bool stop_next_step = false;

		while (dx < max_steps && !found)
		{
			qfePoint nextPoint = simCoord + dx*step_size*direction;
			p[0] = nextPoint.x; p[1] = nextPoint.y; p[2] = nextPoint.z;

			if(!solid.inRange(ceil(p[0]), ceil(p[1]), ceil(p[2])) || !solid.inRange(floor(p[0]), floor(p[1]), floor(p[2])))
				break;

			if(!result.inRange(ceil(p[0]), ceil(p[1]), ceil(p[2])) || !result.inRange(floor(p[0]), floor(p[1]), floor(p[2])))
				break;

			std::vector<float> vector(3);
			vector[0] = p[0]; vector[1] = p[1]; vector[2] = p[2];
			float value2 = solid.interpolate_value(vector);

			currentPoint = nextPoint;
			
			if(stop_next_step)
			{				
				found = true;
				break;
			}

			if(value2 <= 0.0f)
			{
				stop_next_step = true;
			}
			
			value = value2;
			currentPoint = nextPoint;
						
			//currentPoint is part of the interactor
			result(floor(p[0]), floor(p[1]), floor(p[2])) = true;

			dx++;
		}
	}

	return result;
}

bool qfeFluidInteractorCollection::qfeIsValidFluidInteractor(qfeFluidInteractor *interactor)
{
	if(interactor == NULL)
		return false;

	Array3b currentCells = this->qfeGetFluidInteractorCells(interactor);
		
	for(unsigned int k = 1; k<this->covered_cells.nk-1; k++) for(unsigned int j = 1; j<this->covered_cells.nj-1; j++) for(unsigned int i = 1; i<this->covered_cells.ni-1; i++) 
	{
		if((this->covered_cells(i,j,k) != 0 && this->covered_cells(i,j,k) != interactor->getID()) &&
			currentCells(i,j,k))//if the current cell is used by the interactor but it is already used
		{
			return false;
		}
	}
	return true;
}


qfeFluidInteractor* qfeFluidInteractorCollection::qfeGetFluidInteractorInCell(unsigned int i, unsigned int j, unsigned int k)
{
	qfeFluidInteractor *interactor = NULL;

	unsigned int id = this->qfeCellContainsFluidInteractor(i,j,k);
	
	if(id > 0)
	{
		for(unsigned int index = 0; index < this->visFluidInteractors.size(); index++)
		{
			if(visFluidInteractors.at(index).getID() == id)
			{
				interactor = &visFluidInteractors.at(index);
			}
		}
	}

	return interactor;
}

int qfeFluidInteractorCollection::qfeCellContainsFluidInteractor(unsigned int i, unsigned int j, unsigned int k)
{
	if(!this->covered_cells.inRange(i,j,k)) return 0;

	unsigned int val = this->covered_cells(i,j,k);
	
	return val;
}

void qfeFluidInteractorCollection::computePolygon(qfeFluidInteractor *interactor)
{
	std::vector<qfePoint> polygon;
		
	float rad = (interactor->getRadius()+3.0f*1.0f);//*40.0f; //convert radius to "patient coordinates"

	unsigned int steps = 64;
	float angle = 0.0f;
	qfePoint point;
	for(unsigned int step = 0; step < steps; step++)
	{
		angle = 2.0f*PI*(float)step/steps;
		qfeVector direction = interactor->getAxis1()*cos(angle)+interactor->getAxis2()*sin(angle);
		point = computeSolidCrossing(interactor->getOrigin(), direction);

		point = point;
		polygon.push_back(point);
	}
	
	interactor->setPolygon(polygon);
}

qfePoint qfeFluidInteractorCollection::computeSolidCrossing(qfePoint point, qfeVector direction)
{
	qfePoint texCoord = point * V2T;
	qfePoint simCoord(texCoord.x*covered_cells.ni, texCoord.y*covered_cells.nj, texCoord.z*covered_cells.nk);

	//the direction vector is in the right direction in voxel coordinates

	float step_size = 0.05f;
	unsigned int max_steps = ceil((covered_cells.ni + covered_cells.nj + covered_cells.nk)/step_size);
	
	unsigned int step = 0;
	bool found = false;

	Vec3f p(simCoord.x, simCoord.y, simCoord.z);
	
	std::vector<float> vector(3);
	vector[0] = p[0]; vector[1] = p[1]; vector[2] = p[2];
	float value = solid.interpolate_value(vector);

	qfePoint currentPoint = simCoord;

	while (step < max_steps && !found)
	{
		qfePoint nextPoint = simCoord + step*step_size*direction;
		p[0] = nextPoint.x; p[1] = nextPoint.y; p[2] = nextPoint.z;

		if(!solid.inRange(ceil(p[0]), ceil(p[1]), ceil(p[2])) || !solid.inRange(floor(p[0]), floor(p[1]), floor(p[2])))
			break;

		std::vector<float> vector(3);
		vector[0] = p[0]; vector[1] = p[1]; vector[2] = p[2];
		float value2 = solid.interpolate_value(vector);

		if(value2<= 0.0f)
		{
			currentPoint = nextPoint;
			found = true;
			break;
		}

		value = value2;
		currentPoint = nextPoint;

		step++;
	}

	qfePoint texResult(currentPoint.x/covered_cells.ni, currentPoint.y/covered_cells.nj, currentPoint.z/covered_cells.nk);

	return texResult * T2V;
}

Array3<qfeFluidInteractorCollection::CellType> qfeFluidInteractorCollection::qfeGetCellTypes()
{
	Array3<CellType> cells(covered_cells.ni, covered_cells.nj, covered_cells.nk);

	if(visFluidInteractors.size() == 0)
	{
		std::cout<<"No sources and sinks were set, hence all cells are set to be UNDEF, this may lead to unexpected results though..."<<std::endl;

		for(unsigned int k = 1; k<cells.nk-1; k++) for(unsigned int j = 1; j<cells.nj-1; j++) for(unsigned int i = 1; i<cells.ni-1; i++)
		{
			cells(i,j,k) = UNDEF;
		}

		return cells;
	}

	//initialize array
	for(unsigned int k = 0; k<covered_cells.nk; k++) for(unsigned int j = 0; j<covered_cells.nj; j++) for(unsigned int i = 0; i<covered_cells.ni; i++)
	{		
		cells(i,j,k) = qfeFluidInteractorCollection::UNDEF;

		if(solid(i,j,k) <= 0.0f)
		{
			cells(i,j,k) = qfeFluidInteractorCollection::SOLID;
		}

		if(covered_cells(i,j,k) != 0 && cells(i,j,k) != qfeFluidInteractorCollection::SOLID)
		{
			qfeFluidInteractor::InteractorType type = this->qfeGetFluidInteractor(covered_cells(i,j,k))->getType();
			if(type == qfeFluidInteractor::SOURCE)
			{
				cells(i,j,k) = qfeFluidInteractorCollection::SOURCE;
			}
			else if(type == qfeFluidInteractor::SINK)
			{
				cells(i,j,k) = qfeFluidInteractorCollection::SINK;
			}
		}
	}

	std::vector<Vec3ui> queue;
	Vec3i nx1 = Vec3i(-1,0,0); Vec3i nx2 = Vec3i(1,0,0); 
	Vec3i ny1 = Vec3i(0,-1,0); Vec3i ny2 = Vec3i(0,1,0); 
	Vec3i nz1 = Vec3i(0,0,-1); Vec3i nz2 = Vec3i(0,0,1);

	std::vector<Vec3i> neighbors;
	neighbors.push_back(nx1); neighbors.push_back(nx2);
	neighbors.push_back(ny1); neighbors.push_back(ny2);
	neighbors.push_back(nz1); neighbors.push_back(nz2);

	for(unsigned int fi = 0; fi<visFluidInteractors.size(); fi++)
	{
		qfeFluidInteractor *interactor = &visFluidInteractors.at(fi);
		
		qfeFluidInteractor::InteractorType type = interactor->getType();

		//fill queue
		for(unsigned int k = 1; k<covered_cells.nk-1; k++) for(unsigned int j = 1; j<covered_cells.nj-1; j++) for(unsigned int i = 1; i<covered_cells.ni-1; i++)
		{
			if(covered_cells(i,j,k) == interactor->getID())
			{
				//check neighbors (if undef we have to put them in the queue)
				for(unsigned int neighbor = 0; neighbor<neighbors.size(); neighbor++)
				{
					Vec3i coord = neighbors.at(neighbor);
					unsigned int ci = i+coord[0]; unsigned int cj = j+coord[1]; unsigned int ck = k+coord[2];

					if(!cells.inRange(ci,cj,ck))
						continue;

					if(cells(ci,cj,ck) == qfeFluidInteractorCollection::SOLID)
						continue;
					
					if(cells(ci,cj,ck) == qfeFluidInteractorCollection::SOURCE)
						continue;

					if(cells(ci,cj,ck) == qfeFluidInteractorCollection::SINK)
						continue;

					if(cells(ci,cj,ck) == qfeFluidInteractorCollection::UNDEF)
					{
						if(type == qfeFluidInteractor::SOURCE)
						{
							cells(ci,cj,ck) = qfeFluidInteractorCollection::SOURCE_REACHABLE;
						}
						if(type == qfeFluidInteractor::SINK)
						{
							cells(ci,cj,ck) = qfeFluidInteractorCollection::SINK_REACHABLE;
						}

						//add to queue if needed
						bool in_queue = false;
						for(unsigned int q = 0; q<queue.size(); q++)
						{
							Vec3ui cell = queue.at(q);
							if(cell[0] == ci && cell[1] == cj && cell[2] == ck)
							{
								in_queue = true;
								break; //jump out loop
							}
						}
						if(!in_queue)
						{
							queue.push_back(Vec3ui(ci,cj,ck));
						}
					}

					if(cells(ci,cj,ck) == qfeFluidInteractorCollection::SOURCE_REACHABLE && type == qfeFluidInteractor::SINK)
					{
						cells(ci,cj,ck) = qfeFluidInteractorCollection::VALID;
						queue.push_back(Vec3ui(ci,cj,ck));
					}
					if(cells(ci,cj,ck) == qfeFluidInteractorCollection::SINK_REACHABLE && type == qfeFluidInteractor::SOURCE)
					{
						cells(ci,cj,ck) = qfeFluidInteractorCollection::VALID;
						queue.push_back(Vec3ui(ci,cj,ck));
					}
				}
			}
		}

		//inspect all neighbors for elements in queue till the queue is empty
		while(queue.size() != 0)
		{
			Vec3ui cell = queue.front();

			std::vector<Vec3ui> neighbor_queue = this->inspectNeighbors(cell, &cells, type);

			for(unsigned int nq = 0; nq<neighbor_queue.size(); nq++)
			{
				bool in_queue = false;
				for(unsigned int q = 0; q<queue.size(); q++)
				{
					if(queue.at(q)[0] == neighbor_queue.at(nq)[0] &&
					   queue.at(q)[1] == neighbor_queue.at(nq)[1] &&
					   queue.at(q)[2] == neighbor_queue.at(nq)[2] )
					{
						in_queue = true;
						break; //jump out loop
					}					
				}
				if(!in_queue && cells.inRange(neighbor_queue.at(nq)[0], neighbor_queue.at(nq)[1], neighbor_queue.at(nq)[2]))
				{
					queue.push_back(neighbor_queue.at(nq));
				}
			}

			//remove from queue
			queue.erase(queue.begin());
		}
	}

	return cells;
}

Array3b qfeFluidInteractorCollection::qfeGetValidFluidCells()
{
	Array3<qfeFluidInteractorCollection::CellType> cells = this->qfeGetCellTypes();
	Array3b result(cells.ni, cells.nj, cells.nk);

	for(unsigned int k = 1; k<cells.nk-1; k++) for(unsigned int j = 1; j<cells.nj-1; j++) for(unsigned int i = 1; i<cells.ni-1; i++)
	{
		result(i,j,k) = false;

		if(cells(i,j,k) == qfeFluidInteractorCollection::VALID)
		{
			result(i,j,k) = true;
		}
		else if(cells(i,j,k) == qfeFluidInteractorCollection::SOURCE || cells(i,j,k) == qfeFluidInteractorCollection::SINK)
		{
			//if a neighbor is valid then we can approve this  cell
			if( cells(i-1,j,k) == qfeFluidInteractorCollection::VALID || 
				cells(i+1,j,k) == qfeFluidInteractorCollection::VALID || 
				cells(i,j-1,k) == qfeFluidInteractorCollection::VALID || 
				cells(i,j+1,k) == qfeFluidInteractorCollection::VALID || 
				cells(i,j,k-1) == qfeFluidInteractorCollection::VALID || 
				cells(i,j,k+1) == qfeFluidInteractorCollection::VALID )
			{
				result(i,j,k) = true;
			}
		}
	}

	return result;
}

std::vector<Vec3ui> qfeFluidInteractorCollection::inspectNeighbors(Vec3ui cell, Array3<CellType> *cells, qfeFluidInteractor::InteractorType type)
{
	std::vector<Vec3ui> queue;

	Vec3i nx1 = Vec3i(-1,0,0); Vec3i nx2 = Vec3i(1,0,0); 
	Vec3i ny1 = Vec3i(0,-1,0); Vec3i ny2 = Vec3i(0,1,0); 
	Vec3i nz1 = Vec3i(0,0,-1); Vec3i nz2 = Vec3i(0,0,1);

	std::vector<Vec3i> neighbors;
	neighbors.push_back(nx1); neighbors.push_back(nx2);
	neighbors.push_back(ny1); neighbors.push_back(ny2);
	neighbors.push_back(nz1); neighbors.push_back(nz2);

	bool valid = true;

	for(unsigned int nb = 0; nb<neighbors.size(); nb++)
	{
		unsigned int ci = cell[0] + neighbors.at(nb)[0];
		unsigned int cj = cell[1] + neighbors.at(nb)[1];
		unsigned int ck = cell[2] + neighbors.at(nb)[2];

		if(!(*cells).inRange(ci,cj,ck)) //cannot inspect this neighbor since it is not valid
		{
			continue;
		}

		if( (*cells)(ci,cj,ck) == qfeFluidInteractorCollection::SOURCE || 
			(*cells)(ci,cj,ck) == qfeFluidInteractorCollection::SINK   ||
			(*cells)(ci,cj,ck) == qfeFluidInteractorCollection::SOLID  ||
			(*cells)(ci,cj,ck) == qfeFluidInteractorCollection::VALID)
		{
			continue;
		}
				
		if(type == qfeFluidInteractor::SINK && (*cells)(ci,cj,ck) == qfeFluidInteractorCollection::SOURCE_REACHABLE)
		{
			(*cells)(ci,cj,ck) = qfeFluidInteractorCollection::VALID;
			queue.push_back(Vec3ui(ci,cj,ck));
		}
		if(type == qfeFluidInteractor::SOURCE && (*cells)(ci,cj,ck) == qfeFluidInteractorCollection::SINK_REACHABLE)
		{
			(*cells)(ci,cj,ck) = qfeFluidInteractorCollection::VALID;
			queue.push_back(Vec3ui(ci,cj,ck));
		}

		if((*cells)(ci,cj,ck) == qfeFluidInteractorCollection::UNDEF)
		{
			if(type == qfeFluidInteractor::SOURCE)
			{
				(*cells)(ci,cj,ck) = qfeFluidInteractorCollection::SOURCE_REACHABLE;
				queue.push_back(Vec3ui(ci,cj,ck));
			}
			else if(type == qfeFluidInteractor::SINK)
			{
				(*cells)(ci,cj,ck) = qfeFluidInteractorCollection::SINK_REACHABLE;
				queue.push_back(Vec3ui(ci,cj,ck));
			}
			else
			{
				valid = false;
			}
		}		
	}

	return queue;
}