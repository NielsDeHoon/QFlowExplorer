#include "qfeSimulationLinesComparison.h"

//----------------------------------------------------------------------------
qfeSimulationLinesComparison::qfeSimulationLinesComparison()
{
  this->particlePositions.clear();  
  this->particleAttribs.clear();  

  this->seedsVisible              = true;
  this->linesFirstVisible         = true;
  this->linesSecondVisible        = true;
  this->surfaceVisible            = true;
  this->distanceFilter            = 100;
  this->colorType                 = qfeComparisonColorVelocity;

  this->currentTime               = 0;
  this->currentRenderFirst        = false;

  this->particlesCount            = 0;
  this->particleTransferFunction  = 0;

  this->light                     = NULL;

  this->seriesFirst.clear();
  this->seriesSecond.clear();

  this->tracePhases				  = 1.0f;
  this->traceStepSizeFirst        = 0.1;
  this->traceStepDurationFirst    = 10.0;  

  this->traceStepSizeSecond       = 0.1;
  this->traceStepDurationSecond   = 10.0;  

  this->paramParticleSize         = 2.0; 
  this->paramCurrentPhase         = 0.0;
  this->paramFirstHighlight       = false;

  this->firstRender               = true;

  this->shaderProgramRenderSphere = new qfeGLShaderProgram();
  this->shaderProgramRenderSphere->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderSphere->qfeAddShaderFromFile("/shaders/fsr/fsr-render-particle-sphere-splat-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramRenderSphere->qfeAddShaderFromFile("/shaders/fsr/fsr-render-particle-sphere-splat-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderSphere->qfeAddShaderFromFile("/shaders/fsr/fsr-render-particle-sphere-splat-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderSphere->qfeLink();

  this->shaderProgramRenderTuboid = new qfeGLShaderProgram();
  this->shaderProgramRenderTuboid->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderTuboid->qfeAddShaderFromFile("/shaders/fsr/fsr-render-tube-splat-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramRenderTuboid->qfeAddShaderFromFile("/shaders/fsr/fsr-render-tube-splat-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderTuboid->qfeAddShaderFromFile("/shaders/fsr/fsr-render-tube-splat-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderTuboid->qfeLink();

  this->shaderProgramRenderArrows = new qfeGLShaderProgram();
  this->shaderProgramRenderArrows->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderArrows->qfeAddShaderFromFile("/shaders/fsr/fsr-render-arrows-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramRenderArrows->qfeAddShaderFromFile("/shaders/fsr/fsr-render-arrows-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderArrows->qfeAddShaderFromFile("/shaders/fsr/fsr-render-arrows-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderArrows->qfeLink();

  this->shaderProgramRenderSurface = new qfeGLShaderProgram();
  this->shaderProgramRenderSurface->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderSurface->qfeAddShaderFromFile("/shaders/fsr/fsr-render-surface-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramRenderSurface->qfeAddShaderFromFile("/shaders/fsr/fsr-render-surface-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderSurface->qfeAddShaderFromFile("/shaders/fsr/fsr-render-surface-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderSurface->qfeLink();
};

//----------------------------------------------------------------------------
qfeSimulationLinesComparison::qfeSimulationLinesComparison(const qfeSimulationLinesComparison &fv) : qfeFlowLineTrace(fv)
{
  this->firstRender                  = fv.firstRender;
  this->seedsVisible                 = fv.seedsVisible;
  this->linesFirstVisible            = fv.linesFirstVisible;
  this->linesSecondVisible           = fv.linesSecondVisible;
  this->surfaceVisible               = fv.surfaceVisible;
  this->distanceFilter               = fv.distanceFilter;
  this->colorType                    = fv.colorType;

  this->currentTime                  = fv.currentTime;
  this->currentRenderFirst           = fv.currentRenderFirst;

  this->light                        = fv.light;
  this->particlePositions            = fv.particlePositions;    
  this->particleAttribs              = fv.particleAttribs;
  this->particlePositionsVBO         = fv.particlePositionsVBO;  
  this->particleAttribsVBO           = fv.particleAttribsVBO;

  this->linePositionsFirst           = fv.linePositionsFirst;
  this->lineAttribsFirst             = fv.lineAttribsFirst;
  this->linePositionsSecond          = fv.linePositionsSecond;
  this->lineAttribsSecond            = fv.lineAttribsSecond;

  this->linePositionsFirstVBO        = fv.linePositionsFirstVBO;
  this->linePositionsSecondVBO       = fv.linePositionsSecondVBO;
  this->lineAttribsFirstVBO          = fv.lineAttribsFirstVBO;
  this->lineAttribsSecondVBO         = fv.lineAttribsSecondVBO;

  this->locationPosition             = fv.locationPosition;  
  this->locationPositionSecond       = fv.locationPositionSecond;  

  this->particlesCount               = fv.particlesCount;
  this->particleTransferFunction     = fv.particleTransferFunction;

  this->seriesFirst                  = fv.seriesFirst;
  this->seriesSecond                 = fv.seriesSecond;

  this->paramParticleSize            = fv.paramParticleSize;
  this->paramCurrentPhase            = fv.paramCurrentPhase;
  this->paramFirstHighlight          = fv.paramFirstHighlight;

  this->traceStepSizeFirst           = fv.traceStepSizeFirst;
  this->traceStepDurationFirst       = fv.traceStepDurationFirst;  
  this->traceStepSizeFirst           = fv.traceStepSizeSecond;
  this->traceStepDurationFirst       = fv.traceStepDurationSecond;  

  this->shaderProgramRenderSphere    = fv.shaderProgramRenderSphere;
  this->shaderProgramRenderTuboid    = fv.shaderProgramRenderTuboid;  
  this->shaderProgramRenderArrows    = fv.shaderProgramRenderArrows;
  this->shaderProgramRenderSurface   = fv.shaderProgramRenderSurface;
};

//----------------------------------------------------------------------------
qfeSimulationLinesComparison::~qfeSimulationLinesComparison()
{
  delete this->shaderProgramRenderSphere;
  delete this->shaderProgramRenderTuboid;  
  delete this->shaderProgramRenderArrows;
  delete this->shaderProgramRenderSurface;

  if(glIsTexture(this->particleTransferFunction))  
	glDeleteTextures(1, &this->particleTransferFunction);
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetLightSource(qfeLightSource *light)
{
  this->light = light;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetVolumeSeries(qfeVolumeSeries first, qfeVolumeSeries second)
{
  this->seriesFirst  = first;
  this->seriesSecond = second;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetSeedsVisible(bool seeds)
{
  this->seedsVisible = seeds;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetLinesFirstVisible(bool lines)
{
  this->linesFirstVisible = lines;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetLinesSecondVisible(bool lines)
{
  this->linesSecondVisible = lines;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetSurfaceVisible(bool surface)
{
  this->surfaceVisible = surface;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetLinesFirstHighlight(bool highlight)
{
  this->paramFirstHighlight = highlight;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetDistanceFilter(int percentage)
{
  this->distanceFilter = percentage;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetColorType(qfeComparisonColorType color)
{
  this->colorType = color;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetLinesWidth(qfeFloat width)
{
  this->lineWidth = width; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetLinesLength(qfeFloat _phases)
{
	if(abs(this->tracePhases - _phases)>0.01)
	{
		tracePhases = _phases;

		this->qfeGeneratePathlines();
	}

	return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeAddSeedPositions(vector< vector<float> > seeds, qfeVolume * volume)
{
  this->qfeAddParticlePositions(seeds, volume);   

  this->particlesCount = (int)(this->particlePositions[this->currentTime].size()/4.0f); 

  this->qfeInitBufferStatic(&this->particlePositions[this->currentTime], this->particlePositionsVBO);
  this->qfeInitBufferStatic(&this->particleAttribs[this->currentTime], this->particleAttribsVBO);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeClearSeedPositions()
{
  this->particlePositions.clear();  
  this->particlesCount = 0; 
	
  this->qfeClearBufferStatic(this->particlePositionsVBO);
  this->qfeClearBufferStatic(this->particleAttribsVBO);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeGeneratePathlines()
{
  vector<float> linesFirst, linesSecond;  
  vector<float> attribsFirst, attribsSecond, attribsParticle;  

  cout << "Generating pathlines" << endl;

  if((int)this->seriesFirst.size() <= 0)       return qfeError;
  if((int)this->particlePositions.size() <= 0) return qfeError;

  // First time of generation, prepare some global variables
  if(this->firstGenerate)
  {    
	this->seriesFirst.front()->qfeGetVolumeSize(this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);

	qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, this->seriesFirst.front());
	qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, this->seriesFirst.front());

	this->qfeSetStaticUniformLocations();

	this->firstGenerate = false;
  }

  // Initialize    
  this->seedsCount  = int(this->particlesCount);  
  this->linePositionsFirst.clear();
  this->lineAttribsFirst.clear();
  this->linePositionsSecond.clear();
  this->lineAttribsSecond.clear();

  // Set the all the variables necessary to trace the lines
  this->qfeInitTraceStatic();

  // Generate pathlines for each time step
  for(int t=0; t<(int)this->particlePositions.size(); t++)
  {
	linesFirst.clear();
	linesSecond.clear();
	attribsFirst.clear();
	attribsSecond.clear();
	attribsParticle.clear();

	// Generate the pathlines
	if(this->seriesFirst.size() > 1)
	{      
	  this->qfeGeneratePathlines(this->particlePositions[t], this->seriesFirst, this->traceSteps, this->traceStepSizeFirst, this->traceStepDurationFirst, this->P2V, linesFirst);      
	  this->linePositionsFirst.push_back(linesFirst);      
	}

	if(this->seriesSecond.size() > 1)
	{
	  this->qfeGeneratePathlines(this->particlePositions[t], this->seriesSecond, this->traceSteps, this->traceStepSizeSecond, this->traceStepDurationSecond, this->P2V, linesSecond);
	  this->linePositionsSecond.push_back(linesSecond);          
	}

	// Generate the attributes
	if((seriesFirst.size() > 1) && (seriesSecond.size() > 1))
	{
	  this->qfeGeneratePathlineAttributes(linesFirst, linesSecond, this->seriesFirst, this->seriesSecond, this->P2V, attribsFirst, attribsSecond);
	  this->lineAttribsFirst.push_back(attribsFirst);
	  this->lineAttribsSecond.push_back(attribsSecond);
	}

	// Adapt the particle attributes
	if(attribsFirst.size() > 0)
	{
	  int lineSize = this->traceSamples*4;

	  for(int i=0; i<(int)attribsFirst.size(); i+=lineSize)
	  {
		attribsParticle.push_back(attribsFirst[i+0]); // Velocity
		attribsParticle.push_back(attribsFirst[i+2]); // Mean
		attribsParticle.push_back(attribsFirst[i+3]); // Hausdorff        
	  } 
	  
	  this->particleAttribs[t].clear();
	  this->particleAttribs[t] = attribsParticle;
	}
  }

  // Initialize the VBO buffers to the first time step
  if(this->linePositionsFirst.size() > 0)  this->qfeInitBufferStatic(&this->linePositionsFirst.front(),   this->linePositionsFirstVBO);        
  if(this->linePositionsSecond.size() > 0) this->qfeInitBufferStatic(&this->linePositionsSecond.front(),  this->linePositionsSecondVBO);
  if(this->lineAttribsFirst.size() > 0)    this->qfeInitBufferStatic(&this->lineAttribsFirst.front(),     this->lineAttribsFirstVBO);
  if(this->lineAttribsSecond.size() > 0)   this->qfeInitBufferStatic(&this->lineAttribsSecond.front(),    this->lineAttribsSecondVBO);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeClearPathlines()
{
  this->qfeClearBufferStatic(this->linePositionsFirstVBO);
  this->qfeClearBufferStatic(this->linePositionsSecondVBO);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeRenderPathlineComparison(qfeColorMap *colormap, qfeFloat currentPhase)
{  
  // First time of rendering, prepare some global variables
  if(this->firstRender)
  {
	this->qfeSetStaticUniformLocations();

	this->firstRender = false;
  }

  // Update the current phase 
  this->paramCurrentPhase = currentPhase;

  // Update the particle and line buffers
  if(this->currentTime != (int)floor(currentPhase))
  {
	this->currentTime = (int)floor(currentPhase);

	this->particlesCount = (int)(this->particlePositions[this->currentTime].size()/4.0f); 

	this->qfeInitBufferStatic(&this->particlePositions[this->currentTime], this->particlePositionsVBO);
	this->qfeInitBufferStatic(&this->particleAttribs[this->currentTime], this->particleAttribsVBO);

	this->qfeInitBufferStatic(&this->linePositionsFirst[this->currentTime],   this->linePositionsFirstVBO);        
	this->qfeInitBufferStatic(&this->linePositionsSecond[this->currentTime],  this->linePositionsSecondVBO);
	this->qfeInitBufferStatic(&this->lineAttribsFirst[this->currentTime],     this->lineAttribsFirstVBO);
	this->qfeInitBufferStatic(&this->lineAttribsSecond[this->currentTime],    this->lineAttribsSecondVBO);
  }

  // Get the color transferfunction
  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->particleTransferFunction);

  GLsizei lineCount    = this->seedsCount;
  GLsizei lineSize     = this->traceSamples;

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  // Obtain the modelview information
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

  // Bind the textures
  this->qfeBindTextures();

  glPointSize( 5.0 );
  glColor3f(1.0f, 0.0f, 0.0f);

  this->qfeSetDynamicUniformLocations();
  
  this->currentRenderFirst = true;
  this->qfeSetDynamicUniformLocations();

  if(this->linesFirstVisible && (this->seriesFirst.size() > 1))         
	this->qfeRenderPathlines(this->linePositionsFirstVBO, this->lineAttribsFirstVBO,  lineCount, lineSize);       

  this->currentRenderFirst = false;
  this->qfeSetDynamicUniformLocations();  
 
  if(this->linesSecondVisible && (this->seriesSecond.size() > 1))      
	this->qfeRenderPathlines(this->linePositionsSecondVBO, this->lineAttribsSecondVBO, lineCount, lineSize);

  if(this->seedsVisible)
	this->qfeRenderSeeds(this->particlePositionsVBO, this->particleAttribsVBO, this->particlesCount);


  if(this->surfaceVisible)
	this->qfeRenderPathSurfaces(this->linePositionsFirstVBO, this->linePositionsSecondVBO, this->lineAttribsFirstVBO,  lineCount, lineSize);   

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeGeneratePathlines(std::vector<float> seeds, qfeVolumeSeries series, int traceSteps, float traceStepSize, float traceStepDuration, qfeMatrix4f P2V, vector<float> &lines)
{
  const int     traceSamples  = 15;  
  const int     updateEvent   = 70;

  unsigned int  dimensions[3];
  qfePoint      pos, posFirst, posSecond, posTextureFirst, posTextureSecond, seed;
  qfeFloat      seedTime;  
  qfePoint      vertexPositions[traceSamples];    
 
  int seedCount = (int)(seeds.size() / 4.0f);

  this->seriesFirst.front()->qfeGetVolumeSize(dimensions[0], dimensions[1], dimensions[2]);

  lines.clear();
 
  for(int sc=0 ; sc<seedCount; sc++)
  {
	// Get first seed
	seed     = qfePoint(seeds[4*sc+0],seeds[4*sc+1],seeds[4*sc+2]);
	seedTime = seeds[4*sc+3];

	// Set start position
	pos       = seed;
	pos.w     = seedTime;
	posFirst  = pos;
	posSecond = pos;

	// Set the initial seed position
	vertexPositions[1] = posFirst;

	// Trace forward
	for(int i=2; i<traceSamples; i++)
	{
	  qfePoint previousPosFirst = posFirst;      
	  qfePoint posVoxelFirst;

	  for(int j=0; j<traceSteps; j++)
	  {
		qfePoint posTmpFirst = posFirst;

		qfeIntegrateRungeKutta4(series, posFirst, traceStepSize, traceStepDuration, 1, 100, P2V, posFirst);        
	  }

	   // -- Check conditions
	  posTextureFirst.qfeSetPointElements(posFirst.x, posFirst.y, posFirst.z, 1.0);
	  posTextureFirst    = posTextureFirst * P2V;
	  posTextureFirst.x /= dimensions[0];
	  posTextureFirst.y /= dimensions[1];
	  posTextureFirst.z /= dimensions[2];

	  // check the bounding box condition
	  if((posTextureFirst.x <= 0.0 || posTextureFirst.y <= 0.0 || posTextureFirst.z <= 0.0) ||
		 (posTextureFirst.x >= 1.0 || posTextureFirst.y >= 1.0 || posTextureFirst.z >= 1.0))
		 posFirst.qfeSetPointElements(previousPosFirst.x, previousPosFirst.y, previousPosFirst.z);
 
	  // add to the sample array
	  vertexPositions[i] = posFirst;      
	}

	// Fix adjacency information
	vertexPositions[0]              = vertexPositions[1];    
	vertexPositions[traceSamples-1] = vertexPositions[traceSamples-2]; 
 
	// Store the pathline
	for(int i=0; i<traceSamples; i++)
	{      
	  lines.push_back(vertexPositions[i].x);
	  lines.push_back(vertexPositions[i].y);
	  lines.push_back(vertexPositions[i].z);
	  lines.push_back(vertexPositions[i].w);
	}
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeGeneratePathlineAttributes(std::vector<float> linesFirst, std::vector<float> linesSecond, qfeVolumeSeries seriesFirst, qfeVolumeSeries seriesSecond, qfeMatrix4f P2V, vector<float> &attribsFirst, vector<float> &attribsSecond)
{ 
  qfePoint  p1, p2;
  qfeFloat  t1, t2;
  qfeVector v1, v2;
  qfeFloat  s1, s2;
  qfeFloat  distance, vl, vu, vmax;
  qfeFloat  currentMean, currentHausdorff, localMax, meanMax, hausdorffMax;   
  int       attribsCount;
  int       linesSize;
  vector< qfePoint > line1, line2;
  vector<float> mean, hausdorff, max;  

  if(seriesFirst.size() <= 0) return qfeError;

  linesSize    = this->traceSamples;

  seriesFirst.front()->qfeGetVolumeValueDomain(vl, vu);
  vmax = max(abs(vl),abs(vu));

  line1.clear();
  line2.clear();

  mean.clear();
  hausdorff.clear();
  max.clear();

  if(linesFirst.size() == linesSecond.size())
  {    
	currentMean      = 0.0;
	currentHausdorff = 0.0;

	localMax         = 0.0;    
	meanMax          = 0.0;
	hausdorffMax     = 0.0;
	attribsCount     = linesFirst.size()/4.0;    
	
	for(int i=0; i<(int)linesFirst.size();i+=4)
	{
	  p1.x = linesFirst[i+0];
	  p1.y = linesFirst[i+1];
	  p1.z = linesFirst[i+2];    
	  t1   = linesFirst[i+3];    
	  
	  p2.x = linesSecond[i+0];
	  p2.y = linesSecond[i+1];
	  p2.z = linesSecond[i+2];    
	  t2   = linesSecond[i+3];    

	  line1.push_back(p1);
	  line2.push_back(p2);

	  // Obtain the velocity information at each vertex on the line
	  this->qfeFetchVelocityLinearTemporalLinear(seriesFirst,  p1*P2V, t1, v1);
	  this->qfeFetchVelocityLinearTemporalLinear(seriesSecond, p2*P2V, t2, v2);

	  // Obtain the Euclidean distance
	  qfeVector::qfeVectorLength(p1-p2, distance);   
	  qfeVector::qfeVectorLength(v1,s1);
	  qfeVector::qfeVectorLength(v2,s2);

	  // Fill the attribs structure
	  attribsFirst.push_back(s1 / vmax);    // speed
	  attribsFirst.push_back(distance);     // distance local
	  attribsFirst.push_back(0.0);          // distance mean
	  attribsFirst.push_back(0.0);          // distance hausdorff

	  attribsSecond.push_back(s2 / vmax);
	  attribsSecond.push_back(distance);
	  attribsSecond.push_back(0.0);
	  attribsSecond.push_back(0.0);      

	  // Accumulate the local max
	  if(distance > localMax) 
		localMax = distance;

	  // Accumulate the mean
	  currentMean += (1.0/(float)linesSize) * distance;

	  // Update parameter for new line
	  if((int)(i/4.0) % linesSize == 0)
	  {
		// Compute the Hausdorff distance
		this->qfeComputeHausdorff(line1, line2, currentHausdorff);

		// Set the distances
		mean.push_back(currentMean);
		hausdorff.push_back(currentHausdorff);

		// Accumulate maxima
		if(currentMean > meanMax)
		  meanMax = currentMean;
		if(currentHausdorff > hausdorffMax)
		  hausdorffMax  = currentHausdorff;

		// Reset for new line set
		currentMean = 0.0;
		currentHausdorff = 0.0;

		line1.clear();
		line2.clear();       
	  }
	}  
		
	for(int i=0; i<(int)attribsFirst.size(); i+=4)
	{
	  int index = (int)floor(((float)i/4.0)/(float)linesSize);   

	  // Correct the local distance
	  attribsFirst[i+1]  = attribsFirst[i+1]  / localMax;
	  attribsSecond[i+1] = attribsSecond[i+1] / localMax;

	  // Set the mean distance
	  attribsFirst[i+2]  = mean[index] / meanMax;
	  attribsSecond[i+2] = mean[index] / meanMax;

	  // Set the Hausdorff distance
	  attribsFirst[i+3]  = hausdorff[index] / hausdorffMax;
	  attribsSecond[i+3] = hausdorff[index] / hausdorffMax;
	}
  }
  else
  {
	for(int i=0; i<(int)linesFirst.size();i+=4)
	{
	  p1.x = linesFirst[i+0];
	  p1.y = linesFirst[i+1];
	  p1.z = linesFirst[i+2];    
	  t1   = linesFirst[i+3];    

	  // Obtain the velocity information at each vertex on the line
	  this->qfeFetchVelocityLinearTemporalLinear(seriesFirst, p1 * P2V, t1, v1);      
	  qfeVector::qfeVectorLength(v1,s1);
	  
	  // Fill the attribs structure
	  attribsFirst.push_back(s1 / vmax);    // speed
	  attribsFirst.push_back(0.0);          // -
	  attribsFirst.push_back(0.0);          // -
	  attribsFirst.push_back(0.0);          // -
	}  
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeRenderSeeds(GLuint particles, GLuint attribs, int count)
{ 
  this->shaderProgramRenderSphere->qfeGetAttribLocation("qfe_ParticlePosition", this->locationPosition);  
  this->shaderProgramRenderSphere->qfeGetAttribLocation("qfe_ParticleAttribs",  this->locationAttribs);  

  this->shaderProgramRenderSphere->qfeEnable();

  this->qfeDrawParticles(particles, attribs, count);

  this->shaderProgramRenderSphere->qfeDisable();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeRenderPathlines(GLuint lines, GLuint attribs, int count, int size)
{
  // Render the tuboid
  this->shaderProgramRenderTuboid->qfeGetAttribLocation("qfe_Position",  this->locationPosition);  
  this->shaderProgramRenderTuboid->qfeGetAttribLocation("qfe_Attribs",   this->locationAttribs);  

  this->shaderProgramRenderTuboid->qfeEnable();

  this->qfeDrawLines(lines, attribs, count, size);
  
  this->shaderProgramRenderTuboid->qfeDisable();
  
  // Render end arrows
  this->shaderProgramRenderArrows->qfeGetAttribLocation("qfe_Position", this->locationPosition);
  this->shaderProgramRenderArrows->qfeGetAttribLocation("qfe_Attribs",  this->locationAttribute);

  this->shaderProgramRenderArrows->qfeEnable();

  this->qfeDrawLines(lines, attribs, count, size);

  this->shaderProgramRenderArrows->qfeDisable();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeRenderPathSurfaces(GLuint linesFirst, GLuint linesSecond, GLuint attribs, int count, int size)
{  
  this->shaderProgramRenderSurface->qfeGetAttribLocation("qfe_PositionFirst",   this->locationPosition);  
  this->shaderProgramRenderSurface->qfeGetAttribLocation("qfe_PositionSecond",  this->locationPositionSecond);
  this->shaderProgramRenderSurface->qfeGetAttribLocation("qfe_Attribs",         this->locationAttribs);

  this->shaderProgramRenderSurface->qfeEnable();
  
  this->qfeDrawLines(linesFirst, linesSecond, attribs, count, size);
  
  this->shaderProgramRenderSurface->qfeDisable();
  

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeDrawParticles(GLuint particles, GLuint attribs, int count)
{
  if(!glIsBuffer(particles)) return qfeError;
  if(this->locationPosition == -1) return qfeError;

  glBindBuffer(GL_ARRAY_BUFFER, particles);
  glEnableVertexAttribArray(this->locationPosition);
  glVertexAttribPointer(this->locationPosition, 4, GL_FLOAT, GL_FALSE, 0, 0);

  if(glIsBuffer(attribs) && (this->locationAttribs != -1))
  {
	glBindBuffer(GL_ARRAY_BUFFER, attribs);
	glEnableVertexAttribArray(this->locationAttribs);
	glVertexAttribPointer(this->locationAttribs,  3, GL_FLOAT, GL_FALSE, 0, 0);
  }

  glDrawArrays(GL_POINTS, 0, count);

  glBindBuffer(GL_ARRAY_BUFFER, 0);  
  glDisableVertexAttribArray(this->locationPosition);
  glDisableVertexAttribArray(this->locationAttribs);  

  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeDrawLines(GLuint lines, GLuint attribs, int segmentCount, int segmentSize)
{
  if(!glIsBuffer(lines)) return qfeError;
  
  glBindBuffer(GL_ARRAY_BUFFER, lines);
  glEnableVertexAttribArray(this->locationPosition);
  glVertexAttribPointer(this->locationPosition,  4, GL_FLOAT, GL_FALSE, 0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, attribs);
  glEnableVertexAttribArray(this->locationAttribs);
  glVertexAttribPointer(this->locationAttribs,  4, GL_FLOAT, GL_FALSE, 0, 0);

  for(int i=0; i<segmentCount; i++)
  {
	glDrawArrays(GL_LINE_STRIP_ADJACENCY, i*segmentSize, segmentSize);
  }

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDisableVertexAttribArray(this->locationPosition);
  glDisableVertexAttribArray(this->locationAttribs);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeDrawLines(GLuint linesFirst, GLuint linesSecond, GLuint attribs, int segmentCount, int segmentSize)
{
  if(!glIsBuffer(linesFirst) || !glIsBuffer(linesSecond)) return qfeError;
  
  glBindBuffer(GL_ARRAY_BUFFER, linesFirst);
  glEnableVertexAttribArray(this->locationPosition);
  glVertexAttribPointer(this->locationPosition,  4, GL_FLOAT, GL_FALSE, 0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, linesSecond);
  glEnableVertexAttribArray(this->locationPositionSecond);
  glVertexAttribPointer(this->locationPositionSecond,  4, GL_FLOAT, GL_FALSE, 0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, attribs);
  glEnableVertexAttribArray(this->locationAttribs);
  glVertexAttribPointer(this->locationAttribs,  4, GL_FLOAT, GL_FALSE, 0, 0);

  for(int i=0; i<segmentCount; i++)
  {
	glDrawArrays(GL_LINE_STRIP_ADJACENCY, i*segmentSize, segmentSize);
  }

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDisableVertexAttribArray(this->locationPosition);
  glDisableVertexAttribArray(this->locationPositionSecond);
  glDisableVertexAttribArray(this->locationAttribs);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeComputeHausdorff(vector< qfePoint > line1, vector< qfePoint > line2, float &distance)
{
  float h1 = 0.0;
  float h2 = 0.0;
  float shortest, dij; 
  
  // A > B   
  for(int i=0; i<(int)line1.size(); i++)
  {
	shortest = std::numeric_limits<float>::infinity();

	for(int j=0; j<(int)line2.size(); j++)
	{
	  qfeVector::qfeVectorLength(line1[i]-line2[j], dij);

	  if(dij < shortest) 
		shortest = dij;
	}

	if(shortest > h1)
	  h1 = shortest;    
  }

  // B > A   
  for(int i=0; i<(int)line2.size(); i++)
  {
	shortest = std::numeric_limits<float>::infinity();

	for(int j=0; j<(int)line1.size(); j++)
	{
	  qfeVector::qfeVectorLength(line2[i]-line1[j], dij);

	  if(dij < shortest) 
		shortest = dij;
	}

	if(shortest > h2)
	  h2 = shortest;    
  }

  distance = max(h1, h2);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeFetchVelocityLinear(qfeVolume *vol, qfePoint p, qfeVector &v)
{
  unsigned int  dims[3];
  qfeValueType  type;
  void         *data;
  qfeFloat      xd, yd, zd;
  qfePoint      p1;
  qfeVector     i1, i2, i1l, i1r, i2l, i2r;
  qfeVector     j1, j2, j1l, j1r, j2l, j2r;
  qfeVector     w1, w2;
  int           index[2];

  // Fetch the volume data
  vol->qfeGetVolumeData(type, dims[0], dims[1], dims[2], &data);

  // Make sure the boundaries conditions are ok
  (p.x     > 0.0)            ? p1.x = p.x   : p1.x = 1;
  (p.y     > 0.0)            ? p1.y = p.y   : p1.y = 1;
  (p.z     > 0.0)            ? p1.z = p.z   : p1.z = 1;
  (p.x+1.0 < (float)dims[0]) ? p1.x = p1.x  : p1.x = (int)dims[0]-2;
  (p.y+1.0 < (float)dims[1]) ? p1.y = p1.y  : p1.y = (int)dims[1]-2;
  (p.z+1.0 < (float)dims[2]) ? p1.z = p1.z  : p1.z = (int)dims[2]-2;

  // Get the lattice point
  xd = p1.x - floor(p1.x);
  yd = p1.y - floor(p1.y);
  zd = p1.z - floor(p1.z);

  // Compute along z
  index[0] = floor(p1.x)  + (floor(p1.y) *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = floor(p1.x)  + (floor(p1.y) *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  i1l.x = *((float*)data + 3*index[0]+0);
  i1l.y = *((float*)data + 3*index[0]+1);
  i1l.z = *((float*)data + 3*index[0]+2);
  i1r.x = *((float*)data + 3*index[1]+0);
  i1r.y = *((float*)data + 3*index[1]+1);
  i1r.z = *((float*)data + 3*index[1]+2);
  i1.x = i1l.x*(1.0-zd)  + i1r.x*zd;
  i1.y = i1l.y*(1.0-zd)  + i1r.y*zd;
  i1.z = i1l.z*(1.0-zd)  + i1r.z*zd;

  index[0] = floor(p1.x)  + (ceil(p1.y)  *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = floor(p1.x)  + (ceil(p1.y)  *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  i2l.x = *((float*)data + 3*index[0]+0);
  i2l.y = *((float*)data + 3*index[0]+1);
  i2l.z = *((float*)data + 3*index[0]+2);
  i2r.x = *((float*)data + 3*index[1]+0);
  i2r.y = *((float*)data + 3*index[1]+1);
  i2r.z = *((float*)data + 3*index[1]+2);
  i2.x = i2l.x*(1.0-zd)  + i2r.x*zd;
  i2.y = i2l.y*(1.0-zd)  + i2r.y*zd;
  i2.z = i2l.z*(1.0-zd)  + i2r.z*zd;

  index[0] = ceil(p1.x)   + (floor(p1.y) *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = ceil(p1.x)   + (floor(p1.y) *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  j1l.x = *((float*)data + 3*index[0]+0);
  j1l.y = *((float*)data + 3*index[0]+1);
  j1l.z = *((float*)data + 3*index[0]+2);
  j1r.x = *((float*)data + 3*index[1]+0);
  j1r.y = *((float*)data + 3*index[1]+1);
  j1r.z = *((float*)data + 3*index[1]+2);
  j1.x = j1l.x*(1.0-zd)  + j1r.x*zd;
  j1.y = j1l.y*(1.0-zd)  + j1r.y*zd;
  j1.z = j1l.z*(1.0-zd)  + j1r.z*zd;

  index[0] = ceil(p1.x)   + (ceil(p1.y)  *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = ceil(p1.x)   + (ceil(p1.y)  *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  j2l.x = *((float*)data + 3*index[0]+0);
  j2l.y = *((float*)data + 3*index[0]+1);
  j2l.z = *((float*)data + 3*index[0]+2);
  j2r.x = *((float*)data + 3*index[1]+0);
  j2r.y = *((float*)data + 3*index[1]+1);
  j2r.z = *((float*)data + 3*index[1]+2);
  j2.x = j2l.x*(1.0-zd)  + j2r.x*zd;
  j2.y = j2l.y*(1.0-zd)  + j2r.y*zd;
  j2.z = j2l.z*(1.0-zd)  + j2r.z*zd;

  // compute along y
  w1.x = i1.x*(1.0-yd) + i2.x*yd;
  w1.y = i1.y*(1.0-yd) + i2.y*yd;
  w1.z = i1.z*(1.0-yd) + i2.z*yd;

  w2.x = j1.x*(1.0-yd) + j2.x*yd;
  w2.y = j1.y*(1.0-yd) + j2.y*yd;
  w2.z = j1.z*(1.0-yd) + j2.z*yd;

  // compute along z
  v.x = w1.x*(1.0-xd) + w2.x*xd;
  v.y = w1.y*(1.0-xd) + w2.y*xd;
  v.z = w1.z*(1.0-xd) + w2.z*xd;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// p in voxel coordinates
qfeReturnStatus qfeSimulationLinesComparison::qfeFetchVelocityLinearTemporalLinear(qfeVolumeSeries series, qfePoint p, qfeFloat t, qfeVector &v)
{
  float     ti, time;
  int       tl, tr;
  qfeVector vl, vr;

  tl = floor(t);
  tr = tl + 1;
  ti = t - float(tl);

  // Check the boundary conditions and make cyclic
  if(tl < 0) tl = (int)series.size() - abs(tl);
  tl = tl % series.size();
  if(tr < 0) tr = (int)series.size() - abs(tr);
  tr = tr % series.size();

  // Fetch the velocity vectors
  qfeFetchVelocityLinear(series[tl], p, vl);
  qfeFetchVelocityLinear(series[tr], p, vr);

  // Correct the current time
  time = tl + ti;

  // Compute the linear interpolation
  v.x = vl.x*(1.0-ti) + vr.x*ti;
  v.y = vl.y*(1.0-ti) + vr.y*ti;
  v.z = vl.z*(1.0-ti) + vr.z*ti;
  v.w = time;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeIntegrateRungeKutta4(qfeVolumeSeries series, qfePoint seed, float stepSize, float stepDuration, int stepDirection, int stepModulation, qfeMatrix4f p2v, qfePoint &pos)
{
  qfePoint  p, p1, p2, p3, s;
  qfeVector vi, vi_1, vi_2, vi_3;
  qfeVector k1, k2, k3, k4;
  qfeFloat  t, dt, m, sd;

  // Set the trace direction
  if(stepDirection >= 0) sd =  1.0f;
  else                   sd = -1.0f;

  // Determine initial time
  s.qfeSetPointElements(seed.x, seed.y, seed.z);
  t = seed.w;

  // Compute time step in seconds
  dt = stepDuration / 1000.0f;

  // Compute speed modulation
  m = stepModulation / 100.0f;

  // Get the velocity in patient coordinates (cm/s)
  p = s*p2v;
  p.qfeSetPointElements(p.x, p.y, p.z, t);
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi);
  vi = sd * m * vi;

  k1     = 10.0*vi*dt;
  p1     = s + 0.5*k1;
  p1.w   = t + 0.5*stepSize;

  p = p1*p2v;
  p.qfeSetPointElements(p.x, p.y, p.z, p1.w);
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi_1);
  vi_1 = sd * m * vi_1;

  k2     = 10.0*vi_1*dt;
  p2     = s + 0.5*k2;
  p2.w   = t + 0.5*stepSize;

  p = p2*p2v;
  p.qfeSetPointElements(p.x, p.y, p.z, p2.w);
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi_2);
  vi_2 = sd * m * vi_2;

  k3     = 10.0*vi_2*dt;
  p3     = s + k3;
  p3.w   = t + stepSize;

  p = p3*p2v;
  p.qfeSetPointElements(p.x, p.y, p.z, p3.w);
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi_3);
  vi_3 = sd * m * vi_3;

  k4     = 10.0*vi_3*dt;

  // New position in patient coordinates (mm)
  s  = s + (k1 + k2*2.0 + k3*2.0 + k4)*(1.0/6.0);
  t  = t + sd*stepSize;

  pos.qfeSetPointElements(s.x, s.y, s.z, t);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeInitTraceStatic()
{
  float   traceDuration;
  //int     tracePhases;
  int	  tracePhasesSecond;
  float   phaseDurationFirst, phaseDurationSecond;

  // For this visual, we fix the number of samples
  // The odd number represents one mid-point, and 28 forward / backward samples along the line trace
  // Also, we use only one iteration

  phaseDurationFirst = 0.0;
  if((int)this->seriesFirst.size() > 0)
	this->seriesFirst.front()->qfeGetVolumePhaseDuration(phaseDurationFirst);
  
  phaseDurationSecond = 0.0;
  if((int)this->seriesSecond.size() > 0)
	this->seriesSecond.front()->qfeGetVolumePhaseDuration(phaseDurationSecond);

  //tracePhases             =  1;
  this->traceSamples            = 15;
  this->traceSteps              =  5; 
  
  traceDuration                 = tracePhases * phaseDurationFirst;
  this->traceStepDurationFirst  = traceDuration / (float)(this->traceSamples * this->traceSteps);
  this->traceStepSizeFirst      = tracePhases   / (float)(this->traceSamples * this->traceSteps);

  // We do not change the phase duration, as we want the traces to be equally long
  // The sampling remains the same, but the step size as a fraction of the phases changes 
  tracePhasesSecond             = phaseDurationFirst / phaseDurationSecond;                  
  this->traceStepDurationSecond = traceDuration      / (float)(this->traceSamples * this->traceSteps);
  this->traceStepSizeSecond     = tracePhasesSecond  / (float)(this->traceSamples * this->traceSteps);

  this->traceIterations         = 1;
  //this->phaseCycleCount         = (int)((this->phaseStart + tracePhases - 1) / (float)this->seriesFirst.size());

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// seeds should be provided in texture coordinates, 
// and will be stored in patient coordinates
qfeReturnStatus qfeSimulationLinesComparison::qfeAddParticlePositions(vector< vector<float> > seeds, qfeVolume *volume)
{
  if((volume == NULL) || (int(seeds.size()) <= 0)) 
	return qfeError;

  int              currentTime;
  vector< float >  currentParticles;  
  vector< float >  currentAttribs;
  qfeGrid         *grid;
  qfePoint         spacing;
  qfePoint         currentPosition;
  qfeMatrix4f      V2P, T2V, T2P;
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);    
  T2P = T2V * V2P;

  // we fix the particle visualization size based on the voxel size
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(spacing.x, spacing.y, spacing.z);

  this->paramParticleSize = 0.5 * min(min(spacing.x, spacing.y), spacing.z);

  // obtain and transform all particles 
  this->particlePositions.clear();  
  this->particleAttribs.clear();
  currentParticles.clear();
  currentAttribs.clear();
  currentTime = -1;
  
  for(int i=0; i<(int)seeds.size(); i++)
  {     
	// we assume x, y, z in texture coordinates and time in phases
	if((int)seeds[i].size() != 4) continue;
	
	// make a new set for each time phase
	if(currentTime != seeds[i][3])
	{
	  if((int)currentParticles.size() > 0)
	  {
		this->particlePositions.push_back(currentParticles);
		this->particleAttribs.push_back(currentAttribs);
	  }

	  currentParticles.clear();
	  currentAttribs.clear();
	  currentTime = seeds[i][3];
	}
	currentPosition = qfePoint(seeds[i][0],seeds[i][1],seeds[i][2]) * T2P;
	currentParticles.push_back(currentPosition.x);
	currentParticles.push_back(currentPosition.y);
	currentParticles.push_back(currentPosition.z);
	currentParticles.push_back(seeds[i][3]);

	currentAttribs.push_back(0.0);
	currentAttribs.push_back(0.0);      
	currentAttribs.push_back(0.0);              
  }

  // add the last set
  if((int)currentParticles.size() > 0)
  {
	this->particlePositions.push_back(currentParticles);
	this->particleAttribs.push_back(currentAttribs);
  }  
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeInitBufferStatic(vector<float> *vertices, GLuint &VBO)
{
  int dataSize;

  this->qfeClearBufferStatic(VBO);

  dataSize = vertices->size() * sizeof(GLfloat);

  glGenBuffers(1, &VBO);  

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &((*vertices)[0]), GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeClearBufferStatic(GLuint &VBO)
{
  if(glIsBuffer(VBO))
  {
	glDeleteBuffers(1, &VBO);    
  }
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeBindTextures()
{
  glActiveTexture(GL_TEXTURE8);
  glBindTexture(GL_TEXTURE_1D, this->particleTransferFunction);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetStaticUniformLocations()
{  
  this->shaderProgramRenderSphere->qfeSetUniform1i("particleTransferFunction", 8);
  this->shaderProgramRenderTuboid->qfeSetUniform1i("lineTransferFunction", 8);  
  this->shaderProgramRenderArrows->qfeSetUniform1i("lineTransferFunction", 8);
  this->shaderProgramRenderSurface->qfeSetUniform1i("lineTransferFunction", 8);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationLinesComparison::qfeSetDynamicUniformLocations()
{
  qfePoint  lightSpec;
  qfeVector lightDir;
  qfeFloat  phaseCount;  
  int       highlight;

  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 10.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
	this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
	this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
	this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
	this->light->qfeGetLightSourceSpecularPower(lightSpec.w);
	this->light->qfeGetLightSourceDirection(lightDir);
  }

  if((int)this->seriesFirst.size() > 0)
  {
	phaseCount = (qfeFloat)this->seriesFirst.size();
  }
  else
  {
	phaseCount  = this->paramCurrentPhase;
  }

  highlight = false;
  if(this->currentRenderFirst)
	highlight =  this->paramFirstHighlight ? 1 : 0;

  this->shaderProgramRenderSphere->qfeSetUniform4f("light",                                     lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderSphere->qfeSetUniform4f("lightDirection",                            lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramRenderSphere->qfeSetUniform1f("particleSize",                              this->paramParticleSize);
  this->shaderProgramRenderSphere->qfeSetUniform1f("distanceFilter",                            this->distanceFilter / 100.0f);
  this->shaderProgramRenderSphere->qfeSetUniform1i("colorType",                                (int)this->colorType);    
  this->shaderProgramRenderSphere->qfeSetUniformMatrix4f("matrixModelView",                     1, this->matrixModelView);
  this->shaderProgramRenderSphere->qfeSetUniformMatrix4f("matrixModelViewProjection",           1, this->matrixModelViewProjection);
  this->shaderProgramRenderSphere->qfeSetUniformMatrix4f("matrixModelViewProjectionInverse",    1, this->matrixModelViewProjectionInverse);
  this->shaderProgramRenderSphere->qfeSetUniformMatrix4f("matrixProjection",                    1, this->matrixProjection);

  this->shaderProgramRenderTuboid->qfeSetUniform1f("lineWidth",                       this->lineWidth);    
  this->shaderProgramRenderTuboid->qfeSetUniform1i("colorType",                       (int)this->colorType);    
  this->shaderProgramRenderTuboid->qfeSetUniform4f("light",                           lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderTuboid->qfeSetUniform4f("lightDirection",                  lightDir.x, lightDir.y, lightDir.z, lightDir.w);  
  this->shaderProgramRenderTuboid->qfeSetUniform1f("distanceFilter",                  this->distanceFilter / 100.0f);
  this->shaderProgramRenderTuboid->qfeSetUniform1i("highlight",                       highlight);
  this->shaderProgramRenderTuboid->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramRenderTuboid->qfeSetUniformMatrix4f("matrixModelView",           1, this->matrixModelView);
  this->shaderProgramRenderTuboid->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);
  this->shaderProgramRenderTuboid->qfeSetUniformMatrix4f("matrixProjection",          1, this->matrixProjection);

  this->shaderProgramRenderArrows->qfeSetUniform1f("lineWidth",                       this->lineWidth);
  this->shaderProgramRenderArrows->qfeSetUniform1i("colorType",                       (int)this->colorType);    
  this->shaderProgramRenderArrows->qfeSetUniform4f("light",                           lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderArrows->qfeSetUniform4f("lightDirection",                  lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramRenderArrows->qfeSetUniform1f("arrowScale",                      1.5);     
  this->shaderProgramRenderArrows->qfeSetUniform1f("distanceFilter",                  this->distanceFilter / 100.0f);
  this->shaderProgramRenderArrows->qfeSetUniform1i("highlight",                       highlight);
  this->shaderProgramRenderArrows->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);
  this->shaderProgramRenderArrows->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramRenderArrows->qfeSetUniformMatrix4f("matrixProjection"      ,    1, this->matrixProjection);  

  this->shaderProgramRenderSurface->qfeSetUniform1f("lineWidth",                       this->lineWidth);    
  this->shaderProgramRenderSurface->qfeSetUniform1i("colorType",                       (int)this->colorType);    
  this->shaderProgramRenderSurface->qfeSetUniform4f("light",                           lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderSurface->qfeSetUniform4f("lightDirection",                  lightDir.x, lightDir.y, lightDir.z, lightDir.w);  
  this->shaderProgramRenderSurface->qfeSetUniform1f("distanceFilter",                  this->distanceFilter / 100.0f);
  this->shaderProgramRenderSurface->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramRenderSurface->qfeSetUniformMatrix4f("matrixModelView",           1, this->matrixModelView);
  this->shaderProgramRenderSurface->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);
  this->shaderProgramRenderSurface->qfeSetUniformMatrix4f("matrixProjection",          1, this->matrixProjection);

  return qfeSuccess;
}
