#include "qfeMaximumProjection.h"

//----------------------------------------------------------------------------
qfeMaximumProjection::qfeMaximumProjection()
{
  this->shaderMIP = new qfeGLShaderProgram();
  this->shaderMIP->qfeAddShaderFromFile("/shaders/library/transform.glsl", QFE_FRAGMENT_SHADER);
  this->shaderMIP->qfeAddShaderFromFile("/shaders/library/fetch.glsl", QFE_FRAGMENT_SHADER);
  this->shaderMIP->qfeAddShaderFromFile("/shaders/mip/mip-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderMIP->qfeAddShaderFromFile("/shaders/mip/mip-func.glsl", QFE_FRAGMENT_SHADER);
  this->shaderMIP->qfeAddShaderFromFile("/shaders/mip/mip-frag.glsl", QFE_FRAGMENT_SHADER);  
  this->shaderMIP->qfeLink();

  this->firstRender       = true;

  this->texRayStart       = 0;
  this->texRayEnd = 0;
  this->texVolume         = 0;
  this->texColorMap       = 0;

  this->texGeoColor       = 0;
  this->texGeoDepth       = 0;

  this->paramScaling[0]   = 1.0f;
  this->paramScaling[1]   = 1.0f;
  this->paramScaling[2]   = 1.0f;
  this->paramStepSize     = 1.0f/250.0f;

  this->paramGradient     = false;
  this->paramTransparency = false;
  this->paramContrast     = 0.0;
  this->paramBrightness   = 0.0;
};

//----------------------------------------------------------------------------
qfeMaximumProjection::qfeMaximumProjection(const qfeMaximumProjection &pr)
{
  this->firstRender                  = pr.firstRender;
  this->shaderMIP                    = pr.shaderMIP;

  this->viewportSize[0]              = pr.viewportSize[0];
  this->viewportSize[1]              = pr.viewportSize[1];
  this->superSampling                = pr.superSampling;

  this->texRayStart                  = pr.texRayStart;
  this->texRayEnd                    = pr.texRayEnd;
  this->texVolume                    = pr.texVolume;
  this->texColorMap                  = pr.texColorMap;

  this->texGeoColor                  = pr.texGeoColor;
  this->texGeoDepth                  = pr.texGeoDepth;

  this->paramScaling[0]              = pr.paramScaling[0];
  this->paramScaling[1]              = pr.paramScaling[1];
  this->paramScaling[2]              = pr.paramScaling[2];
  this->paramStepSize                = pr.paramStepSize;

  this->paramData                    = pr.paramData;
  this->paramComponent               = pr.paramComponent;
  this->paramRepresentation          = pr.paramRepresentation;
  this->paramDataRange[0]            = pr.paramDataRange[0];
  this->paramDataRange[1]            = pr.paramDataRange[1];
  this->paramMode                    = pr.paramMode;
  this->paramGradient                = pr.paramGradient;
  this->paramTransparency            = pr.paramTransparency;
  this->paramContrast                = pr.paramContrast;
  this->paramBrightness              = pr.paramBrightness;
};

//----------------------------------------------------------------------------
qfeMaximumProjection::~qfeMaximumProjection()
{
  delete this->shaderMIP;

  this->qfeDeleteTextures();
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeRenderMaximumIntensityProjection(GLuint colortex, qfeVolume *volume, qfeColorMap *colormap)
{
  if(volume == NULL) return qfeError;

  qfeStudy      *study    = NULL;

  if(firstRender)
  {
    this->qfeSetStaticUniformLocations();

    firstRender = false;
  }

  // Obtain color texture id
  volume->qfeGetVolumeTextureId(this->texVolume);
  
  if(volume == NULL) return qfeError;

  colormap->qfeGetColorMapTextureId(this->texColorMap);
  volume->qfeGetVolumeType(this->paramMode);

  glDisable(GL_LIGHTING);
  glEnable(GL_DEPTH_TEST);

  // Compute the stepsize and scaling factor
  qfeGrid     *grid;
  unsigned int dims[3];
  qfePoint     extent;
  volume->qfeGetVolumeSize(dims[0], dims[1], dims[2]);
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(extent.x, extent.y, extent.z);

  this->paramStepSize   = min(extent.x, min(extent.y, extent.z));  
  this->paramScaling[0] = dims[0]*extent.x; 
  this->paramScaling[1] = dims[1]*extent.y; 
  this->paramScaling[2] = dims[2]*extent.z;

  // Start rendering
  // 1a. Generate texture with ray start positions
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, this->texRayStart, 0);    
  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  // First render the bounding box start positions
  this->qfeRenderBoundingBox(volume);

  // 1b. Generate texture with ray end position
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->texRayEnd, 0);    
  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glCullFace(GL_FRONT);

  // First render the bounding box stop positions
  this->qfeRenderBoundingBox(volume);
  
  qfeGLSupport::qfeCheckFrameBuffer();

  // 2. Execute the raycasting (cull the boundingbox to overcome double rendering)
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colortex, 0);
  glClear(GL_DEPTH_BUFFER_BIT);

  this->qfeBindTextures();
  this->qfeSetDynamicUniformLocations();  
  this->shaderMIP->qfeEnable();

  glCullFace(GL_BACK);

  this->qfeRenderBoundingBox(volume);

  glDisable(GL_CULL_FACE);

  this->shaderMIP->qfeDisable();
  this->qfeUnbindTextures();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeSetViewportSize(unsigned int x, unsigned int y, int supersampling)
{
  this->viewportSize[0] = x;
  this->viewportSize[1] = y;
  this->superSampling   = supersampling;

  this->qfeUnbindTextures();
  this->qfeDeleteTextures();
  this->qfeCreateTextures();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeSetGeometryBuffers(GLuint color, GLuint depth)
{
  this->texGeoColor = color;
  this->texGeoDepth = depth;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeSetDataComponent(int component)
{
  this->paramComponent = component;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeSetDataRepresentation(int type)
{
  this->paramRepresentation = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeSetDataRange(qfeFloat vl, qfeFloat vu)
{
  this->paramDataRange[0] = vl;
  this->paramDataRange[1] = vu;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeSetGradient(bool gradient)
{
  this->paramGradient = gradient;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeSetTransparency(bool transparency)
{
  this->paramTransparency = transparency;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeSetContrast(qfeFloat contrast)
{
  this->paramContrast = contrast;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeSetBrightness(qfeFloat brightness)
{
  this->paramBrightness = brightness;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeCreateTextures()
{
  if(this->superSampling < 0) this->superSampling = 1;

  glGenTextures(1, &this->texRayStart);
  glBindTexture(GL_TEXTURE_2D, this->texRayStart);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
  glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA16F,
    this->viewportSize[0]*this->superSampling, this->viewportSize[1]*this->superSampling, 0, GL_BGRA, GL_FLOAT, NULL);
  
  glGenTextures(1, &this->texRayEnd);
  glBindTexture(GL_TEXTURE_2D, this->texRayEnd);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
  glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA16F,
    this->viewportSize[0]*this->superSampling, this->viewportSize[1]*this->superSampling, 0, GL_BGRA, GL_FLOAT, NULL);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeDeleteTextures()
{
  if (glIsTexture(this->texRayStart))     glDeleteTextures(1, (GLuint *)&this->texRayStart);
  if (glIsTexture(this->texRayEnd))       glDeleteTextures(1, (GLuint *)&this->texRayEnd);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeBindTextures()
{
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_3D, this->texVolume);

  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_1D, this->texColorMap);

  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_2D, this->texRayStart);

  glActiveTexture(GL_TEXTURE6);
  glBindTexture(GL_TEXTURE_2D, this->texRayEnd);

  if(glIsTexture(this->texGeoColor) && glIsTexture(this->texGeoDepth))
  {
    glActiveTexture(GL_TEXTURE7);
    glBindTexture(GL_TEXTURE_2D, this->texGeoColor);

    glActiveTexture(GL_TEXTURE8);
    glBindTexture(GL_TEXTURE_2D, this->texGeoDepth);  
  }
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeRenderBoundingBox(qfeVolume *volume)
{
  qfeMatrix4f   P2W, V2P, T2V, M;
  GLfloat       matrix[16];

  qfeTransform::qfeGetMatrixPatientToWorld(P2W);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  M = T2V*V2P*P2W;
  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();
  glMultMatrixf(&matrix[0]);  // Texture to World   Coordinates

  glBegin(GL_QUADS);

  // Front
  glNormal3f(0.0f, 0.0f, -1.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);

  // Back
  glNormal3f(0.0f, 0.0f, 1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f, 1.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f, 1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f,  1.0f, 1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f, 1.0f);

  // Left
  glNormal3f(-1.0f, 0.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f,  1.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);

  // Right
  glNormal3f(1.0f, 0.0f, 0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f(1.0f,  0.0f,  1.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f(1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f(1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f(1.0f,  1.0f,  1.0f);

  // Top
  glNormal3f(0.0f, -1.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f,  1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);

  // Bottom
  glNormal3f(0.0f, 1.0f, 0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f, 1.0f,  1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f, 1.0f,  1.0f);

  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeSetStaticUniformLocations()
{
  this->shaderMIP->qfeSetUniform1i("transferFunction" , 4);
  this->shaderMIP->qfeSetUniform1i("rayStart"         , 5); 
  this->shaderMIP->qfeSetUniform1i("rayEnd"           , 6); 
  this->shaderMIP->qfeSetUniform1i("geoColor"         , 7);
  this->shaderMIP->qfeSetUniform1i("geoDepth"         , 8);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeMaximumProjection::qfeSetDynamicUniformLocations()
{
  int geometryAvailable = 0;
  int grad  = 0;
  int trans = 0;

  if(glIsTexture(this->texGeoColor) && glIsTexture(this->texGeoDepth))
    geometryAvailable = 1;

  if(this->paramGradient)     grad = 1;
  if(this->paramTransparency) trans = 1;

  // Provide parameters to the shader
  this->shaderMIP->qfeSetUniform1i("mipDataSet"            , 1);
  this->shaderMIP->qfeSetUniform1i("mipDataComponent"      , this->paramComponent);
  this->shaderMIP->qfeSetUniform1i("mipDataRepresentation" , this->paramRepresentation);
  this->shaderMIP->qfeSetUniform2f("mipDataRange"          , this->paramDataRange[0], this->paramDataRange[1]);
  this->shaderMIP->qfeSetUniform1f("stepSize"              , 1.0/250.0);
  this->shaderMIP->qfeSetUniform1f("scalarSize"            , 16.0);
  this->shaderMIP->qfeSetUniform3f("scaling"               , this->paramScaling[0], this->paramScaling[1], this->paramScaling[2]);
  this->shaderMIP->qfeSetUniform1f("stepSize"              , this->paramStepSize);
  this->shaderMIP->qfeSetUniform1i("mode"                  , this->paramMode);
  this->shaderMIP->qfeSetUniform1i("gradient"              , grad);
  this->shaderMIP->qfeSetUniform1i("transparency"          , trans);
  this->shaderMIP->qfeSetUniform1f("contrast"              , this->paramContrast);
  this->shaderMIP->qfeSetUniform1f("brightness"            , this->paramBrightness);
  this->shaderMIP->qfeSetUniform1i("geoAvailable"          , geometryAvailable);

  return qfeSuccess;
}



