#pragma once

#include "qflowexplorer.h"
#include <sstream>

#include <vtkSmartPointer.h>
#include <vtkRenderer.h>
#include <vtkTextActor.h>
#include <vtkXYPlotActor.h>

#include <vtkFieldData.h>
#include <vtkDoubleArray.h>
#include <vtkDataObject.h>

#include <vtkTextProperty.h>
#include <vtkTextProperty.h>

/**
* \file   qfe2DText.h
* \author Niels de Hoon
* \class  qfe2DText
* \brief  VTK wrapper that draws 2D text on the screen
* \note   Confidential
*
* A wrapper to draw allow simple rendering of 2D text on the screen using VTK
*/

class qfe2DBasicRendering
{
public:
  qfe2DBasicRendering(vtkRenderer *renderer_);
  ~qfe2DBasicRendering();
  
  typedef struct{
	  std::string x_label;
	  std::string y_label;
	  std::vector<float> data;
  } plot_data;

  /* Renders "text" at position x,y on the screen */
  void qfeAdd2DText(unsigned int x, unsigned int y, std::string text);
  void qfeAdd2DText(unsigned int x, unsigned int y, int text);
  void qfeAdd2DText(unsigned int x, unsigned int y, float text);
  void qfeAdd2DText(unsigned int x, unsigned int y, double text);
  void qfeAdd2DText(unsigned int x, unsigned int y, std::ostringstream *text);

  void qfeAdd2DPlot(unsigned int x, unsigned int y, double fraction_of_window, plot_data data);
  void qfeAdd2DPlot(unsigned int x, unsigned int y, double fraction_of_window, std::vector<float> data);

  /* Removes all rendered text from the screen */
  void qfeClear2DText();

  unsigned int qfeGetFontSize();

private:
	vtkRenderer *renderer; 

	std::vector<vtkProp*> renderedText;

	unsigned int font_size;

	template <typename T>
	std::string to_string_with_precision(const T a_value, const int n = 3)
	{
		std::ostringstream out;
		out << std::setprecision(n) << a_value;
		return out.str();
	}
};
