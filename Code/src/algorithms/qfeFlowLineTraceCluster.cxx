#include "qfeFlowLineTraceCluster.h"

const char *qfeFlowLineTraceCluster::feedbackVarsCopy[]     = {"qfe_LinePoints"};
const char *qfeFlowLineTraceCluster::feedbackVarsGenerate[] = {"qfe_LinePoints", "qfe_LineAttribs"};

//----------------------------------------------------------------------------
qfeFlowLineTraceCluster::qfeFlowLineTraceCluster() : qfeFlowLineTrace()
{
  this->phasesPerIteration  = 2;
  this->paramClusterMasking = true;
  this->paramLengthMasking  = 0.0;
  this->paramColorType      = qfeFlowLinesColorSpeed;
  this->paramColor.r        = 0.8;
  this->paramColor.g        = 0.0;
  this->paramColor.b        = 0.0;

  this->shaderProgramGenerate = new qfeGLShaderProgram();
  this->shaderProgramGenerate->qfeAddShaderFromFile("/shaders/library/transform.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramGenerate->qfeAddShaderFromFile("/shaders/library/fetch.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramGenerate->qfeAddShaderFromFile("/shaders/library/integrate.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramGenerate->qfeAddShaderFromFile("/shaders/hcr/hcr-generate-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramGenerate->qfeAddShaderFromFile("/shaders/hcr/hcr-generate-geom.glsl", QFE_GEOMETRY_SHADER);    
  this->shaderProgramGenerate->qfeSetTransformFeedbackVaryings(2, feedbackVarsGenerate, GL_SEPARATE_ATTRIBS);
  this->shaderProgramGenerate->qfeLink();
  
  this->shaderProgramRenderLine = new qfeGLShaderProgram();
  this->shaderProgramRenderLine->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderLine->qfeAddShaderFromFile("/shaders/hcr/hcr-render-line-vert.glsl", QFE_VERTEX_SHADER);  
  this->shaderProgramRenderLine->qfeAddShaderFromFile("/shaders/hcr/hcr-render-line-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderLine->qfeAddShaderFromFile("/shaders/hcr/hcr-render-line-frag.glsl", QFE_FRAGMENT_SHADER);    
  this->shaderProgramRenderLine->qfeLink();

  this->shaderProgramRenderImposter = new qfeGLShaderProgram();    
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/hcr/hcr-render-tube-vert.glsl", QFE_VERTEX_SHADER);  
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/hcr/hcr-render-tube-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/hcr/hcr-render-tube-frag.glsl", QFE_FRAGMENT_SHADER);  
  this->shaderProgramRenderImposter->qfeLink();

  this->shaderProgramRenderArrowEnds = new qfeGLShaderProgram();
  this->shaderProgramRenderArrowEnds->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderArrowEnds->qfeAddShaderFromFile("/shaders/hcr/hcr-render-arrow-vert.glsl", QFE_VERTEX_SHADER);  
  this->shaderProgramRenderArrowEnds->qfeAddShaderFromFile("/shaders/hcr/hcr-render-arrow-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderArrowEnds->qfeAddShaderFromFile("/shaders/hcr/hcr-render-arrow-frag.glsl", QFE_FRAGMENT_SHADER);    
  this->shaderProgramRenderArrowEnds->qfeLink();

  this->clusterLabelSeries.clear();
};

//----------------------------------------------------------------------------
qfeFlowLineTraceCluster::qfeFlowLineTraceCluster(const qfeFlowLineTraceCluster &fv) : qfeFlowLineTrace(fv)
{
  this->clusterLabelSeries                 = fv.clusterLabelSeries;
  this->paramClusterMasking                = fv.paramClusterMasking;
  this->paramLengthMasking                 = fv.paramLengthMasking;
  this->paramColorType                     = fv.paramColorType;
  this->paramColor                         = fv.paramColor;

  this->shaderProgramRenderArrowEnds       = fv.shaderProgramRenderArrowEnds;
};

//----------------------------------------------------------------------------
qfeFlowLineTraceCluster::~qfeFlowLineTraceCluster()
{
  delete this->shaderProgramRenderArrowEnds;
};

//----------------------------------------------------------------------------
// phase - current phase
qfeReturnStatus qfeFlowLineTraceCluster::qfeSetPhase(float phase)
{
  this->phaseCurrent  = phase;
  this->phaseGenerate = phase;
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// masking - when true, line segments outside the cluster boundaries are omitted
qfeReturnStatus qfeFlowLineTraceCluster::qfeSetClusterMask(bool masking)
{
  this->paramClusterMasking = masking;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// length - when the line is not visible when the line is shorter then length (mm)
qfeReturnStatus qfeFlowLineTraceCluster::qfeSetLengthMask(float length)
{
  this->paramLengthMasking = length;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeSetColorType(qfeFlowLinesColorType type)
{
  this->paramColorType = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeSetColor(qfeColorRGBA color)
{
  this->paramColor = color;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeGenerateSeeds(qfeVolumeSeries series, qfeSeeds *seeds)
{
  if(((int)series.size() <= 0)) return qfeError;

  qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, series.front());
  qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, series.front());

  // Set the initial particles in the buffer
  this->qfeInitSeedBuffer(seeds);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeGeneratePathLinesStatic(qfeVolumeSeries series, qfeSeeds *seeds)
{
  const int     traceSamples  = 57;
  const int     centerSample  = (traceSamples-1)/2;
  const float   delta         = 0.0001;

  // start points
  // 1.0 - both visible with / without mask
  // 2.0 - without mask
  // 3.0 - with mask

  // end points
  // 4.0 - with mask
  // 5.0 - without mask
  // 6.0 - both visible with / without mask

  qfeMatrix4f   V2P;
  int           steps;      
  qfePoint      pos, posTexture, attrib, seed; 
  qfeFloat      time, speed, traceLength;
  qfeVector     velocity;
  qfePoint      vertexPositions[traceSamples];
  qfePoint      vertexAttribs[traceSamples];
  vector<float> lines;
  vector<float> attribs;

  if((int)series.size() <= 0) return qfeError;  
  if((int)seeds->size() <= 0) return qfeError;
  
  // First time of generation, prepare some global variables
  if(this->firstGenerate)
  {
    this->volumeSeries       = series;    

    this->volumeSeries.front()->qfeGetVolumeSize(this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);

    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, this->volumeSeries.front());
    qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, this->volumeSeries.front());

    this->qfeSetStaticUniformLocations();

    this->firstGenerate = false;
  }    

  // Initialize  
  qfeMatrix4f::qfeGetMatrixInverse(this->P2V, V2P);
  traceLength       = 0.0f;  
  steps             = int(max(traceSamples-3, traceSteps)/float(traceSamples-3));  
  this->seedsCount  = int(seeds->size());
  lines.clear();

  // Set the all the variables necessary to trace the lines  
  this->qfeInitTraceStatic();
  
  for(int sc=0 ; sc<(int)this->seedsCount; sc++)
  {
    // Get first seed
    time   = (*seeds)[sc].w;
    seed   = (*seeds)[sc];
    seed.w = 1.0;
    qfeFetchVelocityLinearTemporalLinear(series,seed,time,velocity);    

    qfeVector::qfeVectorLength(velocity, speed);
    attrib.qfeSetPointElements(speed,1.0,0.0,0.0);

    // Set start position
    pos    = seed * V2P;
    pos.w  = time;

    // Set the center sample
    vertexPositions[centerSample] = pos;
    vertexAttribs[centerSample]   = attrib;    

    // -- Trace backward  
    for(int i=1; i<(traceSamples-1)/2; i++)
    {           
      qfePoint previousPos    = pos; 
      qfeFloat lengthTmp      = 0.0;
      qfePoint posVoxel;         

      for(int j=0; j<steps; j++)
      {
        qfePoint posTmp = pos;
      
        qfeIntegrateRungeKutta4(series, pos, this->traceStepSize, this->traceStepDuration, -1, 100, this->P2V, pos);        
          
        // accumulate the trace length
        qfeVector::qfeVectorLength(pos-posTmp, lengthTmp);
        traceLength = traceLength + lengthTmp;
      }  
      
      // -- Check conditions
      posTexture.qfeSetPointElements(pos.x, pos.y, pos.z, 1.0);
      posTexture    = posTexture * this->P2V;
      posTexture.x /= this->volumeSize[0];
      posTexture.y /= this->volumeSize[1];
      posTexture.z /= this->volumeSize[2];

      // check the bounding box condition    
      if((posTexture.x <= 0.0 || posTexture.y <= 0.0 || posTexture.z <= 0.0) ||
         (posTexture.x >= 1.0 || posTexture.y >= 1.0 || posTexture.z >= 1.0))
         pos.qfeSetPointElements(previousPos.x, previousPos.y, previousPos.z);
    
      // compute voxel position      
      posVoxel      = pos;
      posVoxel.w    = 1.0;
      posVoxel      = posVoxel * this->P2V;
      
      // set the current position and velocity   
      qfeFetchVelocityLinearTemporalLinear(series, posVoxel, pos.w, velocity);

      qfeVector::qfeVectorLength(velocity, speed);    
      attrib.qfeSetPointElements(speed,0.0,0.0,0.0);
      
      // add to the sample array    
      vertexPositions[centerSample-i] = pos;
      vertexAttribs[centerSample-i]   = attrib;    
    }

    // Fix adjacency information
    vertexPositions[0] = vertexPositions[1];
    vertexAttribs[0]   = vertexAttribs[1];
    
    // Fix start points
    vertexAttribs[0].w = 0.0;    

    for(int i=0; i<centerSample; i++)
      if(abs(int(vertexAttribs[i+1].y-vertexAttribs[i].y)) == 1.0) 
        vertexAttribs[i].w   = 3.0;
        
    vertexAttribs[1].w = 2.0;         

    // -- Trace forward  
    pos = vertexPositions[centerSample];
    for(int i=1; i<(traceSamples-1)/2; i++)
    {           
      qfePoint previousPos    = pos; 
      qfeFloat lengthTmp      = 0.0;
      qfePoint posVoxel;        

      for(int j=0; j<steps; j++)
      {
        qfePoint posTmp = pos;
      
        qfeIntegrateRungeKutta4(series, pos, this->traceStepSize, this->traceStepDuration, 1, 100, this->P2V, pos);        
          
        // accumulate the trace length
        qfeVector::qfeVectorLength(pos-posTmp, lengthTmp);
        traceLength = traceLength + lengthTmp;
      }  

       // -- Check conditions
      posTexture.qfeSetPointElements(pos.x, pos.y, pos.z, 1.0);
      posTexture    = posTexture * this->P2V;
      posTexture.x /= this->volumeSize[0];
      posTexture.y /= this->volumeSize[1];
      posTexture.z /= this->volumeSize[2];

      // check the bounding box condition    
      if((posTexture.x <= 0.0 || posTexture.y <= 0.0 || posTexture.z <= 0.0) ||
         (posTexture.x >= 1.0 || posTexture.y >= 1.0 || posTexture.z >= 1.0))
         pos.qfeSetPointElements(previousPos.x, previousPos.y, previousPos.z);

      // compute voxel position      
      posVoxel      = pos;
      posVoxel.w    = 1.0;
      posVoxel      = posVoxel * this->P2V;
      
      // set the current position and velocity        
      qfeFetchVelocityLinearTemporalLinear(series, posVoxel, pos.w, velocity);

      qfeVector::qfeVectorLength(velocity, speed);
      attrib.qfeSetPointElements(speed,0.0,0.0,0.0);    
      
      // add to the sample array    
      vertexPositions[centerSample+i] = pos;
      vertexAttribs[centerSample+i]   = attrib;    
    }

    // Fix adjacency information
    vertexPositions[traceSamples-1] = vertexPositions[traceSamples-2];  
    vertexAttribs[traceSamples-1]   = vertexAttribs[traceSamples-2];

    // Fix end points
    vertexAttribs[traceSamples-1].w = 0.0;  

    for(int i=centerSample; i<traceSamples; i++)
      if(abs(int(vertexAttribs[i+1].y-vertexAttribs[i].y)) == 1.0) 
        vertexAttribs[i+1].w = 4.0;

    vertexAttribs[traceSamples-2].w = 5.0;     

    // Fix too short lines after masking
    int count = 0;

    for(int i=0; i<traceSamples;i++)    
      if(vertexAttribs[i].y == 1.0) count++;

    if(count <= 2)
    {
      for(int i=0; i<traceSamples;i++) 
      {
        vertexAttribs[i].y = 0.0;
        vertexAttribs[i].w = 0.0; 
      }
      vertexAttribs[0].w              = 2.0;
      vertexAttribs[traceSamples-1].w = 5.0;
    }   

    // Store the pathline
    for(int i=0; i<traceSamples; i++)
    {    
      // Set the trace length
      vertexAttribs[i].z = traceLength;

      lines.push_back(vertexPositions[i].x);
      lines.push_back(vertexPositions[i].y);
      lines.push_back(vertexPositions[i].z);
      lines.push_back(vertexPositions[i].w);

      attribs.push_back(vertexAttribs[i].x);
      attribs.push_back(vertexAttribs[i].y);
      attribs.push_back(vertexAttribs[i].z);
      attribs.push_back(vertexAttribs[i].w);
    }    
  }

  // -- Put the lines and attributes into a buffer object
  
  // Set-up the vertex buffer objects (VBO)
  this->qfeInitLineBufferStatic(&lines, &attribs);
    
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeGeneratePathLinesDynamic(qfeVolumeSeries series, qfeVolumeSeries clusterLabels, qfeSeeds *seeds, qfeFloat phase)
{
  GLuint  count = 0;
  GLuint  query;
  GLuint  activePosVBO, activeAttrVBO;
  int     dataSize;  
  int     locPosition;

  if(((int)series.size() <= 0)) return qfeError;

  // First time of generation, prepare some global variables
  if(this->firstGenerate)
  {
    this->volumeSeries       = series;
    this->clusterLabelSeries = clusterLabels;

    this->volumeSeries.front()->qfeGetVolumeSize(this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);

    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, this->volumeSeries.front());
    qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, this->volumeSeries.front());

    this->qfeSetStaticUniformLocations();

    this->firstGenerate = false;
  }    
 
  // Set the all the variables necessary to trace the lines
  this->qfeInitTraceDynamic();                                 
  
  // Set the initial particles in the buffer
  this->qfeInitSeedBuffer(seeds, (int)floor(phase));

  // Create the initial line buffer
  this->qfeInitLineBufferDynamic();

  // Create the output buffer & check if we have the necessary buffers
  if(!glIsBuffer(this->seedPositionsVBO))      return qfeError;  
  if(!glIsBuffer(this->linePositionsDynVBO))   return qfeError;
  if(!glIsBuffer(this->lineAttributesDynVBO))  return qfeError;
  activePosVBO  = this->linePositionsDynVBO;
  activeAttrVBO = this->lineAttributesDynVBO;
    
  // No need for rasterization
   glEnable(GL_RASTERIZER_DISCARD);
 
  // Enable the velocity volumes
  this->qfeBindTexturesSliding(series, (int)floor(phase));

  // Attach the cluster labeled volume
  //this->qfeBindTexturesCluster(clusterLabels, (int)floor(phase));
  this->qfeBindTexturesClusterSliding(clusterLabels, (int)floor(phase));

  // -- 1 Render seeds to get a the line --
   
  // Get input location
  this->shaderProgramGenerate->qfeGetAttribLocation("qfe_SeedPoints", locPosition);  

  GLsizei vertexSize =   4*sizeof(GLfloat);
  GLsizei vertexCount =  this->traceSamples;

  // Bind the output buffer at the right offset
  dataSize        = this->seedsCount * vertexSize * vertexCount;    
  glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, activePosVBO, 0, dataSize);    
  glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 1, activeAttrVBO, 0, dataSize);    

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocations();

  // Enable the shader program
  this->shaderProgramGenerate->qfeEnable();

  // Generate the lines using a transform feedback
  glGenQueries(1, &query);
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
  glBeginTransformFeedback(GL_POINTS);

  this->qfeDrawSeeds(this->seedPositionsVBO, locPosition, (int)this->seedsCount); 

  glEndTransformFeedback();
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

  // Check if we have output in the buffer
  glGetQueryObjectuiv(query, GL_QUERY_RESULT, &count);
    
  // Disable the shader program
  this->shaderProgramGenerate->qfeDisable();

  // Clean-up
  glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, 0);

  glFinish(); 

  // Clean up
  glDisable(GL_RASTERIZER_DISCARD);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeGeneratePathLinesStatic2(qfeVolumeSeries series, qfeVolumeSeries clusterLabels, qfeSeeds *seeds)
{
  GLuint  count = 0;
  GLuint  query;
  GLuint  activePosVBO, activeAttrVBO;
  int     dataSize, dataSizeAcc, seedCountAcc;  
  int     locPosition;

  if(((int)series.size() <= 0)) return qfeError;

  // First time of generation, prepare some global variables
  if(this->firstGenerate)
  {
    this->volumeSeries       = series;
    this->clusterLabelSeries = clusterLabels;

    this->volumeSeries.front()->qfeGetVolumeSize(this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);

    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, this->volumeSeries.front());
    qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, this->volumeSeries.front());

    this->qfeSetStaticUniformLocations();

    this->firstGenerate = false;
  }     
  
  // -- Iterate over all phases to generate all possible lines
  dataSizeAcc  = 0;
  seedCountAcc = 0;
  for(int i=0; i<(int)this->volumeSeries.size(); i++)
  {
    // Set the all the variables necessary to trace the lines
    this->qfeInitTraceDynamic();  
  
    // Set the initial particles in the buffer
    this->qfeInitSeedBuffer(seeds, i);
    seedCountAcc += this->seedsCount;

    // Create the initial line buffer
    this->qfeInitLineBufferDynamic();

    // Create the output buffer & check if we have the necessary buffers
    if(!glIsBuffer(this->seedPositionsVBO))      return qfeError;  
    if(!glIsBuffer(this->linePositionsDynVBO))   return qfeError;
    if(!glIsBuffer(this->lineAttributesDynVBO))  return qfeError;
    activePosVBO  = this->linePositionsDynVBO;
    activeAttrVBO = this->lineAttributesDynVBO;
      
    // No need for rasterization
    glEnable(GL_RASTERIZER_DISCARD);
   
    // Enable the velocity volumes
    this->qfeBindTexturesSliding(series, i);

    // Attach the cluster labeled volume    
    this->qfeBindTexturesClusterSliding(clusterLabels, i);

    // -- 1 Render seeds to get a the line --
     
    // Get input location
    this->shaderProgramGenerate->qfeGetAttribLocation("qfe_SeedPoints", locPosition);  

    GLsizei vertexSize =   4*sizeof(GLfloat);
    GLsizei vertexCount =  this->traceSamples;

    // Bind the output buffer at the right offset
    dataSize        = this->seedsCount * vertexSize * vertexCount;    
    glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, activePosVBO,  dataSizeAcc, dataSize);    
    glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 1, activeAttrVBO, dataSizeAcc, dataSize);  
    dataSizeAcc    += dataSize;

    // Set the dynamic uniform inputs
    this->qfeSetDynamicUniformLocations();

    // Enable the shader program
    this->shaderProgramGenerate->qfeEnable();

    // Generate the lines using a transform feedback
    glGenQueries(1, &query);
    glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
    glBeginTransformFeedback(GL_POINTS);

    this->qfeDrawSeeds(this->seedPositionsVBO, locPosition, (int)this->seedsCount); 

    glEndTransformFeedback();
    glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

    // Check if we have output in the buffer
    glGetQueryObjectuiv(query, GL_QUERY_RESULT, &count);
      
    // Disable the shader program
    this->shaderProgramGenerate->qfeDisable();

    // Clean-up
    glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, 0);

    glFinish(); 

    // Clean up
    glDisable(GL_RASTERIZER_DISCARD);
  }

  this->seedsCount = seedCountAcc;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeGeneratePathLinesStatic(qfeVolumeSeries series, qfeVolumeSeries clusterLabels, qfeSeeds *seeds)
{
  const int     traceSamples  = 57;
  const int     centerSample  = (traceSamples-1)/2;
  const float   delta         = 0.0001;

  // start points
  // 1.0 - both visible with / without mask
  // 2.0 - without mask
  // 3.0 - with mask

  // end points
  // 4.0 - with mask
  // 5.0 - without mask
  // 6.0 - both visible with / without mask

  qfeMatrix4f   V2P;
  int           steps;
  bool          insideFlag    = true;  
  qfeFloat      insideCluster = 1.0;
  qfeFloat      clusterLabel, currentLabel;
  qfePoint      pos, posTexture, attrib, seed; 
  qfeFloat      time, speed, traceLength;
  qfeVector     velocity;
  qfePoint      vertexPositions[traceSamples];
  qfePoint      vertexAttribs[traceSamples];
  vector<float> lines;
  vector<float> attribs;

  if((int)series.size() <= 0) return qfeError;  
  if((int)seeds->size() <= 0) return qfeError;
  
  // First time of generation, prepare some global variables
  if(this->firstGenerate)
  {
    this->volumeSeries       = series;
    this->clusterLabelSeries = clusterLabels;

    this->volumeSeries.front()->qfeGetVolumeSize(this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);

    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, this->volumeSeries.front());
    qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, this->volumeSeries.front());

    this->qfeSetStaticUniformLocations();

    this->firstGenerate = false;
  }    

  // Initialize  
  qfeMatrix4f::qfeGetMatrixInverse(this->P2V, V2P);
  traceLength       = 0.0f;  
  steps             = int(max(traceSamples-3, traceSteps)/float(traceSamples-3));  
  this->seedsCount  = int(seeds->size());
  lines.clear();

  // Set the all the variables necessary to trace the lines  
  this->qfeInitTraceStatic();
  
  for(int sc=0 ; sc<(int)this->seedsCount; sc++)
  {
    // Get first seed
    time   = (*seeds)[sc].w;
    seed   = (*seeds)[sc];
    seed.w = 1.0;
    qfeFetchVelocityLinearTemporalLinear(series,seed,time,velocity);
    qfeFetchClusterLabelTemporalNearest(clusterLabels, seed, time, clusterLabel);

    qfeVector::qfeVectorLength(velocity, speed);
    attrib.qfeSetPointElements(speed,1.0,0.0,0.0);

    // Set start position
    pos    = seed * V2P;
    pos.w  = time;

    // Set the center sample
    vertexPositions[centerSample] = pos;
    vertexAttribs[centerSample]   = attrib;    

    // -- Trace backward  
    insideFlag    = true;
    insideCluster = 1.0;
    for(int i=1; i<(traceSamples-1)/2; i++)
    {           
      qfePoint previousPos    = pos; 
      //qfeFloat previousInside = insideCluster;    
      qfeFloat lengthTmp      = 0.0;
      qfePoint posVoxel;         

      for(int j=0; j<steps; j++)
      {
        qfePoint posTmp = pos;
      
        qfeIntegrateRungeKutta4(series, pos, this->traceStepSize, this->traceStepDuration, -1, 100, this->P2V, pos);        
          
        // accumulate the trace length
        qfeVector::qfeVectorLength(pos-posTmp, lengthTmp);
        traceLength = traceLength + lengthTmp;
      }  
      
      // -- Check conditions
      posTexture.qfeSetPointElements(pos.x, pos.y, pos.z, 1.0);
      posTexture    = posTexture * this->P2V;
      posTexture.x /= this->volumeSize[0];
      posTexture.y /= this->volumeSize[1];
      posTexture.z /= this->volumeSize[2];

      // check the bounding box condition    
      if((posTexture.x <= 0.0 || posTexture.y <= 0.0 || posTexture.z <= 0.0) ||
         (posTexture.x >= 1.0 || posTexture.y >= 1.0 || posTexture.z >= 1.0))
         pos.qfeSetPointElements(previousPos.x, previousPos.y, previousPos.z);
    
      // compute voxel position      
      posVoxel      = pos;
      posVoxel.w    = 1.0;
      posVoxel      = posVoxel * this->P2V;

      // check cluster bounding condition         
      qfeFetchClusterLabelTemporalNearest(clusterLabels, posVoxel, pos.w, currentLabel);
      
      if((insideFlag == true) && (currentLabel > clusterLabel-delta) && (currentLabel < clusterLabel+delta))         
        insideCluster = 1.0;   
      else      
      {
        insideCluster = 0.0;
        insideFlag    = false;     
      }

      // set the current position and velocity   
      qfeFetchVelocityLinearTemporalLinear(series, posVoxel, pos.w, velocity);

      qfeVector::qfeVectorLength(velocity, speed);    
      attrib.qfeSetPointElements(speed,insideCluster,0.0,0.0);
      
      // add to the sample array    
      vertexPositions[centerSample-i] = pos;
      vertexAttribs[centerSample-i]   = attrib;    
    }

    // Fix adjacency information
    vertexPositions[0] = vertexPositions[1];
    vertexAttribs[0]   = vertexAttribs[1];
    
    // Fix start points
    vertexAttribs[0].w = 0.0;    

    if(insideFlag == true)
      vertexAttribs[1].w = 1.0;
    else
    {
      for(int i=0; i<centerSample; i++)
        if(abs(int(vertexAttribs[i+1].y-vertexAttribs[i].y)) == 1.0) 
          vertexAttribs[i].w   = 3.0;
        
      vertexAttribs[1].w = 2.0;     
    }

    // -- Trace forward  
    pos = vertexPositions[centerSample];
    insideFlag    = true;
    insideCluster = 1.0;
    for(int i=1; i<(traceSamples-1)/2; i++)
    {           
      qfePoint previousPos    = pos; 
      qfeFloat previousInside = insideCluster;    
      qfeFloat lengthTmp      = 0.0;
      qfePoint posVoxel;        

      for(int j=0; j<steps; j++)
      {
        qfePoint posTmp = pos;
      
        qfeIntegrateRungeKutta4(series, pos, this->traceStepSize, this->traceStepDuration, 1, 100, this->P2V, pos);        
          
        // accumulate the trace length
        qfeVector::qfeVectorLength(pos-posTmp, lengthTmp);
        traceLength = traceLength + lengthTmp;
      }  

       // -- Check conditions
      posTexture.qfeSetPointElements(pos.x, pos.y, pos.z, 1.0);
      posTexture    = posTexture * this->P2V;
      posTexture.x /= this->volumeSize[0];
      posTexture.y /= this->volumeSize[1];
      posTexture.z /= this->volumeSize[2];

      // check the bounding box condition    
      if((posTexture.x <= 0.0 || posTexture.y <= 0.0 || posTexture.z <= 0.0) ||
         (posTexture.x >= 1.0 || posTexture.y >= 1.0 || posTexture.z >= 1.0))
         pos.qfeSetPointElements(previousPos.x, previousPos.y, previousPos.z);

      // compute voxel position      
      posVoxel      = pos;
      posVoxel.w    = 1.0;
      posVoxel      = posVoxel * this->P2V;

      // check cluster bounding condition        
      qfeFetchClusterLabelTemporalNearest(clusterLabels, posVoxel, pos.w, currentLabel);

      if((insideFlag == true) && (currentLabel > clusterLabel-delta) && (currentLabel < clusterLabel+delta))         
        insideCluster = 1.0;   
      else
      {
        insideCluster = 0.0;
        insideFlag    = false;              
      }
     
      // set the current position and velocity        
      qfeFetchVelocityLinearTemporalLinear(series, posVoxel, pos.w, velocity);

      qfeVector::qfeVectorLength(velocity, speed);
      attrib.qfeSetPointElements(speed,insideCluster,0.0,0.0);    
      
      // add to the sample array    
      vertexPositions[centerSample+i] = pos;
      vertexAttribs[centerSample+i]   = attrib;    
    }

    // Fix adjacency information
    vertexPositions[traceSamples-1] = vertexPositions[traceSamples-2];  
    vertexAttribs[traceSamples-1]   = vertexAttribs[traceSamples-2];

    // Fix end points
    vertexAttribs[traceSamples-1].w = 0.0;  

    if(insideFlag == true)
      vertexAttribs[traceSamples-2].w = 6.0;   
    else
    {
      for(int i=centerSample; i<traceSamples; i++)
        if(abs(int(vertexAttribs[i+1].y-vertexAttribs[i].y)) == 1.0) 
          vertexAttribs[i+1].w = 4.0;

      vertexAttribs[traceSamples-2].w = 5.0; 
    }

    // Fix too short lines after masking
    int count = 0;

    for(int i=0; i<traceSamples;i++)    
      if(vertexAttribs[i].y == 1.0) count++;

    if(count <= 2)
    {
      for(int i=0; i<traceSamples;i++) 
      {
        vertexAttribs[i].y = 0.0;
        vertexAttribs[i].w = 0.0; 
      }
      vertexAttribs[0].w              = 2.0;
      vertexAttribs[traceSamples-1].w = 5.0;
    }   

    // Store the pathline
    for(int i=0; i<traceSamples; i++)
    {    
      // Set the trace length
      vertexAttribs[i].z = traceLength;

      lines.push_back(vertexPositions[i].x);
      lines.push_back(vertexPositions[i].y);
      lines.push_back(vertexPositions[i].z);
      lines.push_back(vertexPositions[i].w);

      attribs.push_back(vertexAttribs[i].x);
      attribs.push_back(vertexAttribs[i].y);
      attribs.push_back(vertexAttribs[i].z);
      attribs.push_back(vertexAttribs[i].w);
    }    
  }

  // -- Put the lines and attributes into a buffer object
  
  // Set-up the vertex buffer objects (VBO)
  this->qfeInitLineBufferStatic(&lines, &attribs);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeRenderClusterSeeds()
{
  // Check if we have the necessary buffers
  if(!glIsBuffer(this->seedPositionsVBO))      return qfeError;  

  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glEnable(GL_POINT_SMOOTH);
  glPointSize(4.0);
  glColor3f(this->paramColor.r,this->paramColor.g,this->paramColor.b);
    
  glBindBuffer(GL_ARRAY_BUFFER, this->seedPositionsVBO);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0,  3, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
  
  glDrawArrays(GL_POINTS, 0, (int)this->seedsCount);

  glDisableVertexAttribArray(0); 

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeRenderClusterPathlinesStatic(qfeColorMap *colormap)
{
  // First time of rendering, prepare some global variables
  if(this->firstRender)
  {
    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->colormap); 

  // Check if the buffer is available
  if(!glIsBuffer(this->linePositionsStatVBO))  return qfeError;  

  // Compute the amount of points to render  
  GLsizei lineCount    = this->seedsCount;
  GLsizei lineSize     = this->traceSamples;
  
  // Obtain the modelview information
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Set point parameters, in case of normal point rendering
  glLineWidth(this->supersampling * this->lineWidth * 2);
  glEnable(GL_LINE_SMOOTH);
  glPointSize(4.0);
  glEnable(GL_POINT_SMOOTH);
  glColor3f(0.0,1.0,0.0);

  // Bind textures
  this->qfeBindTexturesRender();

  // Allow blending for transparency
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocations();

  switch(this->traceStyle)
  {
  case qfeFlowLinesTuboid : 
    this->qfeRenderStyleTube(this->linePositionsStatVBO, this->lineAttributesStatVBO, lineCount, lineSize);
    break;
  case qfeFlowLinesLine   :   
    this->qfeRenderStyleLine(this->linePositionsStatVBO, this->lineAttributesStatVBO, lineCount, lineSize);
    break;
  case qfeFlowLinesArrow   :   
    this->qfeRenderStyleArrow(this->linePositionsStatVBO, this->lineAttributesStatVBO, lineCount, lineSize);
    break;
  default :
    this->qfeRenderStyleLine(this->linePositionsStatVBO, this->lineAttributesStatVBO, lineCount, lineSize);  
    break;
  } 

  glDisable(GL_BLEND);

  // Unbind textures
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeRenderClusterPathlinesDynamic(qfeColorMap *colormap)
{
  // First time of rendering, prepare some global variables
  if(this->firstRender)
  {
    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->colormap); 

  // Dynamic trace generates lines for each phase
  //this->qfeGenerateLines(this->volumeSeries, this->phaseCurrent, qfeFlowLinesPathDynamic);

  // Check if the buffer is available
  if(!glIsBuffer(this->linePositionsDynVBO))  return qfeError;  

  // Compute the amount of points to render  
  GLsizei lineCount    = this->seedsCount;
  GLsizei lineSize     = this->traceSamples;
  
  // Obtain the modelview information
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Set point parameters, in case of normal point rendering
  glLineWidth(this->supersampling * this->lineWidth * 2);
  glEnable(GL_LINE_SMOOTH);
  glPointSize(4.0);
  glEnable(GL_POINT_SMOOTH);
  glColor3f(0.0,1.0,0.0);

  // Bind textures
  this->qfeBindTexturesRender();

  // Allow blending for transparency
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocations();

  switch(this->traceStyle)
  {
  case qfeFlowLinesTuboid : 
    this->qfeRenderStyleTube(this->linePositionsDynVBO, this->lineAttributesDynVBO, lineCount, lineSize);
    break;
  case qfeFlowLinesLine   :   
    this->qfeRenderStyleLine(this->linePositionsDynVBO, this->lineAttributesDynVBO, lineCount, lineSize);
    break;
  case qfeFlowLinesArrow   :   
    this->qfeRenderStyleArrow(this->linePositionsDynVBO, this->lineAttributesDynVBO, lineCount, lineSize);
    break;
  default :
    this->qfeRenderStyleLine(this->linePositionsDynVBO, this->lineAttributesDynVBO, lineCount, lineSize);  
    break;
  } 
   
  /*
  glBindBuffer(GL_ARRAY_BUFFER, this->linePositionsDynVBO);
  glVertexPointer(4, GL_FLOAT, 0, 0);

  glEnableClientState(GL_VERTEX_ARRAY);

  glDrawArrays(GL_POINTS, 0, this->seedsCount);

  glDisableClientState(GL_VERTEX_ARRAY);
  glBindBuffer(GL_ARRAY_BUFFER, 0); 
  */

  glDisable(GL_BLEND);

  // Unbind textures
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeInitTraceStatic()
{
  float   traceDuration;  
  int     tracePhases;

  // For this visual, we fix the number of samples
  // The odd number represents one mid-point, and 28 forward / backward samples along the line trace
  // Also, we use only one iteration

  tracePhases              = 2*3;
  traceDuration            = tracePhases*this->phaseDuration;    
  
  this->traceSamples       = 57;//29;
  this->traceStepDuration  = traceDuration / (float)this->traceSteps;
  this->traceStepSize      = tracePhases   / (float)this->traceSteps;

  this->traceIterations    = 1; 
  this->phaseCycleCount    = (int)((this->phaseStart + tracePhases - 1) / (float)this->volumeSeries.size());  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeInitTraceDynamic()
{
  float   traceDuration;  
  int     tracePhases;

  // For this visual, we fix the number of samples
  // The odd number represents one mid-point, and 8 forward / backward samples along the line trace
  // Also, we use only one iteration

  tracePhases              = 2*2;
  traceDuration            = tracePhases*this->phaseDuration;    
  
  this->traceSamples       = 29;
  this->traceStepDuration  = traceDuration / this->traceSteps;
  this->traceStepSize      = this->tracePhasesDynamic / (float)this->traceSteps;

  this->traceIterations    = 1; 
  this->phaseCycleCount    = (int)((this->phaseStart + tracePhases - 1) / (float)this->volumeSeries.size());  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeInitSeedBuffer(qfeSeeds *seeds)
{
  int   vertexSize;
  int   dataSize;  

  if((int)seeds->size() <= 0) return qfeError;

  this->qfeClearSeedBuffer();
  this->seedPositions.clear();
  this->seedsCount = 0;
  
  for(int i=0; i<(int)seeds->size(); i++)
  {
    qfePoint seed;

    // Convert from voxel to patient coordinates
    seed.qfeSetPointElements((*seeds)[i].x, (*seeds)[i].y, (*seeds)[i].z);
    seed = seed * this->V2P;
    
    // Add the current seed
    this->seedPositions.push_back(seed.x);
    this->seedPositions.push_back(seed.y);
    this->seedPositions.push_back(seed.z);
    this->seedPositions.push_back((*seeds)[i].w);

    this->seedsCount++;    
  }

  vertexSize = 4 * sizeof(GLfloat);
  dataSize   = (int)this->seedsCount * vertexSize;

  glGenBuffers(1, &this->seedPositionsVBO);    

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->seedPositionsVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &this->seedPositions[0], GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeInitSeedBuffer(qfeSeeds *seeds, float phase)
{
  int   vertexSize;
  int   dataSize;  

  if((int)seeds->size() <= 0) return qfeError;

  this->qfeClearSeedBuffer();
  this->seedPositions.clear();
  this->seedsCount = 0;
  
  for(int i=0; i<(int)seeds->size(); i++)
  {
    if((int)((*seeds)[i].w + 0.5) == (int)phase) // TODO closest time moment here
    {
      qfePoint seed;

      // Convert from voxel to patient coordinates
      seed.qfeSetPointElements((*seeds)[i].x, (*seeds)[i].y, (*seeds)[i].z);
      seed = seed * this->V2P;

      // Add the current seed
      this->seedPositions.push_back(seed.x);
      this->seedPositions.push_back(seed.y);
      this->seedPositions.push_back(seed.z);
      this->seedPositions.push_back((*seeds)[i].w);

      this->seedsCount++;
    }
  }

  vertexSize = 4 * sizeof(GLfloat);
  dataSize   = (int)this->seedsCount * vertexSize;

  glGenBuffers(1, &this->seedPositionsVBO);    

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->seedPositionsVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &this->seedPositions[0], GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeClearSeedBuffer()
{
  if(glIsBuffer(this->seedPositionsVBO))
    glDeleteBuffers(1, &this->seedPositionsVBO);    

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeBindTexturesCluster(qfeVolumeSeries clusterLabels, int phase)
{
  GLuint tid = 0;
  
  if((int)clusterLabels.size() <= 0) return qfeError;

  if(phase >= 0 && phase < (int)clusterLabels.size())
    clusterLabels[phase]->qfeGetVolumeTextureId(tid);
  else
    clusterLabels[0]->qfeGetVolumeTextureId(tid);

    glActiveTexture(GL_TEXTURE10);
    glBindTexture(GL_TEXTURE_3D, tid);

    glActiveTexture(GL_TEXTURE11);
    glBindTexture(GL_TEXTURE_3D, tid);

    glActiveTexture(GL_TEXTURE12);
    glBindTexture(GL_TEXTURE_3D, tid);

    glActiveTexture(GL_TEXTURE13);
    glBindTexture(GL_TEXTURE_3D, tid);

    glActiveTexture(GL_TEXTURE14);
    glBindTexture(GL_TEXTURE_3D, tid);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeBindTexturesClusterSliding(qfeVolumeSeries clusterLabels, int phase)
{
  GLuint        tids[5];
  int           phasesCount;

  phasesCount = (int)clusterLabels.size();

  if(phasesCount > 0)
  {
    // We take a range [phase-2, phase+2] datasets, surrounding the current phase
    int index;
    for(int i=-2; i<=2; i++)
    {
      if(phasesCount == 1)
        index = 0;
      else
      {
        index = phase+i;
        qfeMath::qfeMod(index, phasesCount, index);
      }
      
      clusterLabels[index]->qfeGetVolumeTextureId(tids[i+2]);
    }    

    glActiveTexture(GL_TEXTURE10);
    glBindTexture(GL_TEXTURE_3D, tids[0]);

    glActiveTexture(GL_TEXTURE11);
    glBindTexture(GL_TEXTURE_3D, tids[1]);

    glActiveTexture(GL_TEXTURE12);
    glBindTexture(GL_TEXTURE_3D, tids[2]);

    glActiveTexture(GL_TEXTURE13);
    glBindTexture(GL_TEXTURE_3D, tids[3]);

    glActiveTexture(GL_TEXTURE14);
    glBindTexture(GL_TEXTURE_3D, tids[4]);
  }

  return qfeSuccess;

}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeRenderStyleLine(int currentBuffer, int attribBuffer, int lineCount, int lineSize)
{
  this->shaderProgramRenderLine->qfeGetAttribLocation("qfe_Position",  this->locationPosition);
  this->shaderProgramRenderLine->qfeGetAttribLocation("qfe_Attribute", this->locationAttribute);

  this->shaderProgramRenderLine->qfeEnable();  
  
  this->qfeDrawLines(currentBuffer, attribBuffer, lineCount, lineSize);  
  
  this->shaderProgramRenderLine->qfeDisable();  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeRenderStyleTube(int currentBuffer, int attribBuffer, int lineCount, int lineSize)
{
  this->shaderProgramRenderImposter->qfeGetAttribLocation("qfe_Position",  this->locationPosition);
  this->shaderProgramRenderImposter->qfeGetAttribLocation("qfe_Attribute", this->locationAttribute);

  this->shaderProgramRenderImposter->qfeEnable();  
  
  this->qfeDrawLines(currentBuffer, attribBuffer, lineCount, lineSize);  
  
  this->shaderProgramRenderImposter->qfeDisable();  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeRenderStyleArrow(int currentBuffer, int attribBuffer, int lineCount, int lineSize)
{
  this->shaderProgramRenderImposter->qfeGetAttribLocation("qfe_Position",  this->locationPosition);
  this->shaderProgramRenderImposter->qfeGetAttribLocation("qfe_Attribute", this->locationAttribute);

  // Render the tuboids
  this->shaderProgramRenderImposter->qfeEnable();  
  
  this->qfeDrawLines(currentBuffer, attribBuffer, lineCount, lineSize);  
  
  this->shaderProgramRenderImposter->qfeDisable();  

  // Render the arrow heads
  this->shaderProgramRenderArrowEnds->qfeEnable();  

  this->qfeDrawLines(currentBuffer, attribBuffer, lineCount, lineSize);  

  this->shaderProgramRenderArrowEnds->qfeDisable();  

  return qfeSuccess;
}

/*
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeDrawLines(GLuint lines, GLuint attribs, int lineCount, int lineSize)
{
  if(!glIsBuffer(lines)) return qfeError;

  GLfloat vertexSize = 4*sizeof(GLfloat);
  GLfloat stride = (2+3)*vertexSize;
  
  glBindBuffer(GL_ARRAY_BUFFER, lines);
  glEnableVertexAttribArray(this->locationPosition);
  glVertexAttribPointer(this->locationPosition,  4, GL_FLOAT, GL_FALSE,  0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, attribs);
  glEnableVertexAttribArray(this->locationAttribute);
  glVertexAttribPointer(this->locationAttribute,  4, GL_FLOAT, GL_FALSE, 0, 0);

  glDrawArrays(GL_LINE_STRIP_ADJACENCY_ARB, 0, (3+2)*lineCount);
  
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDisableVertexAttribArray(this->locationPosition);
  glDisableVertexAttribArray(this->locationAttribute);  

  return qfeSuccess;
}
*/

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeFetchVelocityLinear(qfeVolume *vol, qfePoint p, qfeVector &v)
{
  unsigned int  dims[3];
  qfeValueType  type;
  void         *data;
  qfeFloat      xd, yd, zd;
  qfePoint      p1;
  qfeVector     i1, i2, i1l, i1r, i2l, i2r;
  qfeVector     j1, j2, j1l, j1r, j2l, j2r;
  qfeVector     w1, w2;
  int           index[2];

  // Fetch the volume data
  vol->qfeGetVolumeData(type, dims[0], dims[1], dims[2], &data);

  // Make sure the boundaries conditions are ok
  (p.x     > 0.0)            ? p1.x = p.x   : p1.x = 1;        
  (p.y     > 0.0)            ? p1.y = p.y   : p1.y = 1;
  (p.z     > 0.0)            ? p1.z = p.z   : p1.z = 1;
  (p.x+1.0 < (float)dims[0]) ? p1.x = p1.x   : p1.x = (int)dims[0]-2;        
  (p.y+1.0 < (float)dims[1]) ? p1.y = p1.y   : p1.y = (int)dims[1]-2;
  (p.z+1.0 < (float)dims[2]) ? p1.z = p1.z   : p1.z = (int)dims[2]-2;

  // Get the lattice point
  xd = p1.x - floor(p1.x);
  yd = p1.y - floor(p1.y);
  zd = p1.z - floor(p1.z);

  // Compute along z
  index[0] = floor(p1.x)  + (floor(p1.y) *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = floor(p1.x)  + (floor(p1.y) *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  i1l.x = *((float*)data + 3*index[0]+0);
  i1l.y = *((float*)data + 3*index[0]+1);
  i1l.z = *((float*)data + 3*index[0]+2);
  i1r.x = *((float*)data + 3*index[1]+0);
  i1r.y = *((float*)data + 3*index[1]+1);
  i1r.z = *((float*)data + 3*index[1]+2);
  i1.x = i1l.x*(1.0-zd)  + i1r.x*zd;
  i1.y = i1l.y*(1.0-zd)  + i1r.y*zd;
  i1.z = i1l.z*(1.0-zd)  + i1r.z*zd;

  index[0] = floor(p1.x)  + (ceil(p1.y)  *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = floor(p1.x)  + (ceil(p1.y)  *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  i2l.x = *((float*)data + 3*index[0]+0);
  i2l.y = *((float*)data + 3*index[0]+1);
  i2l.z = *((float*)data + 3*index[0]+2);
  i2r.x = *((float*)data + 3*index[1]+0);
  i2r.y = *((float*)data + 3*index[1]+1);
  i2r.z = *((float*)data + 3*index[1]+2);
  i2.x = i2l.x*(1.0-zd)  + i2r.x*zd;
  i2.y = i2l.y*(1.0-zd)  + i2r.y*zd;
  i2.z = i2l.z*(1.0-zd)  + i2r.z*zd;

  index[0] = ceil(p1.x)   + (floor(p1.y) *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = ceil(p1.x)   + (floor(p1.y) *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  j1l.x = *((float*)data + 3*index[0]+0);
  j1l.y = *((float*)data + 3*index[0]+1);
  j1l.z = *((float*)data + 3*index[0]+2);
  j1r.x = *((float*)data + 3*index[1]+0);
  j1r.y = *((float*)data + 3*index[1]+1);
  j1r.z = *((float*)data + 3*index[1]+2);
  j1.x = j1l.x*(1.0-zd)  + j1r.x*zd;
  j1.y = j1l.y*(1.0-zd)  + j1r.y*zd;
  j1.z = j1l.z*(1.0-zd)  + j1r.z*zd;

  index[0] = ceil(p1.x)   + (ceil(p1.y)  *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = ceil(p1.x)   + (ceil(p1.y)  *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  j2l.x = *((float*)data + 3*index[0]+0);
  j2l.y = *((float*)data + 3*index[0]+1);
  j2l.z = *((float*)data + 3*index[0]+2);
  j2r.x = *((float*)data + 3*index[1]+0);
  j2r.y = *((float*)data + 3*index[1]+1);
  j2r.z = *((float*)data + 3*index[1]+2);
  j2.x = j2l.x*(1.0-zd)  + j2r.x*zd;
  j2.y = j2l.y*(1.0-zd)  + j2r.y*zd;
  j2.z = j2l.z*(1.0-zd)  + j2r.z*zd;

  // compute along y
  w1.x = i1.x*(1.0-yd) + i2.x*yd;
  w1.y = i1.y*(1.0-yd) + i2.y*yd;
  w1.z = i1.z*(1.0-yd) + i2.z*yd;

  w2.x = j1.x*(1.0-yd) + j2.x*yd;
  w2.y = j1.y*(1.0-yd) + j2.y*yd;
  w2.z = j1.z*(1.0-yd) + j2.z*yd;

  // compute along z
  v.x = w1.x*(1.0-xd) + w2.x*xd;
  v.y = w1.y*(1.0-xd) + w2.y*xd;
  v.z = w1.z*(1.0-xd) + w2.z*xd;
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// p in voxel coordinates
qfeReturnStatus qfeFlowLineTraceCluster::qfeFetchVelocityLinearTemporalLinear(qfeVolumeSeries series, qfePoint p, qfeFloat t, qfeVector &v)
{  
  float     ti, time;
  int       tl, tr;
  qfeVector vl, vr;  

  tl = floor(t);
  tr = tl + 1;
  ti = t - float(tl);

  // Check the boundary conditions and make cyclic
  if(tl < 0) tl = (int)series.size() - abs(tl);
  tl = tl % series.size();
  if(tr < 0) tr = (int)series.size() - abs(tr);
  tr = tr % series.size();
  
  // Fetch the velocity vectors
  qfeFetchVelocityLinear(series[tl], p, vl);
  qfeFetchVelocityLinear(series[tr], p, vr);

  // Correct the current time  
  time = tl + ti;

  // Compute the linear interpolation
  v.x = vl.x*(1.0-ti) + vr.x*ti;
  v.y = vl.y*(1.0-ti) + vr.y*ti;
  v.z = vl.z*(1.0-ti) + vr.z*ti;
  v.w = time;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// p in voxel coordinates
qfeReturnStatus qfeFlowLineTraceCluster::qfeFetchClusterLabelTemporalNearest(qfeVolumeSeries clusterLabels, qfePoint p, qfeFloat t, qfeFloat &label)
{  
  qfePoint      p1;
  unsigned int  dims[3];
  qfeValueType  type;
  void         *data;
  int           index;
  int           time;
  
  time = int(t+0.5f);
  
  // Check the boundary conditions and make cyclic
  if(time < 0) time = (int)clusterLabels.size() - abs(time);
  time = time % clusterLabels.size();

  // Fetch the volume data
  clusterLabels[time]->qfeGetVolumeData(type, dims[0], dims[1], dims[2], &data);

  // Make sure the boundaries conditions are ok
  (p.x   > 0)            ? p1.x = p.x   : p1.x = 1;        
  (p.y   > 0)            ? p1.y = p.y   : p1.y = 1;
  (p.z   > 0)            ? p1.z = p.z   : p1.z = 1;
  (p.x+1 < (int)dims[0]) ? p1.x = p.x   : p1.x = (int)dims[0]-2;        
  (p.y+1 < (int)dims[1]) ? p1.y = p.y   : p1.y = (int)dims[1]-2;
  (p.z+1 < (int)dims[2]) ? p1.z = p.z   : p1.z = (int)dims[2]-2;

  // Select the nearest neighbor spatially as well
  p1.x = int(p1.x+0.5f);
  p1.y = int(p1.y+0.5f);
  p1.z = int(p1.z+0.5f);
  
  // Fetch the label
  index = p1.x + (p1.y *dims[0]) + (p1.z *dims[0]*dims[1]);  
  
  // Return the nearest label
  label = *((float*)data + index);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeIntegrateRungeKutta4(qfeVolumeSeries series, qfePoint seed, float stepSize, float stepDuration, int stepDirection, int stepModulation, qfeMatrix4f p2v, qfePoint &pos)
{
  qfePoint  p, p1, p2, p3, s;
  qfeVector vi, vi_1, vi_2, vi_3; 
  qfeVector k1, k2, k3, k4;
  qfeFloat  t, dt, m, sd;  
  
  // Set the trace direction
  if(stepDirection >= 0) sd =  1.0f;
  else                   sd = -1.0f;
    
  // Determine initial time
  s.qfeSetPointElements(seed.x, seed.y, seed.z);
  t = seed.w;

  // Compute time step in seconds  
  dt = stepDuration / 1000.0f;  

  // Compute speed modulation
  m = stepModulation / 100.0f; 

  // Get the velocity in patient coordinates (cm/s)
  p = s*p2v;  
  p.qfeSetPointElements(p.x, p.y, p.z, t);  
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi);
  vi = sd * m * vi;  
 
  k1     = 10.0*vi*dt;  
  p1     = s + 0.5*k1;
  p1.w   = t + 0.5*stepSize;
  
  p = p1*p2v;  
  p.qfeSetPointElements(p.x, p.y, p.z, p1.w);  
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi_1);
  vi_1 = sd * m * vi_1;  

  k2     = 10.0*vi_1*dt;
  p2     = s + 0.5*k2;
  p2.w   = t + 0.5*stepSize;
  
  p = p2*p2v;  
  p.qfeSetPointElements(p.x, p.y, p.z, p2.w);  
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi_2);  
  vi_2 = sd * m * vi_2;  

  k3     = 10.0*vi_2*dt;
  p3     = s + k3;
  p3.w   = t + stepSize;

  p = p3*p2v;  
  p.qfeSetPointElements(p.x, p.y, p.z, p3.w);  
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi_3);  
  vi_3 = sd * m * vi_3;  
  
  k4     = 10.0*vi_3*dt;

  // New position in patient coordinates (mm)
  s  = s + (k1 + k2*2.0 + k3*2.0 + k4)*(1.0/6.0);
  t  = t + sd*stepSize;
 
  pos.qfeSetPointElements(s.x, s.y, s.z, t);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeInitLineBufferStatic(vector<float> *vertices, vector<float> *attribs)
{
  int dataSize;

  this->qfeClearLineBufferStatic();

  dataSize = vertices->size() * sizeof(GLfloat);

  glGenBuffers(1, &this->linePositionsStatVBO);  
  glGenBuffers(1, &this->lineAttributesStatVBO);  

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->linePositionsStatVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &((*vertices)[0]), GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindBuffer(GL_ARRAY_BUFFER, this->lineAttributesStatVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &((*attribs)[0]), GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  //cout << "line position  : "  << this->linePositionsStatVBO << endl;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeSetStaticUniformLocations()
{
  qfeFlowLineTrace::qfeSetStaticUniformLocations();

  this->shaderProgramGenerate->qfeSetUniform1i("volumeClusters0", 10);
  this->shaderProgramGenerate->qfeSetUniform1i("volumeClusters1", 11);
  this->shaderProgramGenerate->qfeSetUniform1i("volumeClusters2", 12);
  this->shaderProgramGenerate->qfeSetUniform1i("volumeClusters3", 13);
  this->shaderProgramGenerate->qfeSetUniform1i("volumeClusters4", 14);
  
  this->shaderProgramRenderArrowEnds->qfeSetUniform1i("lineTransferFunction", 7); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceCluster::qfeSetDynamicUniformLocations()
{
  qfePoint  lightSpec;
  qfeVector lightDir;
  qfeFloat  vl, vu;
  GLfloat   patientVoxel[16];
  float     angle;
  int       vertexCount;
  int       totalCount;

  // Obtain actual lighting parameters
  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 10.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w); 
    this->light->qfeGetLightSourceDirection(lightDir);
  }

  // Obtain the dataset venc
  if((int)this->volumeSeries.size() > 0)     
  {
    this->volumeSeries.front()->qfeGetVolumeValueDomain(vl, vu);
  }
  else
  {
    vu = -200.0;
    vl = 200.0;
  }

  // Obtain the current P2V matrix
  qfeMatrix4f::qfeGetMatrixElements(this->P2V, patientVoxel);

  // Compute the current phase, including wrap-around cycles
  angle       = cos((float)this->traceMaxAngle * (float)(PI/180.0));
  vertexCount = (int)this->traceSamples;
  totalCount  = (int)(this->traceIterations * (this->traceSamples-2));

  qfeFlowLineTrace::qfeSetDynamicUniformLocations();

  this->shaderProgramRenderLine->qfeSetUniform1i("clusterMasking", this->paramClusterMasking);
  this->shaderProgramRenderLine->qfeSetUniform1f("lengthMasking",  this->paramLengthMasking);
  this->shaderProgramRenderLine->qfeSetUniform1i("colorType",      (int)this->paramColorType);  
  this->shaderProgramRenderLine->qfeSetUniform3f("colorUni",       this->paramColor.r, this->paramColor.g, this->paramColor.b);  
  this->shaderProgramRenderLine->qfeSetUniform1i("lineShading",    (int)this->lineShading); 
  this->shaderProgramRenderLine->qfeSetUniformMatrix4f("patientVoxelMatrix",        1, patientVoxel);
  this->shaderProgramRenderLine->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramRenderLine->qfeSetUniformMatrix4f("matrixModelView",           1, this->matrixModelView);
  this->shaderProgramRenderLine->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);

  this->shaderProgramRenderImposter->qfeSetUniform1i("colorType",      this->paramColorType);  
  this->shaderProgramRenderImposter->qfeSetUniform3f("colorUni",       this->paramColor.r, this->paramColor.g, this->paramColor.b);  
  this->shaderProgramRenderImposter->qfeSetUniform1i("clusterMasking", this->paramClusterMasking);
  this->shaderProgramRenderImposter->qfeSetUniform1f("lengthMasking",  this->paramLengthMasking);
  this->shaderProgramRenderImposter->qfeSetUniform1i("phaseCount",     (int)this->volumeSeries.size()); 
  this->shaderProgramRenderImposter->qfeSetUniform1f("phaseCurrent",    this->phaseCurrent); 
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("patientVoxelMatrix" ,       1, patientVoxel);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixModelView",           1, this->matrixModelView);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixProjection",          1, this->matrixProjection);
 

  this->shaderProgramRenderArrowEnds->qfeSetUniform1i("colorScale",              this->colorScale);    
  this->shaderProgramRenderArrowEnds->qfeSetUniform1i("colorType",               (int)this->paramColorType);  
  this->shaderProgramRenderArrowEnds->qfeSetUniform3f("colorUni",                this->paramColor.r, this->paramColor.g, this->paramColor.b);  
  this->shaderProgramRenderArrowEnds->qfeSetUniform1i("lineShading",             (int)this->lineShading); 
  this->shaderProgramRenderArrowEnds->qfeSetUniform1f("lineWidth",               this->lineWidth);      
  this->shaderProgramRenderArrowEnds->qfeSetUniform1f("phaseCurrent",            this->phaseCurrent);    
  this->shaderProgramRenderArrowEnds->qfeSetUniform1i("phaseCount",              (int)this->volumeSeries.size());    
  this->shaderProgramRenderArrowEnds->qfeSetUniform1i("vertexCount",             vertexCount);   
  this->shaderProgramRenderArrowEnds->qfeSetUniform1i("totalCount",              totalCount);   
  this->shaderProgramRenderArrowEnds->qfeSetUniform4f("light",                   lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderArrowEnds->qfeSetUniform4f("lightDirection",          lightDir.x, lightDir.y, lightDir.z, lightDir.w);  
  this->shaderProgramRenderArrowEnds->qfeSetUniform2f("dataRange",               vu, vl);      
  this->shaderProgramRenderArrowEnds->qfeSetUniform3f("volumeDimensions",        (GLfloat)this->volumeSize[0], (GLfloat)this->volumeSize[1], (GLfloat)this->volumeSize[2]);  
  this->shaderProgramRenderArrowEnds->qfeSetUniform1i("clusterMasking",          this->paramClusterMasking);
  this->shaderProgramRenderArrowEnds->qfeSetUniform1f("lengthMasking",           this->paramLengthMasking);
  this->shaderProgramRenderArrowEnds->qfeSetUniformMatrix4f("patientVoxelMatrix" , 1, patientVoxel);
  this->shaderProgramRenderArrowEnds->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramRenderArrowEnds->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);

  return qfeSuccess;
}


