#include "qfe2DBasicRendering.h"

//----------------------------------------------------------------------------
qfe2DBasicRendering::qfe2DBasicRendering(vtkRenderer *renderer_)
{
	this->renderer = renderer_;
	font_size = 12;
};


//----------------------------------------------------------------------------
qfe2DBasicRendering::~qfe2DBasicRendering()
{
};

/* Renders "text" at position x,y on the screen */
void qfe2DBasicRendering::qfeAdd2DText(unsigned int x, unsigned int y, std::string text)
{
	vtkSmartPointer<vtkTextProperty> textProperty = 
		vtkSmartPointer<vtkTextProperty>::New();

	textProperty->BoldOff();
	textProperty->SetFontFamilyToTimes();
	textProperty->SetFontSize(font_size);
	textProperty->SetJustificationToCentered();

	// Setup the text and add it to the window
	vtkSmartPointer<vtkTextActor> textActor = 
		vtkSmartPointer<vtkTextActor>::New();
	textActor->SetDisplayPosition(x,y);
	textActor->SetTextProperty(textProperty);
	renderer->AddActor2D(textActor);
	textActor->SetInput(text.c_str());
	textActor->GetTextProperty()->SetColor(0.0,0.0,0.0);

	renderedText.push_back((vtkProp*) textActor);
}

void qfe2DBasicRendering::qfeAdd2DText(unsigned int x, unsigned int y, int text)
{
	this->qfeAdd2DText(x,y,to_string_with_precision(text));
}

void qfe2DBasicRendering::qfeAdd2DText(unsigned int x, unsigned int y, float text)
{
	this->qfeAdd2DText(x,y,to_string_with_precision(text));
}

void qfe2DBasicRendering::qfeAdd2DText(unsigned int x, unsigned int y, double text)
{
	this->qfeAdd2DText(x,y,to_string_with_precision(text));
}

void qfe2DBasicRendering::qfeAdd2DText(unsigned int x, unsigned int y, std::ostringstream *text)
{
	this->qfeAdd2DText(x,y,text->str());
}

void qfe2DBasicRendering::qfeAdd2DPlot(unsigned int x, unsigned int y, double fraction_of_window, plot_data data)
{
	if(data.data.size() == 0)
		return;

	// Setup the text and add it to the window
	vtkSmartPointer<vtkXYPlotActor> plotActor = 
		vtkSmartPointer<vtkXYPlotActor>::New();
	plotActor->SetDisplayPosition(x,y);
	plotActor->SetWidth(fraction_of_window);
	plotActor->SetHeight(fraction_of_window);
	
	plotActor->ExchangeAxesOff();
	plotActor->SetLabelFormat( "%g" );
	if(data.x_label.size() != 0)
		plotActor->SetXTitle( data.x_label.c_str() );
	if(data.y_label.size() != 0)
		plotActor->SetYTitle( data.y_label.c_str() );
  
	vtkSmartPointer<vtkDoubleArray> array_s = 
		vtkSmartPointer<vtkDoubleArray>::New();
	vtkSmartPointer<vtkFieldData> field = 
		vtkSmartPointer<vtkFieldData>::New();
	vtkSmartPointer<vtkDataObject> dataObj = 
		vtkSmartPointer<vtkDataObject>::New();

	for(unsigned int b = 0; b < data.data.size(); b++)   /// Assuming an array of 30 elements
	{
		double val = data.data.at(b);
		array_s->InsertValue(b,val);
	}
	field->AddArray(array_s);
	dataObj->SetFieldData(field);

	plotActor->AddDataObjectInput(dataObj); 	

	plotActor->SetPlotColor(0,1,0,0);

	renderer->AddActor(plotActor);
}

void qfe2DBasicRendering::qfeAdd2DPlot(unsigned int x, unsigned int y, double fraction_of_window, std::vector<float> data)
{
	plot_data pdata;
	pdata.data = data;
	pdata.x_label = "";
	pdata.y_label = "";

	this->qfeAdd2DPlot(x,y,fraction_of_window,pdata);
}

  /* Removes all rendered text from the screen */
void qfe2DBasicRendering::qfeClear2DText()
{
	while(renderedText.size() != 0)
	{
		if(renderer->HasViewProp(renderedText.back()))
		{
			renderedText.back()->VisibilityOff();
		}

		if(renderedText.size()>0)
			renderedText.pop_back();
	}
}

unsigned int qfe2DBasicRendering::qfeGetFontSize()
{
	return font_size;
}



