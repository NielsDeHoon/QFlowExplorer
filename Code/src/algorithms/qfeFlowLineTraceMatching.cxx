#include "qfeFlowLineTraceMatching.h"

//----------------------------------------------------------------------------
qfeFlowLineTraceMatching::qfeFlowLineTraceMatching() : qfeFlowLineTrace()
{
  this->phasesPerIteration  = 2;
  this->paramPhaseSpan      = 50.0;
  this->paramPhaseVisible   = -1; // all phases
  this->paramArrowScale     = 150;
  this->paramColorType      = qfeFlowLinesColorPatternType;
  this->paramColor.r        = 0.8;
  this->paramColor.g        = 0.0;
  this->paramColor.b        = 0.0;

  this->shaderProgramRenderImposter = new qfeGLShaderProgram();
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/pmr/pmr-render-tube-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/pmr/pmr-render-tube-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramRenderImposter->qfeAddShaderFromFile("/shaders/pmr/pmr-render-tube-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramRenderImposter->qfeLink();

  this->shaderProgramEndCaps = new qfeGLShaderProgram();
  this->shaderProgramEndCaps->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramEndCaps->qfeAddShaderFromFile("/shaders/pmr/pmr-render-endcaps-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramEndCaps->qfeAddShaderFromFile("/shaders/pmr/pmr-render-endcaps-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramEndCaps->qfeAddShaderFromFile("/shaders/pmr/pmr-render-endcaps-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramEndCaps->qfeLink();

  this->shaderProgramArrows = new qfeGLShaderProgram();
  this->shaderProgramArrows->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramArrows->qfeAddShaderFromFile("/shaders/pmr/pmr-render-arrows-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramArrows->qfeAddShaderFromFile("/shaders/pmr/pmr-render-arrows-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramArrows->qfeAddShaderFromFile("/shaders/pmr/pmr-render-arrows-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramArrows->qfeLink();

  this->clusterLabelSeries.clear();
};

//----------------------------------------------------------------------------
qfeFlowLineTraceMatching::qfeFlowLineTraceMatching(const qfeFlowLineTraceMatching &fv) : qfeFlowLineTrace(fv)
{
  this->clusterLabelSeries                 = fv.clusterLabelSeries;
  this->paramColorType                     = fv.paramColorType;
  this->paramColor                         = fv.paramColor;
  this->paramArrowScale                    = fv.paramArrowScale;

  this->paramPhaseSpan                     = fv.paramPhaseSpan;
  this->paramPhaseVisible                  = fv.paramPhaseVisible;

  this->shaderProgramEndCaps               = fv.shaderProgramEndCaps;
  this->shaderProgramArrows                = fv.shaderProgramArrows;
};

//----------------------------------------------------------------------------
qfeFlowLineTraceMatching::~qfeFlowLineTraceMatching()
{
  delete this->shaderProgramEndCaps;
  delete this->shaderProgramArrows;
};

//----------------------------------------------------------------------------
// phase - current phase
qfeReturnStatus qfeFlowLineTraceMatching::qfeSetPhase(float phase)
{
  this->phaseCurrent  = phase;
  this->phaseGenerate = phase;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// phase span - the span of the lines that is visible
qfeReturnStatus qfeFlowLineTraceMatching::qfeSetPhaseSpan(float span)
{
  this->paramPhaseSpan  = span;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// phase visibility - set the phase that is visualized (-1 = all phases)
qfeReturnStatus qfeFlowLineTraceMatching::qfeSetPhaseVisible(int visible)
{
  this->paramPhaseVisible = visible;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// arrow scale in percentage with respect to the line width
qfeReturnStatus qfeFlowLineTraceMatching::qfeSetArrowScale(int percentage)
{
  this->paramArrowScale= percentage;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeSetColorType(qfeFlowLinesColorType type)
{
  this->paramColorType = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeSetColor(qfeColorRGBA color)
{
  this->paramColor = color;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeGenerateSeeds(qfeVolumeSeries series, qfeSeeds *seeds)
{
  if(((int)series.size() <= 0)) return qfeError;

  qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, series.front());
  qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, series.front());

  // Set the initial particles in the buffer
  this->qfeInitSeedBuffer(seeds);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeGeneratePathLinesStatic(qfeVolumeSeries series, qfeSeeds *seeds, qfeSeeds *attribs)
{
  const int     traceSamples  = 97;
  const int     centerSample  = (traceSamples-1)/2;
  const float   delta         = 0.0001;

  qfeMatrix4f   V2P;
  int           steps;
  qfePoint      pos, posTexture, attrib, seed;
  qfeFloat      seedTime, currentTime, speed;
  qfeVector     velocity;
  qfePoint      vertexPositions[traceSamples];
  qfePoint      vertexAttribs[traceSamples];
  vector<float> lines;
  vector<float> attributes;

  if((int)series.size() <= 0) return qfeError;
  if((int)seeds->size() <= 0) return qfeError;

  // First time of generation, prepare some global variables
  if(this->firstGenerate)
  {
    this->volumeSeries       = series;

    this->volumeSeries.front()->qfeGetVolumeSize(this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);

    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, this->volumeSeries.front());
    qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, this->volumeSeries.front());

    this->qfeSetStaticUniformLocations();

    this->firstGenerate = false;
  }

  // Initialize
  qfeMatrix4f::qfeGetMatrixInverse(this->P2V, V2P);
  steps             = int(max(traceSamples-3, traceSteps)/float(traceSamples-3));
  this->seedsCount  = int(seeds->size());
  lines.clear();

  // Set the all the variables necessary to trace the lines
  this->qfeInitTraceStatic();

  // TODO adding the attribs per vertex is rather expensive as they are constant over the line.
  // glVertexAttribDivisor can deal with a smaller buffer for the same result.

  for(int sc=0 ; sc<(int)this->seedsCount; sc++)
  {
    // Get first seed
    seedTime = (*seeds)[sc].w;
    seed     = (*seeds)[sc];
    seed.w   = 1.0;

    // Set start position
    pos     = seed * V2P;
    pos.w   = seedTime;

    qfeFetchVelocityLinearTemporalLinear(series, seed, seedTime, velocity);
    qfeVector::qfeVectorLength(velocity, speed); 

    attrib.qfeSetPointElements(seedTime, speed, (*attribs)[sc].x,(*attribs)[sc].z);

    // Set the center sample
    vertexPositions[centerSample] = pos;
    vertexAttribs[centerSample]   = attrib;

    // -- Trace backward
    for(int i=1; i<(traceSamples-1)/2; i++)
    {
      qfePoint previousPos    = pos;
      qfeFloat lengthTmp      = 0.0;
      qfePoint posVoxel;

      for(int j=0; j<steps; j++)
      {
        qfePoint posTmp = pos;

        qfeIntegrateRungeKutta4(series, pos, this->traceStepSize, this->traceStepDuration, -1, 100, this->P2V, pos);
      }

      // -- Check conditions
      posTexture.qfeSetPointElements(pos.x, pos.y, pos.z, 1.0);
      posTexture    = posTexture * this->P2V;
      posTexture.x /= this->volumeSize[0];
      posTexture.y /= this->volumeSize[1];
      posTexture.z /= this->volumeSize[2];

      // check the bounding box condition
      if((posTexture.x <= 0.0 || posTexture.y <= 0.0 || posTexture.z <= 0.0) ||
         (posTexture.x >= 1.0 || posTexture.y >= 1.0 || posTexture.z >= 1.0))
         pos.qfeSetPointElements(previousPos.x, previousPos.y, previousPos.z);

      // compute voxel position
      currentTime   = pos.w;
      posVoxel      = pos;
      posVoxel.w    = 1.0;
      posVoxel      = posVoxel * this->P2V;

      qfeFetchVelocityLinearTemporalLinear(series, posVoxel, currentTime, velocity);
      qfeVector::qfeVectorLength(velocity, speed); 

      attrib.qfeSetPointElements(seedTime, speed, (*attribs)[sc].x,(*attribs)[sc].z);

      // add to the sample array
      vertexPositions[centerSample-i] = pos;
      vertexAttribs[centerSample-i]   = attrib;
    }

    // Fix adjacency information
    vertexPositions[0] = vertexPositions[1];
    vertexAttribs[0]   = vertexAttribs[1];

    // -- Trace forward
    pos = vertexPositions[centerSample];
    for(int i=1; i<(traceSamples-1)/2; i++)
    {
      qfePoint previousPos    = pos;
      qfeFloat lengthTmp      = 0.0;
      qfePoint posVoxel;

      for(int j=0; j<steps; j++)
      {
        qfePoint posTmp = pos;

        qfeIntegrateRungeKutta4(series, pos, this->traceStepSize, this->traceStepDuration, 1, 100, this->P2V, pos);        
      }

       // -- Check conditions
      posTexture.qfeSetPointElements(pos.x, pos.y, pos.z, 1.0);
      posTexture    = posTexture * this->P2V;
      posTexture.x /= this->volumeSize[0];
      posTexture.y /= this->volumeSize[1];
      posTexture.z /= this->volumeSize[2];

      // check the bounding box condition
      if((posTexture.x <= 0.0 || posTexture.y <= 0.0 || posTexture.z <= 0.0) ||
         (posTexture.x >= 1.0 || posTexture.y >= 1.0 || posTexture.z >= 1.0))
         pos.qfeSetPointElements(previousPos.x, previousPos.y, previousPos.z);

      // compute voxel position
      currentTime   = pos.w;
      posVoxel      = pos;
      posVoxel.w    = 1.0;
      posVoxel      = posVoxel * this->P2V;

      qfeFetchVelocityLinearTemporalLinear(series, posVoxel, currentTime, velocity);
      qfeVector::qfeVectorLength(velocity, speed); 

      attrib.qfeSetPointElements(seedTime, speed, (*attribs)[sc].x,(*attribs)[sc].z);

      // add to the sample array
      vertexPositions[centerSample+i] = pos;
      vertexAttribs[centerSample+i]   = attrib;
    }

    // Fix adjacency information
    vertexPositions[traceSamples-1] = vertexPositions[traceSamples-2];
    vertexAttribs[traceSamples-1]   = vertexAttribs[traceSamples-2];
 
    // Store the pathline
    for(int i=0; i<traceSamples; i++)
    {      
      lines.push_back(vertexPositions[i].x);
      lines.push_back(vertexPositions[i].y);
      lines.push_back(vertexPositions[i].z);
      lines.push_back(vertexPositions[i].w);

      attributes.push_back(vertexAttribs[i].x);
      attributes.push_back(vertexAttribs[i].y);
      attributes.push_back(vertexAttribs[i].z);
      attributes.push_back(vertexAttribs[i].w);
    }
  }

  // -- Put the lines and attributes into a buffer object

  // Set-up the vertex buffer objects (VBO)
  this->qfeInitLineBufferStatic(&lines, &attributes);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeRenderMatchingSeeds()
{
  // Check if we have the necessary buffers
  if(!glIsBuffer(this->seedPositionsVBO))      return qfeError;

  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glEnable(GL_POINT_SMOOTH);
  glPointSize(4.0);
  glColor3f(this->paramColor.r,this->paramColor.g,this->paramColor.b);

  glBindBuffer(GL_ARRAY_BUFFER, this->seedPositionsVBO);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0,  3, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);

  glDrawArrays(GL_POINTS, 0, (int)this->seedsCount);

  glDisableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeRenderMatchingPathlinesStatic(qfeColorMap *colormap)
{
  // First time of rendering, prepare some global variables
  if(this->firstRender)
  {
    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->colormap);

  // Check if the buffer is available
  if(!glIsBuffer(this->linePositionsStatVBO))  return qfeError;

  // Compute the amount of points to render
  GLsizei lineCount    = this->seedsCount;
  GLsizei lineSize     = this->traceSamples;

  // Obtain the modelview information
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Bind textures  
  this->qfeBindTexturesRender();

  // Allow blending for transparency
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocations();

  this->qfeRenderStyleTube(this->linePositionsStatVBO, this->lineAttributesStatVBO, lineCount, lineSize);
  
  glDisable(GL_BLEND);

  // Unbind textures
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeRenderMatchingPathlineArrows(qfeColorMap *colormap)
{ 
   // First time of rendering, prepare some global variables
  if(this->firstRender)
  {
    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->colormap);

  // Check if the buffer is available
  if(!glIsBuffer(this->linePositionsStatVBO))  return qfeError;

  // Compute the amount of points to render
  GLsizei lineCount    = this->seedsCount;
  GLsizei lineSize     = this->traceSamples;

  // Obtain the modelview information
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
 
  // Bind textures  
  this->qfeBindTexturesRender();

  // Allow blending for transparency
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocations();

  this->qfeRenderStyleArrow(this->linePositionsStatVBO, this->lineAttributesStatVBO, lineCount, lineSize);
  
  glDisable(GL_BLEND);

  // Unbind textures
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeInitTraceStatic()
{
  float   traceDuration;
  int     tracePhases;

  // For this visual, we fix the number of samples
  // The odd number represents one mid-point, and 28 forward / backward samples along the line trace
  // Also, we use only one iteration

  tracePhases              = 2*3;
  traceDuration            = tracePhases*this->phaseDuration;

  this->traceSamples       = 97;//57;//29;
  this->traceStepDuration  = traceDuration / (float)this->traceSteps;
  this->traceStepSize      = tracePhases   / (float)this->traceSteps;

  this->traceIterations    = 1;
  this->phaseCycleCount    = (int)((this->phaseStart + tracePhases - 1) / (float)this->volumeSeries.size());

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeInitSeedBuffer(qfeSeeds *seeds)
{
  int   vertexSize;
  int   dataSize;

  if((int)seeds->size() <= 0) return qfeError;

  this->qfeClearSeedBuffer();
  this->seedPositions.clear();
  this->seedsCount = 0;

  for(int i=0; i<(int)seeds->size(); i++)
  {
    qfePoint seed;

    // Convert from voxel to patient coordinates
    seed.qfeSetPointElements((*seeds)[i].x, (*seeds)[i].y, (*seeds)[i].z);
    seed = seed * this->V2P;

    // Add the current seed
    this->seedPositions.push_back(seed.x);
    this->seedPositions.push_back(seed.y);
    this->seedPositions.push_back(seed.z);
    this->seedPositions.push_back((*seeds)[i].w);

    this->seedsCount++;
  }

  vertexSize = 4 * sizeof(GLfloat);
  dataSize   = (int)this->seedsCount * vertexSize;

  glGenBuffers(1, &this->seedPositionsVBO);

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->seedPositionsVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &this->seedPositions[0], GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeClearSeedBuffer()
{
  if(glIsBuffer(this->seedPositionsVBO))
    glDeleteBuffers(1, &this->seedPositionsVBO);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeRenderStyleTube(int currentBuffer, int attribBuffer, int lineCount, int lineSize)
{
  this->shaderProgramRenderImposter->qfeGetAttribLocation("qfe_Position",  this->locationPosition);
  this->shaderProgramRenderImposter->qfeGetAttribLocation("qfe_Attribute", this->locationAttribute);

  this->shaderProgramRenderImposter->qfeEnable();

  this->qfeDrawLines(currentBuffer, attribBuffer, lineCount, lineSize);

  this->shaderProgramRenderImposter->qfeDisable();
  
  // Render the end caps
  this->shaderProgramEndCaps->qfeGetAttribLocation("qfe_Position",  this->locationPosition);
  this->shaderProgramEndCaps->qfeGetAttribLocation("qfe_Attribute", this->locationAttribute);

  this->shaderProgramEndCaps->qfeEnable();

  this->qfeDrawLines(currentBuffer, attribBuffer, lineCount, lineSize);

  this->shaderProgramEndCaps->qfeDisable();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeRenderStyleArrow(int currentBuffer, int attribBuffer, int lineCount, int lineSize)
{    
  this->shaderProgramArrows->qfeGetAttribLocation("qfe_Position",  this->locationPosition);
  this->shaderProgramArrows->qfeGetAttribLocation("qfe_Attribute", this->locationAttribute);

  this->shaderProgramArrows->qfeEnable();

  this->qfeDrawLines(currentBuffer, attribBuffer, lineCount, lineSize);

  this->shaderProgramArrows->qfeDisable();  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeFetchVelocityLinear(qfeVolume *vol, qfePoint p, qfeVector &v)
{
  unsigned int  dims[3];
  qfeValueType  type;
  void         *data;
  qfeFloat      xd, yd, zd;
  qfePoint      p1;
  qfeVector     i1, i2, i1l, i1r, i2l, i2r;
  qfeVector     j1, j2, j1l, j1r, j2l, j2r;
  qfeVector     w1, w2;
  int           index[2];

  // Fetch the volume data
  vol->qfeGetVolumeData(type, dims[0], dims[1], dims[2], &data);

  // Make sure the boundaries conditions are ok
  (p.x     > 0.0)            ? p1.x = p.x   : p1.x = 1;
  (p.y     > 0.0)            ? p1.y = p.y   : p1.y = 1;
  (p.z     > 0.0)            ? p1.z = p.z   : p1.z = 1;
  (p.x+1.0 < (float)dims[0]) ? p1.x = p1.x  : p1.x = (int)dims[0]-2;
  (p.y+1.0 < (float)dims[1]) ? p1.y = p1.y  : p1.y = (int)dims[1]-2;
  (p.z+1.0 < (float)dims[2]) ? p1.z = p1.z  : p1.z = (int)dims[2]-2;

  // Get the lattice point
  xd = p1.x - floor(p1.x);
  yd = p1.y - floor(p1.y);
  zd = p1.z - floor(p1.z);

  // Compute along z
  index[0] = floor(p1.x)  + (floor(p1.y) *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = floor(p1.x)  + (floor(p1.y) *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  i1l.x = *((float*)data + 3*index[0]+0);
  i1l.y = *((float*)data + 3*index[0]+1);
  i1l.z = *((float*)data + 3*index[0]+2);
  i1r.x = *((float*)data + 3*index[1]+0);
  i1r.y = *((float*)data + 3*index[1]+1);
  i1r.z = *((float*)data + 3*index[1]+2);
  i1.x = i1l.x*(1.0-zd)  + i1r.x*zd;
  i1.y = i1l.y*(1.0-zd)  + i1r.y*zd;
  i1.z = i1l.z*(1.0-zd)  + i1r.z*zd;

  index[0] = floor(p1.x)  + (ceil(p1.y)  *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = floor(p1.x)  + (ceil(p1.y)  *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  i2l.x = *((float*)data + 3*index[0]+0);
  i2l.y = *((float*)data + 3*index[0]+1);
  i2l.z = *((float*)data + 3*index[0]+2);
  i2r.x = *((float*)data + 3*index[1]+0);
  i2r.y = *((float*)data + 3*index[1]+1);
  i2r.z = *((float*)data + 3*index[1]+2);
  i2.x = i2l.x*(1.0-zd)  + i2r.x*zd;
  i2.y = i2l.y*(1.0-zd)  + i2r.y*zd;
  i2.z = i2l.z*(1.0-zd)  + i2r.z*zd;

  index[0] = ceil(p1.x)   + (floor(p1.y) *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = ceil(p1.x)   + (floor(p1.y) *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  j1l.x = *((float*)data + 3*index[0]+0);
  j1l.y = *((float*)data + 3*index[0]+1);
  j1l.z = *((float*)data + 3*index[0]+2);
  j1r.x = *((float*)data + 3*index[1]+0);
  j1r.y = *((float*)data + 3*index[1]+1);
  j1r.z = *((float*)data + 3*index[1]+2);
  j1.x = j1l.x*(1.0-zd)  + j1r.x*zd;
  j1.y = j1l.y*(1.0-zd)  + j1r.y*zd;
  j1.z = j1l.z*(1.0-zd)  + j1r.z*zd;

  index[0] = ceil(p1.x)   + (ceil(p1.y)  *dims[0]) + (floor(p1.z) *dims[0]*dims[1]);
  index[1] = ceil(p1.x)   + (ceil(p1.y)  *dims[0]) + (ceil(p1.z)  *dims[0]*dims[1]);
  j2l.x = *((float*)data + 3*index[0]+0);
  j2l.y = *((float*)data + 3*index[0]+1);
  j2l.z = *((float*)data + 3*index[0]+2);
  j2r.x = *((float*)data + 3*index[1]+0);
  j2r.y = *((float*)data + 3*index[1]+1);
  j2r.z = *((float*)data + 3*index[1]+2);
  j2.x = j2l.x*(1.0-zd)  + j2r.x*zd;
  j2.y = j2l.y*(1.0-zd)  + j2r.y*zd;
  j2.z = j2l.z*(1.0-zd)  + j2r.z*zd;

  // compute along y
  w1.x = i1.x*(1.0-yd) + i2.x*yd;
  w1.y = i1.y*(1.0-yd) + i2.y*yd;
  w1.z = i1.z*(1.0-yd) + i2.z*yd;

  w2.x = j1.x*(1.0-yd) + j2.x*yd;
  w2.y = j1.y*(1.0-yd) + j2.y*yd;
  w2.z = j1.z*(1.0-yd) + j2.z*yd;

  // compute along z
  v.x = w1.x*(1.0-xd) + w2.x*xd;
  v.y = w1.y*(1.0-xd) + w2.y*xd;
  v.z = w1.z*(1.0-xd) + w2.z*xd;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// p in voxel coordinates
qfeReturnStatus qfeFlowLineTraceMatching::qfeFetchVelocityLinearTemporalLinear(qfeVolumeSeries series, qfePoint p, qfeFloat t, qfeVector &v)
{
  float     ti, time;
  int       tl, tr;
  qfeVector vl, vr;

  tl = floor(t);
  tr = tl + 1;
  ti = t - float(tl);

  // Check the boundary conditions and make cyclic
  if(tl < 0) tl = (int)series.size() - abs(tl);
  tl = tl % series.size();
  if(tr < 0) tr = (int)series.size() - abs(tr);
  tr = tr % series.size();

  // Fetch the velocity vectors
  qfeFetchVelocityLinear(series[tl], p, vl);
  qfeFetchVelocityLinear(series[tr], p, vr);

  // Correct the current time
  time = tl + ti;

  // Compute the linear interpolation
  v.x = vl.x*(1.0-ti) + vr.x*ti;
  v.y = vl.y*(1.0-ti) + vr.y*ti;
  v.z = vl.z*(1.0-ti) + vr.z*ti;
  v.w = time;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeIntegrateRungeKutta4(qfeVolumeSeries series, qfePoint seed, float stepSize, float stepDuration, int stepDirection, int stepModulation, qfeMatrix4f p2v, qfePoint &pos)
{
  qfePoint  p, p1, p2, p3, s;
  qfeVector vi, vi_1, vi_2, vi_3;
  qfeVector k1, k2, k3, k4;
  qfeFloat  t, dt, m, sd;

  // Set the trace direction
  if(stepDirection >= 0) sd =  1.0f;
  else                   sd = -1.0f;

  // Determine initial time
  s.qfeSetPointElements(seed.x, seed.y, seed.z);
  t = seed.w;

  // Compute time step in seconds
  dt = stepDuration / 1000.0f;

  // Compute speed modulation
  m = stepModulation / 100.0f;

  // Get the velocity in patient coordinates (cm/s)
  p = s*p2v;
  p.qfeSetPointElements(p.x, p.y, p.z, t);
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi);
  vi = sd * m * vi;

  k1     = 10.0*vi*dt;
  p1     = s + 0.5*k1;
  p1.w   = t + 0.5*stepSize;

  p = p1*p2v;
  p.qfeSetPointElements(p.x, p.y, p.z, p1.w);
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi_1);
  vi_1 = sd * m * vi_1;

  k2     = 10.0*vi_1*dt;
  p2     = s + 0.5*k2;
  p2.w   = t + 0.5*stepSize;

  p = p2*p2v;
  p.qfeSetPointElements(p.x, p.y, p.z, p2.w);
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi_2);
  vi_2 = sd * m * vi_2;

  k3     = 10.0*vi_2*dt;
  p3     = s + k3;
  p3.w   = t + stepSize;

  p = p3*p2v;
  p.qfeSetPointElements(p.x, p.y, p.z, p3.w);
  qfeFetchVelocityLinearTemporalLinear(series,p,p.w,vi_3);
  vi_3 = sd * m * vi_3;

  k4     = 10.0*vi_3*dt;

  // New position in patient coordinates (mm)
  s  = s + (k1 + k2*2.0 + k3*2.0 + k4)*(1.0/6.0);
  t  = t + sd*stepSize;

  pos.qfeSetPointElements(s.x, s.y, s.z, t);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeInitLineBufferStatic(vector<float> *vertices, vector<float> *attribs)
{
  int dataSize;

  this->qfeClearLineBufferStatic();

  dataSize = vertices->size() * sizeof(GLfloat);

  glGenBuffers(1, &this->linePositionsStatVBO);
  glGenBuffers(1, &this->lineAttributesStatVBO);

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->linePositionsStatVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &((*vertices)[0]), GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindBuffer(GL_ARRAY_BUFFER, this->lineAttributesStatVBO);
  glBufferData(GL_ARRAY_BUFFER, dataSize, &((*attribs)[0]), GL_STREAM_COPY);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  //cout << "line position  : "  << this->linePositionsStatVBO << endl;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeSetStaticUniformLocations()
{
  qfeFlowLineTrace::qfeSetStaticUniformLocations();

  this->shaderProgramEndCaps->qfeSetUniform1i("lineTransferFunction", 7);
  this->shaderProgramArrows->qfeSetUniform1i("lineTransferFunction", 7);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowLineTraceMatching::qfeSetDynamicUniformLocations()
{
  qfePoint  lightSpec;
  qfeVector lightDir;
  qfeFloat  vl, vu;
  GLfloat   patientVoxel[16];
  float     angle;
  int       vertexCount;
  int       totalCount;

  // Obtain actual lighting parameters
  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 10.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w);
    this->light->qfeGetLightSourceDirection(lightDir);
  }

  // Obtain the dataset venc
  if((int)this->volumeSeries.size() > 0)
  {
    this->volumeSeries.front()->qfeGetVolumeValueDomain(vl, vu);
  }
  else
  {
    vu = -200.0;
    vl = 200.0;
  }

  // Obtain the current P2V matrix
  qfeMatrix4f::qfeGetMatrixElements(this->P2V, patientVoxel);

  // Compute the current phase, including wrap-around cycles
  angle       = cos((float)this->traceMaxAngle * (float)(PI/180.0));
  vertexCount = (int)this->traceSamples;
  totalCount  = (int)(this->traceIterations * (this->traceSamples-2));

  qfeFlowLineTrace::qfeSetDynamicUniformLocations();

  this->shaderProgramRenderImposter->qfeSetUniform1i("colorType",      this->paramColorType);
  this->shaderProgramRenderImposter->qfeSetUniform3f("colorUni",       this->paramColor.r, this->paramColor.g, this->paramColor.b);  
  this->shaderProgramRenderImposter->qfeSetUniform1f("phaseSpan",      this->paramPhaseSpan/this->phaseDuration);
  this->shaderProgramRenderImposter->qfeSetUniform1i("phaseVisible",   this->paramPhaseVisible);
  this->shaderProgramRenderImposter->qfeSetUniform1f("lineWidth",      this->lineWidth);  
  this->shaderProgramRenderImposter->qfeSetUniform1i("lineShading",    (int)this->lineShading);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("patientVoxelMatrix" ,       1, patientVoxel);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixModelView",           1, this->matrixModelView);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);
  this->shaderProgramRenderImposter->qfeSetUniformMatrix4f("matrixProjection",          1, this->matrixProjection);
   
  this->shaderProgramEndCaps->qfeSetUniform1i("colorType",               (int)this->paramColorType);
  this->shaderProgramEndCaps->qfeSetUniform3f("colorUni",                this->paramColor.r, this->paramColor.g, this->paramColor.b);
  this->shaderProgramEndCaps->qfeSetUniform1i("lineShading",             (int)this->lineShading);
  this->shaderProgramEndCaps->qfeSetUniform1f("lineWidth",               this->lineWidth);
  this->shaderProgramEndCaps->qfeSetUniform1f("phaseSpan",               this->paramPhaseSpan/this->phaseDuration); 
  this->shaderProgramEndCaps->qfeSetUniform1i("phaseVisible",            this->paramPhaseVisible);
  this->shaderProgramEndCaps->qfeSetUniform4f("light",                   lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramEndCaps->qfeSetUniform4f("lightDirection",          lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramEndCaps->qfeSetUniform2f("dataRange",               vu, vl);
  this->shaderProgramEndCaps->qfeSetUniform3f("volumeDimensions",        (GLfloat)this->volumeSize[0], (GLfloat)this->volumeSize[1], (GLfloat)this->volumeSize[2]);
  this->shaderProgramEndCaps->qfeSetUniformMatrix4f("patientVoxelMatrix" , 1, patientVoxel);
  this->shaderProgramEndCaps->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgramEndCaps->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);

  this->shaderProgramArrows->qfeSetUniform1i("colorType",               (int)this->paramColorType);
  this->shaderProgramArrows->qfeSetUniform3f("colorUni",                this->paramColor.r, this->paramColor.g, this->paramColor.b);
  this->shaderProgramArrows->qfeSetUniform1i("lineShading",             (int)this->lineShading);
  this->shaderProgramArrows->qfeSetUniform1f("lineWidth",               this->lineWidth);
  this->shaderProgramArrows->qfeSetUniform1f("phaseSpan",               this->paramPhaseSpan/this->phaseDuration); 
  this->shaderProgramArrows->qfeSetUniform1i("phaseVisible",            this->paramPhaseVisible);
  this->shaderProgramArrows->qfeSetUniform1f("phaseCurrent",            this->phaseCurrent); 
  this->shaderProgramArrows->qfeSetUniform4f("light",                   lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramArrows->qfeSetUniform4f("lightDirection",          lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramArrows->qfeSetUniform2f("dataRange",               vu, vl);
  this->shaderProgramArrows->qfeSetUniform1f("arrowScale",              this->paramArrowScale / 100.0f);
  this->shaderProgramArrows->qfeSetUniform3f("volumeDimensions",        (GLfloat)this->volumeSize[0], (GLfloat)this->volumeSize[1], (GLfloat)this->volumeSize[2]);
  this->shaderProgramArrows->qfeSetUniformMatrix4f("patientVoxelMatrix"    ,    1, patientVoxel);
  this->shaderProgramArrows->qfeSetUniformMatrix4f("matrixModelView"       ,    1, this->matrixModelView);  
  this->shaderProgramArrows->qfeSetUniformMatrix4f("matrixModelViewInverse",    1, this->matrixModelViewInverse);
  this->shaderProgramArrows->qfeSetUniformMatrix4f("matrixModelViewProjection",    1, this->matrixModelViewProjection);
  this->shaderProgramArrows->qfeSetUniformMatrix4f("matrixProjection"      ,    1, this->matrixProjection);  
  
    
  return qfeSuccess;
}


