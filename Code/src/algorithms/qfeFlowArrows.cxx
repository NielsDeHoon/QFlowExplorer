#include "qfeFlowArrows.h"

//----------------------------------------------------------------------------
qfeFlowArrows::qfeFlowArrows()
{
  this->shaderProgram = new qfeGLShaderProgram();
  this->shaderProgram->qfeAddShaderFromFile("/shaders/ifr/arrow-vert.glsl", QFE_VERTEX_SHADER);  
  this->shaderProgram->qfeAddShaderFromFile("/shaders/ifr/arrow-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgram->qfeAddShaderFromFile("/shaders/ifr/arrow-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgram->qfeBindAttribLocation("qfe_Vertex", ATTRIB_VERTEX);
  this->shaderProgram->qfeBindFragDataLocation("qfe_FragColor", 0);  
  this->shaderProgram->qfeLink();

  this->firstRender = true;

  this->paramLineWidth    = 1.0;
};

//----------------------------------------------------------------------------
qfeFlowArrows::qfeFlowArrows(const qfeFlowArrows &fv)
{
  this->firstRender       = fv.firstRender;
  this->shaderProgram     = fv.shaderProgram;
  this->light             = fv.light;

  this->V2P               = fv.V2P;
  this->P2V               = fv.P2V;
  this->volumeSize[0]     = fv.volumeSize[0];
  this->volumeSize[1]     = fv.volumeSize[1];
  this->volumeSize[2]     = fv.volumeSize[2];
  this->phaseDuration     = fv.phaseDuration;

  // Dynamic uniform locations
  this->paramArrowColor   = fv.paramArrowColor;
  this->paramArrowScale   = fv.paramArrowScale;
};

//----------------------------------------------------------------------------
qfeFlowArrows::~qfeFlowArrows()
{
  delete this->shaderProgram;
};

/*
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeRenderBigArrowGPU(int currentRing, int currentPhase, bool arrowTrails)
{
  int trailLength = 4;
  int trailCount  = trailLength;
  int trailMax;

  if(arrowTrails)
  {
    this->shaderProgram->qfeSetUniform1i("trailMax", trailLength);

    if(currentPhase > trailLength)
    {    
      for(int i=currentPhase-trailLength; i<=currentPhase; i++)
      {
        this->shaderProgram->qfeSetUniform1i("trailCount", trailCount);
        this->qfeRenderBigArrowGPU(this->rings[currentRing], this->bigArrowDirections[i][currentRing], this->bigArrowPositions[i][currentRing]);
        trailCount --;
      }
    }
    else
    {
      trailMax   = min(currentPhase, trailLength);
      trailCount = trailMax;

      for(int i=0; i<=trailMax; i++)
      {
        this->shaderProgram->qfeSetUniform1i("trailCount", trailCount);
        this->qfeRenderBigArrowGPU(this->rings[currentRing], this->bigArrowDirections[i][currentRing], this->bigArrowPositions[i][currentRing]);
        trailCount--;
      }
    }
  }
  else
  {
    this->shaderProgramFlowRateArrow->qfeSetUniform1i("trailMax", 1);
    this->shaderProgramFlowRateArrow->qfeSetUniform1i("trailCount", 0);

    this->qfeRenderBigArrowGPU(this->rings[currentRing], this->bigArrowDirections[currentPhase][currentRing], this->bigArrowPositions[currentPhase][currentRing]);
  }
 
  return qfeSuccess;
}
*/

/*
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeRenderBigArrowGPU(ADCenterlineRing *ring, qfeVector vec, qfePoint pos)
{
  Coord3DDouble axisX;
  qfePoint      x;
  GLuint        vbo;
  qfeFloat     *vertex = new qfeFloat[3];

  // Get ring characteristic
  axisX  = adCenterlineRingGetBaseVx(ring);

  // Convert to qfe format
  x.qfeSetPointElements(axisX.x, axisX.y, axisX.z);

  // Create and fill the VBO
  vertex[0] = pos.x;
  vertex[1] = pos.y;
  vertex[2] = pos.z;

  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, 3*sizeof(GLfloat), vertex, GL_STATIC_DRAW);
  
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in patient coordinates
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  glLineWidth(this->paramLineWidth);  
  glPointSize(2.0);  

  this->shaderProgram->qfeSetUniform3f("axisX", x.x, x.y, x.z);
  this->shaderProgram->qfeSetUniform3f("vector", vec.x, vec.y, vec.z);

  this->shaderProgram->qfeEnable();
  
  glColor3f(this->paramArrowColor.r, this->paramArrowColor.g, this->paramArrowColor.b);

  glEnableVertexAttribArray(ATTRIB_VERTEX);
  
  glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  
  glDrawArrays(GL_POINTS, 0, 1);

  glDisableVertexAttribArray(ATTRIB_VERTEX);
  
  //glBegin(GL_POINTS);    
  //  glVertex3f(pos.x, pos.y, pos.z);
  //glEnd();

  this->shaderProgram->qfeDisable();

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND); 

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}
*/
/*
//----------------------------------------------------------------------------
// \todo fix value domain
qfeReturnStatus qfeFlowArrows::qfeRenderBigArrowCPU(ADCenterlineRing *ring, qfeVolume *volume)
{
  qfeMatrix4f   V2P, P2V;
  GLfloat       matrix[16];
  const int     precision = 20;
  Coord3DDouble origin, axisZ;
  qfeMatrix4f   M, MVinv;  
  qfeVector     x, z, e, v;
  qfePoint      o, p;
  qfeFloat      flowRate, scaleFlowRate, scaleFactor, scale;

  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]);
  
  // Get ring characteristic
  origin = adCenterlineRingGetVesselOrigin(ring);
  axisZ  = adCenterlineRingGetBaseVz(ring);
  
  // Convert to qfe format
  o.qfeSetPointElements(origin.x, origin.y, origin.z);

  // Get the direction of the maximum velocity
  qfeComputeMaximumVelocity(ring, volume, 10, v, p);
  qfeComputeFlowRate(ring, volume, flowRate);

  scaleFlowRate = flowRate;
  scaleFactor   = 1.0 / 300.0;
  scale         = scaleFlowRate * scaleFactor;

  // Get the view vector  
  qfeMatrix4f::qfeGetMatrixInverse(this->matrixModelView, MVinv);  
  M = MVinv;

  e.qfeSetVectorElements(0.0,0.0,-1.0,0.0);
  qfeMatrix4f::qfeMatrixPreMultiply(e, M, e);
  qfeVector::qfeVectorNormalize(e);

  // Set the origin to patient coordinates
  o = o*V2P;

  // Get the axes (Outer product of view vector with long axis)
  x = e^v; 

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in patient coordinates

  glLineWidth(this->paramLineWidth);
  glColor3f(0.2,0.2,0.2);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  this->qfeDrawArrow(o, x, v, 4.0*scale, 8.0*scale, 8.0*scale, 4.0*scale);

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND); 

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}
*/
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeDrawArrow(qfePoint origin, qfeVector axisX, qfeVector axisY,
                                           qfeFloat baseWidth, qfeFloat baseHeight, 
                                           qfeFloat headWidth, qfeFloat headHeight)
{
  qfePoint p[8];

  qfeVector::qfeVectorNormalize(axisX);
  qfeVector::qfeVectorNormalize(axisY);
  
  p[0] = origin;
  p[1] = origin  + axisX*(baseWidth/2.0);
  p[2] = p[1]    + axisY*(baseHeight);
  p[3] = p[2]    + axisX*(headWidth/2.0 - baseWidth/2.0);
  p[4] = origin  + axisY*(baseHeight    + headHeight);
  p[5] = p[3]    - axisX*(headWidth);
  p[6] = p[2]    - axisX*(baseWidth);
  p[7] = origin  - axisX*(baseWidth/2.0);

  //glBegin(GL_POLYGON);
  //for(int i=0; i<9; i++)
  //  glVertex3f(p[i%8].x,p[i%8].y,p[i%8].z);  
  //glEnd();

  glBegin(GL_LINE_STRIP);
    for(int i=0; i<9; i++)
      glVertex3f(p[i%8].x,p[i%8].y,p[i%8].z);  
  glEnd();

  return qfeSuccess;
}

/*
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeComputeMaximumVelocity(ADCenterlineRing *ring, qfeVolume *volume, int percentage, qfeVector &v, qfePoint &p)
{
  qfeMatrix4f    V2P,P2V;
  qfeGrid       *grid;
  qfeFloat       ex, ey, ez, emin, sd;
  Coord3DDouble  origin, axisX, axisY;
  double         radius;
  qfePoint       o, s, avgPos;
  qfeVector      x, y, avgDir, tmpPos;
  double         pi = 3.14159265;
  int            count;
  qfeVectorGroup velocities, velocitiesSorted;
  qfePointGroup  positions;

  if(ring == NULL || volume == NULL) return qfeError;

  if(percentage == 0) percentage = 1;

  // Get the transformation matrix
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  // Get the smallest voxel size component
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(ex, ey, ez);  
  emin = min(min(ex, ey), ez);

  // Get ring characteristics
  radius = adCenterlineRingGetRadius(ring);
  origin = adCenterlineRingGetVesselOrigin(ring);
  axisX  = adCenterlineRingGetBaseVx(ring);
  axisY  = adCenterlineRingGetBaseVy(ring);

  o.qfeSetPointElements(origin.x, origin.y, origin.z);
  x.qfeSetVectorElements(axisX.x, axisX.y, axisX.z);
  y.qfeSetVectorElements(axisY.x, axisY.y, axisY.z);
  o  = o * V2P;
  x  = x * V2P; qfeVector::qfeVectorNormalize(x);
  y  = y * V2P; qfeVector::qfeVectorNormalize(y);

  // Compute the sample distance and set start point
  sd = emin / 2.0;
  s  = o - x*radius - y*radius;

  // Steps in y direction
  for(double dy=0; dy < 2.0*radius; dy+=sd)  
  {
    // Steps in x direction
    for(double dx=0; dx< 2.0*radius; dx+=sd)
    {
      qfeVector current;
      qfePoint  pos;

      pos = (s + x*dx + y*dy) * P2V;
      if(this->qfeInsideRing(ring, pos) == true)
      {
        volume->qfeGetVolumeVectorAtPositionLinear(pos, current);
        positions.push_back(pos);
        velocities.push_back(current);
      }
    }
  }

  // Sort by size
  velocitiesSorted.clear();
  velocitiesSorted.resize(velocities.size());

  copy(velocities.begin(), velocities.end(), velocitiesSorted.begin());
  sort(velocitiesSorted.begin(), velocitiesSorted.end());

  // Determine how many vectors we need
  count = (int)((velocities.size() / 100.0) * percentage);

  int peakOffset = (int)(0.1*count); //leave out some peak values
  for(int i=0; i<count; i++)
  {
    qfeFloat  index      = velocities.size()-1-peakOffset-i;               
    qfeVector currentDir = velocitiesSorted[index > 0 ? index : 0];
    qfeVector cvPos;
    qfePoint  cpPos;
   
    qfeVectorGroup::iterator it = find(velocities.begin(), velocities.end(), currentDir); 
    cpPos = positions[it - velocities.begin()];
    cvPos.qfeSetVectorElements(cpPos.x, cpPos.y, cpPos.z);

    avgDir = avgDir + currentDir;
    tmpPos = tmpPos + cvPos;
  }
  tmpPos = tmpPos * (1.0/(float)count);
  avgDir = avgDir * (1.0/(float)count);
  avgPos.qfeSetPointElements(tmpPos.x, tmpPos.y, tmpPos.z);
  
  // Return the result
  v = avgDir;
  p = avgPos;

  velocities.clear();
  velocitiesSorted.clear();
  positions.clear();

  return qfeSuccess;
}
*/
/*
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeComputeFlowRate(ADCenterlineRing *ring, qfeVolume *volume, qfeFloat &flowRate)
{
  qfeMatrix4f   V2P,P2V;
  qfeGrid      *grid;
  qfeFloat      ex, ey, ez, emin, sd;
  Coord3DDouble origin, axisX, axisY, axisZ;
  double        radius, area;
  qfePoint      o, s;
  qfeVector     x, y, n;
  double        pi = 3.14159265;
  qfeFloat      fluxSum = 0.0, fluxAvg = 0.0;
  int           count   = 0;

  // Get the transformation matrix
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  // Get the smallest voxel size component
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(ex, ey, ez);  
  emin = min(min(ex, ey), ez);

  // Get ring characteristics
  area   = adCenterlineRingGetVesselArea(ring);
  radius = adCenterlineRingGetRadius(ring);
  origin = adCenterlineRingGetVesselOrigin(ring);
  axisX  = adCenterlineRingGetBaseVx(ring);
  axisY  = adCenterlineRingGetBaseVy(ring);
  axisZ  = adCenterlineRingGetBaseVz(ring);

  o.qfeSetPointElements(origin.x, origin.y, origin.z);
  x.qfeSetVectorElements(axisX.x, axisX.y, axisX.z);
  y.qfeSetVectorElements(axisY.x, axisY.y, axisY.z);
  n.qfeSetVectorElements(axisZ.x, axisZ.y, axisZ.z);
  o = o * V2P;
  x = x * V2P; qfeVector::qfeVectorNormalize(x);
  y = y * V2P; qfeVector::qfeVectorNormalize(y);
  n = n * V2P; qfeVector::qfeVectorNormalize(n); 

  // Compute the sample distance and set start point
  sd = emin / 2.0;
  s  = o - x*radius - y*radius;

  // Steps in y direction
  for(double dy=0; dy < 2.0*radius; dy+=sd)  
  {
    // Steps in x direction
    for(double dx=0; dx< 2.0*radius; dx+=sd)
    {
      qfeVector v;
      qfePoint  pos;
      qfeFloat  flux;

      pos = (s + x*dx + y*dy) * P2V;
      if(this->qfeInsideRing(ring, pos) == true)
      {
        volume->qfeGetVolumeVectorAtPositionLinear(pos, v);
        qfeVector::qfeVectorLength((n*v)*n, flux);

        fluxSum += flux;
        count   ++;
      }
    }
  }

  // flow in cm/s and area in mm^2
  fluxAvg   = fluxSum / count;
  flowRate  = fluxAvg * (area/100.0);

  return qfeSuccess;

}
*/
/*
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfePrecomputeBigArrows(qfeRingGroup rings, qfeVolumeSeries series, int maxperc, qfeVectorGroupSeries &arrows, qfePointGroupSeries &positions)
{
  if(rings.size() <= 0 || series.size() <= 0) return qfeError;
 
  qfeVectorGroupSeries velocities;
  qfeFloatGroupSeries  flowrates;

  arrows.clear();
  positions.clear();

  this->qfePrecomputeMaxVelocities(rings, series, maxperc, velocities, positions);
  this->qfePrecomputeFlowRates(rings, series, flowrates);

  for(int i=0; i<(int)series.size(); i++)
  {
    qfeVectorGroup vg;
    for(int j=0; j<(int)rings.size(); j++)
    {
      qfeVector::qfeVectorNormalize(velocities[i][j]);
      vg.push_back(velocities[i][j] * flowrates[i][j]);
    }
    arrows.push_back(vg);
  }

  return qfeSuccess;
}
*/
/*
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfePrecomputeMaxVelocities(qfeRingGroup rings, qfeVolumeSeries series, int maxperc, qfeVectorGroupSeries &velocityDirections, qfePointGroupSeries &velocityPositions)
{
  if(rings.size() <= 0 || series.size() <= 0) return qfeError;

  velocityDirections.clear();
  velocityPositions.clear();

  // For each volume in this series
  for(int i=0; i<(int)series.size(); i++)
  {
    qfeVectorGroup vg;
    qfePointGroup  pg;
    this->qfePrecomputeMaxVelocities(rings, series[i], maxperc, vg, pg);
    velocityDirections.push_back(vg);
    velocityPositions.push_back(pg);
  }  

  return qfeSuccess;
}
*/
/*
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfePrecomputeMaxVelocities(qfeRingGroup rings, qfeVolume *volume, int maxperc, qfeVectorGroup &velocityDirections, qfePointGroup &velocityPositions)
{
  if(rings.size() <= 0 || volume == NULL) return qfeError;

  velocityDirections.clear();
  velocityPositions.clear();

  // For each ring in this volume
  for(int i=0; i<(int)rings.size(); i++)
  {
    qfeVector v;
    qfePoint  o;
    this->qfeComputeMaximumVelocity(rings[i], volume, maxperc, v, o);
    velocityDirections.push_back(v);
    velocityPositions.push_back(o);
  }  

  return qfeSuccess;
}
*/
/*
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfePrecomputeFlowRates(qfeRingGroup rings, qfeVolumeSeries series,  qfeFloatGroupSeries &flowrate)
{
  if(rings.size() <= 0 || series.size() <= 0) return qfeError;

  flowrate.clear();

  // For each volume in this series
  for(int i=0; i<(int)series.size(); i++)
  {
    qfeFloatGroup g;
    this->qfePrecomputeFlowRates(rings, series[i], g);
    flowrate.push_back(g);
  }  

  return qfeSuccess;
}
*/
/*
//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfePrecomputeFlowRates(qfeRingGroup rings, qfeVolume *volume, qfeFloatGroup &flowrate)
{
  if(rings.size() <= 0 || volume == NULL) return qfeError;

  flowrate.clear();

  // For each ring in this volume
  for(int i=0; i<(int)rings.size(); i++)
  {
    qfeFloat fr;
    this->qfeComputeFlowRate(rings[i], volume, fr);
    flowrate.push_back(fr);
  }  

  return qfeSuccess;
}
*/
/*
//----------------------------------------------------------------------------
// Point p in voxel coordinates
// \todo should be a support function of probe 2D
bool qfeFlowArrows::qfeInsideRing(ADCenterlineRing *ring, qfePoint p)
{
  const int     precision = 20;
  bool result = false;  

  Coord3DDouble origin, axisX, axisY, r[precision];
  qfePoint      o, o2D, p2D, q2D, r3D_A, r3D_B, r2D_A, r2D_B;
  qfeVector     x, y;

  // Get ring characteristics
  adCenterlineRingGetPoints(ring, &r[0], precision);
  origin = adCenterlineRingGetVesselOrigin(ring);
  axisX  = adCenterlineRingGetBaseVx(ring);
  axisY  = adCenterlineRingGetBaseVy(ring);

  o.qfeSetPointElements(origin.x, origin.y, origin.z);
  x.qfeSetVectorElements(axisX.x, axisX.y, axisX.z);
  y.qfeSetVectorElements(axisY.x, axisY.y, axisY.z);
  qfeVector::qfeVectorNormalize(x);
  qfeVector::qfeVectorNormalize(y);

  // Get two point of the line we will check
  o2D.x = o2D.y = o2D.z = 0.0;
  this->qfeProjectPointToPlane(x, y, o, p, p2D);

  int count = 0;
  for(int i=0; i<precision; i++)
  {
    // Get the two points of the line segment in 3D
    r3D_A.qfeSetPointElements(r[i].x, r[i].y, r[i].z);
    r3D_B.qfeSetPointElements(r[(i+1)%precision].x, r[(i+1)%precision].y, r[(i+1)%precision].z);

    // Project the two points to the 2D plane
    this->qfeProjectPointToPlane(x, y, o, r3D_A, r2D_A);
    this->qfeProjectPointToPlane(x, y, o, r3D_B, r2D_B);

    if(this->qfeIntersectLines(o2D, p2D, r2D_A, r2D_B, q2D) ==  true)
    {
      count++;
    } 
  }

  // True when we have an even number of intersections we are inside the contour
  if((count % 2) == 0)
    return true;
  else 
    return false;
}
*/

//----------------------------------------------------------------------------
void qfeFlowArrows::qfeProjectPointToPlane(qfeVector x, qfeVector y, qfePoint o, qfePoint p3D, qfePoint &p2D)
{
  p2D.x = x * (p3D - o);
  p2D.y = y * (p3D - o);
  p2D.z = 0.0;
}

//----------------------------------------------------------------------------
bool qfeFlowArrows::qfeIntersectLines(qfePoint a, qfePoint b, qfePoint c, qfePoint d, qfePoint &p)
{
  qfeFloat r, s, den;

  den =  (b.x-a.x)*(d.y-c.y) - (b.y-a.y)*(d.x-c.x);
  r   = ((a.y-c.y)*(d.x-c.x) - (a.x-c.x)*(d.y-c.y)) / den;
  s   = ((a.y-c.y)*(b.x-a.x) - (a.x-c.x)*(b.y-a.y)) / den;

  p.x = a.x + (b.x - a.x)*r;
  p.y = a.y + (b.y - a.y)*s;

  if(r >= 0 && r <=1 && s >=0 && s <=1) 
    return true;
  else 
    return false;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeBindTextures(qfeVolume *volume, qfeColorMap *colormap)
{
  GLuint tid, cid;

  volume->qfeGetVolumeTextureId(tid);
  colormap->qfeGetColorMapTextureId(cid);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_3D, tid);

  glActiveTexture(GL_TEXTURE7);
  glBindTexture(GL_TEXTURE_3D, cid);


  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeSetStaticUniformLocations()
{
  GLfloat matrix1[16], matrix2[16];

  qfeMatrix4f::qfeGetMatrixElements(this->V2P, &matrix1[0]);
  qfeMatrix4f::qfeGetMatrixElements(this->P2V, &matrix2[0]);

  this->shaderProgram->qfeSetUniform1i("phaseDuration", this->phaseDuration);
  this->shaderProgram->qfeSetUniformMatrix4f("matrixVoxelPatient", 1, matrix1);
  this->shaderProgram->qfeSetUniformMatrix4f("matrixPatientVoxel", 1, matrix2);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeSetDynamicUniformLocations()
{
  this->shaderProgram->qfeSetUniform1f("scaleFactor", this->paramArrowScale);
  
  this->shaderProgram->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);
  this->shaderProgram->qfeSetUniformMatrix4f("matrixModelViewInverse"   , 1, this->matrixModelViewInverse);
  this->shaderProgram->qfeSetUniformMatrix4f("matrixModelView"          , 1, this->matrixModelView);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeSetArrowColor(qfeColorRGBA color)
{
  this->paramArrowColor = color;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeSetArrowScale(double scale)
{
  this->paramArrowScale = scale;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeSetArrowLineWidth(int width)
{
  this->paramLineWidth = width;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowArrows::qfeSetLightSource(qfeLightSource *light)
{
  this->light = light;

  return qfeSuccess;
}


