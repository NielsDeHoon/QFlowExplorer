#pragma once

#include "qfeVector.h"
#include "qfeMatrix4f.h"
#include "qfeClusterNode.h"

#include <vector>

/**
* \file   qfeClusterNodeLinearModel.h
* \author Sander Jacobs
* \class  qfeClusterNodeLinearModel
* \brief  Implementation of a cluster node using the Linear model metric proposed by Carmo et al.
* \note   Confidential
*/

class qfeClusterNodeLinearModel : public qfeClusterNode
{
public:
  qfeClusterNodeLinearModel(qfeClusterPoint point);
  ~qfeClusterNodeLinearModel();

  qfeReturnStatus qfeMergeRepresentative(qfeClusterNode *c2);
  qfeReturnStatus qfeAddModelPoint(qfeClusterPoint point);
  qfeReturnStatus qfeCalculateClusterModelError();
  qfeFloat        qfeComputeDistanceTo(qfeClusterNode *c2);

private:
  static qfeReturnStatus qfeEstimateLinearModel(qfeFloat X[4], qfeFloat V[3], qfeFloat XX[10], qfeFloat XV[12], \
                                                unsigned int modelSize, qfeFloat A[12], qfeFloat v0[3], bool &singular);

  static qfeReturnStatus qfeCalculateModelError(qfeFloat A[12], qfeFloat v0[3], vector<qfeClusterPoint>* clusterPoints, float &modelError);
  static qfeReturnStatus qfeCalculateModelError(qfeFloat v0[3], vector<qfeClusterPoint>* clusterPoints, float &modelError);


  vector<qfeClusterPoint> clusterPoints;
  unsigned int            modelSize;
  qfeFloat                modelError;

  qfeFloat  X[4];   // 4d vector
  qfeFloat  V[3];   // 3d vector
  qfeFloat  XX[10]; // 4x4 tensor (symmetric: 00, 01, 02, 03, 11, 12, 13, 22, 23, 33)
  qfeFloat  XV[12]; // 4x3 tensor
};
