#include "qfeQuantification.h"

//----------------------------------------------------------------------------
qfeQuantification::qfeQuantification()
{

};

//----------------------------------------------------------------------------
qfeQuantification::qfeQuantification(const qfeQuantification &fv)
{

};

//----------------------------------------------------------------------------
qfeQuantification::~qfeQuantification()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeQuantification::qfeGetMaximumSpeed(qfeVolumeSeries *series, qfeProbe2D &probe, int percentage)
{
  qfeVector                  maxVelocity;
  qfePoint                   maxPosition;
  qfeFloat                   maxSpeed;
  qfeFloat                   max;
  qfeProbeQuantificationItem measure;
  int                        phases;
  float                      phaseDuration;

  if(series == NULL) return qfeError;

  phases         = (int)(*series).size();

  if(phases <= 0) return qfeError;

  (*series).front()->qfeGetVolumePhaseDuration(phaseDuration);
  
  max = 0.0;
  measure.values.clear();
  for(int i=0; i<phases; i++)
  {
    float vl = 0.0, vu = 0.0;
    
    (*series).at(i)->qfeGetVolumeValueDomain(vl, vu);
    if(abs(vl) > max) max = abs(vl);
    if(abs(vu) > max) max = abs(vu);

    if(qfeQuantification::qfeGetMaximumVelocity(&probe, series->at(i), percentage, maxVelocity, maxPosition) == qfeSuccess)
    {

      qfeVector::qfeVectorLength(maxVelocity, maxSpeed);   

      measure.values.push_back(maxSpeed);
    }
    else
    {
      measure.values.push_back(0.0);
    }
  }
  
  measure.label     = "Max. speed";
  measure.labelX    = "Trigger time (ms)";
  measure.labelY    = "Maximum speed (cm/s)";
  measure.rangeX[0] = 0.0;
  measure.rangeX[1] = phases * phaseDuration;
  measure.stepsX    = phaseDuration;
  measure.rangeY[0] = 0.0;
  measure.rangeY[1] = max;
  measure.stepsY    = 10.0;
  
  probe.quantification.push_back(measure);

  return qfeSuccess;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeQuantification::qfeGetFlowRate(qfeVolumeSeries *series, qfeProbe2D &probe)
{  
  qfeFloat                   flowRate;
  qfeFloat                   max;
  qfeProbeQuantificationItem measure;
  int                        phases;
  float                      phaseDuration;

  if(series == NULL) return qfeError;

  phases         = (int)(*series).size();

  if(phases <= 0) return qfeError;

  (*series).front()->qfeGetVolumePhaseDuration(phaseDuration);

  std::cout<<"Phase duration "<<phaseDuration<<std::endl;
  
  max = 0.0;
  measure.values.clear();
  for(int i=0; i<phases; i++)
  {
    qfeQuantification::qfeGetFlowRate(&probe, series->at(i), flowRate);

    measure.values.push_back(flowRate);

    if(flowRate > max) max = flowRate;
  }
  
  measure.label     = "Flow rate";
  measure.labelX    = "Trigger time (ms)";
  measure.labelY    = "Flow rate (cm2/s)";
  measure.rangeX[0] = 0.0;
  measure.rangeX[1] = phases * phaseDuration;
  measure.stepsX    = phaseDuration;
  measure.rangeY[0] = 0.0;
  measure.rangeY[1] = max;
  measure.stepsY    = 10.0;
  
  probe.quantification.push_back(measure);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeQuantification::qfeGetMaximumVelocity(qfeProbe2D *probe, qfeVolume *volume, int percentage, qfeVector &maxVelocity, qfePoint &maxPosition)
{
  qfeMatrix4f    P2V;
  qfeGrid       *grid;
  qfeFloat       ex, ey, ez, emin, sd;
  qfePoint       s, avgPos;
  qfeVector      avgDir, tmpPos;
  double         pi = 3.14159265;
  int            count;
  qfeVectorGroup velocities, velocitiesSorted;
  qfePointGroup  positions;

  if(volume == NULL) return qfeError;

  if(percentage == 0) percentage = 1;

  // Get the transformation matrix
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  // Get the smallest voxel size component
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(ex, ey, ez);  
  emin = min(min(ex, ey), ez);

  // Compute the sample distance and set start point
  sd = emin / 2.0;
  s  = probe->origin - probe->axisX*probe->radius - probe->axisY*probe->radius;

  // Steps in y direction
  positions.clear();
  velocities.clear();
  for(double dy=0; dy < 2.0*probe->radius; dy+=sd)  
  {
    // Steps in x direction
    for(double dx=0; dx< 2.0*probe->radius; dx+=sd)
    {
      qfeVector current;
      qfePoint  posPatient, posVoxel;

      posPatient = s + probe->axisX*dx + probe->axisY*dy;
      if(qfeSeeding::qfeInProbe(posPatient, probe) == true)
      {
        posVoxel = posPatient * P2V;

        volume->qfeGetVolumeVectorAtPositionLinear(posVoxel, current);

        positions.push_back(posPatient);
        velocities.push_back(current);
      }
    }
  }

  // Sort by size
  velocitiesSorted.clear();
  velocitiesSorted.resize(velocities.size());

  copy(velocities.begin(), velocities.end(), velocitiesSorted.begin());
  sort(velocitiesSorted.begin(), velocitiesSorted.end());

  // Determine how many vectors we need
  count = (int)((velocities.size() / 100.0) * percentage);

  int peakOffset = (int)(0.1*count); //leave out some peak values
  for(int i=0; i<count; i++)
  {
    qfeFloat  index      = velocities.size()-1-peakOffset-i;               
    qfeVector currentDir = velocitiesSorted[index > 0 ? index : 0];
    qfeVector cvPos;
    qfePoint  cpPos;
   
    qfeVectorGroup::iterator it = find(velocities.begin(), velocities.end(), currentDir); 
    cpPos = positions[it - velocities.begin()];
    cvPos.qfeSetVectorElements(cpPos.x, cpPos.y, cpPos.z);

    avgDir = avgDir + currentDir;
    tmpPos = tmpPos + cvPos;
  }
  tmpPos = tmpPos * (1.0/(float)count);
  avgDir = avgDir * (1.0/(float)count);
  avgPos.qfeSetPointElements(tmpPos.x, tmpPos.y, tmpPos.z);
  
  // Return the result
  maxVelocity = avgDir;
  maxPosition = avgPos;

  velocities.clear();
  velocitiesSorted.clear();
  positions.clear();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeQuantification::qfeGetFlowRate(qfeProbe2D *probe, qfeVolume *volume, qfeFloat &flowRate)
{
  qfeMatrix4f    P2V;
  qfeGrid       *grid;
  qfeFloat       ex, ey, ez, emin, sd;
  qfePoint       s, avgPos;
  qfeVector      avgDir, tmpPos;
  double         pi = 3.14159265;
  qfeVectorGroup velocities, velocitiesSorted;
  qfePointGroup  positions;
  qfeFloat       fluxSum = 0.0, fluxAvg = 0.0;
  int            count = 0;

  if(volume == NULL) return qfeError;

  // Get the transformation matrix
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);

  // Get the smallest voxel size component
  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(ex, ey, ez);  
  emin = min(min(ex, ey), ez);

  // Compute the sample distance and set start point
  sd = emin / 2.0;
  s  = probe->origin - probe->axisX*probe->radius - probe->axisY*probe->radius;

  std::cout<<probe->origin.x<<" "<<probe->origin.y<<" "<<probe->origin.z<<" "<<probe->radius<<std::endl;

  // Steps in y direction
  for(double dy=0; dy < 2.0*probe->radius; dy+=sd)  
  {
    // Steps in x direction
    for(double dx=0; dx< 2.0*probe->radius; dx+=sd)
    {
      qfeVector v;
      qfePoint  posPatient, posVoxel;
      qfeFloat  flux;

      posPatient = s + probe->axisX*dx + probe->axisY*dy;
      if(qfeSeeding::qfeInProbe(posPatient, probe) == true)
      {
        posVoxel = posPatient * P2V;

        volume->qfeGetVolumeVectorAtPositionLinear(posVoxel, v);
        qfeVector::qfeVectorLength((probe->normal*v)*probe->normal, flux);

        fluxSum += flux;
        count   ++;
      }
    }
  }

  // flow in cm/s and area in mm^2
  // flow rate in cm2/s
  fluxAvg   = fluxSum / count;
  flowRate  = fluxAvg * (probe->area/100.0);

  std::cout<<"Count "<<count<<" area "<<probe->area<<" fluxAvg "<<fluxAvg<<std::endl;

  return qfeSuccess;

}
