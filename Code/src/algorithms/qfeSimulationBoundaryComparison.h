#pragma once

#include "qflowexplorer.h"

#include "qfeColorMap.h"
#include "qfeFlowLineTrace.h"
#include "qfeGLShaderProgram.h"
#include "qfeGLSupport.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeMath.h"
#include "qfePoint.h"
#include "qfeSeeding.h"
#include "qfeTransform.h"
#include "qfeVolume.h"

#include <vector>
#include <limits>

#define MAX_PARTICLES 8192

/**
* \file   qfeSimulationBoundaryComparison.h
* \author Roy van Pelt
* \class  qfeSimulationBoundaryComparison
* \brief  Implements rendering of a comparative arrows at the boundaries
* \note   Confidential
*
* Rendering of a line comparison
*/

class qfeSimulationBoundaryComparison : public qfeAlgorithmFlow
{
public:
  enum qfeComparisonColorType
  {
    qfeComparisonColorVelocity,
    qfeComparisonColorDistanceLocal,
    qfeComparisonColorDistanceMean,
    qfeComparisonColorDistanceHausdorff
  };
  qfeSimulationBoundaryComparison();
  qfeSimulationBoundaryComparison(const qfeSimulationBoundaryComparison &fv);
  ~qfeSimulationBoundaryComparison();

  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);

  qfeReturnStatus qfeSetVolumeSeries(qfeVolumeSeries first, qfeVolumeSeries second);
  qfeReturnStatus qfeSetArrowsVisible(bool arrows);
  qfeReturnStatus qfeSetAngleFilter(int angle);
  qfeReturnStatus qfeSetColorType(qfeComparisonColorType color);
  qfeReturnStatus qfeSetArrowWidth(qfeFloat width);
  qfeReturnStatus qfeSetArrowFirstHighlight(bool highlight);

  qfeReturnStatus qfeAddSeedsFromModel(qfeModel *model, qfeFloat offset, qfeVolume *volume);  
  qfeReturnStatus qfeClearSeeds();

  qfeReturnStatus qfeRenderArrowsComparison(qfeColorMap *colormap, qfeFloat currentPhase);

protected:
  qfeReturnStatus qfeInitTraceStatic();

  qfeReturnStatus qfeAddParticlePositions(vector< vector<float> > seeds, qfeVolume *volume);

  qfeReturnStatus qfeRenderSeeds(GLuint particles, GLuint velocities1, GLuint velocities2, int count);
  qfeReturnStatus qfeRenderBody(GLuint particles, GLuint velocities1, GLuint velocities2, int count);

  qfeReturnStatus qfeDrawParticles(GLuint particles, int count);
  qfeReturnStatus qfeDrawParticles(GLuint particles, GLuint velocities1, GLuint velocities2, int count);

  qfeReturnStatus qfeFetchVelocityLinear(qfeVolume *vol, qfePoint p, qfeVector &v);
  qfeReturnStatus qfeFetchVelocityLinearTemporalLinear(qfeVolumeSeries series, qfePoint p, qfeFloat t, qfeVector &v);
  
  qfeReturnStatus qfeInitBufferStatic(vector<float> *vertices, GLuint &VBO);
  qfeReturnStatus qfeClearBufferStatic(GLuint &VBO);

  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

private :
  bool                           firstRender;
  bool                           arrowsVisible;
  bool                           surfaceVisible;
  int                            angleFilter;
  qfeComparisonColorType         colorType;

  qfeLightSource                *light;

  qfeVolumeSeries                seriesFirst;
  qfeVolumeSeries                seriesSecond;

  float                          currentTime;
  bool                           currentRenderFirst;

  vector< qfeFloat >             particlePositions;  
  vector< qfeFloat >             particleNormals;  
  vector< vector< qfeFloat > >   particleVelocitiesFirst;  
  vector< vector< qfeFloat > >   particleVelocitiesSecond;    
  GLuint                         particlePositionsVBO;  
  GLuint                         particleNormalsVBO; 
  GLuint                         particleVelocitiesFirstVBO; 
  GLuint                         particleVelocitiesSecondVBO; 
  

  qfeFloat  			               arrowWidth;
  int                            seedsCount;

  GLuint                         particlesCount;
  GLuint                         particleTransferFunction;

  GLint                          locationPosition;
  GLint                          locationVelocitiesFirst;  
  GLint                          locationVelocitiesSecond;  
  GLint                          locationNormals;

  qfeFloat                       paramParticleSize;
  qfeFloat                       paramCurrentPhase;
  bool                           paramFirstHighlight;


  qfeGLShaderProgram            *shaderProgramRenderArrowSphere;
  qfeGLShaderProgram            *shaderProgramRenderArrowBody;
};
