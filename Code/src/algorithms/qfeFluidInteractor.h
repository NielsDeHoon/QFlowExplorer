#pragma once

#include <assert.h>
#include <tuple>

#include "qflowexplorer.h"
#include "qfeFlowProbe2D.h"
#include "fluidsim\array3.h"
#include "fluidsim\vec.h"

#include <iostream>
#include <sstream>

#include <math.h>

#include "qfeProbe.h"

/**
* \file   qfeSimulation.h
* \author Niels de Hoon
* \class  qfeFluidInteractor
* \brief  Implements a fluid interactor object
* \note   Confidential
*
* Computetion of fluid simulation for coupling with measured data
*
*/


const float source_color[]			= {0.000f, 0.537f, 0.486f, 0.9f};
const float source_creation_color[]= {0.000f, 0.500f, 1.000f, 0.5f};
const float sink_color[]			= {1.000f, 0.537f, 0.157f, 0.9f};
const float sink_creation_color[]	= {1.000f, 0.741f, 0.533f, 0.5f};

const float incorrect_color[]    	= {1.000f, 0.000f, 0.000f, 0.5f};

const float inspection_color[]		= {0.800f, 0.800f, 1.000f, 0.3f};

const float normal_color[]   = {0.2f, 0.2f, 0.2f, 1.0f};
const float outline_color[]  = {0.0f, 0.0f, 0.0f, 1.0f};

const float corner_color0[]	= {1.0f,0.0f,0.0f};
const float corner_color1[]	= {0.0f,0.75f,0.0f};
const float corner_color2[]	= {0.0f,0.0f,1.0f};
const float corner_color3[]	= {1.0f,0.5f,0.0f};

class qfeFluidInteractor : public qfeProbe2D
{
public:		
	enum InteractorStyle { SOURCE, SINK, SOURCE_SELECTION, SINK_SELECTION, INCORRECT, INSPECTOR };
	typedef InteractorStyle InteractorType;

	typedef struct{
		std::string label;
		float time;
		unsigned int size;
		float range_min;
		float range_max;
		unsigned int colorMapId;
	} texture_descriptor;
	
	qfeFluidInteractor()
	{
		origin = qfePoint(0.0f,0.0f,0.0f);
		radius = 0.0f;
		normal = qfeVector(0.0f, 0.0f, 0.0f);
		type = SOURCE;
		dx = 0.0f;
		id = 0;
	}

	~qfeFluidInteractor()
	{
	};

	void Initialize(qfePoint _origin, float _radius, qfeVector _normal, qfeVector _axis1, qfeVector _axis2, InteractorType _type, float _dx, qfeMatrix4f _V2P, qfeMatrix4f _V2T, qfeMatrix4f _T2V, Array3f *_solid);

	//get and set functions:
	qfePoint getOrigin();
	void setOrigin(qfePoint _origin);
	float getRadius();
	void setRadius(float _radius);
	qfeVector getNormal();
	void setNormal(qfeVector _normal);
	qfeVector getAxis1();
	qfeVector getAxis2();
	void setAxis(qfeVector _axis1, qfeVector _axis2);
	qfeFluidInteractor::InteractorType getType();
	void setType(qfeFluidInteractor::InteractorType _type);
	float getdx();
	void setdx(float _dx);
	unsigned int getID();
	void setID(unsigned int _id);
	void setV2PMatrix(qfeMatrix4f _V2P);
	void setV2TMatrix(qfeMatrix4f _V2T);
	void setT2VMatrix(qfeMatrix4f _T2V);
	void setSolid(Array3f *_solid);
	void setPolygon(std::vector<qfePoint>  _polygon);
	std::vector<qfePoint> getPolygon();

	//rendering functionalities
	void renderPolygon(bool isSelected);

	bool textureExists(string label, float time, GLfloat* &image, unsigned int size, float &range_min, float &range_max, unsigned int colorMapId);

	void mapTo2D(Array3f *source, string label, float time, GLfloat* &image, unsigned int size, float range_min, float range_max, qfeColorMap *colorMap, unsigned int colorMapId);

private:
	InteractorType type;

	std::vector<qfePoint> polygon;

	//store a list of textures using: label, time, texture, size
	std::vector<std::pair<texture_descriptor, GLfloat*>> textures;
	
	float dx;

	unsigned int id;
	qfeMatrix4f V2P;
	qfeMatrix4f V2T;
	qfeMatrix4f T2V;

	Array3f *solid;
	
	/**
	Computes an approximation of where the solid is crossed when traversing the line given by direction from point
	**/
	qfePoint computeSolidCrossing(qfePoint point, qfeVector direction);

	void renderQuad();
};