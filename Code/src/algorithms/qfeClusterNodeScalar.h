#pragma once

#include "qfeVector.h"
#include "qfeClusterNode.h"
#include "qfeClusterNodeScalar.h"

/**
* \file   qfeClusterNodeScalar.h
* \author Casper van Leeuwen
* \class  qfeClusterNodeScalar
* \brief  Implementation of a cluster node using a scalar cluster metric.
* \note   Confidential
*/


class qfeClusterNodeScalar : public qfeClusterNode
{
public:
  qfeClusterNodeScalar(qfeClusterPoint point);
  ~qfeClusterNodeScalar();

  qfeReturnStatus qfeMergeRepresentative(qfeClusterNode *c2);
  qfeFloat        qfeComputeDistanceTo(qfeClusterNode *c2);

  static double posWeight;
  static double scalarWeight;
  static double maxScalarVal;

private:

  qfeFloat   meanPosition [4];
  qfeFloat   meanScalar;
  unsigned int  size;
};
