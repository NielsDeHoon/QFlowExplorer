#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithmFlow.h"
#include "qfeColorMap.h"
#include "qfeGLShaderProgram.h"
#include "qfeGLSupport.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeMath.h"
#include "qfePoint.h"
#include "qfeSeeding.h"
#include "qfeTransform.h"
#include "qfeVolume.h"

#include <vector>

#define MAX_PARTICLES 8192

typedef vector< qfeVector > qfeVelocities;        

/**
* \file   qfeSimulationParticleTrace.h
* \author Roy van Pelt
* \class  qfeSimulationParticleTrace
* \brief  Implements rendering of a particle trace
* \note   Confidential
*
* Rendering of a particle trace
*/

class qfeSimulationParticleTrace : public qfeAlgorithmFlow
{
public:
  enum qfeFlowParticlesStyle
  {
	  qfeFlowParticlesSphere,
    qfeFlowParticlesEllipsoid	    
  };

  enum qfeFlowParticlesShading
  {
	  qfeFlowParticlesToon,
	  qfeFlowParticlesPhong  
  };

  qfeSimulationParticleTrace();
  qfeSimulationParticleTrace(const qfeSimulationParticleTrace &fv);
  ~qfeSimulationParticleTrace();

  qfeReturnStatus qfeSetParticlePercentage(int percentage);
  qfeReturnStatus qfeSetParticleSize(qfeFloat size);
  qfeReturnStatus qfeSetParticleStyle(qfeFlowParticlesStyle style);
  qfeReturnStatus qfeSetParticleShading(qfeFlowParticlesShading shading);
  qfeReturnStatus qfeSetParticleContour(bool contour);  
  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);

  qfeReturnStatus qfeClearSeedPositions();  
  qfeReturnStatus qfeAddSeedPositions(vector< vector<float> >  seeds, qfeVolume *volume);
  qfeReturnStatus qfeAddSeedVelocities(vector< vector<float> > velocities);

  qfeReturnStatus qfeRenderParticles(qfeColorMap *colormap);  

protected:
  qfeReturnStatus qfeAddParticlePositions(vector< vector<float> > seeds, qfeVolume *volume);
  qfeReturnStatus qfeAddParticleVelocities(vector< vector<float> > velocities);

  qfeReturnStatus qfeInitParticleBuffers();
  qfeReturnStatus qfeClearParticleBuffers();

  qfeReturnStatus qfeDrawParticles(GLuint particles, GLuint velocities, int count);

  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

private :
  bool                     firstRender;  

  qfeLightSource          *light;

  vector< qfeFloat >       particlePositions;
  vector< qfeFloat >       particleVelocities;
  GLuint                   particlePositionsVBO;        
  GLuint                   particleVelocitiesVBO;          

  GLuint                   particlesCount;  
  GLuint                   particleTransferFunction;
  
  GLint                    locationPosition;
  GLint                    locationVelocity;

  int                      paramParticlePercentage;
  qfeFloat                 paramParticleSize;
  qfeFlowParticlesStyle    paramParticleStyle;
  qfeFlowParticlesShading  paramParticleShading;
  bool                     paramParticleContour;
  
  qfeGLShaderProgram      *shaderProgramRenderQuadric;
  

};
