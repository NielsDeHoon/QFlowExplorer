#pragma once

#include "qflowexplorer.h"
#include <gl/glew.h>
#include <vtkPNGReader.h>
#include <string>

/**
* \file   qfeLitSphereMap.h
* \author Arjan Broos
* \class  qfeLitSphereMap
* \brief  For mapping a normal to a certain color and opacity
* \note   Confidential
*
* Implementation of lit sphere map as described in the 2007 paper:
* "Style Transfer Functions for Illustrative Volume Rendering"
*
*/
class qfeLitSphereMap {
public:
  qfeLitSphereMap();

  void qfeCreateDotMap();
  void qfeLoadFromPNG(const std::string &filePath);

  void qfeLoadPreset(int preset);

  void qfeDestroyTexture();
  GLuint qfeGetMapTexture();

  void qfeSetNrDots(unsigned nrDots);

  // This is the lambda of the exponential distribution for generating dot map
  // Higher values mean more dots towards the edge of the disk
  void qfeSetLambda(float lambda);

  void qfeSaveToFile(float *map, unsigned width, unsigned height, unsigned nrComp);

private:
  float Clamp(float v, float min, float max);
  float GetRandomFloat();
  void SampleDiskInverseCosine(unsigned width, unsigned height, unsigned &x, unsigned &y, float ru, float re);

  GLuint texture;
  unsigned nrDots;
  float lambda;
};