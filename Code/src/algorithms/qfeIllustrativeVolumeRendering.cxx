#include "qfeIllustrativeVolumeRendering.h"

//----------------------------------------------------------------------------
qfeIllustrativeVolumeRendering::qfeIllustrativeVolumeRendering() {
  shaderIVR = new qfeGLShaderProgram();
  shaderIVR->qfeAddShaderFromFile("/shaders/library/transform.glsl",QFE_FRAGMENT_SHADER);
  shaderIVR->qfeAddShaderFromFile("/shaders/library/fetch.glsl",    QFE_FRAGMENT_SHADER);
  shaderIVR->qfeAddShaderFromFile("/shaders/library/normalize.glsl",QFE_FRAGMENT_SHADER);
  shaderIVR->qfeAddShaderFromFile("/shaders/library/composite.glsl",QFE_FRAGMENT_SHADER);
  shaderIVR->qfeAddShaderFromFile("/shaders/library/shading.glsl",  QFE_FRAGMENT_SHADER);
  shaderIVR->qfeAddShaderFromFile("/shaders/ivr/ivr-vert.glsl",     QFE_VERTEX_SHADER);  
  shaderIVR->qfeAddShaderFromFile("/shaders/ivr/ivr-frag.glsl",     QFE_FRAGMENT_SHADER);
  shaderIVR->qfeLink();

  firstRender           = true;

  texRayEnd             = 0;
  texRayEndDepth        = 0;
  texVolume             = 0;
  texColorMap           = 0;

  texIntersectColor     = 0;
  texIntersectDepth     = 0;  

  paramScaling[0]       = 1.0f;
  paramScaling[1]       = 1.0f;
  paramScaling[2]       = 1.0f;
  paramStepSize         = 1.0f/250.0f;

  maxGradLength         = 0.f;
  dirTransExp           = 0.f;

  depthTransparency     = true;
  litSphereMapRendering = false;
  lsmPreset             = 0;
  gradientSampling      = false;

  simpleOpacity         = 1.f;

  useVoIClipping        = false;

  light = nullptr;
}

//----------------------------------------------------------------------------
qfeIllustrativeVolumeRendering::qfeIllustrativeVolumeRendering(const qfeIllustrativeVolumeRendering &pr) {
  firstRender                  = pr.firstRender;
  shaderIVR                    = pr.shaderIVR;

  viewportSize[0]              = pr.viewportSize[0];
  viewportSize[1]              = pr.viewportSize[1];
  superSampling                = pr.superSampling;

  texRayEnd                    = pr.texRayEnd;
  texRayEndDepth               = pr.texRayEndDepth;
  texVolume                    = pr.texVolume;
  texColorMap                  = pr.texColorMap;

  texIntersectColor            = pr.texIntersectColor;
  texIntersectDepth            = pr.texIntersectDepth;  

  paramScaling[0]              = pr.paramScaling[0];
  paramScaling[1]              = pr.paramScaling[1];
  paramScaling[2]              = pr.paramScaling[2];
  paramStepSize                = pr.paramStepSize;
  paramDataRange[0]            = pr.paramDataRange[0];
  paramDataRange[1]            = pr.paramDataRange[1];

  maxGradLength                = pr.maxGradLength;
  dirTransExp                  = pr.dirTransExp;

  depthTransparency            = pr.depthTransparency;
  litSphereMapRendering        = pr.litSphereMapRendering;
  gradientSampling             = pr.gradientSampling;
  lsmPreset                    = pr.lsmPreset;

  simpleOpacity                = pr.simpleOpacity;

  useVoIClipping               = pr.useVoIClipping;

  light                        = pr.light;
  V2P                          = pr.V2P;
}

//----------------------------------------------------------------------------
qfeIllustrativeVolumeRendering::~qfeIllustrativeVolumeRendering() {
  delete shaderIVR;
  qfeDeleteTextures();
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *volume, qfeColorMap *colormap) {
  if (!volume) return qfeError;
  return qfeRenderRaycasting(colortex, depthtex, volume, volume, colormap);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeColorMap *colormap) {
  if (!boundingVolume || !textureVolume) return qfeError;

  qfeStudy *study = nullptr;

  if (firstRender) {
    qfeTransform::qfeGetMatrixVoxelToPatient(V2P, boundingVolume);
    qfeGrid *grid;
    boundingVolume->qfeGetVolumeGrid(&grid);
    grid->qfeGetGridExtent(w, h, d);
    qfeSetStaticUniformLocations();   
    firstRender = false;
  }

  // Obtain color texture id
  textureVolume->qfeGetVolumeTextureId(texVolume);

  colormap->qfeGetColorMapTextureId(texColorMap);  
  colormap->qfeGetGradientMapTextureId(texGradientMap);

  // Compute the stepsize and scaling factor
  qfeGrid     *grid;
  unsigned int dims[3];
  qfePoint     extent;
  textureVolume->qfeGetVolumeSize(dims[0], dims[1], dims[2]);
  textureVolume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(extent.x, extent.y, extent.z);

  this->paramStepSize   = min(extent.x, min(extent.y, extent.z));  
  this->paramScaling[0] = dims[0]*extent.x; 
  this->paramScaling[1] = dims[1]*extent.y; 
  this->paramScaling[2] = dims[2]*extent.z; 

  glDisable(GL_LIGHTING);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  

  qfeGLSupport::qfeCheckFrameBuffer();

  // Start rendering
  // 1. Generate texture with ray end position
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, texRayEnd, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT,   GL_TEXTURE_2D, texRayEndDepth, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, texRayEndDepth, 0);
  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // First render the bounding box stop positions
  glEnable(GL_CULL_FACE);
  glCullFace(GL_FRONT);

  qfeRenderBoundingBox(boundingVolume, textureVolume);

  glDisable(GL_CULL_FACE);

  qfeGLSupport::qfeCheckFrameBuffer();

  // 2. Execute the raycasting (cull the boundingbox to overcome double rendering)
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, colortex, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT,   GL_TEXTURE_2D, depthtex, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depthtex, 0);

  qfeBindTextures();
  qfeSetDynamicUniformLocations();

  shaderIVR->qfeEnable();

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  qfeRenderBoundingBox(boundingVolume, textureVolume);

  glDisable(GL_CULL_FACE);
  glDisable(GL_BLEND);

  shaderIVR->qfeDisable();
  qfeUnbindTextures();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetViewportSize(unsigned int x, unsigned int y, int supersampling) {
  if (viewportSize[0] != x || viewportSize[1] != y) {
    viewportSize[0] = x;
    viewportSize[1] = y;

    qfeUnbindTextures();
    qfeDeleteTextures();
    qfeCreateTextures();
    lsm.qfeLoadPreset(lsmPreset);
  }

  this->superSampling = supersampling;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetIntersectionBuffers(GLuint color, GLuint depth) {
  texIntersectColor = color;
  texIntersectDepth = depth;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetDataRange(qfeFloat vl, qfeFloat vu) {
  paramDataRange[0] = vl;
  paramDataRange[1] = vu;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetLightSource(qfeLightSource *light) {
  light = light;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetMaxGradientLength(float length) {
  maxGradLength = length;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetDirectionalTransparencyExponent(float exponent) {
  dirTransExp = exponent;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetDepthTransparency(bool on) {
  depthTransparency = on;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetLitSphereMapRendering(bool on) {
  litSphereMapRendering = on;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetLitSphereMapPreset(int preset) {
  if (preset != lsmPreset) {
    lsmPreset = preset;
    lsm.qfeLoadPreset(preset);
  }
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetGradientSampling(bool on) {
  gradientSampling = on;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetDepthToneMapping(bool on) {
  depthToneMapping = on;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetDepthToneMappingFactor(float factor) {
  depthToneMappingFactor = factor;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetLsmNrDots(unsigned nrDots) {
  lsm.qfeSetNrDots(nrDots);
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetLsmLambda(float lambda) {
  lsm.qfeSetLambda(lambda);
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeCreateDotMap()
{
  lsm.qfeCreateDotMap();
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetVoITInv(const qfeMatrix4f &TInv)
{
  voiTInv = TInv;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetVoICenter(const qfePoint &center)
{
  voiCenter = center;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetClipVoI(bool on)
{
  useVoIClipping = on;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetSimpleOpacity(float opacity)
{
  simpleOpacity = opacity;
  return qfeSuccess;
}

qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetEyePosition(qfePoint eye)
{
  eyePos = eye;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeCreateTextures() {
  if (superSampling < 0) superSampling = 1;

  glGenTextures(1, &texRayEnd);
  glBindTexture(GL_TEXTURE_2D, texRayEnd);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA16F,
    viewportSize[0]*superSampling, viewportSize[1]*superSampling, 0, GL_BGRA, GL_UNSIGNED_BYTE, nullptr);

  glGenTextures(1, &texRayEndDepth);
  glBindTexture(GL_TEXTURE_2D, texRayEndDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8_EXT, 
    viewportSize[0]*superSampling, viewportSize[1]*superSampling, 0, GL_DEPTH_STENCIL_EXT, GL_UNSIGNED_INT_24_8_EXT, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeDeleteTextures() {  
  if (glIsTexture(texRayEnd))       glDeleteTextures(1, (GLuint *)&texRayEnd);
  if (glIsTexture(texRayEndDepth))  glDeleteTextures(1, (GLuint *)&texRayEndDepth);
  lsm.qfeDestroyTexture();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeBindTextures() {
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_3D, texVolume);

  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_1D, texColorMap);

  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_1D, texGradientMap);

  glActiveTexture(GL_TEXTURE6);
  glBindTexture(GL_TEXTURE_2D, texRayEnd);

  glActiveTexture(GL_TEXTURE7);
  glBindTexture(GL_TEXTURE_2D, texRayEndDepth);

  glActiveTexture(GL_TEXTURE23);
  glBindTexture(GL_TEXTURE_2D, lsm.qfeGetMapTexture());

  if (glIsTexture(texIntersectColor) && glIsTexture(texIntersectDepth)) {
    glActiveTexture(GL_TEXTURE8);
    glBindTexture(GL_TEXTURE_2D, texIntersectColor);

    glActiveTexture(GL_TEXTURE9);
    glBindTexture(GL_TEXTURE_2D, texIntersectDepth);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeUnbindTextures() {
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeRenderBoundingBox(qfeVolume *volume) {
  qfeMatrix4f   P2W, V2P, T2V, M;
  GLfloat       matrix[16];

  qfeTransform::qfeGetMatrixPatientToWorld(P2W);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

  // OpenGL works with column-major matrices
  // OpenGL works with right-multiplication
  M = T2V*V2P*P2W;
  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();
  glMultMatrixf(&matrix[0]);  // Texture to World   Coordinates

  qfeGetModelViewMatrix();

  glBegin(GL_QUADS);

  // Front
  glNormal3f(0.0f, 0.0f, -1.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);

  // Back
  glNormal3f(0.0f, 0.0f, 1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f, 1.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f, 1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f,  1.0f, 1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f, 1.0f);

  // Left
  glNormal3f(-1.0f, 0.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f,  1.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);

  // Right
  glNormal3f(1.0f, 0.0f, 0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f(1.0f,  0.0f,  1.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f(1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f(1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f(1.0f,  1.0f,  1.0f);

  // Top
  glNormal3f(0.0f, -1.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f,  1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);

  // Bottom
  glNormal3f(0.0f, 1.0f, 0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f, 1.0f,  1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f, 1.0f,  1.0f);

  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeRenderBoundingBox(qfeVolume *boundingVolume, qfeVolume* textureVolume) {
  qfePoint      c[4], v[4], min, max;
  qfeMatrix4f   P2W, V2P, T2V, M, T2P, P2T, P2V, V2T, TextureVolumeCoords, BoundingVolumeCoords;
  GLfloat       matrix[16];

  if (!boundingVolume || !textureVolume) return qfeError;

  // apply the modelview matrix
  qfeTransform::qfeGetMatrixPatientToWorld(P2W);

  // bounding volume transformations
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, boundingVolume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, boundingVolume);
  T2P = T2V*V2P;

  // texture volume transformations
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, textureVolume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, textureVolume);
  P2T = P2V*V2T;  

  TextureVolumeCoords = T2P*P2T;
  qfeMatrix4f::qfeGetMatrixInverse(TextureVolumeCoords, BoundingVolumeCoords);

  // clip the bounding box with ortho planes
  max.x = 1.0;
  max.y = 1.0;
  max.z = 1.0;

  min.x = 0.0;
  min.y = 0.0;
  min.z = 0.0;

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  M = T2V*V2P*P2W;
  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();
  glMultMatrixf(&matrix[0]);  // Texture to World Coordinates

  qfeGetModelViewMatrix();

  glBegin(GL_QUADS);

  // Front
  glNormal3f(0.0f, 0.0f, -1.0f);  
  v[0].qfeSetPointElements(min.x,min.y,min.z);
  v[1].qfeSetPointElements(min.x,max.y,min.z);
  v[2].qfeSetPointElements(max.x,max.y,min.z);
  v[3].qfeSetPointElements(max.x,min.y,min.z);
  for (int i = 0; i < 4; i++) {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);    
  }

  // Back
  glNormal3f(0.0f, 0.0f, 1.0f);
  v[0].qfeSetPointElements(min.x,min.y,max.z);
  v[1].qfeSetPointElements(max.x,min.y,max.z);
  v[2].qfeSetPointElements(max.x,max.y,max.z);
  v[3].qfeSetPointElements(min.x,max.y,max.z);
  for (int i = 0; i < 4; i++) {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);    
  }  

  // Left
  glNormal3f(-1.0f, 0.0f, 0.0f);
  v[0].qfeSetPointElements(min.x,min.y,min.z);
  v[1].qfeSetPointElements(min.x,min.y,max.z);
  v[2].qfeSetPointElements(min.x,max.y,max.z);
  v[3].qfeSetPointElements(min.x,max.y,min.z);
  for (int i = 0; i < 4; i++)   {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }

  // Right
  glNormal3f(1.0f, 0.0f, 0.0f);
  v[0].qfeSetPointElements(max.x,min.y,max.z);
  v[1].qfeSetPointElements(max.x,min.y,min.z);
  v[2].qfeSetPointElements(max.x,max.y,min.z);
  v[3].qfeSetPointElements(max.x,max.y,max.z);
  for (int i = 0; i < 4; i++)   {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }

  // Top
  glNormal3f(0.0f, -1.0f, 0.0f);
  v[0].qfeSetPointElements(min.x,min.y,min.z);
  v[1].qfeSetPointElements(max.x,min.y,min.z);
  v[2].qfeSetPointElements(max.x,min.y,max.z);
  v[3].qfeSetPointElements(min.x,min.y,max.z);
  for (int i=0; i<4; i++) {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }

  // Bottom
  glNormal3f(0.0f, 1.0f, 0.0f);
  v[0].qfeSetPointElements(max.x,max.y,min.z);
  v[1].qfeSetPointElements(min.x,max.y,min.z);
  v[2].qfeSetPointElements(min.x,max.y,max.z);
  v[3].qfeSetPointElements(max.x,max.y,max.z);
  for (int i = 0; i < 4; i++) {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }

  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeClampTextureRange(qfePoint pos, qfePoint &newpos) {
  if (pos.x < 0.5) newpos.x = std::max(pos.x, 0.0f);
  else             newpos.x = std::min(pos.x, 1.0f);
  if (pos.y < 0.5) newpos.y = std::max(pos.y, 0.0f);
  else             newpos.y = std::min(pos.y, 1.0f);
  if (pos.z < 0.5) newpos.z = std::max(pos.z, 0.0f);
  else             newpos.z = std::min(pos.z, 1.0f);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetStaticUniformLocations() {
  shaderIVR->qfeSetUniform1i("rayEnd"          , 6);
  shaderIVR->qfeSetUniform1i("rayEndDepth"     , 7);
  shaderIVR->qfeSetUniform1i("intersectColor"  , 8);
  shaderIVR->qfeSetUniform1i("intersectDepth"  , 9);
  shaderIVR->qfeSetUniform1i("gradients"       , 22);
  shaderIVR->qfeSetUniform1i("lsm"             , 23);
  shaderIVR->qfeSetUniform3f("dimensions"      , w, h, d);
  shaderIVR->qfeSetUniformMatrix4f("voxelPatientMatrix", 1, V2P);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeSetDynamicUniformLocations() {
  int       intersectAvailable = 0;
  qfePoint  lightSpec;
  qfeVector lightDir;

  if (glIsTexture(texIntersectColor) && glIsTexture(texIntersectDepth))
    intersectAvailable = 1;

  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 100.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if (light) {
    light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    light->qfeGetLightSourceSpecularPower(lightSpec.w); 
    light->qfeGetLightSourceDirection(lightDir);
  }

  // Provide parameters to the shader
  if (gradientSampling) {
    shaderIVR->qfeSetUniform1i("ivrDataSet", 22); // Assign gradients as the dataset to take samples from
    shaderIVR->qfeSetUniform2f("ivrDataRange", 0.f, maxGradLength);
    shaderIVR->qfeSetUniform1i("transferFunction", 5); // Assign gradient transfer function
  } else {
    shaderIVR->qfeSetUniform1i("ivrDataSet", 1); // Assign magnitudes as the dataset to take samples from
    shaderIVR->qfeSetUniform2f("ivrDataRange", paramDataRange[0], paramDataRange[1]);
    shaderIVR->qfeSetUniform1i("transferFunction", 4); // Assign usual transfer function
  }
  shaderIVR->qfeSetUniform3f("scaling"               , paramScaling[0], paramScaling[1], paramScaling[2]);
  shaderIVR->qfeSetUniform1f("stepSize"              , paramStepSize);
  shaderIVR->qfeSetUniform1i("intersectAvailable"    , intersectAvailable);
  shaderIVR->qfeSetUniform4f("light"                 , lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  shaderIVR->qfeSetUniform4f("lightDirection"        , lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  shaderIVR->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, matrixModelViewProjection);
  shaderIVR->qfeSetUniform1f("dirTransExp"           , dirTransExp);
  shaderIVR->qfeSetUniform1i("depthTransparency"     , depthTransparency);
  shaderIVR->qfeSetUniform1i("litSphereMapRendering" , litSphereMapRendering);
  shaderIVR->qfeSetUniform1i("sampleGradient"        , gradientSampling);
  shaderIVR->qfeSetUniform1f("maxGradLength"         , maxGradLength);
  shaderIVR->qfeSetUniform1i("depthToneMapping"      , depthToneMapping);
  shaderIVR->qfeSetUniform1f("depthToneMappingFactor", depthToneMappingFactor);
  shaderIVR->qfeSetUniform1f("simpleOpacity"         , simpleOpacity);
  shaderIVR->qfeSetUniform3f("eyePos", eyePos.x, eyePos.y, eyePos.z);

  float fVoiTInv[16];
  qfeMatrix4f::qfeGetMatrixElements(voiTInv, fVoiTInv);
  shaderIVR->qfeSetUniformMatrix4f("voiTInv"         , 1, fVoiTInv);
  shaderIVR->qfeSetUniform4f("voiCenter", voiCenter.x, voiCenter.y, voiCenter.z, voiCenter.w);
  shaderIVR->qfeSetUniform1i("useVoI", useVoIClipping);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeIllustrativeVolumeRendering::qfeGetModelViewMatrix() {
  qfeMatrix4f mv, p, mvp, mvinv, mvpinv;

  GLfloat modelview[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f::qfeSetMatrixElements(mv, modelview);

  GLfloat projection[16];
  glGetFloatv(GL_PROJECTION_MATRIX, projection);
  qfeMatrix4f::qfeSetMatrixElements(p, projection);

  mvp    = mv*p;
  qfeMatrix4f::qfeGetMatrixInverse(mvp, mvpinv);
  qfeMatrix4f::qfeGetMatrixInverse(mv, mvinv);

  qfeMatrix4f::qfeGetMatrixElements(mvinv,  this->matrixModelViewInverse);
  qfeMatrix4f::qfeGetMatrixElements(mvp,    this->matrixModelViewProjection);
  qfeMatrix4f::qfeGetMatrixElements(mvpinv, this->matrixModelViewProjectionInverse);

  return qfeSuccess;
}