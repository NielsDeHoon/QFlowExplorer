#pragma once

#include "qfeVector.h"
#include "qfeClusterNode.h"

/**
* \file   qfeClusterNodeElliptic.h
* \author Sander Jacobs
* \class  qfeClusterNodeElliptic
* \brief  Implementation of a cluster node using the Elliptic cluster metric proped by Telea et al.
* \note   Confidential
*/


class qfeClusterNodeElliptic : public qfeClusterNode
{
public:
  qfeClusterNodeElliptic(qfeClusterPoint point);
  ~qfeClusterNodeElliptic();

  qfeReturnStatus qfeMergeRepresentative(qfeClusterNode *c2);
  qfeFloat        qfeComputeDistanceTo(qfeClusterNode *c2);

  static double maxSpatialDistance;
  static double maxDirectionalCost;
  static double A;
  static double B;

private:
  qfeVector     meanPosition;
  qfeVector     meanFlow;
  unsigned int  size;
};





