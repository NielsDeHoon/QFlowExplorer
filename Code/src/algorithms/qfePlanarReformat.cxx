#include "qfePlanarReformat.h"
//----------------------------------------------------------------------------
qfePlanarReformat::qfePlanarReformat()
{
  this->shaderProgramPlaneStatic = new qfeGLShaderProgram();
  this->shaderProgramPlaneStatic->qfeAddShaderFromFile("/shaders/mpr/mpr-stat-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramPlaneStatic->qfeAddShaderFromFile("/shaders/mpr/mpr-func.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramPlaneStatic->qfeAddShaderFromFile("/shaders/mpr/mpr-stat-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramPlaneStatic->qfeBindAttribLocation("qfe_Vertex",    ATTRIB_VERTEX);
  this->shaderProgramPlaneStatic->qfeBindAttribLocation("qfe_TexCoord0", ATTRIB_TEXCOORD0);
  this->shaderProgramPlaneStatic->qfeBindAttribLocation("qfe_TexCoord1", ATTRIB_TEXCOORD1);
  this->shaderProgramPlaneStatic->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderProgramPlaneStatic->qfeLink();  

  this->shaderProgramPlaneDynamic = new qfeGLShaderProgram();
  this->shaderProgramPlaneDynamic->qfeAddShaderFromFile("/shaders/mpr/mpr-dyn-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramPlaneDynamic->qfeAddShaderFromFile("/shaders/mpr/mpr-func.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramPlaneDynamic->qfeAddShaderFromFile("/shaders/mpr/mpr-dyn-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramPlaneDynamic->qfeBindAttribLocation("qfe_Vertex",    ATTRIB_VERTEX);
  this->shaderProgramPlaneDynamic->qfeBindAttribLocation("qfe_TexCoord0", ATTRIB_TEXCOORD0);
  this->shaderProgramPlaneDynamic->qfeBindAttribLocation("qfe_TexCoord1", ATTRIB_TEXCOORD1);
  this->shaderProgramPlaneDynamic->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderProgramPlaneDynamic->qfeLink();

  this->shaderProgramPlaneUltrasound = new qfeGLShaderProgram();
  this->shaderProgramPlaneUltrasound->qfeAddShaderFromFile("/shaders/mpr/mpr-us-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramPlaneUltrasound->qfeAddShaderFromFile("/shaders/mpr/mpr-func.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramPlaneUltrasound->qfeAddShaderFromFile("/shaders/mpr/mpr-us-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderProgramPlaneUltrasound->qfeBindAttribLocation("qfe_Vertex",    ATTRIB_VERTEX);
  this->shaderProgramPlaneUltrasound->qfeBindAttribLocation("qfe_TexCoord0", ATTRIB_TEXCOORD0);
  this->shaderProgramPlaneUltrasound->qfeBindAttribLocation("qfe_TexCoord1", ATTRIB_TEXCOORD1);
  this->shaderProgramPlaneUltrasound->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderProgramPlaneUltrasound->qfeLink();

  this->shaderProgramPlaneArrowHeads = new qfeGLShaderProgram();
  this->shaderProgramPlaneArrowHeads->qfeAddShaderFromFile("/shaders/mpr/mpr-arrow-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderProgramPlaneArrowHeads->qfeAddShaderFromFile("/shaders/mpr/mpr-func.glsl", QFE_FRAGMENT_SHADER);  
  this->shaderProgramPlaneArrowHeads->qfeAddShaderFromFile("/shaders/mpr/mpr-arrow-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramPlaneArrowHeads->qfeAddShaderFromFile("/shaders/mpr/mpr-arrow-frag.glsl", QFE_FRAGMENT_SHADER); 
  this->shaderProgramPlaneArrowHeads->qfeBindAttribLocation("qfe_Vertex",    ATTRIB_VERTEX);
  this->shaderProgramPlaneArrowHeads->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderProgramPlaneArrowHeads->qfeLink();

  this->paramInterpolation        = qfePlanarReformatInterpolateLinear;
  this->paramPlaneBorderColor.r   = 0.952;
  this->paramPlaneBorderColor.g   = 0.674;
  this->paramPlaneBorderColor.b   = 0.133;
  this->paramPlaneBorderWidth     = 2.0;
  this->paramDataScale            = 100;
  this->paramDataComponent        = 0;
  this->paramDataComponent2       = 0;
  this->paramDataComponentCount   = 3;
  this->paramDataRepresentation   = 0;  
  this->paramDataRepresentation2  = 0;  
  this->paramDataRange1[0]        = 0.0;
  this->paramDataRange1[1]        = 255.0;  
  this->paramDataRange2[0]        = 0.0;
  this->paramDataRange2[1]        = 255.0;  
  this->paramTriggerTime          = 0.0;
  this->paramArrowSpacing         = 1.0;
  this->paramArrowScaling         = 1.0;
  this->paramBalance              = 0;
  this->paramSpeedThreshold       = 20;
  this->paramViewAngle            = 0;
  this->paramBrightness           = 0.0;
  this->paramContrast             = 0.0;
  this->paramContour              = true;
  this->paramFlip                 = false;
  this->paramPlaneOpacity         = 1.0;

  this->colorMapUS                = new qfeColorMap;
  this->colorMapUS2               = new qfeColorMap;

  this->qfeCreateColorMap();      
  this->qfeCreateTextures();

  this->texVolume1      = 0;
  this->texVolume2      = 0;
  this->texColorMap     = 0;
  this->texColorMapUS   = 0;
  this->texColorMapUS2  = 0;
};

//----------------------------------------------------------------------------
qfePlanarReformat::qfePlanarReformat(const qfePlanarReformat &pr)
{  
  this->shaderProgramPlaneStatic     = pr.shaderProgramPlaneStatic;
  this->shaderProgramPlaneDynamic    = pr.shaderProgramPlaneDynamic;
  this->shaderProgramPlaneUltrasound = pr.shaderProgramPlaneUltrasound;  
  this->shaderProgramPlaneArrowHeads = pr.shaderProgramPlaneArrowHeads;
  this->texVolume1                   = pr.texVolume1;
  this->texVolume2                   = pr.texVolume2;
  this->texColorMap                  = pr.texColorMap;
  this->texColorMapUS                = pr.texColorMapUS;
  this->texColorMapUS2               = pr.texColorMapUS2;

  this->paramInterpolation           = pr.paramInterpolation;
  this->paramPlaneBorderColor        = pr.paramPlaneBorderColor;
  this->paramPlaneBorderWidth        = pr.paramPlaneBorderWidth;
  this->paramPlaneNormal             = pr.paramPlaneNormal;
  this->paramPlaneDir                = pr.paramPlaneDir;
  this->paramDataScale               = pr.paramDataScale;
  this->paramDataComponent           = pr.paramDataComponent;
  this->paramDataComponent2          = pr.paramDataComponent2;
  this->paramDataComponentCount      = pr.paramDataComponentCount;
  this->paramDataRepresentation      = pr.paramDataRepresentation;  
  this->paramDataRepresentation2     = pr.paramDataRepresentation2;  
  this->paramDataRange1[0]           = pr.paramDataRange1[0];
  this->paramDataRange1[1]           = pr.paramDataRange1[1];
  this->paramDataRange2[0]           = pr.paramDataRange2[0];
  this->paramDataRange2[1]           = pr.paramDataRange2[1];
  this->paramTriggerTime             = pr.paramTriggerTime;  
  this->paramTotalTime               = pr.paramTotalTime;
  this->paramBalance                 = pr.paramBalance;
  this->paramSpeedThreshold          = pr.paramSpeedThreshold;
  this->paramViewAngle               = pr.paramViewAngle;
  this->paramBrightness              = pr.paramBrightness;
  this->paramContrast                = pr.paramContrast;
  this->paramContour                 = pr.paramContour;
  this->paramFlip                    = pr.paramFlip;
  this->paramPlaneOpacity            = pr.paramPlaneOpacity;

  this->paramArrowSpacing            = pr.paramArrowSpacing;
  this->paramArrowScaling            = pr.paramArrowScaling;

  for(int i=0; i<16; i++)
    this->paramT2P[i]                = pr.paramT2P[i];
};

//----------------------------------------------------------------------------
qfePlanarReformat::~qfePlanarReformat()
{
  delete this->shaderProgramPlaneStatic;
  delete this->shaderProgramPlaneDynamic;  
  delete this->shaderProgramPlaneUltrasound;
  delete this->shaderProgramPlaneArrowHeads;

  delete this->colorMapUS;
  delete this->colorMapUS2;

  this->qfeDeleteTextures();
};

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderPlane3D(qfeVolume *volume, qfeFrameSlice *plane, qfeColorMap *colormap)
{
  return this->qfeRenderPlane3D(volume, 1, plane, colormap); 
}

//----------------------------------------------------------------------------
// Frameslice properties in patient coordinates (mm)
qfeReturnStatus qfePlanarReformat::qfeRenderPlane3D(qfeVolume *volume, int phases, qfeFrameSlice *plane, qfeColorMap *colormap)
{
  qfeMatrix4f   P2W, M;
  qfeColorRGB   color;
  qfeFloat      pd;
  unsigned int  cc;
  qfeVector     x, y, n;

  if(plane == NULL) return qfeError;
  
  volume->qfeGetVolumeTextureId(this->texVolume1);
  volume->qfeGetVolumePhaseDuration(pd);
  volume->qfeGetVolumeComponentsPerVoxel(cc);

  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->texColorMap);    

  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, n.x, n.y, n.z);
  qfeVector::qfeVectorNormalize(n);  

  this->paramPlaneNormal        = n;
  this->paramPlaneDir           = y;
  this->paramTotalTime          = pd*phases;
  this->paramDataComponentCount = cc;

  color.r = color.g = color.b = 1.0;

  // Render the polygon
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  if(colormap != NULL) 
  {
    this->qfeBindTextures();
    this->qfeSetDynamicUniformLocations();      
    this->shaderProgramPlaneStatic->qfeEnable();

    this->qfeDrawPlane(plane, color);

    this->shaderProgramPlaneStatic->qfeDisable();
    this->qfeUnbindTextures();
  }
  else
  {
    this->qfeDrawPlane(plane, color);
  } 

  // Render the outline
  if(this->paramContour)
    this->qfeDrawLine(plane, this->paramPlaneBorderColor, this->paramPlaneBorderWidth);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Frameslice properties in patient coordinates (mm)
// vol1 - 2D cine qflow slice
// vol2 - 3D flow volume
qfeReturnStatus qfePlanarReformat::qfeRenderPlane3D(qfeVolume *vol1, qfeVolume *vol2, int phases, qfeFrameSlice *plane, qfeColorMap *colormap)
{
  qfeMatrix4f   P2W, M;
  qfeColorRGB   color;
  qfeFloat      pd;
  unsigned int  cc;
  qfeVector     x, y, n;

  if(plane == NULL) return qfeError;

  // Render the outline
  if(this->paramContour)
    this->qfeDrawLine(plane, this->paramPlaneBorderColor, this->paramPlaneBorderWidth);

  if(vol1 == NULL || vol2 == NULL) return qfeError;
  
  vol1->qfeGetVolumeTextureId(this->texVolume1);
  vol1->qfeGetVolumePhaseDuration(pd);
  vol1->qfeGetVolumeComponentsPerVoxel(cc);

  if(vol2 != NULL)
  {
    vol2->qfeGetVolumeTextureId(this->texVolume2);
  }

  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->texColorMap);    

  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, n.x, n.y, n.z);
  qfeVector::qfeVectorNormalize(n);  

  this->paramPlaneNormal        = n;
  this->paramPlaneDir           = y;
  this->paramTotalTime          = pd*phases;
  this->paramDataComponentCount = cc;

  color.r = color.g = color.b = 1.0;

  glEnable(GL_BLEND);

  // Render the polygon
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  if(colormap != NULL) 
  {
    this->qfeBindTextures();
    this->qfeSetDynamicUniformLocations();      
    this->shaderProgramPlaneStatic->qfeEnable();
    
    this->qfeDrawPlane(plane, vol2, color);

    this->shaderProgramPlaneStatic->qfeDisable();
    this->qfeUnbindTextures();
  }
  else
  {
    this->qfeDrawPlane(plane, vol2, color);
  } 

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Frameslice properties in patient coordinates (mm)
qfeReturnStatus qfePlanarReformat::qfeRenderPolygon3D(qfeVolume *volume, qfeFrameSlice *plane, qfeColorMap *colormap)
{
  return this->qfeRenderPolygon3D(volume, volume, plane, colormap);  
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderPolygon3D(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeColorMap *colormap)
{
  qfeColorRGB   color;
  unsigned int  cc, s[3];
  qfeVector     x, y, n;

  if(boundingVolume == NULL || plane == NULL) return qfeError;

  // Initialize global variables for shader  
  if(textureVolume != NULL)
  {
    textureVolume->qfeGetVolumeTextureId(this->texVolume1);
    textureVolume->qfeGetVolumeComponentsPerVoxel(cc);
    textureVolume->qfeGetVolumeSize(s[0], s[1], s[2]);

    boundingVolume->qfeGetVolumeTextureId(this->texVolume2);  
  }
  else
  {
    boundingVolume->qfeGetVolumeTextureId(this->texVolume1);
    boundingVolume->qfeGetVolumeComponentsPerVoxel(cc);
    boundingVolume->qfeGetVolumeSize(s[0], s[1], s[2]);
  }
    
  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->texColorMap);

  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, n.x, n.y, n.z);
  qfeVector::qfeVectorNormalize(n);  

  this->paramPlaneNormal        = n;
  this->paramPlaneDir           = y;
  this->paramDataComponentCount = cc;
  this->voxelSize.x             = 1.0f / (qfeFloat)s[0];
  this->voxelSize.y             = 1.0f / (qfeFloat)s[1];
  this->voxelSize.z             = 1.0f / (qfeFloat)s[2];

  color.r = color.g = color.b = 1.0;

  glEnable(GL_BLEND);

  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
    
  // Draw the polygon
  if(colormap != NULL)
  {
    this->qfeBindTextures();
    this->qfeSetDynamicUniformLocations();      
    this->shaderProgramPlaneDynamic->qfeEnable();

    this->qfeDrawPolygon(boundingVolume, textureVolume, plane, color);

    this->shaderProgramPlaneDynamic->qfeDisable();
    this->qfeUnbindTextures();  
  }
  else
  {
    this->qfeDrawPolygon(boundingVolume, textureVolume, plane, color);
  }  

  // Draw the outline  
  if(this->paramContour)
    this->qfeDrawLine(boundingVolume, plane, this->paramPlaneBorderColor, this->paramPlaneBorderWidth);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderPolygonUS2D(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius,  qfeColorMap *colormap)
{
  qfeColorRGB   color;
  unsigned int  cc, s[3];
  qfeVector     o, x, y, n, e;
 
  if(boundingVolume == NULL || plane == NULL) return qfeError;

  // Initialize global variables for shader  
  if(textureVolume != NULL)
  {
    textureVolume->qfeGetVolumeTextureId(this->texVolume1);
    textureVolume->qfeGetVolumeComponentsPerVoxel(cc);
    textureVolume->qfeGetVolumeSize(s[0], s[1], s[2]);

    boundingVolume->qfeGetVolumeTextureId(this->texVolume2);  
  }
  else
  {
    boundingVolume->qfeGetVolumeTextureId(this->texVolume1);
    boundingVolume->qfeGetVolumeComponentsPerVoxel(cc);
    boundingVolume->qfeGetVolumeSize(s[0], s[1], s[2]);
  }

  // Get the user defined color map
  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->texColorMap);

  // Get the doppler US color map 
  if(!glIsTexture(this->texColorMapUS))
  {
    this->colorMapUS->qfeUploadColorMap();
    this->colorMapUS->qfeGetColorMapTextureId(this->texColorMapUS);
  }
  if(!glIsTexture(this->texColorMapUS2))
  {
    this->colorMapUS2->qfeUploadColorMap();
    this->colorMapUS2->qfeGetColorMapTextureId(this->texColorMapUS2);
  }

  plane->qfeGetFrameOrigin(o.x, o.y, o.z);
  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, n.x, n.y, n.z);
  plane->qfeGetFrameExtent(e.x, e.y, e.z);
  qfeVector::qfeVectorNormalize(n);  

  this->paramPlaneNormal        = n;
  this->paramPlaneDir           = y;
  this->paramDataComponentCount = cc;
  this->voxelSize.x             = 1.0f / (qfeFloat)s[0];
  this->voxelSize.y             = 1.0f / (qfeFloat)s[1];
  this->voxelSize.z             = 1.0f / (qfeFloat)s[2];

  color.r = color.g = color.b = 1.0;

  // We force a basic geometry to render the slice
  GLfloat m[16];

  qfeMatrix4f T, R, M;
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  qfeMatrix4f::qfeSetMatrixIdentity(R);
  qfeMatrix4f::qfeSetMatrixIdentity(M);

  if(this->paramFlip)
  {
    x = -1.0f*x;
    y = -1.0f*y;
  }

  T(3,0) = o.x;
  T(3,1) = o.y;
  T(3,2) = o.z;

  R(0,0) = x.x; R(1,0) = y.x; R(2,0) = n.x;
  R(0,1) = x.y; R(1,1) = y.y; R(1,2) = y.z;
  R(0,2) = x.z; R(2,1) = n.y; R(2,2) = n.z;  

  qfeMatrix4f::qfeGetMatrixInverse(R*T, M);
  qfeMatrix4f::qfeGetMatrixElements(M, m);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(m);

  this->qfeGetModelViewMatrix();
  
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
    
  // Draw the polygon
  if(colormap != NULL)
  {
    this->qfeBindTextures();
    this->qfeSetDynamicUniformLocations();      
    this->shaderProgramPlaneUltrasound->qfeEnable();
    this->shaderProgramPlaneUltrasound->qfeEnable();

    this->qfeDrawPolygonUS(boundingVolume, textureVolume, plane, baseRadius, topRadius, color);

    this->shaderProgramPlaneUltrasound->qfeDisable();
    this->shaderProgramPlaneUltrasound->qfeDisable();
    this->qfeUnbindTextures();  
  }
  else
  {
    this->qfeDrawPolygonUS(boundingVolume, textureVolume, plane, baseRadius, topRadius, color);
  }  

  if(colormap != NULL) // bit of a hack here, nicer if the plane is rendered to the stencil buffer
  {
    this->qfeDrawLinePolygonUS(plane, baseRadius, topRadius, this->paramPlaneBorderColor, this->paramPlaneBorderWidth);
    this->qfeDrawTicksPolygonUS(plane, baseRadius, topRadius, this->paramPlaneBorderColor, this->paramPlaneBorderWidth);
  }

  glDisable(GL_BLEND);  

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderPolygonUS3D(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeColorMap *colormap)
{
    qfeColorRGB   color;
  unsigned int  cc, s[3];
  qfeVector     o, x, y, n, e;
 
  if(boundingVolume == NULL || plane == NULL) return qfeError;

  // Initialize global variables for shader  
  if(textureVolume != NULL)
  {
    textureVolume->qfeGetVolumeTextureId(this->texVolume1);
    textureVolume->qfeGetVolumeComponentsPerVoxel(cc);
    textureVolume->qfeGetVolumeSize(s[0], s[1], s[2]);

    boundingVolume->qfeGetVolumeTextureId(this->texVolume2);  
  }
  else
  {
    boundingVolume->qfeGetVolumeTextureId(this->texVolume1);
    boundingVolume->qfeGetVolumeComponentsPerVoxel(cc);
    boundingVolume->qfeGetVolumeSize(s[0], s[1], s[2]);
  }

  // Get the user defined color map
  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->texColorMap);

  // Get the doppler US color map 
  if(!glIsTexture(this->texColorMapUS))
  {
    this->colorMapUS->qfeUploadColorMap();
    this->colorMapUS->qfeGetColorMapTextureId(this->texColorMapUS);
  }
  if(!glIsTexture(this->texColorMapUS2))
  {
    this->colorMapUS2->qfeUploadColorMap();
    this->colorMapUS2->qfeGetColorMapTextureId(this->texColorMapUS2);
  }

  plane->qfeGetFrameOrigin(o.x, o.y, o.z);
  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, n.x, n.y, n.z);
  plane->qfeGetFrameExtent(e.x, e.y, e.z);
  qfeVector::qfeVectorNormalize(n);  

  this->paramPlaneNormal        = n;
  this->paramPlaneDir           = y;
  this->paramDataComponentCount = cc;
  this->voxelSize.x             = 1.0f / (qfeFloat)s[0];
  this->voxelSize.y             = 1.0f / (qfeFloat)s[1];
  this->voxelSize.z             = 1.0f / (qfeFloat)s[2];

  color.r = color.g = color.b = 1.0;

  // We force a basic geometry to render the slice
  GLfloat m[16];

  qfeMatrix4f T, R, M;
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  qfeMatrix4f::qfeSetMatrixIdentity(R);
  qfeMatrix4f::qfeSetMatrixIdentity(M);

  if(this->paramFlip)
  {
    x = -1.0f*x;
    y = -1.0f*y;
  }

  T(3,0) = o.x;
  T(3,1) = o.y;
  T(3,2) = o.z;

  R(0,0) = x.x; R(1,0) = y.x; R(2,0) = n.x;
  R(0,1) = x.y; R(1,1) = y.y; R(1,2) = y.z;
  R(0,2) = x.z; R(2,1) = n.y; R(2,2) = n.z;  

  qfeMatrix4f::qfeGetMatrixInverse(R*T, M);
  qfeMatrix4f::qfeGetMatrixElements(M, m);

  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
    
  // Draw the polygon
  if(colormap != NULL)
  {
    this->qfeBindTextures();
    this->qfeSetDynamicUniformLocations();      
    this->shaderProgramPlaneUltrasound->qfeEnable();

    this->qfeDrawPolygonUS2(boundingVolume, textureVolume, plane, baseRadius, topRadius, color);

    this->shaderProgramPlaneUltrasound->qfeDisable();
    this->qfeUnbindTextures();  
  }
  else
  {
    this->qfeDrawPolygonUS2(boundingVolume, textureVolume, plane, baseRadius, topRadius, color);
  }  

  //this->qfeDrawLinePolygonUS(plane, baseRadius, topRadius, this->paramPlaneBorderColor, this->paramPlaneBorderWidth);  

  glDisable(GL_BLEND);  

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderCircleUS2D(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeColorMap *colormap)
{
  qfeColorRGB   color;
  unsigned int  cc, s[3];
  qfeVector     o, x, y, n, t, e;  
  
  if(boundingVolume == NULL || plane == NULL) return qfeError;

  // Initialize global variables for shader  
  if(textureVolume != NULL)
  {
    textureVolume->qfeGetVolumeTextureId(this->texVolume1);
    textureVolume->qfeGetVolumeComponentsPerVoxel(cc);
    textureVolume->qfeGetVolumeSize(s[0], s[1], s[2]);

    boundingVolume->qfeGetVolumeTextureId(this->texVolume2);  
  }
  else
  {
    boundingVolume->qfeGetVolumeTextureId(this->texVolume1);
    boundingVolume->qfeGetVolumeComponentsPerVoxel(cc);
    boundingVolume->qfeGetVolumeSize(s[0], s[1], s[2]);
  }

  // Get the user defined color map
  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->texColorMap);

  // Get the doppler US color map 
  if(!glIsTexture(this->texColorMapUS))
  {
    this->colorMapUS->qfeUploadColorMap();
    this->colorMapUS->qfeGetColorMapTextureId(this->texColorMapUS);
  }
  if(!glIsTexture(this->texColorMapUS2))
  {
    this->colorMapUS2->qfeUploadColorMap();
    this->colorMapUS2->qfeGetColorMapTextureId(this->texColorMapUS2);
  }

  plane->qfeGetFrameOrigin(o.x, o.y, o.z);
  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, n.x, n.y, n.z);
  plane->qfeGetFrameExtent(e.x, e.y, e.z);
  qfeVector::qfeVectorNormalize(n);  

  this->paramPlaneNormal        = n;
  this->paramPlaneDir           = n;
  this->paramDataComponentCount = cc;
  this->voxelSize.x             = 1.0f / (qfeFloat)s[0];
  this->voxelSize.y             = 1.0f / (qfeFloat)s[1];
  this->voxelSize.z             = 1.0f / (qfeFloat)s[2];

  color.r = color.g = color.b = 1.0;

  // We force a basic geometry to render the slice
  GLfloat m[16];

  qfeMatrix4f T, R, M;
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  qfeMatrix4f::qfeSetMatrixIdentity(R);
  qfeMatrix4f::qfeSetMatrixIdentity(M);

  // Rotate the circle
  t = y;
  y = -1.0*x;
  x = t;

  T(3,0) = o.x;
  T(3,1) = o.y;
  T(3,2) = o.z;

  R(0,0) = x.x; R(1,0) = y.x; R(2,0) = n.x;
  R(0,1) = x.y; R(1,1) = y.y; R(1,2) = y.z;
  R(0,2) = x.z; R(2,1) = n.y; R(2,2) = n.z;  

  qfeMatrix4f::qfeGetMatrixInverse(R*T, M);
  qfeMatrix4f::qfeGetMatrixElements(M, m);
  
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(m);
  
  this->qfeGetModelViewMatrix();
  
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
   
  // Draw the polygon
  if(colormap != NULL)
  {
    this->qfeBindTextures();
    this->qfeSetDynamicUniformLocations();      
    this->shaderProgramPlaneUltrasound->qfeEnable();
    
    this->qfeDrawCircle(boundingVolume, textureVolume, plane); 

    this->shaderProgramPlaneUltrasound->qfeDisable();
    this->qfeUnbindTextures();  
  }
  else
  {
    this->qfeDrawCircle(boundingVolume, textureVolume, plane);
  }  

  if(colormap != NULL)
  {
    // Draw the outline  
    this->qfeDrawLineCircle(plane, this->paramPlaneBorderColor, this->paramPlaneBorderWidth);

    // Draw the tick marks
    this->qfeDrawTicksCircle(plane, this->paramPlaneBorderColor, this->paramPlaneBorderWidth);
  }

  glDisable(GL_BLEND);  
  glEnable(GL_DEPTH_TEST);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderOverlayUS2D(qfeVolume *boundingVolume, qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeFloat angle)
{
  qfeColorRGB   color;
  qfePoint      o, co;
  qfeVector     x, y, n, e;
  qfePoint      a[7], c[42];
  qfeFloat      radius, alpha, beta;
  qfeFloat      widthHead, widthBase;
  int           quality;
  
  if(boundingVolume == NULL || plane == NULL) return qfeError;

  plane->qfeGetFrameOrigin(o.x, o.y, o.z);
  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, n.x, n.y, n.z);
  plane->qfeGetFrameExtent(e.x, e.y, e.z);
  qfeVector::qfeVectorNormalize(n);  
  
  quality   = 40;
  color.r   = color.g = color.b = 1.0;
  widthHead = 3.0f;
  widthBase = 1.0f;

  // We force a basic geometry to render the slice
  GLfloat m[16];

  qfeMatrix4f T, R, M;
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  qfeMatrix4f::qfeSetMatrixIdentity(R);
  qfeMatrix4f::qfeSetMatrixIdentity(M);

  T(3,0) = o.x;
  T(3,1) = o.y;
  T(3,2) = o.z;

  R(0,0) = x.x; R(1,0) = y.x; R(2,0) = n.x;
  R(0,1) = x.y; R(1,1) = y.y; R(1,2) = y.z;
  R(0,2) = x.z; R(2,1) = n.y; R(2,2) = n.z;  

  qfeMatrix4f::qfeGetMatrixInverse(R*T, M);
  qfeMatrix4f::qfeGetMatrixElements(M, m);

  // Construct arrow
  if(this->paramFlip)
  {
    a[0]    = o + 0.666f*0.5f*e.y*y + widthBase*x;
    a[1]    = o + 0.444f*0.5f*e.y*y + widthBase*x;
    a[2]    = o + 0.444f*0.5f*e.y*y + widthHead*x;
    a[3]    = o + 0.333f*0.5f*e.y*y;
    a[4]    = o + 0.444f*0.5f*e.y*y - widthHead*x; 
    a[5]    = o + 0.444f*0.5f*e.y*y - widthBase*x;
    a[6]    = o + 0.666f*0.5f*e.y*y - widthBase*x;
  }
  else
  {
    a[0]    = o - 0.666f*0.5f*e.y*y - widthBase*x;
    a[1]    = o - 0.444f*0.5f*e.y*y - widthBase*x;
    a[2]    = o - 0.444f*0.5f*e.y*y - widthHead*x;
    a[3]    = o - 0.333f*0.5f*e.y*y;
    a[4]    = o - 0.444f*0.5f*e.y*y + widthHead*x; 
    a[5]    = o - 0.444f*0.5f*e.y*y + widthBase*x;
    a[6]    = o - 0.666f*0.5f*e.y*y + widthBase*x;
  }

  // Construct circle  
  alpha   = (angle/2.0f)*(float(PI)/180.0f);
  beta    = (float(2*PI) - 2.0*alpha)/float(quality);
  radius  = 0.4f*0.5f*e.x; 

  if(paramFlip)
  {
    co      = o - 0.5f*0.5f*e.y*y;

    c[0]    = co;
    c[1]    = co - y*cos(alpha)*radius + x*sin(alpha)*radius;
    for(int i=1; i<=quality; i++)
      c[i+1] = co - y*cos(alpha+i*beta)*radius + x*sin(alpha+i*beta)*radius;
  }
  else
  {
    co      = o + 0.5f*0.5f*e.y*y;

    c[0]    = co;
    c[1]    = co + y*cos(alpha)*radius + x*sin(alpha)*radius;
    for(int i=1; i<=quality; i++)
      c[i+1] = co + y*cos(alpha+i*beta)*radius + x*sin(alpha+i*beta)*radius;
  }

  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(m);

  glEnable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);   

  glLineWidth(this->paramPlaneBorderWidth);  
    
  glColor4f(this->paramPlaneBorderColor.r,this->paramPlaneBorderColor.g,this->paramPlaneBorderColor.b, 0.4);
  glBegin(GL_POLYGON);
    // Circle
    for(int i=0; i<=(quality+2); i++) glVertex3f(c[i%(quality+2)].x, c[i%(quality+2)].y, c[i%(quality+2)].z);    
  glEnd();  

  glColor4f(this->paramPlaneBorderColor.r,this->paramPlaneBorderColor.g,this->paramPlaneBorderColor.b, 1.0);
  glBegin(GL_LINE_STRIP);
    // Arrow
    for(int i=0; i<=7; i++) glVertex3f(a[i%7].x, a[i%7].y, a[i%7].z);       
  glEnd();
  glBegin(GL_LINE_STRIP);
    // Circle
    for(int i=0; i<=(quality+2); i++) glVertex3f(c[i%(quality+2)].x, c[i%(quality+2)].y, c[i%(quality+2)].z);
  glEnd();
  
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_BLEND);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderPolygonLine3D(qfeVolume *volume, qfeFrameSlice *plane)
{
  if(volume == NULL || plane == NULL) return qfeError;
  
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
    
  // Draw the outline  
  this->qfeDrawLine(volume, plane, this->paramPlaneBorderColor, this->paramPlaneBorderWidth);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// 3D is a bad naming convention here
// It serves just to distinguish from the 2D projection
qfeReturnStatus qfePlanarReformat::qfeRenderCircle3D(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeColorMap *colormap)
{
  unsigned int  cc, s[3];
  qfeVector     x, y, n;

  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, n.x, n.y, n.z);
  qfeVector::qfeVectorNormalize(n);  

  this->paramPlaneNormal  = n;
  this->paramPlaneDir     = y;

  if(boundingVolume == NULL || plane == NULL) return qfeError;

  // Initialize global variables for shader
  if(textureVolume != NULL)
  {
    textureVolume->qfeGetVolumeTextureId(this->texVolume1);
    textureVolume->qfeGetVolumeComponentsPerVoxel(cc);
    textureVolume->qfeGetVolumeSize(s[0], s[1], s[2]);

    boundingVolume->qfeGetVolumeTextureId(this->texVolume2);  
  }
  else
  {
    boundingVolume->qfeGetVolumeTextureId(this->texVolume1);
    boundingVolume->qfeGetVolumeComponentsPerVoxel(cc);
    boundingVolume->qfeGetVolumeSize(s[0], s[1], s[2]);
  }

  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->texColorMap);

  // Get the doppler US color map 
  if(!glIsTexture(this->texColorMapUS))
  {
    this->colorMapUS->qfeUploadColorMap();
    this->colorMapUS->qfeGetColorMapTextureId(this->texColorMapUS);
  }
  if(!glIsTexture(this->texColorMapUS2))
  {
    this->colorMapUS2->qfeUploadColorMap();
    this->colorMapUS2->qfeGetColorMapTextureId(this->texColorMapUS2);
  }

  this->paramPlaneNormal        = n;
  this->paramPlaneDir           = n;
  this->paramDataComponentCount = cc;
  this->voxelSize.x = 1.0f / (qfeFloat)s[0];
  this->voxelSize.y = 1.0f / (qfeFloat)s[1];
  this->voxelSize.z = 1.0f / (qfeFloat)s[2];

  // Render the polygon
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  if(colormap != NULL)
  {
    this->qfeBindTextures();    

    if(this->paramDataRepresentation == 2)
    {
      this->paramDataRepresentation = 0;
      this->qfeSetDynamicUniformLocations();      
      this->shaderProgramPlaneUltrasound->qfeEnable();
      this->qfeDrawCircle(boundingVolume, textureVolume, plane);
      this->shaderProgramPlaneUltrasound->qfeDisable();
      this->paramDataRepresentation = 2;
    }
    else
    {
      this->qfeSetDynamicUniformLocations();      
      this->shaderProgramPlaneDynamic->qfeEnable();
      this->qfeDrawCircle(boundingVolume, textureVolume, plane);
      this->shaderProgramPlaneDynamic->qfeDisable();
    }     

    this->qfeUnbindTextures();      
  }
  else
  {
    this->qfeDrawCircle(boundingVolume, textureVolume, plane);
  }

  this->qfeDrawLineCircle(plane, this->paramPlaneBorderColor, this->paramPlaneBorderWidth);
  
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderClipBox3D(qfeVolume* volume, qfeFrameSlice *plane, qfeFloat size, qfeFloat depth)
{
  // Create a local plane, and set the origin close to the middle
  // We ensure that the box actually clips the whole volume
  // The origin is taken closest to the origin of the volume
  qfeGrid       *grid;
  qfePoint       go, po;
  qfeVector      pdx, pdy, pn, a[3];
  qfeFrameSlice  clipBoxPlane;

  clipBoxPlane = *plane;

  volume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridOrigin(go.x,go.y,go.x);
  grid->qfeGetGridAxes(a[0].x,a[0].y,a[0].z, a[1].x,a[1].y,a[1].z, a[2].x,a[2].y,a[2].z);
  clipBoxPlane.qfeGetFrameAxes(pdx.x,pdx.y,pdx.z,pdy.x,pdy.y,pdy.z,pn.x,pn.y,pn.z);
  clipBoxPlane.qfeGetFrameOrigin(po.x,po.y,po.z);     

  po   = go + ((po-go)*pn)*pn;

  a[0] = a[0] - (a[0]*pn)*pn;
  a[1] = a[1] - (a[1]*pn)*pn;
  a[2] = a[2] - (a[2]*pn)*pn;

  pdx  = max(max(a[0],a[1]),a[2]);

  // serious hack here
  pn   = -1.0f*pn;

  clipBoxPlane.qfeSetFrameOrigin(po.x,po.y,po.z);
  clipBoxPlane.qfeSetFrameAxes(pdx.x,pdx.y,pdx.z, pdy.x, pdy.y, pdy.z, pn.x,pn.y,pn.z);

  return this->qfeRenderClipBox3D(&clipBoxPlane, size, depth);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderClipBox3D(qfeFrameSlice *plane, qfeFloat size, qfeFloat depth)
{
  qfeMatrix4f   P2W, M;
  qfeColorRGB   color;
  qfePoint      o, e, p, oNew;
  qfeVector     x, y, z;
  unsigned char d;

  qfeVolume    *proxyVolume = new qfeVolume();
  qfeGrid      *proxyGrid   = new qfeGrid();  

 // qfeFrameSlice *front, *back, *left, *right, *top, *bottom;

  if(plane == NULL) return qfeError;

  plane->qfeGetFrameOrigin(o.x, o.y, o.z);
  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);
  plane->qfeGetFrameExtent(e.x, e.y, e.z);

  qfeVector::qfeVectorNormalize(x);
  qfeVector::qfeVectorNormalize(z);
  qfeVector::qfeVectorOrthonormalBasis1(z, x, y);

  //z = -1.0*z;
  d = 1;
  p = o + 0.5*depth*z;
 
  proxyGrid->qfeSetGridAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);
  proxyGrid->qfeSetGridOrigin(p.x,p.y,p.z);
  proxyGrid->qfeSetGridExtent(size, size, depth);  

  proxyVolume->qfeSetVolumeData(qfeValueTypeGrey, 1,1,1, &d);
  proxyVolume->qfeSetVolumeGrid(proxyGrid);

  color.r = 1.0;
  color.g = color.b = 0.0;

  // Render the box
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
 
  this->qfeRenderBoundingBox(proxyVolume);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix(); 

  delete proxyVolume;
  delete proxyGrid;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderArrowHeads3D(qfeVolume *volume, qfeFrameSlice *plane, qfeFloat spacing, qfeFloat scaling, qfeColorMap *colormap, int transparency)
{
  qfeMatrix4f   P2W, V2P, T2V, MVP, F;
  qfeColorRGBA  color;
  qfeVector     x, y, n;  
  qfeFloat      f[16]; // Unintended, but funny :-)
  unsigned int  cc;

  if(volume == NULL || plane==NULL) return qfeError;

  volume->qfeGetVolumeTextureId(this->texVolume1);  
  volume->qfeGetVolumeComponentsPerVoxel(cc);
  
  color.r = 1.0;
  color.g = color.b = 0.0;
  color.a = 1.0-(transparency/100.0);

  qfeTransform::qfeGetMatrixPatientToWorld(P2W);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, n.x, n.y, n.z);
  qfeVector::qfeVectorNormalize(n);  

  this->paramPlaneNormal        = n;
  this->paramPlaneDir           = y;
  this->paramArrowSpacing       = spacing;
  this->paramArrowScaling       = scaling;  
  this->paramDataComponentCount = cc;
  
  qfeMatrix4f::qfeGetMatrixElements(T2V*V2P, &this->paramT2P[0]);

  this->qfeGetModelViewMatrix();

  if(this->paramFlip)
  {
    f[0]  = -1.0; f[1]  =  0.0; f[2]  = 0.0; f[3]  =  0.0;
    f[4]  =  0.0; f[5]  = -1.0; f[6]  = 0.0; f[7]  =  0.0;
    f[8]  =  0.0; f[9]  =  0.0; f[10] = 1.0; f[11] =  0.0;
    f[12] =  0.0; f[13] =  0.0; f[14] = 0.0; f[15] =  1.0;

    qfeMatrix4f::qfeSetMatrixElements(F, f);
    qfeMatrix4f::qfeSetMatrixElements(MVP, this->matrixModelViewProjection);
    MVP = MVP*F;
    qfeMatrix4f::qfeGetMatrixElements(MVP, &this->matrixModelViewProjection[0]);
  }

  // Render the point samples
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  //glMultMatrixf(this->paramT2P);

  this->qfeBindTextures();
  this->qfeSetDynamicUniformLocations();      
  this->shaderProgramPlaneArrowHeads->qfeEnable();

  this->qfeDrawPointSamples(volume, plane, color, spacing);

  this->shaderProgramPlaneArrowHeads->qfeDisable();
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderAxes(qfeFrameSlice *plane)
{
  qfePoint      o, p;
  qfeVector     x, y, n, e;
 
  plane->qfeGetFrameOrigin(o.x, o.y, o.z);
  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, n.x, n.y, n.z);
  plane->qfeGetFrameExtent(e.x, e.y, e.z);
  qfeVector::qfeVectorNormalize(n);  
 
  // We force a basic geometry to render the slice
  GLfloat m[16];

  qfeMatrix4f T, R, M;
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  qfeMatrix4f::qfeSetMatrixIdentity(R);
  qfeMatrix4f::qfeSetMatrixIdentity(M);

  T(3,0) = o.x;
  T(3,1) = o.y;
  T(3,2) = o.z;

  R(0,0) = x.x; R(1,0) = y.x; R(2,0) = n.x;
  R(0,1) = x.y; R(1,1) = y.y; R(1,2) = y.z;
  R(0,2) = x.z; R(2,1) = n.y; R(2,2) = n.z;  

  qfeMatrix4f::qfeGetMatrixInverse(R*T, M);
  qfeMatrix4f::qfeGetMatrixElements(M, m);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(m);

  glDisable(GL_DEPTH_TEST);
    
  glBegin(GL_LINES);

  p = o + 10.0f*x;
  glColor3f(1.0,0.0,0.0);
  glVertex3f(o.x, o.y, o.z);
  glVertex3f(p.x, p.y, p.z);

  p = o + 10.0f*y;
  glColor3f(0.0,1.0,0.0);
  glVertex3f(o.x, o.y, o.z);
  glVertex3f(p.x, p.y, p.z);

  p = o + 10.0f*n;
  glColor3f(0.0,0.0,1.0);
  glVertex3f(o.x, o.y, o.z);
  glVertex3f(p.x, p.y, p.z);

  glEnd();

  glEnable(GL_DEPTH_TEST);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Draw a plane, with properties defined in patient coordinates (mm)
qfeReturnStatus qfePlanarReformat::qfeDrawPlane(qfeFrameSlice *plane, qfeColorRGB color)
{
  // Compute the polygon
  qfePoint   *polygon = NULL;
  int         count;

  this->qfeComputePlane(plane, &polygon, count); 

  this->qfeDrawPlane(polygon, count, color);

  delete [] polygon;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Draw a plane, with properties defined in patient coordinates (mm)
qfeReturnStatus qfePlanarReformat::qfeDrawPlane(qfeFrameSlice *plane, qfeVolume *volume, qfeColorRGB color)
{
  // Compute the polygon
  qfePoint   *polygon = NULL;
  int         count;

  this->qfeComputePlane(plane, &polygon, count); 

  this->qfeDrawPlane(polygon, volume, count, color);

  delete [] polygon;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPolygon(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeColorRGB color)
{
  qfeMatrix4f P2V, V2T, P2T1, P2T2;
  qfePoint   *polygon = NULL;
  int         count;

  if(boundingVolume == NULL || textureVolume == NULL || plane == NULL) return qfeError;

  // Selected volume
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, textureVolume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, textureVolume);
  P2T1 = P2V*V2T;  

  // PCA-P volume
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, boundingVolume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, boundingVolume);
  P2T2 = P2V*V2T;  

  this->qfeComputePolygon(boundingVolume, plane, &polygon, count); 
 
  this->qfeDrawPolygon(polygon, count, color, P2T1, P2T2);

  delete [] polygon;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPolygonUS(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeColorRGB color)
{
  qfeMatrix4f P2V, V2T, P2T1, P2T2;
  qfePoint   *polygon = NULL;
  int         count;

  if(boundingVolume == NULL || textureVolume == NULL || plane == NULL) return qfeError;

  // Selected volume
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, textureVolume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, textureVolume);
  P2T1 = P2V*V2T;  

  // PCA-P volume
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, boundingVolume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, boundingVolume);
  P2T2 = P2V*V2T;  

  this->qfeComputePolygonUS(plane, baseRadius, topRadius, &polygon, count); 
 
  this->qfeDrawPolygon(polygon, count, color, P2T1, P2T2);

  delete [] polygon;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPolygonUS2(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeColorRGB color)
{
    qfeMatrix4f P2V, V2T, P2T1, P2T2;
  qfePoint   *polygon = NULL;
  int         count;

  if(boundingVolume == NULL || textureVolume == NULL || plane == NULL) return qfeError;

  // Selected volume
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, textureVolume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, textureVolume);
  P2T1 = P2V*V2T;  

  // PCA-P volume
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, boundingVolume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, boundingVolume);
  P2T2 = P2V*V2T;  

  this->qfeComputePolygonUS2(plane, baseRadius, topRadius, &polygon, count); 
 
  this->qfeDrawPolygon(polygon, count, color, P2T1, P2T2);

  delete [] polygon;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawCircle(qfeVolume *boundingVolume, qfeVolume* textureVolume, qfeFrameSlice *plane)
{
  qfeMatrix4f P2V, V2T, P2T1, P2T2;
  qfePoint   *polygon = NULL;
  int         count;
  qfeColorRGB color;
  qfePoint    origin;
  qfeVector   axisx, axisy, normal;
  qfeFloat    extent[3], radius;

  if(boundingVolume == NULL || textureVolume == NULL || plane == NULL) return qfeError;

  color.r = 1.0;
  color.g = color.b = 0.0;

  plane->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  plane->qfeGetFrameAxes(axisx.x, axisx.y, axisx.z, axisy.x, axisy.y, axisy.z, normal.x, normal.y, normal.z);
  plane->qfeGetFrameExtent(extent[0], extent[1], extent[2]);

  qfeVector::qfeVectorNormalize(axisx);
  qfeVector::qfeVectorNormalize(axisy);
  qfeVector::qfeVectorNormalize(normal);

  radius = max(extent[0], extent[1])/2.0f; 

  // Selected volume
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, textureVolume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, textureVolume);
  P2T1 = P2V*V2T;  

  // PCA-P volume
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, boundingVolume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, boundingVolume);
  P2T2 = P2V*V2T;  

  this->qfeComputeCircle(origin, normal, radius, &polygon, count); 
 
  this->qfeDrawPolygon(polygon, count, color, P2T1, P2T1);

  delete [] polygon;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawLine(qfeVolume *volume, qfeFrameSlice *plane, qfeColorRGB color, int lineWidth)
{
  qfePoint   *polygon = NULL;
  int         count;

  this->qfeComputePolygon(volume, plane, &polygon, count);
  
  this->qfeDrawLine(polygon, count, color, lineWidth);

  delete [] polygon;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawLine(qfeFrameSlice *plane, qfeColorRGB color, int lineWidth)
{
  qfePoint   *polygon = NULL;
  int         count;

  this->qfeComputePlane(plane, &polygon, count);
  
  this->qfeDrawLine(polygon, count, color, lineWidth);

  delete [] polygon;

  return qfeSuccess;  
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawLineCircle(qfeFrameSlice *plane, qfeColorRGB color, int lineWidth)
{
  qfePoint   *polygon = NULL;
  int         count;
  qfePoint    origin;
  qfeVector   axisx, axisy, normal;
  qfeFloat    extent[3], radius;

  plane->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  plane->qfeGetFrameAxes(axisx.x, axisx.y, axisx.z, axisy.x, axisy.y, axisy.z, normal.x, normal.y, normal.z);
  plane->qfeGetFrameExtent(extent[0], extent[1], extent[2]);

  qfeVector::qfeVectorNormalize(normal);

  radius = max(extent[0], extent[1])/2.0f;

  this->qfeComputeCircle(origin, normal, radius, &polygon, count); 
 
  this->qfeDrawLine(polygon, count, color, lineWidth);

  delete [] polygon;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawTicksCircle(qfeFrameSlice *plane, qfeColorRGB color, int lineWidth)
{
  qfePoint    origin, p1, p2;
  qfeVector   dir;
  qfeVector   axisx, axisy, normal;
  qfeFloat    extent[3], radius, angle, arclength;
  qfeColorRGB colorTick;

  plane->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  plane->qfeGetFrameAxes(axisx.x, axisx.y, axisx.z, axisy.x, axisy.y, axisy.z, normal.x, normal.y, normal.z);
  plane->qfeGetFrameExtent(extent[0], extent[1], extent[2]);

  qfeVector::qfeVectorNormalize(normal);

  arclength = 10.0f; 
  radius    = max(extent[0], extent[1])/2.0f;
  angle     = (arclength / radius);

  colorTick.r = 0.0f;
  colorTick.g = 0.8f;
  colorTick.b = 0.0f;

  p1 = origin + (radius + 4.0f) * axisy;    

  this->qfeDrawCircleTick(p1, 2.0, normal, colorTick);

  glLineWidth(lineWidth);  
  glColor3f(color.r, color.g, color.b);
  glBegin(GL_LINES);
  for(int i=1; i<(int)2*PI/angle; i++)
  {
    dir = axisx*cos(i*angle + 0.5f*PI) + axisy*sin(i*angle + 0.5f*PI);
    
    p1 = origin + (radius + 1.0f) * dir;
    p2 = origin + (radius + 3.0f) * dir;

    glVertex3f(p1.x, p1.y, p1.z);
    glVertex3f(p2.x, p2.y, p2.z);
  } 
  glEnd();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawLinePolygonUS(qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeColorRGB color, int lineWidth)
{
  qfePoint   *polygon = NULL;
  int         count;
  //qfePoint    origin;
  //qfeVector   axisx, axisy, normal;
  //qfeFloat    extent[2], radius;

  //plane->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  //plane->qfeGetFrameAxes(axisx.x, axisx.y, axisx.z, axisy.x, axisy.y, axisy.z, normal.x, normal.y, normal.z);
  //plane->qfeGetFrameExtent(extent[0], extent[1]);

  //qfeVector::qfeVectorNormalize(normal);

  //radius = max(extent[0], extent[1])/2.0f;

  this->qfeComputePolygonUS(plane, baseRadius, topRadius, &polygon, count); 
 
  this->qfeDrawLine(polygon, count, color, lineWidth);

  delete [] polygon;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawTicksPolygonUS(qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeColorRGB color, int lineWidth)
{
  qfePoint    origin, o, p0, p1, p2;
  qfeVector   d, bn;
  qfeVector   axisx, axisy, normal;
  qfeFloat    extent[3], arcRadius, nrticks;
  qfeColorRGB colorTick;
  qfeMatrix4f T;

  plane->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  plane->qfeGetFrameAxes(axisx.x, axisx.y, axisx.z, axisy.x, axisy.y, axisy.z, normal.x, normal.y, normal.z);
  plane->qfeGetFrameExtent(extent[0], extent[1], extent[2]);

  qfeVector::qfeVectorNormalize(normal);

  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(0,0) = axisx.x;  T(0,1) = axisx.y;  T(0,2) = axisx.z;
  T(1,0) = axisy.x;  T(1,1) = axisy.y;  T(1,2) = axisy.z;
  T(2,0) = normal.x; T(2,1) = normal.y; T(2,2) = normal.z;

  colorTick.r = 0.0f;
  colorTick.g = 0.8f;
  colorTick.b = 0.0f;

  nrticks = sqrt(pow(topRadius-baseRadius, 2.0f) + pow(extent[1], 2.0f)) / 10.0f;

  d.qfeSetVectorElements(topRadius-baseRadius, extent[1], 0.0);  
  d = d*T;
  qfeVector::qfeVectorNormalize(d);
  qfeVector::qfeVectorOrthonormalBasis1(normal, d, bn);

  arcRadius = sqrt(pow(topRadius,2.0f)+ pow(0.5f*extent[1],2.0f));
  o  = origin - (-0.25f*extent[1]+0.5f*arcRadius)*axisy;
  
  p0 = o + (0.5f*(baseRadius+topRadius) + 4.0f) * axisx;    

  this->qfeDrawCircleTick(p0, 2.0, normal, colorTick);

  for(int i = 1; i<=(int)floor(nrticks/2.0f); i++)
  {
    p1 = p0 + i*10.f*d;
    p2 = p0 + i*10.f*d + 3.0f*bn;    
    this->qfeDrawLineTick(p1, p2, this->paramPlaneBorderWidth, this->paramPlaneBorderColor);
  }
  for(int i = 1; i<=(int)floor(nrticks/2.0f); i++)
  {
    p1 = p0 - i*10.f*d;
    p2 = p0 - i*10.f*d + 3.0f*bn;
    this->qfeDrawLineTick(p1, p2, this->paramPlaneBorderWidth, this->paramPlaneBorderColor);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPointSamples(qfeVolume *volume, qfeFrameSlice *plane, qfeColorRGBA color, qfeFloat spacing)
{
  qfePoint   *polygon = NULL;
  int         count;

  this->qfeComputePointSamples(volume, plane, spacing, &polygon, count);

  this->qfeDrawPointSamples(polygon, count, color);

  delete [] polygon;

  return qfeSuccess;  
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawCircleTick(qfePoint p, qfeFloat r, qfeVector n, qfeColorRGB color)
{  
  qfeVector x, y;
  qfeVector::qfeVectorOrthonormalBasis2(n, x, y);  
  
  glColor3f(color.r, color.g, color.b);
  glBegin(GL_TRIANGLE_FAN);
    glVertex3f(p.x, p.y, p.z);
    for(int angle = 0; angle <= 360; angle += 5)
    {
      qfePoint pos = p + (sin(angle * (float(PI)/180.0f)) * r) * x + (cos(angle * (float(PI)/180.0f)) * r) * y;      
      glVertex3f(pos.x, pos.y, pos.z);
    }
  glEnd();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawLineTick(qfePoint p1, qfePoint p2, qfeFloat lineWidth, qfeColorRGB color)
{
  glLineWidth(lineWidth);
  glColor3f(color.r, color.g, color.b);
  glBegin(GL_LINES);
    glVertex3f(p1.x, p1.y, p1.z);
    glVertex3f(p2.x, p2.y, p2.z);  
  glEnd();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// \brief Draws the plane in patient coordinates
//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPlane(qfePoint *plane, int pointCount, qfeColorRGB color)
{
  if(pointCount < 4) return qfeError;

  // Create the vertex buffer
  GLuint         planeStatVBO;
  qfePlaneVertex buffer[4];

  buffer[0].x  = plane[0].x; buffer[0].y  = plane[0].y; buffer[0].z  = plane[0].z;
  buffer[0].s0 =        0.0; buffer[0].t0 =        0.0; buffer[0].u0 =      0.225;
  buffer[1].x  = plane[1].x; buffer[1].y  = plane[1].y; buffer[1].z  = plane[1].z;
  buffer[1].s0 =        1.0; buffer[1].t0 =        0.0; buffer[1].u0 =      0.225;
  buffer[2].x  = plane[2].x; buffer[2].y  = plane[2].y; buffer[2].z  = plane[2].z;
  buffer[2].s0 =        1.0; buffer[2].t0 =        1.0; buffer[2].u0 =      0.225;
  buffer[3].x  = plane[3].x; buffer[3].y  = plane[3].y; buffer[3].z  = plane[3].z;
  buffer[3].s0 =        0.0; buffer[3].t0 =        1.0; buffer[3].u0 =      0.225;

  glGenBuffers(1, &planeStatVBO);
  glBindBuffer(GL_ARRAY_BUFFER, planeStatVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(qfePlaneVertex)*4, &buffer[0].x, GL_STATIC_DRAW);

  // Render the polygon  
  glEnableVertexAttribArray(ATTRIB_VERTEX);  
  glEnableVertexAttribArray(ATTRIB_TEXCOORD0);

  glVertexAttribPointer(ATTRIB_VERTEX,     3, GL_FLOAT, GL_FALSE, sizeof(qfePlaneVertex), BUFFER_OFFSET(0));
  glVertexAttribPointer(ATTRIB_TEXCOORD0,  3, GL_FLOAT, GL_FALSE, sizeof(qfePlaneVertex), BUFFER_OFFSET(3*sizeof(float)));
    
  glDrawArrays(GL_POINTS, 0, 4);

  glDisableVertexAttribArray(ATTRIB_VERTEX); 
  glDisableVertexAttribArray(ATTRIB_TEXCOORD0); 

  // Delete the buffer
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDeleteBuffers(1, &planeStatVBO);

  //glBegin(GL_QUADS);
  //  glTexCoord3f( 0.0, 0.0, 0.225);                
  //  glVertex3f(plane[0].x, plane[0].y, plane[0].z);
  //  glTexCoord3f( 1.0, 0.0, 0.225);                
  //  glVertex3f(plane[1].x, plane[1].y, plane[1].z);
  //  glTexCoord3f( 1.0, 1.0, 0.225);                
  //  glVertex3f(plane[2].x, plane[2].y, plane[2].z);
  //  glTexCoord3f( 0.0, 1.0, 0.225);                
  //  glVertex3f(plane[3].x, plane[3].y, plane[3].z);
  //glEnd(); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// \brief Draws the plane in patient coordinates
//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPlane(qfePoint *plane, qfeVolume *volume, int pointCount, qfeColorRGB color)
{
  if(pointCount < 4) return qfeError;
  if(volume == NULL) return qfeError;
  
  qfeMatrix4f P2V, V2T;
  qfePoint    t[4]; 

  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);

  // Compute texture coordinates for the volume
  t[0] = plane[0] * (P2V*V2T);
  t[1] = plane[1] * (P2V*V2T);
  t[2] = plane[2] * (P2V*V2T);
  t[3] = plane[3] * (P2V*V2T);
   
  // Create the vertex buffer
  GLuint         planeStatVBO;
  qfePlaneVertex buffer[4];

  buffer[0].x  = plane[0].x; buffer[0].y  = plane[0].y; buffer[0].z  = plane[0].z;
  buffer[0].s0 =        0.0; buffer[0].t0 =        0.0; buffer[0].u0 =      0.225;
  buffer[0].s1 =     t[0].x; buffer[0].t1 =     t[0].y; buffer[0].u1 =     t[0].z;
  buffer[1].x  = plane[1].x; buffer[1].y  = plane[1].y; buffer[1].z  = plane[1].z;
  buffer[1].s0 =        1.0; buffer[1].t0 =        0.0; buffer[1].u0 =      0.225;
  buffer[1].s1 =     t[1].x; buffer[1].t1 =     t[1].y; buffer[1].u1 =     t[1].z;
  buffer[2].x  = plane[2].x; buffer[2].y  = plane[2].y; buffer[2].z  = plane[2].z;
  buffer[2].s0 =        1.0; buffer[2].t0 =        1.0; buffer[2].u0 =      0.225;
  buffer[2].s1 =     t[2].x; buffer[2].t1 =     t[2].y; buffer[2].u1 =     t[2].z;
  buffer[3].x  = plane[3].x; buffer[3].y  = plane[3].y; buffer[3].z  = plane[3].z;
  buffer[3].s0 =        0.0; buffer[3].t0 =        1.0; buffer[3].u0 =      0.225;
  buffer[3].s1 =     t[3].x; buffer[3].t1 =     t[3].y; buffer[3].u1 =     t[3].z;

  glGenBuffers(1, &planeStatVBO);
  glBindBuffer(GL_ARRAY_BUFFER, planeStatVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(qfePlaneVertex)*4, &buffer[0].x, GL_STATIC_DRAW);

  // Render the polygon
  glEnableVertexAttribArray(ATTRIB_VERTEX);  
  glEnableVertexAttribArray(ATTRIB_TEXCOORD0);
  glEnableVertexAttribArray(ATTRIB_TEXCOORD1);

  glVertexAttribPointer(ATTRIB_VERTEX,     3, GL_FLOAT, GL_FALSE, sizeof(qfePlaneVertex), BUFFER_OFFSET(0));
  glVertexAttribPointer(ATTRIB_TEXCOORD0,  3, GL_FLOAT, GL_FALSE, sizeof(qfePlaneVertex), BUFFER_OFFSET(3*sizeof(float)));
  glVertexAttribPointer(ATTRIB_TEXCOORD1,  3, GL_FLOAT, GL_FALSE, sizeof(qfePlaneVertex), BUFFER_OFFSET(6*sizeof(float)));
    
  glDrawArrays(GL_QUADS, 0, 4);

  glDisableVertexAttribArray(ATTRIB_VERTEX); 
  glDisableVertexAttribArray(ATTRIB_TEXCOORD0); 
  glDisableVertexAttribArray(ATTRIB_TEXCOORD1);

  // Delete the buffer
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDeleteBuffers(1, &planeStatVBO);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// \brief Draws the reformat plane as polygon
//        Polygon in texture coordinates
//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPolygon(qfePoint *polygon, int pointCount, qfeColorRGB color)
{  
  glColor3f(color.r, color.g, color.b);

  if(pointCount == 0) return qfeError;

  glBegin(GL_POLYGON);
  for(int i=0; i<pointCount+1;i++)
  {
    glTexCoord3f( polygon[i%pointCount].x,
                  polygon[i%pointCount].y,
                  polygon[i%pointCount].z);
    glVertex3f(   polygon[i%pointCount].x,
                  polygon[i%pointCount].y,
                  polygon[i%pointCount].z);
  }
  glEnd(); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// \brief Draws the reformat plane as polygon
//        Polygon in patient coordinates
//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPolygon(qfePoint *polygon, int pointCount, qfeColorRGB color, qfeMatrix4f p2t)
{  
  return this->qfeDrawPolygon(polygon, pointCount, color, p2t, p2t);  
}


//----------------------------------------------------------------------------
// \brief Draws the reformat plane as polygon
//        Polygon in patient coordinates
//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPolygon(qfePoint *polygon, int pointCount, qfeColorRGB color, qfeMatrix4f p2t1, qfeMatrix4f p2t2)
{  
  //glColor3f(color.r, color.g, color.b);

  if(pointCount == 0) return qfeError;

  // Create the vertex buffer
  GLuint          planeStatVBO;
  qfePlaneVertex *buffer = new qfePlaneVertex[pointCount+1];

  for(int i=0; i<pointCount+1;i++)
  {
    qfePoint texCoord1, texCoord2;

    texCoord1 = polygon[i%pointCount] * p2t1;
    texCoord2 = polygon[i%pointCount] * p2t2;

    buffer[i].x = polygon[i%pointCount].x; 
    buffer[i].y = polygon[i%pointCount].y; 
    buffer[i].z = polygon[i%pointCount].z;

    buffer[i].s0 = texCoord1.x;
    buffer[i].t0 = texCoord1.y;
    buffer[i].u0 = texCoord1.z;

    buffer[i].s1 = texCoord2.x;
    buffer[i].t1 = texCoord2.y;
    buffer[i].u1 = texCoord2.z;   
  }

  glGenBuffers(1, &planeStatVBO);
  glBindBuffer(GL_ARRAY_BUFFER, planeStatVBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(qfePlaneVertex)*pointCount, buffer, GL_STATIC_DRAW);

  // Render the polygon
  glEnableVertexAttribArray(ATTRIB_VERTEX);  
  glEnableVertexAttribArray(ATTRIB_TEXCOORD0);
  glEnableVertexAttribArray(ATTRIB_TEXCOORD1);

  glVertexAttribPointer(ATTRIB_VERTEX,     3, GL_FLOAT, GL_FALSE, sizeof(qfePlaneVertex), BUFFER_OFFSET(0));
  glVertexAttribPointer(ATTRIB_TEXCOORD0,  3, GL_FLOAT, GL_FALSE, sizeof(qfePlaneVertex), BUFFER_OFFSET(3*sizeof(float)));
  glVertexAttribPointer(ATTRIB_TEXCOORD1,  3, GL_FLOAT, GL_FALSE, sizeof(qfePlaneVertex), BUFFER_OFFSET(6*sizeof(float)));
    
  glDrawArrays(GL_POLYGON, 0, pointCount);

  glDisableVertexAttribArray(ATTRIB_VERTEX); 
  glDisableVertexAttribArray(ATTRIB_TEXCOORD0); 
  glDisableVertexAttribArray(ATTRIB_TEXCOORD1);

  // Delete the buffer
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDeleteBuffers(1, &planeStatVBO);

  delete [] buffer;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPolygon(qfePoint *polygon, qfePoint *texcoords, int pointCount, qfeColorRGB color)
{
  return this->qfeDrawPolygon(polygon, texcoords, texcoords, pointCount, color);   
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPolygon(qfePoint *polygon, qfePoint *texcoords1, qfePoint *texcoords2, int pointCount, qfeColorRGB color)
{
  glColor3f(color.r, color.g, color.b);

  if(pointCount == 0) return qfeError;
  
  glBegin(GL_POLYGON);
  for(int i=0; i<pointCount+1;i++)
  {
    glMultiTexCoord3f(GL_TEXTURE0, texcoords1[i%pointCount].x, texcoords1[i%pointCount].y, texcoords1[i%pointCount].z);
    glMultiTexCoord3f(GL_TEXTURE1, texcoords2[i%pointCount].x, texcoords2[i%pointCount].y, texcoords2[i%pointCount].z);
    glVertex3f(   polygon[i%pointCount].x,
                  polygon[i%pointCount].y,
                  polygon[i%pointCount].z);
  }
  glEnd(); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawLine(qfePoint *polygon, int pointCount, qfeColorRGB color, int lineWidth)
{
  if(pointCount == 0) return qfeError;

  glLineWidth(lineWidth);
  glColor3f(color.r, color.g, color.b);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);

  glBegin(GL_LINE_STRIP);  
  for(int i=0; i<pointCount+1;i++)
  {
    glVertex3f( polygon[i%pointCount].x,
      polygon[i%pointCount].y,
      polygon[i%pointCount].z);
  }
  glEnd();

  glDisable(GL_LINE_SMOOTH);
  glDisable(GL_BLEND); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDrawPointSamples(qfePoint *polygon, int pointCount, qfeColorRGBA color)
{
  if(polygon == NULL || pointCount == 0) return qfeError;

  glPointSize(8.0f);
  glColor4f(color.r, color.g, color.b, color.a);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_POINT_SMOOTH);

  GLfloat *buffer = new GLfloat[pointCount*3]; 

  for(int i=0; i<pointCount; i++)
  {
    buffer[3*i+0] = polygon[i].x;
    buffer[3*i+1] = polygon[i].y;
    buffer[3*i+2] = polygon[i].z;
  }
   
  GLuint          pointSampleVBO;
  glGenBuffers(1, &pointSampleVBO);
  glBindBuffer(GL_ARRAY_BUFFER, pointSampleVBO);
  glBufferData(GL_ARRAY_BUFFER, 3*sizeof(GLfloat)*pointCount, buffer, GL_STATIC_DRAW);
  
  // Render the polygon
  glEnableVertexAttribArray(ATTRIB_VERTEX);  
  
  glVertexAttribPointer(ATTRIB_VERTEX,  3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));  
      
  glDrawArrays(GL_POINTS, 0, pointCount);

  glDisableVertexAttribArray(ATTRIB_VERTEX);   
  
  // Delete the buffer
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDeleteBuffers(1, &pointSampleVBO);
  
  glDisable(GL_BLEND); 

  delete [] buffer;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeComputePlane(qfeFrameSlice *plane, qfePoint **polygon, int &count)
{  
  qfeMatrix4f   T, S, R, M;
  qfePoint      o, e;
  qfeVector     x, y, z;  
  qfePoint     *poly = new qfePoint[4];  

  if(plane == NULL) return qfeError;
  
  plane->qfeGetFrameOrigin(o.x, o.y, o.z);
  plane->qfeGetFrameAxes(x.x, x.y, x.z, y.x, y.y, y.z, z.x, z.y, z.z);  
  plane->qfeGetFrameExtent(e.x, e.y, e.z);  

  qfeVector::qfeVectorNormalize(x);
  qfeVector::qfeVectorNormalize(y);
  qfeVector::qfeVectorNormalize(z);

  qfeMatrix4f::qfeSetMatrixIdentity(S);
  S(0,0) = e.x;
  S(1,1) = e.y;
  S(2,2) = e.z;
  
  qfeMatrix4f::qfeSetMatrixIdentity(R);  
  R(0,0) = x.x; R(0,1) = x.y; R(0,2) = x.z;
  R(1,0) = y.x; R(1,1) = y.y; R(1,2) = y.z;
  R(2,0) = z.x; R(2,1) = z.y; R(2,2) = z.z;

  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = o.x; 
  T(3,1) = o.y; 
  T(3,2) = o.z;

  M = S*R*T;

  // Compute the polygon points in patient coordinates
  poly[0].qfeSetPointElements(-0.5f, -0.5f, 0.0f);
  poly[1].qfeSetPointElements( 0.5f, -0.5f, 0.0f);
  poly[2].qfeSetPointElements( 0.5f,  0.5f, 0.0f);
  poly[3].qfeSetPointElements(-0.5f,  0.5f, 0.0f);

  for(int i=0; i<4; i++)
  {    
    poly[i] = poly[i]*M;
  }

  // Return the results
  *polygon = poly;
  count    = 4;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeComputeCircle(qfePoint origin, qfeVector normal, qfeFloat radius, qfePoint **polygon, int &count)
{
  qfePoint   *circle;
  qfePoint    vertex;
  GLint       slices;
  GLfloat     theta;
  qfeVector   n, u, v;
  qfeMatrix4f R, T;
    
  slices = 40;

  circle = new qfePoint[(slices+1)]; 
  theta  = float(2*PI) / float(slices);
  n      = normal;

  qfeVector::qfeVectorOrthonormalBasis2(n, u, v);
  qfeVector::qfeVectorNormalize(n);
  qfeVector::qfeVectorNormalize(u);
  qfeVector::qfeVectorNormalize(v);

  qfeMatrix4f::qfeSetMatrixIdentity(R);
  R(0,0) = u.x; 
  R(0,1) = u.y;
  R(0,2) = u.z;
  R(1,0) = v.x; 
  R(1,1) = v.y;
  R(1,2) = v.z;
  R(2,0) = n.x; 
  R(2,1) = n.y;
  R(2,2) = n.z;

  qfeMatrix4f::qfeSetMatrixIdentity(T); 
  T(3,0) = origin.x;
  T(3,1) = origin.y;
  T(3,2) = origin.z;

  for (int i = 0; i <= slices; i++) 
  {     
    float currentAngle = i * theta;

    // Compute the base strip
    vertex.x = cos(currentAngle) * radius;   
    vertex.y = sin(currentAngle) * radius;   
    vertex.z = 0.0f;  

    // Store the vertices
    circle[i] = vertex*(R*T);     
  }    

  (*polygon) = circle;
  count      = slices;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Real time volume graphics, page 74
// Plane properties in patient coordinates (mm)
// Polygon properties in patient coordinates (mm)
qfeReturnStatus qfePlanarReformat::qfeComputePolygon(qfeVolume *volume, qfeFrameSlice *plane, qfePoint **polygon, int &count)
{
  qfePoint     *polys = NULL;

  qfeMatrix4f   S, Sinv;
  qfeGrid      *grid;
  unsigned int  size[3];
  qfePoint      o;
  qfeVector     e;
  qfeVector     axes[3];
  qfeFloat      dims[3];  
  qfeVector     n, x, y;
  qfePoint      p;
  
  // Get volume properties
  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(size[0],size[1],size[2]);
  grid->qfeGetGridOrigin(o.x, o.y, o.z);
  grid->qfeGetGridExtent(e.x, e.y, e.z);
  grid->qfeGetGridAxes(axes[0].x, axes[0].y, axes[0].z, axes[1].x, axes[1].y, axes[1].z, axes[2].x, axes[2].y, axes[2].z);

  // Get plane properties
  plane->qfeGetFrameAxes( x.x, x.y, x.z,
                          y.x, y.y, y.z,
                          n.x, n.y, n.z);
  plane->qfeGetFrameOrigin(p.x, p.y, p.z);  

  // Compute volume dimensions, bounding box and edges
  // We need to work in a scaled volume in mm (voxels * voxel size)
  // The origin will be in the corner, as in voxel dimensions.
  dims[0] = size[0] * e.x;
  dims[1] = size[1] * e.y;
  dims[2] = size[2] * e.z;

  qfePoint box[8]        = { o + 0.5f*dims[0]*axes[0] + 0.5f*dims[1]*axes[1] + 0.5f*dims[2]*axes[2],
                             o + 0.5f*dims[0]*axes[0] - 0.5f*dims[1]*axes[1] + 0.5f*dims[2]*axes[2],
                             o + 0.5f*dims[0]*axes[0] + 0.5f*dims[1]*axes[1] - 0.5f*dims[2]*axes[2],
                             o - 0.5f*dims[0]*axes[0] + 0.5f*dims[1]*axes[1] + 0.5f*dims[2]*axes[2],
                             o - 0.5f*dims[0]*axes[0] - 0.5f*dims[1]*axes[1] + 0.5f*dims[2]*axes[2],
                             o + 0.5f*dims[0]*axes[0] - 0.5f*dims[1]*axes[1] - 0.5f*dims[2]*axes[2],
                             o - 0.5f*dims[0]*axes[0] + 0.5f*dims[1]*axes[1] - 0.5f*dims[2]*axes[2],
                             o - 0.5f*dims[0]*axes[0] - 0.5f*dims[1]*axes[1] - 0.5f*dims[2]*axes[2]};

  qfeFloat edges[12][2] = { {0,1}, {1,4}, {4,3}, {3,0},
                            {2,5}, {5,7}, {7,6}, {6,2},
                            {2,0}, {5,1}, {7,4}, {6,3}};

  // Compute transformation matrix from voxel coordinates to size in mm (only scaling!)
  qfeMatrix4f::qfeSetMatrixIdentity(S);  
  S(0,0) = e.x;
  S(1,1) = e.y;
  S(2,2) = e.z;
  qfeMatrix4f::qfeGetMatrixInverse(S,  Sinv);  

  // 1. Compute intersection points
  //    Use the plane in Hessian normal form
  qfePoint   s[12];
  qfeFloat   l, d;
  qfeVector  eij;
  
  for(int edge=0; edge<12; edge++)
  {
    qfePoint  point;
    qfePoint  vo;
    qfeVector vi;

    vo.x = dims[0]/2.0f;
    vo.y = dims[1]/2.0f;
    vo.z = dims[2]/2.0f;

    int v0 = edges[edge][0];         // start vertex index
    int v1 = edges[edge][1];         // end   vertex index

    eij = box[v1] - box[v0];         // current edge
    vi  = box[v0] - vo;              // point on bounding box
    d   = n*(p-vo);                  // distance to plane
    l   = (d - (n*vi)) / (n*eij);    // lambda

    if(0<=l && l<=1) point = box[v0] + eij*l;         
    else             point.x = point.y = point.z = -1;  // set dirty bits

    s[edge] = point;
  }

  // 2. Remove duplicates
  count = 0;
  for(int i=0; i<12; i++)
  {
    for(int j=0; j<12; j++)
    {
      if(i!=j)
      {
        if(s[i]==s[j])
        {
          s[j].x = -1.0; s[j].y = -1.0; s[j].z = -1.0;
        }
      }
    }
    if(s[i]!=qfePoint(-1.0,-1.0,-1.0)) count++;
  }

  polys = new qfePoint[count];

  int k=0;
  for(int i=0; i<12; i++)
  {
    if(s[i]!=qfePoint(-1.0,-1.0,-1.0))
    {
      polys[k] = s[i];
      k++;
    }
  }

  // Order the intersections to create a polygon
  for (int i=0; i<count ;i++)
  {
    bool done = true;

    for (int v=0; v<count-2;v++)
    {
      qfeVector c,i,j;
      qfePoint  t;

      i = polys[v+2] - polys[0];
      j = polys[v+1] - polys[0];

      qfeVector::qfeVectorCross(i,j,c);
      qfeVector::qfeVectorNormalize(c);

      qfeFloat dot;
      qfeVector::qfeVectorDot(c,n,dot);

      if (dot <0.0f)
      {
        done = false;

        t = polys[v+2];

        polys[v+2] = polys[v+1];
        polys[v+1] = t;
      }
    }
    if (done) break;
  }

  *polygon = polys;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Typical tapered US plane
qfeReturnStatus qfePlanarReformat::qfeComputePolygonUS(qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfePoint **polygon, int &count)
{
  qfePoint     *polys = NULL;

  qfePoint      o, p1, p2;
  qfeVector     e, v1, v2;
  qfeVector     axes[3];
  qfeVector     n, x, y;
  qfeFloat      angle, angleStart, angleStep, angleCurrent, radius, length;

  int slices    = 10;
    
  // Get plane properties
  plane->qfeGetFrameAxes( x.x, x.y, x.z,
                          y.x, y.y, y.z,
                          n.x, n.y, n.z);
  plane->qfeGetFrameOrigin(o.x, o.y, o.z);  
  plane->qfeGetFrameExtent(e.x, e.y, e.z);

  // Get the round side properties
  p1        = o + (-topRadius*x  + 0.5*e.y*y);
  p2        = o + ( topRadius*x  + 0.5*e.y*y);
  v1        = p1-o;
  v2        = p2-o;
  qfeVector::qfeVectorLength(v1, radius);
  qfeVector::qfeVectorNormalize(v1);
  qfeVector::qfeVectorNormalize(v2);
  angle      = acos(v1*v2);  
  angleStep  = angle / (float)slices;
  angleStart = (PI-angle)/2.0f;
  length     = 0.5f*e.y + radius;

  // Shift the origin to be in the middle, with arc on top
  o = o - 0.5f*(length-e.y)*y;

  // Compute the poly points in patient coordinates
  count = 4 + (slices - 1);  
  polys = new qfePoint[count];

  polys[0]       = o + (-baseRadius*x - 0.5*e.y*y);
  polys[1]       = p1;//o + (-topRadius*x  + 0.5*e.y*y);
  polys[1]       = o + (-topRadius*x  + 0.5*e.y*y);
  for(int i=2; i<count-2; i++)
  {
    angleCurrent = angleStart+(i-1)*angleStep;
    polys[i]     = o + (-cos(angleCurrent)*radius*x + sin(angleCurrent)*radius*y);
    angleCurrent = angleStart+(i-1)*angleStep;
    polys[i]     = o + (-cos(angleCurrent)*radius*x + sin(angleCurrent)*radius*y);
  }
  polys[count-2] = p2; //o + ( topRadius*x  + 0.5*e.y*y);
  polys[count-2] = o + ( topRadius*x  + 0.5*e.y*y);
  polys[count-1] = o + ( baseRadius*x - 0.5*e.y*y);

  *polygon = polys;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Non tapered US plane
qfeReturnStatus qfePlanarReformat::qfeComputePolygonUS2(qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfePoint **polygon, int &count)
{
  qfePoint     *polys = NULL;

  qfePoint      o, p1, p2, e;
  qfeVector     axes[3];
  qfeVector     n, x, y;
  qfeFloat      length;

  // Get plane properties
  plane->qfeGetFrameAxes( x.x, x.y, x.z,
                          y.x, y.y, y.z,
                          n.x, n.y, n.z);
  plane->qfeGetFrameOrigin(o.x, o.y, o.z);  
  plane->qfeGetFrameExtent(e.x, e.y, e.z);

  // Get the round side properties
  p1        = o + (-topRadius*x  + 0.5*e.y*y);
  p2        = o + ( topRadius*x  + 0.5*e.y*y);
  length    = 0.5f*e.y;

  // Shift the origin to be in the middle, with arc on top
  //o = o - 0.5f*e.y*y;

  // Compute the poly points in patient coordinates
  count = 4;  
  polys = new qfePoint[count];

  polys[0] = o + (-baseRadius*x - 0.5*e.y*y);
  polys[1] = o + (-topRadius*x  + 0.5*e.y*y);    
  polys[2] = o + ( topRadius*x  + 0.5*e.y*y);
  polys[3] = o + ( baseRadius*x - 0.5*e.y*y);

  *polygon = polys;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
// plane is defined in patient coordinates
// point samples are defined in texture coordinates
qfeReturnStatus qfePlanarReformat::qfeComputePointSamples(qfeVolume *volume, qfeFrameSlice *plane, qfeFloat spacing, qfePoint **points, int &count)
{
  qfeGrid      *grid  = NULL;
  qfePoint     *polys = NULL;  

  qfeMatrix4f   P2V, V2T, P2T, T2P, R, S;

  GLfloat       g[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1};
  qfeVector     n, x, y, xtex, ytex, t;
  qfePoint      o, otex, p, xp, xq, yp, yq, s;
  qfePoint      extend;
  unsigned int  size[3];
  int           c, nx, ny;

  if(volume == NULL) return qfeError;
  
  // Transform from patient coordinates to texture coordinates
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);
  
  P2T = P2V*V2T;  
  qfeMatrix4f::qfeGetMatrixInverse(P2T, T2P);

  plane->qfeGetFrameAxes( x.x, x.y, x.z,
                          y.x, y.y, y.z,
                          n.x, n.y, n.z);
  plane->qfeGetFrameOrigin(o.x, o.y, o.z);  

  volume->qfeGetVolumeGrid(&grid);
  volume->qfeGetVolumeSize(size[0], size[1], size[2]);

  // Transform the plane axes and origin to texture coordinates
  xtex = x*P2T;
  ytex = y*P2T;
  otex = o*P2T;

  qfeVector::qfeVectorNormalize(xtex);
  qfeVector::qfeVectorNormalize(ytex);  

  // Assume the size of the plane in texture coordinates to be 1.0 x 1.0
  // Take the outermost points in x and y direction, 
  // and transform to patient coordinates
  xp = (otex - 0.5f*xtex) * T2P;
  xq = (otex + 0.5f*xtex) * T2P;
  yp = (otex - 0.5f*ytex) * T2P;
  yq = (otex + 0.5f*ytex) * T2P;

  // Compute the size of the plane in patient coordinates
  qfeVector::qfeVectorLength(xq-xp, t.x);
  qfeVector::qfeVectorLength(yq-yp, t.y);  

  // Divide the size of the plane with the requested spacing to determine
  // the amount of samples for each direction
  nx    = (int)t.x/spacing;
  ny    = (int)t.y/spacing;

  count = (nx+1)*(ny+1);
  polys = new qfePoint[count];

  c = 0;
  p = otex - 0.5f*xtex - 0.5f*ytex;
  for(int i=0; i<=nx; i++)
  {
    for(int j=0; j<=ny; j++)
    {
      polys[c] = p + i*(1.0/nx)*xtex + j*(1.0/ny)*ytex;   
      c++;
    }
  }
  
  *points = polys;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeCreateColorMap()
{
  // Initialize doppler colormap
  vector<qfeRGBMapping>     rgb, rgb2;
  vector<qfeOpacityMapping> o;  
  
  rgb.resize(5);
  rgb[0].color.r = 0.840; rgb[0].color.g = 0.879; rgb[0].color.b = 0.355; rgb[0].value = 0.00;
  rgb[1].color.r = 0.594; rgb[1].color.g = 0.137; rgb[1].color.b = 0.000; rgb[1].value = 0.25;
  rgb[2].color.r = 0.001; rgb[2].color.g = 0.001; rgb[2].color.b = 0.001; rgb[2].value = 0.50;
  rgb[3].color.r = 0.047; rgb[3].color.g = 0.551; rgb[3].color.b = 1.000; rgb[3].value = 0.75;
  rgb[4].color.r = 0.484; rgb[4].color.g = 0.965; rgb[4].color.b = 0.990; rgb[4].value = 1.00;

  rgb2.resize(5);
  rgb2[0].color.r = 0.840; rgb2[0].color.g = 0.879; rgb2[0].color.b = 0.355; rgb2[0].value = 0.00;
  rgb2[1].color.r = 0.594; rgb2[1].color.g = 0.137; rgb2[1].color.b = 0.000; rgb2[1].value = 0.25;
  rgb2[2].color.r = 0.001; rgb2[2].color.g = 0.001; rgb2[2].color.b = 0.001; rgb2[2].value = 0.50;
  rgb2[3].color.r = 0.047; rgb2[3].color.g = 0.551; rgb2[3].color.b = 1.000; rgb2[3].value = 0.75;
  rgb2[4].color.r = 0.418; rgb2[4].color.g = 0.738; rgb2[4].color.b = 0.406; rgb2[4].value = 1.00;  

  o.resize(2);
  o[0].opacity = 1.0; o[0].value = 0.0;
  o[1].opacity = 1.0; o[1].value = 1.0;
  
  this->colorMapUS->qfeSetColorMapRGB(5, rgb);
  this->colorMapUS->qfeSetColorMapA(2, o);
  this->colorMapUS->qfeSetColorMapG(2, o);
  this->colorMapUS->qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
  this->colorMapUS->qfeSetColorMapSpace(qfeColorSpaceLab);

  this->colorMapUS2->qfeSetColorMapRGB(5, rgb2);
  this->colorMapUS2->qfeSetColorMapA(2, o);
  this->colorMapUS2->qfeSetColorMapG(2, o);
  this->colorMapUS2->qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
  this->colorMapUS2->qfeSetColorMapSpace(qfeColorSpaceLab);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeCreateTextures()
{
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeDeleteTextures()
{
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeBindTextures()
{  
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_3D, this->texVolume1);

  if(this->paramInterpolation == qfePlanarReformatInterpolateNearest)
  {
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }
  else
  {
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }

  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_3D, this->texVolume2);  

  if(this->paramInterpolation == qfePlanarReformatInterpolateNearest)
  {
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }
  else
  {
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }
    
  glActiveTexture(GL_TEXTURE3);
  glBindTexture(GL_TEXTURE_1D, this->texColorMap);

  if(glIsTexture(this->texColorMapUS))
  {
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_1D, this->texColorMapUS);
  }
  if(glIsTexture(this->texColorMapUS2))
  {
    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_1D, this->texColorMapUS2);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeUnbindTextures()
{
  // Reset to default linear interpolation
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_3D, this->texVolume1);  
  glBindTexture(GL_TEXTURE_3D, this->texVolume2);  
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  // Unbind
  glBindTexture  (GL_TEXTURE_1D, 0);  
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetDynamicUniformLocations()
{
  this->shaderProgramPlaneStatic->qfeSetUniform1i("planeDataSet1"            , 1);
  this->shaderProgramPlaneStatic->qfeSetUniform1i("planeDataSet2"            , 2);
  this->shaderProgramPlaneStatic->qfeSetUniform1i("planeDataComponent"       , this->paramDataComponent);
  this->shaderProgramPlaneStatic->qfeSetUniform1i("planeDataRepresentation"  , this->paramDataRepresentation);    
  this->shaderProgramPlaneStatic->qfeSetUniform2f("planeDataRange1"          , this->paramDataRange1[0], this->paramDataRange1[1]);
  this->shaderProgramPlaneStatic->qfeSetUniform2f("planeDataRange2"          , this->paramDataRange2[0], this->paramDataRange2[1]);
  this->shaderProgramPlaneStatic->qfeSetUniform3f("planeNormal"              , this->paramPlaneNormal.x, this->paramPlaneNormal.y, this->paramPlaneNormal.z);
  this->shaderProgramPlaneStatic->qfeSetUniform1i("transferFunction"         , 3);
  this->shaderProgramPlaneStatic->qfeSetUniform1f("triggerTime"              , this->paramTriggerTime/(float)this->paramTotalTime);
  this->shaderProgramPlaneStatic->qfeSetUniform1i("balance"                  , this->paramBalance);
  this->shaderProgramPlaneStatic->qfeSetUniform1f("brightness"               , this->paramBrightness / 100.0);
  this->shaderProgramPlaneStatic->qfeSetUniform1f("contrast"                 , this->paramContrast / 100.0);
  this->shaderProgramPlaneStatic->qfeSetUniform1f("opacity"                  , this->paramPlaneOpacity);
  this->shaderProgramPlaneStatic->qfeSetUniformMatrix4f("matrixModelViewProjection",        1, this->matrixModelViewProjection);   

  this->shaderProgramPlaneDynamic->qfeSetUniform1i("planeDataSet1"           , 1);
  this->shaderProgramPlaneDynamic->qfeSetUniform1i("planeDataSet2"           , 2);
  this->shaderProgramPlaneDynamic->qfeSetUniform1i("planeDataComponent"      , this->paramDataComponent);
  this->shaderProgramPlaneDynamic->qfeSetUniform1i("planeDataComponentCount" , this->paramDataComponentCount);
  this->shaderProgramPlaneDynamic->qfeSetUniform1i("planeDataRepresentation" , this->paramDataRepresentation);    
  this->shaderProgramPlaneDynamic->qfeSetUniform2f("planeDataRange1"         , this->paramDataRange1[0], this->paramDataRange1[1]);  
  this->shaderProgramPlaneDynamic->qfeSetUniform2f("planeDataRange2"         , this->paramDataRange2[0], this->paramDataRange2[1]);  
  this->shaderProgramPlaneDynamic->qfeSetUniform3f("planeNormal"             , this->paramPlaneNormal.x, this->paramPlaneNormal.y, this->paramPlaneNormal.z);
  this->shaderProgramPlaneDynamic->qfeSetUniform3f("voxelSize"               , this->voxelSize.x, this->voxelSize.y, this->voxelSize.z);
  this->shaderProgramPlaneDynamic->qfeSetUniform1i("transferFunction"        , 3);  
  this->shaderProgramPlaneDynamic->qfeSetUniform1i("balance"                 , this->paramBalance);
  this->shaderProgramPlaneDynamic->qfeSetUniform1f("brightness"              , this->paramBrightness / 100.0);
  this->shaderProgramPlaneDynamic->qfeSetUniform1f("contrast"                , this->paramContrast / 100.0);
  this->shaderProgramPlaneDynamic->qfeSetUniform1f("opacity"                 , this->paramPlaneOpacity);
  this->shaderProgramPlaneDynamic->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);  
  this->shaderProgramPlaneDynamic->qfeSetUniformMatrix4f("matrixModelViewProjectionInverse", 1, this->matrixModelViewProjectionInverse); 

  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("planeDataSet1"           , 1);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("planeDataSet2"           , 2);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("planeDataContext"        , this->paramDataComponent);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("planeDataFocus"          , this->paramDataComponent2);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("planeDataComponentCount" , this->paramDataComponentCount);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("planeDataRepresentation" , this->paramDataRepresentation);  
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("planeDataRepresentation2", this->paramDataRepresentation2);  
  this->shaderProgramPlaneUltrasound->qfeSetUniform2f("planeDataRange1"         , this->paramDataRange1[0], this->paramDataRange1[1]);  
  this->shaderProgramPlaneUltrasound->qfeSetUniform2f("planeDataRange2"         , this->paramDataRange2[0], this->paramDataRange2[1]);  
  this->shaderProgramPlaneUltrasound->qfeSetUniform3f("planeNormal"             , this->paramPlaneNormal.x, this->paramPlaneNormal.y, this->paramPlaneNormal.z);
  this->shaderProgramPlaneUltrasound->qfeSetUniform3f("planeDirection"          , this->paramPlaneDir.x,    this->paramPlaneDir.y,    this->paramPlaneDir.z);
  this->shaderProgramPlaneUltrasound->qfeSetUniform3f("voxelSize"               , this->voxelSize.x, this->voxelSize.y, this->voxelSize.z);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("transferFunction"        , 3);  
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("transferFunctionUS"      , 4);  
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("transferFunctionUS2"     , 5);  
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("scale"                   , this->paramDataScale);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("balance"                 , this->paramBalance);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("speedThreshold"          , this->paramSpeedThreshold);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1i("viewAngle"               , this->paramViewAngle);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1f("brightness"              , this->paramBrightness / 100.0);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1f("contrast"                , this->paramContrast / 100.0);
  this->shaderProgramPlaneUltrasound->qfeSetUniform1f("opacity"                 , this->paramPlaneOpacity);
  this->shaderProgramPlaneUltrasound->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection); 

  this->shaderProgramPlaneArrowHeads->qfeSetUniform1i("planeDataSet"            , 1);
  this->shaderProgramPlaneArrowHeads->qfeSetUniform2f("planeDataRange"          , this->paramDataRange1[0], this->paramDataRange1[1]); 
  this->shaderProgramPlaneArrowHeads->qfeSetUniform1i("planeDataRepresentation" , this->paramDataRepresentation);    
  this->shaderProgramPlaneArrowHeads->qfeSetUniform3f("planeNormal"             , this->paramPlaneNormal.x, this->paramPlaneNormal.y, this->paramPlaneNormal.z);  
  this->shaderProgramPlaneArrowHeads->qfeSetUniform1i("transferFunction"        , 3);
  this->shaderProgramPlaneArrowHeads->qfeSetUniform1f("arrowSpacing"            , this->paramArrowSpacing);
  this->shaderProgramPlaneArrowHeads->qfeSetUniform1f("arrowScaling"            , this->paramArrowScaling);
  this->shaderProgramPlaneArrowHeads->qfeSetUniform1i("speedThreshold"          , this->paramSpeedThreshold);
  this->shaderProgramPlaneArrowHeads->qfeSetUniformMatrix4f("matrixTexturePatient", 1, this->paramT2P);
  this->shaderProgramPlaneArrowHeads->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneBorderColor(qfeColorRGB color)
{
  this->paramPlaneBorderColor.r = color.r;
  this->paramPlaneBorderColor.g = color.g;
  this->paramPlaneBorderColor.b = color.b;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneBorderWidth(qfeFloat width)
{
  this->paramPlaneBorderWidth = width; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneDataComponent(int component)
{
  this->paramDataComponent = component;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneDataComponent2(int component)
{
  this->paramDataComponent2 = component;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneDataRepresentation(int type)
{
  this->paramDataRepresentation = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneDataRepresentation2(int type)
{
  this->paramDataRepresentation2 = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneDataInterpolation(qfePlanarReformatInterpolation interpolation)
{
  this->paramInterpolation =  interpolation;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneDataRange(qfeFloat vl, qfeFloat vu)
{
  this->paramDataRange1[0] = vl;
  this->paramDataRange1[1] = vu;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneDataRange2(qfeFloat vl, qfeFloat vu)
{
  this->paramDataRange2[0] = vl;
  this->paramDataRange2[1] = vu;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneScale(int scale)
{
  this->paramDataScale = scale;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneTriggerTime(qfeFloat time)
{
  this->paramTriggerTime = time;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// brightness - [-100,100]
qfeReturnStatus qfePlanarReformat::qfeSetPlaneBrightness(qfeFloat brightness)
{
  this->paramBrightness = brightness;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// contrast - [-100,100]
qfeReturnStatus qfePlanarReformat::qfeSetPlaneContrast(qfeFloat contrast)
{
  this->paramContrast = contrast;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneBalance(qfeFloat balance)
{
  this->paramBalance = balance;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneContour(bool visible)
{
  this->paramContour = visible;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneHorizontalFlip(bool flip)
{
  this->paramFlip = flip;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneOpacity(qfeFloat value)
{
  this->paramPlaneOpacity = min(max(value,0.0),1.0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneSpeedThreshold(int threshold)
{
  this->paramSpeedThreshold = threshold;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeSetPlaneViewAngle(int angle)
{
  this->paramViewAngle = angle;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeRenderBoundingBox(qfeVolume *volume)
{
  qfeMatrix4f   P2W, V2P, T2V, M;
  GLfloat       matrix[16];

  qfeTransform::qfeGetMatrixPatientToWorld(P2W);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  M = T2V*V2P*P2W;
  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();
  glMultMatrixf(&matrix[0]);  // Texture to World   Coordinates

  glBegin(GL_QUADS);

  // Front
  glNormal3f(0.0f, 0.0f, -1.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);

  // Back
  glNormal3f(0.0f, 0.0f, 1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f, 1.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f, 1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f,  1.0f, 1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f, 1.0f);

  // Left
  glNormal3f(-1.0f, 0.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f,  1.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);

  // Right
  glNormal3f(1.0f, 0.0f, 0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f(1.0f,  0.0f,  1.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f(1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f(1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f(1.0f,  1.0f,  1.0f);

  // Top
  glNormal3f(0.0f, -1.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f,  1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);

  // Bottom
  glNormal3f(0.0f, 1.0f, 0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f, 1.0f,  1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f, 1.0f,  1.0f);

  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
};


//----------------------------------------------------------------------------
qfeReturnStatus qfePlanarReformat::qfeClampPoint(qfePoint &p)
{
  if(p.x < 0.0) p.x = 0.0;
  if(p.x > 1.0) p.x = 1.0;
  if(p.y < 0.0) p.y = 0.0;
  if(p.y > 1.0) p.y = 1.0;
  if(p.z < 0.0) p.z = 0.0;
  if(p.z > 1.0) p.z = 1.0;
  return qfeSuccess;
}
