#pragma once

#include "qflowexplorer.h"

#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>

#include "qfeAlgorithm.h"
#include "qfeColorMap.h"
#include "qfeMatrix4f.h"
#include "qfeGLShaderProgram.h"
#include "qfeFrame.h"
#include "qfeTransform.h"
#include "qfeVector.h"
#include "qfeVolume.h"

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

struct qfePlaneVertex
{
  GLfloat  x,  y,  z;    // vertex coords
  GLfloat s0, t0, u0; // tex coords 0;
  GLfloat s1, t1, u1; // tex coords 0;
};

enum qfePlanarReformatType
{
	qfePlanarReformatTypeColorMap,
	qfePlanarReformatTypeGray
};

enum qfePlanarReformatInterpolation
{
	qfePlanarReformatInterpolateNearest,
	qfePlanarReformatInterpolateLinear
};

/**
* \file   qfePlanarReformat.h
* \author Roy van Pelt
* \class  qfePlanarReformat
* \brief  Implements a planar reformat
* \note   Confidential
*
* Rendering of a planar reformat
*
*/
using namespace std;

class qfePlanarReformat : public qfeAlgorithm
{
public:
  qfePlanarReformat();
  qfePlanarReformat(const qfePlanarReformat &pr);
  ~qfePlanarReformat();

  qfeReturnStatus qfeRenderPlane3D(qfeVolume *volume, qfeFrameSlice *plane, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderPlane3D(qfeVolume *volume, int phases, qfeFrameSlice *plane, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderPlane3D(qfeVolume *vol1, qfeVolume *vol2, int phases, qfeFrameSlice *plane, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderPolygon3D(qfeVolume *volume, qfeFrameSlice *plane, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderPolygon3D(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderPolygonUS2D(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderPolygonUS3D(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderCircleUS2D(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeColorMap *colormap);    
  qfeReturnStatus qfeRenderOverlayUS2D(qfeVolume *boundingVolume, qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeFloat angle);
  qfeReturnStatus qfeRenderPolygonLine3D(qfeVolume *volume, qfeFrameSlice *plane);
  qfeReturnStatus qfeRenderCircle3D(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeColorMap *colormap);  
  qfeReturnStatus qfeRenderClipBox3D(qfeVolume* volume, qfeFrameSlice *plane, qfeFloat size, qfeFloat depth);  
  qfeReturnStatus qfeRenderClipBox3D(qfeFrameSlice *plane, qfeFloat size, qfeFloat depth);  
  qfeReturnStatus qfeRenderArrowHeads3D(qfeVolume *volume, qfeFrameSlice *plane, qfeFloat spacing, qfeFloat scaling, qfeColorMap *colormap, int transparency);
  qfeReturnStatus qfeRenderAxes(qfeFrameSlice *plane);

  qfeReturnStatus qfeDrawPlane(qfeFrameSlice *plane, qfeColorRGB color);
  qfeReturnStatus qfeDrawPlane(qfeFrameSlice *plane, qfeVolume *volume, qfeColorRGB color);
  qfeReturnStatus qfeDrawPolygon(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeColorRGB color);  
  qfeReturnStatus qfeDrawPolygonUS(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeColorRGB color);    
  qfeReturnStatus qfeDrawPolygonUS2(qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeColorRGB color);    
  qfeReturnStatus qfeDrawCircle(qfeVolume *boundingVolume, qfeVolume* textureVolume, qfeFrameSlice *plane);
  qfeReturnStatus qfeDrawLine(qfeVolume *volume, qfeFrameSlice *plane, qfeColorRGB color, int lineWidth);
  qfeReturnStatus qfeDrawLine(qfeFrameSlice *plane, qfeColorRGB color, int lineWidth);
  qfeReturnStatus qfeDrawLineCircle(qfeFrameSlice *plane, qfeColorRGB color, int lineWidth);
  qfeReturnStatus qfeDrawTicksCircle(qfeFrameSlice *plane, qfeColorRGB color, int lineWidth);
  qfeReturnStatus qfeDrawLinePolygonUS(qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeColorRGB color, int lineWidth);
  qfeReturnStatus qfeDrawTicksPolygonUS(qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfeColorRGB color, int lineWidth);
  qfeReturnStatus qfeDrawPointSamples(qfeVolume *volume, qfeFrameSlice *plane, qfeColorRGBA color, qfeFloat spacing);
  
  qfeReturnStatus qfeSetPlaneBorderColor(qfeColorRGB color);
  qfeReturnStatus qfeSetPlaneBorderWidth(qfeFloat width);
  qfeReturnStatus qfeSetPlaneDataComponent(int component);
  qfeReturnStatus qfeSetPlaneDataComponent2(int component);
  qfeReturnStatus qfeSetPlaneDataRepresentation(int type);
  qfeReturnStatus qfeSetPlaneDataRepresentation2(int type);
  qfeReturnStatus qfeSetPlaneDataInterpolation(qfePlanarReformatInterpolation interpolation);
  qfeReturnStatus qfeSetPlaneDataRange(qfeFloat vl, qfeFloat vu);
  qfeReturnStatus qfeSetPlaneDataRange2(qfeFloat vl, qfeFloat vu);
  qfeReturnStatus qfeSetPlaneScale(int scale);
  qfeReturnStatus qfeSetPlaneTriggerTime(qfeFloat time);
  qfeReturnStatus qfeSetPlaneBrightness(qfeFloat brightness);
  qfeReturnStatus qfeSetPlaneContrast(qfeFloat contrast);
  qfeReturnStatus qfeSetPlaneBalance(qfeFloat balance);
  qfeReturnStatus qfeSetPlaneContour(bool visible);
  qfeReturnStatus qfeSetPlaneHorizontalFlip(bool flip);
  qfeReturnStatus qfeSetPlaneOpacity(qfeFloat value);

  qfeReturnStatus qfeSetPlaneSpeedThreshold(int threshold);
  qfeReturnStatus qfeSetPlaneViewAngle(int angle);

  qfeReturnStatus qfeComputePolygon(qfeVolume *volume, qfeFrameSlice *plane, qfePoint **polygon, int &count);  

protected:
  qfeReturnStatus qfeDrawPlane(qfePoint *plane, int pointCount, qfeColorRGB color);
  qfeReturnStatus qfeDrawPlane(qfePoint *plane, qfeVolume* volume, int pointCount, qfeColorRGB color);
  qfeReturnStatus qfeDrawPolygon(qfePoint *polygon, int pointCount, qfeColorRGB color, qfeMatrix4f p2t1, qfeMatrix4f p2t2);  
  qfeReturnStatus qfeDrawPolygon(qfePoint *polygon, int pointCount, qfeColorRGB color, qfeMatrix4f p2t);
  qfeReturnStatus qfeDrawPolygon(qfePoint *polygon, int pointCount, qfeColorRGB color);
  qfeReturnStatus qfeDrawPolygon(qfePoint *polygon, qfePoint *texcoords1, qfePoint *texcoords2, int pointCount, qfeColorRGB color);
  qfeReturnStatus qfeDrawPolygon(qfePoint *polygon, qfePoint *texcoords, int pointCount, qfeColorRGB color);
  qfeReturnStatus qfeDrawLine(qfePoint *polygon, int pointCount, qfeColorRGB color, int lineWidth);
  qfeReturnStatus qfeDrawPointSamples(qfePoint *polygon, int pointCount, qfeColorRGBA color);
  qfeReturnStatus qfeDrawCircleTick(qfePoint p, qfeFloat r, qfeVector n, qfeColorRGB color);
  qfeReturnStatus qfeDrawLineTick(qfePoint p1, qfePoint p2, qfeFloat lineWidth, qfeColorRGB color);

  qfeReturnStatus qfeComputePlane(qfeFrameSlice *plane, qfePoint **polygon, int &count);
  qfeReturnStatus qfeComputeCircle(qfePoint origin, qfeVector normal, qfeFloat radius, qfePoint **polygon, int &count);
  qfeReturnStatus qfeComputePolygonUS(qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfePoint **polygon, int &count);  
  qfeReturnStatus qfeComputePolygonUS2(qfeFrameSlice *plane, qfeFloat baseRadius, qfeFloat topRadius, qfePoint **polygon, int &count);  
  qfeReturnStatus qfeComputePointSamples(qfeVolume *volume, qfeFrameSlice *plane, qfeFloat spacing, qfePoint **points, int &count);

  qfeReturnStatus qfeRenderBoundingBox(qfeVolume *volume);

  qfeReturnStatus qfeClampPoint(qfePoint &p);

  qfeReturnStatus qfeCreateColorMap();
  
  qfeReturnStatus qfeCreateTextures();
  qfeReturnStatus qfeDeleteTextures();
  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

private :
  qfeGLShaderProgram *shaderProgramPlaneStatic;
  qfeGLShaderProgram *shaderProgramPlaneDynamic;
  qfeGLShaderProgram *shaderProgramPlaneUltrasound;
  qfeGLShaderProgram *shaderProgramPlaneArrowHeads;

  qfeColorMap        *colorMapUS;
  qfeColorMap        *colorMapUS2;
  
  GLuint              texVolume1;
  GLuint              texVolume2;
  GLuint              texColorMap;
  GLuint              texColorMapUS;
  GLuint              texColorMapUS2;
  qfePoint            voxelSize;

  qfePlanarReformatInterpolation  paramInterpolation;
  qfeColorRGB                     paramPlaneBorderColor;
  qfeFloat                        paramPlaneBorderWidth;
  qfeVector                       paramPlaneNormal;
  qfeVector                       paramPlaneDir;
  int                             paramDataScale;
  int                             paramDataComponent;
  int                             paramDataComponent2;
  int                             paramDataComponentCount;  
  int                             paramDataRepresentation;  
  int                             paramDataRepresentation2;  
  qfeFloat                        paramDataRange1[2];
  qfeFloat                        paramDataRange2[2];
  qfeFloat                        paramTriggerTime;  
  qfeFloat                        paramTotalTime;
  qfeFloat                        paramT2P[16];
  qfeFloat                        paramArrowSpacing;
  qfeFloat                        paramArrowScaling;   
  int                             paramBalance;
  int                             paramSpeedThreshold;
  int                             paramViewAngle;
  qfeFloat                        paramBrightness;
  qfeFloat                        paramContrast;
  bool                            paramContour;
  bool                            paramFlip;  
  qfeFloat                        paramPlaneOpacity;
};
