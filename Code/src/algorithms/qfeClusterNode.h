#pragma once

#include "qfeMath.h"
#include "qfeDistanceListNode.h"

#include <map>

/**
* \file   qfeClusterNode.h
* \author Sander Jacobs
* \class  qfeClusterNode
* \brief  Abstract class for a cluster node
* \note   Confidential
*/

struct qfeClusterPoint
{
  qfeClusterPoint::qfeClusterPoint(){};

  qfeClusterPoint::qfeClusterPoint(qfeFloat position[4], qfeFloat flow[3], qfeFloat scalar)
  {
    for(int i=0; i<4; i++)
      this->position[i] = position[i];

    for(int i=0; i<3; i++)
      this->flow[i] = flow[i];

    this->scalar = scalar;
  }
  qfeFloat position[4];
  qfeFloat flow[3];
  qfeFloat scalar;
};


class qfeClusterNode
{
public:
  qfeClusterNode();
  ~qfeClusterNode();

  qfeReturnStatus qfeAddDistancePointer(qfeClusterNode *nPtr, qfeDistanceListNode *dPtr);
  qfeReturnStatus qfeMergeDistances(qfeClusterNode *c);
  qfeReturnStatus qfeMergeAndMoveDistances(qfeClusterNode *c, qfeDistanceListNode* &firstDistance, qfeDistanceListNode* &lastDistance);
  qfeReturnStatus qfeMergeToCluster(qfeClusterNode *c, qfeDistanceListNode* &firstDistance, qfeDistanceListNode* &lastDistance);
  qfeReturnStatus qfePrintDistances();
  
  int             qfeGetNeighbourCount();
  qfeReturnStatus qfeSetClusterLabel(int clusterLabel);
  int             qfeGetClusterLabel();

  static qfeReturnStatus qfeSetNextClusterLabel(int l);
  static int             qfeGetNextClusterLabel();
  static qfeReturnStatus qfeIncrementNextClusterLabel();

  virtual qfeReturnStatus qfeMergeRepresentative(qfeClusterNode *c2)=0;
  virtual qfeFloat        qfeComputeDistanceTo(qfeClusterNode *c2)=0;
  
private:
  map<qfeClusterNode*, qfeDistanceListNode*> distancePointers;

  int                   clusterLabel;

  static int            nextClusterLabel;
};






