#include "qfeSeeding.h"

//----------------------------------------------------------------------------
qfeSeeding::qfeSeeding()
{

};

//----------------------------------------------------------------------------
qfeSeeding::qfeSeeding(const qfeSeeding &fv)
{

};

//----------------------------------------------------------------------------
qfeSeeding::~qfeSeeding()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeeds(qfeProbe2D *probe, qfeSeedType seedStrategy, double seedDensity, qfeSeeds &seeds)
{
  seeds.clear();

  switch(seedStrategy)
  {
  case(QFE_SEED_POISSON_DISK): qfeSeeding::qfeGetSeedsRandomPoissonDisk(probe, seedDensity, seeds);
                               break;
  case(QFE_SEED_RANDOM_NORM) : qfeSeeding::qfeGetSeedsRandomUniform(probe, seedDensity, seeds);
                               break;
  case(QFE_SEED_RANDOM_RECT) : qfeSeeding::qfeGetSeedsRandomRectilinear(probe, seedDensity, seeds);
                               break;
  case(QFE_SEED_RADIAL)      : qfeSeeding::qfeGetSeedsRadial(probe, seedDensity, seeds);
                               break;
  case(QFE_SEED_CIRCLES)     : qfeSeeding::qfeGetSeedsCircles(probe, seedDensity, seeds);
                               break;
  case(QFE_SEED_RECTILINEAR) : qfeSeeding::qfeGetSeedsRectilinear(probe, seedDensity, seeds);
                               break;  
  } 


  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsRandomUniform(qfeFrameSlice *plane, int seedsCount, qfeSeeds &seeds)
{
  qfePoint    origin;
  qfePoint    extent;
  qfeVector   axes[3];
  qfePoint    point, o;
  qfeSeeds    set;
  qfeMatrix4f T;

  // clear the sets
  set.clear();
  seeds.clear();

  // get the plane properties  
  plane->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  plane->qfeGetFrameExtent(extent.x, extent.y, extent.z);
  plane->qfeGetFrameAxes(axes[0].x, axes[0].y, axes[0].z, axes[1].x, axes[1].y, axes[1].z, axes[2].x, axes[2].y, axes[2].z);
  
  // determine the transformation matrix
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x; 
  T(3,1) = origin.y; 
  T(3,2) = origin.z;

  T(0,0) = axes[0].x; T(0,1) = axes[0].y; T(0,2) = axes[0].z;
  T(1,0) = axes[1].x; T(1,1) = axes[1].y; T(1,2) = axes[1].z;
  T(2,0) = axes[2].x; T(2,1) = axes[2].y; T(2,2) = axes[2].z;

  // generate points
  o.qfeSetPointElements(0.0,0.0,0.0);
  for(int i = 0; i < seedsCount; i++)
  {
    qfeGenerateUniformSeed2D(point, extent);
    
    if (qfeInCircle(point, o, 0.5f*min(extent.x, extent.y)))
    {        
      set.push_back(point);      
    }
    else i--;
  }

  // transform the point to the plane coordinates
  for(int i=0; i<(int)set.size(); i++)
    seeds.push_back(set[i]*T);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsRandomUniform(qfeProbe2D *probe, double density, qfeSeeds &seeds)
{
  int           seedCount;
  double        randomX, randomY;  

  if(probe == NULL) return qfeError;

  // Determine how many seeds will be computed
  seedCount = (int)(probe->area * density);

  // Compute the seeds
  seeds.clear();

  srand((unsigned)time(0));    
  int lowest=0, highest=(int)(2.0*probe->radius*1000);
  int range=(highest-lowest)+1;

  for(int i=0; i<seedCount; i++)
  {    
    qfePoint  seed;
    qfeVector dx, dy;

    // Get a random offset
    randomX = (lowest+int(range*rand()/(RAND_MAX + 1.0)))/1000.0;
    randomY = (lowest+int(range*rand()/(RAND_MAX + 1.0)))/1000.0; 

    randomX = randomX - probe->radius;
    randomY = randomY - probe->radius;

    dx   = (probe->axisX*(float)randomX); 
    dy   = (probe->axisY*(float)randomY);
    seed = probe->origin + dx + dy;

    if(qfeSeeding::qfeInProbe(seed, probe) == false)
    {
      i--;
    }
    else
    {      
      seeds.push_back(seed);
    }
  }   

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsRandomUniform(qfeProbe3D *probe, int seedsCount, qfeSeeds &seeds)
{
  qfePoint    origin;
  qfePoint    extent;
  qfeVector   axes[3];
  qfePoint    point, o;
  qfeSeeds    set;
  qfeMatrix4f T;

  // clear the sets
  set.clear();
  seeds.clear();

  // get the probe properties  
  extent.x = extent.z = 2.0f*max(probe->topRadius, probe->baseRadius);
  extent.y = probe->length;
  axes[0]  = probe->axes[0];
  axes[1]  = probe->axes[2];
  axes[2]  = probe->axes[1];
  origin   = probe->origin;
  
  // determine the transformation matrix
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x; 
  T(3,1) = origin.y; 
  T(3,2) = origin.z;

  T(0,0) = axes[0].x; T(0,1) = axes[0].y; T(0,2) = axes[0].z;
  T(1,0) = axes[1].x; T(1,1) = axes[1].y; T(1,2) = axes[1].z;
  T(2,0) = axes[2].x; T(2,1) = axes[2].y; T(2,2) = axes[2].z;

  // generate points
  o.qfeSetPointElements(0.0,0.0,0.0);
  for(int i = 0; i < seedsCount; i++)
  {
    qfeGenerateUniformSeed3D(point, extent);
    
    if(qfeInProbe(point, probe))
    {        
      set.push_back(point);      
    }
    else i--;
  }

  // transform the point to the plane coordinates
  for(int i=0; i<(int)set.size(); i++)
    seeds.push_back(set[i]*T);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsRandomUniform(qfeModel *model, int seedsCount, qfeSeeds &seeds)
{
  qfePoint    point, extent;
  double      bounds[6];
 
  // clear the sets
  seeds.clear();

  // get the model properties  
  model->qfeGetModelBounds(bounds[0], bounds[1], bounds[2], bounds[3], bounds[4], bounds[5]);

  for(int i = 0; i < seedsCount; i++)
  {
    qfeGenerateUniformSeed3D(point, bounds);

    if(qfeInModel(point, model) == true)
      seeds.push_back(point);      
    else
      i--;
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsRandomPoissonDisk(qfeFrameSlice *plane, int seedsCount, qfeSeeds &seeds)
{
  qfePoint    origin;
  qfePoint    extent;
  qfeVector   axes[3];
  qfePoint    point, newPoint, firstPoint;
  qfeSeeds    set;
  qfeFloat    minDist; 
  qfeMatrix4f T;

  // clear the sets
  set.clear();
  seeds.clear();

  // get the plane properties  
  plane->qfeGetFrameOrigin(origin.x, origin.y, origin.z);
  plane->qfeGetFrameExtent(extent.x, extent.y, extent.z);
  plane->qfeGetFrameAxes(axes[0].x, axes[0].y, axes[0].z, axes[1].x, axes[1].y, axes[1].z, axes[2].x, axes[2].y, axes[2].z);

  // determine the minimal distance
  minDist = (((qfeFloat)PI/4.0f) * 0.8f) / sqrt(seedsCount / (extent.x*extent.y));
  
  // determine the transformation matrix
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x; 
  T(3,1) = origin.y; 
  T(3,2) = origin.z;

  T(0,0) = axes[0].x; T(0,1) = axes[0].y; T(0,2) = axes[0].z;
  T(1,0) = axes[1].x; T(1,1) = axes[1].y; T(1,2) = axes[1].z;
  T(2,0) = axes[2].x; T(2,1) = axes[2].y; T(2,2) = axes[2].z;

  // set the initial point
  firstPoint.qfeSetPointElements(0.0,0.0,0.0);
  set.push_back(firstPoint);

  // generate points
  for(int i = 0; i <(seedsCount-1); i++)
  {
    int j = qfeGetRandomInt(0, (int)set.size()-1);

    point = set[j]; //random i

    qfeGeneratePoissonDiskSeed2D(point, minDist, newPoint);
    
    if (qfeInCircle(newPoint, firstPoint, 0.5f*min(extent.x, extent.y)) && !qfeInNeighborhood2D(newPoint, &set, minDist))
    {        
      set.push_back(newPoint);      
    }
    else i--;
  }

  // transform the point to the plane coordinates
  for(int i=0; i<(int)set.size(); i++)
    seeds.push_back(set[i]*T);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsRandomPoissonDisk(qfeProbe2D *probe, double density, qfeSeeds &seeds)
{
  int         seedsCount;
  qfePoint    point, newPoint, firstPoint;
  qfeSeeds    set;
  qfeFloat    minDist; 
  qfeMatrix4f T;

  // clear the sets
  set.clear();
  seeds.clear();

  // determine the minimal distance
  seedsCount  = (int)(probe->area * density);
  minDist     = (((qfeFloat)PI/4.0f) * 0.8f) / sqrt(seedsCount / ((2.0f*probe->radius)*(2.0f*probe->radius)));
  
  // determine the transformation matrix
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = probe->origin.x; 
  T(3,1) = probe->origin.y; 
  T(3,2) = probe->origin.z;

  T(0,0) = probe->axisX.x;  T(0,1) = probe->axisX.y;  T(0,2) = probe->axisX.z;
  T(1,0) = probe->axisY.x;  T(1,1) = probe->axisY.y;  T(1,2) = probe->axisY.z;
  T(2,0) = probe->normal.x; T(2,1) = probe->normal.y; T(2,2) = probe->normal.z;

  // set the initial point
  firstPoint.qfeSetPointElements(0.0,0.0,0.0);
  set.push_back(firstPoint);

  // generate points
  for(int i = 0; i <(seedsCount-1); i++)
  {
    int j = qfeGetRandomInt(0, (int)set.size()-1);

    point = set[j]; //random i

    qfeGeneratePoissonDiskSeed2D(point, minDist, newPoint);
    
    if (qfeInCircle(newPoint, firstPoint, probe->radius) && !qfeInNeighborhood2D(newPoint, &set, minDist))
    {        
      set.push_back(newPoint);      
    }
    else i--;
  }

  // transform the point to the plane coordinates
  for(int i=0; i<(int)set.size(); i++)
    seeds.push_back(set[i]*T);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsRandomPoissonDisk(qfeProbe3D *probe, int seedsCount, qfeSeeds &seeds)
{
  qfePoint    origin;
  qfePoint    extent;
  qfeVector   axes[3];
  qfePoint    point, newPoint, firstPoint;
  qfeSeeds    set;
  qfeFloat    minDist; 
  qfeMatrix4f T;

  // clear the sets
  set.clear();
  seeds.clear();

  // get the probe properties  
  extent.x = extent.z = 2.0f*max(probe->topRadius, probe->baseRadius);
  extent.y = probe->length;
  axes[0]  = probe->axes[0];
  axes[1]  = probe->axes[2];
  axes[2]  = probe->axes[1];
  origin   = probe->origin;  

  // determine the minimal distance
//  minDist = (((qfeFloat)PI/4.0f) * 0.8f) / sqrt(seedsCount / (extent.x*extent.y*extent.z));
  minDist = (((qfeFloat)PI/4.0f) * 0.8f) / pow(seedsCount / (extent.x*extent.y*extent.z), 0.33f);
  
  // determine the transformation matrix
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x; 
  T(3,1) = origin.y; 
  T(3,2) = origin.z;

  T(0,0) = axes[0].x; T(0,1) = axes[0].y; T(0,2) = axes[0].z;
  T(1,0) = axes[1].x; T(1,1) = axes[1].y; T(1,2) = axes[1].z;
  T(2,0) = axes[2].x; T(2,1) = axes[2].y; T(2,2) = axes[2].z;

  // set the initial point
  firstPoint.qfeSetPointElements(0.0,0.0,0.0);
  set.push_back(firstPoint);

  // generate points
  for(int i = 0; i <(seedsCount-1); i++)
  {
    int j = qfeGetRandomInt(0, (int)set.size()-1);

    point = set[j]; //random i

    qfeGeneratePoissonDiskSeed3D(point, minDist, newPoint);
    
    if (qfeInProbe(newPoint, probe) && !qfeInNeighborhood3D(newPoint, &set, minDist))
    {        
      set.push_back(newPoint);      
    }
    else i--;
  }

  // transform the point to the plane coordinates
  for(int i=0; i<(int)set.size(); i++)
    seeds.push_back(set[i]*T);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsRandomRectilinear(qfeProbe2D *probe, double density, qfeSeeds &seeds)
{ 
  int           seedCount;
  qfeFloat      seedArea, sampleSpacing;
  qfePoint      s, seed;
  qfeVector     x, y;
  int           count   = 0;

  // Compute the sample distance and set start point
  // We sample twice the given density in order to randomly remove afterwards.
  seedCount     = (int)   (probe->area * density);
  seedArea      = (float) (1.0 / density);
  sampleSpacing = sqrt(seedArea) / 2.0f;

  x  = probe->axisX;
  y  = probe->axisY;
  qfeVector::qfeVectorNormalize(x);
  qfeVector::qfeVectorNormalize(y);
  s  = probe->origin - x*probe->radius - y*probe->radius;
  
  // Initialize obtaining a random number
  srand((unsigned)time(0));    
  int lowest=0, highest=10000;
  int range=(highest-lowest)+1;

  // Steps in y direction
  for(float dy=0; dy < 2.0*probe->radius; dy+=sampleSpacing)  
  {
    // Steps in x direction
    for(float dx=0; dx< 2.0*probe->radius; dx+=sampleSpacing)
    {
      double randomVal = (lowest+int(range*rand()/(RAND_MAX + 1.0)))/10000.0;

      if(randomVal < 0.5)
      {
        seed = s + x*dx + y*dy;
        
        if(qfeSeeding::qfeInProbe(seed, probe) == true)
        {
          seeds.push_back(seed);
        }
      }      
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsRadial(qfeProbe2D *probe, double density, qfeSeeds &seeds)
{
  qfeFloat      pi = 3.14159265f;
  qfeFloat      seedArea, sampleSpacing, lineAngle;
  int           seedCount, lineSamples, lineCount;  

  // Determine how many seeds will be computed and how they will be spaced
  seedCount     = (int)   (probe->area * density);
  seedArea      = (float) (1.0 / density);
  sampleSpacing = sqrt(seedArea);

  lineCount   = (int)ceil((seedCount * sampleSpacing)/probe->radius);  
  if(lineCount == 0) return qfeError;

  lineSamples = (int)(probe->radius / sampleSpacing);
  lineAngle   = 360 / (float)lineCount;

  // Compute the seeds
  seeds.clear();
  seeds.push_back(probe->origin);

  for(int i=0; i<lineCount; i++)
  {
    for(int j=1; j<=lineSamples; j++)
    {
      qfePoint  seed;

      qfeFloat x2d, y2d;
      qfeFloat r, w;

      r = j*sampleSpacing;
      w = 1.0f - (r / probe->radius);

      r = probe->radius*w;

      x2d = r * cos(i * lineAngle * (pi/180.0f));
      y2d = r * sin(i * lineAngle * (pi/180.0f));

      seed = probe->origin + probe->axisX*x2d + probe->axisY*y2d;

      if(qfeSeeding::qfeInProbe(seed, probe) == true)
      {
        seeds.push_back(seed);
      }
    }    
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsCircles(qfeProbe2D *probe, double density, qfeSeeds &seeds)
{
  qfeFloat      pi = 3.14159265f;
  qfeFloat      seedSpacing, ringLength, ringSpacing;
  int           seedCount, ringCount;  

  // Determine how many seeds will be computed and how they will be spaced
  ringSpacing = (float)(1.0f / density);
  ringCount   = (int)(probe->radius / ringSpacing);
  seedCount   = (int)(probe->area * density);

  if(ringCount < 3) ringCount = 3;

  ringLength  = 0.0;
  for(int i=0; i<ringCount; i++)
  {
    qfeFloat r  = i*(probe->radius/ringCount);
    ringLength += 2.0f*pi*r;
  }

  seedSpacing = ringLength / (float)seedCount;

  // Compute the seeds
  seeds.clear();

  for(int i=0; i<ringCount; i++)
  {    
    qfePoint  seed;
    qfeFloat  r, c;
    int       n;
    
    r    = i*(probe->radius/ringCount); // radius
    c    = 2.0f*pi*r;                   // circumference
    n    = (int)(c / seedSpacing);      // number of points

    if(r == 0)
    {
      seed = probe->origin;
      seeds.push_back(seed);
    }
    else
    {
      qfeVector v;
      qfeFloat  opposite, degr;
      
      v         = probe->axisX;
      degr      = 360.0f/(qfeFloat)n;
      opposite  = r * tan(degr * (pi/180.0f));

      for(int j=0; j<n; j++)
      {
        qfeVector w;        
        
        w = v^probe->normal;
        v = v*r + w*opposite;
        qfeVector::qfeVectorNormalize(v);

        seed = probe->origin + v*r;

        if(qfeSeeding::qfeInProbe(seed, probe) == true)
          seeds.push_back(seed);
      }
    }    
  }  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsRectilinear(qfeProbe2D *probe, double density, qfeSeeds &seeds)
{

  int           seedCount;
  qfeFloat      seedArea, sampleSpacing;
  qfePoint      s, seed;
  qfeVector     x, y;
  int           count   = 0;

  // Compute the sample distance and set start point
  seedCount     = (int)   (probe->area * density);
  seedArea      = (float) (1.0 / density);
  sampleSpacing = sqrt(seedArea);

  x  = probe->axisX;
  y  = probe->axisY;
  qfeVector::qfeVectorNormalize(x);
  qfeVector::qfeVectorNormalize(y);
  s  = probe->origin - x*probe->radius - y*probe->radius;
  
  // Steps in y direction
  for(float dy=0; dy < 2.0*probe->radius; dy+=sampleSpacing)  
  {
    // Steps in x direction
    for(float dx=0; dx< 2.0*probe->radius; dx+=sampleSpacing)
    {
      seed = s + x*dx + y*dy;
      
      if(qfeSeeding::qfeInProbe(seed, probe) == true)
      {
        seeds.push_back(seed);
      }
    }
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsCircle(qfePoint origin, qfeVector normal, qfeFloat radius, int seedsCount, qfeSeeds &seeds)
{ 
  qfeVector   axes[3];
  qfeFloat    angle;
  qfeSeeds    set;
  qfeMatrix4f T, R, S, M;

  axes[2] = normal;
  qfeVector::qfeVectorNormalize(axes[2]);
  qfeVector::qfeVectorOrthonormalBasis2(axes[2], axes[0], axes[1]);
  
  // clear the sets
  set.clear();
  seeds.clear();

  // generate a unit circle
  angle = (qfeFloat)(2.0*PI) / (qfeFloat)seedsCount;

  for(int i=0; i<seedsCount;i++)
  {
    qfePoint p;

    p.x = cos(i*angle);
    p.y = sin(i*angle);
    p.z = 0.0;    

    set.push_back(p);
  }

  // transform to patient coordinates
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x;
  T(3,1) = origin.y;
  T(3,2) = origin.z;

  qfeMatrix4f::qfeSetMatrixIdentity(R);
  R(0,0) = axes[0].x; R(0,1) = axes[0].y; R(0,2) = axes[0].z;
  R(1,0) = axes[1].x; R(1,1) = axes[1].y; R(1,2) = axes[1].z;
  R(2,0) = axes[2].x; R(2,1) = axes[2].y; R(2,2) = axes[2].z;
  
  qfeMatrix4f::qfeSetMatrixIdentity(S);
  S(0,0) = radius;
  S(1,1) = radius;
  S(2,2) = radius;

  // return the seeds
  for(int i=0; i<seedsCount;i++)
  {
    seeds.push_back(set[i]*(S*R*T));
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsCircle(qfeProbe3D *probe, int seedsCount, qfeSeeds &seeds)
{
  qfePoint    origin;
  qfeFloat    topRadius, baseRadius;
  qfeVector   axes[3];
  qfeFloat    angle, radius, offset;
  qfeSeeds    set;
  qfeMatrix4f T, R, S, M;

  origin     = probe->origin;
  axes[0]    = probe->axes[0];
  axes[1]    = probe->axes[1];
  axes[2]    = probe->axes[2];
  topRadius  = probe->topRadius;
  baseRadius = probe->baseRadius;

  offset     = 0.5f;
  radius     = offset * (min(baseRadius,topRadius) + 0.5f*abs(topRadius-baseRadius));

  // clear the sets
  set.clear();
  seeds.clear();

  // generate a unit circle
  angle = (qfeFloat)(2.0*PI) / (qfeFloat)seedsCount;

  for(int i=0; i<seedsCount;i++)
  {
    qfePoint p;

    p.x = cos(i*angle);
    p.y = sin(i*angle);
    p.z = 0.0;    

    set.push_back(p);
  }

  // transform to patient coordinates
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x;
  T(3,1) = origin.y;
  T(3,2) = origin.z;

  qfeMatrix4f::qfeSetMatrixIdentity(R);
  R(0,0) = axes[0].x; R(0,1) = axes[0].y; R(0,2) = axes[0].z;
  R(1,0) = axes[1].x; R(1,1) = axes[1].y; R(1,2) = axes[1].z;
  R(2,0) = axes[2].x; R(2,1) = axes[2].y; R(2,2) = axes[2].z;
  
  qfeMatrix4f::qfeSetMatrixIdentity(S);
  S(0,0) = radius;
  S(1,1) = radius;
  S(2,2) = radius;

  // return the seeds
  for(int i=0; i<seedsCount;i++)
  {
    seeds.push_back(set[i]*(S*R*T));
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsLine(qfePoint origin, qfeVector direction, qfeFloat radius, int seedsCount, qfeSeeds &seeds)
{   
  qfeVector   axes[3];
  qfeFloat    stepsize, offset;
  qfeSeeds    set;
  qfeMatrix4f T, R, S, M;

  offset     = 0.5f;

  axes[0] = direction;
  qfeVector::qfeVectorNormalize(axes[0]);
  qfeVector::qfeVectorOrthonormalBasis2(axes[0], axes[2], axes[1]);

  // clear the sets
  set.clear();
  seeds.clear();

  // generate a line along the x direction
  stepsize = 1.0f / (qfeFloat)seedsCount;

  for(int i=0; i<seedsCount;i++)
  {
    qfePoint p;

    p.x = (-offset + i*stepsize);
    p.y = 0.0;
    p.z = 0.0;    

    set.push_back(p);
  }

  // transform to patient coordinates
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x;
  T(3,1) = origin.y;
  T(3,2) = origin.z;

  qfeMatrix4f::qfeSetMatrixIdentity(R);
  R(0,0) = axes[0].x; R(0,1) = axes[0].y; R(0,2) = axes[0].z;
  R(1,0) = axes[1].x; R(1,1) = axes[1].y; R(1,2) = axes[1].z;
  R(2,0) = axes[2].x; R(2,1) = axes[2].y; R(2,2) = axes[2].z;
  
  qfeMatrix4f::qfeSetMatrixIdentity(S);
  S(0,0) = radius;
  S(1,1) = radius;
  S(2,2) = radius;

  // return the seeds
  for(int i=0; i<seedsCount;i++)
  {
    seeds.push_back(set[i]*(S*R*T));
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsLine(qfeProbe3D *probe, qfeVector vec2D, int seedsCount, qfeSeeds &seeds)
{
  qfePoint    origin;
  qfeFloat    topRadius, baseRadius;
  qfeVector   axes[3];
  qfeFloat    radius, stepsize, offset;
  qfeSeeds    set;
  qfeMatrix4f T, R, S, M;

  origin     = probe->origin;
  axes[0]    = probe->axes[0];
  axes[1]    = probe->axes[1];
  axes[2]    = probe->axes[2];
  topRadius  = probe->topRadius;
  baseRadius = probe->baseRadius;

  offset     = 0.5f;
  radius     = offset * (min(baseRadius,topRadius) + 0.5f*abs(topRadius-baseRadius));

  // clear the sets
  set.clear();
  seeds.clear();

  // generate a line along the x direction
  stepsize = 1.0f / (qfeFloat)seedsCount;

  for(int i=0; i<seedsCount;i++)
  {
    qfePoint p;

    p.x = (-offset + i*stepsize)*vec2D.x;
    p.y = (-offset + i*stepsize)*vec2D.y;
    p.z = 0.0;    

    set.push_back(p);
  }

  // transform to patient coordinates
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x;
  T(3,1) = origin.y;
  T(3,2) = origin.z;

  qfeMatrix4f::qfeSetMatrixIdentity(R);
  R(0,0) = axes[0].x; R(0,1) = axes[0].y; R(0,2) = axes[0].z;
  R(1,0) = axes[1].x; R(1,1) = axes[1].y; R(1,2) = axes[1].z;
  R(2,0) = axes[2].x; R(2,1) = axes[2].y; R(2,2) = axes[2].z;
  
  qfeMatrix4f::qfeSetMatrixIdentity(S);
  S(0,0) = radius;
  S(1,1) = radius;
  S(2,2) = radius;

  // return the seeds
  for(int i=0; i<seedsCount;i++)
  {
    seeds.push_back(set[i]*(S*R*T));
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGetSeedsLine(qfeProbe3D *probe, int seedsCount, qfeSeeds &seeds)
{
  qfeVector v;

  v.qfeSetVectorElements(1.0,0.0,0.0);

  return qfeGetSeedsLine(probe, v, seedsCount, seeds);
 
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeComputeSeedsTransferFunction(qfeProbe2D *probe, qfeSeedTransferFunction tf, qfeSeeds &seeds)
{
  qfeSeeds      seedsCopy;

  if(seeds.size() <= 0) return qfeError;

  seedsCopy = seeds;
  seeds.clear();

  for(int i=0; i<(int)seedsCopy.size(); i++)
  {
    qfeVector os, qs;
    qfePoint  seed, q;
    qfeFloat  length;
    
    switch(tf)
    {
    case QFE_SEED_DENSITY_HOMOGENEOUS   : seed = seedsCopy[i]; 
                                          break;
    case QFE_SEED_DENSITY_VESSEL_CENTER : os = (seedsCopy[i] - probe->origin);
                                          qfeVector::qfeVectorLength(os, length);                                        
                                          seed = probe->origin + (os* (length / probe->radius));
                                          break;
    case QFE_SEED_DENSITY_VESSEL_WALL   : os = (seedsCopy[i] - probe->origin);                                          
                                          qfeVector::qfeVectorNormalize(os); 
                                          q  = probe->origin + (os * probe->radius);
                                          qs = seedsCopy[i] - q;
                                          qfeVector::qfeVectorLength(qs, length); 
                                          seed = q + (qs* (length / probe->radius));
                                          break;
    default                             : seed = seedsCopy[i]; 
    }

    if(qfeSeeding::qfeInProbe(seed, probe) == true)
    {
      seeds.push_back(seed);
    }    
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
bool qfeSeedDepthSortPredicate(qfePoint i, qfePoint j)
{
  if(i.z < j.z) return true;
  else return false;
}

//---------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeComputeSeedsDepthSort(qfeMatrix4f modelview, qfeSeeds &seeds)
{
  if(seeds.size() <= 0) return qfeError;

  qfeMatrix4f modelviewInv;
 
  // Get the transformation matrix
  qfeMatrix4f::qfeGetMatrixInverse(modelview, modelviewInv);

  qfeSeeds seedsWorld;

  seedsWorld.resize(seeds.size());
  copy(seeds.begin(), seeds.end(), seedsWorld.begin());

  // Transform to world space
  for(int i=0; i<(int)seedsWorld.size(); i++)
  {
    seedsWorld[i] = seedsWorld[i] * modelview;
  }

  // Sort by depth value
  std::sort(seedsWorld.begin(), seedsWorld.end(), qfeSeedDepthSortPredicate);
  seeds.clear();

  // Transform back to voxel space
  for(int i=0; i<(int)seedsWorld.size(); i++)
  {    
    seeds.push_back(seedsWorld[i]*modelviewInv);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGenerateUniformSeed2D(qfePoint &p, qfePoint extent)
{
  p.x = (qfeFloat)qfeGetRandomDouble(-0.5f*extent.x, 0.5f*extent.x);
  p.y = (qfeFloat)qfeGetRandomDouble(-0.5f*extent.y, 0.5f*extent.y);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGenerateUniformSeed3D(qfePoint &p, qfePoint extent)
{
  p.x = (qfeFloat)qfeGetRandomDouble(-0.5f*extent.x, 0.5f*extent.x);
  p.y = (qfeFloat)qfeGetRandomDouble(-0.5f*extent.y, 0.5f*extent.y);
  p.z = (qfeFloat)qfeGetRandomDouble(-0.5f*extent.z, 0.5f*extent.z);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSeeding::qfeGenerateUniformSeed3D(qfePoint &p, double bounds[])
{
  p.x = (qfeFloat)qfeGetRandomDouble(bounds[0], bounds[1]);
  p.y = (qfeFloat)qfeGetRandomDouble(bounds[2], bounds[3]);
  p.z = (qfeFloat)qfeGetRandomDouble(bounds[4], bounds[5]);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Based on particle flurries paper
qfeReturnStatus qfeSeeding::qfeGeneratePoissonDiskSeed2D(qfePoint p, qfeFloat minDist, qfePoint &q)
{   
  qfePoint point;
  qfeFloat r1, r2;
  qfeFloat radius, angle;

  //non-uniform, favours points closer to the inner ring, leads to denser packings
  r1 = (qfeFloat)qfeGetRandomDouble();
  r2 = (qfeFloat)qfeGetRandomDouble();

  //random radius between minDist and 2 * minDist
  radius = minDist * (r1 + 1.0f);
  
  //random angle
  angle = 2.0f * (qfeFloat)PI * r2;

  //the new point is generated around the point (x, y)
  point.x = p.x + radius * cos(angle);
  point.y = p.y + radius * sin(angle);

  q       = point;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// Based on particle flurries paper
qfeReturnStatus qfeSeeding::qfeGeneratePoissonDiskSeed3D(qfePoint p, qfeFloat minDist, qfePoint &q)
{   
  qfePoint point;
  qfeFloat r1, r2, r3;
  qfeFloat radius, angle1, angle2;

  //non-uniform, favours points closer to the inner ring, leads to denser packings
  r1 = (qfeFloat)qfeGetRandomDouble();
  r2 = (qfeFloat)qfeGetRandomDouble();
  r3 = (qfeFloat)qfeGetRandomDouble();

  //random radius between minDist and 2 * minDist
  radius = minDist * (r1 + 1.0f);
  
  //random angle
  angle1 = 2.0f * (qfeFloat)PI * r2;
  angle2 = 2.0f * (qfeFloat)PI * r3;

  //the new point is generated around the point (x, y)
  point.x = p.x + radius * cos(angle1) * sin(angle2);
  point.y = p.y + radius * sin(angle1) * sin(angle2);
  point.z = p.z + radius * cos(angle2);

  q       = point;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
bool qfeSeeding::qfeInRectangle(qfePoint p, qfeFloat width, qfeFloat height)
{
  if((p.x > -0.5f*width)  && (p.x < 0.5f*width) && 
     (p.y > -0.5f*height) && (p.y < 0.5f*height))
    return true;
  else
    return false;
}

//----------------------------------------------------------------------------
bool qfeSeeding::qfeInCircle(qfePoint p, qfePoint origin, qfeFloat radius)
{
  qfeFloat length;
  qfeVector::qfeVectorLength(p - origin, length);
  
  if(length < radius)    
    return true;
  else
    return false;
}

//----------------------------------------------------------------------------
bool qfeSeeding::qfeInProbe(qfePoint p, qfeProbe2D *probe)
{
  bool result = false;  

  qfePoint o2D, p2D, q2D, r3D_A, r3D_B, r2D_A, r2D_B;

  // Get two point of the line we will check
  o2D.x = o2D.y = o2D.z = 0.0;
  qfeSeeding::qfeProjectPointToPlane(probe->axisX, probe->axisY, probe->origin, p, p2D);

  int count = 0;
  for(int i=0; i<(int)probe->points.size(); i++)
  {
    // Get the two points of the line segment in 3D    
    r3D_A = probe->points[i];
    r3D_B = probe->points[(i+1)%probe->points.size()];

    // Project the two points to the 2D plane
    qfeSeeding::qfeProjectPointToPlane(probe->axisX, probe->axisY, probe->origin, r3D_A, r2D_A);
    qfeSeeding::qfeProjectPointToPlane(probe->axisX, probe->axisY, probe->origin, r3D_B, r2D_B);

    if(qfeSeeding::qfeIntersectLines(o2D, p2D, r2D_A, r2D_B, q2D) ==  true)
    {
      count++;
    } 
  }

  // True when we have an even number of intersections we are inside the contour
  if((count % 2) == 0)
    return true;
  else 
    return false;
}

//----------------------------------------------------------------------------
bool qfeSeeding::qfeInProbe(qfePoint p, qfeProbe3D *probe)
{
  qfeFloat length;
  qfeFloat topRadius, baseRadius, localRadius;  
   
  baseRadius  = probe->baseRadius;
  topRadius   = probe->topRadius;

  localRadius = min(baseRadius, topRadius) + ((0.5f+(p.y / probe->length)) * abs(topRadius-baseRadius));
  length      = sqrt(p.x*p.x + p.z*p.z);

  if((length < localRadius) && (p.y<0.5f*probe->length) && (p.y>-0.5f*probe->length))    
    return true;
  else
    return false;
}

//----------------------------------------------------------------------------
bool qfeSeeding::qfeInModel(qfePoint p, qfeModel *model)
{
  vector< float> point;

  point.push_back(p.x);
  point.push_back(p.y);
  point.push_back(p.z);

  return model->qfeGetModelContainsPoint(point);
}

//----------------------------------------------------------------------------
bool qfeSeeding::qfeInNeighborhood2D(qfePoint p, qfeSeeds *set, qfeFloat minDist)
{
  qfeFloat  distance;
  qfeVector vec;

  for(int i=0; i<(int)set->size(); i++)
  {
    vec = p - set->at(i);
    distance = sqrt(vec.x*vec.x + vec.y*vec.y);

    if(distance < minDist) return true;
  }  
  return false;
}

//----------------------------------------------------------------------------
bool qfeSeeding::qfeInNeighborhood3D(qfePoint p, qfeSeeds *set, qfeFloat minDist)
{
  qfeFloat distance;  

  for(int i=0; i<(int)set->size(); i++)
  {
    qfeVector::qfeVectorLength(p - set->at(i), distance);
    if(distance < minDist) return true;
  }  
  return false;
}

//----------------------------------------------------------------------------
double qfeSeeding::qfeGetRandomDouble()
{
  return qfeGetRandomDouble(0.0, 1.0);
}

//----------------------------------------------------------------------------
double qfeSeeding::qfeGetRandomDouble(double low, double high)
{
  double range = high - low;
  double num   = static_cast<double>( rand() ) * range / static_cast<double>( RAND_MAX ) + low ;

  return num;
}

//----------------------------------------------------------------------------
int qfeSeeding::qfeGetRandomInt(int low, int high)
{
  int range = high - low;
  int num   = static_cast<int>( rand() ) * range / static_cast<int>( RAND_MAX ) + low ;

  return num;
}

//----------------------------------------------------------------------------
void qfeSeeding::qfeProjectPointToPlane(qfeVector x, qfeVector y, qfePoint o, qfePoint p3D, qfePoint &p2D)
{
  p2D.x = x * (p3D - o);
  p2D.y = y * (p3D - o);
  p2D.z = 0.0;
}

//----------------------------------------------------------------------------
bool qfeSeeding::qfeIntersectLines(qfePoint a, qfePoint b, qfePoint c, qfePoint d, qfePoint &p)
{
  qfeFloat r, s, den;

  den =  (b.x-a.x)*(d.y-c.y) - (b.y-a.y)*(d.x-c.x);
  r   = ((a.y-c.y)*(d.x-c.x) - (a.x-c.x)*(d.y-c.y)) / den;
  s   = ((a.y-c.y)*(b.x-a.x) - (a.x-c.x)*(b.y-a.y)) / den;

  p.x = a.x + (b.x - a.x)*r;
  p.y = a.y + (b.y - a.y)*s;

  if(r >= 0 && r <=1 && s >=0 && s <=1) 
    return true;
  else 
    return false;
}