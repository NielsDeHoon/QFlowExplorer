#pragma once

#include "qflowexplorer.h"

#include "qfeFluidInteractor.h"

#include "fluidsim\FLIP_simulation.h"
#include "fluidsim\MACGrid.h"

#include "qfeAlgorithm.h"
#include "qfeColorMap.h"
#include "qfeDriver.h"
#include "qfeGLShaderProgram.h"
#include "qfeImageData.h"
#include "qfeMatrix4f.h"
#include "qfeTransform.h"
#include "qfeVolume.h"
#include "qfeModel.h"
#include <QtCore>

// TODO NDH move to data manager
#include "io/qfeVTIImageDataWriter.h"
#include "core/qfeConvert.h"

// vtk includes (needed for the mesh)
#include <vtkActor.h>
#include <vtkCellArray.h>
#include <vtkCurvatures.h>
#include <vtkPoints.h>
#include <vtkPolyDataMapper.h>
#include <vtkTriangleFilter.h>
#include <vtkProperty.h>
#include <vtkPolyData.h>
#include <vtkRegularPolygonSource.h>
#include <vtkFeatureEdges.h>
#include <vtkCleanPolyData.h>
#include <vtkExtractVOI.h>
#include <vtkStructuredPoints.h>
#include <vtkContourFilter.h>
#include <vtkPolyDataNormals.h>

// vtk includes needed for writing files (above vtk imports might also be needed)
#include <vtkXMLImageDataWriter.h>
#include <vtkPolyDataWriter.h>
#include <vtkIntArray.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>

#include <vector>
#include <limits>

/**
* \file   qfeSimulation.h
* \author Niels de Hoon & Roy van Pelt
* \class  qfeSimulation
* \brief  Implements a fluid simulation
* \note   Confidential
*
* Computetion of fluid simulation for coupling with measured data
*
*/

#define BLOOD_DENSITY 1.060
// mu_blood = (3 - 4) * 10^-3 Pa * s
#define BLOOD_VISCOSITY 3.5*1e-3

#define ENRIGHT_EXPERIMENT false

class qfeSimulation : public qfeAlgorithm
{
public:
  qfeSimulation();
  qfeSimulation(const qfeSimulation &pr);
  ~qfeSimulation();

  qfeReturnStatus qfeGetSimulationModel(qfeModel &model);

  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);
  qfeReturnStatus qfeSetViewportSize(unsigned int x, unsigned int y, int supersampling);
  
  qfeReturnStatus qfeSetVolume(qfeVolume *volume);
  qfeReturnStatus qfeSetGeometryBuffers(GLuint color, GLuint depth);
  qfeReturnStatus qfeSetVelocityField(qfeVolumeSeries mVelocityField);

  qfeReturnStatus qfeSetNearBoundary(bool remove);
  qfeReturnStatus qfeSetFractionOfParticles(double fraction);
  qfeReturnStatus qfeSetTailLength( unsigned int tailLength);
  
  qfeReturnStatus qfeGetParticlePositions(std::vector<std::vector<float>> &positions);
  qfeReturnStatus qfeGetParticleVelocities(std::vector<std::vector<float>> &velocities);
  double          qfeGetSimulationTime();
  qfeReturnStatus qfeInitFluidSimulation(unsigned int _nx, qfeVolume *volume, vector< qfeModel  * > modelSeries, qfeStudy *_study);
  void qfeAdvanceOneFrame(double frameTime);
  void qfeSeedParticles();

  void qfeUpdateSerie(unsigned int index);
  void qfeUpdateTime(double t);  
  
  qfeReturnStatus qfeRenderParticles(qfeVolume *volume);
  qfeReturnStatus qfeRenderEnergy(qfeVolume *volume);
  qfeReturnStatus qfeRenderInterpolation(qfeVolume *volume);
  qfeReturnStatus qfeRenderGrid(qfeVolume *volume);
  
  qfeReturnStatus qfeWriteErrors();
  qfeReturnStatus qfeInterpolationErrors();
  qfeReturnStatus qfeNoiseErrors();
  void qfeWriteInterpolationErrors();

  Vec3f VoxelVelocityToSimVelocity(Vec3f vel);
  Vec3f SimVelocityToVoxelVelocity(Vec3f vel);

  qfeVolume *WriteCurrentVelocityFieldToVolume(int id, int phaseCount, float phaseDuration, float phaseTriggerTime);
  qfeVolume *WriteSimulationVelocityFieldToVolume(int id, int phaseCount, string fileName, float phaseDuration, float phaseTriggerTime, Array3Vec3 velocityField);

  Vec3f EnrightData(float x, float y, float z, float phase);
  qfeReturnStatus qfeGenerateEnrightData();

  void applyDifferenceMethod();
  void computeNabla(Array3f &result, Array3f &input, bool x, bool y, bool z);

  //fluid simulation:
  void initializeFluidSimulation(unsigned int _nx, qfeVolume *volume, vector< qfeModel  * > ModelSeries);
  double actualCellWidth;
  void setMeasurementVelocityField(double measurementTime);

  Vec3f simulationToTexture(Vec3f in);
  Vec3f textureToSimulation(Vec3f in);

  bool removeNearBoundary;

  qfeReturnStatus qfeStoreSimulationData(qfeVolume *volume,int measurementNumber);
  qfeReturnStatus qfeGenerateSyntheticData();

  float GaussianNoise();
  float GaussianNoise(float SNR);

  Vec3f qfeInterpolateMeasurements(float x, float y, float z, float inPhase);

  void qfeCompareSimInterpol(qfeVolume *volume);

  qfeReturnStatus qfeComputeFluidInteractor(qfePoint simCoord, qfeFluidInteractor::InteractorType type, qfeFluidInteractor& interactor);

  bool negative_time_step;

  qfeReturnStatus qfeUseViscosity(bool use_viscosity);

  void qfeMakeIncompressible(int timeDirection);

//protected:

//private :
  bool                firstRender; 
  
  qfeStudy *study;

  bool allErrorsWritten;

  unsigned int        viewportSize[2];

  Vec3f interpolateMeasurement(float x, float y, float z, double inPhase);
  Vec3f interpolateSimulation(float x, float y, float z);
  float interpolatePressure(float x, float y, float z);
  float interpolateSolidPhi(float x, float y, float z);
  
  Vec3f interpolateSyntheticData(float x, float y, float z, float phase);

  qfeReturnStatus preprocessMesh(vector< qfeModel  * > modelSeries, qfeVolume *volume);
  qfeReturnStatus buildMesh(qfeVolume *volume);

  qfeReturnStatus qfeRenderBoundingBox(qfeVolume *volume);

  float phaseDuration;

  //Fluid simulation:
  FLIP_simulation	  *fluidsim;
  FLIP_simulation	  *fluidsimNoise;
  float noise;

  std::vector<MACGrid> MacGridsInnerSteps;

  vtkPolyData *mesh;

  qfeVolumeSeries velocityField;

  vector<Vec3f> Vertices;
  QList<Vec3f> Normals; //needs a lot of replacements and is faster than std vector or list in this case
  vector<Vec3ui> Triangles;

  Vec3f GetSimulationVelocityAsMeasurement(Vec3f point);

  unsigned int nx,ny,nz;
  
  unsigned int serie;

  double time; //represents the time within the animation related to the frames
  
  double scaleSimX;
  double scaleSimY;
  double scaleSimZ;

  qfeLightSource       *light;

  //parameters:
  float fractionOfParticlesToRender;
  unsigned int tailLength; 

  Vec3f trilerpMeasurement(float x, float y, float z, unsigned int serie);
	
  qfeVolume *volumeMeasurement;

  double dx;

  double lx, ly, lz;
  unsigned int volumeDimX;
  unsigned int volumeDimY;
  unsigned int volumeDimZ;

  Array3i numberOfParticlesInitialized;

  int measurementsWritten;
  bool writingErrors;

  void AverageVelocity();

  float avgVeloc;

  qfeModel simulation_model;
  bool simulation_model_computed;
};
