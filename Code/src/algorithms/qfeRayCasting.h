#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithm.h"
#include "qfeColorMap.h"
#include "qfeDisplay.h"
#include "qfeDriver.h"
#include "qfeGLShaderProgram.h"
#include "qfeGeometry.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeTransform.h"
#include "qfeVolume.h"

/**
* \file   qfeRayCasting.h
* \author Roy van Pelt
* \class  qfeRayCasting
* \brief  Implements a raycaster
* \note   Confidential
*
* Rendering a volume using raycasting
*
*/

class qfeRayCasting : qfeAlgorithm
{
public:
  qfeRayCasting();
  qfeRayCasting(const qfeRayCasting &pr);
  ~qfeRayCasting();

  qfeReturnStatus qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *volume, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeColorMap *colormap);

  qfeReturnStatus qfeSetViewportSize(unsigned int x, unsigned int y, int supersampling);
  qfeReturnStatus qfeSetIntersectionBuffers(GLuint color, GLuint depth);  
  qfeReturnStatus qfeSetConvexClippingBuffers(GLuint depthFront, GLuint dephBack);  
  qfeReturnStatus qfeSetOrthoClippingRanges(qfeRange x, qfeRange y, qfeRange z);
  qfeReturnStatus qfeSetObliqueClippingPlane(qfeFrameSlice *plane);
  qfeReturnStatus qfeSetDataComponent(int component);
  qfeReturnStatus qfeSetDataRepresentation(int type);
  qfeReturnStatus qfeSetDataRange(qfeFloat vl, qfeFloat vu);
  qfeReturnStatus qfeSetGradientRendering(bool shading);  
  qfeReturnStatus qfeSetShading(bool shading);  
  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);
  qfeReturnStatus qfeSetQuality(float factor);
  qfeReturnStatus qfeSetStyle(int style);
  qfeReturnStatus qfeSetMIDAgamma(float gamma);
  
  qfeReturnStatus qfeRenderBoundingBox(qfeVolume *volume);
protected:  
  bool                firstRender;
  qfeGLShaderProgram *shaderDVR;

  qfeLightSource     *light;
  qfeMatrix4f         V2P;

  unsigned int        viewportSize[2];
  int                 superSampling;

  GLuint              texRayEnd;
  GLuint              texRayEndDepth;
  GLuint              texVolume;
  GLuint              texColorMap;
  GLuint              texGradientMap;
  GLuint              texIntersectColor;
  GLuint              texIntersectDepth;  
  GLuint              texClippingFrontDepth;
  GLuint              texClippingBackDepth;
  qfeVector           planeClippingOblique;

  int                 paramData;
  float               paramScaling[3];           
  float               paramStepSize;
  float               paramQuality;
  int                 paramComponent;
  int                 paramRepresentation;
  float               paramDataRange[2];
  int                 paramMode;
  bool                paramGradient;
  bool                paramShading;
  float               paramClipping[6];
  int                 paramStyle;
  float               paramMIDAgamma;

  qfeReturnStatus qfeCreateTextures();
  qfeReturnStatus qfeDeleteTextures();
  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

  //qfeReturnStatus qfeRenderBoundingBox(qfeVolume *volume);
  qfeReturnStatus qfeRenderBoundingBox(qfeVolume *boundingVolume, qfeVolume* textureVolume);

  qfeReturnStatus qfeClampTextureRange(qfePoint pos, qfePoint &newpos);  

  qfeReturnStatus qfeGetModelViewMatrix();

  GLfloat    matrixModelViewInverse[16];
  GLfloat    matrixModelViewProjection[16];
  GLfloat    matrixModelViewProjectionInverse[16];
};
