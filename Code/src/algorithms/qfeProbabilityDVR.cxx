#include "qfeProbabilityDVR.h"

//----------------------------------------------------------------------------
qfeProbabilityDVR::qfeProbabilityDVR() {
  shaderIVR = new qfeGLShaderProgram();
  shaderIVR->qfeAddShaderFromFile("/shaders/library/transform.glsl",QFE_FRAGMENT_SHADER);
  shaderIVR->qfeAddShaderFromFile("/shaders/library/fetch.glsl",    QFE_FRAGMENT_SHADER);
  shaderIVR->qfeAddShaderFromFile("/shaders/library/normalize.glsl",QFE_FRAGMENT_SHADER);
  shaderIVR->qfeAddShaderFromFile("/shaders/library/composite.glsl",QFE_FRAGMENT_SHADER);
  shaderIVR->qfeAddShaderFromFile("/shaders/library/shading.glsl",  QFE_FRAGMENT_SHADER);
  shaderIVR->qfeAddShaderFromFile("/shaders/pdvr/pdvr-vert.glsl",     QFE_VERTEX_SHADER);  
  shaderIVR->qfeAddShaderFromFile("/shaders/pdvr/pdvr-frag.glsl",     QFE_FRAGMENT_SHADER);
  shaderIVR->qfeLink();

  firstRender           = true;

  texRayEnd             = 0;
  texRayEndDepth        = 0;
  texVolume0            = 0;
  texVolume1            = 0;
  tfmTex0               = 0;
  tfmTex1               = 0;

  texIntersectColor     = 0;
  texIntersectDepth     = 0;  

  paramScaling[0]       = 1.0f;
  paramScaling[1]       = 1.0f;
  paramScaling[2]       = 1.0f;
  paramStepSize         = 1.0f/250.0f;

  paramDataRange0[0]    = 0.f;
  paramDataRange0[1]    = 1.f;
  paramDataRange1[0]    = 0.f;
  paramDataRange1[1]    = 1.f;

  dataSet0Available     = false;
  dataSet1Available     = false;

  useVoIClipping        = false;
}

//----------------------------------------------------------------------------
qfeProbabilityDVR::qfeProbabilityDVR(const qfeProbabilityDVR &pr) {
  firstRender                  = pr.firstRender;
  shaderIVR                    = pr.shaderIVR;

  viewportSize[0]              = pr.viewportSize[0];
  viewportSize[1]              = pr.viewportSize[1];
  superSampling                = pr.superSampling;

  texRayEnd                    = pr.texRayEnd;
  texRayEndDepth               = pr.texRayEndDepth;
  texVolume0                   = pr.texVolume0;
  texVolume1                   = pr.texVolume1;

  texIntersectColor            = pr.texIntersectColor;
  texIntersectDepth            = pr.texIntersectDepth;  

  paramScaling[0]              = pr.paramScaling[0];
  paramScaling[1]              = pr.paramScaling[1];
  paramScaling[2]              = pr.paramScaling[2];
  paramStepSize                = pr.paramStepSize;
  paramDataRange0[0]           = pr.paramDataRange0[0];
  paramDataRange0[1]           = pr.paramDataRange0[1];
  paramDataRange1[0]           = pr.paramDataRange1[0];
  paramDataRange1[1]           = pr.paramDataRange1[1];

  dataSet0Available            = pr.dataSet0Available;
  dataSet1Available            = pr.dataSet1Available;

  useVoIClipping               = pr.useVoIClipping;

  V2P                          = pr.V2P;
}

//----------------------------------------------------------------------------
qfeProbabilityDVR::~qfeProbabilityDVR() {
  delete shaderIVR;
  qfeDeleteTextures();
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *vol0, qfeVolume *vol1, QTransFunc tf0, QTransFunc tf1) {
  if (!vol0 && !vol1) return qfeError;

  qfeStudy *study = nullptr;
  qfeVolume *boundingVolume = vol0;
  if (!boundingVolume) boundingVolume = vol1;

  if (firstRender) {
    qfeTransform::qfeGetMatrixVoxelToPatient(V2P, boundingVolume);
    qfeGrid *grid;
    boundingVolume->qfeGetVolumeGrid(&grid);
    grid->qfeGetGridExtent(w, h, d);
    qfeSetStaticUniformLocations();   
    firstRender = false;
  }

  // Obtain color texture id
  dataSet0Available = false;
  if (vol0) {
    dataSet0Available = true;
    vol0->qfeGetVolumeTextureId(texVolume0);
  }
  dataSet1Available = false;
  if (vol1) {
    dataSet1Available = true;
    vol1->qfeGetVolumeTextureId(texVolume1);
  }

  qfeCreateTFMaps(tf0, tf1);

  // Compute the stepsize and scaling factor
  qfeGrid     *grid;
  unsigned int dims[3];
  qfePoint     extent;
  boundingVolume->qfeGetVolumeSize(dims[0], dims[1], dims[2]);
  boundingVolume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(extent.x, extent.y, extent.z);

  paramStepSize   = min(extent.x, min(extent.y, extent.z));  
  paramScaling[0] = dims[0]*extent.x; 
  paramScaling[1] = dims[1]*extent.y; 
  paramScaling[2] = dims[2]*extent.z; 

  glDisable(GL_LIGHTING);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_TEXTURE_1D);

  qfeGLSupport::qfeCheckFrameBuffer();

  // Start rendering
  // 1. Generate texture with ray end position
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, texRayEnd, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT,   GL_TEXTURE_2D, texRayEndDepth, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, texRayEndDepth, 0);
  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // First render the bounding box stop positions
  glEnable(GL_CULL_FACE);
  glCullFace(GL_FRONT);

  qfeRenderBoundingBox(boundingVolume);

  glDisable(GL_CULL_FACE);

  qfeGLSupport::qfeCheckFrameBuffer();

  // 2. Execute the raycasting (cull the boundingbox to overcome double rendering)
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, colortex, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT,   GL_TEXTURE_2D, depthtex, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depthtex, 0);

  qfeBindTextures();

  qfeSetDynamicUniformLocations();

  shaderIVR->qfeEnable();

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  qfeRenderBoundingBox(boundingVolume);

  float values[1024];
  glActiveTexture(GL_TEXTURE17);
  glGetTexImage(GL_TEXTURE_1D, 0, GL_RED, GL_FLOAT, values);

  glDisable(GL_CULL_FACE);
  glDisable(GL_BLEND);

  shaderIVR->qfeDisable();
  qfeUnbindTextures();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeSetViewportSize(unsigned int x, unsigned int y, int supersampling) {
  if (viewportSize[0] != x || viewportSize[1] != y) {
    viewportSize[0] = x;
    viewportSize[1] = y;

    qfeUnbindTextures();
    qfeDeleteTextures();
    qfeCreateTextures();
  }

  this->superSampling = supersampling;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeSetIntersectionBuffers(GLuint color, GLuint depth) {
  texIntersectColor = color;
  texIntersectDepth = depth;
  return qfeSuccess;
}

qfeReturnStatus qfeProbabilityDVR::qfeSetRange0(float rangeMin, float rangeMax)
{
  paramDataRange0[0] = rangeMin;
  paramDataRange0[1] = rangeMax;
  return qfeSuccess;
}

qfeReturnStatus qfeProbabilityDVR::qfeSetRange1(float rangeMin, float rangeMax)
{
  paramDataRange1[0] = rangeMin;
  paramDataRange1[1] = rangeMax;
  return qfeSuccess;
}

qfeReturnStatus qfeProbabilityDVR::qfeSetDepthToneMapping(bool on) {
  depthToneMapping = on;
  return qfeSuccess;
}

qfeReturnStatus qfeProbabilityDVR::qfeSetDepthToneMappingFactor(float factor) {
  depthToneMappingFactor = factor;
  return qfeSuccess;
}

qfeReturnStatus qfeProbabilityDVR::qfeSetVoITInv(const qfeMatrix4f &TInv)
{
  voiTInv = TInv;
  return qfeSuccess;
}

qfeReturnStatus qfeProbabilityDVR::qfeSetVoICenter(const qfePoint &center)
{
  voiCenter = center;
  return qfeSuccess;
}

qfeReturnStatus qfeProbabilityDVR::qfeSetClipVoI(bool on)
{
  useVoIClipping = on;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeCreateTextures() {
  if (superSampling < 0) superSampling = 1;

  glGenTextures(1, &texRayEnd);
  glBindTexture(GL_TEXTURE_2D, texRayEnd);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA16F,
    viewportSize[0]*superSampling, viewportSize[1]*superSampling, 0, GL_BGRA, GL_UNSIGNED_BYTE, nullptr);

  glGenTextures(1, &texRayEndDepth);
  glBindTexture(GL_TEXTURE_2D, texRayEndDepth);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8_EXT, 
    viewportSize[0]*superSampling, viewportSize[1]*superSampling, 0, GL_DEPTH_STENCIL_EXT, GL_UNSIGNED_INT_24_8_EXT, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeDeleteTextures() {  
  if (glIsTexture(texRayEnd))       glDeleteTextures(1, (GLuint *)&texRayEnd);
  if (glIsTexture(texRayEndDepth))  glDeleteTextures(1, (GLuint *)&texRayEndDepth);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeBindTextures() {
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_3D, texVolume0);

  glActiveTexture(GL_TEXTURE17);
  glBindTexture(GL_TEXTURE_1D, tfmTex0);

  glActiveTexture(GL_TEXTURE18);
  glBindTexture(GL_TEXTURE_1D, tfmTex1);

  glActiveTexture(GL_TEXTURE19);
  glBindTexture(GL_TEXTURE_3D, texVolume1);

  glActiveTexture(GL_TEXTURE6);
  glBindTexture(GL_TEXTURE_2D, texRayEnd);

  glActiveTexture(GL_TEXTURE7);
  glBindTexture(GL_TEXTURE_2D, texRayEndDepth);

  if (glIsTexture(texIntersectColor) && glIsTexture(texIntersectDepth)) {
    glActiveTexture(GL_TEXTURE8);
    glBindTexture(GL_TEXTURE_2D, texIntersectColor);

    glActiveTexture(GL_TEXTURE9);
    glBindTexture(GL_TEXTURE_2D, texIntersectDepth);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeUnbindTextures() {
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

float qfeProbabilityDVR::qfeGetProbability(QTransFunc tf, float t)
{
  float before = tf.first().first;
  float pBefore = tf.first().second;
  float after;
  float pAfter;
  for (unsigned i = 1; i != tf.size(); i++) {
    if (tf[i].first >= t) {
      after = tf[i].first;
      pAfter = tf[i].second;
      break;
    }
    before = tf[i].first;
    pBefore = tf[i].second;
  }

  float u = (t - before) / (after - before);
  return pBefore + u * (pAfter - pBefore);
}

qfeReturnStatus qfeProbabilityDVR::qfeCreateTFMaps(QTransFunc tf0, QTransFunc tf1)
{
  for (unsigned i = 0; i < 1024; i++) {
    tfm0[i] = qfeGetProbability(tf0, (float)i/1023.f);
    tfm1[i] = qfeGetProbability(tf1, (float)i/1023.f);
  }

  glActiveTexture(GL_TEXTURE17);
  if (glIsTexture(tfmTex0)) glDeleteTextures(1, &tfmTex0);
  glGenTextures(1, &tfmTex0);
  glBindTexture(GL_TEXTURE_1D, tfmTex0);
  glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_R16F, 1024, 0, GL_RED, GL_FLOAT, tfm0);

  glActiveTexture(GL_TEXTURE18);
  if (glIsTexture(tfmTex1)) glDeleteTextures(1, &tfmTex1);
  glGenTextures(1, &tfmTex1);
  glBindTexture(GL_TEXTURE_1D, tfmTex1);
  glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_R16F, 1024, 0, GL_RED, GL_FLOAT, tfm1);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeRenderBoundingBox(qfeVolume *volume) {
  qfeMatrix4f   P2W, V2P, T2V, M;
  GLfloat       matrix[16];

  qfeTransform::qfeGetMatrixPatientToWorld(P2W);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);

  // OpenGL works with column-major matrices
  // OpenGL works with right-multiplication
  M = T2V*V2P*P2W;
  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();
  glMultMatrixf(&matrix[0]);  // Texture to World   Coordinates

  qfeGetModelViewMatrix();

  glBegin(GL_QUADS);

  // Front
  glNormal3f(0.0f, 0.0f, -1.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);

  // Back
  glNormal3f(0.0f, 0.0f, 1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f, 1.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f, 1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f,  1.0f, 1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f, 1.0f);

  // Left
  glNormal3f(-1.0f, 0.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f,  1.0f,  1.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f,  1.0f,  0.0f);

  // Right
  glNormal3f(1.0f, 0.0f, 0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f(1.0f,  0.0f,  1.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f(1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f(1.0f,  1.0f,  0.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f(1.0f,  1.0f,  1.0f);

  // Top
  glNormal3f(0.0f, -1.0f, 0.0f);
  glColor3f(0.0f,0.0f,0.0f); glTexCoord3f(0.0f,0.0f,0.0f); glVertex3f( 0.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,0.0f); glTexCoord3f(1.0f,0.0f,0.0f); glVertex3f( 1.0f,  0.0f,  0.0f);
  glColor3f(1.0f,0.0f,1.0f); glTexCoord3f(1.0f,0.0f,1.0f); glVertex3f( 1.0f,  0.0f,  1.0f);
  glColor3f(0.0f,0.0f,1.0f); glTexCoord3f(0.0f,0.0f,1.0f); glVertex3f( 0.0f,  0.0f,  1.0f);

  // Bottom
  glNormal3f(0.0f, 1.0f, 0.0f);
  glColor3f(1.0f,1.0f,0.0f); glTexCoord3f(1.0f,1.0f,0.0f); glVertex3f( 1.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,0.0f); glTexCoord3f(0.0f,1.0f,0.0f); glVertex3f( 0.0f, 1.0f,  0.0f);
  glColor3f(0.0f,1.0f,1.0f); glTexCoord3f(0.0f,1.0f,1.0f); glVertex3f( 0.0f, 1.0f,  1.0f);
  glColor3f(1.0f,1.0f,1.0f); glTexCoord3f(1.0f,1.0f,1.0f); glVertex3f( 1.0f, 1.0f,  1.0f);

  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeRenderBoundingBox(qfeVolume *boundingVolume, qfeVolume* textureVolume) {
  qfePoint      c[4], v[4], min, max;
  qfeMatrix4f   P2W, V2P, T2V, M, T2P, P2T, P2V, V2T, TextureVolumeCoords, BoundingVolumeCoords;
  GLfloat       matrix[16];

  if (!boundingVolume || !textureVolume) return qfeError;

  // apply the modelview matrix
  qfeTransform::qfeGetMatrixPatientToWorld(P2W);

  // bounding volume transformations
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, boundingVolume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, boundingVolume);
  T2P = T2V*V2P;

  // texture volume transformations
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, textureVolume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, textureVolume);
  P2T = P2V*V2T;  

  TextureVolumeCoords = T2P*P2T;
  qfeMatrix4f::qfeGetMatrixInverse(TextureVolumeCoords, BoundingVolumeCoords);

  // clip the bounding box with ortho planes
  max.x = 1.0;
  max.y = 1.0;
  max.z = 1.0;

  min.x = 0.0;
  min.y = 0.0;
  min.z = 0.0;

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  M = T2V*V2P*P2W;
  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();
  glMultMatrixf(&matrix[0]);  // Texture to World Coordinates

  qfeGetModelViewMatrix();

  glBegin(GL_QUADS);

  // Front
  glNormal3f(0.0f, 0.0f, -1.0f);  
  v[0].qfeSetPointElements(min.x,min.y,min.z);
  v[1].qfeSetPointElements(min.x,max.y,min.z);
  v[2].qfeSetPointElements(max.x,max.y,min.z);
  v[3].qfeSetPointElements(max.x,min.y,min.z);
  for (int i = 0; i < 4; i++) {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);    
  }

  // Back
  glNormal3f(0.0f, 0.0f, 1.0f);
  v[0].qfeSetPointElements(min.x,min.y,max.z);
  v[1].qfeSetPointElements(max.x,min.y,max.z);
  v[2].qfeSetPointElements(max.x,max.y,max.z);
  v[3].qfeSetPointElements(min.x,max.y,max.z);
  for (int i = 0; i < 4; i++) {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);    
  }  

  // Left
  glNormal3f(-1.0f, 0.0f, 0.0f);
  v[0].qfeSetPointElements(min.x,min.y,min.z);
  v[1].qfeSetPointElements(min.x,min.y,max.z);
  v[2].qfeSetPointElements(min.x,max.y,max.z);
  v[3].qfeSetPointElements(min.x,max.y,min.z);
  for (int i = 0; i < 4; i++)   {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }

  // Right
  glNormal3f(1.0f, 0.0f, 0.0f);
  v[0].qfeSetPointElements(max.x,min.y,max.z);
  v[1].qfeSetPointElements(max.x,min.y,min.z);
  v[2].qfeSetPointElements(max.x,max.y,min.z);
  v[3].qfeSetPointElements(max.x,max.y,max.z);
  for (int i = 0; i < 4; i++)   {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }

  // Top
  glNormal3f(0.0f, -1.0f, 0.0f);
  v[0].qfeSetPointElements(min.x,min.y,min.z);
  v[1].qfeSetPointElements(max.x,min.y,min.z);
  v[2].qfeSetPointElements(max.x,min.y,max.z);
  v[3].qfeSetPointElements(min.x,min.y,max.z);
  for (int i=0; i<4; i++) {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }

  // Bottom
  glNormal3f(0.0f, 1.0f, 0.0f);
  v[0].qfeSetPointElements(max.x,max.y,min.z);
  v[1].qfeSetPointElements(min.x,max.y,min.z);
  v[2].qfeSetPointElements(min.x,max.y,max.z);
  v[3].qfeSetPointElements(max.x,max.y,max.z);
  for (int i = 0; i < 4; i++) {
    c[i] = v[i] * TextureVolumeCoords;
    qfeClampTextureRange(c[i], c[i]);
    v[i] = c[i] * BoundingVolumeCoords;
    glColor3f(c[i].x,c[i].y,c[i].z); glTexCoord3f(c[i].x,c[i].y,c[i].z); glVertex3f(v[i].x,v[i].y,v[i].z);  
  }

  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeClampTextureRange(qfePoint pos, qfePoint &newpos) {
  if (pos.x < 0.5) newpos.x = std::max(pos.x, 0.0f);
  else             newpos.x = std::min(pos.x, 1.0f);
  if (pos.y < 0.5) newpos.y = std::max(pos.y, 0.0f);
  else             newpos.y = std::min(pos.y, 1.0f);
  if (pos.z < 0.5) newpos.z = std::max(pos.z, 0.0f);
  else             newpos.z = std::min(pos.z, 1.0f);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeSetStaticUniformLocations() {
  shaderIVR->qfeSetUniform1i("rayEnd"          , 6);
  shaderIVR->qfeSetUniform1i("rayEndDepth"     , 7);
  shaderIVR->qfeSetUniform1i("intersectColor"  , 8);
  shaderIVR->qfeSetUniform1i("intersectDepth"  , 9);
  shaderIVR->qfeSetUniform3f("dimensions"      , w, h, d);
  shaderIVR->qfeSetUniformMatrix4f("voxelPatientMatrix", 1, V2P);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeSetDynamicUniformLocations() {
  int       intersectAvailable = 0;

  if (glIsTexture(texIntersectColor) && glIsTexture(texIntersectDepth))
    intersectAvailable = 1;

  shaderIVR->qfeSetUniform1i("useDataSet0", dataSet0Available);
  shaderIVR->qfeSetUniform1i("dataSet0", 1);
  shaderIVR->qfeSetUniform2f("range0", paramDataRange0[0], paramDataRange0[1]);
  shaderIVR->qfeSetUniform1i("tf0", 17);
  shaderIVR->qfeSetUniform1i("useDataSet1", dataSet1Available);
  shaderIVR->qfeSetUniform1i("dataSet1", 19);
  shaderIVR->qfeSetUniform2f("range1", paramDataRange1[0], paramDataRange1[1]);
  shaderIVR->qfeSetUniform1i("tf1", 18);
  shaderIVR->qfeSetUniform3f("scaling"               , paramScaling[0], paramScaling[1], paramScaling[2]);
  shaderIVR->qfeSetUniform1f("stepSize"              , paramStepSize);
  shaderIVR->qfeSetUniform1i("intersectAvailable"    , intersectAvailable);
  shaderIVR->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, matrixModelViewProjection);
  shaderIVR->qfeSetUniform1i("depthToneMapping"      , depthToneMapping);
  shaderIVR->qfeSetUniform1f("depthToneMappingFactor", depthToneMappingFactor);

  float fVoiTInv[16];
  qfeMatrix4f::qfeGetMatrixElements(voiTInv, fVoiTInv);
  shaderIVR->qfeSetUniformMatrix4f("voiTInv"         , 1, fVoiTInv);
  shaderIVR->qfeSetUniform4f("voiCenter", voiCenter.x, voiCenter.y, voiCenter.z, voiCenter.w);
  shaderIVR->qfeSetUniform1i("useVoI", useVoIClipping);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeProbabilityDVR::qfeGetModelViewMatrix() {
  qfeMatrix4f mv, p, mvp, mvinv, mvpinv;

  GLfloat modelview[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f::qfeSetMatrixElements(mv, modelview);

  GLfloat projection[16];
  glGetFloatv(GL_PROJECTION_MATRIX, projection);
  qfeMatrix4f::qfeSetMatrixElements(p, projection);

  mvp    = mv*p;
  qfeMatrix4f::qfeGetMatrixInverse(mvp, mvpinv);
  qfeMatrix4f::qfeGetMatrixInverse(mv, mvinv);

  qfeMatrix4f::qfeGetMatrixElements(mvinv,  matrixModelViewInverse);
  qfeMatrix4f::qfeGetMatrixElements(mvp,    matrixModelViewProjection);
  qfeMatrix4f::qfeGetMatrixElements(mvpinv, matrixModelViewProjectionInverse);

  return qfeSuccess;
}