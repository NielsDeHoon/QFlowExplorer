#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithmFlow.h"
#include "qfeDisplay.h"
#include "qfeFlowOctree.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeProbe.h"
#include "qfeTransform.h"
#include "qfeVolume.h"
#include "qfeGLSupport.h"
#include "qfeGLShaderProgram.h"

#include <iostream>
#include <fstream>

#define  TEX_PROBE_SIZE 32

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

/**
* \file   qfeFlowProbe3D.h
* \author Roy van Pelt
* \class  qfeFlowProbe3D
* \brief  Implements rendering of a 3D flow probe
* \note   Confidential
*
* Rendering of a 3D flow probe
*
*/
class qfeFlowProbe3D : public qfeAlgorithmFlow
{
public:
  qfeFlowProbe3D();
  qfeFlowProbe3D(const qfeFlowProbe3D &fp);
  ~qfeFlowProbe3D();

  enum qfeFlowProbe3DAppearance
  {
    qfeFlowProbeGlass,
    qfeFlowProbeMetal,
    qfeFlowProbePhong,
    qfeFlowProbeTransparent,
    qfeFlowProbeFlat
  };

  enum qfeFlowProbeSide
  {
    qfeProbeFront,
    qfeProbeBack
  };  

  qfeReturnStatus qfeRenderProbe(qfeProbe3D *probe);    
  qfeReturnStatus qfeRenderProbeHandleRotation(qfeProbe3D *probe, qfeColorRGB color);
  qfeReturnStatus qfeRenderProbeClipPlane(qfeProbe3D *probe);
  qfeReturnStatus qfeRenderProbeClipPlane(qfeProbe3D *probe, qfeFrameSlice *slice);
  qfeReturnStatus qfeRenderProbeClipVolume(qfeProbe3D *probe);    
  qfeReturnStatus qfeRenderProbeCenterline(qfeProbe3D *probe, int lineWidth);
  qfeReturnStatus qfeRenderProbeAxes(qfeProbe3D *probe, int lineWidth);

  qfeReturnStatus qfeGetProbeViewAligned(qfeProbe3D &probe, qfeVector eye);
  qfeReturnStatus qfeGetProbeRotation(qfeProbe3D &probe, qfeFloat angle);
  qfeReturnStatus qfeGetProbeFitLocal(qfeProbe3D &probe, qfeFlowOctree *octree, qfeMatrix4f mv);
  qfeReturnStatus qfeGetProbeFitGlobal(qfeProbe3D &probe, qfeVolume *volume, qfeFlowOctree *octree, qfeMatrix4f mv);
  qfeReturnStatus qfeGetProbeFitUndo(qfeProbe3D &probe);

  qfeReturnStatus qfeGetProbeCenterline(qfeProbe3D *probe, int samples, qfePoint **centerline);  
    
  qfeReturnStatus qfeSetProbeAppearance(qfeFlowProbe3DAppearance mode);
  qfeReturnStatus qfeSetProbeContour(bool contour);
  qfeReturnStatus qfeSetProbeLightSource(qfeLightSource *light);
  qfeReturnStatus qfeSetProbeSuperSamplingFactor(qfeFloat factor);
 
protected:  
  qfeReturnStatus qfeRenderCylinder(qfeProbe3D *probe, qfeFlowProbeSide side);
  qfeReturnStatus qfeRenderCutPlanes(qfeProbe3D *probe);
  qfeReturnStatus qfeRenderCutCaps(qfeProbe3D *probe);
  qfeReturnStatus qfeRenderClipCaps(qfeProbe3D *probe);
  qfeReturnStatus qfeRenderProbeContour(qfeProbe3D *probe, qfeFloat lineWidth);
  qfeReturnStatus qfeRenderBoundingBox(qfeVolume *volume);

  qfeReturnStatus qfeCreateCylinderGeometry(qfeProbe3D *probe, qfeFlowProbeSide side, GLfloat** vertices, GLfloat **normals, GLint *size);
  qfeReturnStatus qfeCreateCylinderTopology(qfeProbe3D *probe, GLubyte **topology, GLint *tsize);  
  qfeReturnStatus qfeCreateCutPlanes(qfeProbe3D *probe, GLfloat **plane1, GLfloat**plane2, GLfloat **normals, GLfloat **planeTex, GLint &size);
  qfeReturnStatus qfeCreateCutPlanesContours(qfeProbe3D *probe, GLfloat **plane1, GLfloat**plane2, GLint &size);
  qfeReturnStatus qfeCreateCutCaps(qfeProbe3D *probe, GLfloat **baseCap, GLfloat**topCap, GLfloat **baseNormals, GLfloat **topNormals, GLint &size);
  qfeReturnStatus qfeCreateCutCapsContours(qfeProbe3D *probe, GLfloat **baseCap, GLfloat**topCap, GLint &size);  
  qfeReturnStatus qfeCreateSphere(qfeFloat radius, qfeFloat slices, qfeFloat stacks, GLfloat**sphere, GLint *size);

  qfeReturnStatus qfeGetInsideBox(qfeVolume *volume, qfePoint p, bool &inside);
  qfeReturnStatus qfeGetIntersectionsBoxLine(qfeVolume *volume, qfePoint p1, qfePoint p2, qfePoint **intersections, int &count);

  qfeReturnStatus qfeCreateTextureProbe(qfeFlowProbe3DAppearance appearance);
  qfeReturnStatus qfeDeleteTextureProbe();

  qfeReturnStatus qfeSetDynamicUniformLocationsRender();

private:
  GLuint                    texProbe;    
  qfeGLShaderProgram       *shaderRenderProbe;

  qfeFlowProbe3DAppearance  appearance;
  bool                      contour;
  qfeLightSource           *light;
  qfeFloat                  color[4];
  qfeFloat                  superSampling;  

  qfePoint                  point1, point2;
  qfeProbe3D                probeCache;
  bool                      probeFit;



};


