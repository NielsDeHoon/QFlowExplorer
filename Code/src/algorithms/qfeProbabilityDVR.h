#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithm.h"
#include "qfeColorMap.h"
#include "qfeDisplay.h"
#include "qfeDriver.h"
#include "qfeGLShaderProgram.h"
#include "qfeGeometry.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeTransform.h"
#include "qfeVolume.h"
#include "qfeLitSphereMap.h"
#include "qtTransferFunctionWidget.h"

typedef QTransferFunctionOpacity QTransFunc;

/**
* \file   qfeProbabilityDVR.h
* \author Arjan Broos
* \class  qfeProbabilityDVR
* \brief  Implements direct volume rendering of probabilities dictated by two transfer functions
* \note   Confidential
*/

class qfeProbabilityDVR : qfeAlgorithm
{
public:
  qfeProbabilityDVR();
  qfeProbabilityDVR(const qfeProbabilityDVR &pr);
  ~qfeProbabilityDVR();

  qfeReturnStatus qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *vol0, qfeVolume *vol1, QTransFunc tf0, QTransFunc tf1);

  qfeReturnStatus qfeSetViewportSize(unsigned int x, unsigned int y, int supersampling);
  qfeReturnStatus qfeSetIntersectionBuffers(GLuint color, GLuint depth);  

  qfeReturnStatus qfeSetRange0(float rangeMin, float rangeMax);
  qfeReturnStatus qfeSetRange1(float rangeMin, float rangeMax);

  qfeReturnStatus qfeSetDepthToneMapping(bool on);
  qfeReturnStatus qfeSetDepthToneMappingFactor(float factor);

  qfeReturnStatus qfeSetVoITInv(const qfeMatrix4f &TInv);
  qfeReturnStatus qfeSetVoICenter(const qfePoint &center);
  qfeReturnStatus qfeSetClipVoI(bool on);

  qfeReturnStatus qfeRenderBoundingBox(qfeVolume *volume);
protected:  
  bool                firstRender;
  qfeGLShaderProgram *shaderIVR;

  qfeMatrix4f         V2P;

  unsigned int        viewportSize[2];
  int                 superSampling;

  GLuint              texRayEnd;
  GLuint              texRayEndDepth;
  GLuint              texVolume0;
  GLuint              texVolume1;
  GLuint              texIntersectColor;
  GLuint              texIntersectDepth;  

  float               paramScaling[3];           
  float               paramStepSize;
  float               paramDataRange0[2];
  float               paramDataRange1[2];

  bool                depthToneMapping;
  float               depthToneMappingFactor;

  bool                dataSet0Available;
  bool                dataSet1Available;

  qfeMatrix4f         voiTInv;
  qfePoint            voiCenter;
  bool                useVoIClipping;
  float               w, h, d;

  qfeReturnStatus qfeCreateTextures();
  qfeReturnStatus qfeDeleteTextures();
  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  float           tfm0[1024];
  float           tfm1[1024];
  GLuint          tfmTex0;
  GLuint          tfmTex1;
  float           qfeGetProbability(QTransFunc tf, float t);
  qfeReturnStatus qfeCreateTFMaps(QTransFunc tf0, QTransFunc tf1);

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

  qfeReturnStatus qfeRenderBoundingBox(qfeVolume *boundingVolume, qfeVolume* textureVolume);

  qfeReturnStatus qfeClampTextureRange(qfePoint pos, qfePoint &newpos);  

  qfeReturnStatus qfeGetModelViewMatrix();

  GLfloat    matrixNormals[16];
  GLfloat    matrixModelViewInverse[16];
  GLfloat    matrixModelViewProjection[16];
  GLfloat    matrixModelViewProjectionInverse[16];
};
