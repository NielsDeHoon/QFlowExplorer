#pragma once

// Pre-declare qfeClusterNode class so we can cross-reference from qfeDistanceListNode
class qfeClusterNode;
class qfeDistanceListNode;
#include "qfeClusterNode.h"

/**
* \file   qfeDistanceListNode.h
* \author Sander Jacobs
* \class  qfeDistanceListNode
* \brief  -
* \note   Confidential
*
*/

using namespace std;

class qfeDistanceListNode //: public qfeAlgorithmFlow
{
public:
  qfeDistanceListNode();
  qfeDistanceListNode(qfeDistanceListNode *prevDistNode);
  qfeDistanceListNode(qfeClusterNode* c1, qfeClusterNode* c2);
  ~qfeDistanceListNode();


  qfeReturnStatus       qfeInsertDistance(qfeDistanceListNode* prevDistNode);
  qfeReturnStatus       qfeMoveDistanceTo(qfeDistanceListNode* prevDistNode);
  qfeReturnStatus       qfeRemoveFromList();
  qfeDistanceListNode  *qfeGetPrevDistanceNode();
  qfeDistanceListNode  *qfeGetNextDistanceNode();

  int qfeGetClusterLabel1();
  int qfeGetClusterLabel2();
  qfeReturnStatus qfeSetClusters(qfeClusterNode* c1, qfeClusterNode* c2);
  qfeReturnStatus qfeChangeCluster(qfeClusterNode* cOld, qfeClusterNode* cNew);
  qfeReturnStatus qfeMergeClusters(qfeDistanceListNode* &firstDistance, qfeDistanceListNode* &lastDistance);

  qfeFloat        qfeGetDistance();
  qfeReturnStatus qfeEvaluateDistance();
  qfeReturnStatus qfeUnevaluateDistance();
  qfeReturnStatus qfeInvalidateDistance();
  qfeReturnStatus qfePrintDistance();

  bool qfeIsDistanceUnevaluated();


private:
  qfeDistanceListNode  *prevNodePtr;
  qfeDistanceListNode  *nextNodePtr;

  qfeClusterNode  *c1;
  qfeClusterNode  *c2;
  qfeFloat         d;
};



