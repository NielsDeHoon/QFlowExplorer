#include "qfeRayCastingVector.h"

//----------------------------------------------------------------------------
qfeRayCastingVector::qfeRayCastingVector()
{
  this->shaderVFR = new qfeGLShaderProgram();
  this->shaderVFR->qfeAddShaderFromFile("/shaders/library/transform.glsl",QFE_FRAGMENT_SHADER);
  this->shaderVFR->qfeAddShaderFromFile("/shaders/library/fetch.glsl",    QFE_FRAGMENT_SHADER);
  this->shaderVFR->qfeAddShaderFromFile("/shaders/library/normalize.glsl",QFE_FRAGMENT_SHADER);
  this->shaderVFR->qfeAddShaderFromFile("/shaders/library/composite.glsl",QFE_FRAGMENT_SHADER);  
  this->shaderVFR->qfeAddShaderFromFile("/shaders/vfr/vfr-vert.glsl",     QFE_VERTEX_SHADER);
  this->shaderVFR->qfeAddShaderFromFile("/shaders/vfr/vfr-func.glsl",     QFE_FRAGMENT_SHADER);
  this->shaderVFR->qfeAddShaderFromFile("/shaders/vfr/vfr-frag.glsl",     QFE_FRAGMENT_SHADER);
  this->shaderVFR->qfeLink();

  this->colorMapUS            = new qfeColorMap();
  this->colorMapCoolWarm      = new qfeColorMap();

  this->firstRender           = true;

  this->texRayEnd             = 0;
  this->texRayEndDepth        = 0;
  this->texVolume             = 0;
  this->texContext            = 0;
  this->texColorMap           = 0;
  this->texColorMapUS         = 0;
  this->texColorMapCoolWarm   = 0;
  this->texGradientMap        = 0;

  this->texIntersectColor     = 0;
  this->texIntersectDepth     = 0;
  this->texClippingFrontDepth = 0;
  this->texClippingBackDepth  = 0;

  this->paramStepSize         = 1.0/250.0;
  this->paramShading          = false;
  this->paramQuality          = 4.0;
  this->paramStyle            = 0;
  this->paramThreshold        = 20.0;
  this->paramDir.qfeSetVectorElements(0.0,0.0,1.0); 

  this->paramDataFocus        = qfeDataFocusDoppler;
  this->paramDataContext      = qfeDataContextDepth;

  this->paramClipping[0] = this->paramClipping[2] = this->paramClipping[4] = 0.0;
  this->paramClipping[1] = this->paramClipping[3] = this->paramClipping[5] = 1.0;

  this->light = NULL;

  this->qfeCreateColorMaps();
};

//----------------------------------------------------------------------------
qfeRayCastingVector::qfeRayCastingVector(const qfeRayCastingVector &pr)
{
  this->firstRender          = pr.firstRender;
  this->shaderVFR            = pr.shaderVFR;
  this->colorMapUS           = pr.colorMapUS;
  this->colorMapCoolWarm     = pr.colorMapCoolWarm;
  this->texContext           = pr.texContext;
  this->texColorMapUS        = pr.texColorMapUS;
  this->texColorMapCoolWarm  = pr.texColorMapCoolWarm;
  this->texGradientMap       = pr.texGradientMap;
  this->paramDir             = pr.paramDir;
  this->paramThreshold       = pr.paramThreshold;
  this->paramDataFocus       = pr.paramDataFocus;
  this->paramDataContext     = pr.paramDataContext;
};

//----------------------------------------------------------------------------
qfeRayCastingVector::~qfeRayCastingVector()
{
  delete this->shaderVFR; 
  delete this->colorMapUS;
  delete this->colorMapCoolWarm;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *volume, qfeColorMap *colormap)
{
  if(volume == NULL) return qfeError;

  return qfeRenderRaycasting(colortex, depthtex, volume, volume, colormap);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *flowVolume, qfeVolume *contextVolume, qfeColorMap *colormap)
{
  if(flowVolume == NULL || contextVolume == NULL) return qfeError;

  qfeStudy      *study    = NULL;

  if(firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, flowVolume);

    this->qfeSetStaticUniformLocations();

    firstRender = false;
  }

  // Obtain color texture id
  contextVolume->qfeGetVolumeTextureId(this->texVolume);

  colormap->qfeGetColorMapTextureId(this->texColorMap);
  colormap->qfeGetColorMapTextureId(this->texGradientMap);
  contextVolume->qfeGetVolumeType(this->paramMode);

  // Get the US color map 
  if(!glIsTexture(this->texColorMapUS))
  {
    this->colorMapUS->qfeUploadColorMap();
    this->colorMapUS->qfeGetColorMapTextureId(this->texColorMapUS);
  }

  // Get the cool warm color map 
  if(!glIsTexture(this->texColorMapCoolWarm))
  {
    this->colorMapCoolWarm->qfeUploadColorMap();
    this->colorMapCoolWarm->qfeGetColorMapTextureId(this->texColorMapCoolWarm);
  }

  // Compute the stepsize
  qfeGrid     *grid;
  unsigned int dims[3];
  qfePoint     extent;
  contextVolume->qfeGetVolumeSize(dims[0], dims[1], dims[2]);
  contextVolume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(extent.x, extent.y, extent.z);

  if(extent.x <= extent.y)
  {
    if(extent.x <= extent.z) this->paramStepSize = extent.x / (float)dims[0];
    if(extent.x >= extent.z) this->paramStepSize = extent.z / (float)dims[2];
  }
  else
  {
    if(extent.y <= extent.z) this->paramStepSize = extent.y / (float)dims[1];
    if(extent.y >= extent.z) this->paramStepSize = extent.z / (float)dims[2];
  }

  glDisable(GL_LIGHTING);
  glEnable(GL_DEPTH_TEST);

  // Start rendering
  // 1. Generate texture with ray end position
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, this->texRayEnd, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT,   GL_TEXTURE_2D, this->texRayEndDepth, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, this->texRayEndDepth, 0);
  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // First render the bounding box stop positions
  glEnable(GL_CULL_FACE);
  glCullFace(GL_FRONT);

  this->qfeRenderBoundingBox(flowVolume, contextVolume);

  glDisable(GL_CULL_FACE);

  qfeGLSupport::qfeCheckFrameBuffer();

  // 2. Execute the raycasting (cull the boundingbox to overcome double rendering)
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, colortex, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT,   GL_TEXTURE_2D, depthtex, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depthtex, 0);
  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  
  this->qfeBindTextures();
  this->qfeSetDynamicUniformLocations();
  this->shaderVFR->qfeEnable();

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);

  this->qfeRenderBoundingBox(flowVolume, contextVolume);

  glDisable(GL_CULL_FACE);

  this->shaderVFR->qfeDisable();
  this->qfeUnbindTextures();

  glDisable(GL_BLEND);

  qfeGLSupport::qfeCheckFrameBuffer();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *volume, qfeProbe3D *probe, qfeColorMap *colormap)
{
  return this->qfeRenderRaycasting(colortex, depthtex, volume, volume, probe, colormap);
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *flowVolume, qfeVolume *contextVolume, qfeProbe3D *probe, qfeColorMap *colormap)
{
  if(flowVolume == NULL || contextVolume == NULL || probe == NULL) return qfeError;

  qfeStudy      *study    = NULL;
  qfeMatrix4f    T, R1, R2;
  qfeFloat       theta, arcRadius;
  GLfloat        matrix[16];  

  if(firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, flowVolume);

    this->qfeSetStaticUniformLocations();

    firstRender = false;
  }

  // Obtain color texture id
  flowVolume->qfeGetVolumeTextureId(this->texVolume);

  contextVolume->qfeGetVolumeTextureId(this->texContext);
  contextVolume->qfeGetVolumeType(this->paramMode);
  
  colormap->qfeGetColorMapTextureId(this->texColorMap);  
  colormap->qfeGetGradientMapTextureId(this->texGradientMap);

  // Get the US color map 
  if(!glIsTexture(this->texColorMapUS))
  {
    this->colorMapUS->qfeUploadColorMap();
    this->colorMapUS->qfeGetColorMapTextureId(this->texColorMapUS);
  }

  // Get the cool warm color map 
  if(!glIsTexture(this->texColorMapCoolWarm))
  {
    this->colorMapCoolWarm->qfeUploadColorMap();
    this->colorMapCoolWarm->qfeGetColorMapTextureId(this->texColorMapCoolWarm);
  }

  // Compute the stepsize and scaling factor
  qfeGrid     *grid;
  unsigned int dims[3];
  qfePoint     extent;
  flowVolume->qfeGetVolumeSize(dims[0], dims[1], dims[2]);
  flowVolume->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(extent.x, extent.y, extent.z);

  this->paramStepSize   = min(extent.x, min(extent.y, extent.z));  
  this->paramScaling[0] = dims[0]*extent.x; 
  this->paramScaling[1] = dims[1]*extent.y; 
  this->paramScaling[2] = dims[2]*extent.z; 

  // Store the probe long axis
  this->paramDir = probe->axes[2];

  // Render the background halo
  qfeColorRGBA colorHalo, colorTick;
  colorHalo.r = 0.952;
  colorHalo.g = 0.674;
  colorHalo.b = 0.133;
  colorHalo.a = 0.5;

  colorTick.r = 0.0;
  colorTick.g = 0.8;
  colorTick.b = 0.0;
  colorTick.a = 1.0;

  glEnable(GL_BLEND);

  // Rotation: rotate the cylinder to an upright position
  //           with the normal towards the viewer  
  qfeMatrix4f::qfeSetMatrixIdentity(T);
  arcRadius = sqrt(pow(0.5f*probe->length, 2.0f) + pow(probe->topRadius, 2.0f));
  T(3,2)  = 0.25f*probe->length - 0.5f*arcRadius;

  qfeMatrix4f::qfeSetMatrixIdentity(R1); // different indexation = different rotation!
  theta   = (float(PI) / 2.0f);
  R1(0,0) =  cos(theta); 
  R1(0,1) =  sin(theta)*-1.0f;  
  R1(1,0) =  sin(theta); 
  R1(1,1) =  cos(theta);    

  qfeMatrix4f::qfeSetMatrixIdentity(R2);
  theta   = (float(PI) / 2.0f);
  R2(1,1) =  cos(theta);
  R2(1,2) =  sin(theta)*-1.0f;  
  R2(2,1) =  sin(theta);
  R2(2,2) =  cos(theta);

  qfeMatrix4f::qfeGetMatrixElements(T*R1*R2, &matrix[0]);

  // Start the rendering
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();  

  glMultMatrixf(matrix);

  // Render the volume
  // 1. Generate texture with ray end position   
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, this->texRayEnd, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT,   GL_TEXTURE_2D, this->texRayEndDepth, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, this->texRayEndDepth, 0);
  glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);    

  // First render the bounding box stop positions  
  glEnable(GL_CULL_FACE);
  glCullFace(GL_FRONT);

  this->qfeRenderCylinder(flowVolume, probe->origin, probe->axes[0], probe->axes[2], probe->length, probe->baseRadius, probe->topRadius);

  glDisable(GL_CULL_FACE);

  qfeGLSupport::qfeCheckFrameBuffer();

  // 2. Execute the raycasting (cull the boundingbox to overcome double rendering)  
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0,  GL_TEXTURE_2D, colortex, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT,   GL_TEXTURE_2D, depthtex, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, depthtex, 0);
  glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);

  this->qfeBindTextures();
  this->qfeSetDynamicUniformLocations();
  this->shaderVFR->qfeEnable();

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  
  this->qfeRenderCylinder(flowVolume, probe->origin, probe->axes[0], probe->axes[2], probe->length, probe->baseRadius, probe->topRadius);

  glDisable(GL_CULL_FACE);

  this->shaderVFR->qfeDisable();
  this->qfeUnbindTextures();

  // Render additionals
  this->qfeRenderCylinderHalo(flowVolume, probe, colorHalo);
  this->qfeRenderCylinderTick(flowVolume, probe, colorTick);

  glDisable(GL_BLEND);  

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// origin: in patient coordinates
// axisX:  short axis (bi-normal)
// axisY:  long axis
// length: in mm
// topRadius
qfeReturnStatus qfeRayCastingVector::qfeRenderCylinder(qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, qfeFloat length, qfeFloat baseRadius, qfeFloat topRadius)
{  
  qfeColorRGBA c;
  
  c.r = c.g = c.g = c.a = -1.0;

  return this->qfeRenderCylinder(volume, origin, axisX, axisZ, length, baseRadius, topRadius, c);
}

//----------------------------------------------------------------------------
// origin: in patient coordinates
// axisX:  short axis (bi-normal)
// axisY:  long axis
// length: in mm
// topRadius
qfeReturnStatus qfeRayCastingVector::qfeRenderCylinder(qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, qfeFloat length, qfeFloat baseRadius, qfeFloat topRadius, qfeColorRGBA color)
{  
  
  GLfloat   *vertices;    
  GLubyte   *topology;
  GLfloat   *colors;
  GLint      size, tsize; 
  GLint      sizeBuffer; 
  GLint      quality;

  GLfloat   *vertSphere;
  GLint      sizeSphere;  
  GLfloat   *colorsSphere;

  GLfloat   *vertCap;
  GLubyte   *topoCap, *topoCaptmp;
  GLint      sizeCap, tsizeCap;
  GLfloat   *colorsCap;

  qfeFloat    theta, arcRadius;
  qfeMatrix4f C;
  GLfloat     matrix[16];

  // Number of slices of geometry
  quality = 40;

  // Get the cylinder geometry and topology
  this->qfeCreateCylinderGeometry(baseRadius, topRadius, length, quality, &vertices, &size); 
  this->qfeCreateCylinderTopology(baseRadius, topRadius, quality, &topology, &tsize);    

  // Get the caps geometry and topology
  this->qfeCreateCapGeometry(baseRadius, quality, &vertCap, &sizeCap);  
  this->qfeCreateCapTopology(baseRadius, quality, &topoCaptmp, &tsizeCap);

  // Get the sphere geometry and topology
  this->qfeCreateSphereGeometry(topRadius, length, quality, &vertSphere, &sizeSphere);

  // Translate the sphere to the top
  for(int i=0; i<sizeSphere; i++)
    vertSphere[3*i + 2] = vertSphere[3*i + 2] + (0.5f*length);

  // Flip the base slice
  topoCap = new GLubyte[tsizeCap];
  for(int i=0; i<tsizeCap; i++)
    topoCap[i] = topoCaptmp[tsizeCap-i-1];

  // Obtain the color coding for the volume rendering
  this->qfeCreateCylinderColors(vertices, size, volume, origin, axisX, axisZ, length, &colors);
  this->qfeCreateCapColors(vertCap, sizeCap, volume, origin, axisX, axisZ, length, &colorsCap);  
  this->qfeCreateSphereColors(vertSphere, sizeSphere, volume, origin, axisX, axisZ, length, &colorsSphere);
  
  // Correction: For OpenGL, rotate towards probe coordinates, 
  //             and set the pivot point to the center
  arcRadius = sqrt(pow(0.5f*length, 2.0f) + pow(topRadius, 2.0f));
  theta = -1.0f*(float(PI) / 2.0f);
  qfeMatrix4f::qfeSetMatrixIdentity(C);
  C(0,0) =  cos(theta); 
  C(0,1) =  sin(theta)*-1.0f;  
  C(1,0) =  sin(theta); 
  C(1,1) =  cos(theta);  
  C(3,2) = -0.5f*length;
  //C(3,2) =  0.25f*length + 0.5f*arcRadius;

  qfeMatrix4f::qfeGetMatrixElements(C, &matrix[0]);

  // Create the vertex buffer
  sizeBuffer = size*3*sizeof(GLfloat);

  // Start the rendering
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();  

  glMultMatrixf(matrix);

  // Draw the cylinder
  if(color.a == -1)
    this->qfeDrawCylinder(vertices, vertCap,   vertSphere,
                          topology, topoCap, 
                          colors,   colorsCap, colorsSphere,
                          tsize,    tsizeCap,  sizeSphere);
  else
    this->qfeDrawCylinder(vertices, vertCap,   vertSphere,
                          topology, topoCap, 
                          tsize,    tsizeCap,  sizeSphere, 
                          color.r,  color.g,   color.b,  color.a);

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
 
  delete [] vertices;  
  delete [] vertSphere;
  delete [] vertCap;

  delete [] topology;
  delete [] topoCap;
  delete [] topoCaptmp;

  delete [] colors;
  delete [] colorsCap;
  delete [] colorsSphere;
  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeRenderBox(qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, qfeFloat length, qfeFloat baseRadius, qfeFloat topRadius)
{
  GLfloat   *vertices;    
  GLubyte   *topology;
  GLfloat   *colors;  
  GLint      size, tsize; 

  qfeFloat    theta;
  qfeMatrix4f R, C, T, R2, P2W, T2V, V2P, T2P, M;
  GLfloat     matrix[16];

  // Get the cylinder geometry and topology
  this->qfeCreateBoxGeometry(max(baseRadius, topRadius), length, &vertices, &size); 
  this->qfeCreateBoxTopology(&topology, &tsize); 
  this->qfeCreateBoxColors(vertices, size, volume, origin, axisX, axisZ, length, &colors);
  
  // Correction: For OpenGL, flip along the negative z-axes, 
  //             and set the pivot point to the center
  theta = -1.0f*(float(PI) / 2.0f);

  qfeMatrix4f::qfeSetMatrixIdentity(C);
  C(0,0) =  cos(theta); 
  C(0,1) =  sin(theta)*-1.0f;  
  C(1,0) =  sin(theta); 
  C(1,1) =  cos(theta);
  C(3,2) =  length*-0.5f;

  for(int i=0; i<size; i++)
  {
    qfePoint x;

    x.qfeSetPointElements(vertices[3*i+0], vertices[3*i+1], vertices[3*i+2]);

    x = x*C;

    vertices[3*i+0] = x.x;
    vertices[3*i+1] = x.y;
    vertices[3*i+2] = x.z;
  }

  //qfeMatrix4f::qfeGetMatrixElements(C, &matrix[0]);

  // Load the required transformation matrices  
  qfeTransform::qfeGetMatrixPatientToWorld(P2W);
  
  // bounding volume transformations
  qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
  T2P = T2V*V2P;

  M = P2W;
  qfeMatrix4f::qfeGetMatrixElements(M, &matrix[0]);

  // Start the rendering
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();  

  //glMultMatrixf(matrix);
  glLoadIdentity();
  glMultMatrixf(&matrix[0]);  // Texture to World Coordinates
    
  // Draw the box
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  
  glColor3f(1.0,0.0,0.0);
  glColorPointer(3, GL_FLOAT, 0, colors);  
  glTexCoordPointer(3, GL_FLOAT, 0, colors);
  glVertexPointer(3, GL_FLOAT, 0, vertices);    
  glDrawElements(GL_TRIANGLES, tsize, GL_UNSIGNED_BYTE, topology);      
  
  glDisableClientState(GL_COLOR_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);   
    
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
 
  delete [] vertices;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeRenderCylinderHalo(qfeVolume *volume, qfeProbe3D *probe, qfeColorRGBA color)
{
   // Render background silhouette
  qfeFloat     scale;
  qfeColorRGBA colorStencil;

  scale            = 0.05;
  colorStencil.r   = 1.0; colorStencil.g = colorStencil.b = 0.0;
 
  glEnable(GL_DEPTH_TEST);  
  glDisable(GL_LIGHTING);

  // Render the cylinder to the stencil buffer
  // Disable updates in the color and depth attachments;  
  glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
  glDepthMask(GL_FALSE);

  glClear(GL_STENCIL_BUFFER_BIT);
  glEnable(GL_STENCIL_TEST);

  // Always pass a one to the stencil buffer where we draw
  glStencilFunc(GL_ALWAYS, 1, 0xFFFFFFFF);
  glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE);

    this->qfeRenderCylinder(volume, probe->origin, probe->axes[0], probe->axes[2], probe->length, probe->baseRadius, probe->topRadius, colorStencil);

  // Turn the color and depth buffers back on
  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  glDepthMask(GL_TRUE);

  // Until stencil test is disabled, only write to areas where the stencil buffer equals one.
  glStencilFunc(GL_EQUAL, 0, 0xFFFFFFFF);
  glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
  
  glScalef(1.0+scale,1.0+scale,1.0+scale);

    this->qfeRenderCylinder(volume, probe->origin, probe->axes[0], probe->axes[2], probe->length, probe->baseRadius, probe->topRadius, color);

  glScalef(1.0-scale,1.0-scale,1.0-scale);  

  glDisable(GL_STENCIL_TEST);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeRenderCylinderTick(qfeVolume *volume, qfeProbe3D *probe, qfeColorRGBA color)
{
  qfePoint  o, p;
  qfeVector y;
  qfeFloat  r;

  o.qfeSetPointElements(0.0,0.0,0.0);
  y.qfeSetVectorElements(0.0,1.0,0.0);
  r = (probe->topRadius + probe->baseRadius) / 2.0f;
  p = o + (r+ 6.0f)*y;
  
  glEnable(GL_LIGHTING);
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);

  this->qfeDrawSphere(p, 2.0, color);

  glDisable(GL_COLOR_MATERIAL);
  glDisable(GL_LIGHTING);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeSetSpeedThreshold(int threshold)
{
  this->paramThreshold = (float)threshold;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeSetDataRepresentationFocus(qfeRaycastingVectorFocus type)
{
  this->paramDataFocus = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeSetDataRepresentationContext(qfeRaycastingVectorContext type)
{
  this->paramDataContext = type;

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeBindTextures()
{
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_3D, this->texVolume);

  if(glIsTexture(this->texContext))
  {
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_3D, this->texContext);
  }

  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_1D, this->texColorMap);

  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_1D, this->texColorMapUS);

  glActiveTexture(GL_TEXTURE6);
  glBindTexture(GL_TEXTURE_1D, this->texColorMapCoolWarm);

  glActiveTexture(GL_TEXTURE7);
  glBindTexture(GL_TEXTURE_1D, this->texGradientMap);

  glActiveTexture(GL_TEXTURE8);
  glBindTexture(GL_TEXTURE_2D, this->texRayEnd);

  glActiveTexture(GL_TEXTURE9);
  glBindTexture(GL_TEXTURE_2D, this->texRayEndDepth);

  if(glIsTexture(this->texIntersectColor) && glIsTexture(this->texIntersectDepth))
  {
    glActiveTexture(GL_TEXTURE10);
    glBindTexture(GL_TEXTURE_2D, this->texIntersectColor);

    glActiveTexture(GL_TEXTURE11);
    glBindTexture(GL_TEXTURE_2D, this->texIntersectDepth);
  }

  if(glIsTexture(this->texClippingFrontDepth))
  {
    glActiveTexture(GL_TEXTURE12);
    glBindTexture(GL_TEXTURE_2D, this->texClippingFrontDepth);
  }

  if(glIsTexture(this->texClippingBackDepth))
  {
    glActiveTexture(GL_TEXTURE13);
    glBindTexture(GL_TEXTURE_2D, this->texClippingBackDepth);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeSetStaticUniformLocations()
{
  this->shaderVFR->qfeSetUniform1i("vfrDataSetFlow"    , 1);
  this->shaderVFR->qfeSetUniform1i("vfrDataSetContext" , 2);
  this->shaderVFR->qfeSetUniform1i("transferFunction"  , 4);  
  this->shaderVFR->qfeSetUniform1i("transferFunctionUS", 5);  
  this->shaderVFR->qfeSetUniform1i("transferFunctionCW", 6); 
  this->shaderVFR->qfeSetUniform1i("transferGradient"  , 7); 
  this->shaderVFR->qfeSetUniform1i("rayEnd"            , 8);
  this->shaderVFR->qfeSetUniform1i("rayEndDepth"       , 9);
  this->shaderVFR->qfeSetUniform1i("intersectColor"    ,10);
  this->shaderVFR->qfeSetUniform1i("intersectDepth"    ,11);
  this->shaderVFR->qfeSetUniform1i("clippingFrontDepth",12);
  this->shaderVFR->qfeSetUniform1i("clippingBackDepth" ,13);
  this->shaderVFR->qfeSetUniformMatrix4f("voxelPatientMatrix", 1, this->V2P);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeSetDynamicUniformLocations()
{
  int       intersectAvailable = 0;
  int       clippingAvailable = 0;
  qfePoint  lightSpec;
  qfeVector lightDir;

  if(glIsTexture(this->texIntersectColor) && glIsTexture(this->texIntersectDepth))
    intersectAvailable = 1;

  if(glIsTexture(this->texClippingFrontDepth) && glIsTexture(this->texClippingBackDepth))
    clippingAvailable = 1;
    
  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 100.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w); 
    this->light->qfeGetLightSourceDirection(lightDir);
  }

  // Provide parameters to the shader
  this->shaderVFR->qfeSetUniform1i("vfrDataComponent"      , this->paramComponent);
  //this->shaderVFR->qfeSetUniform1i("vfrDataRepresentation" , this->paramRepresentation);
  this->shaderVFR->qfeSetUniform1i("vfrDataFocus"          , (int)this->paramDataFocus);
  this->shaderVFR->qfeSetUniform1i("vfrDataContext"        , (int)this->paramDataContext);
  this->shaderVFR->qfeSetUniform2f("vfrDataRange"          , this->paramDataRange[0], this->paramDataRange[1]);
  this->shaderVFR->qfeSetUniform3f("vfrDirection"          , this->paramDir.x,    this->paramDir.y,    this->paramDir.z);
  this->shaderVFR->qfeSetUniform1f("threshold"             , this->paramThreshold);
  this->shaderVFR->qfeSetUniform1f("stepSize"              , this->paramStepSize);
  this->shaderVFR->qfeSetUniform3f("scaling"               , this->paramScaling[0], this->paramScaling[1], this->paramScaling[2]);
  this->shaderVFR->qfeSetUniform1f("quality"               , this->paramQuality);  
  this->shaderVFR->qfeSetUniform1i("mode"                  , this->paramMode);  
  //this->shaderVFR->qfeSetUniform1i("shading"               , (int)this->paramShading);
  //this->shaderVFR->qfeSetUniform1i("intersectAvailable"    , intersectAvailable);
  //this->shaderVFR->qfeSetUniform1i("clippingAvailable"     , clippingAvailable);
  this->shaderVFR->qfeSetUniform4f("light"                 , lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderVFR->qfeSetUniform4f("lightDirection"        , lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderVFR->qfeSetUniform1i("style"                 , this->paramStyle);  
  this->shaderVFR->qfeSetUniformMatrix4f("modelViewProjection", 1, this->matrixModelViewProjection);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeGetModelViewMatrix(qfeVolume *volume)
{
  qfeMatrix4f mv, p, t2v, v2p;

  GLfloat modelview[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f::qfeSetMatrixElements(mv, modelview);

  GLfloat projection[16];
  glGetFloatv(GL_PROJECTION_MATRIX, projection);
  qfeMatrix4f::qfeSetMatrixElements(p, projection);

  qfeTransform::qfeGetMatrixTextureToVoxel(t2v, volume);
  qfeTransform::qfeGetMatrixVoxelToPatient(v2p, volume);

  qfeMatrix4f::qfeGetMatrixElements(t2v*v2p*mv*p, this->matrixModelViewProjection);    

  return qfeSuccess;
}

//-------------------------------------------------------------------  ---------
qfeReturnStatus qfeRayCastingVector::qfeDrawCylinder(GLfloat *vertCylinder,  GLfloat *vertCap, GLfloat *vertSphere,                                                       
                                                     GLubyte *topoCylinder,  GLubyte *topoCap,                                                    
                                                     GLfloat *colCylinder,   GLfloat *colCap,  GLfloat *colSphere,
                                                     GLint    sizeCylinder,  GLint    sizeCap, GLint    sizeSphere
                                                     )
{
   // Draw the cylinder
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
    
  glColorPointer(3, GL_FLOAT, 0, colCylinder);  
  glTexCoordPointer(3, GL_FLOAT, 0, colCylinder);
  glVertexPointer(3, GL_FLOAT, 0, vertCylinder);    
  glDrawElements(GL_TRIANGLE_STRIP, sizeCylinder, GL_UNSIGNED_BYTE, topoCylinder);    

  glColorPointer(3, GL_FLOAT, 0, colCap);
  glTexCoordPointer(3, GL_FLOAT, 0, colCap);
  glVertexPointer(3, GL_FLOAT, 0, vertCap);  
  glDrawElements(GL_TRIANGLE_FAN, sizeCap, GL_UNSIGNED_BYTE, topoCap);    

  glColorPointer(3, GL_FLOAT, 0, colSphere);
  glTexCoordPointer(3, GL_FLOAT, 0, colSphere);
  glVertexPointer(3, GL_FLOAT, 0, vertSphere);  
  glDrawArrays(GL_TRIANGLE_STRIP, 0, sizeSphere);

  glDisableClientState(GL_COLOR_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_VERTEX_ARRAY);         

  return qfeSuccess;
}

//-------------------------------------------------------------------  ---------
qfeReturnStatus qfeRayCastingVector::qfeDrawCylinder(GLfloat *vertCylinder,  GLfloat *vertCap, GLfloat *vertSphere,                                                       
                                                     GLubyte *topoCylinder,  GLubyte *topoCap,                                                                                                         
                                                     GLint    sizeCylinder,  GLint    sizeCap, GLint    sizeSphere,
                                                     GLfloat  r, GLfloat  g, GLfloat  b, GLfloat a
                                                     )
{
  // Draw the cylinder
  glEnableClientState(GL_VERTEX_ARRAY);  

  glColor4f(r,g,b,a);
   
  glVertexPointer(3, GL_FLOAT, 0, vertCylinder);    
  glDrawElements(GL_TRIANGLE_STRIP, sizeCylinder, GL_UNSIGNED_BYTE, topoCylinder);    
  
  glVertexPointer(3, GL_FLOAT, 0, vertCap);  
  glDrawElements(GL_TRIANGLE_FAN, sizeCap, GL_UNSIGNED_BYTE, topoCap);    
 
  glVertexPointer(3, GL_FLOAT, 0, vertSphere);  
  glDrawArrays(GL_TRIANGLE_STRIP, 0, sizeSphere);

  glDisableClientState(GL_VERTEX_ARRAY);         

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeDrawSphere(qfePoint o, qfeFloat r, qfeColorRGBA color)
{
  qfeFloat   *sphere;
  qfeFloat   *normals;
  int         size;
  qfeMatrix4f T;
  GLfloat     matrix[16];

  this->qfeCreateSphereHandle(20, 20, &sphere, &normals, &size);

  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(0,0) =  r;
  T(1,1) =  r;
  T(2,2) =  r;
  T(3,0) =  o.x;
  T(3,1) =  o.y;
  T(3,2) =  o.z;
  qfeMatrix4f::qfeGetMatrixElements(T, matrix);

  // Start the rendering
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glMultMatrixf(matrix);

  // Draw the sphere    
  glEnableClientState(GL_NORMAL_ARRAY);
  glEnableClientState(GL_VERTEX_ARRAY);

  glNormalPointer(GL_FLOAT, 0, normals);  
  glVertexPointer(3, GL_FLOAT, 0, sphere);  
 
  glColor4f(color.r, color.g, color.b, color.a);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, size);

  glDisableClientState(GL_VERTEX_ARRAY); 
  glDisableClientState(GL_NORMAL_ARRAY);  

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  delete [] sphere;
  delete [] normals;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeCreateSphereHandle(qfeFloat slices, qfeFloat stacks, GLfloat**sphere, GLfloat **normals, GLint *size)
{     
  GLfloat  *s;
  GLfloat  *n;
  int       count;

  s     = new GLfloat[stacks*(slices+1)*2.0f*3.0f];
  n     = new GLfloat[stacks*(slices+1)*2.0f*3.0f];
  count = 0;

  for(int lat=0; lat<stacks; lat++) 
  {
	  for(int lon=0; lon<=slices; lon++) 
    {
      double la1, la2, lo;

      la1 = 2.0f*float(PI)*(lat/stacks);
      la2 = 2.0f*float(PI)*((lat+1)/stacks);
      lo  =     (float(PI)*(lon/slices)) - (0.5f*float(PI));
       
     /* s[3*count + 0] = cos(lo)*sin(la1);
      s[3*count + 1] = sin(lo)*sin(la1);
      s[3*count + 2] = cos(la1);

      n[3*count + 0] = s[3*count + 0];
      n[3*count + 1] = s[3*count + 1];
      n[3*count + 2] = s[3*count + 2];

      count++;

      s[3*count + 0] = cos(lo)*sin(la2);
      s[3*count + 1] = sin(lo)*sin(la2);
      s[3*count + 2] = cos(la2);

      n[3*count + 0] = s[3*count + 0];
      n[3*count + 1] = s[3*count + 1];
      n[3*count + 2] = s[3*count + 2];

      count++;*/
      s[3*count + 0] = cos(la1) * cos(lo);
      s[3*count + 1] = sin(la1);
      s[3*count + 2] = cos(la1) * sin(lo); 

      n[3*count + 0] = s[3*count + 0];
      n[3*count + 1] = s[3*count + 1];
      n[3*count + 2] = s[3*count + 2];

      count++;

      s[3*count + 0] = cos(la2) * cos(lo);
      s[3*count + 1] = sin(la2);
      s[3*count + 2] = cos(la2) * sin(lo);

      n[3*count + 0] = s[3*count + 0];
      n[3*count + 1] = s[3*count + 1];
      n[3*count + 2] = s[3*count + 2];

      count++;     
    }  	
  }  

  *size       = (GLint)(stacks*(slices+1))*2;
  *sphere     = s;
  *normals    = n;
  
  return qfeSuccess;
}


//----------------------------------------------------------------------------
// \brief Creates a cylinder geometry, similar to gluCylinder
//        The centerline of the cylinder is aligned along the positive z-axes,
//        with the origin at the cylinder base
qfeReturnStatus qfeRayCastingVector::qfeCreateCylinderGeometry(GLfloat baseRadius, GLfloat topRadius, GLfloat length, GLint quality, GLfloat** vertices, GLint *size)
{
  GLfloat   theta, radiusStep;  
  int       slices, numSlices, numStacks;

  qfePoint  currentVertex;
  qfePoint  neighborVertex[2];  

  GLfloat  *v;    

  numSlices     = quality;
  numStacks     = 1;

  if(numSlices < 1 || numStacks < 1) return qfeError;
    
  slices        = (int)(numSlices);
  theta         = float(2.0f*PI) / float(slices);  
  radiusStep    = (topRadius - baseRadius) / float(numStacks);        

  *size         = (slices+1) * (numStacks+1);  
  v             = new GLfloat[*size*3];
  
  for (int i = 0; i <= numStacks; i++) 
  {         
    float currentRadius = baseRadius + (radiusStep * float(i));
    float nextRadius    = baseRadius + (radiusStep * float(i+1));    

    // We translate by half the length to get the origin in the middle
    float currentZ      = float(i)   * (length / float(numStacks)); 
    float nextZ         = float(i+1) * (length / float(numStacks));

    for (int j = 0; j <= slices; j++) 
    { 
      qfeVector u, w;  
      float     currentAngle = j * theta;
      float     nextAngle    = (j+1) * theta;
      int       currentIndex = (i*(numSlices+1)) + j;

      // Compute the vertices
      currentVertex.x = cos(currentAngle) * currentRadius;   
      currentVertex.y = sin(currentAngle) * currentRadius;   
      currentVertex.z = currentZ;  
      
      // Store the vertex
      v[3*currentIndex + 0 ] = currentVertex.x; 
      v[3*currentIndex + 1 ] = currentVertex.y; 
      v[3*currentIndex + 2 ] = currentVertex.z;       
    }    
  }   

  (*vertices) = v;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeCreateCylinderTopology(GLfloat baseRadius, GLfloat topRadius, GLint quality, GLubyte **topology, GLint *tsize)
{       
  GLfloat   theta, radiusStep;  
  int       slices;
  int       currentIndex;
  int       tcount = 0;  
  int       numSlices, numStacks;

  qfePoint  currentVertex;
  qfePoint  neighborVertex[2];

  GLubyte  *t;

  numSlices     = quality;
  numStacks     = 1;
    
  slices        = (int)(numSlices);
  theta         = float(2*PI) / float(slices);
  radiusStep    = (topRadius - baseRadius) / float(numStacks);        
    
  // GL_TRIANGLE_STRIP     
  *tsize        = (slices+1) * (numStacks) * 2;
   t             = new GLubyte[*tsize];

  for (int i = 0; i < numStacks; i++) 
  {  
    for (int j = 0; j <= slices; j++) 
    {             
      // Take care of correct winding here
      // Reverse order of strip for each stack
      if(i%2 == 0)
      {
        currentIndex = (i*(slices+1))+j;

        t[2*tcount + 0 ] = currentIndex + slices + 1;
        t[2*tcount + 1 ] = currentIndex;  
      }
      else
      {
        currentIndex = (i*(slices+1))+(slices-j);

        t[2*tcount + 0 ] = currentIndex;
        t[2*tcount + 1 ] = currentIndex + slices + 1;        
      }
    
      tcount ++;        
    }
  } 

  (*topology) = t;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeCreateCylinderColors(GLfloat *geometry, GLint size, qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, GLfloat length, GLfloat** colors)
{
  qfeMatrix4f P2V, V2T, P2T, C, T, R, M;  
  qfeVector   x, y, z;
  GLfloat     a;
  GLfloat    *c; 
  qfePoint    p;

  if(volume == NULL || size <= 0) return qfeError;
  
  // Obtain the transformation to texture coordinates
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);

  P2T = P2V*V2T;

  // Build the transformation of the probe positions to
  // patient coordinates. 
  // This involves a rotation and a translation only
  a = -1.0f*(float(PI) / 2.0f);
  x = axisX;
  z = axisZ;
  qfeVector::qfeVectorNormalize(x);
  qfeVector::qfeVectorNormalize(z);
  qfeVector::qfeVectorOrthonormalBasis1(z, x, y);
  
  qfeMatrix4f::qfeSetMatrixIdentity(C);
  C(0,0) =  cos(a); 
  C(0,1) =  sin(a)*-1.0f;  
  C(1,0) =  sin(a); 
  C(1,1) =  cos(a);  
  C(3,2) =  length*-0.5f;

  qfeMatrix4f::qfeSetMatrixIdentity(R);
  R(0,0) = x.x; 
  R(0,1) = x.y;
  R(0,2) = x.z;
  R(1,0) = y.x; 
  R(1,1) = y.y;
  R(1,2) = y.z;
  R(2,0) = z.x; 
  R(2,1) = z.y;
  R(2,2) = z.z;

  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x;
  T(3,1) = origin.y;
  T(3,2) = origin.z;

  M = C*R*T;

  // Fill the array with color information
  c = new GLfloat[size*3];

  for(int i=0; i<size; i++)
  {
    // Read current point on the cylinder
    p.x = geometry[3*i + 0];
    p.y = geometry[3*i + 1];
    p.z = geometry[3*i + 2];

    // Transform to patient coordinates
    p = p*M;

    // Transform back to texture coordinates
    p = p*P2T;

    // Store the color
    c[3*i + 0] = p.x; 
    c[3*i + 1] = p.y; 
    c[3*i + 2] = p.z;       
  }

  (*colors) = c; 
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// \brief Creates a cap geometry, for use with the cylinder rendering
qfeReturnStatus qfeRayCastingVector::qfeCreateCapGeometry(GLfloat radius, GLint quality, GLfloat** vertices, GLint *size)
{
  GLfloat   theta;  
  int       slices;
  int       numSlices;

  qfePoint  currentVertex;
  qfePoint  neighborVertex[2];  

  GLfloat  *v;    

  numSlices     = quality;

  if(numSlices < 1) return qfeError;
    
  slices        = (int)(numSlices);
  theta         = float(2*PI) / float(slices);   

  *size         = slices+1;  
  v             = new GLfloat[*size*3];
  
  v[0] = 0.0f;
  v[1] = 0.0f;
  v[2] = 0.0f;

  for (int i = 1; i <= slices; i++) 
  { 
    qfeVector u, w;  
    float     currentAngle = (i-1) * theta;
    float     nextAngle    =  i    * theta;

    // Compute the vertices
    currentVertex.x = cos(currentAngle) * radius;   
    currentVertex.y = sin(currentAngle) * radius;   
    currentVertex.z = 0.0;  
    
    // Store the vertex
    v[3*i + 0 ] = currentVertex.x; 
    v[3*i + 1 ] = currentVertex.y; 
    v[3*i + 2 ] = currentVertex.z;       
  }   

  (*vertices) = v;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// \brief Creates a cap topology, for use with the cylinder rendering
//        Note that this topology is a GL_TRIANGLE_FAN
qfeReturnStatus qfeRayCastingVector::qfeCreateCapTopology(GLfloat radius, GLint quality, GLubyte** topology, GLint *tsize)
{
  GLfloat   theta;  
  int       slices;
  int       tcount = 0;  
  int       numSlices;

  qfePoint  currentVertex;
  qfePoint  neighborVertex[2];

  GLubyte  *t;

  numSlices     = quality;
    
  slices        = (int)(numSlices);
  theta         = float(2*PI) / float(slices);
    
  // GL_TRIANGLE_STRIP     
  *tsize        =  slices+2;
   t             = new GLubyte[*tsize];

  for (int i = 0; i <= slices; i++) 
  { 
    t[tcount] = i;  
    tcount ++;        
  }
  t[tcount] = 1;    

  (*topology) = t;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// \brief Creates the cap colors, values will range between [0,1]
//        and can be transformed directly to patient coordinates
//        using the modelview matrix
qfeReturnStatus qfeRayCastingVector::qfeCreateCapColors(GLfloat *geometry, GLint size, qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, GLfloat length, GLfloat **colors)
{
  qfeMatrix4f P2V, V2T, P2T, T, R, C, M;  
  qfeVector   x, y, z;
  GLfloat     a;
  GLfloat    *c; 
  qfePoint    p;

  if(volume == NULL || size <= 0) return qfeError;
  
  // Obtain the transformation to texture coordinates
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);

  P2T = P2V*V2T;

  // Build the transformation of the probe positions to
  // patient coordinates. 
  // This involves a rotation and a translation only
  a = -1.0f*(float(PI) / 2.0f);
  x = axisX;
  z = axisZ;
  qfeVector::qfeVectorNormalize(x);
  qfeVector::qfeVectorNormalize(z);
  qfeVector::qfeVectorOrthonormalBasis1(z, x, y);

  qfeMatrix4f::qfeSetMatrixIdentity(C);
  C(0,0) =  cos(a); 
  C(0,1) =  sin(a)*-1.0f;  
  C(1,0) =  sin(a); 
  C(1,1) =  cos(a);  
  C(3,2) =  length*-0.5f;

  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x;
  T(3,1) = origin.y;
  T(3,2) = origin.z;

  qfeMatrix4f::qfeSetMatrixIdentity(R);
  R(0,0) = x.x; 
  R(0,1) = x.y;
  R(0,2) = x.z;
  R(1,0) = y.x; 
  R(1,1) = y.y;
  R(1,2) = y.z;
  R(2,0) = z.x; 
  R(2,1) = z.y;
  R(2,2) = z.z;

  M = C*R*T;

  // Fill the array with color information
  c = new GLfloat[size*3];

  for(int i=0; i<size; i++)
  {
    // Read current point on the cylinder
    p.x = geometry[3*i + 0];
    p.y = geometry[3*i + 1];
    p.z = geometry[3*i + 2];

    // Transform to patient coordinates
    p = p*M;

    // Transform back to texture coordinates
    p = p*P2T;

    // Store the color
    c[3*i + 0] = p.x; 
    c[3*i + 1] = p.y; 
    c[3*i + 2] = p.z;       
  }

  (*colors) = c; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// \brief Creates a half sphere geometry, for use with the cylinder rendering
qfeReturnStatus qfeRayCastingVector::qfeCreateSphereGeometry(GLfloat topRadius, GLfloat length, GLint quality, GLfloat** vertices, GLint *size)
{
  GLfloat   stacks, slices, radius, angle;
  GLfloat  *v;    
  GLint     count;
  
  stacks  = 20;
  slices  = quality;

  *size   = (GLint)(stacks*(slices+1))*2;  
  v       = new GLfloat[*size*3];

  radius  = sqrt(pow(topRadius,2.0f) + pow(0.5f*length, 2.0f));
  angle   = atan((topRadius / (0.5f*length)));

  count   = 0;

  for(int lat=0; lat<stacks; lat++) 
  {
	  for(int lon=0; lon<=slices; lon++) 
    {
      double la1, la2, lo;

      la1 = float(angle)*lat/stacks;
      la2 = float(angle)*(lat+1)/stacks;
      lo  = float(2.0*PI)*lon/slices;
       
      v[3*count + 0] = radius*sin(la1)*cos(lo);
      v[3*count + 1] = radius*sin(la1)*sin(lo);
      v[3*count + 2] = radius*cos(la1);

      count++;

      v[3*count + 0] = radius*sin(la2)*cos(lo);
      v[3*count + 1] = radius*sin(la2)*sin(lo);
      v[3*count + 2] = radius*cos(la2);

      count++;
    }  	
  }  

  (*vertices) = v;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// \brief Creates the sphere colors, values will range between [0,1]
//        and can be transformed directly to patient coordinates
//        using the modelview matrix
qfeReturnStatus qfeRayCastingVector::qfeCreateSphereColors(GLfloat *geometry, GLint size, qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, GLfloat length, GLfloat **colors)
{
  qfeMatrix4f P2V, V2T, P2T, T, R, C, M;  
  qfeVector   x, y, z;
  GLfloat     a;
  GLfloat    *c; 
  qfePoint    p;

  if(volume == NULL || size <= 0) return qfeError;
  
  // Obtain the transformation to texture coordinates
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);

  P2T = P2V*V2T;

  // Build the transformation of the probe positions to
  // patient coordinates. 
  // This involves a rotation and a translation only
  a = -1.0f*(float(PI) / 2.0f);
  x = axisX;
  z = axisZ;
  qfeVector::qfeVectorNormalize(x);
  qfeVector::qfeVectorNormalize(z);
  qfeVector::qfeVectorOrthonormalBasis1(z, x, y);

  qfeMatrix4f::qfeSetMatrixIdentity(C);
  C(0,0) =  cos(a); 
  C(0,1) =  sin(a)*-1.0f;  
  C(1,0) =  sin(a); 
  C(1,1) =  cos(a);  
  C(3,2) =  length*-0.5f;

  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x;
  T(3,1) = origin.y;
  T(3,2) = origin.z;

  qfeMatrix4f::qfeSetMatrixIdentity(R);
  R(0,0) = x.x; 
  R(0,1) = x.y;
  R(0,2) = x.z;
  R(1,0) = y.x; 
  R(1,1) = y.y;
  R(1,2) = y.z;
  R(2,0) = z.x; 
  R(2,1) = z.y;
  R(2,2) = z.z;

  M = C*R*T;

  // Fill the array with color information
  c = new GLfloat[size*3];

  for(int i=0; i<size; i++)
  {
    // Read current point on the cylinder
    p.x = geometry[3*i + 0];
    p.y = geometry[3*i + 1];
    p.z = geometry[3*i + 2];

    // Transform to patient coordinates
    p = p*M;

    // Transform back to texture coordinates
    p = p*P2T;

    // Store the color
    c[3*i + 0] = p.x; 
    c[3*i + 1] = p.y; 
    c[3*i + 2] = p.z;       
  }

  (*colors) = c; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeCreateBoxGeometry(GLfloat radius, GLfloat length, GLfloat** vertices, GLint *size)
{
  GLfloat  *v;    

  *size         = 8;  
  v             = new GLfloat[*size*3];

  v[3*0+0]      = -radius;
  v[3*0+1]      = -radius;
  v[3*0+2]      =  0.0f;

  v[3*1+0]      =  radius;
  v[3*1+1]      = -radius;
  v[3*1+2]      =  0.0f;

  v[3*2+0]      = -radius;
  v[3*2+1]      =  radius;
  v[3*2+2]      =  0.0f;

  v[3*3+0]      =  radius;
  v[3*3+1]      =  radius;
  v[3*3+2]      =  0.0f;

  v[3*4+0]      = -radius;
  v[3*4+1]      = -radius;
  v[3*4+2]      =  length;

  v[3*5+0]      =  radius;
  v[3*5+1]      = -radius;
  v[3*5+2]      =  length;

  v[3*6+0]      = -radius;
  v[3*6+1]      =  radius;
  v[3*6+2]      =  length;

  v[3*7+0]      =  radius;
  v[3*7+1]      =  radius;
  v[3*7+2]      =  length;

  (*vertices) = v;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeCreateBoxTopology(GLubyte** topology, GLint *tsize)
{
  GLubyte  *t;
    
  // GL_TRIANGLE     
  *tsize        = 36;
   t            = new GLubyte[*tsize];

  // back
  t[0]  = 0;  t[3]  = 3;
  t[1]  = 1;  t[4]  = 2;
  t[2]  = 2;  t[5]  = 1;

  // front
  t[6]  = 4;  t[9]  = 7;
  t[7]  = 6;  t[10] = 5;
  t[8]  = 5;  t[11] = 6;  

  // left
  t[12] = 0;  t[15] = 6;
  t[13] = 2;  t[16] = 4;
  t[14] = 4;  t[17] = 2;  

  // right
  t[18] = 5;  t[21] = 3;
  t[19] = 7;  t[22] = 1;
  t[20] = 1;  t[23] = 7;  

  // top
  t[24] = 2;  t[27] = 7;
  t[25] = 3;  t[28] = 6;
  t[26] = 6;  t[29] = 3;  

  // bottom
  t[30] = 0;  t[33] = 5;
  t[31] = 4;  t[34] = 1;
  t[32] = 1;  t[35] = 4; 

  (*topology)   = t;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeCreateBoxColors(GLfloat *geometry, GLint size, qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, GLfloat length, GLfloat** colors)
{
  qfeMatrix4f P2V, V2T, P2T, C, T, R, M;  
  qfeVector   x, y, z;
  GLfloat     a;
  GLfloat    *c; 
  qfePoint    p;

  if(volume == NULL || size <= 0) return qfeError;
  
  // Obtain the transformation to texture coordinates
  qfeTransform::qfeGetMatrixPatientToVoxel(P2V, volume);
  qfeTransform::qfeGetMatrixVoxelToTexture(V2T, volume);

  P2T = P2V*V2T;

  // Build the transformation of the probe positions to
  // patient coordinates. 
  // This involves a rotation and a translation only
  a = -1.0f*(float(PI) / 2.0f);
  x = axisX;
  z = axisZ;
  qfeVector::qfeVectorCross(x, z, y);
  qfeVector::qfeVectorNormalize(x);
  qfeVector::qfeVectorNormalize(y);
  qfeVector::qfeVectorNormalize(z);

  qfeMatrix4f::qfeSetMatrixIdentity(C);
  C(0,0) =  cos(a); 
  C(0,1) =  sin(a)*-1.0f;  
  C(1,0) =  sin(a); 
  C(1,1) =  cos(a);  
  C(3,2) =  length*-0.5f;

  qfeMatrix4f::qfeSetMatrixIdentity(R);
  R(0,0) = x.x; 
  R(0,1) = x.y;
  R(0,2) = x.z;
  R(1,0) = y.x; 
  R(1,1) = y.y;
  R(1,2) = y.z;
  R(2,0) = z.x; 
  R(2,1) = z.y;
  R(2,2) = z.z;

  qfeMatrix4f::qfeSetMatrixIdentity(T);
  T(3,0) = origin.x;
  T(3,1) = origin.y;
  T(3,2) = origin.z;

  M = C*R*T;

  // Fill the array with color information
  c = new GLfloat[size*3];

  for(int i=0; i<size; i++)
  {
    // Read current point on the cylinder
    p.x = geometry[3*i + 0];
    p.y = geometry[3*i + 1];
    p.z = geometry[3*i + 2];

    // Transform to patient coordinates
    p = p*M;

    // Transform back to texture coordinates
    p = p*P2T;

    // Store the color
    c[3*i + 0] = p.x; 
    c[3*i + 1] = p.y; 
    c[3*i + 2] = p.z;       
  }

  (*colors) = c; 
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeRayCastingVector::qfeCreateColorMaps()
{ 
  // Initialize doppler colormap
  vector<qfeRGBMapping>     rgb;
  vector<qfeOpacityMapping> o;  
  
  rgb.resize(5);
  rgb[0].color.r = 0.840; rgb[0].color.g = 0.879; rgb[0].color.b = 0.355; rgb[0].value = 0.00;
  rgb[1].color.r = 0.594; rgb[1].color.g = 0.137; rgb[1].color.b = 0.000; rgb[1].value = 0.25;
  rgb[2].color.r = 0.001; rgb[2].color.g = 0.001; rgb[2].color.b = 0.001; rgb[2].value = 0.50;
  rgb[3].color.r = 0.047; rgb[3].color.g = 0.551; rgb[3].color.b = 1.000; rgb[3].value = 0.75;
  rgb[4].color.r = 0.484; rgb[4].color.g = 0.965; rgb[4].color.b = 0.990; rgb[4].value = 1.00;

  o.resize(2);
  o[0].opacity = 1.0; o[0].value = 0.0;
  o[1].opacity = 1.0; o[1].value = 1.0;
  
  this->colorMapUS->qfeSetColorMapRGB(5, rgb);
  this->colorMapUS->qfeSetColorMapA(2, o);
  this->colorMapUS->qfeSetColorMapG(2, o);
  this->colorMapUS->qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
  this->colorMapUS->qfeSetColorMapSpace(qfeColorSpaceLab);

  rgb.resize(2);
  rgb[0].color.r = 0.0; rgb[0].color.g = 0.0; rgb[0].color.b = 0.8; rgb[0].value = 0.00;
  rgb[1].color.r = 0.8; rgb[1].color.g = 0.8; rgb[1].color.b = 0.0; rgb[1].value = 1.0;

  o.resize(2);
  o[0].opacity = 1.0; o[0].value = 0.0;
  o[1].opacity = 1.0; o[1].value = 1.0;

  this->colorMapCoolWarm->qfeSetColorMapRGB(2, rgb);
  this->colorMapCoolWarm->qfeSetColorMapA(2, o);
  this->colorMapCoolWarm->qfeSetColorMapG(2, o);
  this->colorMapCoolWarm->qfeSetColorMapInterpolation(qfeColorInterpolationLinear);
  this->colorMapCoolWarm->qfeSetColorMapSpace(qfeColorSpaceLab);

  return qfeSuccess;
}

