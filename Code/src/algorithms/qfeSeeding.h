#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithm.h"
#include "qfeFrame.h"
#include "qfeMatrix4f.h"
#include "qfeModel.h"
#include "qfePoint.h"
#include "qfeProbe.h"
#include "qfeVector.h"

#include <algorithm> // for sort
#include <cstdlib> 
#include <vector>
#include <ctime>

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

typedef vector< qfePoint >  qfeSeeds;

enum qfeSeedType
{
  QFE_SEED_POISSON_DISK,
  QFE_SEED_RANDOM_NORM,
  QFE_SEED_RANDOM_RECT,
  QFE_SEED_RADIAL,
  QFE_SEED_CIRCLES,    
  QFE_SEED_RECTILINEAR,
};

enum qfeSeedTransferFunction
{
  QFE_SEED_DENSITY_HOMOGENEOUS, 
  QFE_SEED_DENSITY_VESSEL_CENTER,
  QFE_SEED_DENSITY_VESSEL_WALL  
};

/**
* \file   qfeSeeding.h
* \author Roy van Pelt
* \class  qfeSeeding
* \brief  Implements seeding for difference geometries and visualization styles
* \note   Confidential
*
* Generate seeding positions for various geometries
*/

class qfeSeeding : public qfeAlgorithm
{
public:
  qfeSeeding();
  qfeSeeding(const qfeSeeding &fv);
  ~qfeSeeding();

  static qfeReturnStatus qfeGetSeeds(qfeProbe2D *probe, qfeSeedType seedStrategy, double seedDensity, qfeSeeds &seeds);
    
  static qfeReturnStatus qfeGetSeedsRandomUniform(qfeFrameSlice *plane, int seedsCount, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsRandomUniform(qfeProbe2D *probe, double density, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsRandomUniform(qfeProbe3D *probe, int seedsCount, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsRandomUniform(qfeModel *model, int seedsCount, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsRandomPoissonDisk(qfeFrameSlice *plane, int seedsCount, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsRandomPoissonDisk(qfeProbe2D *probe, double density, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsRandomPoissonDisk(qfeProbe3D *probe, int seedsCount, qfeSeeds &seeds);
  //static qfeReturnStatus qfeGetSeedsRandomPoissonDisk(qfeModel *model, int seedsCount, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsRandomRectilinear(qfeProbe2D *probe, double density, qfeSeeds &seeds);

  static qfeReturnStatus qfeGetSeedsRadial(qfeProbe2D *probe, double density, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsCircles(qfeProbe2D *probe, double density, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsRectilinear(qfeProbe2D *probe, double density, qfeSeeds &seeds);

  static qfeReturnStatus qfeGetSeedsCircle(qfePoint origin, qfeVector normal, qfeFloat radius, int seedsCount, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsCircle(qfeProbe3D *probe, int seedsCount, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsLine(qfePoint origin, qfeVector direction, qfeFloat radius, int seedsCount, qfeSeeds &seeds);
  static qfeReturnStatus qfeGetSeedsLine(qfeProbe3D *probe, int seedsCount, qfeSeeds &seeds);  
  static qfeReturnStatus qfeGetSeedsLine(qfeProbe3D *probe, qfeVector vec2D, int seedsCount, qfeSeeds &seeds);  

  static qfeReturnStatus qfeComputeSeedsTransferFunction(qfeProbe2D *probe, qfeSeedTransferFunction tf, qfeSeeds &seeds);
  static qfeReturnStatus qfeComputeSeedsDepthSort(qfeMatrix4f modelview, qfeSeeds &seeds);

  static bool   qfeInProbe(qfePoint p, qfeProbe2D *probe);
  static bool   qfeInProbe(qfePoint p, qfeProbe3D *probe);
  static bool   qfeInModel(qfePoint p, qfeModel *model);

protected:
  static qfeReturnStatus qfeGenerateUniformSeed2D(qfePoint &p, qfePoint extent);
  static qfeReturnStatus qfeGenerateUniformSeed3D(qfePoint &p, qfePoint extent);
  static qfeReturnStatus qfeGenerateUniformSeed3D(qfePoint &p, double   bounds[]);

  static qfeReturnStatus qfeGeneratePoissonDiskSeed2D(qfePoint p, qfeFloat minDist, qfePoint &q);
  static qfeReturnStatus qfeGeneratePoissonDiskSeed3D(qfePoint p, qfeFloat minDist, qfePoint &q);

  static bool   qfeInRectangle(qfePoint p, qfeFloat width, qfeFloat height);
  static bool   qfeInCircle(qfePoint p, qfePoint origin, qfeFloat radius);

  static bool   qfeInNeighborhood2D(qfePoint p, qfeSeeds *set, qfeFloat minDist);
  static bool   qfeInNeighborhood3D(qfePoint p, qfeSeeds *set, qfeFloat minDist);

  static double qfeGetRandomDouble(); 
  static double qfeGetRandomDouble(double low, double high); 
  static int    qfeGetRandomInt(int low, int high); 

private :
  static void qfeProjectPointToPlane(qfeVector x, qfeVector y, qfePoint o, qfePoint p3D, qfePoint &p2D);
  static bool qfeIntersectLines(qfePoint a, qfePoint b, qfePoint c, qfePoint d, qfePoint &p);

};
