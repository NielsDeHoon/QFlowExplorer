#include "qfeSurfaceShading.h"

//----------------------------------------------------------------------------
qfeSurfaceShading::qfeSurfaceShading()
{
  this->shaderSilhouette = new qfeGLShaderProgram();
  this->shaderSilhouette->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderSilhouette->qfeAddShaderFromFile("/shaders/ifr/silhouette-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderSilhouette->qfeAddShaderFromFile("/shaders/ifr/silhouette-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderSilhouette->qfeBindAttribLocation("qfe_Vertex", ATTRIB_VERTEX);
  this->shaderSilhouette->qfeBindAttribLocation("qfe_Normal", ATTRIB_NORMAL); 
  this->shaderSilhouette->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderSilhouette->qfeLink();

  this->shaderCulling = new qfeGLShaderProgram();
  this->shaderCulling->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderCulling->qfeAddShaderFromFile("/shaders/ifr/silhouette-culling-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderCulling->qfeAddShaderFromFile("/shaders/ifr/silhouette-culling-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderCulling->qfeAddShaderFromFile("/shaders/ifr/silhouette-culling-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderCulling->qfeBindAttribLocation("qfe_Vertex", ATTRIB_VERTEX);
  this->shaderCulling->qfeBindAttribLocation("qfe_Normal", ATTRIB_NORMAL); 
  this->shaderCulling->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderCulling->qfeLink();

  this->shaderFresnel = new qfeGLShaderProgram();
  this->shaderFresnel->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);
  this->shaderFresnel->qfeAddShaderFromFile("/shaders/ifr/silhouette-fresnel-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderFresnel->qfeAddShaderFromFile("/shaders/ifr/silhouette-fresnel-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderFresnel->qfeAddShaderFromFile("/shaders/ifr/silhouette-fresnel-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderFresnel->qfeBindAttribLocation("qfe_Vertex", ATTRIB_VERTEX);
  this->shaderFresnel->qfeBindAttribLocation("qfe_Normal", ATTRIB_NORMAL); 
  this->shaderFresnel->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderFresnel->qfeLink();

  this->shaderDilate = new qfeGLShaderProgram();
  this->shaderDilate->qfeAddShaderFromFile("/shaders/ifr/silhouette-dilate-vert.glsl", QFE_VERTEX_SHADER);
  this->shaderDilate->qfeAddShaderFromFile("/shaders/ifr/silhouette-dilate-geom.glsl", QFE_GEOMETRY_SHADER);
  this->shaderDilate->qfeAddShaderFromFile("/shaders/ifr/silhouette-dilate-frag.glsl", QFE_FRAGMENT_SHADER);
  this->shaderDilate->qfeBindAttribLocation("qfe_Vertex", ATTRIB_VERTEX);
  this->shaderDilate->qfeBindAttribLocation("qfe_Normal", ATTRIB_NORMAL); 
  this->shaderDilate->qfeBindFragDataLocation("qfe_FragColor", 0);
  this->shaderDilate->qfeLink();

  this->firstRender   = true;
  this->texLighting   = 0;
  this->light         = NULL;

  this->color.r       = 0.7;
  this->color.g       = 0.7;
  this->color.b       = 0.7;
  this->culling       = true;
  this->dilation      = 0.0;
  this->shading       = qfeSurfaceShadingFlat;

  this->wssAvailable  = false;

  this->qfeCreateTextures();
};

//----------------------------------------------------------------------------
qfeSurfaceShading::qfeSurfaceShading(const qfeSurfaceShading &surfshad)
{
  this->firstRender      = surfshad.firstRender;
  this->shaderSilhouette = surfshad.shaderSilhouette;
  this->shaderCulling    = surfshad.shaderCulling;
  this->shaderFresnel    = surfshad.shaderFresnel;
  this->shaderDilate     = surfshad.shaderDilate;
  this->texLighting      = surfshad.texLighting;
  this->light            = surfshad.light;
  
  this->v2p              = surfshad.v2p;
  this->color.r          = surfshad.color.r;
  this->color.g          = surfshad.color.g;
  this->color.b          = surfshad.color.b;
  this->culling          = surfshad.culling;
  this->dilation         = surfshad.dilation;
  this->shading          = surfshad.shading; 
  this->wssAvailable     = surfshad.wssAvailable;
};

//----------------------------------------------------------------------------
qfeSurfaceShading::~qfeSurfaceShading()
{
  this->qfeDeleteTextures();

  delete this->shaderSilhouette;
  delete this->shaderCulling;
  delete this->shaderFresnel;
  delete this->shaderDilate;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeSetSurfaceColor(qfeColorRGB color)
{
  this->color = color;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeSetSurfaceCulling(bool culling)
{
  this->culling = culling;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeSetSurfaceShading(qfeSurfaceShadingType type)
{
  this->shading = type;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeSetLightSource(qfeLightSource *light)
{
  this->light = light;
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeRenderMesh(vtkPolyData *mesh, qfeVolume *volume)
{
  qfeMatrix4f   V2P;  
  GLfloat       matrix[16];

  if(mesh == NULL || volume == NULL) return qfeError;

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);   
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]);

  if(this->firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->v2p, volume);

    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in voxel coordinates
  glMultMatrixf(&matrix[0]);  

  // 1 Draw mesh 
  this->qfeBindTextures();
  this->qfeSetDynamicUniformLocations();
  this->shaderSilhouette->qfeEnable();
  
  glPolygonMode(GL_BACK, GL_FILL); 
  glPolygonOffset(5.0,30.0);
  glEnable(GL_POLYGON_OFFSET_FILL);

  glDisable(GL_BLEND);
  if(this->culling)
  {
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
  }

  this->qfeDrawMeshTriangleStrips(mesh, this->color);  

  glDisable(GL_CULL_FACE);
  glEnable(GL_BLEND);

  this->shaderSilhouette->qfeDisable();
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeRenderMesh(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshIndices *triangles, qfeVolume *volume)
{
  qfeMatrix4f   V2P;  
  GLfloat       matrix[16];

  if(vertexBuffer == NULL || normalBuffer == NULL || volume == NULL) 
    return qfeError;

  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);   
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]);

  if(this->firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->v2p, volume);

    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }
  
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in voxel coordinates
  glMultMatrixf(&matrix[0]);  

  // Obtain the modelview matrix
  this->qfeGetModelViewMatrix();

  // 1 Draw mesh 
  this->qfeBindTextures();
  this->qfeSetDynamicUniformLocations();
  this->shaderSilhouette->qfeEnable();
  
  glDisable(GL_BLEND);
  if(this->culling)
  {
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
  }

  this->qfeDrawMeshTriangles(vertexBuffer, normalBuffer, triangles);  

  glDisable(GL_CULL_FACE);
  glEnable(GL_BLEND);

  this->shaderSilhouette->qfeDisable();
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();  
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeRenderMesh(qfeMeshStrips *vertexBuffer, qfeMeshStrips *normalBuffer, qfeVolume *volume)
{
  qfeMatrix4f   V2P;  
  GLfloat       matrix[16];

  if(vertexBuffer == NULL || normalBuffer == NULL || volume == NULL) 
    return qfeError;

  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);   
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]);

  if(this->firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->v2p, volume);

    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in voxel coordinates
  glMultMatrixf(&matrix[0]);  

  // 1 Draw mesh 
  this->qfeBindTextures();
  this->qfeSetDynamicUniformLocations();
  this->shaderSilhouette->qfeEnable();
  
  //glPolygonMode(GL_BACK, GL_FILL); 
  //glPolygonOffset(5.0,30.0);
  //glEnable(GL_POLYGON_OFFSET_FILL);

  glDisable(GL_BLEND);
  if(this->culling)
  {
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
  }

  this->qfeDrawMeshTriangleStrips(vertexBuffer, normalBuffer, this->color);  

  glDisable(GL_CULL_FACE);
  glEnable(GL_BLEND);

  this->shaderSilhouette->qfeDisable();
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeRenderMesh(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshStripsIndices *triangleStrips, qfeVolume *volume)
{
  qfeMatrix4f   V2P;  
  GLfloat       matrix[16];

  if(vertexBuffer == NULL || normalBuffer == NULL || volume == NULL) 
    return qfeError;

  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);   
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]);

  if(this->firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->v2p, volume);

    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }
  
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in voxel coordinates
  glMultMatrixf(&matrix[0]);  

  // Obtain the modelview matrix
  this->qfeGetModelViewMatrix();

  // 1 Draw mesh 
  this->qfeBindTextures();
  this->qfeSetDynamicUniformLocations();
  this->shaderSilhouette->qfeEnable();
  
  //glPolygonMode(GL_BACK, GL_FILL); 
  //glPolygonOffset(5.0,30.0);
  //glEnable(GL_POLYGON_OFFSET_FILL);

  glDisable(GL_BLEND);
  if(this->culling)
  {
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
  }

  this->qfeDrawMeshTriangleStrips(vertexBuffer, normalBuffer, triangleStrips);  

  glDisable(GL_CULL_FACE);
  glEnable(GL_BLEND);

  this->shaderSilhouette->qfeDisable();
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeRenderMeshCulling(vtkPolyData *mesh, qfeVolume *volume, qfeColorRGB color, bool clipping, int type)
{
  qfeMatrix4f   V2P;  
  GLfloat       matrix[16];

  if(mesh == NULL || volume == NULL) return qfeError;

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);   
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]);

  if(this->firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->v2p, volume);

    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in voxel coordinates
  glMultMatrixf(&matrix[0]);  

  // Obtain the modelview matrix
  this->qfeGetModelViewMatrix();

  // 1 Draw mesh 
  this->qfeBindTextures();
  this->qfeSetDynamicUniformLocations();
 
  glEnable(GL_BLEND);

  this->shaderCulling->qfeEnable();
  
  this->qfeDrawMeshTriangleStrips(mesh, this->color);

  this->shaderCulling->qfeDisable();

  if(type == qfeSurfaceShadingFresnel) // Include Fresnel front faces
  {
    glDepthMask(false);
    this->shaderFresnel->qfeEnable();
  
    this->qfeDrawMeshTriangleStrips(mesh, this->color); 

    this->shaderFresnel->qfeDisable();
    glDepthMask(true);
  }
  
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeRenderMeshDilate(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshIndices *triangles, float offset, qfeVolume *volume)
{
  qfeMatrix4f   V2P;  
  GLfloat       matrix[16];

  if(vertexBuffer == NULL || normalBuffer == NULL || volume == NULL) 
    return qfeError;

  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);   
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]);

  if(this->firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->v2p, volume);

    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  this->dilation = offset;

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in voxel coordinates
  glMultMatrixf(&matrix[0]);  

  // Obtain the modelview matrix
  this->qfeGetModelViewMatrix();

  // 1 Draw mesh 
  this->qfeBindTextures();
  this->qfeSetDynamicUniformLocations();
  this->shaderDilate->qfeEnable();
  
  //glPolygonMode(GL_BACK, GL_FILL); 
  //glPolygonOffset(5.0,30.0);
  //glEnable(GL_POLYGON_OFFSET_FILL);

  glDisable(GL_BLEND);
  if(this->culling)
  {
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
  }

  this->qfeDrawMeshTriangles(vertexBuffer, normalBuffer, triangles);  

  glDisable(GL_CULL_FACE);
  glEnable(GL_BLEND);

  this->shaderDilate->qfeDisable();
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeRenderDebug(vtkPolyData *mesh, qfeVolume *volume)
{
  qfeMatrix4f   V2P;  
  GLfloat       matrix[16];

  if(mesh == NULL || volume == NULL) return qfeError;
  
  vtkCellArray *strips   = mesh->GetStrips();
  vtkPoints    *points   = mesh->GetPoints();  

  vtkTriangleFilter *triangleFilter = vtkTriangleFilter::New();
  triangleFilter->SetInput(mesh);
  triangleFilter->Update();
/* 
  vtkExtractEdges *extractEdges = vtkExtractEdges::New();
  extractEdges->SetInput(triangleFilter->GetOutput());
  extractEdges->Update();

  edges = extractEdges->GetOutput();
  */

  if(strips == NULL || points == NULL )  return qfeError;  

  // OpenGL works with column-major matrices
  // OpenGL works with left-multiplication
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);   
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]);

  if(this->firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->v2p, volume);

    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glEnable(GL_POINT_SMOOTH);  

  // Render in voxel coordinates
  glMultMatrixf(&matrix[0]);  

  vtkIdType nPts = 0;
  vtkIdType *ptIds = strips->GetPointer();

  vtkIdList *neighborsP1 = vtkIdList::New();
  vtkIdList *neighborsP2 = vtkIdList::New();
  vtkIdList *mutual      = vtkIdList::New();

  int index=0;
  for (strips->InitTraversal(); strips->GetNextCell(nPts,ptIds);)
  {
    if(index==50)
    {
      // draw points of the strip
      glPointSize(4.0);
      glBegin(GL_POINTS);
      for (int j = 0; j < nPts; j++)
      {
        double v[3];

        points->GetPoint(ptIds[j], v);
        
        glColor4f(0.0,0.0,0.0,1.0);      
        glVertex3f (v[0], v[1], v[2]);
      }
      glEnd();

      glLineWidth(2.0);
      glBegin(GL_LINE_STRIP);
      for (int j = 0; j < nPts; j++)
      {
        double v[3];

        points->GetPoint(ptIds[j], v);
        
        glColor4f(0.0,0.0,0.0,1.0);      
        glVertex3f (v[0], v[1], v[2]);
      }
      glEnd();

      // draw two neigborhoods
      double n[3];
      
      this->qfeGetMeshConnectedPoints(triangleFilter->GetOutput(), ptIds[0], neighborsP1);
      this->qfeGetMeshConnectedPoints(triangleFilter->GetOutput(), ptIds[1], neighborsP2);

      glPointSize(10.0);
      glBegin(GL_POINTS);
        points->GetPoint(ptIds[0], n);
        
        glColor4f(1.0,0.0,0.0,1.0);      
        glVertex3f (n[0], n[1], n[2]);      
      glEnd();

      glPointSize(6.0);
      glBegin(GL_POINTS);
      for (int j = 0; j < neighborsP1->GetNumberOfIds(); j++)
      {

        points->GetPoint(neighborsP1->GetId(j), n);
        
        glColor4f(1.0,0.0,0.0,1.0);      
        glVertex3f (n[0], n[1], n[2]);
      }
      glEnd();

      //
      glPointSize(10.0);
      glBegin(GL_POINTS);
        points->GetPoint(ptIds[1], n);
        
        glColor4f(0.0,0.0,1.0,1.0);      
        glVertex3f (n[0], n[1], n[2]);      
      glEnd();

      glPointSize(6.0);
      glBegin(GL_POINTS);
      for (int j = 0; j < neighborsP2->GetNumberOfIds(); j++)
      {

        points->GetPoint(neighborsP2->GetId(j), n);
        
        glColor4f(0.0,0.0,1.0,1.0);      
        glVertex3f (n[0], n[1], n[2]);
      }
      glEnd();

      for(int i=0; i<neighborsP1->GetNumberOfIds(); i++)
      {
        for(int j=0; j<neighborsP2->GetNumberOfIds(); j++)
        {
          // Every shared point that is not the 
          // third vertex of the triangle is added to the list
          if(neighborsP1->GetId(i) == neighborsP2->GetId(j))          
            mutual->InsertNextId(neighborsP1->GetId(i));          
        }    
      }

      glPointSize(12.0);
      glBegin(GL_POINTS);
      for (int j = 0; j < mutual->GetNumberOfIds(); j++)
      {

        points->GetPoint(mutual->GetId(j), n);
        
        glColor4f(0.0,1.0,0.0,1.0);      
        glVertex3f (n[0], n[1], n[2]);
      }
      glEnd();



    }

    index++;
  }

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeRenderDebug(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshStripsIndices *indices, qfeVolume *volume)
{
  qfeMatrix4f   V2P;  
  GLfloat       matrix[16];

  if(vertexBuffer == NULL || normalBuffer == NULL || volume == NULL) 
    return qfeError;

  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);   
  qfeMatrix4f::qfeGetMatrixElements(V2P, &matrix[0]);

  if(this->firstRender)
  {
    qfeTransform::qfeGetMatrixVoxelToPatient(this->v2p, volume);

    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in voxel coordinates
  glMultMatrixf(&matrix[0]);

  glEnable(GL_POINT_SMOOTH); 
  glPointSize(4.0);

  // 1 Draw mesh 

  for (int i=0; i<(int)indices->size(); i++)
  {    
    if(i==50)
    {
      //GLushort *indexBuffer = &indices->at(i).front(); 
      GLint     indexSize   =  indices->at(i).size();

      vector<GLushort> odd;
      vector<GLushort> even;

      for(int j=0; j<indexSize; j++)
      {
        if((j%2) == 0) 
          even.push_back(indices->at(i).at(j));
        else
          odd.push_back(indices->at(i).at(j));
      }

      // Render
      glColor3f(0.0, 0.0, 0.0);

      glEnableClientState(GL_NORMAL_ARRAY);
      glEnableClientState(GL_VERTEX_ARRAY);

      glNormalPointer(GL_FLOAT, 0, &normalBuffer->front());  
      glVertexPointer(3, GL_FLOAT, 0, &vertexBuffer->front());  
     
      glDrawElements(GL_LINE_STRIP, even.size(), GL_UNSIGNED_SHORT, &even.front());

      glDisableClientState(GL_VERTEX_ARRAY); 
      glDisableClientState(GL_NORMAL_ARRAY); 

      // Render
      glColor3f(1.0, 0.0, 0.0);

      glEnableClientState(GL_NORMAL_ARRAY);
      glEnableClientState(GL_VERTEX_ARRAY);

      glNormalPointer(GL_FLOAT, 0, &normalBuffer->front());  
      glVertexPointer(3, GL_FLOAT, 0, &vertexBuffer->front());  
     
      glDrawElements(GL_POINTS, odd.size(), GL_UNSIGNED_SHORT, &odd.front());

      glDisableClientState(GL_VERTEX_ARRAY); 
      glDisableClientState(GL_NORMAL_ARRAY); 
    }
  }

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeGetMeshConnectedPoints(vtkPolyData *edges, int id, vtkIdList *neighbors)
{
  //get all cells that vertex 'id' is a part of
  vtkIdList   *cellIdList = vtkIdList::New();  

  edges->GetPointCells(id, cellIdList);
 
  /*
  cout << "Vertex 0 is used in cells ";
  for(vtkIdType i = 0; i < cellIdList->GetNumberOfIds(); i++)
    {
    cout << cellIdList->GetId(i) << ", ";
    }
  cout << endl;
  */
 
  for(vtkIdType i = 0; i < cellIdList->GetNumberOfIds(); i++)
  {
    //cout << "id " << i << " : " << cellIdList->GetId(i) << endl;
 
    vtkIdList *pointIdList = vtkIdList::New();
    edges->GetCellPoints(cellIdList->GetId(i), pointIdList);
 
    //cout << "End points are " << pointIdList->GetId(0) << " and " << pointIdList->GetId(1) << endl;
 
    //if(pointIdList->GetId(0) != id)
    //{
    //  //cout << "Connected to " << pointIdList->GetId(0) << endl;
    //  neighbors->InsertNextId(pointIdList->GetId(0));
    //}
    //else
    //{
    //  //cout << "Connected to " << pointIdList->GetId(1) << endl;
    //  neighbors->InsertNextId(pointIdList->GetId(1));
    //}
    for(vtkIdType j = 0; j<pointIdList->GetNumberOfIds(); j++)
    {
      if(pointIdList->GetId(j) != id)
      {
        neighbors->InsertNextId(pointIdList->GetId(j));
      }
    }

    pointIdList->Delete();
  }

  cellIdList->Delete();
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeDrawMeshTriangles(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshIndices *indices)
{
  if(vertexBuffer == NULL || normalBuffer == NULL || indices == NULL) 
    return qfeError;

  GLuint     vboIndices;
  GLuint     vboPosition;
  GLuint     vboNormals;

  glGenBuffers(1, &vboPosition);
  glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
  glBufferData(GL_ARRAY_BUFFER, vertexBuffer->size()*sizeof(qfeMeshVertex), &vertexBuffer->front(), GL_STATIC_DRAW);  

  glGenBuffers(1, &vboNormals);
  glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
  glBufferData(GL_ARRAY_BUFFER, normalBuffer->size()*sizeof(qfeMeshVertex), &normalBuffer->front(), GL_STATIC_DRAW);  

  glGenBuffers(1, &vboIndices);  
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices->size()*sizeof(unsigned short), &indices->front(), GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, vboPosition);        
  glEnableVertexAttribArray(ATTRIB_VERTEX);
  glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

  glBindBuffer(GL_ARRAY_BUFFER, vboNormals);        
  glEnableVertexAttribArray(ATTRIB_NORMAL);
  glVertexAttribPointer(ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  
  // Render           
  glDrawElements(GL_TRIANGLES, indices->size(), GL_UNSIGNED_SHORT, BUFFER_OFFSET(0));    

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  
  glDisableVertexAttribArray(ATTRIB_NORMAL); 
  glDisableVertexAttribArray(ATTRIB_VERTEX);  

  glBindBuffer(GL_ARRAY_BUFFER, 0);    

  glDeleteBuffers(1, &vboIndices);
  glDeleteBuffers(1, &vboPosition);
  glDeleteBuffers(1, &vboNormals);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeDrawMeshTriangleStrips(vtkPolyData *mesh, qfeColorRGB color)
{
  if(mesh == NULL) return qfeError;

  vtkCellArray *strips   = mesh->GetStrips();
  vtkPoints    *points   = mesh->GetPoints();
  vtkDataArray *normals  = mesh->GetPointData()->GetNormals();
  vtkDataArray *wss      = mesh->GetPointData()->GetArray("wall_shear");

  if(strips == NULL || points == NULL || normals == NULL)
  {
    cout << "qfeDriverIFR::qfeDrawMesh - Could not read mesh information" << endl;
    return qfeError;
  }

  if(wss != NULL)
    this->wssAvailable = true;
  else
    this->wssAvailable = false;

  vtkIdType nPts = 0;
  vtkIdType *ptIds = strips->GetPointer();

  int index=0;
  for (strips->InitTraversal(); strips->GetNextCell(nPts,ptIds);)
  {
    glBegin(GL_TRIANGLE_STRIP);
    for (int j = 0; j < nPts; j++)
    {
      double v[3], n[3], s;

      s = 140.0;
      if(this->wssAvailable)      
        s = wss->GetTuple1(ptIds[j]);             

      points->GetPoint(ptIds[j], v);
      normals->GetTuple(ptIds[j], n);

      glColor4f(color.r, color.g, color.b, s/140.0);
      glNormal3f(n[0], n[1], n[2]);
      glVertex3f (v[0], v[1], v[2]);
    }
    glEnd();
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeDrawMeshTriangleStrips(qfeMeshStrips *vertexBuffer, qfeMeshStrips *normalBuffer, qfeColorRGB color)
{
  if(vertexBuffer == NULL || normalBuffer == NULL) 
    return qfeError;

  GLint sizeBuffer = 0;
  
  for (int i=0; i<(int)vertexBuffer->size(); i++)
  {
    // Build the buffers
    sizeBuffer = vertexBuffer->at(i).size()*sizeof(qfeMeshVertex);
    
    // Render
    glColor3f(color.r, color.g, color.b);

    //glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);

    //glColorPointer(3, GL_FLOAT, 0, &color[0]);
    glNormalPointer(GL_FLOAT, 0, &normalBuffer->front());  
    glVertexPointer(3, GL_FLOAT, 0, &vertexBuffer->front());  
   
    glDrawArrays(GL_TRIANGLE_STRIP, 0, (int)vertexBuffer->at(i).size());

    glDisableClientState(GL_VERTEX_ARRAY); 
    glDisableClientState(GL_NORMAL_ARRAY); 
    //glDisableClientState(GL_COLOR_ARRAY); 

  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeDrawMeshTriangleStrips(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshStripsIndices *indices)
{
  if(vertexBuffer == NULL || normalBuffer == NULL || indices == NULL) 
    return qfeError;

  GLuint     vboIndices;
  GLuint     vboPosition;
  GLuint     vboNormals;

  glGenBuffers(1, &vboPosition);
  glBindBuffer(GL_ARRAY_BUFFER, vboPosition);
  glBufferData(GL_ARRAY_BUFFER, vertexBuffer->size()*sizeof(qfeMeshVertex), &vertexBuffer->front(), GL_STATIC_DRAW);  

  glGenBuffers(1, &vboNormals);
  glBindBuffer(GL_ARRAY_BUFFER, vboNormals);
  glBufferData(GL_ARRAY_BUFFER, normalBuffer->size()*sizeof(qfeMeshVertex), &normalBuffer->front(), GL_STATIC_DRAW);  

  glGenBuffers(1, &vboIndices);  

  glBindBuffer(GL_ARRAY_BUFFER_ARB, vboPosition);        
  glEnableVertexAttribArray(ATTRIB_VERTEX);
  glVertexAttribPointer(ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

  glBindBuffer(GL_ARRAY_BUFFER_ARB, vboNormals);        
  glEnableVertexAttribArray(ATTRIB_NORMAL);
  glVertexAttribPointer(ATTRIB_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  
  for (int i=0; i<(int)indices->size(); i++)
  {    
    GLushort *indexBuffer = &indices->at(i).front(); 
    GLint     indexSize   =  indices->at(i).size();   

    // Get the indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSize*sizeof(unsigned short), indexBuffer, GL_STATIC_DRAW);

    // Render         
    //glDrawElements(GL_TRIANGLE_STRIP_ADJACENCY, indexSize, GL_UNSIGNED_SHORT, BUFFER_OFFSET(0));
    glDrawElements(GL_TRIANGLE_STRIP, indexSize, GL_UNSIGNED_SHORT, BUFFER_OFFSET(0));    

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  }

  glDisableVertexAttribArray(ATTRIB_NORMAL); 
  glDisableVertexAttribArray(ATTRIB_VERTEX);  

  glBindBuffer(GL_ARRAY_BUFFER, 0);    

  glDeleteBuffers(1, &vboIndices);
  glDeleteBuffers(1, &vboPosition);
  glDeleteBuffers(1, &vboNormals);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeDrawMeshNormals(vtkPolyData *mesh)
{
  vtkPoints    *points   = mesh->GetPoints();
  vtkDataArray *normals  = mesh->GetPointData()->GetNormals();

  double v[3];
  double n[3];

  int nrPoints = points->GetNumberOfPoints();

  glBegin(GL_LINES);

  int index=0;
  for (int i=0; i<nrPoints; i++)
  {
    points->GetPoint(i, v);
    normals->GetTuple(i, n);

    glColor3f(0.0,0.0,0.0);
    glVertex3f (v[0], v[1], v[2]);
    glColor3f(abs(n[0]), abs(n[1]), abs(n[2]));
    glVertex3f (v[0]+n[0], v[1]+n[1], v[2]+n[2]);
  }

  glEnd();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeDrawMeshCurvature(vtkPolyData *mesh)
{
  vtkCellArray *strips   = mesh->GetStrips();
  vtkPoints    *points   = mesh->GetPoints();
  vtkDataArray *normals  = mesh->GetPointData()->GetNormals();
  vtkDataArray *curv_max = mesh->GetPointData()->GetArray("Maximum_Curvature_Vector");
  vtkDataArray *curv_min = mesh->GetPointData()->GetArray("Minimum_Curvature_Vector");
  vtkDataArray *curv_val = mesh->GetPointData()->GetArray("Principal_Curvatures");

  if(strips == NULL || points == NULL || normals == NULL)
  {
    cout << "qfeDriverIFR::qfeDrawMesh - Could not read mesh information" << endl;
    return qfeError;
  }

  if(curv_max == NULL || curv_min == NULL)
  {
    cout << "qfeDriverIFR::qfeDrawMesh - Could not read curvature information" << endl;
    return qfeError;
  }

  double v[3];
  double n[3];
  double pc1[3], pc2[3], pval[2];

  vtkIdType nPts = 0;
  vtkIdType *ptIds = strips->GetPointer();

  int index=0;
  for (strips->InitTraversal(); strips->GetNextCell(nPts,ptIds);)
  {
    glBegin(GL_LINES);
    for (int j = 0; j < nPts; j++)
    {
      points->GetPoint(ptIds[j], v);
      normals->GetTuple(ptIds[j], n);
      curv_max->GetTuple(ptIds[j], pc1);
      curv_min->GetTuple(ptIds[j], pc2);
      curv_val->GetTuple(ptIds[j], pval);

      glColor3f(1.0,0.0,0.0);
      glVertex3f (v[0], v[1], v[2]);
      glVertex3f (v[0]+n[0], v[1]+n[1], v[2]+n[2]);

      glColor3f(0.0,1.0,0.0);
      glVertex3f (v[0], v[1], v[2]);
      glVertex3f (v[0]+pc1[0], v[1]+pc1[1], v[2]+pc1[2]);

      glColor3f(0.0,0.0,1.0);
      glVertex3f (v[0], v[1], v[2]);
      glVertex3f (v[0]+pc2[0], v[1]+pc2[1], v[2]+pc2[2]);
    }
    glEnd();
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeDrawMeshWireframe(vtkPolyData *mesh)
{
  vtkCellArray *strips   = mesh->GetStrips();
  vtkPoints    *points   = mesh->GetPoints();

  if(strips == NULL || points == NULL)
  {
    cout << "qfeDriverIFR::qfeDrawMeshWireframe - Could not read mesh information" << endl;
    return qfeError;
  }

  vtkIdType nPts = 0;
  vtkIdType *ptIds = strips->GetPointer();

  glPolygonMode(GL_BACK,  GL_LINE);
  glPolygonMode(GL_FRONT, GL_LINE);

  int index=0;
  for (strips->InitTraversal(); strips->GetNextCell(nPts,ptIds);)
  {
    double v[3];

    glBegin(GL_TRIANGLE_STRIP);
    for (int j = 0; j < nPts; j++)
    {
      points->GetPoint(ptIds[j], v);

      glColor3f(0.4,0.4,0.4);
      glVertex3f (v[0], v[1], v[2]);
    }
    glEnd();
  }

  glPolygonMode(GL_BACK,  GL_FILL);
  glPolygonMode(GL_FRONT, GL_FILL);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeCreateTextures()
{
  const int     texsizeLight = 256;
  unsigned char texLight[3*texsizeLight];

  glGenTextures(1, &this->texLighting);

  glBindTexture(GL_TEXTURE_1D, this->texLighting);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

  for (int i = 0; i < texsizeLight; i++) {
    float z = float(i + 1 - texsizeLight/2) / (0.5f * texsizeLight);
    int tmp = int(255 * z);
    texLight[i] = min(max(2*(tmp-50), 230), 255);
  }
  glTexImage1D(GL_TEXTURE_1D, 0, 1, texsizeLight, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, texLight);
  glBindTexture(GL_TEXTURE_1D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeDeleteTextures()
{
  // Remove textures and shaders
  if (glIsTexture(this->texLighting))
    glDeleteTextures(1, (GLuint *)&this->texLighting);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeBindTextures()
{
  glActiveTexture(GL_TEXTURE6);
  glBindTexture(GL_TEXTURE_1D, this->texLighting);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeUnbindTextures()
{
  glBindTexture(GL_TEXTURE_1D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeSetStaticUniformLocations()
{
  float         matrix[16];
  qfeMatrix4f::qfeGetMatrixElements(this->v2p, &matrix[0]);

  this->shaderSilhouette->qfeSetUniform1i("lightTex"  , 6);
  this->shaderSilhouette->qfeSetUniformMatrix4f("voxelPatientMatrix", 1, matrix);

  this->shaderCulling->qfeSetUniform1i("lightTex"  , 6);
  this->shaderCulling->qfeSetUniformMatrix4f("voxelPatientMatrix", 1, matrix);

  this->shaderDilate->qfeSetUniformMatrix4f("voxelPatientMatrix", 1, matrix);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSurfaceShading::qfeSetDynamicUniformLocations()
{  
  qfePoint  lightSpec;
  qfeVector lightDir;
  
  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 10.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w); 
    this->light->qfeGetLightSourceDirection(lightDir);
  }  

  this->shaderSilhouette->qfeSetUniform1i("silhouetteShadingMode"  , (int)this->shading);
  this->shaderSilhouette->qfeSetUniform1i("culling"                , this->culling);
  this->shaderSilhouette->qfeSetUniform3f("lightDir"               , lightDir.x, lightDir.y, lightDir.z);
  this->shaderSilhouette->qfeSetUniform4f("lightSpec"              , lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderSilhouette->qfeSetUniform3f("color"                  , this->color.r,this->color.g,this->color.b);
  this->shaderSilhouette->qfeSetUniformMatrix4f("matrixModelViewInverse", 1, this->matrixModelViewInverse); 
  this->shaderSilhouette->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection); 

  this->shaderCulling->qfeSetUniform1i("silhouetteShadingMode"  , (int)this->shading);
  this->shaderCulling->qfeSetUniform1i("culling"                , this->culling);
  this->shaderCulling->qfeSetUniform3f("lightDir"               , lightDir.x, lightDir.y, lightDir.z);
  this->shaderCulling->qfeSetUniform4f("lightSpec"              , lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderCulling->qfeSetUniform3f("color"                  , this->color.r,this->color.g,this->color.b);
  this->shaderCulling->qfeSetUniform1i("wss"                    , (int)this->wssAvailable);
    
  this->shaderFresnel->qfeSetUniform3f("lightDir"               , lightDir.x, lightDir.y, lightDir.z);
  this->shaderFresnel->qfeSetUniform4f("lightSpec"              , lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderFresnel->qfeSetUniform1i("wss"                    , (int)this->wssAvailable);
  this->shaderFresnel->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection); 
  
  this->shaderDilate->qfeSetUniform3f("color"                  , this->color.r,this->color.g,this->color.b);
  this->shaderDilate->qfeSetUniform1f("vertexOffset"           , this->dilation);    
  this->shaderDilate->qfeSetUniformMatrix4f("matrixModelViewProjection", 1, this->matrixModelViewProjection); 
  
  return qfeSuccess;
}


