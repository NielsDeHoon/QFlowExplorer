#pragma once

#include "qflowexplorer.h"

#include <vector>

#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>

#include "qfeAlgorithmFlow.h"
#include "qfeColorMap.h" 
#include "qfeGLShaderProgram.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeTransform.h"
#include "qfeVector.h"
#include "qfeVolume.h"

/*
    switch(i)
    {
    case 0:   this->paramLineColorCurrent = paramRing.color;
              break;
    case 1:   this->paramLineColorCurrent.r =   0.0;  
              this->paramLineColorCurrent.g = 170.0/256.0;  
              this->paramLineColorCurrent.b =   3.0/256.0;              
              break;
    case 2:   this->paramLineColorCurrent.r = 168.0/256.0;  
              this->paramLineColorCurrent.g =   1.0/256.0;  
              this->paramLineColorCurrent.b = 255.0/256.0;
              break;
    case 3:   
    case 4 :  this->paramLineColorCurrent.r =   0.0;  
              this->paramLineColorCurrent.g = 215.0/256.0;  
              this->paramLineColorCurrent.b = 234.0/256.0;
              break;
    default:  this->paramLineColorCurrent = paramRing.color;            
    }
*/

typedef vector< qfePoint >         qfeLine;               // Per seed
typedef vector< qfeLine >          qfeLineSet;            // Per ring
typedef vector< qfeLineSet >       qfeLineSetGroup;       // Per volume
typedef vector< qfeLineSetGroup >  qfeLineSetGroupSeries; // Per series

enum qfeLineType
{
  QFE_VECTOR,
  QFE_STREAMLINE,
  QFE_PATHLINE
};

/**
* \file   qfeFlowLines.h
* \author Roy van Pelt
* \class  qfeFlowLines
* \brief  Implements rendering of velocity vectors
* \note   Confidential
*
* Rendering of streamlines
*
*/
using namespace std;

class qfeFlowLines : public qfeAlgorithmFlow
{
public:
  qfeFlowLines();
  qfeFlowLines(const qfeFlowLines &fv);
  ~qfeFlowLines();

  qfeReturnStatus qfeRenderVectorLines(qfeSeeds seeds, qfeVolume *volume, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderStreamLines(qfeSeeds seeds, qfeVolume *volume, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderPathLines(qfeSeeds seeds, qfeVolumeSeries *series, qfeColorMap *colormap);

  qfeReturnStatus qfeRenderStreamlinesCPU(qfeLineSet streamlines, qfeVolume *volume);    
  qfeReturnStatus qfeRenderPathLinesCPU(qfeLineSet pathlines, qfeVolume *volume);

  qfeReturnStatus qfeRenderLinesGPU(qfeSeeds seeds, qfeVolumeSeries *series, qfeColorMap *colormap, qfeLineType type);

  qfeReturnStatus qfePrecomputeStreamlines(qfeSeedsGroup seeds, qfeVolumeSeries series, qfeFloat traceStep, int traceIterations, qfeLineSetGroupSeries &lines);
  qfeReturnStatus qfePrecomputeStreamlines(qfeSeedsGroup seeds, qfeVolume *volume, qfeFloat traceStep, int traceIterations, qfeLineSetGroup &lines);
  qfeReturnStatus qfePrecomputeStreamlines(qfeSeeds seeds, qfeVolume *volume, qfeFloat traceStep, int traceIterations, qfeLineSet &lines);

  qfeReturnStatus qfePrecomputePathlines(qfeSeedsGroup seeds, qfeVolumeSeries series, qfeFloat traceStep, int traceIterations, qfeFloat phaseDuration, qfeLineSetGroupSeries &lines);
  qfeReturnStatus qfePrecomputePathlines(qfeSeedsGroup seeds, qfeFloat seedTime, qfeVolumeSeries series, qfeFloat traceStep, int traceIterations, qfeFloat phaseDuration, qfeLineSetGroup &lines);
  qfeReturnStatus qfePrecomputePathlines(qfeSeeds seeds, qfeFloat seedTime, qfeVolumeSeries series, qfeFloat traceStep, int traceIterations, qfeFloat phaseDuration, qfeLineSet &lines);

  qfeReturnStatus qfeComputePathline(qfePoint seed, qfeFloat seedTime, qfeVolumeSeries series, qfeFloat step, int iterations, qfeFloat phaseDuration, vector<qfePoint> &pathline);

  qfeReturnStatus qfeSetPhase(double current);
  qfeReturnStatus qfeSetPhaseDuration(double duration);  
  qfeReturnStatus qfeSetLineTrace(int direction, double step, int iterations);
  qfeReturnStatus qfeSetLineSeedTime(double phase);
  qfeReturnStatus qfeSetLineHighlight(int counter);  
  qfeReturnStatus qfeSetLineColorMode(int mode);
  qfeReturnStatus qfeSetLineColor(qfeColorRGB color);
  qfeReturnStatus qfeSetLineShadingMode(int mode);
  qfeReturnStatus qfeSetLineWidth(int width);
  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);  

protected:

  qfeReturnStatus qfeBindTextures(qfeVolumeSeries *series, qfeColorMap *colormap);
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

  qfeReturnStatus qfeComputeStreamline(qfePoint seed, qfeVolume *volume, qfeFloat step, int iterations, vector<qfePoint> &streamline);
  
  qfeReturnStatus qfeDrawLine(qfeLine line);

  qfeReturnStatus qfeComputeRungeKuttaSpatial(qfePoint seed, qfeFloat step, qfeVolume *volume, qfePoint &point);
  qfeReturnStatus qfeComputeRungeKuttaTemporal(qfePoint seed, qfeFloat seedTime, qfeFloat step, qfeFloat phaseDuration, qfeVolumeSeries series, qfePoint &point, qfeFloat &time);

  qfeReturnStatus qfeVolumeSeriesGetVectorAtPositionLinear(qfeVolumeSeries series, qfePoint p, qfeFloat t, qfeVector &v);

private :

  bool                firstRender;
  qfeGLShaderProgram *shaderProgramVectorLines;
  qfeGLShaderProgram *shaderProgramStreamLines;
  qfeGLShaderProgram *shaderProgramPathLines;
  qfeLightSource     *light;

  // Static uniform locations
  qfeMatrix4f  V2P, P2V;
  unsigned int volumeSize[3];
  double       phaseDuration;
  
  // Dynamic uniform locations
  double       phaseCurrent;
  int          paramLineColorMode;
  qfeColorRGB  paramLineColor;
  int          paramLineShading;
  double       paramLineWidth;
  int          paramTraceDirection;
  double       paramTraceStep;
  int          paramTraceIterations;
  int          paramTraceCounter;
  double       paramSeedTime;

};
