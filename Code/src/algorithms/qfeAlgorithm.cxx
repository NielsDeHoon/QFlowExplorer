#include "qfeAlgorithm.h"

//----------------------------------------------------------------------------
qfeAlgorithm::qfeAlgorithm()
{
  GLenum err = glewInit();
  if (GLEW_OK != err)
  {
    cout << "qfeAlgorithm::qfeAlgorithm - " << glewGetErrorString(err) << endl;
  }
};

//----------------------------------------------------------------------------
qfeAlgorithm::qfeAlgorithm(const qfeAlgorithm &af)
{
  for(int i=0; i<16; i++)
  {    
    this->matrixModelView[i]                  = af.matrixModelView[i];
    this->matrixProjection[i]                 = af.matrixProjection[i];
    this->matrixModelViewInverse[i]           = af.matrixModelViewInverse[i];
    this->matrixModelViewProjection[i]        = af.matrixModelViewProjection[i];
    this->matrixModelViewProjectionInverse[i] = af.matrixModelViewProjectionInverse[i];
  }
}

//----------------------------------------------------------------------------
qfeAlgorithm::~qfeAlgorithm()
{

};

//----------------------------------------------------------------------------
qfeReturnStatus qfeAlgorithm::qfeGetModelViewMatrix()
{
  qfeMatrix4f mv, p, mvp, mvinv, mvpinv;

  GLfloat modelview[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f::qfeSetMatrixElements(mv, modelview);

  GLfloat projection[16];
  glGetFloatv(GL_PROJECTION_MATRIX, projection);
  qfeMatrix4f::qfeSetMatrixElements(p, projection);

  mvp    = mv*p;
  qfeMatrix4f::qfeGetMatrixInverse(mvp, mvpinv);
  qfeMatrix4f::qfeGetMatrixInverse(mv, mvinv);

  qfeMatrix4f::qfeGetMatrixElements(p,      this->matrixProjection);
  qfeMatrix4f::qfeGetMatrixElements(mv,     this->matrixModelView);  
  qfeMatrix4f::qfeGetMatrixElements(mvinv,  this->matrixModelViewInverse);
  qfeMatrix4f::qfeGetMatrixElements(mvp,    this->matrixModelViewProjection);
  qfeMatrix4f::qfeGetMatrixElements(mvpinv, this->matrixModelViewProjectionInverse);

  return qfeSuccess;
}




