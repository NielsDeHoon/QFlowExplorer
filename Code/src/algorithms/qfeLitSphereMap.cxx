#include "qfeLitSphereMap.h"
#include <random>
#include <fstream>
#include <vtkSmartPointer.h>
#include <vtkImageData.h>
#include <vtkPointData.h>
#include <direct.h>
#include <QDirIterator>

qfeLitSphereMap::qfeLitSphereMap() {
  nrDots = 512*512;
  lambda = 8.f;
  texture = -1;
}

void qfeLitSphereMap::qfeCreateDotMap() {
  const unsigned width = 512;
  const unsigned height = 512;
  const unsigned nrComp = 4; // Number of components (rgba)
  const unsigned N = width*height*nrComp;
  float *map = new float[N];
  for (unsigned i = 0; i < width*height; i++) {
    map[i*nrComp] = 1.f;
    map[i*nrComp+1] = 1.f;
    map[i*nrComp+2] = 1.f;
    map[i*nrComp+3] = 0.f;
  }

  std::random_device rd; 
  std::exponential_distribution<> exp(lambda);
  std::uniform_real_distribution<> uni;
  std::mt19937 mt(rd());

  unsigned x, y;
  for (unsigned i = 0; i < nrDots; i++) {
    SampleDiskInverseCosine(width, height, x, y, uni(mt), exp(mt));
    unsigned index = (y*width + x) * nrComp;
    map[index] = 0.f;
    map[index + 1] = 0.f;
    map[index + 2] = 0.f;
    map[index + 3] = 1.f;
  }

  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, map);

  qfeSaveToFile(map, width, height, nrComp);

  delete[] map;
}

void qfeLitSphereMap::qfeLoadFromPNG(const std::string &filePath) {
  vtkSmartPointer<vtkPNGReader> reader = vtkPNGReader::New();
  reader->SetFileName(filePath.c_str());
  reader->Update();
  vtkDataArray *dataArray = reader->GetOutput()->GetPointData()->GetArray(0);

  // Assume that lsm files are 512 by 512 pixels
  const unsigned nrPixels = 512*512;
  const unsigned nrComp = 4; // Number of components per pixel
  float *map = new float[nrPixels*nrComp];
  double tuple[nrComp];
  for (int i = 0; i < nrPixels; i++) {
    dataArray->GetTuple(i, tuple);
    map[i*nrComp] = (float)tuple[0] / 255.f;
    map[i*nrComp+1] = (float)tuple[1] / 255.f;
    map[i*nrComp+2] = (float)tuple[2] / 255.f;
    map[i*nrComp+3] = 1.f;
  }

  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, 512, 512, 0, GL_RGBA, GL_FLOAT, map);

  delete[] map;
}

void qfeLitSphereMap::qfeLoadPreset(int preset) {
  // Find current working directory and then lit sphere map directory
  char *cwd = (char *)calloc(_MAX_PATH, sizeof(char));
  _getcwd(cwd, _MAX_PATH);
  std::string scwd(cwd);
  std::string lsmDir = scwd + "\\lsm\\";

  // Find all png files' paths in the lit sphere map directory
  std::vector<std::string> litSphereMapPaths;
  QDirIterator dirIt(lsmDir.c_str(),QDirIterator::Subdirectories);
  while (dirIt.hasNext()) {
    dirIt.next();
    if (QFileInfo(dirIt.filePath()).isFile()) {
      if (QFileInfo(dirIt.filePath()).suffix() == "png") {
        std::string filePath = dirIt.filePath().toUtf8().constData();
        litSphereMapPaths.push_back(filePath);
      }
    }
  }

  if (preset == 0) {
    qfeCreateDotMap();
  } 
  else if(litSphereMapPaths.size()>=preset)
  {
    qfeLoadFromPNG(litSphereMapPaths[preset - 1]);
  }
}

void qfeLitSphereMap::qfeDestroyTexture() {
  if (glIsTexture(texture))
    glDeleteTextures(1, &texture);
}

GLuint qfeLitSphereMap::qfeGetMapTexture() {
  return texture;
}

void qfeLitSphereMap::qfeSetNrDots(unsigned nrDots) {
  if (this->nrDots != nrDots) {
    this->nrDots = nrDots;
    qfeCreateDotMap();
  }
}

void qfeLitSphereMap::qfeSetLambda(float lambda) {
  if (this->lambda != lambda) {
    this->lambda = lambda;
    qfeCreateDotMap();
  }
}

void qfeLitSphereMap::qfeSaveToFile(float *map, unsigned width, unsigned height, unsigned nrComp) {
  std::ofstream file("litSphereMap.ppm");
  file << "P3" << std::endl;
  file << width << " " << height << std::endl;
  file << 255 << std::endl;

  for (unsigned y = 0; y < height; y++) {
    for (unsigned x = 0; x < width; x++) {
      unsigned index = (y*width + x) * nrComp;
      file << (int)(map[index]*255) << '\t'
        << (int)(map[index + 1]*255) << '\t'
        << (int)(map[index + 2]*255);
      if (x != width-1)
        file << "\t\t";
    }
    file << std::endl;
  }
}

float qfeLitSphereMap::Clamp(float v, float min, float max) {
  float r = v;
  if (r < min) r = min;
  if (r > max) r = max;
  return r;
}

float qfeLitSphereMap::GetRandomFloat() {
  return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}

void qfeLitSphereMap::SampleDiskInverseCosine(unsigned width, unsigned height, unsigned &x, unsigned &y, float ru, float re) {
  float r = Clamp(1.f - re, 0.f, 1.f);
  const float PI2 = 6.283181308f;
  float theta = PI2*ru;
  x = ((r*cosf(theta) + 1.f) / 2.f) * width;
  y = ((r*sinf(theta) + 1.f) / 2.f) * height;
}

