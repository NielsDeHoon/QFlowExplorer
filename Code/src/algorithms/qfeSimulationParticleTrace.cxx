#include "qfeSimulationParticleTrace.h"

//----------------------------------------------------------------------------
qfeSimulationParticleTrace::qfeSimulationParticleTrace()
{  
  this->particlePositions.clear();
  this->particleVelocities.clear();  

  this->paramParticlePercentage  = 100;
  this->paramParticleSize        = 2.0;
  this->paramParticleStyle       = qfeFlowParticlesSphere;
  this->paramParticleShading     = qfeFlowParticlesToon;
  this->paramParticleContour     = true;
  
  this->particlesCount           = 0;    
  this->particleTransferFunction = 0;

  this->light                    = NULL;

  this->firstRender              = true;  
  
  this->shaderProgramRenderQuadric = new qfeGLShaderProgram();   
  this->shaderProgramRenderQuadric->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);    
  this->shaderProgramRenderQuadric->qfeAddShaderFromFile("/shaders/fsr/fsr-render-particle-quadric-vert.glsl", QFE_VERTEX_SHADER);    
  this->shaderProgramRenderQuadric->qfeAddShaderFromFile("/shaders/fsr/fsr-render-particle-quadric-geom.glsl", QFE_GEOMETRY_SHADER);      
  this->shaderProgramRenderQuadric->qfeAddShaderFromFile("/shaders/fsr/fsr-render-particle-quadric-frag.glsl", QFE_FRAGMENT_SHADER);      
  this->shaderProgramRenderQuadric->qfeLink();
 
};

//----------------------------------------------------------------------------
qfeSimulationParticleTrace::qfeSimulationParticleTrace(const qfeSimulationParticleTrace &fv) : qfeAlgorithmFlow(fv)
{
  this->firstRender                  = fv.firstRender;
  this->light                        = fv.light;
  this->particlePositions            = fv.particlePositions;
  this->particleVelocities           = fv.particleVelocities;
  this->particlePositionsVBO         = fv.particlePositionsVBO;  
  this->particleVelocitiesVBO        = fv.particleVelocitiesVBO;       

  this->locationPosition             = fv.locationPosition;
  this->locationVelocity             = fv.locationVelocity; 

  this->particlesCount               = fv.particlesCount;  
  this->particleTransferFunction     = fv.particleTransferFunction;

  this->paramParticlePercentage      = fv.paramParticlePercentage;
  this->paramParticleSize            = fv.paramParticleSize;
  this->paramParticleStyle           = fv.paramParticleStyle;
  this->paramParticleShading         = fv.paramParticleShading;
  this->paramParticleContour         = fv.paramParticleContour;
  
  this->shaderProgramRenderQuadric = fv.shaderProgramRenderQuadric;
};

//----------------------------------------------------------------------------
qfeSimulationParticleTrace::~qfeSimulationParticleTrace()
{  
  delete this->shaderProgramRenderQuadric;

  if(glIsTexture(this->particleTransferFunction))  glDeleteTextures(1, &this->particleTransferFunction);
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeSetParticlePercentage(int percentage)
{
  this->paramParticlePercentage = percentage;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeSetParticleSize(qfeFloat size)
{
  this->paramParticleSize = size; 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeSetParticleStyle(qfeFlowParticlesStyle style)
{
  this->paramParticleStyle = style;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeSetParticleShading(qfeFlowParticlesShading shading)
{
  this->paramParticleShading = shading;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeSetParticleContour(bool contour)
{
  this->paramParticleContour = contour;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeSetLightSource(qfeLightSource *light)
{
  this->light = light;
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeClearSeedPositions()
{
  this->particlePositions.clear();  
  this->particleVelocities.clear();
  this->particlesCount = 0;
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeAddSeedPositions(vector< vector<float> > seeds, qfeVolume * volume)
{
  this->qfeAddParticlePositions(seeds, volume);   
 
  this->particlesCount = (int)(seeds.size()); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeAddSeedVelocities(vector< vector<float> > velocities)
{
  this->qfeAddParticleVelocities(velocities);   

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeRenderParticles(qfeColorMap *colormap)
{ 
  // First time of rendering, prepare some global variables  
  if(this->firstRender)
  {    
    this->qfeSetStaticUniformLocations();

    this->firstRender = false;
  }

  // Get the color transferfunction
  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->particleTransferFunction); 
	  
	// OpenGL works with column-major matrices
	// OpenGL works with left-multiplication
  // Obtain the modelview information
  this->qfeGetModelViewMatrix();

  glMatrixMode(GL_MODELVIEW);
 	glPushMatrix();	

  // Bind the textures
  this->qfeBindTextures();
	
	glPointSize( 5.0 );
	glColor3f(1.0f, 0.0f, 0.0f);

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocations();  

  this->shaderProgramRenderQuadric->qfeGetAttribLocation("qfe_ParticlePosition", this->locationPosition);
  this->shaderProgramRenderQuadric->qfeGetAttribLocation("qfe_ParticleVelocity", this->locationVelocity);

  this->shaderProgramRenderQuadric->qfeEnable();

  this->qfeDrawParticles(this->particlePositionsVBO, this->particleVelocitiesVBO, this->particlesCount);

  this->shaderProgramRenderQuadric->qfeDisable();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeDrawParticles(GLuint particles, GLuint velocities, int count)
{  
  if(!glIsBuffer(particles)) return qfeError;
  if((this->locationPosition == -1)) return qfeError;

  glBindBuffer(GL_ARRAY_BUFFER, particles);  
  glEnableVertexAttribArray(this->locationPosition);  
  glVertexAttribPointer(this->locationPosition, 4, GL_FLOAT, GL_FALSE, 0, 0);
  
  if(glIsBuffer(velocities) && (this->locationVelocity != -1))
  {
    glBindBuffer(GL_ARRAY_BUFFER, velocities);
    glEnableVertexAttribArray(this->locationVelocity);
    glVertexAttribPointer(this->locationVelocity,  3, GL_FLOAT, GL_FALSE, 0, 0);
  }
  
  glDrawArrays(GL_POINTS, 0, count);

  glBindBuffer(GL_ARRAY_BUFFER, 0);  
  glDisableVertexAttribArray(this->locationPosition);
  if(glIsBuffer(velocities) && (this->locationVelocity != -1))
    glDisableVertexAttribArray(this->locationVelocity);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// seeds should be provided in texture coordinates, 
// and will be stored in patient coordinates
qfeReturnStatus qfeSimulationParticleTrace::qfeAddParticlePositions(vector< vector<float> > seeds, qfeVolume *volume)
{
  if((volume == NULL) || (int(seeds.size()) <= 0)) 
    return qfeError;

  qfePoint      currentPosition;
  qfeMatrix4f   V2P, T2V, T2P;
	qfeTransform::qfeGetMatrixVoxelToPatient(V2P, volume);
	qfeTransform::qfeGetMatrixTextureToVoxel(T2V, volume);    
  T2P = T2V * V2P;

  this->particlePositions.clear();
  
  for(int i=0; i<(int)seeds.size(); i++)
  { 
    // we assume x, y, z in texture coordinates and time in phases
    if((int)seeds[i].size() == 4)
    {
      currentPosition = qfePoint(seeds[i][0],seeds[i][1],seeds[i][2]) * T2P;
      this->particlePositions.push_back(currentPosition.x);
      this->particlePositions.push_back(currentPosition.y);
      this->particlePositions.push_back(currentPosition.z);
      this->particlePositions.push_back(seeds[i][3]);
    }
  }

  this->qfeInitParticleBuffers();
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeAddParticleVelocities(vector< vector<float> > velocities)
{
  if(int(velocities.size()) <= 0) return qfeError;

  this->particleVelocities.clear();
  
  for(int i=0; i<(int)velocities.size(); i++)
  { 
    for(int j=0; j<(int)velocities[i].size(); j++)
      this->particleVelocities.push_back(velocities[i][j]);    
  }

  this->qfeInitParticleBuffers();
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeInitParticleBuffers()
{
  int dataSizeP, dataSizeV;

  if((int)this->particlePositions.size() <= 0) return qfeError;

  this->qfeClearParticleBuffers();

  dataSizeP = this->particlePositions.size()  * sizeof(GLfloat);
  dataSizeV = this->particleVelocities.size() * sizeof(GLfloat);
  
  glGenBuffers(1, &this->particlePositionsVBO);       
  
  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->particlePositionsVBO);    
  glBufferData(GL_ARRAY_BUFFER, dataSizeP, &this->particlePositions.front(), GL_STREAM_COPY); 
  glBindBuffer(GL_ARRAY_BUFFER, 0);  

  if((int)(this->particleVelocities.size()/3.0) == (int)this->particlePositions.size()/4.0)
  {
    glGenBuffers(1, &this->particleVelocitiesVBO);    

    glBindBuffer(GL_ARRAY_BUFFER, this->particleVelocitiesVBO);    
    glBufferData(GL_ARRAY_BUFFER, dataSizeV, &this->particleVelocities.front(), GL_STREAM_COPY); 
    glBindBuffer(GL_ARRAY_BUFFER, 0);      
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeClearParticleBuffers()
{
  if(glIsBuffer(this->particlePositionsVBO))
  {    
    glDeleteBuffers(1, &this->particlePositionsVBO);    
  }
  if(glIsBuffer(this->particleVelocitiesVBO))
  {
    glDeleteBuffers(1, &this->particleVelocitiesVBO);    
  }
  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeBindTextures()
{
  glActiveTexture(GL_TEXTURE8);
  glBindTexture(GL_TEXTURE_1D, this->particleTransferFunction);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeSetStaticUniformLocations()
{   
  this->shaderProgramRenderQuadric->qfeSetUniform1i("particleTransferFunction", 8);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeSimulationParticleTrace::qfeSetDynamicUniformLocations()
{   
  qfePoint  lightSpec;
  qfeVector lightDir;

  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 10.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w); 
    this->light->qfeGetLightSourceDirection(lightDir);
  }

  this->shaderProgramRenderQuadric->qfeSetUniform4f("light",                                    lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderQuadric->qfeSetUniform4f("lightDirection",                           lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramRenderQuadric->qfeSetUniform1f("particleSize",                             this->paramParticleSize);
  this->shaderProgramRenderQuadric->qfeSetUniform1i("particleShading",                          (int)this->paramParticleShading);  
  this->shaderProgramRenderQuadric->qfeSetUniform1i("particleContour",                          (int)this->paramParticleContour);    
  this->shaderProgramRenderQuadric->qfeSetUniform1i("particleStyle",                            (int)this->paramParticleStyle);    
  this->shaderProgramRenderQuadric->qfeSetUniform1f("particlePercentage",                       (int)this->paramParticlePercentage/100.0f);    
  this->shaderProgramRenderQuadric->qfeSetUniformMatrix4f("matrixModelView",                     1, this->matrixModelView);  
  this->shaderProgramRenderQuadric->qfeSetUniformMatrix4f("matrixModelViewProjection",           1, this->matrixModelViewProjection);
  this->shaderProgramRenderQuadric->qfeSetUniformMatrix4f("matrixModelViewProjectionInverse",    1, this->matrixModelViewProjectionInverse);          

  return qfeSuccess;
}

