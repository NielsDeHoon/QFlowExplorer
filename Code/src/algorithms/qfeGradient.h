#pragma once

#include "qfeAlgorithm.h"
#include "qfeVolume.h"

/**
* \file   qfeGradient.h
* \author Arjan Broos
* \class  qfeGradient
* \brief  Algorithm that generates the gradients for a given volume series
* \note   Confidential
*
* According to the following paper:
* Gradient Estimation in Volume Data using 4D Linear Regression
*
*/
class qfeGradient : public qfeAlgorithm {
public:
  qfeGradient();

  void qfeSetData(qfeVolumeSeries *magnitudeData);
  void qfeGetGradientTensors(qfeVolume *volume, qfeTensor3f *gradients);
  float **qfeCalculateGradients();
  float qfeGetMaxGradientLength();

private:
  qfeVolumeSeries *magnitudeData;

  unsigned size;
  unsigned width, height, depth;
  unsigned nrFrames;
  
  float sx, sy, sz; // Spacing between voxel grid points
  float wA, wB, wC, wD; // Weights for gradient calculation
  float wk[27]; // Weighting function for gradient calculation

  float maxGradLength;

  void CalculateWk();
  float GetWk(int x, int y, int z);
  void GetWeights(float &wA, float &wB, float &wC, float &wD);
  void GetGradient(float *magnitudes, int nrComp, int x, int y, int z, float &A, float &B, float &C, float &D);
  void GetGradient(unsigned short *magnitudes, int nrComp, int x, int y, int z, float &A, float &B, float &C, float &D);
  void GetGradient(float *values, int x, int y, int z, float &A, float &B, float &C, float &D);
  int GetIndex(int x, int y, int z);
};