#include "qfeHierarchicalClustering.h"

//----------------------------------------------------------------------------
qfeHierarchicalClustering::qfeHierarchicalClustering()
{
  this->clusterDefinitions = NULL;
  this->clusterDistances   = NULL;
  this->clusterCount       = 0;
};

//----------------------------------------------------------------------------
qfeHierarchicalClustering::~qfeHierarchicalClustering()
{
  if(this->clusterDefinitions != NULL)
    delete[] this->clusterDefinitions;

  if(this->clusterDistances != NULL)
    delete[] this->clusterDistances;
};

//----------------------------------------------------------------------------
bool qfeVoxelSortPredicate(qfePoint i, qfePoint j)
{
  if(i.w < j.w) return true;
  else return false;
}

//----------------------------------------------------------------------------
bool qfeVoxelUniquePredicate(qfePoint i, qfePoint j)
{
  if(int(i.w+0.5f) == int(j.w+0.5f)) return true;
  else return false;
}

//----------------------------------------------------------------------------
// TODO: Move this to qfeClusterHierarchy???
qfeReturnStatus qfeHierarchicalClustering::qfeGetInitialClusterLabels(qfeClusterHierarchy *hierarchy, qfeVolumeSeries *clusters)
{
  qfeValueType t;
  unsigned int nx,ny,nz;
  void *data;

  clusters->front()->qfeGetVolumeData(t, nx, ny, nz, &data);
  if(t != qfeValueTypeFloat)
  {
    cout << "qfeHierarchicalClustering::qfeGetInitialClusterLabels - Encountered invalid value type (need float)" << endl;
    return qfeError;
  }
  unsigned int leafCountPerPhase = hierarchy->leaves.size();
  unsigned int phaseCount = clusters->size();

  // Define the number of unique cluster labels
  int labelCount = 50;

  for(unsigned int phaseIndex=0; phaseIndex<phaseCount; phaseIndex++)
  {
    // Load individual cluster label volumes
    clusters->at(phaseIndex)->qfeSetVolumeValueDomain(0.0, 1.0);
    clusters->at(phaseIndex)->qfeGetVolumeData(t, nx, ny, nz, &data);

    // Fill individual cluster label volumes with leaf indices
    for(unsigned int leafIndex=0; leafIndex<leafCountPerPhase; leafIndex++)
    {
      unsigned int volumeIndex = hierarchy->leaves[leafIndex];
      unsigned int clusterIndex = phaseIndex*leafCountPerPhase + leafIndex;

      // Assign a random label coverted to float (0,1]
      qfeFloat clusterLabel = (qfeFloat)(rand()%(labelCount-1)+1)/labelCount;
      ((qfeFloat*)data)[volumeIndex] = clusterLabel;
    }
    clusters->at(phaseIndex)->qfeUpdateVolume();
  }
  
  return qfeSuccess;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeGetClusterLabels(qfeClusterHierarchy *hierarchy, qfeVolumeSeries *clusters, int level)
{
  this->qfeGetInitialClusterLabels(hierarchy, clusters);

  int phaseCount = clusters->size();
  int leafCountPerPhase = hierarchy->leaves.size();
  int totalLeafCount = leafCountPerPhase * phaseCount;
  int totalClusterCount = totalLeafCount + level;

  qfeValueType t;
  unsigned int nx,ny,nz;
  void *data;
  qfeFloat** dataPointers = new qfeFloat*[phaseCount];

  // Retrieve data pointers
  for(int i=0; i<phaseCount; i++)
  {
    clusters->at(i)->qfeGetVolumeData(t, nx, ny, nz, &data);
    dataPointers[i] = (qfeFloat*)data;
  }
  int voxelCountPerPhase = nx*ny*nz;

  // TODO: why does this take so long?
  if(this->clusterDefinitions != NULL)
    delete[] this->clusterDefinitions;

  this->clusterCount       = totalClusterCount;
  this->clusterDefinitions = new list<int>[totalClusterCount];
  list<int>::iterator it;  

  // Initialize leaf definitions
  for(int i=0; i<totalLeafCount; i++)
    this->clusterDefinitions[i].push_back(hierarchy->leaves[i%leafCountPerPhase] + (i/leafCountPerPhase)*voxelCountPerPhase);

  int c1,c2;
  int phaseIndex,voxelIndex;
  qfeFloat newLabel;

  // Merge cluster definitions until the specified hierarchy level is reached
  for(int i=totalLeafCount; i<totalClusterCount; i++)
  {
    // Get merged cluster labels
    c1 = hierarchy->tree[i-totalLeafCount].c1;
    c2 = hierarchy->tree[i-totalLeafCount].c2;
    
    // Merge definitions
    if(this->clusterDefinitions[c1].size() > this->clusterDefinitions[c2].size())
    {
      this->clusterDefinitions[i].swap(this->clusterDefinitions[c1]);
      this->clusterDefinitions[i].splice(this->clusterDefinitions[i].begin(),this->clusterDefinitions[c2]);
    }
    else
    {
      this->clusterDefinitions[i].swap(this->clusterDefinitions[c2]);
      this->clusterDefinitions[i].splice(this->clusterDefinitions[i].begin(),this->clusterDefinitions[c1]);
    }

    if((i%(int)(totalClusterCount/100.0)) == 0)
      cout << "\rProcessed " << i << "/" << totalClusterCount << " cluster voxel labels";
  }
  cout << "\rProcessed " << totalClusterCount << "/" << totalClusterCount << " cluster voxel labels" << endl;

  // Update labels in the cluster label volume
  for(int i=totalLeafCount; i<totalClusterCount; i++)
  {
    for(it=this->clusterDefinitions[i].begin(); it!=this->clusterDefinitions[i].end(); it++)
    {
      phaseIndex  = (*it)/voxelCountPerPhase;
      voxelIndex = (*it)%voxelCountPerPhase;

      if(it == this->clusterDefinitions[i].begin())   // Define merged cluster label
        newLabel = dataPointers[phaseIndex][voxelIndex];
      else
        dataPointers[phaseIndex][voxelIndex] = newLabel;
    }
  }

  //delete[] clusterDefinitions;
  delete[] dataPointers;

  // Refresh the updated cluster label volumes on the GPU
  for(int i=0; i<phaseCount; i++)
    clusters->at(i)->qfeUpdateVolume();

  return qfeSuccess;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeGetClusterVoxelPositions(qfeClusterHierarchy *hierarchy, qfeVolume *boundingVolume, int phaseCount, int level, qfeVoxelSeries &clusters)
{ 
  int leafCountPerPhase = hierarchy->leaves.size();
  int totalLeafCount    = leafCountPerPhase * phaseCount;
  //int totalClusterCount = totalLeafCount + level;
  
  qfeValueType t;
  unsigned int nx,ny,nz;
  void *data;

  // Check if the labels are generated on this level
  if((this->clusterDefinitions == NULL) || (this->clusterCount <= 0))
  {
    cout << "Generate cluster labels first" << endl;
    return qfeError;
  }

  // Obtain the volume dimensions
  boundingVolume->qfeGetVolumeData(t, nx, ny, nz, &data);

  int voxelCountPerPhase = nx*ny*nz;

  int phaseIndex,voxelIndex; 
  list<int>::iterator it;
 
  // Compute the list of positions per cluster
  for(int i=totalLeafCount; i<this->clusterCount; i++)
  {
    qfeVoxelArray clusterVoxels;

    for(it=this->clusterDefinitions[i].begin(); it!=this->clusterDefinitions[i].end(); it++)
    {
      qfePoint voxel;

      phaseIndex = (*it)/voxelCountPerPhase;
      voxelIndex = (*it)%voxelCountPerPhase;

      this->qfeGetVoxelCoord(boundingVolume, voxelIndex, voxel);

      voxel.qfeSetPointElements(voxel.x, voxel.y, voxel.z, phaseIndex);    

      clusterVoxels.push_back(voxel);
    }   

    if((int)clusterVoxels.size() > 0) clusters.push_back(clusterVoxels);

    if((i%(int)(this->clusterCount/100.0)) == 0)
      cout << "\rProcessed " << i << "/" << this->clusterCount << " cluster voxel lists";
  }

  cout << "\rProcessed " << this->clusterCount << "/" << this->clusterCount << " cluster voxel lists" << endl;

  return qfeSuccess;
};

//----------------------------------------------------------------------------
// clusters:  vector with all voxel positions per cluster
// type:      the type of center computation
// samples:   for stochastic centers - amount of samples
// centers:   vector with center positions of the clusters
qfeReturnStatus qfeHierarchicalClustering::qfeGetClusterCenterPositions(qfeVoxelSeries *clusters, qfeClusterCentersType type, int samples, qfeVoxelArray &centers)
{
  qfePoint p;
  unsigned int t;
   
  switch(type)
  {
  case qfeCenterOfGravity4D : 
    for(int i=0; i<(int)clusters->size(); i++)
    {
      this->qfeGetCenterOfGravity(&(*clusters)[i], p, t);
      p.qfeSetPointElements(p.x, p.y, p.z, t);
      centers.push_back(p);
      if((i%(int)std::max((clusters->size()/100.0f),1.0f)) == 0)
        cout << "\rProcessed " << i << "/" << (int)clusters->size() << " cluster centers";
    }
    break;
  case qfeCenterStochastic3D :
    for(int i=0; i<(int)clusters->size(); i++)
    {
      qfeVoxelArray voxelPhases;
      qfeVoxelArray::iterator it;

      // sort based on the current phase, and find the amount of unique phases in this cluster      
      voxelPhases = (*clusters)[i];
      std::sort(voxelPhases.begin(), voxelPhases.end(), qfeVoxelSortPredicate);
      it=unique_copy (voxelPhases.begin(), voxelPhases.end(), voxelPhases.begin(), qfeVoxelUniquePredicate);                                              
      voxelPhases.resize( it - voxelPhases.begin() );  

      // add a center for the cluster at each time point
      for(int j=int(voxelPhases.front().w+0.5f); j<=int(voxelPhases.back().w+0.5f); j++)
      {
        this->qfeGetCenterStochastic3D(&(*clusters)[i], j, samples, p);
        p.qfeSetPointElements(p.x, p.y, p.z, j);
        centers.push_back(p);
      }
      if((i%(int)std::max((clusters->size()/100.0f),1.0f)) == 0)
        cout << "\rProcessed " << i << "/" << (int)clusters->size() << " cluster centers";
    }
    break;
  case qfeCenterStochastic4D :
    for(int i=0; i<(int)clusters->size(); i++)
    {     
      this->qfeGetCenterStochastic4D(&(*clusters)[i], samples, p, t);
      p.qfeSetPointElements(p.x, p.y, p.z, t);
      centers.push_back(p);
      
      if( (i%(int)std::max((clusters->size()/100.0f),1.0f)) == 0)
        cout << "\rProcessed " << i << "/" << (int)clusters->size() << " cluster centers";
    }
    break;
  default:
    return qfeError;
  }

  cout << "\rProcessed " << (int)clusters->size() << "/" << (int)clusters->size() << " cluster centers" << endl;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeRenderClusterCenters(qfeVolume *boundingVolume, qfeVoxelArray *centers, int phase)
{
  qfeMatrix4f      V2P;
  qfePoint         p;

  // Get transformation matrices
  qfeTransform::qfeGetMatrixVoxelToPatient(V2P, boundingVolume);

  // Initialize GL state
  glEnable(GL_POINT_SMOOTH);
  glPointSize(4.0);

  // Render the polygon
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Render in patient coordinates
  glColor3f(1.0,0.0,0.0);
  glBegin(GL_POINTS);

  for(int i=0; i<(int)centers->size(); i++)
  {
    if(phase == (*centers)[i].w)
    {
      p = (*centers)[i] * V2P;
      glVertex3f(p.x, p.y, p.z);   
    }
  }

  glEnd();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeGetVoxelCoord(qfeVolume *boundingVolume, unsigned int voxelIndex, qfePoint &voxelCoord)
{  
  unsigned int coordX, coordY, coordZ;

  qfeValueType t;
  unsigned int nx,ny,nz;
  void *data;

  // Obtain the volume dimensions
  boundingVolume->qfeGetVolumeData(t, nx, ny, nz, &data);

  // Compute the coordinate
  coordX = voxelIndex % nx;
  coordY = voxelIndex / nx % ny;
  coordZ = voxelIndex / nx / ny % nz;

  voxelCoord.qfeSetPointElements(coordX, coordY, coordZ);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeGetCenterOfGravity(qfeVoxelArray *voxels, qfePoint &center, unsigned int &phase)
{
  qfeFloat factor;
  qfeFloat time;

  factor = 1.0f/(qfeFloat)voxels->size();

  center.qfeSetPointElements(0.0,0.0,0.0);
  time   = 0.0f;  
  
  for(int i=0; i<(int)voxels->size(); i++)
  {
    qfePoint p;
    qfeFloat t;

    p = (*voxels)[i];
    t = p.w;
    p.qfeSetPointElements(factor*p.x, factor*p.y, factor*p.z, 1.0);

    center = center + p;
    time   = time   + factor*t;
  }

  phase = (unsigned int)time;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeGetCenterStochastic3D(qfeVoxelArray *voxels, unsigned int phase, int sampleCount, qfePoint &center)
{
  qfeVoxelArray    randomSamples;  
  vector<qfeFloat> distanceToRandomPoints;

  // select a number of random samples
  for(int i=0; i<min((int)voxels->size(),sampleCount); i++)
  {
    randomSamples.push_back((*voxels)[rand()%(int)voxels->size()]);
  }

  // calculate the total distance (L2-Norm) from all cluster voxels to the random samples
  for(int i=0; i<(int)randomSamples.size(); i++)
  {
    if(randomSamples[i].w == phase)
    {
      qfeFloat norm, total;   
      total = 0;
         
      for(int j=0; j<(int)voxels->size(); j++)
      {
        qfeVector::qfeVectorLength((*voxels)[j] - randomSamples[i], norm);
        total += norm;
      }   

      distanceToRandomPoints.push_back(total);
    }
  }

  // return the cluster voxel with the smallest distance to the random samples 
  int                        minIndex;
  vector<qfeFloat>::iterator minPos;
  minPos   = min_element (distanceToRandomPoints.begin(), distanceToRandomPoints.end());
  minIndex = minPos - distanceToRandomPoints.begin();

  // return the center and phase
  center.qfeSetPointElements(randomSamples[minIndex].x, randomSamples[minIndex].y, randomSamples[minIndex].z);
  //phase = randomSamples[minIndex].w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeGetCenterStochastic4D(qfeVoxelArray *voxels, int sampleCount, qfePoint &center, unsigned int &phase)
{
  qfeVoxelArray    randomSamples;  
  qfeVoxelArray    voxelPhases;
  vector<qfeFloat> distanceToRandomPoints;

  // select a number of random samples
  for(int i=0; i<min((int)voxels->size(),sampleCount); i++)
  {
    randomSamples.push_back((*voxels)[rand()%(int)voxels->size()]);
    voxelPhases.push_back(randomSamples[i]);
  }

  // sort based on the current phase, and find the amount of unique phases in this cluster      
  qfeVoxelArray::iterator it;      
  std::sort(voxelPhases.begin(), voxelPhases.end(), qfeVoxelSortPredicate);
  it=unique_copy (voxelPhases.begin(), voxelPhases.end(), voxelPhases.begin(), qfeVoxelUniquePredicate);                                              
  voxelPhases.resize( it - voxelPhases.begin() );  

  // take the median in time, determining the phase for this cluster
  // note that there is a slight bias towards earlier phases for odd sized arrays
  phase = int(voxelPhases[int(voxelPhases.size()/2.0f)].w+0.5f);

  // calculate the total distance (L2-Norm) from all cluster voxels to the random samples
  for(int i=0; i<(int)randomSamples.size(); i++)
  {
    if(int(randomSamples[i].w+0.5f) == phase)
    {
      qfeFloat norm, total;   
      total = 0;
         
      for(int j=0; j<(int)voxels->size(); j++)
      {
        qfeVector::qfeVectorLength((*voxels)[j] - randomSamples[i], norm);
        total += norm;
      }   

      distanceToRandomPoints.push_back(total);
    }
  }

  // return the cluster voxel with the smallest distance to the random samples 
  int                        minIndex;
  vector<qfeFloat>::iterator minPos;
  minPos   = min_element (distanceToRandomPoints.begin(), distanceToRandomPoints.end());
  minIndex = minPos - distanceToRandomPoints.begin();

  // return the center and phase
  center.qfeSetPointElements(randomSamples[minIndex].x, randomSamples[minIndex].y, randomSamples[minIndex].z);
  phase = randomSamples[minIndex].w;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeSelectLeavesByPercentage(qfeClusterHierarchy *hierarchy, qfeVolume *volume,\
                                                                       double percentage)
{
  if(volume==NULL || hierarchy==NULL) return qfeError;

  unsigned int nx, ny, nz = 0;
  volume->qfeGetVolumeSize(nx, ny, nz);

  // Get histogram of the specified volume
  int   size      = 0;
  int  *histogram = NULL;
  volume->qfeGetVolumeHistogram(&histogram, size);

  // If our histogram is invalid, simply select all voxels as leaves
  if(histogram == NULL)
  {
    cout << "Invalid histogram -> selecting all voxels" << endl;
    qfeSelectLeavesByVolume(hierarchy, volume);
    return qfeSuccess;
  }

  int threshold = size;
  double voxelCount = 0;
  double totalVoxelCount = nx*ny*nz;

  // Find the lower velocity bound such that the selected volume ratio (just) exceeds the specified ratio
  while(voxelCount/totalVoxelCount*100 < percentage && threshold >= 0)
  {
    threshold--;
    voxelCount += histogram[threshold];
  }
  cout << "Selected " << voxelCount/totalVoxelCount*100 << "%, using threshold = " << threshold << endl;

  this->qfeSelectLeavesByThreshold(hierarchy, volume, (double)threshold);
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeSelectLeavesByThreshold(qfeClusterHierarchy *hierarchy, qfeVolume *volume,\
                                                                      double threshold)
{
  if(volume==NULL || hierarchy==NULL) return qfeError;

  // Get volume data
  qfeValueType  t;
  unsigned int  dims[3];
  void         *data;
  volume->qfeGetVolumeData(t,dims[0],dims[1],dims[2],&data);

  unsigned int nc = 0;
  volume->qfeGetVolumeComponentsPerVoxel(nc);

  unsigned int totalVoxelCount = dims[0]*dims[1]*dims[2];

  // Store volume dimensions
  for(int i=0; i<3; i++) hierarchy->dims[i]=dims[i];

  // Select voxels and neighbours based on minimum speed
  hierarchy->leaves.clear();
  hierarchy->tree.clear();
  double magnitude;
  for(unsigned int voxelIndex=0; voxelIndex<totalVoxelCount; voxelIndex++)
  {
    if(nc == 1 && t==qfeValueTypeUnsignedInt16)  // t-mip
    {
      magnitude = ((qfeUnsignedInt16*)data)[voxelIndex];
    }
    else if(nc == 3 && t==qfeValueTypeFloat)     // pca-p
    {
      qfeFloat *flow = ((qfeFloat*)data)+3*voxelIndex;
      magnitude = sqrt(flow[0]*flow[0] + flow[1]*flow[1] + flow[2]*flow[2]);
    }
    else
    {
      cout << "Invalid combination of value type / number of components detected!" << endl;
      break;
    }
    if(magnitude>=threshold) 
      hierarchy->leaves.push_back(voxelIndex);
  }
 
  cout << "Selected " << hierarchy->leaves.size() << " voxels (" << 100*hierarchy->leaves.size()/totalVoxelCount << "%)" << endl;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeSelectLeavesByVolume(qfeClusterHierarchy *hierarchy, qfeVolume *volume)
{
  if(volume==NULL || hierarchy==NULL) return qfeError;

  // Get volume data
  qfeValueType  t;
  unsigned int  dims[3];
  void         *data;
  volume->qfeGetVolumeData(t,dims[0],dims[1],dims[2],&data);

  unsigned int totalVoxelCount = dims[0]*dims[1]*dims[2];

  // Store volume dimensions
  for(int i=0; i<3; i++) hierarchy->dims[i]=dims[i];

  // Select voxels and neighbours based on z-index
  hierarchy->leaves.clear();
  hierarchy->tree.clear();
  for(unsigned int voxelIndex=0; voxelIndex<totalVoxelCount; voxelIndex++)
  {
    hierarchy->leaves.push_back(voxelIndex);
  }

  cout << "Selected " << hierarchy->leaves.size() << " voxels (" << 100*hierarchy->leaves.size()/totalVoxelCount << "%)" << endl;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeSelectLeavesByZSlice(qfeClusterHierarchy *hierarchy, qfeVolume *volume, \
                                                                   unsigned int zSliceIndex)
{
  if(volume==NULL || hierarchy==NULL) return qfeError;

  // Get volume data
  qfeValueType  t;
  unsigned int  dims[3];
  void         *data;
  volume->qfeGetVolumeData(t,dims[0],dims[1],dims[2],&data);

  unsigned int totalVoxelCount = dims[0]*dims[1]*dims[2];

  // Store volume dimensions
  for(int i=0; i<3; i++) hierarchy->dims[i]=dims[i];
  
  // Select voxels and neighbours based on z-index
  hierarchy->leaves.clear();
  hierarchy->tree.clear();
  for(unsigned int voxelIndex=0; voxelIndex<totalVoxelCount; voxelIndex++)
  {
    if(voxelIndex / dims[0] / dims[1] % dims[2] == zSliceIndex)
      hierarchy->leaves.push_back(voxelIndex);
  }

  cout << "Selected " << hierarchy->leaves.size() << " voxels (" << 100*hierarchy->leaves.size()/totalVoxelCount << "%)" << endl;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeInitializeLeafNodes(qfeClusterHierarchy *hierarchy, qfeVolumeSeries volumes)
{
  if(hierarchy==NULL) return qfeError;

  time_t start = clock();

  ////////////////////////////////////////////////////
  //
  //    Define leaf neighbours
  //
  ////////////////////////////////////////////////////

  unsigned int dims[3];
  for(int i=0;i<3;i++) dims[i]=hierarchy->dims[i];

  unsigned int leafCountPerPhase = hierarchy->leaves.size();
  unsigned int voxelCountPerPhase = dims[0]*dims[1]*dims[2];

  vector<vector<unsigned int>> leafNeighbours = vector<vector<unsigned int>>(leafCountPerPhase);

  int x, y, z, voxelIndex;
  int leafNeighbourLabel;
  
  // Initialize all leaf levels (invalid by default: -1)
  int *leafLabels = (int*) malloc (voxelCountPerPhase*sizeof(int));
  for(unsigned int i=0; i<voxelCountPerPhase; i++) leafLabels[i] = -1;

  for(int currentLeafLabel=0; currentLeafLabel<(int)leafCountPerPhase; currentLeafLabel++)
  {
    voxelIndex = hierarchy->leaves[currentLeafLabel];
    leafLabels[voxelIndex] = currentLeafLabel;
    
    x = voxelIndex % dims[0];
    y = voxelIndex / dims[0] % dims[1];
    z = voxelIndex / dims[0] / dims[1];

    if(z>0)
    {
      leafNeighbourLabel = leafLabels[voxelIndex-dims[0]*dims[1]];
      if(leafNeighbourLabel>=0)
      {
        leafNeighbours[currentLeafLabel].push_back(leafNeighbourLabel);
        leafNeighbours[leafNeighbourLabel].push_back(currentLeafLabel);
      }
    }
    if(y>0)
    {
      leafNeighbourLabel = leafLabels[voxelIndex-dims[0]];
      if(leafNeighbourLabel>=0)
      {
        leafNeighbours[currentLeafLabel].push_back(leafNeighbourLabel);
        leafNeighbours[leafNeighbourLabel].push_back(currentLeafLabel);
      }
    }
    if(x>0)
    {
      leafNeighbourLabel = leafLabels[voxelIndex-1];
      if(leafNeighbourLabel>=0)
      {
        leafNeighbours[currentLeafLabel].push_back(leafNeighbourLabel);
        leafNeighbours[leafNeighbourLabel].push_back(currentLeafLabel);
      }
    }
  }

  ////////////////////////////////////////////////////
  //
  //    Initialize leaf nodes
  //
  ////////////////////////////////////////////////////

  unsigned int phaseCount = volumes.size();
  hierarchy->phaseCount = phaseCount;
  if(phaseCount == 0) return qfeError;
  

  // Retrieve volume data

  qfeValueType    type;
  unsigned int    sourceDims[3];
  void           *data;
  qfeFloat      **dataSeries = new qfeFloat*[phaseCount];
  bool            consistentDimensions = true;

  for(unsigned int phase=0; phase<phaseCount; phase++)
  {
    volumes[phase]->qfeGetVolumeData(type, sourceDims[0], sourceDims[1], sourceDims[2], &data);
    dataSeries[phase] = (qfeFloat*)data;
    for(int d=0; d<3; d++) consistentDimensions &= dims[d]==sourceDims[d];
  }

  if(consistentDimensions == false)
  {
    cout << "qfeHierarchicalClustering::qfeInitializeLeafNodes - Inconsistent data dimensions detected!" << endl;
    return qfeError;
  }

  // Get patient coordinate information:

  qfeGrid *grid;
  qfeFloat m[16];
  qfeFloat ex, ey, ez;

  volumes.front()->qfeGetVolumeGrid(&grid);
  grid->qfeGetGridExtent(ex,ey,ez);

  grid->qfeGetGridAxes(   m[ 0], m[ 1], m[ 2],\
                          m[ 4], m[ 5], m[ 6],\
                          m[ 8], m[ 9], m[10]);
  grid->qfeGetGridOrigin( m[12], m[13], m[14]);

  m[ 0] *= ex;
  m[ 5] *= ey;
  m[10] *= ez;
  m[ 3]=0;
  m[ 7]=0;
  m[11]=0;
  m[15]=1;

  qfeMatrix4f voxelTransform = qfeMatrix4f(m);

  // Construct all leaf node pointers
  qfeClusterNode **leaves = new qfeClusterNode*[leafCountPerPhase * phaseCount];


  ////////////////////////////////////////////////////
  //
  //    Leaf node construction
  //
  ////////////////////////////////////////////////////

  qfeClusterNode::qfeSetNextClusterLabel(0);
  int constructedNodeCount = 0;
  int connectionCount = 0;
  for(unsigned int phase=0; phase<phaseCount; phase++)
  {
    for(unsigned int leafIndex=0; leafIndex<leafCountPerPhase; leafIndex++)
    {
      unsigned int leafLabel = leafIndex + phase*leafCountPerPhase;

      // Count the number of neighbours
      unsigned int leafNeighbourCount = leafNeighbours[leafIndex].size();

      // Isolated leaves need not be constructed; they are never clustered
      if(leafNeighbourCount==0)
      {
        qfeClusterNode::qfeIncrementNextClusterLabel();
        continue;
      }

      unsigned int voxelIndex = hierarchy->leaves[leafIndex];

      unsigned int x = voxelIndex % dims[0];
      unsigned int y = voxelIndex / dims[0] % dims[1];
      unsigned int z = voxelIndex / dims[0] / dims[1] % dims[2];
      unsigned int t = phase;

      // Create leaf cluster point
      qfeClusterPoint point;

      // Apply patient coordinate transformation
      qfePoint leafVoxelPosition = qfePoint(x,y,z);
      qfePoint leafPatientPosition;
      leafPatientPosition = leafVoxelPosition * voxelTransform;

      leafPatientPosition.qfeGetPointElements(point.position);
      point.position[3] = t;
      
      if(hierarchy->clusterType == qfeClusterTypeScalar){
        point.scalar = dataSeries[phase][voxelIndex];
      }
      else{
        for(int i=0; i<3; i++)
          point.flow[i] = dataSeries[phase][3*voxelIndex+i];
      }
      
      unsigned int nSize;
      unsigned int nIndices[8][4];

      // Create the actual cluster node object
      switch(hierarchy->clusterType)
      {
      case qfeClusterTypeL2Norm:
        leaves[leafLabel] = new qfeClusterNodeL2Norm(point);
        break;
      case qfeClusterTypeElliptical:
        leaves[leafLabel] = new qfeClusterNodeElliptic(point);
        break;
      case qfeClusterTypeElliptical4D:
        leaves[leafLabel] = new qfeClusterNodeElliptic4D(point);
        break;      
      case qfeClusterTypeScalar:
        leaves[leafLabel] = new qfeClusterNodeScalar(point);
        break;
      case qfeClusterTypeLinearModel:
        leaves[leafLabel] = new qfeClusterNodeLinearModel(point);

        // Define cluster neighbourhood for its initial model
        nSize=0;      

        if(x>0){nIndices[nSize][0]= x-1; nIndices[nSize][1]= y;   nIndices[nSize][2]= z;   nIndices[nSize][3] = t;   nSize++;}
        if(y>0){nIndices[nSize][0]= x;   nIndices[nSize][1]= y-1; nIndices[nSize][2]= z;   nIndices[nSize][3] = t;   nSize++;}
        if(z>0){nIndices[nSize][0]= x;   nIndices[nSize][1]= y;   nIndices[nSize][2]= z-1; nIndices[nSize][3] = t;   nSize++;}
        if(t>0){nIndices[nSize][0]= x;   nIndices[nSize][1]= y;   nIndices[nSize][2]= z;   nIndices[nSize][3] = t-1; nSize++;}

        if((x+1)<dims[0])   {nIndices[nSize][0]= x+1; nIndices[nSize][1]= y;   nIndices[nSize][2]= z;   nIndices[nSize][3] = t;   nSize++;}
        if((y+1)<dims[1])   {nIndices[nSize][0]= x;   nIndices[nSize][1]= y+1; nIndices[nSize][2]= z;   nIndices[nSize][3] = t;   nSize++;}
        if((z+1)<dims[2])   {nIndices[nSize][0]= x;   nIndices[nSize][1]= y;   nIndices[nSize][2]= z+1; nIndices[nSize][3] = t;   nSize++;}
        if((t+1)<phaseCount){nIndices[nSize][0]= x;   nIndices[nSize][1]= y;   nIndices[nSize][2]= z;   nIndices[nSize][3] = t+1; nSize++;}

        for(unsigned int n=0; n<nSize; n++)
        {
          leafVoxelPosition.qfeSetPointElements(nIndices[n][0],nIndices[n][1],nIndices[n][2],0.0);
          leafPatientPosition = leafVoxelPosition * voxelTransform;

          leafPatientPosition.qfeGetPointElements(point.position);
          point.position[3] = nIndices[n][3];
          
          unsigned int vIndex = (nIndices[n][2]*dims[1] + nIndices[n][1])*dims[0] + nIndices[n][0];
          unsigned int tIndex = nIndices[n][3];
          
          point.flow[0] = dataSeries[tIndex][3*vIndex+0];
          point.flow[1] = dataSeries[tIndex][3*vIndex+1];
          point.flow[2] = dataSeries[tIndex][3*vIndex+2];

          ((qfeClusterNodeLinearModel*)leaves[leafLabel])->qfeAddModelPoint(point);
        }
        ((qfeClusterNodeLinearModel*)leaves[leafLabel])->qfeCalculateClusterModelError();

        break;
       default:
        cout << "Invalid cluster type specified -> defaulting to Euclidean distance" << endl;
        leaves[leafLabel] = new qfeClusterNodeL2Norm(point);
        break;
      }

      constructedNodeCount++;
      connectionCount+=leafNeighbourCount;
    }
  }
  cout << "Constructed " << constructedNodeCount << " cluster nodes in " << (clock()-start)*1000/CLOCKS_PER_SEC << " ms" << endl;

  ////////////////////////////////////////////////////
  //
  //    Cluster distance list construction
  //
  ////////////////////////////////////////////////////

  start = clock();
  
  this->totalDistanceCount     = constructedNodeCount / phaseCount * (phaseCount-1) + connectionCount/2;
  //this->totalDistanceCount     = 2621820; 

  if(this->clusterDistances != NULL) delete[] this->clusterDistances;
  this->clusterDistances = new qfeDistanceListNode[this->totalDistanceCount];

  this->firstDistance = NULL;
  this->lastDistance = NULL;
  
  // Fill in all neighbour node references & initialize distances
  unsigned int constructedDistanceCount = 0;
  for(unsigned int leafIndex=0; leafIndex<leafCountPerPhase; leafIndex++)
  {
    int leafNeighbourCount = leafNeighbours[leafIndex].size();

    // Again: Skip isolated clusters
    if(leafNeighbourCount == 0)
      continue;

    // Loop over all phases
    for(unsigned int phase=0; phase<phaseCount; phase++)
    {
      int leafLabel = leafIndex + phase*leafCountPerPhase;

      // Create distances between different phases when dealing with multiple phases (temporal neighbours)
      if(phase>0)
      {
        if(constructedDistanceCount < this->totalDistanceCount)
        {
          this->qfeAddClusterDistance(leaves[leafLabel-leafCountPerPhase],leaves[leafLabel],this->clusterDistances+constructedDistanceCount);
          constructedDistanceCount++;
        }
        else
          cout << "Insufficient distance nodes allocated!" << endl;
      }

      // Create distances between spatial neighbours
      for(int neighbourIndex=0; neighbourIndex<leafNeighbourCount; neighbourIndex++)
      {
        int neighbourLabel = leafNeighbours[leafIndex][neighbourIndex] + phase*leafCountPerPhase;

        if(leafLabel<=neighbourLabel)
          continue;

        if(constructedDistanceCount < this->totalDistanceCount)
        {
          this->qfeAddClusterDistance(leaves[neighbourLabel],leaves[leafLabel],this->clusterDistances+constructedDistanceCount);
          constructedDistanceCount++;
        }
        else
          cout << "Insufficient distance nodes allocated!" << endl;

      }
    }   
  }

  cout << "Constructed " << constructedDistanceCount << " distance nodes in " << (clock()-start)*1000/CLOCKS_PER_SEC << " ms" << endl;

  // Delete pointers to all constructed nodes (they're still referenced in the distance list)
  delete[] leaves;
  delete[] dataSeries;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeAddClusterDistance(qfeClusterNode *c1, qfeClusterNode *c2, qfeDistanceListNode* dPtr)
{
  if(dPtr == NULL)
  {
    dPtr = new qfeDistanceListNode(c1,c2);
  }
  else
  {
    dPtr->qfeSetClusters(c1, c2);
    dPtr->qfeUnevaluateDistance();
  }

  if(this->firstDistance == NULL)
    this->firstDistance = dPtr;
  else
    dPtr->qfeInsertDistance(this->lastDistance);

  this->lastDistance = dPtr;

  return qfeSuccess;
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeBuildTree(qfeClusterHierarchy *hierarchy)
{
  if(hierarchy==NULL)
  {
    cout << "qfeHierarchicalClustering::qfeBuildTree - Cluster hierarchy is not defined" << endl;
    return qfeError;
  }

  // Reset clustering statistics
  hierarchy->clusterTime = 0;
  this->computedDistanceCount  = 0;
  this->processedDistanceCount = 0;

  double hierarchicalStepSize = hierarchy->stepSize;

  time_t start,stop;
  time(&start);

  if(hierarchicalStepSize>0.0)
  {
    double distanceThreshold = 0;
    while(this->firstDistance != NULL)
    {
      //if(this->processedDistanceCount == (this->totalDistanceCount-35))
      /*if( (this->processedDistanceCount >= (this->totalDistanceCount-40)) &&
          (this->processedDistanceCount <= (this->totalDistanceCount-30)) )
      {        
        time(&start);
        cout << " start" << endl;
      }*/

      this->qfeProcessDistancesByThreshold(hierarchy, (qfeFloat)distanceThreshold);

      distanceThreshold = ceil(((double)this->smallestDistance) / hierarchicalStepSize) * hierarchicalStepSize;
    
      if(this->firstDistance != NULL)
      {
        while(this->smallestDistance > distanceThreshold)
          distanceThreshold += hierarchicalStepSize;
      }
    }
  }
  else
  {
    while(this->firstDistance != NULL) this->qfeProcessSmallestDistance(hierarchy);
  }
  time(&stop);

  double elapsedTime = difftime(stop,start);

  cout << "\rProcessed " << this->processedDistanceCount << "/" << this->totalDistanceCount << \
    " distances in " << elapsedTime << " s." << endl;
  cout << "Computed " << this->computedDistanceCount << " distances" << endl;

  hierarchy->clusterTime = elapsedTime;
  hierarchy->processedDistanceCount = this->processedDistanceCount;
  hierarchy->computedDistanceCount = this->computedDistanceCount;


  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeProcessSmallestDistance(qfeClusterHierarchy *hierarchy)
{
  qfeFloat d;
  qfeFloat dMin = 1E+37f;
  qfeDistanceListNode* dMinPtr = NULL;

  qfeDistanceListNode* nullPtr = NULL;

  qfeDistanceListNode* currentDistance = this->firstDistance;
  qfeDistanceListNode* nextDistance;

  while(currentDistance != NULL)
  {
    if(currentDistance->qfeIsDistanceUnevaluated())
      this->computedDistanceCount++;

    d = currentDistance->qfeGetDistance();
    nextDistance = currentDistance->qfeGetNextDistanceNode();

    // Remove invalid distance nodes
    if(d < -1.5)
    {
      if(this->firstDistance == currentDistance)
        this->firstDistance = currentDistance->qfeGetNextDistanceNode();

      if(this->lastDistance == currentDistance)
        this->lastDistance = currentDistance->qfeGetPrevDistanceNode();

      currentDistance->qfeRemoveFromList();
      this->processedDistanceCount++;
      //delete currentDistance;
    }
    else if(d < dMin)
    {
      dMin = d;
      dMinPtr = currentDistance;
    }
    if(clock()-this->displayTime > CLOCKS_PER_SEC/2)
    {
      this->displayTime = clock();
      cout << "\rProcessed " << this->processedDistanceCount << "/" << this->totalDistanceCount << " distances";
    }
    currentDistance = nextDistance;
  }

  if(dMinPtr != NULL)
  {
    // Merge cluster only is distance is valid (-2 means invalid)
    hierarchy->tree.push_back(qfeClusterDefinition(dMinPtr->qfeGetClusterLabel1(),dMinPtr->qfeGetClusterLabel2(),dMin));
    dMinPtr->qfeMergeClusters(nullPtr,nullPtr);

    if(this->firstDistance == dMinPtr)
      this->firstDistance = dMinPtr->qfeGetNextDistanceNode();
    if(this->lastDistance == dMinPtr)
      this->lastDistance = dMinPtr->qfeGetPrevDistanceNode();

    dMinPtr->qfeRemoveFromList();
    this->processedDistanceCount++;
    //delete dMinPtr;

    return qfeSuccess;
  }
  else
  {
    cout << "\rError: could not find smallest distance" << endl;
    return qfeError;
  }
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfeProcessDistancesByThreshold(qfeClusterHierarchy *hierarchy, qfeFloat distanceThreshold)
{
  /////////////////////////////////////////////////////////////////
  //
  //  Merge all clusters with d <= Distance-Threshold
  //
  /////////////////////////////////////////////////////////////////

  clock_t start = clock();

  clock_t distTime = 0;
  clock_t mergeTime = 0;
  clock_t temp = 0;

  this->smallestDistance = 1E+37f;

  int mergeCount = 0;
  qfeFloat d;

  qfeDistanceListNode* currentDistance = this->firstDistance;
  qfeDistanceListNode* nextDistance;

  while(currentDistance != NULL)
  {
    if(currentDistance->qfeIsDistanceUnevaluated())
      this->computedDistanceCount++;

    //temp = clock();
    d = currentDistance->qfeGetDistance();
    //distTime += (clock()-temp)*1000/CLOCKS_PER_SEC;

    if(d <= distanceThreshold)
    {
      // Merge cluster only if distance is valid (-2 means invalid)
      if(d > -1.5)
      {
        //temp = clock();
        hierarchy->tree.push_back(qfeClusterDefinition(currentDistance->qfeGetClusterLabel1(),currentDistance->qfeGetClusterLabel2(),d));
        currentDistance->qfeMergeClusters(this->firstDistance,this->lastDistance);
        //mergeCount++;
        //mergeTime += (clock()-temp)*1000/CLOCKS_PER_SEC;
      }
      nextDistance = currentDistance->qfeGetNextDistanceNode();

      if(this->firstDistance == currentDistance)
        this->firstDistance = currentDistance->qfeGetNextDistanceNode();

      if(this->lastDistance == currentDistance)
        this->lastDistance = currentDistance->qfeGetPrevDistanceNode();

      currentDistance->qfeRemoveFromList();
      this->processedDistanceCount++;
      //delete currentDistance;
    }
    else
    {
      if(d < this->smallestDistance)
        this->smallestDistance = d;

      nextDistance = currentDistance->qfeGetNextDistanceNode();
    }
    currentDistance = nextDistance;
    if(clock()-this->displayTime > CLOCKS_PER_SEC/2)
    {
      this->displayTime = clock();
      cout << "\rProcessed " << this->processedDistanceCount << "/" << this->totalDistanceCount << " distances";
    }
  }
  clock_t stop = clock();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeHierarchicalClustering::qfePrintDistanceList()
{
  cout << "Current Distance List:" << endl;
  qfeDistanceListNode* currentDistance = this->firstDistance;
  while(currentDistance != NULL)
  {
    //currentDistance->qfeEvaluateDistance();
    cout << "* ";
    currentDistance->qfePrintDistance();
    currentDistance = currentDistance->qfeGetNextDistanceNode();
  }

  return qfeSuccess;
}
