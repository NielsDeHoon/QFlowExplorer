#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithm.h"
#include "qfeMatrix4f.h"
#include "qfePoint.h"
#include "qfeProbe.h"
#include "qfeSeeding.h"
#include "qfeTransform.h"
#include "qfeVector.h"
#include "qfeVolume.h"

#include <algorithm> // for sort
#include <cstdlib>
#include <vector>
#include <ctime>

typedef vector< qfeVector >        qfeVectorGroup;        // Per volume
typedef vector< qfeVectorGroup >   qfeVectorGroupSeries;  // Per series

typedef vector< qfePoint >         qfePointGroup;         
typedef vector< qfePointGroup >    qfePointGroupSeries;  

typedef vector< qfeFloat >         qfeFloatGroup;         // Per volume
typedef vector< qfeFloatGroup >    qfeFloatGroupSeries;   // Per series

/**
* \file   qfeQuantification.h
* \author Roy van Pelt
* \class  qfeQuantification
* \brief  Implements blood-flow quantification measures, based on flow probes
* \note   Confidential
*
* Compute derived measured from the blood-flow field
*/

class qfeQuantification : public qfeAlgorithm
{
public:
  qfeQuantification();
  qfeQuantification(const qfeQuantification &fv);
  ~qfeQuantification();

  static qfeReturnStatus qfeGetMaximumSpeed(qfeVolumeSeries *series, qfeProbe2D &probe, int percentage);
  static qfeReturnStatus qfeGetFlowRate(qfeVolumeSeries *series, qfeProbe2D &probe);
  

  static qfeReturnStatus qfeGetMaximumVelocity(qfeProbe2D *probe, qfeVolume *volume, int percentage, qfeVector &maxVelocity, qfePoint &maxPosition);
  static qfeReturnStatus qfeGetFlowRate(qfeProbe2D *probe, qfeVolume *volume, qfeFloat &flowRate);


protected:

private :

};
