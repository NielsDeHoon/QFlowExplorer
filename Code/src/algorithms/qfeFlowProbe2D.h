#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithmFlow.h"
#include "qfeConvert.h"
#include "qfeDisplay.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeProbe.h"
#include "qfeQuantification.h"
#include "qfeTransform.h"
#include "qfeVolume.h"
#include "qfeStudy.h"
#include "qfeTensor3f.h"

#include <iostream>
#include <fstream>


/**
* \file   qfeFlowProbe2D.h
* \author Roy van Pelt
* \class  qfeFlowProbe2D
* \brief  Implements positioning and rendering of a 2D flow probe
* \note   Confidential
*
* Positioning and rendering of a 2D flow probe
*
*/
class qfeFlowProbe2D : public qfeAlgorithmFlow
{
public:
  qfeFlowProbe2D();
  qfeFlowProbe2D(const qfeFlowProbe2D &fp);
  ~qfeFlowProbe2D();

  qfeReturnStatus qfeRenderProbe(qfeProbe2D probe);
  qfeReturnStatus qfeRenderProbeAxes(qfeProbe2D probe);
  qfeReturnStatus qfeRenderProbePlane(qfeProbe2D probe);

  qfeReturnStatus qfeLocateProbe(qfePoint s, qfeStudy *study, qfeProbe2D &probe);

  qfeReturnStatus qfeQuantifyProbe(qfeVolumeSeries *series, qfeProbe2D &probe);

  qfeReturnStatus qfeSetProbeColor(qfeColorRGBA color);

protected:
  qfeReturnStatus qfeGetProbeOrientation(int cutSize, qfeFloat sigma, int kernelSize, qfePoint s, qfeStudy *study, qfeVector *v1, qfeVector *v2, qfeVector *v3);
  qfeReturnStatus qfeGetProbePoints(qfePoint s, qfeVolume *volume, qfeVector *v1, qfeVector *v2, qfeVector *v3, int rays, int rmax, qfePoint *cAVG, qfeFloat *rAVG, qfePoint *points);

  qfeReturnStatus qfeCheckRadii(qfeProbe2D *probe, float rmax);
  qfeReturnStatus qfeCheckConvexPolygon(qfeProbe2D *probe);

  qfeReturnStatus qfeDrawProbe(qfeProbe2D probe, qfeColorRGBA color);
  qfeReturnStatus qfeDrawProbePlane(qfeProbe2D probe, qfeColorRGBA color);

private:
  // Set the size of the subvolume of which the gradients will be localy computed, the sigma en kernel size for the Gaussian derivative,
  // the maximum radius and the number of rays.
  int       cutSize; 
  float     sigma;
  int       kernelSize;
  int       rmax;
  float     radiusmax;
  static const int rays = 30;

  qfeColorRGBA              color;
  qfeFloat                  lineWidth;

  qfeProbe2D                probeCache;

  qfeVolume                *currentVolume;
};