#include "qfeFlowParticleTrace.h"

const char *qfeFlowParticleTrace::feedbackVars[] = {"particlePos", "particleAttribs"};

//----------------------------------------------------------------------------
qfeFlowParticleTrace::qfeFlowParticleTrace()
{  
  qfePoint initPos;

  initPos.qfeSetPointElements(0.0,0.0,0.0);

  this->shaderProgramEvolution = new qfeGLShaderProgram();
  this->shaderProgramEvolution->qfeAddShaderFromFile("/shaders/ptr/ptr-evolve-vert.glsl", QFE_VERTEX_SHADER);      
  this->shaderProgramEvolution->qfeSetTransformFeedbackVaryings(2, feedbackVars, GL_SEPARATE_ATTRIBS);    
  this->shaderProgramEvolution->qfeLink();

  this->shaderProgramAdvection = new qfeGLShaderProgram();
  this->shaderProgramAdvection->qfeAddShaderFromFile("/shaders/library/transform.glsl", QFE_GEOMETRY_SHADER);
  this->shaderProgramAdvection->qfeAddShaderFromFile("/shaders/library/fetch.glsl", QFE_GEOMETRY_SHADER);  
  this->shaderProgramAdvection->qfeAddShaderFromFile("/shaders/library/integrate.glsl", QFE_GEOMETRY_SHADER);  
  this->shaderProgramAdvection->qfeAddShaderFromFile("/shaders/ptr/ptr-advect-vert.glsl", QFE_VERTEX_SHADER);    
  this->shaderProgramAdvection->qfeAddShaderFromFile("/shaders/ptr/ptr-advect-geom.glsl", QFE_GEOMETRY_SHADER);  
  this->shaderProgramAdvection->qfeSetTransformFeedbackVaryings(2, feedbackVars, GL_SEPARATE_ATTRIBS);      
  this->shaderProgramAdvection->qfeLink();

  this->shaderProgramRenderSphere = new qfeGLShaderProgram();   
  this->shaderProgramRenderSphere->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);    
  this->shaderProgramRenderSphere->qfeAddShaderFromFile("/shaders/ptr/ptr-render-vert.glsl", QFE_VERTEX_SHADER);    
  this->shaderProgramRenderSphere->qfeAddShaderFromFile("/shaders/ptr/ptr-render-imposter-sphere-geom.glsl", QFE_GEOMETRY_SHADER);      
  this->shaderProgramRenderSphere->qfeAddShaderFromFile("/shaders/ptr/ptr-render-imposter-sphere-frag.glsl", QFE_FRAGMENT_SHADER);      
  this->shaderProgramRenderSphere->qfeLink();

  this->shaderProgramRenderEllipsoid = new qfeGLShaderProgram();
  this->shaderProgramRenderEllipsoid->qfeAddShaderFromFile("/shaders/library/transform.glsl", QFE_GEOMETRY_SHADER);    
  this->shaderProgramRenderEllipsoid->qfeAddShaderFromFile("/shaders/library/fetch.glsl", QFE_GEOMETRY_SHADER);    
  this->shaderProgramRenderEllipsoid->qfeAddShaderFromFile("/shaders/library/shading.glsl", QFE_FRAGMENT_SHADER);    
  this->shaderProgramRenderEllipsoid->qfeAddShaderFromFile("/shaders/ptr/ptr-render-vert.glsl", QFE_VERTEX_SHADER);    
  this->shaderProgramRenderEllipsoid->qfeAddShaderFromFile("/shaders/ptr/ptr-render-imposter-ellipsoid-geom.glsl", QFE_GEOMETRY_SHADER);      
  this->shaderProgramRenderEllipsoid->qfeAddShaderFromFile("/shaders/ptr/ptr-render-imposter-ellipsoid-frag.glsl", QFE_FRAGMENT_SHADER);      
  this->shaderProgramRenderEllipsoid->qfeLink();

  this->shaderProgramRenderTrail = new qfeGLShaderProgram();
  this->shaderProgramRenderTrail->qfeAddShaderFromFile("/shaders/library/transform.glsl", QFE_GEOMETRY_SHADER);    
  this->shaderProgramRenderTrail->qfeAddShaderFromFile("/shaders/library/fetch.glsl", QFE_GEOMETRY_SHADER);    
  this->shaderProgramRenderTrail->qfeAddShaderFromFile("/shaders/library/integrate.glsl", QFE_GEOMETRY_SHADER);  
  this->shaderProgramRenderTrail->qfeAddShaderFromFile("/shaders/ptr/ptr-render-vert.glsl", QFE_VERTEX_SHADER);    
  this->shaderProgramRenderTrail->qfeAddShaderFromFile("/shaders/ptr/ptr-render-trail-geom.glsl", QFE_GEOMETRY_SHADER);      
  this->shaderProgramRenderTrail->qfeAddShaderFromFile("/shaders/ptr/ptr-render-trail-frag.glsl", QFE_FRAGMENT_SHADER);      
  this->shaderProgramRenderTrail->qfeLink();
    
  this->firstEvolve     = true;
  this->firstAdvect     = true;
  this->firstRender     = true;  

  this->volumeSeries.clear();

  this->light    = NULL;
  this->colormap = 0;

  this->particlePositionsCurrent[0].clear();
  this->particlePositionsCurrent[1].clear();
  this->particlePositionsNewBorn.clear();
  this->particleAttributes[0].clear();
  this->particleAttributes[1].clear();  
  this->particlesCount = 0;
  this->setCount       = 0;
  this->setColor.clear();
  
  this->phaseCurrent      = 0.0;
  this->phaseDuration     = 0.0;

  this->traceSteps        = 10;
  this->traceStepsPhase   = 100;
  this->traceStepSize     = 0.1;
  this->traceStepDuration = 10.0;  
  this->traceDirection    = 1;  
  this->traceScheme       = qfeFlowParticlesRK4;    
  this->traceSpeed        = 100;
  this->traceLifeTime     = 25;
  this->trailStyle        = qfeFlowParticlesTrailHalo;
  this->trailLifeTime     = 0.5;  
  this->particleSize      = 2;
  this->particleStyle     = qfeFlowParticlesSphere;
  this->particleColor     = qfeFlowParticlesColorSpeed;
  this->particleShading   = qfeFlowParticlesCel;
  this->particleContour   = true;  
  this->colorScale        = 50;

  this->ping              = true;    

  this->qfeInitParticleData();
  this->qfeInitNewBornData();

  this->qfeCreateParticleNormalMap(this->particleNormalMap);
};

//----------------------------------------------------------------------------
qfeFlowParticleTrace::qfeFlowParticleTrace(const qfeFlowParticleTrace &fv) : qfeAlgorithmFlow(fv)
{  
  this->firstEvolve                 = fv.firstEvolve;
  this->firstAdvect                 = fv.firstAdvect;
  this->firstRender                 = fv.firstRender;
  
  this->shaderProgramEvolution      = fv.shaderProgramEvolution;
  this->shaderProgramAdvection      = fv.shaderProgramAdvection;
  this->shaderProgramRenderSphere   = fv.shaderProgramRenderSphere;
  this->shaderProgramRenderEllipsoid= fv.shaderProgramRenderEllipsoid;
  this->shaderProgramRenderTrail    = fv.shaderProgramRenderTrail;

  this->volumeSeries                = fv.volumeSeries;
  this->light                       = fv.light;
  this->colormap                    = fv.colormap;

  this->V2P                         = fv.V2P;
  this->P2V                         = fv.P2V;
  this->volumeSize[0]               = fv.volumeSize[0];
  this->volumeSize[1]               = fv.volumeSize[1];
  this->volumeSize[2]               = fv.volumeSize[2]; 

  this->phaseCurrent                = fv.phaseCurrent;  
  this->phaseDuration               = fv.phaseDuration; 

  this->traceSteps                  = fv.traceSteps;
  this->traceStepsPhase             = fv.traceStepsPhase;
  this->traceStepSize               = fv.traceStepSize;
  this->traceStepDuration           = fv.traceStepDuration;
  this->traceScheme                 = fv.traceScheme;  
  this->traceSpeed                  = fv.traceSpeed;
  this->traceDirection              = fv.traceDirection;
  this->traceLifeTime               = fv.traceLifeTime;
  this->trailStyle                  = fv.trailStyle;
  this->trailLifeTime               = fv.trailLifeTime;
  this->particleStyle               = fv.particleStyle;
  this->particleShading             = fv.particleShading;
  this->particleColor               = fv.particleColor;
  this->particleContour             = fv.particleContour;  
  this->particleSize                = fv.particleSize;
  this->colorScale                  = fv.colorScale;

  this->particlePositionsVBO[0]     = fv.particlePositionsVBO[0];  
  this->particlePositionsVBO[1]     = fv.particlePositionsVBO[1];  
  this->particlePositionsNewBornVBO = fv.particlePositionsNewBornVBO;
  this->particleAttributesVBO[0]    = fv.particleAttributesVBO[0];  
  this->particleAttributesVBO[1]    = fv.particleAttributesVBO[1];    
   
  this->particlePositionsCurrent[0] = fv.particlePositionsCurrent[0];
  this->particlePositionsCurrent[1] = fv.particlePositionsCurrent[1];
  this->particlePositionsNewBorn    = fv.particlePositionsNewBorn;
  this->particleAttributes[0]       = fv.particleAttributes[0];
  this->particleAttributes[1]       = fv.particleAttributes[1];    
  this->particlesCount              = fv.particlesCount;  
  this->setCount                    = fv.setCount;
  this->setColor                    = fv.setColor;

  this->locationPosition            = fv.locationPosition;
  this->locationAttribute           = fv.locationAttribute;
  this->locationPositionBirth       = fv.locationPositionBirth;

  this->particleColorMap            = fv.particleColorMap;
  this->particleNormalMap           = fv.particleNormalMap;
  /*
  for(int i=0; i<16; i++)
  {
    this->matrixProjection[i]                 = fv.matrixProjection[i];
    this->matrixModelView[i]                  = fv.matrixModelView[i];
    this->matrixModelViewInverse[i]           = fv.matrixModelViewInverse[i];
    this->matrixModelViewProjection[i]        = fv.matrixModelViewProjection[i];
    this->matrixModelViewProjectionInverse[i] = fv.matrixModelViewProjectionInverse[i];
  }
  */
  
  this->ping                        = fv.ping;
};

//----------------------------------------------------------------------------
qfeFlowParticleTrace::~qfeFlowParticleTrace()
{
  delete this->shaderProgramEvolution;
  delete this->shaderProgramAdvection;
  delete this->shaderProgramRenderSphere;
  delete this->shaderProgramRenderEllipsoid;
  delete this->shaderProgramRenderTrail;
 
  this->qfeClearSeedPositions();

  this->particlePositionsNewBorn.clear();

  if(glIsTexture(this->particleColorMap))  glDeleteTextures(1, &this->particleColorMap);
  if(glIsTexture(this->particleNormalMap)) glDeleteTextures(1, &this->particleNormalMap);
};

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeClearSeedPositions()
{
  // Erase all information
  this->qfeClearParticleBuffers();
  this->qfeClearParticleData();

  this->qfeClearNewBornBuffers();
  this->qfeClearNewBornData();

  // Build new empty structures
  this->qfeInitParticleData();
  this->qfeInitNewBornData();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeAddSeedPositions(qfeSeeds seeds)
{
  qfeColorRGBA color;
  color.r = 0.8;
  color.g = color.b = 0.3;
  color.a = 1.0;

  this->qfeAddParticlePositions(seeds);   
 
  this->particlesCount += (int)seeds.size();
  this->setCount       += 1;
  this->setColor.push_back(color);

  this->qfeClearNewBornData();
  this->qfeInitNewBornData();
  this->qfeUpdateParticlePositionsNewBorn(this->phaseCurrent);

  this->qfeCreateParticleColorMap(this->particleColorMap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeAddSeedPositions(qfeSeeds seeds, float phase)
{
  qfeColorRGBA color;
  color.r = 0.8;
  color.g = color.b = 0.3;
  color.a = 1.0;

  this->qfeAddSeedPositions(seeds, phase, color);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeAddSeedPositions(qfeSeeds seeds, float phase, qfeColorRGBA color)
{
  this->qfeAddParticlePositionsCurrent(seeds, phase);   
 
  this->particlesCount += (int)seeds.size();
  this->setCount       += 1;
  this->setColor.push_back(color);

  this->qfeClearNewBornData();
  this->qfeInitNewBornData();
  this->qfeUpdateParticlePositionsNewBorn(phase);

  this->qfeCreateParticleColorMap(this->particleColorMap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// phase         - current phase in phases
// traceDuration - total time to trace in ms
qfeReturnStatus qfeFlowParticleTrace::qfeAdvectParticles(qfeVolumeSeries series, qfeFloat phase, qfeFloat traceDuration)
{
  GLuint  count = 0;
  GLuint  query;
  int     src, dst;

  // First time of advection, prepare some global variables
  if(this->firstAdvect)
  {
    this->volumeSeries  = series;       

    this->volumeSeries.front()->qfeGetVolumeSize(this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);

    qfeTransform::qfeGetMatrixVoxelToPatient(this->V2P, this->volumeSeries.front());
    qfeTransform::qfeGetMatrixPatientToVoxel(this->P2V, this->volumeSeries.front());   

    this->qfeSetStaticUniformLocationsAdvect();

    this->firstAdvect = false;
  }

  // Update phase information
  this->phaseCurrent      = phase;
  this->traceSteps        = ceil(this->traceStepsPhase * (traceDuration / this->phaseDuration));
  this->traceStepSize     = 1.0 / this->traceStepsPhase;
  this->traceStepDuration = this->traceStepSize * this->phaseDuration;    

  // Set the state for the ping-pong vertex buffers
  if(ping)
  {
    src  = 0;
    dst  = 1;
    ping = false;
  }
  //pong
  else 
  {
    src  = 1;
    dst  = 0;
    ping = true;
  }

  // Check if we have the necessary buffers
  if(!glIsBuffer(this->particlePositionsVBO[src]))  return qfeError;
  if(!glIsBuffer(this->particlePositionsVBO[dst]))  return qfeError;
  if(!glIsBuffer(this->particleAttributesVBO[src])) return qfeError;
  if(!glIsBuffer(this->particleAttributesVBO[dst])) return qfeError;

  // Get attribute locations
  this->shaderProgramAdvection->qfeGetAttribLocation("qfe_ParticlePosition",      this->locationPosition);
  this->shaderProgramAdvection->qfeGetAttribLocation("qfe_ParticleAttribute",     this->locationAttribute);
   
  // No need for rasterization
  glEnable(GL_RASTERIZER_DISCARD);
  
  // Enable the velocity volumes
  this->qfeBindTextureSliding();

  // Bind the output buffer
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, this->particlePositionsVBO[dst]);
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, this->particleAttributesVBO[dst]);  

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocationsAdvect();

  // Enable the shader program
  this->shaderProgramAdvection->qfeEnable(); 

  // Perform the advection using a transform feedback  
  glGenQueries(1, &query); 
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
  glBeginTransformFeedback(GL_POINTS);    

  this->qfeDrawParticles(this->particlePositionsVBO[src], this->particleAttributesVBO[src], MAX_PARTICLES);  
    
  glEndTransformFeedback(); 
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

  // Check if we have output in the buffer
  //glGetQueryObjectuiv(query, GL_QUERY_RESULT, &count); 
  //cout << count << endl;

  // Disable the shader program
  this->shaderProgramAdvection->qfeDisable(); 
  this->qfeUnbindTextures();

  // Clean up
  glDisable(GL_RASTERIZER_DISCARD);
  glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeEvolveParticles()
{
  GLuint  count = 0;
  GLuint  query;
  int     src, dst;

  // First time of evolution, prepare some global variables  
  if(this->firstEvolve)
  {    
    this->qfeSetStaticUniformLocationsEvolve();

    this->firstEvolve = false;
  }  

  // Set the state for the ping-pong vertex buffers
  if(ping)
  {
    src  = 0;
    dst  = 1;
    ping = false;
  }
  //pong
  else 
  {
    src  = 1;
    dst  = 0;
    ping = true;
  }

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocationsEvolve();

  //cout << "Evolve: read  "  << src << endl;
  //cout << "Evolve: write " << dst << endl;

  // Check if we have a buffer the necessary buffers
  if(!glIsBuffer(this->particlePositionsVBO[src]))  return qfeError;
  if(!glIsBuffer(this->particlePositionsVBO[dst]))  return qfeError; 
  if(!glIsBuffer(this->particleAttributesVBO[src])) return qfeError;
  if(!glIsBuffer(this->particleAttributesVBO[dst])) return qfeError; 
  if(!glIsBuffer(this->particlePositionsNewBornVBO))return qfeError;

  // Get attribute locations
  this->shaderProgramEvolution->qfeGetAttribLocation("qfe_ParticlePosition",      this->locationPosition);
  this->shaderProgramEvolution->qfeGetAttribLocation("qfe_ParticleAttribute",     this->locationAttribute);
  this->shaderProgramEvolution->qfeGetAttribLocation("qfe_ParticlePositionBirth", this->locationPositionBirth);
  
  // Bind the output buffer  
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, this->particlePositionsVBO[dst]);
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 1, this->particleAttributesVBO[dst]);  
  
  // Enable the shader program
  this->shaderProgramEvolution->qfeEnable();   

  // Perform the evolution using a transform feedback   
  glEnable(GL_RASTERIZER_DISCARD);
  
  glGenQueries(1, &query);   
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
  glBeginTransformFeedback(GL_POINTS);    

  glBindBuffer(GL_ARRAY_BUFFER, this->particlePositionsNewBornVBO);
  glEnableVertexAttribArray(this->locationPositionBirth);
  glVertexAttribPointer(this->locationPositionBirth,  4, GL_FLOAT, GL_FALSE, 0, 0);

  this->qfeDrawParticles(this->particlePositionsVBO[src], this->particleAttributesVBO[src], MAX_PARTICLES);  

  glDisableVertexAttribArray(this->locationPositionBirth);
 
  glEndTransformFeedback(); 
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

  glDisable(GL_RASTERIZER_DISCARD);

  // Check if we have output in the buffer
  //glGetQueryObjectuiv(query, GL_QUERY_RESULT, &count); 
  //cout << count << endl;

  // Disable the shader program
  this->shaderProgramEvolution->qfeDisable();   

  // Clean up
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, 0);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeRenderParticles(qfeColorMap *colormap)
{
  int currentParticles;

  // First time of rendering, prepare some global variables  
  if(this->firstRender)
  {    
    this->qfeSetStaticUniformLocationsRender();

    this->firstRender = false;
  }

  if(colormap != NULL) colormap->qfeGetColorMapTextureId(this->colormap); 

  if(this->ping) currentParticles = 0;
  else           currentParticles = 1;

  if(!glIsBuffer(this->particlePositionsVBO[currentParticles]))  return qfeError;
  if(!glIsBuffer(this->particleAttributesVBO[currentParticles])) return qfeError;

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  // Obtain the modelview information
  this->qfeGetModelViewMatrix();

  // Set point parameters, in case of normal point rendering
  glPointSize(4.0);
  glEnable(GL_POINT_SMOOTH);  

  // Bind textures
  this->qfeBindTextureSliding();
  this->qfeBindTextureRender();

  // Allow blending for transparency
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);  

  // Set the dynamic uniform inputs
  this->qfeSetDynamicUniformLocationsRender();
  
  if(this->trailStyle != qfeFlowParticlesTrailNone)
    this->qfeRenderStyleTrail(currentParticles);

  switch(this->particleStyle)
  {
  case qfeFlowParticlesEllipsoid : 
    this->qfeRenderStyleEllipsoid(currentParticles);
    break;
  case qfeFlowParticlesSphere : 
    this->qfeRenderStyleSphere(currentParticles);
    break;
  default : 
    this->qfeRenderStyleSphere(currentParticles);
    break;
  } 

  glDisable(GL_BLEND);  

  // Unbind textures
  this->qfeUnbindTextures();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// step - amount of integration per phase
qfeReturnStatus qfeFlowParticleTrace::qfeSetTraceSteps(int steps)
{
  this->traceStepsPhase  = steps;  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// direction - 1 = forward, -1 = backward
qfeReturnStatus qfeFlowParticleTrace::qfeSetTraceDirection(int dir)
{
  this->traceDirection = dir;
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// lifetime - number of phases to live
qfeReturnStatus qfeFlowParticleTrace::qfeSetTraceLifeTime(int lifetime)
{
  this->traceLifeTime = lifetime;
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetTrailStyle(qfeFlowParticlesTrailType style)
{
  this->trailStyle = style;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// lifetime in phases [0..1]
qfeReturnStatus qfeFlowParticleTrace::qfeSetTrailLifeTime(float lifeTime)
{
  this->trailLifeTime = min(max(lifeTime, 0.0), 1.0);

  return qfeSuccess;
}
  

//----------------------------------------------------------------------------
// scheme - 1st, 2nd or 4th order Runge Kutta integration
qfeReturnStatus qfeFlowParticleTrace::qfeSetTraceScheme(qfeFlowParticlesIntegrationScheme scheme)
{
  this->traceScheme = scheme;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
// percentage - speed modulation in percentage
qfeReturnStatus qfeFlowParticleTrace::qfeSetTraceSpeed(int percentage)
{
  if(percentage <= 0)
    this->traceSpeed = 1;
  else
    this->traceSpeed = percentage;
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// duration - total time for one phase in ms
qfeReturnStatus qfeFlowParticleTrace::qfeSetPhaseDuration(qfeFloat duration)
{
  this->phaseDuration = duration;
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetParticleStyle(qfeFlowParticlesStyle style)
{
  this->particleStyle = style;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetParticleColor(qfeFlowParticlesColor color)
{
  this->particleColor = color;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetParticleShading(qfeFlowParticlesShading shading)
{
  this->particleShading = shading;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetParticleContour(bool contour)
{
  this->particleContour = contour;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetParticleSize(int size)
{
  this->particleSize = size;

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetLightSource(qfeLightSource *light)
{
  this->light = light;
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetColorScale(int scale)
{
  this->colorScale = scale;
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
// If you assign a data pointer to glVertexAttribPointer or glVertexPointer,
// do NOT bind a VBO. If you bind a VBO, pointer assignment should be 0.
qfeReturnStatus qfeFlowParticleTrace::qfeDrawParticles(GLuint particles, GLuint attribs, int count)
{
  if(!glIsBuffer(particles) || !glIsBuffer(attribs)) return qfeError;
  if((this->locationPosition == -1) || (this->locationAttribute == -1)) return qfeError;

  glBindBuffer(GL_ARRAY_BUFFER, particles);
  glEnableVertexAttribArray(this->locationPosition);
  glVertexAttribPointer(this->locationPosition,  4, GL_FLOAT, GL_FALSE, 0, 0);

  glBindBuffer(GL_ARRAY_BUFFER, attribs);
  glEnableVertexAttribArray(this->locationAttribute);
  glVertexAttribPointer(this->locationAttribute,  4, GL_FLOAT, GL_FALSE, 0, 0);
  
  glDrawArrays(GL_POINTS, 0, count);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glDisableVertexAttribArray(this->locationPosition);
  glDisableVertexAttribArray(this->locationAttribute);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeInitParticleData()
{ 
  this->qfeClearParticleData();

  for(int i=0; i<(int)MAX_PARTICLES; i++)
  {
    qfeParticle particle;
   
    particle.x         =  0.0;
    particle.y         =  0.0;
    particle.z         =  0.0;
    particle.t         =  0.0;       
    particle.age       = -1.0;       
    particle.speed     =  0.0;
    particle.color     =  0.0;
    particle.birth     =  0.0;

    this->particlePositionsCurrent[0].push_back(particle.x);
    this->particlePositionsCurrent[0].push_back(particle.y);
    this->particlePositionsCurrent[0].push_back(particle.z);
    this->particlePositionsCurrent[0].push_back(particle.t);

    this->particleAttributes[0].push_back(particle.age);
    this->particleAttributes[0].push_back(particle.speed);
    this->particleAttributes[0].push_back(particle.color);
    this->particleAttributes[0].push_back(particle.birth);

    this->particlePositionsCurrent[1].push_back(particle.x);
    this->particlePositionsCurrent[1].push_back(particle.y);
    this->particlePositionsCurrent[1].push_back(particle.z);
    this->particlePositionsCurrent[1].push_back(particle.t);

    this->particleAttributes[1].push_back(particle.age);
    this->particleAttributes[1].push_back(particle.speed);
    this->particleAttributes[1].push_back(particle.color);
    this->particleAttributes[1].push_back(particle.birth);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeClearParticleData()
{
  this->particlePositionsCurrent[0].clear();
  this->particlePositionsCurrent[1].clear();
  this->particleAttributes[0].clear();
  this->particleAttributes[1].clear();
  this->particlesCount = 0;
  this->setCount       = 0;
  this->setColor.clear();

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeInitParticleBuffers()
{
  int dataSize;

  this->qfeClearParticleBuffers();

  dataSize = MAX_PARTICLES * 4 * sizeof(GLfloat);
  
  glGenBuffers(2, this->particlePositionsVBO);     
  glGenBuffers(2, this->particleAttributesVBO);    

  cout << "particles 0: " << this->particlePositionsVBO[0] << endl;
  cout << "particles 1: " << this->particlePositionsVBO[1] << endl;
  cout << "attribs   0: " << this->particleAttributesVBO[0] << endl;
  cout << "attribs   1: " << this->particleAttributesVBO[1] << endl;

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->particlePositionsVBO[0]);    
  glBufferData(GL_ARRAY_BUFFER, dataSize, &(this->particlePositionsCurrent[0])[0], GL_STREAM_COPY); 
  glBindBuffer(GL_ARRAY_BUFFER, 0);  

  glBindBuffer(GL_ARRAY_BUFFER, this->particlePositionsVBO[1]);    
  glBufferData(GL_ARRAY_BUFFER, dataSize, &(this->particlePositionsCurrent[1])[0], GL_STREAM_COPY); 
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  glBindBuffer(GL_ARRAY_BUFFER, this->particleAttributesVBO[0]);    
  glBufferData(GL_ARRAY_BUFFER, dataSize, &(this->particleAttributes[0])[0], GL_STREAM_COPY); 
  glBindBuffer(GL_ARRAY_BUFFER, 0);  

  glBindBuffer(GL_ARRAY_BUFFER, this->particleAttributesVBO[1]);    
  glBufferData(GL_ARRAY_BUFFER, dataSize, &(this->particleAttributes[1])[0], GL_STREAM_COPY); 
  glBindBuffer(GL_ARRAY_BUFFER, 0);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeClearParticleBuffers()
{
  if(glIsBuffer(this->particlePositionsVBO[0]) || glIsBuffer(this->particlePositionsVBO[1]))
  {    
    glDeleteBuffers(1, &this->particlePositionsVBO[0]);
    glDeleteBuffers(1, &this->particlePositionsVBO[1]);
    glDeleteBuffers(1, &this->particleAttributesVBO[0]);
    glDeleteBuffers(1, &this->particleAttributesVBO[1]);
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeInitNewBornData()
{ 
  this->qfeClearNewBornData();
  for(int i=0; i<(int)MAX_PARTICLES; i++)
  {
    qfeParticle particle;
   
    particle.x      =  0.0;
    particle.y      =  0.0;
    particle.z      =  0.0;  
    particle.t      =  0.0;
    
    this->particlePositionsNewBorn.push_back(particle.x);
    this->particlePositionsNewBorn.push_back(particle.y);
    this->particlePositionsNewBorn.push_back(particle.z);
    this->particlePositionsNewBorn.push_back(particle.t);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeClearNewBornData()
{
  this->particlePositionsNewBorn.clear();  
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeInitNewBornBuffers()
{
  int dataSize;  

  this->qfeClearNewBornBuffers();

  dataSize = MAX_PARTICLES * 4 * sizeof(GLfloat);
  
  glGenBuffers(1, &this->particlePositionsNewBornVBO);   

  //cout << "newborn: " << this->particlePositionsNewBornVBO << endl;

  // Set-up the vertex buffer objects (VBO)
  glBindBuffer(GL_ARRAY_BUFFER, this->particlePositionsNewBornVBO);    
  glBufferData(GL_ARRAY_BUFFER, dataSize, &this->particlePositionsNewBorn[0], GL_STREAM_COPY); 
  glBindBuffer(GL_ARRAY_BUFFER, 0);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeClearNewBornBuffers()
{
  if(glIsBuffer(this->particlePositionsNewBornVBO))
  {    
    glDeleteBuffers(1, &this->particlePositionsNewBornVBO);    
  }
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeCreateParticleColorMap(GLuint &colormap)
{
  if((int)this->setColor.size() <= 0) return qfeError;
  
  int dims = (int)this->setColor.size();

  qfeFloat *map = (qfeFloat *)calloc(dims*3, sizeof(qfeFloat));

  for(int i=0; i<(int)this->setColor.size(); i++)
  {
    map[3*i+0] = this->setColor[i].r;
    map[3*i+1] = this->setColor[i].g;
    map[3*i+2] = this->setColor[i].b;
  }

  if(glIsTexture(colormap)) glDeleteTextures(1, &colormap);

  glGenTextures(1, &colormap);
  glBindTexture(GL_TEXTURE_1D, colormap);
  glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB16F, dims, 0, GL_RGB, GL_FLOAT, map); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeCreateParticleNormalMap(GLuint &normalmap)
{
  const int dims = 64;
  
  qfeFloat map[dims][dims][4];

  qfeFloat xv, yv, zv;

  for(int x=0; x<dims; x++)
  {
    for(int y=0; y<dims; y++)
    {
      xv = 2.0f*(x /(float)(dims-1)) - 1.0f;
      yv = 2.0f*(y /(float)(dims-1)) - 1.0f;
      zv = 0 - (xv*xv + yv*yv - 1);
      
      map[x][y][0] = yv; 
      map[x][y][1] = xv;
      map[x][y][2] = zv;
      
      if(xv*xv + yv*yv < 1)
        map[x][y][3] = 1.0;
      else     
        map[x][y][3] = 0.0;     
    }  
  }  

  if(glIsTexture(normalmap)) glDeleteTextures(1, &normalmap);

  glGenTextures(1, &normalmap);
  glBindTexture(GL_TEXTURE_2D, normalmap);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, dims, dims, 0, GL_RGBA, GL_FLOAT, map);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeGetModelViewMatrix()
{
  qfeMatrix4f mv, p, mvinv, mvp, mvpinv;

  GLfloat modelview[16];
  glGetFloatv(GL_MODELVIEW_MATRIX, modelview);
  qfeMatrix4f::qfeSetMatrixElements(mv, modelview);

  GLfloat projection[16];
  glGetFloatv(GL_PROJECTION_MATRIX, projection);
  qfeMatrix4f::qfeSetMatrixElements(p, projection);
  
  qfeMatrix4f::qfeGetMatrixInverse(mv, mvinv);

  mvp    = mv*p;
  qfeMatrix4f::qfeGetMatrixInverse(mvp, mvpinv);  

  qfeMatrix4f::qfeGetMatrixElements(p,      this->matrixProjection);
  qfeMatrix4f::qfeGetMatrixElements(mv,     this->matrixModelView);
  qfeMatrix4f::qfeGetMatrixElements(mvinv,  this->matrixModelViewInverse);
  qfeMatrix4f::qfeGetMatrixElements(mvp,    this->matrixModelViewProjection);
  qfeMatrix4f::qfeGetMatrixElements(mvpinv, this->matrixModelViewProjectionInverse);
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeAddParticlePositions(qfeSeeds seeds)
{
  if(int(seeds.size()) <= 0) return qfeError;

  int l, h;
  int src;
  int index;

  // Determine where to insert the new particles
  l = this->particlesCount;
  h = min(l + (int)seeds.size(), MAX_PARTICLES);

  // Compute the current source buffer
  // Set the state for the ping-pong vertex buffers
  if(this->ping)
    src  = 0;   
  else 
    src  = 1;
  
  for(int i=0; i<(int)seeds.size(); i++)
  {
    qfeParticle particle;
   
    particle.x          =  seeds[i].x;
    particle.y          =  seeds[i].y;
    particle.z          =  seeds[i].z;
    particle.t          =  seeds[i].w;           
    particle.age        =  0.0;       
    particle.speed      =  0.0;
    particle.color      =  this->setCount;
    particle.birth      =  seeds[i].w;

    index           = (4*l)+(4*i);

    this->particlePositionsCurrent[src][index+0] = particle.x;
    this->particlePositionsCurrent[src][index+1] = particle.y;
    this->particlePositionsCurrent[src][index+2] = particle.z;
    this->particlePositionsCurrent[src][index+3] = particle.t;

    this->particleAttributes[src][index+0] = particle.age;
    this->particleAttributes[src][index+1] = particle.speed;
    this->particleAttributes[src][index+2] = particle.color;
    this->particleAttributes[src][index+3] = particle.birth;
  }

  this->qfeInitParticleBuffers();
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeAddParticlePositionsCurrent(qfeSeeds seeds, float phase)
{
  if(int(seeds.size()) <= 0) return qfeError;

  int l, h;
  int src;
  int index;

  // Determine where to insert the new particles
  l = this->particlesCount;
  h = min(l + (int)seeds.size(), MAX_PARTICLES);

  // Compute the current source buffer
  // Set the state for the ping-pong vertex buffers
  if(this->ping)
    src  = 0;   
  else 
    src  = 1;
  
  for(int i=0; i<(int)seeds.size(); i++)
  {
    qfeParticle particle;
   
    particle.x          =  seeds[i].x;
    particle.y          =  seeds[i].y;
    particle.z          =  seeds[i].z;
    particle.t          =  phase;           
    particle.age        =  0.0;       
    particle.speed      =  0.0;
    particle.color      =  this->setCount;
    particle.birth      =  phase;

    // Hack: single reseeding
    /*
    if(i>(int)seeds.size()/2.0f) 
    {
      particle.t        = phase + 4;
    }
    */

    // Hack: multiple reseeding
    /*
    if(i<(int)seeds.size()*(1.0f/3.0f)) 
    {
      particle.t        =  phase;
      particle.birth    =  phase;
    }
    else if(i<(int)seeds.size()*(2.0f/3.0f)) 
    {
      particle.t        =  phase + 3;
      particle.birth    =  phase + 3;
    }
    else 
    {
      particle.t        =  phase + 6;
      particle.birth    =  phase + 6;
    }
    */

    index           = (4*l)+(4*i);

    this->particlePositionsCurrent[src][index+0] = particle.x;
    this->particlePositionsCurrent[src][index+1] = particle.y;
    this->particlePositionsCurrent[src][index+2] = particle.z;
    this->particlePositionsCurrent[src][index+3] = particle.t;

    this->particleAttributes[src][index+0] = particle.age;
    this->particleAttributes[src][index+1] = particle.speed;
    this->particleAttributes[src][index+2] = particle.color;
    this->particleAttributes[src][index+3] = particle.birth;
  }

  this->qfeInitParticleBuffers();
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeUpdateParticlePositionsNewBorn(float phase)
{
  int l, h;
  int src;
  int index;

  // Determine where to insert the new particles
  //l = this->particlesCount;
  l = 0;
  h = min(this->particlesCount, MAX_PARTICLES);

    // Compute the current source buffer
  // Set the state for the ping-pong vertex buffers
  if(this->ping)
    src  = 0;   
  else 
    src  = 1;
  
  for(int i=0; i<(int)this->particlesCount; i++)
  {
    qfeParticle particle;
   

    // Hack: single reseeding
    /*
    if(i>(int)seeds.size()/2.0f) 
    {
      particle.t        = phase + 4;
    }
    */

    // Hack: multiple reseeding
    /*
    if(i<(int)seeds.size()*(1.0f/3.0f)) 
    {
      particle.t        =  phase;
      particle.birth    =  phase;
    }
    else if(i<(int)seeds.size()*(2.0f/3.0f)) 
    {
      particle.t        =  phase + 3;
      particle.birth    =  phase + 3;
    }
    else 
    {
      particle.t        =  phase + 6;
      particle.birth    =  phase + 6;
    }
    */

    index           =  4*i;

    particle.x      =  this->particlePositionsCurrent[src][index+0];
    particle.y      =  this->particlePositionsCurrent[src][index+1];
    particle.z      =  this->particlePositionsCurrent[src][index+2];    
    particle.t      =  phase;

    this->particlePositionsNewBorn[index+0] = particle.x;
    this->particlePositionsNewBorn[index+1] = particle.y;
    this->particlePositionsNewBorn[index+2] = particle.z;
    this->particlePositionsNewBorn[index+3] = particle.t;
  }

  this->qfeInitNewBornBuffers();
 
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeRenderStyleSphere(int currentBuffer)
{
  this->shaderProgramRenderSphere->qfeGetAttribLocation("qfe_ParticlePosition",  this->locationPosition);
  this->shaderProgramRenderSphere->qfeGetAttribLocation("qfe_ParticleAttribute", this->locationAttribute);

  this->shaderProgramRenderSphere->qfeEnable();

  this->qfeDrawParticles(this->particlePositionsVBO[currentBuffer], this->particleAttributesVBO[currentBuffer], MAX_PARTICLES);

  this->shaderProgramRenderSphere->qfeDisable(); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeRenderStyleEllipsoid(int currentBuffer)
{
  this->shaderProgramRenderEllipsoid->qfeGetAttribLocation("qfe_ParticlePosition",  this->locationPosition);
  this->shaderProgramRenderEllipsoid->qfeGetAttribLocation("qfe_ParticleAttribute", this->locationAttribute);

  this->shaderProgramRenderEllipsoid->qfeEnable();

  this->qfeDrawParticles(this->particlePositionsVBO[currentBuffer], this->particleAttributesVBO[currentBuffer], MAX_PARTICLES);

  this->shaderProgramRenderEllipsoid->qfeDisable(); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeRenderStyleTrail(int currentBuffer)
{
  this->shaderProgramRenderTrail->qfeGetAttribLocation("qfe_ParticlePosition",  this->locationPosition);
  this->shaderProgramRenderTrail->qfeGetAttribLocation("qfe_ParticleAttribute", this->locationAttribute);

  this->shaderProgramRenderTrail->qfeEnable();

  this->qfeDrawParticles(this->particlePositionsVBO[currentBuffer], this->particleAttributesVBO[currentBuffer], MAX_PARTICLES);

  this->shaderProgramRenderTrail->qfeDisable(); 

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeBindTextures()
{
  GLuint        tids[5];
  int           phasesCount;

  phasesCount = (int)this->volumeSeries.size();

  if(phasesCount > 0) 
  {
    // We take a range [phase-2, phase+2] datasets, surrounding the current phase 
    int index;
    for(int i=0; i<5; i++)
    {    
      index = ((int)floor(this->phaseCurrent))+i-2;
      if(index < 0) index = phasesCount + index - 1;
      else          index = index % phasesCount;

      this->volumeSeries[index]->qfeGetVolumeTextureId(tids[i]);    
    }  

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_3D, tids[0]);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_3D, tids[1]);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_3D, tids[2]);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_3D, tids[3]);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_3D, tids[4]);
  }

  glActiveTexture(GL_TEXTURE8);
  glBindTexture(GL_TEXTURE_2D, this->particleNormalMap);

  glActiveTexture(GL_TEXTURE9);
  glBindTexture(GL_TEXTURE_1D, this->particleColorMap);

  glActiveTexture(GL_TEXTURE10);
  glBindTexture(GL_TEXTURE_1D, this->colormap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeBindTextureSliding()
{
  GLuint        tids[5];
  int           phasesCount;

  phasesCount = (int)this->volumeSeries.size();

  if(phasesCount > 0)
  {    
    // We take a range [phase-2, phase+2] datasets, surrounding the current phase
    int index;
    for(int i=-2; i<=2; i++)
    {
      if(phasesCount == 1)
        index = 0;
      else
      {
        index = ((int)floor(this->phaseCurrent))+i;
        qfeMath::qfeMod(index, phasesCount, index);
      }      

      this->volumeSeries[index]->qfeGetVolumeTextureId(tids[i+2]);
    }    

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_3D, tids[0]);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_3D, tids[1]);

    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_3D, tids[2]);

    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_3D, tids[3]);

    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_3D, tids[4]);
  }

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeBindTextureRender()
{ 
  glActiveTexture(GL_TEXTURE8);
  glBindTexture(GL_TEXTURE_2D, this->particleNormalMap);

  glActiveTexture(GL_TEXTURE9);
  glBindTexture(GL_TEXTURE_1D, this->particleColorMap);

  glActiveTexture(GL_TEXTURE10);
  glBindTexture(GL_TEXTURE_1D, this->colormap);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeUnbindTextures()
{
  glBindTexture  (GL_TEXTURE_1D, 0);
  glBindTexture  (GL_TEXTURE_2D, 0);
  glBindTexture  (GL_TEXTURE_3D, 0);

  return qfeSuccess;
}


//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetStaticUniformLocationsEvolve()
{ 
  this->shaderProgramEvolution->qfeSetUniform3i("volumeDimensions", this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);      
  this->shaderProgramEvolution->qfeSetUniformMatrix4f("voxelPatientMatrix", 1, this->V2P);
  this->shaderProgramEvolution->qfeSetUniformMatrix4f("patientVoxelMatrix", 1, this->P2V);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetStaticUniformLocationsAdvect()
{   
  this->shaderProgramAdvection->qfeSetUniform1i("volumeData0", 1);
  this->shaderProgramAdvection->qfeSetUniform1i("volumeData1", 2);
  this->shaderProgramAdvection->qfeSetUniform1i("volumeData2", 3);
  this->shaderProgramAdvection->qfeSetUniform1i("volumeData3", 4);
  this->shaderProgramAdvection->qfeSetUniform1i("volumeData4", 5);
  this->shaderProgramAdvection->qfeSetUniform3i("volumeDimensions", this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);      
  this->shaderProgramAdvection->qfeSetUniformMatrix4f("patientVoxelMatrix", 1, this->P2V);
  this->shaderProgramAdvection->qfeSetUniform1i("phaseCount", (int)this->volumeSeries.size());  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetStaticUniformLocationsRender()
{   
  this->shaderProgramRenderSphere->qfeSetUniform1i("particleNormalMap", 8);
  this->shaderProgramRenderSphere->qfeSetUniform1i("particleTransferFunction", 10);

  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("volumeData0", 2);
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("volumeData1", 3);
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("volumeData2", 4);
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("particleNormalMap", 8);
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("particleTransferFunction", 10);

  this->shaderProgramRenderTrail->qfeSetUniform1i("volumeData0", 2);
  this->shaderProgramRenderTrail->qfeSetUniform1i("volumeData1", 3);  
  this->shaderProgramRenderTrail->qfeSetUniform1i("volumeData2", 4);

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetDynamicUniformLocationsEvolve()
{   
  this->shaderProgramEvolution->qfeSetUniform1f("particleDeathAge",    (float)this->traceLifeTime);
  this->shaderProgramEvolution->qfeSetUniform1f("phaseCurrent",         this->phaseCurrent);   
  
  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetDynamicUniformLocationsAdvect()
{   
  this->shaderProgramAdvection->qfeSetUniform1f("phaseCurrent",         this->phaseCurrent);
  this->shaderProgramAdvection->qfeSetUniform1i("traceSteps",           this->traceSteps);
  this->shaderProgramAdvection->qfeSetUniform1f("traceStepSize",        this->traceStepSize);  
  this->shaderProgramAdvection->qfeSetUniform1f("traceStepDuration",    this->traceStepDuration);    
  this->shaderProgramAdvection->qfeSetUniform1i("traceScheme",          this->traceScheme); 
  this->shaderProgramAdvection->qfeSetUniform1i("traceSpeed",           this->traceSpeed); 
  this->shaderProgramAdvection->qfeSetUniform1i("traceDirection",       this->traceDirection);
  this->shaderProgramAdvection->qfeSetUniformMatrix4f("modelViewProjectionInverse", 1, this->matrixModelViewProjectionInverse);  

  return qfeSuccess;
}

//----------------------------------------------------------------------------
qfeReturnStatus qfeFlowParticleTrace::qfeSetDynamicUniformLocationsRender()
{   
  qfeFloat trailStepSize;
  qfeFloat trailStepDuration;
  qfePoint  lightSpec;
  qfeVector lightDir;
  qfeFloat  vu, vl;

  lightSpec.x = 0.1;
  lightSpec.y = 0.3;
  lightSpec.z = 0.3;
  lightSpec.w = 10.0;

  lightDir.x  =  0.0;
  lightDir.y  =  0.0;
  lightDir.z  =  1.0;

  if(this->light != NULL)
  {
    this->light->qfeGetLightSourceAmbientContribution(lightSpec.x);
    this->light->qfeGetLightSourceDiffuseContribution(lightSpec.y);
    this->light->qfeGetLightSourceSpecularContribution(lightSpec.z);
    this->light->qfeGetLightSourceSpecularPower(lightSpec.w); 
    this->light->qfeGetLightSourceDirection(lightDir);
  }

   // Obtain the dataset venc
  if((int)this->volumeSeries.size() > 0)     
  {
    this->volumeSeries.front()->qfeGetVolumeValueDomain(vl, vu);
  }
  else
  {
    vl = -200.0;
    vu = 200.0;
  }

  trailStepSize     = this->trailLifeTime / 5.0;
  trailStepDuration = trailStepSize * this->phaseDuration;

  this->shaderProgramRenderSphere->qfeSetUniform4f("light",                               lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderSphere->qfeSetUniform4f("lightDirection",                      lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramRenderSphere->qfeSetUniform2f("dataRange",                           vl, vu);
  this->shaderProgramRenderSphere->qfeSetUniform1i("particleSize",                        this->particleSize);
  this->shaderProgramRenderSphere->qfeSetUniform1i("phaseCount",                          (int)this->volumeSeries.size());
  this->shaderProgramRenderSphere->qfeSetUniform1i("colorType",                           (int)this->particleColor);
  this->shaderProgramRenderSphere->qfeSetUniform1i("colorScale",                          this->colorScale);  
  this->shaderProgramRenderSphere->qfeSetUniform1i("shading",                             (int)this->particleShading);  
  this->shaderProgramRenderSphere->qfeSetUniform1i("contour",                             (int)this->particleContour);  
  this->shaderProgramRenderSphere->qfeSetUniformMatrix4f("modelView",                     1, this->matrixModelView);
  this->shaderProgramRenderSphere->qfeSetUniformMatrix4f("modelViewProjection",           1, this->matrixModelViewProjection);
  this->shaderProgramRenderSphere->qfeSetUniformMatrix4f("modelViewProjectionInverse",    1, this->matrixModelViewProjectionInverse);        

  this->shaderProgramRenderEllipsoid->qfeSetUniform4f("light",                            lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderEllipsoid->qfeSetUniform4f("lightDirection",                   lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramRenderEllipsoid->qfeSetUniform2f("dataRange",                        vl, vu);
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("particleSize",                     this->particleSize);
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("phaseCount",                       (int)this->volumeSeries.size());
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("colorType",                        (int)this->particleColor);
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("colorScale",                       this->colorScale);
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("shading",                          (int)this->particleShading);
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("contour",                          (int)this->particleContour);
  this->shaderProgramRenderEllipsoid->qfeSetUniform3i("volumeDimensions",                 this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);      
  this->shaderProgramRenderEllipsoid->qfeSetUniform1f("phaseCurrent",                     this->phaseCurrent);      
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("setCount",                         this->setCount);      
  this->shaderProgramRenderEllipsoid->qfeSetUniform1i("particleColorMap",                 9);      
  this->shaderProgramRenderEllipsoid->qfeSetUniformMatrix4f("modelView",                  1, this->matrixModelView);
  this->shaderProgramRenderEllipsoid->qfeSetUniformMatrix4f("projection",                 1, this->matrixProjection);
  this->shaderProgramRenderEllipsoid->qfeSetUniformMatrix4f("modelViewInverse",           1, this->matrixModelViewInverse);
  this->shaderProgramRenderEllipsoid->qfeSetUniformMatrix4f("modelViewProjection",        1, this->matrixModelViewProjection);
  this->shaderProgramRenderEllipsoid->qfeSetUniformMatrix4f("modelViewProjectionInverse", 1, this->matrixModelViewProjectionInverse);      
  this->shaderProgramRenderEllipsoid->qfeSetUniformMatrix4f("patientVoxelMatrix",         1, this->P2V);
      
  this->shaderProgramRenderTrail->qfeSetUniform1f("phaseCurrent",                         this->phaseCurrent);
  this->shaderProgramRenderTrail->qfeSetUniform1f("trailStepSize",                        trailStepSize);  
  this->shaderProgramRenderTrail->qfeSetUniform1f("trailStepDuration",                    trailStepDuration);   
  this->shaderProgramRenderTrail->qfeSetUniform1i("trailType",                            (int)this->trailStyle);   
  this->shaderProgramRenderTrail->qfeSetUniform1i("traceDirection",                       this->traceDirection);
  this->shaderProgramRenderTrail->qfeSetUniform3i("volumeDimensions",                     this->volumeSize[0], this->volumeSize[1], this->volumeSize[2]);     
  this->shaderProgramRenderTrail->qfeSetUniform1i("particleSize",                         this->particleSize);
  this->shaderProgramRenderTrail->qfeSetUniform1i("colorScale",                           this->colorScale);
  this->shaderProgramRenderTrail->qfeSetUniform2f("dataRange",                            vl, vu);
  this->shaderProgramRenderTrail->qfeSetUniform4f("light",                                lightSpec.x, lightSpec.y, lightSpec.z, lightSpec.w);
  this->shaderProgramRenderTrail->qfeSetUniform4f("lightDirection",                       lightDir.x, lightDir.y, lightDir.z, lightDir.w);
  this->shaderProgramRenderTrail->qfeSetUniformMatrix4f("projection",                     1, this->matrixProjection);
  this->shaderProgramRenderTrail->qfeSetUniformMatrix4f("modelView",                      1, this->matrixModelView);
  this->shaderProgramRenderTrail->qfeSetUniformMatrix4f("modelViewProjection",            1, this->matrixModelViewProjection);
  this->shaderProgramRenderTrail->qfeSetUniformMatrix4f("modelViewProjectionInverse",     1, this->matrixModelViewProjectionInverse);  
  this->shaderProgramRenderTrail->qfeSetUniformMatrix4f("patientVoxelMatrix",             1, this->P2V);

  return qfeSuccess;
}

