#pragma once

#include "qfeAlgorithm.h"
#include "qfeVolume.h"

/**
* \file   qfeVectorMedianFilter.h
* \author Arjan Broos
* \class  qfeVectorMedianFilter
* \brief  Algorithm that filters the flow data based on the spatial neighborhood of a voxel
* \note   Confidential
*
* For every voxel, assign the voxel in its neighborhood that has the smallest difference
* vector to others in the neighborhood
*
*/
class qfeVectorMedianFilter : public qfeAlgorithm {
public:
  qfeVectorMedianFilter();

  void qfeSetData(qfeVolume *volume);
  void qfeUpdate();
  void qfeApplyFilter(qfeVolumeSeries *velocityData, bool inverted);

private:
  void FilterMedian(qfeVolume *volume);
};