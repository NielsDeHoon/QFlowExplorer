#pragma once

#include "qfeVector.h"
#include "qfeClusterNode.h"
#include "qfeClusterNodeElliptic.h"

/**
* \file   qfeClusterNodeElliptic4D.h
* \author Sander Jacobs
* \class  qfeClusterNodeElliptic
* \brief  Implementation of a cluster node using the Elliptic cluster metric proped by Telea et al.
* \note   Confidential
*/


class qfeClusterNodeElliptic4D : public qfeClusterNode
{
public:
  qfeClusterNodeElliptic4D(qfeClusterPoint point);
  ~qfeClusterNodeElliptic4D();

  qfeReturnStatus qfeMergeRepresentative(qfeClusterNode *c2);
  qfeFloat        qfeComputeDistanceTo(qfeClusterNode *c2);

  static double maxSpatialDistance;
  static double maxDirectionalCost;
  static double A;
  static double B;

private:
  qfeFloat   meanPosition [4];
  qfeFloat   meanFlow [4];
  unsigned int  size;
};





