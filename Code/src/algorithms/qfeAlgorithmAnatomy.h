#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithm.h"

/**
* \file   qfeAlgorithmAnatomy.h
* \author Roy van Pelt
* \class  qfeAlgorithmAnatomy
* \brief  Abstract class for a driver algorithm
* \note   Confidential
*
* Abstract class for driver algorithm.
* A driver processor outsources a part of the rendering work in the driver.
* The rendering work may be GPU based using GLSL
*
*/

class qfeAlgorithmAnatomy : public qfeAlgorithm
{
public:
  qfeAlgorithmAnatomy();
  ~qfeAlgorithmAnatomy();

};
