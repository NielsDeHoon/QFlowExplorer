#include "qfeDistanceListNode.h"

//----------------------------------------------------------------------------
//  Constructors
//----------------------------------------------------------------------------
qfeDistanceListNode::qfeDistanceListNode()
{
  this->prevNodePtr = NULL;
  this->nextNodePtr = NULL;

  this->c1          = NULL;
  this->c2          = NULL;
  this->d           = -1;
};

qfeDistanceListNode::qfeDistanceListNode(qfeDistanceListNode *prevDistNode)
{
  qfeInsertDistance(prevDistNode);

  this->c1          = NULL;
  this->c2          = NULL;
  this->d           = -1;
};

qfeDistanceListNode::qfeDistanceListNode(qfeClusterNode *c1, qfeClusterNode *c2)
{
  this->prevNodePtr = NULL;
  this->nextNodePtr = NULL;

  qfeSetClusters(c1, c2);

  this->d           = -1;
};

//----------------------------------------------------------------------------
//  Destructor
//----------------------------------------------------------------------------
qfeDistanceListNode::~qfeDistanceListNode()
{
  // If distance node is part of a list, remove it from the list properly
  this->qfeRemoveFromList();
};

//----------------------------------------------------------------------------
//  Methods
//----------------------------------------------------------------------------

// Insert current distance entry after the specified pointer
qfeReturnStatus qfeDistanceListNode::qfeInsertDistance(qfeDistanceListNode* prevDistNode)
{
  if(this==prevDistNode)
    return qfeSuccess;
  
  this->prevNodePtr = prevDistNode;

  if(prevDistNode == NULL)
  {
    this->nextNodePtr = NULL;
  }
  else
  {
    this->nextNodePtr              = prevDistNode->nextNodePtr;
    prevDistNode->nextNodePtr      = this;
    if(this->nextNodePtr != NULL)
    {
      this->nextNodePtr->prevNodePtr = this;
    }
  }

  return qfeSuccess;
};


// Remove reference to this distance entry from the list
qfeReturnStatus qfeDistanceListNode::qfeRemoveFromList()
{
  if(this->prevNodePtr != NULL)
    this->prevNodePtr->nextNodePtr = this->nextNodePtr;
    

  if(this->nextNodePtr != NULL)
    this->nextNodePtr->prevNodePtr = this->prevNodePtr;

  this->prevNodePtr = NULL;
  this->nextNodePtr = NULL;

  return qfeSuccess;
};

qfeReturnStatus qfeDistanceListNode::qfeMoveDistanceTo(qfeDistanceListNode* prevDistNode)
{
  if(this==prevDistNode)
    return qfeSuccess;

  this->qfeRemoveFromList();
  this->qfeInsertDistance(prevDistNode);

  return qfeSuccess;
};

qfeDistanceListNode* qfeDistanceListNode::qfeGetPrevDistanceNode()
{
  return this->prevNodePtr;
};

qfeDistanceListNode* qfeDistanceListNode::qfeGetNextDistanceNode()
{
  return this->nextNodePtr;
};

int qfeDistanceListNode::qfeGetClusterLabel1()
{
  if(this->c1 == NULL)
    return -1;
  else
    return this->c1->qfeGetClusterLabel();
};

int qfeDistanceListNode::qfeGetClusterLabel2()
{
  if(this->c2 == NULL)
    return -1;
  else
    return this->c2->qfeGetClusterLabel();
};

qfeReturnStatus qfeDistanceListNode::qfeSetClusters(qfeClusterNode* c1, qfeClusterNode* c2)
{
  this->c1 = c1;
  this->c2 = c2;

  if(this->c1 != NULL)
    this->c1->qfeAddDistancePointer(c2,this);

  if(this->c2 != NULL)
    this->c2->qfeAddDistancePointer(c1,this);

  return qfeSuccess;
};

qfeReturnStatus qfeDistanceListNode::qfeChangeCluster(qfeClusterNode* cOld, qfeClusterNode* cNew)
{
  if(this->c1 == cOld)
    this->c1 = cNew;

  if(this->c2 == cOld)
    this->c2 = cNew;

  return qfeSuccess;
}

qfeReturnStatus qfeDistanceListNode::qfeMergeClusters(qfeDistanceListNode* &firstDistance, qfeDistanceListNode* &lastDistance)
{
  if(c1->qfeGetNeighbourCount() > c2->qfeGetNeighbourCount())
  {
    c1->qfeMergeToCluster(c2,firstDistance,lastDistance);
    delete c2;
  }
  else
  {
    c2->qfeMergeToCluster(c1,firstDistance,lastDistance);
    delete c1;
  }
  return qfeSuccess;
}
  
qfeFloat qfeDistanceListNode::qfeGetDistance()
{
  if(this->d == -1)
    this->qfeEvaluateDistance();

  return this->d;
};

qfeReturnStatus qfeDistanceListNode::qfeEvaluateDistance()
{
  if(this->d == -1)
    this->d = c1->qfeComputeDistanceTo(c2);

  return qfeSuccess;
};

qfeReturnStatus qfeDistanceListNode::qfeUnevaluateDistance()
{
  this->d = -1;
  return qfeSuccess;
};

qfeReturnStatus qfeDistanceListNode::qfeInvalidateDistance()
{
  this->d = -2;
  return qfeSuccess;
};

bool qfeDistanceListNode::qfeIsDistanceUnevaluated()
{
  return(this->d > -1.5 && this->d < -0.5);
}

qfeReturnStatus qfeDistanceListNode::qfePrintDistance()
{
  //cout << this << " -> " << this->c1 << ", " << this->c2 << " -> ";
  cout << "d(";
  if(this->c1==NULL)
    cout << "NULL,";
  else
    cout << this->c1->qfeGetClusterLabel() << ",";
  if(this->c2==NULL)
    cout << "NULL";
  else
    cout << this->c2->qfeGetClusterLabel();
  cout << ") = " << this->d;

  /*

  if(this->prevNodePtr != NULL)
  {
    cout << "\tprev: d(";
    if(this->prevNodePtr->c1==NULL)
      cout << "NULL,";
    else
      cout << this->prevNodePtr->c1->label << ",";
    if(this->prevNodePtr->c2==NULL)
      cout << "NULL";
    else
      cout << this->prevNodePtr->c2->label;
    cout << ") = " << this->prevNodePtr->d;
  }

  if(this->nextNodePtr != NULL)
  {
    cout << "\tnext: d(";
    if(this->nextNodePtr->c1==NULL)
      cout << "NULL,";
    else
      cout << this->nextNodePtr->c1->label << ",";
    if(this->nextNodePtr->c2==NULL)
      cout << "NULL";
    else
      cout << this->nextNodePtr->c2->label;
    cout << ") = " << this->nextNodePtr->d;
  }

  */

  cout << endl;
  return qfeSuccess;
};