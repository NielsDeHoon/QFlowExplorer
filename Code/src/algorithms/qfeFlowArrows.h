#pragma once

#include "qflowexplorer.h"

#include <vector>

#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>

#include "qfeAlgorithmFlow.h"
#include "qfeColorMap.h"
#include "qfeGLShaderProgram.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeQuantification.h"
#include "qfeTransform.h"
#include "qfeVector.h"
#include "qfeVolume.h"

/**
* \file   qfeFlowArrows.h
* \author Roy van Pelt
* \class  qfeFlowArrows
* \brief  Implements rendering of flow rate arrows
* \note   Confidential
*
* Rendering of flow rate arrows
*
*/
using namespace std;

class qfeFlowArrows : public qfeAlgorithmFlow
{
public:
  qfeFlowArrows();
  qfeFlowArrows(const qfeFlowArrows &fv);
  ~qfeFlowArrows();  
  
  //qfeReturnStatus qfeRenderBigArrowGPU(int currentRing, int currentPhase, bool trail); 
  //qfeReturnStatus qfeRenderBigArrowGPU(ADCenterlineRing *ring, qfeVector vec, qfePoint pos);  
  //qfeReturnStatus qfeRenderBigArrowCPU(ADCenterlineRing *ring, qfeVolume *volume);  

  qfeReturnStatus qfeSetArrowColor(qfeColorRGBA color);
  qfeReturnStatus qfeSetArrowScale(double scale);
  qfeReturnStatus qfeSetArrowLineWidth(int width);
  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);

protected:

  qfeReturnStatus qfeDrawArrow(qfePoint origin, qfeVector axisX, qfeVector axisY, qfeFloat baseWidth, qfeFloat baseHeight, qfeFloat headWidth, qfeFloat headHeight);

  //qfeReturnStatus qfeComputeMaximumVelocity(ADCenterlineRing *ring, qfeVolume *volume, int percentage, qfeVector &v, qfePoint &p);
  //qfeReturnStatus qfeComputeFlowRate(ADCenterlineRing *ring, qfeVolume *volume, qfeFloat &flowRate);

  // \todo move this into a specific quantification class, based on flow probe 2D
  //qfeReturnStatus qfePrecomputeBigArrows(qfeRingGroup rings, qfeVolumeSeries series, int maxperc, qfeVectorGroupSeries &arrows, qfePointGroupSeries &positions);
  //qfeReturnStatus qfePrecomputeMaxVelocities(qfeRingGroup rings, qfeVolumeSeries series, int maxperc,  qfeVectorGroupSeries &velocityDirections, qfePointGroupSeries &velocityPositions);
  //qfeReturnStatus qfePrecomputeMaxVelocities(qfeRingGroup rings, qfeVolume *volume, int maxperc, qfeVectorGroup &velocityDirections, qfePointGroup &velocityPositions);
  //qfeReturnStatus qfePrecomputeFlowRates(qfeRingGroup rings, qfeVolumeSeries series,  qfeFloatGroupSeries &flowrate);
  //qfeReturnStatus qfePrecomputeFlowRates(qfeRingGroup rings, qfeVolume *volume, qfeFloatGroup &flowrate);

  //bool qfeInsideRing(ADCenterlineRing *ring, qfePoint p);
  void qfeProjectPointToPlane(qfeVector x, qfeVector y, qfePoint o, qfePoint p3D, qfePoint &p2D);
  bool qfeIntersectLines(qfePoint a, qfePoint b, qfePoint c, qfePoint d, qfePoint &p);

  qfeReturnStatus qfeBindTextures(qfeVolume *volume, qfeColorMap *colormap);
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

private :

  bool                firstRender;
  qfeGLShaderProgram *shaderProgram;
  qfeLightSource     *light;

  // Static uniform locations
  qfeMatrix4f  V2P, P2V;
  unsigned int volumeSize[3];
  int          phaseDuration;
  double       paramLineWidth;

  // Dynamic uniform locations
  qfeColorRGBA  paramArrowColor;
  double        paramArrowScale;

};
