#pragma once

#include "qflowexplorer.h"

#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>

#include <vtkTriangleFilter.h>
#include <vtkExtractEdges.h>

#include "qfeAlgorithmAnatomy.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeGLShaderProgram.h"
#include "qfeTransform.h"
#include "qfeVector.h"
#include "qfeVolume.h"

enum qfeSurfaceShadingType
{
  qfeSurfaceShadingToon1,
  qfeSurfaceShadingToon2,
  qfeSurfaceShadingDiffuse,
  qfeSurfaceShadingFresnel,
  qfeSurfaceShadingFlat,
  qfeSurfaceShadingXRay,
  qfeSurfaceShadingGooch
};

/**
* \file   qfeSurfaceShading.h
* \author Roy van Pelt
* \class  qfeSurfaceShading
* \brief  Implements shading strategies for meshes
* \note   Confidential
*
* Rendering of surfaces with various shading strategies
*
*/
using namespace std;

class qfeSurfaceShading : qfeAlgorithmAnatomy
{
public:
  qfeSurfaceShading();
  qfeSurfaceShading(const qfeSurfaceShading &surfshad);
  ~qfeSurfaceShading();

  qfeReturnStatus qfeSetSurfaceColor(qfeColorRGB color);
  qfeReturnStatus qfeSetSurfaceCulling(bool culling);
  qfeReturnStatus qfeSetSurfaceShading(qfeSurfaceShadingType type);

  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);
  
  qfeReturnStatus qfeRenderMesh(vtkPolyData *mesh, qfeVolume *volume);
  qfeReturnStatus qfeRenderMesh(qfeMeshStrips *vertexBuffer, qfeMeshStrips *normalBuffer, qfeVolume *volume);    
  qfeReturnStatus qfeRenderMesh(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshIndices *triangles, qfeVolume *volume);  
  qfeReturnStatus qfeRenderMesh(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshStripsIndices *triangleStrips, qfeVolume *volume);  
  qfeReturnStatus qfeRenderMeshCulling(vtkPolyData *mesh, qfeVolume *volume, qfeColorRGB color, bool clipping, int type);  
  qfeReturnStatus qfeRenderMeshDilate(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshIndices *triangles, float offset, qfeVolume *volume);  
  
  qfeReturnStatus qfeRenderDebug(vtkPolyData *mesh, qfeVolume *volume);
  qfeReturnStatus qfeRenderDebug(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshStripsIndices *indices, qfeVolume *volume);  
    
  qfeReturnStatus qfeDrawMeshTriangles(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshIndices *indices);  
  qfeReturnStatus qfeDrawMeshTriangleStrips(vtkPolyData *mesh, qfeColorRGB color);  
  qfeReturnStatus qfeDrawMeshTriangleStrips(qfeMeshStrips *vertexBuffer, qfeMeshStrips *normalBuffer, qfeColorRGB color);
  qfeReturnStatus qfeDrawMeshTriangleStrips(qfeMeshVertices *vertexBuffer, qfeMeshVertices *normalBuffer, qfeMeshStripsIndices *indices);  

  qfeReturnStatus qfeDrawMeshNormals(vtkPolyData *mesh);
  qfeReturnStatus qfeDrawMeshCurvature(vtkPolyData *mesh);
  qfeReturnStatus qfeDrawMeshWireframe(vtkPolyData *mesh);

protected:

  qfeReturnStatus qfeCreateTextures();
  qfeReturnStatus qfeDeleteTextures();
  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

private : 

  bool                  firstRender;
  qfeGLShaderProgram   *shaderSilhouette;
  qfeGLShaderProgram   *shaderCulling;
  qfeGLShaderProgram   *shaderFresnel;
  qfeGLShaderProgram   *shaderDilate;
  GLuint                texLighting;
  qfeLightSource       *light;

  // Static uniforms
  qfeMatrix4f           v2p;

  // Dynamic uniforms
  qfeColorRGB           color;  
  bool                  culling;
  float                 dilation;
  qfeSurfaceShadingType shading;  
  bool                  wssAvailable;

  qfeReturnStatus qfeGetMeshConnectedPoints(vtkPolyData *edges, int id, vtkIdList *neighbors);


};
