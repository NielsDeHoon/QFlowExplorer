#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithm.h"
#include "qfeColorMap.h"
#include "qfeDisplay.h"
#include "qfeDriver.h"
#include "qfeGLShaderProgram.h"
#include "qfeGeometry.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeRaycasting.h"
#include "qfeTransform.h"
#include "qfeVolume.h"

#ifndef PI
#define PI 3.1415926535897932384626433832795
#endif

/**
* \file   qfeRayCastingVector.h
* \author Roy van Pelt
* \class  qfeRayCastingVector
* \brief  Implements a raycaster
* \note   Confidential
*
* Rendering a volume using vectorfield raycasting
*
*/
class qfeRayCastingVector : public qfeRayCasting
{
public:
  enum qfeRaycastingVectorFocus
  {
    qfeDataFocusDoppler,
    qfeDataFocusAngle,
    qfeDataFocusAngleDeriv,
    qfeDataFocusCoherence,
    qfeDataFocusVorticity
  };

   enum qfeRaycastingVectorContext
  {
    qfeDataContextDepth,
    qfeDataContextDVR,
    qfeDataContextNone
  };

  qfeRayCastingVector();
  qfeRayCastingVector(const qfeRayCastingVector &pr);
  ~qfeRayCastingVector();
  
  qfeReturnStatus qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *volume, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *flowVolume, qfeVolume *contextVolume, qfeColorMap *colormap);

  qfeReturnStatus qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *volume, qfeProbe3D *probe, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *flowVolume, qfeVolume *contextVolume, qfeProbe3D *probe, qfeColorMap *colormap);  

  qfeReturnStatus qfeRenderCylinder(qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, qfeFloat length, qfeFloat baseRadius, qfeFloat topRadius);   
  qfeReturnStatus qfeRenderCylinder(qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, qfeFloat length, qfeFloat baseRadius, qfeFloat topRadius, qfeColorRGBA color);   
  qfeReturnStatus qfeRenderBox(qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, qfeFloat length, qfeFloat baseRadius, qfeFloat topRadius);   

  qfeReturnStatus qfeRenderCylinderHalo(qfeVolume *volume, qfeProbe3D *probe, qfeColorRGBA color);   
  qfeReturnStatus qfeRenderCylinderTick(qfeVolume *volume, qfeProbe3D *probe, qfeColorRGBA color);

  qfeReturnStatus qfeSetSpeedThreshold(int threshold);
  qfeReturnStatus qfeSetDataRepresentationFocus(qfeRaycastingVectorFocus type);
  qfeReturnStatus qfeSetDataRepresentationContext(qfeRaycastingVectorContext type);
  
protected:

  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

  qfeReturnStatus qfeGetModelViewMatrix(qfeVolume *volume);

  qfeReturnStatus qfeDrawSphere(qfePoint o, qfeFloat r, qfeColorRGBA color);
  

private :
  bool                        firstRender;
  qfeGLShaderProgram         *shaderVFR;

  qfeColorMap                *colorMapUS;
  qfeColorMap                *colorMapCoolWarm;

  GLuint                      texContext;
  GLuint                      texColorMapUS;
  GLuint                      texColorMapCoolWarm;

  qfeVector                   paramDir;
  qfeFloat                    paramThreshold;
  qfeRaycastingVectorFocus    paramDataFocus;
  qfeRaycastingVectorContext  paramDataContext;

  qfeMatrix4f                 matrixModelViewProjection;

  qfeReturnStatus qfeDrawCylinder(GLfloat *vertCylinder,  GLfloat *vertCap, GLfloat *vertSphere,                                                       
                                  GLubyte *topoCylinder,  GLubyte *topoCap,                                                    
                                  GLfloat *colCylinder,   GLfloat *colCap,  GLfloat *colSphere,
                                  GLint    sizeCylinder,  GLint    sizeCap, GLint    sizeSphere);

  qfeReturnStatus qfeDrawCylinder(GLfloat *vertCylinder,  GLfloat *vertCap, GLfloat *vertSphere,                                                       
                                  GLubyte *topoCylinder,  GLubyte *topoCap,                                                                                  
                                  GLint    sizeCylinder,  GLint    sizeCap, GLint    sizeSphere,
                                  GLfloat  r, GLfloat  g, GLfloat  b, GLfloat a);

  qfeReturnStatus qfeCreateCylinderGeometry(GLfloat baseRadius, GLfloat topRadius, GLfloat length, GLint quality, GLfloat** vertices, GLint *size);
  qfeReturnStatus qfeCreateCylinderTopology(GLfloat baseRadius, GLfloat topRadius, GLint quality, GLubyte **topology, GLint *tsize);  
  qfeReturnStatus qfeCreateCylinderColors(GLfloat *geometry, GLint size, qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, GLfloat length, GLfloat** colors);

  qfeReturnStatus qfeCreateCapGeometry(GLfloat radius, GLint quality, GLfloat** vertices, GLint *size);
  qfeReturnStatus qfeCreateCapTopology(GLfloat radius, GLint quality, GLubyte** topology, GLint *tsize);
  qfeReturnStatus qfeCreateCapColors(GLfloat *geometry, GLint size, qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, GLfloat length, GLfloat **colors);

  qfeReturnStatus qfeCreateSphereGeometry(GLfloat topRadius, GLfloat length, GLint quality, GLfloat** vertices, GLint *size);
  qfeReturnStatus qfeCreateSphereColors(GLfloat *geometry, GLint size, qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, GLfloat length, GLfloat** colors);
  qfeReturnStatus qfeCreateSphereHandle(qfeFloat slices, qfeFloat stacks, GLfloat**sphere, GLfloat **normals, GLint *size);

  qfeReturnStatus qfeCreateBoxGeometry(GLfloat radius, GLfloat length, GLfloat** vertices, GLint *size);
  qfeReturnStatus qfeCreateBoxTopology(GLubyte** topology, GLint *tsize);
  qfeReturnStatus qfeCreateBoxColors(GLfloat *geometry, GLint size, qfeVolume *volume, qfePoint origin, qfeVector axisX, qfeVector axisZ, GLfloat length, GLfloat **colors);

  qfeReturnStatus qfeCreateColorMaps();
  
};
