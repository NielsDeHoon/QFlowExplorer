#include "qfeClusterNode.h"

int qfeClusterNode::nextClusterLabel = 0;
//----------------------------------------------------------------------------
//  Constructors
//----------------------------------------------------------------------------
qfeClusterNode::qfeClusterNode()
{
  this->clusterLabel = qfeClusterNode::nextClusterLabel++;
};

//----------------------------------------------------------------------------
//  Destructor
//----------------------------------------------------------------------------
qfeClusterNode::~qfeClusterNode()
{
};

//----------------------------------------------------------------------------
//  Methods
//----------------------------------------------------------------------------

qfeReturnStatus qfeClusterNode::qfeSetNextClusterLabel(int l)
{
  qfeClusterNode::nextClusterLabel = l;
  return qfeSuccess;
}

int qfeClusterNode::qfeGetNextClusterLabel()
{
  return qfeClusterNode::nextClusterLabel;
}

qfeReturnStatus qfeClusterNode::qfeIncrementNextClusterLabel()
{
  qfeClusterNode::nextClusterLabel++;
  return qfeSuccess;
}


qfeReturnStatus qfeClusterNode::qfeSetClusterLabel(int clusterLabel)
{
  this->clusterLabel = clusterLabel;
  return qfeSuccess;
};

int qfeClusterNode::qfeGetClusterLabel()
{
  return this->clusterLabel;
};

int qfeClusterNode::qfeGetNeighbourCount()
{
  //cout << "Neighbour Count " << this->label << " = " << this->distancePointers.size() << endl;
  return this->distancePointers.size();
};

qfeReturnStatus qfeClusterNode::qfeMergeDistances(qfeClusterNode *c)
{
  qfeDistanceListNode *currentDistance;

  map<qfeClusterNode*,qfeDistanceListNode*>::iterator dPairIt;
  dPairIt=this->distancePointers.find(c);
  if(dPairIt != this->distancePointers.end())
  {
    currentDistance = dPairIt->second;
    dPairIt->second->qfeInvalidateDistance();
    this->distancePointers.erase(dPairIt++);
  }
  else
  {
    cout << "ERROR: Could not find distance between merged clusters!" << endl;
    return qfeError;
  }

  // Unevaluate old distances from "this" cluster
  dPairIt = this->distancePointers.begin();
  while(dPairIt!=this->distancePointers.end())
  {
    dPairIt->second->qfeUnevaluateDistance();
    dPairIt++;
  }

  map<qfeClusterNode*,qfeDistanceListNode*>::iterator  nPairIt;
  qfeDistanceListNode*                                          nDistPtr;
  // Gather neighbours from "c" cluster, add new neighbours to "this" and unevaluate their distances
  // Distances to common neighbours are discarded
  dPairIt = c->distancePointers.begin();
  while(dPairIt!=c->distancePointers.end())
  {
    if(dPairIt->first != this && this->distancePointers.insert(*dPairIt).second == true)
    {
      // New neighbour distance was inserted successfully
      nPairIt = dPairIt->first->distancePointers.find(c);
      nDistPtr = nPairIt->second;
      dPairIt->first->distancePointers.erase(nPairIt);
      dPairIt->first->distancePointers.insert(pair<qfeClusterNode*,qfeDistanceListNode*>(this,nDistPtr));
      dPairIt->second->qfeChangeCluster(c,this);
      dPairIt->second->qfeUnevaluateDistance();
    }
    else
    {
      // A distance to this neighbour was already present
      dPairIt->first->distancePointers.erase(c);
      dPairIt->second->qfeInvalidateDistance();
    }
    dPairIt++;
  }
  return qfeSuccess;
};

qfeReturnStatus qfeClusterNode::qfeMergeAndMoveDistances(qfeClusterNode *c, qfeDistanceListNode* &firstDistance, qfeDistanceListNode* &lastDistance)
{
  map<qfeClusterNode*,qfeDistanceListNode*>::iterator dPairIt;

  // Find the distance between the clusters being currently merged
  qfeDistanceListNode* currentDistance;
  dPairIt=this->distancePointers.find(c);
  if(dPairIt != this->distancePointers.end())
  {
    currentDistance = dPairIt->second;
    dPairIt->second->qfeInvalidateDistance();
    this->distancePointers.erase(dPairIt++);
  }
  else
  {
    cout << "ERROR: Could not find distance between merged clusters!" << endl;
    return qfeError;
  }

  // Gather neighbours from "this" cluster, and unevaluate old distances
  dPairIt = this->distancePointers.begin();
  while(dPairIt!=this->distancePointers.end())
  {
    dPairIt->second->qfeUnevaluateDistance();

    // Move this distance to the back of distanceList
    // If we move around the first distance, make sure we update the distanceList bounds
    if(dPairIt->second == firstDistance)
      firstDistance = dPairIt->second->qfeGetNextDistanceNode();

    dPairIt->second->qfeMoveDistanceTo(lastDistance);
    lastDistance = dPairIt->second;
    dPairIt++;
  }

  map<qfeClusterNode*,qfeDistanceListNode*>::iterator  nPairIt;
  qfeDistanceListNode*                                          nDistPtr;
  // Gather neighbours from "c" cluster, add new neighbours to "this" and unevaluate their distances
  // Distances to common neighbours are discarded
  dPairIt = c->distancePointers.begin();
  while(dPairIt!=c->distancePointers.end())
  {
    if(dPairIt->first == this)
    {
      // Distance to 'host' cluster is no longer relevant
      (dPairIt++)->second->qfeInvalidateDistance();
    }
    else if(this->distancePointers.insert(*dPairIt).second == true)
    {
      //cout << "Client neighbour distance to " << dPairIt->first->label << " inserted successfully:" << endl;
      // New neighbour distance was inserted successfully
      nPairIt = dPairIt->first->distancePointers.find(c);
      nDistPtr = nPairIt->second;
      dPairIt->first->distancePointers.erase(nPairIt);
      dPairIt->first->distancePointers.insert(pair<qfeClusterNode*,qfeDistanceListNode*>(this,nDistPtr));
      dPairIt->second->qfeChangeCluster(c,this);
      dPairIt->second->qfeUnevaluateDistance();

      //dPairIt->second->qfePrintDistance();

      // Move this distance to the back of distanceList
      // If we move around the first distance, make sure we update the distanceList bounds
      if(dPairIt->second == firstDistance)
        firstDistance = dPairIt->second->qfeGetNextDistanceNode();

      dPairIt->second->qfeMoveDistanceTo(lastDistance);
      lastDistance = dPairIt->second;
      dPairIt++;
    }
    else
    {
      // A distance to this neighbour was already present
      dPairIt->first->distancePointers.erase(c);
      (dPairIt++)->second->qfeInvalidateDistance();
    }
  }
  //cout << "New ";
  //this->qfePrintDistances();
  return qfeSuccess;
};

qfeReturnStatus qfeClusterNode::qfeMergeToCluster(qfeClusterNode *c, qfeDistanceListNode* &firstDistance, qfeDistanceListNode* &lastDistance)
{
  //cout << "Merging cluster " << c->label << " into 'host' cluster " << this->label << " -> " << l << endl;

  //cout << "Host ";
  //this->qfePrintDistances();
  //cout << "Guest ";
  //c->qfePrintDistances();

  this->clusterLabel = qfeClusterNode::nextClusterLabel;
  qfeClusterNode::nextClusterLabel++;

  // Update the merged cluster representative
  this->qfeMergeRepresentative(c);
  
  // Merge cluster distances. If source list is specified, move the new distance to the back.
  if(firstDistance==NULL || lastDistance==NULL)
    return qfeMergeDistances(c);
  else
    return qfeMergeAndMoveDistances(c, firstDistance, lastDistance);
};

qfeReturnStatus qfeClusterNode::qfePrintDistances()
{
  cout << "Cluster " << this->clusterLabel << " distances:" << endl;
  map<qfeClusterNode*,qfeDistanceListNode*>::iterator dPairIt = this->distancePointers.begin();
  while(dPairIt!=this->distancePointers.end())
  {
    cout << "* " <<  dPairIt->first << " " << dPairIt->first->clusterLabel << " -> ";
    dPairIt->second->qfePrintDistance();
    dPairIt++;
  }
  return qfeSuccess;
};

qfeReturnStatus qfeClusterNode::qfeAddDistancePointer(qfeClusterNode* nPtr, qfeDistanceListNode* dPtr)
{
  //this->distancePairs.push_back(qfeClusterDistancePair(nPtr,dPtr));
  this->distancePointers.insert(pair<qfeClusterNode*, qfeDistanceListNode*>(nPtr,dPtr));
  return qfeSuccess;
};
