#pragma once

#include "qflowexplorer.h"

#include "qfeAlgorithm.h"
#include "qfeColorMap.h"
#include "qfeDisplay.h"
#include "qfeDriver.h"
#include "qfeGLShaderProgram.h"
#include "qfeGeometry.h"
#include "qfeLightSource.h"
#include "qfeMatrix4f.h"
#include "qfeTransform.h"
#include "qfeVolume.h"
#include "qfeLitSphereMap.h"

/**
* \file   qfeIllustrativeVolumeRendering.h
* \author Arjan Broos
* \class  qfeIllustrativeVolumeRendering
* \brief  Implements illustrative volume rendering
* \note   Confidential
*
* Implements illustrative volume rendering. Transparency that conserves structure.
* Contours. Illustrative rendering.
*
*/

class qfeIllustrativeVolumeRendering : qfeAlgorithm
{
public:
  qfeIllustrativeVolumeRendering();
  qfeIllustrativeVolumeRendering(const qfeIllustrativeVolumeRendering &pr);
  ~qfeIllustrativeVolumeRendering();

  qfeReturnStatus qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *volume, qfeColorMap *colormap);
  qfeReturnStatus qfeRenderRaycasting(GLuint colortex, GLuint depthtex, qfeVolume *boundingVolume, qfeVolume *textureVolume, qfeColorMap *colormap);

  qfeReturnStatus qfeSetViewportSize(unsigned int x, unsigned int y, int supersampling);
  qfeReturnStatus qfeSetIntersectionBuffers(GLuint color, GLuint depth);  
  qfeReturnStatus qfeSetDataRange(qfeFloat vl, qfeFloat vu);
  qfeReturnStatus qfeSetLightSource(qfeLightSource *light);
  qfeReturnStatus qfeSetMaxGradientLength(float length);
  qfeReturnStatus qfeSetDirectionalTransparencyExponent(float exponent);
  qfeReturnStatus qfeSetDepthTransparency(bool on);
  qfeReturnStatus qfeSetLitSphereMapRendering(bool on);
  qfeReturnStatus qfeSetLitSphereMapPreset(int preset);
  qfeReturnStatus qfeSetGradientSampling(bool on);
  qfeReturnStatus qfeSetDepthToneMapping(bool on);
  qfeReturnStatus qfeSetDepthToneMappingFactor(float factor);
  qfeReturnStatus qfeSetLsmNrDots(unsigned nrDots);
  qfeReturnStatus qfeSetLsmLambda(float lambda);
  qfeReturnStatus qfeCreateDotMap();
  qfeReturnStatus qfeSetVoITInv(const qfeMatrix4f &TInv);
  qfeReturnStatus qfeSetVoICenter(const qfePoint &center);
  qfeReturnStatus qfeSetClipVoI(bool on);
  qfeReturnStatus qfeSetSimpleOpacity(float opacity);
  qfeReturnStatus qfeSetEyePosition(qfePoint eye);

  qfeReturnStatus qfeRenderBoundingBox(qfeVolume *volume);
protected:  
  bool                firstRender;
  qfeGLShaderProgram *shaderIVR;

  qfeLightSource     *light;
  qfeMatrix4f         V2P;

  qfeLitSphereMap     lsm;

  qfePoint eyePos;

  unsigned int        viewportSize[2];
  int                 superSampling;

  GLuint              texRayEnd;
  GLuint              texRayEndDepth;
  GLuint              texVolume;
  GLuint              texColorMap;
  GLuint              texGradientMap;
  GLuint              texIntersectColor;
  GLuint              texIntersectDepth;  

  float               paramScaling[3];           
  float               paramStepSize;
  float               paramDataRange[2];

  float               maxGradLength;
  float               dirTransExp; // Directional transparency exponent
  bool                depthTransparency;
  bool                litSphereMapRendering;
  int                 lsmPreset;
  bool                gradientSampling;
  bool                depthToneMapping;
  float               depthToneMappingFactor;
  float               simpleOpacity;

  qfeMatrix4f         voiTInv;
  qfePoint            voiCenter;
  bool                useVoIClipping;
  float               w, h, d;

  qfeReturnStatus qfeCreateTextures();
  qfeReturnStatus qfeDeleteTextures();
  qfeReturnStatus qfeBindTextures();
  qfeReturnStatus qfeUnbindTextures();

  qfeReturnStatus qfeSetStaticUniformLocations();
  qfeReturnStatus qfeSetDynamicUniformLocations();

  //qfeReturnStatus qfeRenderBoundingBox(qfeVolume *volume);
  qfeReturnStatus qfeRenderBoundingBox(qfeVolume *boundingVolume, qfeVolume* textureVolume);

  qfeReturnStatus qfeClampTextureRange(qfePoint pos, qfePoint &newpos);  

  qfeReturnStatus qfeGetModelViewMatrix();

  GLfloat    matrixNormals[16];
  GLfloat    matrixModelViewInverse[16];
  GLfloat    matrixModelViewProjection[16];
  GLfloat    matrixModelViewProjectionInverse[16];
};
