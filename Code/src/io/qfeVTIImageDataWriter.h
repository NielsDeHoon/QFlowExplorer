/**
* \file   vtkXMLImageDataWriter.h
* \author Roy van Pelt
* \class  qfeVTIImageDataWriter
* \brief  Implements a data writer for qflow data
*
* vtkXMLImageDataWriter derives from vtkXMLImageDataWriter.
*
* The XML structured file (extension *.vti), contains
* the morphologic data in the scalar structure.
* The flow velocities are contained in the in the vector structure.
*/

#ifndef __qfeVTIImageDataWriter_h
#define __qfeVTIImageDataWriter_h

#include "vtkXMLImageDataWriter.h"
#include "qfeImageData.h"

class qfeImageData;

class qfeVTIImageDataWriter : public vtkXMLImageDataWriter
{
public:
  static qfeVTIImageDataWriter* New();
  vtkTypeRevisionMacro(qfeVTIImageDataWriter,vtkXMLImageDataWriter);
  void PrintSelf(ostream& os, vtkIndent indent);

  //BTX
  // Description:
  // Get/Set the writer's input.
  qfeImageData* GetInput();
  //ETX

protected:
  qfeVTIImageDataWriter();
  ~qfeVTIImageDataWriter();

  void WritePrimaryElementAttributes(ostream &os, vtkIndent indent);

private:
  qfeVTIImageDataWriter(const qfeVTIImageDataWriter&);  // Not implemented.
  void operator=(const qfeVTIImageDataWriter&);  // Not implemented.
};

#endif
