/**
* \file   qfeVTIImageDataReader.h
* \author Roy van Pelt
* \class  qfeVTIImageDataReader
* \brief  Implements a data reader for qflow data
*
* qfeVTIImageDataReader derives from vtkXMLImageDataReader.
*
* The XML structured file (extension *.vti), contains
* the morphologic data in the scalar structure.
* The flow velocities are contained in the in the vector structure.
*/

#ifndef __qfeVTIImageDataReader_h
#define __qfeVTIImageDataReader_h

#include "vtkXMLImageDataReader.h"

#include "qfeImageData.h"

class vtkImageData;

class qfeVTIImageDataReader : public vtkXMLImageDataReader
{
public:

  vtkTypeRevisionMacro(qfeVTIImageDataReader,vtkXMLImageDataReader);
  void PrintSelf(ostream& os, vtkIndent indent);
  static qfeVTIImageDataReader *New();

  // Description:
  // Get/Set the reader's output.
  void SetOutput(qfeImageData *output);
  qfeImageData *GetOutput();
  qfeImageData *GetOutput(int idx);

  // Description:
  // For the specified port, copy the information this reader sets up in
  // SetupOutputInformation to outInfo
  virtual void CopyOutputInformation(vtkInformation *outInfo, int port);

  static vtkInformationIntegerKey *PHASES();
protected:
  qfeVTIImageDataReader();
  ~qfeVTIImageDataReader();

  int                 Phases;
  double              PhaseDuration;
  double              PhaseTriggerTime;
  double              Geometry[16];
  double              AxisX[3];
  double              AxisY[3];
  double              AxisZ[3];
  double              Venc[3];
  qfeDataType         ScanType;
  qfeDataOrientation  ScanOrientation;

  int ReadPrimaryElement(vtkXMLDataElement* ePrimary);

  // Setup the output's information.
  void SetupOutputInformation(vtkInformation *outInfo);

  virtual int FillOutputPortInformation(int, vtkInformation*);

private:
  qfeVTIImageDataReader(const qfeVTIImageDataReader&);  // Not implemented.
  void operator=(const qfeVTIImageDataReader&);  // Not implemented.
};

#endif
