#include "qfeVTIImageDataReader.h"

#include "vtkDataArray.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkXMLDataElement.h"
#include "vtkInformation.h"
#include "vtkStreamingDemandDrivenPipeline.h"

vtkCxxRevisionMacro(qfeVTIImageDataReader, "$Revision: 1.11 $");
vtkStandardNewMacro(qfeVTIImageDataReader);

//----------------------------------------------------------------------------
qfeVTIImageDataReader::qfeVTIImageDataReader()
{
  qfeImageData *output = qfeImageData::New();
  this->SetOutput(output);
  // Releasing data for pipeline parallelism.
  // Filters will know it is empty.
  output->ReleaseData();
  output->Delete();
}

//----------------------------------------------------------------------------
qfeVTIImageDataReader::~qfeVTIImageDataReader()
{
}

//----------------------------------------------------------------------------
void qfeVTIImageDataReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
void qfeVTIImageDataReader::SetOutput(qfeImageData *output)
{
  this->GetExecutive()->SetOutputData(0, output);
}

//----------------------------------------------------------------------------
qfeImageData* qfeVTIImageDataReader::GetOutput()
{
  return this->GetOutput(0);
}

//----------------------------------------------------------------------------
qfeImageData* qfeVTIImageDataReader::GetOutput(int idx)
{
  return qfeImageData::SafeDownCast( this->GetOutputDataObject(idx) );
}


//----------------------------------------------------------------------------
int qfeVTIImageDataReader::ReadPrimaryElement(vtkXMLDataElement* ePrimary)
{
  if(!this->Superclass::ReadPrimaryElement(ePrimary)) { return 0; }

  // Get the patient name.
  const char *patientName = ePrimary->GetAttribute("PatientName");
  if(patientName != NULL)
    this->GetOutput()->SetPatientName(patientName);

  // Get the image's origin.
  if(ePrimary->GetVectorAttribute("Origin", 3, this->Origin) != 3)
    {
    this->Origin[0] = 0;
    this->Origin[1] = 0;
    this->Origin[2] = 0;
    }

  // Get the image spacing.
  if(ePrimary->GetVectorAttribute("Spacing", 3, this->Spacing) != 3)
    {
    this->Spacing[0] = 1;
    this->Spacing[1] = 1;
    this->Spacing[2] = 1;
    }

  // Get the image axes.
  if(ePrimary->GetVectorAttribute("AxisX", 3, this->AxisX) != 3)
  {
    this->AxisX[0] = 1;
    this->AxisX[1] = 0;
    this->AxisX[2] = 0;
  }
  this->GetOutput()->SetAxisX(this->AxisX);

  if(ePrimary->GetVectorAttribute("AxisY", 3, this->AxisY) != 3)
  {
    this->AxisY[0] = 0;
    this->AxisY[1] = 1;
    this->AxisY[2] = 0;
  }
  this->GetOutput()->SetAxisY(this->AxisY);

  if(ePrimary->GetVectorAttribute("AxisZ", 3, this->AxisZ) != 3)
  {
    this->AxisZ[0] = 0;
    this->AxisZ[1] = 0;
    this->AxisZ[2] = 1;
  }
  this->GetOutput()->SetAxisZ(this->AxisZ);

  // Get number of phases
  if(ePrimary->GetScalarAttribute("Phases", this->Phases) < 1)
  {
    this->Phases = 1;
  }
  this->GetOutput()->SetPhaseCount(this->Phases);

  if(ePrimary->GetScalarAttribute("PhaseDuration", this->PhaseDuration) < 1)
  {
    this->PhaseDuration = 40.0;
  }
  this->GetOutput()->SetPhaseDuration(this->PhaseDuration);

  ePrimary->GetScalarAttribute("PhaseTriggerTime", this->PhaseTriggerTime);
  this->GetOutput()->SetPhaseTriggerTime(this->PhaseTriggerTime);

  // Get the Venc
  if(ePrimary->GetVectorAttribute("Venc", 3, this->Venc) != 3)
  {
    this->Venc[0] = 200.0;
    this->Venc[1] = 200.0;
    this->Venc[2] = 200.0;
  }
  this->GetOutput()->SetVenc(this->Venc);

  // Scan type  
  if(ePrimary->GetScalarAttribute("ScanType", (int &)this->ScanType) < 1)
  {
    // Check legacy type
    if(ePrimary->GetScalarAttribute("Type", (int &)this->ScanType) < 1) 
    {
      // Neither, set default
      this->ScanType = qfeDataFlow4D;
    }   
  }
  this->GetOutput()->SetScanType(this->ScanType);

  // Scan orientation
  if(ePrimary->GetScalarAttribute("ScanOrientation", (int &)this->ScanOrientation) < 1)
  {
    this->ScanOrientation = qfeSagittal;
  }
  this->GetOutput()->SetScanOrientation(this->ScanOrientation);

  return 1;
}

//----------------------------------------------------------------------------
// Note that any changes (add or removing information) made to this method
// should be replicated in CopyOutputInformation
void qfeVTIImageDataReader::SetupOutputInformation(vtkInformation *outInfo)
{
  this->Superclass::SetupOutputInformation(outInfo);

  outInfo->Set(vtkDataObject::ORIGIN(), this->Origin, 3);
  outInfo->Set(vtkDataObject::SPACING(), this->Spacing, 3);
}


//----------------------------------------------------------------------------
void qfeVTIImageDataReader::CopyOutputInformation(vtkInformation *outInfo, int port)
{
  this->Superclass::CopyOutputInformation(outInfo, port);
  vtkInformation *localInfo = this->GetExecutive()->GetOutputInformation( port );

  if ( localInfo->Has(vtkDataObject::ORIGIN()) )
    {
    outInfo->CopyEntry( localInfo, vtkDataObject::ORIGIN() );
    }
  if ( localInfo->Has(vtkDataObject::SPACING()) )
    {
    outInfo->CopyEntry( localInfo, vtkDataObject::SPACING() );
    }
}


//----------------------------------------------------------------------------
int qfeVTIImageDataReader::FillOutputPortInformation(int, vtkInformation* info)
{
  info->Set(vtkDataObject::DATA_TYPE_NAME(), "qfeImageData");
  return 1;
}

