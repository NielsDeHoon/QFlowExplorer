#include "qfeVTIImageDataWriter.h"
#include "vtkObjectFactory.h"
#include "vtkInformation.h"

vtkCxxRevisionMacro(qfeVTIImageDataWriter, "$Revision: 1.7 $");
vtkStandardNewMacro(qfeVTIImageDataWriter);

//----------------------------------------------------------------------------
qfeVTIImageDataWriter::qfeVTIImageDataWriter()
{
}

//----------------------------------------------------------------------------
qfeVTIImageDataWriter::~qfeVTIImageDataWriter()
{
}

//----------------------------------------------------------------------------
void qfeVTIImageDataWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
qfeImageData* qfeVTIImageDataWriter::GetInput()
{
  return static_cast<qfeImageData*>(this->Superclass::GetInput());
}


//----------------------------------------------------------------------------
void qfeVTIImageDataWriter::WritePrimaryElementAttributes(ostream &os, vtkIndent indent)
{  
  this->Superclass::WritePrimaryElementAttributes(os, indent);
  qfeImageData* input = this->GetInput();

  this->WriteStringAttribute("PatientName",      input->GetPatientName());
  this->WriteStringAttribute("PatientID",        input->GetPatientID());
  this->WriteVectorAttribute("AxisX", 3,         input->GetAxisX());
  this->WriteVectorAttribute("AxisY", 3,         input->GetAxisY());
  this->WriteVectorAttribute("AxisZ", 3,         input->GetAxisZ());
  this->WriteScalarAttribute("Phases",           input->GetPhaseCount());
  this->WriteScalarAttribute("PhaseDuration",    input->GetPhaseDuration());
  this->WriteScalarAttribute("PhaseTriggerTime", input->GetPhaseTriggerTime());
  this->WriteVectorAttribute("Venc", 3,          input->GetVenc());
  this->WriteScalarAttribute("ScanType",         input->GetScanType());
  this->WriteScalarAttribute("ScanOrientation",  input->GetScanOrientation());

}
