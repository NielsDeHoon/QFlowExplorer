#include "qfeBlandAltmanPlot.h"

qfeBlandAltmanPlot::qfeBlandAltmanPlot()
{
	windowTitle = "Bland-Altman Plot";
}

qfeBlandAltmanPlot::~qfeBlandAltmanPlot()
{
}

void qfeBlandAltmanPlot::setData(QVector<double> sampleSet1, QVector<double> sampleSet2, QPair<QString, QString> labels)
{
	//this->addHorizontalLine(1.0,"lol",QColor(255,255,0),"","");
	if(sampleSet1.size() != sampleSet2.size() && sampleSet1.size()>0)
		return;

	QVector<double> x(sampleSet1.size());
	QVector<double> y(sampleSet1.size());

	double xmin = 1e6;
	double xmax = -1e6;
	double ymin = 1e6;
	double ymax = -1e6;

	double mean = 0.0;
	double sdev = 0.0;

	for(unsigned int i = 0; i<sampleSet1.size(); i++)
	{
		x[i] = (sampleSet1[i]+sampleSet2[i])/2.0;
		y[i] = sampleSet1[i] - sampleSet2[i];

		xmin = x[i] < xmin ? x[i] : xmin;
		xmax = x[i] > xmax ? x[i] : xmax;
		ymin = y[i] < ymin ? y[i] : ymin;
		ymax = y[i] > ymax ? y[i] : ymax;

		mean += y[i];
	}
	
	mean /= sampleSet1.size();

	for(unsigned int i = 0; i<sampleSet1.size(); i++)
	{
		sdev += pow(y[i]-mean, 2.0);
	}

	sdev /= x.size();

	std::cout<<"Number of samples: "<<sampleSet1.size()<<"Mean: "<<mean<<" Standard Deviation: "<<sdev<<std::endl;

	this->addScatterPlot(x, y, labels.first + " vs " + labels.second, this->getUniqueColor(0, 3), "Average of " + labels.first + " and " + labels.second, "Difference between " + labels.first + " and " + labels.second);

	ymax = max(abs(ymin), abs(ymax));
	ymin = -ymax;

	if(this->for_paper)
	{
		ymax = 50.0;
		ymin = -50.0;
	}

	this->setRange(xmin, xmax, ymin, ymax);

	this->addLine(QPoint(xmin,mean),QPoint(xmax,mean),"Mean",this->getUniqueColor(1, 3),"","");

	//this->addLine(QPoint(xmin,mean-sdev),QPoint(xmax,mean-sdev),"Standard deviation",this->getUniqueColor(2, 3),"","");
	//this->addLine(QPoint(xmin,mean+sdev),QPoint(xmax,mean+sdev),"Standard deviation",this->getUniqueColor(2, 3),"","");
}