#pragma once

//framework includes
#include "qflowexplorer.h"
#include <iostream>

//Qt includes
#include <QDialog.h>
#include <QDialogButtonBox.h>
#include <QCheckBox.h>
#include <QVBoxLayout>

#include <vector>

//local includes
#include "qfePlot.h"

class qfeHistogramPlot : public qfePlot
{
public:
  qfeHistogramPlot();
  ~qfeHistogramPlot();

  //input contains the data and class labels, the data is binned based on the number of classes available based on the range
  void setData(QVector<double> data, unsigned int number_of_classes, QPair<double,double> value_range, QString label);
  //input contains the data and class labels, the data is binned based on the number of class labels available based on the range
  void setData(QVector<double> data, QVector<QString> class_labels, QPair<double,double> value_range, QString label);
private:
};