#pragma once

//framework includes
#include "qflowexplorer.h"
#include <iostream>

//Qt includes
#include <QDialog.h>
#include <QDialogButtonBox.h>
#include <QCheckBox.h>
#include <QVBoxLayout>

#include <vector>

//local includes
#include "qfePlot.h"

class qfeScatterPlot : public qfePlot
{
public:
  qfeScatterPlot();
  ~qfeScatterPlot();

  //input list of scatter plots containing pairs of values and a pair of names
  void setData(QVector<QPair<double,double>> data, QPair<QString, QString> labels);
private:
};