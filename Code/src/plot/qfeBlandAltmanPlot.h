#pragma once

//framework includes
#include "qflowexplorer.h"
#include <iostream>

//Qt includes
#include <QDialog.h>
#include <QDialogButtonBox.h>
#include <QCheckBox.h>
#include <QVBoxLayout>

//local includes
#include "qfePlot.h"

class qfeBlandAltmanPlot : public qfePlot
{
public:
  qfeBlandAltmanPlot();
  ~qfeBlandAltmanPlot();

  void setData(QVector<double> sampleSet1, QVector<double> sampleSet2, QPair<QString, QString> labels);
private:
};