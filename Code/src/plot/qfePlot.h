#pragma once

//framework includes
#include "qflowexplorer.h"
#include <iostream>
#include "engine/qfeColorMaps.h"

//Qt includes
#include <QtCore/QObject>
#include <QDialog.h>
#include <QDialogButtonBox.h>
#include <QCheckBox.h>
#include <QVBoxLayout>
#include <QPushButton>

//qcustomplot include
#include "gui/qcustomplot.h"

class  QfeCustomPlot : public QCustomPlot
{	

};

class qfePlot : public QObject
{
public:
  qfePlot();
  ~qfePlot();

  virtual void showWindow();
  virtual void showWindow(unsigned int width, unsigned int height);
  virtual void closeWindow();
  virtual void setData();
  
  QString windowTitle;

  QfeCustomPlot* plot;
  void setRange(double xmin, double xmax, double ymin, double ymax);
  void resetRange();

  bool isClosed();

  void setLegendVisible(bool value);

  void addScatterPlot(QVector<double> x, QVector<double> y, QString data_name, QColor color, QString x_label, QString y_label);

  void addGraphPlot(QVector<double> x, QVector<double> y, QString data_name, QColor color, QString x_label, QString y_label);
  void addGraphPlots(QList<QPair<QVector<double>, QVector<double>>> dataSets, QVector<QString> data_names, QString x_label, QString y_label);
  void addUncertainGraphPlot(QVector<double> x, QVector<double> y, QVector<double> y_min_sdev, QVector<double> y_plus_sdev, QString data_name, QColor color, QString x_label, QString y_label);
  void addUncertainGraphPlots(QList<QVector<double>> x, QList<QVector<double>> y, QList<QVector<double>> y_min_sdev, QList<QVector<double>> y_plus_sdev, QVector<QString> data_names, QString x_label, QString y_label);

  void addHistogram(QVector<double> data, QVector<QString> class_labels, QString x_label);

  void addLine(QPoint point1, QPoint point2, QString data_name, QColor color, QString x_label, QString y_label);

  QColor getUniqueColor(int item_number, int total_number_of_items);

  void savePNG(QString filename);

  bool for_paper;

private:
  QWidget *window;
  QPushButton *save_PNG_button;

  double range_xmin;
  double range_xmax;
  double range_ymin;
  double range_ymax;

  std::vector<qfeColorRGB> colorMapColors;
};