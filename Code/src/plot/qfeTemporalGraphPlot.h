#pragma once

//framework includes
#include "qflowexplorer.h"
#include <iostream>

//Qt includes
#include <QDialog.h>
#include <QDialogButtonBox.h>
#include <QCheckBox.h>
#include <QVBoxLayout>

#include <vector>

//local includes
#include "qfePlot.h"

typedef struct{
	float average;
	float standard_deviation;
	float time;
} TemporalGraphStatistic;

class qfeTemporalGraphPlot : public qfePlot
{
public:
  qfeTemporalGraphPlot();
  ~qfeTemporalGraphPlot();

  //input line containing of average and time
  void setData(QVector<QPair<double,double>> dataSet, QString label);
  //input list of lines containing of average and time
  void setData(QList<QVector<QPair<double,double>>> dataSets, QVector<QString> labels);
  //input line containing of average, standard deviation and time
  void setData(QVector<TemporalGraphStatistic> dataSets, QString label);
  //input list of lines containing of average, standard deviation and time
  void setData(QList<QVector<TemporalGraphStatistic>> dataSets, QVector<QString> labels);

  void drawTime(float phase, QString label);
  void drawTime2(float phase, QString label);
private:
	double xmin;
	double xmax;
	double ymin;
	double ymax;

	int timePlotIndex;
	int timePlotIndex2;
};