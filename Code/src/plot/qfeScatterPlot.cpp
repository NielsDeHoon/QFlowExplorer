#include "qfeScatterPlot.h"

qfeScatterPlot::qfeScatterPlot()
{
	windowTitle = "Scatter Plot";
}

qfeScatterPlot::~qfeScatterPlot()
{
}

void qfeScatterPlot::setData(QVector<QPair<double,double>> data, QPair<QString, QString> labels)
{
	double xmin = 1e6;
	double xmax = -1e6;
	double ymin = 1e6;
	double ymax = -1e6;

	QVector<double> x(data.size());
	QVector<double> y(data.size());	

	double correlation = 0.0;
	double sum_x = 0.0;
	double sum_y = 0.0;
	double sum_x2 = 0.0;
	double sum_y2 = 0.0;
	double sum_xy = 0.0;

	//also compute the correlations:
	//https://www.mathsisfun.com/data/correlation.html

	for(unsigned int i = 0; i<data.size(); i++)
	{
		x[i] = data.at(i).first;
		y[i] = data.at(i).second;

		sum_x += x[i];
		sum_y += y[i];
		sum_xy += x[i]*y[i];
		sum_x2 += x[i]*x[i];
		sum_y2 += y[i]*y[i];
		
		xmin = x[i] < xmin ? x[i] : xmin;
		xmax = x[i] > xmax ? x[i] : xmax;
		ymin = y[i] < ymin ? y[i] : ymin;
		ymax = y[i] > ymax ? y[i] : ymax;
	}

	float n = data.size();
	correlation = (n*sum_xy - sum_x*sum_y)/(sqrt(n*sum_x2-pow(sum_x,2))*sqrt(n*sum_y2-pow(sum_y,2)));

	//compute trendline:
	//http://stackoverflow.com/a/43229/3192947
	//Y = a + bX
	//where:
	//b = (sum(x*y) - sum(x)sum(y)/n) / (sum(x^2) - sum(x)^2/n)
	//a = sum(y)/n - b(sum(x)/n)
	double b = (sum_xy-(sum_x*sum_y)/n)/(sum_x2 - pow(sum_x,2)/n);
	double a = sum_y/n-b*(sum_x/n);
	
	this->addScatterPlot(x, y,  labels.first + " vs " + labels.second, this->getUniqueColor(0, 2), labels.first, labels.second);

	//update the range
	xmin = min(xmin, ymin); ymin = xmin;
	xmax = max(xmax, ymax); ymax = xmax;

	double range = abs(xmax - xmin);
	xmin -= range*0.05; ymin -= range*0.05;
	xmax += range*0.05; ymax += range*0.05;

	//render trendline:
	QPoint point1(xmin, a + b * xmin);
	QPoint point2(xmax, a + b * xmax);

	this->addLine(point1, point2, "Trend line, correlation ("+QString::number(correlation)+")", this->getUniqueColor(1, 2), "", "");

	this->setRange(xmin, xmax, ymin, ymax);
}