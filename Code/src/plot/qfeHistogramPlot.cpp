#include "qfeHistogramPlot.h"

qfeHistogramPlot::qfeHistogramPlot()
{
	windowTitle = "Histogram Plot";
}

qfeHistogramPlot::~qfeHistogramPlot()
{
}

void qfeHistogramPlot::setData(QVector<double> data, unsigned int number_of_classes, QPair<double,double> value_range, QString label)
{
	if(number_of_classes == 0)
		return;

	QVector<QString> labels(number_of_classes);
	for(unsigned int i = 0; i<number_of_classes; i++)
	{
		labels[i] = "";
	}

	this->setData(data, labels, value_range, label);
}

//class labels represents the number of classes that should be shown
void qfeHistogramPlot::setData(QVector<double> data, QVector<QString> class_labels, QPair<double,double> value_range, QString label)
{
	if(value_range.second <= value_range.first)
		return;

	if(class_labels.size() == 0)
		return;

	QVector<double> bins(class_labels.size());

	double domain = value_range.second - value_range.first;

	double bin_size = domain/class_labels.size();

	unsigned int total_values = 0;

	for(unsigned int i = 0; i<data.size(); i++)
	{
		for(unsigned int bin = 0; bin<bins.size(); bin++)
		{
			if(data[i]<=value_range.first + bin*bin_size)
			{
				bins[bin]++;
				total_values++;
				break;
			}
		}
	}

	//divide by total to get a percentage
	for(unsigned int bin = 0; bin<bins.size(); bin++)
	{
		bins[bin] = 100.0 * (bins[bin]/total_values);
	}

	//check if the labels are empty, otherwise we add the values for at most 10 bins
	bool empty_labels = false;
	for(unsigned int i = 0; i<class_labels.size(); i++)
	{
		empty_labels = empty_labels || class_labels[i].isEmpty();
	}
	if(empty_labels)
	{
		int nLabels = min(class_labels.size(), 10);

		for(unsigned int i = 0; i<nLabels; i++)
		{
			float index = (float)i/(nLabels-1);

			unsigned int bin_number = (unsigned int) floor(((float)bins.size()-1) * index);
			
			class_labels[bin_number] = QString::number(value_range.first + bin_number*bin_size);
			//if range wanted:
				//"["+QString::number(value_range.first + bin_number*bin_size) + ", " + 
				//QString::number(value_range.first + (bin_number+1)*bin_size)+")";

			if(nLabels>20)
				class_labels[bin_number] = QString::number(value_range.first + bin_number*bin_size);
		}
	}

	this->addHistogram(bins, class_labels, label);
}