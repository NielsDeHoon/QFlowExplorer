#include "qfeTemporalGraphPlot.h"

qfeTemporalGraphPlot::qfeTemporalGraphPlot()
{
	windowTitle = "Temporal Graph Plot";
	
	xmin = 1e6;
	xmax = -1e6;
	ymin = 1e6;
	ymax = -1e6;

	timePlotIndex = -1;
	timePlotIndex2 = -1;
}

qfeTemporalGraphPlot::~qfeTemporalGraphPlot()
{
}

void qfeTemporalGraphPlot::setData(QVector<QPair<double,double>> dataSet, QString label)
{
	if(dataSet.size() == 0)
		return;

	xmin = 1e6;
	xmax = -1e6;
	ymin = 1e6;
	ymax = -1e6;
		
	QVector<double> x(dataSet.size());
	QVector<double> y(dataSet.size());
		
	for(unsigned int j = 0; j<dataSet.size(); j++)
	{
		x[j] = dataSet.at(j).first;
		y[j] = dataSet.at(j).second;
			
		xmin = x[j] < xmin ? x[j] : xmin;
		xmax = x[j] > xmax ? x[j] : xmax;
		ymin = y[j] < ymin ? y[j] : ymin;
		ymax = y[j] > ymax ? y[j] : ymax;
	}
		
	this->addGraphPlot(x, y, label, this->getUniqueColor(1, 1), "Time", "");
		
	//ensure we see the 0-line
	if(ymin>0.0f)
		ymin = 0.0f;
	if(ymax<0.0f)
		ymax = 0.0f;

	this->setRange(xmin, xmax, ymin, ymax);
}

void qfeTemporalGraphPlot::setData(QList<QVector<QPair<double,double>>> dataSets, QVector<QString> labels)
{
	if(dataSets.size() != labels.size())
		return;

	xmin = 1e6;
	xmax = -1e6;
	ymin = 1e6;
	ymax = -1e6;

	QList<QPair<QVector<double>, QVector<double>>> plotDataSets;
		
	for(unsigned int i = 0; i<dataSets.size(); i++)
	{
		QVector<QPair<double,double>> dataSet = dataSets.at(i);
		
		QVector<double> x(dataSet.size());
		QVector<double> y(dataSet.size());
		
		for(unsigned int j = 0; j<dataSet.size(); j++)
		{
			x[j] = dataSet.at(j).first;
			y[j] = dataSet.at(j).second;
			
			xmin = x[j] < xmin ? x[j] : xmin;
			xmax = x[j] > xmax ? x[j] : xmax;
			ymin = y[j] < ymin ? y[j] : ymin;
			ymax = y[j] > ymax ? y[j] : ymax;
		}
		
		QPair<QVector<double>, QVector<double>> line;
		line.first = x;
		line.second = y;

		plotDataSets.push_back(line);

	}	

	this->addGraphPlots(plotDataSets, labels, "Time", "");

	this->setRange(xmin, xmax, ymin, ymax);
}

void qfeTemporalGraphPlot::setData(QVector<TemporalGraphStatistic> dataSet, QString label)
{
	xmin = 1e6;
	xmax = -1e6;
	ymin = 1e6;
	ymax = -1e6;

	QVector<double> x(dataSet.size());
	QVector<double> y(dataSet.size());
						
	QVector<double> y_sdev_plus(dataSet.size());
	QVector<double> y_sdev_min(dataSet.size());
		
	for(unsigned int j = 0; j<dataSet.size(); j++)
	{
		x[j] = dataSet.at(j).time;
		y[j] = dataSet.at(j).average;

		y_sdev_plus[j] = y[j] + fabs(dataSet.at(j).standard_deviation);
		y_sdev_min[j]  = y[j] - fabs(dataSet.at(j).standard_deviation);
			
		xmin = x[j] < xmin ? x[j] : xmin;
		xmax = x[j] > xmax ? x[j] : xmax;
		ymin = y_sdev_min[j] < ymin ? y_sdev_min[j] : ymin;
		ymax = y_sdev_plus[j] > ymax ? y_sdev_plus[j] : ymax;
	}
		
	this->addUncertainGraphPlot(x, y, y_sdev_min, y_sdev_plus, label, this->getUniqueColor(1, 1), "Time", label);
		
	this->setRange(xmin, xmax, ymin, ymax);
}

void qfeTemporalGraphPlot::setData(QList<QVector<TemporalGraphStatistic>> dataSets, QVector<QString> labels)
{
	if(dataSets.size() != labels.size())
		return;

	xmin = 1e6;
	xmax = -1e6;
	ymin = 1e6;
	ymax = -1e6;
		
	QList<QVector<double>> xs;
	QList<QVector<double>> ys;
	QList<QVector<double>> y_sdev_mins;
	QList<QVector<double>> y_sdev_pluss;

	for(unsigned int i = 0; i<dataSets.size(); i++)
	{
		QVector<TemporalGraphStatistic> dataSet = dataSets.at(i);
		
		QVector<double> x(dataSet.size());
		QVector<double> y(dataSet.size());
						
		QVector<double> y_sdev_plus(dataSet.size());
		QVector<double> y_sdev_min(dataSet.size());
		
		for(unsigned int j = 0; j<dataSet.size(); j++)
		{
			x[j] = dataSet.at(j).time;
			y[j] = dataSet.at(j).average;

			y_sdev_plus[j] = y[j] + fabs(dataSet.at(j).standard_deviation);
			y_sdev_min[j]  = y[j] - fabs(dataSet.at(j).standard_deviation);
			
			xmin = x[j] < xmin ? x[j] : xmin;
			xmax = x[j] >= xmax ? x[j] : xmax;
			ymin = y_sdev_min[j] < ymin ? y_sdev_min[j] : ymin;
			ymax = y_sdev_plus[j] > ymax ? y_sdev_plus[j] : ymax;
		}

		xs.push_back(x);
		ys.push_back(y);
		y_sdev_mins.push_back(y_sdev_min);
		y_sdev_pluss.push_back(y_sdev_plus);
	}		

	this->addUncertainGraphPlots(xs, ys, y_sdev_mins, y_sdev_pluss, labels, "Time", "Value");

	this->setRange(xmin, xmax, ymin, ymax);
}

void qfeTemporalGraphPlot::drawTime(float phase, QString label)
{
	if(timePlotIndex < 0)
	{
		int index = plot->graphCount();
		QVector<double> x(2);
		QVector<double> y(2);

		x[0] = phase; x[1] = phase; 
		y[0] = ymin; y[1] = ymax;

		QColor color(100,200,100);
		
		plot->addGraph(plot->xAxis, plot->yAxis);
		
		plot->graph(index)->setData(x, y);
		plot->graph(index)->setPen(QPen(color,2));
		plot->graph(index)->setName(label);
		QCPGraph::LineStyle style = QCPGraph::LineStyle::lsLine;
		plot->graph(index)->setLineStyle(style);	
		//plot->graph(index)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));

		timePlotIndex = index;	
	}
	else
	{
		QVector<double> x(2);
		QVector<double> y(2);

		x[0] = phase; x[1] = phase; 
		y[0] = ymin; y[1] = ymax;

		plot->graph(timePlotIndex2)->setName(label);
		plot->graph(timePlotIndex)->setData(x, y);
	}
	
	this->plot->replot();
}

void qfeTemporalGraphPlot::drawTime2(float phase, QString label)
{
	if(timePlotIndex2 < 0)
	{
		int index = plot->graphCount();
		QVector<double> x(2);
		QVector<double> y(2);

		x[0] = phase; x[1] = phase; 
		y[0] = ymin; y[1] = ymax;

		QColor color(200,100,100);
		
		plot->addGraph(plot->xAxis, plot->yAxis);
		
		plot->graph(index)->setData(x, y);
		plot->graph(index)->setPen(QPen(color,2));
		plot->graph(index)->setName(label);
		QCPGraph::LineStyle style = QCPGraph::LineStyle::lsLine;
		plot->graph(index)->setLineStyle(style);	
		//plot->graph(index)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));

		timePlotIndex2 = index;	
	}
	else
	{
		QVector<double> x(2);
		QVector<double> y(2);

		x[0] = phase; x[1] = phase; 
		y[0] = ymin; y[1] = ymax;

		plot->graph(timePlotIndex2)->setName(label);
		plot->graph(timePlotIndex2)->setData(x, y);
	}
	
	this->plot->replot();
}