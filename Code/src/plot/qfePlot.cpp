#include "qfePlot.h"

qfePlot::qfePlot()
{
	window = new QWidget();
	window->setAttribute( Qt::WA_QuitOnClose, false );
	window->setWindowFlags(Qt::WindowStaysOnTopHint);

	QVBoxLayout *mainLayout = new QVBoxLayout;

	plot = new QfeCustomPlot();

	plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables | QCP::iSelectItems);

	unsigned int font_size = 12;
	QFont pfont("Newyork", font_size);
	pfont.setStyleHint(QFont::SansSerif);
	pfont.setPointSize(font_size);
	plot->setFont(pfont);

	plot->xAxis->setLabelFont(pfont);
	plot->yAxis->setLabelFont(pfont);
	//smaller font
	pfont.setPointSize((int)font_size*0.75f);
	plot->legend->setFont(pfont);
	plot->xAxis->setTickLabelFont(pfont);
	plot->yAxis->setTickLabelFont(pfont);

	//set the graph theme from http://www.qcustomplot.com/index.php/demos/styleddemo

	// set some pens, brushes and backgrounds:
	//(if changed also update function "getUniqueColor")
	//dark theme: nicer on the eye, bad for printers
	/*
	QColor textColor(190, 198, 202);
	QColor backgroundColor(33, 39, 43);
	QColor gridColor(140, 140, 140);
	QColor subGridColor = gridColor.darker(200);
	*/

	//light theme:
	QColor textColor = QColor(33, 39, 43);
	QColor backgroundColor = QColor(255, 255, 255);
	QColor gridColor = QColor(140, 140, 140);
	QColor subGridColor = gridColor.lighter(200);

	plot->xAxis->setBasePen(QPen(textColor, 1));
	plot->yAxis->setBasePen(QPen(textColor, 1));
	plot->xAxis->setTickPen(QPen(textColor, 1));
	plot->yAxis->setTickPen(QPen(textColor, 1));
	plot->xAxis->setSubTickPen(QPen(textColor, 1));
	plot->yAxis->setSubTickPen(QPen(textColor, 1));
	plot->xAxis->setTickLabelColor(textColor);
	plot->yAxis->setTickLabelColor(textColor);
	plot->xAxis->setLabelColor(textColor);
	plot->yAxis->setLabelColor(textColor);
	plot->xAxis->grid()->setPen(QPen(gridColor, 1, Qt::DotLine));
	plot->yAxis->grid()->setPen(QPen(gridColor, 1, Qt::DotLine));
	plot->xAxis->grid()->setSubGridPen(QPen(subGridColor, 1, Qt::DotLine));
	plot->yAxis->grid()->setSubGridPen(QPen(subGridColor, 1, Qt::DotLine));
	plot->xAxis->grid()->setSubGridVisible(true);
	plot->yAxis->grid()->setSubGridVisible(true);
	plot->xAxis->grid()->setZeroLinePen(Qt::NoPen);
	plot->yAxis->grid()->setZeroLinePen(Qt::NoPen);
	//plot->xAxis->setLowerEnding(QCPLineEnding::esSpikeArrow);
	//plot->yAxis->setLowerEnding(QCPLineEnding::esSpikeArrow);
	//plot->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
	//plot->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);

	plot->legend->setBrush(Qt::NoBrush);
	plot->legend->setBorderPen(QPen(textColor, 2));
	plot->legend->setTextColor(textColor);

	QLinearGradient plotGradient;
	plotGradient.setStart(0, 0);
	plotGradient.setFinalStop(0, 350);
	plotGradient.setColorAt(0, backgroundColor);
	plotGradient.setColorAt(1, backgroundColor);
	plot->setBackground(plotGradient);

	mainLayout->addWidget(plot);	

	window->setLayout(mainLayout);

	range_xmin = 1e6;
	range_xmax = -1e6;
	range_ymin = 1e6;
	range_ymax = -1e6;

	qfeColorMap colorMap;
	qfeColorMaps colorMaps;
	colorMap.qfeUploadColorMap();
	std::string name;
	colorMaps.qfeGetColorMap(26, name, colorMap); 
	colorMap.qfeGetAllColorsRGB(this->colorMapColors);

	for_paper = false;

	for_paper = true;

	if(!for_paper)
		this->setLegendVisible(false);
}

qfePlot::~qfePlot()
{
}

void qfePlot::savePNG(QString filename)
{
	std::cout<<"Saving plot"<<std::endl;
    plot->savePng(filename+".png",600,600);
}

void qfePlot::showWindow()
{
	if(window == NULL)
		return;	
		
	window->resize(600, 600);
	window->setWindowTitle(windowTitle);
	window->show();
}

void qfePlot::showWindow(unsigned int width, unsigned int height)
{
	if(window == NULL)
		return;
		
	window->resize(width, height);
	window->setWindowTitle(windowTitle);
	window->show();
	window->raise();
}

void qfePlot::closeWindow()
{
	if(window == NULL)
		return;

	window->close();
}

void qfePlot::setData()
{
  std::cout << "qfePlot::setData - Pure virtual function" << endl;
}

void qfePlot::setRange(double xmin, double xmax, double ymin, double ymax)
{
	if(plot==NULL)
		return;
	
	//store and increase the range 5% so all data is showns
	range_xmin = xmin-0.1*fabs(xmin);
	range_xmax = xmax+0.1*fabs(xmax);
	range_ymin = ymin-0.1*fabs(ymin);
	range_ymax = ymax+0.1*fabs(ymax);

	plot->xAxis->setRange(range_xmin, range_xmax);
	plot->yAxis->setRange(range_ymin, range_ymax);

	plot->replot();
}

void qfePlot::resetRange()
{
	std::cout<<"Reset range"<<std::endl;
	plot->xAxis->setRange(range_xmin, range_xmax);
	plot->yAxis->setRange(range_ymin, range_ymax);

	plot->replot();
}

bool qfePlot::isClosed()
{
	if(window!=NULL)
		return this->window->isVisible();
	else
		return false;
}

void qfePlot::setLegendVisible(bool value)
{
	if(!for_paper)
	{
		plot->legend->setVisible(value);
	}
	else
	{
		plot->legend->setVisible(false);
	}
}

void qfePlot::addScatterPlot(QVector<double> x, QVector<double> y, QString data_name, QColor color, QString x_label, QString y_label)
{
	if(x.size() != y.size())
		return;

	int index = plot->graphCount();

	// create graph and assign data to it:
	plot->addGraph();

	QPen pen;
	pen.setColor(color);

	plot->graph(index)->setData(x, y);

	plot->graph()->setPen(pen);
	plot->graph()->setName(data_name);
	plot->graph()->setLineStyle(QCPGraph::lsNone);
	// set scatter style:
	QCPScatterStyle style = QCPScatterStyle::ssDisc;
	style.setSize(1.5);
	plot->graph()->setScatterStyle(style);

	// give the axes some labels if they are non-zero:
	if(!for_paper)
	{
		if(x_label.length() > 0)
			plot->xAxis->setLabel(x_label);
		if(y_label.length() > 0)
			plot->yAxis->setLabel(y_label);
	}

	plot->replot();
}

void qfePlot::addGraphPlot(QVector<double> x, QVector<double> y, QString data_name, QColor color, QString x_label, QString y_label)
{
	if(x.size() != y.size())
		return;

	int index = plot->graphCount();

	plot->addGraph(plot->xAxis, plot->yAxis);

	plot->graph(index)->setData(x, y);
	plot->graph(index)->setPen(QPen(color,2));
	plot->graph(index)->setName(data_name);
	QCPGraph::LineStyle style = QCPGraph::LineStyle::lsLine;
	plot->graph(index)->setLineStyle(style);	
	plot->graph(index)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));

	// give the axes some labels if they are non-zero:
	if(!for_paper)
	{
		if(x_label.length() > 0)
			plot->xAxis->setLabel(x_label);
		if(y_label.length() > 0)
			plot->yAxis->setLabel(y_label);
	}

	plot->replot();
}

void qfePlot::addGraphPlots(QList<QPair<QVector<double>, QVector<double>>> dataSets, QVector<QString> data_names, QString x_label, QString y_label)
{
	if(dataSets.size() != data_names.size())
		return;

	for(unsigned int i = 0; i<dataSets.size(); i++)
	{	
		QVector<double> x = dataSets.at(i).first;
		QVector<double> y = dataSets.at(i).second;

		if(x.size() != y.size() || x.size() == 0)
		{
			std::cout<<"addGraphPlots Missing data for plot "<<data_names.at(i).toStdString()<<std::endl;
			continue;
		}

		int index = plot->graphCount();

		plot->addGraph(plot->xAxis, plot->yAxis);
		plot->graph(index)->setData(x, y);
		plot->graph(index)->setPen(QPen(this->getUniqueColor(i,dataSets.size()),2));
		plot->graph(index)->setName(data_names.at(i));
		QCPGraph::LineStyle style = QCPGraph::LineStyle::lsLine;
		plot->graph(index)->setLineStyle(style);	
		plot->graph(index)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));
	}

	// give the axes some labels if they are non-zero:
	if(!for_paper)
	{
		if(x_label.length() > 0)
			plot->xAxis->setLabel(x_label);
		if(y_label.length() > 0)
			plot->yAxis->setLabel(y_label);
	}

	plot->replot();
}

void qfePlot::addUncertainGraphPlot(QVector<double> x, QVector<double> y, QVector<double> y_min_sdev, QVector<double> y_plus_sdev, QString data_name, QColor color, QString x_label, QString y_label)
{
	if(x.size() != y.size() || x.size() != y_min_sdev.size() || x.size() != y_plus_sdev.size())
	{
		std::cout<<"addUncertainGraphPlot: missing data"<<std::endl;
		return;
	}

	int index = plot->graphCount();

	// create graph and assign data to it:
	plot->addGraph(plot->xAxis, plot->yAxis);

	plot->graph(index)->setData(x, y);

	plot->graph(index)->setPen(QPen(color,2));
	plot->graph(index)->setName(data_name);
	QCPGraph::LineStyle style = QCPGraph::LineStyle::lsLine;
	plot->graph(index)->setLineStyle(style);	
	plot->graph(index)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));

	QColor brush_color = color;
	brush_color.setAlpha(64);

	plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(index+1)->setData(x, y_plus_sdev);
	plot->graph(index+1)->setPen(QPen(color, 1, Qt::DotLine));
	plot->graph(index+1)->setBrush(QBrush(brush_color));
	plot->graph(index+1)->setName(data_name+" standard deviation");
	plot->graph(index+1)->setChannelFillGraph(plot->graph(index));

	plot->addGraph(plot->xAxis, plot->yAxis);
	plot->graph(index+2)->setData(x, y_min_sdev);
	plot->graph(index+2)->setPen(QPen(color, 1, Qt::DotLine));
	plot->graph(index+2)->setBrush(QBrush(brush_color));
	plot->graph(index+2)->setName(data_name+" standard deviation");
	plot->graph(index+2)->setChannelFillGraph(plot->graph(index));

	// give the axes some labels if they are non-zero:
	if(!for_paper)
	{
		if(x_label.length() > 0)
			plot->xAxis->setLabel(x_label);
		if(y_label.length() > 0)
			plot->yAxis->setLabel(y_label);
	}

	plot->replot();
}

void qfePlot::addUncertainGraphPlots(QList<QVector<double>> x, QList<QVector<double>> y, QList<QVector<double>> y_min_sdev, QList<QVector<double>> y_plus_sdev, QVector<QString> data_names, QString x_label, QString y_label)
{
	if(x.size() != y.size() || x.size() != y_min_sdev.size() || x.size() != y_plus_sdev.size())
	{
		std::cout<<"addUncertainGraphPlot: Missing data"<<std::endl;
		return;
	}

	for(unsigned int i = 0; i<x.size(); i++)
	{



		if(x.at(i).size() != y.at(i).size() || x.at(i).size() != y_min_sdev.at(i).size() || x.at(i).size() != y_plus_sdev.at(i).size() || x.size() == 0)
		{
			std::cout<<"addUncertainGraphPlot: Missing data for plot "<<data_names.at(i).toStdString()<<std::endl;
			continue;
		}

		QColor color = this->getUniqueColor(i,x.size());

		// create graph and assign data to it:
		int index = plot->graphCount();
		plot->addGraph(plot->xAxis, plot->yAxis);
		plot->graph(index)->setData(x.at(i), y.at(i));
		plot->graph(index)->setPen(QPen(color,2));
		plot->graph(index)->setName(data_names.at(i));
		QCPGraph::LineStyle style = QCPGraph::LineStyle::lsLine;
		plot->graph(index)->setLineStyle(style);	
		plot->graph(index)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));

		QColor brush_color = color;
		brush_color.setAlpha(64);

		index = plot->graphCount();
		plot->addGraph(plot->xAxis, plot->yAxis);
		plot->graph(index)->setData(x.at(i), y_plus_sdev.at(i));
		plot->graph(index)->setPen(QPen(color, 1, Qt::DotLine));
		plot->graph(index)->setBrush(QBrush(brush_color));
		plot->graph(index)->setName(data_names.at(i)+" standard deviation");
		plot->graph(index)->setChannelFillGraph(plot->graph(index-1));

		index = plot->graphCount();
		plot->addGraph(plot->xAxis, plot->yAxis);
		plot->graph(index)->setData(x.at(i), y_min_sdev.at(i));
		plot->graph(index)->setPen(QPen(color, 1, Qt::DotLine));
		plot->graph(index)->setBrush(QBrush(brush_color));
		plot->graph(index)->setName(data_names.at(i)+" standard deviation");
		plot->graph(index)->setChannelFillGraph(plot->graph(index-2));
	}

	// give the axes some labels if they are non-zero:
	if(!for_paper)
	{
		if(x_label.length() > 0)
			plot->xAxis->setLabel(x_label);
		if(y_label.length() > 0)
			plot->yAxis->setLabel(y_label);
	}

	plot->replot();
}

void qfePlot::addHistogram(QVector<double> data, QVector<QString> class_labels, QString x_label)
{
	if(data.size() != class_labels.size())
		return;

	// create empty bar chart objects:
	QCPBars *bar = new QCPBars(plot->xAxis, plot->yAxis);

	bar->setName(x_label);
			
	QColor color = this->getUniqueColor(1, 1);

	bar->setPen(QPen(color.darker(200),2)); //border should be 200% darker
	bar->setBrush(color);

	// prepare x axis with labels:
	QVector<double> ticks(data.size());
	QVector<QString> labels(data.size());

	for(unsigned int i = 0; i<data.size(); i++)
	{
		ticks[i] = i;
		labels[i] = class_labels[i];
	}

	QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
	textTicker->addTicks(ticks, labels);
	textTicker->setTickStepStrategy(QCPAxisTicker::tssReadability);
	if(data.size()>20)
		textTicker->setTickCount(data.size()/10);
	plot->xAxis->setTicker(textTicker);
	plot->xAxis->setTickLabelRotation(60);
	plot->xAxis->setSubTicks(false);
	plot->xAxis->setTickLength(0, 4);
	float range = ticks.size();
	plot->xAxis->setRange(-range*0.05f, ticks.size()+range*0.05f);
	if(data.size()>20)
		plot->xAxis->grid()->setVisible(false);

	// prepare y axis:
	double biggest_bin_size = 0.0;
	for(unsigned int i = 0; i<data.size(); i++)
	{
		if(data[i]>biggest_bin_size)
			biggest_bin_size = data[i];
	}

	plot->yAxis->setRange(-0.05*biggest_bin_size, biggest_bin_size+0.05*biggest_bin_size);

	if(for_paper)
		plot->yAxis->setRange(0.0, 25.0);

	QVector<double> values(data.size());
	for(unsigned int i = 0; i<data.size(); i++)
	{
		values[i] = data[i];
	}

	bar->setData(ticks,values);
}

void qfePlot::addLine(QPoint point1, QPoint point2, QString data_name, QColor color, QString x_label, QString y_label)
{
	int index = plot->graphCount();
	QVector<double> x(2);
	QVector<double> y(2);

	x[0] = point1.x(); x[1] = point2.x(); 
	y[0] = point1.y(); y[1] = point2.y();

	this->addGraphPlot(x, y, data_name, color, x_label, y_label);
}

QColor qfePlot::getUniqueColor(int item_number, int total_number_of_items)
{
	float index = 0.0f;
	if(total_number_of_items <= 1)
	{
		index = 0.5f;
	}
	else
	{
		index = (float)item_number/(total_number_of_items-1);
	}

	if(item_number >= total_number_of_items)
	{
		index = 1.0f;
	}

	qfeColorRGB col = colorMapColors.at(
		(unsigned int) floor(
			((float)colorMapColors.size()-1) * index
		)
	);

	//light theme
	return QColor(col.r*255, col.g*255, col.b*255);

	//dark them: make the color slightly lighter
	//return QColor(col.r*255, col.g*255, col.b*255).lighter(120);
}