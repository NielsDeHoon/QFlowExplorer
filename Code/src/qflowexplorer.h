/**
 * Copyright (c) 2012, Roy van Pelt
 * QFlow Explorer
 * Biomedical Image Analysis Eindhoven (BMIA/e)
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 * 
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the 
 *     distribution.
 * 
 *   - Neither the name of Eindhoven University of Technology nor the
 *     names of its contributors may be used to endorse or promote 
 *     products derived from this software without specific prior 
 *     written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#pragma once

#include <vector>

//----------------------------------------------------------------------------
// Global macros
//----------------------------------------------------------------------------
#ifndef max
  #define max(a,b) (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
  #define min(a,b) (((a) < (b)) ? (a) : (b))
#endif

//----------------------------------------------------------------------------
// Global defines
//----------------------------------------------------------------------------
#define AXIAL     0
#define SAGITTAL  1
#define CORONAL   2
#define VIEWNORM  3

#define AP        0
#define PA        1
#define LR        2
#define RL        3
#define HF        4
#define FH        5
#define VA        6 // default view angle

#define QFE_MAX_SCENES 4

//----------------------------------------------------------------------------
// Typedefs
//----------------------------------------------------------------------------

// Philips related typedefs
typedef int qfeId;

typedef   signed char  qfeInt8;
typedef   signed short qfeInt16;
typedef unsigned char  qfeUnsignedInt8;
typedef unsigned short qfeUnsignedInt16;
typedef unsigned char  qfeGrey;
typedef          float qfeFloat;

typedef enum {
  qfeSuccess,
  qfeError
} qfeReturnStatus;

typedef enum {
  qfeValueTypeInt8,
  qfeValueTypeInt16,
  qfeValueTypeUnsignedInt8,
  qfeValueTypeUnsignedInt16,
  qfeValueTypeGrey,
  qfeValueTypeColor,
  qfeValueTypeFloat
} qfeValueType;

typedef enum {
  qfeParamTypeUndefined,
  qfeParamTypeBool,
  qfeParamTypeInt,
  qfeParamTypeFlagList,
  qfeParamTypeDouble,
  qfeParamTypeString,
  qfeParamTypeStringList,
  qfeParamTypeVector,
  qfeParamTypeColorRGB,
  qfeParamTypeRing,
  qfeParamTypeRange,
  qfeParamTypeMesh
} qfeParamType;

typedef enum {
  qfeColorInterpolationNearest,
  qfeColorInterpolationLinear,
} qfeColorInterpolation;

typedef enum {
  qfeColorSpaceRGB,
  qfeColorSpaceLuv,
  qfeColorSpaceLab,
} qfeColorSpace;

/**
* \brief Structure for RGB color
*/
struct qfeColorRGB {
  float r;
  float g;
  float b;
  qfeColorRGB() : r(0.f), g(0.f), b(0.f) {}
  qfeColorRGB(float r, float g, float b) : r(r), g(g), b(b) {}
};

/**
* \brief Structure for RGBa color
*/
typedef struct {
  float r;
  float g;
  float b;
  float a;
} qfeColorRGBA;

/**
* \brief Structure for HSV color
*/
typedef struct {
  float h;
  float s;
  float v;
} qfeColorHSV;

/**
* \brief Structure for HLS color
*/
typedef struct {
  float h;
  float l;
  float s;
} qfeColorHLS;

/**
* \brief Structure for XYZ color
*/
typedef struct {
  float x;
  float y;
  float z;
} qfeColorXYZ;

/**
* \brief Structure for Lab color
*/
typedef struct {
  float L;
  float a;
  float b;
} qfeColorLab;

/**
* \brief Structure for Luv color
*/
typedef struct {
  float L;
  float u;
  float v;
} qfeColorLuv;

/**
* \brief Structure for 1D opacity map
*/
typedef struct {
  qfeFloat     value;
  qfeFloat     opacity;
} qfeOpacityMapping;

/**
* \brief Structure for 1D color map
*/
typedef struct {
  qfeFloat     value;
  qfeColorRGB  color;
} qfeRGBMapping;

/**
* \brief Structure for an opacity map point
*/
struct qfeOpacityPoint
{
	double x;
	double a;
};

/**
* \brief Structure for a color map point
*/
struct qfeColorPoint
{
	double x;
	double color[3];
};

/**
* \brief Structure for a 1D transfer function
*/
struct qfeTransferFunction
{
	qfeOpacityPoint *opacity;
	int              opacitySize;
	qfeColorPoint   *color;
	int              colorSize;
};

/**
* \brief Structure for range
*/
struct qfeRange
{
	double min;
	double max;
};

/**
* \brief Structure for mesh vertices
*/
typedef struct qfeMeshVertex
{
  qfeFloat x;
  qfeFloat y;
  qfeFloat z;
} qfeMeshVertex;

typedef std::vector<qfeMeshVertex>   qfeMeshVertices;
typedef std::vector<qfeMeshVertices> qfeMeshStrips;
typedef std::vector<unsigned short>  qfeMeshIndices;
typedef std::vector<qfeMeshIndices>  qfeMeshStripsIndices;