#pragma once
#include "MACGrid.h"
#include "vec.h"


class FLIP_Particles
{
public:
	FLIP_Particles(MACGrid &cGrid): 
	  grid(cGrid),
	  numberOfParticles(0),
	  sum(cGrid.vel_pressure.ni+1, cGrid.vel_pressure.nj+1, cGrid.vel_pressure.nk+1),
	  alpha_ratio(0.0),	  
	  xmin((float)1.001*grid.dx),
	  xmax(grid.lx-(float)2.001*grid.dx),
	  ymin((float)1.001*grid.dx),
	  ymax(grid.ly-(float)2.001*grid.dx),
	  zmin((float)1.001*grid.dx),
	  zmax(grid.lz-(float)2.001*grid.dx)
	  {}
	~FLIP_Particles(void);

	float alpha_ratio;

   //functions defined on the set of particles
   void add_particle(const Vec3f &px, const Vec3f &pu);
   void remove_particle(unsigned int index);   
   Vec3ui get_cell_of_particle(unsigned int index);
   void transfer_to_grid(void);
   void update_from_grid(void);
   void transfer_velocity_from_grid(void);
   void move_particles_in_grid(float dt);
   void save_old_velocity(void);

   unsigned int size();

   void compute_phi();

   Array3f compute_fluid_sdf();

   //variables:
   MACGrid &grid; //define the grid
   unsigned int numberOfParticles; //number of particles
   Array3f sum;
   std::vector<Vec3f> x, u; //the locations and velocity of the particles

private:
   template<class T> void accumulate(T &accum, float q, unsigned int i, unsigned int j, unsigned int k, float fx, float fy, float fz);
   void clamper(unsigned int i, unsigned int j, unsigned int k);
   float kernel(float distance);

   Vec3f distanceRay(Vec3f xold, Vec3f xnew);
   
   float xmin, xmax;
   float ymin, ymax;
   float zmin, zmax;

   std::vector<Vec3f> uOld; //the velocities before the advection step, needed for bouncing off the solid(s)
};
