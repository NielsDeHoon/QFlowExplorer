// This file is the 'manager' of the FLIP fluid simulation

// it manages the particles and the grid, only communicate with those via this class
// For debuging the fluid simulation set DEBUG to true in MACGrid.h
// Do not alter the MACGRID or FLIP_Particles classes unless you know what you're doing
// Enjoy :)


#include "FLIP_simulation.h"

//FLIP_simulation::FLIP_simulation(int mx, int my, int mz, float lx)
//{
//}
void FLIP_simulation::init(unsigned int mx, unsigned int my, unsigned int mz, float lx, float density)
{
	phi_solid.set_zero();
	firstRun = true;
	frameTime = 0.0;

	fillHolesAutomatically = false;
	fillHolesCells = Array3b(mx, my, mz);
	
	sink_cells   = Array3b(mx, my, mz);
	source_cells = Array3b(mx, my, mz);

	velocity_field_current_frame = Array3Vec3(mx, my, mz);
	velocity_field_next_frame    = Array3Vec3(mx, my, mz);

	std::array<float,3> zero;
	zero[0] = 0.0f; zero[1] = 0.0f; zero[2] = 0.0f;

	for(unsigned int k = 0; k<mz; k++) for(unsigned int j = 0; j<my; j++) for(unsigned int i = 0; i<mx; i++)
	{
		sink_cells(i,j,k)   = false;
		source_cells(i,j,k) = false;

		velocity_field_current_frame(i,j,k) = zero;
		velocity_field_next_frame(i,j,k)    = zero;
	}

	velocity_field_current_frame_set = false;
	velocity_field_next_frame_set    = false;

	randomSeed = 0;

	using_negative_time_step = false;
}

FLIP_simulation::~FLIP_simulation(void)
{
}

std::vector<Vec3f> FLIP_simulation::getParticleLocations(void)
{
	return particles.x;
}

std::vector<Vec3f> FLIP_simulation::getParticleVelocities(void)
{
	return particles.u;
}

unsigned int FLIP_simulation::getInitialParticlesPerCell()
{
	return grid.initialParticlesPerCell;
}

Array3f FLIP_simulation::getFluidSurface()
{
	return particles.compute_fluid_sdf();
}

Array3f FLIP_simulation::getSolidSurface()
{
	return grid.nodal_solid_phi;
}

Array3f FLIP_simulation::getPressure()
{
	return grid.vel_pressure;
}

void FLIP_simulation::getVelocityField(Array3Vec3 &velocity)
{
	velocity.clear();
	velocity.resize(grid.ni, grid.nj, grid.nk);
	this->particles.transfer_to_grid();

	for(unsigned int k = 0; k<grid.nk; k++) for(unsigned int j = 0; j<grid.nj; j++) for(unsigned int i = 0; i<grid.ni; i++)
	{
		float u = (float)((grid.u(i,j,k)+grid.u(i+1,j,k))/2.0);
		float v = (float)((grid.v(i,j,k)+grid.v(i,j+1,k))/2.0);
		float w = (float)((grid.w(i,j,k)+grid.w(i,j,k+1))/2.0);

		velocity(i,j,k) = velocity.init_vector(u,v,w);
	}
	return;
}

Vec3f FLIP_simulation::getVelocityAtPoint(Vec3f point)
{
	unsigned int i, ui, j, vj, k, wk;
	float fx, ufx, fy, vfy, fz, wfz;
	
	grid.bary_x(point[0], ui, ufx);
	grid.bary_x_centre(point[0], i, fx);
	grid.bary_y(point[1], vj, vfy);
	grid.bary_y_centre(point[1], j, fy);
	grid.bary_z(point[2], wk, wfz);
	grid.bary_z_centre(point[2], k, fz);

	return Vec3f(grid.u.gridtrilerp(ui, j, k, ufx, fy, fz),
				 grid.v.gridtrilerp(i, vj, k, fx, vfy, fz),
				 grid.w.gridtrilerp(i, j, wk, fx, fy, wfz));
}

unsigned int FLIP_simulation::getNumberOfParticles()
{
	return (unsigned int) particles.x.size();
}

Array3f FLIP_simulation::getKineticEnergyField()
{
	Array3f result(grid.ni, grid.nj, grid.nk);

	Array3f volumeFraction = particles.compute_fluid_sdf();

	float cellVolume = grid.dx * grid.dx * grid.dx;

	float rho = grid.rho;

	for(unsigned int k = 0; k<grid.nk; k++) for(unsigned int j = 0; j<grid.nj; j++) for(unsigned int i = 0; i<grid.ni; i++)
	{
		float u = (float)((grid.u(i,j,k)+grid.u(i+1,j,k))/2.0);
		float v = (float)((grid.v(i,j,k)+grid.v(i,j+1,k))/2.0);
		float w = (float)((grid.w(i,j,k)+grid.w(i,j,k+1))/2.0);

		result(i,j,k) = 0.5f * rho * (cellVolume*volumeFraction(i,j,k)) * (u*u + v*v + w*w); // 1/2 * rho * V * vel^2 = 1/2 * m * vel^2
	}

	return result;
}

float FLIP_simulation::getTotalKineticEnergy()
{
	Array3f field = getKineticEnergyField();

	float total = 0.0;

	for(unsigned int k = 0; k<grid.nk; k++) for(unsigned int j = 0; j<grid.nj; j++) for(unsigned int i = 0; i<grid.ni; i++)
	{
		total += field(i,j,k);
	}

	return total; 
}

void FLIP_simulation::setFillHolesAutomatically(bool set)
{
	fillHolesAutomatically = set;
}

void FLIP_simulation::setFillHolesCells(Array3b _array)
{
	this->fillHolesCells = _array;
}

void FLIP_simulation::setSinkCell(unsigned int i,unsigned int j,unsigned int k)
{
	sink_cells(i,j,k) = true;
}

void FLIP_simulation::removeSinkCell(unsigned int i,unsigned int j,unsigned int k)
{
	sink_cells(i,j,k) = false;
}

void FLIP_simulation::setSourceCell(unsigned int i,unsigned int j,unsigned int k)
{
	source_cells(i,j,k) = true;
}

void FLIP_simulation::removeSourceCell(unsigned int i,unsigned int j,unsigned int k)
{
	source_cells(i,j,k) = false;
}

void FLIP_simulation::setSourceVelocityFieldCurrentFrame(Array3Vec3 velocity_field)
{
	assert(velocity_field_current_frame.ni == velocity_field.ni);
	assert(velocity_field_current_frame.nj == velocity_field.nj);
	assert(velocity_field_current_frame.nk == velocity_field.nk);

	velocity_field_current_frame_set = true;
	velocity_field_current_frame = velocity_field; 
}

void FLIP_simulation::setSourceVelocityFieldNextFrame(Array3Vec3 velocity_field)
{
	assert(velocity_field_next_frame.ni == velocity_field.ni);
	assert(velocity_field_next_frame.nj == velocity_field.nj);
	assert(velocity_field_next_frame.nk == velocity_field.nk);

	velocity_field_next_frame_set = true;
	velocity_field_next_frame = velocity_field;
}

void FLIP_simulation::advance_one_step(double dt)
{
   grid.timestep = dt;
   //FLIP - PIC ratio has to be set depending on the timestep, viscosity and grid cell size
   float alpha = (float)(6.0*dt*grid.visc)/(grid.dx*grid.dx);
   if (alpha>1.0) 
	   alpha=1.0;

   particles.alpha_ratio=alpha;

   //std::cout<<"    Save old particle velocity"<<std::endl;
   particles.save_old_velocity();

   //std::cout<<"    Move particles in grid"<<std::endl;
   unsigned int number_of_rk2_steps = 1; //1 should work because of the CFL condition
   for(unsigned int i=0; i<number_of_rk2_steps; ++i)
   {
	  particles.move_particles_in_grid((float)((1.0/(float) number_of_rk2_steps)*dt)); //small steps to be more stable
   }
   
   //std::cout<<"    Count particles per grid cell"<<std::endl;
   // identify where particles are in grid
   grid.particleCells.set_zero();
   for(unsigned int p=0; p<particles.numberOfParticles; ++p)
   {
	   unsigned int i,j,k;
	   float fy,fx,fz;

	   grid.bary_x((float)particles.x.at(p)[0], i, fx);
	   grid.bary_y((float)particles.x.at(p)[1], j, fy);
	   grid.bary_z((float)particles.x.at(p)[2], k, fz);

	   grid.particleCells(i,j,k)++;
   }

   float timeFraction = (float)(frameTimeProcessed/abs(frameTime));

   //std::cout<<"    Initialize source cells"<<std::endl;
   for(unsigned int k = 1; k<grid.nk-1; k++) for(unsigned int j = 1; j<grid.nj-1; j++) for(unsigned int i = 1; i<grid.ni-1; i++)
   {    
	   if((!using_negative_time_step && source_cells(i,j,k)) || //normal case
		  ( using_negative_time_step && sink_cells(i,j,k)) )     //negative timestep case
	   {
		   fillSourceCell(i,j,k, timeFraction);
	   }
   }

   //std::cout<<"    Update particles based on sources and sinks"<<std::endl;
   unsigned int p = particles.numberOfParticles;
   while(p>=1)
   {
	   p--;
	   Vec3ui cell = particles.get_cell_of_particle(p);

	   if((!using_negative_time_step && source_cells(cell[0],cell[1],cell[2])) || //normal case
		  ( using_negative_time_step && sink_cells(cell[0],cell[1],cell[2])) )    //negative timestep case
	   {
		   Vec3f velocity(0.0f,0.0f,0.0f);
		   Vec3f location = particles.x.at(p);

		   if(velocity_field_current_frame_set)
		   {			   
			   velocity  = trilerpVectorField(&velocity_field_current_frame, location);
		   }
		   if(velocity_field_next_frame_set)
		   {
			   Vec3f velocityNext = trilerpVectorField(&velocity_field_next_frame,    location);

			   velocity[0] = (1.0f - timeFraction) * velocity[0] + timeFraction * velocityNext[0];
			   velocity[1] = (1.0f - timeFraction) * velocity[1] + timeFraction * velocityNext[1];
			   velocity[2] = (1.0f - timeFraction) * velocity[2] + timeFraction * velocityNext[2];
		   }

		   particles.u.at(p) = velocity;
	   }

	   if((!using_negative_time_step && sink_cells(cell[0],cell[1],cell[2])) || //normal case
		  ( using_negative_time_step && source_cells(cell[0],cell[1],cell[2])) )//negative timestep case
	   {
		   particles.remove_particle(p);
	   }	   
   }
   
   //std::cout<<"    Compute phi (fluid surface)"<<std::endl;
   particles.compute_phi();

   //std::cout<<"    Move particles to the grid"<<std::endl;
   particles.transfer_to_grid();

   //std::cout<<"    Save grid velocities grid"<<std::endl;
   grid.save_velocities();
	
   //std::cout<<"    Apply potential forces"<<std::endl;
   grid.add_potential((float) dt);
	
   //std::cout<<"    Apply body forces"<<std::endl;
   grid.add_body_force((float) dt);
	
   if(grid.use_viscosity)
   {
	   //std::cout<<"    Apply viscosity"<<std::endl;
	   grid.apply_viscosity((float) dt);
   }
	
   //std::cout<<"    Apply projection (pressure solve)"<<std::endl;
   grid.apply_projection((float) dt);

   //Pressure projection only produces valid velocities in faces with non-zero associated face area.
   //Because the advection step may interpolate from these invalid faces, 
   //we must extrapolate velocities from the fluid domain into these zero-region cells.
   //std::cout<<"    Extrapolate velocity field"<<std::endl;
   grid.extend_velocity();

   //std::cout<<"    Update velocity grid"<<std::endl;
   grid.get_velocity_update();

   //std::cout<<"    Update particle velocity"<<std::endl;
   particles.update_from_grid();

   if(fillHolesAutomatically)
   {	   
	   //std::cout<<"    Add particles to empty cells (if necessary)"<<std::endl;
	   for(unsigned int k = 0; k<grid.nk-1; k++)
	   {
		   for(unsigned int j = 0; j<grid.nj-1; j++)
		   {
			   for(unsigned int i = 0; i<grid.ni-1; i++)
			   {
				   if(grid.particleCells(i,j,k)==0)
				   {
					   if(grid.nodal_solid_phi(i,j,k)<0) continue;

					   if(!fillHolesCells(i,j,k)) continue;

					   for(unsigned int n = 0; n<grid.initialParticlesPerCell; n++)
						   {
						   unsigned int idi; unsigned int idj; unsigned int idk;
						   unsigned int ui;  unsigned int vj;  unsigned int wk;
						   float ufx; float vfy; float wfz;
						   float fx; float fy; float fz;

						   float x=(float)i+(float)rand()/RAND_MAX;
						   float y=(float)j+(float)rand()/RAND_MAX;
						   float z=(float)k+(float)rand()/RAND_MAX;
						   
						   //indices to texture coordinates
						   x/=(float)grid.ni;
						   y/=(float)grid.nj;
						   z/=(float)grid.nk;
						   
						   Vec3f location;
						   location[0] = x*grid.ni*grid.dx;
						   location[1] = y*grid.nj*grid.dx;
						   location[2] = z*grid.nk*grid.dx;
											   
						   grid.bary_x((float)location[0], ui, ufx);
						   grid.bary_x_centre((float)location[0], idi, fx);
						   grid.bary_y((float)location[1], vj, vfy);
						   grid.bary_y_centre((float)location[1], idj, fy);
						   grid.bary_z((float)location[2], wk, wfz);
						   grid.bary_z_centre((float)location[2], idk, fz);

						   Vec3f velocity;

						   velocity[0]= grid.u.gridtrilerp(ui,j,k,ufx,fy,fz);
						   velocity[1]= grid.v.gridtrilerp(i,vj,k,fx,vfy,fz);
						   velocity[2]= grid.w.gridtrilerp(i,j,wk,fx,fy,wfz);

						   setParticle(location,velocity);
					   }
				   }
			   }
		   }
	   }
	   
	   unsigned int p = this->particles.numberOfParticles;
	   while(p>=1)
	   {
		   p--;
		   Vec3ui cell = this->particles.get_cell_of_particle(p);
		   
		   if(!fillHolesCells(cell[0], cell[1], cell[2]))
		   {
			   this->removeParticle(p);
		   }
	   }
   }
   //std::cout<<"    Simulation step finished"<<std::endl;
}

void FLIP_simulation::advance_one_frame(double frametime)
{
   //std::cout<<"Advance one frame"<<std::endl;

   frameTime = frametime;
   frameTimeProcessed = 0.0;
   double t=0;
   double dt;
   bool finished=false;

   if((frameTime>0.0f && using_negative_time_step) || (frameTime<0.0f && !using_negative_time_step))
	   //negative timestep => reverse velocities of particles and body forces
   {
	   using_negative_time_step = (frameTime<0.0f); //such that we know for later steps (such as the sources and sinks that things are different)
	   
	   for(unsigned int p = 0; p<particles.u.size(); p++)
	   {
		   particles.u.at(p)[0] = -particles.u.at(p)[0];
		   particles.u.at(p)[1] = -particles.u.at(p)[1];
		   particles.u.at(p)[2] = -particles.u.at(p)[2];
	   }

	   Vec3f body_force;
	   body_force[0] = -grid.body_force[0];
	   body_force[1] = -grid.body_force[1];
	   body_force[2] = -grid.body_force[2];
	   grid.set_body_force(body_force);

	   particles.transfer_to_grid();
   }

   //OutputDebugString(L"Advance one frame\n");
   if(firstRun)
   {
	   particles.transfer_to_grid(); // make sure that the first time there is a velocity field to compute the cfl with
	   firstRun = false;
   }

   while(!finished){
	   //place everything on the field (which will be made 0 anyway during the process)
	  dt=grid.cfl();
	  if(abs(t)+abs(dt)>=abs(frametime)) //abs to be able to work with negative time steps
	  {
		 dt=frametime-t; //also works with negative time steps
		 finished=true;
	  }
	  else 
	  {
		  //split up  the last step so we do not have to take a very small timestep in the end
		  if(abs(t)+abs(1.5*dt)>=abs(frametime)) //abs to be able to work with negative time steps
		  {
			  dt=0.5*(frametime-t); //also works with negative time steps
		  }
	  }
	  
	  if(frameTime<0.0f) //if the time step should be negative it is fixed here
		  dt = -abs(dt);

	  double td = abs(dt);
	  advance_one_step(td); //simulate with possitve timestep since we reversed the velocity and forces already
	  frameTimeProcessed += dt; //also works with negative time steps
	  t+=dt;					//also works with negative time steps
   }
   if(firstRun) firstRun = false;

   //std::cout<<"Frame finished"<<std::endl;
}

/**
* Use a no stick boundary condtion
*/
void FLIP_simulation::set_no_stick()
{
	grid.no_stick = true;
}

/**
* Use a no slip boundary condtion (most accurate but can create a layer of fluid near a boundary)
*/
void FLIP_simulation::set_no_slip()
{
	grid.no_stick = false;
}

void FLIP_simulation::setSolids(Array3f f)
{
	Array3f nodal_solid(grid.nodal_solid_phi.ni,grid.nodal_solid_phi.nj,grid.nodal_solid_phi.nk);

	for (unsigned int k = 0; k<f.nk; k++)
	{
		for (unsigned int j = 0; j<f.nj; j++)
		{
			for (unsigned int i = 0; i<f.ni; i++)
			{
				phi_solid(i,j,k) = f(i,j,k);
			}
		}
	}
	grid.set_boundary(f);
}

void FLIP_simulation::setPotentialField(Array3f f)
{
	grid.set_potential(f);
}

void FLIP_simulation::setViscosistyPerCell(Array3f f)
{
	grid.set_viscosity_per_cell(f);
}

void FLIP_simulation::setViscosity(float f)
{
	grid.set_viscosity(f);
}

void FLIP_simulation::setParticle(Vec3f position, Vec3f velocity)
{
	unsigned int i,j,k;
	float fx,fy,fz;
	grid.bary_x((float)position[0], i, fx);
	grid.bary_y((float)position[1], j, fy);
	grid.bary_z((float)position[2], k, fz);
	
	//if(grid.particleCells.inRange(i,j,k) && grid.particleCells.inRange(i+1,j+1,k+1))
	//{
		/**
		if(sink_cells(i,j,k))
		{
			std::cout<<"Cannot add particle in sink cell"<<std::endl;
			return;
		}		
		*/

		particles.add_particle(position,velocity);
		grid.particleCells(i,j,k)++;
	//}
}

void  FLIP_simulation::removeParticle(unsigned int index)
{
	if(index>=this->getNumberOfParticles())
		return;

	Vec3f position = this->particles.x.at(index);
	particles.remove_particle(index);
	grid.particleCells(floor(position[0]+0.5f), floor(position[1]+0.5f), floor(position[2]+0.5f))--;
}

void FLIP_simulation::fillSourceCell(unsigned int i, unsigned int j, unsigned int k, float timeFraction)
{
	if(!grid.particleCells.inRange(i,j,k))
		return;
	if(!grid.particleCells.inRange(i+1,j+1,k+1)) //to make sure we can have an offset within the cell
		return;

	float x = (float)i/grid.ni;
	float y = (float)j/grid.nj;
	float z = (float)k/grid.nk;

	float lx = grid.dx * grid.ni;
	float ly = grid.dx * grid.nj;
	float lz = grid.dx * grid.nk;

	float range = 1.0f/(float)grid.ni;

	Vec3f location;

	if(grid.initialParticlesPerCell - grid.particleCells(i,j,k)>0)
	{
		for(unsigned int p = 0; p < grid.initialParticlesPerCell - grid.particleCells(i,j,k); p++)
		{
			location[0] = lx*(x + range*randhashf(++randomSeed, 0,1));
			location[1] = ly*(y + range*randhashf(++randomSeed, 0,1));
			location[2] = lz*(z + range*randhashf(++randomSeed, 0,1));

			Vec3f velocityCur  = trilerpVectorField(&velocity_field_current_frame, location);
			Vec3f velocityNext = trilerpVectorField(&velocity_field_next_frame,    location);

			Vec3f velocity(0.0f,0.0f,0.0f);

			velocity[0] = (1.0f - timeFraction) * velocityCur[0] + timeFraction * velocityNext[0];
			velocity[1] = (1.0f - timeFraction) * velocityCur[1] + timeFraction * velocityNext[1];
			velocity[2] = (1.0f - timeFraction) * velocityCur[2] + timeFraction * velocityNext[2];

			setParticle(location, velocity);
		}
	}
}

void FLIP_simulation::setBodyForce(Vec3f Force)
{
	bodyForce = Force;

	grid.set_body_force(Force);
}

Vec3f FLIP_simulation::getBodyForce(void)
{
	return bodyForce;
}

Array3f FLIP_simulation::meshToLevelSet(std::vector<Vec3ui> triangles, std::vector<Vec3f> vertices)
{
	Array3f result(grid.nodal_solid_phi.ni,grid.nodal_solid_phi.nj,grid.nodal_solid_phi.nk);
	result.assign(0);
	Vec3f origin(0.0,0.0,0.0); //sets the origin to (0,0,0)
	make_level_set3(triangles, vertices,
					 origin, grid.dx, result.ni, result.nj, result.nk,
					 result,1);

	return result;
}

Vec3f FLIP_simulation::trilerpVectorField(Array3Vec3 *field, Vec3f location)
{
	unsigned int i,j,k;
	float fx,fy,fz;
	grid.bary_x((float)location[0], i, fx);
	grid.bary_y((float)location[1], j, fy);
	grid.bary_z((float)location[2], k, fz);

	Vec3f result(0.0f,0.0f,0.0f);
	if(!field->inRange(i,j,k) || !field->inRange(i+1,j+1,k+1)) // cannot do propper trilerp at the  boundary
		return result;

	unsigned int ni = field->ni; unsigned int nj = field->nj; unsigned int nk = field->nk;

	for(unsigned int it = 0; it<3; it++)
	{
		float d00 = field->a[i+ni*(j+nj*k)][it]      *(1-fx)+field->a[i+1+ni*(j+nj*k)][it]      *fx;//(i,j,k) and (i,j+1,k)
		float d10 = field->a[i+ni*(j+1+nj*k)][it]    *(1-fx)+field->a[i+1+ni*(j+1+nj*k)][it]    *fx;//(i,j+1,k) and (i+1,j+1,k)
		float d01 = field->a[i+ni*(j+nj*(k+1))][it]  *(1-fx)+field->a[i+1+ni*(j+nj*(k+1))][it]  *fx;//(i,j,k+1) and (i+1,j,k+1)
		float d11 = field->a[i+ni*(j+1+nj*(k+1))][it]*(1-fx)+field->a[i+1+ni*(j+1+nj*(k+1))][it]*fx;//(i,j+1,k+1) and (i+1,j+1,k+1)
		//now we do not need the x dimension anymore, do bilerp on y using the corners above to get a line:
		float e0 = d00*(1-fy)+d10*fy;
		float e1 = d01*(1-fy)+d11*fy;
		//now we only need liniear interpolation on the line defined above:
		result[it] = e0*(1-fz)+e1*fz;
	}
	return result;
}