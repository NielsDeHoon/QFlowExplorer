#pragma once
#include "array3.h"
#include "util.h"
#include "vec.h"
#include <vector>

#include "sparse_matrix.h"
#include "pcg_solver.h"

class MACGrid
{
public:
	~MACGrid(void);

	MACGrid(unsigned int ni_, unsigned int nj_, unsigned int nk_, float width_, float density)
	{ init(ni_, nj_, nk_, width_, density); }

	//initialization functions:
	void init(unsigned int cell_ni, unsigned int cell_nj, unsigned int cell_nk, float width_, float density); //initializes the grid
	void set_boundary(Array3f phi);								 //sets the implicit boundary
	void set_viscosity_per_cell(Array3f visc);					 //sets viscosity
	void set_viscosity(float visc);								 //sets viscosity
	void set_potential(Array3f Pot);							 //sets a potential field
	void set_body_force(Vec3f);									 //sets a body force to the velocity field
	
	//runtime functions:
	float cfl();												 //computes the cfl condition
	void save_velocities(void);									 //makes a backup of the velocities
	void get_velocity_update(void);								 //makes a backup of the velocities
	void add_potential(float dt); 								 //adds a potential field to the velocity field
	void add_body_force(float dt);								 //adds a body force to the velocity field
	void compute_curl();										 //computes the curl, stored on cell edges
	void apply_projection(float dt);							 //applies the pressure projection
	void apply_viscosity(float dt);								 //applies the viscosity
	void compute_pressure_weights();							 //computes the weights used for the pressure computations
	void compute_viscosity_weights();							 //computes the stress samples used for the viscosity computations
	void solve_pressure(float dt);								 //resolves the pressure of the velocity field
	void solve_viscosity(float dt);								 //resolves the viscosity
	void extend_velocity();										 //extends the velocity field
	Vec3f calculateSurfaceNormal(Array3f& grid, Vec3f location); //calculates the surface normal at the given position using the gradient

	//Externally available variables:
	Array3f u, v, w; 											 //contains the velocity field
	Array3f du, dv, dw; 										 //contains the backup velocity field
	Array3f vel_pressure;										 //contains the pressure of the velocity field

	//Static geometry representation
	Array3f nodal_solid_phi;
	Array3f solid_phi;
	
	//Fluid surface representation
	Array3f liquid_phi;											 //extracted from particles
	
	Array3f particleCells;										 //contains the number of particles per cell
	std::vector<Vec3f> particleLocations;						 //contains the particle locations

	unsigned int initialParticlesPerCell;						 //stores the number of particles that should be inserted per grid cell

	double timestep;
	float particle_radius;
	
	//Grid dimensions
	unsigned int ni,nj,nk;
	float lx,ly,lz; //grid width, height and depth
	float dx; //cell width

	//to use a no-stick boundary condition instead of a no-slip one:
	bool no_stick;

	//bary_q calculates the cell id i, j or k and the offset fq dependend on q in {x,y,z}
	//bary_q_centre calculates the offset with respect to the centre of the cell
	//calculate the barycentric points:
	void bary_x(float x, unsigned int &i, float &fx)
	{
		float sx=x*overh;
		i=(unsigned int)sx;
		fx=sx-floor(sx);
	}

	void bary_x_centre(float x, unsigned int &i, float &fx)
	{
		float sx=x*overh-(float)0.5;
		i=(unsigned int)sx;
		if(i<1){ i=1; fx=0.0; }
		else if(i>ni-2){ i=ni-2; fx=1.0; }
		else{ fx=sx-floor(sx); }
	}

	void bary_y(float y, unsigned int &j, float &fy)
	{	
		float sy=y*overh;
		j=(unsigned int)sy;
		fy=sy-floor(sy);
	}

	void bary_y_centre(float y, unsigned int &j, float &fy)
	{
		float sy=y*overh-(float)0.5;
		j=(unsigned int)sy;
		if(j<1){ j=1; fy=0.0; }
		else if(j>nj-2){ j=nj-2; fy=1.0; }
		else{ fy=sy-floor(sy); }
	}

	void bary_z(float z, unsigned int &k, float &fz)
	{
		float sz=z*overh;
		k=(unsigned int)sz;
		fz=sz-floor(sz);
	}

	void bary_z_centre(float z, unsigned int &k, float &fz)
	{
		float sz=z*overh-(float)0.5;
		k=(unsigned int)sz;
		if(k<1){ k=1; fz=0.0; }
		else if(k>nk-2){ k=nk-2; fz=1.0; }
		else{ fz=sz-floor(sz); }
	}

	void trilerp_uvw(float px, float py, float pz, float &pu, float &pv, float &pw)
	{
		unsigned int i, j, k; //cell id in which p currently is
		float fx, fy, fz; //offset
		//calculate the offset in q direction on the cell boundary to get the speed in the q direction
		bary_x(px, i, fx); 
		bary_y(py, j, fy);
		bary_z(pz, k, fz);
		//for the other directions we need the offset of the centre.
		bary_x_centre(px, i, fx);
		bary_y_centre(py, j, fy);
		bary_z_centre(pz, k, fz);
		//now we can calculate the speed of the particle in the u (x) direction with trilinear interpolation
		pu=u.gridtrilerp(i, j, k, fx, fy, fz);
		pv=v.gridtrilerp(i, j, k, fx, fy, fz);
		pw=w.gridtrilerp(i, j, k, fx, fy, fz);
	}

//private:
	//velocity field data:
	Array3f u_weights, v_weights, w_weights, c_weights;			 //contains the velocity weights for the pressure solve
	Array3f temp_u, temp_v, temp_w;
	float overh;

	bool use_viscosity;

	float rho;													// contains the (constant) density of the fluid

	Array3f potential;											 //contains the potential field

	Vec3f body_force;							 //stores the body force applied on the system

	//Solver data:
	PCGSolver<double> solver;
	SparseMatrixd matrix;
	std::vector<double> rhs;
	std::vector<double> pressure;

	//Data arrays for extrapolation
	Array3c u_valid, v_valid, w_valid;
	Array3c valid, old_valid;

	//Data arrays for viscosity computations (storing stress samples)
	Array3f u_vol, v_vol, w_vol;
	Array3f c_vol, ex_vol, ey_vol, ez_vol;

	//Data arrays for curl computations (storing curl on cell edges)
	Array3f curl_x, curl_y, curl_z;

	Array3f viscosity;
	SparseMatrixd vmatrix;
	std::vector<double> vrhs;
	std::vector<double> vsoln;

	float visc;

	//Helper functions
	//velocity sweeping functions, used to extend the velocity field
	void extrapolate(Array3f& grid, Array3c& valid);
	void constrain_velocity(); //constrains the velocity field
	float fraction_inside(float phi_left, float phi_right);
	float fraction_inside(float lt, float lu, float rt, float ru);
	float interpolate_value(const Vec3f& point, const Array3f& grid);
	void compute_volume_fractions(const Array3f& levelset, Array3f& fractions, Vec3f fraction_origin, unsigned int subdivision);
	unsigned int matrixIndex(unsigned int i, unsigned int j, unsigned int k);

	float interpolate_phi(const Vec3f& point, const Array3f& grid, const Vec3f& origin, const float dx);

	void estimate_volume_fractions(Array3f& volumes,
							   const Vec3f& start_centre, const float dx,
							   const Array3f& phi, const Vec3f& phi_origin, const float phi_dx);
	 
	//viscocity indexing functions:
	unsigned int u_ind(unsigned int i, unsigned int j, unsigned int k);
	unsigned int v_ind(unsigned int i, unsigned int j, unsigned int k);
	unsigned int w_ind(unsigned int i, unsigned int j, unsigned int k);
};