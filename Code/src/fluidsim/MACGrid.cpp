#include "MACGrid.h"
#include "volume_fractions.h"

/*INITIALIZATION FUNCTIONS*/

/** 
 * Initializes the grid 
 * cell_ni, cell_nj and cell_nk should represent the number of cells should be 
 * in the grid in respectively the x,y and z direction
 * width_ specifies the width of the grid
 * density specifies the (constant) density of the fluid
 * 
 * Initializes the MAC grid and allocates all required data structures
 */
void MACGrid::init(unsigned int cell_ni, unsigned int cell_nj, unsigned int cell_nk, float width_, float density)
{
	ni = cell_ni;
	nj = cell_nj;
	nk = cell_nk;

	//initialize all the grid variables
	lx = width_;
	ly=cell_nj*lx/cell_ni;
	lz=cell_nk*lx/cell_ni;
	dx = width_/(float)cell_ni;
	overh=cell_ni/lx;

	rho = density;
	
	u.init(cell_ni+1, cell_nj, cell_nk);
	v.init(cell_ni, cell_nj+1, cell_nk);
	w.init(cell_ni, cell_nj, cell_nk+1);

	du.init(cell_ni+1, cell_nj, cell_nk);
	dv.init(cell_ni, cell_nj+1, cell_nk);
	dw.init(cell_ni, cell_nj, cell_nk+1);

	u_weights.init(cell_ni+1, cell_nj, cell_nk);
	v_weights.init(cell_ni, cell_nj+1, cell_nk);
	w_weights.init(cell_ni, cell_nj, cell_nk+1);

	temp_u.init(cell_ni+1, cell_nj, cell_nk);
	temp_v.init(cell_ni, cell_nj+1, cell_nk);
	temp_w.init(cell_ni, cell_nj, cell_nk+1);

	u_valid.init(cell_ni+1, cell_nj, cell_nk);
	v_valid.init(cell_ni, cell_nj+1, cell_nk);
	w_valid.init(cell_ni, cell_nj, cell_nk+1);

	u_vol.init(cell_ni+1,cell_nj,cell_nk);
	v_vol.init(cell_ni,cell_nj+1,cell_nk);
	w_vol.init(cell_ni,cell_nj,cell_nk+1);
	
	c_vol.init(cell_ni,cell_nj,cell_nk);
	ex_vol.init(cell_ni,cell_nj+1,cell_nk+1);
	ey_vol.init(cell_ni+1,cell_nj,cell_nk+1);
	ez_vol.init(cell_ni+1,cell_nj+1,cell_nk);

	curl_x.init(cell_ni, cell_nj, cell_nk);
	curl_y.init(cell_ni, cell_nj, cell_nk);
	curl_z.init(cell_ni, cell_nj, cell_nk);
	
	viscosity.init(cell_ni,cell_nj,cell_nk);
	visc = 0.0f;

	vel_pressure.init(cell_ni, cell_nj, cell_nk);

	potential.init(cell_ni, cell_nj, cell_nk);
	particleCells.init(cell_ni, cell_nj, cell_nk);

	nodal_solid_phi.init(cell_ni+1, cell_nj+1, cell_nk+1);
	solid_phi.init(cell_ni, cell_nj, cell_nk);
	liquid_phi.init(cell_ni, cell_nj, cell_nk);

	valid.resize(cell_ni+1, cell_nj+1, cell_nk+1);
	old_valid.resize(cell_ni+1, cell_nj+1, cell_nk+1);

	body_force[0] = 0.0; body_force[1] = 0.0; body_force[2] = 0.0;

	initialParticlesPerCell = 8;
	
	particle_radius =  dx/sqrt(2.0f);

	use_viscosity = false;
	
	no_stick = false;
}

/**
* Destructor
*/
MACGrid::~MACGrid(void)
{
}

/**
* Initializes an implicit boundary
*/
void MACGrid::set_boundary(Array3f phi)
{
	for(unsigned int k = 0; k< nodal_solid_phi.nk; ++k) for(unsigned int j = 0; j < nodal_solid_phi.nj; ++j) for(unsigned int i = 0; i < nodal_solid_phi.ni; ++i)
	{	
		nodal_solid_phi(i,j,k) = -1.0f/dx;
		if(i<ni && j<nj && k<nk)
		{
			nodal_solid_phi(i,j,k) = phi(i,j,k);		
			solid_phi(i,j,k) = phi(i,j,k);
		}
	}
}

/**
* sets a viscosity
*/
void MACGrid::set_viscosity_per_cell(Array3f visc)
{
	use_viscosity = true;
	for(unsigned int k = 0; k < nk; k++) for(unsigned int j = 0; j < nj; j++) for(unsigned int i = 0; i < ni; i++)
	{
		viscosity(i,j,k) = visc(i,j,k);
		this->visc += visc(i,j,k);
	}
	this->visc /= viscosity.a.size();
}

/**
* sets a viscosity
*/
void MACGrid::set_viscosity(float visc)
{
   if(visc == 0.0f) 
	   use_viscosity = false;
   else
	   use_viscosity = true;
   for(unsigned int k = 0; k < nk; k++) for(unsigned int j = 0; j < nj; j++) for(unsigned int i = 0; i < ni; i++)
   {
	   viscosity(i,j,k) = visc;
   }
   this->visc = visc;
}

/**
* sets a potential field
*/
void MACGrid::set_potential(Array3f Pot)
{
	potential.set_zero();
	for(unsigned int k = 0; k<potential.nk; k++) for(unsigned int j = 0; j<potential.nj; j++) for(unsigned int i = 0; i<potential.ni; i++)
	{
		potential(i,j,k)=Pot(i,j,k);
	}
}

/**
* sets a body force to the velocity field
*/
void MACGrid::set_body_force(Vec3f Force)
{
	body_force = Force;
}
	
/*RUNTIME FUNCTIONS*/

/**
* computes the cfl condition the system should obbey
*/
float MACGrid::cfl()
{
	float maxvel = 0;
	for(unsigned int i = 0; i < u.a.size(); ++i)
	{
		maxvel = max(maxvel, fabs(u.a[i]));
	}
	for(unsigned int i = 0; i < v.a.size(); ++i)
	{
		maxvel = max(maxvel, fabs(v.a[i]));
	}
	for(unsigned int i = 0; i < w.a.size(); ++i)
	{
		maxvel = max(maxvel, fabs(w.a[i]));
	}
	if(maxvel == 0.0f)
		return 1e12f; //if no velocity in the system the timestep can be huge
	return (dx / maxvel);
}

void MACGrid::save_velocities(void)
{
	for(unsigned int i = 0; i < u.ni; i++) for(unsigned int j = 0; j < u.nj; j++) for(unsigned int k = 0; k < u.nk; k++)
	{
		du(i,j,k)=u(i,j,k);
	}
	for(unsigned int i = 0; i < v.ni; i++) for(unsigned int j = 0; j < v.nj; j++) for(unsigned int k = 0; k < v.nk; k++)
	{
		dv(i,j,k)=v(i,j,k);
	}
	for(unsigned int i = 0; i < w.ni; i++) for(unsigned int j = 0; j < w.nj; j++) for(unsigned int k = 0; k < w.nk; k++)
	{
		dw(i,j,k)=w(i,j,k);
	}
}

void MACGrid::get_velocity_update(void)
{
	for(unsigned int i = 0; i < u.ni; i++) for(unsigned int j = 0; j < u.nj; j++) for(unsigned int k = 0; k < u.nk; k++)
	{
		du(i,j,k)=u(i,j,k)-du(i,j,k);
	}
	for(unsigned int i = 0; i < v.ni; i++) for(unsigned int j = 0; j < v.nj; j++) for(unsigned int k = 0; k < v.nk; k++)
	{
		dv(i,j,k)=v(i,j,k)-dv(i,j,k);
	}
	for(unsigned int i = 0; i < w.ni; i++) for(unsigned int j = 0; j < w.nj; j++)	for(unsigned int k = 0; k < w.nk; k++)
	{
		dw(i,j,k)=w(i,j,k)-dw(i,j,k);
	}
}

/**
* Apllies the given potential field to the velocity field
*/
void MACGrid::add_potential(float dt)
{
	for(unsigned int k = 1; k<potential.nk; k++) for(unsigned int j = 1; j<potential.nj; j++) for(unsigned int i = 1; i<potential.ni; i++)
	{
		//alter the speed using the negative gradient of the potential:
		assert(potential(i,j,k)>=0); //Potential should be >= 0 to be valid
		
		u(i,j,k)  += (float) dt*(potential(i-1,j,k)-potential(i,j,k));

		v(i,j,k)  += (float) dt*(potential(i,j-1,k)-potential(i,j,k));

		w(i,j,k)  += (float) dt*(potential(i,j,k-1)-potential(i,j,k));
	}
}

/**
* Applies the given body force to the velocity field
*/
void MACGrid::add_body_force(float dt)
{
	for (unsigned int k = 0; k < u.nk; k++) for (unsigned int j = 0; j < u.nj; j++) for (unsigned int i = 0; i < u.ni; i++)
	{
		u(i,j,k)+=(float)body_force[0]*dt;
	}
	for (unsigned int k = 0; k < v.nk; k++) for (unsigned int j = 0; j < v.nj; j++) for (unsigned int i = 0; i < v.ni; i++)
	{
		v(i,j,k)+=(float)body_force[1]*dt;
	}
	for (unsigned int k = 0; k < w.nk; k++) for (unsigned int j = 0; j < w.nj; j++) for (unsigned int i = 0; i < w.ni; i++)
	{
		w(i,j,k)+=(float)body_force[2]*dt;
	}
}

/**
 * Extends the fluid velocity field in the space outside the fluid
*/
void MACGrid::extend_velocity()
{
	extrapolate(u, u_valid);
	extrapolate(v, v_valid);
	extrapolate(w, w_valid);

	constrain_velocity();
}

Vec3f MACGrid::calculateSurfaceNormal(Array3f& grid, Vec3f location)
{
	float length;
	unsigned int i,j,k;
	float fx,fy,fz;
	Vec3f normal(3);

	bary_x_centre((float)location[0], i, fx);
	bary_y_centre((float)location[1], j, fy);
	bary_z_centre((float)location[2], k, fz);

	//take the gradient:
	float fCentre = grid.gridtrilerp(i,j,k,fx,fy,fz);

	//one sided:
	normal[0] = -(fCentre- grid.gridtrilerp(i+1,j,k,fx,fy,fz));
	normal[1] = -(fCentre- grid.gridtrilerp(i,j+1,k,fx,fy,fz));
	normal[2] = -(fCentre- grid.gridtrilerp(i,j,k+1,fx,fy,fz));

	length = sqrt(sqr(normal[0])+sqr(normal[1])+sqr(normal[2]));
	if (length==0.0) // if null vector take left handed gradient
	{
		normal[0] = -(grid.gridtrilerp(i-1,j,k,fx,fy,fz)-fCentre);
		normal[1] = -(grid.gridtrilerp(i,j-1,k,fx,fy,fz)-fCentre);
		normal[2] = -(grid.gridtrilerp(i,j,k-1,fx,fy,fz)-fCentre);
		length = sqrt(sqr(normal[0])+sqr(normal[1])+sqr(normal[2]));
	}
	
	//normalize it:
	if(length==0.0)
	{
		normal[0] = 1.0; normal[1] = 0.0; normal[2] = 0.0;
		length = 1.0f;
	}

	normal[0]/=length;
	normal[1]/=length;
	normal[2]/=length;

	return normal;
}

/**
* Applies the pressure projection on the velocity field
*/
void MACGrid::apply_projection(float dt)
{
   //Compute finite-volume type face area weight for each velocity sample.
   //compute_pressure_weights();
   compute_volume_fractions(nodal_solid_phi, u_weights, Vec3f(-0.5,0.0, 0.0 ), 2);
   compute_volume_fractions(nodal_solid_phi, v_weights, Vec3f(0.0, -0.5,0.0 ), 2);
   compute_volume_fractions(nodal_solid_phi, w_weights, Vec3f(0.0, 0.0, -0.5), 2);
   compute_volume_fractions(nodal_solid_phi, c_weights, Vec3f(0.0, 0.0, 0.0 ), 2);

   //inverse:
   for(unsigned int k = 0; k<u_weights.nk; ++k) for(unsigned int j = 0; j<u_weights.nj; ++j) for(unsigned int i = 0; i<u_weights.ni; ++i) 
	   u_weights(i,j,k) = 1.0f - u_weights(i,j,k);
   for(unsigned int k = 0; k<v_weights.nk; ++k) for(unsigned int j = 0; j<v_weights.nj; ++j) for(unsigned int i = 0; i<v_weights.ni; ++i) 
	   v_weights(i,j,k) = 1.0f - v_weights(i,j,k);
   for(unsigned int k = 0; k<w_weights.nk; ++k) for(unsigned int j = 0; j<w_weights.nj; ++j) for(unsigned int i = 0; i<w_weights.ni; ++i) 
	   w_weights(i,j,k) = 1.0f - w_weights(i,j,k);
   for(unsigned int k = 0; k<c_weights.nk; ++k) for(unsigned int j = 0; j<c_weights.nj; ++j) for(unsigned int i = 0; i<c_weights.ni; ++i) 
	   c_weights(i,j,k) = 1.0f - c_weights(i,j,k);
   
   //Set up and solve the variational pressure solve.
   solve_pressure(dt);
}

void MACGrid::apply_viscosity(float dt) 
{   
   if(use_viscosity)
   {
	   //Estimate weights at velocity and stress positions
	   compute_viscosity_weights();

	   //Set up and solve the linear system
	   solve_viscosity(dt);
   }
}  

/**
* Computes the weights used for the pressure computations
*/
void MACGrid::compute_pressure_weights()
{
	u_weights.set_zero();
	v_weights.set_zero();
	w_weights.set_zero();

	//for the u component
	for(unsigned int k = 0; k < u_weights.nk; ++k) for(unsigned int j = 0; j < u_weights.nj; ++j) for(unsigned int i = 0; i < u_weights.ni; ++i) 
	{
		u_weights(i,j,k) = 1.0f - fraction_inside(nodal_solid_phi(i,j+1,k), nodal_solid_phi(i,j,k),nodal_solid_phi(i,j+1,k+1), nodal_solid_phi(i,j,k+1));
	}

	//for the v component
	for(unsigned int k = 0; k < v_weights.nk; ++k) for(unsigned int j = 0; j < v_weights.nj; ++j) for(unsigned int i = 0; i < v_weights.ni; ++i) 
	{
		v_weights(i,j,k) = 1.0f - fraction_inside(nodal_solid_phi(i+1,j,k), nodal_solid_phi(i,j,k),nodal_solid_phi(i+1,j,k+1), nodal_solid_phi(i,j,k+1));
	}

	//for the w component
	for(unsigned int k = 0; k < w_weights.nk; ++k) for(unsigned int j = 0; j < w_weights.nj; ++j) for(unsigned int i = 0; i < w_weights.ni; ++i) 
	{
		w_weights(i,j,k) = 1.0f - fraction_inside(nodal_solid_phi(i+1,j,k), nodal_solid_phi(i,j,k),nodal_solid_phi(i+1,j+1,k), nodal_solid_phi(i,j+1,k));
	}
}

void MACGrid::compute_viscosity_weights() 
{   
	estimate_volume_fractions(c_vol, Vec3f(0.5f*dx, 0.5f*dx, 0.5f*dx), dx, liquid_phi, Vec3f(0,0,0), dx);
	
	estimate_volume_fractions(u_vol, Vec3f(0, 0.5f*dx, 0.5f*dx), dx, liquid_phi, Vec3f(0,0,0), dx);
	estimate_volume_fractions(v_vol, Vec3f(0.5f*dx, 0, 0.5f*dx), dx, liquid_phi, Vec3f(0,0,0), dx);
	estimate_volume_fractions(w_vol, Vec3f(0.5f*dx, 0.5f*dx, 0), dx, liquid_phi, Vec3f(0,0,0), dx);
	
	estimate_volume_fractions(ex_vol, Vec3f(0.5f*dx, 0, 0), dx, liquid_phi, Vec3f(0,0,0), dx);
	estimate_volume_fractions(ey_vol, Vec3f(0, 0.5f*dx, 0), dx, liquid_phi, Vec3f(0,0,0), dx);
	estimate_volume_fractions(ez_vol, Vec3f(0, 0, 0.5f*dx), dx, liquid_phi, Vec3f(0,0,0), dx);
}

/**
* Resolves the pressure of the velocity field
*/
void MACGrid::solve_pressure(float dt)
{
	unsigned int system_size = ni*nj*nk;

	if(rhs.size() != system_size)
	{
		rhs.resize(system_size);
		pressure.resize(system_size);
		matrix.resize(system_size);
	}
	
	matrix.zero();

	float minimal_theta = 0.01f;

	//Build the linear system for pressure:
	//density (rho) is left out
	float term;
	float theta;
	float mat_scale = (float)timestep/(rho*sqr(dx)); //with rho: timestep/(rho^2*dx^2)
	float rhs_scale = 1.0f/dx;			//with rho: 1.0/(rho*dx)

	for(unsigned int k = 1; k < nk-1; ++k) 
	{
		for(unsigned int j = 1; j < nj-1; ++j) 
		{
			for(unsigned int i = 1; i < ni-1; ++i) 
			{
				int index = matrixIndex(i,j,k);

				rhs[index] = 0;
				pressure[index] = 0;

				float centre_phi = liquid_phi(i,j,k);
				
				if(centre_phi < 0)
				{
					rhs[index] =  -rhs_scale * u_weights(i+1,j,k) * u(i+1,j,k) + rhs_scale * u_weights(i,j,k) * u(i,j,k)
								+ -rhs_scale * v_weights(i,j+1,k) * v(i,j+1,k) + rhs_scale * v_weights(i,j,k) * v(i,j,k)
								+ -rhs_scale * w_weights(i,j,k+1) * w(i,j,k+1) + rhs_scale * w_weights(i,j,k) * w(i,j,k);
					//no moving solids are taken into account
				}
			} //end for i	
		} //end for j
	} //end for k

	for(unsigned int k = 1; k < nk-1; ++k) 
	{
		for(unsigned int j = 1; j < nj-1; ++j) 
		{
			for(unsigned int i = 1; i < ni-1; ++i) 
			{
				unsigned int index_centre = matrixIndex(i,j,k);
				unsigned int index_right  = matrixIndex(i+1,j,k);
				unsigned int index_left   = matrixIndex(i-1,j,k);
				unsigned int index_top	  = matrixIndex(i,j+1,k);
				unsigned int index_bottom = matrixIndex(i,j-1,k);
				unsigned int index_front  = matrixIndex(i,j,k+1);
				unsigned int index_back	  = matrixIndex(i,j,k-1);

				float centre_phi = liquid_phi(i,j,k);
				float right_phi = liquid_phi(i+1,j,k);
				float left_phi = liquid_phi(i-1,j,k);
				float top_phi = liquid_phi(i,j+1,k);
				float bottom_phi = liquid_phi(i,j-1,k);
				float front_phi = liquid_phi(i,j,k+1);
				float back_phi = liquid_phi(i,j,k-1);
				
				if(centre_phi < 0)
				{
					term = mat_scale*u_weights(i+1,j,k);
					if(right_phi < 0)
					{
						matrix.add_to_element(index_centre, index_centre,  term);
						matrix.add_to_element(index_centre, index_right,  -term);
					}
					else
					{
						theta = fraction_inside(centre_phi, right_phi);
						if(theta < minimal_theta) theta = minimal_theta;
						matrix.add_to_element(index_centre, index_centre, term/theta);
					}
					
					term = mat_scale*u_weights(i,j,k);
					if(left_phi < 0)
					{
						matrix.add_to_element(index_centre, index_centre,  term);
						matrix.add_to_element(index_centre, index_left,   -term);
					}
					else
					{
						theta = fraction_inside(centre_phi, left_phi);
						if(theta < minimal_theta) theta = minimal_theta;
						matrix.add_to_element(index_centre, index_centre, term/theta);
					}

					term = mat_scale*v_weights(i,j+1,k);
					if(top_phi < 0)
					{
						matrix.add_to_element(index_centre, index_centre,  term);
						matrix.add_to_element(index_centre, index_top,    -term);
					}
					else
					{
						theta = fraction_inside(centre_phi, top_phi);
						if(theta < minimal_theta) theta = minimal_theta;
						matrix.add_to_element(index_centre, index_centre, term/theta);
					}

					term = mat_scale*v_weights(i,j,k);
					if(bottom_phi < 0)
					{
						matrix.add_to_element(index_centre, index_centre,  term);
						matrix.add_to_element(index_centre, index_bottom, -term);
					}
					else
					{
						theta = fraction_inside(centre_phi, bottom_phi);
						if(theta < minimal_theta) theta = minimal_theta;
						matrix.add_to_element(index_centre, index_centre, term/theta);
					}

					term = mat_scale*w_weights(i,j,k+1);
					if(front_phi < 0)
					{
						matrix.add_to_element(index_centre, index_centre,  term);
						matrix.add_to_element(index_centre, index_front,  -term);
					}
					else
					{
						theta = fraction_inside(centre_phi, front_phi);
						if(theta < minimal_theta) theta = minimal_theta;
						matrix.add_to_element(index_centre, index_centre, term/theta);
					}

					term = mat_scale*w_weights(i,j,k);
					if(back_phi < 0)
					{
						matrix.add_to_element(index_centre, index_centre,  term);
						matrix.add_to_element(index_centre, index_back,   -term);
					}
					else
					{
						theta = fraction_inside(centre_phi, back_phi);
						if(theta < minimal_theta) theta = minimal_theta;
						matrix.add_to_element(index_centre, index_centre, term/theta);
					}
				}
			} //end for i	
		} //end for j
	} //end for k

	//Solve the system using Robert Bridson's incomplete Cholesky PCG solver
	double residu;
	int iterations;
	bool success = solver.solve(matrix, rhs, pressure, residu, iterations);
	if(!success) 
	{
		printf("WARNING: Pressure solve failed! Tolerance = %f after %d iterations\n",residu,iterations);
	}
	 
	//Apply the velocity update
	for(unsigned int k = 0; k < vel_pressure.nk; ++k) for(unsigned int j = 0; j < vel_pressure.nj; ++j) for(unsigned int i = 0; i < vel_pressure.ni; ++i)
	{
		unsigned int index = i+ni*(j+nj*k);
		vel_pressure(i,j,k) = (float)pressure[index];
	}

	u_valid.assign(0);
	v_valid.assign(0);
	w_valid.assign(0);

	for(unsigned int k = 0; k < u.nk; ++k) for(unsigned int j = 0; j < u.nj; ++j) for(unsigned int i = 1; i < u.ni-1; ++i) 
	{
		if(u_weights(i,j,k) > 0 && (liquid_phi(i,j,k) < 0 || liquid_phi(i-1,j,k) < 0))
		{
			theta = 1.0f;
			if(liquid_phi(i,j,k) >= 0 || liquid_phi(i-1,j,k) >= 0)
				theta = fraction_inside(liquid_phi(i,j,k),liquid_phi(i-1,j,k));
			if(theta < minimal_theta) theta = minimal_theta;
			u(i,j,k) -= (float)timestep * (vel_pressure(i,j,k) - vel_pressure(i-1,j,k)) / dx / theta;
			u_valid(i,j,k) =  1;
		}
	}
	
	for(unsigned int k = 0; k < v.nk; ++k) for(unsigned int j = 1; j < v.nj-1; ++j) for(unsigned int i = 0; i < v.ni; ++i) 
	{
		if(v_weights(i,j,k) > 0 && (liquid_phi(i,j,k) < 0 || liquid_phi(i,j-1,k) < 0))
		{
			theta = 1.0f;
			if(liquid_phi(i,j,k) >= 0 || liquid_phi(i,j-1,k) >= 0)
				theta = fraction_inside(liquid_phi(i,j,k),liquid_phi(i,j-1,k));
			if(theta < minimal_theta) theta = minimal_theta;
			v(i,j,k) -= (float)timestep * (vel_pressure(i,j,k) - vel_pressure(i,j-1,k)) / dx / theta;
			v_valid(i,j,k) =  1;
		}
	}
		
	for(unsigned int k = 1; k < w.nk-1; ++k) for(unsigned int j = 0; j < w.nj; ++j) for(unsigned int i = 0; i < w.ni; ++i) 
	{
		if(w_weights(i,j,k) > 0 && (liquid_phi(i,j,k) < 0 || liquid_phi(i,j,k-1) < 0))
		{
			theta = 1.0f;
			if(liquid_phi(i,j,k) >= 0 || liquid_phi(i,j,k-1) >= 0)
				theta = fraction_inside(liquid_phi(i,j,k),liquid_phi(i,j,k-1));
			if(theta < minimal_theta) theta = minimal_theta;
			w(i,j,k) -= (float)timestep * (vel_pressure(i,j,k) - vel_pressure(i,j,k-1)) / dx / theta;
			w_valid(i,j,k) =  1;
		}
	}

	//implement no stick condition
	if(no_stick)
	{
		for(unsigned int k = 1; k < nk-1; ++k) for(unsigned int j = 0; j < nj; ++j) for(unsigned int i = 0; i < ni; ++i)
		{
			if(w_weights(i,j,k) <= 0 && (liquid_phi(i,j,k) < 0))
			{
				Vec3f normal = calculateSurfaceNormal(nodal_solid_phi,Vec3f(i/lx,j/ly,k/lz));
				u(i,j,k) = -normal[0]*(float)timestep*dx;
				v(i,j,k) = -normal[1]*(float)timestep*dx;
				w(i,j,k) = -normal[2]*(float)timestep*dx;
			}
		}
	}
	else
	{
		for(unsigned int i = 0; i < u_valid.a.size(); ++i)
			if(u_valid.a[i] == 0)
				u.a[i] = 0;
		for(unsigned int i = 0; i < v_valid.a.size(); ++i)
			if(v_valid.a[i] == 0)
				v.a[i] = 0;
		for(unsigned int i = 0; i < w_valid.a.size(); ++i)
			if(w_valid.a[i] == 0)
				w.a[i] = 0;
	}
}

void MACGrid::solve_viscosity(float dt)
{   
   //static obstacles for simplicity - for moving objects, 
   //use a spatially varying 3d array, and modify the linear system appropriately

	float over_dx = 1.0f/dx;

	Array3c u_state(ni+1,nj,nk);
	Array3c v_state(ni,nj+1,nk);
	Array3c w_state(ni,nj,nk+1);

	int dim = (ni+1)*nj*nk + ni*(nj+1)*nk + ni*nj*(nk+1);
	if(vmatrix.n != dim) 
	{
		u_state.resize(ni+1,nj,nk);
		v_state.resize(ni,nj+1,nk);
		w_state.resize(ni,nj,nk+1);
		vmatrix.resize(dim);
		vrhs.resize(dim);
		vsoln.resize(dim);
	}
	matrix.resize(0);
	rhs.resize(0);

	u_state.assign(0);
	v_state.assign(0);
	w_state.assign(0);
	vmatrix.zero();

	vrhs.assign(dim, 0);
	vsoln.assign(dim, 0);

	const int SOLID = 3;
	const int FLUID = 2;
	//const int AIR = 1;

	//check if interpolated velocity positions are inside solid
	for(int k = 0; (unsigned)k < nk; ++k) for(int j = 0; (unsigned)j < nj; ++j) for(int i = 0; (unsigned)i < ni+1; ++i) 
	{
		if(i - 1 < 0 || (unsigned)i >= ni || solid_phi(i-1,j,k) + solid_phi(i,j,k) <= 0)
			u_state(i,j,k) = SOLID;
		else
			u_state(i,j,k) = FLUID;
	}

	for(int k = 0; (unsigned)k < nk; ++k) for(int j = 0; (unsigned)j < nj+1; ++j) for(int i = 0; (unsigned)i < ni; ++i) 
	{
		if(j - 1 < 0 || (unsigned)j >= nj || solid_phi(i,j-1,k) + solid_phi(i,j,k) <= 0)
			v_state(i,j,k) = SOLID;
		else
			v_state(i,j,k) = FLUID;
	}

	for(int k = 0; (unsigned)k < nk+1; ++k) for(int j = 0; (unsigned)j < nj; ++j) for(int i = 0; (unsigned)i < ni; ++i) 
	{
		if(k - 1 < 0 || (unsigned)k >= nk || solid_phi(i,j,k-1) + solid_phi(i,j,k) <= 0)
			w_state(i,j,k) = SOLID;
		else
			w_state(i,j,k) = FLUID;
	}

	float factor = dt*sqr(over_dx);

	//u-terms
	//2u_xx+ v_xy +uyy + u_zz + w_xz
	for(unsigned int k = 1; k < nk-1; ++k) for(unsigned int j = 1; j < nj-1; ++j) for(unsigned int i = 1; i < ni-1; ++i) 
	{
		if(u_state(i,j,k) == FLUID) 
		{			
			int index = u_ind(i,j,k);
			vrhs[index] = u_vol(i,j,k)*u(i,j,k);

			vmatrix.set_element(index,index,u_vol(i,j,k));

			float visc_right = viscosity(i,j,k);
			float visc_left = viscosity(i-1,j,k);
			float vol_right = c_vol(i,j,k);
			float vol_left = c_vol(i-1,j,k);

			float visc_top = 0.25f*(viscosity(i-1,j+1,k) + viscosity(i-1,j,k) + viscosity(i,j+1,k) + viscosity(i,j,k));
			float visc_bottom = 0.25f*(viscosity(i-1,j,k) + viscosity(i-1,j-1,k) + viscosity(i,j,k) + viscosity(i,j-1,k));
			float vol_top = ez_vol(i,j+1,k);
			float vol_bottom = ez_vol(i,j,k);

			float visc_front = 0.25f*(viscosity(i-1,j,k+1) + viscosity(i-1,j,k) + viscosity(i,j,k+1) + viscosity(i,j,k));
			float visc_back = 0.25f*(viscosity(i-1,j,k) + viscosity(i-1,j,k-1) + viscosity(i,j,k) + viscosity(i,j,k-1));
			float vol_front = ey_vol(i,j,k+1);
			float vol_back = ey_vol(i,j,k);

			//u_x_right
			vmatrix.add_to_element(index,index, 2*factor*visc_right*vol_right);
			if(u_state(i+1,j,k) == FLUID)
				vmatrix.add_to_element(index,u_ind(i+1,j,k), -2*factor*visc_right*vol_right);
			else if(u_state(i+1,j,k) == SOLID)
				vrhs[index] -= -2*factor*visc_right*vol_right*u(i+1,j,k);

			//u_x_left
			vmatrix.add_to_element(index,index, 2*factor*visc_left*vol_left);
			if(u_state(i-1,j,k) == FLUID)
				vmatrix.add_to_element(index,u_ind(i-1,j,k), -2*factor*visc_left*vol_left);
			else if(u_state(i-1,j,k) == SOLID)
				vrhs[index] -= -2*factor*visc_left*vol_left*u(i-1,j,k);

			//u_y_top
			vmatrix.add_to_element(index,index, +factor*visc_top*vol_top);
			if(u_state(i,j+1,k) == FLUID)
				vmatrix.add_to_element(index,u_ind(i,j+1,k), -factor*visc_top*vol_top);
			else if(u_state(i,j+1,k) == SOLID)
				vrhs[index] -= -u(i,j+1,k)*factor*visc_top*vol_top;

			//u_y_bottom
			vmatrix.add_to_element(index,index, +factor*visc_bottom*vol_bottom);
			if(u_state(i,j-1,k) == FLUID)
				vmatrix.add_to_element(index,u_ind(i,j-1,k), -factor*visc_bottom*vol_bottom);
			else if(u_state(i,j-1,k) == SOLID)
				vrhs[index] -= -u(i,j-1,k)*factor*visc_bottom*vol_bottom;

			//u_z_front
			vmatrix.add_to_element(index,index, +factor*visc_front*vol_front);
			if(u_state(i,j,k+1) == FLUID)
				vmatrix.add_to_element(index,u_ind(i,j,k+1), -factor*visc_front*vol_front);
			else if(u_state(i,j,k+1) == SOLID)
				vrhs[index] -= -u(i,j,k+1)*factor*visc_front*vol_front;

			//u_z_back
				vmatrix.add_to_element(index,index, +factor*visc_back*vol_back);
			if(u_state(i,j,k-1) == FLUID)
				vmatrix.add_to_element(index,u_ind(i,j,k-1), -factor*visc_back*vol_back);
			else if(u_state(i,j,k-1) == SOLID)
				vrhs[index] -= -u(i,j,k-1)*factor*visc_back*vol_back;

			//v_x_top
			if(v_state(i,j+1,k) == FLUID)
				vmatrix.add_to_element(index,v_ind(i,j+1,k), -factor*visc_top*vol_top);
			else if(v_state(i,j+1,k) == SOLID)
				vrhs[index] -= -v(i,j+1,k)*factor*visc_top*vol_top;

			if(v_state(i-1,j+1,k) == FLUID)
				vmatrix.add_to_element(index,v_ind(i-1,j+1,k), factor*visc_top*vol_top);
			else if(v_state(i-1,j+1,k) == SOLID)
				vrhs[index] -= v(i-1,j+1,k)*factor*visc_top*vol_top;

			//v_x_bottom
			if(v_state(i,j,k) == FLUID)
				vmatrix.add_to_element(index,v_ind(i,j,k), +factor*visc_bottom*vol_bottom);
			else if(v_state(i,j,k) == SOLID)
				vrhs[index] -= v(i,j,k)*factor*visc_bottom*vol_bottom;

			if(v_state(i-1,j,k) == FLUID)
				vmatrix.add_to_element(index,v_ind(i-1,j,k), -factor*visc_bottom*vol_bottom);
			else if(v_state(i-1,j,k) == SOLID)
				vrhs[index] -= -v(i-1,j,k)*factor*visc_bottom*vol_bottom;

			//w_x_front
			if(w_state(i,j,k+1) == FLUID)
				vmatrix.add_to_element(index,w_ind(i,j,k+1), -factor*visc_front*vol_front);
			else if(w_state(i,j,k+1) == SOLID)
				vrhs[index] -= -w(i,j,k+1)*factor*visc_front*vol_front;

			if(w_state(i-1,j,k+1) == FLUID)
				vmatrix.add_to_element(index,w_ind(i-1,j,k+1), factor*visc_front*vol_front);
			else if(w_state(i-1,j,k+1) == SOLID)
				vrhs[index] -= w(i-1,j,k+1)*factor*visc_front*vol_front;

			//w_x_back
			if(w_state(i,j,k) == FLUID)
				vmatrix.add_to_element(index,w_ind(i,j,k), +factor*visc_back*vol_back);
			else if(w_state(i,j,k) == SOLID)
				vrhs[index] -= w(i,j,k)*factor*visc_back*vol_back;

			if(w_state(i-1,j,k) == FLUID)
				vmatrix.add_to_element(index,w_ind(i-1,j,k), -factor*visc_back*vol_back);
			else if(w_state(i-1,j,k) == SOLID)
				vrhs[index] -= -w(i-1,j,k)*factor*visc_back*vol_back;
		}
	}
	//v-terms
	//vxx + 2vyy + vzz + u_yx + w_yz
	for(unsigned int k = 1; k < nk-1; ++k) for(unsigned int j = 1; j < nj-1; ++j) for(unsigned int i = 1; i < ni-1; ++i) 
	{
		if(v_state(i,j,k) == FLUID) 
		{
			int index = v_ind(i,j,k);
			vrhs[index] = v_vol(i,j,k)*v(i,j,k);

			vmatrix.set_element(index, index, v_vol(i,j,k));

			float visc_right = 0.25f*(viscosity(i,j-1,k) + viscosity(i+1,j-1,k) + viscosity(i,j,k) + viscosity(i+1,j,k));
			float visc_left = 0.25f*(viscosity(i,j-1,k) + viscosity(i-1,j-1,k) + viscosity(i,j,k) + viscosity(i-1,j,k));
			float vol_right = ez_vol(i+1,j,k);
			float vol_left = ez_vol(i,j,k);

			float visc_top = viscosity(i,j,k);
			float visc_bottom = viscosity(i,j-1,k);
			float vol_top = c_vol(i,j,k);
			float vol_bottom = c_vol(i,j-1,k);

			float visc_front = 0.25f*(viscosity(i,j-1,k) + viscosity(i,j-1,k+1) + viscosity(i,j,k) + viscosity(i,j,k+1));
			float visc_back = 0.25f*(viscosity(i,j-1,k) + viscosity(i,j-1,k-1) + viscosity(i,j,k) + viscosity(i,j,k-1));
			float vol_front = ex_vol(i,j,k+1);
			float vol_back = ex_vol(i,j,k);

			//v_x_right
			vmatrix.add_to_element(index,index, +factor*visc_right*vol_right);
			if(v_state(i+1,j,k) == FLUID)
				vmatrix.add_to_element(index,v_ind(i+1,j,k), -factor*visc_right*vol_right);
			else if(v_state(i+1,j,k) == SOLID)
				vrhs[index] -= -v(i+1,j,k)*factor*visc_right*vol_right;

			//v_x_left
				vmatrix.add_to_element(index,index, +factor*visc_left*vol_left);
			if(v_state(i-1,j,k) == FLUID)
				vmatrix.add_to_element(index,v_ind(i-1,j,k), -factor*visc_left*vol_left);
			else if(v_state(i-1,j,k) == SOLID)
				vrhs[index] -= -v(i-1,j,k)*factor*visc_left*vol_left;

			//vy_top
			vmatrix.add_to_element(index,index, +2*factor*visc_top*vol_top);
			if(v_state(i,j+1,k) == FLUID)
				vmatrix.add_to_element(index,v_ind(i,j+1,k), -2*factor*visc_top*vol_top);
			else if (v_state(i,j+1,k) == SOLID)
				vrhs[index] -= -2*factor*visc_top*vol_top*v(i,j+1,k);

			//vy_bottom
			vmatrix.add_to_element(index,index, +2*factor*visc_bottom*vol_bottom);
			if(v_state(i,j-1,k) == FLUID)
				vmatrix.add_to_element(index,v_ind(i,j-1,k), -2*factor*visc_bottom*vol_bottom);
			else if(v_state(i,j-1,k) == SOLID)
				vrhs[index] -= -2*factor*visc_bottom*vol_bottom*v(i,j-1,k);

			//v_z_front
			vmatrix.add_to_element(index,index, +factor*visc_front*vol_front);
			if(v_state(i,j,k+1) == FLUID)
				vmatrix.add_to_element(index,v_ind(i,j,k+1), -factor*visc_front*vol_front);
			else if(v_state(i+1,j,k) == SOLID)
				vrhs[index] -= -v(i,j,k+1)*factor*visc_front*vol_front;

			//v_z_back
			vmatrix.add_to_element(index,index, +factor*visc_back*vol_back);
			if(v_state(i,j,k-1) == FLUID)
				vmatrix.add_to_element(index,v_ind(i,j,k-1), -factor*visc_back*vol_back);
			else if(v_state(i,j,k-1) == SOLID)
				vrhs[index] -= -v(i,j,k-1)*factor*visc_back*vol_back;

			//u_y_right
			if(u_state(i+1,j,k) == FLUID)
				vmatrix.add_to_element(index,u_ind(i+1,j,k), -factor*visc_right*vol_right);
			else if(u_state(i+1,j,k) == SOLID)
				vrhs[index] -= -u(i+1,j,k)*factor*visc_right*vol_right;

			if(u_state(i+1,j-1,k) == FLUID)
				vmatrix.add_to_element(index,u_ind(i+1,j-1,k), factor*visc_right*vol_right);
			else if(u_state(i+1,j-1,k) == SOLID)
				vrhs[index] -= u(i+1,j-1,k)*factor*visc_right*vol_right;

			//u_y_left
			if(u_state(i,j,k) == FLUID)
				vmatrix.add_to_element(index,u_ind(i,j,k), factor*visc_left*vol_left);
			else if(u_state(i,j,k) == SOLID)
				vrhs[index] -= u(i,j,k)*factor*visc_left*vol_left;

			if(u_state(i,j-1,k) == FLUID)
				vmatrix.add_to_element(index,u_ind(i,j-1,k), -factor*visc_left*vol_left);
			else if(u_state(i,j-1,k) == SOLID)
				vrhs[index] -= -u(i,j-1,k)*factor*visc_left*vol_left;

			//w_y_front
			if(w_state(i,j,k+1) == FLUID)
				vmatrix.add_to_element(index,w_ind(i,j,k+1), -factor*visc_front*vol_front);
			else if(w_state(i,j,k+1) == SOLID)
				vrhs[index] -= -w(i,j,k+1)*factor*visc_front*vol_front;

			if(w_state(i,j-1,k+1) == FLUID)
				vmatrix.add_to_element(index,w_ind(i,j-1,k+1), factor*visc_front*vol_front);
			else if(w_state(i,j-1,k+1) == SOLID)
				vrhs[index] -= w(i,j-1,k+1)*factor*visc_front*vol_front;

			//w_y_back
			if(w_state(i,j,k) == FLUID)
				vmatrix.add_to_element(index,w_ind(i,j,k), factor*visc_back*vol_back);
			else if(w_state(i,j,k) == SOLID)
				vrhs[index] -= w(i,j,k)*factor*visc_back*vol_back;

			if(w_state(i,j-1,k) == FLUID)
				vmatrix.add_to_element(index,w_ind(i,j-1,k), -factor*visc_back*vol_back);
			else if(w_state(i,j-1,k) == SOLID)
				vrhs[index] -= -w(i,j-1,k)*factor*visc_back*vol_back;
		}
	}
	//w-terms
	//wxx+ wyy+ 2wzz + u_zx + v_zy
	for(unsigned int k = 1; k < nk-1; ++k) for(unsigned int j = 1; j < nj-1; ++j) for(unsigned int i = 1; i < ni-1; ++i) 
	{
		if(w_state(i,j,k) == FLUID) 
		{
			int index = w_ind(i,j,k);
			vrhs[index] = w_vol(i,j,k)*w(i,j,k);

			vmatrix.set_element(index,index, w_vol(i,j,k));

			float visc_right = 0.25f*(viscosity(i,j,k) + viscosity(i,j,k-1) + viscosity(i+1,j,k) + viscosity(i+1,j,k-1));
			float visc_left = 0.25f*(viscosity(i,j,k) + viscosity(i,j,k-1) + viscosity(i-1,j,k) + viscosity(i-1,j,k-1));
			float vol_right = ey_vol(i+1,j,k);
			float vol_left = ey_vol(i,j,k);

			float visc_top = 0.25f*(viscosity(i,j,k) + viscosity(i,j,k-1) + viscosity(i,j+1,k) + viscosity(i,j+1,k-1));
			float visc_bottom = 0.25f*(viscosity(i,j,k) + viscosity(i,j,k-1) + viscosity(i,j-1,k) + viscosity(i,j-1,k-1));
			float vol_top = ex_vol(i,j+1,k);
			float vol_bottom = ex_vol(i,j,k);

			float visc_front = viscosity(i,j,k);
			float visc_back = viscosity(i,j,k-1);
			float vol_front = c_vol(i,j,k);
			float vol_back = c_vol(i,j,k-1);

			//w_x_right
			vmatrix.add_to_element(index,index, +factor*visc_right*vol_right);
			if(w_state(i+1,j,k) == FLUID)
				vmatrix.add_to_element(index,w_ind(i+1,j,k), -factor*visc_right*vol_right);
			else if (w_state(i+1,j,k) == SOLID)
				vrhs[index] -= -factor*visc_right*vol_right*w(i+1,j,k);

			//w_x_left
			vmatrix.add_to_element(index,index, factor*visc_left*vol_left);
			if(w_state(i-1,j,k) == FLUID)
				vmatrix.add_to_element(index,w_ind(i-1,j,k), -factor*visc_left*vol_left);
			else if (w_state(i-1,j,k) == SOLID)
				vrhs[index] -= -factor*visc_left*vol_left*w(i-1,j,k);

			//w_y_top
			vmatrix.add_to_element(index,index, +factor*visc_top*vol_top);
			if(w_state(i,j+1,k) == FLUID)
				vmatrix.add_to_element(index,w_ind(i,j+1,k), -factor*visc_top*vol_top);
			else if (w_state(i,j+1,k) == SOLID)
				vrhs[index] -= -factor*visc_top*vol_top*w(i,j+1,k);

			//w_y_bottom
			vmatrix.add_to_element(index,index, factor*visc_bottom*vol_bottom);
			if(w_state(i,j-1,k) == FLUID)
				vmatrix.add_to_element(index,w_ind(i,j-1,k), -factor*visc_bottom*vol_bottom);
			else if (w_state(i,j-1,k) == SOLID)
				vrhs[index] -= -factor*visc_bottom*vol_bottom*w(i,j-1,k);

			//w_z_front
			vmatrix.add_to_element(index,index, +2*factor*visc_front*vol_front);
			if(w_state(i,j,k+1) == FLUID)
				vmatrix.add_to_element(index,w_ind(i,j,k+1), -2*factor*visc_front*vol_front);
			else if (w_state(i,j,k+1) == SOLID)
				vrhs[index] -= -2*factor*visc_front*vol_front*w(i,j,k+1);

			//w_z_back
			vmatrix.add_to_element(index,index, +2*factor*visc_back*vol_back);
			if(w_state(i,j,k-1) == FLUID)
				vmatrix.add_to_element(index,w_ind(i,j,k-1), -2*factor*visc_back*vol_back);
			else if (w_state(i,j,k-1) == SOLID)
				vrhs[index] -= -2*factor*visc_back*vol_back*w(i,j,k-1);

			//u_z_right
			if(u_state(i+1,j,k) == FLUID)
				vmatrix.add_to_element(index,u_ind(i+1,j,k), -factor*visc_right*vol_right);
			else if(u_state(i+1,j,k) == SOLID)
				vrhs[index] -= -u(i+1,j,k)*factor*visc_right*vol_right;

			if(u_state(i+1,j,k-1) == FLUID)
				vmatrix.add_to_element(index,u_ind(i+1,j,k-1), factor*visc_right*vol_right);
			else if(u_state(i+1,j,k-1) == SOLID)
				vrhs[index] -= u(i+1,j,k-1)*factor*visc_right*vol_right;

			//u_z_left
			if(u_state(i,j,k) == FLUID)
				vmatrix.add_to_element(index,u_ind(i,j,k), factor*visc_left*vol_left);
			else if(u_state(i,j,k) == SOLID)
				vrhs[index] -= u(i,j,k)*factor*visc_left*vol_left;

			if(u_state(i,j,k-1) == FLUID)
				vmatrix.add_to_element(index,u_ind(i,j,k-1), -factor*visc_left*vol_left);
			else if(u_state(i,j,k-1) == SOLID)
				vrhs[index] -= -u(i,j,k-1)*factor*visc_left*vol_left;

			//v_z_top
			if(v_state(i,j+1,k) == FLUID)
				vmatrix.add_to_element(index,v_ind(i,j+1,k), -factor*visc_top*vol_top);
			else if(v_state(i,j+1,k) == SOLID)
				vrhs[index] -= -v(i,j+1,k)*factor*visc_top*vol_top;

			if(v_state(i,j+1,k-1) == FLUID)
				vmatrix.add_to_element(index,v_ind(i,j+1,k-1), factor*visc_top*vol_top);
			else if(v_state(i,j+1,k-1) == SOLID)
				vrhs[index] -= v(i,j+1,k-1)*factor*visc_top*vol_top;

			//v_z_bottom
			if(v_state(i,j,k) == FLUID)
				vmatrix.add_to_element(index,v_ind(i,j,k), +factor*visc_bottom*vol_bottom);
			else if(v_state(i,j,k) == SOLID)
				vrhs[index] -= v(i,j,k)*factor*visc_bottom*vol_bottom;

			if(v_state(i,j,k-1) == FLUID)
				vmatrix.add_to_element(index,v_ind(i,j,k-1), -factor*visc_bottom*vol_bottom);
			else if(v_state(i,j,k-1) == SOLID)
				vrhs[index] -= -v(i,j,k-1)*factor*visc_bottom*vol_bottom;
		}
	}
	////strip out near zero entries to speed this thing up!
	//printf("Stripping out near-zeros.\n");
	////SparseMatrixd matrix2(matrix.m,matrix.n,15);
	//for(unsigned int row = 0; row < matrix.m; ++row){
	// for(unsigned int col = 0; col < matrix.index[row].size(); ++col) {
	// int index = matrix.index[row][col];
	// double val = matrix(row,index);
	// if(std::abs(val) > 1e-10)
	// matrix2.set_element(row,index, val);
	// }
	//}

	PCGSolver<double> solver;
	double res_out;
	int iter_out;
	solver.set_solver_parameters(1e-18, 1000);
	bool succes = solver.solve(vmatrix, vrhs, vsoln, res_out, iter_out);
	if(!succes || iter_out >= 500)
		printf("\n\n\n**********FAILED**************\n\n\n");
	
	for(unsigned int k = 0; k < nk; ++k) for(unsigned int j = 0; j < nj; ++j) for(unsigned int i = 0; i < ni+1; ++i) 
	{
		if(u_state(i,j,k) == FLUID) 
		{
			u(i,j,k) = (float)vsoln[u_ind(i,j,k)];
		}
		else 
		{
			u(i,j,k) = 0;
		}
	}

	for(unsigned int k = 0; k < nk; ++k) for(unsigned int j = 0; j < nj+1; ++j) for(unsigned int i = 0; i < ni; ++i) 
	{
		if(v_state(i,j,k) == FLUID) 
		{
			v(i,j,k) = (float)vsoln[v_ind(i,j,k)];
		}
		else
			v(i,j,k) = 0;
	}

	for(unsigned int k = 0; k < nk+1; ++k) for(unsigned int j = 0; j < nj; ++j) for(unsigned int i = 0; i < ni; ++i) 
	{
		if(w_state(i,j,k)== FLUID) 
		{
			w(i,j,k) = (float)vsoln[w_ind(i,j,k)];
		}
		else
			w(i,j,k) = 0;
	}
}

void MACGrid::compute_curl()
{
	float scale = 1.0/this->dx;

	for(unsigned int k=1; k<this->nk-1; ++k) for(unsigned int j=1; j<this->nj-1; ++j) for(unsigned int i=1; i<this->ni-1; ++i)
	{
		curl_x(i,j,k) = scale * ( this->w(i,j,k) - this->w(i,j-1,k) - this->v(i,j,k) + this->v(i,j,k-1));
		curl_y(i,j,k) = scale * ( this->u(i,j,k) - this->u(i,j,k-1) - this->w(i,j,k) + this->w(i-1,j,k));
		curl_z(i,j,k) = scale * ( this->v(i,j,k) - this->v(i-1,j,k) - this->u(i,j,k) + this->u(i,j-1,k));				
	}
	//for now we ignore the boundaries, no flow should be there in this application any way
}

/**
* Constrains the velocity field
*/
void MACGrid::constrain_velocity()
{
	//constrain the velocity near the solids:
	//everywhere where the velocity is made 0 the solids velocity should be used when moving solids exist
   temp_u = u;
   temp_v = v;
   temp_w = w;
	
   for(unsigned int k=0; k<u.nk; ++k) for(unsigned int j=0; j<u.nj; ++j) for(unsigned int i=0; i<u.ni; ++i)
   {
	   if(u_weights(i,j,k) == 0)
	   {
		   temp_u(i,j,k) = 0;
	   }
   }

   for(unsigned int k=0; k<v.nk; ++k) for(unsigned int j=0; j<v.nj; ++j) for(unsigned int i=0; i<v.ni; ++i)
   {
	   if(v_weights(i,j,k) == 0)
	   {
		   temp_v(i,j,k) = 0;
	   }
   }

   for(unsigned int k=0; k<w.nk; ++k) for(unsigned int j=0; j<w.nj; ++j) for(unsigned int i=0; i<w.ni; ++i)
   {
	   if(w_weights(i,j,k) == 0)
	   {
		   temp_w(i,j,k) = 0;
	   }
   }

   u = temp_u;
   v = temp_v;
   w = temp_w;

   return;

   //advanced normal based constraining (not used now):
   //constrain u
   for(unsigned int k=0; k<u_weights.nk; ++k)
   {
	   for(unsigned int j = 0; j < u_weights.nj; ++j) 
	   {
		   for(unsigned int i = 0; i < u_weights.ni; ++i)
		   {
			   if(u_weights(i,j,k) == 0) 
			   {
				   if(i>0 && nodal_solid_phi(i-1,j,k)<0 && nodal_solid_phi(i,j,k)<0) continue;
				   //apply constraint
				   Vec3f pos;
				   pos[0]=(float)i;
				   pos[1]=j-0.5f;
				   pos[2]=k-0.5f;
				   Vec3f vel;
				  
				   trilerp_uvw((float)pos[0],(float)pos[1],(float)pos[2],vel[0],vel[1],vel[2]);
				   
				   Vec3f normal;
				   normal = calculateSurfaceNormal(nodal_solid_phi, pos);
				   float perp_component = vel[0]*normal[0]+vel[1]*normal[1]+vel[2]*normal[2];
				   if(perp_component>0) continue; //the velocity is already in the right direction
				   vel[0] -= perp_component*normal[0];
				   vel[1] -= perp_component*normal[1];
				   vel[2] -= perp_component*normal[2];
				   temp_u(i,j,k) = vel[0];
			   }
		   }
	   }
   }
   //constrain v
   for(unsigned int k=0; k<v_weights.nk; ++k)
   {
	   for(unsigned int j = 0; j < v_weights.nj; ++j) 
	   {
		   for(unsigned int i = 0; i < v_weights.ni; ++i)
		   {
			   if(v_weights(i,j,k) == 0) 
			   {
				   if(j>0 && nodal_solid_phi(i,j-1,k)<0 && nodal_solid_phi(i,j,k)<0) continue;
				   //apply constraint
				   Vec3f pos;
				   pos[0]=i-0.5f;
				   pos[1]=(float)j;
				   pos[2]=k-0.5f;
				   Vec3f vel;

				   trilerp_uvw((float)pos[0],(float)pos[1],(float)pos[2],vel[0],vel[1],vel[2]);	

				   Vec3f normal;
				   normal = calculateSurfaceNormal(nodal_solid_phi, pos);
				   float perp_component = vel[0]*normal[0]+vel[1]*normal[1]+vel[2]*normal[2];
				   if(perp_component>0) continue; //the velocity is already in the right direction
				   vel[0] -= perp_component*normal[0];
				   vel[1] -= perp_component*normal[1];
				   vel[2] -= perp_component*normal[2];
				   temp_v(i,j,k) = vel[1];
			   }
		   }
	   }
   }
   //constrain w
   for(unsigned int k=1; k<w_weights.nk; ++k)
   {
	   for(unsigned int j = 0; j < w_weights.nj; ++j) 
	   {
		   for(unsigned int i = 0; i < w_weights.ni; ++i)
		   {
			   if(w_weights(i,j,k) == 0) 
			   {
				   if(k>0 && nodal_solid_phi(i,j,k-1)<0 && nodal_solid_phi(i,j,k)<0) continue;
				   //apply constraint
				   Vec3f pos;
				   pos[0]=i-0.5f;
				   pos[1]=j-0.5f;
				   pos[2]=(float)k;
				   Vec3f vel;

				   trilerp_uvw((float)pos[0],(float)pos[1],(float)pos[2],vel[0],vel[1],vel[2]);

				   Vec3f normal;
				   normal = calculateSurfaceNormal(nodal_solid_phi, pos);
				   float perp_component = vel[0]*normal[0]+vel[1]*normal[1]+vel[2]*normal[2];
				   if(perp_component>0) continue; //the velocity is already in the right direction
				   vel[0] -= perp_component*normal[0];
				   vel[1] -= perp_component*normal[1];
				   vel[2] -= perp_component*normal[2];
				   temp_w(i,j,k) = vel[2];
			   }
		   }
	   }
   }

   u = temp_u;
   v = temp_v;
   w = temp_w;
}

/*HELPER FUNCTIONS*/
//Apply several iterations of a very simple "Jacobi"-style propagation of valid velocity data in all directions
void MACGrid::extrapolate(Array3f& grid, Array3c& valid)
{
	Array3c old_valid(valid.ni,valid.nj,valid.nk);
	for(unsigned int layers = 0; layers < 10; ++layers)
	{
		old_valid = valid;
		for(unsigned int k = 1; k < grid.nk-1; ++k) for(unsigned int j = 1; j < grid.nj-1; ++j) for(unsigned int i = 1; i < grid.ni-1; ++i)
		{
			float sum = 0;
			int count = 0;
			if(!old_valid(i,j,k))
			{
				if(old_valid(i+1,j,k))
				{
					sum += grid(i+1,j,k);
					++count;
				}
				if(old_valid(i-1,j,k))
				{
					sum += grid(i-1,j,k);
					++count;
				}
				if(old_valid(i,j+1,k))
				{
					sum += grid(i,j+1,k);
					++count;
				}
				if(old_valid(i,j-1,k))
				{
					sum += grid(i,j-1,k);
					++count;
				}
				if(old_valid(i,j,k+1))
				{
					sum += grid(i,j,k+1);
					++count;
				}
				if(old_valid(i,j,k+1))
				{
					sum += grid(i,j,k-1);
					++count;
				}

				//if any neighbour cell was valid,
				//we assign the cell their average value and make it valid
				if(count > 0)
				{
					grid(i,j,k) = sum/(float) count;
					valid(i,j,k) = 1;
				}
			}
		}
	}
}

//Given two signed distance values, determine what fraction of a connecting segment is "inside"
float MACGrid::fraction_inside(float phi_left, float phi_right)
{
	if(phi_left < 0 && phi_right < 0)
		return 1;
	if (phi_left < 0 && phi_right >= 0)
		return phi_left / (phi_left - phi_right);
	if(phi_left >= 0 && phi_right < 0)
		return phi_right / (phi_right - phi_left);
	else
		return 0;
}


//fraction_inside(float phi_left_top, float phi_left_under, float phi_right_top, float phi_right_under)
float MACGrid::fraction_inside(float lt, float lu, float rt, float ru) 
{
	//basically marching squares
	//needed to calculate the face fraction:
	float fx,fy;

	//input:
	//
	//  lt---rt
	//	|     |
	//  |     |
	//  lu---ru

	// 16 cases:
	/**
	where o is solid and x is not solid

	o---o 1
	|   |
	o---o

	x---o 2  o---x 3  o---o 4  o---o 5
	|   |    |   |    |   |    |   |
	o---o    o---o    x---o    o---x

	x---x 6  x---o 7  o---x 8  o---o 9  x---o 10 o---x 11
	|   |    |   |    |   |    |   |    |   |    |   |
	o---o    x---o    o---x    x---x    0---x    x---o

	o---x 12 x---o 13 x---x 14 x---x 15
	|   |    |   |    |   |    |   |
	x---x    x---x    o---x    x---o

	x---x 16
	|   |
	x---x
	**/

	//case 1:
	// 	o---o
	//  |   |
	//  o---o
	if (lt<0 && lu<0 && rt<0 && ru<0)
	{
		return 1.0f; //the whole cell is solid
	}

	//case 2:
	//  x---o
	//  |   |
	//  o---o
	if(lt>=0 && lu<0 && rt<0 && ru<0)
	{
		//we have a triangle that is inside so we have to calculate the area of this triangle (0.5*b*h)
		fx = fraction_inside(lt,rt);
		fy = fraction_inside(lt,lu);
		return 1-0.5f*fx*fy;
	}

	//case 3:
	//  o---x
	//  |   |
	//  o---o
	if(lt<0 && lu<0 && rt>=0 && ru<0)
	{
		//we have a triangle that is inside so we have to calculate the area of this triangle (0.5*b*h)
		fx = fraction_inside(rt,lt);
		fy = fraction_inside(rt,ru);
		return 1-0.5f*fx*fy;
	}
	
	//case 4:
	//  o---o
	//  |   |
	//  x---o
	if(lt<0 && lu>=0 && rt<0 && ru<0)
	{
		//we have a triangle that is inside so we have to calculate the area of this triangle (0.5*b*h)
		fx = fraction_inside(lu,lt);
		fy = fraction_inside(lu,ru);
		return 1-0.5f*fx*fy;
	}

	//case 5:
	//  o---o
	//  |   |
	//  o---x
	if(lt<0 && lu<0 && rt<0 && ru>=0)
	{
		//we have a triangle that is inside so we have to calculate the area of this triangle (0.5*b*h)
		fx = fraction_inside(ru,rt);
		fy = fraction_inside(ru,lu);
		return 1-0.5f*fx*fy;
	}

	//case 6:
	//  x---x
	//  |   |
	//  o---o
	if(lt>=0 && lu<0 && rt>=0 && ru<0)
	{
		//we have 4 cornered shape of whiche we have to subtract its area from 1
		//basically we have a rectangle plus a triangle
		fx = fraction_inside(lt,lu);
		fy = fraction_inside(rt,ru);
		float area = min(fx,fy)*1.0f; //area rectangle
		//area traingle
		area += 0.5f*(max(fx,fy)-min(fx,fy))*1.0f;
		return 1-area;
	}

	//case 7:
	//  x---o
	//  |   |
	//  x---o
	if(lt>=0 && lu>=0 && rt<0 && ru<0)
	{
		//we have 4 cornered shape of whiche we have to subtract its area from 1
		//basically we have a rectangle plus a triangle
		fx = fraction_inside(lt,rt);
		fy = fraction_inside(lu,ru);
		float area = min(fx,fy)*1.0f; //area rectangle
		//area traingle
		area += 0.5f*(max(fx,fy)-min(fx,fy))*1.0f;
		return 1-area;
	}

	//case 8:
	//  o---x
	//  |   |
	//  o---x
	if(lt<0 && lu<0 && rt>=0 && ru>=0)
	{
		//we have 4 cornered shape of whiche we have to subtract its area from 1
		//basically we have a rectangle plus a triangle
		fx = fraction_inside(rt,lt);
		fy = fraction_inside(ru,lu);
		float area = min(fx,fy)*1.0f; //area rectangle
		//area traingle
		area += 0.5f*(max(fx,fy)-min(fx,fy))*1.0f;
		return 1-area;
	}

	//case 9:
	//  o---o
	//  |   |
	//  x---x
	if(lt<0 && lu>=0 && rt<0 && ru>=0)
	{
		//we have 4 cornered shape of whiche we have to subtract its area from 1
		//basically we have a rectangle plus a triangle
		fx = fraction_inside(lu,lt);
		fy = fraction_inside(ru,rt);
		float area = min(fx,fy)*1.0f; //area rectangle
		//area traingle
		area += 0.5f*(max(fx,fy)-min(fx,fy))*1.0f;
		return 1-area;
	}

	//case 10:
	//  x---o
	//  |   |
	//  o---x
	if(lt>=0 && lu<0 && rt<0 && ru>=0)
	{
		//here we subtract 2 corners (triangles), however we could also use another shape
		float area = 0;
		fx = fraction_inside(lt,rt);
		fy = fraction_inside(lt,lu);
		float fx2 = fraction_inside(ru,rt);
		float fy2 = fraction_inside(ru,lu);
		area = 0.5f*fx*fy;
		area += 0.5f*fx2*fy2;
		return 1-area;
	}

	//case 11:
	//  o---x
	//  |   |
	//  x---o
	if(lt<0 && lu>=0 && rt>=0 && ru<0)
	{
		//here we subtract 2 corners (triangles), however we could also use another shape
		float area = 0.0;
		fx = fraction_inside(rt,lt);
		fy = fraction_inside(rt,ru);
		float fx2 = fraction_inside(lu,lt);
		float fy2 = fraction_inside(lu,ru);
		area = 0.5f*fx*fy;
		area += 0.5f*fx2*fy2;
		return 1-area;
	}

	//case 12:
	//  o---x
	//  |   |
	//  x---x
	if(lt<0 && lu>=0 && rt>=0 && ru>=0)
	{
		//we return the area of the triangle that is in the solid
		fx = fraction_inside(lt,rt);
		fy = fraction_inside(lt,lu);
		return 0.5f*fx*fy;
	}

	//case 13:
	//  x---o
	//  |   |
	//  x---x
	if(lt>=0 && lu>=0 && rt<0 && ru>=0)
	{
		//we return the area of the triangle that is in the solid
		fx = fraction_inside(rt,lt);
		fy = fraction_inside(rt,ru);
		return 0.5f*fx*fy;
	}

	//case 14:
	//  x---x
	//  |   |
	//  o---x
	if(lt>=0 && lu<0 && rt>=0 && ru>=0)
	{
		//we return the area of the triangle that is in the solid
		fx = fraction_inside(lu,lt);
		fy = fraction_inside(lu,ru);
		return 0.5f*fx*fy;
	}

	//case 15:
	//  x---x
	//  |   |
	//  x---o
	if(lt>=0 && lu>=0 && rt>=0 && ru<0)
	{
		//we return the area of the triangle that is in the solid
		fx = fraction_inside(ru,rt);
		fy = fraction_inside(ru,lu);
		return 0.5f*fx*fy;
	}

	
	//case 16:
	//  x---x
	//  |   |
	//  x---x
	if(lt>=0 && lu>=0 && rt>=0 && ru>=0)
	{
		//no corner is inside a solid so we return 0
		return 0.0;
	}

	return 0;
}

void MACGrid::compute_volume_fractions(const Array3f& levelset, Array3f& fractions, Vec3f fraction_origin, unsigned int subdivision) 
{   
   //Assumes levelset and fractions have the same dx
   float sub_dx = 1.0f / subdivision;
   int sample_max = subdivision*subdivision*subdivision;
   for(unsigned int k = 0; k < fractions.nk; ++k)
   {
	   for(unsigned int j = 0; j < fractions.nj; ++j) 
	   {
		  for(unsigned int i = 0; i < fractions.ni; ++i) 
		  {
			 float start_x = fraction_origin[0] + (float)i;
			 float start_y = fraction_origin[1] + (float)j;
			 float start_z = fraction_origin[2] + (float)k;
			 int incount = 0;

			 for(unsigned int sub_k = 0; sub_k < subdivision; ++sub_k)
			 {
				 for(unsigned int sub_j = 0; sub_j < subdivision; ++sub_j) 
				 {
					for(unsigned int sub_i = 0; sub_i < subdivision; ++sub_i) 
					{
					   float x_pos = start_x + (sub_i+0.5f)*sub_dx;
					   float y_pos = start_y + (sub_j+0.5f)*sub_dx;
					   float z_pos = start_z + (sub_k+0.5f)*sub_dx;

					   float phi_val = interpolate_value(Vec3f(x_pos,y_pos,z_pos), levelset);
					   if(phi_val < 0) 
						  ++incount;
					}
				 }
			 }
			 fractions(i,j,k) = (float)incount / (float)sample_max;
		  }
	   }
   }	   
}

float MACGrid::interpolate_phi(const Vec3f& point, const Array3f& grid, const Vec3f& origin, const float dx) 
{
	float inv_dx = 1/dx;
	Vec3f temp = (point-origin)*inv_dx;
	return interpolate_value(temp, grid);
}

void MACGrid::estimate_volume_fractions(Array3f& volumes,
							   const Vec3f& start_centre, const float dx,
							   const Array3f& phi, const Vec3f& phi_origin, const float phi_dx)
{
	for(unsigned int k = 0; k < volumes.nk; ++k) for(unsigned int j = 0; j < volumes.nj; ++j) for(unsigned int i = 0; i < volumes.ni; ++i) 
	{
		Vec3f centre = start_centre + Vec3f(i*dx, j*dx, k*dx);
		float offset = 0.5f*dx;

		float phi000 = interpolate_phi(centre + Vec3f(-offset,-offset,-offset), phi, phi_origin, phi_dx);
		float phi001 = interpolate_phi(centre + Vec3f(-offset,-offset,+offset), phi, phi_origin, phi_dx);
		float phi010 = interpolate_phi(centre + Vec3f(-offset,+offset,-offset), phi, phi_origin, phi_dx);
		float phi011 = interpolate_phi(centre + Vec3f(-offset,+offset,+offset), phi, phi_origin, phi_dx);
		float phi100 = interpolate_phi(centre + Vec3f(+offset,-offset,-offset), phi, phi_origin, phi_dx);
		float phi101 = interpolate_phi(centre + Vec3f(+offset,-offset,+offset), phi, phi_origin, phi_dx);
		float phi110 = interpolate_phi(centre + Vec3f(+offset,+offset,-offset), phi, phi_origin, phi_dx);
		float phi111 = interpolate_phi(centre + Vec3f(+offset,+offset,+offset), phi, phi_origin, phi_dx);

		volumes(i,j,k) = volume_fraction(phi000, phi100, phi010, phi110, phi001, phi101, phi011, phi111);
	}
}

float MACGrid::interpolate_value(const Vec3f& point, const Array3f& grid)
{
   unsigned int i,j,k;
   float fx,fy,fz;

   get_barycentric(point[0], i, fx, 0, grid.ni);
   get_barycentric(point[1], j, fy, 0, grid.nj);
   get_barycentric(point[2], k, fz, 0, grid.nk);

   return grid.gridtrilerp(i,j,k,fx,fy,fz);
}

unsigned int MACGrid::matrixIndex(unsigned int i, unsigned int j, unsigned int k)
{
   return i+ni*(j+nj*k);
}

unsigned int MACGrid::u_ind(unsigned int i, unsigned int j, unsigned int k) 
{
   return i+j*(ni+1)+k*(ni+1)*nj;
}

unsigned int MACGrid::v_ind(unsigned int i, unsigned int j, unsigned int k) 
{
   //(ni+1)*nj*nk + i + j*ni + k*ni*(nj+1)
   //= (largest u_ind + 1) + i + j*ni + k*ni*(nj+1)
   return i+j*ni+k*ni*(nj+1)+(ni+1)*nj*nk;
}

unsigned int MACGrid::w_ind(unsigned int i, unsigned int j, unsigned int k) 
{
   //= (largest v_ind + 1) + i + j*ni + k*ni*nj
   return i+j*ni+k*ni*nj+(ni+1)*nj*nk+ni*(nj+1)*nk;
}