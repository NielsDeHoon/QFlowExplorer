// This file is the 'manager' of the FLIP fluid simulation

// it manages the particles and the grid, only communicate with those via this class
// For debuging the fluid simulation set DEBUG to true in MACGrid.h
// Do not alter the MACGRID or FLIP_Particles classes unless you know what you're doing
// Enjoy the fluid :)

#pragma once
#include "FLIP_Particles.h"
#include "makelevelset3.h"

class FLIP_simulation
{
public:
	FLIP_simulation();
	//define the grid size, which will be mx*my*mz cells and will have width, lenght and height lx, and constant density
	FLIP_simulation(unsigned int mx, unsigned int my, unsigned int mz, float lx, float density):
	//initializer list, for private variables
		grid(mx,my,mz,lx,density),
		particles(grid),
		phi_solid(grid.nodal_solid_phi.ni,grid.nodal_solid_phi.nj,grid.nodal_solid_phi.nk)
	//initialization
	{
		init(mx, my, mz, lx, density);
	}
	~FLIP_simulation(void);

	void advance_one_step(double dt);
	void advance_one_frame(double frametime);

	std::vector<Vec3f> getParticleLocations(void);
	std::vector<Vec3f> getParticleVelocities(void);

	Vec3f getBodyForce(void);

	void setSolids(Array3f f);

	void setPotentialField(Array3f f);

	void setViscosistyPerCell(Array3f f);

	void setViscosity(float f);

	void setParticle(Vec3f position, Vec3f velocity); //used to define the initial particles (or to add more particles)
	void removeParticle(unsigned int index); //used to remove particles
	void fillSourceCell(unsigned int i, unsigned int j, unsigned int k, float timeFraction);

	void setBodyForce(Vec3f Force);

	void setFillHolesAutomatically(bool set);
	void setFillHolesCells(Array3b _array);

	void setSinkCell(unsigned int i,unsigned int j,unsigned int k);
	void removeSinkCell(unsigned int i,unsigned int j,unsigned int k);

	void setSourceCell(unsigned int i,unsigned int j,unsigned int k);
	void removeSourceCell(unsigned int i,unsigned int j,unsigned int k);

	void setSourceVelocityFieldCurrentFrame(Array3Vec3 velocity_field);
	void setSourceVelocityFieldNextFrame(Array3Vec3 velocity_field);
	
	unsigned int getInitialParticlesPerCell();

	Array3f getFluidSurface();
	Array3f getSolidSurface();

	Array3f getPressure();

	void getVelocityField(Array3Vec3 &velocity);
	
	Vec3f getVelocityAtPoint(Vec3f point);

	unsigned int getNumberOfParticles();

	Array3f getKineticEnergyField();

	float getTotalKineticEnergy();
	
	Array3f FLIP_simulation::meshToLevelSet(std::vector<Vec3ui> triangles, std::vector<Vec3f> vertices); 
	//generates a signed distance function from the given triangles, 
	//the corners of the triangles are stored in vert and 
	//can be found by looking up the index of a corner in the corresponding Vec3ui
	//std::vector<Vec3ui> triangles; //contains the triangles, each Vec3ui contains 3 indices of the vertices stored in vertices
	//std::vector<Vec3f> vertices;   //contains the vertices of the triangles

	/**
	* Use a no stick boundary condtion
	*/
	void set_no_stick();

	/**
	* Use a no slip boundary condtion (most accurate but can create a layer of fluid near a boundary)
	*/
	void set_no_slip();

//private:	
	void init(unsigned int mx, unsigned int my, unsigned int mz, float lx, float density);

	//variables
	MACGrid grid;
	FLIP_Particles particles;
	bool firstRun;
	Vec3f bodyForce;
	float FLIP_PIC_ratio;
	Array3f phi_solid;	
	bool fillHolesAutomatically;
	Array3b fillHolesCells;

	double frameTime;
	double frameTimeProcessed;

	unsigned int randomSeed;

	Array3b sink_cells;
	Array3b source_cells;

	Array3Vec3 velocity_field_current_frame;
	Array3Vec3 velocity_field_next_frame;

	Vec3f trilerpVectorField(Array3Vec3 *field, Vec3f location);

	bool velocity_field_current_frame_set;
	bool velocity_field_next_frame_set;

	bool using_negative_time_step;
};
