#ifndef ARRAY3_H
#define ARRAY3_H

#include "array1.h"
//#include "vec.h"
#include <algorithm>
#include <cassert>
#include <vector>
#include <array>
#include <windows.h> //for debugging
#include <type_traits>

inline float lerp(const float& value0, const float& value1, float f)
{ return (1-f)*value0 + f*value1; }

inline float bilerp(const float& v00, const float& v10, 
				const float& v01, const float& v11, 
				float fx, float fy)
{ 
   return lerp(lerp(v00, v10, fx),
			   lerp(v01, v11, fx), 
			   fy);
}

inline float trilerp(const float& v000, const float& v100,
				 const float& v010, const float& v110,
				 const float& v001, const float& v101,  
				 const float& v011, const float& v111,
				 float fx, float fy, float fz) 
{
   return lerp(bilerp(v000, v100, v010, v110, fx, fy),
			   bilerp(v001, v101, v011, v111, fx, fy),
			   fz);
}

template<typename T> struct is_vector : public std::false_type {};

template<typename T, typename A>
struct is_vector<std::vector<T, A>> : public std::true_type {};

#define DEBUGING true

template<class T, class ArrayT=std::vector<T> >
struct Array3
{
	// STL-friendly typedefs

	typedef typename ArrayT::iterator iterator;
	typedef typename ArrayT::const_iterator const_iterator;
	typedef typename ArrayT::size_type size_type;
	typedef long difference_type;
	typedef T& reference;
	typedef const T& const_reference;
	typedef T value_type;
	typedef T* pointer;
	typedef const T* const_pointer;
	typedef typename ArrayT::reverse_iterator reverse_iterator;
	typedef typename ArrayT::const_reverse_iterator const_reverse_iterator;

	// the actual representation

	const inline void check_input(unsigned int i, unsigned int j, unsigned int k) const
	{
		static int error_printed = 0;
		if(!DEBUGING)
		{
			assert(i>=0 && "i index is negative, enable debuging in array3.h");
			assert(i<ni && "i index is too large, enable debuging in array3.h");
			assert(j>=0 && "j index is negative, enable debuging in array3.h");
			assert(j<nj && "j index is too large, enable debuging in array3.h");
			assert(k>=0 && "k index is negative, enable debuging in array3.h");
			assert(k<nk && "k index is too large, enable debuging in array3.h");
		}

		if(DEBUGING && (i<0 || i>=ni || j<0 || j>=nj || k<0 || k>=nk))
		{
			if(error_printed == 0)
			{
				std::cout<<std::endl<<"--------------------------------"<<std::endl;
				std::cout<<"Index out of range:\nGiven is ("<<i<<","<<j<<","<<k<<")\nWhile the array dimensions are ("<<ni<<","<<nj<<","<<nk<<")\nYou can add a breakpoint in array3.h at line "<<__LINE__<<" to find the calling function."<<std::endl;
				error_printed = 1;
				std::cout<<"--------------------------------"<<std::endl<<std::endl;
			}
			if(i<0)		i = 0;
			if(i>=ni)	i = ni-1;
			if(j<0)		j = 0;
			if(j>=nj)	j = nj-1;
			if(k<0)		k = 0;
			if(k>=nk)	k = nk-1;
		}
	}

	//comment out ";check_input(i,j,k)" function for a performance increase;
	#define CHECK_INPUT ;check_input(i,j,k)

	unsigned int ni, nj, nk;
	ArrayT a;

	// the interface

	Array3(void)
		: ni(0), nj(0), nk(0)
	{}

	Array3(unsigned int ni_, unsigned int nj_, unsigned int nk_)
		: ni(ni_), nj(nj_), nk(nk_), a(ni_*nj_*nk_)
	{ assert(ni_>=0 && nj_>=0 && nk_>=0); }

	Array3(unsigned int ni_, unsigned int nj_, unsigned int nk_, ArrayT& a_)
		: ni(ni_), nj(nj_), nk(nk_), a(a_)
	{ assert(ni_>=0 && nj_>=0 && nk_>=0); }

	Array3(unsigned int ni_, unsigned int nj_, unsigned int nk_, const T& value)
		: ni(ni_), nj(nj_), nk(nk_), a(ni_*nj_*nk_, value)
	{ assert(ni_>=0 && nj_>=0 && nk_>=0); }

	Array3(unsigned int ni_, unsigned int nj_, unsigned int nk_, const T& value, size_type max_n_)
		: ni(ni_), nj(nj_), nk(nk_), a(ni_*nj_*nk_, value, max_n_)
	{ assert(ni_>=0 && nj_>=0 && nk_>=0); }

	Array3(unsigned int ni_, unsigned int nj_, unsigned int nk_, T* data_)
		: ni(ni_), nj(nj_), nk(nk_), a(ni_*nj_*nk_, data_)
	{ assert(ni_>=0 && nj_>=0 && nk_>=0); }

	Array3(unsigned int ni_, unsigned int nj_, unsigned int nk_, T* data_, size_type max_n_)
		: ni(ni_), nj(nj_), nk(nk_), a(ni_*nj_*nk_, data_, max_n_)
	{ assert(ni_>=0 && nj_>=0 && nk_>=0); }

	~Array3(void)
	{
#ifndef NDEBUG
		ni=nj=nk=0;
#endif
	}

	const T& operator()(unsigned int i, unsigned int j, unsigned int k) const
	{
		CHECK_INPUT;
		return a[i+ni*(j+nj*k)];
	}

	T& operator()(unsigned int i, unsigned int j, unsigned int k)
	{
		CHECK_INPUT;
		return a[i+ni*(j+nj*k)];
	}

	bool operator==(const Array3<T>& x) const
	{ return ni==x.ni && nj==x.nj && nk==x.nk && a==x.a; }

	bool operator!=(const Array3<T>& x) const
	{ return ni!=x.ni || nj!=x.nj || nk!=x.nk || a!=x.a; }

	bool operator<(const Array3<T>& x) const
	{
		if(ni<x.ni) return true; else if(ni>x.ni) return false;
		if(nj<x.nj) return true; else if(nj>x.nj) return false;
		if(nk<x.nk) return true; else if(nk>x.nk) return false;
		return a<x.a;
	}

	bool operator>(const Array3<T>& x) const
	{
		if(ni>x.ni) return true; else if(ni<x.ni) return false;
		if(nj>x.nj) return true; else if(nj<x.nj) return false;
		if(nk>x.nk) return true; else if(nk<x.nk) return false;
		return a>x.a;
	}

	bool operator<=(const Array3<T>& x) const
	{
		if(ni<x.ni) return true; else if(ni>x.ni) return false;
		if(nj<x.nj) return true; else if(nj>x.nj) return false;
		if(nk<x.nk) return true; else if(nk>x.nk) return false;
		return a<=x.a;
	}

	bool operator>=(const Array3<T>& x) const
	{
		if(ni>x.ni) return true; else if(ni<x.ni) return false;
		if(nj>x.nj) return true; else if(nj<x.nj) return false;
		if(nk>x.nk) return true; else if(nk<x.nk) return false;
		return a>=x.a;
	}

	void assign(const T& value)
	{ a.assign(value); }

	void assign(unsigned int ni_, unsigned int nj_, unsigned int nk_, const T& value)
	{
		a.assign(ni_*nj_*nk_, value);
		ni=ni_;
		nj=nj_;
		nk=nk_;
	}

	void assign(unsigned int ni_, unsigned int nj_, unsigned int nk_, const T* copydata)
	{
		a.assign(ni_*nj_*nk_, copydata);
		ni=ni_;
		nj=nj_;
		nk=nk_;
	}

	const T& at(unsigned int i, unsigned int j, unsigned int k) const
	{
		CHECK_INPUT;
		return a[i+ni*(j+nj*k)];
	}

	T& at(unsigned int i, unsigned int j, unsigned int k)
	{
		CHECK_INPUT;
		return a[i+ni*(j+nj*k)];
	}

	const T& back(void) const
	{ 
		assert(a.size());
		return a.back();
	}

	T& back(void)
	{
		assert(a.size());
		return a.back();
	}

	const_iterator begin(void) const
	{ return a.begin(); }

	iterator begin(void)
	{ return a.begin(); }

	size_type capacity(void) const
	{ return a.capacity(); }

	void clear(void)
	{
		a.clear();
		ni=nj=nk=0;
	}

	bool is_empty(void) const
	{ return a.is_empty(); }

	const_iterator end(void) const
	{ return a.end(); }

	iterator end(void)
	{ return a.end(); }

	void fill(unsigned int ni_, unsigned int nj_, unsigned int nk_, const T& value)
	{
		a.fill(ni_*nj_*nk_, value);
		ni=ni_;
		nj=nj_;
		nk=nk_;
	}

	const T& front(void) const
	{
		assert(a.size());
		return a.front();
	}

	T& front(void)
	{
		assert(a.size());
		return a.front();
	}

	size_type max_size(void) const
	{ return a.max_size(); }

	reverse_iterator rbegin(void)
	{ return reverse_iterator(end()); }

	const_reverse_iterator rbegin(void) const
	{ return const_reverse_iterator(end()); }

	reverse_iterator rend(void)
	{ return reverse_iterator(begin()); }

	const_reverse_iterator rend(void) const
	{ return const_reverse_iterator(begin()); }

	void reserve(unsigned int reserve_ni, unsigned int reserve_nj, unsigned int reserve_nk)
	{ a.reserve(reserve_ni*reserve_nj*reserve_nk); }

	void resize(unsigned int ni_, unsigned int nj_,unsigned  int nk_)
	{
		assert(ni_>=0 && nj_>=0 && nk_>=0);
		a.resize(ni_*nj_*nk_);
		ni=ni_;
		nj=nj_;
		nk=nk_;
	}

	void resize(unsigned int ni_, unsigned int nj_, unsigned int nk_, const T& value)
	{
		assert(ni_>=0 && nj_>=0 && nk_>=0);
		a.resize(ni_*nj_*nk_, value);
		ni=ni_;
		nj=nj_;
		nk=nk_;
	}

	void set_zero(void)
	{ a.set_zero(); }

	size_type size(void) const
	{ return a.size(); }

	void swap(Array3<T>& x)
	{
		std::swap(ni, x.ni);
		std::swap(nj, x.nj);
		std::swap(nk, x.nk);
		a.swap(x.a);
	}

	void trim(void)
	{ a.trim(); }

	bool inRange(unsigned int i, unsigned int j, unsigned int k) const
	{
		return (i>=0 && i<ni && j>=0 && j<nj && k>=0 && k<nk);
	}

	void clamp(unsigned int &i, unsigned int &j, unsigned int &k)
	{
		if(!(i>=0)) i = 1;
		if(!(i<ni)) i = ni-2; 
		if(!(j>=0)) j = 2; 
		if(!(j<nj)) j = nj-2;
		if(!(k>=0)) k = 1;
		if(!(k<nk)) k = nk-2;
	}

	void init(unsigned int ni_, unsigned int nj_, unsigned int nk_)
	{
		assert(ni_>=0 && nj_>=0 && nk_>=0);
		ni=ni_;
		nj=nj_;
		nk=nk_;
		resize(ni,nj,nk);
		set_zero();
	}

	unsigned int number_of_non_zeros()
	{
		unsigned int count = 0;
		for(unsigned int i = 0; i<a.size(); i++)
		{
			if (a[i]!=0)
				count++;
		}
		return count;
	}
	
	T gridtrilerp(unsigned int i, unsigned int j, unsigned int k, float fx, float fy, float fz) const
	{ 
		/**

		 C011_______C111
		   /|	    / |
		  / |	   /  |
	  C001--+---C101  |
		|   |_____|___|C110
		|  /C010  |  /
		| /       | /
		|/________|/
		C000      C100

		**/
		//find intermediate  values of the corners of a square around the value we want the find
		if(!DEBUGING)
			assert(i+1+ni*(j+1+nj*(k+1))<(int)a.size());
		else
		{
			if(i+1+ni*(j+1+nj*(k+1))>=(int)a.size())
			{
				std::cout<<"Index out of range:\nYou can add a breakpoint in array3.h at line "<<__LINE__<<" to find the calling function."<<std::endl;
				return 0;
			}
		}

		if(!DEBUGING)
			assert(i>=0 && j>=0 && k>=0);
		else
		{
			if(i<0 || j<0 || k<0)
			{
				std::cout<<"Index out of range:\nYou can add a breakpoint in array3.h at line "<<__LINE__<<" to find the calling function."<<std::endl;
				return 0;
			}
		}

		if(std::is_floating_point<T>::value)
		{
			T d00 = a[i+ni*(j+nj*k)]      *(1-fx)+a[i+1+ni*(j+nj*k)]      *fx;//(i,j,k) and (i,j+1,k)
			T d10 = a[i+ni*(j+1+nj*k)]    *(1-fx)+a[i+1+ni*(j+1+nj*k)]    *fx;//(i,j+1,k) and (i+1,j+1,k)
			T d01 = a[i+ni*(j+nj*(k+1))]  *(1-fx)+a[i+1+ni*(j+nj*(k+1))]  *fx;//(i,j,k+1) and (i+1,j,k+1)
			T d11 = a[i+ni*(j+1+nj*(k+1))]*(1-fx)+a[i+1+ni*(j+1+nj*(k+1))]*fx;//(i,j+1,k+1) and (i+1,j+1,k+1)
			//now we do not need the x dimension anymore, do bilerp on y using the corners above to get a line:
			T e0 = d00*(1-fy)+d10*fy;
			T e1 = d01*(1-fy)+d11*fy;
			//now we only need liniear interpolation on the line defined above:
			return e0*(1-fz)+e1*fz;
		}
	}
	
	void get_barycentric(float x, unsigned int& i, float& f, unsigned int i_low, unsigned int i_high) const
	{
		float s=std::floor(x);
		i=(int)s;
		if(i<i_low)
		{
			i=i_low;
			f=0;
		}
		else if(i>i_high-2)
		{
			i=i_high-2;
			f=1;
		}
		else
			f=(float)(x-s);
	}

	T interpolate_value(const float x, const float y, const float z) const 
	{
		std::vector<float> point(3);
		point[0] = x; point[1] = y; point[2] = z;

		return interpolate_value(point);
	}

	T interpolate_value(const std::vector<float>& point) const 
	{
		unsigned int i,j,k;
		T fi,fj,fk;
		
		get_barycentric(point[0], i, fi, 0, this->ni);
		get_barycentric(point[1], j, fj, 0, this->nj);
		get_barycentric(point[2], k, fk, 0, this->nk);
		
		return gridtrilerp(i,j,k,fi,fj,fk);
	}

	T interpolate_value(const std::array<float,3>& point) const 
	{
		unsigned int i,j,k;
		T fi,fj,fk;
		
		get_barycentric(point[0], i, fi, 0, this->ni);
		get_barycentric(point[1], j, fj, 0, this->nj);
		get_barycentric(point[2], k, fk, 0, this->nk);
		
		return gridtrilerp(i,j,k,fi,fj,fk);
	}
	
	std::vector<float> interpolate_gradient(float x, float y, float z) const 
	{
		std::vector<float> point(3);
		point[0] = x; point[1] = y; point[2] = z;

		return interpolate_gradient(point);
	}
	
	std::array<float,3> interpolate_gradient(const std::array<float,3>& point) const 
	{
		std::vector<float> p;
		p.resize(3);
		p[0] = point[0]; p[1] = point[1]; p[2] = point[2];

		std::array<float,3> result;

		std::vector<float> out = interpolate_gradient(p);

		result[0] = out[0]; result[1] = out[1]; result[2] = out[2];
		return result;
	}

	std::vector<float> interpolate_gradient(const std::vector<float>& point) const 
	{
		std::vector<float> gradient(3);

		unsigned int i,j,k;
		float fx,fy,fz;
		
		get_barycentric(point[0], i, fx, 0, this->ni);
		get_barycentric(point[1], j, fy, 0, this->nj);
		get_barycentric(point[2], k, fz, 0, this->nk);
		
		T v000 = this->at(i,j,k);
		T v001 = this->at(i,j,k+1);
		T v010 = this->at(i,j+1,k);
		T v011 = this->at(i,j+1,k+1);
		T v100 = this->at(i+1,j,k);
		T v101 = this->at(i+1,j,k+1);
		T v110 = this->at(i+1,j+1,k);
		T v111 = this->at(i+1,j+1,k+1);
		
		T ddx00 = (v100 - v000);
		T ddx10 = (v110 - v010);
		T ddx01 = (v101 - v001);
		T ddx11 = (v111 - v011);
		T dv_dx = bilerp(ddx00,ddx10,ddx01,ddx11, fy,fz);
		
		T ddy00 = (v010 - v000);
		T ddy10 = (v110 - v100);
		T ddy01 = (v011 - v001);
		T ddy11 = (v111 - v101);
		T dv_dy = bilerp(ddy00,ddy10,ddy01,ddy11, fx,fz);
		
		T ddz00 = (v001 - v000);
		T ddz10 = (v101 - v100);
		T ddz01 = (v011 - v010);
		T ddz11 = (v111 - v110);
		T dv_dz = bilerp(ddz00,ddz10,ddz01,ddz11, fx,fy);
		
		gradient[0] = dv_dx;
		gradient[1] = dv_dy;
		gradient[2] = dv_dz;
		
		return gradient;
	}
};

// some common arrays

typedef Array3<bool, Array1<bool> > Array3b;
typedef Array3<double, Array1<double> > Array3d;
typedef Array3<float, Array1<float> > Array3f;
typedef Array3<long long, Array1<long long> > Array3ll;
typedef Array3<unsigned long long, Array1<unsigned long long> > Array3ull;
typedef Array3<int, Array1<int> > Array3i;
typedef Array3<unsigned int, Array1<unsigned int> > Array3ui;
typedef Array3<short, Array1<short> > Array3s;
typedef Array3<unsigned short, Array1<unsigned short> > Array3us;
typedef Array3<char, Array1<char> > Array3c;
typedef Array3<unsigned char, Array1<unsigned char> > Array3uc;


//specialized 3D vectorfield implementation
class Array3Vec3 : public Array3<std::array<float,3>>
{
	public:	

	Array3Vec3(void)
	{
		this->ni = 0;
		this->nj = 0;
		this->nk = 0;
		this->a.resize(0);
	}

	Array3Vec3(unsigned int ni_, unsigned int nj_, unsigned int nk_)
	{
		assert(ni_>=0 && nj_>=0 && nk_>=0);
		this->ni = ni_;
		this->nj = nj_;
		this->nk = nk_;
		this->a.resize(ni_*nj_*nk_);
	}

	std::array<float,3> convert_indices_to_point(unsigned int i, unsigned int j, unsigned int k) const
	{
		std::array<float,3> point;

		point[0] = (float) i;
		point[1] = (float) j;
		point[2] = (float) k;

		return point;
	}

	bool is_null_vector(std::array<float,3> *vector) const
	{
		for(unsigned int i = 0; i<vector->size(); i++)
		{
			if(vector->at(i) != vector->at(i))
				return false;

			if(vector->at(i) != 0.0f)
				return false;
		}

		return true;
	}

	std::array<float,3> null_vector() const
	{
		std::array<float,3> vec;

		vec[0] = 0.0f; vec[1] = 0.0f; vec[2] = 0.0f;

		return vec;
	}

	std::array<float,3> init_vector(float x, float y, float z) const
	{
		std::array<float,3> vec;

		vec[0] = x; vec[1] = y; vec[2] = z;

		return vec;
	}

	float vector_length(std::array<float,3> *vector) const
	{
		float sqr_sum = 0.0f; 

		for(unsigned int i = 0; i<vector->size(); i++)
		{
			sqr_sum += vector->at(i) * vector->at(i);
		}

		if(sqr_sum>=0.0f)
			return sqrt(sqr_sum);
		else
			return 0.0f;
	}

	std::array<float,3> vector_normalize(std::array<float,3> *vector) const
	{
		float length = vector_length(vector);

		std::array<float,3> resulting_vector;

		for(unsigned int i = 0; i<vector->size(); i++)
		{
			resulting_vector.at(i) = vector->at(i)/length;
		}

		return resulting_vector;
	}

	float vector_dot_product(std::array<float,3> *vector1, std::array<float,3> *vector2) const
	{
		float result = 0.0f;

		for(unsigned int i = 0; i<vector1->size(); i++)
		{
			result += vector1->at(i) * vector2->at(i);
		}

		return result;
	}

	std::array<float,3> add_vectors(std::array<float,3> *vector1, std::array<float,3> *vector2) const
	{
		std::array<float,3> result;

		for(unsigned int i = 0; i<vector1->size(); i++)
		{
			result.at(i) = vector1->at(i) + vector2->at(i);
		}

		return result;
	}

	std::array<float,3> subtract_vectors(std::array<float,3> *vector1, std::array<float,3> *vector2) const
	{
		std::array<float,3> result;

		for(unsigned int i = 0; i<vector1->size(); i++)
		{
			result.at(i) = vector1->at(i) - vector2->at(i);
		}

		return result;
	}

	std::array<float,3> negate_vector(std::array<float,3> *vector) const
	{
		std::array<float,3> result;

		for(unsigned int i = 0; i<vector->size(); i++)
		{
			result.at(i) = -vector->at(i);
		}

		return result;
	}

	std::array<float,3> multiple_vector_scalar(std::array<float,3> *vector, float scalar) const
	{
		std::array<float,3> result;

		for(unsigned int i = 0; i<vector->size(); i++)
		{
			result.at(i) = vector->at(i) * scalar;
		}

		return result;
	}

	std::array<float,3> divide_vector_scalar(std::array<float,3> *vector, float scalar) const
	{
		std::array<float,3> result;

		for(unsigned int i = 0; i<vector->size(); i++)
		{
			result.at(i) = vector->at(i) / scalar;
		}

		return result;
	}
	
	void assign(const std::array<float,3>& value)
	{ 
		for(unsigned int k = 0; k<nk; k++)
		for(unsigned int j = 0; j<nj; j++)
		for(unsigned int i = 0; i<ni; i++)
		{
			this->at(i,j,k) = value;
		}
	}
	
	std::array<float,3> gridtrilerp(unsigned int i, unsigned int j, unsigned int k, float fx, float fy, float fz) const
	{
		assert(i+this->ni*(j+this->nj*(k))<(int)this->a.size());
		assert(this->a.size()>0);
	
		std::array<float,3> vector;

		if(fx == 0.0f && fy == 0.0f && fz == 0.0f)
		{
			return this->at(i,j,k);
		}

		for(unsigned int it = 0; it<this->at(i,j,k).size(); it++)
		{
			float d00 = this->at(i,j,k)[it]         *(1-fx)+
						this->at(i+1,j,k)[it]       *fx;//(i,j,k) and (i,j+1,k)
			float d10 = this->at(i,j+1,k)[it]       *(1-fx)+
						this->at(i+1,j+1,k)[it]     *fx;//(i,j+1,k) and (i+1,j+1,k)
			float d01 = this->at(i,j,k+1)[it]       *(1-fx)+
						this->at(i+1,j,k+1)[it]     *fx;//(i,j,k+1) and (i+1,j,k+1)
			float d11 = this->at(i,j+1,k+1)[it]     *(1-fx)+
						this->at(i+1,j+1,k+1)[it]   *fx;//(i,j+1,k+1) and (i+1,j+1,k+1)
		
			//now we do not need the x dimension anymore, do bilerp on y using the corners above to get a line:
			float e0 = d00*(1-fy)+d10*fy;
			float e1 = d01*(1-fy)+d11*fy;
		
			//now we only need liniear interpolation on the line defined above:
			vector[it] = e0*(1-fz)+e1*fz;
		}
		return vector;
	}

	std::array<float,3> interpolate_value(const std::array<float,3>& point) const
	{
		unsigned int i,j,k;
		float fx,fy,fz;
	
		this->get_barycentric(point[0], i, fx, 0, this->ni);
		this->get_barycentric(point[1], j, fy, 0, this->nj);
		this->get_barycentric(point[2], k, fz, 0, this->nk);
	
		return this->gridtrilerp(i,j,k,fx,fy,fz);
	}
	
	std::array<std::array<float,3>,3> interpolate_gradient(const std::array<float,3>& point)
	{
		std::array<std::array<float,3>,3> gradient;

		unsigned int i,j,k;
		float fx,fy,fz;
	
		this->get_barycentric(point[0], i, fx, 0, this->ni);
		this->get_barycentric(point[1], j, fy, 0, this->nj);
		this->get_barycentric(point[2], k, fz, 0, this->nk);
		
		for(unsigned int mi = 0; mi<2; mi++) for(unsigned int mj = 0; mj<2; mj++)
			gradient[mi][mj] = 0.0f;
		
		float offset = 0.1f;
		
		std::array<float,3> px0,px2;
		std::array<float,3> py0,py2;
		std::array<float,3> pz0,pz2;

		px0[0] = point[0]-offset; px0[1] = point[1]; px0[2] = point[2];
		px2[0] = point[0]+offset; px2[1] = point[1]; px2[2] = point[2];

		py0[0] = point[0]; py0[1] = point[1]-offset; py0[2] = point[2];
		py2[0] = point[0]; py2[1] = point[1]+offset; py2[2] = point[2];
		
		pz0[0] = point[0]; pz0[1] = point[1]; pz0[2] = point[2]-offset;
		pz2[0] = point[0]; pz2[1] = point[1]; pz2[2] = point[2]+offset;

		std::array<float,3> center = this->interpolate_value(point);

		std::array<float,3> x0 = this->interpolate_value(px0);
		std::array<float,3> x2 = this->interpolate_value(px2);
		std::array<float,3> y0 = this->interpolate_value(py0);
		std::array<float,3> y2 = this->interpolate_value(py2);
		std::array<float,3> z0 = this->interpolate_value(pz0);
		std::array<float,3> z2 = this->interpolate_value(pz2);
			
		//compute gradient tensor
		gradient[0][0] = x0[0] + x2[0] - 2.0f*center[0];
		gradient[1][0] = x0[1] + x2[1] - 2.0f*center[1];
		gradient[2][0] = x0[2] + x2[2] - 2.0f*center[2];
		gradient[0][1] = y0[0] + y2[0] - 2.0f*center[0];
		gradient[1][1] = y0[1] + y2[1] - 2.0f*center[1];
		gradient[2][1] = y0[2] + y2[2] - 2.0f*center[2];
		gradient[0][2] = z0[0] + z2[0] - 2.0f*center[0];
		gradient[1][2] = z0[1] + z2[1] - 2.0f*center[1];
		gradient[2][2] = z0[2] + z2[2] - 2.0f*center[2];

		return gradient;
	}

	std::array<std::array<float,3>,3> interpolate_gradient(const unsigned int i, const unsigned int j, const unsigned int k) 
	{
		return interpolate_gradient(convert_indices_to_point(i, j, k));
	}
};

#endif
