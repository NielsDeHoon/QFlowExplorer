#include "FLIP_Particles.h"
#include <iostream>

//FLIP_Particles::FLIP_Particles(MACGrid &cGrid)
//{
//}

FLIP_Particles::~FLIP_Particles(void)
{
}

void FLIP_Particles::add_particle(const Vec3f &px, const Vec3f &pu)
{
	unsigned int i,j,k;
	float fx,fy,fz;
	grid.bary_x((float)px[0], i, fx);
	grid.bary_y((float)px[1], j, fy);
	grid.bary_z((float)px[2], k, fz);	

	if (!grid.vel_pressure.inRange(i,j,k)) //clamp the indices
	{
		//std::cout<<"Particle initialized outside the grid, discarded"<<std::endl;
		return;
	}

	if(grid.nodal_solid_phi.gridtrilerp(i,j,k,fx,fy,fz)<0.0)
	{
		//std::cout<<"Particle initialized in a solid cell, discarded"<<std::endl;
		return;
	}

	//if(grid.particleCells(i,j,k)>grid.initialParticlesPerCell)
	//{
		//return;
	//}

	x.push_back(px);
	u.push_back(pu);

	numberOfParticles++;
	grid.particleLocations.push_back(px);
}

void FLIP_Particles::remove_particle(unsigned int index)
{
	if(index >= numberOfParticles)
		return;
	x.erase(x.begin()+index);
	u.erase(u.begin()+index);

	numberOfParticles--;
}

Vec3ui FLIP_Particles::get_cell_of_particle(unsigned int index)
{
	assert(index<x.size());

	unsigned int i,j,k;
	float fx,fy,fz;

	grid.bary_x((float)x.at(index)[0], i, fx);
	grid.bary_y((float)x.at(index)[1], j, fy);
	grid.bary_z((float)x.at(index)[2], k, fz);
	
	return Vec3ui(i,j,k);
}

void FLIP_Particles::move_particles_in_grid(float dt)
{
	Vec3f midx(3), gu(3);

   unsigned int i,j,k;
   float fx, fy, fz;
   unsigned int oldi,oldj,oldk;
   float oldfx, oldfy, oldfz;
   float px,py,pz;
   float oldx, oldy, oldz;

   //index of the cell we initiated from
   for(unsigned int p=0; p<numberOfParticles; ++p)
   {
	   //clamp((float) x.at(p)[0],xmin,xmax);
	   //clamp((float) x.at(p)[1],ymin,ymax);
	   //clamp((float) x.at(p)[2],zmin,zmax);
	   oldx = x.at(p)[0]; px = x.at(p)[0];
	   oldy = x.at(p)[1]; py = x.at(p)[1];
	   oldz = x.at(p)[2]; pz = x.at(p)[2];
	   grid.bary_x((float)x.at(p)[0], oldi, oldfx);
	   grid.bary_y((float)x.at(p)[1], oldj, oldfy);
	   grid.bary_z((float)x.at(p)[2], oldk, oldfz);

	   gu[0]=gu[1]=gu[2]=0;
   
	   if (!grid.vel_pressure.inRange(oldi,oldj,oldk)) //clamp the indices
	   {
		   grid.vel_pressure.clamp(oldi,oldj,oldk);
	   }

	   //assert(grid.nodal_solid_phi.gridtrilerp(oldi,oldj,oldk,oldfx,oldfy,oldfz)>=0.0); 
	   //particles should not start inside a solid

	   if(grid.nodal_solid_phi.gridtrilerp(oldi,oldj,oldk,oldfx,oldfy,oldfz)<=0.0) 
	   {
		   //remove_particle(p);
		   continue;
	   }

	   // first stage of Runge-Kutta 2 (do a half Euler step)
	   grid.trilerp_uvw((float)px, (float)py, (float)pz, gu[0], gu[1], gu[2]);

	   //calculate midx which is the half Euler step position
	   midx[0] = (float)px+0.5f*dt*gu[0];
	   midx[1] = (float)py+0.5f*dt*gu[1];
	   midx[2] = (float)pz+0.5f*dt*gu[2];

	  // second stage of Runge-Kutta 2
	  grid.trilerp_uvw(midx[0], midx[1], midx[2], gu[0], gu[1], gu[2]);

	  //update the location:
	  px += dt*gu[0];
	  py += dt*gu[1];
	  pz += dt*gu[2];

	  x.at(p)[0] = px;
	  x.at(p)[1] = py;
	  x.at(p)[2] = pz;

	  //apply boundaries
	  grid.bary_x((float)x.at(p)[0], i, fx);
	  grid.bary_y((float)x.at(p)[1], j, fy);
	  grid.bary_z((float)x.at(p)[2], k, fz);	
	  
	  if (!grid.vel_pressure.inRange(i,j,k)) 
		  grid.vel_pressure.clamp(i,j,k);

	  float currentPhi = grid.nodal_solid_phi.gridtrilerp(i,j,k,fx,fy,fz);

	  if(currentPhi<0.0)
	  {
		  Vec3f xold(3);
		  xold[0] = oldx;
		  xold[1] = oldy;
		  xold[2] = oldz;
		  
		  Vec3f xnew(3);
		  xnew[0] = x.at(p)[0];
		  xnew[1] = x.at(p)[1];
		  xnew[2] = x.at(p)[2];
		  
		  x.at(p) = distanceRay(xold,xnew);

		  //now calculate the distance we travelled without collision detection:
		  float distanceToMoveAfterBouncing = (float)sqrt(sqr(xnew[0]-xold[0])+sqr(xnew[1]-xold[1])+sqr(xnew[2]-xold[2]));

		  //minus the distance to the collision yields the distance left to be travelled:
		  distanceToMoveAfterBouncing -= (float)sqrt(sqr(xold[0]-x.at(p)[0])+sqr(xold[1]-x.at(p)[1])+sqr(xold[2]-x.at(p)[2]));
		  
		  Vec3f normal(3);
		  normal = grid.calculateSurfaceNormal(grid.nodal_solid_phi, xnew);
		  
		  //using Incoming vector I, Normal N and Resulting vector R we have to bounce of using this formula:
		  //R= 2*(-I dot N)*N + I
		  std::vector<double> I(3);
		  I[0] = xnew[0]-xold[0];
		  I[1] = xnew[1]-xold[1];
		  I[2] = xnew[2]-xold[2];
		  //normalize it:
		  double length = sqrt(sqr(I[0])+sqr(I[1])+sqr(I[2]));
		  I[0]/=length;
		  I[1]/=length;
		  I[2]/=length;

		  double dotProduct = -I[0]*normal[0]+-I[1]*normal[1]+-I[2]*normal[2];
		  
		  std::vector<double> result(3);
		  result[0] = 2*dotProduct*normal[0]+I[0];
		  result[1] = 2*dotProduct*normal[1]+I[1];
		  result[2] = 2*dotProduct*normal[2]+I[2];

		  result[0] = 2*normal[0]+I[0];
		  result[1] = 2*normal[1]+I[1];
		  result[2] = 2*normal[2]+I[2];

		  //normalize it:
		  length = sqrt(sqr(result[0])+sqr(result[1])+sqr(result[2]));
		  result[0]/=length;
		  result[1]/=length;
		  result[2]/=length;	

		  //scale such that the number of steps we "moved" in the solids reflects the bounce back speed:
		  float scale = 1.0f;
		  float magnitude = sqrt(sqr((float)uOld.at(p)[0])+sqr((float)uOld.at(p)[1])+sqr((float)uOld.at(p)[2]));
		  
		  //find velocity in the -normal direction:
		  float dot = (float)(normal[0]*uOld.at(p)[0]+normal[1]*uOld.at(p)[1]+normal[2]*uOld.at(p)[2]);
		  u.at(p)[0] -= dot*normal[0];
		  u.at(p)[1] -= dot*normal[1];
		  u.at(p)[2] -= dot*normal[2];
	  }
	}
}

float FLIP_Particles::kernel(float distance)
{
	assert(distance>=0.0);
	assert(distance<=1.0);
	float result = 0.0;
	float x = distance*grid.dx;
	//A particle can only influence one cell !!!
	//also the function used here must be strictly decreasing when distance increases
	//by using the variable x you always take the gridsize into account (which is not always == 1)
	//some examples of (simple) kernel functions
	result = grid.dx - x;
	//result = cos((float) x);
	//result = 1.0/(x + 1.0);

	if (result < 0.0) result = 0.0; //a particle is not allowed to have negative influence
	if (result > grid.dx) result = grid.dx; //just for safety
	return result;
}

template<class T>
void FLIP_Particles::accumulate(T &accum, float q, unsigned int i, unsigned int j, unsigned int k, float fx, float fy, float fz)
{
	if(fz>1.0 || fz<0.0 || fy>1.0 || fy<0.0 || fx>1.0 || fx<0.0||
		i<0 || j<0 || k<0 || i>=accum.ni || j>=accum.nj || k>=accum.nk)
	{
		std::cout<<"fx="<<fx<<",	fy="<<fy<<",	fz="<<fz<<std::endl<<"i="<<i<<",	j="<<j<<",	k="<<k<<std::endl;
	}
	
	//make sure the fractions of locations in the cell are between 0 and 1
	assert(fz<=1.0 && fz>=0.0);
	assert(fy<=1.0 && fy>=0.0);
	assert(fx<=1.0 && fx>=0.0);

	float weight; //used as "Kernel" function value
   //diffusion

   assert(i>=0);
   assert(i<accum.ni);
   assert(j>=0);
   assert(j<accum.nj);
   assert(k>=0);
   assert(k<accum.nk);

   weight=kernel(fx)*kernel(fy)*kernel(fz);
   accum(i,j,k)+=weight*q;
   sum(i,j,k)+=weight;

   weight=kernel(1-fx)*kernel(fy)*kernel(fz);
   accum(i+1,j,k)+=weight*q;
   sum(i+1,j,k)+=weight;
   
   weight=kernel(fx)*kernel(1-fy)*kernel(fz);
   accum(i,j+1,k)+=weight*q;
   sum(i,j+1,k)+=weight;

   weight=kernel(fx)*kernel(fy)*kernel(1-fz);
   accum(i,j,k+1)+=weight*q;
   sum(i,j,k+1)+=weight;

   weight=kernel(1-fx)*kernel(1-fy)*kernel(fz);
   accum(i+1,j+1,k)+=weight*q;
   sum(i+1,j+1,k)+=weight;

   weight=kernel(1-fx)*kernel(fy)*kernel(1-fz);
   accum(i+1,j,k+1)+=weight*q;
   sum(i+1,j,k+1)+=weight;

   weight=kernel(fx)*kernel(1-fy)*kernel(1-fz);
   accum(i,j+1,k+1)+=weight*q;
   sum(i,j+1,k+1)+=weight;

   weight=kernel(1-fx)*kernel(1-fy)*kernel(1-fz);
   accum(i+1,j+1,k+1)+=weight*q;
   sum(i+1,j+1,k+1)+=weight;
}

void FLIP_Particles::clamper(unsigned int i, unsigned int j, unsigned int k)
{
	if(i<1) i=1;
	if(j<1) j=1;
	if(k<1) k=1;
	if(i>grid.ni-2) i=grid.ni-2;
	if(j>grid.nj-2) j=grid.ni-2;
	if(k>grid.nk-2) k=grid.ni-2;
}

void FLIP_Particles::transfer_to_grid(void)
{	
   grid.particleCells.set_zero();

   unsigned int p, i, ui, j, vj, k, wk;
   float fx, ufx, fy, vfy, fz, wfz;

   grid.u.set_zero();
   grid.v.set_zero();
   grid.w.set_zero();

   sum.set_zero();

   //copy the particle locations to the grid:
   grid.particleLocations.clear();
   for(p=0; p<numberOfParticles; ++p)
   {
	   grid.particleLocations.push_back(x.at(p));
   }

   //first the x coordinates
   for(p=0; p<numberOfParticles; ++p)
   {
	  grid.bary_x((float)x.at(p)[0], ui, ufx);
	  grid.bary_y_centre((float)x.at(p)[1], j, fy);
	  grid.bary_z_centre((float)x.at(p)[2], k, fz);
	  accumulate(grid.u, (float)u.at(p)[0], ui, j, k, ufx, fy, fz);
   }

   for(k=0; k<grid.u.nk-1; ++k)
   {
		for(j=0; j<grid.u.nj-1; ++j) 
		{
			for(i=0; i<grid.u.ni-1; ++i)
			{
				if(sum(i,j,k)!=0) 
				{
					grid.u(i,j,k)/=sum(i,j,k);
				}
			}
		}
   }

   grid.v.set_zero();
   sum.set_zero();
   for(p=0; p<numberOfParticles; ++p){
	  grid.bary_x_centre((float)x.at(p)[0], i, fx);
	  grid.bary_y((float)x.at(p)[1], vj, vfy);
	  grid.bary_z_centre((float)x.at(p)[2], k, fz);
	  accumulate(grid.v, (float)u.at(p)[1], i, vj, k, fx, vfy, fz);
   }

	for(unsigned k=0; k<grid.v.nk-1; ++k)
	{
		for(unsigned j=0; j<grid.v.nj-1; ++j) 
		{
			for(unsigned i=0; i<grid.v.ni-1; ++i)
			{
				if(sum(i,j,k)!=0) 
				{
					grid.v(i,j,k)/=sum(i,j,k);
				}
			}
		}
	}

   grid.w.set_zero();
   sum.set_zero();
   for(p=0; p<numberOfParticles; ++p){
	  grid.bary_x_centre((float)x.at(p)[0], i, fx);
	  grid.bary_y_centre((float)x.at(p)[1], j, fy);
	  grid.bary_z((float)x.at(p)[2], wk, wfz);
	  accumulate(grid.w, (float)u.at(p)[2], i, j, wk, fx, fy, wfz);
   }

	for(k=0; k<grid.w.nk-1; ++k)
	{
		for(j=0; j<grid.w.nj-1; ++j) 
		{
			for(i=0; i<grid.w.ni-1; ++i)
			{
				if(sum(i,j,k)!=0) 
				{
					grid.w(i,j,k)/=sum(i,j,k);
				}
			}
		}
	}

   // identify where particles are in grid
   for(p=0; p<numberOfParticles; ++p)
   {
	  grid.bary_x((float)x.at(p)[0], i, fx);
	  grid.bary_y((float)x.at(p)[1], j, fy);
	  grid.bary_z((float)x.at(p)[2], k, fz);
	  
	  grid.particleCells(i,j,k)++;
   }
}

void FLIP_Particles::update_from_grid(void)
{
   unsigned int p;
   unsigned int i, ui, j, vj, k, wk;
   float fx, ufx, fy, vfy, fz, wfz;

   for(p=0; p<numberOfParticles; ++p)
   {
	  grid.bary_x((float)x.at(p)[0], ui, ufx);
	  grid.bary_x_centre((float)x.at(p)[0], i, fx);
	  grid.bary_y((float)x.at(p)[1], vj, vfy);
	  grid.bary_y_centre((float)x.at(p)[1], j, fy);
	  grid.bary_z((float)x.at(p)[2], wk, wfz);
	  grid.bary_z_centre((float)x.at(p)[2], k, fz);


	  //u.at(p)[0]= alpha_ratio*u.at(p)[0];
	  //u.at(p)[1]= alpha_ratio*u.at(p)[1];
	  //u.at(p)[2]= alpha_ratio*u.at(p)[2];
	  
	  /**
	  //FLIP:
	  u.at(p)[0]+= alpha_ratio * grid.du.gridtrilerp(ui,j,k,ufx,fy,fz);
	  u.at(p)[1]+= alpha_ratio * grid.dv.gridtrilerp(i,vj,k,fx,vfy,fz);
	  u.at(p)[2]+= alpha_ratio * grid.dw.gridtrilerp(i,j,wk,fx,fy,wfz);

	  //PIC:
	  u.at(p)[0]+= (1-alpha_ratio) * grid.u.gridtrilerp(ui, j, k, ufx, fy, fz);
	  u.at(p)[1]+= (1-alpha_ratio) * grid.v.gridtrilerp(i, vj, k, fx, vfy, fz); 
	  u.at(p)[2]+= (1-alpha_ratio) * grid.w.gridtrilerp(i, j, wk, fx, fy, wfz);
	  **/
	  
	  assert(alpha_ratio>=0.0f && alpha_ratio<=1.0f);
	  u.at(p)[0] = (alpha_ratio)*grid.u.gridtrilerp(ui, j, k, ufx, fy, fz) + 
				   (1.0f-alpha_ratio)*(u.at(p)[0]+grid.du.gridtrilerp(ui,j,k,ufx,fy,fz));
	  u.at(p)[1] = (alpha_ratio)*grid.v.gridtrilerp(i, vj, k, fx, vfy, fz) + 
				   (1.0f-alpha_ratio)*(u.at(p)[1]+grid.dv.gridtrilerp(i,vj,k,fx,vfy,fz));
	  u.at(p)[2] = (alpha_ratio)*grid.w.gridtrilerp(i, j, wk, fx, fy, wfz) + 
				   (1.0f-alpha_ratio)*(u.at(p)[2]+grid.dw.gridtrilerp(i,j,wk,fx,fy,wfz));
	  
	  //bounce the velocity from the wall:
	  /**
	  if (i>0 && grid.marker(i-1,j,k)==SOLIDCELL && u.at(p)[0]<0.0) u.at(p)[0] = std::abs(u.at(p)[0]);
	  if (j>0 && grid.marker(i,j-1,k)==SOLIDCELL && u.at(p)[1]<0.0) u.at(p)[1] = std::abs(u.at(p)[1]);
	  if (k>0 && grid.marker(i,j,k-1)==SOLIDCELL && u.at(p)[2]<0.0) u.at(p)[2] = std::abs(u.at(p)[2]);	  
	  if (i<grid.marker.ni-1 && grid.marker(i+1,j,k)==SOLIDCELL && u.at(p)[0]>0.0) u.at(p)[0] = -(u.at(p)[0]);
	  if (j<grid.marker.ni-1 && grid.marker(i,j+1,k)==SOLIDCELL && u.at(p)[1]>0.0) u.at(p)[1] = -(u.at(p)[1]);
	  if (k<grid.marker.ni-1 && grid.marker(i,j,k+1)==SOLIDCELL && u.at(p)[2]>0.0) u.at(p)[2] = -(u.at(p)[2]);
	  **/
   }
}

//Replace velocity of particles by the velocity of the grid: PIC
void FLIP_Particles::transfer_velocity_from_grid(void)
{
	unsigned int p;
	unsigned int i, ui, j, vj, k, wk;
	float fx, ufx, fy, vfy, fz, wfz;
	
	for(p=0; p<numberOfParticles; ++p)
	{
		Vec3f position = x.at(p);
		Vec3f velocity;

		grid.bary_x((float)position[0], ui, ufx);
		grid.bary_x_centre((float)position[0], i, fx);
		grid.bary_y((float)position[1], vj, vfy);
		grid.bary_y_centre((float)position[1], j, fy);
		grid.bary_z((float)position[2], wk, wfz);
		grid.bary_z_centre((float)position[2], k, fz);

		velocity[0] = grid.u.gridtrilerp(ui, j, k, ufx, fy, fz);
		velocity[1] = grid.v.gridtrilerp(i, vj, k, fx, vfy, fz); 
		velocity[2] = grid.w.gridtrilerp(i, j, wk, fx, fy, wfz);

		u.at(p) = velocity;
	}
}

Vec3f FLIP_Particles::distanceRay(Vec3f xold, Vec3f xnew)
{
	Vec3f result(3);
	unsigned int newi,newj,newk;
	float newfx,newfy,newfz;

	unsigned int oldi,oldj,oldk;
	float oldfx,oldfy,oldfz;

	float foundPhi = 1e2;

	result[0] = xold[0];
	result[1] = xold[1];
	result[2] = xold[2];

	float thresshold = 1.0;
	do{
		grid.bary_x((float)xold[0], oldi, oldfx);
		grid.bary_y((float)xold[1], oldj, oldfy);
		grid.bary_z((float)xold[2], oldk, oldfz);	

		grid.bary_x((float)xnew[0], newi, newfx);
		grid.bary_y((float)xnew[1], newj, newfy);
		grid.bary_z((float)xnew[2], newk, newfz);	

		float previousPhi = grid.nodal_solid_phi.gridtrilerp(oldi,oldj,oldk,oldfx,oldfy,oldfz);
		float currentPhi = grid.nodal_solid_phi.gridtrilerp(newi,newj,newk,newfx,newfy,newfz);

		//assert(previousPhi>=0.0);
		//assert(currentPhi<=0.0);

		//assert(previousPhi>=0.0);
		//get the fraction of the original and new phi to get the fraction of movement we want
		float fraction = 0.0;
		//calculate at which fraction phi = 0
		fraction = previousPhi/(previousPhi-currentPhi);
		//to ensure that we do not place the particle exactly on the boundary we add some offset:
		fraction -= (float)1e-1;
		if (fraction > 1.0) fraction = 1.0;
		if (fraction < 0.0) fraction = 0.0;

		//move this fraction instead of the whole movement
		result[0] = ((float)1.0-fraction)*xold[0] + fraction*xnew[0];
		result[1] = ((float)1.0-fraction)*xold[1] + fraction*xnew[1];
		result[2] = ((float)1.0-fraction)*xold[2] + fraction*xnew[2];

		grid.bary_x((float)result[0], oldi, oldfx);
		grid.bary_y((float)result[1], oldj, oldfy);
		grid.bary_z((float)result[2], oldk, oldfz);	

		foundPhi = grid.nodal_solid_phi.gridtrilerp(oldi,oldj,oldk,oldfx,oldfy,oldfz);
		if (foundPhi>0.0)
		{
			xnew[0] = result[0];
			xnew[1] = result[1];
			xnew[2] = result[2];
		}
		else
		{
			xold[0] = result[0];
			xold[1] = result[1];
			xold[2] = result[2];
		}
	}
	while (false && foundPhi>thresshold);//just one iteration

	//do some clamping
	if (result[0]<0.0) result[0] = 0.0;
	if (result[1]<0.0) result[1] = 0.0;
	if (result[2]<0.0) result[2] = 0.0;
	if (result[0]>grid.dx*grid.ni) result[0] = grid.dx*grid.ni;
	if (result[1]>grid.dx*grid.nj) result[1] = grid.dx*grid.nj;
	if (result[2]>grid.dx*grid.nk) result[2] = grid.dx*grid.nk;

	return result;
}

void FLIP_Particles::save_old_velocity(void)
{
	uOld.clear();
	uOld.resize(u.size());
	for( int p = 0; p < (int)u.size(); p++)
	{
		uOld.at(p)=u.at(p);
	}
}

unsigned int FLIP_Particles::size()
{
	return (unsigned int) x.size();
}

void FLIP_Particles::compute_phi() 
{
   //Estimate from particles
   grid.liquid_phi.assign(3*grid.dx);
   for(unsigned int p = 0; p < x.size(); ++p) 
   {
	  Vec3f point;
	  point[0] = (float)x.at(p)[0];
	  point[1] = (float)x.at(p)[1];
	  point[2] = (float)x.at(p)[2];

	  unsigned int i,j,k;
	  float fx,fy,fz;

	  //determine containing cell;
	  get_barycentric((point[0])/grid.dx-0.5f, i, fx, 0, grid.ni);
	  get_barycentric((point[1])/grid.dx-0.5f, j, fy, 0, grid.nj);
	  get_barycentric((point[2])/grid.dx-0.5f, k, fz, 0, grid.nk);
	  
	  //compute distance to surrounding few points, keep if it's the minimum
	  for(unsigned int k_off = k-2; k_off<=k+2; ++k_off) for(unsigned int j_off = j-2; j_off<=j+2; ++j_off) for(unsigned int i_off = i-2; i_off<=i+2; ++i_off) {
		 if(i_off < 0 || i_off >= grid.ni || j_off < 0 || j_off >= grid.nj  || k_off < 0 || k_off >= grid.nk)
			continue;
		 
		 Vec3f pos((i_off+0.5f)*grid.dx, (j_off+0.5f)*grid.dx, (k_off+0.5f)*grid.dx);
		 float phi_temp = dist(pos, point) - 1.02f*grid.particle_radius;
		 grid.liquid_phi(i_off,j_off,k_off) = min(grid.liquid_phi(i_off,j_off,k_off), phi_temp);
	  }
   }
   
   //"extrapolate" phi into solids if nearby
   for(unsigned int k = 0; k < grid.nk; ++k) for(unsigned int j = 0; j < grid.nj; ++j) for(unsigned int i = 0; i < grid.ni; ++i) 
   {
	   if(grid.liquid_phi(i,j,k) < 0.5*grid.dx)
	   {
		   float solid_phi_val = 0.125f*(grid.nodal_solid_phi(i,j,k) + grid.nodal_solid_phi(i+1,j,k) + grid.nodal_solid_phi(i,j+1,k) + grid.nodal_solid_phi(i+1,j+1,k)
									  +grid.nodal_solid_phi(i,j,k+1) + grid.nodal_solid_phi(i+1,j,k+1) + grid.nodal_solid_phi(i,j+1,k+1) + grid.nodal_solid_phi(i+1,j+1,k+1));
			if(solid_phi_val < 0)
			   grid.liquid_phi(i,j,k) = -0.5f*grid.dx;
	   }
   }  
}

Array3f FLIP_Particles::compute_fluid_sdf() 
{
   Array3f result(grid.ni,grid.nj,grid.nk);

   //Estimate from particles
   result.assign(3*grid.dx);
   for(unsigned int p = 0; p < x.size(); ++p) 
   {
	  Vec3f point;
	  point[0] = (float)x.at(p)[0];
	  point[1] = (float)x.at(p)[1];
	  point[2] = (float)x.at(p)[2];

	  unsigned int i,j,k;
	  float fx,fy,fz;

	  //determine containing cell;
	  get_barycentric((point[0])/grid.dx-0.5f, i, fx, 0, grid.ni);
	  get_barycentric((point[1])/grid.dx-0.5f, j, fy, 0, grid.nj);
	  get_barycentric((point[2])/grid.dx-0.5f, k, fz, 0, grid.nk);
	  
	  //compute distance to surrounding few points, keep if it's the minimum
	  for(unsigned int k_off = k-2; k_off<=k+2; ++k_off) for(unsigned int j_off = j-2; j_off<=j+2; ++j_off) for(unsigned int i_off = i-2; i_off<=i+2; ++i_off) {
		 if(i_off < 0 || i_off >= grid.ni || j_off < 0 || j_off >= grid.nj  || k_off < 0 || k_off >= grid.nj)
			continue;
		 
		 Vec3f pos((i_off+0.5f)*grid.dx, (j_off+0.5f)*grid.dx, (k_off+0.5f)*grid.dx);
		 float phi_temp = dist(pos, point) - 1.02f*grid.particle_radius;
		 result(i_off,j_off,k_off) = min(result(i_off,j_off,k_off), phi_temp);
		 
		 //make sure phi doesn't penetrate the solid:
		 if(grid.nodal_solid_phi(i_off,j_off,k_off)<0)
		 {
			  result(i_off,j_off,k_off) = abs(grid.nodal_solid_phi(i_off,j_off,k_off));
		 }
	  }
   }

   return result;
}