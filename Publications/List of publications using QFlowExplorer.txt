Real-time Illustrative Visualization of Cardiovascular Hemodynamics,
Roy van Pelt, 2012
https://diglib.eg.org/handle/10.2312/8293

Niels de Hoon, Roy van Pelt, Andrei Jalba, Anna Vilanova, 2014
http://graphics.tudelft.nl/Publications-new/2014/DPJV14/

Temporal Interpolation of 4D PC-MRI Blood-flow Measurements Using Bidirectional Physics-based Fluid Simulation   Additional_materials.pdf EGauthorGuidelines-vcbm16-fin.pdf
Niels de Hoon, Andrei Jalba, Elmar Eisemann, A. Vilanova, 2016
http://graphics.tudelft.nl/Publications-new/2016/DJEV16/

A framework for fast initial exploration of PC-MRI cardiac flow
Arjan Broos, Niels de Hoon, Patrick de Koning, Rob van der Geest, Anna Vilanova, Andrei Jalba, 2016
http://graphics.tudelft.nl/Publications-new/2016/BDDVVJ16/

Presence of aortic root vortex formation after TAVI with CENTERA confrmed using 4D-flow magnetic resonance imaging
Jeroen Vendrik, Emile Farag, Niels de Hoon, Jolanda Kluin, Jan Baan, 2018
http://graphics.tudelft.nl/Publications-new/2018/VFDKB18/