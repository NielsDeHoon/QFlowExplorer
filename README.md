# QFlow Explorer

The QFlow Explorer (QFE) framework provides various tools for the analyses of PC-MRI acquired blood-flow data.
Various visualization and filtering techniques are implement to guide the user in understanding the complex behavior of the flow of blood. 

This software was developed by the Eindhoven University of Technology (TU/e) and Delft University of Technology (TU Delft).

The original creator of the framework is Roy van Pelt (TU/e).
An overview of his publications using QFlowExplorer can be found here:
https://diglib.eg.org/handle/10.2312/8293

Later the framework was extended by Niels de Hoon (TU Delft).

Another major contributor is Anna Vilanova (TU/e and TU Delft).