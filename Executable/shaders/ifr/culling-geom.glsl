#version 330

uniform int silhouetteShadingMode;
// culling-frag.glsl

uniform int culling;

in  vec3 vertexNormal[3];
out vec3 normal;

//----------------------------------------------------------------------------
void main()
{ 
  vec3  triangleNormal, eye;
  float ndote; 
  
  // Initialize
  triangleNormal = vec3(0.0);
  eye            = vec3(0.0,0.0,-1.0);

  // Determine the triangle normal  
  for(int i = 0; i < 3; ++i)
  {
    triangleNormal = triangleNormal + 1.0/3.0*vertexNormal[i]; 
  }
  
  // Compute the angle
  ndote = dot(triangleNormal, eye);
  
  // Render the triangle, dependent on the culling status
  if((culling == 0) || ((culling == 1) && (ndote >= 0.0)))
  {
    // Render the triangle
    for(int i = 0; i < gl_VerticesIn; ++i)
    {       
      normal        = vertexNormal[i];
      gl_FrontColor = gl_FrontColorIn[i];   
      gl_Position   = gl_PositionIn[i];  
  
      EmitVertex();
    }  
  }
}