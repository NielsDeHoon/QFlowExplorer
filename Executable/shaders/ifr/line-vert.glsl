// linecolor-vert.c
#version 330
uniform mat4 matrixVoxelPatient;

in vec3 qfe_Vertex;
in vec3 qfe_TexCoord;

out vec3 seedPatientCoord;
out vec3 texCoord;

void main()
{
  seedPatientCoord = vec4(qfe_Vertex,1.0).xyz;
  texCoord         = qfe_TexCoord;
}