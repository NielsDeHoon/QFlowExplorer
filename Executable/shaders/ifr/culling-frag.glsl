// culling-frag.glsl

uniform int       silhouetteShadingMode;
uniform sampler1D lightTex;
uniform vec3      lightDir;
uniform vec4      lightSpec;
uniform int       culling;
uniform int       wss;

in  vec3 normal;
out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Cel2(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
void main()
{
  float ndotl, ndote, index, diffuse;
  vec4 color;
  vec3 n = normalize(normal);
  vec3 l = normalize(-lightDir);
  vec3 e = vec3(0.0,0.0,1.0);

  // We are rendering backfaces, get the right angle (-n * l)  
  if(culling == 1)
    n = normalize(-normal);  
    
  ndotl = dot(n,l);  
  ndote = dot(n,-e);
  color = vec4(gl_Color.rgb,1.0);
  
  if(wss == 1)
  {
  //vec4 wssColor = vec4(0.43,0.48,1.0,1.0);
  vec4 wssColor = vec4(0.1,0.1,1.0,1.0);
  if(gl_Color.a > 0.75)
    color = mix(color, wssColor, 1.0);
  else if (gl_Color.a > 0.5)
    color = mix(color, wssColor, 0.5);
  else if (gl_Color.a > 0.25)
    color = mix(color, wssColor, 0.2);
  }
  
  if(silhouetteShadingMode == 0) // Toon shading 1
  {
    col = Cel(col, l.xyz, n.xyz);
  }
  else  
  if(silhouetteShadingMode == 1) // Toon shading 2
  {
    col = Cel2(col, l.xyz, n.xyz);
  }
  else
  if(silhouetteShadingMode == 2) // Diffuse shading
  {
    col = Phong(col, l.xyz, n.xyz, e.xyz, lightSpec.x, lightSpec.y, vec2(lightSpec.z,lightSpec.w));
  }
  else
  //silhouetteShadingMode == 3 // Fresnel (different shader)
  if(silhouetteShadingMode == 4) // Flat shading
  {
	col = col;
  }
  else
  if(silhouetteShadingMode == 5) // X-Ray shading
  {
	col = XRay(col, l, n);
  }      
  
  qfe_FragColor = color;
}