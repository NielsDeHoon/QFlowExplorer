// silhouette-vert.glsl
#version 330

uniform mat4 matrixModelViewInverse;
uniform mat4 matrixModelViewProjection;

in  vec3 qfe_Vertex;
in  vec3 qfe_Normal;

out vec3 vertexNormal;

//----------------------------------------------------------------------------
void main()
{
  mat4 matrixNormal;
  
  matrixNormal   = inverse(matrixModelViewInverse);
  vertexNormal   = (matrixNormal * vec4(qfe_Normal,0.0)).xyz;   
    
  gl_Position    = matrixModelViewProjection * vec4(qfe_Vertex,1.0);  
  
}