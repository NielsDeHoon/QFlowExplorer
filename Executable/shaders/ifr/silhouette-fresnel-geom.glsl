// fresnel-geom
#version 330

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in  vec3 vertexPosition[3];
in  vec3 vertexNormal[3];

out vec3 normal;

//----------------------------------------------------------------------------
void main()
{ 
  vec3  triangleNormal, eye;
  float ndote; 
  
  // Initialize
  triangleNormal = vec3(0.0);
  eye            = vec3(0.0,0.0,-1.0);

  // Determine the triangle normal  
  for(int i = 0; i < 3; ++i)
  {
    triangleNormal = triangleNormal + 1.0/3.0*vertexNormal[i]; 
  }
  
  // Compute the angle
  ndote = dot(triangleNormal, eye);
  
  // Render the front facing triangles
  if(ndote < 0.0)
  {
    // Render the triangle
    for(int i = 0; i < 3; ++i)
    {       
      normal        = vertexNormal[i];
  
      gl_Position   = vec4(vertexPosition[i],1.0);  
  
      EmitVertex();
    }  
  }
}