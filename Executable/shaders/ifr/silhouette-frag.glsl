// silhouette-frag.glsl
#version 330

uniform int       silhouetteShadingMode;
uniform sampler1D lightTex;
uniform vec3      lightDir;
uniform vec4      lightSpec;
uniform int       culling;
uniform vec3      color;

in  vec3 vertexNormal;
out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Cel2(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 XRay(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec4 Gooch(in vec4 col, in vec3 l, in vec3 n, in vec3 v);

//----------------------------------------------------------------------------
void main()
{  
  float ndotl, ndote, index, diffuse;
  vec4 col;
  vec3 n = normalize(vertexNormal);
  vec3 l = normalize(-lightDir);
  vec3 e = vec3(0.0,0.0,1.0);

  // We are rendering backfaces, get the right angle (-n * l)  
  if(culling == 1)
    n = normalize(-vertexNormal);  
    
  col   = vec4(color,1.0);
  ndotl = dot(n,l);  
  
  if(silhouetteShadingMode == 0) // Toon shading 1
  {
    col = Cel2(col, l.xyz, n.xyz);    
  }
  else  
  if(silhouetteShadingMode == 1) // Toon shading 2
  {
    index = max(ndotl,0.0);  
    col = texture(lightTex, index);
    col = col * vec4(color,1.0);   
  }
  else
  //silhouetteShadingMode == 2 // Fresnel (different shader)
  if(silhouetteShadingMode == 3) // Diffuse shading
  {
    col = Phong(col, l.xyz, n.xyz, e.xyz, lightSpec.x, lightSpec.y, vec2(lightSpec.z,lightSpec.w));
  }  
  else
  if(silhouetteShadingMode == 4) // Flat shading
  {
	col = col;
  }
  else
  if(silhouetteShadingMode == 5) // X-Ray shading
  {
	col = XRay(col, e, n);
  }
  else
  if(silhouetteShadingMode == 6) // Gooch shading
  {
	col = Gooch(col, l.xyz, n.xyz, e.xyz);
  }
 
  qfe_FragColor = col;
}
