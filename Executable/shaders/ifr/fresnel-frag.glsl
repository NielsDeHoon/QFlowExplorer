// contour-frag.glsl

varying vec3 normal;

uniform vec3      lightDir;
uniform vec4      lightSpec;
uniform int       wss;

//----------------------------------------------------------------------------
vec4 Cel2(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
void main()
{
  float ndotl, ndote, index, diffuse;
  vec4 color;
  vec3  n = normalize(normal);
  vec3  l = normalize(-lightDir);
  vec3  e = vec3(0.0,0.0,1.0);
  float f, r;

  // Compute light and view angles
  ndotl = dot(n,l);  
  ndote = dot(n,-e);
  
  color   = gl_Color;
  
  // Process wss values
  if(wss == 1)
  {
    //vec4 wssColor = vec4(0.43,0.48,1.0,1.0);
    vec4 wssColor = vec4(0.1,0.1,1.0,1.0);
    if(gl_Color.a > 0.75)
      color = mix(color, wssColor, 1.0);
    else if (gl_Color.a > 0.5)
      color = mix(color, wssColor, 0.5);
    else if (gl_Color.a > 0.25)
      color = mix(color, wssColor, 0.2);
  }
  
  // Compute Fresnel term
  r = 0.5;
  f = 1.0-pow(abs(ndote),r);
  
  //color   = gl_Color;
  color   = Cel2(color, l.xyz, n.xyz);  
  color.a = f;  
  
  //color = Phong(color, l.xyz, n.xyz, e.xyz, lightSpec.x, lightSpec.y, vec2(lightSpec.z,lightSpec.w));
  
  gl_FragColor = color;
}