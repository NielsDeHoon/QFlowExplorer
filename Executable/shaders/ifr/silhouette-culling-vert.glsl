// silhouette-culling-vert.glsl
#version 330

uniform mat4 matrixModelViewProjection;

in  vec3 qfe_Vertex;
in  vec3 qfe_Normal;

out vec3 vertexPosition;
out vec3 vertexNormal;

//----------------------------------------------------------------------------
void main()
{  
  vertexNormal   = qfe_Normal;   
  
  vertexPosition = (matrixModelViewProjection * vec4(qfe_Vertex,1.0)).xyz;
}