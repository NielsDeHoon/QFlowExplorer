uniform sampler3D volumeData;

uniform int       planeData;
uniform vec3      planeNormal;
uniform vec3      planeVector;

//----------------------------------------------------------------------------
void main()
{
  vec4 sample_coord = gl_TexCoord[0];
  vec4 sample_color = texture3D(volumeData, sample_coord.xyz);
  vec4 color        = sample_color;
  
  sample_color.a = 1.0;
  
  if(sample_coord.x < 0.0 || sample_coord.x > 1.0 ||
     sample_coord.y < 0.0 || sample_coord.y > 1.0 ||
     sample_coord.z < 0.0 || sample_coord.z > 1.0)
     discard;
    
  vec3  c;
  float val   = 0.3;
  float angle = 0.0;
  
  // pca-p magnitude
  if(planeData == 0)
  {
    val   = sqrt(pow(sample_color.r,2.0)+pow(sample_color.g,2.0)+pow(sample_color.b,2.0))/200.0;
    color = vec4(val, val, val, sample_color.a);
  }
  
  // pca-p x
  if(planeData == 1)
  { 
    val = ((sample_color.r+200.0)/400.0)*1.5;
    color = vec4(val, val, val, sample_color.a);
  }
  
  // pca-p y
  if(planeData == 2)
  {
    val = ((sample_color.g+200.0)/400.0)*1.5;
    color = vec4(val, val, val, sample_color.a);
  }
  
  // pca-p z
  if(planeData == 3) 
  {
    val = ((sample_color.b+200.0)/400.0)*1.5;  
    color = vec4(val, val, val, sample_color.a);
  }
  
  // color coding
  if(planeData == 4 || planeData == 5)
  {
    c     = normalize(sample_color).xyz;
    angle = dot(planeNormal, sample_color.xyz);
    
    if(angle > 0.0)
    {
      color.r = (angle) / 200.0;
      color.b = 0.0;
    }
    else
    {
      color.r = 0.0;    
      color.b = -(angle / 200.0);
    }
    
    color.g = 0.0;
    
    if(planeData == 5)
    {
      color.g = dot(planeVector, sample_color.xyz) / 200.0;    
    }
    
    color.a = sample_color.a;
  }  
 
  gl_FragColor = color;
  
}