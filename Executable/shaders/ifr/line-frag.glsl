// line-velocity-frag
#version 330

uniform sampler1D lineTexture;

uniform int lineShadingMode;

uniform vec4  lightSpec;
uniform vec4  lightDirection;
uniform float ambientContribution;
uniform float diffuseContribution;
uniform float specularContribution;
uniform float specularPower;

uniform mat4  matrixModelView;
uniform mat4  matrixModelViewInverse;

in vec4  lineCol;
in float lineTexCoord;
in vec3  lineTangentNorm;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
float diffuse(  in vec3 l, in vec3 t) 
{
  float ldott = dot(l, t);
  float ndotl = sqrt(1.0-pow(ldott, 2.0));

  return ndotl; 
}

//----------------------------------------------------------------------------
float specular( in vec3 v, in vec3 l, in vec3 t, in float p) 
{

  float ldott = dot(l, t);
  float vdott = dot(v, t);
  float ndotl = sqrt(1.0-pow(ldott, 2.0));
  float rdotv = max(0.0, ndotl * sqrt(1.0-pow(vdott, 2.0)) - ldott*vdott);

  float pf;

  if (ndotl < 0.01)
    pf = 0.0;
  else
    pf = pow(rdotv, p);

  return pf;
}

//----------------------------------------------------------------------------
void main()
{
  vec4 color;
  vec4 e, l;
  
  e = normalize( matrixModelViewInverse*vec4(0.0,0.0,1.0,0.0));   
  l = vec4(normalize( matrixModelView * -lightDirection).xyz, 0.0); 
  
  float d = diffuse(l.xyz, lineTangentNorm);
  float s = specular(e.xyz, l.xyz, lineTangentNorm, lightSpec.w);
  
  if(lineShadingMode == 1)
    color = (lightSpec.x + lightSpec.y*d)*lineCol + vec4(lightSpec.z)*s;
  else 
    color = lineCol;
    
  qfe_FragColor = color;  
}