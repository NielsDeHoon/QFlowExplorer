// arrow-frag
#version 330

in  vec4 color;

out vec4 qfe_FragColor;

void main()
{
  qfe_FragColor = color;

}