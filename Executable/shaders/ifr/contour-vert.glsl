// contour-vert.glsl
varying out vec3  normal;
varying out vec3  pdir1;
varying out vec3  pdir2;
varying out float curv1;
varying out float curv2;

//----------------------------------------------------------------------------
void main()
{  
  normal   = gl_NormalMatrix  * gl_Normal;   
  pdir1    = gl_MultiTexCoord1.stp;
  pdir2    = gl_MultiTexCoord2.stp;
  curv1    = gl_MultiTexCoord3.s;
  curv2    = gl_MultiTexCoord3.t;
  
  gl_FrontColor = gl_Color;
  
  gl_Position = ftransform();
}