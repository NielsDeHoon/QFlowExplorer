#version 120
#extension GL_EXT_geometry_shader4 : enable

varying in vec3  normal[3];
varying in vec3  pdir1[3];
varying in vec3  pdir2[3];
varying in float curv1[3];
varying in float curv2[3];

varying out float t_kr;
varying out float t_ndote;

//----------------------------------------------------------------------------
float NormalizeRange(float value, vec2 range)
{
  float result;

  result = (value - range.x) * ( 1.0 / (range.y - range.x) );
  
  return result;
}

//----------------------------------------------------------------------------
void main()
{
  float ndote[3], kr[3];
  vec3  e;
  
  e      = vec3(0.0,0.0,1.0);

  // Determine ndote and kr per vertex
  for(int i = 0; i < gl_VerticesIn; ++i)
  {
    vec3  w  = normalize(e - normal[i] * dot(e, normal[i]));
    float u  = dot(w, pdir1[i]);
    float v  = dot(w, pdir2[i]);
    float u2 = u*u;
    float v2 = v*v;
    
    ndote[i] = dot(normal[i],e);
    kr[i]  = (curv1[i]*u2) + (curv2[i]*v2);
      
  }  
  
  //if( (ndote[0] >= 0.0) || (ndote[1] >= 0.0) || (ndote[2] >= 0.0) )
  {
    vec4 color;
    
    for(int i = 0; i < gl_VerticesIn; ++i)
    {
      t_ndote = ndote[i];
      t_kr    = kr[i];
      
      color   = gl_FrontColorIn[i];
      
      if(ndote[0] < 0.0)
        color.a = 0.3;      
      
      gl_FrontColor = gl_FrontColorIn[i];     
      gl_Position   = gl_PositionIn[i];
    
      EmitVertex();
    }  
  }
}