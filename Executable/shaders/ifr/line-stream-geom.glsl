// line-stream-geom
#version 330

uniform sampler3D volumeData;
uniform ivec3     volumeDimensions;

uniform sampler1D transferFunction;

uniform mat4      matrixVoxelPatient;
uniform mat4      matrixPatientVoxel;
uniform mat4      matrixModelViewProjection;

uniform float     traceStep;
uniform int       traceIterations;
uniform int       traceCounter;   // for animated highlight
uniform int       traceDirection; //[-1,1]

uniform vec3      lineColor;
uniform int       lineColorMode;

layout(points) in;
layout(line_strip, max_vertices=90) out;

in  vec3  seedPatientCoord[1];
out vec4  lineCol;
out float lineTexCoord;
out vec3  lineTangentNorm;

//----------------------------------------------------------------------------
vec3 transPatientToVoxel(vec3 p)
{
  return (matrixPatientVoxel * vec4(p,1.0)).xyz;
}

//----------------------------------------------------------------------------
// seed   in patient coordinates
// step   in patient coordinates (mm)
// dir    
// volume texture
// returns new point in patient coordinates
vec4 computeRungeKuttaSpatial(vec3 seed, float step, int dir, sampler3D volume)
{
  vec3  vi, vi_1, vi_2, vi_3;
  vec3  s;
  float stepSize;
  
  if(dir >= 0) dir = 1;
  else         dir = -1;
  
  // Phase image velocities are defined in cm/s
  // Step in spatial domain is defined in mm
  stepSize = step/10.0;

  // Get the velocity (requires texture coordinates), returns velocity in patient coordinates
  vi      = dir * texture(volume, transPatientToVoxel(seed) / vec3(volumeDimensions)).xyz;

  vec3 k1 = vi   * stepSize;  
  vec3 p1 = seed + k1*0.5;  

  vi_1    = dir * texture(volume, transPatientToVoxel(p1) / vec3(volumeDimensions)).xyz;

  vec3 k2 = vi_1 * stepSize;
  vec3 p2 = seed + k2*0.5;
  
  vi_2    = dir * texture(volume, transPatientToVoxel(p2) / vec3(volumeDimensions)).xyz;

  vec3 k3 = vi_2 * stepSize;
  vec3 p3 = seed + k3;
  
  vi_3    = dir * texture(volume, transPatientToVoxel(p3) / vec3(volumeDimensions)).xyz; 

  vec3 k4 = vi_3 * stepSize;

  // New position in patient coordinates
  s     = seed + (k1 + k2*2.0 + k3*2.0 + k4)*(1.0/6.0);
  
  return vec4(s, 1.0);
}

//----------------------------------------------------------------------------
// seed in patient coordinates
// tf is a 1D transfer function
vec4 getColorBySpeed(vec3 seed, sampler1D tf)
{
  float speed;
  vec3  texcoord;
  vec4  velocity, color;
  
  texcoord = transPatientToVoxel(seed) / vec3(volumeDimensions);
  velocity = texture(volumeData, texcoord); 
  speed    = length(velocity.xyz);
  color    = texture(tf, speed/200.0); 
  
  return color;
}

//----------------------------------------------------------------------------
// index in the linestrip
// tf is a 1D transfer function
vec4 getColorByPropagation(int index, sampler1D tf)
{
  float time;
  vec4  color;
  
  time   = index / float(traceIterations);
  color  = texture(tf, time); 
  
  return color;
}

//----------------------------------------------------------------------------
// seed in patient coordinates
// tf is a 1D transfer function
vec4 getColorByDistanceToVoxel(vec3 seed, sampler1D tf)
{
  float distance;
  vec3  voxelcoord;
  ivec3 voxelpos[8];
  vec4  velocity, color;
  
  voxelcoord  = transPatientToVoxel(seed);
  voxelpos[0] = ivec3(voxelcoord) + ivec3(0,0,0);
  voxelpos[1] = ivec3(voxelcoord) + ivec3(1,0,0);
  voxelpos[2] = ivec3(voxelcoord) + ivec3(1,1,0);
  voxelpos[3] = ivec3(voxelcoord) + ivec3(0,1,0);
  voxelpos[4] = ivec3(voxelcoord) + ivec3(0,0,1);
  voxelpos[5] = ivec3(voxelcoord) + ivec3(1,0,1);
  voxelpos[6] = ivec3(voxelcoord) + ivec3(1,1,1);
  voxelpos[7] = ivec3(voxelcoord) + ivec3(0,1,1);
  
  
  distance = 2.0;
  for(int i=0; i<8; i++)
  {
    float current = length(voxelcoord - vec3(voxelpos[i]));
    if(current < distance) distance = current;
  }
  
  color    = texture(tf, distance*2.0); 
  
  return color;
}

//----------------------------------------------------------------------------
// seed in patient coordinates
// index is the line strip [0, n]
// tf is a 1D transfer function
vec4 getColor(vec3 seed, int index, sampler1D tf)
{
  vec4 color = vec4(0.8,0.0,0.0,1.0);
  
  if(lineColorMode == 0) color = getColorBySpeed(seed, tf);
  if(lineColorMode == 1) color = getColorByPropagation(index, tf);
  if(lineColorMode == 2) color = getColorByDistanceToVoxel(seed, tf);
  if(lineColorMode == 3) color = vec4(lineColor,1.0);
  
  return color;
}

//----------------------------------------------------------------------------
void main(void)
{   
  int  i, j, highlightCounter;
  vec4 nextPos, currPos;
  vec4 highlightColor = vec4(0.9,0.9,0.9,1.0);
  
  currPos         = vec4(seedPatientCoord[0],1.0);
  lineTangentNorm = normalize(computeRungeKuttaSpatial(currPos.xyz, traceStep, traceDirection, volumeData) - currPos).xyz;
  lineCol         = getColor(currPos.xyz, 0, transferFunction); 
  gl_Position     = matrixModelViewProjection*currPos;
  EmitVertex();

  nextPos = vec4(seedPatientCoord[0],1.0);
  for(int j=1; j<=traceIterations; j++)  
  { 
    lineCol = getColor(nextPos.xyz, j, transferFunction); 
        
    if(traceDirection < 0 ) highlightCounter = traceIterations - (traceCounter - 1);    
    else                    highlightCounter = traceCounter    - 1;
    
    if(j == highlightCounter ) lineCol.xyz = highlightColor.xyz;           
    
    nextPos         = computeRungeKuttaSpatial(nextPos.xyz, traceStep, traceDirection, volumeData);     
    lineTangentNorm = normalize(nextPos - currPos).xyz;
    currPos         = nextPos;
    gl_Position     = matrixModelViewProjection*currPos;
    
    EmitVertex();    
  }
  
  EndPrimitive();  
}
 