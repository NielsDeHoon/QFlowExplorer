#version 330

uniform sampler1D particleColorMap;
uniform sampler2D particleNormalMap;
uniform sampler1D particleTransferFunction;
uniform mat4      modelView;
uniform mat4      modelViewInverse;
uniform mat4      modelViewProjection;
uniform mat4      modelViewProjectionInverse;
uniform vec4      light;
uniform vec4      lightDirection;
uniform vec2      dataRange;
uniform int       phaseCount;
uniform int       setCount;
uniform int       colorScale;
uniform int       shading;
uniform int       contour;
uniform int       colorType;

in imposterAttrib
{ 
  vec2 texcoord;
  vec4 attribs;
  mat2 rotation;
} imposter;

out vec4 fragment;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 PhongMultiplicative(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 Cel(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
void main()
{  
  float speed, vorticity, colorIndex, seedTime, index;  
  vec3  color;
  vec4  normal, final;
  vec3  lightDir;
  vec3  eye;
  float ndotv;
  float contourThres;
  float fuzz, frac1, frac2;
  
  fuzz         = 0.05;
  contourThres = 0.2;  
  color        = vec3(0.0);  
  final        = vec4(0.0);
  
  // If the particle is dead, discard the fragment
  if(imposter.attribs.x <= -1.0) discard;
  
  // Note: we solve the illumination in screen space, because of the normal map
  //       for the ellipsoids we need to rotate the in-plane components of the normals
  eye       = normalize(vec4(0.0,0.0,1.0,0.0)).xyz;
  normal    = normalize(texture(particleNormalMap, imposter.texcoord.xy));  
  normal.xy = imposter.rotation*normal.xy;
  lightDir  = normalize(-lightDirection).xyz;
  ndotv     = dot(normal.xyz, eye);
  
  // If the normal is undefined, do not render
  if(normal.a == 0.0) discard;   
    
  // If particle is alive, render in color
  if(imposter.attribs.x > 0.0) 
  {      
    color = vec3(0.3);
  
    // Scale the speed before the color lookup and normalize;    
    if(colorType == 0)
    {
      speed     = imposter.attribs.y / max(abs(dataRange.x), abs(dataRange.y));
      speed     = speed * (float(colorScale)/100.0);    
                
      index     = speed;          
      color     = texture(particleTransferFunction, index).rgb; 
    }
    else
    if(colorType == 1)
    {   
      vorticity = imposter.attribs.z / max(abs(dataRange.x), abs(dataRange.y));
      vorticity = vorticity * (float(colorScale)/100.0);    
                
      index     = vorticity;          
      color     = texture(particleTransferFunction, index).rgb; 
    }
    else
    if(colorType == 2)
    {      
      colorIndex = imposter.attribs.z / float(setCount-1);
      
      index     = colorIndex;
      color     = texture(particleColorMap, index).rgb; 
    }
    else    // seed time
    {
      seedTime  = imposter.attribs.w / float(phaseCount);
          
      index     = seedTime;          
      color     = texture(particleTransferFunction, index).rgb; 
    }          

    // apply shading
    if(shading == 1)
      final = PhongMultiplicative(vec4(color,1.0), lightDir, normal.xyz, eye, light.x, light.y, vec2(light.z, light.w));    
    else
    if(shading == 2)
      final = Phong(vec4(color,1.0), lightDir, normal.xyz, eye, light.x, light.y, vec2(light.z, light.w));    
    else
    if(shading == 3)
      final = vec4(color,1.0);
    else    
      final = Cel(vec4(color,1.0), lightDir, normal.xyz);    

    // apply contour
    frac1 = smoothstep(contourThres+fuzz,contourThres-fuzz, ndotv);
    frac2 = smoothstep(0.0+fuzz,0.0-fuzz, ndotv);
    if((contour == 1) && (ndotv < contourThres))
    {
      if(ndotv < 0.5*contourThres)
      {
        final.xyz = mix(vec3(0.0), vec3(0.2), frac2);  
        if(final.x > 0.1) discard;
      }
      else
        final.xyz = mix(final.xyz, vec3(0.0), frac1);  
    }
  }
  else   
  {
    discard;
  }
  
  fragment         = final;
  //vec4 col = modelViewInverse
  //fragment = vec4( 0.0,1.0);
}