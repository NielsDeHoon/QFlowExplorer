// ptr-frag.glsl

in vec4 attributes;

//----------------------------------------------------------------------------
void main()
{  
  //vec4 color = vec4(attributes.xyz, 1.0);
  vec4 color = vec4(0.0,0.0,1.0, 1.0);
  
  // If particle is dead, do not render
  if(attributes.x < 0.0)
  {    
    color.r = 1.0;
    color.b = 0.0;
  }
  
  gl_FragColor = vec4(color);
}