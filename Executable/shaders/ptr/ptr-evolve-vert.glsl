#version 330

uniform mat4  voxelPatientMatrix;
uniform mat4  patientVoxelMatrix;
uniform ivec3 volumeDimensions;

uniform float phaseCurrent;
uniform float particleDeathAge;

in vec4 qfe_ParticlePosition;
in vec4 qfe_ParticleAttribute;
in vec4 qfe_ParticlePositionBirth;

out vec4 particlePos;
out vec4 particleAttribs;

// attribs.x - age in phases (-1 = dead)
// attribs.y - current velocity (set during advection)
// attribs.z - color index
// attribs.w - birth age

//----------------------------------------------------------------------------
// position - in patient coordinates
bool insideBox(in vec3 position, out vec3 clamped)
{  
  vec3 pos;
  bool inside;
  
  pos = (patientVoxelMatrix * vec4(position,1.0)).xyz;
  
  if(all(greaterThan(pos, vec3(0.0))) && all(lessThan(pos, volumeDimensions)))
  {
    clamped = position; 
    inside  = true;
  }
  else
  {
    clamped = (voxelPatientMatrix * vec4(clamp(pos, vec3(0.0), volumeDimensions), 1.0)).xyz;
    inside  = false;
  }  
  
  return inside;
}

//----------------------------------------------------------------------------
void main(void)
{     
   vec4 position;
   vec4 attribs;  
   vec4 positionClamp;
   
   position      = qfe_ParticlePosition;   
   attribs       = qfe_ParticleAttribute;   
   positionClamp = qfe_ParticlePosition;
      
   // Particle death
   if(attribs.x >= particleDeathAge)   
   {         
     position  = vec4(qfe_ParticlePositionBirth.xyz,attribs.w);
     attribs.x = 0.0;     
   }
   
   // Particle sacrifice
   if((attribs.x >= 0.0) && (insideBox(position.xyz, positionClamp.xyz) == false))
   {
     position.xyz = positionClamp.xyz;
     attribs.x    = -4.0;     
   }  
   
   // Particle reincarnation
   if((attribs.x == -2.0) && (phaseCurrent == attribs.w))
   {
     position  = vec4(qfe_ParticlePositionBirth.xyz,attribs.w);
     attribs.x = 0.0;     
   }
      
   // Transform feedback the new values
   particlePos       = position;   
   particleAttribs   = attribs;
    
}
