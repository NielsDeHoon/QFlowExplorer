#version 330

uniform sampler3D volumeData0; 
uniform sampler3D volumeData1; 
uniform sampler3D volumeData2;

uniform mat4  modelView;
uniform mat4  projection;
uniform mat4  modelViewProjection;
uniform mat4  modelViewProjectionInverse;
uniform mat4  patientVoxelMatrix;

uniform ivec3 volumeDimensions;
uniform float phaseCurrent;       // current phase at start of integration
uniform float trailStepSize;      // step size in phases
uniform float trailStepDuration;  // duration of a full phase in ms
uniform int   traceDirection;
uniform int   particleSize;
uniform int   colorScale;
uniform vec2  dataRange;
uniform int   trailType;

layout(points)    	                in;
layout(triangle_strip, max_vertices=20) out;

in particleAttrib
{
  vec4 position;
  vec4 attribs;
  
} particle[];

out trailAttrib
{
  vec4 attribs;
  vec2 texcoord;
} trail;

//----------------------------------------------------------------------------
vec3 TransformPosition(vec3 pos, mat4 matrix);

//----------------------------------------------------------------------------
vec3 FetchVelocityTemporalLinear(sampler3D vol1, sampler3D vol2, vec3 pos, float posTime, mat4 v2t);

//----------------------------------------------------------------------------
vec4 IntegrateRungeKutta1(sampler3D d0,sampler3D d1,sampler3D d2,sampler3D d3,sampler3D d4, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t);

//----------------------------------------------------------------------------
void main(void)
{    
  const vec2  trailOffset = vec2(0.66,1.0);
  const int   trailCount        = 5;
  const int   velocityThreshold = 10;
  
  const float deltaLineHalo =  0.001;
  const float deltaMax      =  0.01;
    
  float particleTime;
  vec4  particlePatient, pos1, pos2, pos3;  
  vec3  velocity;
  
  mat4  p2v, v2t;
  
  float trailWidth;
  float trailWeight;
  float haloWidth; 

  vec3  trail1[trailCount],   trail2[trailCount];
  vec3  strip1[2*trailCount], strip2[2*trailCount];
  vec3  tangent, binormal, eye, eyePatient;  
  
  trail.attribs  = particle[0].attribs;
  trail.texcoord = vec2(0.0);
   
  // compute the transformation matrices
  p2v = patientVoxelMatrix;
  v2t = mat4(1.0/float(volumeDimensions[0]), 0.0, 0.0,0.0,
             0.0, 1.0/float(volumeDimensions[1]), 0.0, 0.0,
             0.0, 0.0, 1.0/float(volumeDimensions[2]), 0.0,
             0.0, 0.0, 0.0, 1.0);
  
  // compute the eye vector
  eyePatient = normalize(modelViewProjectionInverse * vec4(0.0,0.0,1.0,0.0)).xyz;
  eye        = vec3(0.0,0.0,1.0);
  
  //gl_ModelViewMatrix
             
  // get the particle information
  particlePatient = vec4(particle[0].position.xyz, 1.0);
  particleTime    = particle[0].position.w;    
  velocity        = FetchVelocityTemporalLinear(volumeData1, volumeData2, TransformPosition(particlePatient.xyz, p2v), particleTime, v2t);  
  
  // compute the velocity dependent width
  //trailWeight   = 1.0 - (length(velocity)*(float(colorScale)/100.0) / max(abs(dataRange.x),abs(dataRange.y)));    
  //trailWidth    = (1.0 - trailWeight) * 0.1 + trailWeight * 0.8;       
  //haloWidth     = 1.0;       
  trailWidth      = 0.4;
  
  // compute the two start positions
  tangent         = normalize(-1.0*velocity);
  binormal        = normalize(cross(eyePatient, tangent));
  pos1            = particle[0].position + trailOffset.x*(0.5*float(particleSize))*vec4(binormal,0.0) + float(traceDirection)*(trailOffset.y*float(particleSize))*vec4(tangent,0.0);
  pos2            = particle[0].position - trailOffset.x*(0.5*float(particleSize))*vec4(binormal,0.0) + float(traceDirection)*(trailOffset.y*float(particleSize))*vec4(tangent,0.0);  
  pos3            = particle[0].position + (trailOffset.y*float(particleSize))*vec4(tangent,0.0);  
    
  // If the particle is alive, start the trail after his birth
  if((trail.attribs.x > 0) && (length(velocity) >= velocityThreshold))
  {
    // perform the trace integration
    trail1[0] = (modelView * vec4(pos1.xyz,1.0)).xyz;      
    trail2[0] = (modelView * vec4(pos2.xyz,1.0)).xyz;      
    
    for(int i=1; i<trailCount; i++)
    {    
      pos1 = IntegrateRungeKutta1(volumeData0, volumeData0, volumeData1, volumeData2, volumeData2, pos1, phaseCurrent, trailStepSize, trailStepDuration, -1*traceDirection, 100, p2v, v2t);    
      pos2 = IntegrateRungeKutta1(volumeData0, volumeData0, volumeData1, volumeData2, volumeData2, pos2, phaseCurrent, trailStepSize, trailStepDuration, -1*traceDirection, 100, p2v, v2t);              
      
      trail1[i] = (modelView * vec4(pos1.xyz,1.0)).xyz;  
      trail2[i] = (modelView * vec4(pos2.xyz,1.0)).xyz;        
    }             
    
    // compute strip start    
    tangent    = normalize(trail1[0] - (modelView*vec4(particlePatient.xyz,1.0)).xyz);
    binormal   = normalize(cross(eye, tangent));    
    strip1[0]  = trail1[0]+0.5*trailWidth*binormal;
    strip1[1]  = trail1[0]-0.5*trailWidth*binormal;
    
    tangent    = normalize(trail2[0] - (modelView*vec4(particlePatient.xyz,1.0)).xyz);
    binormal   = normalize(cross(eye, tangent));    
    strip2[0]  = trail2[0]+0.5*trailWidth*binormal;
    strip2[1]  = trail2[0]-0.5*trailWidth*binormal;
        
    // compute the strips
    for(int i=1; i<trailCount-1; i++)
    {       
      tangent   = normalize(trail1[i+1] - trail1[i]);
      binormal  = normalize(cross(eye, tangent));
      
      strip1[2*i+0] = trail1[i]+0.5*trailWidth*binormal;
      strip1[2*i+1] = trail1[i]-0.5*trailWidth*binormal;  
      
      tangent   = normalize(trail2[i+1] - trail2[i]);
      binormal  = normalize(cross(eye, tangent));
      
      strip2[2*i+0] = trail2[i]+0.5*trailWidth*binormal;
      strip2[2*i+1] = trail2[i]-0.5*trailWidth*binormal;  
    }  
    
    // compute the end       
    tangent   = normalize(trail1[trailCount-1] - trail1[trailCount-2]);
    binormal  = normalize(cross(eye, tangent));
    strip1[(2*trailCount)-2+0] = trail1[trailCount-1]+0.5*trailWidth*binormal;
    strip1[(2*trailCount)-2+1] = trail1[trailCount-1]-0.5*trailWidth*binormal;
    
    tangent   = normalize(trail2[trailCount-1] - trail2[trailCount-2]);
    binormal  = normalize(cross(eye, tangent));
    strip2[2*(trailCount-1)+0] = trail2[trailCount-1]+0.5*trailWidth*binormal;
    strip2[2*(trailCount-1)+1] = trail2[trailCount-1]-0.5*trailWidth*binormal;    
            
    // render the first strip
    for(int i=0; i<trailCount; i++)
    {
      trail.texcoord = vec2(float(i)/float(trailCount-1.0), 0.0);      
      gl_Position    = projection * vec4(strip1[2*i+0],1.0);
      EmitVertex();        
      
      trail.texcoord = vec2(float(i)/float(trailCount-1.0), 1.0);      
      gl_Position    = projection * vec4(strip1[2*i+1],1.0);
      EmitVertex();        
    }    
    EndPrimitive();
    
    // render the second strip
    for(int i=0; i<trailCount; i++)
    {
      trail.texcoord = vec2(float(i)/float(trailCount-1.0), 0.0);
      gl_Position    = projection * vec4(strip2[2*i+0],1.0);
      EmitVertex();        
      
      trail.texcoord = vec2(float(i)/float(trailCount-1.0), 1.0);
      gl_Position    = projection * vec4(strip2[2*i+1],1.0);
      EmitVertex();        
    }    
    EndPrimitive();          
  
  }  
}
