#version 330

uniform vec4      light;
uniform vec4      lightDirection;

in trailAttrib
{
  vec4 attribs;
  vec2 texcoord;  
} trail;

out vec4 fragment;

//----------------------------------------------------------------------------
void main()
{  
  vec4  color;
  float s;
  
  float stripWidth = 1.2;
  float lineWidth  = 0.6;
  
  float taperStart  = 0.8;
  float taperFactor = 1.0;  
  float r, frac1, frac2, fuzz;
  
  // initialize
  color = vec4(vec3(0.1),1.0);  
  fuzz  = 0.1;  
  
  // determine the tapering
  r           = (0.0-1.0)/(1.0-taperStart);
  taperFactor = (trail.texcoord.x-taperStart)*r + 1.0;
  taperFactor = clamp(taperFactor, 0.0, 1.0);  
  
  // determine distance to center
  s     = stripWidth*abs(trail.texcoord.y-0.5);
  
  // determine strip smoothing (Hermite polynomial)
  frac1 = smoothstep(taperFactor*lineWidth -fuzz,taperFactor*lineWidth,  2.0*s);
  frac2 = smoothstep(taperFactor*stripWidth-fuzz,taperFactor*stripWidth, 2.0*s);
          
  // If particle is alive, render in color
  if(trail.attribs.x >= 0.0 && (s <= 0.5*taperFactor*stripWidth)) 
  {      
    if(s < 0.5*taperFactor*lineWidth)      
      color      = mix(vec4(vec3(0.0),1.0), vec4(1.0), frac1);   
    else
      color      = mix(vec4(1.0), vec4(vec3(0.8),1.0), frac2);   
  } 
  else discard;
  
  fragment         = vec4(color);
}