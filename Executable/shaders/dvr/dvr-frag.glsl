// dvr-frag.glsl
#version 120

uniform sampler3D dvrDataSet1;
uniform sampler3D dvrDataSet2;
uniform vec2      dvrDataRange;
uniform int       dvrDataComponent;
uniform sampler1D transferFunction;
uniform sampler1D transferGradient;
uniform sampler2D rayEnd;
uniform sampler2D rayEndDepth;
uniform sampler2D intersectColor;
uniform sampler2D intersectDepth;
uniform int       intersectAvailable;
uniform sampler2D clippingFrontDepth;
uniform sampler2D clippingBackDepth;
uniform vec4      clippingPlane;
uniform int       clippingAvailable;
uniform vec3      scaling;   // dimensions * voxel size (mm)
uniform float     stepSize;  // minimum of voxel size (mm)
uniform int       mode; 
uniform int       gradient;
uniform int       shading;
uniform float     gamma;
uniform float     quality;
uniform vec4      light;
uniform vec4      lightDirection;
uniform int       style;

uniform mat4      voxelPatientMatrix;

// This shader only deals with scalar data
// For vector data is will use the magnitudes (alpha component)
const   int       SCALARS  = 0;
const   int       VECTOR   = 1;

varying vec4 posTexture;
varying vec4 posScreen;

//----------------------------------------------------------------------------
vec3 FetchVectorGradient(sampler3D vol, vec3 pos, vec3 step);

//----------------------------------------------------------------------------
vec3 NormalizeRange(vec3 value, vec2 range);

//----------------------------------------------------------------------------
vec4 CompositeScalarFrontToBack(vec4 src, vec4 dst);

//----------------------------------------------------------------------------
vec4 CompositeScalarFrontToBackMIDA(vec4 src, vec4 dst, float beta);

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 Cel(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec4 Glass(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in vec2 s) ;

//----------------------------------------------------------------------------
void main()
{
  vec4  dataSample;
  float dataValue; 
  vec3  dataGradient;
  vec2  dataRange;    
  float normalizedValue;
  vec3  classifiedValue;
  float maximumValue;
  float delta;
  float beta;
  float stepSizeLocal;  
  
  vec3  ray;  
  vec3  rayVector;
  vec3  rayDir;
  float rayMaxLength; 
  float rayOffset;
  
  vec3  lightDir;
  
  float depthFront;
  float depthBack;
  float intersectOffset;
  float clipFrontOffset;
  float clipBackOffset;
  
  vec4  src;
  vec4  dst;
  float gradientOpacity;
  float ndotv;
  float contour;
  vec4  contourColor;
  
  vec3  intersectC;
  float intersectD;
  float clipFrontD;
  float clipBackD;
  vec4  intersectPos;
  vec4  clipFrontPos;
  vec4  clipBackPos;
  vec4  colmax;
  vec4  color;  
  
  // Initialize gradient
  dataGradient = vec3(0.0);
  
  // Initialize src and dst
  src = vec4(0.0,0.0,0.0,0.0);
  dst = vec4(0.0,0.0,0.0,0.0);
  
  stepSizeLocal = stepSize / quality;  
  
  // Initialize the data range
  dataRange.xy = dvrDataRange.xy;
  if(mode == VECTOR && dvrDataComponent == 0) 
    dataRange = vec2(0.0, max(abs(dvrDataRange.x),abs(dvrDataRange.y))); 
  
  // Compute normalized device coordinate
  vec2 texCoord =  ((posScreen.xy / posScreen.w) + 1.0) / 2.0;
  
  //--------------------------
  // Ray Setup
  //--------------------------
  // Determine color and depth for geometry intersection and clipping
  intersectC      = vec3(1.0);
  intersectD      = 1.0;
  clipFrontD      = 0.0;
  clipBackD       = 1.0;
  clipFrontOffset = 0.0;
  clipBackOffset  = 0.0;
  intersectOffset = 0.0;
              
  // Determine start and end of rays  
  vec3 start      = posTexture.rgb;
  vec3 end        = texture2D(rayEnd, texCoord.xy).rgb;
    
  start           = clamp(start, 0.0, 1.0) * scaling;
  end             = clamp(end,   0.0, 1.0) * scaling;
  
  depthFront      = gl_FragCoord.z;
  depthBack       = texture2D(rayEndDepth, texCoord.xy).r;    
  
  //--------------------------
  // Clipping of convex opaque geometry
  //--------------------------
  if(clippingAvailable == 1)  
  {
    clipFrontD      = texture2D(clippingFrontDepth, texCoord.xy).r;  
    clipBackD       = texture2D(clippingBackDepth,  texCoord.xy).r;  
   
    clipFrontOffset = abs(clamp(clipFrontD, depthFront, depthBack) - depthFront)/abs(depthBack-depthFront);
    clipBackOffset  = abs(clamp(clipBackD,  depthFront, depthBack) - depthFront)/abs(depthBack-depthFront);
    
    clipFrontPos    = vec4(start + (clipFrontOffset * (end - start)),1.0);
    clipBackPos     = vec4(start + (clipBackOffset  * (end - start)),1.0);
    
    // Determine if there is front clipping
    if(clipFrontD != 1.0)  end   = clipFrontPos.xyz;
    else
    if(clipBackD  != 1.0)  start = clipBackPos.xyz; 
  }  
  
  //--------------------------
  // Intersection with opaque geometry
  //--------------------------
  if(intersectAvailable == 1)
  {
    intersectC      = texture2D(intersectColor, texCoord.xy).rgb;
    intersectD      = texture2D(intersectDepth, texCoord.xy).r;
    
    intersectOffset = abs(clamp(intersectD, depthFront, depthBack) - depthFront)/abs(depthBack-depthFront);
      
    intersectPos    = vec4(start + (intersectOffset * (end - start)),1.0);
            
    // Determine if ray hit opaque geometry        
    // Be aware of possible clipping geometry
    if(intersectD != 1.0)
    {   
      end = intersectPos.xyz;  
      
      if((intersectOffset < clipBackOffset) && (clipBackOffset < 1.0) && (clipFrontOffset != clipBackOffset))      
        dst = vec4(intersectC, 1.0);        
      
      if((intersectOffset > clipFrontOffset) && (clipFrontOffset < 1.0) && (clipFrontOffset != clipBackOffset))      
        dst = vec4(intersectC, 1.0);        
    }     
  }    
  
  ray          = start;    
  rayVector    = end - start;
  rayDir       = normalize(rayVector);
  rayMaxLength = length(rayVector);  
  rayOffset    = 0.0;
  maximumValue = dataRange.x;
  
  // If there is no ray, discard the fragment
  if(rayMaxLength <= 0.01) discard;
  
  float aView;
  
  //--------------------------
  // Ray Propagation
  //--------------------------
  int i;
  for(i=0; i <= int(rayMaxLength/stepSizeLocal); i++)
  {    
    // Early ray termination
    // Determine if ray is within bounding box 
    // Determine if ray is fully opaque
    if(rayOffset >= rayMaxLength)
    {
      break;
    } 
    
    if((gamma == -1.0) && (dst.a >= 1.0))
    {
      break;
    }
    
    //--------------------------
    // Sampling
    //--------------------------
    dataSample      = texture3D(dvrDataSet1, ray / scaling);       
       
    dataValue       = length(dataSample.rgb);    
    
    normalizedValue = NormalizeRange(vec3(dataValue), dataRange).r;    
    
    // parameters for MIDA
    delta = 0.0;
    if(abs(normalizedValue) > maximumValue)
    {
      delta = abs(normalizedValue) - maximumValue;
      maximumValue = abs(normalizedValue);    
    }    
    if(gamma < 0.0)
    {
      delta = delta * (1.0 + gamma);               
    }
    beta = 1.0 - delta;  

    //--------------------------
    // Classification
    //--------------------------
    // Post-interpolative transfer function
    src             = texture1D(transferFunction, normalizedValue);        
    
    //--------------------------
    // Gradient Information
    //--------------------------    
    if(((shading==1) || (gradient == 1)) && (src.a > 0.01))
    {
      // we use the gradient / normal in voxel coordinates
      // this reduces matrix computations towards patient coordinates          
      dataGradient   = NormalizeRange(FetchVectorGradient(dvrDataSet1, ray/scaling, vec3(1.0)/scaling), dataRange);                
      dataGradient   = (-1.0 * vec4(dataGradient,0.0)).xyz;   
    }      
    
    //--------------------------
    // Gradient Rendering
    //--------------------------
    if((gradient == 1) && (src.a > 0.01))
    {      
      gradientOpacity = texture1D(transferGradient, length(dataGradient)).a;             
      src.a = min(src.a, gradientOpacity);
    }
        
    //--------------------------
    // Shading
    //--------------------------        
    if((shading==1) && (src.a > 0.01))
    {       
      lightDir       = normalize(gl_ModelViewMatrixInverse * -lightDirection).xyz;      
      ndotv          = dot(normalize(dataGradient), -rayDir);
      contour        = length(dataGradient) * pow((1.0 - ndotv), 32.0); 
      contourColor   = vec4(0.0,0.0,0.0,1.0);
            
      if(style == 1)
        src            = Cel(src, lightDir, normalize(dataGradient));        
      else if(style == 2)
        src            = Glass(vec4(0.5,0.5,0.6,src.a), normalize(lightDir), normalize(dataGradient), -rayDir, vec2(2.0, 1.0));   
      else
        src            = Phong(src, lightDir, normalize(dataGradient), -rayDir, light.x, light.y, vec2(light.z, light.w));      
                  
      aView = (2.0/3.14 * acos(ndotv));  
      //src.xyz = vec3(aView);
      //src.a = 1.0 - (aView * (1.0-src.a));
      //src.a = 0.001;
      
      // clip plane visualization
      if(i==0 && !any(equal(ray / scaling,vec3(0.0))) && !any(equal(ray / scaling,vec3(1.0))))
      {      
        src.xyz = src.xyz * 1.6;
        //src.a   = src.a   * 1.6;        
      }
    } 
           
    //--------------------------
    // Compositing
    //--------------------------
    // Opacity correction   
    src.a   = 1.0 - pow((1.0 - src.a), (stepSizeLocal/stepSize));   
    
    // Front-to-back compositing
    //dst = CompositeScalarFrontToBack(src,dst);    
    dst = CompositeScalarFrontToBackMIDA(src,dst,beta);    
    
    ray        = ray      + rayDir * stepSizeLocal;
    rayOffset += stepSizeLocal;
  }  
  
  // Determine the final color
  color = dst;   
  
  if(gamma > 0)
  {
    colmax = vec4(vec3(maximumValue),1.0);
    color  = mix(dst, colmax, gamma);
  }
  
  gl_FragColor = color;     

}
