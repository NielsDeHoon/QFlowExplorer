#version 330

uniform mat4  matrixModelViewProjection;
uniform mat4  matrixModelViewProjectionInverse;

uniform float particleSize;
uniform int   particleStyle;
uniform float particlePercentage;

const   int   ELLIPSOID = 1;

layout(points )                        in;
layout(triangle_strip, max_vertices=4) out;

in particleAttrib
{
  vec4 position;
  vec3 velocity;
  
} particle[];

out imposterAttrib
{ 
  vec3 parametercoord;
  vec3 parameternormal;  
  mat4 quadric;
  mat4 transform;
  vec3 velocity;  
} imposter;


//----------------------------------------------------------------------------
void GetOrthonormalBasis(in vec3 v, out vec3 u, out vec3 w)
{  
  vec3 n = normalize(v);
  
  u = normalize(vec3(0.0,-n.z,n.y));
  w = normalize(cross(n,u));  
}

//----------------------------------------------------------------------------
float GetRandomNumber(vec3 v)
{
  return fract(sin(dot(v, vec3(12.9898, 78.233, 1.0)))* 43758.5453);
}

//----------------------------------------------------------------------------
void main(void)
{   
  mat4  T, Ti;  
  vec3  lambda, v[3];
  vec4  x, y, z;
  vec4  p[4];  
  float rand;
  
  rand = GetRandomNumber(particle[0].position.xyz);
  
  if((rand <= particlePercentage) && (particlePercentage != 0.0))
  {    
    // determine the scaling of the glyph
    if(particleStyle == ELLIPSOID)
    {
      lambda.x = 0.5*particleSize + particleSize*length(particle[0].velocity.xyz);
      lambda.y = 0.5*particleSize;
      lambda.z = 0.5*particleSize;  
    }
    else
    {
      lambda.x = lambda.y = lambda.z = 1.0;
    }

    // get the orthonormal basis of the ellipsoid in worldspace  
    GetOrthonormalBasis(particle[0].velocity.xyz,v[1],v[2]);  
    v[0]     = normalize(particle[0].velocity.xyz) * lambda.x;  
    v[1]     = normalize(v[1])                     * lambda.y;
    v[2]     = normalize(v[2])                     * lambda.z;  

    // build the variance matrix: transformation from parameter space to world space
    T[0]     = vec4(v[0],0.0);
    T[1]     = vec4(v[1],0.0);
    T[2]     = vec4(v[2],0.0);
    T[3]     = vec4(particle[0].position.xyz,1.0);    
    Ti      = inverse(T);

    // render the screen-aligned quad at twice the particle size
    // the quad is positioned towards the viewer to enable a start for the raycasting
    x        = particleSize * normalize(matrixModelViewProjectionInverse*vec4(1.0,0.0,0.0,0.0)); 
    y        = particleSize * normalize(matrixModelViewProjectionInverse*vec4(0.0,1.0,0.0,0.0));     
    z        = particleSize * normalize(matrixModelViewProjectionInverse*vec4(0.0,0.0,1.0,0.0));     

    p[0]     = vec4(particle[0].position.xyz,1.0) - x - y - z;
    p[1]     = vec4(particle[0].position.xyz,1.0) - x + y - z;
    p[2]     = vec4(particle[0].position.xyz,1.0) + x - y - z;
    p[3]     = vec4(particle[0].position.xyz,1.0) + x + y - z;

    imposter.quadric         = mat4(1.0, 0.0, 0.0,  0.0,
                                    0.0, 1.0, 0.0,  0.0,
                                    0.0, 0.0, 1.0,  0.0,
                                    0.0, 0.0, 0.0, -1.0);  
    imposter.transform       = T;
    imposter.parameternormal = (Ti * matrixModelViewProjectionInverse * vec4(0.0,0.0,1.0,0.0)).xyz ;
    imposter.velocity        = particle[0].velocity;   

    imposter.parametercoord  = (Ti * p[0]).xyz;
    gl_Position              = matrixModelViewProjection * p[0];
    EmitVertex();

    imposter.parametercoord  = (Ti * p[1]).xyz;
    gl_Position              = matrixModelViewProjection * p[1];
    EmitVertex();

    imposter.parametercoord  = (Ti * p[2]).xyz;
    gl_Position              = matrixModelViewProjection * p[2];
    EmitVertex();

    imposter.parametercoord  = (Ti * p[3]).xyz;
    gl_Position              = matrixModelViewProjection * p[3];
    EmitVertex();   

    EndPrimitive();    
  }  
}

