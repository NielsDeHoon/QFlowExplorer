#version 330

in vec4 qfe_Position;
in vec4 qfe_Attribs;

out seedAttrib
{
  vec4 position;
  vec4 attribs;
} seed;

//----------------------------------------------------------------------------
void main(void)
{      
   seed.position = qfe_Position;
   seed.attribs  = qfe_Attribs;  
}
