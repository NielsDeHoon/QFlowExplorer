#version 330

uniform sampler1D lineTransferFunction;

uniform float lineWidth;
uniform float distanceFilter;
uniform int   colorType;
uniform float arrowScale;
uniform mat4  matrixModelViewInverse;
uniform mat4  matrixModelViewProjection;
uniform mat4  matrixProjection;

layout(lines_adjacency )                in;
layout(triangle_strip, max_vertices=38) out;

in seedAttrib
{
  vec4 position; // in patient coordinates (x,y,z,t)
  vec4 attribs;  // x = speed, y = vertex id, z = distance local, w = distance global
} seed[];

out arrowAttrib
{  
  vec3  normal;
  vec4  attribs;
  vec4  color;
  float height;
  float total_height;
  float curvature;
  vec3  curve_direction;
  float radius;
} arrow;



//----------------------------------------------------------------------------
void orthonormalBasis2(in vec3 v, out vec3 u, out vec3 w)
{
  vec3 s, sabs;
  
  s    = normalize(v);
  sabs = abs(s);
  u    = s;
  
  if((sabs.x <= sabs.y) && (sabs.x <= sabs.z))  
    u = vec3(0.0, -s.z, s.y); 
  else
  if((sabs.y <= sabs.x) && (sabs.y <= sabs.z))  
    u = vec3(-s.z, 0.0, s.x);  
  else
  if((sabs.z <= sabs.x) && (sabs.z <= sabs.y))  
    u = vec3(-s.y, s.x, 0.0);  
    
  u = normalize(u);  
  
  w = cross(s, u); 
}

//----------------------------------------------------------------------------
void renderCone(vec3 start, vec3 end, float radius, vec4 color)
{
  const float twoPi    = 2.0 * 3.14159;
  const int   sections = 16;
  vec3  p[2*sections+3], n[2*sections+3];
  vec3  lineTangent, u, v;
  vec3  base;
  float theta;
  
  mat4  matrixNormal   = inverse(matrixModelViewInverse);  
  
  // Initialize
  lineTangent = end-start;  
  base        = end - lineTangent;
  theta       = twoPi / float(sections);  
  
  orthonormalBasis2(normalize(lineTangent), u, v);
   
  // Generate the cone
  p[0] = end;
  n[0] = normalize(end - start);
  for(int i = 0; i <= sections; i++) 
  { 
    p[2*i+1] = vec3(base + ( sin(float(i)*theta)*radius*u ) + ( cos(float(i)*theta)*radius*v ));
    p[2*i+2] = end;   
    
    n[2*i+1] = normalize(p[2*i+1]-base);
    n[2*i+2] = normalize(lineTangent);
    
  }  
  
  float height = sqrt(  (base.x - end.x)*(base.x - end.x) + 
						(base.y - end.y)*(base.y - end.y) + 
						(base.z - end.z)*(base.z - end.z));
  
  // Render the cone
  for(int i=0; i < 2*sections+3; i++)
  {
    arrow.normal  = (matrixNormal * vec4(n[i],0.0)).xyz;
    arrow.attribs = seed[1].attribs;
    arrow.color   = color; 
    gl_Position   = matrixModelViewProjection*vec4(p[i],1.0);
	
	int ind_min = i-1;
	if(ind_min < 0)
		ind_min = 2*sections+1;
	int ind_min2= i-2;
	if(ind_min2 < 0)
		ind_min2 = 2*sections+1;
	
	int ind_plus = i+1;
	if(ind_plus >= 2*sections+1)
		ind_plus = 1;
	int ind_plus2= i+2;
	if(ind_plus2 >= 2*sections+1)
		ind_plus2 = 1;
		
	vec4 p_min2 = vec4(p[ind_min2],1);	
	vec4 p_min1 = vec4(p[ind_min],1);
	vec4 p_0 = vec4(p[i],1);
	vec4 p_plus1 = vec4(p[ind_plus],1);
	vec4 p_plus2 = vec4(p[ind_plus2],1);
	
	arrow.curve_direction = vec3(1);
	
	if(i%2 == 1)
	{
		arrow.height = height;

		vec3 curve_dir = p_plus2.xyz - p_min2.xyz;
		curve_dir=vec3(matrixNormal*vec4(curve_dir,0));
		
		arrow.curve_direction = normalize(curve_dir);
		arrow.radius = radius;
	}
	else
	{
		arrow.height = 0.0;
		
		vec3 curve_dir = p_plus1.xyz - p_min1.xyz;//normalize(end - start);
		curve_dir=vec3(matrixNormal*vec4(curve_dir,0));

		arrow.curve_direction = normalize(curve_dir);
		
		arrow.radius = 0;
	}
	
	arrow.total_height = height;
	
	arrow.curvature = (height) / (2 * radius * sqrt( radius*radius + height*height));
  
    EmitVertex();
  }
  
  EndPrimitive();
}

//----------------------------------------------------------------------------
void main()
{   
  const float delta = 0.001;
  const float ratio = 3.0;

  vec3  eye, eyeWorld;
  vec4  posScreen;
  vec3  lineTangent;
  float lineLength;
  vec3  linePoints[2];
  vec4  vertexColor;
  
  float speed, speedScale;    
    
  // initialize
  eye            = vec3(0.0,0.0,1.0);        
  eyeWorld       = normalize( matrixModelViewInverse*vec4(0.0,0.0,1.0,0.0)).xyz;   
    
  vertexColor    = vec4(1.0,0.0,0.0,1.0);   
  
  bool  passFilter  = true;
  if(colorType == 3) // Hausdorff  
    passFilter = (seed[1].attribs.w > (1.0 - distanceFilter));  
  else
    passFilter = (seed[1].attribs.z > (1.0 - distanceFilter));
  
  speedScale     = 1.0;
    
  bool isEndPoint   = length(seed[3].position.xyz - seed[2].position.xyz) < delta;    
    
  if(passFilter)
  {    
    // Render arrow at the current phase, within the user-defined span
    if(isEndPoint)
    { 
      // render arrow
      lineTangent         = normalize(seed[2].position.xyz - seed[1].position.xyz);
      lineLength          = length(seed[2].position.xyz - seed[1].position.xyz);        
      linePoints[0]       = seed[2].position.xyz;
      linePoints[1]       = seed[2].position.xyz + ratio*arrowScale*speedScale*lineWidth*lineTangent;

      renderCone(linePoints[0], linePoints[1], arrowScale*speedScale*lineWidth, vertexColor);              
    }
    
  }
    
}