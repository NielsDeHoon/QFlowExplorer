#version 330

uniform sampler1D particleTransferFunction;

uniform float lineWidth;
uniform float arrowScale;
uniform int   angleFilter;
uniform mat4  matrixModelViewInverse;
uniform mat4  matrixModelViewProjection;
uniform mat4  matrixProjection;

layout(points)                          in;
layout(triangle_strip, max_vertices=152) out;

in particleAttrib
{
  vec3 position;
  vec3 velocity1;
  vec3 velocity2;
  vec3 normal;
} particle[];

out arrowAttrib
{  
  vec3  normal;  
  vec4  color;
} arrow;

//----------------------------------------------------------------------------
void orthonormalBasis2(in vec3 v, out vec3 u, out vec3 w)
{
  vec3 s, sabs;
  
  s    = normalize(v);
  sabs = abs(s);
  u    = s;
  
  if((sabs.x <= sabs.y) && (sabs.x <= sabs.z))  
    u = vec3(0.0, -s.z, s.y); 
  else
  if((sabs.y <= sabs.x) && (sabs.y <= sabs.z))  
    u = vec3(-s.z, 0.0, s.x);  
  else
  if((sabs.z <= sabs.x) && (sabs.z <= sabs.y))  
    u = vec3(-s.y, s.x, 0.0);  
    
  u = normalize(u);  
  
  w = cross(s, u); 
}

//----------------------------------------------------------------------------
void renderShaft(vec3 start, vec3 end, float radius, vec4 color)
{
  const float twoPi    = 2.0 * 3.14159;
  const int   sections = 8;
  vec3  p[2*sections+3], n[2*sections+3];
  vec3  lineTangent, u, v;
  vec3  base;
  float theta;
  
  mat4  matrixNormal   = inverse(matrixModelViewInverse);  
  
  // Initialize
  lineTangent = end-start;  
  base        = end - lineTangent;
  theta       = twoPi / float(sections);  
  
  orthonormalBasis2(normalize(lineTangent), u, v);
   
  // Generate the cylinder
  p[0] = end;
  for(int i = 0; i <= sections; i++) 
  { 
    p[2*i+1] = vec3(base + ( sin(float(i)*theta)*radius*u ) + ( cos(float(i)*theta)*radius*v ));
    p[2*i+2] = vec3(end  + ( sin(float(i)*theta)*radius*u ) + ( cos(float(i)*theta)*radius*v ));   
    
    n[2*i+1] = normalize(p[2*i+1]-base);
    n[2*i+2] = normalize(p[2*i+2]-end);
    
  }  
  
  // Render the cylinder
  for(int i=0; i < 2*sections+3; i++)
  {
    arrow.normal  = (matrixNormal * vec4(n[i],0.0)).xyz;    
    arrow.color   = color; 
    gl_Position   = matrixModelViewProjection*vec4(p[i],1.0);
    EmitVertex();
  }
  
  EndPrimitive();

}


//----------------------------------------------------------------------------
void renderCone(vec3 start, vec3 end, float radius, vec4 color)
{
  const float twoPi    = 2.0 * 3.14159;
  const int   sections = 8;
  vec3  p[2*sections+3], n[2*sections+3];
  vec3  lineTangent, u, v;
  vec3  base;
  float theta;
  
  mat4  matrixNormal   = inverse(matrixModelViewInverse);  
  
  // Initialize
  lineTangent = end-start;  
  base        = end - lineTangent;
  theta       = twoPi / float(sections);  
  
  orthonormalBasis2(normalize(lineTangent), u, v);
   
  // Generate the cone
  p[0] = end;
  for(int i = 0; i <= sections; i++) 
  { 
    p[2*i+1] = vec3(base + ( sin(float(i)*theta)*radius*u ) + ( cos(float(i)*theta)*radius*v ));
    p[2*i+2] = end;   
    
    n[2*i+1] = normalize(p[2*i+1]-base);
    n[2*i+2] = normalize(lineTangent);
    
  }  
  
  // Render the cone
  for(int i=0; i < 2*sections+3; i++)
  {
    arrow.normal  = (matrixNormal * vec4(n[i],0.0)).xyz;    
    arrow.color   = color; 
    gl_Position   = matrixModelViewProjection*vec4(p[i],1.0);
    EmitVertex();
  }
  
  EndPrimitive();
}

//----------------------------------------------------------------------------
void main()
{   
  const float delta = 0.001;
  const float coneLengthRatio = 2.0/3.0;  
  const float coneWidthRatio  = 0.6;  
  const float shaftWidthRatio = 0.3;  

  vec3  eye, eyeWorld;
  vec4  posScreen;
  vec3  lineTangent;
  float lineLength;
  float angle;
  vec3  linePoints[2];
  vec4  vertexColor;
  vec4  highlightColor = vec4(0.2,0.2,0.2,1.0);  
  
  float speed;    
    
  // initialize
  eye              = vec3(0.0,0.0,1.0);        
  eyeWorld         = normalize( matrixModelViewInverse*vec4(0.0,0.0,1.0,0.0)).xyz;   
  
  angle            = dot(normalize(particle[0].velocity1),normalize(particle[0].velocity2));
    
  bool passFilter  = 2.0-(1.0-angle) <= (float(angleFilter)/90.0);  
      
  if(passFilter)
  {     
    // Render arrow 1   
    vertexColor         = highlightColor;  
    lineTangent         = normalize(particle[0].velocity1);
    lineLength          = max(1.0,length(particle[0].velocity1) * arrowScale);               
   
    linePoints[0]       = particle[0].position.xyz;
    linePoints[1]       = particle[0].position.xyz  + coneLengthRatio*lineLength*lineTangent;
    renderShaft(linePoints[0], linePoints[1], shaftWidthRatio*lineWidth, vertexColor);                  
    
    linePoints[0]       = particle[0].position.xyz + coneLengthRatio*lineLength*lineTangent;
    linePoints[1]       = particle[0].position.xyz +                 lineLength*lineTangent;
    renderCone(linePoints[0], linePoints[1], coneWidthRatio*lineWidth, vertexColor);                  
    
    // Render arrow 2
    vertexColor         = texture(particleTransferFunction, (1.0-angle)/2.0);  
    lineTangent         = normalize(particle[0].velocity2);
    lineLength          = max(1.0,length(particle[0].velocity2) * arrowScale);            
    
    linePoints[0]       = particle[0].position.xyz;
    linePoints[1]       = particle[0].position.xyz  + coneLengthRatio*lineLength*lineTangent;
    renderShaft(linePoints[0], linePoints[1], shaftWidthRatio*lineWidth, vertexColor);                  
        
    linePoints[0]       = particle[0].position.xyz + coneLengthRatio*lineLength*lineTangent;
    linePoints[1]       = particle[0].position.xyz +                 lineLength*lineTangent;
    renderCone(linePoints[0], linePoints[1], coneWidthRatio*lineWidth, vertexColor);                  
  }
    
}