#version 330

uniform sampler1D particleTransferFunction;
uniform mat4      matrixProjection;
uniform vec4      light;
uniform vec4      lightDirection;
uniform float     particleSize;

in imposterAttrib
{ 
  vec4 position;
  vec4 texcoord;  
} imposter;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 Desaturate(const vec4 color, const float desaturation)
{
  //const vec3 gray_conv = vec3(0.30, 0.59, 0.11);  
  const vec3 gray_conv = vec3(0.30, 0.3, 0.3);  
  vec3 gray = vec3(dot(gray_conv, color.rgb));
  
  return vec4(vec3(mix(color.rgb, gray, desaturation)),1.0);
}

//----------------------------------------------------------------------------
void main()
{  
  vec3  eye;
  vec3  lightDir;  
  vec4  normal;
  float lensqr;
  float ndotv;
  float speed, index;    
  vec3  color;
  vec4  final; 
  
  // Initialization
  color        = vec3(0.0);  
  final        = vec4(0.0);
  
  // Make the point sprite
  lensqr = dot(imposter.texcoord.xy, imposter.texcoord.xy);
  if(lensqr > 1.0) discard;
  
  // Solve the illumination characteristics (in screen space)
  eye       = normalize(vec4(0.0,0.0,1.0,0.0)).xyz;
  lightDir  = normalize(-lightDirection).xyz;
  normal    = vec4(imposter.texcoord.xy, sqrt(1.0 - lensqr),0.0);  
  ndotv     = dot(normal.xyz, eye); 
     
  // Color by distance function
  color = vec3(0.2);  
 
  // Shading
  final = Phong(vec4(color,1.0), lightDir, normal.xyz, eye, light.x, light.y, vec2(light.z, light.w));    
    
  if(lensqr > 0.75) final = vec4(0.0,0.0,0.0,1.0);
    
  qfe_FragColor = final; 
  
   // adjust the depth
  vec4 screenpos  = vec4(imposter.position.xyz,1.0);
  screenpos.z    += normal.z*0.5*particleSize;  
  screenpos       = matrixProjection * screenpos;

  float far       = gl_DepthRange.far; 
  float near      = gl_DepthRange.near;  
  float ndc_depth = screenpos.z / screenpos.w;  
  float depth     = (((far-near) * ndc_depth) + near + far) / 2.0;
    
  gl_FragDepth    = depth;
}