#version 330

uniform sampler1D lineTransferFunction;

uniform vec4      light;
uniform vec4      lightDirection;
uniform int       highlight;
uniform mat4      matrixModelView;
uniform mat4      matrixModelViewInverse;

in arrowAttrib
{
  vec3  normal;    
  vec4  color;
} arrow;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 Desaturate(const vec4 color, const float desaturation)
{
  //const vec3 gray_conv = vec3(0.30, 0.59, 0.11);  
  const vec3 gray_conv = vec3(0.30, 0.3, 0.3);  
  vec3 gray = vec3(dot(gray_conv, color.rgb));
  
  return vec4(vec3(mix(color.rgb, gray, desaturation)),1.0);
}

//----------------------------------------------------------------------------
void main()
{ 
  const float delta = 0.05;
  
  vec4  color, shaded;
  
  // initialize vectors  
  vec3 n = normalize(arrow.normal);
  vec3 l = normalize(-lightDirection.xyz);
  vec3 e = vec3(0.0,0.0,1.0);
  
  // set color
  color = arrow.color;
    
  // apply shading (we bump the color a bit to highlight the arrow heads)  
  shaded = Phong(color, l, n, e, light.x, light.y, vec2(light.z,light.w));  

  qfe_FragColor = shaded;

}