#version 330

in vec4 qfe_PositionFirst;
in vec4 qfe_PositionSecond;
in vec4 qfe_Attribs;

out seedAttrib
{
  vec4 positionFirst;  
  vec4 positionSecond;  
  vec4 attribs;
} seed;

//----------------------------------------------------------------------------
void main(void)
{      
   seed.positionFirst  = qfe_PositionFirst;
   seed.positionSecond = qfe_PositionSecond;
   seed.attribs        = qfe_Attribs;
}
