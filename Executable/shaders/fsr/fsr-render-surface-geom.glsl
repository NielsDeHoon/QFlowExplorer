#version 330

uniform float lineWidth;
uniform float distanceFilter;
uniform mat4  matrixModelViewProjection;
uniform mat4  matrixModelViewInverse;
uniform int   colorType;

layout(lines_adjacency )                in;
layout(triangle_strip, max_vertices=4) out;

in seedAttrib
{
  vec4 positionFirst;  // in patient coordinates (x,y,z,t)
  vec4 positionSecond; // in patient coordinates (x,y,z,t)
  vec4 attribs;
} seed[];

out imposterAttrib
{   
  vec3  normal;
  vec2  texcoord; // (x = arclength, y = left-right)
  vec4  attribs;
} imposter;

//----------------------------------------------------------------------------
void main()
{ 
  bool  passFilter;
  vec3  eye;
  vec3  tangentFirst, tangentSecond;
  vec3  binormalFirst, binormalSecond;
  mat4  matrixNormal;
  
  const float delta = 0.0001;
  
  passFilter           = true;
    
  if(colorType == 3) // Hausdorff  
    passFilter = (seed[1].attribs.w > (1.0 - distanceFilter));  
  else
    passFilter = (seed[1].attribs.z > (1.0 - distanceFilter));
    
  if(passFilter)
  {  
    
    // initialize
    eye                  = vec3(0.0,0.0,1.0);  
    matrixNormal         = inverse(matrixModelViewInverse); 
    
    // -----------------------------
    // compute tangent and binormal
    // -----------------------------
    tangentFirst         = normalize(seed[2].positionFirst.xyz  - seed[1].positionFirst.xyz);
    tangentSecond        = normalize(seed[2].positionSecond.xyz - seed[1].positionSecond.xyz);
    binormalFirst        = normalize(seed[1].positionSecond.xyz - seed[1].positionFirst.xyz);
    binormalSecond       = normalize(seed[2].positionSecond.xyz - seed[2].positionFirst.xyz);    

    // -----------------------------
    // render the strip
    // -----------------------------

    imposter.texcoord    = vec2(0.0,0.0);      
    imposter.attribs     = seed[1].attribs;
    imposter.normal      = (matrixNormal * vec4(cross(tangentFirst, binormalFirst),0.0)).xyz;    
    gl_Position          = matrixModelViewProjection * vec4(seed[1].positionFirst.xyz,1.0);     
    EmitVertex();

    imposter.texcoord    = vec2(0.0,1.0);
    imposter.attribs     = seed[1].attribs;
    imposter.normal      = (matrixNormal * vec4(cross(tangentSecond, binormalFirst),0.0)).xyz;    
    gl_Position          = matrixModelViewProjection * vec4(seed[1].positionSecond.xyz,1.0);     
    EmitVertex();

    imposter.texcoord    = vec2(0.0,0.0);  
    imposter.attribs     = seed[2].attribs;    
    imposter.normal      = (matrixNormal * vec4(cross(tangentFirst, binormalSecond),0.0)).xyz;    
    gl_Position          = matrixModelViewProjection * vec4(seed[2].positionFirst.xyz,1.0);     
    EmitVertex();

    imposter.texcoord    = vec2(0.0,1.0);  
    imposter.attribs     = seed[2].attribs;
    imposter.normal      = (matrixNormal * vec4(cross(tangentSecond, binormalSecond),0.0)).xyz;    
    gl_Position          = matrixModelViewProjection * vec4(seed[2].positionSecond.xyz,1.0);     
    EmitVertex();  

    EndPrimitive();
  }
}