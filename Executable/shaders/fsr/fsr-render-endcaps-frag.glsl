#version 330

uniform sampler1D lineTransferFunction;

uniform int       colorType;
uniform vec4      light;
uniform vec4      lightDirection;
uniform int       lineShading;
uniform float     lineWidth;
uniform mat4      matrixProjection;

in capAttrib
{  
  vec4  position;
  vec3  normal;
  vec2  texcoord; // (x = arclength, y = left-right)    
  vec4  attribs;
} cap;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 Desaturate(const vec4 color, const float desaturation)
{
  //const vec3 gray_conv = vec3(0.30, 0.59, 0.11);  
  const vec3 gray_conv = vec3(0.30, 0.3, 0.3);  
  vec3 gray = vec3(dot(gray_conv, color.rgb));
  
  return vec4(vec3(mix(color.rgb, gray, desaturation)),1.0);
}

//----------------------------------------------------------------------------
void main()
{ 
  const float delta = 0.1;
  
  vec4 color;
  vec4 shaded;
  float yw,zw;
    
  // make a point sprite
  if(pow(cap.texcoord.x-0.5,2.0)+pow(cap.texcoord.y-0.5,2.0) > 0.25) discard;
  
  // compute tex coord derivatives
  yw = (cap.texcoord.y*2.0)-1.0;  
  zw  = 1.0 - abs(yw);
    
  // initialize vectors  
  vec3 n  = normalize(cap.normal);
  vec3 l  = normalize(-lightDirection.xyz);
  vec3 e  = vec3(0.0,0.0,1.0);
  
  vec3 bn = normalize(vec3(n.xy * yw, zw));
  
  // fetch the color
  color  = vec4(1.0,0.0,0.0,1.0);
    
  if(colorType == 0) // speed
    color  = texture(lineTransferFunction, cap.attribs.x);  
      
  if(colorType == 1) // distance local
    color  = texture(lineTransferFunction, cap.attribs.y);  
        
  if(colorType == 2) // distance mean
    color  = texture(lineTransferFunction, cap.attribs.z);  
      
  if(colorType == 3) // distance Hausdorff
    color  = texture(lineTransferFunction, cap.attribs.w);  
 
  // Desaturate
  color =  Desaturate(color, 0.6);
 
  // Shading   
  shaded = Phong(color, l, n, e, light.x, light.y, vec2(light.z,light.w));    
	  
  qfe_FragColor = shaded;  
    
  /*  
  // adjust the depth
  vec4 screenpos  = vec4(cap.position.xyz,1.0);
  //screenpos.z    += bn.z*0.5*lineWidth;  
  screenpos       = matrixProjection * screenpos;
  
  float far       = gl_DepthRange.far; 
  float near      = gl_DepthRange.near;  
  float ndc_depth = screenpos.z / screenpos.w;  
  float depth     = (((far-near) * ndc_depth) + near + far) / 2.0;
      
  gl_FragDepth    = depth;  
  */
  
}