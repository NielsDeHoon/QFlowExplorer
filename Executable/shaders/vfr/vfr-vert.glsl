// vfr-vert.glsl
varying vec4 posTexture;
varying vec4 posScreen;

void main()
{
  gl_FrontColor  = gl_Color;  
  posTexture     = gl_TextureMatrix[0] * gl_MultiTexCoord0;
  posScreen      = gl_ModelViewProjectionMatrix * gl_Vertex;  
  gl_Position    = ftransform();
}