#version 330

in  vec4 qfe_SeedPoints;

out vec4 seedPosition;

void main(void)
{     
   seedPosition   = qfe_SeedPoints;
}
