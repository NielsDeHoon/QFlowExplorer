#version 330

uniform mat4  matrixModelViewProjection;

layout(lines_adjacency)                  in;
layout(triangle_strip, max_vertices=4) out; // GL_MAX_GEOMETRY_OUTPUT_VERTICES

in  vec4  line1[4];
in  vec4  line2[4];
in  vec4  normals1[4];
in  vec4  normals2[4];
in  vec4  attribs1[4];
in  vec4  attribs2[4];

out vec4  frontColor;
out vec4  normal;
out vec2  texCoord;
out float speed;

//----------------------------------------------------------------------------
void main() {
  
  frontColor  = vec4(1.0,0.0,0.0,1.0);  
  normal      = normals1[1];
  texCoord    = vec2(attribs1[1].zw);
  speed       = attribs1[1].x;
  gl_Position = matrixModelViewProjection * vec4(line1[1].xyz,1.0); 
  EmitVertex(); 
    
  frontColor  = vec4(1.0,0.0,0.0,1.0);
  normal      = normals1[2];
  texCoord    = vec2(attribs1[2].zw);
  speed       = attribs1[2].x;
  gl_Position = matrixModelViewProjection * vec4(line1[2].xyz,1.0);   
  EmitVertex();    
    
  frontColor  = vec4(1.0,0.0,0.0,1.0);
  normal      = normals2[1];
  texCoord    = vec2(attribs2[1].zw);
  speed       = attribs2[1].x;
  gl_Position = matrixModelViewProjection * vec4(line2[1].xyz,1.0);   
  EmitVertex();  
    
  frontColor  = vec4(1.0,0.0,0.0,1.0);
  normal      = normals2[2];
  texCoord    = vec2(attribs2[2].zw);
  speed       = attribs2[2].x;
  gl_Position = matrixModelViewProjection * vec4(line2[2].xyz,1.0);   
  EmitVertex();      
  
  //EndPrimitive();
} 
 