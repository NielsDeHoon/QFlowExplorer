#version 330

in  vec4 qfe_QuadLine;
in  vec4 qfe_QuadLineLeft;
in  vec4 qfe_QuadLineRight;

out vec4 line;
out vec4 lineLeft;
out vec4 lineRight;

void main(void)
{     
   line      = qfe_QuadLine;
   lineLeft  = qfe_QuadLineLeft;
   lineRight = qfe_QuadLineRight;
}
