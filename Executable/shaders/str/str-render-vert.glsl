//#version 130
//#extension GL_EXT_gpu_shader4 : enable
#version 330

in  vec4 qfe_QuadLine1;
in  vec4 qfe_QuadLine2;
in  vec4 qfe_QuadNormals1;
in  vec4 qfe_QuadNormals2;
in  vec4 qfe_QuadAttribs1;
in  vec4 qfe_QuadAttribs2;

out vec4 line1;
out vec4 line2;
out vec4 normals1;
out vec4 normals2;
out vec4 attribs1;
out vec4 attribs2;

void main(void)
{     
  line1    = qfe_QuadLine1;
  line2    = qfe_QuadLine2;
  normals1 = qfe_QuadNormals1;
  normals2 = qfe_QuadNormals2;
  attribs1 = qfe_QuadAttribs1;
  attribs2 = qfe_QuadAttribs2;
}
