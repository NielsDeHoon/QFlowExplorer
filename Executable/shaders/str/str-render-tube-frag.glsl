#version 330

uniform sampler1D transferFunction;
uniform vec2      dataRange;
uniform vec4      light;
uniform vec4      lightDirection;

uniform int       surfaceShading;

uniform mat4      matrixModelViewInverse;

in  vec4  frontColor;
in  vec4  normal;
in  vec2  texCoord;
in  float speed;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Cel(in vec4 col, in vec3 l, in vec3 n);

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s) ;

//----------------------------------------------------------------------------
void main()
{   
  const float PI = 3.141592;
  vec4  color, shaded;
  vec3  eye, lightDir; 
  float ndotv;
  float speedIndex;
  
  float scaleT, frac1, frac2;
  float fuzz   = 0.1;
  float width  = 0.2;
  
  
  //float aview; 
  //vec2  ds, dt;
  //float ls, lt;
  
  //ds = vec2(dFdx(texCoord.x), dFdy(texCoord.x));
  //dt = vec2(dFdx(texCoord.y), dFdy(texCoord.y));
  //ls = log2(length(ds));
  //lt = log2(length(dt));
  //aview = (2.0/PI) * acos(dot(normal.xyz, eye.xyz));
  //color = vec4(vec3(texCoord.y*pow(2.0,-1.0*lt)),1.0);
  //color = frontColor;
    
  eye       = normalize(matrixModelViewInverse * vec4(0.0,0.0,1.0,0.0)).xyz;
  lightDir  = normalize(matrixModelViewInverse * -lightDirection).xyz;   
  
  speedIndex    = speed / max(abs(dataRange.x), abs(dataRange.y));
  //speedIndex    = speedIndex * (float(colorScale)/100.0);  
  
  color = texture(transferFunction, speedIndex);
  
  if(surfaceShading == 0) // cel
  {
    shaded = Cel(color, lightDir, normal.xyz);    
  }
  else
  if(surfaceShading == 1) // cel
  {
    shaded = Phong(color, lightDir, normal.xyz, eye, light.x, light.y, vec2(light.z, light.w));  
  }
  else
  if(surfaceShading == 2) // procedural stripes
  {
    ndotv  = dot(normal.xyz, eye.xyz);
    
    scaleT = fract(texCoord.y * 20.0);
    frac1  = clamp(scaleT/fuzz,0.0,1.0);
    frac2  = clamp((scaleT - width)/fuzz, 0.0, 1.0);
      
    frac1 = frac1 * (1.0-frac2);
    frac1 = frac1 * frac1 * (3.0 - (2.0*frac1));
    
    color  = mix(vec4(0.4,0.4,0.4,1.0), color, frac1);
    
    if(ndotv < 0.0) color.rgb = vec3(max(max(color.r, color.g),color.b)) - vec3(0.3);
    
    shaded = Phong(color, lightDir, normal.xyz, eye, light.x, light.y, vec2(light.z, light.w)); 
  } 
  else  
  {
    shaded = color;
  }
    
  if(texCoord.x > 0.96) shaded = vec4(vec3(0.0),1.0);
  
  if(shaded.a   == 0.0)       discard;
     
  qfe_FragColor = shaded;
}