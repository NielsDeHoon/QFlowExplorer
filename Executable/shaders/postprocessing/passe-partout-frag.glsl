#version 430

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D depthTex;
uniform sampler2D sceneTex;

uniform float znear;
uniform float zfar;

float readDepth( in vec2 coord )
{
	return (2 * znear) / (zfar + znear - texture2D( depthTex, textureCoord ).x * (zfar - znear));
}

void main() 
{ 
	//concept: http://resources.mpi-inf.mpg.de/VirtualPassepartouts/
	fragColor = vec3(0.0);
	if(textureCoord.y > 0.1 && textureCoord.y < 0.9 && textureCoord.x > 0.1 && textureCoord.x < 0.9)//if we are within the passe-partout we render the scene normally
	{
		fragColor.rgb = texture2D(sceneTex, textureCoord).rgb;
	}
	else
	{
		float depth = readDepth(textureCoord);
		
		if(depth>0.99)//background
			return;
		else if(3.0*znear >= zfar-1)
		{
			depth = (depth - 0.63) * 32.0; //some manual correction for the small range the depth is actually in
		}
		
		if(depth < 0.25) //if the depth is closer we draw the actual rendering, otherwise we use black
		{
			fragColor.rgb = texture2D(sceneTex, textureCoord).rgb;
		}
	}
	return;
}