#version 430

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D sceneTex;

void main() 
{ 
	fragColor = texture2D(sceneTex, textureCoord.xy).rgb;
}