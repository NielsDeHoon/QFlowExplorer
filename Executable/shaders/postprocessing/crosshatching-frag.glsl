#version 430

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D sceneTex;
uniform sampler2D normalTex;

//http://www.geeks3d.com/20110219/shader-library-crosshatching-glsl-filter/
 
void main()
{   
	float hatch_y_offset = 5.0;
	float lum_threshold_1 = 1.0;
	float lum_threshold_2 = 0.7;
	float lum_threshold_3 = 0.5;
	float lum_threshold_4 = 0.3;
	
	vec2 uv = textureCoord.xy;

	vec3 tc = vec3(1.0, 1.0, 1.0);

	//float lum = length(texture2D(sceneTex, uv).rgb);
	vec3 color = texture2D(sceneTex, uv).rgb;
	float lum = color.r*0.2126 + color.g*0.7152 + color.b*0.0722;
    
	if (lum < lum_threshold_1) 
	{
		if (mod(gl_FragCoord.x + gl_FragCoord.y, 10.0) == 0.0) 
		tc = vec3(0.0, 0.0, 0.0);
	}  
  
	if (lum < lum_threshold_2) 
	{
		if (mod(gl_FragCoord.x - gl_FragCoord.y, 10.0) == 0.0) 
			tc = vec3(0.0, 0.0, 0.0);
	}  
  
	if (lum < lum_threshold_3) 
	{
		if (mod(gl_FragCoord.x + gl_FragCoord.y - hatch_y_offset, 10.0) == 0.0) 
			tc = vec3(0.0, 0.0, 0.0);
	}  
  
	if (lum < lum_threshold_4) 
	{
	  if (mod(gl_FragCoord.x - gl_FragCoord.y - hatch_y_offset, 10.0) == 0.0) 
		tc = vec3(0.0, 0.0, 0.0);
	}
  
	fragColor = vec3(tc);
}