#version 430

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D sceneTex;
 
void main()
{   
	//anti aliasing based on:
	//https://www.shadertoy.com/view/MtSGRG

	ivec2 screenSize = textureSize2D(sceneTex, 0).xy;
	
	vec2 inverse_resolution=vec2(1.0/screenSize.x,1.0/screenSize.y);
	
	vec3 pixelColor = vec3(0.0);
	
	vec2 fragCoord = vec2(textureCoord.x * screenSize.x, textureCoord.y * screenSize.y);
	
	bool xOdd = (floor(mod(fragCoord.x,2.0)) == 1.0);
	bool yOdd = (floor(mod(fragCoord.y,2.0)) == 1.0);

	vec2 a = vec2(xOdd ? 0.25 : -0.25, yOdd ? -0.5  :  0.5 );
	vec2 b = vec2(xOdd ? 0.5  : -0.5 , yOdd ?  0.25 : -0.25 );
	vec2 c = a * vec2(-1);
	vec2 d = b * vec2(-1);
	
	vec2 coordA = (fragCoord.xy + a) * inverse_resolution;
	vec2 coordB = (fragCoord.xy + b) * inverse_resolution;
	vec2 coordC = (fragCoord.xy + c) * inverse_resolution;
	vec2 coordD = (fragCoord.xy + d) * inverse_resolution;
	
	pixelColor  = texture(sceneTex, coordA).rgb / 4.0;
    pixelColor += texture(sceneTex, coordB).rgb / 4.0;
    pixelColor += texture(sceneTex, coordC).rgb / 4.0;
    pixelColor += texture(sceneTex, coordD).rgb / 4.0;     
	
	fragColor = pixelColor;
}