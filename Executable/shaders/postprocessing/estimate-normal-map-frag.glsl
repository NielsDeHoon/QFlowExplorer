#version 430

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D depthTex;

uniform float znear;
uniform float zfar;

const float epsilon = 0.01;

float linearDepth(float depthSample)
{
	float z_n = 2.0 * depthSample - 1.0;
    return 2.0 * znear * zfar / (zfar + znear - z_n * (zfar - znear));
}

void main() 
{ 
	fragColor = vec3(0.0);
	
	ivec2 tex_size = textureSize2D(depthTex, 0).xy;

	vec2 epsilon_x = vec2(1.0/tex_size.x, 0.0);
	vec2 epsilon_y = vec2(0.0, 1.0/tex_size.y);
	
	float x = linearDepth(texture2D(depthTex, textureCoord.xy+epsilon_x).r)
		- linearDepth(texture2D(depthTex, textureCoord.xy-epsilon_x).r);
	float y = linearDepth(texture2D(depthTex, textureCoord.xy+epsilon_y).r)
		- linearDepth(texture2D(depthTex, textureCoord.xy-epsilon_y).r);
	
	if(abs(x) < epsilon && abs(y) < epsilon)
	{
		fragColor = vec3(0.5, 0.5, 1.0);
	}
	else
	{
		float r = (x+1.0)/2.0;
		float g = (y+1.0)/2.0;
	
		fragColor = vec3(r, g , 0.5);
	}
}