#version 430

#extension GL_EXT_gpu_shader4 : enable

// Interpolated values from the vertex shaders
in vec2 textureCoord;

// Ouput data
out vec3 fragColor;

uniform sampler2D sceneTex;

uniform int kernel_size;

float sampleGaussian(int index)
{
	float sigma = float(kernel_size)/2.0;
	
	return 1.0/(sigma*sqrt(2.0*3.14))*pow(2.7,-(float(index)*float(index))/(2.0*sigma*sigma));
}
 
void main()
{
	ivec2 tex_size = textureSize2D(sceneTex, 0).xy;

	vec2 u_Scale = vec2(0, 1.0/tex_size.y);
	
	vec4 color = vec4(0.0);
	for( int i = -kernel_size; i < kernel_size+1; i++ )
	{
		color += texture2D( sceneTex, vec2(textureCoord.x+i*u_Scale.x, textureCoord.y+i*u_Scale.y ) )*sampleGaussian(i);
	}
 
	fragColor = color.rgb;
}