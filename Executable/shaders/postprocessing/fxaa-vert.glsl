#version 430

const vec2 madd=vec2(0.5,0.5);

layout(location = 0) in vec2 Vertex;

out vec2 textureCoord;

out vec4 posPos;
uniform float FXAA_SUBPIX_SHIFT = 1.0/4.0;

layout(location = 1) uniform sampler2D sceneTex;

void main()
{
	textureCoord = Vertex.xy*madd+madd; // scale vertex attribute to [0-1] range
	   
	gl_Position = vec4(Vertex.xy,0.0,1.0);
}