#version 400

#extension GL_EXT_gpu_shader4 : enable
#extension GL_EXT_shader_image_load_store : enable
#extension GL_ARB_shader_image_size : enable

in vec2 tc;

in float f_depth;

in vec4 color;
 
//out vec4 fragColor;
layout(location = 0) out vec4 fragColor;

uniform float particle_size;

uniform int particle_type;

uniform float znear;
uniform float zfar;

uniform int depth_darkening;

uniform float depth_darkening_amount;

uniform int depth_darkening_light;

uniform layout(RGBA32F) image2D depth_buffer;

float gaussianFilter(layout(RGBA32F) image2D tex, ivec2 texcoord, int size, float std=-1)
{
    if(std==-1)
		std=size*size;
		
	ivec2 screenSize = imageSize(depth_buffer).xy;
			
	float step_x=1.0f/(screenSize.x);
	float step_y=1.0f/(screenSize.y);
	float arg=0;
	float h=0;
	float sum=0;
	float pixel = 0.0;   
   
	if(size==0)
	{
	   pixel=imageLoad(tex,texcoord.xy).r;
	   sum=1;
	}
				  
	for(int i=0;i<size;i++)
	{
	   for(int j=0;j<size;j++)
	   {
		   float x=-(float(size)-1)/2.0f+i;
		   float y=-(float(size)-1)/2.0f+j;
		   arg=-(x*x+y*y)/(2*std*std);
		   h=exp(arg);
		   sum+=h;
		  
		  
		   float depth = imageLoad(tex, texcoord.xy+ivec2(x*step_x,y*step_y)).r;
		   
		   if(depth == 0.0)
			depth = 2.0;
		   
		   if (depth > 0.0)
				pixel+=h*imageLoad(tex,texcoord.xy+ivec2(x*step_x,y*step_y)).r;
		   else
				pixel+=h*1.0;
	   }
	}
	return pixel/sum;
}

float linearDepth(float depthSample)
{
	float z_n = 2.0 * depthSample - 1.0;
    return 2.0 * znear * zfar / (zfar + znear - z_n * (zfar - znear));
}
 
void main()
{          
    float radius=1.0;
		
	float contour_fraction = 0.5;
	if(particle_size<1.0)
		contour_fraction = 0.5;
	else
		contour_fraction = 1.0/(particle_size*2.0);
		
    if(length(tc)<radius-contour_fraction)
    {   
		float sphere_splat = cos((3.14/2*length(tc))/radius);
				
		if(particle_type == 0 || (color.a == 1.0 && particle_type == 1)) //flat
		{
			fragColor.rgb = color.rgb;
			fragColor.a = color.a;
		}		
		else if(particle_type == 1)//Gaussian like
		{
			float sigma = 0.3;
			float gaus = 1.0/(sigma*sqrt(2.0*3.14))*pow(2.7,-(length(tc)*length(tc))/(2.0*sigma*sigma));
			float max_gaus = 1.35; //maximum value of the gaussian length(tc) == 0.0
			
			fragColor.rgb = color.rgb;
							
			fragColor.a = color.a*(gaus/max_gaus);
			
			float boundary = 0.75;
			if(length(tc)>boundary*radius)
			{
				fragColor.a *= 1.0-(length(tc)-boundary*radius)/(radius-boundary*radius);
			}
		}		
		else if(particle_type == 2) //sphere:
		{
			fragColor = color.rgba*vec4(vec3(sphere_splat),1.0);
		}
		else
		{
			fragColor.rgb = color.rgb;
			fragColor.a = color.a;
		}
		
		vec2 texelSize = 1.0 / vec2(imageSize(depth_buffer));
		vec2 screenCoordsFloat = gl_FragCoord.xy * texelSize;
		ivec2 screenCoords = ivec2(int(screenCoordsFloat.x), int(screenCoordsFloat.y));
  
		//store the depth in the depth buffer	
		if(depth_darkening>0)
			imageStore(depth_buffer, screenCoords, vec4(f_depth));
	}
	else
	{	
		if((length(tc)<radius) &&  depth_darkening>0)
		{		
			fragColor = vec4(1.0,0.0,0.0,1.0);
		
			vec2 texelSize = 1.0 / vec2(imageSize(depth_buffer));
			vec2 screenCoordsFloat = gl_FragCoord.xy * texelSize;
			ivec2 screenCoords = ivec2(int(screenCoordsFloat.x), int(screenCoordsFloat.y));
		
			ivec2 tex_size = imageSize(depth_buffer).xy;
			float tex_x = float(gl_FragCoord.x)/float(tex_size.x);
			float tex_y = float(gl_FragCoord.y)/float(tex_size.y);
			vec2 tex_coord = vec2(tex_x, tex_y);
						
			float depthFF = f_depth;//no need to load the depth since we know the depth
			//imageLoad(depth_buffer, ivec2(gl_FragCoord.x, gl_FragCoord.y)).x;
			
			float pseudodepthFF = gaussianFilter(depth_buffer, screenCoords, 2);
						
			float deltaD = pseudodepthFF - depthFF; //difference average surrounding depth and particle depth
			float shadow = 0.0;

			if(deltaD<0.000)
			{
				shadow = 1.0-1.5*abs(deltaD);
			}
			
			float weight = radius-length(tc); //make the shadows dissapear further away from the particle
			fragColor.rgba = vec4(shadow,shadow,shadow,color.a*0.1*weight*-depth_darkening_amount);
			
			if(depth_darkening_light != 0)
			{
				if(length(tc)<radius-contour_fraction/3.0)
				{
					float value = 1.0-round((depth_darkening_amount+1.0)/2.0);
					fragColor.rgba = vec4(value,value,value,color.a);					
				}
			}
		}
		else
		{
			discard;	
		}
	}
}