#version 400
#extension GL_EXT_geometry_shader4 : enable
 
layout(triangles) in;
layout(triangle_strip, max_vertices = 4) out; 
 
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform float particle_size;

uniform int particle_type;

uniform float znear;
uniform float zfar;

uniform int depth_darkening;

uniform int glyph_type;

in vec3 viewVec[];

out float f_depth;
 
out vec2 tc;
 
out vec4 color;
 
void drawQuad(vec4 p1, vec4 n1, vec4 n2, float width)
{
  gl_Position = projectionMatrix * modelViewMatrix * (p1-width*n1-width*n2); 
  tc=vec2(-1,-1);
  EmitVertex();
 
  gl_Position = projectionMatrix * modelViewMatrix * (p1-width*n1+width*n2); 
  tc=vec2(-1,1);
  EmitVertex();
 
  gl_Position = projectionMatrix * modelViewMatrix * (p1+width*n1-width*n2); 
  tc=vec2(1,-1);
  EmitVertex();
 
  gl_Position = projectionMatrix * modelViewMatrix * (p1+width*n1+width*n2); 
  tc=vec2(1,1);
  EmitVertex();
 
  EndPrimitive();
}

float glyph_dashed(float t)
{
	if(t<0.5)
		return 1.0;
		
	return 0.0;
}

float glyph_arrow(float t)
{
	if(t<0.5)
		return (t)*2.0;
		
	return 0.25;
}

float glyph_tadpole(float t)
{
	return 1.0-t;
}

float glyph_ellipsoid(float t)
{
	if((t>0.0 && t<0.25) || (t>0.5 && t<0.75))
		return 1.0;
		
	return 0.0;
}
 
void main()
{          	
    float width= particle_size/2.0;
    		
    vec4 p = vec4(gl_in[0].gl_Position.xyz,1);
	
	color.rgb = gl_in[1].gl_Position.rgb;	
	
	color.a = gl_in[2].gl_Position.r; //get alpha channel	
	f_depth = gl_in[2].gl_Position.g; //get depth
	float rel_age = gl_in[2].gl_Position.b; //get relative age
	
    vec3 tangent  = vec3(1,0,0);
    vec4 shiftDir  = vec4(vec3(normalize(cross(tangent, viewVec[0]))),0);
    vec4 shiftDir2 = vec4(vec3(normalize(cross(shiftDir.xyz, viewVec[0]))),0);
	
	//Update the width according to the selected glyph
	if(glyph_type != 0)
	{	
		rel_age = rel_age - floor(rel_age);
		if(glyph_type == 1)
		{
			width = width*glyph_dashed(rel_age);
		}
		
		if(glyph_type == 2)
		{
			width = 1.5*width*glyph_arrow(rel_age);
		}
		
		if(glyph_type == 3)
		{
			width = 1.25*width*glyph_tadpole(rel_age);
		}
		
		if(glyph_type == 4)
		{
			width = width*glyph_ellipsoid(rel_age);
		}
	}

    drawQuad(p,shiftDir,shiftDir2,width);               
}