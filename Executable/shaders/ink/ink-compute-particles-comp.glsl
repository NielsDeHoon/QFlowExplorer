#version 310 es
#define M_PI 3.14159265358979323846

#extension GL_EXT_gpu_shader4 : enable

precision highp sampler3D; //defines the precision of the sampler data

layout(std430) buffer;

//declare inputs:
uniform sampler3D 	flowData;			//read only (in cm/s), all velocity fields stored in one texture(atlas texture) stacked in the z dimension of the texture
uniform sampler3D 	snrData;			//read only, signal strength (x component) and sigma/noise level (y component)

uniform sampler3D 	meshData;			//read only, contains the mesh signed distance field
uniform sampler3D 	particleSinkData;	//read only, contains the per voxel particle sink id, 0 if no sink covers the voxel

uniform ivec3    	volumeDimensions; 	//read only, size of one velocity field (required to decode the velocity (flowData) atlas texture)
uniform int 		phaseCount;			//read only, total number of phases
uniform int	  		particleCount;		//read only
uniform float		stepSize;			//read only, in phases
uniform float 	  	phaseDuration;		//read only, in seconds
uniform mat4      	patientVoxelMatrix;	//read only, P2V matrix (patient to voxel)
uniform float 		CFL;				//read only
uniform int			advectionType;		//read only (Stream, path, streak)
uniform float 		venc;				//read only(in m/s)

uniform int deleteOutsideMesh; //read only, determines whether particles should be removed if they lay outside the mesh

struct Particle
{
  float position_x;
  float position_y;
  float position_z;
  
  float age;
  float seedTime;
  
  float use_uncertainty;
  float deviation;

  float initial_voxel_x;
  float initial_voxel_y;
  float initial_voxel_z;

  float deletion_reason; //-1 if the particle is not marked as "to be deleted", otherwise >= 0
	
  float speed;
  float padding0;
  float padding1;
  float padding2;
  float padding3;
} particle;

layout(std430, binding=5) buffer Particles //for reading and writing
{
	Particle particles[];
};

uint LOCAL_X = 32u;
uint LOCAL_Y = 1u;
uint LOCAL_Z = 1u;

//declare general variables:
layout (local_size_x = 32, local_size_y = 1, local_size_z = 1) in;

//----------------------------------------------------------------------------
// pos  - position
//----------------------------------------------------------------------------
vec3 TransformPosition(vec3 pos, mat4 matrix)
{
  return (matrix * vec4(pos,1.0)).xyz;
}

//----------------------------------------------------------------------------
// vec  - vector
//----------------------------------------------------------------------------
vec3 TransformVector(vec3 vec, mat4 matrix)
{
  return (matrix * vec4(vec,0.0)).xyz;
}

float random(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float GaussianNoise(float mu, float sigma, vec2 seed)
{
	const float epsilon = 1e-12; //min float
	const float two_pi = 2.0*M_PI;

	float z0, z1;
	
	float u1, u2;
	float loop_id = 1.0;
	do
	{
		u1 = random(vec2(seed.x,loop_id+sin(seed.y)*seed.y));
		u2 = random(vec2(loop_id-seed.y,cos(seed.x)*seed.x));
		loop_id = loop_id + 1.0;
	}
	while(u1<epsilon);//too small values make the log function return NaN

	z0 = sqrt(-2.0 * log(u1)) * cos(two_pi * u2);
	z1 = sqrt(-2.0 * log(u1)) * sin(two_pi * u2);
	return z0 * sigma + mu;
}

// phase - phase from which the velocity should be loaded
// pos   - in voxel coordinates
// returns velocity in patient coordinates
vec4 FetchVelocityLineary(vec3 pos, int phase, bool use_uncertainty)
{    
  if(phase>=phaseCount)
	phase = phase % phaseCount;
  
  vec3 position;
  position.x = pos.x;
  position.y = pos.y;
  position.z = pos.z + float(phase) * float(volumeDimensions.z);
  
  ivec3 tex_size = textureSize(flowData, 0).xyz;
  
  vec3 texPos;
  texPos.x = position.x/float(tex_size.x);
  texPos.y = position.y/float(tex_size.y);
  texPos.z = position.z/float(tex_size.z);
  
  vec4 velocity = vec4(texture3D(flowData,texPos).xyz,0);
      
  if(use_uncertainty)
  {
	float A = texture3D(snrData,texPos).x;
	float sigma = texture3D(snrData,texPos).y;
	float variance = ((2.0*(sigma*sigma))/(A*A))*((venc*venc)/(M_PI*M_PI));
			
	//float dev = sqrt(variance)*100.0; //convert from m/s to cm/s
	float dev = variance*100.0; //convert from m/s to cm/s
	if(dev > venc * 100.0)
		dev = venc * 100.0;
			
	vec4 velocity_old = velocity.xyzw;	
	
	velocity.x = GaussianNoise(velocity.x, dev, vec2(texPos.y+1.0,texPos.z+1.0));
	velocity.y = GaussianNoise(velocity.y, dev, vec2(texPos.x+1.0,texPos.z+1.0));
	velocity.z = GaussianNoise(velocity.z, dev, vec2(texPos.y+1.0,texPos.x+1.0));
	
	velocity.w = length(velocity_old-velocity)/100.0; //convert from cm/s to m/s
	
	return velocity;
  }
    
  return velocity;
}

// phase - phase from which the velocity should be loaded
// pos   - in voxel coordinates
// returns velocity in patient coordinates
vec4 FetchVelocityLinearTemporalLinear(vec3 pos, float phase, bool use_uncertainty)
{    
  int t1 = int(floor(phase));
  int t2 = t1+1;
  float t = phase - float(t1);
  
  vec4 v1 = FetchVelocityLineary(pos, t1, use_uncertainty).xyzw;
  vec4 v2 = FetchVelocityLineary(pos, t2, use_uncertainty).xyzw;
    
  // Linear interpolation
  vec4 v = mix(v1, v2, t).xyzw;
  return v;
}

//position: position of the particle
//time: current time of the particle
//h: step size in phases
vec4 rk4(vec3 position, float time, float h, bool use_uncertainty)
{
	float dt = (h * phaseDuration) / 1000.0; //convert step size from phases to seconds
	vec3 p = TransformPosition(position.xyz, patientVoxelMatrix).xyz;

	float t = time;

	// Get the velocity in patient coordinates (cm/s)
	vec4 vi = FetchVelocityLinearTemporalLinear(p.xyz,t, use_uncertainty);

	vec4 k1 = 10.0*vi*dt;    
	vec3 p1 = position + 0.5*k1.xyz;
	float t1   = t + 0.5*h;
	if(advectionType == 0) //stream advection
		t1 = t;
	
	p = TransformPosition(p1.xyz, patientVoxelMatrix).xyz;  
	vec4 vi_1 = FetchVelocityLinearTemporalLinear(p.xyz,t1, use_uncertainty);
	
	vec4 k2 = 10.0*vi_1*dt;
	vec3 p2 = position + 0.5*k2.xyz;
	float t2 = t + 0.5*h;
	if(advectionType == 0) //stream advection
		t2 = t;

	p = TransformPosition(p2.xyz, patientVoxelMatrix).xyz;  
	vec4 vi_2 = FetchVelocityLinearTemporalLinear(p.xyz,t2, use_uncertainty);   
	
	vec4 k3 = 10.0*vi_2*dt;
	vec3 p3 = position + k3.xyz;
	float t3 = t + h;
	if(advectionType == 0) //stream advection
		t3 = t;
	
	p = TransformPosition(p3.xyz, patientVoxelMatrix).xyz; 
	vec4 vi_3   = FetchVelocityLinearTemporalLinear(p.xyz,t3, use_uncertainty);  
	
	vec4 k4 = 10.0*vi_3*dt;
	
	// New position in patient coordinates (mm)
	vec4 result;
	result.xyz = position.xyz;
	result.w = 0.0;
	return result + (k1 + k2*2.0 + k3*2.0 + k4)*(1.0/6.0);
}

void main()
{
	uint ident = gl_GlobalInvocationID.x;
	
	if (ident < uint(particleCount))
	{		
		vec3 pos = vec3(particles[ident].position_x, particles[ident].position_y, particles[ident].position_z);	//patient coords
		float age  = particles[ident].age;			//in phases
		float seed_time = particles[ident].seedTime; //in phases
		bool use_uncertainty = particles[ident].use_uncertainty > 0.5;
		
		float time = seed_time+age;		//in phases
		
		float dt = stepSize;
		
		bool force_run = false;
				
		if(age<0.0 && abs(age) < stepSize)
		{
			dt = stepSize - abs(age);
			time = seed_time;
			
			force_run = true; //make sure it is actually run in this special case
		}
		
		if(age+dt>0.0 || force_run)
		{
			float processed = 0.0f;
			float t_step = CFL;
			bool finished = false;
			
			vec4 newPos = vec4(pos,0);
			
			if(particles[ident].deletion_reason >= -0.5) //particle is previously marked to be deleted
			{
				finished = true;
			}
			
			while(!finished)
			{
				t_step = CFL;				
								
				if(processed + t_step >= dt)
				{
					t_step = dt - processed;
					finished = true;
				}
								
				vec4 update;
				if(advectionType == 0) //stream advection
				{
					update = rk4(newPos.xyz, seed_time, t_step, use_uncertainty).xyzw;
				}
				else
				{
					update = rk4(newPos.xyz, time+processed, t_step, use_uncertainty).xyzw;
				}
				newPos.xyz = update.xyz; //position should be updated
				newPos.w += update.w; //deviation should be added to previous deviation
				
				vec3 voxelPosition = TransformPosition(newPos.xyz, patientVoxelMatrix).xyz; 
 				
				ivec3 meshDataSize = textureSize3D(meshData, 0).xyz;
				//check whether particles outside the mesh should be removed, and whether the mesh data exists
				if(deleteOutsideMesh != 0 && meshDataSize.x > 1 && meshDataSize.y > 1 && meshDataSize.z > 1)
				{
					  vec3 meshTexPos;
					  meshTexPos.x = voxelPosition.x/float(meshDataSize.x);
					  meshTexPos.y = voxelPosition.y/float(meshDataSize.y);
					  meshTexPos.z = voxelPosition.z/float(meshDataSize.z);
  
					//check whether the particle lays outside the mesh, if so mark it as to be deleted
					float sdf = texture3D(meshData, meshTexPos).x; //read signed distance from meshData texture
					if(sdf < 0.0)
					{
						particles[ident].deletion_reason = 0.0;
						
						finished = true;
					}
				}

				ivec3 particleSinkDataSize = textureSize3D(particleSinkData, 0).xyz;
				//check whether particle sink data exists
				if(particleSinkDataSize.x > 1 && particleSinkDataSize.y > 1 && particleSinkDataSize.z > 1)
				{
					vec3 particleSinkTexPos;
					particleSinkTexPos.x = voxelPosition.x/float(particleSinkDataSize.x);
					particleSinkTexPos.y = voxelPosition.y/float(particleSinkDataSize.y);
					particleSinkTexPos.z = voxelPosition.z/float(particleSinkDataSize.z);
					
					float value = texture3D(particleSinkData, particleSinkTexPos).x; //read signed distance from meshData texture
					if(value > 0.5)
					{
						particles[ident].deletion_reason = value;
						
						finished = true;
					}
				}
								
				processed += t_step;
			}
		
			particles[ident].position_x = newPos.x;
			particles[ident].position_y = newPos.y;
			particles[ident].position_z = newPos.z;
			particles[ident].deviation += newPos.w;
		}
		
		particles[ident].age = age + stepSize;//age in phases
		
		vec3 finalPos = vec3(particles[ident].position_x, particles[ident].position_y, particles[ident].position_z);
		vec3 positionTransformed = TransformPosition(finalPos.xyz, patientVoxelMatrix).xyz;
		vec3 particleVelocity = FetchVelocityLinearTemporalLinear(positionTransformed.xyz, time, false).xyz;
		particles[ident].speed = length(particleVelocity)/(venc*100.0);
	}
}