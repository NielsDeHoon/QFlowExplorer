// mip-frag.glsl
uniform sampler3D mipDataSet;
uniform vec2      mipDataRange;
uniform int       mipDataComponent;
uniform int       mipDataRepresentation;
uniform sampler1D transferFunction;
uniform sampler2D rayStart;
uniform sampler2D rayEnd;
uniform sampler2D geoColor;
uniform sampler2D geoDepth;
uniform int       geoAvailable;
uniform int       mode; 
uniform float     scalarSize;
uniform vec3      scaling;   // dimensions * voxel size (mm)
uniform float     stepSize;  // minimum of voxel size (mm)
uniform float     contrast;
uniform float     brightness;
uniform int       gradient;
uniform int       transparency;


// This shader only deals with scalar data
// For vector data is will use the magnitudes (alpha component)
const   int       SCALARS  = 0;
const   int       VECTOR   = 1;

varying vec4 pos;

//----------------------------------------------------------------------------
vec3 FetchVectorGradient(sampler3D vol, vec3 pos, vec3 step);

//----------------------------------------------------------------------------
vec3 NormalizeRange(vec3 value, vec2 range);

//----------------------------------------------------------------------------
void main()
{
  vec3  dataGradient;
  vec4  dataSample;
  float dataValue; 
  vec2  dataRange;
  float maximumValue;
  float actualValue;
  float normalizedValue;
  vec3  classifiedValue;  
  
  vec3  ray;  
  vec3  rayVector;
  vec3  rayDir;
  float rayMaxLength; 
  float rayOffset;
  
  vec3  geoC;
  float geoD;
  vec3  color;
  float alpha;
  
  // Compute normalized device coordinate
  vec2 texCoord =  ((pos.xy / pos.w) + 1.0) / 2.0;
  
  // Determine color and depth for geometry
  if(geoAvailable == 1)
  {
    geoC = texture2D(geoColor, texCoord.xy).rgb;
    geoD = texture2D(geoDepth, texCoord.xy).r;
  }
  else
  {
    geoC = vec3(1.0);
    geoD = 1.0;
  }

  // Determine start and end of rays
  vec3 start      = texture2D(rayStart, texCoord.xy).rgb;
  vec3 end        = texture2D(rayEnd,   texCoord.xy).rgb;

  start           = clamp(start, 0.0, 1.0) * scaling;
  end             = clamp(end,   0.0, 1.0) * scaling;

  ray             = start;  

  rayVector       = end - start;
  rayDir          = normalize(rayVector);
  rayMaxLength    = length(rayVector); 
  rayOffset       = 0.0;

  // Initialize at the minimum of the data range
  maximumValue    = mipDataRange.x; 
  actualValue     = 0.0;
  normalizedValue = 0.0;

  int i;
  for(i=0; i < 400; i++)
  {
    // Determine if ray hit opaque geometry
    // Determine if ray is within bounding box  
    if((geoD != 1.0) || (rayOffset >= rayMaxLength))
    {
      break;
    }    
    
    dataSample = texture3D(mipDataSet, ray / scaling);    
    dataValue  = 0.0;
    
    //dataGradient = FetchVectorGradient(mipDataSet, ray/scaling, vec3(1.0)/scaling);
    if(gradient == 1)
    {
      dataGradient = FetchVectorGradient(mipDataSet, ray/scaling, vec3(1.0)/scaling);
      dataGradient = dataGradient - (dot(dataGradient,rayDir)*rayDir);
      dataSample   = vec4(dataGradient,1.0);
    }
        
    if(mode == VECTOR)
    {    
      if(mipDataComponent == 0) dataValue = length(dataSample.rgb);
      if(mipDataComponent == 1) dataValue = dataSample.r;
      if(mipDataComponent == 2) dataValue = dataSample.g;
      if(mipDataComponent == 3) dataValue = dataSample.b;
    }
    else
      dataValue       = length(dataSample.rgb);

    if(abs(dataValue) > maximumValue) 
    {
      maximumValue = abs(dataValue);
      actualValue  = dataValue;
    }
    
    ray        = ray      + rayDir * stepSize;
    rayOffset += stepSize;
  }
  
  if(geoD == 1.0)
  {  
    // We have a ray, determine the correct intensity / color
    
    // Determine the correct range for the current data mode
    dataRange.xy = mipDataRange.xy;
    if(mode == VECTOR && mipDataComponent == 0) 
      dataRange = vec2(0.0, max(abs(mipDataRange.x),abs(mipDataRange.y))); 

    // Scale data to [0,1] range
    normalizedValue = NormalizeRange(vec3(actualValue), dataRange).r;
    
    // Upscale if we have a gradient
    if(gradient == 1)
      normalizedValue = clamp(pow(normalizedValue+1.0,2.0)-1.0, 0.0, 1.0);

    // Scale the contrast [-1,1] to [0,inf)
    float c = contrast;
    if   (c == 1.0) c = 0.9999999;
    if   (c <= 0.0) c = c + 1.0;
    else  c = 1.0 + (1.0/(1.0-c));       

    // Color TF
    if(mipDataRepresentation == 1) 
    {   
      color  = texture1D(transferFunction, normalizedValue).rgb;  
    }
    // Gray
    else 
    {     
      normalizedValue = (normalizedValue - 0.5) * c + 0.5 + brightness;         
      color =  vec3(normalizedValue);
    }
  }
  else
  {
    // We have opaque geometry, use it's color
    color = geoC;
  }  
    
  alpha = 1.0;
  if(transparency == 1)
  {
    // Apply transparency
    alpha = pow(normalizedValue,2.0) * (3.0 - 2.0 * normalizedValue);
    if(alpha < 0.05) alpha = 0.0;
  }
  
  gl_FragColor = vec4(color, alpha);      
  //gl_FragColor = vec4(rayVector.xyz, 1.0);      
  //gl_FragColor = vec4(geoD, 1.0);      
  //gl_FragColor = vec4(start, 1.0);      
}
