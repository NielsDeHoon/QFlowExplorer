#version 120

uniform sampler1D lineTransferFunction;

uniform int       clusterMasking;
uniform float     lengthMasking;
uniform vec4      light;
uniform vec4      lightDirection;
uniform vec2      dataRange;
uniform int       colorScale;
uniform int       colorType;
uniform vec3      colorUni;
uniform int       lineShading;

varying in vec3   tangent;
varying in vec4   attribs;

//----------------------------------------------------------------------------
vec4 PhongLine(in vec4 col, in vec3 l, in vec3 t, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
void main()
{ 
  vec4  eye, lightDir, color, shaded;
  float speed;
  
  if(gl_Color.a == 0.0) discard;
  
  eye         = normalize( gl_ModelViewMatrixInverse*vec4(0.0,0.0,1.0,0.0)); 
  lightDir    = vec4(normalize( gl_ModelViewMatrix * -lightDirection).xyz, 0.0);  


  if((clusterMasking == 1) && (attribs.y <= 0.0)) discard;
  if(attribs.z <= lengthMasking)                  discard;

  color = vec4(colorUni, 1.0);
  
  if(colorType == 1) //speed 
  {
    speed    = attribs.x / max(abs(dataRange.x), abs(dataRange.y));
    speed    = speed * (float(colorScale)/100.0);
    
    color    = texture1D(lineTransferFunction, speed);
  }
  
  shaded   = color;
  if(lineShading < 3)
    shaded   = PhongLine(color, lightDir.xyz, tangent.xyz, eye.xyz, light.x, light.y, vec2(light.z, light.w));  
  shaded.a = 1.0;  
  
  gl_FragColor = shaded;  
}