#version 120

uniform sampler1D lineTransferFunction;

uniform float     phaseCurrent;
uniform int       phaseCount;
uniform int       clusterMasking;
uniform float     lengthMasking;
uniform vec4      light;
uniform vec4      lightDirection;
uniform vec2      dataRange;
uniform int       colorScale;
uniform int       lineShading;
uniform int       colorType;

varying in float  radiusDepth;
varying in vec3   binormal;
varying in vec2   texcoord;
varying in vec4   attribs;
varying in float  phase;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
vec4 Desaturate(vec4 color, float desaturation)
{
  const vec3 gray_conv = vec3(0.30, 0.59, 0.11);
 
  vec3 gray = vec3(dot(gray_conv, color.rgb));
  
  return vec4(mix(color.rgb, gray, desaturation), 1.0);
}

//----------------------------------------------------------------------------
int InTemporalWindow(float timePoint, float current, float scope, int phaseCount)
{
  int result = 0;
  
  float t  = timePoint;
  float c  = current;
  float s  = scope;
  float pc = float(phaseCount);
  
  // map current phase in the temporal range
  if(t < 0.0) t = pc - mod(abs(t),pc);
  if(t > pc)  t = mod(t, pc);
  
  // check wrap around 0
  if((c>=0) && (c<0.5*s))
  {
    if(((t >  pc+c-0.5*s) && (t <= pc)) ||
       ((t >= 0.0)        && (t <  c+0.5*s)) )
       result = 1;
  }
  else
  // check wrap around at phase count
  if((c>pc-0.5*s) && (c<=pc))
  {
    if(((t >  c-0.5*s) && (t <= pc)) ||
       ((t >= 0.0)     && (t <  c+0.5*s-pc)) )
       result = 1;
  }
  else
  // check normal window behavior    
  if((t > c-0.5*s) && (t < c+0.5*s))
    result = 1;
    
  return result;
}

//----------------------------------------------------------------------------
void main()
{ 
  const float delta = 0.1;
  
  vec4  eye, normal, lightDir, color, shaded;
  float depth, yCoordNorm1, yCoordNorm2;    
  float speed;   
  
  // discard fragments if necessary
  if((clusterMasking == 1) && (attribs.y <= 0.0)) discard;
  if(attribs.z <= lengthMasking)                  discard;
  
  // determine distance to center    
  yCoordNorm1 = (texcoord.y*2.0)-1.0;
  yCoordNorm2 = 1.0 - abs(yCoordNorm1);
 
  // initialize vectors
  eye         = normalize( gl_ModelViewMatrixInverse*vec4(0.0,0.0,1.0,0.0));   
  //lightDir    = normalize( vec4((gl_ModelViewMatrix * -lightDirection).xyz, 0.0) );    
  lightDir    = normalize( vec4((-lightDirection).xyz, 0.0) );    
  normal      = normalize( vec4(binormal.xy * yCoordNorm1, yCoordNorm2, 0.0) );

  // set the depth  
  depth = gl_FragCoord.z + (1.0-yCoordNorm2)*(radiusDepth-gl_FragCoord.z);
  
  // apply procedural arrow texture
  color = gl_Color;
  if(texcoord.x < 0.5 && texcoord.y < 0.5) if(((texcoord.x)+(0.5- texcoord.y)     ) < 0.5) color = vec4(color.rgb-vec3(0.2),1.0);
  if(texcoord.x < 0.5 && texcoord.y > 0.5) if(((texcoord.x)+(     texcoord.y-0.5) ) < 0.5) color = vec4(color.rgb-vec3(0.2),1.0);  
  
  // apply shading
  shaded = color;  
  if(lineShading  < 3)
    shaded = Phong(color, lightDir.xyz, normal.xyz, eye.xyz, light.x, light.y, vec2(light.z,light.w));  
  shaded.a = 1.0;  
  
  // determine time visibility
  float scope = 2.0;
  
  if((colorType == 3) || (colorType == 4))
  {
    //phaseleft  = phaseCurrent - 0.5*scope;
    //phaseright = phaseCurrent + 0.5*scope;   
    
    //if((current > phaseright) || (current < phaseleft) ) 
    if(InTemporalWindow(phase, phaseCurrent, scope, phaseCount) == 0)
    {
      if(colorType == 3)  discard;
      if(colorType == 4)
      {
        color = vec4(0.5,0.5,0.5,1.0);
        if(texcoord.x < 0.5 && texcoord.y < 0.5) if(((texcoord.x)+(0.5- texcoord.y)     ) < 0.5) color = vec4(color.rgb-vec3(0.2),1.0);
        if(texcoord.x < 0.5 && texcoord.y > 0.5) if(((texcoord.x)+(     texcoord.y-0.5) ) < 0.5) color = vec4(color.rgb-vec3(0.2),1.0);    
        shaded = Phong(color, lightDir.xyz, normal.xyz, eye.xyz, light.x, light.y, vec2(light.z,light.w));
      }
    }      
  }  
  
  gl_FragColor = shaded;  
}