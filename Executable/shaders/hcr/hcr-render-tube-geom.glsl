#version 120
#extension GL_EXT_gpu_shader4 : enable
#extension GL_EXT_geometry_shader4 : enable

uniform sampler1D lineTransferFunction;

uniform float phaseCurrent; // keep counting after wrap-around
uniform float phaseStart;
uniform float phaseEnd;
uniform int   phaseCount;
uniform mat4  patientVoxelMatrix;
uniform vec3  volumeDimensions;
uniform float lineWidth;

uniform vec2  dataRange;
uniform int   colorScale;
uniform int   colorType;
uniform vec3  colorUni;

varying in vec4 position[4];
varying in vec4 attributes[4];

varying out float radiusDepth;
varying out vec3  binormal;
varying out vec2  texcoord;     // (x = arclength, y = left-right)
varying out float phase;
varying out vec4  attribs;

//----------------------------------------------------------------------------
void main()
{   
  const float delta = 0.001;

  vec3  eye, eyeWorld;
  vec4  posScreen;
  vec4  strip[4];
  vec4  lineEye[4];
  vec3  lineTangentAdj[2], lineNormalAdj[2];
  vec3  lineTangentEye[2], lineNormalEye[2];  
  vec4  vertexColor[2];
  
  float speed[2];  
  
  // initialize
  eye            = vec3(0.0,0.0,1.0);        
  eyeWorld       = normalize( gl_ModelViewMatrixInverse*vec4(0.0,0.0,1.0,0.0)).xyz;   
  
  // we assume to get a line with adjacency information
  // at this point we mainly compute the tangent for lighting
  // and pass on the vertex attributes
  
  // we assume to get a line with adjacency information  
  // we calculate the imposters in screen space  
  lineEye[0] = gl_ModelViewMatrix * vec4(position[0].xyz,1.0);
  lineEye[1] = gl_ModelViewMatrix * vec4(position[1].xyz,1.0);
  lineEye[2] = gl_ModelViewMatrix * vec4(position[2].xyz,1.0);
  lineEye[3] = gl_ModelViewMatrix * vec4(position[3].xyz,1.0);
  
   // compute tangent          
  lineTangentEye[0] = normalize(lineEye[1].xyz - lineEye[0].xyz); 
  lineTangentEye[1] = normalize(lineEye[2].xyz - lineEye[1].xyz);
  
  lineTangentAdj[0] = normalize(position[1].xyz - position[0].xyz); 
  lineTangentAdj[1] = normalize(position[2].xyz - position[1].xyz); 
  
  // correct tangent of the first part of the line
  if(length(lineEye[1] - lineEye[0]) < delta)    
  {
    lineTangentEye[0]  = lineTangentEye[1];
    lineTangentAdj[0]  = lineTangentAdj[1];
  }
    
  // compute normal  
  lineNormalEye[0]  = normalize(cross(eye, lineTangentEye[0]));
  lineNormalEye[1]  = normalize(cross(eye, lineTangentEye[1]));
  
  lineNormalAdj[0]  = normalize(cross(eyeWorld, lineTangentAdj[0]));
  lineNormalAdj[1]  = normalize(cross(eyeWorld, lineTangentAdj[1]));
 
  // compute color
  vertexColor[0] = vec4(colorUni,1.0);
  vertexColor[1] = vec4(colorUni,1.0);
  
  if(colorType == 1 || colorType ==3 || colorType == 4) // speed
  {
    speed[0]       = attributes[1].x / max(abs(dataRange.x), abs(dataRange.y));
    speed[0]       = speed[0] * (float(colorScale)/100.0);
    speed[1]       = attributes[2].x / max(abs(dataRange.x), abs(dataRange.y));
    speed[1]       = speed[1] * (float(colorScale)/100.0);
      
    vertexColor[0] = texture1D(lineTransferFunction, speed[0]);
    vertexColor[1] = texture1D(lineTransferFunction, speed[1]);
  } 
  
  if(colorType == 2) // time
  {
    vertexColor[0] = texture1D(lineTransferFunction, position[1].w/float(phaseCount));
    vertexColor[1] = texture1D(lineTransferFunction, position[2].w/float(phaseCount));
  }
  
  // create the strip
  strip[0] = vec4(lineEye[1].xyz - 0.5*lineWidth*lineNormalEye[0], 1.0);
  strip[1] = vec4(lineEye[1].xyz + 0.5*lineWidth*lineNormalEye[0], 1.0);
  strip[2] = vec4(lineEye[2].xyz - 0.5*lineWidth*lineNormalEye[1], 1.0);
  strip[3] = vec4(lineEye[2].xyz + 0.5*lineWidth*lineNormalEye[1], 1.0);
  
  // render the strip  
  posScreen     = gl_ProjectionMatrix * (strip[0] + vec4(0.0,0.0,0.5*lineWidth,0.0));
  radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
  binormal      = lineNormalAdj[0];
  texcoord      = vec2(0.0,0.0);    
  attribs       = attributes[1];
  phase         = position[1].w;
  gl_FrontColor = vertexColor[0];
  gl_Position   = gl_ProjectionMatrix * strip[0];     
  EmitVertex();

  posScreen     = gl_ProjectionMatrix * (strip[1] + vec4(0.0,0.0,0.5*lineWidth,0.0));
  radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
  binormal      = lineNormalAdj[0];
  texcoord      = vec2(0.0,1.0);
  attribs       = attributes[1];
  phase         = position[1].w;
  gl_FrontColor = vertexColor[0];
  gl_Position   = gl_ProjectionMatrix * strip[1];     
  EmitVertex();

  posScreen     = gl_ProjectionMatrix * (strip[2] + vec4(0.0,0.0,0.5*lineWidth,0.0));
  radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
  binormal      = lineNormalAdj[1];
  texcoord      = vec2(1.0,0.0);
  attribs       = attributes[2];
  phase         = position[2].w;
  gl_FrontColor = vertexColor[1];
  gl_Position   = gl_ProjectionMatrix * strip[2];     
  EmitVertex();

  posScreen     = gl_ProjectionMatrix * (strip[3] + vec4(0.0,0.0,0.5*lineWidth,0.0));
  radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
  binormal      = lineNormalAdj[1];
  texcoord      = vec2(1.0,1.0);
  attribs       = attributes[2];
  phase         = position[2].w;
  gl_FrontColor = vertexColor[1];
  gl_Position   = gl_ProjectionMatrix * strip[3];     
  EmitVertex();   
    
}