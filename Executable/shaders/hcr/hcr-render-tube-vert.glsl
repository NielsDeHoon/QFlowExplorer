#version 120
#extension GL_EXT_gpu_shader4 : enable

varying in vec4 qfe_Position;
varying in vec4 qfe_Attribute;

varying out vec4 position;
varying out vec4 attributes;

//----------------------------------------------------------------------------
void main(void)
{      
   // gl_Vertex.xyz - location in patient coordinates
   // gl_Vertex.w   - time in phases
   gl_Position = gl_Vertex;
   
   position    = qfe_Position;
   attributes  = qfe_Attribute;  
}
