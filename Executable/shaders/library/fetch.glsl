#version 330

//----------------------------------------------------------------------------
vec3 TransformPosition(vec3 pos, mat4 matrix);

//----------------------------------------------------------------------------
// Fetch from transfer function
// tf    - transfer function
// value - [0,1]
vec3 FetchTransferFunction(sampler1D tf, float value)
{
  return texture(tf, value).rgb;
}

//----------------------------------------------------------------------------
// vol  - assumes linearly sampled texture
// dims - dimensions of the volume 
// pos  - in texture coordinates
// returns the gradient in a scalar volume
//----------------------------------------------------------------------------
vec3 FetchScalarGradient(sampler3D vol, vec3 pos, float step)
{
  vec3 gradient;
  vec3 posTex[8];             
  
  if(all(greaterThan(pos, vec3(step))) && all(lessThan(pos, vec3(1.0-step))))
  {
    posTex[0] = pos + vec3( step, 0.0,  0.0);
    posTex[1] = pos + vec3(-step, 0.0,  0.0);
    posTex[2] = pos + vec3( 0.0, step,  0.0);
    posTex[3] = pos + vec3( 0.0,-step,  0.0);
    posTex[4] = pos + vec3( 0.0,  0.0, step);
    posTex[5] = pos + vec3( 0.0,  0.0,-step);
    
    gradient.x = 0.5 * (texture(vol, posTex[0]).r - texture(vol, posTex[1]).r);  
    gradient.y = 0.5 * (texture(vol, posTex[2]).r - texture(vol, posTex[3]).r);  
    gradient.z = 0.5 * (texture(vol, posTex[4]).r - texture(vol, posTex[5]).r);  
  }
  else
    gradient = vec3(0.0);  
  
  return gradient;
}

//----------------------------------------------------------------------------
// vol  - assumes linearly sampled texture
// dims - dimensions of the volume 
// pos  - in voxel coordinates
// returns the gradient in a vector volume (by taking the norm)
//----------------------------------------------------------------------------
vec3 FetchVectorGradient(sampler3D vol, vec3 pos, vec3 step)
{
  vec3 gradient;
  vec3 posTex[8];             
  
  //if(all(greaterThan(pos, vec3(step))) && all(lessThan(pos, vec3(1.0-step))))
  //{
    posTex[0] = pos + vec3( step.x, 0.0,  0.0);
    posTex[1] = pos + vec3(-step.x, 0.0,  0.0);
    posTex[2] = pos + vec3( 0.0, step.y,  0.0);
    posTex[3] = pos + vec3( 0.0,-step.y,  0.0);
    posTex[4] = pos + vec3( 0.0,  0.0, step.z);
    posTex[5] = pos + vec3( 0.0,  0.0,-step.z);
    
    gradient.x = 0.5 * (length(texture(vol, posTex[0]).xyz) - length(texture(vol, posTex[1]).xyz));  
    gradient.y = 0.5 * (length(texture(vol, posTex[2]).xyz) - length(texture(vol, posTex[3]).xyz));  
    gradient.z = 0.5 * (length(texture(vol, posTex[4]).xyz) - length(texture(vol, posTex[5]).xyz));
  //}
  //else
  //  gradient = vec3(0.0); 
  
  return gradient;
}

//----------------------------------------------------------------------------
// vol  - assumes linearly sampled texture
// dims - dimensions of the volume 
// pos  - in voxel coordinates
// returns velocity in patient coordinates
//----------------------------------------------------------------------------
vec3 FetchVelocityLinear(sampler3D vol, vec3 pos, mat4 v2t)
{
  vec3 velocity;
  vec3 posTex;             
  
  posTex   = TransformPosition(round(pos), v2t);
  velocity = texture(vol, posTex).xyz;  
  
  return velocity;
}

//----------------------------------------------------------------------------
// vol  - assumes linearly sampled texture
// dims - dimensions of the volume 
// pos  - in voxel coordinates
// returns velocity in patient coordinates
//----------------------------------------------------------------------------
vec3 FetchVelocityLinearSeparate(sampler3D vol, vec3 pos, mat4 v2t)
{
  vec3  velocity[7];
  vec3  direction;
  float magnitude;
  vec3  posVoxel, posTex;  

  posVoxel = round(pos);
 
  velocity[0] = texture(vol, TransformPosition(pos, v2t)).xyz;  
  velocity[1] = texture(vol, TransformPosition(pos + vec3( 1.0, 0.0, 0.0),v2t)).xyz;  
  velocity[2] = texture(vol, TransformPosition(pos + vec3(-1.0, 0.0, 0.0),v2t)).xyz;  
  velocity[3] = texture(vol, TransformPosition(pos + vec3( 0.0, 1.0, 0.0),v2t)).xyz;  
  velocity[4] = texture(vol, TransformPosition(pos + vec3( 0.0,-1.0, 0.0),v2t)).xyz;  
  velocity[5] = texture(vol, TransformPosition(pos + vec3( 0.0, 0.0, 1.0),v2t)).xyz;  
  velocity[6] = texture(vol, TransformPosition(pos + vec3( 0.0, 0.0,-1.0),v2t)).xyz;  
    
  direction = vec3(0.0);
  for(int i=0; i<7; i++)
  {
    direction = direction + 1.0/7.0*normalize(velocity[i]);
    magnitude = magnitude + 1.0/7.0*length(velocity[i]);
  }

  return direction * magnitude;
}

//----------------------------------------------------------------------------
// v[0..4]  - assumes linearly sampled texture
// dims     - dimensions of the volume 
// pos      - in voxel coordinates
// posTime  - in phases [0, 1, ..., N]
// seedTime - in phases [0, 1, ..., N] 
// returns velocity in patient coordinates + time in phases
//----------------------------------------------------------------------------
vec3 FetchVelocityLinearTemporalNearest(sampler3D d0, sampler3D d1, sampler3D d2, sampler3D d3, sampler3D d4,
                                        vec3 pos, float posTime, float seedTime, mat4 v2t)
{
  vec3 v = vec3(0.0);

  // The uniform samplers proved a range of volumes [phaseSeed-2, phaseSeed+2]
  // We need to compute the phase for the data to index the samples
  //int phaseData = (int(floor(posTime))-int(floor(seedTime))+2); // ?
  int phaseData = (int(posTime)-int(seedTime)+2);
  
  if(phaseData == 0) v.xyz = FetchVelocityLinear(d0, pos.xyz, v2t);
  if(phaseData == 1) v.xyz = FetchVelocityLinear(d1, pos.xyz, v2t);
  if(phaseData == 2) v.xyz = FetchVelocityLinear(d2, pos.xyz, v2t);
  if(phaseData == 3) v.xyz = FetchVelocityLinear(d3, pos.xyz, v2t);
  if(phaseData == 4) v.xyz = FetchVelocityLinear(d4, pos.xyz, v2t);

  return v;
}

//----------------------------------------------------------------------------
// v[0..4]  - assumes linearly sampled texture
// dims     - dimensions of the volume 
// pos      - in voxel coordinates
// posTime  - in phases [0, 1, ..., N]
// seedTime - in phases [0, 1, ..., N] 
// returns velocity in patient coordinates + time in phases
//----------------------------------------------------------------------------
vec3 FetchVelocityLinearTemporalNearest(sampler3D[5] series, vec3 pos, float posTime, float seedTime, mat4 v2t)
{
  return FetchVelocityLinearTemporalNearest(series[0],series[1],series[2],series[3],series[4],pos,posTime, seedTime,v2t);
}

//----------------------------------------------------------------------------
// v[0..4]  - assumes linearly sampled texture
// dims     - dimensions of the volume 
// pos      - in voxel coordinates
// posTime  - in phases
// seedTime - in phases
// returns velocity in patient coordinates + time in phases
//----------------------------------------------------------------------------
vec3 FetchVelocityLinearTemporalLinear(sampler3D d0, sampler3D d1, sampler3D d2, sampler3D d3, sampler3D d4,
                                       vec3 pos, float posTime, float seedTime, mat4 v2t)
{
  vec3  v, yl, yr;

  float tl = floor(posTime);
  float tr = (tl+1);
  float ti = posTime - float(tl);
  
  yl = FetchVelocityLinearTemporalNearest(d0,d1,d2,d3,d4, pos, tl, seedTime, v2t).xyz;
  yr = FetchVelocityLinearTemporalNearest(d0,d1,d2,d3,d4, pos, tr, seedTime, v2t).xyz;  

  // Linear interpolation
  v = mix(yl, yr, ti);
  
  return v;
}

//----------------------------------------------------------------------------
// v[0..4]  - assumes linearly sampled texture
// dims     - dimensions of the volume 
// pos      - in voxel coordinates
// posTime  - in phases
// seedTime - in phases
// returns velocity in patient coordinates + time in phases
//----------------------------------------------------------------------------
vec3 FetchVelocityLinearTemporalLinear(sampler3D[5] series, vec3 pos, float posTime, float seedTime, mat4 v2t)
{
  vec3  v, yl, yr;

  float tl = floor(posTime);
  float tr = (tl+1);
  float ti = posTime - float(tl);
  
  yl = FetchVelocityLinearTemporalNearest(series, pos, tl, seedTime, v2t).xyz;
  yr = FetchVelocityLinearTemporalNearest(series, pos, tr, seedTime, v2t).xyz;  

  // Linear interpolation
  v = mix(yl, yr, ti);
  
  return v;
}

//----------------------------------------------------------------------------
// v[0..2]  - assumes linearly sampled texture
// dims     - dimensions of the volume 
// pos      - in voxel coordinates
// returns velocity in patient coordinates + time in phases
//----------------------------------------------------------------------------
vec3 FetchVelocityTemporalLinear(sampler3D vol1, sampler3D vol2, vec3 pos, float posTime, mat4 v2t)
{
  vec3  v, yl, yr;

  float tl = floor(posTime);
  float tr = (tl+1);
  float ti = posTime - float(tl);
  
  yl = FetchVelocityLinear(vol1, pos.xyz, v2t);
  yr = FetchVelocityLinear(vol2, pos.xyz, v2t);

  // Linear interpolation
  v = mix(yl, yr, ti);
  
  return v;
}