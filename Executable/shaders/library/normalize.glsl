#version 330

//----------------------------------------------------------------------------
// Normalize to [0,1] range
// r = (d-c)/(b-a) (here we map to (0,1), so (d-c) = (1 - 0) = 1.0
// y = (x-a)*r+c
//----------------------------------------------------------------------------
vec3 NormalizeRange(vec3 value, vec2 range)
{
  vec3 result;

  result.r = (value.r - range.x) * ( 1.0 / (range.y - range.x) );
  result.g = (value.g - range.x) * ( 1.0 / (range.y - range.x) );
  result.b = (value.b - range.x) * ( 1.0 / (range.y - range.x) );

  return result;
}
