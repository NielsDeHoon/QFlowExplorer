#version 330

//----------------------------------------------------------------------------
// Default Front-to-Back compositing
//----------------------------------------------------------------------------
vec4 CompositeScalarFrontToBack(vec4 src, vec4 dst)
{
  vec4 result;
  
  // Accumulate RGB 
  result.rgb = dst.rgb  + (1.0 - dst.a) * src.rgb * src.a;


  // Accumulate Opacity
  result.a = dst.a + (1.0 - dst.a) * src.a;

  return result;

}

//----------------------------------------------------------------------------
// MIDA Front-to-Back compositing
//----------------------------------------------------------------------------
vec4 CompositeScalarFrontToBackMIDA(vec4 src, vec4 dst, float beta)
{
  vec4 result;
  
  // Accumulate RGB 
  result.rgb = beta * dst.rgb  + (1.0 - beta * dst.a) * src.rgb * src.a;


  // Accumulate Opacity
  result.a = beta * dst.a + (1.0 - beta * dst.a) * src.a;

  return result;

}
