#version 330

//----------------------------------------------------------------------------
vec3 TransformPosition(vec3 pos, mat4 matrix);

//----------------------------------------------------------------------------
vec3 FetchVelocityLinearTemporalLinear(sampler3D[5] series, vec3 pos, float posTime, float seedTime, mat4 v2t);

//----------------------------------------------------------------------------
vec3 FetchVelocityLinearTemporalLinear(sampler3D d0, sampler3D d1, sampler3D d2, sampler3D d3, sampler3D d4, vec3 pos, float posTime, float seedTime, mat4 v2t);

//----------------------------------------------------------------------------
// series         - linearly interpolated volumes [t-2,...t+2]
// seed.xyz       - in patient coordinates
// seed.w         - in phases
// seedInitial    - in phases 
// stepSize       - in phases
// stepDuration   - in milliseconds
// stepDirection  - trace direction (sign)
// stepModulation - speed up or slow down integration scheme in %
// p2v            - patient to voxel transformation matrix
// v2t            - voxel to texture transformation matrix
// returns new point in patient coordinates (with point.w is new time in phases)
vec4 IntegrateRungeKutta1(sampler3D d0,sampler3D d1,sampler3D d2,sampler3D d3,sampler3D d4, 
                          vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t)
{
  vec4  p;
  vec3  v, s;  
  float t, dt, m;  
  
  // Set the trace direction
  if(stepDirection >= 0) stepDirection =  1;
  else                   stepDirection = -1;
    
  // Determine initial time
  s = seed.xyz;
  t = seed.w;  

  // Compute time step in seconds  
  dt = stepDuration / 1000.0;  
  
  // Compute speed modulation
  m = stepModulation / 100.0; 

  // Get the velocity in patient coordinates (cm/s)
  p = vec4(TransformPosition(s, p2v), t);  
  v = stepDirection * m * FetchVelocityLinearTemporalLinear(d0,d1,d2,d3,d4,p.xyz,p.w,seedInitial,v2t);  

  // New position in patient coordinates (mm)
  s  = s + (10.0*v*dt);
  t  = t + stepDirection*stepSize;
  
  return vec4(s, t);  
}

//----------------------------------------------------------------------------
// series         - linearly interpolated volumes [t-2,...t+2]
// seed.xyz       - in patient coordinates
// seed.w         - in phases
// seedInitial    - in phases 
// stepSize       - in phases
// stepDuration   - in milliseconds
// stepDirection  - trace direction (sign)
// stepModulation - speed up or slow down integration scheme in %
// p2v            - patient to voxel transformation matrix
// v2t            - voxel to texture transformation matrix
// returns new point in patient coordinates (with point.w is new time in phases)
vec4 IntegrateRungeKutta1(sampler3D[5] series, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t)
{
  return IntegrateRungeKutta1(series[0],series[1],series[2],series[3],series[4],seed,seedInitial, stepSize, stepDuration, stepDirection, stepModulation, p2v,v2t);  
}

//----------------------------------------------------------------------------
// series         - linearly interpolated volumes [t-2,...t+2]
// seed.xyz       - in patient coordinates
// seed.w         - in phases
// seedInitial    - in phases 
// stepSize       - in phases
// stepDuration   - in milliseconds
// stepDirection  - trace direction (sign)
// stepModulation - speed up or slow down integration scheme in %
// p2v            - patient to voxel transformation matrix
// v2t            - voxel to texture transformation matrix
// returns new point in patient coordinates (with point.w is new time in phases)
vec4 IntegrateRungeKutta2(sampler3D d0,sampler3D d1,sampler3D d2,sampler3D d3,sampler3D d4, 
                          vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t)
{
  vec4  p, p1, p2;
  vec3  vi, vi_1, s; 
  vec3  k1, k2;
  float t, dt, m;
  
  // Set the trace direction
  if(stepDirection >= 0) stepDirection =  1;
  else                   stepDirection = -1;
      
  // Determine initial time
  s = seed.xyz;
  t = seed.w;

  // Compute time step in seconds  
  dt = stepDuration / 1000.0;
  
  // Compute speed modulation
  m = stepModulation / 100.0; 

  // Get the velocity in patient coordinates (cm/s)
  p  = vec4(TransformPosition(s, p2v), t);  
  vi = stepDirection * m * FetchVelocityLinearTemporalLinear(d0,d1,d2,d3,d4,p.xyz,p.w,seedInitial,v2t);    

  k1     = 10.0*vi*dt;  
  p1.xyz = s + 0.5*k1;  
  p1.w   = t + 0.5*stepSize;

  p      = vec4(TransformPosition(p1.xyz, p2v), p1.w);  
  vi_1   = stepDirection * m * FetchVelocityLinearTemporalLinear(d0,d1,d2,d3,d4,p.xyz,p.w,seedInitial,v2t);     

  k2     = 10.0*vi_1*dt;

  // New position in patient coordinates (mm)
  s  = s + k2;
  t  = t + stepDirection*stepSize;
 
  return vec4(s, t);  
}

//----------------------------------------------------------------------------
// series         - linearly interpolated volumes [t-2,...t+2]
// seed.xyz       - in patient coordinates
// seed.w         - in phases
// seedInitial    - in phases 
// stepSize       - in phases
// stepDuration   - in milliseconds
// stepDirection  - trace direction (sign)
// stepModulation - speed up or slow down integration scheme in %
// p2v            - patient to voxel transformation matrix
// v2t            - voxel to texture transformation matrix
// returns new point in patient coordinates (with point.w is new time in phases)
vec4 IntegrateRungeKutta2(sampler3D[5] series, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t)
{
  return IntegrateRungeKutta2(series[0], series[1], series[2], series[3], series[4], seed, seedInitial, stepSize, stepDuration, stepDirection, stepModulation, p2v, v2t);  
}

//----------------------------------------------------------------------------
// series         - linearly interpolated volumes [t-2,...t+2]
// seed.xyz       - in patient coordinates
// seed.w         - in phases
// seedInitial    - in phases 
// stepSize       - in phases
// stepDuration   - in milliseconds
// stepDirection  - trace direction (sign)
// stepModulation - speed up or slow down integration scheme in %
// p2v            - patient to voxel transformation matrix
// v2t            - voxel to texture transformation matrix
// returns new point in patient coordinates (with point.w is new time in phases)
vec4 IntegrateRungeKutta4(sampler3D d0,sampler3D d1,sampler3D d2,sampler3D d3,sampler3D d4, 
                          vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t)
{
  vec4  p, p1, p2, p3;
  vec3  vi, vi_1, vi_2, vi_3, s; 
  vec3  k1, k2, k3, k4;
  float t, dt, m;
  
  // Set the trace direction
  if(stepDirection >= 0) stepDirection =  1;
  else                   stepDirection = -1;
    
  // Determine initial time
  s = seed.xyz;
  t = seed.w;

  // Compute time step in seconds  
  dt = stepDuration / 1000.0;  
  
  // Compute speed modulation
  m = stepModulation / 100.0; 

  // Get the velocity in patient coordinates (cm/s)
  p  = vec4(TransformPosition(s, p2v), t);  
  vi = stepDirection * m * FetchVelocityLinearTemporalLinear(d0,d1,d2,d3,d4,p.xyz,p.w,seedInitial,v2t); 

  k1     = 10.0*vi*dt;  
  p1.xyz = s + 0.5*k1;  
  p1.w   = t + 0.5*stepSize;

  p      = vec4(TransformPosition(p1.xyz, p2v), p1.w);  
  vi_1   = stepDirection * m * FetchVelocityLinearTemporalLinear(d0,d1,d2,d3,d4,p.xyz,p.w,seedInitial,v2t);

  k2     = 10.0*vi_1*dt;
  p2.xyz = s + 0.5*k2;
  p2.w   = t + 0.5*stepSize;
  
  p      = vec4(TransformPosition(p2.xyz, p2v), p2.w);  
  vi_2   = stepDirection * m * FetchVelocityLinearTemporalLinear(d0,d1,d2,d3,d4,p.xyz,p.w,seedInitial,v2t);   

  k3     = 10.0*vi_2*dt;
  p3.xyz = s + k3;
  p3.w   = t + stepSize;

  p      = vec4(TransformPosition(p3.xyz, p2v), p3.w);  
  vi_3   = stepDirection * m * FetchVelocityLinearTemporalLinear(d0,d1,d2,d3,d4,p.xyz,p.w,seedInitial,v2t);  

  k4     = 10.0*vi_3*dt;

  // New position in patient coordinates (mm)
  s  = s + (k1 + k2*2.0 + k3*2.0 + k4)*(1.0/6.0);
  t  = t + stepDirection*stepSize;
 
  return vec4(s, t);  
}

//----------------------------------------------------------------------------
// series         - linearly interpolated volumes [t-2,...t+2]
// seed.xyz       - in patient coordinates
// seed.w         - in phases
// seedInitial    - in phases 
// stepSize       - in phases
// stepDuration   - in milliseconds
// stepDirection  - trace direction (sign)
// stepModulation - speed up or slow down integration scheme in %
// p2v            - patient to voxel transformation matrix
// v2t            - voxel to texture transformation matrix
// returns new point in patient coordinates (with point.w is new time in phases)
vec4 IntegrateRungeKutta4(sampler3D[5] series, vec4 seed, float seedInitial, float stepSize, float stepDuration, int stepDirection, int stepModulation, mat4 p2v, mat4 v2t)
{ 
  return IntegrateRungeKutta4(series[0], series[1], series[2], series[3], series[4], seed, seedInitial, stepSize, stepDuration, stepDirection, stepModulation, p2v, v2t);    
}