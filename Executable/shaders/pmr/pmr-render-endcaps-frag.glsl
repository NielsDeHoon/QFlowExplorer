#version 330

uniform sampler1D lineTransferFunction;

uniform vec4      light;
uniform vec4      lightDirection;
uniform int       lineShading;
uniform mat4      matrixModelView;
uniform mat4      matrixModelViewInverse;

in capAttrib
{
  float radiusDepth;
  vec3  normal;
  vec2  texcoord; // (x = arclength, y = left-right)
  vec4  attribs;  
  vec4  color;
} cap;

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
vec4 Phong(in vec4 col, in vec3 l, in vec3 n, in vec3 v, in float a, in float d, in vec2 s);

//----------------------------------------------------------------------------
void main()
{ 
  const float delta = 0.1;
  
  vec4 shaded;
  
  // determine if completely transparent
  if(cap.color.a <= delta) discard;
  
  // make a point sprite
  if(pow(cap.texcoord.x-0.5,2.0)+pow(cap.texcoord.y-0.5,2.0) > 0.25) discard;
    
  // initialize vectors  
  vec3 n = normalize(cap.normal);
  vec3 l = normalize(-lightDirection.xyz);
  vec3 e = vec3(0.0,0.0,1.0);

  // set the depth  
  //depth = gl_FragCoord.z + (1.0-yCoordNorm2)*(tube.radiusDepth-gl_FragCoord.z);
  
  // apply shading
  shaded = cap.color;  
  if(lineShading  < 3)
    shaded = Phong(cap.color, l, n, e, light.x, light.y, vec2(light.z,light.w));  
  shaded.a = cap.color.a;    
      
  qfe_FragColor = shaded;  
}