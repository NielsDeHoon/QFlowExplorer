#version 330

uniform sampler1D lineTransferFunction;

uniform float phaseSpan;
uniform int   phaseVisible;
uniform mat4  patientVoxelMatrix;
uniform float lineWidth;
uniform mat4  matrixModelView;
uniform mat4  matrixModelViewInverse;
uniform mat4  matrixProjection;

uniform vec2  dataRange;
uniform int   colorScale;
uniform int   colorType;
uniform vec3  colorUni;

layout(lines_adjacency )                in;
layout(triangle_strip, max_vertices=4)  out;

in seedAttrib
{
  vec4 position; // in patient coordinates (x,y,z,t)
  vec4 attribs;  // x = seedPhase, y = speed, z = z-val, w = cluster id
} seed[];

out tubeAttrib
{
  float radiusDepth;
  vec3  binormal;
  vec3  normal;
  vec2  texcoord; // (x = arclength, y = left-right)
  vec4  attribs;
  float phase;
  vec4  color;
} tube;

//----------------------------------------------------------------------------
// Copyright (c) 2012 Corey Tabaka
vec4 hsv_to_rgb(float h, float s, float v, float a)
{
	float c = v * s;
	h = mod((h * 6.0), 6.0);
	float x = c * (1.0 - abs(mod(h, 2.0) - 1.0));
	vec4 color;
 
	if (0.0 <= h && h < 1.0) {
		color = vec4(c, x, 0.0, a);
	} else if (1.0 <= h && h < 2.0) {
		color = vec4(x, c, 0.0, a);
	} else if (2.0 <= h && h < 3.0) {
		color = vec4(0.0, c, x, a);
	} else if (3.0 <= h && h < 4.0) {
		color = vec4(0.0, x, c, a);
	} else if (4.0 <= h && h < 5.0) {
		color = vec4(x, 0.0, c, a);
	} else if (5.0 <= h && h < 6.0) {
		color = vec4(c, 0.0, x, a);
	} else {
		color = vec4(0.0, 0.0, 0.0, a);
	}
 
	color.rgb += v - c;
 
	return color;
}

//----------------------------------------------------------------------------
void main()
{   
  const float delta = 0.001;

  vec3  eye, eyeWorld;
  vec4  posScreen;
  vec4  strip[4];
  vec4  lineEye[4];
  vec3  lineTangentAdj[2], lineNormalAdj[2];
  vec3  lineTangentEye[2], lineNormalEye[2];  
  vec4  vertexColor[2];  
  
  float speed[2];  
  float phaseSeed;
  float goldenRatio; 
  
  // initialize
  eye            = vec3(0.0,0.0,1.0);        
  eyeWorld       = normalize( matrixModelViewInverse*vec4(0.0,0.0,1.0,0.0)).xyz;   
  
  phaseSeed      = seed[1].attribs.x;
  goldenRatio    = (1.0 + sqrt(5.0))/2.0;
   
  // we assume to get a line with adjacency information
  // at this point we mainly compute the tangent for lighting
  // and pass on the vertex attributes
  
  // we assume to get a line with adjacency information  
  // we calculate the imposters in screen space  
  lineEye[0] = matrixModelView * vec4(seed[0].position.xyz,1.0);
  lineEye[1] = matrixModelView * vec4(seed[1].position.xyz,1.0);
  lineEye[2] = matrixModelView * vec4(seed[2].position.xyz,1.0);
  lineEye[3] = matrixModelView * vec4(seed[3].position.xyz,1.0);
  
   // compute tangent          
  lineTangentEye[0] = normalize(lineEye[1].xyz - lineEye[0].xyz); 
  lineTangentEye[1] = normalize(lineEye[2].xyz - lineEye[1].xyz);
  
  lineTangentAdj[0] = normalize(seed[1].position.xyz - seed[0].position.xyz); 
  lineTangentAdj[1] = normalize(seed[2].position.xyz - seed[1].position.xyz); 
  
  // correct tangent of the first part of the line
  if(length(lineEye[1] - lineEye[0]) < delta)    
  {
    lineTangentEye[0]  = lineTangentEye[1];
    lineTangentAdj[0]  = lineTangentAdj[1];
  }
    
  // compute normal  
  lineNormalEye[0]  = normalize(cross(eye, lineTangentEye[0]));
  lineNormalEye[1]  = normalize(cross(eye, lineTangentEye[1]));
  
  lineNormalAdj[0]  = normalize(cross(eyeWorld, lineTangentAdj[0]));
  lineNormalAdj[1]  = normalize(cross(eyeWorld, lineTangentAdj[1]));
 
  // compute color
  vertexColor[0] = vec4(colorUni,1.0);
  vertexColor[1] = vec4(colorUni,1.0);
 
  if(colorType == 1) // Pattern Type
  {
    vertexColor[0] = texture(lineTransferFunction, (seed[1].attribs.z+1.0)/2.0);      
    vertexColor[1] = vertexColor[0];  
  }
  
  if(colorType == 2) // Pattern Cluster ID
  {
    float value = seed[1].attribs.z*goldenRatio;
    float h     = value - floor(value);    
    vertexColor[0] = hsv_to_rgb(h, 0.5, 0.95, 1.0);
    vertexColor[1] = vertexColor[0];  
  }  
  
  if(colorType == 3) // Speed  
  {    
    speed[0]       = seed[1].attribs.y / max(abs(dataRange.x), abs(dataRange.y));
    //speed[0]       = speed[0] * (float(colorScale)/100.0);
    speed[1]       = seed[2].attribs.y / max(abs(dataRange.x), abs(dataRange.y));
    //speed[1]       = speed[1] * (float(colorScale)/100.0);
    
    vertexColor[0] = texture(lineTransferFunction, speed[0]);      
    vertexColor[1] = texture(lineTransferFunction, speed[1]); 
  }
  
  // create the strip
  strip[0] = vec4(lineEye[1].xyz - 0.5*lineWidth*lineNormalEye[0], 1.0);
  strip[1] = vec4(lineEye[1].xyz + 0.5*lineWidth*lineNormalEye[0], 1.0);
  strip[2] = vec4(lineEye[2].xyz - 0.5*lineWidth*lineNormalEye[1], 1.0);
  strip[3] = vec4(lineEye[2].xyz + 0.5*lineWidth*lineNormalEye[1], 1.0);
  
  bool isInSpan;
  bool isVisible; 
  
  isInSpan = ((phaseSpan != 0.0) && (seed[1].position.w > phaseSeed - 0.5*phaseSpan) && (seed[2].position.w < phaseSeed + 0.5*phaseSpan));
  
  if(phaseVisible == -1)
    isVisible = true;
  else 
    isVisible = (phaseVisible == round(seed[1].attribs.x));
  
  // keep within the user-defined phase span
  if(isInSpan && isVisible)
  {  
  
    // render the strip  
    posScreen          = matrixProjection * (strip[0] + vec4(0.0,0.0,0.5*lineWidth,0.0));
    tube.radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
    tube.binormal      = lineNormalAdj[0];    
    tube.texcoord      = vec2(0.0,0.0);    
    tube.attribs       = seed[1].attribs;
    tube.phase         = seed[1].position.w;
    tube.color         = vertexColor[0];
    gl_Position        = matrixProjection * strip[0];     
    EmitVertex();

    posScreen          = matrixProjection * (strip[1] + vec4(0.0,0.0,0.5*lineWidth,0.0));
    tube.radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
    tube.binormal      = lineNormalAdj[0];    
    tube.texcoord      = vec2(0.0,1.0);
    tube.attribs       = seed[1].attribs;
    tube.phase         = seed[1].position.w;
    tube.color         = vertexColor[0];
    gl_Position        = matrixProjection * strip[1];     
    EmitVertex();

    posScreen          = matrixProjection * (strip[2] + vec4(0.0,0.0,0.5*lineWidth,0.0));
    tube.radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
    tube.binormal      = lineNormalAdj[1];    
    tube.texcoord      = vec2(1.0,0.0);
    tube.attribs       = seed[2].attribs;
    tube.phase         = seed[2].position.w;
    tube.color         = vertexColor[1];
    gl_Position        = matrixProjection * strip[2];     
    EmitVertex();

    posScreen          = matrixProjection * (strip[3] + vec4(0.0,0.0,0.5*lineWidth,0.0));
    tube.radiusDepth   = 0.5 + 0.5*(posScreen.z/posScreen.w);
    tube.binormal      = lineNormalAdj[1];    
    tube.texcoord      = vec2(1.0,1.0);
    tube.attribs       = seed[2].attribs;
    tube.phase         = seed[2].position.w;
    tube.color         = vertexColor[1];
    gl_Position        = matrixProjection * strip[3];     
    EmitVertex(); 
  }
    
}