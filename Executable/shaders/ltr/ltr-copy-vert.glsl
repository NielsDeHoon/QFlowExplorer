#version 330

in  vec4 qfe_SeedPoints;

out vec4 qfe_LinePoints;

void main(void)
{     
  // Copy the value to other VBO
  qfe_LinePoints  = qfe_SeedPoints;       
}
