#version 330

uniform float phaseCurrent; // keep counting after wrap-around
uniform float phaseStart;
uniform float phaseEnd;
uniform mat4  patientVoxelMatrix;
uniform vec3  volumeDimensions;
uniform int   seedingType;
uniform float lineWidth;
//uniform int   lineHalo;
//uniform int   vertexCount;
uniform mat4  matrixModelView;
uniform mat4  matrixProjection;

layout(lines_adjacency )                in;
layout(triangle_strip, max_vertices=12) out;

in seedAttrib
{
  vec4 position; // in patient coordinates (x,y,z,t)
  vec4 attribs;  // x = velocity
} seed[];

out imposterAttrib
{
  float radiusDepth;
  vec3  binormal;
  vec3  texcoord; // (x = arclength, y = left-right, z = type)
  vec4  attribs;
} imposter;

//----------------------------------------------------------------------------
void main()
{    
  vec4  lineEye[4], strip[4], p;
  vec3  lineTangent, lineTangentAdj[2];
  vec3  lineNormalAdj[2];  
  vec3  eye;
  float type; 
  bool  visible;    
  float lineOffset;
    
  // initialize
  type                 = -1;
  eye                  = vec3(0.0,0.0,1.0);    
  imposter.attribs     = vec4(0.0);
  imposter.binormal    = vec3(0.0);
  imposter.radiusDepth = 0.0;
  
  lineOffset = 0.5;
    
  // we assume adjacency information, hence we have 4 vertices  
  // we calculate the imposters in screen space  
  lineEye[0] = matrixModelView * vec4(seed[0].position.xyz,1.0);
  lineEye[1] = matrixModelView * vec4(seed[1].position.xyz,1.0);
  lineEye[2] = matrixModelView * vec4(seed[2].position.xyz,1.0);
  lineEye[3] = matrixModelView * vec4(seed[3].position.xyz,1.0);

  // compute tangent          
  lineTangentAdj[0] = normalize(lineEye[1].xyz - lineEye[0].xyz); 
  lineTangentAdj[1] = normalize(lineEye[2].xyz - lineEye[1].xyz); 
   
  // correct tangent of the first part of the line
  if(length(lineEye[1] - lineEye[0]) < 0.01)    
    lineTangentAdj[0]  = lineTangentAdj[1];
  
  // compute normal  
  lineNormalAdj[0]  = normalize(cross(eye, lineTangentAdj[0]));
  lineNormalAdj[1]  = normalize(cross(eye, lineTangentAdj[1]));
    
  strip[0] = vec4(lineEye[1].xyz - 0.5   *lineWidth*lineNormalAdj[0], 1.0);
  strip[1] = vec4(lineEye[1].xyz + 0.5   *lineWidth*lineNormalAdj[0], 1.0);
  strip[2] = vec4(lineEye[2].xyz - lineOffset*lineWidth*lineNormalAdj[1], 1.0);
  strip[3] = vec4(lineEye[2].xyz + lineOffset*lineWidth*lineNormalAdj[1], 1.0);
      
  if((seed[1].position.w >= phaseStart) && (seed[1].position.w < phaseCurrent) && (seed[1].position.w < phaseEnd))      
    visible = true;
  else
    visible = false;
    
  if(seedingType == 0) // Dynamic seeding always visible
    visible = true;
    
  // -----------------------------
  // render the line strip
  // -----------------------------
  
  if(visible == true)
    type = 1;      
  
  p                    = matrixProjection * (strip[0] + vec4(0.0,0.0,lineWidth,0.0));
  imposter.radiusDepth = 0.5 + 0.5*(p.z/p.w);
  imposter.binormal    = lineNormalAdj[0];
  imposter.texcoord    = vec3(0.0,0.0,type);    
  imposter.attribs     = seed[1].attribs;
  gl_Position          = matrixProjection * strip[0];     
  EmitVertex();
    
  p                    = matrixProjection * (strip[1] + vec4(0.0,0.0,lineWidth,0.0));
  imposter.radiusDepth = 0.5 + 0.5*(p.z/p.w);
  imposter.binormal    = lineNormalAdj[0];
  imposter.texcoord    = vec3(0.0,1.0,type);
  imposter.attribs     = seed[1].attribs;
  gl_Position          = matrixProjection * strip[1];     
  EmitVertex();
  
  p                    = matrixProjection * (strip[2] + vec4(0.0,0.0,lineWidth,0.0));
  imposter.radiusDepth = 0.5 + 0.5*(p.z/p.w);
  imposter.binormal    = lineNormalAdj[1];
  imposter.texcoord    = vec3(0.0,0.0,type);
  imposter.attribs     = seed[2].attribs;
  gl_Position          = matrixProjection * strip[2];     
  EmitVertex();
  
  p                    = matrixProjection * (strip[3] + vec4(0.0,0.0,lineWidth,0.0));
  imposter.radiusDepth = 0.5 + 0.5*(p.z/p.w);
  imposter.binormal    = lineNormalAdj[1];
  imposter.texcoord    = vec3(0.0,1.0,type);
  imposter.attribs     = seed[2].attribs;
  gl_Position          = matrixProjection * strip[3];     
  EmitVertex();  
  
  EndPrimitive();
}