#version 120

varying vec4 posTexture;
varying vec4 posScreen;

uniform mat4 matrixModelViewProjection;

void main()
{
  gl_FrontColor  = gl_Color;  
  posTexture     = gl_TextureMatrix[0] * gl_MultiTexCoord0;
  posScreen      = matrixModelViewProjection * gl_Vertex;  
  gl_Position    = matrixModelViewProjection * gl_Vertex;
}