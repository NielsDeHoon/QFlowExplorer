#version 120

uniform sampler3D ivrDataSet;
uniform vec2      ivrDataRange;
uniform sampler1D transferFunction;
uniform sampler2D rayEnd;
uniform sampler2D rayEndDepth;
uniform sampler2D intersectColor;
uniform sampler2D intersectDepth;
uniform sampler2D lsm; // Lit Sphere Map
uniform sampler3D gradients;
uniform int       intersectAvailable;
uniform vec3      scaling;   // dimensions * voxel size (mm)
uniform float     stepSize;  // minimum of voxel size (mm)
uniform float	    dirTransExp;
uniform int       litSphereMapRendering;
uniform int       depthTransparency;
uniform int		    sampleGradient;
uniform float     maxGradLength;
uniform int       depthToneMapping;
uniform float     depthToneMappingFactor;
uniform mat4      voxelPatientMatrix;
uniform float     simpleOpacity;
uniform vec3      eyePos;

// Use clipping volume of interest or not
uniform int useVoI;
uniform mat4 voiTInv;
uniform vec4 voiCenter;

uniform vec3 dimensions;

varying vec4 posTexture;
varying vec4 posScreen;

vec4 CompositeScalarFrontToBack(vec4 src, vec4 dst);
vec3 GetDepthColor(float depth) {
  return mix(vec3(1.0, 0.5, 0.5), vec3(0.2, 0.2, 0.5), depth);
}

bool DiscardOutsideVoI(vec4 p) {
  vec4 pp = transpose(voiTInv)*(p - voiCenter);
  if (pp.x*pp.x + pp.y*pp.y + pp.z*pp.z > 1)
    return true;
  else
    return false;
}

void main() {    
  // Compute normalized device coordinate
  vec2 texCoord = (posScreen.xy/posScreen.w + 1.0) / 2.0;
              
  // Determine start and end of rays  
  vec3 start = posTexture.xyz;
  vec3 end = texture2D(rayEnd, texCoord.xy).xyz;
    
  start = clamp(start, 0.0, 1.0)*scaling;
  end = clamp(end, 0.0, 1.0)*scaling;
  
  vec4 dst = vec4(0.0);
  
  float depthFront = gl_FragCoord.z;
  float depthBack = texture2D(rayEndDepth, texCoord.xy).r;
  
  // If front is further than back, swap
  if (depthFront > depthBack) {
    float temp = depthFront;
    depthFront = depthBack;
    depthBack = temp;
    vec3 temp2 = start;
    start = end;
    end = temp2;
  }
  
  // Intersection with opaque geometry
  if (intersectAvailable == 1) {
    // Determine if ray hit opaque geometry
    float intersectD = texture2D(intersectDepth, texCoord.xy).r;
    if (intersectD != 1.0) {
      vec4 intersectC = texture2D(intersectColor, texCoord.xy);
      float intersectOffset = abs(clamp(intersectD, depthFront, depthBack) - depthFront)/abs(depthBack-depthFront);
      vec3 intersectPos = start + intersectOffset*(end - start);
      end = intersectPos;
    }
  }
  
  vec3 ray = start;
  vec3 rayVector = end - start;
  vec3 rayDir = normalize(rayVector);
  float rayMaxLength = length(rayVector);
  float rayOffset = 0.0;
  float maximumValue = ivrDataRange.x;
  
  // If there is no ray, discard the fragment
  if (rayMaxLength <= 0.01)
    discard;

  float stepSizeLocal = stepSize/3.0;
  vec3 previousNormal = texture3D(gradients, ray/scaling).xyz;
  for (int i=0; i <= int(rayMaxLength/stepSizeLocal); i++) {
    // Early ray termination if out of bounding box or ray is fully opaque
    if (rayOffset >= rayMaxLength || dst.a >= 1.0)
      break;
      
    // Clipping outside of volume of interest
    vec4 patientPos = voxelPatientMatrix * vec4( ray/dimensions, 1);
    if (useVoI == 1 && DiscardOutsideVoI(patientPos)) {
      ray += rayDir * stepSizeLocal;
      rayOffset += stepSizeLocal;
      continue;
    }
    
    // Retrieve sample color
    vec4 sample = texture3D(ivrDataSet, ray/scaling);
    float sampleValue = length(sample.xyz);
    float normalizedValue = sampleValue / ivrDataRange.y;
    vec4 sampleColor = texture1D(transferFunction, normalizedValue);
    float sampleOpacity = sampleColor.a;
    
    // Skip sample if it is too transparent
    if (sampleColor.a < 0.01) {
      ray += rayDir * stepSizeLocal;
      rayOffset += stepSizeLocal;
      continue;
    }

    // Retrieve depth color
    float depth = rayOffset / rayMaxLength;
    vec3 depthColor = GetDepthColor(depth);

    // Calculate normal
    vec4 gradient = texture3D(gradients, ray/scaling);
    float gradientLength = length(gradient.xyz);
    vec3 normal = normalize(gradient.xyz) * -1.0;
    
    // Calculate lit sphere map color
    vec3 eyeNormal = normalize(gl_NormalMatrix * normal);
    vec2 lsmEyeNormal = (eyeNormal.xy * 0.5) + vec2(0.5); // Project eye normal to xy-plane and map it to [0, 1]
    vec4 litSphereMapColor = texture2D(lsm, lsmEyeNormal);
    
    // Calculate cosine of angle between normal and ray direction
    float nDotV = dot(normal, -rayDir);
    if (nDotV < 0.0)
      nDotV = -nDotV;
    
    // Determine whether sample is on contour
    float T = 0.002;
    float kv = dot(normal, previousNormal) / stepSizeLocal; // approx. for curvature along view direction
    float cul = sqrt(T*kv*(2 - T*kv)); // contour upper limit
    bool onContour = nDotV <= cul;
    previousNormal = normal;
    
    // Calculate transparency according to "Style Transfer Functions for Illustrative Volume Rendering"
    float illustrativeOpacity = pow(sampleOpacity, 0.5 + max(0.0, nDotV - cul) * (1.0 - gradientLength/maxGradLength));
    float directionalOpacity = pow(1.0 - nDotV, dirTransExp);
    
    // Determine final color and opacity
    float depthOpacity = 1.0;
    if (depthTransparency == 1)
      depthOpacity = (1.0 - depth)*(1.0 - depth);
    float opacity = illustrativeOpacity * directionalOpacity * depthOpacity;
    vec4 color = vec4(sampleColor.rgb, opacity);
    if (litSphereMapRendering == 1)
      color.rgb = litSphereMapColor.rgb;
    if (depthToneMapping == 1)
      color.rgb = mix(color.rgb, depthColor.rgb, depthToneMappingFactor);
    
    // Front-to-back compositing
    dst = CompositeScalarFrontToBack(color, dst);
    
    ray += rayDir * stepSizeLocal;
    rayOffset += stepSizeLocal;
  }  
  
  if (intersectAvailable == 1)
    dst = mix(dst, texture2D(intersectColor, texCoord.xy), 1.0 - simpleOpacity * dst.a);
	
  gl_FragColor = dst;
}