#version 400

#extension GL_EXT_gpu_shader4 : enable

uniform int renderSurface;
uniform int selected;

uniform vec3 color;
uniform vec3 viewDir;

uniform float lambda2;

uniform vec3 axis2;

uniform int planar;

layout(location = 0) out vec4 fragColor;

in fragmentData
{
    float distance_to_top;
    vec3 normal;
} vertex_properties;

void main() 
{ 
	bool renderCylinderSurface = false;
	if(renderSurface != 0)
	{
		renderCylinderSurface = true;
	}
	
	bool is_selected = false;
	if(selected != 0)
	{
		is_selected = true;
	}
	
	bool is_planar = false;
	if(planar != 0)
	{
		is_planar = true;
	}

	float NdotL = dot(vertex_properties.normal, viewDir);
		
	fragColor.a = 1.0;
	fragColor.rgb = color;
		
	float dist = vertex_properties.distance_to_top;
	float line_thickness = 1.0/lambda2;
	
	bool is_contour = false;
	
	if(dist > 1.0-line_thickness || dist < line_thickness)
	{
		is_contour = true;
	}	
	
	if(is_planar && dist > 1.0-line_thickness)
	{
		is_contour = false;
	}
		
	//compare axis2 and viewDir, if almost paralel we don't need the contour (only the rings on top and bottom)
	float viewAngleCheck = abs(dot(axis2, viewDir));
	
	float contour_thresshold = 0.25;
	if(viewAngleCheck>0.9)
	{
		contour_thresshold = 3.0*(1.0 - viewAngleCheck);
	}

	if(NdotL<0.0 && abs(NdotL) + 0.1*viewAngleCheck<contour_thresshold  && !is_planar)
	{
		is_contour = true;
	}
	
	if(renderCylinderSurface && is_contour)
	{	
		fragColor.rgb = vec3(0.1);
		if(is_selected)
		{
			fragColor.rgb = vec3(1.0, 0.0, 0.0);
		}
	}
	
	if(renderCylinderSurface && !is_contour)
	{
		if(is_selected && mod(gl_FragCoord.x + gl_FragCoord.y, 2.0) == 0.0) 
		{
			discard;
		}
	}
	
	if(!renderCylinderSurface && !is_contour)
	{
		discard;
	}
	
	//shade:		
	fragColor *= 0.5+2.0*pow(abs(NdotL),5);
	
	if(NdotL>0.0) //inside of cylinder
	fragColor /= 4.0;
}