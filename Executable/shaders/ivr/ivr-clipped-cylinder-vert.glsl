#version 400
#extension GL_EXT_gpu_shader4 : enable

in vec4 pos;

void main() 
{	
	gl_Position = vec4(pos);
}