#version 400
#extension GL_EXT_gpu_shader4 : enable

layout(points) in;
layout(triangle_strip, max_vertices = 128) out; //256 = 4 *32 (nr of steps)

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform vec3 color;

uniform vec4 center;

uniform vec3 axis1;
uniform vec3 axis2;
uniform vec3 axis3;

uniform float lambda1;
uniform float lambda2;
uniform float lambda3;

uniform int useClipPlane;

out fragmentData
{
    float distance_to_top;
    vec3 normal;
}vertex_properties;

vec4 projectVertex(vec4 input)
{
	return projectionMatrix * modelViewMatrix * input;
}

void main() 
{	
	int steps = 32;
	float angle = 0.0f;
	
	float w = gl_in[0].gl_Position.w;
	w = 1.0;
	
	vec3 clip_plane_offset = vec3(0.0);
	if(useClipPlane == 0)
	{
		clip_plane_offset = -lambda2*axis2;
	}
	
	vec3 cap_top_center = center.xyz+lambda2*axis2;
	vec3 cap_bottom_center = center.xyz+clip_plane_offset;
	
	for(int step = 0; step < steps; step++)
	{
		angle = 2.0 * 3.14 * step/steps;
		vec3 direction = lambda1*axis1*cos(angle) + lambda3*axis3*sin(angle);
		
		vec3 normal = normalize(direction);
		
		vec3 point1 = center.xyz+direction+clip_plane_offset;
		vertex_properties.distance_to_top = 0.0;
		vertex_properties.normal = normal;
		gl_Position = projectVertex(vec4(point1, w));
		EmitVertex();
		
		vec3 point2 = center.xyz+direction+lambda2*axis2;		
		vertex_properties.distance_to_top = 1.0;
		vertex_properties.normal = normal;
		gl_Position = projectVertex(vec4(point2, w));
		EmitVertex();
		
		angle = 2.0 * 3.14 * ((step+1)%steps)/steps;
		direction = lambda1*axis1*cos(angle) + lambda3*axis3*sin(angle);
		
		normal = normalize(direction);
		
		vec3 point4 = center.xyz+direction+clip_plane_offset;
		vertex_properties.distance_to_top = 0.0;
		vertex_properties.normal = normal;
		gl_Position = projectVertex(vec4(point4, w));
		EmitVertex();
		
		vec3 point3 = center.xyz+direction+lambda2*axis2;
		vertex_properties.distance_to_top = 1.0;
		vertex_properties.normal = normal;
		gl_Position = projectVertex(vec4(point3, w));
		EmitVertex();
				
		EndPrimitive();
	}	
}