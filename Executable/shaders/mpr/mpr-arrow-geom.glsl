// mpr-arrow-geom
#version 330

uniform sampler3D planeDataSet;
uniform vec2      planeDataRange;
uniform int       planeDataRepresentation;
uniform vec3      planeNormal;
uniform float     arrowSpacing;
uniform float     arrowScaling;
uniform int       speedThreshold;
uniform sampler1D transferFunction;

uniform mat4      matrixModelViewProjection;

layout(points) in;
layout(triangle_strip, max_vertices=6) out;

in seedAttrib
{
  vec3 patientCoord;
  vec3 textureCoord;
} seed[];

out vec4 frontColor;

//----------------------------------------------------------------------------
// Normalize to [0,1] range
float NormalizeRange(float value, vec2 range)
{
  float result;

  result = (value - range.x) * ( 1.0 / (range.y - range.x) );
  
  return result;
}

//----------------------------------------------------------------------------
vec3  NormalizeRange(vec3 value, vec2 range)
{
  vec3 result;

  result.r = (value.r - range.x) * ( 1.0 / (range.y - range.x) );
  result.g = (value.g - range.x) * ( 1.0 / (range.y - range.x) );
  result.b = (value.b - range.x) * ( 1.0 / (range.y - range.x) );

  return result;

}

//----------------------------------------------------------------------------
vec3  FetchVectorValue(sampler3D vol, vec3 pos)
{
  return texture(vol, pos).rgb;
}

//----------------------------------------------------------------------------
vec3  FetchTransferFunction(sampler1D tf, float value)
{
  return texture(tf, value).rgb;
}

//----------------------------------------------------------------------------
// a||n = |a| n * a * n with a, n both normalized 
vec3  FetchVectorInPlane(vec3 vec, vec3 normal)
{  
  float al;
  vec3  a, n, v;
  
  al = length(vec);
  a  = normalize(vec);
  n  = normalize(normal);
  
  v  = cross(al*n, cross(a, n));  
  
  return v;
}

//----------------------------------------------------------------------------
void main(void)
{   
  float triangleLength, triangleBase;
  vec3  p0, p1, p2, p3, u, v, w, zf;   
  vec3  color;
  float alpha;
  vec3  samplePos;
  float sampleScalar;
  vec3  sampleVector;
  vec3  vectorInPlane;
  vec2  dataRange;  
  
  dataRange      = vec2(0.0, max(abs(planeDataRange.x),abs(planeDataRange.y)));  
  samplePos      = seed[0].textureCoord;   
  sampleVector   = FetchVectorValue(planeDataSet, samplePos);   
  sampleScalar   = NormalizeRange(length(sampleVector), dataRange);
  
  if(length(sampleVector) >= float(speedThreshold))
  {
    vectorInPlane  = FetchVectorInPlane(sampleVector, planeNormal);     

    triangleLength = (length(vectorInPlane) / dataRange[1]) * arrowSpacing * arrowScaling;    
    triangleBase   = 0.6 * triangleLength;

    // Determine the points of the arrow head
    u              = normalize(vectorInPlane)         * triangleLength;
    v              = normalize(cross(u, planeNormal)) * (0.5*triangleBase);
    w              = normalize(planeNormal)           * (0.5*triangleBase);
    
    p0             = seed[0].patientCoord - (0.5*u);    
    p1             = p0 + v;
    p2             = p0 + u;
    p3             = p0 - v;

    // Determine the color
    if(planeDataRepresentation == 0)    
      color = FetchTransferFunction(transferFunction, sampleScalar);  
    else
      color = vec3(1.0,1.0,1.0);
      
    alpha = 0.4;

    // Small offset to overcome z-fighting with the plane
    zf             = 0.01*planeNormal;

    frontColor    = vec4(color,alpha);
    gl_Position   = matrixModelViewProjection * vec4(p1+zf,1.0);  
    EmitVertex();

    frontColor    = vec4(color,alpha);
    gl_Position   = matrixModelViewProjection * vec4(p2+zf,1.0);
    EmitVertex();

    frontColor    = vec4(color,alpha);
    gl_Position   = matrixModelViewProjection * vec4(p3+zf,1.0);
    EmitVertex();    
    

    frontColor    = vec4(color,alpha);
    gl_Position   = matrixModelViewProjection * vec4(p1-zf,1.0);  
    EmitVertex();

    frontColor    = vec4(color,alpha);
    gl_Position   = matrixModelViewProjection * vec4(p2-zf,1.0);
    EmitVertex();

    frontColor    = vec4(color,alpha);
    gl_Position   = matrixModelViewProjection * vec4(p3-zf,1.0);
    EmitVertex();

    EndPrimitive();
  }
}
 