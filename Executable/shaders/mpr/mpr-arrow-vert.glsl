// mpr-arrow-vert
#version 330

uniform mat4 matrixTexturePatient;
uniform mat4 matrixModelViewProjection;

in vec3 qfe_Vertex;

out seedAttrib
{
  vec3 patientCoord;
  vec3 textureCoord;
} seed;

void main()
{
  seed.patientCoord = (matrixTexturePatient * vec4(qfe_Vertex,1.0)).xyz;
  seed.textureCoord = qfe_Vertex.xyz;
}