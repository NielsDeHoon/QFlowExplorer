// mpr-dyn-frag.glsl
#version 330

uniform sampler3D planeDataSet1;
uniform sampler3D planeDataSet2;
uniform int       planeDataComponent;
uniform int       planeDataComponentCount;
uniform int       planeDataRepresentation;
uniform vec2      planeDataRange1;
uniform vec2      planeDataRange2;
uniform vec3      planeNormal;
uniform vec3      voxelSize;
uniform sampler1D transferFunction;
uniform float     triggerTime;
uniform int       balance;
uniform float     contrast;
uniform float     brightness;
uniform float     opacity;
uniform mat4      matrixModelViewProjectionInverse;

in vec3 texCoord[];

out vec4 qfe_FragColor;

//----------------------------------------------------------------------------
float NormalizeRange(float value, vec2 range);

//----------------------------------------------------------------------------
vec3  NormalizeRange(vec3 value, vec2 range);

//----------------------------------------------------------------------------
float FetchScalarValue(sampler3D vol, vec3 pos);

//----------------------------------------------------------------------------
vec3  FetchVectorValue(sampler3D vol, vec3 pos);

//----------------------------------------------------------------------------
vec3  FetchTransferFunction(sampler1D tf, float value);

//----------------------------------------------------------------------------
float FetchDivergence(sampler3D vol, vec3 pos, vec3 stepsize);

//----------------------------------------------------------------------------
vec3  FetchCurl(sampler3D vol, vec3 pos, vec3 stepsize);

//----------------------------------------------------------------------------
float FetchLPC(sampler3D vol, vec3 pos, vec3 stepsize);

//----------------------------------------------------------------------------
float FetchEDC(sampler3D vol, vec3 pos, vec3 stepsize);

//----------------------------------------------------------------------------
mat3  FetchStructureTensor(sampler3D vol, vec3 pos);

//----------------------------------------------------------------------------
mat3  FetchStructureTensorAvg(sampler3D vol, vec3 pos, vec3 stepsize, int neighbourlevel);

//----------------------------------------------------------------------------
vec3  FetchEigenValuesSymmetric(mat3 m);

//----------------------------------------------------------------------------
// Note: second data set is always pca-p
//----------------------------------------------------------------------------
void main()
{
  vec4  color, color1, color2;
  vec2  dataRange1, dataRange2;
  vec3  samplePos1, samplePos2; 
  vec3  sampleVector1, sampleVector2;
  float sampleScalar1 = 0.0, sampleScalar2 = 0.0;   
  float fraction;

  color    = vec4(0.0);
  color1   = vec4(0.0);
  color2   = vec4(0.0);
  fraction = float(balance) / 100.0;
 
  dataRange1.xy = planeDataRange1.xy;
  dataRange2.xy = planeDataRange2.xy;
  if(planeDataComponent == 0)
  {
    dataRange1 = vec2(0.0, max(abs(planeDataRange1.x),abs(planeDataRange1.y))); 
    dataRange2 = vec2(0.0, max(abs(planeDataRange2.x),abs(planeDataRange2.y))); 
  }  
      
  samplePos1    = texCoord[0].xyz;   
  samplePos2    = texCoord[1].xyz;   
  
  if(any(lessThan(texCoord[0].xyz, vec3(0.0))) || any(greaterThan(texCoord[0].xyz, vec3(1.0))))
    discard;   
    
  // Determine the selected dataset rendering  
  if(planeDataComponentCount == 3 || planeDataComponentCount == 9)
  {
    sampleVector1 = NormalizeRange(FetchVectorValue(planeDataSet1, samplePos1), dataRange1);     
  
    if(planeDataComponent == 0) // magnitude
      sampleScalar1 = length(sampleVector1);  
    if(planeDataComponent == 1) // x-component
      sampleScalar1 = sampleVector1.r;       
    if(planeDataComponent == 2) // y-component
      sampleScalar1 = sampleVector1.g;   
    if(planeDataComponent == 3) // z-component
      sampleScalar1 = sampleVector1.b;       
    if(planeDataComponent == 4) // divergence
      sampleScalar1 = NormalizeRange(FetchDivergence(planeDataSet1, samplePos1, voxelSize), dataRange1);      
    if(planeDataComponent == 5) // curl x
      sampleScalar1 = NormalizeRange(FetchCurl(planeDataSet1, samplePos1, voxelSize).x, dataRange1);    
    if(planeDataComponent == 6) // curl y
      sampleScalar1 = NormalizeRange(FetchCurl(planeDataSet1, samplePos1, voxelSize).y, dataRange1);      
    if(planeDataComponent == 7) // curl z
      sampleScalar1 = NormalizeRange(FetchCurl(planeDataSet1, samplePos1, voxelSize).z, dataRange1);      
    if(planeDataComponent == 8) // coherence (LPC)
      sampleScalar1 = FetchLPC(planeDataSet1, samplePos1, voxelSize);      
    if(planeDataComponent == 9) // coherence (EDC)         
      sampleScalar1 = FetchEDC(planeDataSet1, samplePos1, voxelSize);
  }
  else 
    sampleScalar1 = NormalizeRange(FetchScalarValue(planeDataSet1, samplePos1), dataRange1); 
    
  // Determine the pca-p dataset rendering
  sampleVector2 = NormalizeRange(FetchVectorValue(planeDataSet2, samplePos2), dataRange2); 
  
  if(planeDataComponent == 0) // magnitude
    sampleScalar2 = length(sampleVector2);  
  if(planeDataComponent == 1) // x-component
    sampleScalar2 = sampleVector2.r;       
  if(planeDataComponent == 2) // y-component
    sampleScalar2 = sampleVector2.g;   
  if(planeDataComponent == 3) // z-component
    sampleScalar2 = sampleVector2.b; 
  if(planeDataComponent == 4) // divergence
    sampleScalar2 = NormalizeRange(FetchDivergence(planeDataSet2, samplePos2, voxelSize), dataRange2);
  if(planeDataComponent == 5) // curl x
    sampleScalar2 = NormalizeRange(FetchCurl(planeDataSet2, samplePos2, voxelSize).x, dataRange2);
  if(planeDataComponent == 6) // curl y
    sampleScalar2 = NormalizeRange(FetchCurl(planeDataSet2, samplePos2, voxelSize).y, dataRange2);
  if(planeDataComponent == 7) // curl z
    sampleScalar2 = NormalizeRange(FetchCurl(planeDataSet2, samplePos2, voxelSize).z, dataRange2);
  if(planeDataComponent == 8) // coherence (LPC)
    sampleScalar2 = FetchLPC(planeDataSet2, samplePos2, voxelSize);
  if(planeDataComponent == 9) // coherence (EDC)
    sampleScalar2 = FetchEDC(planeDataSet2, samplePos2, voxelSize);  

  // Get the appearance right
  if(planeDataRepresentation == 0) // gray
  {
    color1 = vec4(vec3(sampleScalar1), 1.0);    
    color2 = vec4(vec3(sampleScalar2), 1.0);    
  }    
  if(planeDataRepresentation == 1) // color
  {
    color1 = vec4(FetchTransferFunction(transferFunction,sampleScalar1), 1.0);
    color2 = vec4(FetchTransferFunction(transferFunction,sampleScalar2), 1.0);
  }   
  if(planeDataRepresentation == 2) // red-blue
  {
    float angle, throughplane;
    vec3  col;
    vec3  eye;
    
    eye = normalize(matrixModelViewProjectionInverse*vec4(0.0,0.0,1.0,0.0)).xyz;  
    
    sampleVector2 = NormalizeRange(FetchVectorValue(planeDataSet2, samplePos2), planeDataRange2.xy);   
    sampleVector2 = (sampleVector2*2.0)-1.0;
    angle         = dot(normalize(sampleVector2.xyz), eye);
    throughplane  = abs(dot(normalize(planeNormal.xyz), sampleVector2));
      
    if(angle <= 0.0)
    {
      col.r = throughplane;
      col.b = 0.0;
    }
    else
    {
      col.r = 0.0;    
      col.b = throughplane;
    }        
    col.g = 0.0;
    
    color1 = vec4(col, 1.0);    
    color2 = vec4(col, 1.0);    
  }
  
  // Mix the fragment colors
  color = vec4((1.0-fraction)*color1 + fraction*color2);  
  
  // Final adjustments    
  // Apply contrast
  float c;
  c = (1.0/(1.0-contrast));

  color.rgb = ((color.rgb - 0.5) * c) + 0.5;

  // Apply brightness
  color.rgb += brightness;    
  
  color.a    = opacity; 
    
  qfe_FragColor = color;  
}